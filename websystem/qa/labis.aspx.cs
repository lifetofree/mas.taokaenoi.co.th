﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using MessagingToolkit.QRCode.Codec;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

public partial class websystem_qa_labis : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- labis --// 
    static string _urlQaGetplace = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetplace"];
    static string _urlQaGetDateSampleList = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDateSampleList"];
    static string _urlQaGetMaterialList = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetMaterialList"];
    static string _urlQaGetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetail"];
    static string _urlQaGetSetTestDetail = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSetTestDetail"];
    static string _urlQaGetM0Lab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetM0Lab"];

    static string _urlQaSetDocument = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetDocument"];
    static string _urlQaGetU0Document = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetU0Document"];
    static string _urlQaGetDetailU0Document = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDetailU0Document"];
    static string _urlQaGetTestDetailU0Document = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetailU0Document"];
    static string _urlQaGetSampleView = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSampleView"];
    static string _urlQaGetTestSample = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestSample"];
    static string _urlQaGetHistoryDoc = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetHistoryDoc"];
    static string _urlQaGetDecision2 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDecision2"];
    static string _urlQaSetAppoveNode2 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetAppoveNode2"];
    static string _urlQaGetDocActor4 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDocActor4"];
    static string _urlQaGetTestDetailLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetTestDetailLab"];
    static string _urlQaGetDecision3 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDecision3"];
    static string _urlQaGetDecision4 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDecision4"];
    static string _urlQaGetSampleInLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSampleInLab"];
    static string _urlQaSetAppoveNode3 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetAppoveNode3"];
    static string _urlQaSetApproveNode4 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetApproveNode4"];
    static string _urlQaGetLabTypeOnSelectNode3 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetLabTypeOnSelectNode3"];
    static string _urlQaGetHistorySample = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetHistorySample"];
    static string _urlQaGetDecision7 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDecision7"];
    static string _urlQaSetApproveNode7 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetApproveNode7"];
    static string _urlQaGetDecision8 = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDecision8"];
    static string _urlQaSetApproveNode8 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetApproveNode8"];
    static string _urlQaSetApproveNode11 = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetApproveNode11"];
    static string _urlQaGetSearchSampleCode = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSearchSampleCode"];
    static string _urlQaGetSelectForm = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSelectForm"];
    static string _urlQaSetRecordResultTest = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetRecordResultTest"];
    static string _urlQaGetDetailResult = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDetailResult"];
    static string _urlQaGetRecordResultTest = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetRecordResultTest"];
    static string _urlQaSetImportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetImportExcel"];
    static string _urlQaSearchDetailIndex = _serviceUrl + ConfigurationManager.AppSettings["urlQaSearchDetailIndex"];
    static string _urlQaSearchDetailInLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaSearchDetailInLab"];
    static string _urlQaGetDateSampleListWhere = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDateSampleListWhere"];
    static string _urlQaSearchDetailInLabNew = _serviceUrl + ConfigurationManager.AppSettings["urlQaSearchDetailInLabNew"];
    static string _urlQaGetViewRsecReference = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetViewRsecReference"];
    static string _urlQaGetExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetExportExcel"];
    static string _urlQaGetDetailExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDetailExportExcel"];
    static string _urlQaGetDetailResultExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDetailResultExportExcel"];
    static string _urlQaGetDetailForUpdateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDetailForUpdateDocument"];
    static string _urlQaSetDetailForUpdateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetDetailForUpdateDocument"];
    static string _urlQaSetUpdateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetUpdateDocument"];
    static string _urlQaGetDataExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetDataExportExcel"];
    static string _urlQaGetSelectPlaceLab = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSelectPlaceLab"];
    static string _urlQaGetSearchDateForPrint = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSearchDateForPrint"];
    static string _urlQaGetSearchDataExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetSearchDataExportExcel"];

    //-- labis --//

    //-- key sample code --//
    static string _keysamplecode = _serviceUrl + ConfigurationManager.AppSettings["keysamplecode"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    //datable department
    string department_data = "";
    string Temp_department_data = "";

    //datable departmentexcel
    string department_dataexcel = "";
    string Temp_department_dataexcel = "";

    //datable sample
    string sample_data = "";
    string Temp_sample_data = "";

    // rdept permission
    int[] rdept_qmr = { 20 }; //QMR:26
    ArrayList _importSample = new ArrayList() { "40", "47" };


    //desision 
    string DECISION_APPROVE = "8";
    string DECISION_NOTAPPROVE = "9";
    //set id type lab 
    int ExternalID = 2;
    int ExternalLabStatus = 13;
    int ExternalCondition = 4;
    int ExternalM0LabIdx = 5;
    int Not_SentSample = 20;

    string[] color = new string[12];


    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());



        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        foreach (int item in rdept_qmr)
        {
            if (_dataEmployee.employee_list[0].rdept_idx == item)
            {
                _flag_qmr = true;
                break;
            }
        }
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

        }

        linkBtnTrigger(lbCreate);
        linkBtnTrigger(lbLabResult);


    }

    #region import file excel
    protected void actionImport(string FilePath, string Extension, string isHDR, string fileName)
    {
        // fvEmpDetail
        HiddenField hfEmpOrgIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
        HiddenField hfEmpRdeptIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
        HiddenField hfEmpRsecIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
        // fvEmpDetail

        // fvActor1Node1
        HiddenField hfActorIDX_excel = (HiddenField)fvActor1Node1.FindControl("hfActorIDX");
        HiddenField hfNodeIDX_excel = (HiddenField)fvActor1Node1.FindControl("hfNodeIDX");
        HiddenField hfU0IDX_excel = (HiddenField)fvActor1Node1.FindControl("hfU0IDX");
        DropDownList ddlPlace_excel = (DropDownList)fvActor1Node1.FindControl("ddlPlace");
        CheckBoxList chk_test_excel = (CheckBoxList)fvActor1Node1.FindControl("chk_test");
        CheckBoxList chk_settest_excel = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
        TextBox chk_select_excel = (TextBox)fvActor1Node1.FindControl("chk_select");

        ///
        FormView FvAddImport_excel_show = (FormView)docCreate.FindControl("FvAddImport");
        //Panel Dept_Excel = (Panel)FvAddImport_excel.FindControl("Dept_Excel");
        //DropDownList ddlOrg_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
        //DropDownList ddlDept_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
        //DropDownList ddlSec_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");
        GridView GvExcel_Show = (GridView)FvAddImport_excel_show.FindControl("GvExcel_Show");


        //

        int actor_idx_excel = int.TryParse(hfActorIDX_excel.Value, out _default_int) ? int.Parse(hfActorIDX_excel.Value) : _default_int;
        int node_idx_excel = int.TryParse(hfNodeIDX_excel.Value, out _default_int) ? int.Parse(hfNodeIDX_excel.Value) : _default_int;

        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, FilePath, isHDR);
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();

        data_qa _data_qa_import = new data_qa();

        _data_qa_import.qa_lab_u0_qalab_list = new qa_lab_u0_qalab[1];
        _data_qa_import.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        _data_qa_import.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
        _data_qa_import.qa_lab_u3_qalab_list = new qa_lab_u3_qalab[1];
        _data_qa_import.qa_lab_u4_qalab_list = new qa_lab_u4_qalab[1];

        qa_lab_u0_qalab _data_u0_importexcel = new qa_lab_u0_qalab();
        qa_lab_u1_qalab _data_u1_importexcel = new qa_lab_u1_qalab();
        qa_lab_u2_qalab _data_u2_importexcel = new qa_lab_u2_qalab();
        qa_lab_u3_qalab _data_u3_importexcel = new qa_lab_u3_qalab();
        qa_lab_u4_qalab _data_u4_importexcel = new qa_lab_u4_qalab();

        // u0_qalab
        _data_u0_importexcel.unidx = int.TryParse(hfU0IDX_excel.Value, out _default_int) ? int.Parse(hfU0IDX_excel.Value) : _default_int;//dt.Rows.Count;
        _data_u0_importexcel.place_idx = int.TryParse(ddlPlace_excel.SelectedValue, out _default_int) ? int.Parse(ddlPlace_excel.SelectedValue) : _default_int;
        _data_u0_importexcel.cemp_idx = _emp_idx;
        _data_u0_importexcel.cemp_org_idx = int.TryParse(hfEmpOrgIDX_excel.Value, out _default_int) ? int.Parse(hfEmpOrgIDX_excel.Value) : _default_int;
        _data_u0_importexcel.cemp_rdept_idx = int.TryParse(hfEmpRdeptIDX_excel.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX_excel.Value) : _default_int;
        _data_u0_importexcel.cemp_rsec_idx = int.TryParse(hfEmpRsecIDX_excel.Value, out _default_int) ? int.Parse(hfEmpRsecIDX_excel.Value) : _default_int;
        _data_u0_importexcel.staidx = 1; // status document


        // u1_qalab
        var count_alert_u1 = dt.Rows.Count;

        var u0_waterimport = new qa_lab_u1_qalab[dt.Rows.Count];

        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            //if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
            //{
            u0_waterimport[i] = new qa_lab_u1_qalab();
            u0_waterimport[i].customer_name = dt.Rows[i][0].ToString().Trim();
            u0_waterimport[i].other_details = dt.Rows[i][1].ToString().Trim();//dr_sample["other_details"].ToString();
            u0_waterimport[i].rdept_idx_production_name = dt.Rows[i][2].ToString().Trim();//dr_sample["other_details"].ToString();
            u0_waterimport[i].rsec_idx_production_name = dt.Rows[i][3].ToString().Trim();//dr_sample["other_details"].ToString();
            u0_waterimport[i].shift_time = dt.Rows[i][4].ToString().Trim();//dr_sample["other_details"].ToString();

            i++;
            //}
        }

        ViewState["vsExcelSwab_show"] = u0_waterimport;
        setGridData(GvExcel_Show, ViewState["vsExcelSwab_show"]);


        // u2_qalab   
        //test detail
        var u2_doc_data_excel = new qa_lab_u2_qalab[chk_test_excel.Items.Count];
        int sumcheck_excel = 0;
        int i_excel = 0;

        List<String> AddoingList_excel = new List<string>();
        foreach (ListItem chkListFaci_excel in chk_test_excel.Items)
        {
            if (chkListFaci_excel.Selected)
            {
                u2_doc_data_excel[i_excel] = new qa_lab_u2_qalab();
                u2_doc_data_excel[i_excel].test_detail_idx = int.Parse(chkListFaci_excel.Value);
                //u2_doc_data[i].material_code = u1_doc_insert.material_code;
                //u2_doc_data.count_chktest = i + 1;

                sumcheck_excel = sumcheck_excel + 1;

                i_excel++;
            }
        }

        //li_count.Text = SumT_check.ToString();
        //set testdetail
        var u2_doc_setdata_excel = new qa_lab_u2_qalab[chk_settest_excel.Items.Count];

        int k_excel = 0;
        string sumcheck_sysdept_excel = "";
        List<String> AddoingList_set_excel = new List<string>();
        foreach (ListItem chkListSet_excel in chk_settest_excel.Items)
        {
            if (chkListSet_excel.Selected)
            {
                u2_doc_setdata_excel[k_excel] = new qa_lab_u2_qalab();
                u2_doc_setdata_excel[k_excel].test_detail_idx = int.Parse(chkListSet_excel.Value);

                sumcheck_sysdept_excel += chkListSet_excel.Value + ",";
                k_excel++;
            }
        }





        // u3_qalab                    
        var ds_dept_insert_excel = (DataSet)ViewState["vsDepartment_Excel"];
        var u3_doc_insert_excel = new qa_lab_u3_qalab[ds_dept_insert_excel.Tables[0].Rows.Count];
        var SumTotalLinse_excel = ds_dept_insert_excel.Tables[0].Rows.Count;
        //Check องค์กร ฝ่าย ตอนแบ่ง License
        int j_excel = 0;
        foreach (DataRow drw_excel in ds_dept_insert_excel.Tables[0].Rows)
        {

            u3_doc_insert_excel[j_excel] = new qa_lab_u3_qalab();
            u3_doc_insert_excel[j_excel].org_idx = int.Parse(drw_excel["OrgIDX_Excel"].ToString()); //["OrgIDX"] = int.Parse(ddlOrg.SelectedValue);
            u3_doc_insert_excel[j_excel].rdept_idx = int.Parse(drw_excel["RDeptIDX_Excel"].ToString());//["OrgNameEN"] = ddlOrg.SelectedItem.Text;
            u3_doc_insert_excel[j_excel].rsec_idx = int.Parse(drw_excel["RSecIDX_Excel"].ToString());//["RDeptIDX"] = int.Parse(ddlDept.SelectedValue);
            u3_doc_insert_excel[j_excel].cemp_idx = _emp_idx;//int.Parse(drw["OrgIDX_dataset"].ToString());//["DeptNameEN"] = ddlDept.SelectedItem.Text;

            j_excel++;

        }

        //u4_qalab                       
        _data_u4_importexcel.m0_actor_idx = actor_idx_excel;
        _data_u4_importexcel.u0_node_idx = node_idx_excel;
        _data_u4_importexcel.decision_idx = 0;
        _data_u4_importexcel.comment = "-";
        _data_u4_importexcel.cemp_idx = _emp_idx;
        _data_u4_importexcel.count_detail_tested = sumcheck_excel;
        _data_u4_importexcel.count_sample_tested = count_alert_u1;

        ////_data_qa_import.qa_lab_u0_qalab_list[0] = _data_u0_importexcel;
        _data_qa_import.qa_lab_u1_qalab_list = u0_waterimport;
        ////_data_qa_import.qa_lab_u2_qalab_list = u2_doc_data_excel;//_u2_doc;  
        ////_data_qa_import.qa_lab_u3_qalab_list = u3_doc_insert_excel;//_u2_doc; 
        ////_data_qa_import.qa_lab_u4_qalab_list[0] = _data_u4_importexcel;

        litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_import));
        ////if (ddlPlace_excel.SelectedValue == "-1")
        ////{
        ////    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('กรุณาเลือกสถานที่ตรวจสอบ!!!');", dt.Rows.Count.ToString()), true);
        ////}
        ////else
        ////{
        ////    _data_qa_import = callServicePostQA(_urlQaSetImportExcel, _data_qa_import);

        ////    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวนตัวอย่างทั่งหมด = {0} ข้อมูล');", dt.Rows.Count.ToString()), true);
        ////}

    }


    #endregion

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, "0");

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "btnInsertDept":

                RequiredFieldValidator validator = (RequiredFieldValidator)fvDeptSelect.FindControl("RequiredddlOrg_create");
                //FormView fvDeptSelect = (FormView)fvActor1Node1.FindControl("fvDeptSelect");
                DropDownList ddlOrg = (DropDownList)fvDeptSelect.FindControl("ddlOrg_create");
                DropDownList ddlDept = (DropDownList)fvDeptSelect.FindControl("ddlDept_create");
                DropDownList ddlSec = (DropDownList)fvDeptSelect.FindControl("ddlSec_create");
                GridView GvDeptCreate = (GridView)fvDeptSelect.FindControl("GvDeptCreate");

                //Add-Department
                var ds_Add_department = (DataSet)ViewState["vsDepartment"];
                var dr_Add_department = ds_Add_department.Tables[0].NewRow();

                int numrow_dept = ds_Add_department.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlorg_idxinsert_check"] = ddlOrg.SelectedValue;
                ViewState["_ddlrdept_idxinsert_check"] = ddlDept.SelectedValue;
                ViewState["_ddlsec_check"] = ddlSec.SelectedValue;

                if (numrow_dept > 0)
                {

                    foreach (DataRow check in ds_Add_department.Tables[0].Rows)
                    {
                        ViewState["check_org"] = check["OrgIDX"];
                        ViewState["check_dept"] = check["RDeptIDX"];

                        ViewState["check_sec"] = check["RSecIDX"];


                        if (ViewState["_ddlsec_check"].ToString() == ViewState["check_sec"].ToString())
                        {
                            ViewState["CheckDataset"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset"] = "1";

                        }
                    }

                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        dr_Add_department["OrgIDX"] = int.Parse(ddlOrg.SelectedValue);
                        dr_Add_department["OrgNameEN"] = ddlOrg.SelectedItem.Text;
                        dr_Add_department["RDeptIDX"] = int.Parse(ddlDept.SelectedValue);
                        dr_Add_department["DeptNameEN"] = ddlDept.SelectedItem.Text;
                        dr_Add_department["RSecIDX"] = int.Parse(ddlSec.SelectedValue);
                        dr_Add_department["SecNameEN"] = ddlSec.SelectedItem.Text;

                        ds_Add_department.Tables[0].Rows.Add(dr_Add_department);
                        ViewState["vsDepartment"] = ds_Add_department;

                        //////  create data set show detail license  ////////
                        setGridData(GvDeptCreate, (DataSet)ViewState["vsDepartment"]);

                        getOrganizationList(ddlOrg);
                        getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedValue));
                        getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDept.SelectedValue));

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แผนกนี่ได้ถูกเพิ่มไปแล้ว!!!');", true);
                        //break;
                    }

                }
                else
                {

                    dr_Add_department["OrgIDX"] = int.Parse(ddlOrg.SelectedValue);
                    dr_Add_department["OrgNameEN"] = ddlOrg.SelectedItem.Text;
                    dr_Add_department["RDeptIDX"] = int.Parse(ddlDept.SelectedValue);
                    dr_Add_department["DeptNameEN"] = ddlDept.SelectedItem.Text;
                    dr_Add_department["RSecIDX"] = int.Parse(ddlSec.SelectedValue);
                    dr_Add_department["SecNameEN"] = ddlSec.SelectedItem.Text;

                    ds_Add_department.Tables[0].Rows.Add(dr_Add_department);
                    ViewState["vsDepartment"] = ds_Add_department;

                    //////  create data set show detail license  ////////
                    setGridData(GvDeptCreate, (DataSet)ViewState["vsDepartment"]);

                    getOrganizationList(ddlOrg);
                    getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedValue));
                    getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDept.SelectedValue));

                }

                foreach (GridViewRow row in GvDeptCreate.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        department_data = (row.Cells[0].FindControl("lbl_RSecIDX") as Label).Text;
                        Temp_department_data += department_data + ",";


                    }
                }

                ViewState["Depart_RSecIDX"] = Temp_department_data;
                //   validator.Enabled = false;

                break;
            case "btnInsertDeptExcel":

                //FormView fvDeptSelect = (FormView)fvActor1Node1.FindControl("fvDeptSelect");
                FormView FvAddImport_excel = (FormView)docCreate.FindControl("FvAddImport");
                Panel Dept_Excel = (Panel)FvAddImport_excel.FindControl("Dept_Excel");
                DropDownList ddlOrg_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
                DropDownList ddlDept_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
                DropDownList ddlSec_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");
                GridView GvDeptExcel = (GridView)Dept_Excel.FindControl("GvDeptExcel");

                //Add-Department
                var ds_Add_department_excel = (DataSet)ViewState["vsDepartment_Excel"];
                var dr_Add_department_excel = ds_Add_department_excel.Tables[0].NewRow();

                int numrow_dept_excel = ds_Add_department_excel.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlorg_idxinsert_checkexcel"] = ddlOrg_excel_insert.SelectedValue;
                ViewState["_ddlrdept_idxinsert_check_excel"] = ddlDept_excel_insert.SelectedValue;
                ViewState["_ddlsec_check_excel"] = ddlSec_excel_insert.SelectedValue;

                //litDebug.Text = numrow_dept_excel.ToString();

                if (numrow_dept_excel > 0)
                {
                    foreach (DataRow check_excel in ds_Add_department_excel.Tables[0].Rows)
                    {
                        ViewState["check_org_excel"] = check_excel["OrgIDX_Excel"];
                        ViewState["check_dept_excel"] = check_excel["RDeptIDX_Excel"];

                        ViewState["check_sec_excel"] = check_excel["RSecIDX_Excel"];


                        if (ViewState["_ddlsec_check_excel"].ToString() == ViewState["check_sec_excel"].ToString())
                        {
                            ViewState["CheckDataset_Excel"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset_Excel"] = "1";

                        }
                    }

                    if (ViewState["CheckDataset_Excel"].ToString() == "1")
                    {

                        dr_Add_department_excel["OrgIDX_Excel"] = int.Parse(ddlOrg_excel_insert.SelectedValue);
                        dr_Add_department_excel["OrgNameEN_Excel"] = ddlOrg_excel_insert.SelectedItem.Text;
                        dr_Add_department_excel["RDeptIDX_Excel"] = int.Parse(ddlDept_excel_insert.SelectedValue);
                        dr_Add_department_excel["DeptNameEN_Excel"] = ddlDept_excel_insert.SelectedItem.Text;
                        dr_Add_department_excel["RSecIDX_Excel"] = int.Parse(ddlSec_excel_insert.SelectedValue);
                        dr_Add_department_excel["SecNameEN_Excel"] = ddlSec_excel_insert.SelectedItem.Text;

                        ds_Add_department_excel.Tables[0].Rows.Add(dr_Add_department_excel);
                        ViewState["vsDepartment_Excel"] = ds_Add_department_excel;

                        //////  create data set show detail license  ////////
                        setGridData(GvDeptExcel, (DataSet)ViewState["vsDepartment_Excel"]);

                        getOrganizationList(ddlOrg_excel_insert);
                        getDepartmentList(ddlDept_excel_insert, int.Parse(ddlOrg_excel_insert.SelectedValue));
                        getSectionList(ddlSec_excel_insert, int.Parse(ddlOrg_excel_insert.SelectedValue), int.Parse(ddlDept_excel_insert.SelectedValue));

                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แผนกนี่ได้ถูกเพิ่มไปแล้ว!!!');", true);
                        //break;
                    }

                }
                else
                {

                    dr_Add_department_excel["OrgIDX_Excel"] = int.Parse(ddlOrg_excel_insert.SelectedValue);
                    dr_Add_department_excel["OrgNameEN_Excel"] = ddlOrg_excel_insert.SelectedItem.Text;
                    dr_Add_department_excel["RDeptIDX_Excel"] = int.Parse(ddlDept_excel_insert.SelectedValue);
                    dr_Add_department_excel["DeptNameEN_Excel"] = ddlDept_excel_insert.SelectedItem.Text;
                    dr_Add_department_excel["RSecIDX_Excel"] = int.Parse(ddlSec_excel_insert.SelectedValue);
                    dr_Add_department_excel["SecNameEN_Excel"] = ddlSec_excel_insert.SelectedItem.Text;


                    ds_Add_department_excel.Tables[0].Rows.Add(dr_Add_department_excel);
                    ViewState["vsDepartment_Excel"] = ds_Add_department_excel;

                    //////  create data set show detail license  ////////
                    setGridData(GvDeptExcel, (DataSet)ViewState["vsDepartment_Excel"]);

                    getOrganizationList(ddlOrg_excel_insert);
                    getDepartmentList(ddlDept_excel_insert, int.Parse(ddlOrg_excel_insert.SelectedValue));
                    getSectionList(ddlSec_excel_insert, int.Parse(ddlOrg_excel_insert.SelectedValue), int.Parse(ddlDept_excel_insert.SelectedValue));

                }

                foreach (GridViewRow row in GvDeptExcel.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        department_dataexcel = (row.Cells[0].FindControl("lbl_RSecIDXExcel") as Label).Text;
                        Temp_department_dataexcel += department_dataexcel + ",";


                    }
                }

                ViewState["Depart_RSecIDX_Excel"] = Temp_department_data;


                break;

            case "btn_delete_department": //delete dept in datatable 
                string RSecIDX = cmdArg;
                // FormView fvDeptDel = (FormView)fvActor1Node1.FindControl("fvDeptSelect");
                GridView GvDeptCreate_del = (GridView)fvDeptSelect.FindControl("GvDeptCreate");
                var ds_depart_delete = (DataSet)ViewState["vsDepartment"];
                ViewState["Depart_RSecIDX"] = RSecIDX;

                for (int counter = 0; counter < ds_depart_delete.Tables[0].Rows.Count; counter++)
                {
                    if (ds_depart_delete.Tables[0].Rows[counter]["RSecIDX"].ToString() == ViewState["Depart_RSecIDX"].ToString())
                    {
                        ds_depart_delete.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }
                setGridData(GvDeptCreate_del, (DataSet)ViewState["vsDepartment"]);
                break;

            case "btn_delete_departmentexcel": //delete dept in datatable excel

                string RSecIDX_Excel = cmdArg;

                //litDebug.Text = RSecIDX_Excel.ToString();

                FormView FvAddImport_del = (FormView)docCreate.FindControl("FvAddImport");
                Panel Dept_Excel_del = (Panel)FvAddImport_del.FindControl("Dept_Excel");
                GridView GvDeptExcel_del = (GridView)Dept_Excel_del.FindControl("GvDeptExcel");

                var ds_departexcel_delete = (DataSet)ViewState["vsDepartment_Excel"];
                ViewState["Depart_RSecIDX_Excel"] = RSecIDX_Excel;

                for (int counter1 = 0; counter1 < ds_departexcel_delete.Tables[0].Rows.Count; counter1++)
                {
                    if (ds_departexcel_delete.Tables[0].Rows[counter1]["RSecIDX_Excel"].ToString() == ViewState["Depart_RSecIDX_Excel"].ToString())
                    {
                        ds_departexcel_delete.Tables[0].Rows[counter1].Delete();
                        break;
                    }
                }
                setGridData(GvDeptExcel_del, (DataSet)ViewState["vsDepartment_Excel"]);
                break;

            case "btnSearchMat": // search material to create

                TextBox txt_material_search = (TextBox)fvActor1Node1.FindControl("txt_material_search");

                if (txt_material_search.Text != "")
                {
                    getMaterialList((TextBox)fvActor1Node1.FindControl("txt_material_search"), "0");
                    txt_material_search.Text = string.Empty;
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก Mat.!!!');", true);
                }

                break;

            case "btnSearchMatEdit":

                TextBox txt_material_search_edit = (TextBox)fvUserEditDocument.FindControl("txt_material_search_edit");
                if (txt_material_search_edit.Text != "")
                {

                    getMaterialEditList((TextBox)fvUserEditDocument.FindControl("txt_material_search_edit"), "0");
                    txt_material_search_edit.Text = string.Empty;
                }
                else
                {

                }

                break;

            case "btnInsertSample":

                //fvDetailMat Insert sample datatable
                TextBox txt_material_code_create = (TextBox)fvDetailMat.FindControl("txt_material_code_create");
                TextBox txt_samplename_create = (TextBox)fvDetailMat.FindControl("txt_samplename_create");
                DropDownList ddldate_sample1_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample1_idx_create");
                TextBox txt_data_sample1_create = (TextBox)fvDetailMat.FindControl("txt_data_sample1_create");
                DropDownList ddldate_sample2_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample2_idx_create");
                TextBox txt_data_sample2_create = (TextBox)fvDetailMat.FindControl("txt_data_sample2_create");

                TextBox txt_batch_create = (TextBox)fvDetailMat.FindControl("txt_batch_create");
                TextBox txt_cabinet_create = (TextBox)fvDetailMat.FindControl("txt_cabinet_create");
                TextBox txt_customer_name_create = (TextBox)fvDetailMat.FindControl("txt_customer_name_create");
                TextBox txt_time_create = (TextBox)fvDetailMat.FindControl("txt_time_create");
                TextBox txt_shift_time_create = (TextBox)fvDetailMat.FindControl("txt_shift_time_create");
                TextBox txt_other_details_create = (TextBox)fvDetailMat.FindControl("txt_other_details_create");
                TextBox txt_typesample = (TextBox)fvDetailMat.FindControl("txt_typesample");

                DropDownList ddlOrg_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate");
                DropDownList ddlDept_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlDept_samplecreate");
                DropDownList ddlSec_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlSec_samplecreate");


                //RadioButtonList rd_certificate = (RadioButtonList)fvDetailMat.FindControl("rd_certificate");

                HiddenField HiddenTimeCreate = (HiddenField)fvDetailMat.FindControl("HiddenTimeCreate");

                //fvActor1Node1
                CheckBoxList chk_settest_create = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
                CheckBoxList chk_test_create = (CheckBoxList)fvActor1Node1.FindControl("chk_test");

                //GridView GvSample = (GridView)fvDetailMat.FindControl("GvSample");


                //check test detail loop
                // u2_qalab   
                //test detail
                var u2_doc_data_create = new qa_lab_u2_qalab[chk_test_create.Items.Count];
                int sumcheck_create = 0;
                int i_create = 0;
                string test_detail = "";
                string test_detail_idx = "";

                List<String> AddoingList_create = new List<string>();
                foreach (ListItem chkLis_create in chk_test_create.Items)
                {
                    if (chkLis_create.Selected)
                    {
                        u2_doc_data_create[sumcheck_create] = new qa_lab_u2_qalab();
                        u2_doc_data_create[sumcheck_create].test_detail_idx = int.Parse(chkLis_create.Value);

                        sumcheck_create = sumcheck_create + 1;


                        test_detail += chkLis_create.Text + ',' + "<br>";
                        test_detail_idx += chkLis_create.Value + ',';



                        //litDebug.Text = test_detail;
                        //litDebug1.Text = test_detail_idx;
                        sumcheck_create++;
                    }
                }


                // string[] value_u2 = remove1.Split(',');


                var u2_doc_data_chk = new qa_lab_u2_qalab[chk_test_create.Items.Count];
                int sumcheck_count = 0;
                int i_in = 0;

                List<String> AddoingList = new List<string>();
                foreach (ListItem chk_count_insert in chk_test_create.Items)
                {
                    if (chk_count_insert.Selected)
                    {
                        u2_doc_data_chk[i_in] = new qa_lab_u2_qalab();
                        u2_doc_data_chk[i_in].test_detail_idx = int.Parse(chk_count_insert.Value);

                        sumcheck_count = i_in + 1;

                        i_in++;
                    }
                }


                if (sumcheck_count.ToString() != "0")
                {

                    //Add-Sample
                    var ds_Add_Sample = (DataSet)ViewState["vsSample"];
                    var dr_Add_Sample = ds_Add_Sample.Tables[0].NewRow();

                    int numrow = ds_Add_Sample.Tables[0].Rows.Count; //จำนวนแถว

                    ViewState["_materialcode_check"] = txt_material_code_create.Text;

                    if (numrow > 0)
                    {
                        foreach (DataRow check_mat in ds_Add_Sample.Tables[0].Rows)
                        {
                            ViewState["check_materialcode"] = check_mat["material_code"];

                            if (ViewState["_materialcode_check"].ToString() == ViewState["check_materialcode"].ToString())
                            {
                                ViewState["CheckDataset_mat"] = "1";
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset_mat"] = "0";

                            }
                        }

                        //if (ViewState["CheckDataset_mat"].ToString() == "0")
                        //{

                        if (txt_other_details_create.Text == "")
                        { dr_Add_Sample["other_details"] = "-"; }
                        else
                        { dr_Add_Sample["other_details"] = txt_other_details_create.Text; }


                        if (txt_material_code_create.Text == "")
                        { dr_Add_Sample["material_code"] = "-"; }
                        else
                        { dr_Add_Sample["material_code"] = txt_material_code_create.Text; }


                        if (txt_samplename_create.Text == "")
                        { dr_Add_Sample["samplename"] = "-"; }
                        else
                        { dr_Add_Sample["samplename"] = txt_samplename_create.Text; }

                        if (txt_typesample.Text == "")
                        { dr_Add_Sample["txt_typesample"] = "-"; }
                        else
                        { dr_Add_Sample["txt_typesample"] = txt_typesample.Text; }


                        if (int.Parse(ddldate_sample1_idx_create.SelectedValue) == -1)
                        {
                            dr_Add_Sample["sample1_idx_name"] = "";
                            dr_Add_Sample["sample1_idx"] = 0;
                        }
                        else
                        {

                            if (int.Parse(ddldate_sample1_idx_create.SelectedValue) == 5)
                            {
                                dr_Add_Sample["sample1_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);
                            }
                            else
                            {
                                dr_Add_Sample["sample1_idx_name"] = ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);
                            }
                            //dr_Add_Sample["sample1_idx_name"] = ddldate_sample1_idx_create.SelectedItem.Text;
                            //dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);

                        }

                        if (txt_data_sample1_create.Text == "")
                        { dr_Add_Sample["data_sample1"] = "-"; }
                        else
                        {
                            dr_Add_Sample["data_sample1"] = txt_data_sample1_create.Text;
                        }

                        if (int.Parse(ddldate_sample2_idx_create.SelectedValue) == -1)
                        {
                            dr_Add_Sample["sample2_idx_name"] = "";
                            dr_Add_Sample["sample2_idx"] = 0;
                        }
                        else
                        {

                            if (int.Parse(ddldate_sample2_idx_create.SelectedValue) == 5)
                            {
                                dr_Add_Sample["sample2_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                            }
                            else
                            {
                                dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                                dr_Add_Sample["sample2_idx_name"] = ddldate_sample2_idx_create.SelectedItem.Text;
                            }

                            //dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                            //dr_Add_Sample["sample2_idx_name"] = ddldate_sample2_idx_create.SelectedItem.Text;

                        }

                        if (chk_settest_create.SelectedValue == "")
                        {
                            dr_Add_Sample["chk_settest_create"] = "-";
                            dr_Add_Sample["chk_settestidx_create"] = 0;


                        }
                        else
                        {
                            dr_Add_Sample["chk_settest_create"] = chk_settest_create.SelectedItem.ToString();
                            dr_Add_Sample["chk_settestidx_create"] = chk_settest_create.SelectedValue;
                            //litDebug.Text = chk_settest_create.SelectedItem.ToString();

                        }

                        if (test_detail == "")
                        {
                            dr_Add_Sample["chk_test_create"] = "-";
                            dr_Add_Sample["chk_testidx_create"] = 0;

                        }
                        else
                        {
                            string remove1 = test_detail.Remove(test_detail.Length - 1);

                            dr_Add_Sample["chk_test_create"] = remove1;
                            dr_Add_Sample["chk_testidx_create"] = test_detail_idx;

                            //litDebug.Text = chk_settest_create.SelectedItem.ToString();
                        }


                        if (txt_data_sample2_create.Text == "")
                        {
                            dr_Add_Sample["data_sample2"] = "-";
                        }
                        else
                        {
                            dr_Add_Sample["data_sample2"] = txt_data_sample2_create.Text;
                        }

                        if (txt_cabinet_create.Text == "")
                        { dr_Add_Sample["cabinet"] = "-"; }
                        else
                        { dr_Add_Sample["cabinet"] = txt_cabinet_create.Text; }


                        if (txt_batch_create.Text == "")
                        { dr_Add_Sample["batch"] = "-"; }
                        else
                        { dr_Add_Sample["batch"] = txt_batch_create.Text; }


                        if (txt_customer_name_create.Text == "")
                        { dr_Add_Sample["customer_name"] = "-"; }
                        else
                        { dr_Add_Sample["customer_name"] = txt_customer_name_create.Text; }


                        if (txt_shift_time_create.Text == "")
                        { dr_Add_Sample["shift_time"] = "-"; }
                        else
                        {
                            dr_Add_Sample["shift_time"] = txt_shift_time_create.Text;
                        }


                        if (txt_time_create.Text == "")
                        {
                            dr_Add_Sample["time"] = "-";
                            //dr_Add_Sample["time"] = HiddenTimeCreate.Value.ToString();
                        }

                        else
                        {
                            dr_Add_Sample["time"] = HiddenTimeCreate.Value.ToString();//txt_time_create.Text;
                        }

                        if (int.Parse(ddlOrg_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlOrg_samplecreate"] = "";
                            dr_Add_Sample["ddlOrg_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlOrg_idxsamplecreate"] = int.Parse(ddlOrg_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlOrg_samplecreate"] = ddlOrg_samplecreate.SelectedItem.Text;

                        }

                        if (int.Parse(ddlDept_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlDept_samplecreate"] = "";
                            dr_Add_Sample["ddlDept_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlDept_idxsamplecreate"] = int.Parse(ddlDept_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlDept_samplecreate"] = ddlDept_samplecreate.SelectedItem.Text;

                        }

                        if (int.Parse(ddlSec_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlSec_samplecreate"] = "";
                            dr_Add_Sample["ddlSec_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlSec_idxsamplecreate"] = int.Parse(ddlSec_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlSec_samplecreate"] = ddlSec_samplecreate.SelectedItem.Text;

                        }

                        ds_Add_Sample.Tables[0].Rows.Add(dr_Add_Sample);
                        ViewState["vsSample"] = ds_Add_Sample;

                        divGvSample_scroll.Visible = true;
                        GvSample.Visible = true;
                        setGridData(GvSample, (DataSet)ViewState["vsSample"]);

                        getTestDetail_chk();
                        //linkBtnTrigger(lbDocSaveImport);
                        getSetTestDetail_chk();
                        //}
                        //else
                        //{
                        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Matrial นี้ ถูกเพิ่มไปแล้ว!!!');", true);
                        //    //break;
                        //}

                    }
                    else
                    {

                        if (txt_other_details_create.Text == "")
                        { dr_Add_Sample["other_details"] = "-"; }
                        else
                        { dr_Add_Sample["other_details"] = txt_other_details_create.Text; }


                        if (txt_material_code_create.Text == "")
                        { dr_Add_Sample["material_code"] = "-"; }
                        else
                        { dr_Add_Sample["material_code"] = txt_material_code_create.Text; }

                        if (txt_samplename_create.Text == "")
                        { dr_Add_Sample["samplename"] = "-"; }
                        else
                        { dr_Add_Sample["samplename"] = txt_samplename_create.Text; }

                        if (txt_typesample.Text == "")
                        { dr_Add_Sample["txt_typesample"] = "-"; }
                        else
                        { dr_Add_Sample["txt_typesample"] = txt_typesample.Text; }


                        if (int.Parse(ddldate_sample1_idx_create.SelectedValue) == -1)
                        {
                            dr_Add_Sample["sample1_idx_name"] = "";
                            dr_Add_Sample["sample1_idx"] = 0;
                        }
                        else
                        {
                            if (int.Parse(ddldate_sample1_idx_create.SelectedValue) == 5)
                            {
                                dr_Add_Sample["sample1_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);
                            }
                            else
                            {
                                dr_Add_Sample["sample1_idx_name"] = ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);
                            }


                        }

                        if (chk_settest_create.SelectedValue == "")
                        {
                            dr_Add_Sample["chk_settest_create"] = "-";
                            dr_Add_Sample["chk_settestidx_create"] = 0;

                        }
                        else
                        {
                            dr_Add_Sample["chk_settest_create"] = chk_settest_create.SelectedItem.ToString();
                            dr_Add_Sample["chk_settestidx_create"] = chk_settest_create.SelectedValue;

                            //litDebug.Text = chk_settest_create.SelectedItem.ToString();
                        }

                        if (test_detail == "")
                        {
                            dr_Add_Sample["chk_test_create"] = "-";
                            dr_Add_Sample["chk_testidx_create"] = 0;

                        }
                        else
                        {
                            string remove1 = test_detail.Remove(test_detail.Length - 1);

                            dr_Add_Sample["chk_test_create"] = remove1;
                            dr_Add_Sample["chk_testidx_create"] = test_detail_idx;

                            //litDebug.Text = chk_settest_create.SelectedItem.ToString();
                        }

                        if (txt_data_sample1_create.Text == "")
                        { dr_Add_Sample["data_sample1"] = "-"; }
                        else
                        {
                            dr_Add_Sample["data_sample1"] = txt_data_sample1_create.Text;
                        }

                        if (int.Parse(ddldate_sample2_idx_create.SelectedValue) == -1)
                        {
                            dr_Add_Sample["sample2_idx_name"] = "";
                            dr_Add_Sample["sample2_idx"] = 0;
                        }
                        else
                        {

                            if (int.Parse(ddldate_sample2_idx_create.SelectedValue) == 5)
                            {
                                dr_Add_Sample["sample2_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                            }
                            else
                            {
                                dr_Add_Sample["sample2_idx_name"] = ddldate_sample2_idx_create.SelectedItem.Text;
                                dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                            }


                            //dr_Add_Sample["sample2_idx"] = int.Parse(ddldate_sample2_idx_create.SelectedValue);
                            //dr_Add_Sample["sample2_idx_name"] = ddldate_sample2_idx_create.SelectedItem.Text;

                        }


                        if (txt_data_sample2_create.Text == "")
                        {
                            dr_Add_Sample["data_sample2"] = "-";
                        }
                        else
                        {
                            dr_Add_Sample["data_sample2"] = txt_data_sample2_create.Text;
                        }

                        if (txt_cabinet_create.Text == "")
                        { dr_Add_Sample["cabinet"] = "-"; }
                        else
                        { dr_Add_Sample["cabinet"] = txt_cabinet_create.Text; }


                        if (txt_batch_create.Text == "")
                        { dr_Add_Sample["batch"] = "-"; }
                        else
                        { dr_Add_Sample["batch"] = txt_batch_create.Text; }


                        if (txt_customer_name_create.Text == "")
                        { dr_Add_Sample["customer_name"] = "-"; }
                        else
                        { dr_Add_Sample["customer_name"] = txt_customer_name_create.Text; }


                        if (txt_shift_time_create.Text == "")
                        { dr_Add_Sample["shift_time"] = "-"; }
                        else
                        {
                            dr_Add_Sample["shift_time"] = txt_shift_time_create.Text;
                        }


                        if (txt_time_create.Text == "")
                        {
                            dr_Add_Sample["time"] = "-";
                            //dr_Add_Sample["time"] = HiddenTimeCreate.Value.ToString();
                        }

                        else
                        {
                            dr_Add_Sample["time"] = HiddenTimeCreate.Value.ToString();
                        }

                        if (int.Parse(ddlOrg_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlOrg_samplecreate"] = "";
                            dr_Add_Sample["ddlOrg_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlOrg_idxsamplecreate"] = int.Parse(ddlOrg_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlOrg_samplecreate"] = ddlOrg_samplecreate.SelectedItem.Text;

                        }

                        if (int.Parse(ddlDept_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlDept_samplecreate"] = "";
                            dr_Add_Sample["ddlDept_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlDept_idxsamplecreate"] = int.Parse(ddlDept_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlDept_samplecreate"] = ddlDept_samplecreate.SelectedItem.Text;

                        }

                        if (int.Parse(ddlSec_samplecreate.SelectedValue) == -1)
                        {
                            dr_Add_Sample["ddlSec_samplecreate"] = "";
                            dr_Add_Sample["ddlSec_idxsamplecreate"] = 0;
                        }
                        else
                        {
                            dr_Add_Sample["ddlSec_idxsamplecreate"] = int.Parse(ddlSec_samplecreate.SelectedValue);
                            dr_Add_Sample["ddlSec_samplecreate"] = ddlSec_samplecreate.SelectedItem.Text;

                        }

                        ds_Add_Sample.Tables[0].Rows.Add(dr_Add_Sample);
                        ViewState["vsSample"] = ds_Add_Sample;

                        divGvSample_scroll.Visible = true;
                        GvSample.Visible = true;
                        setGridData(GvSample, (DataSet)ViewState["vsSample"]);

                        getTestDetail_chk();
                        //linkBtnTrigger(lbDocSaveImport);
                        getSetTestDetail_chk();
                    }

                    foreach (GridViewRow row in GvSample.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            sample_data = (row.Cells[0].FindControl("lbl_material_code") as Label).Text;
                            Temp_sample_data += sample_data + ",";
                        }
                    }

                    ViewState["sample_material_code"] = Temp_department_data;

                    ViewState["num_row"] = GvSample.Rows.Count;

                    div_other_detail.Visible = false;
                    fvDetailMat.Visible = false;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกรายการตรวจให้เรียบร้อยก่อนทำการเพิ่มตัวอย่าง!!!');", true);
                    break;
                }




                break;
            ////case "btn_DeleteSample": //delete dept in datatable 
            ////    string material_code = cmdArg;

            ////    var ds_sample_delete = (DataSet)ViewState["vsSample"];
            ////    ViewState["sample_material_code"] = material_code;

            ////    for (int counter_sam = 0; counter_sam < ds_sample_delete.Tables[0].Rows.Count; counter_sam++)
            ////    {
            ////        //if (ds_sample_delete.Tables[0].Rows[counter_sam]["material_code"].ToString() == ViewState["sample_material_code"].ToString())
            ////        //{
            ////            ds_sample_delete.Tables[0].Rows[counter_sam].Delete();
            ////        //    break;
            ////        //}
            ////    }

            ////    setGridData(GvSample, (DataSet)ViewState["vsSample"]);
            ////    break;

            case "cmdDocSave":

                // fvEmpDetail
                HiddenField hfEmpOrgIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
                // fvEmpDetail

                // fvActor1Node1
                HiddenField hfActorIDX = (HiddenField)fvActor1Node1.FindControl("hfActorIDX");
                HiddenField hfNodeIDX = (HiddenField)fvActor1Node1.FindControl("hfNodeIDX");
                HiddenField hfU0IDX = (HiddenField)fvActor1Node1.FindControl("hfU0IDX");
                DropDownList ddlPlace = (DropDownList)fvActor1Node1.FindControl("ddlPlace");
                CheckBoxList chk_test = (CheckBoxList)fvActor1Node1.FindControl("chk_test");
                CheckBoxList chk_settest = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
                TextBox chk_select = (TextBox)fvActor1Node1.FindControl("chk_select");

                CheckBox Check_LabOut = (CheckBox)fvActor1Node1.FindControl("Check_LabOut");
                // fvActor1Node1

                FileUpload fuAttachFile = (FileUpload)fvActor1Node1.FindControl("fuAttachFile");

                int actor_idx = int.TryParse(hfActorIDX.Value, out _default_int) ? int.Parse(hfActorIDX.Value) : _default_int;
                int node_idx = int.TryParse(hfNodeIDX.Value, out _default_int) ? int.Parse(hfNodeIDX.Value) : _default_int;

                _data_qa.qa_lab_u0_qalab_list = new qa_lab_u0_qalab[1];
                _data_qa.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                _data_qa.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                _data_qa.qa_lab_u3_qalab_list = new qa_lab_u3_qalab[1];
                _data_qa.qa_lab_u4_qalab_list = new qa_lab_u4_qalab[1];

                qa_lab_u0_qalab _u0_doc = new qa_lab_u0_qalab();
                qa_lab_u1_qalab _u1_doc = new qa_lab_u1_qalab();
                qa_lab_u2_qalab _u2_doc = new qa_lab_u2_qalab();
                qa_lab_u3_qalab _u3_doc = new qa_lab_u3_qalab();
                qa_lab_u4_qalab _u4_doc = new qa_lab_u4_qalab();

                // switch action by node
                switch (node_idx)
                {
                    case 1:

                        // u0_qalab
                        _u0_doc.unidx = int.TryParse(hfU0IDX.Value, out _default_int) ? int.Parse(hfU0IDX.Value) : _default_int;
                        _u0_doc.place_idx = int.TryParse(ddlPlace.SelectedValue, out _default_int) ? int.Parse(ddlPlace.SelectedValue) : _default_int;
                        _u0_doc.cemp_idx = _emp_idx;
                        _u0_doc.cemp_org_idx = int.TryParse(hfEmpOrgIDX.Value, out _default_int) ? int.Parse(hfEmpOrgIDX.Value) : _default_int;
                        _u0_doc.cemp_rdept_idx = int.TryParse(hfEmpRdeptIDX.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX.Value) : _default_int;
                        _u0_doc.cemp_rsec_idx = int.TryParse(hfEmpRsecIDX.Value, out _default_int) ? int.Parse(hfEmpRsecIDX.Value) : _default_int;
                        _u0_doc.staidx = 1; // status document

                        //sent to lab out
                        if (Check_LabOut.Checked)
                        {
                            _u0_doc.sent_labout = 1;
                        }
                        else
                        {
                            _u0_doc.sent_labout = 0;
                        }

                        ////if(chk_settest.SelectedValue != "0")
                        ////{
                        ////    _u0_doc.set_test_detail_idx = int.Parse(chk_settest.SelectedValue);
                        ////}
                        ////else
                        ////{
                        ////    _u0_doc.set_test_detail_idx = 0;
                        ////}

                        // u1_qalab                     
                        var ds_sample_insert = (DataSet)ViewState["vsSample"];
                        var u1_doc_insert = new qa_lab_u1_qalab[ds_sample_insert.Tables[0].Rows.Count];
                        var SumT_check = ds_sample_insert.Tables[0].Rows.Count;
                        //Check องค์กร ฝ่าย ตอนแบ่ง License
                        int s = 0;
                        foreach (DataRow dr_sample in ds_sample_insert.Tables[0].Rows)
                        {

                            u1_doc_insert[s] = new qa_lab_u1_qalab();
                            u1_doc_insert[s].material_code = dr_sample["material_code"].ToString();
                            u1_doc_insert[s].sample_name = dr_sample["samplename"].ToString();
                            u1_doc_insert[s].date_sample1_idx = int.Parse(dr_sample["sample1_idx"].ToString());
                            u1_doc_insert[s].data_sample1 = dr_sample["data_sample1"].ToString();
                            u1_doc_insert[s].date_sample2_idx = int.Parse(dr_sample["sample2_idx"].ToString());
                            u1_doc_insert[s].data_sample2 = dr_sample["data_sample2"].ToString();
                            u1_doc_insert[s].batch = dr_sample["batch"].ToString();
                            u1_doc_insert[s].shift_time = dr_sample["shift_time"].ToString();
                            u1_doc_insert[s].cabinet = dr_sample["cabinet"].ToString();
                            u1_doc_insert[s].test_detailidx_insert = dr_sample["chk_testidx_create"].ToString();
                            u1_doc_insert[s].materail_type = dr_sample["txt_typesample"].ToString();
                            u1_doc_insert[s].org_idx_production = int.Parse(dr_sample["ddlOrg_idxsamplecreate"].ToString());
                            u1_doc_insert[s].rdept_idx_production = int.Parse(dr_sample["ddlDept_idxsamplecreate"].ToString());
                            u1_doc_insert[s].rsec_idx_production = int.Parse(dr_sample["ddlSec_idxsamplecreate"].ToString());
                            u1_doc_insert[s].settest_idx = int.Parse(dr_sample["chk_settestidx_create"].ToString());

                            string remove = dr_sample["chk_testidx_create"].ToString().Remove(dr_sample["chk_testidx_create"].ToString().Length - 1);
                            string[] value_u2 = remove.Split(',');

                            u1_doc_insert[s].count_test_detail_idx = value_u2.Count();


                            //foreach (string re_u2 in value_u2)
                            //{
                            //    var u2_doc_value_u2 = new qa_lab_u2_qalab[value_u2.Count()];
                            //}

                            if (dr_sample["time"].ToString() != "-")
                            {
                                u1_doc_insert[s].time = dr_sample["time"].ToString();
                            }
                            else
                            {
                                u1_doc_insert[s].time = "00:00";
                            }

                            u1_doc_insert[s].customer_name = dr_sample["customer_name"].ToString();
                            u1_doc_insert[s].other_details = dr_sample["other_details"].ToString();
                            // u1_doc_insert[s].certificate = int.Parse(dr_sample["certificate"].ToString());

                            s++;

                        }


                        //string[] value_u2 = { };//remove.Split(',');
                        //var u2_doc_insert = new qa_lab_u2_qalab[ds_sample_insert.Tables[0].Rows.Count];


                        int s_i = 0;
                        foreach (DataRow dr_sample in ds_sample_insert.Tables[0].Rows)
                        {


                            //litDebug2.Text += dr_sample["chk_testidx_create"].ToString();
                            ////string remove = dr_sample["chk_testidx_create"].ToString().Remove(dr_sample["chk_testidx_create"].ToString().Length - 1);
                            ////string[] value_u2 = remove.Split(',');




                            ////foreach (string re_u2 in value_u2)
                            ////{

                            ////    var u2_doc_value_u2 = new qa_lab_u2_qalab[value_u2.Count()];

                            ////    //litDebug.Text = value_u2.Count().ToString();

                            ////    int s_in = 0;

                            ////    if (re_u2 != String.Empty)
                            ////    {
                            ////        //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                            ////        u2_doc_value_u2[s_in] = new qa_lab_u2_qalab();
                            ////        u2_doc_value_u2[s_in].test_detail_idx = int.Parse(re_u2); //int.Parse(dr_sample["chk_testidx_create"].ToString());
                            ////        //litDebug.Text += rEmail;

                            ////        //litDebug.Text += u2_doc_value_u2.ToString() + ',';
                            ////        s_in++;
                            ////    }

                            ////    _data_qa.qa_lab_u2_qalab_list = u2_doc_value_u2;
                            ////    //ViewState["u2_doc_value_u2"] = _data_qa.qa_lab_u2_qalab_list;

                            ////    litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa));

                            ////}



                            //u2_doc_insert[s_i] = new qa_lab_u2_qalab();
                            //u1_doc_insert[s_i].material_code = dr_sample["material_code"].ToString();

                            //CheckBox check = (CheckBox)row.FindControl("CheckBox1");
                            //CheckBox check2 = (CheckBox)row.FindControl("CheckBox2");

                            //Label lbl_chk_testidx_create = (Label)row.FindControl("lbl_chk_testidx_create");

                            //litDebug2.Text = lbl_chk_testidx_create.Text;


                            //EX!!!
                            //IF AccessType = 1
                            //{
                            //    check.Checked = true;
                            //}

                            //IF AccessType = 2
                            //{
                            //    check2.Checked = true;
                            //}
                        }

                        ////new data in detail
                        //string[] recepientEmail = recepientEmailList.Split(',');
                        //foreach (string rEmail in recepientEmail)
                        //{
                        //    if (rEmail != String.Empty)
                        //    {
                        //        //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                        //    }
                        //}



                        // u2_qalab   
                        //test detail
                        //var u2_doc_data = new qa_lab_u2_qalab[chk_test.Items.Count];
                        //int sumcheck_ = 0;
                        //int i = 0;

                        //List<String> AddoingList = new List<string>();
                        //foreach (ListItem chkListFaci in chk_test.Items)
                        //{
                        //    if (chkListFaci.Selected)
                        //    {
                        //        u2_doc_data[i] = new qa_lab_u2_qalab();
                        //        u2_doc_data[i].test_detail_idx = int.Parse(chkListFaci.Value);

                        //        sumcheck_ = i + 1;

                        //        i++;
                        //    }
                        //}

                        //li_count.Text = SumT_check.ToString();
                        //set testdetail
                        ////var u2_doc_setdata = new qa_lab_u2_qalab[chk_settest.Items.Count];

                        ////int k = 0;
                        ////string sumcheck_sysdept = "";
                        ////List<String> AddoingList_set = new List<string>();
                        ////foreach (ListItem chkListSet in chk_settest.Items)
                        ////{
                        ////    if (chkListSet.Selected)
                        ////    {
                        ////        u2_doc_setdata[k] = new qa_lab_u2_qalab();
                        ////        u2_doc_setdata[k].test_detail_idx = int.Parse(chkListSet.Value);

                        ////        sumcheck_sysdept += chkListSet.Value + ",";
                        ////        k++;
                        ////    }
                        ////}
                        /////////////////////////////////////

                        // u3_qalab                    
                        var ds_dept_insert = (DataSet)ViewState["vsDepartment"];
                        var u3_doc_insert = new qa_lab_u3_qalab[ds_dept_insert.Tables[0].Rows.Count];
                        var SumTotalLinse = ds_dept_insert.Tables[0].Rows.Count;
                        //Check องค์กร ฝ่าย ตอนแบ่ง License
                        int j = 0;
                        foreach (DataRow drw in ds_dept_insert.Tables[0].Rows)
                        {

                            u3_doc_insert[j] = new qa_lab_u3_qalab();
                            u3_doc_insert[j].org_idx = int.Parse(drw["OrgIDX"].ToString());
                            u3_doc_insert[j].rdept_idx = int.Parse(drw["RDeptIDX"].ToString());
                            u3_doc_insert[j].rsec_idx = int.Parse(drw["RSecIDX"].ToString());
                            u3_doc_insert[j].cemp_idx = _emp_idx;

                            j++;

                        }

                        //u4_qalab                       
                        _u4_doc.m0_actor_idx = actor_idx;
                        _u4_doc.u0_node_idx = node_idx;
                        _u4_doc.decision_idx = 0;
                        _u4_doc.comment = "-";
                        _u4_doc.cemp_idx = _emp_idx;
                        ////_u4_doc.count_detail_tested = sumcheck_;
                        ////_u4_doc.count_sample_tested = SumT_check;

                        //data insert to sql
                        _data_qa.qa_lab_u0_qalab_list[0] = _u0_doc;
                        _data_qa.qa_lab_u1_qalab_list = u1_doc_insert;
                        //_data_qa.qa_lab_u2_qalab_list = ViewState["u2_doc_value_u2"];//_u2_doc;                 
                        _data_qa.qa_lab_u3_qalab_list = u3_doc_insert;//_u3_doc;
                        _data_qa.qa_lab_u4_qalab_list[0] = _u4_doc;
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa));
                        if (SumT_check.ToString() != "0")
                        {
                            _data_qa = callServicePostQA(_urlQaSetDocument, _data_qa);
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                            setActiveTab("docCreate", 0, 0, 0, "0");
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกรายการตรวจ และเพิ่มตัวอย่างให้เรียบร้อยก่อนทำการบันทึก!!!');", true);
                            break;
                        }

                        break;
                }
                break;
            case "btnImport":

                UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                DropDownList ddlPlace_excelchk = (DropDownList)fvActor1Node1.FindControl("ddlPlace");
                Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");
                GridView GvExcel_Show = (GridView)GvExcel_Import.FindControl("GvExcel_Show");

                Panel Save_Excel = (Panel)FvAddImport.FindControl("Save_Excel");

                if (upload.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(upload.PostedFile.FileName);
                    string extension = Path.GetExtension(upload.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_water_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        upload.SaveAs(filePath);

                        string conStr = String.Empty;
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        conStr = String.Format(conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection(conStr);
                        OleDbCommand cmdExcel = new OleDbCommand();
                        OleDbDataAdapter oda = new OleDbDataAdapter();
                        DataTable dt = new DataTable();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                        connExcel.Close();
                        connExcel.Open();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill(dt);
                        connExcel.Close();

                        // u1_qalab
                        var count_alert_u1 = dt.Rows.Count;

                        var u0_waterimport = new qa_lab_u1_qalab[dt.Rows.Count];

                        for (var i = 0; i <= dt.Rows.Count - 1;)
                        {
                            //if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
                            //{
                            u0_waterimport[i] = new qa_lab_u1_qalab();

                            if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                            {
                                u0_waterimport[i].customer_name = dt.Rows[i][0].ToString().Trim();
                            }
                            else
                            {
                                u0_waterimport[i].customer_name = "-";
                            }

                            if (dt.Rows[i][1].ToString().Trim() != String.Empty)
                            {
                                u0_waterimport[i].other_details = dt.Rows[i][1].ToString().Trim();
                            }
                            else
                            {
                                u0_waterimport[i].other_details = "-";
                            }

                            if (dt.Rows[i][2].ToString().Trim() != String.Empty)
                            {
                                u0_waterimport[i].rdept_idx_production_name = dt.Rows[i][2].ToString().Trim();
                            }
                            else
                            {
                                u0_waterimport[i].rdept_idx_production_name = "-";
                            }

                            if (dt.Rows[i][3].ToString().Trim() != String.Empty)
                            {
                                u0_waterimport[i].rsec_idx_production_name = dt.Rows[i][3].ToString().Trim();
                            }
                            else
                            {
                                u0_waterimport[i].rsec_idx_production_name = "-";
                            }

                            if (dt.Rows[i][4].ToString().Trim() != String.Empty)
                            {
                                u0_waterimport[i].shift_time = dt.Rows[i][4].ToString().Trim();
                            }
                            else
                            {
                                u0_waterimport[i].shift_time = "-";
                            }

                            i++;
                            //}
                        }

                        ViewState["vsExcelSwab_show"] = u0_waterimport;
                        //if(ViewState["vsExcelSwab_show"] != null)
                        if (dt.Rows.Count != 0)
                        {
                            //litDebug.Text = "555";
                            GvExcel_Import.Visible = true;
                            Save_Excel.Visible = true;
                            setGridData(GvExcel_Show, ViewState["vsExcelSwab_show"]);

                            btnImport.Focus();
                        }
                        else
                        {

                            _funcTool.showAlert(this, "ไม่มีข้อมูล!!");
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert(ไม่มีข้อมูล!!);"), true);
                            //GvExcel_Import.Visible = true;
                            //setGridData(GvExcel_Show, null);

                            //_funcTool.showAlert(this, "ไม่มีข้อมูล");

                            btnImport.Focus();
                            break;
                        }

                        ////actionImport(filePath, extension, "Yes", FileName);
                        ////File.Delete(filePath);

                        ////if (ddlPlace_excelchk.SelectedValue != "-1")
                        ////{
                        ////    setActiveTab("docDetail", 0, 0, 0, "0");
                        ////}
                        ////else
                        ////{
                        ////    _funcTool.showAlert(this, "กรุณาเลือกสถานที่ตรวจสอบก่อนทำการบันทึก!!!");
                        ////}

                    }
                    else
                    {

                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }


                }

                break;

            case "cmdSaveFileExcel":

                // fvEmpDetail
                HiddenField hfEmpOrgIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_excel = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
                // fvEmpDetail

                // fvActor1Node1
                HiddenField hfActorIDX_excel = (HiddenField)fvActor1Node1.FindControl("hfActorIDX");
                HiddenField hfNodeIDX_excel = (HiddenField)fvActor1Node1.FindControl("hfNodeIDX");
                HiddenField hfU0IDX_excel = (HiddenField)fvActor1Node1.FindControl("hfU0IDX");
                DropDownList ddlPlace_excel = (DropDownList)fvActor1Node1.FindControl("ddlPlace");
                CheckBoxList chk_test_excel = (CheckBoxList)fvActor1Node1.FindControl("chk_test");
                CheckBoxList chk_settest_excel = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
                TextBox chk_select_excel = (TextBox)fvActor1Node1.FindControl("chk_select");
                CheckBox Check_LabOut_excel = (CheckBox)fvActor1Node1.FindControl("Check_LabOut");

                ///
                FormView FvAddImport_excel_show = (FormView)docCreate.FindControl("FvAddImport");
                //Panel Dept_Excel = (Panel)FvAddImport_excel.FindControl("Dept_Excel");
                //DropDownList ddlOrg_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
                //DropDownList ddlDept_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
                //DropDownList ddlSec_excel_insert = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");
                GridView GvExcel_Show_excel = (GridView)FvAddImport_excel_show.FindControl("GvExcel_Show");

                int actor_idx_excel = int.TryParse(hfActorIDX_excel.Value, out _default_int) ? int.Parse(hfActorIDX_excel.Value) : _default_int;
                int node_idx_excel = int.TryParse(hfNodeIDX_excel.Value, out _default_int) ? int.Parse(hfNodeIDX_excel.Value) : _default_int;


                data_qa _data_qa_import = new data_qa();

                _data_qa_import.qa_lab_u0_qalab_list = new qa_lab_u0_qalab[1];
                _data_qa_import.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                _data_qa_import.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                _data_qa_import.qa_lab_u3_qalab_list = new qa_lab_u3_qalab[1];
                _data_qa_import.qa_lab_u4_qalab_list = new qa_lab_u4_qalab[1];

                qa_lab_u0_qalab _data_u0_importexcel = new qa_lab_u0_qalab();
                qa_lab_u1_qalab _data_u1_importexcel = new qa_lab_u1_qalab();
                qa_lab_u2_qalab _data_u2_importexcel = new qa_lab_u2_qalab();
                qa_lab_u3_qalab _data_u3_importexcel = new qa_lab_u3_qalab();
                qa_lab_u4_qalab _data_u4_importexcel = new qa_lab_u4_qalab();

                // u0_qalab
                _data_u0_importexcel.unidx = int.TryParse(hfU0IDX_excel.Value, out _default_int) ? int.Parse(hfU0IDX_excel.Value) : _default_int;//dt.Rows.Count;
                _data_u0_importexcel.place_idx = int.TryParse(ddlPlace_excel.SelectedValue, out _default_int) ? int.Parse(ddlPlace_excel.SelectedValue) : _default_int;
                _data_u0_importexcel.cemp_idx = _emp_idx;
                _data_u0_importexcel.cemp_org_idx = int.TryParse(hfEmpOrgIDX_excel.Value, out _default_int) ? int.Parse(hfEmpOrgIDX_excel.Value) : _default_int;
                _data_u0_importexcel.cemp_rdept_idx = int.TryParse(hfEmpRdeptIDX_excel.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX_excel.Value) : _default_int;
                _data_u0_importexcel.cemp_rsec_idx = int.TryParse(hfEmpRsecIDX_excel.Value, out _default_int) ? int.Parse(hfEmpRsecIDX_excel.Value) : _default_int;
                _data_u0_importexcel.staidx = 1; // status document


                if (Check_LabOut_excel.Checked)
                {
                    _data_u0_importexcel.sent_labout = 1;
                }
                else
                {
                    _data_u0_importexcel.sent_labout = 0;
                }


                // u1_qalab
                var count_alert_u1excel = GvExcel_Show_excel.Rows.Count;
                var u1_doc_insert_excel = new qa_lab_u1_qalab[GvExcel_Show_excel.Rows.Count];
                int s_excel = 0;
                foreach (GridViewRow row in GvExcel_Show_excel.Rows)
                {
                    Label lbl_customer_name_excel = (Label)row.FindControl("lbl_customer_name_excel");
                    Label lbl_other_details_excel = (Label)row.FindControl("lbl_other_details_excel");
                    Label lbl_rdept_idx_production_name_excel = (Label)row.FindControl("lbl_rdept_idx_production_name_excel");
                    Label lbl_rsec_idx_production_name_excel = (Label)row.FindControl("lbl_rsec_idx_production_name_excel");
                    Label lbl_shift_time_excel = (Label)row.FindControl("lbl_shift_time_excel");
                    //CheckBox check2 = (CheckBox)row.FindControl("CheckBox2");
                    u1_doc_insert_excel[s_excel] = new qa_lab_u1_qalab();
                    u1_doc_insert_excel[s_excel].customer_name = lbl_customer_name_excel.Text;
                    u1_doc_insert_excel[s_excel].other_details = lbl_other_details_excel.Text;
                    u1_doc_insert_excel[s_excel].rdept_idx_production_name = lbl_rdept_idx_production_name_excel.Text;
                    u1_doc_insert_excel[s_excel].rsec_idx_production_name = lbl_rsec_idx_production_name_excel.Text;
                    u1_doc_insert_excel[s_excel].shift_time = lbl_shift_time_excel.Text;

                    s_excel++;

                }

                // u2_qalab   
                //test detail
                var u2_doc_data_excel = new qa_lab_u2_qalab[chk_test_excel.Items.Count];
                int sumcheck_excel = 0;
                int i_excel = 0;

                List<String> AddoingList_excel = new List<string>();
                foreach (ListItem chkListFaci_excel in chk_test_excel.Items)
                {
                    if (chkListFaci_excel.Selected)
                    {
                        u2_doc_data_excel[i_excel] = new qa_lab_u2_qalab();
                        u2_doc_data_excel[i_excel].test_detail_idx = int.Parse(chkListFaci_excel.Value);
                        //u2_doc_data[i].material_code = u1_doc_insert.material_code;
                        //u2_doc_data.count_chktest = i + 1;

                        sumcheck_excel = sumcheck_excel + 1;

                        i_excel++;
                    }
                }

                //li_count.Text = SumT_check.ToString();
                //set testdetail
                var u2_doc_setdata_excel = new qa_lab_u2_qalab[chk_settest_excel.Items.Count];

                int k_excel = 0;
                string sumcheck_sysdept_excel = "";
                List<String> AddoingList_set_excel = new List<string>();
                foreach (ListItem chkListSet_excel in chk_settest_excel.Items)
                {
                    if (chkListSet_excel.Selected)
                    {
                        u2_doc_setdata_excel[k_excel] = new qa_lab_u2_qalab();
                        u2_doc_setdata_excel[k_excel].test_detail_idx = int.Parse(chkListSet_excel.Value);

                        sumcheck_sysdept_excel += chkListSet_excel.Value + ",";
                        k_excel++;
                    }
                }

                // u3_qalab                    
                var ds_dept_insert_excel = (DataSet)ViewState["vsDepartment_Excel"];
                var u3_doc_insert_excel = new qa_lab_u3_qalab[ds_dept_insert_excel.Tables[0].Rows.Count];
                var SumTotalLinse_excel = ds_dept_insert_excel.Tables[0].Rows.Count;
                //Check องค์กร ฝ่าย ตอนแบ่ง License
                int j_excel = 0;
                foreach (DataRow drw_excel in ds_dept_insert_excel.Tables[0].Rows)
                {

                    u3_doc_insert_excel[j_excel] = new qa_lab_u3_qalab();
                    u3_doc_insert_excel[j_excel].org_idx = int.Parse(drw_excel["OrgIDX_Excel"].ToString()); //["OrgIDX"] = int.Parse(ddlOrg.SelectedValue);
                    u3_doc_insert_excel[j_excel].rdept_idx = int.Parse(drw_excel["RDeptIDX_Excel"].ToString());//["OrgNameEN"] = ddlOrg.SelectedItem.Text;
                    u3_doc_insert_excel[j_excel].rsec_idx = int.Parse(drw_excel["RSecIDX_Excel"].ToString());//["RDeptIDX"] = int.Parse(ddlDept.SelectedValue);
                    u3_doc_insert_excel[j_excel].cemp_idx = _emp_idx;//int.Parse(drw["OrgIDX_dataset"].ToString());//["DeptNameEN"] = ddlDept.SelectedItem.Text;

                    j_excel++;

                }

                //u4_qalab                       
                _data_u4_importexcel.m0_actor_idx = actor_idx_excel;
                _data_u4_importexcel.u0_node_idx = node_idx_excel;
                _data_u4_importexcel.decision_idx = 0;
                _data_u4_importexcel.comment = "-";
                _data_u4_importexcel.cemp_idx = _emp_idx;
                _data_u4_importexcel.count_detail_tested = sumcheck_excel;
                _data_u4_importexcel.count_sample_tested = count_alert_u1excel;

                _data_qa_import.qa_lab_u0_qalab_list[0] = _data_u0_importexcel;
                _data_qa_import.qa_lab_u1_qalab_list = u1_doc_insert_excel;
                _data_qa_import.qa_lab_u2_qalab_list = u2_doc_data_excel;//_u2_doc;  
                _data_qa_import.qa_lab_u3_qalab_list = u3_doc_insert_excel;//_u2_doc; 
                _data_qa_import.qa_lab_u4_qalab_list[0] = _data_u4_importexcel;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_import));

                if (ddlPlace_excel.SelectedValue == "-1")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('กรุณาเลือกสถานที่ตรวจสอบ!!!');"), true);
                    break;
                }
                else
                {
                    _data_qa_import = callServicePostQA(_urlQaSetImportExcel, _data_qa_import);

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวนตัวอย่างทั่งหมด = {0} ข้อมูล');", GvExcel_Show_excel.Rows.Count.ToString()), true);
                }

                setActiveTab("docDetail", 0, 0, 0, "0");
                setOntop.Focus();



                break;

            case "cmdDocDetail":
                setActiveTab("docCreate", int.Parse(cmdArg), 0, 0, "0");
                break;
            case "cmdDocSaveReceive":
                // fvEmpDetail
                HiddenField hfEmpOrgIDX_n2 = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n2 = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n2 = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
                // fvEmpDetail

                TextBox tbU0IDX = (TextBox)fvDetail.FindControl("tbU0IDX");
                DropDownList ddlActor2Approve = (DropDownList)fvActor2_Receive.FindControl("ddlActor2Approve");
                TextBox tbActor2Comment = (TextBox)fvActor2_Receive.FindControl("tbActor2Comment");

                data_qa _data_save_node2 = new data_qa();
                _data_save_node2.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                qa_lab_u2_qalab _node2_sta1 = new qa_lab_u2_qalab();

                _node2_sta1.u0_qalab_idx = int.Parse(tbU0IDX.Text);
                _node2_sta1.decision = int.Parse(ddlActor2Approve.SelectedValue);
                _node2_sta1.comment = tbActor2Comment.Text;
                _node2_sta1.admin_emp_idx = _emp_idx;
                _node2_sta1.admin_org_idx = int.TryParse(hfEmpOrgIDX_n2.Value, out _default_int) ? int.Parse(hfEmpOrgIDX_n2.Value) : _default_int;
                _node2_sta1.admin_rdept_idx = int.TryParse(hfEmpRdeptIDX_n2.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX_n2.Value) : _default_int;
                _node2_sta1.admin_rsec_idx = int.TryParse(hfEmpRsecIDX_n2.Value, out _default_int) ? int.Parse(hfEmpRsecIDX_n2.Value) : _default_int;

                _data_save_node2.qa_lab_u2_qalab_list[0] = _node2_sta1;

                _data_save_node2 = callServicePostQA(_urlQaSetAppoveNode2, _data_save_node2);

                setActiveTab("docDetail", 0, 0, 0, tbValuePlace.Text);
                setOntop.Focus();
                break;
            case "cmdDocDetailLab":

                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(';');
                int u1_idx_lab = int.Parse(arg1[0]);
                int staidx_lab = int.Parse(arg1[1]);

                //if(int.Parse(ViewState["vsArgPlaceLab"].ToString()) != 0)
                //{
                //    litDebug.Text = ViewState["vsArgPlaceLab"].ToString();
                //}


                switch (staidx_lab)
                {
                    case 6:

                        setActiveTab("docLab", 0, u1_idx_lab, staidx_lab, "0");
                        setOntop.Focus();

                        break;

                    case 7:
                    case 8:
                    case 10:
                        setActiveTab("docLab", 0, u1_idx_lab, staidx_lab, "0");
                        setOntop.Focus();
                        break;
                    case 13:
                        setActiveTab("docLab", 0, u1_idx_lab, staidx_lab, "0");
                        setOntop.Focus();
                        break;

                }

                break;
            case "cmdDocSaveReceiveN4":
                // fvEmpDetailLab
                HiddenField hfEmpOrgIDX_n4 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n4 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n4 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRsecIDX");
                // fvEmpDetailLab

                TextBox tbu1_qalab_idx = (TextBox)fvDetailLab.FindControl("tbu1_qalab_idx");
                TextBox tbu0_qalab_idx = (TextBox)fvDetailLab.FindControl("tbu0_qalab_idx");
                Repeater rp_testdetail_lab = (Repeater)fvDetailLab.FindControl("rp_testdetail_lab");

                DropDownList ddlActor3Approve = (DropDownList)fvActor3_Receive.FindControl("ddlActor3Approve");
                TextBox tbActor3Comment = (TextBox)fvActor3_Receive.FindControl("tbActor3Comment");

                data_qa _data_node4_s6 = new data_qa();

                _data_node4_s6.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _node4_sta6_u1 = new qa_lab_u1_qalab();
                //update u1
                _node4_sta6_u1.cemp_idx = _emp_idx;
                _node4_sta6_u1.u0_qalab_idx = int.Parse(tbu0_qalab_idx.Text);
                _node4_sta6_u1.u1_qalab_idx = int.Parse(tbu1_qalab_idx.Text);
                _node4_sta6_u1.decision = int.Parse(ddlActor3Approve.SelectedValue);


                _data_node4_s6.qa_lab_u1_qalab_list[0] = _node4_sta6_u1;


                //update u2
                _data_node4_s6.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                qa_lab_u2_qalab _node4_sta6 = new qa_lab_u2_qalab();


                var u2_lab_n4_s6 = new qa_lab_u2_qalab[rp_testdetail_lab.Items.Count];
                int n4_s_6_u2 = 0;
                foreach (RepeaterItem rpt_detail in rp_testdetail_lab.Items)
                {
                    Label u2_lab_detail = rpt_detail.FindControl("u2_lab_detail") as Label;
                    Label u2_labtest_detail_idx = rpt_detail.FindControl("u2_labtest_detail_idx") as Label;

                    u2_lab_n4_s6[n4_s_6_u2] = new qa_lab_u2_qalab();
                    u2_lab_n4_s6[n4_s_6_u2].u0_qalab_idx = int.Parse(tbu0_qalab_idx.Text);
                    u2_lab_n4_s6[n4_s_6_u2].u1_qalab_idx = int.Parse(tbu1_qalab_idx.Text);
                    u2_lab_n4_s6[n4_s_6_u2].u2_qalab_idx = int.Parse(u2_lab_detail.Text);
                    u2_lab_n4_s6[n4_s_6_u2].test_detail_idx = int.Parse(u2_labtest_detail_idx.Text);
                    u2_lab_n4_s6[n4_s_6_u2].cemp_idx = _emp_idx;
                    u2_lab_n4_s6[n4_s_6_u2].decision = int.Parse(ddlActor3Approve.SelectedValue);

                    if (tbActor3Comment.Text != "")
                    {
                        u2_lab_n4_s6[n4_s_6_u2].comment = tbActor3Comment.Text;
                    }
                    else
                    {
                        u2_lab_n4_s6[n4_s_6_u2].comment = "-";
                    }

                    n4_s_6_u2++;

                }
                _data_node4_s6.qa_lab_u1_qalab_list[0] = _node4_sta6_u1;
                _data_node4_s6.qa_lab_u2_qalab_list = u2_lab_n4_s6;

                _data_node4_s6 = callServicePostQA(_urlQaSetApproveNode4, _data_node4_s6);

                setActiveTab("docLab", 0, 0, 0, "0");
                setOntop.Focus();
                break;

            case "cmdSaveReceiveN4":

                string[] arg4 = new string[4];
                arg4 = e.CommandArgument.ToString().Split(';');
                int index_lab4 = int.Parse(arg4[0]);
                int u0_idx_lab4 = int.Parse(arg4[1]);
                int u1_idx_lab4 = int.Parse(arg4[2]);
                int staidx_lab4 = int.Parse(arg4[3]);

                data_qa _data_node4_r4 = new data_qa();

                _data_node4_r4.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _node4_sta6_u1_r4 = new qa_lab_u1_qalab();
                //update u1
                _node4_sta6_u1_r4.cemp_idx = _emp_idx;
                _node4_sta6_u1_r4.u0_qalab_idx = u0_idx_lab4;
                _node4_sta6_u1_r4.u1_qalab_idx = u1_idx_lab4;
                _node4_sta6_u1_r4.decision = staidx_lab4;


                _data_node4_r4.qa_lab_u1_qalab_list[0] = _node4_sta6_u1_r4;


                //foreach (GridViewRow row in gvLab.Rows)
                //{
                var textadd = (gvLab.Rows[index_lab4].FindControl("rp_testdetail_lab") as Repeater);
                //Repeater rp_testdetail_labr4 = (Repeater)rowCountRecive.FindControl("rp_testdetail_lab");

                var _u2doc_node4 = new qa_lab_u2_qalab[textadd.Items.Count];
                int count_u2doc_n4 = 0;

                foreach (RepeaterItem rpt in textadd.Items)
                {
                    //litDebug.Text = "555";

                    Label test_detail_idxlbl_n4 = rpt.FindControl("test_detail_idxlbl") as Label;
                    Label lblU2IDX_Lab_r4_n4 = rpt.FindControl("lblU2IDX_Lab_r4") as Label;


                    //Label u2qalab_idx = rpt.FindControl("lblu2qalab_idx") as Label;
                    _u2doc_node4[count_u2doc_n4] = new qa_lab_u2_qalab();
                    _u2doc_node4[count_u2doc_n4].u0_qalab_idx = u0_idx_lab4;
                    _u2doc_node4[count_u2doc_n4].u1_qalab_idx = u1_idx_lab4;
                    _u2doc_node4[count_u2doc_n4].u2_qalab_idx = int.Parse(lblU2IDX_Lab_r4_n4.Text);
                    _u2doc_node4[count_u2doc_n4].test_detail_idx = int.Parse(test_detail_idxlbl_n4.Text);
                    _u2doc_node4[count_u2doc_n4].cemp_idx = _emp_idx;
                    _u2doc_node4[count_u2doc_n4].decision = staidx_lab4;
                    _u2doc_node4[count_u2doc_n4].comment = "-";

                    count_u2doc_n4++;


                }
                _data_node4_r4.qa_lab_u1_qalab_list[0] = _node4_sta6_u1_r4;
                _data_node4_r4.qa_lab_u2_qalab_list = _u2doc_node4;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_node4_r4));
                _data_node4_r4 = callServicePostQA(_urlQaSetApproveNode4, _data_node4_r4);

                //litDebug.Text = ViewState["vsArgPlaceLab"].ToString();


                if (ViewState["vsArgPlaceLab"].ToString() == "0")
                {
                    setActiveTab("docLab", 0, 0, 0, "0");
                    setOntop.Focus();
                }
                else
                {
                    data_qa _dataApprovePlaceN4 = new data_qa();
                    _dataApprovePlaceN4.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _u1_doc_lab_placen4 = new qa_lab_u1_qalab();
                    _u1_doc_lab_placen4.m0_lab_idx = int.Parse(ViewState["vsArgPlaceLab"].ToString());
                    _dataApprovePlaceN4.qa_lab_u1_qalab_list[0] = _u1_doc_lab_placen4;

                    _dataApprovePlaceN4 = callServicePostQA(_urlQaGetSelectPlaceLab, _dataApprovePlaceN4);

                    if (_dataApprovePlaceN4.return_code == 0)
                    {

                        lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4.qa_lab_u1_qalab_list[0].lab_name.ToString();

                        if (_dataApprovePlaceN4.qa_lab_u1_qalab_list[0].lab_name.ToString() != null)
                        {
                            lbdetailsShow.Visible = true;

                            ViewState["vs_gvLab_Detail"] = _dataApprovePlaceN4.qa_lab_u1_qalab_list;
                        }
                        else
                        {

                            ViewState["vs_gvLab_Detail"] = null;

                        }


                    }

                    else
                    {
                        ViewState["vs_gvLab_Detail"] = null;
                        lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4.return_msg.ToString();
                        lbdetailsShow.Visible = true;
                    }



                    show_search_lab.Visible = true;
                    gvLab.Visible = true;
                    gvLab.PageIndex = 0;



                    setGridData(gvLab, ViewState["vs_gvLab_Detail"]);
                    setOntop.Focus();
                }


                break;

            case "cmdRejectN4e":

                string[] arg5 = new string[4];
                arg5 = e.CommandArgument.ToString().Split(';');
                int index_lab5 = int.Parse(arg5[0]);
                int u0_idx_lab5 = int.Parse(arg5[1]);
                int u1_idx_lab5 = int.Parse(arg5[2]);
                int staidx_lab5 = int.Parse(arg5[3]);

                data_qa _data_node4_r5 = new data_qa();

                _data_node4_r5.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _node4_sta6_u1_r5 = new qa_lab_u1_qalab();
                //update u1
                _node4_sta6_u1_r5.cemp_idx = _emp_idx;
                _node4_sta6_u1_r5.u0_qalab_idx = u0_idx_lab5;
                _node4_sta6_u1_r5.u1_qalab_idx = u1_idx_lab5;
                _node4_sta6_u1_r5.decision = staidx_lab5;


                _data_node4_r5.qa_lab_u1_qalab_list[0] = _node4_sta6_u1_r5;


                //foreach (GridViewRow row in gvLab.Rows)
                //{
                var textadd5 = (gvLab.Rows[index_lab5].FindControl("rp_testdetail_lab") as Repeater);
                //Repeater rp_testdetail_labr4 = (Repeater)rowCountRecive.FindControl("rp_testdetail_lab");

                var _u2doc_node4_5 = new qa_lab_u2_qalab[textadd5.Items.Count];
                int count_u2doc_n4_5 = 0;

                foreach (RepeaterItem rpt5 in textadd5.Items)
                {
                    //litDebug.Text = "555";

                    Label test_detail_idxlbl_n4_5 = rpt5.FindControl("test_detail_idxlbl") as Label;
                    Label lblU2IDX_Lab_r4_n4_5 = rpt5.FindControl("lblU2IDX_Lab_r4") as Label;


                    //Label u2qalab_idx = rpt.FindControl("lblu2qalab_idx") as Label;
                    _u2doc_node4_5[count_u2doc_n4_5] = new qa_lab_u2_qalab();
                    _u2doc_node4_5[count_u2doc_n4_5].u0_qalab_idx = u0_idx_lab5;
                    _u2doc_node4_5[count_u2doc_n4_5].u1_qalab_idx = u1_idx_lab5;
                    _u2doc_node4_5[count_u2doc_n4_5].u2_qalab_idx = int.Parse(lblU2IDX_Lab_r4_n4_5.Text);
                    _u2doc_node4_5[count_u2doc_n4_5].test_detail_idx = int.Parse(test_detail_idxlbl_n4_5.Text);
                    _u2doc_node4_5[count_u2doc_n4_5].cemp_idx = _emp_idx;
                    _u2doc_node4_5[count_u2doc_n4_5].decision = staidx_lab5;
                    _u2doc_node4_5[count_u2doc_n4_5].comment = "-";

                    count_u2doc_n4_5++;


                }
                _data_node4_r5.qa_lab_u1_qalab_list[0] = _node4_sta6_u1_r5;
                _data_node4_r5.qa_lab_u2_qalab_list = _u2doc_node4_5;
                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_node4_r4));
                _data_node4_r5 = callServicePostQA(_urlQaSetApproveNode4, _data_node4_r5);

                if (ViewState["vsArgPlaceLab"].ToString() == "0")//////
                {
                    setActiveTab("docLab", 0, 0, 0, "0");
                    setOntop.Focus();
                }
                else
                {
                    data_qa _dataApprovePlaceN4r = new data_qa();
                    _dataApprovePlaceN4r.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _u1_doc_lab_placen4r = new qa_lab_u1_qalab();
                    _u1_doc_lab_placen4r.m0_lab_idx = int.Parse(ViewState["vsArgPlaceLab"].ToString());
                    _dataApprovePlaceN4r.qa_lab_u1_qalab_list[0] = _u1_doc_lab_placen4r;

                    _dataApprovePlaceN4r = callServicePostQA(_urlQaGetSelectPlaceLab, _dataApprovePlaceN4r);

                    if (_dataApprovePlaceN4r.return_code == 0)

                    {

                        lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4r.qa_lab_u1_qalab_list[0].lab_name.ToString();

                        if (_dataApprovePlaceN4r.qa_lab_u1_qalab_list[0].lab_name.ToString() != null)
                        {
                            lbdetailsShow.Visible = true;

                            ViewState["vs_gvLab_Detail"] = _dataApprovePlaceN4r.qa_lab_u1_qalab_list;
                        }
                        else
                        {
                            ViewState["vs_gvLab_Detail"] = null;
                            //   lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4r.qa_lab_u1_qalab_list[0].lab_name.ToString();
                        }
                    }
                    else
                    {

                        //  lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4r.qa_lab_u1_qalab_list[0].lab_name.ToString();
                        ViewState["vs_gvLab_Detail"] = null;
                        lbdetailsShow.Visible = true;
                        lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4r.return_msg.ToString();
                    }



                    show_search_lab.Visible = true;
                    gvLab.Visible = true;
                    gvLab.PageIndex = 0;



                    setGridData(gvLab, ViewState["vs_gvLab_Detail"]);
                    setOntop.Focus();
                }

                //setActiveTab("docLab", 0, 0, 0);
                //setOntop.Focus();
                break;

            case "cmdDocCancelN4":
                setActiveTab("docLab", 0, 0, 0, "0");
                setOntop.Focus();
                break;

            case "cmdDocDetailAdminUser":

                string[] arg2 = new string[2];
                arg2 = e.CommandArgument.ToString().Split(';');
                int u0_idx_view = int.Parse(arg2[0]);
                int u1_idx_view = int.Parse(arg2[1]);
                int staidx_view = int.Parse(arg2[2]);

                setActiveTab("docCreate", u0_idx_view, u1_idx_view, staidx_view, "0");
                setOntop.Focus();
                break;
            case "cmdSaveNode3":
                // fvEmpDetail
                HiddenField hfEmpOrgIDX_n3 = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n3 = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n3 = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
                GridView gvSampleTestDetal = (GridView)fvSampleTestDetail.FindControl("gvSampleTestDetal");
                // fvEmpDetail

                TextBox _tbu0IDX = (TextBox)fvDetail.FindControl("tbU0IDX");

                foreach (GridViewRow gr in gvSampleTestDetal.Rows)
                {

                    Label lblStaidx = (Label)gr.FindControl("lblStaidx");
                    Repeater rp_test_sample = (Repeater)gr.FindControl("rp_test_sample");
                    DropDownList ddlChooseLabType = (DropDownList)gr.FindControl("ddlChooseLabType");
                    DropDownList ddlChooseLocation = (DropDownList)gr.FindControl("ddlChooseLocation");
                    TextBox txtCommentBeforeExtanalLab = (TextBox)gr.FindControl("txtCommentBeforeExtanalLab");
                    TextBox tet = (TextBox)gr.FindControl("tet");

                    if (lblStaidx.Text == "5") // จะเลือกวนหาข้อมูลถ้าเป็น node ที่เลือกสถานที่เพื่อทำการส่งตรวจ
                    {

                        data_qa _dtqa = new data_qa();
                        _dtqa.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];

                        var _u1doc_node3 = new qa_lab_u1_qalab[gvSampleTestDetal.Rows.Count];
                        int count_u1doc = 0;

                        Label lblDocIDX_Sample1 = (Label)gr.FindControl("lblDocIDX_Sample");

                        _u1doc_node3[count_u1doc] = new qa_lab_u1_qalab();
                        _u1doc_node3[count_u1doc].u1_qalab_idx = int.Parse(lblDocIDX_Sample1.Text);
                        _u1doc_node3[count_u1doc].decision = int.Parse(ddlChooseLabType.SelectedValue);
                        count_u1doc++;

                        var _u2doc_node3 = new qa_lab_u2_qalab[rp_test_sample.Items.Count];
                        int count_u2doc = 0;
                        foreach (RepeaterItem rpt in rp_test_sample.Items)
                        {
                            Label u2qalab_idx = rpt.FindControl("lblu2qalab_idx") as Label;

                            _u2doc_node3[count_u2doc] = new qa_lab_u2_qalab();
                            _u2doc_node3[count_u2doc].u2_qalab_idx = int.Parse(u2qalab_idx.Text);
                            _u2doc_node3[count_u2doc].m0_lab_idx = int.Parse(ddlChooseLocation.SelectedValue);
                            _u2doc_node3[count_u2doc].decision = int.Parse(ddlChooseLabType.SelectedValue);
                            _u2doc_node3[count_u2doc].staidx = int.Parse(lblStaidx.Text);
                            _u2doc_node3[count_u2doc].u1_qalab_idx = int.Parse(lblDocIDX_Sample1.Text);
                            _u2doc_node3[count_u2doc].comment = txtCommentBeforeExtanalLab.Text;
                            _u2doc_node3[count_u2doc].admin_emp_idx = _emp_idx;
                            _u2doc_node3[count_u2doc].u0_qalab_idx = int.Parse(_tbu0IDX.Text);
                            _u2doc_node3[count_u2doc].admin_org_idx = int.TryParse(hfEmpOrgIDX_n3.Value, out _default_int) ? int.Parse(hfEmpOrgIDX_n3.Value) : _default_int;
                            _u2doc_node3[count_u2doc].admin_rdept_idx = int.TryParse(hfEmpRdeptIDX_n3.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX_n3.Value) : _default_int;
                            _u2doc_node3[count_u2doc].admin_rsec_idx = int.TryParse(hfEmpRsecIDX_n3.Value, out _default_int) ? int.Parse(hfEmpRsecIDX_n3.Value) : _default_int;

                            count_u2doc++;
                        }

                        count_u1doc++;

                        _dtqa.qa_lab_u2_qalab_list = _u2doc_node3;
                        _dtqa.qa_lab_u1_qalab_list = _u1doc_node3;
                        _dtqa = callServicePostQA(_urlQaSetAppoveNode3, _dtqa);

                    }
                }

                setActiveTab("docDetail", 0, 0, 0, tbValuePlace.Text);
                setOntop.Focus();
                break;

            case "cmdSaveApproveActor1":
                // fvEmpDetailLab
                HiddenField hfEmpOrgIDX_n7 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n7 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n7 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRsecIDX");
                // fvEmpDetailLab

                TextBox tbu1_qalab_idx_n7 = (TextBox)fvSampleCode.FindControl("tbu1_qalab_idx_view");
                TextBox tbu0_qalab_idx_n7 = (TextBox)fvSampleCode.FindControl("tbu0_qalab_idx_view");
                Repeater rp_testdetail_lab_n7 = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");

                DropDownList ddlActor1Approve = (DropDownList)fvActor1_Approve.FindControl("ddlActor1Approve");
                TextBox tbActor1Comment = (TextBox)fvActor1_Approve.FindControl("tbActor1Comment");

                data_qa _data_node7 = new data_qa();

                _data_node7.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _node7_u1 = new qa_lab_u1_qalab();
                //update u1
                _node7_u1.cemp_idx = _emp_idx;
                _node7_u1.u0_qalab_idx = int.Parse(tbu0_qalab_idx_n7.Text);
                _node7_u1.u1_qalab_idx = int.Parse(tbu1_qalab_idx_n7.Text);
                _node7_u1.decision = int.Parse(ddlActor1Approve.SelectedValue);

                _data_node7.qa_lab_u1_qalab_list[0] = _node7_u1;


                //update u2
                _data_node7.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                qa_lab_u2_qalab _node7_u2 = new qa_lab_u2_qalab();


                var u2_node7 = new qa_lab_u2_qalab[rp_testdetail_lab_n7.Items.Count];
                int n7_u2 = 0;
                foreach (RepeaterItem rpt_detail_n7 in rp_testdetail_lab_n7.Items)
                {
                    Label u2_lab_detail_n7 = rpt_detail_n7.FindControl("u2_lab_detail_view") as Label;
                    Label u2_labtest_detail_idx_n7 = rpt_detail_n7.FindControl("u2_labtest_detailidx_view") as Label;

                    u2_node7[n7_u2] = new qa_lab_u2_qalab();
                    u2_node7[n7_u2].u0_qalab_idx = int.Parse(tbu0_qalab_idx_n7.Text);
                    u2_node7[n7_u2].u1_qalab_idx = int.Parse(tbu1_qalab_idx_n7.Text);
                    u2_node7[n7_u2].u2_qalab_idx = int.Parse(u2_lab_detail_n7.Text);
                    u2_node7[n7_u2].test_detail_idx = int.Parse(u2_labtest_detail_idx_n7.Text);
                    u2_node7[n7_u2].cemp_idx = _emp_idx;
                    u2_node7[n7_u2].decision = int.Parse(ddlActor1Approve.SelectedValue);

                    if (tbActor1Comment.Text != "")
                    {
                        u2_node7[n7_u2].comment = tbActor1Comment.Text;
                    }
                    else
                    {
                        u2_node7[n7_u2].comment = "-";
                    }

                    n7_u2++;

                }
                _data_node7.qa_lab_u1_qalab_list[0] = _node7_u1;
                _data_node7.qa_lab_u2_qalab_list = u2_node7;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_node7));
                _data_node7 = callServicePostQA(_urlQaSetApproveNode7, _data_node7);
                setActiveTab("docCreate", int.Parse(tbu0_qalab_idx_n7.Text), 0, 0, "0");

                break;
            case "cmdSaveApproveActor2N8":
                // fvEmpDetailLab
                HiddenField hfEmpOrgIDX_n8 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n8 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n8 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRsecIDX");
                // fvEmpDetailLab

                TextBox tbu1_qalab_idx_n8 = (TextBox)fvSampleCode.FindControl("tbu1_qalab_idx_view");
                TextBox tbu0_qalab_idx_n8 = (TextBox)fvSampleCode.FindControl("tbu0_qalab_idx_view");
                Repeater rp_testdetail_lab_n8 = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");

                DropDownList ddlActor2ApproveN8 = (DropDownList)fvActor2_Approve.FindControl("ddlActor2ApproveN8");
                TextBox tbActor2CommentN8 = (TextBox)fvActor2_Approve.FindControl("tbActor2CommentN8");

                data_qa _data_node8 = new data_qa();

                _data_node8.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _node8_u1 = new qa_lab_u1_qalab();
                //update u1
                _node8_u1.cemp_idx = _emp_idx;
                _node8_u1.u0_qalab_idx = int.Parse(tbu0_qalab_idx_n8.Text);
                _node8_u1.u1_qalab_idx = int.Parse(tbu1_qalab_idx_n8.Text);
                _node8_u1.decision = int.Parse(ddlActor2ApproveN8.SelectedValue);

                _data_node8.qa_lab_u1_qalab_list[0] = _node8_u1;


                //update u2
                _data_node8.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                qa_lab_u2_qalab _node8_u2 = new qa_lab_u2_qalab();


                var u2_node8 = new qa_lab_u2_qalab[rp_testdetail_lab_n8.Items.Count];
                int n8_u2 = 0;
                foreach (RepeaterItem rpt_detail_n7 in rp_testdetail_lab_n8.Items)
                {
                    Label u2_lab_detail_n8 = rpt_detail_n7.FindControl("u2_lab_detail_view") as Label;
                    Label u2_labtest_detail_idx_n8 = rpt_detail_n7.FindControl("u2_labtest_detailidx_view") as Label;

                    u2_node8[n8_u2] = new qa_lab_u2_qalab();
                    u2_node8[n8_u2].u0_qalab_idx = int.Parse(tbu0_qalab_idx_n8.Text);
                    u2_node8[n8_u2].u1_qalab_idx = int.Parse(tbu1_qalab_idx_n8.Text);
                    u2_node8[n8_u2].u2_qalab_idx = int.Parse(u2_lab_detail_n8.Text);
                    u2_node8[n8_u2].test_detail_idx = int.Parse(u2_labtest_detail_idx_n8.Text);
                    u2_node8[n8_u2].cemp_idx = _emp_idx;
                    u2_node8[n8_u2].decision = int.Parse(ddlActor2ApproveN8.SelectedValue);

                    if (tbActor2CommentN8.Text != "")
                    {
                        u2_node8[n8_u2].comment = tbActor2CommentN8.Text;
                    }
                    else
                    {
                        u2_node8[n8_u2].comment = "-";
                    }

                    n8_u2++;

                }
                _data_node8.qa_lab_u1_qalab_list[0] = _node8_u1;
                _data_node8.qa_lab_u2_qalab_list = u2_node8;

                _data_node8 = callServicePostQA(_urlQaSetApproveNode8, _data_node8);
                setActiveTab("docCreate", int.Parse(tbu0_qalab_idx_n8.Text), 0, 0, "0");

                break;
            case "cmdSaveNode11":

                //decision
                string node11_sucess = cmdArg;

                // fvEmpDetailLab
                HiddenField hfEmpOrgIDX_n11 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX_n11 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX_n11 = (HiddenField)fvEmpDetailLab.FindControl("hfEmpRsecIDX");
                // fvEmpDetailLab
                TextBox _tbu0IDX_11 = (TextBox)fvDetail.FindControl("tbU0IDX");

                data_qa _data_node11 = new data_qa();
                _data_node11.qa_lab_u0_qalab_list = new qa_lab_u0_qalab[1];
                qa_lab_u0_qalab _node11_u0 = new qa_lab_u0_qalab();
                //update u1
                _node11_u0.cemp_idx = _emp_idx;
                _node11_u0.u0_qalab_idx = int.Parse(_tbu0IDX_11.Text);
                _node11_u0.decision = int.Parse(node11_sucess);

                _data_node11.qa_lab_u0_qalab_list[0] = _node11_u0;

                _data_node11 = callServicePostQA(_urlQaSetApproveNode11, _data_node11);
                setActiveTab("docDetail", 0, 0, 0, "0");
                setOntop.Focus();
                break;
            case "cmdDocCancelUserAdmin":
                TextBox tbu0_qalab_idx_external_cancel = (TextBox)fvSampleCode.FindControl("tbu0_qalab_idx_view");
                setActiveTab("docCreate", int.Parse(tbu0_qalab_idx_external_cancel.Text), 0, 0, "0");
                setOntop.Focus();
                break;
            case "cmdDocCancel":

                //  TextBox check_m0_lab = (TextBox)fvDetailLab.FindControl("check_m0_lab");

                if (tbValuePlace.Text == "0")
                {
                    setActiveTab("docDetail", 0, 0, 0, "0");
                }
                else
                {
                    setActiveTab("docDetail", 0, 0, 0, tbValuePlace.Text);
                }
                setOntop.Focus();


                break;
            case "cmdDocCancelLab":
                //litDebug.Text = ViewState["vsArgPlaceLab"].ToString();


                TextBox check_m0_lab = (TextBox)fvDetailLab.FindControl("check_m0_lab");

                if (check_m0_lab.Text == "0")
                {
                    setActiveTab("docLab", 0, 0, 0, "0");
                    setOntop.Focus();
                }
                else
                {

                    //litDebug.Text = check_m0_lab.Text + ',';
                    setActiveTab("docLab", 0, 0, 0, check_m0_lab.Text);
                    setOntop.Focus();
                }


                //setActiveTab("docLab", 0, 0, 0, "0");
                //setOntop.Focus();
                break;
            case "cmdDocCancelResult":
                setActiveTab("docLabResult", 0, 0, 0, "0");
                setOntop.Focus();
                break;
            case "cmdSaveNode5":

                string[] cmdArgDecision = cmdArg.Split(',');
                int _decision_internal_lab = int.TryParse(cmdArgDecision[0].ToString(), out _default_int) ? int.Parse(cmdArgDecision[0].ToString()) : _default_int;
                int _decision_external_lab = int.TryParse(cmdArgDecision[1].ToString(), out _default_int) ? int.Parse(cmdArgDecision[1].ToString()) : _default_int;

                GridView GvFormRecordResult = (GridView)fvAnalyticalResults.FindControl("GvFormRecordResult");
                TextBox txtSampleCodeResult = (TextBox)fvAnalyticalResults.FindControl("txtSampleCodeResult");
                FileUpload UploadFileResult = (FileUpload)fvAnalyticalResults.FindControl("UploadFileResult");

                foreach (GridViewRow grform in GvFormRecordResult.Rows)
                {
                    // Record test result in level 1
                    Label _status_decision = (Label)grform.FindControl("Lbstaidx_result");
                    Label _u2_idx_result = (Label)grform.FindControl("Lbu2_qalab_idx");
                    Label _r1_form_idx_result = (Label)grform.FindControl("Lbr1form_create_idx");
                    Label _u1_idx_result = (Label)grform.FindControl("Lbu1_qalab_idx");
                    Label _u0_idx_result = (Label)grform.FindControl("Lbu0_qalab_idx");
                    Label _lbl_lab_type_idx = (Label)grform.FindControl("lbl_lab_type_idx");

                    TextBox _input_textResult = (TextBox)grform.FindControl("txtRecordResultTest");

                    switch (_status_decision.Text)
                    {
                        case "7":
                            ViewState["vs_status_result"] = int.Parse(_decision_internal_lab.ToString());
                            break;

                        case "13":
                            ViewState["vs_status_result"] = int.Parse(_decision_external_lab.ToString());
                            break;
                    }

                    data_qa _data_lab_result = new data_qa();
                    _data_lab_result.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    _data_lab_result.qa_lab_u2_qalab_list = new qa_lab_u2_qalab[1];
                    _data_lab_result.qa_lab_u4_qalab_list = new qa_lab_u4_qalab[1];
                    _data_lab_result.qa_lab_u5_qalab_list = new qa_lab_u5_qalab[1];

                    var _u1doc_node5 = new qa_lab_u1_qalab[GvFormRecordResult.Rows.Count];
                    var _u2doc_node5 = new qa_lab_u2_qalab[GvFormRecordResult.Rows.Count];
                    var _u5doc_node5 = new qa_lab_u5_qalab[GvFormRecordResult.Rows.Count];

                    int count_u2idx = 0;

                    _u1doc_node5[count_u2idx] = new qa_lab_u1_qalab();
                    _u1doc_node5[count_u2idx].u1_qalab_idx = int.Parse(_u1_idx_result.Text);
                    _u1doc_node5[count_u2idx].cemp_idx = _emp_idx;
                    _u1doc_node5[count_u2idx].decision = int.Parse(ViewState["vs_status_result"].ToString());

                    _u2doc_node5[count_u2idx] = new qa_lab_u2_qalab();
                    _u2doc_node5[count_u2idx].u2_qalab_idx = int.Parse(_u2_idx_result.Text);
                    _u2doc_node5[count_u2idx].u1_qalab_idx = int.Parse(_u1_idx_result.Text);
                    _u2doc_node5[count_u2idx].u0_qalab_idx = int.Parse(_u0_idx_result.Text);
                    _u2doc_node5[count_u2idx].cemp_idx = _emp_idx;
                    _u2doc_node5[count_u2idx].decision = int.Parse(ViewState["vs_status_result"].ToString());

                    _u5doc_node5[count_u2idx] = new qa_lab_u5_qalab();
                    _u5doc_node5[count_u2idx].u2_qalab_idx = int.Parse(_u2_idx_result.Text);
                    _u5doc_node5[count_u2idx].r1_form_create_idx = int.Parse(_r1_form_idx_result.Text);
                    _u5doc_node5[count_u2idx].detail_tested = _input_textResult.Text;
                    _u5doc_node5[count_u2idx].cemp_idx = _emp_idx;

                    count_u2idx++;

                    _data_lab_result.qa_lab_u1_qalab_list = _u1doc_node5;
                    _data_lab_result.qa_lab_u2_qalab_list = _u2doc_node5;
                    _data_lab_result.qa_lab_u5_qalab_list = _u5doc_node5;

                    _data_lab_result = callServicePostQA(_urlQaSetRecordResultTest, _data_lab_result);

                    if (UploadFileResult.HasFile)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["path_flie_external_lab"];
                        string sample_code = txtSampleCodeResult.Text;
                        string fileName_upload = sample_code;
                        string filePath_upload = Server.MapPath(getPathfile + sample_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(UploadFileResult.FileName);

                        UploadFileResult.SaveAs(Server.MapPath(getPathfile + sample_code) + "\\" + fileName_upload + extension);

                    }
                    else
                    {

                    }
                }

                setActiveTab("docLabResult", 0, 0, 0, "0");
                //setOntop.Focus();

                break;
            case "btnHistoryDocument":
                HyperLink setOnSamplelist = (HyperLink)rptHistoryDocument.Items[0].FindControl("setOnSamplelist");
                btnHistoryDocument.Visible = false;
                btnHistoryBack.Visible = true;

                rptHistoryDocument.Visible = true;
                setOnSamplelist.Focus();
                break;
            case "btnHistoryBack":

                btnHistoryBack.Visible = false;
                btnHistoryDocument.Visible = true;

                rptHistoryDocument.Visible = false;
                setOnSample.Focus();
                break;
            case "btnSearchIndex":
                linkBtnTrigger(btnResetSearch);
                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearchIndex.Visible = false;
                btnResetSearchPage.Visible = false;

                break;
            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;
                btnSearchIndex.Visible = true;
                btnResetSearchPage.Visible = true;
                setActiveTab("docDetail", 0, 0, 0, tbValuePlace.Text);
                break;

            case "btnSearchIndexDetail":

                TextBox txt_document_nosearch = (TextBox)docDetail.FindControl("txt_document_nosearch");
                TextBox txt_sample_codesearch = (TextBox)docDetail.FindControl("txt_sample_codesearch");
                TextBox txt_mat_search = (TextBox)docDetail.FindControl("txt_mat_search");
                TextBox txt_date_search = (TextBox)docDetail.FindControl("txt_date_search");
                TextBox txt_too_search = (TextBox)docDetail.FindControl("txt_too_search");
                TextBox txt_customer_search = (TextBox)docDetail.FindControl("txt_customer_search");
                TextBox txt_shift_search = (TextBox)docDetail.FindControl("txt_shift_search");
                TextBox txt_time_search = (TextBox)docDetail.FindControl("txt_time_search");
                HiddenField HiddenDateSearch = (HiddenField)docDetail.FindControl("HiddenDateSearch");
                HiddenField HiddenTimeSearch = (HiddenField)docDetail.FindControl("HiddenTimeSearch");


                gvDoingList.PageIndex = 0;
                if (ViewState["rdept_permission"].ToString() != "27")
                {
                    data_qa _data_searchdetail = new data_qa();
                    _data_searchdetail.search_document_list = new seach_document_detail[1];
                    seach_document_detail _searchindex = new seach_document_detail();
                    //search detail

                    _searchindex.document_code = txt_document_nosearch.Text;
                    _searchindex.sample_code = txt_sample_codesearch.Text;
                    _searchindex.material_code = txt_mat_search.Text;
                    _searchindex.create_date = HiddenDateSearch.Value.ToString();
                    _searchindex.cabinet = txt_too_search.Text;
                    _searchindex.customer_name = txt_customer_search.Text;
                    _searchindex.shift_time = txt_shift_search.Text;
                    _searchindex.time = HiddenTimeSearch.Value.ToString();
                    _searchindex.cemp_idx = _emp_idx;
                    _searchindex.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                    _searchindex.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    _searchindex.place_idx = int.Parse(tbValuePlace.Text);


                    _data_searchdetail.search_document_list[0] = _searchindex;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));
                    _data_searchdetail = callServicePostQA(_urlQaSearchDetailInLabNew, _data_searchdetail);
                    if (_data_searchdetail.return_code == 0)
                    {
                        ViewState["vs_gvDetail"] = _data_searchdetail.search_document_list;
                        setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                        setGridData(gvDoingList, null);
                        break;
                    }

                }

                else if (ViewState["rdept_permission"].ToString() == "27")
                {
                    data_qa _data_searchdetail = new data_qa();
                    _data_searchdetail.search_document_list = new seach_document_detail[1];
                    seach_document_detail _searchindex = new seach_document_detail();
                    //search detail

                    _searchindex.document_code = txt_document_nosearch.Text;
                    _searchindex.sample_code = txt_sample_codesearch.Text;
                    _searchindex.material_code = txt_mat_search.Text;
                    _searchindex.create_date = HiddenDateSearch.Value.ToString();
                    _searchindex.cabinet = txt_too_search.Text;
                    _searchindex.customer_name = txt_customer_search.Text;
                    _searchindex.shift_time = txt_shift_search.Text;
                    _searchindex.time = HiddenTimeSearch.Value.ToString();
                    _searchindex.cemp_idx = _emp_idx;
                    _searchindex.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                    _searchindex.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    _searchindex.place_idx = int.Parse(tbValuePlace.Text);

                    _data_searchdetail.search_document_list[0] = _searchindex;

                     //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));

                    _data_searchdetail = callServicePostQA(_urlQaSearchDetailInLabNew, _data_searchdetail);


                    if (_data_searchdetail.return_code == 0)
                    {
                        ViewState["vs_gvDetail"] = _data_searchdetail.search_document_list;
                        setGridData(gvDoingList, ViewState["vs_gvDetail"]);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูล!!!');", true);
                        setGridData(gvDoingList, null);
                        break;
                    }

                }

                

                break;

            case "btnSearchSampleLab":
                gvLab.PageIndex = 0;
                TextBox txtseach_samplecode_lab = (TextBox)docLab.FindControl("txtseach_samplecode_lab");

                data_qa _data_search_inlab = new data_qa();
                _data_search_inlab.search_document_inlab_list = new seach_document_inlab_detail[1];
                seach_document_inlab_detail _search_inlab = new seach_document_inlab_detail();

                _search_inlab.m0_lab_idx = int.Parse(ViewState["vsArgPlaceLab"].ToString());
                _search_inlab.sample_code = txtseach_samplecode_lab.Text;

                _data_search_inlab.search_document_inlab_list[0] = _search_inlab;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_search_inlab));

                _data_search_inlab = callServicePostQA(_urlQaSearchDetailInLab, _data_search_inlab);

                ViewState["vs_gvLab_Detail"] = _data_search_inlab.search_document_inlab_list;

                setGridData(gvLab, ViewState["vs_gvLab_Detail"]);

                txtseach_samplecode_lab.Text = string.Empty;

                //search detail

                break;
            case "btnResetInLab":

                TextBox txtseach_samplecode_lab_reset = (TextBox)docLab.FindControl("txtseach_samplecode_lab");
                txtseach_samplecode_lab_reset.Text = String.Empty;
                lbdetailsShow.Text = "";
                setActiveTab("docLab", 0, 0, 0, "0");

                break;

            case "printSampleCode":

                int _U0IDX = int.Parse(cmdArg);

                RadioButton _rdNeedPrintShowQrCode = (RadioButton)fvSampleTestDetail.FindControl("rdbuttonNeedPrint");
                RadioButton _rdNotNeedPrintShowQrCode = (RadioButton)fvSampleTestDetail.FindControl("rdbuttonNotNeedPrint");

                CheckBox _ChkPrintQrCode = (CheckBox)fvSampleTestDetail.FindControl("ChkPrintQrCode");
                CheckBox _ChkPrintTestedItems = (CheckBox)fvSampleTestDetail.FindControl("ChkPrintTestedItems");

                TextBox _genplaceidx = (TextBox)fvDetail.FindControl("tbplaceidx");


                GridView gvSampleTestDetals = (GridView)fvSampleTestDetail.FindControl("gvSampleTestDetal");
                int _rowCountGrid = 0;


                
                foreach (GridViewRow rowCountGide in gvSampleTestDetals.Rows)
                {
                    Label _SampleCodeName = (Label)rowCountGide.FindControl("lblSampleCode");
                    Label _U1IDX = (Label)rowCountGide.FindControl("lblDocIDX_Sample");

                    string txtGenQrCode = "http://mas.taokaenoi.co.th/labis-lab-details/" + _funcTool.getEncryptRC4(_U1IDX.Text, _keysamplecode);

                    string getPathimages = ConfigurationManager.AppSettings["path_qr_code_qa"];
                    string fileNameimage = _SampleCodeName.Text; // ชื่อรูป QRCode
                    string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

                    QRCodeEncoder encoder = new QRCodeEncoder();
                    Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

                    bi.Save(Server.MapPath(getPathimages + _genplaceidx.Text + fileNameimage + ".jpg"), ImageFormat.Jpeg);

                    _rowCountGrid++;

                    Session["_SESSION_U1DOCIDX"] = int.Parse(_U1IDX.Text).ToString();
                    Session["_SESSION_Place"] = (_genplaceidx.Text).ToString();


                    if (txtGenQrCode != null)
                    {

                    }
                    else
                    {

                    }
                }

                if (_ChkPrintQrCode.Checked)
                {
                    Session["_SESSION_PrintShowQrCode"] = 1;
                }
                else if (!_ChkPrintQrCode.Checked)
                {
                    Session["_SESSION_PrintShowQrCode"] = 0;
                }
                if (_ChkPrintTestedItems.Checked)
                {
                    Session["_SESSION_PrintShowTestItems"] = 1;
                }
                else if (!_ChkPrintTestedItems.Checked)
                {
                    Session["_SESSION_PrintShowTestItems"] = 0;
                }

                Session["_SESSION_U0DOCIDX"] = _U0IDX;


                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('labis-lab-print', '', '');", true);

                break;

            case "btnSearchResult":
                getSmapleList_AnalyticalResults((TextBox)fvAnalyticalResults.FindControl("txtSearchSampleCode"));
                break;

            case "resetSearch":

                TextBox _txt_document_nosearch = (TextBox)docDetail.FindControl("txt_document_nosearch");
                TextBox _txt_sample_codesearch = (TextBox)docDetail.FindControl("txt_sample_codesearch");
                TextBox _txt_mat_search = (TextBox)docDetail.FindControl("txt_mat_search");
                TextBox _txt_date_search = (TextBox)docDetail.FindControl("txt_date_search");
                TextBox _txt_too_search = (TextBox)docDetail.FindControl("txt_too_search");
                TextBox _txt_customer_search = (TextBox)docDetail.FindControl("txt_customer_search");
                TextBox _txt_shift_search = (TextBox)docDetail.FindControl("txt_shift_search");
                TextBox _txt_time_search = (TextBox)docDetail.FindControl("txt_time_search");
                HiddenField _HiddenDateSearch = (HiddenField)docDetail.FindControl("HiddenDateSearch");
                HiddenField _HiddenTimeSearch = (HiddenField)docDetail.FindControl("HiddenTimeSearch");

                _txt_document_nosearch.Text = string.Empty;
                _txt_sample_codesearch.Text = string.Empty;
                _txt_mat_search.Text = string.Empty;
                _txt_date_search.Text = string.Empty;
                _txt_too_search.Text = string.Empty;
                _txt_customer_search.Text = string.Empty;
                _txt_shift_search.Text = string.Empty;
                _txt_time_search.Text = string.Empty;
                _HiddenDateSearch.Value = string.Empty;
                _HiddenTimeSearch.Value = string.Empty;

                gvDoingList.PageIndex = 0;

                // ssss


                data_qa _data_qaindexreset = new data_qa();
                _data_qaindexreset.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                qa_lab_u0doc_qalab _u0_doc_index_reset = new qa_lab_u0doc_qalab();
                _u0_doc_index_reset.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                _u0_doc_index_reset.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                _u0_doc_index_reset.cemp_idx = _emp_idx;
                _u0_doc_index_reset.place_idx = int.Parse(tbValuePlace.Text);

                _data_qaindexreset.qa_lab_u0doc_qalab_list[0] = _u0_doc_index_reset;

                _data_qaindexreset = callServicePostQA(_urlQaGetViewRsecReference, _data_qaindexreset);

                if (_data_qaindexreset.return_code == 0)
                {
                    //litDebug.Text = "9999911111";
                    lbl_detail_placeindex.Visible = true;
                    var linqDocumentPlace = from _dtdocumentplace in _data_qaindexreset.qa_lab_u0doc_qalab_list
                                            where _dtdocumentplace.place_idx == int.Parse(tbValuePlace.Text)
                                            select _dtdocumentplace;

                    if (tbValuePlace.Text != "0")
                    {
                        ViewState["vs_gvDetail"] = linqDocumentPlace.ToList();
                        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    }
                    else
                    {
                        //litDebug.Text = "10211111";
                        ViewState["vs_gvDetail"] = _data_qaindexreset.qa_lab_u0doc_qalab_list;
                        lbl_detail_placeindex.Visible = false;
                        //lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    }



                    //var t1 = linqDocumentPlace.ToArray();

                    //foreach (var c in linqDocumentPlace)
                    //{

                    //    if (c.place_name != null || c.place_name != "")
                    //    {
                    //        //litDebug.Text = "10211111";
                    //        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + c.place_name;
                    //    }
                    //    else
                    //    {
                    //        //litDebug.Text = "4444";
                    //        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    //    }

                    //}


                }
                else
                {
                    //litDebug.Text = "11111";
                    ViewState["vs_gvDetail"] = null;
                    if (tb_PlaceName.Text != "")
                    {
                        lbl_detail_placeindex.Visible = true;
                        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    }
                    else
                    {
                        lbl_detail_placeindex.Visible = false;
                        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + "";
                    }

                }


                //ViewState["vs_gvDetail"] = _data_qaindexreset.qa_lab_u0doc_qalab_list;
                setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                break;

            case "cmdPrintReport":

                int _U1ReportIDX = int.Parse(cmdArg);
                Session["_SESSION_REPORT_ANALYSIS"] = _U1ReportIDX;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('labis-print-report-analysis', '', '');", true);

                break;

            case "cmdReportSucess":

                int _U1ReportIDX_Lab = int.Parse(cmdArg);
                Session["_SESSION_REPORT_ANALYSIS"] = _U1ReportIDX_Lab;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('labis-print-report-analysis', '', '');", true);

                break;

            case "CmdExport":

                string[] cmdArgExportExcel = cmdArg.Split(';');
                int U0IDXExpoetExcel = int.TryParse(cmdArgExportExcel[0].ToString(), out _default_int) ? int.Parse(cmdArgExportExcel[0].ToString()) : _default_int;
                int U1IDXExpoetExcel = int.TryParse(cmdArgExportExcel[1].ToString(), out _default_int) ? int.Parse(cmdArgExportExcel[1].ToString()) : _default_int;

                //TextBox tbplaceidx = (TextBox)fvDetail.FindControl("tbplaceidx");

                //int _u0docidx_export = int.Parse(cmdArg);
                //  ViewState["vsU0docidxExportExcel"] = U0IDXExpoetExcel;
                data_qa _dataExport = new data_qa();
                _dataExport.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _export_result = new qa_lab_u1_qalab();
                _export_result.u0_qalab_idx = U0IDXExpoetExcel;
                _export_result.u1_qalab_idx = U1IDXExpoetExcel;
                _dataExport.qa_lab_u1_qalab_list[0] = _export_result;

                _dataExport = callServicePostQA(_urlQaGetDetailExportExcel, _dataExport);

                if (_dataExport.return_code == 0)
                {

                    data_qa _dataExport_header = new data_qa();
                    _dataExport_header.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _export_header = new qa_lab_u1_qalab();
                    _export_header.condition = 0;
                    _dataExport_header.qa_lab_u1_qalab_list[0] = _export_header;
                    _dataExport_header = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_header);

                    ViewState["vs_listLabExportExcel"] = _dataExport_header.qa_lab_u1_qalab_list;

                    string _header_text_export = String.Empty;
                    int _header_idx_export;
                    string _setHeader_idx_export = "";
                    int _countrowExport = 3;
                    DataTable table = new DataTable();

                    table.Columns.Add("วันทดสอบ", typeof(String));
                    table.Columns.Add("Document Code", typeof(String));
                    table.Columns.Add("Sample Code", typeof(String));
                    table.Columns.Add("Mat", typeof(String));
                    table.Columns.Add("Sample Name", typeof(String));
                    table.Columns.Add("ประเภทตัวอย่าง", typeof(String));
                    table.Columns.Add("MFD/EXP", typeof(String));
                    table.Columns.Add("Batch", typeof(String));
                    table.Columns.Add("ชื่อลุกค้า", typeof(String));
                    table.Columns.Add("เลขตู้", typeof(String));
                    table.Columns.Add("กะ", typeof(String));
                    table.Columns.Add("เวลา", typeof(String));
                    table.Columns.Add("รายละเอียดอื่นๆ", typeof(String));
                    table.Columns.Add("องค์กรสายการผลิต", typeof(String));
                    table.Columns.Add("ฝ่ายสายการผลิต", typeof(String));
                    table.Columns.Add("แผนกสายการผลิต", typeof(String));
                    table.Columns.Add("วันที่รับตัวอย่าง", typeof(String));

                    string resultString1 = "";
                    foreach (var _loop_header_export in _dataExport_header.qa_lab_u1_qalab_list)
                    {
                        ViewState["_header_idx_export_row1"] = int.Parse(_loop_header_export.test_detail_idx.ToString());
                        _header_idx_export = int.Parse(_loop_header_export.test_detail_idx.ToString());
                        _header_text_export = _loop_header_export.test_detail_name.ToString();
                        table.Columns.Add(_header_text_export, typeof(String));
                        _setHeader_idx_export += "," + _header_idx_export;
                        _countrowExport++;
                        //  ViewState["rowColumnExportExcel"] = _countrowExport;
                        resultString1 = _setHeader_idx_export.IndexOf(',') > -1
                        ? _setHeader_idx_export.Substring(_setHeader_idx_export.IndexOf(',') + 1)
                        : _setHeader_idx_export;


                    }

                    data_qa _dataExport_documentNo = new data_qa();
                    _dataExport_documentNo.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _export_document = new qa_lab_u1_qalab();
                    _export_document.u0_qalab_idx = U0IDXExpoetExcel;
                    _export_document.condition = 1;
                    _dataExport_documentNo.qa_lab_u1_qalab_list[0] = _export_document;
                    _dataExport_documentNo = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_documentNo);
                    ViewState["vs_listLabExportExcel1"] = _dataExport_documentNo.qa_lab_u1_qalab_list;
                    //  table.Columns.Add("Approve", typeof(String));
                    // table.Rows.Add(88888, 80000, 7777, 001);
                    string _text = String.Empty;
                    int _countrow1 = 1;
                    // table.Rows.Add(88888, 80000, 7777, 001);
                    if (_dataExport_documentNo.qa_lab_u1_qalab_list == null)
                    {
                        //  GvShowExportExcel.Visible = false;
                        //  litDebug.Text = "11111";
                    }
                    else
                    {

                        foreach (var _loop_rowText in _dataExport_documentNo.qa_lab_u1_qalab_list)
                        {

                            //gen label
                            Label dynamicLabelExport_Testdate = new Label();
                            Label dynamicLabelExport_Document = new Label();
                            Label dynamicLabelExport_SampleCode = new Label();
                            Label dynamicLabelExport_Mat = new Label();
                            Label dynamicLabelExport_SampleName = new Label();
                            Label dynamicLabelExport_materail_type = new Label();
                            Label dynamicLabelExport_EXP = new Label();
                            Label dynamicLabelExport_Batch = new Label();
                            Label dynamicLabelExport_CustomerName = new Label();
                            Label dynamicLabelExport_Cabinet = new Label();
                            Label dynamicLabelExport_Shifttime = new Label();
                            Label dynamicLabelExport_Time = new Label();
                            Label dynamicLabelExport_Other = new Label();
                            Label dynamicLabelExport_Orgname = new Label();
                            Label dynamicLabelExport_Deptname = new Label();
                            Label dynamicLabelExport_Secname = new Label();
                            Label dynamicLabelExport_ReceiveDate = new Label();

                            dynamicLabelExport_Testdate.Text = _loop_rowText.test_date.ToString();
                            dynamicLabelExport_Document.Text = _loop_rowText.document_code.ToString();
                            dynamicLabelExport_SampleCode.Text = _loop_rowText.sample_code.ToString();
                            dynamicLabelExport_Mat.Text = _loop_rowText.material_code.ToString();
                            dynamicLabelExport_SampleName.Text = _loop_rowText.sample_name.ToString();
                            dynamicLabelExport_materail_type.Text = _loop_rowText.materail_type.ToString();
                            dynamicLabelExport_EXP.Text = _loop_rowText.EXP_MFD.ToString();
                            dynamicLabelExport_Batch.Text = _loop_rowText.batch.ToString();
                            dynamicLabelExport_CustomerName.Text = _loop_rowText.customer_name.ToString();
                            dynamicLabelExport_Cabinet.Text = _loop_rowText.cabinet.ToString();
                            dynamicLabelExport_Shifttime.Text = _loop_rowText.shift_time.ToString();
                            dynamicLabelExport_Time.Text = _loop_rowText.time.ToString();
                            dynamicLabelExport_Other.Text = _loop_rowText.other_details.ToString();
                            dynamicLabelExport_Orgname.Text = _loop_rowText.org_name_th.ToString();
                            dynamicLabelExport_Deptname.Text = _loop_rowText.dept_name_th.ToString();
                            dynamicLabelExport_Secname.Text = _loop_rowText.sec_name_th.ToString();
                            dynamicLabelExport_ReceiveDate.Text = _loop_rowText.received_sample_date.ToString();


                            _countrow1++;
                            DataRow newRow1 = table.NewRow();
                            //newRow[0] = "0";
                            newRow1[0] = dynamicLabelExport_Testdate.Text;
                            newRow1[1] = dynamicLabelExport_Document.Text;
                            newRow1[2] = dynamicLabelExport_SampleCode.Text;
                            newRow1[3] = dynamicLabelExport_Mat.Text;
                            newRow1[4] = dynamicLabelExport_SampleName.Text;
                            newRow1[5] = dynamicLabelExport_materail_type.Text;
                            newRow1[6] = dynamicLabelExport_EXP.Text;
                            newRow1[7] = dynamicLabelExport_Batch.Text;
                            newRow1[8] = dynamicLabelExport_CustomerName.Text;
                            newRow1[9] = dynamicLabelExport_Cabinet.Text;
                            newRow1[10] = dynamicLabelExport_Shifttime.Text;
                            newRow1[11] = dynamicLabelExport_Time.Text;
                            newRow1[12] = dynamicLabelExport_Other.Text;
                            newRow1[13] = dynamicLabelExport_Orgname.Text;
                            newRow1[14] = dynamicLabelExport_Deptname.Text;
                            newRow1[15] = dynamicLabelExport_Secname.Text;
                            newRow1[16] = dynamicLabelExport_ReceiveDate.Text;


                            data_qa _dataExport_document_result = new data_qa();
                            _dataExport_document_result.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                            qa_lab_u1_qalab _result_rowExport = new qa_lab_u1_qalab();
                            _result_rowExport.u0_qalab_idx = U0IDXExpoetExcel;
                            //_result_rowExport.place_idx = int.Parse(tbplaceidx.Text);
                            _result_rowExport.condition = 3;

                            _dataExport_document_result.qa_lab_u1_qalab_list[0] = _result_rowExport;
                            _dataExport_document_result = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_document_result);

                            ViewState["data_result5"] = _dataExport_document_result.qa_lab_u1_qalab_list;

                            //litDebug.Text = tbplaceidx.Text;

                            int t3 = 0;
                            for (int t0 = 17; t0 < table.Columns.Count; t0++)
                            {
                                newRow1[t0] = _functionInputResultToColumn(U0IDXExpoetExcel, _loop_rowText.sample_code.ToString(), _dataExport_header.qa_lab_u1_qalab_list[t3].test_detail_idx.ToString());

                                t3++;
                            }


                            table.Rows.InsertAt(newRow1, 0);

                        }

                        //GvShowExportExcel.Visible = true;

                        ViewState["vsTableExportExcel"] = table;
                        GvShowExportExcel.DataSource = table;
                        GvShowExportExcel.DataBind();

                        WriteExcelWithNPOI(table, "xls", "Report");

                        ////string strBody = string.Empty;
                        ////strBody = @"<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                        ////"xmlns:w='urn:schemas-microsoft-com:office:word'" +
                        ////"xmlns='http://www.w3.org/TR/REC-html40'>";

                        ////strBody = strBody + "<!--[if gte mso 9]>" +
                        ////"<xml>" +
                        ////"<w:WordDocument>" +
                        ////"<w:View>Print</w:View>" +
                        ////"<w:Zoom>100</w:Zoom>" +
                        ////"</w:WordDocument>" +
                        ////"</xml>" +
                        ////"<![endif]-->";
                        ////HttpContext.Current.Response.Clear();
                        ////HttpContext.Current.Response.Charset = "";
                        ////HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        ////HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=Report.xls");

                        ////HttpContext.Current.Response.Write("<meta http-equiv='ContentType' content='text/html; charset=utf-8'>");

                        ////StringBuilder htmlCode = new StringBuilder();
                        ////htmlCode.Append("<html>");
                        ////htmlCode.Append("<head>" + strBody + " <style type=\"text/css\">body {font-family:arial;font-size:14.5;}</style></head>");
                        ////htmlCode.Append("<body>");

                        ////HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                        //// "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                        //// "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
                        //////am getting my grid's column headers
                        ////int columnscount = GvShowExportExcel.Columns.Count;

                        //////   HttpContext.Current.Response.Write("<TR>");
                        ////// On all tables' columns
                        ////foreach (DataColumn dc in table.Columns)
                        ////{
                        ////    //var field1 = dtRow[dc].ToString();
                        ////    HttpContext.Current.Response.Write("<Td>");
                        ////    HttpContext.Current.Response.Write(dc.ToString());
                        ////    HttpContext.Current.Response.Write("</Td>");
                        ////}
                        ////HttpContext.Current.Response.Write("</TR>");

                        //////   HttpContext.Current.Response.Write("</TR>");
                        ////foreach (DataRow row in table.Rows)
                        ////{//write in new row
                        ////    HttpContext.Current.Response.Write("<TR>");
                        ////    for (int i = 0; i < table.Columns.Count; i++)
                        ////    {
                        ////        if (i == 0)
                        ////        {
                        ////            HttpContext.Current.Response.Write("<Td>");
                        ////            HttpContext.Current.Response.Write(row[i].ToString());
                        ////            HttpContext.Current.Response.Write("</Td>");
                        ////        }
                        ////        else
                        ////        {

                        ////            HttpContext.Current.Response.Write("<Td>");
                        ////            HttpContext.Current.Response.Write(row[i].ToString());
                        ////            HttpContext.Current.Response.Write("</Td>");
                        ////        }

                        ////    }

                        ////    HttpContext.Current.Response.Write("</TR>");
                        ////}

                        ////htmlCode.Append("</body></html>");
                        ////HttpContext.Current.Response.Write(htmlCode.ToString());
                        ////HttpContext.Current.Response.End();
                        ////HttpContext.Current.Response.Flush();
                        ////HttpContext.Current.Response.Write("</TR>");



                        //HttpContext.Current.Response.Clear();
                        //HttpContext.Current.Response.ClearContent();
                        //HttpContext.Current.Response.ClearHeaders();
                        //HttpContext.Current.Response.Buffer = true;

                        //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");


                        //HttpContext.Current.Response.Charset = "utf-8";
                        //HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");

                        //HttpContext.Current.Response.ContentType = "application/ms-excel";
                        //HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        //HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");

                        ////sets font
                        //HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
                        //HttpContext.Current.Response.Write("<BR><BR><BR>");
                        ////sets the table border, cell spacing, border color, font of the text, background, foreground, font height
                        //HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                        //  "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                        //  "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
                        ////am getting my grid's column headers
                        //int columnscount = GvShowExportExcel.Columns.Count;


                        //HttpContext.Current.Response.Write("<TR>");
                        //// On all tables' columns
                        //foreach (DataColumn dc in table.Columns)
                        //{
                        //    //var field1 = dtRow[dc].ToString();
                        //    HttpContext.Current.Response.Write("<Td>");
                        //    HttpContext.Current.Response.Write(dc.ToString());
                        //    HttpContext.Current.Response.Write("</Td>");
                        //}
                        //HttpContext.Current.Response.Write("</TR>");


                        //HttpContext.Current.Response.Write("</TR>");
                        //foreach (DataRow row in table.Rows)
                        //{//write in new row
                        //    HttpContext.Current.Response.Write("<TR>");
                        //    for (int i = 0; i < table.Columns.Count; i++)
                        //    {
                        //        if (i == 0)
                        //        {
                        //            HttpContext.Current.Response.Write("<Td>");
                        //            HttpContext.Current.Response.Write(row[i].ToString());
                        //            HttpContext.Current.Response.Write("</Td>");
                        //        }
                        //        else
                        //        {

                        //            HttpContext.Current.Response.Write("<Td>");
                        //            HttpContext.Current.Response.Write(row[i].ToString());
                        //            HttpContext.Current.Response.Write("</Td>");
                        //        }

                        //    }

                        //    HttpContext.Current.Response.Write("</TR>");
                        //}

                        //HttpContext.Current.Response.Write("</Table>");
                        //HttpContext.Current.Response.Write("</font>");
                        //HttpContext.Current.Response.Flush();
                        //HttpContext.Current.Response.End();

                    }
                }

                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่มีข้อมูลค่ะ!');", true);
                }

                break;


            case "cmdUserEditDocument":

                string[] cmdArgUserForUpdate = cmdArg.Split(';');
                int _U0DocUserForUpdate = int.TryParse(cmdArgUserForUpdate[0].ToString(), out _default_int) ? int.Parse(cmdArgUserForUpdate[0].ToString()) : _default_int;
                int _U1DocUserForUpdate = int.TryParse(cmdArgUserForUpdate[1].ToString(), out _default_int) ? int.Parse(cmdArgUserForUpdate[1].ToString()) : _default_int;
                int _StaidUserForUpdate = int.TryParse(cmdArgUserForUpdate[2].ToString(), out _default_int) ? int.Parse(cmdArgUserForUpdate[2].ToString()) : _default_int;

                setFormData(fvDetail, FormViewMode.ReadOnly, null);

                //detail sample in user and admin
                data_qa _data_sample_vuser = new data_qa();
                _data_sample_vuser.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _u1_sample_vuser = new qa_lab_u1_qalab();

                _u1_sample_vuser.u1_qalab_idx = _U1DocUserForUpdate;

                _data_sample_vuser.qa_lab_u1_qalab_list[0] = _u1_sample_vuser;

                _data_sample_vuser = callServicePostQA(_urlQaGetSampleInLab, _data_sample_vuser);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                setFormData(fvSampleCode, FormViewMode.ReadOnly, _data_sample_vuser.qa_lab_u1_qalab_list);

                // Bind repeater Test detail view
                Repeater rp_testdetail_lab_view = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");
                //ViewState["test_detail_view"]
                data_qa _data_test_vuser = new data_qa();

                _data_test_vuser.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                bindqa_m0_test_detail _testv_detail_user = new bindqa_m0_test_detail();

                _testv_detail_user.u1_qalab_idx = _U1DocUserForUpdate;//int.Parse(ViewState["test_detail_view"].ToString());

                _data_test_vuser.bind_qa_m0_test_detail_list[0] = _testv_detail_user;
                _data_test_vuser = callServicePostQA(_urlQaGetTestSample, _data_test_vuser);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                setRepeaterData(rp_testdetail_lab_view, _data_test_vuser.bind_qa_m0_test_detail_list);

                //// Bind repeater Test detail
                //Repeater rp_testdetail_forEdit = (Repeater)fvDetail.FindControl("rp_testdetail");

                //data_qa _data_test_v_forEdit = new data_qa();
                //_data_test_v_forEdit.qa_lab_vtestdetail_list = new qa_lab_vtestdetail[1];
                //qa_lab_vtestdetail _testv_detail = new qa_lab_vtestdetail();

                //_testv_detail.u0_qalab_idx = _U0DocUserForUpdate;

                //_data_test_v_forEdit.qa_lab_vtestdetail_list[0] = _testv_detail;
                //_data_test_v_forEdit = callServicePostQA(_urlQaGetTestDetailU0Document, _data_test_v_forEdit);
                //setRepeaterData(rp_testdetail_forEdit, _data_test_v_forEdit.qa_lab_vtestdetail_list);

                setFormData(fvUserShowDetailEditDocument, FormViewMode.ReadOnly, null);

                data_qa _dataqa_sampleEdit = new data_qa();
                _dataqa_sampleEdit.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _selectDetailForEdit = new qa_lab_u1_qalab();
                _selectDetailForEdit.u1_qalab_idx = _U1DocUserForUpdate;
                _dataqa_sampleEdit.qa_lab_u1_qalab_list[0] = _selectDetailForEdit;
                _dataqa_sampleEdit = callServicePostQA(_urlQaGetDetailForUpdateDocument, _dataqa_sampleEdit);
                ViewState["vsDataSampleEdit"] = _dataqa_sampleEdit.qa_lab_u1_qalab_list;
                setFormData(fvUserEditDocument, FormViewMode.Edit, ViewState["vsDataSampleEdit"]);

                var _IDXDateSample1 = (TextBox)fvUserEditDocument.FindControl("tbSample1IDX_Edit");
                var _IDXDateSample2 = (TextBox)fvUserEditDocument.FindControl("tbSample2IDX_Edit");

                TextBox _txt_org_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_org_idx_production_edit");
                TextBox _txt_rdept_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_rdept_idx_production_edit");
                TextBox _txt_rsec_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_rsec_idx_production_edit");

                DropDownList ddlorg_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit");
                DropDownList ddlrdept_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrdept_idx_production_edit");
                DropDownList ddlrsec_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrsec_idx_production_edit");


                var _SelectIDXDateSample1 = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit");
                getDateSampleListEdit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit"), _IDXDateSample1.Text);
                getDateSampleList2Edit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit"), int.Parse(_SelectIDXDateSample1.SelectedValue), _IDXDateSample2.Text);


                getOrganizationListEdit((DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit"), int.Parse(_txt_org_idx_production_edit.Text));
                getDepartmentListEdit(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(_txt_rdept_idx_production_edit.Text));
                getSectionListEdit(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(ddlrdept_idx_production_edit.SelectedValue), int.Parse(_txt_rsec_idx_production_edit.Text));

                break;

            case "cmdUserSaveEdit":

                string[] cmdArgUserUpdate = cmdArg.Split(';');
                int _U0DocIDXUpdate = int.TryParse(cmdArgUserUpdate[0].ToString(), out _default_int) ? int.Parse(cmdArgUserUpdate[0].ToString()) : _default_int;
                int _U1DocIDXUpdate = int.TryParse(cmdArgUserUpdate[1].ToString(), out _default_int) ? int.Parse(cmdArgUserUpdate[1].ToString()) : _default_int;

                TextBox _InputUpdate_MaterailCode = (TextBox)fvUserEditDocument.FindControl("txt_material_code_edit");
                TextBox _InputUpdate_SampleName = (TextBox)fvUserEditDocument.FindControl("txt_samplename_edit");
                TextBox _InputUpdate_DateSample1 = (TextBox)fvUserEditDocument.FindControl("txt_data_sample1_edit");
                TextBox _InputUpdate_DateSample2 = (TextBox)fvUserEditDocument.FindControl("txt_data_sample2_edit");
                TextBox _InputUpdate_Batch = (TextBox)fvUserEditDocument.FindControl("txt_batch_edit");
                TextBox _InputUpdate_CustomerName = (TextBox)fvUserEditDocument.FindControl("txt_customer_name_edit");
                TextBox _InputUpdate_Cabinet = (TextBox)fvUserEditDocument.FindControl("txt_cabinet_edit");
                TextBox _InputUpdate_ShiftTime = (TextBox)fvUserEditDocument.FindControl("txt_shift_time_edit");
                TextBox _InputUpdate_Time = (TextBox)fvUserEditDocument.FindControl("txt_time_edit");
                TextBox _InputUpdate_TypeSample = (TextBox)fvUserEditDocument.FindControl("txt_typesample_edit");
                HiddenField HiddenTimeEdit = (HiddenField)fvUserEditDocument.FindControl("HiddenTimeEdit");



                TextBox _InputUpdate_OtherDetails = (TextBox)fvUserEditDocument.FindControl("txt_other_details_edit");
                DropDownList _Selected_DateSampleIDX1 = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit");
                DropDownList _Selected_DateSampleIDX2 = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit");

                DropDownList _ddlorg_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit");
                DropDownList _ddlrdept_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrdept_idx_production_edit");
                DropDownList _ddlrsec_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrsec_idx_production_edit");




                //  RadioButtonList _CheckedUpdate_Certificate = (RadioButtonList)fvUserEditDocument.FindControl("rd_certificate_edit");

                data_qa _dataqa_update = new data_qa();
                _dataqa_update.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _UpdateDocument = new qa_lab_u1_qalab();

                _UpdateDocument.cemp_idx = _emp_idx;
                _UpdateDocument.u0_qalab_idx = _U0DocIDXUpdate;
                _UpdateDocument.u1_qalab_idx = _U1DocIDXUpdate;
                _UpdateDocument.sample_name = _InputUpdate_SampleName.Text;
                _UpdateDocument.material_code = _InputUpdate_MaterailCode.Text;
                _UpdateDocument.data_sample1 = _InputUpdate_DateSample1.Text;
                _UpdateDocument.data_sample2 = _InputUpdate_DateSample2.Text;
                _UpdateDocument.batch = _InputUpdate_Batch.Text;
                _UpdateDocument.customer_name = _InputUpdate_CustomerName.Text;
                _UpdateDocument.cabinet = _InputUpdate_Cabinet.Text;
                _UpdateDocument.shift_time = _InputUpdate_ShiftTime.Text;

                _UpdateDocument.time = _InputUpdate_Time.Text;//HiddenTimeEdit.Value.ToString();

                _UpdateDocument.other_details = _InputUpdate_OtherDetails.Text;
                _UpdateDocument.date_sample1_idx = int.Parse(_Selected_DateSampleIDX1.SelectedValue);
                _UpdateDocument.date_sample2_idx = int.Parse(_Selected_DateSampleIDX2.SelectedValue);
                _UpdateDocument.materail_type = _InputUpdate_TypeSample.Text;
                _UpdateDocument.org_idx_production = int.Parse(_ddlorg_idx_production_edit.SelectedValue);
                _UpdateDocument.rdept_idx_production = int.Parse(_ddlrdept_idx_production_edit.SelectedValue);
                _UpdateDocument.rsec_idx_production = int.Parse(_ddlrsec_idx_production_edit.SelectedValue);

                _dataqa_update.qa_lab_u1_qalab_list[0] = _UpdateDocument;

                _dataqa_update = callServicePostQA(_urlQaSetDetailForUpdateDocument, _dataqa_update);

                setActiveTab("docCreate", _U0DocIDXUpdate, 0, 0, "0");

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqa_update));

                break;

            case "cmdCancelDocUserEdit":
                TextBox _U1QaLabIDX = (TextBox)fvUserEditDocument.FindControl("txtUpdate_U1QaLabIDX");
                TextBox _U0QaLabIDX = (TextBox)fvUserEditDocument.FindControl("txtUpdate_U0QaLabIDX");
                //TextBox _StaidxQaLabIDX = (TextBox)fvDetail.FindControl("tbstaidx");
                setActiveTab("docCreate", int.Parse(_U0QaLabIDX.Text), 0, 0, "0");
                break;

            case "cmdSaveEditDocument":

                int cmdArgDecisionEditDoc = int.Parse(cmdArg);
                TextBox _InputUpdateU0Doc = (TextBox)fvDetail.FindControl("tbU0IDX");

                GridView gvDocumentEditList = (GridView)fvUserShowDetailEditDocument.FindControl("gvDocumentEditList");
                int countU1docEdit = 0;
                string U1docEditIDX = "";
                foreach (GridViewRow CountRowU1Doc in gvDocumentEditList.Rows)
                {
                    var _u1docidxEdit = (Label)CountRowU1Doc.FindControl("lblDocIDX_Sample_edit");

                    U1docEditIDX += _u1docidxEdit.Text + ",";

                    countU1docEdit++;


                }

                data_qa _dataqa_update_u0doc = new data_qa();
                _dataqa_update_u0doc.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _UpdateDocumentU0 = new qa_lab_u1_qalab();

                _UpdateDocumentU0.cemp_idx = _emp_idx;
                _UpdateDocumentU0.decision = cmdArgDecisionEditDoc;
                _UpdateDocumentU0.u0_qalab_idx = int.Parse(_InputUpdateU0Doc.Text);
                _UpdateDocumentU0.str_u1edit = U1docEditIDX.ToString();

                _dataqa_update_u0doc.qa_lab_u1_qalab_list[0] = _UpdateDocumentU0;
                // urlQaSetUpdateDocument
                _dataqa_update_u0doc = callServicePostQA(_urlQaSetUpdateDocument, _dataqa_update_u0doc);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataqa_update_u0doc));
                setActiveTab("docDetail", 0, 0, 0, "0");
                setOntop.Focus();
                break;

            case "cmdCancelEditDocument":
                setActiveTab("docDetail", 0, 0, 0, "0");
                setOntop.Focus();
                break;

            case "cmdPlaceLab":

                int cmdArgPlaceLab = int.Parse(cmdArg);
                ViewState["vsArgPlaceLab"] = cmdArgPlaceLab;
                data_qa _dataSelectPlace = new data_qa();
                _dataSelectPlace.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _u1_doc_lab_place = new qa_lab_u1_qalab();
                _u1_doc_lab_place.m0_lab_idx = cmdArgPlaceLab;
                _dataSelectPlace.qa_lab_u1_qalab_list[0] = _u1_doc_lab_place;

                _dataSelectPlace = callServicePostQA(_urlQaGetSelectPlaceLab, _dataSelectPlace);

                if (_dataSelectPlace.return_code == 0)
                {

                    lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString();
                    //if (_dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString() != null)
                    //{
                    lbdetailsShow.Visible = true;

                    ViewState["vs_gvLab_Detail"] = _dataSelectPlace.qa_lab_u1_qalab_list;
                    //}
                    //else
                    //{
                    //    ViewState["vs_gvLab_Detail"] = null;
                    //    //  lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString();
                    //}
                }
                else
                {

                    //litDebug.Text = "2222";
                    lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.return_msg.ToString();
                    lbdetailsShow.Visible = true;
                    ViewState["vs_gvLab_Detail"] = null;
                }


                show_search_lab.Visible = true;
                gvLab.Visible = true;
                gvLab.PageIndex = 0;

                setGridData(gvLab, ViewState["vs_gvLab_Detail"]);

                break;

            case "cmdPlaceLabSup":

                string[] arg6 = new string[2];
                arg6 = e.CommandArgument.ToString().Split(';');
                int cmdArgPlaceLabSup = int.Parse(arg6[0]);
                string cmdArgPlaceLabSupName = arg6[1];



                //int cmdArgPlaceLabSup = int.Parse(cmdArg);
                ViewState["vsArgPlaceLabSup"] = cmdArgPlaceLabSup;
                tbValuePlaceSup.Text = ViewState["vsArgPlaceLabSup"].ToString();
                tbValuePlaceNameSup.Text = cmdArgPlaceLabSupName;

                DataTableSupervisor(cmdArgPlaceLabSup);

                if (cmdArgPlaceLabSup != 0)
                {
                    lbl_showlabsup.Visible = true;
                    lbl_showlabsup.Text = cmdArgPlaceLabSupName;
                }
                else
                {
                    lbl_showlabsup.Visible = false;

                }


                GvSupervisor.DataSource = ViewState["vsTable"];
                GvSupervisor.DataBind();



                //data_qa _dataSelectPlaceSup = new data_qa();
                //_dataSelectPlaceSup.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                //qa_lab_u1_qalab _u1_doc_lab_placesup = new qa_lab_u1_qalab();
                //_u1_doc_lab_placesup.m0_lab_idx = cmdArgPlaceLabSup;
                //_dataSelectPlaceSup.qa_lab_u1_qalab_list[0] = _u1_doc_lab_placesup;

                //_dataSelectPlaceSup = callServicePostQA(_urlQaGetSelectPlaceLab, _dataSelectPlaceSup);

                //if (_dataSelectPlaceSup.return_code == 0)
                //{

                //    lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString();
                //    //if (_dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString() != null)
                //    //{
                //    lbdetailsShow.Visible = true;

                //    ViewState["vs_gvLab_Detail"] = _dataSelectPlace.qa_lab_u1_qalab_list;
                //    //}
                //    //else
                //    //{
                //    //    ViewState["vs_gvLab_Detail"] = null;
                //    //    //  lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.qa_lab_u1_qalab_list[0].lab_name.ToString();
                //    //}
                //}
                //else
                //{

                //    //litDebug.Text = "2222";
                //    lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataSelectPlace.return_msg.ToString();
                //    lbdetailsShow.Visible = true;
                //    ViewState["vs_gvLab_Detail"] = null;
                //}


                //show_search_lab.Visible = true;
                //gvLab.Visible = true;
                //gvLab.PageIndex = 0;

                //setGridData(gvLab, ViewState["vs_gvLab_Detail"]);

                break;


            case "cmdSearchDatePrint":

                data_qa _dataqaSearch = new data_qa();
                _dataqaSearch.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _searchDatePrint = new qa_lab_u1_qalab();
                _searchDatePrint.received_date = HiddenSearchDateRecive.Value.ToString();
                _searchDatePrint.m0_lab_idx = int.Parse(ddlSearchPlace.SelectedValue);
                // _searchDatePrint.test_date = HiddenSearchDateTested.Value.ToString();
                _dataqaSearch.qa_lab_u1_qalab_list[0] = _searchDatePrint;

                _dataqaSearch = callServicePostQA(_urlQaGetSearchDateForPrint, _dataqaSearch);

                if (_dataqaSearch.qa_lab_u1_qalab_list != null)
                {
                    // ViewState["vsSampleTestedItems"]
                    setGridData(gvSampleCodeSearchPrint, _dataqaSearch.qa_lab_u1_qalab_list);
                    //setRepeaterData(rp_tested_sample, ViewState["vsSampleTestedItems"]);
                    divShowToolPrint.Visible = true;
                    divAlertSearchnoneDate.Visible = false;
                    ViewState["vsPlaceIDX"] = _dataqaSearch.qa_lab_u1_qalab_list[0].place_idx.ToString();


                }
                else
                {
                    divShowToolPrint.Visible = false;
                    divAlertSearchnoneDate.Visible = true;
                }

                break;

            case "cmdSearchDateReset":

                setGridData(gvSampleCodeSearchPrint, null);
                divShowToolPrint.Visible = false;
                divAlertSearchnoneDate.Visible = false;
                tbSearchDateRecive.Text = string.Empty;
                HiddenSearchDateRecive.Value = string.Empty;
                ddlSearchPlace.SelectedValue = "0";

                break;

            case "printSampleCodeSearchDate":

                int _rowCountGridSearch = 0;
                foreach (GridViewRow rowCountGide in gvSampleCodeSearchPrint.Rows)
                {
                    Label lblSampleCode_search_print = (Label)rowCountGide.FindControl("lblSampleCode_search_print");
                    Label lblDocIDX_Sample_search_print = (Label)rowCountGide.FindControl("lblDocIDX_Sample_search_print");

                    string txtGenQrCode = "http://mas.taokaenoi.co.th/labis-lab-details/" + _funcTool.getEncryptRC4(lblDocIDX_Sample_search_print.Text, _keysamplecode);
                    string getPathimages = ConfigurationManager.AppSettings["path_qr_code_qa"];
                    string fileNameimage = lblSampleCode_search_print.Text; // ชื่อรูป QRCode
                    string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

                    Session["SESSIONU1DOC"] += lblDocIDX_Sample_search_print.Text + ",";
                    Session["SESSIONSAMPLECODE"] += lblSampleCode_search_print.Text + ",";

                    QRCodeEncoder encoder = new QRCodeEncoder();
                    Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

                    bi.Save(Server.MapPath(getPathimages + ViewState["vsPlaceIDX"].ToString() + fileNameimage + ".jpg"), ImageFormat.Jpeg);

                    _rowCountGridSearch++;

                    if (txtGenQrCode != null)
                    {

                    }
                    else
                    {

                    }

                    Session["SESSION_PlacePrint"] = ViewState["vsPlaceIDX"].ToString();

                    if (ChkPrintQrCodeSearch.Checked)
                    {
                        Session["SESSION_PrintShowQrCode"] = 1;
                    }
                    else if (!ChkPrintQrCodeSearch.Checked)
                    {
                        Session["SESSION_PrintShowQrCode"] = 0;
                    }
                    if (ChkPrintTestedItemsSearch.Checked)
                    {
                        Session["SESSION_PrintShowTestItems"] = 1;
                    }
                    else if (!ChkPrintTestedItemsSearch.Checked)
                    {
                        Session["SESSION_PrintShowTestItems"] = 0;
                    }

                }

                //litDebug.Text = test.ToString();

                Session["SESSIONRECIVEDATE"] = HiddenSearchDateRecive.Value.ToString();
                Session["SESSION_M0LAB"] = ddlSearchPlace.SelectedValue;

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('labis-print-recive-date', '', '');", true);
                break;

            case "CmdExportSearchDate":

                data_qa _dataExportSearch = new data_qa();
                _dataExportSearch.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                qa_lab_u1_qalab _export_result_search = new qa_lab_u1_qalab();

                string _u1doclist = "";
                foreach (GridViewRow rowCountGide in gvSampleCodeSearchPrint.Rows)
                {
                    Label lblSampleCode_search_print = (Label)rowCountGide.FindControl("lblSampleCode_search_print");
                    Label lblDocIDX_Sample_search_print = (Label)rowCountGide.FindControl("lblDocIDX_Sample_search_print");

                    _u1doclist += lblDocIDX_Sample_search_print.Text + ",";
                }

                _export_result_search.u1_export_excel_string = _u1doclist;
                // _export_result_search.m0_lab_idx = int.Parse(ddlSearchPlace.SelectedValue);
                _dataExportSearch.qa_lab_u1_qalab_list[0] = _export_result_search;

                _dataExportSearch = callServicePostQA(_urlQaGetSearchDataExportExcel, _dataExportSearch);

                ViewState["data_result6"] = _dataExportSearch.qa_lab_u1_qalab_list;

                if (_dataExportSearch.return_code == 0)
                {

                    data_qa _dataExport_header = new data_qa();
                    _dataExport_header.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _export_header = new qa_lab_u1_qalab();
                    _export_header.condition = 0;
                    _dataExport_header.qa_lab_u1_qalab_list[0] = _export_header;
                    _dataExport_header = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_header);

                    ViewState["vslistLabExportExcelSearch"] = _dataExport_header.qa_lab_u1_qalab_list;

                    string _header_text_export = String.Empty;
                    int _header_idx_export;
                    string _setHeader_idx_export = "";
                    int _countrowExport = 2;
                    DataTable table = new DataTable();
                    // table.Columns.Add("Document Code", typeof(String));
                    table.Columns.Add("วันทดสอบ", typeof(String));
                    table.Columns.Add("Sample Code", typeof(String));
                    table.Columns.Add("Mat", typeof(String));
                    table.Columns.Add("Sample Name", typeof(String));
                    table.Columns.Add("ประเภทตัวอย่าง", typeof(String));
                    table.Columns.Add("MFD/EXP", typeof(String));
                    table.Columns.Add("Batch", typeof(String));
                    table.Columns.Add("ชื่อลุกค้า", typeof(String));
                    table.Columns.Add("เลขตู้", typeof(String));
                    table.Columns.Add("กะ", typeof(String));
                    table.Columns.Add("เวลา", typeof(String));
                    table.Columns.Add("รายละเอียดอื่นๆ", typeof(String));
                    table.Columns.Add("องค์กรสายการผลิต", typeof(String));
                    table.Columns.Add("ฝ่ายสายการผลิต", typeof(String));
                    table.Columns.Add("แผนกสายการผลิต", typeof(String));
                    table.Columns.Add("วันที่รับตัวอย่าง", typeof(String));

                    string resultString1 = "";
                    foreach (var _loop_header_export_search in _dataExport_header.qa_lab_u1_qalab_list)
                    {
                        ViewState["_header_idx_export_row1"] = int.Parse(_loop_header_export_search.test_detail_idx.ToString());
                        _header_idx_export = int.Parse(_loop_header_export_search.test_detail_idx.ToString());
                        _header_text_export = _loop_header_export_search.test_detail_name.ToString();
                        table.Columns.Add(_header_text_export, typeof(String));
                        _setHeader_idx_export += "," + _header_idx_export;
                        _countrowExport++;
                        //  ViewState["rowColumnExportExcel"] = _countrowExport;
                        resultString1 = _setHeader_idx_export.IndexOf(',') > -1
                        ? _setHeader_idx_export.Substring(_setHeader_idx_export.IndexOf(',') + 1)
                        : _setHeader_idx_export;


                    }
                    //     table.Rows.Add(88888, 80000, 7777, 001);
                    data_qa _dataExport_documentNo = new data_qa();
                    _dataExport_documentNo.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    qa_lab_u1_qalab _export_document = new qa_lab_u1_qalab();
                    _export_document.u1_export_excel_string = _u1doclist;
                    _export_document.condition = 1;
                    _dataExport_documentNo.qa_lab_u1_qalab_list[0] = _export_document;
                    _dataExport_documentNo = callServicePostQA(_urlQaGetSearchDataExportExcel, _dataExport_documentNo);

                    ViewState["vslistLabExportExcelSearch"] = _dataExport_documentNo.qa_lab_u1_qalab_list;
                    //  table.Columns.Add("Approve", typeof(String));
                    // table.Rows.Add(88888, 80000, 7777, 001);
                    string _text = String.Empty;
                    int _countrow1 = 1;
                    // table.Rows.Add(88888, 80000, 7777, 001);
                    if (_dataExport_documentNo.qa_lab_u1_qalab_list == null)
                    {
                        //  GvShowExportExcel.Visible = false;
                        //  litDebug.Text = "11111";
                    }
                    else
                    {

                        foreach (var _loop_rowText in _dataExport_documentNo.qa_lab_u1_qalab_list)
                        {

                            //gen label
                            //  Label dynamicLabelExport_Document = new Label();
                            Label dynamicLabelExport_Testdate = new Label();
                            Label dynamicLabelExport_SampleCode = new Label();
                            Label dynamicLabelExport_Mat = new Label();
                            Label dynamicLabelExport_SampleName = new Label();
                            Label dynamicLabelExport_materail_type = new Label();
                            Label dynamicLabelExport_EXP = new Label();
                            Label dynamicLabelExport_Batch = new Label();
                            Label dynamicLabelExport_CustomerName = new Label();
                            Label dynamicLabelExport_Cabinet = new Label();
                            Label dynamicLabelExport_Shifttime = new Label();
                            Label dynamicLabelExport_Time = new Label();
                            Label dynamicLabelExport_Other = new Label();
                            Label dynamicLabelExport_Orgname = new Label();
                            Label dynamicLabelExport_Deptname = new Label();
                            Label dynamicLabelExport_Secname = new Label();
                            Label dynamicLabelExport_ReceiveDate = new Label();

                            // dynamicLabelExport_Document.Text = _loop_rowText.document_code.ToString();
                            dynamicLabelExport_Testdate.Text = _loop_rowText.test_date.ToString();
                            dynamicLabelExport_SampleCode.Text = _loop_rowText.sample_code.ToString();
                            dynamicLabelExport_Mat.Text = _loop_rowText.material_code.ToString();
                            dynamicLabelExport_SampleName.Text = _loop_rowText.sample_name.ToString();
                            dynamicLabelExport_materail_type.Text = _loop_rowText.materail_type.ToString();
                            dynamicLabelExport_EXP.Text = _loop_rowText.EXP_MFD.ToString();
                            dynamicLabelExport_Batch.Text = _loop_rowText.batch.ToString();
                            dynamicLabelExport_CustomerName.Text = _loop_rowText.customer_name.ToString();
                            dynamicLabelExport_Cabinet.Text = _loop_rowText.cabinet.ToString();
                            dynamicLabelExport_Shifttime.Text = _loop_rowText.shift_time.ToString();
                            dynamicLabelExport_Time.Text = _loop_rowText.time.ToString();
                            dynamicLabelExport_Other.Text = _loop_rowText.other_details.ToString();
                            dynamicLabelExport_Orgname.Text = _loop_rowText.org_name_th.ToString();
                            dynamicLabelExport_Deptname.Text = _loop_rowText.dept_name_th.ToString();
                            dynamicLabelExport_Secname.Text = _loop_rowText.sec_name_th.ToString();
                            dynamicLabelExport_ReceiveDate.Text = _loop_rowText.received_sample_date.ToString();

                            _countrow1++;
                            DataRow newRow1 = table.NewRow();
                            //newRow[0] = "0";
                            // newRow1[0] = dynamicLabelExport_Document.Text;
                            newRow1[0] = dynamicLabelExport_Testdate.Text;
                            newRow1[1] = dynamicLabelExport_SampleCode.Text;
                            newRow1[2] = dynamicLabelExport_Mat.Text;
                            newRow1[3] = dynamicLabelExport_SampleName.Text;
                            newRow1[4] = dynamicLabelExport_materail_type.Text;
                            newRow1[5] = dynamicLabelExport_EXP.Text;
                            newRow1[6] = dynamicLabelExport_Batch.Text;
                            newRow1[7] = dynamicLabelExport_CustomerName.Text;
                            newRow1[8] = dynamicLabelExport_Cabinet.Text;
                            newRow1[9] = dynamicLabelExport_Shifttime.Text;
                            newRow1[10] = dynamicLabelExport_Time.Text;
                            newRow1[11] = dynamicLabelExport_Other.Text;
                            newRow1[12] = dynamicLabelExport_Orgname.Text;
                            newRow1[13] = dynamicLabelExport_Deptname.Text;
                            newRow1[14] = dynamicLabelExport_Secname.Text;
                            newRow1[15] = dynamicLabelExport_ReceiveDate.Text;

                            int t3 = 0;
                            for (int t0 = 16; t0 < table.Columns.Count; t0++)
                            {
                                newRow1[t0] = _function1InputResultToColumn(_u1doclist, _loop_rowText.sample_code.ToString(), _dataExport_header.qa_lab_u1_qalab_list[t3].test_detail_idx.ToString());

                                t3++;
                            }


                            table.Rows.InsertAt(newRow1, 0);

                        }
                    }
                    //gvExportSearchDate.Visible = true;
                    ViewState["vsTableExportExcelSearch"] = table;
                    gvExportSearchDate.DataSource = table;
                    gvExportSearchDate.DataBind();

                    WriteExcelWithNPOI(table, "xls", "Report");

                    ////string strBody = string.Empty;
                    ////strBody = @"<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
                    ////"xmlns:w='urn:schemas-microsoft-com:office:word'" +
                    ////"xmlns='http://www.w3.org/TR/REC-html40'>";

                    ////strBody = strBody + "<!--[if gte mso 9]>" +
                    ////"<xml>" +
                    ////"<w:WordDocument>" +
                    ////"<w:View>Print</w:View>" +
                    ////"<w:Zoom>100</w:Zoom>" +
                    ////"</w:WordDocument>" +
                    ////"</xml>" +
                    ////"<![endif]-->";
                    ////HttpContext.Current.Response.Clear();
                    ////HttpContext.Current.Response.Charset = "";
                    ////HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    ////HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=Report.xls");

                    ////HttpContext.Current.Response.Write("<meta http-equiv='ContentType' content='text/html; charset=utf-8'>");

                    ////StringBuilder htmlCode = new StringBuilder();
                    ////htmlCode.Append("<html>");
                    ////htmlCode.Append("<head>" + strBody + " <style type=\"text/css\">body {font-family:arial;font-size:14.5;}</style></head>");
                    ////htmlCode.Append("<body>");

                    ////HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
                    //// "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
                    //// "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
                    //////am getting my grid's column headers
                    ////int columnscount = GvShowExportExcel.Columns.Count;

                    //////  HttpContext.Current.Response.Write("<TR>");
                    ////// On all tables' columns
                    ////foreach (DataColumn dc in table.Columns)
                    ////{
                    ////    //var field1 = dtRow[dc].ToString();
                    ////    HttpContext.Current.Response.Write("<Td>");
                    ////    HttpContext.Current.Response.Write(dc.ToString());
                    ////    HttpContext.Current.Response.Write("</Td>");
                    ////}
                    ////HttpContext.Current.Response.Write("</TR>");

                    //////   HttpContext.Current.Response.Write("</TR>");
                    ////foreach (DataRow row in table.Rows)
                    ////{//write in new row
                    ////    HttpContext.Current.Response.Write("<TR>");
                    ////    for (int i = 0; i < table.Columns.Count; i++)
                    ////    {
                    ////        if (i == 0)
                    ////        {
                    ////            HttpContext.Current.Response.Write("<Td>");
                    ////            HttpContext.Current.Response.Write(row[i].ToString());
                    ////            HttpContext.Current.Response.Write("</Td>");
                    ////        }
                    ////        else
                    ////        {

                    ////            HttpContext.Current.Response.Write("<Td>");
                    ////            HttpContext.Current.Response.Write(row[i].ToString());
                    ////            HttpContext.Current.Response.Write("</Td>");
                    ////        }

                    ////    }

                    ////    HttpContext.Current.Response.Write("</TR>");
                    ////}

                    ////htmlCode.Append("</body></html>");
                    ////HttpContext.Current.Response.Write(htmlCode.ToString());
                    ////HttpContext.Current.Response.End();
                    ////HttpContext.Current.Response.Flush();
                    ////HttpContext.Current.Response.Write("</TR>");

                }     
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่มีข้อมูลค่ะ!');", true);
                }

                break;

            case "cmdDocumentPlaceLab":

                string[] arg3 = new string[2];
                arg3 = e.CommandArgument.ToString().Split(';');
                int SETSHOWDOCUMENTPLACE = int.Parse(arg3[0]);
                string SET_PlaceName = arg3[1];

                //int SETSHOWDOCUMENTPLACE = int.Parse(cmdArg);

                ViewState["DocumentPlaceLab"] = SETSHOWDOCUMENTPLACE;
                tbValuePlace.Text = (SETSHOWDOCUMENTPLACE.ToString());
                tb_PlaceName.Text = SET_PlaceName;

                data_qa _data_qaindex = new data_qa();
                _data_qaindex.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                qa_lab_u0doc_qalab _u0_doc_index = new qa_lab_u0doc_qalab();
                _u0_doc_index.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                _u0_doc_index.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                _u0_doc_index.cemp_idx = _emp_idx;
                _u0_doc_index.place_idx = int.Parse(tbValuePlace.Text);

                _data_qaindex.qa_lab_u0doc_qalab_list[0] = _u0_doc_index;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qaindex));
                _data_qaindex = callServicePostQA(_urlQaGetViewRsecReference, _data_qaindex);


                if (_data_qaindex.return_code == 0)
                {
                    //litDebug.Text = "99999";
                    lbl_detail_placeindex.Visible = true;
                    var linqDocumentPlace = from _dtdocumentplace in _data_qaindex.qa_lab_u0doc_qalab_list
                                            where _dtdocumentplace.place_idx == int.Parse(tbValuePlace.Text)
                                            select _dtdocumentplace;

                    ViewState["vs_gvDetail"] = linqDocumentPlace.ToList();



                    lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    //var t1 = linqDocumentPlace.ToArray();

                    //foreach (var c in linqDocumentPlace)
                    //{

                    //    if(c.place_name != null || c.place_name != "")
                    //    {
                    //        litDebug.Text = "10211111";
                    //        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + c.place_name;
                    //    }
                    //    else
                    //    {
                    //        litDebug.Text = "4444";
                    //        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    //    }



                    //}


                }
                else
                {
                    //litDebug.Text = "8888";
                    ViewState["vs_gvDetail"] = null;
                    if (tb_PlaceName.Text != "")
                    {
                        //litDebug.Text = "33333";
                        lbl_detail_placeindex.Visible = true;
                        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                    }
                    else
                    {
                        //litDebug.Text = "22222";
                        lbl_detail_placeindex.Visible = false;
                        lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + "";
                    }

                }

                setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                break;

            case "cmdResetSearchPage":
                lbl_detail_placeindex.Visible = false;
                data_qa _data_qaindexReset = new data_qa();
                _data_qaindexReset.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                qa_lab_u0doc_qalab _u0_doc_indexReset = new qa_lab_u0doc_qalab();
                _u0_doc_indexReset.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                _u0_doc_indexReset.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                _u0_doc_indexReset.cemp_idx = _emp_idx;

                _data_qaindexReset.qa_lab_u0doc_qalab_list[0] = _u0_doc_indexReset;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qaindex));
                _data_qaindexReset = callServicePostQA(_urlQaGetViewRsecReference, _data_qaindexReset);

                ViewState["vs_gvDetail"] = _data_qaindexReset.qa_lab_u0doc_qalab_list;
                setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                tbValuePlace.Text = "0";
                tb_PlaceName.Text = "";

                break;

        }
    }
    //endbtn
    protected data_qa setQAU0DocumentList(data_qa _data_qa)
    {
        _data_qa = callServicePostQA(_urlQaSetDocument, _data_qa);
        return _data_qa;
    }

    #endregion event command

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvDoingList":
                setGridData(gvDoingList, ViewState["vs_gvDetail"]);
                break;
            case "gvLab":
                setGridData(gvLab, ViewState["vs_gvLab_Detail"]);
                setOntop.Focus();
                break;
                //case "GvSupervisor":

                //    setGridData(GvSupervisor, ViewState["vsTable"]);


                //    GvSupervisor.PageIndex = e.NewPageIndex;
                //    GvSupervisor.DataBind();



                //break;
        }
    }
    #endregion paging

    #region RadioButton
    protected void getTestDetail(RadioButton rd)
    {
        Repeater rp_TestDetail = (Repeater)fvActor1Node1.FindControl("rp_TestDetail");

        data_qa _data_qa_testdetail = new data_qa();
        _data_qa_testdetail.qa_m0_test_detail_list = new qa_m0_test_detail[1];
        qa_m0_test_detail _testdetail = new qa_m0_test_detail();
        _testdetail.condition = 1;//int.Parse(rd.ToString());

        _data_qa_testdetail.qa_m0_test_detail_list[0] = _testdetail;

        _data_qa_testdetail = callServicePostMasterQA(_urlQaGetTestDetail, _data_qa_testdetail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_testdetail));

        setRepeaterData(rp_TestDetail, _data_qa_testdetail.qa_m0_test_detail_list);


    }

    protected void RadioButton_CheckedChanged(object sender, System.EventArgs e)
    {
        var rd = (RadioButton)sender;

        setFormData(fvDetailSearchCreate, FormViewMode.ReadOnly, null);
        fvDetailMat.Visible = true;
        //div_saveinsert.Visible = true;

        switch (rd.ID)
        {
            case "rd_other_detail":
                if (rd_other_detail.Checked)
                {

                    setFormData(fvDetailMat, FormViewMode.Insert, null);
                    DropDownList ddlOrg_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate");
                    DropDownList ddlDept_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlDept_samplecreate");
                    DropDownList ddlSec_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlSec_samplecreate");

                    getDateSampleList((DropDownList)fvDetailMat.FindControl("ddldate_sample1_idx_create"), "0");
                    getDateSampleList((DropDownList)fvDetailMat.FindControl("ddldate_sample2_idx_create"), "0");


                    getOrganizationList((DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate"));
                    getDepartmentList(ddlDept_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedValue));
                    getSectionList(ddlSec_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedValue), int.Parse(ddlDept_samplecreate.SelectedValue));


                    //fvDeptSelect
                    setFormData(fvDeptSelect, FormViewMode.Insert, ViewState["vsDepartment"]);
                    GridView GvDeptCreate = (GridView)fvDeptSelect.FindControl("GvDeptCreate");
                    DropDownList ddlOrg_create = (DropDownList)fvDeptSelect.FindControl("ddlOrg_create");
                    DropDownList ddlDept_create = (DropDownList)fvDeptSelect.FindControl("ddlDept_create");
                    DropDownList ddlSec_create = (DropDownList)fvDeptSelect.FindControl("ddlSec_create");
                    //litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                    getOrganizationList((DropDownList)fvDeptSelect.FindControl("ddlOrg_create"));
                    getDepartmentList(ddlDept_create, int.Parse(ddlOrg_create.SelectedValue));
                    getSectionList(ddlSec_create, int.Parse(ddlOrg_create.SelectedValue), int.Parse(ddlDept_create.SelectedValue));

                    setGridData(GvDeptCreate, (DataSet)ViewState["vsDepartment"]);

                    div_other_detail.Visible = true;
                    fvDetailSearchCreate.Visible = true;
                    //div_saveinsert.Visible = true;

                }
                else
                {
                    setFormData(fvDetailSearchCreate, FormViewMode.ReadOnly, null);
                    div_other_detail.Visible = false;
                    //div_saveinsert.Visible = false;
                }
                break;
            case "rd_testdetail": //test detail

                //fvActor1Node1
                RadioButton rd_testdetail = (RadioButton)fvActor1Node1.FindControl("rd_testdetail");

                if (rd_testdetail.Checked)
                {

                    data_qa _data_qa_testdetail = new data_qa();

                    _data_qa_testdetail.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                    qa_m0_test_detail _testdetail = new qa_m0_test_detail();
                    _testdetail.condition = 1;//int.Parse(rd.ToString());

                    _data_qa_testdetail.qa_m0_test_detail_list[0] = _testdetail;

                    _data_qa_testdetail = callServicePostMasterQA(_urlQaGetTestDetail, _data_qa_testdetail);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_testdetail));

                    ViewState["data_test_sample"] = _data_qa_testdetail.qa_m0_test_detail_list;

                    CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
                    chk_TestDetail.Items.Clear();
                    chk_TestDetail.AppendDataBoundItems = true;
                    chk_TestDetail.DataSource = ViewState["data_test_sample"];
                    chk_TestDetail.DataTextField = "test_detail_name";
                    chk_TestDetail.DataValueField = "test_detail_idx";
                    chk_TestDetail.DataBind();

                    ViewState["data_test_sample"] = chk_TestDetail.SelectedValue;



                }
                else
                {

                }

                break;

        }
    }

    #endregion RadioButton

    #region TextBoxIndexChanged

    protected void getMaterialEditList(TextBox txtNameEdit, string _CheckMatEdit)
    {

        string noEdit = txtNameEdit.Text;
        string[] numberArrayEdit = new string[noEdit.Length];
        int counterEdit = 0;

        for (int ic = 0; ic < noEdit.Length; ic++)
        {
            numberArrayEdit[ic] = noEdit.Substring(counterEdit, 1); // 1 is split length


            var itemAsString = numberArrayEdit[ic].ToString();

            counterEdit++;

            // += numberArray;
        }


        data_qa _data_qa_mat_edit = new data_qa();
        _data_qa_mat_edit.qa_m0_material_list = new qa_m0_material_detail[1];
        qa_m0_material_detail _materialEditList = new qa_m0_material_detail();
        _materialEditList.material_code = txtNameEdit.Text;
        _data_qa_mat_edit.qa_m0_material_list[0] = _materialEditList;
        _materialEditList.type_matcode = numberArrayEdit[0].ToString();


        _data_qa_mat_edit = callServicePostMasterQA(_urlQaGetMaterialList, _data_qa_mat_edit);

        setFormData(fvUserEditDocument, FormViewMode.Edit, ViewState["vsDataSampleEdit"]);

        TextBox txt_material_search_edit = (TextBox)fvUserEditDocument.FindControl("txt_material_search_edit");
        TextBox txt_material_code_edit = (TextBox)fvUserEditDocument.FindControl("txt_material_code_edit");
        TextBox txt_samplename_edit = (TextBox)fvUserEditDocument.FindControl("txt_samplename_edit");

        TextBox txt_data_sample1_edit = (TextBox)fvUserEditDocument.FindControl("txt_data_sample1_edit");
        TextBox txt_data_sample2_edit = (TextBox)fvUserEditDocument.FindControl("txt_data_sample2_edit");
        TextBox txt_batch_edit = (TextBox)fvUserEditDocument.FindControl("txt_batch_edit");
        TextBox txt_customer_name_edit = (TextBox)fvUserEditDocument.FindControl("txt_customer_name_edit");
        TextBox txt_cabinet_edit = (TextBox)fvUserEditDocument.FindControl("txt_cabinet_edit");
        TextBox txt_shift_time_edit = (TextBox)fvUserEditDocument.FindControl("txt_shift_time_edit");
        TextBox txt_time_edit = (TextBox)fvUserEditDocument.FindControl("txt_time_edit");
        TextBox txt_other_details_edit = (TextBox)fvUserEditDocument.FindControl("txt_other_details_edit");
        HiddenField HiddenTimeEdit = (HiddenField)fvUserEditDocument.FindControl("HiddenTimeEdit");

        DropDownList ddldate_sample1_idx_edit = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit");
        DropDownList ddldate_sample2_idx_edit = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit");
        Panel pnUserEditDocAction = (Panel)fvUserEditDocument.FindControl("pnUserEditDocAction");
        RadioButtonList rd_certificate_edit = (RadioButtonList)fvUserEditDocument.FindControl("rd_certificate_edit");

        TextBox _txt_org_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_org_idx_production_edit");
        TextBox _txt_rdept_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_rdept_idx_production_edit");
        TextBox _txt_rsec_idx_production_edit = (TextBox)fvUserEditDocument.FindControl("txt_rsec_idx_production_edit");

        DropDownList ddlorg_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit");
        DropDownList ddlrdept_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrdept_idx_production_edit");
        DropDownList ddlrsec_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrsec_idx_production_edit");

        var _IDXDateSample1_edit = (TextBox)fvUserEditDocument.FindControl("tbSample1IDX_Edit");
        var _IDXDateSample2_edit = (TextBox)fvUserEditDocument.FindControl("tbSample2IDX_Edit");
        var _SelectIDXDateSample1_edit = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit");
        TextBox txt_typesample_edit = (TextBox)fvUserEditDocument.FindControl("txt_typesample_edit");


        if (_data_qa_mat_edit.return_code == 0) // มีตัวอย่าง
        {
            // กรณีเลข Materail ไม่ซ้ำ
            if (_CheckMatEdit == "0")
            {
                if (_data_qa_mat_edit.return_typemat != null)
                {
                    txt_typesample_edit.Text = _data_qa_mat_edit.return_typemat;
                    txt_typesample_edit.Enabled = false;
                }
                else
                {
                    //txt_typesample.Text = _data_qa_mat.return_typemat;
                    txt_typesample_edit.Enabled = true;
                }


                txt_material_search_edit.Text = "";
                txt_material_code_edit.Text = _data_qa_mat_edit.qa_m0_material_list[0].material_code.ToString();
                txt_samplename_edit.Text = _data_qa_mat_edit.qa_m0_material_list[0].material_name.ToString();
                HiddenTimeEdit.Value = txt_time_edit.Text;

                getDateSampleListEdit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit"), _IDXDateSample1_edit.Text);
                getDateSampleList2Edit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit"), int.Parse(_SelectIDXDateSample1_edit.SelectedValue), _IDXDateSample2_edit.Text);

                getOrganizationListEdit((DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit"), int.Parse(_txt_org_idx_production_edit.Text));
                getDepartmentListEdit(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(_txt_rdept_idx_production_edit.Text));
                getSectionListEdit(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(ddlrdept_idx_production_edit.SelectedValue), int.Parse(_txt_rsec_idx_production_edit.Text));

                txt_data_sample1_edit.Enabled = true;
                txt_data_sample2_edit.Enabled = true;
                txt_batch_edit.Enabled = true;
                txt_customer_name_edit.Enabled = true;
                txt_cabinet_edit.Enabled = true;
                txt_shift_time_edit.Enabled = true;
                txt_time_edit.Enabled = true;
                txt_other_details_edit.Enabled = true;
                ddldate_sample1_idx_edit.Enabled = true;
                ddldate_sample2_idx_edit.Enabled = true;
                rd_certificate_edit.Enabled = true;
                pnUserEditDocAction.Visible = true;
            }
            else
            {

                getDateSampleListEdit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit"), _IDXDateSample1_edit.Text);
                getDateSampleList2Edit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit"), int.Parse(_SelectIDXDateSample1_edit.SelectedValue), _IDXDateSample2_edit.Text);

                getOrganizationListEdit((DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit"), int.Parse(_txt_org_idx_production_edit.Text));
                getDepartmentListEdit(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(_txt_rdept_idx_production_edit.Text));
                getSectionListEdit(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(ddlrdept_idx_production_edit.SelectedValue), int.Parse(_txt_rsec_idx_production_edit.Text));
            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบเลข Mat.!');", true);
            // กรณีเลข Materail ไม่ซ้ำ
            if (_CheckMatEdit == "0")
            {

                HiddenTimeEdit.Value = txt_time_edit.Text;
                txt_material_search_edit.Text = "";
                txt_material_code_edit.Text = "";
                txt_samplename_edit.Text = "";
                txt_material_code_edit.Enabled = true;
                txt_typesample_edit.Enabled = true;
                txt_samplename_edit.Enabled = true;
                txt_data_sample1_edit.Enabled = true;
                txt_data_sample2_edit.Enabled = true;
                txt_batch_edit.Enabled = true;
                txt_customer_name_edit.Enabled = true;
                txt_cabinet_edit.Enabled = true;
                txt_shift_time_edit.Enabled = true;
                txt_time_edit.Enabled = true;
                txt_other_details_edit.Enabled = true;
                ddldate_sample1_idx_edit.Enabled = true;
                ddldate_sample2_idx_edit.Enabled = true;
                rd_certificate_edit.Enabled = true;
                pnUserEditDocAction.Visible = true;

                getDateSampleListEdit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit"), _IDXDateSample1_edit.Text);
                getDateSampleList2Edit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit"), int.Parse(_SelectIDXDateSample1_edit.SelectedValue), _IDXDateSample2_edit.Text);

                getOrganizationListEdit((DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit"), int.Parse(_txt_org_idx_production_edit.Text));
                getDepartmentListEdit(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(_txt_rdept_idx_production_edit.Text));
                getSectionListEdit(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(ddlrdept_idx_production_edit.SelectedValue), int.Parse(_txt_rsec_idx_production_edit.Text));
            }
            else
            {
                getDateSampleListEdit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit"), _IDXDateSample1_edit.Text);
                getDateSampleList2Edit((DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit"), int.Parse(_SelectIDXDateSample1_edit.SelectedValue), _IDXDateSample2_edit.Text);

                getOrganizationListEdit((DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit"), int.Parse(_txt_org_idx_production_edit.Text));
                getDepartmentListEdit(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(_txt_rdept_idx_production_edit.Text));
                getSectionListEdit(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedValue), int.Parse(ddlrdept_idx_production_edit.SelectedValue), int.Parse(_txt_rsec_idx_production_edit.Text));
            }
        }
    }

    protected void getMaterialList(TextBox txtName, string CheckMat)
    {

        string no = txtName.Text;
        string[] numberArray = new string[no.Length];
        int counter = 0;

        for (int i = 0; i < no.Length; i++)
        {
            numberArray[i] = no.Substring(counter, 1); // 1 is split length


            var itemAsString = numberArray[i].ToString();

            counter++;

            // += numberArray;
        }

        data_qa _data_qa_mat = new data_qa();
        _data_qa_mat.qa_m0_material_list = new qa_m0_material_detail[1];
        qa_m0_material_detail _materialList = new qa_m0_material_detail();
        _materialList.material_code = txtName.Text;
        _materialList.type_matcode = numberArray[0].ToString();

        //litDebug.Text = numberArray[0].ToString();

        _data_qa_mat.qa_m0_material_list[0] = _materialList;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_mat));

        //litDebug.Text += charArray.ToArray();

        _data_qa_mat = callServicePostMasterQA(_urlQaGetMaterialList, _data_qa_mat);
        if (_data_qa_mat.return_code == 0) // มีตัวอย่าง
        {
            if (CheckMat.ToString() == "0")
            {
                //fvDetailMat
                setFormData(fvDetailMat, FormViewMode.ReadOnly, _data_qa_mat.qa_m0_material_list);

                TextBox txt_typesample = (TextBox)fvDetailMat.FindControl("txt_typesample");

                if (_data_qa_mat.return_typemat != null)
                {
                    txt_typesample.Text = _data_qa_mat.return_typemat;
                    txt_typesample.Enabled = false;
                }
                else
                {
                    //txt_typesample.Text = _data_qa_mat.return_typemat;
                    txt_typesample.Enabled = true;
                }


                DropDownList ddldate_sample1_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample1_idx_create");
                DropDownList ddldate_sample2_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample2_idx_create");

                DropDownList ddlOrg_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate");
                DropDownList ddlDept_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlDept_samplecreate");
                DropDownList ddlSec_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlSec_samplecreate");



                getDateSampleList((ddldate_sample1_idx_create), "0");
                getDateSampleList2(ddldate_sample2_idx_create, int.Parse(ddldate_sample1_idx_create.SelectedValue), "0");

                getOrganizationList((DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate"));
                getDepartmentList(ddlDept_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedValue));
                getSectionList(ddlSec_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedValue), int.Parse(ddlDept_samplecreate.SelectedValue));



                //fvDeptSelect
                setFormData(fvDeptSelect, FormViewMode.Insert, (DataSet)ViewState["vsDepartment"]);

                DropDownList ddlOrg_create = (DropDownList)fvDeptSelect.FindControl("ddlOrg_create");
                DropDownList ddlDept_create = (DropDownList)fvDeptSelect.FindControl("ddlDept_create");
                DropDownList ddlSec_create = (DropDownList)fvDeptSelect.FindControl("ddlSec_create");
                //litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                GridView GvDeptCreate = (GridView)fvDeptSelect.FindControl("GvDeptCreate");
                setGridData(GvDeptCreate, (DataSet)ViewState["vsDepartment"]);

                getOrganizationList((DropDownList)fvDeptSelect.FindControl("ddlOrg_create"));
                getDepartmentList(ddlDept_create, int.Parse(ddlOrg_create.SelectedValue));
                getSectionList(ddlSec_create, int.Parse(ddlOrg_create.SelectedValue), int.Parse(ddlDept_create.SelectedValue));

                RadioButton rd_other_detail = (RadioButton)div_other_detail.FindControl("rd_other_detail");

                div_other_detail.Visible = false;
                rd_other_detail.Checked = false;
                fvDetailSearchCreate.Visible = false;
                //div_saveinsert.Visible = true;
            }
        }
        else // ไม่มีตัวอย่าง
        {
            setFormData(fvDetailSearchCreate, FormViewMode.Insert, null);


            RadioButton rd_other_detail = (RadioButton)div_other_detail.FindControl("rd_other_detail");

            fvDetailMat.Visible = false;

            div_other_detail.Visible = true;
            rd_other_detail.Checked = false;
            fvDetailSearchCreate.Visible = true;
        }

    }

    protected void getSmapleList_AnalyticalResults(TextBox txtName)
    {

        LinkButton lbDocSaveReceiveN5 = (LinkButton)fvAnalyticalResults.FindControl("lbDocSaveReceiveN5");
        linkBtnTrigger(lbDocSaveReceiveN5);

        TextBox txtSearchSampleCode = (TextBox)fvAnalyticalResults.FindControl("txtSearchSampleCode");
        TextBox txtSampleCodeResult = (TextBox)fvAnalyticalResults.FindControl("txtSampleCodeResult");
        DropDownList ddlSearchPlaceSaveResult = (DropDownList)fvAnalyticalResults.FindControl("ddlSearchPlaceSaveResult");
        Panel pnRecordResult = (Panel)fvAnalyticalResults.FindControl("pnRecordResult");
        GridView GvFormRecordResult = (GridView)fvAnalyticalResults.FindControl("GvFormRecordResult");
        Panel pnUploadfileResult = (Panel)fvAnalyticalResults.FindControl("pnUploadfileResult");

        data_qa _data_qa_result = new data_qa();
        _data_qa_result.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        qa_lab_u1_qalab _sampleCode = new qa_lab_u1_qalab();
        _sampleCode.sample_code = txtSearchSampleCode.Text;
        _sampleCode.m0_lab_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);

        _data_qa_result.qa_lab_u1_qalab_list[0] = _sampleCode;

        _data_qa_result = callServicePostQA(_urlQaGetSearchSampleCode, _data_qa_result);

        if (_data_qa_result.return_code == 0) // มี sample code 
        {

            // Bind repeater Test detail for record result
            if (_data_qa_result.bind_qa_lab_form_list[0].sample_code != null)
            { txtSampleCodeResult.Text = _data_qa_result.bind_qa_lab_form_list[0].sample_code.ToString(); }
            //check staidx external lab
            if (_data_qa_result.bind_qa_lab_form_list[0].staidx.ToString() != null)
            {
                int ValueExternalLabINDB = _data_qa_result.bind_qa_lab_form_list[0].staidx;

                if (ValueExternalLabINDB == ExternalLabStatus)
                {
                    pnUploadfileResult.Visible = true;
                }
                else
                {
                    pnUploadfileResult.Visible = false;
                }
            }

            Repeater rpt_testdetail_list = (Repeater)fvAnalyticalResults.FindControl("rpt_testdetail_list");
            setRepeaterData(rpt_testdetail_list, _data_qa_result.bind_qa_lab_form_list);

            // Bind gridview form for record result
            data_qa _data_qa_result1 = new data_qa();
            _data_qa_result1.bind_qa_lab_form_list = new bind_qa_lab_form_detail[1];
            bind_qa_lab_form_detail _sampleCode1 = new bind_qa_lab_form_detail();
            _sampleCode1.sample_code = txtSearchSampleCode.Text;
            _sampleCode1.m0_lab_idx = int.Parse(ddlSearchPlaceSaveResult.SelectedValue);
            _sampleCode1.form_detail_root_idx = 0;

            _data_qa_result1.bind_qa_lab_form_list[0] = _sampleCode1;

            _data_qa_result1 = callServicePostQA(_urlQaGetSelectForm, _data_qa_result1);
            setGridData(GvFormRecordResult, _data_qa_result1.bind_qa_lab_form_list);

            pnRecordResult.Visible = true;
        }

        else
        {
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบ Sample Code. กรุณากรอกใหม่อีกครั้งค่ะ!');", true);
            setFormData(fvAnalyticalResults, FormViewMode.Insert, null);
            getSelectLabForSearch((DropDownList)fvAnalyticalResults.FindControl("ddlSearchPlaceSaveResult"), "0");

            if (_data_qa_result.return_code != 0)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบ Sample Code. กรุณากรอกใหม่อีกครั้งค่ะ!');", true);

            }




        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        fvDetailMat.Visible = true;
        //div_saveinsert.Visible = true;

        // fvActor1Node1
        TextBox txt_material_search = (TextBox)fvActor1Node1.FindControl("txt_material_search");
        TextBox txtSearchSampleCode = (TextBox)fvAnalyticalResults.FindControl("txtSearchSampleCode");
        TextBox txt_material_search_edit = (TextBox)fvUserEditDocument.FindControl("txt_material_search_edit");


        switch (txtName.ID)
        {
            case "txt_material_search":

                if (txt_material_search.Text != "")
                {
                    string searchValue = txt_material_search.Text;



                    var ds_Check_Sample = (DataSet)ViewState["vsSample"];
                    var dr_Check_Sample = ds_Check_Sample.Tables[0].NewRow();

                    int numRowMat = ds_Check_Sample.Tables[0].Rows.Count; //จำนวนแถว

                    ViewState["_materialcode_checkDuplicate"] = txt_material_search.Text;

                    //if (numRowMat > 0)
                    //{
                    //    foreach (DataRow checkMat in ds_Check_Sample.Tables[0].Rows)
                    //    {

                    //        ViewState["checkMaterialcode"] = checkMat["material_code"];

                    //        if (ViewState["_materialcode_checkDuplicate"].ToString() == ViewState["checkMaterialcode"].ToString())
                    //        {
                    //            ViewState["CheckDataset_mat"] = "1";
                    //            break;

                    //        }
                    //        else
                    //        {
                    //            ViewState["CheckDataset_mat"] = "0";

                    //        }
                    //    }
                    //    //if (ViewState["CheckDataset_mat"].ToString() == "1")
                    //    //{
                    //    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณได้ทำการเพิ่มเลข Mat. นี้แล้ว กรุณเลือกใหม่อีกครั้งค่ะ !');", true);
                    //    //    setFormData(fvDetailMat, FormViewMode.ReadOnly, null);
                    //    //}
                    //    //else if (ViewState["CheckDataset_mat"].ToString() == "0")
                    //    //{
                    //        ViewState["CheckDataset_mat"] = "0";
                    //        getMaterialList((TextBox)fvActor1Node1.FindControl("txt_material_search"), ViewState["CheckDataset_mat"].ToString());
                    //    //}
                    //}

                    //else if (numRowMat == 0)
                    //{
                    ViewState["CheckDataset_mat"] = "0";
                    getMaterialList((TextBox)fvActor1Node1.FindControl("txt_material_search"), ViewState["CheckDataset_mat"].ToString());
                    //}

                    txt_material_search.Text = string.Empty;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก Mat.!!!');", true);
                }

                break;

            case "txtSearchSampleCode":


                if (txtSearchSampleCode.Text != "")
                {
                    getSmapleList_AnalyticalResults((TextBox)fvAnalyticalResults.FindControl("txtSearchSampleCode"));
                    txtSearchSampleCode.Text = string.Empty;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก Sample Code.!!!');", true);

                }

                break;

            case "txt_material_search_edit":

                qa_lab_u1_qalab[] _templist_cheackMat = (qa_lab_u1_qalab[])ViewState["vsDocumentEdit"];

                //var _linqCheckMat = (from dataMat in _templist_cheackMat
                //                     where dataMat.material_code == txt_material_search_edit.Text
                //                     select new
                //                     {
                //                         dataMat.material_code
                //                     }).ToList();

                //string _listMateCheck = "";
                //// check materail ซ้ำ
                //for (int mat = 0; mat < _linqCheckMat.Count(); mat++)
                //{
                //    _listMateCheck += _linqCheckMat[mat].material_code.ToString();

                //    if (_listMateCheck == txt_material_search_edit.Text)
                //    {
                //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลเลข Materail " + txt_material_search_edit.Text + " นี้แล้ว กรุณาทำการกรอกเลข Materail ใหม่ค่ะ!');", true);
                //        getMaterialEditList((TextBox)fvUserEditDocument.FindControl("txt_material_search_edit"), "1");
                //    }
                //    else
                //    {
                getMaterialEditList((TextBox)fvUserEditDocument.FindControl("txt_material_search_edit"), "0");
                //   }

                //  }

                break;

        }

    }
    #endregion

    #region CheckBox

    protected void setTestdetail_chk()
    {
        CheckBoxList chk_settest1 = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
        CheckBoxList chk_test1 = (CheckBoxList)fvActor1Node1.FindControl("chk_test");
        TextBox chk_select_un1 = (TextBox)fvActor1Node1.FindControl("chk_select_un");
        TextBox chk_selectdetail = (TextBox)fvActor1Node1.FindControl("chk_selectdetail");
        TextBox chk_select_count = (TextBox)fvActor1Node1.FindControl("chk_select_count");
        int count = 0;
        chk_select_count.Text = chk_settest1.Items.Count.ToString();
        for (int i = 0; i < chk_settest1.Items.Count; i++)
        {
            if (chk_settest1.Items[i].Selected == true)
            {
                string str = chk_settest1.Items[i].Text;
                //MessageBox.Show(str);

                var itemValue = chk_settest1.Items[i].Value;

                //chk_select_un1.Text += itemValue;
                count++;
                chk_selectdetail.Text = count.ToString();

            }
            else
            {
                //count = 0;
                //count = i + 1;
                count--;
                chk_selectdetail.Text = count.ToString();

                if (Math.Abs(int.Parse(chk_selectdetail.Text)) == (int.Parse(chk_select_count.Text)))
                {
                    fvActor1Node1.DataSource = null;
                    fvActor1Node1.DataBind();

                    //div_water_import.Visible = true;
                    UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                    FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                    LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                    LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
                    FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                    Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
                    Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");
                    Panel Save_Excel = (Panel)FvAddImport.FindControl("Save_Excel");

                    upload.Enabled = false;
                    btnImport.Visible = false;
                    lbDocCancelExcel.Visible = false;
                    Dept_Excel.Visible = false;
                    GvExcel_Import.Visible = false;
                    Save_Excel.Visible = false;
                    //clearDataSet();
                    linkBtnTrigger(btnImport);
                }
                else
                {

                }


                //chk_settest1.SelectedItem.Selected == false;
            }
            chk_select_un1.Text = chk_selectdetail.Text;


            //chk_settest1.Items.Count.ToString();
        }
    }

    protected void getTestDetail_chk()
    {

        data_qa _data_qa_testdetail = new data_qa();

        _data_qa_testdetail.qa_m0_test_detail_list = new qa_m0_test_detail[1];
        qa_m0_test_detail _testdetail = new qa_m0_test_detail();
        _testdetail.condition = 1;//int.Parse(rd.ToString());

        _data_qa_testdetail.qa_m0_test_detail_list[0] = _testdetail;

        _data_qa_testdetail = callServicePostMasterQA(_urlQaGetTestDetail, _data_qa_testdetail);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_testdetail));

        ViewState["data_chkbox_test"] = _data_qa_testdetail.qa_m0_test_detail_list;

        CheckBoxList chk_test = (CheckBoxList)fvActor1Node1.FindControl("chk_test");
        chk_test.Items.Clear();
        chk_test.AppendDataBoundItems = true;
        chk_test.DataSource = ViewState["data_chkbox_test"];
        chk_test.DataTextField = "test_detail_name";
        chk_test.DataValueField = "test_detail_idx";
        chk_test.DataBind();
        //linkBtnTrigger(lbDocSaveImport);
    }

    protected void getSetTestDetail_chk()
    {

        data_qa _data_qa_settestdetail = new data_qa();

        _data_qa_settestdetail.qa_r0_set_test_detail_list = new qa_r0_set_test_detail[1];
        qa_r0_set_test_detail _settestdetail = new qa_r0_set_test_detail();
        _settestdetail.condition = 1;//int.Parse(rd.ToString());

        _data_qa_settestdetail.qa_r0_set_test_detail_list[0] = _settestdetail;

        _data_qa_settestdetail = callServicePostMasterQA(_urlQaGetSetTestDetail, _data_qa_settestdetail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_testdetail));

        ViewState["data_chkbox_settest"] = _data_qa_settestdetail.qa_r0_set_test_detail_list;

        CheckBoxList chk_settest = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
        chk_settest.Items.Clear();
        chk_settest.AppendDataBoundItems = true;
        chk_settest.DataSource = ViewState["data_chkbox_settest"];
        chk_settest.DataTextField = "set_test_detail_name";
        chk_settest.DataValueField = "set_test_detail_idx";
        chk_settest.DataBind();
        //linkBtnTrigger(lbDocSaveImport);
    }

    protected void CheckBox_Clicked(object sender, EventArgs e)
    {
        var cb = (CheckBoxList)sender;

        //div_water_import.Visible = true;
        // fvActor1Node1
        CheckBoxList chk_settest = (CheckBoxList)fvActor1Node1.FindControl("chk_settest");
        CheckBoxList chk_test = (CheckBoxList)fvActor1Node1.FindControl("chk_test");

        TextBox chk_select = (TextBox)fvActor1Node1.FindControl("chk_select");
        TextBox chk_selectdetail = (TextBox)fvActor1Node1.FindControl("chk_selectdetail");

        //FormView FvAddImport_chk = (FormView)docCreate.FindControl("FvAddImport");
        //  divCreate
        ///GridView GvDeptExcel_chk = (GridView)FvAddImport_chk.FindControl("GvDeptExcel");

        TextBox chk_select_un = (TextBox)fvActor1Node1.FindControl("chk_select_un");

        //Panel div_swab = (Panel)fvActor1Node1.FindControl("div_swab");
        Panel div_searchcreate = (Panel)fvActor1Node1.FindControl("div_searchcreate");

        //ViewState["Depart_RSecIDX_Excel"] = null;
        //GvDeptExcel_chk.DataSource = null;
        //GvDeptExcel_chk.DataBind();

        switch (cb.ID)
        {

            case "chk_settest":

                data_qa _data_qa_testdetail1 = new data_qa();

                _data_qa_testdetail1.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                qa_m0_test_detail _testdetail1 = new qa_m0_test_detail();
                _testdetail1.condition = 2;//int.Parse(rd.ToString());

                _data_qa_testdetail1.qa_m0_test_detail_list[0] = _testdetail1;

                _data_qa_testdetail1 = callServicePostMasterQA(_urlQaGetTestDetail, _data_qa_testdetail1);
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qa_testdetail));

                ViewState["data_chkbox_test1"] = _data_qa_testdetail1.qa_m0_test_detail_list;

                qa_m0_test_detail[] _itemTestDetail = (qa_m0_test_detail[])ViewState["data_chkbox_test1"];
                //ViewState["data_chkbox_test"]

                //if(chk_settest)      
                string[] _value_chk;
                List<String> _value = new List<string>();
                List<String> _value_in = new List<string>();
                _value.Clear();
                int v_chk = 0;
                setTestdetail_chk();

                //               
                foreach (ListItem item in chk_settest.Items)
                {
                    //linkBtnTrigger(btnImport);
                    if (item.Selected == true)
                    {

                        ++v_chk;


                        // If the item is selected, add the value to the list.
                        chk_select.Text = item.Value;
                        //  string _listitem_sentmail_all = "";
                        ArrayList _importSample = new ArrayList() { "26", "30", "35", "40", "74", "126" };
                        //ArrayList _importSample1 = new ArrayList() { "40"};
                        foreach (string _i in _importSample)
                        {
                            if (item.Value == _i)
                            {
                                //clearDataSet();
                                _value.Clear();
                                _value_chk = null;
                                chk_settest.ClearSelection();
                                chk_test.ClearSelection();
                                chk_test.Enabled = false;
                                item.Selected = true;

                                div_searchcreate.Visible = false;

                                UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                                FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                                LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                                LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
                                FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                                Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
                                //Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");

                                upload.Enabled = true;
                                btnImport.Visible = true;
                                lbDocCancelExcel.Visible = false;

                                Dept_Excel.Visible = true;

                                //fvDeptSelect
                                DropDownList ddlOrg_excel = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
                                DropDownList ddlDept_excel = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
                                DropDownList ddlSec_excel = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");
                                //litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                                getOrganizationList((DropDownList)Dept_Excel.FindControl("ddlOrg_excel"));
                                getDepartmentList(ddlDept_excel, int.Parse(ddlOrg_excel.SelectedValue));
                                getSectionList(ddlSec_excel, int.Parse(ddlOrg_excel.SelectedValue), int.Parse(ddlDept_excel.SelectedValue));

                                fvDetailSearchCreate.Visible = false;
                                fvDetailMat.Visible = false;
                                div_other_detail.Visible = false;

                                setFormData(fvDeptSelect, FormViewMode.ReadOnly, null);

                                ViewState["ClearDataSetSampleCode"] = 1;
                                linkBtnTrigger(btnImport);

                                clearDataSetOnClickCheckBox();
                                clearDataSetOnClickCheckBoxSampleList();

                                // clearDataSet();

                                break;
                            }

                            else
                            {

                                UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                                FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                                LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                                LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
                                FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                                Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
                                Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");
                                Panel Save_Excel = (Panel)FvAddImport.FindControl("Save_Excel");

                                upload.Enabled = false;
                                btnImport.Visible = false;
                                lbDocCancelExcel.Visible = false;
                                Dept_Excel.Visible = false;
                                GvExcel_Import.Visible = false;
                                Save_Excel.Visible = false;
                                div_searchcreate.Visible = true;

                                ViewState["ClearDataSetSampleCode"] = 0;
                            }

                        }

                        qa_r0_set_test_detail[] _templist_IdSetTest = (qa_r0_set_test_detail[])ViewState["data_chkbox_settest"];

                        var _linqIdSetTest = (from dt in _templist_IdSetTest
                                              where dt.set_test_detail_idx != (26) && dt.set_test_detail_idx != (30) && dt.set_test_detail_idx != (35)
                                              && dt.set_test_detail_idx != (40) && dt.set_test_detail_idx != (74) && dt.set_test_detail_idx != (126)
                                              select new
                                              {
                                                  dt.set_test_detail_idx
                                              }).ToList();

                        string SetTestId = "";
                        string listSetTestId = "";

                        for (int idset = 0; idset < _linqIdSetTest.Count(); idset++)
                        {
                            SetTestId = _linqIdSetTest[idset].set_test_detail_idx.ToString();
                            listSetTestId += "," + SetTestId;

                        }
                        //Split comma frist posision
                        string resultString = listSetTestId.IndexOf(',') > -1
                        ? listSetTestId.Substring(listSetTestId.IndexOf(',') + 1)
                        : listSetTestId;

                        // If the item is selected, add the value to the list.
                        chk_select.Text = item.Value;

                        string[] setTestChkOnly = resultString.Split(',');
                        foreach (string _i in setTestChkOnly)
                        {
                            if (item.Value == _i)
                            {
                                _value.Clear();
                                _value_chk = null;
                                chk_settest.ClearSelection();
                                chk_test.ClearSelection();
                                chk_test.Enabled = true;
                                item.Selected = true;

                                div_searchcreate.Visible = true;

                                //UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                                //FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                                //LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                                //LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
                                //FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                                //Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");

                                //upload.Enabled = true;
                                //btnImport.Visible = true;
                                //lbDocCancelExcel.Visible = true;
                                //Dept_Excel.Visible = true;

                                ////fvDeptSelect
                                //DropDownList ddlOrg_excel = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
                                //DropDownList ddlDept_excel = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
                                //DropDownList ddlSec_excel = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");
                                ////litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                                //getOrganizationList((DropDownList)Dept_Excel.FindControl("ddlOrg_excel"));
                                //getDepartmentList(ddlDept_excel, int.Parse(ddlOrg_excel.SelectedValue));
                                //getSectionList(ddlSec_excel, int.Parse(ddlOrg_excel.SelectedValue), int.Parse(ddlDept_excel.SelectedValue));

                                //fvDetailMat.Visible = false;
                                //div_other_detail.Visible = false;

                                //setFormData(fvDeptSelect, FormViewMode.ReadOnly, null);

                                //ViewState["ClearDataSetSampleCode"] = 1;
                                //linkBtnTrigger(btnImport);

                                break;
                            }

                            else
                            {

                                //UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                                //FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                                //LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
                                //LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
                                //FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
                                //Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");

                                //upload.Enabled = false;
                                //btnImport.Visible = false;
                                //lbDocCancelExcel.Visible = false;
                                //Dept_Excel.Visible = false;
                                //div_searchcreate.Visible = true;

                                //ViewState["ClearDataSetSampleCode"] = 0;
                            }

                        }



                        _value_in.Add(item.Value);

                        data_qa _data_qa_testdetail = new data_qa();
                        _data_qa_testdetail.qa_m0_test_detail_list = new qa_m0_test_detail[1];
                        qa_m0_test_detail _testdetail = new qa_m0_test_detail();
                        //var var_testdetail = ViewState["data_chkbox_test"];


                        var _linqTestDetail = (from data in _itemTestDetail //_itemTestDetail
                                               where data.root_detail == int.Parse(item.Value)//int.Parse(chk_select.Text)/*_itemTestDetail[i]*/
                                               select new
                                               {
                                                   //data.root_detail,
                                                   data.test_detail_idx,

                                               }).ToList();



                        for (int z = 0; z < _linqTestDetail.Count(); z++)
                        {
                            _value.Add(_linqTestDetail[z].test_detail_idx.ToString());

                        }
                        _value_chk = _value.ToArray();
                        //ViewState[""] = 
                        //  litDebug.Text = _value;

                        foreach (ListItem chkListSet in chk_test.Items)
                        {

                            if (Array.IndexOf(_value_chk, chkListSet.Value) > -1)
                            {
                                chkListSet.Selected = true;



                            }
                            else
                            {
                                chkListSet.Selected = false;
                                //item.Value = "0";

                            }

                        }

                    }

                }
                setTestdetail_chk();
                //  clearDataSet();

                break;

        }

    }

    #endregion

    #region dropdown list

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
    }

    protected void getOrganizationListEdit(DropDownList ddlName, int txt_org_idx)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
        ddlName.SelectedValue = txt_org_idx.ToString();
    }

    protected void getDepartmentListEdit(DropDownList ddlName, int _org_idx, int txt_rdept_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
        ddlName.SelectedValue = txt_rdept_idx.ToString();
    }

    protected void getSectionListEdit(DropDownList ddlName, int _org_idx, int _rdept_idx, int txt_rsec_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
        ddlName.SelectedValue = txt_rsec_idx.ToString();
    }

    protected void getPlaceList(DropDownList ddlName)
    {
        _data_qa.qa_m0_place_list = new qa_m0_place_detail[1];
        qa_m0_place_detail _placeList = new qa_m0_place_detail();
        _placeList.condition = 1;
        _data_qa.qa_m0_place_list[0] = _placeList;


        _data_qa = callServicePostMasterQA(_urlQaGetplace, _data_qa);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_qa));
        setDdlData(ddlName, _data_qa.qa_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "-1"));
    }

    protected void getPlaceCreateList(DropDownList ddlName)
    {
        _data_qa.qa_m0_place_list = new qa_m0_place_detail[1];
        qa_m0_place_detail _placeList = new qa_m0_place_detail();
        _placeList.condition = 2;
        _data_qa.qa_m0_place_list[0] = _placeList;


        _data_qa = callServicePostMasterQA(_urlQaGetplace, _data_qa);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_qa));
        setDdlData(ddlName, _data_qa.qa_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "-1"));
    }

    protected void getDateSampleList(DropDownList ddlName, string _GetSample1Idx)
    {
        data_qa _data_qa_sam = new data_qa();
        _data_qa_sam.qa_m0_datesample_list = new qa_m0_datesample_detail[1];
        qa_m0_datesample_detail _datesampleList = new qa_m0_datesample_detail();

        _data_qa_sam.qa_m0_datesample_list[0] = _datesampleList;

        _data_qa_sam = callServicePostMasterQA(_urlQaGetDateSampleList, _data_qa_sam);

        setDdlData(ddlName, _data_qa_sam.qa_m0_datesample_list, "date_sample_name", "date_sample_idx");
        ddlName.Items.Insert(0, new ListItem("-- วันที่บนตัวอย่าง --", "-1"));
        ddlName.SelectedValue = _GetSample1Idx;

    }

    protected void getDateSampleList2(DropDownList ddlName, int ddl_date1, string _GetSample2Idx)
    {


        //litDebug.Text = ddl_date1.ToString();
        data_qa _data_date = new data_qa();
        _data_date.qa_m0_datesample_list = new qa_m0_datesample_detail[1];
        qa_m0_datesample_detail _datesampleList_where = new qa_m0_datesample_detail();
        _datesampleList_where.date_sample_idx = ddl_date1;

        _data_date.qa_m0_datesample_list[0] = _datesampleList_where;

        _data_date = callServicePostMasterQA(_urlQaGetDateSampleListWhere, _data_date);

        setDdlData(ddlName, _data_date.qa_m0_datesample_list, "date_sample_name", "date_sample_idx");
        ddlName.Items.Insert(0, new ListItem("-- วันที่บนตัวอย่าง --", "-1"));
        //   ddlName.SelectedValue = _GetSample2Idx;
    }


    protected void getDateSampleListEdit(DropDownList ddlName, string _GetSample1Idx)
    {
        data_qa _data_qa_sam = new data_qa();
        _data_qa_sam.qa_m0_datesample_list = new qa_m0_datesample_detail[1];
        qa_m0_datesample_detail _datesampleList = new qa_m0_datesample_detail();

        _data_qa_sam.qa_m0_datesample_list[0] = _datesampleList;

        _data_qa_sam = callServicePostMasterQA(_urlQaGetDateSampleList, _data_qa_sam);

        setDdlData(ddlName, _data_qa_sam.qa_m0_datesample_list, "date_sample_name", "date_sample_idx");
        ddlName.Items.Insert(0, new ListItem("-- วันที่บนตัวอย่าง --", "-1"));
        ddlName.SelectedValue = _GetSample1Idx;

    }

    protected void getDateSampleList2Edit(DropDownList ddlName, int ddl_date1_edit, string _GetSample2Idx)
    {


        //litDebug.Text = ddl_date1.ToString();
        data_qa _data_date = new data_qa();
        _data_date.qa_m0_datesample_list = new qa_m0_datesample_detail[1];
        qa_m0_datesample_detail _datesampleList_where = new qa_m0_datesample_detail();

        if (ddl_date1_edit != -1)
        {
            _datesampleList_where.date_sample_idx = ddl_date1_edit;
        }
        else
        {
        }

        _data_date.qa_m0_datesample_list[0] = _datesampleList_where;

        _data_date = callServicePostMasterQA(_urlQaGetDateSampleListWhere, _data_date);

        setDdlData(ddlName, _data_date.qa_m0_datesample_list, "date_sample_name", "date_sample_idx");
        ddlName.Items.Insert(0, new ListItem("-- วันที่บนตัวอย่าง --", "-1"));

        if (_GetSample2Idx != "0")
        {
            ddlName.SelectedValue = _GetSample2Idx;
        }

    }

    protected void getDecision7Receive(DropDownList ddlName)
    {
        data_qa _data_node7_receive = new data_qa();
        _data_node7_receive.bind_qa_node_decision_list = new bind_qa_node_decision_detail[1];
        bind_qa_node_decision_detail _u0_doc_receive7 = new bind_qa_node_decision_detail();

        //_u0_doc_historyl.u0_qalab_idx = uidx;

        _data_node7_receive.bind_qa_node_decision_list[0] = _u0_doc_receive7;
        _data_node7_receive = callServicePostQA(_urlQaGetDecision7, _data_node7_receive);

        setDdlData(ddlName, _data_node7_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผลการพิจารณา ---", "-1"));
    }

    protected void getDecision8Approve(DropDownList ddlName)
    {
        data_qa _data_node8_receive = new data_qa();
        _data_node8_receive.bind_qa_node_decision_list = new bind_qa_node_decision_detail[1];
        bind_qa_node_decision_detail _u0_doc_receive8 = new bind_qa_node_decision_detail();

        //_u0_doc_historyl.u0_qalab_idx = uidx;

        _data_node8_receive.bind_qa_node_decision_list[0] = _u0_doc_receive8;
        _data_node8_receive = callServicePostQA(_urlQaGetDecision8, _data_node8_receive);

        setDdlData(ddlName, _data_node8_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผลการพิจารณา ---", "-1"));
    }

    protected void getDecision2Receive(DropDownList ddlName)
    {
        data_qa _data_node2_receive = new data_qa();
        _data_node2_receive.bind_qa_node_decision_list = new bind_qa_node_decision_detail[1];
        bind_qa_node_decision_detail _u0_doc_receive = new bind_qa_node_decision_detail();

        //_u0_doc_historyl.u0_qalab_idx = uidx;

        _data_node2_receive.bind_qa_node_decision_list[0] = _u0_doc_receive;
        _data_node2_receive = callServicePostQA(_urlQaGetDecision2, _data_node2_receive);

        setDdlData(ddlName, _data_node2_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผลการพิจารณา ---", "-1"));
    }

    protected void getDecision4Receive(DropDownList ddlName)
    {
        data_qa _data_node4_receive = new data_qa();
        _data_node4_receive.bind_qa_node_decision_list = new bind_qa_node_decision_detail[1];
        bind_qa_node_decision_detail _u0_doc_receive4 = new bind_qa_node_decision_detail();

        //_u0_doc_historyl.u0_qalab_idx = uidx;

        _data_node4_receive.bind_qa_node_decision_list[0] = _u0_doc_receive4;
        _data_node4_receive = callServicePostQA(_urlQaGetDecision4, _data_node4_receive);

        setDdlData(ddlName, _data_node4_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกรับงาน ---", "-1"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        // fvActor1Node1
        //FormView fvDeptSelect = (FormView)fvActor1Node1.FindControl("fvDeptSelect");
        DropDownList ddlOrg_create = (DropDownList)fvDeptSelect.FindControl("ddlOrg_create");
        DropDownList ddlDept_create = (DropDownList)fvDeptSelect.FindControl("ddlDept_create");
        DropDownList ddlSec_create = (DropDownList)fvDeptSelect.FindControl("ddlSec_create");

        // fvDetailMat
        //FormView fvDetailMat = (FormView)fvActor1Node1.FindControl("fvDetailMat");
        DropDownList ddldate_sample1_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample1_idx_create");
        TextBox txt_data_sample1_create = (TextBox)fvDetailMat.FindControl("txt_data_sample1_create");
        DropDownList ddldate_sample2_idx_create = (DropDownList)fvDetailMat.FindControl("ddldate_sample2_idx_create");
        TextBox txt_data_sample2_create = (TextBox)fvDetailMat.FindControl("txt_data_sample2_create");

        DropDownList ddlOrg_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlOrg_samplecreate");
        DropDownList ddlDept_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlDept_samplecreate");
        DropDownList ddlSec_samplecreate = (DropDownList)fvDetailMat.FindControl("ddlSec_samplecreate");

        // fvActor1Node1
        //FormView fvDeptSelect = (FormView)fvActor1Node1.FindControl("fvDeptSelect");
        Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
        DropDownList ddlOrg_excel = (DropDownList)Dept_Excel.FindControl("ddlOrg_excel");
        DropDownList ddlDept_excel = (DropDownList)Dept_Excel.FindControl("ddlDept_excel");
        DropDownList ddlSec_excel = (DropDownList)Dept_Excel.FindControl("ddlSec_excel");

        TextBox tbSample1IDX_Edit = (TextBox)fvUserEditDocument.FindControl("tbSample1IDX_Edit");
        TextBox tbSample2IDX_Edit = (TextBox)fvUserEditDocument.FindControl("tbSample2IDX_Edit");
        DropDownList ddldate_sample1_idx_edit = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample1_idx_edit");
        DropDownList ddldate_sample2_idx_edit = (DropDownList)fvUserEditDocument.FindControl("ddldate_sample2_idx_edit");

        DropDownList ddlorg_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlorg_idx_production_edit");
        DropDownList ddlrdept_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrdept_idx_production_edit");
        DropDownList ddlrsec_idx_production_edit = (DropDownList)fvUserEditDocument.FindControl("ddlrsec_idx_production_edit");


        switch (ddlName.ID)
        {

            case "ddlOrg_create":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_create, int.Parse(ddlOrg_create.SelectedItem.Value));
                ddlSec_create.Items.Clear();
                ddlSec_create.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                //ddlSec_create.Items.Clear();
                break;
            case "ddlDept_create":
                //ddlDocType.Focus();
                getSectionList(ddlSec_create, int.Parse(ddlOrg_create.SelectedItem.Value), int.Parse(ddlDept_create.SelectedItem.Value));
                break;
            case "ddlOrg_excel":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_excel, int.Parse(ddlOrg_excel.SelectedItem.Value));
                ddlSec_excel.Items.Clear();
                ddlSec_excel.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                // ddlSec_create.Items.Clear();
                break;
            case "ddlDept_excel":
                //ddlDocType.Focus();
                getSectionList(ddlSec_excel, int.Parse(ddlOrg_excel.SelectedItem.Value), int.Parse(ddlDept_excel.SelectedItem.Value));

                break;

            case "ddlOrg_samplecreate":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedItem.Value));
                ddlSec_samplecreate.Items.Clear();
                ddlSec_samplecreate.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                //ddlSec_create.Items.Clear();
                break;
            case "ddlDept_samplecreate":
                //ddlDocType.Focus();
                getSectionList(ddlSec_samplecreate, int.Parse(ddlOrg_samplecreate.SelectedItem.Value), int.Parse(ddlDept_samplecreate.SelectedItem.Value));
                break;

            case "ddlorg_idx_production_edit":
                // ddlDocType.Focus();
                getDepartmentList(ddlrdept_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedItem.Value));
                ddlrsec_idx_production_edit.Items.Clear();
                ddlrsec_idx_production_edit.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                //ddlSec_create.Items.Clear();
                break;
            case "ddlrdept_idx_production_edit":
                //ddlDocType.Focus();
                getSectionList(ddlrsec_idx_production_edit, int.Parse(ddlorg_idx_production_edit.SelectedItem.Value), int.Parse(ddlrdept_idx_production_edit.SelectedItem.Value));
                break;

            case "ddldate_sample1_idx_create":

                ViewState["vs_sample1"] = ddldate_sample1_idx_create.SelectedValue;
                ViewState["vs_sample2"] = ddldate_sample2_idx_create.SelectedValue;

                getDateSampleList2(ddldate_sample2_idx_create, int.Parse(ddldate_sample1_idx_create.SelectedItem.Value), "0");

                break;
            case "ddldate_sample2_idx_create":

                ViewState["vs_sample1"] = ddldate_sample1_idx_create.SelectedValue;
                ViewState["vs_sample2"] = ddldate_sample2_idx_create.SelectedValue;

                break;

            case "ddldate_sample1_idx_edit":

                //   litDebug.Text = "222222";
                ViewState["vs_sample1_Edit"] = ddldate_sample1_idx_edit.SelectedValue;
                ViewState["vs_sample2_Edit"] = ddldate_sample2_idx_edit.SelectedValue;

                getDateSampleList2Edit(ddldate_sample2_idx_edit, int.Parse(ddldate_sample1_idx_edit.SelectedItem.Value), "0");

                break;

            case "ddldate_sample2_idx_edit":

                ViewState["vs_sample1_Edit"] = ddldate_sample1_idx_edit.SelectedValue;
                ViewState["vs_sample2_Edit"] = ddldate_sample2_idx_edit.SelectedValue;

                break;

            case "ddlChooseLabType":

                var ddlID = (DropDownList)sender;
                var rowGrid = (GridViewRow)ddlID.NamingContainer;

                DropDownList _selectLabType = (DropDownList)rowGrid.FindControl("ddlChooseLabType");
                DropDownList _selectLocation = (DropDownList)rowGrid.FindControl("ddlChooseLocation");
                TextBox _commentBeforeExtanalLab = (TextBox)rowGrid.FindControl("txtCommentBeforeExtanalLab");
                Panel pnlAction = (Panel)fvSampleTestDetail.FindControl("pnlAction");

                TextBox tbplaceidx = (TextBox)fvDetail.FindControl("tbplaceidx");

                ViewState["vs_labtype"] = _selectLabType.SelectedValue.ToString();

                //litDebug.Text = ViewState["vs_labtype"].ToString();

                if (int.Parse(_selectLabType.SelectedValue) == ExternalCondition)
                {
                    _commentBeforeExtanalLab.Visible = true;
                    _selectLocation.Visible = true;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), "0");
                }
                else if (int.Parse(_selectLabType.SelectedValue) == Not_SentSample) // ไม่สามารถส่งตรวจได้
                {
                    _commentBeforeExtanalLab.Visible = true;
                    _selectLocation.Visible = false;
                }
                else
                {
                    _selectLocation.Visible = true;
                    _commentBeforeExtanalLab.Visible = false;
                    getLocationList((DropDownList)rowGrid.FindControl("ddlChooseLocation"), ViewState["vs_labtype"].ToString(), tbplaceidx.Text);

                }
                pnlAction.Visible = true;

                break;

            case "ddldate_sample1_idx_create_edit":

                var ddlID_edit = (DropDownList)sender;
                var rowGrid_edit = (GridViewRow)ddlID_edit.NamingContainer;

                DropDownList ddldate_sample1_idx_create_edit = (DropDownList)rowGrid_edit.FindControl("ddldate_sample1_idx_create_edit");
                DropDownList ddldate_sample2_idx_create_edit = (DropDownList)rowGrid_edit.FindControl("ddldate_sample2_idx_create_edit");

                //   litDebug.Text = "222222";
                ViewState["vs_sample1_Edit_Create"] = ddldate_sample1_idx_create_edit.SelectedValue;
                ViewState["vs_sample2_Edit_Create"] = ddldate_sample2_idx_create_edit.SelectedValue;

                getDateSampleList2Edit(ddldate_sample2_idx_create_edit, int.Parse(ddldate_sample1_idx_create_edit.SelectedItem.Value), "0");

                break;

            case "ddldate_sample2_idx_create_edit":

                var ddlID2_edit = (DropDownList)sender;
                var rowGrid2_edit = (GridViewRow)ddlID2_edit.NamingContainer;

                DropDownList ddldate_sample1_idx_create_edit2 = (DropDownList)rowGrid2_edit.FindControl("ddldate_sample1_idx_create_edit");
                DropDownList ddldate_sample2_idx_create_edit2 = (DropDownList)rowGrid2_edit.FindControl("ddldate_sample2_idx_create_edit");

                ViewState["vs_sample1_Edit_Create"] = ddldate_sample1_idx_create_edit2.SelectedValue;
                ViewState["vs_sample2_Edit_Create"] = ddldate_sample2_idx_create_edit2.SelectedValue;

                break;

            case "ddlorg_idx_production_edit_create":

                var ddlorg_edit = (DropDownList)sender;
                var rowgvssample_edit = (GridViewRow)ddlorg_edit.NamingContainer;

                DropDownList ddlorg_idx_production_edit_create = (DropDownList)rowgvssample_edit.FindControl("ddlorg_idx_production_edit_create");
                DropDownList ddlrdept_idx_production_edit_create = (DropDownList)rowgvssample_edit.FindControl("ddlrdept_idx_production_edit_create");
                DropDownList ddlrsec_idx_production_edit_create = (DropDownList)rowgvssample_edit.FindControl("ddlrsec_idx_production_edit_create");

                getDepartmentList(ddlrdept_idx_production_edit_create, int.Parse(ddlorg_idx_production_edit_create.SelectedItem.Value));
                ddlrsec_idx_production_edit_create.Items.Clear();
                ddlrsec_idx_production_edit_create.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                //ddlSec_create.Items.Clear();
                break;
            case "ddlrdept_idx_production_edit_create":

                var ddlorg_edit1 = (DropDownList)sender;
                var rowgvssample_edit1 = (GridViewRow)ddlorg_edit1.NamingContainer;

                DropDownList ddlorg_idx_production_edit_create1 = (DropDownList)rowgvssample_edit1.FindControl("ddlorg_idx_production_edit_create");
                DropDownList ddlrdept_idx_production_edit_create1 = (DropDownList)rowgvssample_edit1.FindControl("ddlrdept_idx_production_edit_create");
                DropDownList ddlrsec_idx_production_edit_create1 = (DropDownList)rowgvssample_edit1.FindControl("ddlrsec_idx_production_edit_create");


                getSectionList(ddlrsec_idx_production_edit_create1, int.Parse(ddlorg_idx_production_edit_create1.SelectedItem.Value), int.Parse(ddlrdept_idx_production_edit_create1.SelectedItem.Value));
                break;

        }
    }

    protected void getLocationList(DropDownList ddlName, string _idxplace, string _defaultLocation)
    {

        _data_qa.qa_m0_lab_list = new qa_m0_lab_detail[1];
        qa_m0_lab_detail _m0LabList = new qa_m0_lab_detail();

        _m0LabList.decision = int.Parse(_idxplace);

        _data_qa.qa_m0_lab_list[0] = _m0LabList;

        _data_qa = callServicePostMasterQA(_urlQaGetLabTypeOnSelectNode3, _data_qa);
        ViewState["vsCheckLabTypeDefault"] = _data_qa.qa_m0_lab_list;
        var _linqlocationDefault = (from dt in _data_qa.qa_m0_lab_list
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.lab_type_idx
                                    }).ToList();

        string defaultLocation = "";
        string defaultLabtype = "";



        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].m0_lab_idx.ToString();
            defaultLabtype = _linqlocationDefault[ex].lab_type_idx.ToString();
            ViewState["vsDefaultLocation"] = defaultLabtype;

            //litDebug.Text = _linqlocationDefault[ex].m0_lab_idx.ToString();
        }

        if (_defaultLocation != "0")
        {
            setDdlData(ddlName, _data_qa.qa_m0_lab_list, "lab_name", "m0_lab_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทแลป -", "0"));
            ddlName.SelectedValue = defaultLocation;

            if (defaultLabtype == ExternalID.ToString())
            {
                ddlName.Enabled = false;
            }
            else
            {

                if (_idxplace == "0")
                {

                    var linqInternal = (from dt in _data_qa.qa_m0_lab_list
                                        where dt.lab_type_idx != ExternalID
                                        //&& dt.place_idx == int.Parse(_defaultLocation)
                                        select new
                                        {
                                            dt.m0_lab_idx,
                                            dt.lab_name
                                        }).ToList();


                    string extanalidx1 = "";
                    string placeDefault = "";
                    for (int ex = 0; ex < linqInternal.Count(); ex++)
                    {
                        placeDefault = linqInternal[ex].m0_lab_idx.ToString();
                        extanalidx1 = linqInternal[ex].m0_lab_idx.ToString();
                        setDdlData(ddlName, linqInternal.ToList(), "lab_name", "m0_lab_idx");
                        ddlName.Items.Insert(0, new ListItem("- เลือกประเภทแลป -", "0"));
                        ddlName.SelectedValue = defaultLocation;// "3";//placeDefault;
                        ddlName.Enabled = true;

                        // litDebug.Text = linqInternal[ex].lab_name.ToString();


                    }
                }
                else
                {

                }
            }
        }
        else
        {
            if (_idxplace == ExternalCondition.ToString())
            {
                var linqExtanal = (from dt in _data_qa.qa_m0_lab_list
                                   where dt.lab_type_idx == ExternalID
                                   select new
                                   {
                                       dt.m0_lab_idx,
                                       dt.lab_name
                                   }).ToList();

                string extanalidx = "";

                for (int ex = 0; ex < linqExtanal.Count(); ex++)
                {
                    extanalidx = linqExtanal[ex].m0_lab_idx.ToString();

                    setDdlData(ddlName, _data_qa.qa_m0_lab_list, "lab_name", "m0_lab_idx");
                    ddlName.Items.Insert(0, new ListItem("- เลือกประเภทแลป -", "0"));
                    ddlName.SelectedValue = extanalidx;
                    ddlName.Enabled = false;
                }
            }
            else
            {
                setDdlData(ddlName, _data_qa.qa_m0_lab_list, "lab_name", "m0_lab_idx");
                ddlName.Items.Insert(0, new ListItem("- เลือกประเภทแลป -", "0"));
                ddlName.Enabled = true;

            }

        }
    }

    protected void getChooseLabType(DropDownList ddlName, string _idxLabtype, string _defaultLocation)
    {
        data_qa _data_node3_receive = new data_qa();
        _data_node3_receive.bind_qa_node_decision_list = new bind_qa_node_decision_detail[1];
        bind_qa_node_decision_detail _u0_doc_receive = new bind_qa_node_decision_detail();

        _data_node3_receive.bind_qa_node_decision_list[0] = _u0_doc_receive;
        _data_node3_receive = callServicePostQA(_urlQaGetDecision3, _data_node3_receive);



        qa_m0_lab_detail[] _templist_default = (qa_m0_lab_detail[])ViewState["vsCheckLabTypeDefault"];

        var _linqlocationDefault = (from dt in _templist_default
                                    where dt.place_idx == int.Parse(_defaultLocation)
                                    select new
                                    {
                                        dt.m0_lab_idx,
                                        dt.lab_name,
                                        dt.lab_type_idx
                                    }).ToList();

        string defaultLocation = "";

        for (int ex = 0; ex < _linqlocationDefault.Count(); ex++)
        {
            defaultLocation = _linqlocationDefault[ex].lab_type_idx.ToString();
        }

        if (defaultLocation == ExternalID.ToString())
        {
            setDdlData(ddlName, _data_node3_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทตรวจ -", "0"));
            ddlName.SelectedValue = ExternalCondition.ToString();

        }
        else
        {
            var _linqlocationDefault_interanl = (from dt in _data_node3_receive.bind_qa_node_decision_list
                                                 where dt.decision_idx != (ExternalCondition)
                                                 select new
                                                 {
                                                     dt.decision_idx,
                                                     dt.decision_name
                                                 }).ToList();

            string defaultLocation_internal = "";

            for (int _internal = 0; _internal < _linqlocationDefault.Count(); _internal++)
            {
                defaultLocation_internal = _linqlocationDefault_interanl[_internal].decision_idx.ToString();
            }

            setDdlData(ddlName, _data_node3_receive.bind_qa_node_decision_list, "decision_name", "decision_idx");
            ddlName.Items.Insert(0, new ListItem("- เลือกประเภทตรวจ -", "0"));
            ddlName.SelectedValue = defaultLocation_internal;

        }

    }

    protected void getSelectLabForSearch(DropDownList ddlName, string _idxLabtype)
    {
        data_qa _dataSelectLabSearch = new data_qa();
        _dataSelectLabSearch.qa_m0_lab_list = new qa_m0_lab_detail[1];
        qa_m0_lab_detail _m0LabSelectList = new qa_m0_lab_detail();

        _dataSelectLabSearch.qa_m0_lab_list[0] = _m0LabSelectList;

        _dataSelectLabSearch = callServicePostMasterQA(_urlQaGetLabTypeOnSelectNode3, _dataSelectLabSearch);

        setDdlData(ddlName, _dataSelectLabSearch.qa_m0_lab_list, "lab_name", "m0_lab_idx");
        ddlName.Items.Insert(0, new ListItem("- เลือกสถานที่ Lab -", "0"));

    }
    #endregion dropdown list

    #region FvDetail_DataBound
    protected void fvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "fvDetail":
                if (fvDetail.CurrentMode == FormViewMode.ReadOnly)
                {


                }

                break;
            case "fvActor1Node1":
                if (fvActor1Node1.CurrentMode == FormViewMode.Insert)
                {
                    getTestDetail_chk();
                    getSetTestDetail_chk();
                    getPlaceList((DropDownList)fvActor1Node1.FindControl("ddlPlace"));
                    //linkBtnTrigger(lbDocSaveImport);
                }
                break;
            case "FvAddImport":

                UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                FormView FvAddImport = (FormView)docCreate.FindControl("FvAddImport");
                LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");

                if (FvAddImport.CurrentMode == FormViewMode.Insert)
                {

                }
                break;


        }

    }
    #endregion

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvSampleTestDetal":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    GridView gvSampleTestDetal = (GridView)fvSampleTestDetail.FindControl("gvSampleTestDetal");
                    Label lblDocIDX_Sample = (Label)e.Row.Cells[0].FindControl("lblDocIDX_Sample");
                    Repeater rp_test_sample = (Repeater)e.Row.Cells[1].FindControl("rp_test_sample");
                    Panel pnlChooseLocation = (Panel)e.Row.Cells[4].FindControl("pnlChooseLocation");
                    Panel pnlAction = (Panel)fvSampleTestDetail.FindControl("pnlAction");
                    Label _Staidx = (Label)e.Row.Cells[5].FindControl("lblStaidx");
                    Label lbllab_nameadmin = (Label)e.Row.Cells[4].FindControl("lbllab_nameadmin");
                    LinkButton btnViewSamplelist = (LinkButton)e.Row.Cells[6].FindControl("btnViewSamplelist");
                    LinkButton btnPrintReport = (LinkButton)e.Row.Cells[6].FindControl("btnPrintReport");
                    TextBox txtCommentBeforeExtanalLab = (TextBox)e.Row.Cells[4].FindControl("txtCommentBeforeExtanalLab");

                    //count select button in node11
                    Label lblCount_All = (Label)e.Row.Cells[0].FindControl("lblCount_All");
                    Label lblCount_Success = (Label)e.Row.Cells[0].FindControl("lblCount_Success");
                    Panel pnlActionNode11 = (Panel)fvSampleTestDetail.FindControl("pnlActionNode11");
                    Label lblCount_Place = (Label)e.Row.Cells[0].FindControl("lblCount_Place");

                    //fvDetail
                    TextBox tbstaidx = (TextBox)fvDetail.FindControl("tbstaidx");
                    TextBox tbplaceidx = (TextBox)fvDetail.FindControl("tbplaceidx");

                    //litDebug.Text = lblDocIDX_Sample.ToString();//HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));

                    data_qa _data_test_sample_ = new data_qa();
                    _data_test_sample_.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(lblDocIDX_Sample.Text);

                    _data_test_sample_.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;

                    _data_test_sample_ = callServicePostQA(_urlQaGetTestSample, _data_test_sample_);
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_sample_));

                    setRepeaterData(rp_test_sample, _data_test_sample_.bind_qa_m0_test_detail_list);

                    //setFormData(fvSampleTestDetail, FormViewMode.ReadOnly, _data_sample_de.qa_lab_u1_qalab_list);
                    pnlChooseLocation.Visible = false;
                    //pnlAction.Visible = false;
                    lbllab_nameadmin.Visible = false;
                    btnViewSamplelist.Visible = false;
                    btnPrintReport.Visible = false;
                    pnlActionNode11.Visible = false;
                    // pnlAction.Visible = false;

                    switch (_Staidx.Text)
                    {
                        case "0":
                        case "1":
                        case "2":
                        case "6":
                        case "7":
                        case "8":
                        case "11":
                        case "12":
                        case "13":



                            lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = true;

                            if (tbstaidx.Text == "1")
                            {
                                if (lblCount_All.Text == lblCount_Success.Text && ViewState["rdept_permission"].ToString() == "27")
                                {
                                    pnlActionNode11.Visible = true;

                                }
                                else
                                {
                                    pnlActionNode11.Visible = false;

                                }
                            }
                            else
                            {
                                pnlActionNode11.Visible = false;
                            }

                            //check node 3 select place
                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                if (_Staidx.Text == "5")
                                {
                                    pnlChooseLocation.Visible = true;
                                    pnlAction.Visible = true;

                                    //     getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0", "0");
                                    btnViewSamplelist.Visible = true;

                                }
                                //litDebug.Text = lblCount_Place.Text;


                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                //  getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0", "0");
                                btnViewSamplelist.Visible = true;

                                //litDebug.Text = "33333";

                            }

                            break;
                        case "3":
                            lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = false;

                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                if (_Staidx.Text == "5")
                                {
                                    pnlChooseLocation.Visible = true;
                                    pnlAction.Visible = true;

                                    //   getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                    btnViewSamplelist.Visible = true;
                                }

                                // litDebug.Text = "iiuijdKo";


                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                // getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0", "0");
                                btnViewSamplelist.Visible = false;

                                // litDebug.Text = "33333";

                            }


                            break;
                        case "4":
                            lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = true;

                            //check node 3 select place
                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                if (_Staidx.Text == "5")
                                {
                                    pnlChooseLocation.Visible = true;
                                    pnlAction.Visible = true;

                                    //getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                    //  getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"),"0", "0");
                                    btnViewSamplelist.Visible = true;
                                }
                                //else if (_Staidx.Text == "4")
                                //{
                                //    pnlChooseLocation.Visible = false;
                                //    pnlAction.Visible = false;

                                //    //getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                //    //  getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"),"0", "0");
                                //    btnViewSamplelist.Visible = true;
                                //}

                                // litDebug.Text = "iiuijdKo";


                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                // getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                //  getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"), "0", tbplaceidx.Text);
                                btnViewSamplelist.Visible = true;

                                // litDebug.Text = "33333";

                            }
                            break;


                        case "18":
                            lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = false;

                            //check node 3 select place
                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                if (_Staidx.Text == "5")
                                {
                                    pnlChooseLocation.Visible = true;
                                    pnlAction.Visible = true;

                                    //getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                    //  getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"),"0", "0");
                                    btnViewSamplelist.Visible = true;
                                }

                                // litDebug.Text = "iiuijdKo";


                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                // getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                //  getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"), "0", tbplaceidx.Text);
                                btnViewSamplelist.Visible = false;

                                // litDebug.Text = "33333";

                            }
                            //pnlAction.Visible = false;
                            break;
                        case "5":
                            //lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = true;

                            Label _Status = (Label)e.Row.Cells[0].FindControl("lblStaidx");

                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                var _ddlLocation = (DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation");

                                pnlChooseLocation.Visible = true;
                                pnlAction.Visible = true;
                                getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"), "0", tbplaceidx.Text);
                                getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0", tbplaceidx.Text);
                                _ddlLocation.Enabled = false;
                                if (ViewState["vsDefaultLocation"].ToString() == ExternalID.ToString())
                                {
                                    txtCommentBeforeExtanalLab.Visible = true;
                                }
                                else
                                {
                                    txtCommentBeforeExtanalLab.Visible = false;
                                }
                                btnViewSamplelist.Visible = true;
                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                //getLocationList((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLocation"), "0", tbplaceidx.Text);
                                //getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" ,tbplaceidx.Text);

                                //if (ViewState["vsDefaultLocation"].ToString() == ExternalID.ToString())
                                //{
                                //    txtCommentBeforeExtanalLab.Visible = true;
                                //}
                                //else
                                //{
                                //    txtCommentBeforeExtanalLab.Visible = false;
                                //}

                                // btnViewSamplelist.Visible = true;

                            }

                            break;

                        case "10":
                            btnPrintReport.Visible = true;
                            lbllab_nameadmin.Visible = true;
                            btnViewSamplelist.Visible = true;

                            if (tbstaidx.Text == "1")
                            {
                                if (lblCount_All.Text == lblCount_Success.Text && ViewState["rdept_permission"].ToString() == "27")
                                {
                                    pnlActionNode11.Visible = true;

                                    //litDebug.Text = tbstaidx.Text;
                                }
                                else
                                {
                                    pnlActionNode11.Visible = false;
                                    //litDebug.Text = tbstaidx.Text;
                                }
                            }
                            else
                            {
                                pnlActionNode11.Visible = false;
                            }

                            //check node 3 select place
                            if (lblCount_Place.Text != "0" && ViewState["rdept_permission"].ToString() == "27") //เจ้าหน้าที่ QA
                            {
                                if (_Staidx.Text == "5")
                                {
                                    pnlChooseLocation.Visible = true;
                                    pnlAction.Visible = true;

                                    //  getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0", "0");
                                    btnViewSamplelist.Visible = true;

                                }
                                //litDebug.Text = lblCount_Place.Text;


                            }
                            else
                            {
                                lbllab_nameadmin.Visible = true;

                                pnlChooseLocation.Visible = false;
                                pnlAction.Visible = false;

                                //  getChooseLabType((DropDownList)e.Row.Cells[4].FindControl("ddlChooseLabType"), "0" , "0");
                                btnViewSamplelist.Visible = true;

                                //litDebug.Text = "33333";

                            }


                            break;

                    }

                }

                break;
            case "gvLab":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lblU1IDX_Lab = (Label)e.Row.Cells[1].FindControl("lblU1IDX_Lab");
                    Repeater rp_testdetail_lab = (Repeater)e.Row.Cells[2].FindControl("rp_testdetail_lab");
                    Label lblStaidx_Lab = (Label)e.Row.Cells[4].FindControl("lblStaidx_Lab");
                    LinkButton btnReportSucess = (LinkButton)e.Row.Cells[6].FindControl("btnReportSucess");

                    LinkButton lnkbtnSaveN4Accept = (LinkButton)e.Row.Cells[5].FindControl("lnkbtnSaveN4Accept");
                    LinkButton lnkbtnSaveN4Reject = (LinkButton)e.Row.Cells[5].FindControl("lnkbtnSaveN4Reject");
                    //Label lblStaidx_Lab = (Label)e.Row.Cells[4].FindControl("lblStaidx_Lab");


                    //litDebug.Text = lblDocIDX_Sample.ToString();//HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));

                    data_qa _data_testdetail_lab = new data_qa();
                    _data_testdetail_lab.bindlab_qa_m0_testdetail_list = new bindlab_m0_test_detail[1];
                    bindlab_m0_test_detail _doc_testdetail_lab = new bindlab_m0_test_detail();

                    _doc_testdetail_lab.u1_qalab_idx = int.Parse(lblU1IDX_Lab.Text);

                    _data_testdetail_lab.bindlab_qa_m0_testdetail_list[0] = _doc_testdetail_lab;

                    _data_testdetail_lab = callServicePostQA(_urlQaGetTestDetailLab, _data_testdetail_lab);
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_testdetail_lab));

                    ViewState["rp_node4_receive"] = _data_testdetail_lab.bindlab_qa_m0_testdetail_list;
                    setRepeaterData(rp_testdetail_lab, ViewState["rp_node4_receive"]);

                    //getDecision4Receive((DropDownList)e.Row.Cells[5].FindControl("ddlAppoveN4"));

                    btnReportSucess.Visible = false;
                    lnkbtnSaveN4Accept.Visible = false;
                    lnkbtnSaveN4Reject.Visible = false;

                    switch (lblStaidx_Lab.Text)
                    {
                        case "0":

                            break;
                        case "6":
                            lnkbtnSaveN4Accept.Visible = true;
                            lnkbtnSaveN4Reject.Visible = true;
                            break;
                        case "10":
                            btnReportSucess.Visible = true;
                            break;
                    }

                }

                break;
            case "GvFormRecordResult":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label _nameform_detail = (Label)e.Row.Cells[0].FindControl("lbtext_detail_name");
                    Label _form_detail_name = (Label)e.Row.Cells[0].FindControl("lbform_detail_name");
                    GridView GvRecordResult = (GridView)e.Row.Cells[0].FindControl("GvRecordResult");
                    Label _form_root_idx = (Label)e.Row.Cells[0].FindControl("Lbr1form_create_idx");
                    // Label _RecordResult = (Label)e.Row.Cells[0].FindControl("lbRecordResult");
                    Label _option_idx = (Label)e.Row.Cells[0].FindControl("Lboption_idx");
                    TextBox _inputRecordResultTest = (TextBox)e.Row.Cells[0].FindControl("txtRecordResultTest");
                    DropDownList _ChooseResult = (DropDownList)e.Row.Cells[0].FindControl("ddlChooseResult");

                    if (_option_idx.Text == "1")
                    { _inputRecordResultTest.Visible = true; }
                    else
                    { _inputRecordResultTest.Visible = false; }

                    if (_option_idx.Text == "2")
                    { _ChooseResult.Visible = true; }
                    else
                    { _ChooseResult.Visible = false; }

                    // Bind gridview form for record result
                    data_qa _data_qa_option = new data_qa();
                    _data_qa_option.bind_qa_lab_form_list = new bind_qa_lab_form_detail[1];
                    bind_qa_lab_form_detail _option = new bind_qa_lab_form_detail();
                    _option.form_detail_root_idx = int.Parse(_form_root_idx.Text);

                    _data_qa_option.bind_qa_lab_form_list[0] = _option;

                    _data_qa_option = callServicePostQA(_urlQaGetSelectForm, _data_qa_option);
                    setGridData(GvRecordResult, _data_qa_option.bind_qa_lab_form_list);


                    //if (_nameform_detail.Text == null || _nameform_detail.Text == "")
                    //{
                    //    _form_detail_name.Visible = true;
                    //    _nameform_detail.Visible = false;

                    //}
                    //else
                    //{
                    //    _nameform_detail.Visible = true;
                    //    _form_detail_name.Visible = false;
                    //}


                    //else
                    //{
                    //    _nameform_detail.Text = "Sirirak"; //_data_qa_option.bind_qa_lab_form_list[0].form_detail_name.ToString();
                    //}

                    int _rowcountlv2 = 0;
                    foreach (GridViewRow rowCountLv2 in GvRecordResult.Rows)
                    {   // Bind gridview form for record result level 3
                        GridView GvRecordResultRoot = (GridView)rowCountLv2.FindControl("GvRecordResultRoot");
                        Label _r1FormIDXLv2 = (Label)rowCountLv2.FindControl("Lbr1_form_create_idxlv2");

                        Label _option_idxlv2 = (Label)rowCountLv2.FindControl("Lboption_idxlv2");
                        TextBox _inputRecordResult = (TextBox)rowCountLv2.FindControl("txtRecordResult");

                        data_qa _data_qa_option1 = new data_qa();
                        _data_qa_option1.bind_qa_lab_form_list = new bind_qa_lab_form_detail[1];
                        bind_qa_lab_form_detail _option1 = new bind_qa_lab_form_detail();
                        _option1.form_detail_root_idx = int.Parse(_r1FormIDXLv2.Text);

                        _data_qa_option1.bind_qa_lab_form_list[0] = _option1;

                        _data_qa_option1 = callServicePostQA(_urlQaGetSelectForm, _data_qa_option1);
                        setGridData(GvRecordResultRoot, _data_qa_option1.bind_qa_lab_form_list);


                        if (_option_idxlv2.Text == "1")
                        {
                            _inputRecordResult.Visible = true;
                        }
                        else
                        {
                            _inputRecordResult.Visible = false;
                        }


                        _rowcountlv2++;

                    }

                }
                break;

            case "GvSupervisor":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    bind_qa_lab_result_detail[] _templist = (bind_qa_lab_result_detail[])ViewState["vs_listResultLab"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.test_detail_name.ToString();
                        count_test++;
                        ViewState["rowCoutList"] = count_test;
                    }


                    int _resultDB = int.Parse(ViewState["rowCoutList"].ToString());

                    //litDebug.Text = _resultDB.ToString();

                    int _numrowDB = _resultDB;
                    int _numrowDB_div = _resultDB - 1;
                    for (int _loop = 0; _loop < e.Row.Cells.Count; _loop++)
                    {
                        if (_loop == 0)
                        {
                            e.Row.Cells[_loop].Text = "Sample Code";
                        }
                        //else if (_loop == 1)
                        //{
                        //    e.Row.Cells[_loop].Text = "Sample Code";
                        //}
                        else if (_loop <= _resultDB) // หัว
                        {

                            //// data_qa _data_lab_resulttest_bind = new data_qa();
                            //// _data_lab_resulttest_bind.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
                            //// bind_qa_lab_result_detail _result_labbind = new bind_qa_lab_result_detail();
                            //// _result_labbind.condition = 0;
                            ////// _result_labbind.m0_lab = int.Parse(tbValuePlaceSup.Text);
                            //// _data_lab_resulttest_bind.bind_qa_lab_result_list[0] = _result_labbind;

                            //// _data_lab_resulttest_bind = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resulttest_bind);

                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_lab_resulttest_bind));

                            //ViewState["vs_listResultLabbind"] = _data_lab_resulttest_bind.bind_qa_lab_result_list;


                            string _text_header = String.Empty;
                            int _countrow1 = 1;
                            foreach (var _loop_rowText in _templist)
                            {

                                _text_header = _loop_rowText.test_detail_name.ToString();
                                e.Row.Cells[_countrow1].Text = _text_header.ToString();//"mod";
                                //e.Row.RowIndex[_countrow1] = _text.ToString();
                                _countrow1++;
                            }



                            //e.Row.Cells[_loop].Text = "mod";
                        }
                        else
                        {
                            //e.Row.Cells[_loop].Text = "Approve";//_loop.ToString();
                            ////litDebug.Text = _loop.ToString();
                            //LinkButton lb = new LinkButton();
                            //lb.CommandArgument = e.Row.Cells[_loop].Text;
                            //lb.CommandName = "NumClick";
                            //lb.Text = e.Row.Cells[_loop].Text;
                            //e.Row.Cells[_loop].Controls.Add((Control)lb);
                            //litDebug.Text = _loop1.ToString();
                        }

                    }


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    //litDebug.Text = "77777777";

                    bind_qa_lab_result_detail[] _templist = (bind_qa_lab_result_detail[])ViewState["vs_listResultLab"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.test_detail_name.ToString();
                        count_test++;
                        ViewState["rowCoutList2"] = count_test;
                    }


                    int _resultDB2 = int.Parse(ViewState["rowCoutList2"].ToString());

                    for (int _loop2 = 0; _loop2 < e.Row.Cells.Count; _loop2++)
                    {
                        if (_loop2 == 0)
                        {
                            // e.Row.Cells[_loop2].Text = "sample code";
                        }
                        else if (_loop2 <= _resultDB2)
                        {

                            //string[] _value_chk;
                            //List<String> _value = new List<string>();
                            //List<String> _value_in = new List<string>();

                            ////////data_qa _data_lab_resultrow = new data_qa();
                            ////////_data_lab_resultrow.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
                            ////////bind_qa_lab_result_detail _result_row = new bind_qa_lab_result_detail();

                            ////////_result_row.condition = 3;
                            ////////_result_row.m0_lab = int.Parse(tbValuePlaceSup.Text);

                            ////////_data_lab_resultrow.bind_qa_lab_result_list[0] = _result_row;
                            ////////_data_lab_resultrow = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resultrow);

                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_lab_resultrow));

                            string sample_codeselect = string.Empty;

                            sample_codeselect = e.Row.Cells[0].Text;

                            //litDebug.Text += sample_codeselect;

                            //litDebug.Text = e.Row.Cells[13].Text;

                            //ViewState["data_result"] = _data_lab_resultrow.bind_qa_lab_result_list;
                            bind_qa_lab_result_detail[] _item_Result = (bind_qa_lab_result_detail[])ViewState["vs_listResultLab_result"];


                            var _linqTest = (from data in _item_Result //_itemTestDetail
                                             where data.sample_code == e.Row.Cells[0].Text
                                             && data.test_detail_idx == int.Parse(e.Row.Cells[_loop2].Text) //int.Parse(chk_select.Text)/*_itemTestDetail[i]*/

                                             select new
                                             {
                                                 //data.root_detail,
                                                 data.test_detail_idx,
                                                 data.sample_code,
                                                 data.detail_tested

                                             }).ToList();


                            string test_show = string.Empty;

                            e.Row.Cells[_loop2].Text = "-";

                            for (int z = 0; z < _linqTest.Count(); z++)
                            {
                                //_value.Add(_linqTestDetail[z].test_detail_idx.ToString());

                                test_show += int.Parse(_linqTest[z].test_detail_idx.ToString());
                                //litDebug.Text += test_show;

                                if (e.Row.Cells[0].Text == _linqTest[z].sample_code.ToString()
                                    && e.Row.Cells[_loop2].Text == _linqTest[z].test_detail_idx.ToString())
                                {

                                    if (e.Row.Cells[_loop2].Text != _linqTest[z].test_detail_idx.ToString())
                                    {
                                        //e.Row.Cells[_loop2].Text = "-";
                                    }

                                }
                                else
                                {
                                    e.Row.Cells[_loop2].Text = _linqTest[z].detail_tested.ToString();

                                }

                            }

                        }
                        else
                        {

                            string buttonApprove = "<button type='button' class='btn btn-success btn-sm col-sm-12 loading-icon-button-approve'" + "data-toggle='modal'" + "data-target='.bs - example - modal - lg'" +
                                "OnClick='return getSupervisorApprove(\"" + e.Row.Cells[0].Text + "\",\"" + _emp_idx + "\",\"" + DECISION_APPROVE + "\",\"" + tbValuePlaceSup.Text + "\")'>อนุมัติ</button>";

                            string icon_load = "<div class='loading-icon-approve'>"
                                + "<img src='" + ResolveUrl("~/masterpage/images/loading.gif") + "' width='25' height='25' />"
                                + "</div>";

                            Label _lbApprove = new Label();
                            _lbApprove.ID = "idSupApprove";
                            _lbApprove.Text = buttonApprove;
                            e.Row.Cells[_loop2].Controls.Add(_lbApprove);

                            string buttonNotApprove = "<button type='button' class='btn btn-danger btn-sm col-sm-12 loading-icon-button-approve'" + "data-toggle='modal'" + "data-target='.bs - example - modal - lg'" +
                                "OnClick='return getSupervisorApprove(\"" + e.Row.Cells[0].Text + "\",\"" + _emp_idx + "\",\"" + DECISION_NOTAPPROVE + "\",\"" + tbValuePlaceSup.Text + "\")'>ไม่อนุมัติ</button>";

                            Label _lbNotApprove = new Label();
                            _lbNotApprove.ID = "idSupNotApprove";
                            _lbNotApprove.Text = buttonNotApprove;
                            e.Row.Cells[_loop2].Controls.Add(_lbNotApprove);

                            Label _lbIcon = new Label();
                            _lbIcon.ID = "lbIconLoad";
                            _lbIcon.Text = icon_load;
                            e.Row.Cells[_loop2].Controls.Add(_lbIcon);

                        }

                    }

                }

                break;

            case "gvDoingList":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {


                }
                break;

            case "gvFileResult":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);

                }
                break;

            case "gvFileResultUser":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    HyperLink btnDL112 = (HyperLink)e.Row.Cells[0].FindControl("btnDL112");
                    HiddenField hidFile112 = (HiddenField)e.Row.Cells[0].FindControl("hidFile112");
                    // Display the company name in italics.
                    string LinkHost112 = string.Format("http://{0}", Request.Url.Host);

                    btnDL112.NavigateUrl = LinkHost112 + MapURL(hidFile112.Value);

                }
                break;

            case "GvShowExportExcel":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    //    qa_lab_u1_qalab[] _templistExportExcel = (qa_lab_u1_qalab[])ViewState["vs_listLabExportExcel"];
                    //    int count_columnHeader = 0;
                    //    string countrowColumnExcel = "";
                    //    foreach (var HeaderExportExcel in _templistExportExcel)
                    //    {
                    //        countrowColumnExcel += HeaderExportExcel.test_detail_name.ToString();
                    //        count_columnHeader++;
                    //        ViewState["rowCoutListColumnExportExcel"] = count_columnHeader;
                    //    }


                    //    int _resultExportExcelInDB = int.Parse(ViewState["rowCoutListColumnExportExcel"].ToString());

                    //    //litDebug.Text = _resultDB.ToString();

                    //    int _numrowExportExcelInDB = _resultExportExcelInDB;
                    //    int _numrowExportExcelInDB_div = _resultExportExcelInDB - 1;

                    //    litDebug.Text = "555555555";

                    //    for (int _loopHeaderExcel = 3; _loopHeaderExcel < e.Row.Cells.Count; _loopHeaderExcel++)
                    //    {

                    //        if (_loopHeaderExcel == 5)
                    //        {
                    //            e.Row.Cells[_loopHeaderExcel].Text = "Sample Code";
                    //        }
                    //        else
                    //        {

                    //        }


                    //        //if (_loopHeaderExcel <= _resultExportExcelInDB)
                    //        //{

                    //        //    data_qa _dataExport_header = new data_qa();
                    //        //    _dataExport_header.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    //        //    qa_lab_u1_qalab _export_header = new qa_lab_u1_qalab();
                    //        //    _export_header.condition = 0;
                    //        //    _dataExport_header.qa_lab_u1_qalab_list[0] = _export_header;
                    //        //    _dataExport_header = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_header);

                    //        //    //  ViewState["vs_listLabExportExcel"] = _dataExport_header.qa_lab_u1_qalab_list;


                    //        //    string _text_header_ExportExcel = String.Empty;
                    //        //    int _countrowHeaderExcel = 4;
                    //        //    foreach (var _loop_rowText in _dataExport_header.qa_lab_u1_qalab_list)
                    //        //    {

                    //        //        _text_header_ExportExcel = _loop_rowText.test_detail_name.ToString();
                    //        //        e.Row.Cells[_countrowHeaderExcel].Text = _text_header_ExportExcel.ToString();//"mod";
                    //        //        _countrowHeaderExcel++;
                    //        //    }
                    //        //}
                    //        //else
                    //        //{

                    //        //}

                    //    }

                    //}
                    //if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                    //{// }

                    //    qa_lab_u1_qalab[] _templist = (qa_lab_u1_qalab[])ViewState["vs_listLabExportExcel"];
                    //    int count_test = 0;
                    //    string countrow = "";
                    //    foreach (var test in _templist)
                    //    {
                    //        countrow += test.test_detail_name.ToString();
                    //        count_test++;
                    //        ViewState["rowCoutList2"] = count_test;
                    //    }


                    //    int _resultDB2 = int.Parse(ViewState["rowCoutList2"].ToString());
                    //    litDebug.Text = e.Row.Cells.Count.ToString();
                    //    for (int _loop2 = 3; _loop2 < e.Row.Cells.Count; _loop2++)
                    //    {
                    //        if (_loop2 == 0)
                    //        {
                    //            // e.Row.Cells[_loop2].Text = "sample code";
                    //        }
                    //        else if (_loop2 <= _resultDB2+2) // ที่ บวก 3 ข้างหลังคือ แทน column ที่ทำการใส่ตัว Document , sample code , sample name ไป เพื่อให้ loop วนครบ
                    //        {

                    //            data_qa _dataExport_documentNo = new data_qa();
                    //            _dataExport_documentNo.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                    //            qa_lab_u1_qalab _result_row = new qa_lab_u1_qalab();

                    //            _result_row.condition = 3;

                    //            _dataExport_documentNo.qa_lab_u1_qalab_list[0] = _result_row;
                    //            _dataExport_documentNo = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_documentNo);

                    //            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_lab_resultrow));

                    //            string sample_codeselect = string.Empty;

                    //            sample_codeselect = e.Row.Cells[1].Text;

                    //            ViewState["data_result"] = _dataExport_documentNo.qa_lab_u1_qalab_list;
                    //            qa_lab_u1_qalab[] _item_Result = (qa_lab_u1_qalab[])ViewState["data_result"];


                    //            var _linqTest = (from data in _item_Result //_itemTestDetail
                    //                             where data.sample_code == e.Row.Cells[1].Text
                    //                             && data.test_detail_idx == int.Parse(e.Row.Cells[_loop2].Text) //int.Parse(chk_select.Text)/*_itemTestDetail[i]*/

                    //                             select new
                    //                             {
                    //                                 //data.root_detail,
                    //                                 data.test_detail_idx,
                    //                                 data.sample_code,
                    //                                 data.detail_tested

                    //                             }).ToList();


                    //            string test_show = string.Empty;
                    //            //int temp = GvShowExportExcel.Columns.Count;

                    //            e.Row.Cells[_loop2].Text = "-";
                    //            //  litDebug.Text += e.Row.Cells[2].Text;
                    //            for (int z = 0; z < _linqTest.Count(); z++)
                    //            {
                    //                //_value.Add(_linqTestDetail[z].test_detail_idx.ToString());

                    //                test_show += int.Parse(_linqTest[z].test_detail_idx.ToString());
                    //                //litDebug.Text += test_show;

                    //                if (e.Row.Cells[1].Text == _linqTest[z].sample_code.ToString()
                    //                    && e.Row.Cells[_loop2].Text == _linqTest[z].test_detail_idx.ToString())
                    //                {

                    //                    if (e.Row.Cells[_loop2].Text != _linqTest[z].test_detail_idx.ToString())
                    //                    {
                    //                        //e.Row.Cells[_loop2].Text = "-";
                    //                    }

                    //                }
                    //                else
                    //                {
                    //                    e.Row.Cells[_loop2].Text = _linqTest[z].detail_tested.ToString();

                    //                }

                    //            }

                    //        }
                    //    }
                }

                break;

            case "gvDocumentEditList":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label _IDU1Docedit = (Label)e.Row.Cells[0].FindControl("lblDocIDX_Sample_edit");
                    Repeater rp_test_sample_edit = (Repeater)e.Row.Cells[1].FindControl("rp_test_sample_edit");

                    data_qa _data_test_sample_edit = new data_qa();
                    _data_test_sample_edit.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail_edit = new bindqa_m0_test_detail();

                    _doc_sampledeail_edit.u1_qalab_idx = int.Parse(_IDU1Docedit.Text);

                    _data_test_sample_edit.bind_qa_m0_test_detail_list[0] = _doc_sampledeail_edit;

                    _data_test_sample_edit = callServicePostQA(_urlQaGetTestSample, _data_test_sample_edit);

                    setRepeaterData(rp_test_sample_edit, _data_test_sample_edit.bind_qa_m0_test_detail_list);

                }

                break;

            case "gvSampleCodeSearchPrint":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lblDocIDX_Sample_search_print = (Label)e.Row.Cells[0].FindControl("lblDocIDX_Sample_search_print");
                    Repeater rp_test_sample_search_print = (Repeater)e.Row.Cells[1].FindControl("rp_test_sample_search_print");

                    data_qa _data_test_sample_search_print = new data_qa();
                    _data_test_sample_search_print.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                    bindqa_m0_test_detail _doc_sampledeail = new bindqa_m0_test_detail();

                    _doc_sampledeail.u1_qalab_idx = int.Parse(lblDocIDX_Sample_search_print.Text);

                    _data_test_sample_search_print.bind_qa_m0_test_detail_list[0] = _doc_sampledeail;

                    _data_test_sample_search_print = callServicePostQA(_urlQaGetTestSample, _data_test_sample_search_print);

                    setRepeaterData(rp_test_sample_search_print, _data_test_sample_search_print.bind_qa_m0_test_detail_list);

                }

                break;

            case "GvSample":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox txt_material_code_create_edit = (TextBox)e.Row.FindControl("txt_material_code_create_edit");
                    TextBox txt_samplename_create_edit = (TextBox)e.Row.FindControl("txt_samplename_create_edit");
                    TextBox txt_txt_typesample_edit = (TextBox)e.Row.FindControl("txt_txt_typesample_edit");

                    TextBox txt_sample1_idx_edit = (TextBox)e.Row.FindControl("txt_sample1_idx_edit");
                    TextBox txt_sample2_idx_edit = (TextBox)e.Row.FindControl("txt_sample2_idx_edit");

                    TextBox txt_org_idx_production_edit_create = (TextBox)e.Row.FindControl("txt_org_idx_production_edit_create");
                    TextBox txt_rdept_idx_production_edit_create = (TextBox)e.Row.FindControl("txt_rdept_idx_production_edit_create");
                    TextBox txt_rsec_idx_production_edit_create = (TextBox)e.Row.FindControl("txt_rsec_idx_production_edit_create");

                    DropDownList ddlorg_idx_production_edit_create = (DropDownList)e.Row.FindControl("ddlorg_idx_production_edit_create");
                    DropDownList ddlrdept_idx_production_edit_create = (DropDownList)e.Row.FindControl("ddlrdept_idx_production_edit_create");
                    DropDownList ddlrsec_idx_production_edit_create = (DropDownList)e.Row.FindControl("ddlrsec_idx_production_edit_create");

                    var _SelectIDXDateSample1_edit = (DropDownList)e.Row.FindControl("ddldate_sample1_idx_create_edit");

                    if(txt_material_code_create_edit.Text == "-")
                    {
                       
                        txt_material_code_create_edit.Enabled = true;
                        txt_samplename_create_edit.Enabled = true;
                        txt_txt_typesample_edit.Enabled = true;
                    }
                    else
                    {
                        
                        txt_material_code_create_edit.Enabled = false;
                        txt_samplename_create_edit.Enabled = false;
                        txt_txt_typesample_edit.Enabled = false;
                    }



                    getDateSampleListEdit((DropDownList)e.Row.FindControl("ddldate_sample1_idx_create_edit"), txt_sample1_idx_edit.Text);
                    getDateSampleList2Edit((DropDownList)e.Row.FindControl("ddldate_sample2_idx_create_edit"), int.Parse(_SelectIDXDateSample1_edit.SelectedValue), txt_sample2_idx_edit.Text);


                    getOrganizationListEdit((DropDownList)e.Row.FindControl("ddlorg_idx_production_edit_create"), int.Parse(txt_org_idx_production_edit_create.Text));

                    getDepartmentListEdit(ddlrdept_idx_production_edit_create, int.Parse(ddlorg_idx_production_edit_create.SelectedValue), int.Parse(txt_rdept_idx_production_edit_create.Text));

                    getSectionListEdit(ddlrsec_idx_production_edit_create, int.Parse(ddlorg_idx_production_edit_create.SelectedValue), int.Parse(ddlrdept_idx_production_edit_create.SelectedValue), int.Parse(txt_rsec_idx_production_edit_create.Text));

                }

                break;




        }
    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "GvSample":

                GridView GvSample = (GridView)docCreate.FindControl("GvSample");

                var DeleteSampleDetail = (DataSet)ViewState["vsSample"];
                var drDriving = DeleteSampleDetail.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsSample"] = DeleteSampleDetail;
                GvSample.EditIndex = -1;


                GvSample.DataSource = ViewState["vsSample"];
                GvSample.DataBind();

                break;

        }

    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "GvSample":


                GridView GvSample = (GridView)docCreate.FindControl("GvSample");
                FormView fvActor1Node1 = (FormView)docCreate.FindControl("fvActor1Node1");
                Panel div_searchcreate = (Panel)fvActor1Node1.FindControl("div_searchcreate");

                TextBox txt_material_search = (TextBox)div_searchcreate.FindControl("txt_material_search");
                LinkButton btnSearchMat = (LinkButton)div_searchcreate.FindControl("btnSearchMat");

                LinkButton lbDocSave = (LinkButton)fvDeptSelect.FindControl("lbDocSave");
                LinkButton lbDocCancel = (LinkButton)fvDeptSelect.FindControl("lbDocCancel");

                //HyperLink Focusset = (HyperLink)fvCreatePurchasequipment.FindControl("Focusset");

                lbDocSave.Visible = false;
                lbDocCancel.Visible = false;

                fvDetailMat.Visible = false;
                txt_material_search.Enabled = false;
                btnSearchMat.Visible = false;

                GvSample.EditIndex = e.NewEditIndex;

                GvSample.DataSource = ViewState["vsSample"];
                GvSample.DataBind();





                //Focusset.Focus();

                break;
         
        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "GvSample":

                GridView GvSample = (GridView)docCreate.FindControl("GvSample");

                FormView fvActor1Node1 = (FormView)docCreate.FindControl("fvActor1Node1");
                Panel div_searchcreate = (Panel)fvActor1Node1.FindControl("div_searchcreate");

                TextBox txt_material_search = (TextBox)div_searchcreate.FindControl("txt_material_search");
                LinkButton btnSearchMat = (LinkButton)div_searchcreate.FindControl("btnSearchMat");

                LinkButton lbDocSave = (LinkButton)fvDeptSelect.FindControl("lbDocSave");
                LinkButton lbDocCancel = (LinkButton)fvDeptSelect.FindControl("lbDocCancel");


                TextBox txt_material_code_create_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_material_code_create_edit");
                TextBox txt_samplename_create_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_samplename_create_edit");
                TextBox txt_txt_typesample_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_txt_typesample_edit");

                DropDownList ddldate_sample1_idx_create_edit = (DropDownList)GvSample.Rows[e.RowIndex].FindControl("ddldate_sample1_idx_create_edit");
                TextBox txt_data_sample1_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_data_sample1_edit");

                DropDownList ddldate_sample2_idx_create_edit = (DropDownList)GvSample.Rows[e.RowIndex].FindControl("ddldate_sample2_idx_create_edit");
                TextBox txt_data_sample2_create_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_data_sample2_create_edit");

                TextBox txt_batch_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_batch_edit");
                TextBox txt_customer_name_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_customer_name_edit");
                TextBox txt_cabinet_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_cabinet_edit");
                TextBox txt_shift_time_edit = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_shift_time_edit");
                TextBox txt_time_edit_create = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_time_edit_create");
                TextBox txt_other_details_edit_create = (TextBox)GvSample.Rows[e.RowIndex].FindControl("txt_other_details_edit_create");

                DropDownList ddlorg_idx_production_edit_create = (DropDownList)GvSample.Rows[e.RowIndex].FindControl("ddlorg_idx_production_edit_create");
                DropDownList ddlrdept_idx_production_edit_create = (DropDownList)GvSample.Rows[e.RowIndex].FindControl("ddlrdept_idx_production_edit_create");
                DropDownList ddlrsec_idx_production_edit_create = (DropDownList)GvSample.Rows[e.RowIndex].FindControl("ddlrsec_idx_production_edit_create");

                var dataset_onUpdate = (DataSet)ViewState["vsSample"];
                var datarow_onUpdate = dataset_onUpdate.Tables[0].Rows;


                //ค่าที่จะเช็คก่อนอัพเดต บน dataset เดิม
                //ViewState["purchase_type_idx"] = ddlpurchasetype_edit.SelectedValue;
                //ViewState["equipment_idx"] = ddlequipment_edit.SelectedValue;
                //ViewState["spec_type_idx"] = ddlspec_edit.SelectedValue;

                int _numrow = dataset_onUpdate.Tables[0].Rows.Count; //จำนวนแถว
                int loop = 0;
                //กรณีมีข้อมูลมากกว่าแถวเดียว

                lbDocSave.Visible = true;
                lbDocCancel.Visible = true;


                fvDetailMat.Visible = false;
                txt_material_search.Enabled = true;
                btnSearchMat.Visible = true;


                //ทำการ update ข้อมูล
                if (txt_other_details_edit_create.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["other_details"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["other_details"] = txt_other_details_edit_create.Text;
                }

                if (txt_material_code_create_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["material_code"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["material_code"] = txt_material_code_create_edit.Text;
                }

                if (txt_samplename_create_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["samplename"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["samplename"] = txt_samplename_create_edit.Text;
                }

                if (txt_txt_typesample_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["txt_typesample"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["txt_typesample"] = txt_txt_typesample_edit.Text;
                }

                if (int.Parse(ddldate_sample1_idx_create_edit.SelectedValue) == -1)
                {
                    datarow_onUpdate[e.RowIndex]["sample1_idx_name"] = "";
                    datarow_onUpdate[e.RowIndex]["sample1_idx"] = 0;
                }
                else
                {

                    if (int.Parse(ddldate_sample1_idx_create_edit.SelectedValue) == 5)
                    {
                        datarow_onUpdate[e.RowIndex]["sample1_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                        datarow_onUpdate[e.RowIndex]["sample1_idx"] = int.Parse(ddldate_sample1_idx_create_edit.SelectedValue);
                    }
                    else
                    {
                        datarow_onUpdate[e.RowIndex]["sample1_idx_name"] = ddldate_sample1_idx_create_edit.SelectedItem.Text;
                        datarow_onUpdate[e.RowIndex]["sample1_idx"] = int.Parse(ddldate_sample1_idx_create_edit.SelectedValue);
                    }
                    //dr_Add_Sample["sample1_idx_name"] = ddldate_sample1_idx_create.SelectedItem.Text;
                    //dr_Add_Sample["sample1_idx"] = int.Parse(ddldate_sample1_idx_create.SelectedValue);

                }

                if (txt_data_sample1_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["data_sample1"] = "-"; }
                else
                {
                    datarow_onUpdate[e.RowIndex]["data_sample1"] = txt_data_sample1_edit.Text;
                }

                if (int.Parse(ddldate_sample2_idx_create_edit.SelectedValue) == -1)
                {
                    datarow_onUpdate[e.RowIndex]["sample2_idx_name"] = "";
                    datarow_onUpdate[e.RowIndex]["sample2_idx"] = 0;
                }
                else
                {

                    if (int.Parse(ddldate_sample2_idx_create_edit.SelectedValue) == 5)
                    {
                        datarow_onUpdate[e.RowIndex]["sample2_idx_name"] = "";//ddldate_sample1_idx_create.SelectedItem.Text;
                        datarow_onUpdate[e.RowIndex]["sample2_idx"] = int.Parse(ddldate_sample2_idx_create_edit.SelectedValue);
                    }
                    else
                    {
                        datarow_onUpdate[e.RowIndex]["sample2_idx"] = int.Parse(ddldate_sample2_idx_create_edit.SelectedValue);
                        datarow_onUpdate[e.RowIndex]["sample2_idx_name"] = ddldate_sample2_idx_create_edit.SelectedItem.Text;
                    }

                   

                }

                if (txt_data_sample2_create_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["data_sample2"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["data_sample2"] = txt_data_sample2_create_edit.Text;
                }

                if (txt_cabinet_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["cabinet"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["cabinet"] = txt_cabinet_edit.Text;
                }

                if (txt_batch_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["batch"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["batch"] = txt_batch_edit.Text;
                }

                if (txt_customer_name_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["customer_name"] = "-";
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["customer_name"] = txt_customer_name_edit.Text;
                }

                if (txt_shift_time_edit.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["shift_time"] = "-"; }
                else
                {
                    datarow_onUpdate[e.RowIndex]["shift_time"] = txt_shift_time_edit.Text;
                }

                if (txt_time_edit_create.Text == "")
                {
                    datarow_onUpdate[e.RowIndex]["time"] = "-";
                    //dr_Add_Sample["time"] = HiddenTimeCreate.Value.ToString();
                }

                else
                {
                    datarow_onUpdate[e.RowIndex]["time"] = txt_time_edit_create.Text;//txt_time_create.Text;
                }

                if (int.Parse(ddlorg_idx_production_edit_create.SelectedValue) == 0)
                {
                   
                    datarow_onUpdate[e.RowIndex]["ddlOrg_samplecreate"] = "";
                    datarow_onUpdate[e.RowIndex]["ddlOrg_idxsamplecreate"] = 0;
                }
                else
                {
                   
                    datarow_onUpdate[e.RowIndex]["ddlOrg_idxsamplecreate"] = int.Parse(ddlorg_idx_production_edit_create.SelectedValue);
                    datarow_onUpdate[e.RowIndex]["ddlOrg_samplecreate"] = ddlorg_idx_production_edit_create.SelectedItem.Text;

                }

                if (int.Parse(ddlrdept_idx_production_edit_create.SelectedValue) == 0)
                {
                    datarow_onUpdate[e.RowIndex]["ddlDept_samplecreate"] = "";
                    datarow_onUpdate[e.RowIndex]["ddlDept_idxsamplecreate"] = 0;
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["ddlDept_idxsamplecreate"] = int.Parse(ddlrdept_idx_production_edit_create.SelectedValue);
                    datarow_onUpdate[e.RowIndex]["ddlDept_samplecreate"] = ddlrdept_idx_production_edit_create.SelectedItem.Text;

                }

                if (int.Parse(ddlrsec_idx_production_edit_create.SelectedValue) == 0)
                {
                    datarow_onUpdate[e.RowIndex]["ddlSec_samplecreate"] = "";
                    datarow_onUpdate[e.RowIndex]["ddlSec_idxsamplecreate"] = 0;
                }
                else
                {
                    datarow_onUpdate[e.RowIndex]["ddlSec_idxsamplecreate"] = int.Parse(ddlrsec_idx_production_edit_create.SelectedValue);
                    datarow_onUpdate[e.RowIndex]["ddlSec_samplecreate"] = ddlrsec_idx_production_edit_create.SelectedItem.Text;

                }

                ViewState["vsSample"] = dataset_onUpdate;


                GvSample.EditIndex = -1;

                GvSample.DataSource = ViewState["vsSample"];
                GvSample.DataBind();


                break;

      
        }

    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "GvSample":

                GridView GvSample = (GridView)docCreate.FindControl("GvSample");

                FormView fvActor1Node1 = (FormView)docCreate.FindControl("fvActor1Node1");
                Panel div_searchcreate = (Panel)fvActor1Node1.FindControl("div_searchcreate");

                TextBox txt_material_search = (TextBox)div_searchcreate.FindControl("txt_material_search");
                LinkButton btnSearchMat = (LinkButton)div_searchcreate.FindControl("btnSearchMat");

                LinkButton lbDocSave = (LinkButton)fvDeptSelect.FindControl("lbDocSave");
                LinkButton lbDocCancel = (LinkButton)fvDeptSelect.FindControl("lbDocCancel");

                lbDocSave.Visible = true;
                lbDocCancel.Visible = true;

                fvDetailMat.Visible = false;
                txt_material_search.Enabled = true;
                btnSearchMat.Visible = true;



                GvSample.EditIndex = -1;
                GvSample.DataSource = ViewState["vsSample"];
                GvSample.DataBind();

                break;

            
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;

            }

        }

        GridView gvFile = (GridView)fvDetailResult.FindControl("gvFileResult");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFile.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFile.DataSource = null;
            gvFile.DataBind();

        }



    }

    public void SearchDirectoriesUser(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;

            }

        }

        GridView gvFileUser = (GridView)fvResultUser.FindControl("gvFileResultUser");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvFileUser.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFileUser.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFileUser.DataSource = null;
            gvFileUser.DataBind();

        }



    }

    protected void ViewDetails(object sender, EventArgs e)
    {
        LinkButton lnkView = (sender as LinkButton);
        GridViewRow row = (lnkView.NamingContainer as GridViewRow);
        string id = lnkView.CommandArgument;
        string name = row.Cells[0].Text;
        string country = (row.FindControl("txtCountry") as TextBox).Text;


        if (country != null)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แผนกนี่ได้ถูกเพิ่มไปแล้ว!!!');", true);

        }
        else
        {

        }

        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Id: " + id + " Name: " + name + " Country: " + country + "')", true);fff
    }

    protected void LinkButton_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "ApproveVacation")
        {
            //  litDebug.Text = "78999";
        }
    }


    #region reuse

    protected void initPageLoad()
    {
        #region View-Department data table
        var ds_department_data = new DataSet();
        ds_department_data.Tables.Add("DepartmentData");
        ds_department_data.Tables[0].Columns.Add("OrgIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("OrgNameEN", typeof(String));
        ds_department_data.Tables[0].Columns.Add("RDeptIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("DeptNameEN", typeof(String));
        ds_department_data.Tables[0].Columns.Add("RSecIDX", typeof(int));
        ds_department_data.Tables[0].Columns.Add("SecNameEN", typeof(String));
        ViewState["vsDepartment"] = ds_department_data;
        #endregion
        ViewState["Depart_RSecIDX"] = "";

        ///////// 
        #region View-Sample data table
        var ds_sample_data = new DataSet();
        ds_sample_data.Tables.Add("SampleData");
        ds_sample_data.Tables[0].Columns.Add("material_code", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("samplename", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("sample1_idx", typeof(int));
        ds_sample_data.Tables[0].Columns.Add("sample1_idx_name", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("data_sample1", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("sample2_idx", typeof(int));
        ds_sample_data.Tables[0].Columns.Add("sample2_idx_name", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("data_sample2", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("batch", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("cabinet", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("customer_name", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("time", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("shift_time", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("other_details", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("chk_settestidx_create", typeof(int));
        ds_sample_data.Tables[0].Columns.Add("chk_settest_create", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("chk_testidx_create", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("chk_test_create", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("txt_typesample", typeof(String));

        ds_sample_data.Tables[0].Columns.Add("ddlOrg_samplecreate", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("ddlOrg_idxsamplecreate", typeof(int));

        ds_sample_data.Tables[0].Columns.Add("ddlDept_samplecreate", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("ddlDept_idxsamplecreate", typeof(int));

        ds_sample_data.Tables[0].Columns.Add("ddlSec_samplecreate", typeof(String));
        ds_sample_data.Tables[0].Columns.Add("ddlSec_idxsamplecreate", typeof(int));



        //ds_sample_data.Tables[0].Columns.Add("certificate", typeof(int));

        ViewState["vsSample"] = ds_sample_data;
        #endregion

        ViewState["sample_material_code"] = "";

        #region View-Department data table excel
        var ds_department_dataexcel = new DataSet();
        ds_department_dataexcel.Tables.Add("DepartmentData_Excel");
        ds_department_dataexcel.Tables[0].Columns.Add("OrgIDX_Excel", typeof(int));
        ds_department_dataexcel.Tables[0].Columns.Add("OrgNameEN_Excel", typeof(String));
        ds_department_dataexcel.Tables[0].Columns.Add("RDeptIDX_Excel", typeof(int));
        ds_department_dataexcel.Tables[0].Columns.Add("DeptNameEN_Excel", typeof(String));
        ds_department_dataexcel.Tables[0].Columns.Add("RSecIDX_Excel", typeof(int));
        ds_department_dataexcel.Tables[0].Columns.Add("SecNameEN_Excel", typeof(String));
        ViewState["vsDepartment_Excel"] = ds_department_dataexcel;
        #endregion
        ViewState["Depart_RSecIDX_Excel"] = "";

        DataTableSupervisor(0);
        // DataTableExportExcel();


    }


    protected void DataTableSupervisor(int m0_labsup)
    {


        //gv supervisor
        data_qa _data_lab_resulttest = new data_qa();
        _data_lab_resulttest.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
        bind_qa_lab_result_detail _result_lab = new bind_qa_lab_result_detail();
        _result_lab.condition = 0;
        _data_lab_resulttest.bind_qa_lab_result_list[0] = _result_lab;

        _data_lab_resulttest = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resulttest);

        ViewState["vs_listResultLab"] = _data_lab_resulttest.bind_qa_lab_result_list;
        //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_lab_resulttest));

        string _header_text = String.Empty;
        int _header_idx;
        int _countrow = 0;
        DataTable table = new DataTable();
        // table.Columns.Add("Approve", typeof(String));
        table.Columns.Add("sample code", typeof(String));

        foreach (var _loop_header in _data_lab_resulttest.bind_qa_lab_result_list)
        {

            _header_idx = int.Parse(_loop_header.test_detail_idx.ToString());
            _header_text = _loop_header.test_detail_name.ToString();
            table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

            _countrow++;

        }

        data_qa _data_lab_resulttest1 = new data_qa();
        _data_lab_resulttest1.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
        bind_qa_lab_result_detail _result_lab1 = new bind_qa_lab_result_detail();

        _result_lab1.condition = 1;
        _result_lab1.m0_lab = m0_labsup;
        //_result_lab1.m0_lab = int.Parse(tbValuePlaceSup.Text);

        _data_lab_resulttest1.bind_qa_lab_result_list[0] = _result_lab1;

        _data_lab_resulttest1 = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resulttest1);

        ViewState["vs_listSampleResultLab"] = _data_lab_resulttest1.bind_qa_lab_result_list;

        table.Columns.Add("Approve", typeof(String));
        string _text = String.Empty;
        int _countrow1 = 0;

        if (_data_lab_resulttest1.bind_qa_lab_result_list == null)
        {
            GvSupervisor.Visible = false;
            GvSupervisor_scroll.Visible = false;
            EmptyData.Visible = true;

        }
        else
        {

            foreach (var _loop_rowText in _data_lab_resulttest1.bind_qa_lab_result_list)
            {
                //gen label
                Label dynamicLabel = new Label();
                dynamicLabel.Text = _loop_rowText.sample_code.ToString();
                table.Rows.Add(dynamicLabel.Text);
                _countrow1++;
            }

            GvSupervisor.Visible = true;
            GvSupervisor_scroll.Visible = true;
            EmptyData.Visible = false;


        }

        ViewState["vsTable"] = table;


        data_qa _data_lab_resultrow = new data_qa();
        _data_lab_resultrow.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
        bind_qa_lab_result_detail _result_row = new bind_qa_lab_result_detail();

        _result_row.condition = 3;
        _result_row.m0_lab = int.Parse(tbValuePlaceSup.Text);

        _data_lab_resultrow.bind_qa_lab_result_list[0] = _result_row;
        _data_lab_resultrow = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resultrow);

        ViewState["vs_listResultLab_result"] = _data_lab_resultrow.bind_qa_lab_result_list;

    }

    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("docDetail", 0, 0, 0, "0");

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void clearDataSetOnClickCheckBoxSampleList()
    {
        //clear set sample list
        ViewState["vsSample"] = null;
        ViewState["sample_material_code"] = ViewState["vsSample"];
        GvSample.DataSource = ViewState["sample_material_code"];
        GvSample.DataBind();
        GvSample.Visible = false;

        divGvSample_scroll.Visible = false;

        //clear set depart view
        setFormData(fvDeptSelect, FormViewMode.Insert, null);
        GridView GvDeptCreate = (GridView)fvDeptSelect.FindControl("GvDeptCreate");

        ViewState["vsDepartment"] = null;
        ViewState["Depart_RSecIDX"] = ViewState["vsDepartment"];
        GvDeptCreate.DataSource = ViewState["Depart_RSecIDX"];
        GvDeptCreate.DataBind();
        GvDeptCreate.Visible = false;
        setFormData(fvDeptSelect, FormViewMode.ReadOnly, null);

        initPageLoad();

    }

    protected void clearDataSetOnClickCheckBox()
    {
        Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");
        Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
        FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
        LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
        LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
        GridView GvDeptExcel = (GridView)Dept_Excel.FindControl("GvDeptExcel");
        GridView GvExcel_Show = (GridView)GvExcel_Import.FindControl("GvExcel_Show");

        ViewState["vsDepartment_Excel"] = null;
        ViewState["Depart_RSecIDX_Excel"] = ViewState["vsDepartment_Excel"];

        GvDeptExcel.DataSource = ViewState["Depart_RSecIDX_Excel"];
        GvDeptExcel.DataBind();

        GvExcel_Show.DataSource = ViewState["Depart_RSecIDX_Excel"];
        GvDeptExcel.DataBind();
        //litDebug.Text = "TClear";

        initPageLoad();
    }

    protected void clearDataSet()
    {
        //clear set sample list
        ViewState["sample_material_code"] = null;
        ViewState["Depart_RSecIDX"] = null;
        GvSample.DataSource = ViewState["sample_material_code"];
        GvSample.DataBind();

        //divGvSample_scroll.Visible = true;

        //clear set depart view
        setFormData(fvDeptSelect, FormViewMode.Insert, null);
        GridView GvDeptCreate = (GridView)fvDeptSelect.FindControl("GvDeptCreate");
        GvDeptCreate.DataSource = ViewState["Depart_RSecIDX"];
        GvDeptCreate.DataBind();
        setFormData(fvDeptSelect, FormViewMode.ReadOnly, null);

        //clear set depart view
        //setFormData(FvAddImport, FormViewMode.Insert, null);
        ViewState["Depart_RSecIDX_Excel"] = null;

        Panel Save_Excel = (Panel)FvAddImport.FindControl("Save_Excel");
        Panel GvExcel_Import = (Panel)FvAddImport.FindControl("GvExcel_Import");
        Panel Dept_Excel = (Panel)FvAddImport.FindControl("Dept_Excel");
        FileUpload upload = (FileUpload)FvAddImport.FindControl("upload");
        LinkButton btnImport = (LinkButton)FvAddImport.FindControl("btnImport");
        LinkButton lbDocCancelExcel = (LinkButton)FvAddImport.FindControl("lbDocCancelExcel");
        GridView GvDeptExcel = (GridView)Dept_Excel.FindControl("GvDeptExcel");
        GridView GvExcel_Show = (GridView)GvExcel_Import.FindControl("GvExcel_Show");

        upload.Enabled = false;
        Dept_Excel.Visible = false;
        GvExcel_Import.Visible = false;
        Save_Excel.Visible = false;
        btnImport.Visible = false;
        lbDocCancelExcel.Visible = false;

        GvDeptExcel.DataSource = ViewState["Depart_RSecIDX_Excel"];
        GvDeptExcel.DataBind();

        GvExcel_Show.DataSource = ViewState["Depart_RSecIDX_Excel"];
        GvExcel_Show.DataBind();

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, string m0_lab)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //// clear other formview, repeater except fvEmpDetail, fvActor1Node1
        //setRepeaterData(rptProcessDetail, null);

        //set search index
        txt_document_nosearch.Text = string.Empty;
        txt_sample_codesearch.Text = string.Empty;
        txt_mat_search.Text = string.Empty;
        txt_date_search.Text = string.Empty;
        txt_too_search.Text = string.Empty;
        txt_customer_search.Text = string.Empty;
        txt_shift_search.Text = string.Empty;
        txt_time_search.Text = string.Empty;
        HiddenDateSearch.Value = string.Empty;
        HiddenTimeSearch.Value = string.Empty;


        //div_water_import.Visible = false;
        div_create.Visible = false;
        setFormData(fvDetailMat, FormViewMode.ReadOnly, null);
        setFormData(fvDetailSearchCreate, FormViewMode.ReadOnly, null);
        setFormData(fvDetail, FormViewMode.ReadOnly, null);
        setFormData(fvSampleTestDetail, FormViewMode.ReadOnly, null);
        setRepeaterData(rptHistoryDocument, null);
        div_other_detail.Visible = false;
        setFormData(fvActor2_Receive, FormViewMode.ReadOnly, null);

        // setFormData(FvAddImport, FormViewMode.ReadOnly, null);

        setFormData(fvSampleCode, FormViewMode.ReadOnly, null);
        setFormData(fvDetailSampleCode, FormViewMode.ReadOnly, null);
        setRepeaterData(rptHistorySampleCode, null);

        div_showhistory.Visible = false;
        rptHistoryDocument.Visible = false;

        btnHistoryBack.Visible = false;

        //div_other_detail.Visible = false;
      
        GvSample.Visible = false;
        GvSample.EditIndex = -1;
        divGvSample_scroll.Visible = false;

        showsearch.Visible = false;
        fvBacktoIndex.Visible = false;
        btnSearchIndex.Visible = false;
        btnResetSearchPage.Visible = false;

        //Lab
        gvLab.Visible = false;
        setFormData(fvDetailLab, FormViewMode.ReadOnly, null);
        setFormData(fvActor3_Receive, FormViewMode.ReadOnly, null);
        setFormData(fvEmpDetailLab, FormViewMode.ReadOnly, null);
        setRepeaterData(rptHistorySampleInLab, null);
        show_search_lab.Visible = false;
        setFormData(fvDetailResult, FormViewMode.ReadOnly, null);

        //user
        setFormData(fvActor1_Approve, FormViewMode.ReadOnly, null);
        setFormData(fvResultUser, FormViewMode.ReadOnly, null);
        setFormData(fvUserShowDetailEditDocument, FormViewMode.ReadOnly, null);
        setFormData(fvUserEditDocument, FormViewMode.ReadOnly, null);

        //admin
        setFormData(fvActor2_Approve, FormViewMode.ReadOnly, null);


        //docPrint
        divShowToolPrint.Visible = false;
        setGridData(gvSampleCodeSearchPrint, null);
        tbSearchDateRecive.Text = string.Empty;
        HiddenSearchDateRecive.Value = string.Empty;
        divAlertSearchnoneDate.Visible = false;

        //gvsup
        //tbValuePlaceSup.Text = "0";
        //tbValuePlaceNameSup.Text = "";



        //  ddlSearchPlace.SelectedValue = "0";
        //setFormData(fvActor4, FormViewMode.ReadOnly, null);
        // clear other formview, repeater except fvEmpDetail, fvActor1Node1
        switch (activeTab)
        {
            case "docCreate":

                tbValuePlaceSup.Text = "0";
                tbValuePlaceNameSup.Text = "";


                // fvEmpDetail
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                if (uidx == 0)
                {

                    // fvActor1Node1
                    //linkBtnTrigger(lbDocSaveImport);
                    div_create.Visible = true;
                    setFormData(fvActor1Node1, FormViewMode.Insert, null);
                    clearDataSet();
                    getTestDetail_chk();
                    //linkBtnTrigger(lbDocSaveImport);
                    getSetTestDetail_chk();
                    //getPlaceList((DropDownList)fvActor1Node1.FindControl("ddlPlace"));
                    getPlaceCreateList((DropDownList)fvActor1Node1.FindControl("ddlPlace"));
                    initPageLoad();

                }
                else if (uidx > 0)
                {

                    if (u1idx == 0)
                    {
                        //DEtail Emp Create
                        data_qa _data_qadetail = new data_qa();
                        _data_qadetail.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                        qa_lab_u0doc_qalab _u0_doc_detail = new qa_lab_u0doc_qalab();

                        _u0_doc_detail.u0_qalab_idx = uidx;

                        _data_qadetail.qa_lab_u0doc_qalab_list[0] = _u0_doc_detail;

                        _data_qadetail = callServicePostQA(_urlQaGetDetailU0Document, _data_qadetail);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                        setFormData(fvDetail, FormViewMode.ReadOnly, _data_qadetail.qa_lab_u0doc_qalab_list);

                        // Bind repeater Test detail
                        Repeater rp_testdetail = (Repeater)fvDetail.FindControl("rp_testdetail");
                        //ViewState["test_detail_view"]
                        data_qa _data_test_v = new data_qa();
                        _data_test_v.qa_lab_vtestdetail_list = new qa_lab_vtestdetail[1];
                        qa_lab_vtestdetail _testv_detail = new qa_lab_vtestdetail();

                        _testv_detail.u0_qalab_idx = uidx;//int.Parse(ViewState["test_detail_view"].ToString());

                        _data_test_v.qa_lab_vtestdetail_list[0] = _testv_detail;
                        _data_test_v = callServicePostQA(_urlQaGetTestDetailU0Document, _data_test_v);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                        // setRepeaterData(rp_testdetail, _data_test_v.qa_lab_vtestdetail_list);

                        //Sample Detail Tested
                        data_qa _data_sample_de = new data_qa();
                        _data_sample_de.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                        qa_lab_u1_qalab _u1_doc_sampleeail = new qa_lab_u1_qalab();

                        _u1_doc_sampleeail.u0_qalab_idx = uidx;

                        _data_sample_de.qa_lab_u1_qalab_list[0] = _u1_doc_sampleeail;

                        _data_sample_de = callServicePostQA(_urlQaGetSampleView, _data_sample_de);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sample_de));
                        ViewState["vsSampleTestedItems"] = _data_sample_de.qa_lab_u1_qalab_list;
                        setFormData(fvSampleTestDetail, FormViewMode.ReadOnly, _data_sample_de.qa_lab_u1_qalab_list);

                        GridView gvSampleTestDetal = (GridView)fvSampleTestDetail.FindControl("gvSampleTestDetal");
                        setGridData(gvSampleTestDetal, _data_sample_de.qa_lab_u1_qalab_list);


                        //History In Doument
                        data_qa _data_history_doc = new data_qa();
                        _data_history_doc.qa_m0_history_doc_list = new qa_m0_history_doc_detail[1];
                        qa_m0_history_doc_detail _u0_doc_historyl = new qa_m0_history_doc_detail();

                        _u0_doc_historyl.u0_qalab_idx = uidx;

                        _data_history_doc.qa_m0_history_doc_list[0] = _u0_doc_historyl;

                        _data_history_doc = callServicePostQA(_urlQaGetHistoryDoc, _data_history_doc);

                        //log detail document
                        div_showhistory.Visible = true;
                        btnHistoryDocument.Visible = true;
                        setRepeaterData(rptHistoryDocument, _data_history_doc.qa_m0_history_doc_list);

                        clearDataSet();

                        int status = _data_qadetail.qa_lab_u0doc_qalab_list[0].staidx;

                        // switch action by node
                        switch (status)
                        {
                            case 1:
                            case 2:
                            case 5:
                                break;
                            case 3:

                                var _cemp_idx = (TextBox)fvDetail.FindControl("tbActorCempIDX");
                                var _cemp_rsec_idx = (TextBox)fvDetail.FindControl("txtcemp_rsec_idx");

                                if (_emp_idx == int.Parse(_cemp_idx.Text) || ViewState["rsec_permission"].ToString() == _cemp_rsec_idx.Text)
                                {
                                    setFormData(fvSampleTestDetail, FormViewMode.ReadOnly, null);

                                    data_qa _data_sample_deEdit = new data_qa();
                                    _data_sample_deEdit.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                    qa_lab_u1_qalab _data_Edit = new qa_lab_u1_qalab();
                                    _data_Edit.u0_qalab_idx = uidx;
                                    _data_sample_deEdit.qa_lab_u1_qalab_list[0] = _data_Edit;
                                    _data_sample_deEdit = callServicePostQA(_urlQaGetSampleView, _data_sample_deEdit);
                                    setFormData(fvUserShowDetailEditDocument, FormViewMode.ReadOnly, _data_sample_deEdit.qa_lab_u1_qalab_list);
                                    ViewState["vsDocumentEdit"] = _data_sample_deEdit.qa_lab_u1_qalab_list;
                                    GridView gvDocumentEditList = (GridView)fvUserShowDetailEditDocument.FindControl("gvDocumentEditList");
                                    setGridData(gvDocumentEditList, ViewState["vsDocumentEdit"]);
                                    setFormData(fvUserEditDocument, FormViewMode.ReadOnly, null);
                                    

                                }

                                break;
                            case 4:

                                if (ViewState["rdept_permission"].ToString() == "27")
                                {

                                    //litDebug.Text = "9999999";
                                    setFormData(fvActor2_Receive, FormViewMode.Insert, null);
                                    DropDownList ddlActor2Approve = (DropDownList)fvActor2_Receive.FindControl("ddlActor2Approve");
                                    getDecision2Receive(ddlActor2Approve);
                                    break;
                                }
                                else
                                {
                                    //litDebug.Text = "9991111";
                                }
                                break;

                        }
                    }
                    else if (u1idx > 0)
                    {
                        switch (staidx)
                        {
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 13:
                            case 14:
                            case 15:
                            case 16:


                                //detail sample in user and admin
                                data_qa _data_sample_vuser = new data_qa();
                                _data_sample_vuser.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_sample_vuser = new qa_lab_u1_qalab();

                                _u1_sample_vuser.u1_qalab_idx = u1idx;

                                _data_sample_vuser.qa_lab_u1_qalab_list[0] = _u1_sample_vuser;

                                _data_sample_vuser = callServicePostQA(_urlQaGetSampleInLab, _data_sample_vuser);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                                setFormData(fvSampleCode, FormViewMode.ReadOnly, _data_sample_vuser.qa_lab_u1_qalab_list);


                                // Bind repeater Test detail view
                                Repeater rp_testdetail_lab_view = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");
                                //ViewState["test_detail_view"]
                                data_qa _data_test_vuser = new data_qa();

                                _data_test_vuser.bind_qa_m0_test_detail_list = new bindqa_m0_test_detail[1];
                                bindqa_m0_test_detail _testv_detail_user = new bindqa_m0_test_detail();

                                _testv_detail_user.u1_qalab_idx = u1idx;//int.Parse(ViewState["test_detail_view"].ToString());

                                _data_test_vuser.bind_qa_m0_test_detail_list[0] = _testv_detail_user;
                                _data_test_vuser = callServicePostQA(_urlQaGetTestSample, _data_test_vuser);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                                setRepeaterData(rp_testdetail_lab_view, _data_test_vuser.bind_qa_m0_test_detail_list);


                                //detail sample 
                                data_qa _data_sample_vuserdetail = new data_qa();
                                _data_sample_vuserdetail.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_sample_vuserdetail = new qa_lab_u1_qalab();

                                _u1_sample_vuserdetail.u1_qalab_idx = u1idx;
                                _u1_sample_vuserdetail.condition = 1;

                                _data_sample_vuserdetail.qa_lab_u1_qalab_list[0] = _u1_sample_vuserdetail;

                                _data_sample_vuserdetail = callServicePostQA(_urlQaGetDetailForUpdateDocument, _data_sample_vuserdetail);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sample_vuserdetail));
                                setFormData(fvDetailSampleCode, FormViewMode.ReadOnly, _data_sample_vuserdetail.qa_lab_u1_qalab_list);

                                var _IDXDateSample1View = (TextBox)fvDetailSampleCode.FindControl("tbSample1IDX_view");
                                var _IDXDateSample2View = (TextBox)fvDetailSampleCode.FindControl("tbSample2IDX_view");
                                var _SelectIDXDateSample1 = (DropDownList)fvDetailSampleCode.FindControl("ddldate_sample1_idx_view");
                                getDateSampleListEdit((DropDownList)fvDetailSampleCode.FindControl("ddldate_sample1_idx_view"), _IDXDateSample1View.Text);
                                getDateSampleList2Edit((DropDownList)fvDetailSampleCode.FindControl("ddldate_sample2_idx_view"), int.Parse(_SelectIDXDateSample1.SelectedValue), _IDXDateSample2View.Text);

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sample_vuserdetail));

                                //Detail Result
                                data_qa _data_sample_vuser10 = new data_qa();
                                _data_sample_vuser10.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_sample_vuser10 = new qa_lab_u1_qalab();

                                _u1_sample_vuser10.u1_qalab_idx = u1idx;

                                _data_sample_vuser10.qa_lab_u1_qalab_list[0] = _u1_sample_vuser10;

                                _data_sample_vuser10 = callServicePostQA(_urlQaGetDetailResult, _data_sample_vuser10);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                                setFormData(fvResultUser, FormViewMode.ReadOnly, _data_sample_vuser10.qa_lab_u1_qalab_list);

                                if (_data_sample_vuser10.return_code == 0)
                                {
                                    TextBox _samplecode = (TextBox)fvSampleCode.FindControl("tbsample_code");
                                    TextBox _m0labidx = (TextBox)fvSampleCode.FindControl("tbm0labidx");

                                    GridView GvResultDetailUser = (GridView)fvResultUser.FindControl("GvResultDetailUser");
                                    setGridData(GvResultDetailUser, _data_sample_vuser10.qa_lab_u1_qalab_list);

                                    if (int.Parse(_m0labidx.Text) == int.Parse(ExternalM0LabIdx.ToString()) && staidx == 10)
                                    {
                                        try
                                        {
                                            string getPath = ConfigurationManager.AppSettings["path_flie_external_lab"];

                                            string filePath = Server.MapPath(getPath + _samplecode.Text);
                                            DirectoryInfo myDirfile = new DirectoryInfo(filePath);
                                            SearchDirectoriesUser(myDirfile, _samplecode.Text);


                                        }
                                        catch
                                        {

                                        }

                                    }
                                    else
                                    {

                                    }
                                }
                                else
                                {

                                }

                                //////History In Sample Code
                                data_qa _data_history_sample = new data_qa();
                                _data_history_sample.qa_m0_history_sample_list = new qa_m0_history_sample_detail[1];
                                qa_m0_history_sample_detail _u0_sample_historyl = new qa_m0_history_sample_detail();

                                _u0_sample_historyl.u1_qalab_idx = u1idx;

                                _data_history_sample.qa_m0_history_sample_list[0] = _u0_sample_historyl;

                                _data_history_sample = callServicePostQA(_urlQaGetHistorySample, _data_history_sample);
                                setRepeaterData(rptHistorySampleCode, _data_history_sample.qa_m0_history_sample_list);
                                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_sample));

                                break;
                            case 11:
                                //detail sample in user appove and not appove
                                data_qa _data_vuser_approve11 = new data_qa();
                                _data_vuser_approve11.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_vuser_appove11 = new qa_lab_u1_qalab();

                                _u1_vuser_appove11.u1_qalab_idx = u1idx;

                                _data_vuser_approve11.qa_lab_u1_qalab_list[0] = _u1_vuser_appove11;

                                _data_vuser_approve11 = callServicePostQA(_urlQaGetSampleInLab, _data_vuser_approve11);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                                setFormData(fvSampleCode, FormViewMode.ReadOnly, _data_vuser_approve11.qa_lab_u1_qalab_list);

                                // Bind repeater Test detail view
                                Repeater rp_testdetail_lab_view11 = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");
                                //ViewState["test_detail_view"]
                                data_qa _data_test_vuser11 = new data_qa();

                                _data_test_vuser11.qa_lab_vtestdetail_list = new qa_lab_vtestdetail[1];
                                qa_lab_vtestdetail _testv_detail_user11 = new qa_lab_vtestdetail();

                                _testv_detail_user11.u1_qalab_idx = u1idx;//int.Parse(ViewState["test_detail_view"].ToString());

                                _data_test_vuser11.qa_lab_vtestdetail_list[0] = _testv_detail_user11;
                                _data_test_vuser11 = callServicePostQA(_urlQaGetTestDetailU0Document, _data_test_vuser11);


                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                                setRepeaterData(rp_testdetail_lab_view11, _data_test_vuser11.qa_lab_vtestdetail_list);

                                //litDebug.Text = _data_test_vuser11.qa_lab_vtestdetail_list[0].cemp_idx.ToString();
                                if (_emp_idx == _data_test_vuser11.qa_lab_vtestdetail_list[0].cemp_idx)
                                {
                                    //Dicision Node7 approve and No Approve
                                    setFormData(fvActor1_Approve, FormViewMode.Insert, null);
                                    DropDownList ddlActor1Approve = (DropDownList)fvActor1_Approve.FindControl("ddlActor1Approve");
                                    //getCPM0NodeDecisionList(ddlActor2Approve, node_idx);
                                    getDecision7Receive(ddlActor1Approve);
                                }

                                //detail sample 
                                data_qa _data_sample_Viewdetail = new data_qa();
                                _data_sample_Viewdetail.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_sample_Viewdetail = new qa_lab_u1_qalab();

                                _u1_sample_Viewdetail.u1_qalab_idx = u1idx;
                                _u1_sample_Viewdetail.condition = 1;

                                _data_sample_Viewdetail.qa_lab_u1_qalab_list[0] = _u1_sample_Viewdetail;

                                _data_sample_Viewdetail = callServicePostQA(_urlQaGetDetailForUpdateDocument, _data_sample_Viewdetail);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sample_vuserdetail));
                                setFormData(fvDetailSampleCode, FormViewMode.ReadOnly, _data_sample_Viewdetail.qa_lab_u1_qalab_list);

                                var _IDXDateSample1View1 = (TextBox)fvDetailSampleCode.FindControl("tbSample1IDX_view");
                                var _IDXDateSample2View1 = (TextBox)fvDetailSampleCode.FindControl("tbSample2IDX_view");
                                var _SelectIDXDateSample11 = (DropDownList)fvDetailSampleCode.FindControl("ddldate_sample1_idx_view");
                                getDateSampleListEdit((DropDownList)fvDetailSampleCode.FindControl("ddldate_sample1_idx_view"), _IDXDateSample1View1.Text);
                                getDateSampleList2Edit((DropDownList)fvDetailSampleCode.FindControl("ddldate_sample2_idx_view"), int.Parse(_SelectIDXDateSample11.SelectedValue), _IDXDateSample2View1.Text);


                                //History In Sample Code
                                data_qa _data_history_sample11 = new data_qa();
                                _data_history_sample11.qa_m0_history_sample_list = new qa_m0_history_sample_detail[1];
                                qa_m0_history_sample_detail _u0_sample_historyl11 = new qa_m0_history_sample_detail();

                                _u0_sample_historyl11.u1_qalab_idx = u1idx;

                                _data_history_sample11.qa_m0_history_sample_list[0] = _u0_sample_historyl11;

                                _data_history_sample11 = callServicePostQA(_urlQaGetHistorySample, _data_history_sample11);
                                setRepeaterData(rptHistorySampleCode, _data_history_sample11.qa_m0_history_sample_list);
                                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_sample));

                                break;
                            case 12:
                                //detail sample in user appove and not appove
                                data_qa _data_vuser_approve8 = new data_qa();
                                _data_vuser_approve8.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                                qa_lab_u1_qalab _u1_vuser_appove8 = new qa_lab_u1_qalab();

                                _u1_vuser_appove8.u1_qalab_idx = u1idx;

                                _data_vuser_approve8.qa_lab_u1_qalab_list[0] = _u1_vuser_appove8;

                                _data_vuser_approve8 = callServicePostQA(_urlQaGetSampleInLab, _data_vuser_approve8);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                                setFormData(fvSampleCode, FormViewMode.ReadOnly, _data_vuser_approve8.qa_lab_u1_qalab_list);

                                // Bind repeater Test detail view
                                Repeater rp_testdetail_lab_view8 = (Repeater)fvSampleCode.FindControl("rp_testdetail_lab_view");
                                //ViewState["test_detail_view"]
                                data_qa _data_test_vuser8 = new data_qa();

                                _data_test_vuser8.qa_lab_vtestdetail_list = new qa_lab_vtestdetail[1];
                                qa_lab_vtestdetail _testv_detail_user8 = new qa_lab_vtestdetail();

                                _testv_detail_user8.u1_qalab_idx = u1idx;//int.Parse(ViewState["test_detail_view"].ToString());

                                _data_test_vuser8.qa_lab_vtestdetail_list[0] = _testv_detail_user8;
                                _data_test_vuser8 = callServicePostQA(_urlQaGetTestDetailU0Document, _data_test_vuser8);
                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                                setRepeaterData(rp_testdetail_lab_view8, _data_test_vuser8.qa_lab_vtestdetail_list);

                                if (ViewState["rdept_permission"].ToString() == "27")
                                {
                                    //Dicision Node8 approve to external lab
                                    setFormData(fvActor2_Approve, FormViewMode.Insert, null);
                                    DropDownList ddlActor2ApproveN8 = (DropDownList)fvActor2_Approve.FindControl("ddlActor2ApproveN8");
                                    //getCPM0NodeDecisionList(ddlActor2Approve, node_idx);
                                    getDecision8Approve(ddlActor2ApproveN8);
                                }

                                //History In Sample Code
                                data_qa _data_history_sample8 = new data_qa();
                                _data_history_sample8.qa_m0_history_sample_list = new qa_m0_history_sample_detail[1];
                                qa_m0_history_sample_detail _u0_sample_historyl8 = new qa_m0_history_sample_detail();

                                _u0_sample_historyl8.u1_qalab_idx = u1idx;

                                _data_history_sample8.qa_m0_history_sample_list[0] = _u0_sample_historyl8;

                                _data_history_sample8 = callServicePostQA(_urlQaGetHistorySample, _data_history_sample8);
                                setRepeaterData(rptHistorySampleCode, _data_history_sample8.qa_m0_history_sample_list);
                                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_sample));

                                break;
                        }
                    }

                }

                break;
            case "docDetail":
                tbValuePlaceSup.Text = "0";
                tbValuePlaceNameSup.Text = "";

                // fvEmpDetail
                lbl_detail_placeindex.Visible = false;
                btnSearchIndex.Visible = true;
                btnResetSearchPage.Visible = true;
                if (uidx == 0)
                {
                    //  ViewState["DocumentPlaceLab"] = 0;
                    gvDoingList.PageIndex = 0;
                    if (m0_lab == "0")
                    {
                        tbValuePlace.Text = "0";
                        tb_PlaceName.Text = "";
                        _data_qa.qa_m0_place_list = new qa_m0_place_detail[1];
                        qa_m0_place_detail _placeList = new qa_m0_place_detail();
                        _placeList.condition = 2;
                        _data_qa.qa_m0_place_list[0] = _placeList;

                        _data_qa = callServicePostMasterQA(_urlQaGetplace, _data_qa);
                        setRepeaterData(rptBindPlaceForSelectDoc, _data_qa.qa_m0_place_list);
                        // rptBindPlaceForSelectDoc

                        data_qa _data_qaindex = new data_qa();
                        _data_qaindex.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                        qa_lab_u0doc_qalab _u0_doc_index = new qa_lab_u0doc_qalab();
                        _u0_doc_index.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        _u0_doc_index.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        _u0_doc_index.cemp_idx = _emp_idx;
                        _data_qaindex.qa_lab_u0doc_qalab_list[0] = _u0_doc_index;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qaindex));
                        _data_qaindex = callServicePostQA(_urlQaGetViewRsecReference, _data_qaindex);

                        ViewState["vs_gvDetail"] = _data_qaindex.qa_lab_u0doc_qalab_list;
                        setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                    }
                    else
                    {
                        //litDebug.Text = "2222";
                        // int SETSHOWDOCUMENTPLACE = int.Parse(cmdArg);
                        //ViewState["DocumentPlaceLab"] = SETSHOWDOCUMENTPLACE;
                        //tbValuePlace.Text = (SETSHOWDOCUMENTPLACE.ToString());
                        data_qa _data_qaindex = new data_qa();
                        _data_qaindex.qa_lab_u0doc_qalab_list = new qa_lab_u0doc_qalab[1];
                        qa_lab_u0doc_qalab _u0_doc_index = new qa_lab_u0doc_qalab();
                        _u0_doc_index.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        _u0_doc_index.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        _u0_doc_index.cemp_idx = _emp_idx;
                        _u0_doc_index.place_idx = int.Parse(tbValuePlace.Text);

                        _data_qaindex.qa_lab_u0doc_qalab_list[0] = _u0_doc_index;
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qaindex));
                        _data_qaindex = callServicePostQA(_urlQaGetViewRsecReference, _data_qaindex);

                        if (_data_qaindex.return_code == 0)
                        {

                            var linqDocumentPlace = from _dtdocumentplace in _data_qaindex.qa_lab_u0doc_qalab_list
                                                    where _dtdocumentplace.place_idx == int.Parse(tbValuePlace.Text)
                                                    select _dtdocumentplace;

                            ViewState["vs_gvDetail"] = linqDocumentPlace.ToList();

                            //var t1 = linqDocumentPlace.ToArray();

                            foreach (var c in linqDocumentPlace)
                            {

                                lbl_detail_placeindex.Visible = true;
                                lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + c.place_name;

                            }

                        }
                        else
                        {
                            ViewState["vs_gvDetail"] = null;
                            lbl_detail_placeindex.Visible = true;
                            lbl_detail_placeindex.Text = "รายการตรวจของสถานที่ : " + tb_PlaceName.Text;
                        }

                        setGridData(gvDoingList, ViewState["vs_gvDetail"]);

                    }

                }
                else if (uidx > 0)
                {

                }
                break;
            case "docLab":

                tbValuePlaceSup.Text = "0";
                tbValuePlaceNameSup.Text = "";


                lbdetailsShow.Visible = false;
                if (u1idx == 0)
                {
                    //litDebug.Text = ViewState["vsArgPlaceLab"].ToString();

                    if (m0_lab == "0")
                    {
                        ViewState["vsArgPlaceLab"] = 0;


                        //rp_place
                        data_qa _dataSelectLab = new data_qa();
                        _dataSelectLab.qa_m0_lab_list = new qa_m0_lab_detail[1];
                        qa_m0_lab_detail _m0LabSelectList = new qa_m0_lab_detail();

                        _dataSelectLab.qa_m0_lab_list[0] = _m0LabSelectList;

                        _dataSelectLab = callServicePostMasterQA(_urlQaGetLabTypeOnSelectNode3, _dataSelectLab);
                        setRepeaterData(rp_place, _dataSelectLab.qa_m0_lab_list);

                        //Sample Detail Tested
                        data_qa _data_lab = new data_qa();
                        _data_lab.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                        qa_lab_u1_qalab _u1_doc_lab = new qa_lab_u1_qalab();

                        _data_lab.qa_lab_u1_qalab_list[0] = _u1_doc_lab;

                        _data_lab = callServicePostQA(_urlQaGetDocActor4, _data_lab);

                        show_search_lab.Visible = true;
                        gvLab.Visible = true;
                        gvLab.PageIndex = 0;
                        ViewState["vs_gvLab_Detail"] = _data_lab.qa_lab_u1_qalab_list;

                        setGridData(gvLab, ViewState["vs_gvLab_Detail"]);

                        //litDebug.Text = m0_lab;//ViewState["vsArgPlaceLab"].ToString();
                    }
                    else
                    {
                        data_qa _dataApprovePlaceN4 = new data_qa();
                        _dataApprovePlaceN4.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                        qa_lab_u1_qalab _u1_doc_lab_placen4 = new qa_lab_u1_qalab();
                        _u1_doc_lab_placen4.m0_lab_idx = int.Parse(m0_lab);//int.Parse(ViewState["vsArgPlaceLab"].ToString());
                        _dataApprovePlaceN4.qa_lab_u1_qalab_list[0] = _u1_doc_lab_placen4;

                        _dataApprovePlaceN4 = callServicePostQA(_urlQaGetSelectPlaceLab, _dataApprovePlaceN4);

                        if (_dataApprovePlaceN4.return_code == 0)
                        {
                            lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4.qa_lab_u1_qalab_list[0].lab_name.ToString();

                            if (_dataApprovePlaceN4.qa_lab_u1_qalab_list[0].lab_name.ToString() != null)
                            {
                                lbdetailsShow.Visible = true;

                                ViewState["vs_gvLab_Detail"] = _dataApprovePlaceN4.qa_lab_u1_qalab_list;
                            }
                            else
                            {

                                ViewState["vs_gvLab_Detail"] = null;

                            }
                        }
                        else
                        {
                            ViewState["vs_gvLab_Detail"] = null;
                            lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4.return_msg.ToString();
                            lbdetailsShow.Visible = true;
                            //   lbdetailsShow.Text = "รายการตรวจของสถานที่ : " + _dataApprovePlaceN4.qa_lab_u1_qalab_list[0].lab_name.ToString();
                        }



                        show_search_lab.Visible = true;
                        gvLab.Visible = true;
                        gvLab.PageIndex = 0;



                        setGridData(gvLab, ViewState["vs_gvLab_Detail"]);
                        setOntop.Focus();
                    }

                }
                else if (u1idx > 0)
                {
                    setFormData(fvEmpDetailLab, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                    //ViewState["vsArgPlaceLab"] = m0_lab;

                    if (ViewState["vsArgPlaceLab"].ToString() == "0")
                    {
                        m0_lab = "0";
                    }
                    else
                    {
                        m0_lab = ViewState["vsArgPlaceLab"].ToString();
                        //litDebug.Text = m0_lab;
                    }

                    //litDebug.Text = ViewState["vsArgPlaceLab"].ToString();
                    switch (staidx)
                    {
                        case 6: // Reject and Reseve 

                            //detail sample in lab
                            data_qa _data_sample_vlab = new data_qa();
                            _data_sample_vlab.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                            qa_lab_u1_qalab _u1_sample_vlab = new qa_lab_u1_qalab();

                            _u1_sample_vlab.u1_qalab_idx = u1idx;

                            _data_sample_vlab.qa_lab_u1_qalab_list[0] = _u1_sample_vlab;

                            _data_sample_vlab = callServicePostQA(_urlQaGetSampleInLab, _data_sample_vlab);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                            setFormData(fvDetailLab, FormViewMode.ReadOnly, _data_sample_vlab.qa_lab_u1_qalab_list);

                            TextBox check_m0_lab = (TextBox)fvDetailLab.FindControl("check_m0_lab");
                            if (m0_lab != "0")
                            {
                                check_m0_lab.Text = m0_lab;
                            }
                            else
                            {
                                check_m0_lab.Text = "0";
                            }


                            // Bind repeater Test detail
                            Repeater rp_testdetail_lab = (Repeater)fvDetailLab.FindControl("rp_testdetail_lab");
                            //ViewState["test_detail_view"]
                            data_qa _data_test_vlab = new data_qa();
                            _data_test_vlab.bindlab_qa_m0_testdetail_list = new bindlab_m0_test_detail[1];
                            bindlab_m0_test_detail _testvlab_detail = new bindlab_m0_test_detail();

                            _testvlab_detail.u1_qalab_idx = u1idx;//int.Parse(ViewState["test_detail_view"].ToString());

                            _data_test_vlab.bindlab_qa_m0_testdetail_list[0] = _testvlab_detail;
                            _data_test_vlab = callServicePostQA(_urlQaGetTestDetailLab, _data_test_vlab);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                            setRepeaterData(rp_testdetail_lab, _data_test_vlab.bindlab_qa_m0_testdetail_list);

                            //if (ViewState["rdept_permission"].ToString() == "27")
                            //{
                            //    //detail fo ddl node4
                            //    setFormData(fvActor3_Receive, FormViewMode.Insert, null);
                            //    DropDownList ddlActor3Approve = (DropDownList)fvActor3_Receive.FindControl("ddlActor3Approve");
                            //    //getCPM0NodeDecisionList(ddlActor2Approve, node_idx);
                            //    getDecision4Receive(ddlActor3Approve);
                            //}

                            //History Lab In Sample
                            data_qa _data_history_splab = new data_qa();
                            _data_history_splab.qa_m0_history_sample_list = new qa_m0_history_sample_detail[1];
                            qa_m0_history_sample_detail _u0_doc_history_splab = new qa_m0_history_sample_detail();

                            _u0_doc_history_splab.u1_qalab_idx = u1idx;

                            _data_history_splab.qa_m0_history_sample_list[0] = _u0_doc_history_splab;

                            _data_history_splab = callServicePostQA(_urlQaGetHistorySample, _data_history_splab);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_doc));
                            setRepeaterData(rptHistorySampleInLab, _data_history_splab.qa_m0_history_sample_list);

                            break;

                        case 7:
                        case 8:
                        case 10:
                        case 13:

                            //detail sample in lab 7
                            data_qa _data_sample_vlab7 = new data_qa();
                            _data_sample_vlab7.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                            qa_lab_u1_qalab _u1_sample_vlab7 = new qa_lab_u1_qalab();

                            _u1_sample_vlab7.u1_qalab_idx = u1idx;

                            _data_sample_vlab7.qa_lab_u1_qalab_list[0] = _u1_sample_vlab7;

                            _data_sample_vlab7 = callServicePostQA(_urlQaGetSampleInLab, _data_sample_vlab7);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                            setFormData(fvDetailLab, FormViewMode.ReadOnly, _data_sample_vlab7.qa_lab_u1_qalab_list);

                            TextBox check_m0_lab_ = (TextBox)fvDetailLab.FindControl("check_m0_lab");
                            if (m0_lab != "0")
                            {
                                check_m0_lab_.Text = m0_lab;
                            }
                            else
                            {
                                check_m0_lab_.Text = "0";
                            }


                            // Bind repeater Test detail
                            Repeater rp_testdetail_lab7 = (Repeater)fvDetailLab.FindControl("rp_testdetail_lab");
                            //ViewState["test_detail_view"]
                            data_qa _data_test_vlab7 = new data_qa();
                            _data_test_vlab7.bindlab_qa_m0_testdetail_list = new bindlab_m0_test_detail[1];
                            bindlab_m0_test_detail _testvlab_detail7 = new bindlab_m0_test_detail();

                            _testvlab_detail7.u1_qalab_idx = u1idx;//int.Parse(ViewState["test_detail_view"].ToString());

                            _data_test_vlab7.bindlab_qa_m0_testdetail_list[0] = _testvlab_detail7;
                            _data_test_vlab7 = callServicePostQA(_urlQaGetTestDetailLab, _data_test_vlab7);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_test_v));
                            setRepeaterData(rp_testdetail_lab7, _data_test_vlab7.bindlab_qa_m0_testdetail_list);


                            //Detail Result
                            data_qa _data_sample_vlab10 = new data_qa();
                            _data_sample_vlab10.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
                            qa_lab_u1_qalab _u1_sample_vlab10 = new qa_lab_u1_qalab();

                            _u1_sample_vlab10.u1_qalab_idx = u1idx;

                            _data_sample_vlab10.qa_lab_u1_qalab_list[0] = _u1_sample_vlab10;

                            _data_sample_vlab10 = callServicePostQA(_urlQaGetDetailResult, _data_sample_vlab10);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_qadetail));
                            setFormData(fvDetailResult, FormViewMode.ReadOnly, _data_sample_vlab10.qa_lab_u1_qalab_list);

                            if (_data_sample_vlab10.return_code == 0)
                            {

                                GridView GvResultDetail = (GridView)fvDetailResult.FindControl("GvResultDetail");
                                setGridData(GvResultDetail, _data_sample_vlab10.qa_lab_u1_qalab_list);
                            }
                            else
                            {
                                //setError(_data_googlelicenseshowdelete.return_code.ToString() + " - " + _data_googlelicenseshowdelete.return_msg);
                            }

                            TextBox _samplecode = (TextBox)fvDetailLab.FindControl("tbsample_code");
                            TextBox _m0labidx = (TextBox)fvDetailLab.FindControl("tbm0_labidx");

                            if (int.Parse(_m0labidx.Text) == int.Parse(ExternalM0LabIdx.ToString())
                                && staidx == 10)
                            {
                                try
                                {
                                    string getPath = ConfigurationManager.AppSettings["path_flie_external_lab"];

                                    string filePath = Server.MapPath(getPath + _samplecode.Text);
                                    DirectoryInfo myDirfile = new DirectoryInfo(filePath);
                                    SearchDirectories(myDirfile, _samplecode.Text);


                                }
                                catch
                                {

                                }

                            }
                            else
                            {

                            }

                            ////History Lab In Sample
                            data_qa _data_history_splab7 = new data_qa();
                            _data_history_splab7.qa_m0_history_sample_list = new qa_m0_history_sample_detail[1];
                            qa_m0_history_sample_detail _u0_doc_history_splab7 = new qa_m0_history_sample_detail();

                            _u0_doc_history_splab7.u1_qalab_idx = u1idx;

                            _data_history_splab7.qa_m0_history_sample_list[0] = _u0_doc_history_splab7;

                            _data_history_splab7 = callServicePostQA(_urlQaGetHistorySample, _data_history_splab7);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_doc));
                            setRepeaterData(rptHistorySampleInLab, _data_history_splab7.qa_m0_history_sample_list);


                            break;
                    }
                }

                break;

            case "docLabResult":

                tbValuePlaceSup.Text = "0";
                tbValuePlaceNameSup.Text = "";


                if (ViewState["rdept_permission"].ToString() == "27" || _emp_idx == 1413)
                {
                    setFormData(fvAnalyticalResults, FormViewMode.Insert, null);
                    getSelectLabForSearch((DropDownList)fvAnalyticalResults.FindControl("ddlSearchPlaceSaveResult"), "0");
                    // TxtTrigger(txtSearchSampleCode);

                    LinkButton btnSearchResult = (LinkButton)fvAnalyticalResults.FindControl("btnSearchResult");
                    LinkButton lbDocSaveReceiveN5 = (LinkButton)fvAnalyticalResults.FindControl("lbDocSaveReceiveN5");
                    linkBtnTrigger(btnSearchResult);
                    linkBtnTrigger(lbDocSaveReceiveN5);
                }
                else
                {
                    setFormData(fvAnalyticalResults, FormViewMode.ReadOnly, null);
                }

                


                break;

            case "docSupervisor":


                //rp_place by sup
                data_qa _dataSelectLabSup = new data_qa();
                _dataSelectLabSup.qa_m0_lab_list = new qa_m0_lab_detail[1];
                qa_m0_lab_detail _m0LabsupList = new qa_m0_lab_detail();
                _m0LabsupList.decision = 3;

                _dataSelectLabSup.qa_m0_lab_list[0] = _m0LabsupList;

                _dataSelectLabSup = callServicePostMasterQA(_urlQaGetLabTypeOnSelectNode3, _dataSelectLabSup);
                setRepeaterData(rpt_gvsupervisor, _dataSelectLabSup.qa_m0_lab_list);


                // if()
                if (tbValuePlaceSup.Text == "")
                {

                    lbl_showlabsup.Visible = false;
                    DataTableSupervisor(0);
                    GvSupervisor.DataSource = ViewState["vsTable"];
                    GvSupervisor.DataBind();
                }
                else
                {
                    lbl_showlabsup.Visible = true;
                    lbl_showlabsup.Text = tbValuePlaceNameSup.Text;

                    DataTableSupervisor(int.Parse(tbValuePlaceSup.Text));
                    GvSupervisor.DataSource = ViewState["vsTable"];
                    GvSupervisor.DataBind();
                }




                //DataTableSupervisor(int.Parse(tbValuePlaceSup.Text));
                //GvSupervisor.DataSource = ViewState["vsTable"];
                //GvSupervisor.DataBind();

                ////if (m0_lab == "0")
                ////{
                ////    litDebug.Text = "2222";

                ////    lbl_showlabsup.Visible = false;
                ////    tbValuePlaceSup.Text = "0";
                ////    tbValuePlaceNameSup.Text = "";

                ////    DataTableSupervisor(0);
                ////    GvSupervisor.DataSource = ViewState["vsTable"];
                ////    GvSupervisor.DataBind();
                ////}
                ////else
                ////{
                ////    litDebug.Text = "3333";
                ////    //tbValuePlaceSup.Text = "0";
                ////    DataTableSupervisor(int.Parse(tbValuePlaceSup.Text));
                ////    GvSupervisor.DataSource = ViewState["vsTable"];
                ////    GvSupervisor.DataBind();

                ////}




                break;

            case "docPrint":

                tbValuePlaceSup.Text = "0";
                tbValuePlaceNameSup.Text = "";


                getSelectLabForSearch((DropDownList)docPrint.FindControl("ddlSearchPlace"), "0");

                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, string m0_lab)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, m0_lab);
        switch (activeTab)
        {
            case "docCreate":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                //li3.Attributes.Add("class", "");
                break;
            case "docDetail":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                //li3.Attributes.Add("class", "");
                break;
            case "docLab":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "docLabResult":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");
                break;
            case "docSupervisor":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                li5.Attributes.Add("class", "");
                break;
            case "docPrint":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                li5.Attributes.Add("class", "active");
                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rp_place":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    for (int k = 0; k <= rp_place.Items.Count; k++)
                    {
                        btnPlaceLab.CssClass = ConfigureColors(k);
                        //Console.WriteLine(i);
                    }


                }

                break;

            case "rptBindPlaceForSelectDoc":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    var lbcheck_coler_place = (Label)e.Item.FindControl("lbcheck_coler_place");
                    var btnDocumentPlaceLab = (LinkButton)e.Item.FindControl("btnDocumentPlaceLab");



                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    for (int k_p = 0; k_p <= rptBindPlaceForSelectDoc.Items.Count; k_p++)
                    {
                        btnDocumentPlaceLab.CssClass = ConfigureColorsPlace(k_p);
                        //Console.WriteLine(i);
                    }


                    // btnDocumentPlaceLab.BackColor = System.Drawing.Color.Black;

                    //for (int k = 0; k <= rptBindPlaceForSelectDoc.Items.Count; k++)
                    //{
                    //    btnDocumentPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }


    }

    protected string ConfigureColors(int k)
    {
        string returnResult1 = "";
        if (k == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (k == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (k == 2)
        {
            returnResult1 = "btn btn-primary";
        }
        else if (k == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (k == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-default";
        }

        return returnResult1;
    }

    protected string ConfigureColorsPlace(int k_p)
    {
        string returnResult11 = "";
        if (k_p == 0)
        {
            returnResult11 = "btn btn-success";
        }
        else if (k_p == 1)
        {
            returnResult11 = "btn btn-primary";
        }
        else if (k_p == 2)
        {
            returnResult11 = "btn btn-warning";
        }
        else if (k_p == 3)
        {
            returnResult11 = "btn btn-default";
        }
        else if (k_p == 4)
        {
            returnResult11 = "btn btn-info";
        }
        else
        {
            returnResult11 = "btn btn-default";
        }

        return returnResult11;
    }

    #endregion reuse

    protected string _functionInputResultToColumn(int U0DocIdx, string sample_code, string IDtestdetails)
    {

        //  return sample_code;

        //litDebug.Text = place_check.ToString();

        ////data_qa _dataExport_documentNo = new data_qa();
        ////_dataExport_documentNo.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        ////qa_lab_u1_qalab _export_document = new qa_lab_u1_qalab();
        ////_export_document.u0_qalab_idx = U0DocIdx;
        ////_export_document.condition = 3;
        //////_export_document.place_idx = place_check;

        ////_dataExport_documentNo.qa_lab_u1_qalab_list[0] = _export_document;
        ////_dataExport_documentNo = callServicePostQA(_urlQaGetDataExportExcel, _dataExport_documentNo);

        ////ViewState["data_result5"] = _dataExport_documentNo.qa_lab_u1_qalab_list;
        qa_lab_u1_qalab[] _item_Result2 = (qa_lab_u1_qalab[])ViewState["data_result5"];

        //litDebug.Text = IDtestdetails.ToString();
        //litDebug.Text += sample_code;

        int[] ids = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 30, 31 };

        var _linqTest = (from data in _item_Result2
                         where data.sample_code == sample_code
                         && data.test_detail_idx == int.Parse(IDtestdetails)
                         && data.u0_qalab_idx == U0DocIdx
                         //  && ids.Contains(data.test_detail_idx)
                         select new
                         {
                             data.test_detail_idx,
                             data.sample_code,
                             data.detail_tested

                         }).ToList();


        string returnResult = "";
        if (_linqTest.Count() > 0)
        {

            for (int z = 0; z < _linqTest.Count(); z++)
            {
                if (_linqTest[z].detail_tested.ToString() != null)
                {
                    returnResult += _linqTest[z].detail_tested.ToString();
                }
                else
                {

                }
            }
        }
        else
        {
            //  returnResult = "-";
        }
        return returnResult;

    }

    protected string _function1InputResultToColumn(string U1DocIdx, string sample_code, string IDtestdetails)
    {
        //  return sample_code;
        ////data_qa _dataExport_documentNo = new data_qa();
        ////_dataExport_documentNo.qa_lab_u1_qalab_list = new qa_lab_u1_qalab[1];
        ////qa_lab_u1_qalab _export_document = new qa_lab_u1_qalab();
        ////_export_document.u1_export_excel_string = U1DocIdx;
        //////_export_document.condition = 3;
        ////_dataExport_documentNo.qa_lab_u1_qalab_list[0] = _export_document;
        ////_dataExport_documentNo = callServicePostQA(_urlQaGetSearchDataExportExcel, _dataExport_documentNo);

        ////ViewState["data_result6"] = _dataExport_documentNo.qa_lab_u1_qalab_list;
        qa_lab_u1_qalab[] _item_Result2 = (qa_lab_u1_qalab[])ViewState["data_result6"];

        //litDebug.Text = IDtestdetails.ToString();
        //litDebug.Text += sample_code;

        int[] ids = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 30, 31 };

        var _linqTest = (from data in _item_Result2
                         where data.sample_code == sample_code
                         && data.test_detail_idx == int.Parse(IDtestdetails)

                         //  && ids.Contains(data.test_detail_idx)
                         select new
                         {
                             data.test_detail_idx,
                             data.sample_code,
                             data.detail_tested,

                         }).ToList();


        string returnResult = "";
        if (_linqTest.Count() > 0)
        {

            for (int z = 0; z < _linqTest.Count(); z++)
            {
                if (_linqTest[z].detail_tested.ToString() != null)
                {
                    returnResult += _linqTest[z].detail_tested.ToString();
                }
                else
                {

                }
            }
        }
        else
        {
            //  returnResult = "-";
        }
        return returnResult;

    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }


}