﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_ecom_saleunit")]
public class data_ecom_saleunit
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master data ---//
    [XmlElement("ecom_m0_saleunit_list")]
    public ecom_m0_saleunit[] ecom_m0_saleunit_list { get; set; }

}

[Serializable]
public class ecom_m0_saleunit
{
    [XmlElement("un_idx")]
    public int un_idx { get; set; }

    [XmlElement("unit_name")]
    public string unit_name { get; set; }

    [XmlElement("material")]
    public string material { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("emp_idx_update")]
    public int emp_idx_update { get; set; }

    [XmlElement("un_status")]
    public int un_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

}


