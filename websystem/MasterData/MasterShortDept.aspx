﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="MasterShortDept.aspx.cs" Inherits="websystem_MasterData_MasterShortDept" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="text" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Master Data Short Department</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ค้นหาองค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSearchOrg" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาเลือกองค์กร.....</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="CmdSearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>

                                    </div>
                                </div>

                                <%------------------------ Div ADD  ------------------------%>



                                <asp:GridView ID="GvMaster" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="DeptIDX"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowUpdating="Master_RowUpdating">

                                  
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="#">

                                            <ItemTemplate>
                                                <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("DeptIDX") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>


                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtDeptIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("DeptIDX")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label26" runat="server" Text="Organization" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtorgidx" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("OrgNameTH")%>' />
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="DeptNameEN" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtdepten" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("DeptNameEN")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="DeptNameTH" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtdeptth" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("DeptNameTH")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ShortDept" />
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtshortdept" runat="server" CssClass="form-control" Text='<%# Eval("ShortDept")%>' />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtshortdept" Font-Size="11"
                                                                    ErrorMessage="กรุณากรอกตัวย่อฝ่าย"
                                                                    ValidationExpression="กรุณากรอกตัวย่อฝ่าย"
                                                                    SetFocusOnError="true" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                    ValidationGroup="Save" Display="None"
                                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                    ControlToValidate="txtshortdept"
                                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                                    SetFocusOnError="true" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </EditItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Organization" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lborg" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="DeptNameEN" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbdepen" runat="server" Text='<%# Eval("DeptNameEN") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="DeptNameTH" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbdeptth" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ShortDept" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbshortdept" runat="server" Text='<%# Eval("ShortDept") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

