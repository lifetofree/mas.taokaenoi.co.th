﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_overtime.aspx.cs" Inherits="websystem_hr_hr_overtime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>



    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <%-- <li id="li0" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการ OT กะคงที่<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liDetailOTDay" runat="server" visible="false">

                                    <asp:LinkButton ID="lbDetailDay" runat="server" CommandName="cmdDetail" OnCommand="btnCommand" CommandArgument="1"> รายวัน</asp:LinkButton>
                                </li>

                               

                                <li id="liDetailMonth" runat="server">
                                    <asp:LinkButton ID="lbDetailMonth" runat="server" CommandName="cmdDetail" OnCommand="btnCommand" CommandArgument="2"> กะคงที่</asp:LinkButton>
                                </li>

                            </ul>
                        </li>--%>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreateMonth" runat="server" CommandName="cmdCreate" OnCommand="btnCommand" CommandArgument="2"> สร้างรายการ</asp:LinkButton>
                        </li>

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetailMonth" runat="server" CommandName="cmdDetail" OnCommand="btnCommand" CommandArgument="2"> รายการ OT กะคงที่</asp:LinkButton>
                        </li>


                        <li id="li10" runat="server" class="dropdown" visible="false">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการ OT กะหมุนเวียน<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liDetailAdminOTDay" runat="server">

                                    <asp:LinkButton ID="lbDetailAdminOTDay" runat="server" CommandName="cmdDetailAdminCreateOTDay" OnCommand="btnCommand" CommandArgument="1"> หน้าสำหรับ Admin</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li id="liDetailEmployeeOTDay" runat="server">
                                    <asp:LinkButton ID="lbDetailEmployeeOTDay" runat="server" CommandName="cmdDetailAdminCreateEmployeee" OnCommand="btnCommand" CommandArgument="2"> หน้าสำหรับพนักงาน</asp:LinkButton>
                                </li>

                            </ul>

                            <%--<asp:LinkButton ID="lbDetailAdminOTDay" runat="server" CommandName="cmdDetailAdminOTDay" OnCommand="navCommand" CommandArgument="docDetailAdminOTDay"> หน้าสำหรับ Admin</asp:LinkButton>--%>
                        </li>

                        <%--<li id="li11" runat="server">
                            <asp:LinkButton ID="lbDetailEmployeeOTDay" runat="server" CommandName="cmdDetailEmployeeOTDay" OnCommand="navCommand" CommandArgument="docDetailEmployeeOTDay"> หน้าสำหรับพนักงาน</asp:LinkButton>
                        </li>--%>



                        <%--<li id="li1" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">สร้าง OT<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liCreateOTDay" runat="server" visible="false">
                                    <asp:LinkButton ID="lbCreateDay" runat="server" CommandName="cmdCreate" OnCommand="btnCommand" CommandArgument="1"> Admin สร้าง OT กะหมุนเวียน</asp:LinkButton>
                                </li>

                               

                                <li id="liCreateOTMonth" runat="server">
                                    <asp:LinkButton ID="lbCreateMonth" runat="server" CommandName="cmdCreate" OnCommand="btnCommand" CommandArgument="2"> กะคงที่</asp:LinkButton>
                                </li>

                            </ul>
                        </li>--%>

                        <%-- <li id="li3" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <asp:Label ID="lbl_waitapprove" Font-Bold="true" runat="server"> รายการรออนุมติ OT กะคงที่</asp:Label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="lbWaitDetailApproveDay" runat="server" visible="false">

                                    <asp:LinkButton ID="lbWaitApproveDay" runat="server" CommandName="cmdWaitApprove" OnCommand="btnCommand" CommandArgument="1"> รายวัน</asp:LinkButton>
                                </li>



                                <li id="lbWaitDetailApproveMonth" runat="server">
                                    <asp:LinkButton ID="lbWaitApproveMonth" runat="server" CommandName="cmdWaitApprove" OnCommand="btnCommand" CommandArgument="2"> กะคงที่</asp:LinkButton>
                                </li>

                            </ul>
                        </li>--%>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbWaitApproveMonth" runat="server" CommandName="cmdWaitApprove" OnCommand="btnCommand" CommandArgument="2"> รายการรออนุมัติ</asp:LinkButton>
                        </li>


                        <li id="li12" runat="server" class="dropdown" visible="false">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <asp:Label ID="lbl_waitapprove_" Font-Bold="true" runat="server"> รายการรออนุมัติ OT กะหมุนเวียน</asp:Label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liWaitApproveHeadAdminCreate" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveHeadAdminCreate" runat="server" CommandName="cmdApproveOTDayADminCreate" OnCommand="btnCommand" CommandArgument="12"> หัวหน้างาน</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li id="liWaitApproveHRAdminCreate" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveHRAdminCreate" runat="server" CommandName="cmdApproveOTDayADminCreate" OnCommand="btnCommand" CommandArgument="13"> เจ้าหน้าที่ฝ่ายทรัพยากรบุคคล</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>
                                <li id="liWaitApproveAdminEdit" runat="server">

                                    <asp:LinkButton ID="lbWaitApproveAdminEdit" runat="server" CommandName="cmdApproveOTDayADminCreate" OnCommand="btnCommand" CommandArgument="11"> Admin แก้ไขรายการ</asp:LinkButton>
                                </li>
                            </ul>

                        </li>


                        <% if (Session["emp_idx"].ToString() == "1413" || Session["emp_idx"].ToString() == "24047" || ViewState["rpos_permission"].ToString() == "5903" || ViewState["rpos_permission"].ToString() == "5906"
                                || ViewState["rpos_permission"].ToString() == "5905")
                            { %>
                        <li id="li9" runat="server" visible="false">
                            <asp:LinkButton ID="lbReportOTMonth" runat="server" CommandName="cmdReportOTMonth" OnCommand="navCommand" CommandArgument="docReportOTMonth"> รายงาน OT กะคงที่</asp:LinkButton>
                        </li>
                        <% } %>

                        <li id="li7" runat="server" visible="false">
                            <asp:LinkButton ID="lbDetailImportOTDay" runat="server" CommandName="cmdDetailImportOTDay" OnCommand="navCommand" CommandArgument="docDetailImportOTDay"> รายการ OT รายวัน</asp:LinkButton>
                        </li>

                        <li id="li6" runat="server" visible="false">
                            <asp:LinkButton ID="lbImportOTDay" runat="server" CommandName="cmdImportOTDay" OnCommand="navCommand" CommandArgument="docImportOTDay"> Import OT รายวัน</asp:LinkButton>
                        </li>

                        <li id="li8" runat="server" visible="false">
                            <asp:LinkButton ID="lbReportOTDay" runat="server" CommandName="cmdReportOTDa" OnCommand="navCommand" CommandArgument="docReportOTDay"> รายงาน OT รายวัน</asp:LinkButton>
                        </li>

                        <li id="li5" runat="server" visible="false">
                            <asp:LinkButton ID="lbAddGroupOTDay" runat="server" Visible="false" CommandName="cmdAddGroupOTDay" OnCommand="navCommand" CommandArgument="docAddGroupOTDay"> Admin เพิ่มกลุ่ม OT รายวัน</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server" visible="false">
                            <asp:LinkButton ID="lbSetOTMonth" runat="server" CommandName="cmdSetOTMonth" OnCommand="navCommand" CommandArgument="docSetOTMonth"> หัวหน้าตั้งค่า OT รายเดือน</asp:LinkButton>
                        </li>

                        <li id="li4" runat="server" class="dropdown" visible="false">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <asp:Label ID="lbl_Report" Font-Bold="true" runat="server"> รายงาน</asp:Label>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="lbDetailReportDay" runat="server">

                                    <asp:LinkButton ID="lbReportDay" runat="server" CommandName="cmdReport" OnCommand="btnCommand" CommandArgument="1"> รายวัน</asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li id="lbDetailReportMonth" runat="server">
                                    <asp:LinkButton ID="lbReportMonth" runat="server" CommandName="cmdReport" OnCommand="btnCommand" CommandArgument="2"> รายเดือน</asp:LinkButton>
                                </li>

                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divFlowLiToDocument" runat="server">

                            <asp:HyperLink ID="hplFlowOvertime" NavigateUrl="https://drive.google.com/file/d/1SE2ymo8eZvcPjeOtBM1cEvwV_efcNoM6/view?usp=sharing" Target="_blank" runat="server" CommandName="cmdFlowOvertime" OnCommand="btnCommand"><i class="fas fa-book"></i> Flow การทำงาน</asp:HyperLink>
                        </li>

                        <li id="_divMenuLiToDocument" runat="server" visible="false">

                            <asp:HyperLink ID="hplManualOvertime" NavigateUrl="https://docs.google.com/document/d/1FOdLEriZ27BmxVtNmFqo-X5E2Y7Yu0cM9x2d-KRvgho/edit?usp=sharing" Target="_blank" runat="server" CommandName="cmdManualOvertime" OnCommand="btnCommand"><i class="fas fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <asp:UpdatePanel ID="_PanelManualOvertime" runat="server">
        <ContentTemplate>
            <div class="col-md-12">
                <div class="form-group pull-right">


                    <%--<asp:HyperLink ID="hplManualOvertime" CssClass="btn btn-primary" NavigateUrl="https://docs.google.com/document/d/1FOdLEriZ27BmxVtNmFqo-X5E2Y7Yu0cM9x2d-KRvgho/edit?usp=sharing" Target="_blank" data-toggle="tooltip" title="คู่มือการใช้งาน" runat="server" CommandName="cmdManualOvertime" OnCommand="btnCommand"><i class="fas fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelDetailDay" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info" id="_divheaderday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการขอ OT รายวัน</h3>
                            </div>
                        </div>

                        <!-- Back To Detail To Day -->
                        <asp:UpdatePanel ID="Update_BackToDetailOTDay" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToDetailOTDay" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToDetailOTDay" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail To Day -->

                        <asp:GridView ID="GvDetailOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="5"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <p>
                                            <asp:Label ID="lbl_create_date_detail_otday" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lbl_time_create_date_detail_otday" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_u0_document_idx_detail_otday" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detail_otday" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>องค์กร:</b>
                                            <asp:Label ID="lbl_org_name_th_detail_otday" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>ฝ่าย:</b>
                                            <asp:Label ID="lbl_dept_name_th_detail_otday" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <p>
                                            <b>ชื่อกลุ่ม:</b>
                                            <%--<asp:Label ID="lbl_m0_group_idx_detail_otday" Visible="false" runat="server" Text='<%# Eval("m0_group_idx") %>' />--%>
                                            <asp:Label ID="lbl_group_name_detail_otday" Visible="true" runat="server" Text='<%# Eval("group_name") %>' />
                                        </p>
                                        <p>
                                            <b>จำนวนคน:</b>
                                            <asp:Label ID="lbl_sumhours_detail" Visible="true" runat="server" Text='<%# Eval("count_emp")%>' />
                                            <%--<asp:Label ID="Label2" Visible="true" runat="server" Text='<%# Eval("count_emp") + " " + "ชั่วโมง" %>' />--%>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_otday" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_otday" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_otday" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_otday" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_otday" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_otday" runat="server" Text='<%# Eval("actor_name") %>' />

                                        <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:UpdatePanel ID="Update_PnbtnViewDetailOTDay" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnViewDetailOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                            </ContentTemplate>

                                            <%--<Triggers>
                                                <asp:PostBackTrigger ControlID="btnViewDetail" />
                                            </Triggers>--%>
                                        </asp:UpdatePanel>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <asp:FormView ID="FvDetailOTDay" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfu0_document_idx_viewotday" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfM0NodeIDX_viewotday" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX_viewotday" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX_viewotday" runat="server" Value='<%# Eval("staidx") %>' />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT รายวัน</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="lbl_emp_codeviewotday" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_u0_document_idx_viewotday" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                                    <asp:Label ID="lbl_emp_code_viewotday" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                                    <asp:Label ID="lbl_cemp_idx_viewotday" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />


                                                </div>

                                                <asp:Label ID="lbl_emp_name_thviewotday" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_name_th_viewotday" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_org_name_th_viewotday" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_org_idx_viewotday" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                                </div>

                                                <asp:Label ID="lbl_dept_name_thviewotday" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_dept_name_th_viewotday" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_rdept_idx_viewotday" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_sec_name_th_viewotday" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                                    <asp:Label ID="lbl_rsec_idx_viewotday" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_pos_name_th_viewotday" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                                    <asp:Label ID="lbl_rpos_idx_viewotday" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>

                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_email_viewotday" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                                </div>

                                                <%-- <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_costcenter_no_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                                    <asp:Label ID="lbl_costcenter_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                                </div>--%>


                                                <%-- </div>--%>
                                            </div>

                                            <%--   <div class="form-group">
                                                
                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการจอง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_booking_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_booking_name")%>' />
                                                    <asp:Label ID="lbl_type_booking_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_booking_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการเดินทาง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_travel_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_travel_name")%>' />
                                                    <asp:Label ID="lbl_type_travel_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_travel_idx")%>' />

                                                </div>
                                                
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <asp:GridView ID="GvViewDetailOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="31"
                            ShowFooter="False">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex + 1) %>
                                        </small>

                                        <%--<asp:Label ID="lbl_dateot_from_view" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_emp_code_viewdetailday" runat="server" Text='<%# Eval("emp_code_setotmonth") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายชื่อ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_emp_name_th_viewdetailday" runat="server" Text='<%# Eval("emp_name_th_setotmonth") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่เริ่ม" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_dateot_from_setotmonth_viewdetailday" runat="server" Text='<%# Eval("dateot_from_setotmonth") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สิ้นสุด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_dateot_to_setotmonth_viewdetailday" runat="server" Text='<%# Eval("dateot_to_setotmonth") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาที่เริ่ม" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_time_form_setotmonth_viewdetailday" runat="server" Text='<%# Eval("time_form_setotmonth") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาที่สิ้นสุด" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_time_to_setotmonth_viewdetailday" runat="server" Text='<%# Eval("time_to_setotmonth") %>'></asp:Label>


                                    </ItemTemplate>

                                </asp:TemplateField>

                                <%--<asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_time_to_view" runat="server" Text='<%# Eval("time_to") %>'></asp:Label>

                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: right;">
                                            <asp:Literal ID="lit_timedetail_view" runat="server" Text="เวลารวม :"></asp:Literal>
                                        </div>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_sum_hours_view" runat="server" Text='<%# Eval("sum_hours") %>'></asp:Label>
                                       
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: right; background-color: chartreuse;">
                                            <asp:Label ID="lit_total_hoursmonth_view" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </FooterTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_comment_view" runat="server" Text='<%# Eval("comment") %>'></asp:Label>

                                    </ItemTemplate>

                                    <FooterTemplate>
                                        <div style="text-align: left;">
                                            <asp:Literal ID="lit_hours_detail" runat="server" Text="ชั่วโมง"></asp:Literal>
                                        </div>
                                    </FooterTemplate>

                                </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>


                        <!-- Log Detail OT Day -->
                        <div class="panel panel-info" id="div_LogViewDetailDay" runat="server" visible="false">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogViewDetailDay" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log Detail OT Day -->

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelDetailMonth" runat="server">
                    <ContentTemplate>
                        <div class="col-md-12" id="div_fileshow" runat="server" style="color: transparent;">
                            <%--<div id="div2" runat="server" style="color: transparent;">
                            <label class="f-s-13"></label>--%>
                            <asp:FileUpload ID="FileUpload1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />

                        </div>

                        <div class="form-group" id="div_searchDetailOTMonth" runat="server" visible="false">
                            <asp:LinkButton ID="btnSearchIndexOTMonth" CssClass="btn btn-info" runat="server" data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="cmdSearchIndexOTMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> แสดงแถบเครื่องมือค้นหา</asp:LinkButton>

                            <%-- <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchPage" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>--%>
                        </div>


                        <!-- Back To Detail OT Month -->
                        <asp:UpdatePanel ID="Update_BackToDetailSearchIndexOTMonth" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnCancelSearchIndexOTMonth" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                        CommandName="cmdCancelSearchIndexOTMonth" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail OT Month -->


                        <asp:UpdatePanel ID="_PanelSearchDetail" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ค้นหารายการ OT</h3>
                                    </div>

                                    <div class="panel-body">

                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>เดือน</label>
                                                    <asp:DropDownList ID="ddlSearchMonth" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                        <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                        <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                        <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                        <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                        <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                        <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                        <asp:ListItem Value="07">กรกฎาคม</asp:ListItem>
                                                        <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                        <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                        <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                        <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                        <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                    </asp:DropDownList>

                                                  <%--  <asp:RequiredFieldValidator ID="Required_ddlSearchMonth"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlSearchMonth" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกเดือน"
                                                        ValidationGroup="SearchDetailOTmonth" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_ddlSearchMonth" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ปี</label>
                                                    <asp:DropDownList ID="ddlSearchYear" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>องค์กร</label>
                                                    <asp:DropDownList ID="ddlSearchorg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <%--<p class="help-block"><font color="red">**กรุณาเลือกองค์กร</font></p>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorg" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorg" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ฝ่าย</label>
                                                    <asp:DropDownList ID="ddlSearchrdept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>แผนก</label>
                                                    <asp:DropDownList ID="ddlSearchrsec" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ชื่อพนักงาน</label>
                                                    <asp:DropDownList ID="ddlSearchemp" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="--- เลือกพนักงาน ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Costcenter</label>
                                                    <asp:TextBox ID="txtSearchCostcenter" placeholder="Coscenter" runat="server" CssClass="form-control"> </asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>รหัสพนักงาน</label>
                                                    <asp:TextBox ID="txtSearchEmpcode" placeholder="รหัสพนักงาน" runat="server" MaxLength="8" CssClass="form-control"> </asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>สถานะรายการ</label>
                                                    <asp:DropDownList ID="ddlStatusDoc" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="--- เลือกสถานะรายการ ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <asp:LinkButton ID="btnSearchOTMonth" runat="server" CssClass="btn btn-primary" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchDetailOTmonth" CommandName="cmdSearchDetailOTMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="btnResetSearchOTMonth" runat="server" CssClass="btn btn-default" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchDetailOTMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="panel panel-success" id="_divheadermonth" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการ OT ประเภทกะคงที่</h3>
                            </div>
                        </div>

                        <!-- Back To Detail To Month -->
                        <asp:UpdatePanel ID="Update_BackToDetail" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToDetail" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToDetail" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail To Month -->
                        <div id="div_GvDetailOTMont" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvDetailOTMont" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                ShowHeaderWhenEmpty="True"
                                AllowPaging="True"
                                PageSize="10"
                                BorderStyle="None"
                                CellSpacing="2">
                                <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_time_create_date_detail" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_u0_document_idx_detail" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                            <p>
                                                <b>ชื่อ-สกุลผู้สร้าง:</b>
                                                <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                                <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                            </p>
                                            <p>
                                                <b>องค์กร:</b>
                                                <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>' />
                                            </p>
                                            <p>
                                                <b>ฝ่าย:</b>
                                                <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <p>
                                                <b>ประจำเดือน:</b>
                                                <asp:Label ID="lbl_month_ot_detail" Visible="false" runat="server" Text='<%# Eval("month_ot") %>' />
                                                <asp:Label ID="lbl_month_name_detail" Visible="true" runat="server" Text='<%# Eval("month_ot_name") %>' />
                                            </p>
                                            <%--<p>
                                            <b>ชั่วโมงรวม:</b>
                                            <asp:Label ID="lbl_sumhours_detail" Visible="true" runat="server" Text='<%# Eval("sumhours") + " " + "ชั่วโมง" %>' />
                                        </p>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                        <ItemTemplate>

                                            <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                            <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                            <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                            <br />
                                            โดย<br />
                                            <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />

                                            <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="Update_PnbtnViewDetail" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                        data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                                </ContentTemplate>

                                                <%--<Triggers>
                                                <asp:PostBackTrigger ControlID="btnViewDetail" />
                                            </Triggers>--%>
                                            </asp:UpdatePanel>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                        <asp:FormView ID="FvDetailOTMont" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                <asp:HiddenField ID="hfmonth_ot" runat="server" Value='<%# Eval("month_ot") %>' />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT ประเภทกะคงที่</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_u0_document_idx_view" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                                    <asp:Label ID="lbl_emp_code_view" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                                    <asp:Label ID="lbl_cemp_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />


                                                </div>

                                                <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_org_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_org_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                                </div>

                                                <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_dept_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_rdept_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_sec_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                                    <asp:Label ID="lbl_rsec_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_pos_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                                    <asp:Label ID="lbl_rpos_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>

                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_email_view" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                                </div>

                                                <%-- <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_costcenter_no_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                                    <asp:Label ID="lbl_costcenter_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                                </div>--%>


                                                <%-- </div>--%>
                                            </div>

                                            <%--   <div class="form-group">
                                                
                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการจอง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_booking_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_booking_name")%>' />
                                                    <asp:Label ID="lbl_type_booking_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_booking_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทการเดินทาง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_travel_name_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_travel_name")%>' />
                                                    <asp:Label ID="lbl_type_travel_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("type_travel_idx")%>' />

                                                </div>
                                                
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div id="divscroll_GvViewDetailOTMont" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvViewDetailOTMont" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="True" PageSize="31"
                                ShowFooter="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_dateot_from_view" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>
                                                <asp:Label ID="lbl_detailholiday_view" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                <asp:Label ID="lbl_detaildayoff_view" runat="server"><span class="text-danger">*(DH)</span></asp:Label>

                                                <asp:Label ID="lbl_holiday_idx_view" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>

                                                <asp:Label ID="lbl_day_off_view" runat="server" Visible="false" Text='<%# Eval("day_off") %>'></asp:Label>
                                                <asp:Label ID="lbl_date_condition_view" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_TypeWork_view" runat="server" Text='<%# Eval("TypeWork") %>'></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="งานที่ปฏิบัติ" ItemStyle-HorizontalAlign="left" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_detail_job_view" runat="server" Text='<%# Eval("detail_job") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-HorizontalAlign="right" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_time_form_view" runat="server" Text='<%# Eval("time_form") %>'></asp:Label>

                                            <%-- <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">

                                                    <asp:TextBox ID="txt_timestart_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_start") %>' Enabled="false" CssClass="form-control clockpicker cursor-pointer" />
                                                    <span class="input-group-addon show-time-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                 
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="right" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_time_to_view" runat="server" Text='<%# Eval("time_to") %>'></asp:Label>

                                            <%--<asp:UpdatePanel ID="Update_Timeend" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">
                                                    <asp:TextBox ID="txt_timeend_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_end") %>' Enabled="false" CssClass="form-control clockpickerto cursor-pointer" />

                                                    <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_timedetail_view" runat="server" Text="เวลารวม :"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT ก่อน" ItemStyle-HorizontalAlign="right" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="txtot_before_view" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                                <asp:Label ID="lblot_before_view" runat="server" Text='<%# Eval("hours_ot_before") %>' Font-Bold="true"></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_ot_before_status_view" Visible="false" runat="server" Text='<%# Eval("ot_before_status") %>'></asp:Label>
                                                <asp:CheckBox ID="chk_ot_before_view" runat="server" Enabled="false" Text=" ให้ ot"></asp:CheckBox>
                                            </p>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_before_view" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-HorizontalAlign="right" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_holiday_view" Enabled="false" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                            <asp:Label ID="lblot_holiday_view" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_holiday") %>'></asp:Label>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_holiday_view" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หลัง" ItemStyle-HorizontalAlign="right" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_after_view" Enabled="false" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                            <asp:Label ID="lblot_after_view" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_after") %>'></asp:Label>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_after_view" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sum_hours_view" runat="server" Text='<%# Eval("sum_hours") %>'></asp:Label>
                                            <%--<asp:TextBox ID="txt_sum_hour" runat="server" CssClass="form-control clockpickertotal cursor-pointer" Enabled="false" Text='<%# Eval("sum_time") %>' placeholder="รวม(ชั่วโมง) ..."></asp:TextBox>--%>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursmonth_view" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="center" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_settime_view" runat="server" Text='<%# Eval("settime") %>'></asp:Label>
                                            <%--<asp:Label ID="lbl_sum_hours_view" runat="server" Text='<%# Eval("sum_hours") %>'></asp:Label>--%>
                                            <%--<asp:TextBox ID="txt_head_set" runat="server" Enabled="false" CssClass="form-control" placeholder="OT หัวหน้า Set ..."></asp:TextBox>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_comment_view" runat="server" Text='<%# Eval("comment") %>'></asp:Label>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: left;">
                                                <asp:Literal ID="lit_hours_detail" runat="server" Text="ชั่วโมง"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความเห็นจาก HR" ItemStyle-HorizontalAlign="left" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_comment_hr_view" runat="server" Text='<%# Eval("comment_hr") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="form-group" id="div_GvViewDetailOTMont" runat="server">
                            <p class="help-block"><font color="red"> หมายเหตุ : *(HH) คือวันหยุดบริษัท / *(DH) คือวันหยุดของพนักงาน</font></p>

                        </div>

                        <asp:GridView ID="GvViewDetailHeadSetOTMont" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="10"
                            ShowFooter="False">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_emp_code_viewsetot" runat="server" Text='<%# Eval("emp_code_setotmonth") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อ - สกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_emp_name_th_viewsetot" runat="server" Text='<%# Eval("emp_name_th_setotmonth") %>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่เริ่ม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_dateot_from_viewsetot" runat="server" Text='<%# Eval("dateot_from_setotmonth") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สิ้นสุด" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_dateot_to_viewsetot" runat="server" Text='<%# Eval("dateot_to_setotmonth") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาที่เริ่ม" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_time_form_viewsetot" runat="server" Text='<%# Eval("time_form_setotmonth") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_time_to_viewsetot" runat="server" Text='<%# Eval("time_to_setotmonth") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <div id="div_scrollGvViewEditDetailOTMont" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvViewEditDetailOTMont" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="True" PageSize="31"
                                ShowFooter="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_u1_doc_idx_edit" Visible="false" runat="server" Text='<%# Eval("u0_doc1_idx") %>'></asp:Label>
                                                <asp:CheckBox ID="chk_date_ot_edit" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                <asp:Label ID="lbl_holiday_idx_check_edit" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_work_start_edit" runat="server" Visible="false" Text='<%# Eval("work_start") %>'></asp:Label>
                                                <asp:Label ID="lbl_work_finish_edit" runat="server" Visible="false" Text='<%# Eval("work_finish") %>'></asp:Label>
                                                <asp:Label ID="lbl_break_start_edit" runat="server" Visible="false" Text='<%# Eval("break_start") %>'></asp:Label>
                                                <asp:Label ID="lbl_break_finish_edit" runat="server" Visible="false" Text='<%# Eval("break_finish") %>'></asp:Label>

                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_date_start_edit" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>
                                                <asp:Label ID="lbl_detailholiday_edit" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                <asp:Label ID="lbl_detaildayoff_edit" runat="server"><span class="text-danger">*(DH)</span></asp:Label>
                                                <asp:Label ID="lbl_day_off_edit" runat="server" Visible="false" Text='<%# Eval("day_off") %>'></asp:Label>
                                                <asp:Label ID="lbl_date_condition_edit" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_TypeWork_edit" runat="server" Text='<%# Eval("TypeWork") %>'></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="งานที่ปฏิบัติ" ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <small>
                                                <asp:TextBox ID="txt_job_edit" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" ValidationGroup="SaveEditEmployeeOTMonth" CssClass="form-control" Text='<%# Eval("detail_job") %>' placeholder="กรอกงานที่ปฏิบัติ ..."></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_job_edit"
                                                    runat="server"
                                                    ControlToValidate="txt_job_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกงานที่ปฏิบัติ"
                                                    ValidationGroup="SaveEditEmployeeOTMonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_job_edit" Width="160" PopupPosition="BottomLeft" />
                                            </small>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="13%" HeaderStyle-Width="13%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Timestart_edit" runat="server">
                                                <ContentTemplate>

                                                    <asp:Label ID="lbl_month_idx_start_edit" runat="server" Text='<%# Eval("month_idx_start") %>' Visible="false" />

                                                    <asp:DropDownList ID="ddl_timestart_edit" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                    <%--<div class="input-group time">

                                                    <asp:TextBox ID="txt_timestart_job_edit" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_form") %>' Enabled="false" CssClass="form-control clockpicker cursor-pointer" />
                                                    <span class="input-group-addon show-time-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                    
                                                </div>--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="13%" HeaderStyle-Width="13%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="Update_Timeend_edit" runat="server">
                                                <ContentTemplate>

                                                    <asp:Label ID="lbl_month_idx_end_edit" runat="server" Text='<%# Eval("month_idx_end") %>' Visible="false" />
                                                    <asp:DropDownList ID="ddl_timeend_edit" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>


                                                    <%--<div class="input-group time">
                                                    <asp:TextBox ID="txt_timeend_job_edit" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_to") %>' Enabled="false" CssClass="form-control clockpickerto cursor-pointer" />

                                                    <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>--%>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_timedetail_edit" runat="server" Text="เวลารวม :"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT ก่อน" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" HeaderStyle-Width="8%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="txtot_before_edit" Visible="false" Enabled="false" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                                <asp:DropDownList ID="ddl_timebefore_edit" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                                <asp:Label ID="lblot_before_edit" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_before") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_ot_before_status_edit" Visible="false" runat="server" Text='<%# Eval("ot_before_status") %>'></asp:Label>
                                                <asp:CheckBox ID="chk_ot_before_edit" Visible="false" Checked="true" Enabled="false" runat="server" Text=" ให้ ot"></asp:CheckBox>
                                            </p>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_before_edit" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" HeaderStyle-Width="8%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_holiday_edit" Visible="false" Enabled="false" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timeholiday_edit" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                            <asp:Label ID="lblot_holiday_edit" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_holiday") %>'></asp:Label>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_holiday_edit" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หลัง" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" HeaderStyle-Width="8%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_after_edit" Visible="false" Enabled="false" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timeafter_edit" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                            <asp:Label ID="lblot_after_edit" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_after") %>'></asp:Label>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_after_edit" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="Right" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txt_sum_hour_edit" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("sum_hours") %>' placeholder="รวม(ชั่วโมง) ..."></asp:TextBox>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursmonth_edit" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="center" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txt_head_set_edit" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("settime") %>' placeholder="OT หัวหน้า Set ..."></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txt_remark_edit" TextMode="MultiLine" Rows="2" Style="overflow: auto" runat="server" Text='<%# Eval("comment") %>' CssClass="form-control" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: left;">
                                                <asp:Literal ID="lit_hours_detail_edit" runat="server" Text="ชั่วโมง"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความเห็นจาก HR" ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txt_comment_hr_edit" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" Enabled="false" Text='<%# Eval("comment_hr") %>' CssClass="form-control" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="form-group" id="div_GvViewEditDetailOTMont" runat="server">
                            <p class="help-block"><font color="red"> หมายเหตุ : *(HH) คือวันหยุดบริษัท / *(DH) คือวันหยุดของพนักงาน</font></p>
                        </div>


                        <!-- Panel Edit Document Month -->
                        <asp:UpdatePanel ID="Update_PanelSaveEditDocMonth" runat="server">
                            <ContentTemplate>

                                <div class="form-group" id="div_EditFileMemo" runat="server" visible="false">
                                    <label class="f-s-13">แก้ไขไฟล์ขอ OT ย้อนหลัง</label>
                                    <asp:FileUpload ID="UploadFileMemoEdit" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true"
                                        CssClass="control-label multi max-1 accept-png|jpg|pdf maxsize-1024  with-preview" runat="server" />

                                    <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png, pdf</font></p>


                                    <%--<asp:FileUpload ID="myFileUpload" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" runat="server" />--%>
                                    <%-- <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png</font></p>--%>
                                </div>





                                <div class="form-group pull-right">
                                    <asp:LinkButton ID="btnSaveEditOTMonth" ValidationGroup="SaveEditEmployeeOTMonth" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandName="cmdSaveEditOTMonth"></asp:LinkButton>

                                    <%-- <asp:LinkButton ID="btnCancelToDetailMonth" CssClass="btn btn-danger" runat="server" ValidationGroup="SaveCreateEmployeeOTMonth" data-original-title="Ba" data-toggle="tooltip" 
                                        Text="กลับ" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>--%>
                                </div>
                                <div class="clearfix"></div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnSaveEditOTMonth" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <!-- Panel Edit Document Month -->

                        <asp:GridView ID="GvFileMenoOTMonth" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            HeaderStyle-CssClass="info"
                            OnRowDataBound="gvRowDataBound"
                            BorderStyle="None"
                            CellSpacing="2"
                            Font-Size="Small">

                            <Columns>
                                <asp:TemplateField HeaderText="ไฟล์เอกสาร Memo">
                                    <ItemTemplate>
                                        <div class="col-lg-10">
                                            <asp:Literal ID="ltFileName11_permwait" runat="server" Text='<%# Eval("FileName") %>' />
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:HyperLink runat="server" ID="btnDL11_permwait" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                            <asp:HiddenField runat="server" ID="hidFile11_permwait" Value='<%# Eval("Download") %>' />
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>


                        <!-- Log Detail OT Mont -->
                        <div class="panel panel-info" id="div_LogViewDetailMonth" runat="server" visible="false">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogViewDetailMonth" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log Detail OT Mont -->

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

        </asp:View>
        <!--View Detail-->

        <!--View Detail AdminCreate-->
        <asp:View ID="docDetailAdminOTDay" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelDetailAdminOTDay" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-info" id="_divheader_empshiftadmin" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการขอ OT กะหมุนเวียน(สำหรับ Admin)</h3>
                            </div>
                        </div>

                        <asp:GridView ID="GvDetailAdminOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <%# (Container.DataItemIndex + 1) %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <p>
                                            <asp:Label ID="lbl_create_date_detail_adminotday" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lbl_time_create_date_detail_adminotday" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_u0_document_idx_detail_adminotday" runat="server" Visible="false" Text='<%# Eval("u0_docday_idx") %>'></asp:Label>

                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail_adminotday" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detail_adminotday" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>องค์กร:</b>
                                            <asp:Label ID="lbl_org_name_th_detail_adminotday" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>ฝ่าย:</b>
                                            <asp:Label ID="lbl_dept_name_th_detail_adminotday" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_otday" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_otday" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_otday" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_otday" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_otday" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_otday" runat="server" Text='<%# Eval("actor_name") %>' />

                                       
                                    </ItemTemplate>
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>


                                        <asp:Label ID="lbl_staidx_detail_otday" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:LinkButton ID="btnViewAdminOTDay" CssClass="btn btn-sm btn-info" target="" runat="server" CommandName="cmdViewAdminOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_docday_idx")%>'
                                            data-toggle="tooltip" title="ดูข้อมูล"><i class="far fa-file-alt"> ดูข้อมูล</i></asp:LinkButton>

                                        <%--<asp:LinkButton ID="btnViewDetailOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"> ดูข้อมูล</i></asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <!-- Back To Detail-->
                        <asp:UpdatePanel ID="_PnDetailBackToDetailAdmin" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToDetailAdmin" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToDetailAdmin" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail-->

                        <asp:FormView ID="FvDetailAdminOTDay" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT กะหมุนเวียน</h3>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">ผู้ทำรายการ :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_emp_name_th_viewadmin" runat="server" CssClass="form-control" placeholder="ผู้ทำรายการ" Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                                </div>
                                                <%-- <label class="col-md-2 control-label">เวลา<span class="text-danger">* :</label>--%>
                                                <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_emp_code_viewadmin" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน" Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_create_date_viewadmin" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# Eval("create_date") %>'></asp:TextBox>
                                                </div>
                                                <%-- <label class="col-md-2 control-label">เวลา<span class="text-danger">* :</label>--%>
                                                <label class="col-md-2 control-label">เวลา :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_time_create_date_viewadmin" runat="server" CssClass="form-control" placeholder="เวลา" Enabled="False" Text='<%# Eval("time_create_date") %>'></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <asp:UpdatePanel ID="_PanelGvViewDetailAdminOTDay" runat="server">
                            <ContentTemplate>

                                <div id="div_ViewDetailAdminOTDay" style="overflow: auto" runat="server">

                                    <asp:GridView ID="GvViewDetailAdminOTDay" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="True" PageSize="10"
                                        ShowFooter="true">
                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbl_no_idx_view" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                    </small>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_date_view" runat="server" Text='<%# Eval("shift_date") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lbl_parttime_name_th_view" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                                    </p>
                                                    <asp:Label ID="lbl_parttime_day_off_detail_view" Visible="false" runat="server" Text='<%# Eval("parttime_day_off") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_workdays_detail_view" Visible="false" runat="server" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                                    <asp:Label ID="lbl_date_condition_detail_view" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_detail_view" runat="server" Visible="false" Text='<%# Eval("holiday_idx") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <b>รหัสพนักงาน :</b>
                                                        <asp:Label ID="lbl_emp_idx_otday_view" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_emp_code_otday_view" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ชื่อ-สกุล :</b>
                                                        <asp:Label ID="lbl_emp_name_th_otday_view" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ตำแหน่ง :</b>
                                                        <asp:Label ID="lbl_rpos_idx_otday_view" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_pos_name_th_otday_view" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                    </p>

                                                    <p>
                                                        <b>หมายเหตุ admin :</b>
                                                        <asp:Label ID="lbl_comment_admin_otday_view" runat="server" Text='<%# Eval("comment_admin") %>'></asp:Label>
                                                    </p>

                                                    <%--<p>
                                                        <b>ความเห็น หัวหน้างาน :</b>
                                                        <asp:Label ID="lbl_comment_head_otday_view" runat="server" Text='<%# Eval("comment_head") %>'></asp:Label>
                                                    </p>

                                                    <p>
                                                        <b>ความเห็น hr :</b>
                                                        <asp:Label ID="lbl_comment_hr_otday_view" runat="server" Text='<%# Eval("comment_hr") %>'></asp:Label>
                                                    </p>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_timestart_view" Text='<%# Eval("datetime_start") %>' runat="server"></asp:Label>

                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:Label ID="lbl_shift_timeend_view" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                                        <%-- <asp:Label ID="lbl_shift_timeend_uidx" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_timestart_otscan_view" runat="server" Text='<%# Eval("datetime_scanot_in") %>'></asp:Label>
                                                        <%--<asp:Label ID="lbl_shift_timestart_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:Label ID="lbl_shift_timeend_otscan_view" runat="server" Text='<%# Eval("datetime_scanot_out") %>'></asp:Label>
                                                        <%--<asp:Label ID="lbl_shift_timeend_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีก่อน (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otbefore_view" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otbefore_view" runat="server" Text='<%# Eval("hours_otbefore") %>'></asp:Label>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีหลัง (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otafter_view" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otafter_view" runat="server" Text='<%# Eval("hours_otafter") %>'></asp:Label>

                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีวันหยุด (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otholiday_view" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otholiday_view" runat="server" Text='<%# Eval("hours_otholiday") %>'></asp:Label>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="lbl_staidx_detail_otday_view" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_status_name_detail_otday_view" runat="server" Text='<%# Eval("status_name") %>' />
                                                    <asp:Label ID="lbl_m0_node_idx_detail_otday_view" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_node_name_detail_otday_view" runat="server" Text='<%# Eval("node_name") %>' />
                                                    <br />
                                                    โดย<br />
                                                    <asp:Label ID="lbl_m0_actor_idx_detail_otday_view" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_actor_name_detail_otday_view" runat="server" Text='<%# Eval("actor_name") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <b>admin :</b>
                                                        <asp:Label ID="lbl_comment_admin_otday_view" runat="server" Text='<%# Eval("comment_admin") %>'></asp:Label>
                                                    </p>

                                                    <p>
                                                        <b>หัวหน้างาน :</b>
                                                        <asp:Label ID="lbl_comment_head_otday_view" runat="server" Text='<%# Eval("comment_head") %>'></asp:Label>
                                                    </p>

                                                    <p>
                                                        <b>hr :</b>
                                                        <asp:Label ID="lbl_comment_hr_otday_view" runat="server" Text='<%# Eval("comment_hr") %>'></asp:Label>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </ContentTemplate>
                </asp:UpdatePanel>



                <asp:UpdatePanel ID="_PndocDetailEmployeeOTDay" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-info" id="_divheader_empshiftempployee" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการขอ OT กะหมุนเวียน(สำหรับพนักงาน)</h3>
                            </div>
                        </div>

                        <asp:GridView ID="GvDetailToEmployeeCreateAdmin" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex + 1) %>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <p>
                                            <asp:Label ID="lbl_create_date_detail_adminotday" runat="server" Text='<%# Eval("create_date_admin") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lbl_time_create_date_detail_adminotday" runat="server" Text='<%# Eval("time_create_date_admin") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_u0_document_idx_detail_adminotday" runat="server" Visible="false" Text='<%# Eval("u0_docday_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_u1_docday_idx_adminotday" runat="server" Visible="false" Text='<%# Eval("u1_docday_idx") %>'></asp:Label>
                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <%--<asp:Label ID="lbl_cemp_idx_detail_adminotday" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />--%>
                                            <asp:Label ID="lbl_emp_name_th_admin_adminotday" runat="server" Text='<%# Eval("emp_name_th_admin") %>' />
                                        </p>
                                        <p>
                                            <b>องค์กร:</b>
                                            <asp:Label ID="lbl_org_name_th_detail_adminotday" runat="server" Text='<%# Eval("org_name_th_admin") %>' />
                                        </p>
                                        <p>
                                            <b>ฝ่าย:</b>
                                            <asp:Label ID="lbl_dept_name_th_detail_adminotday" runat="server" Text='<%# Eval("dept_name_th_admin") %>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_otday" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_otday" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_otday" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_otday" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_otday" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_otday" runat="server" Text='<%# Eval("actor_name") %>' />


                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>



                                        <asp:LinkButton ID="btnViewEmployeeDetailOTDay" CssClass="btn btn-sm btn-info" target="" runat="server" CommandName="cmdViewEmployeeDetailOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_docday_idx")+ ";" + Eval("u1_docday_idx")%>'
                                            data-toggle="tooltip" title="ดูข้อมูล"><i class="far fa-file-alt"> ดูข้อมูล</i></asp:LinkButton>

                                        <%--<asp:LinkButton ID="btnViewDetailOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"> ดูข้อมูล</i></asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <!-- Back To Detail-->
                        <asp:UpdatePanel ID="_PnBackToDetailEmployee" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToDetailEmployee" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToDetailEmployee" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail-->

                        <asp:FormView ID="FvDetailOTDayToEmployee" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT กะหมุนเวียน</h3>
                                    </div>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">ผู้ทำรายการ :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_emp_name_th_viewot" runat="server" CssClass="form-control" placeholder="ผู้ทำรายการ" Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                                </div>
                                                <%-- <label class="col-md-2 control-label">เวลา<span class="text-danger">* :</label>--%>
                                                <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_emp_code_viewot" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน" Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_create_date_viewot" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# Eval("create_date") %>'></asp:TextBox>
                                                </div>
                                                <%-- <label class="col-md-2 control-label">เวลา<span class="text-danger">* :</label>--%>
                                                <label class="col-md-2 control-label">เวลา :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txt_time_create_date_viewot" runat="server" CssClass="form-control" placeholder="เวลา" Enabled="False" Text='<%# Eval("time_create_date") %>'></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <asp:UpdatePanel ID="_PanelDetailOTDayToEmployee" runat="server">
                            <ContentTemplate>

                                <div id="div_DetailOTDayToEmployee" style="overflow-x: scroll; width: auto" runat="server">

                                    <asp:GridView ID="GvDetailOTDayToEmployee" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="True" PageSize="10"
                                        ShowFooter="true">
                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_date_detail_toemp" runat="server" Text='<%# Eval("shift_date") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lbl_parttime_name_th_detail_toemp" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                                    </p>
                                                    <asp:Label ID="lbl_parttime_day_off_detail_toemp" Visible="false" runat="server" Text='<%# Eval("parttime_day_off") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_workdays_detail_toemp" Visible="false" runat="server" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                                    <asp:Label ID="lbl_date_condition_detail_toemp" Visible="false" runat="server" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_detail_toemp" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>

                                                    <%--<asp:Label ID="lbl_STIDX_otday" runat="server" Visible="true" Text='<%# Eval("STIDX") %>'></asp:Label>
                                                    <asp:Label ID="lbl_m0_parttime_idx" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_workdays" runat="server" Visible="false" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                                    <asp:Label ID="lbl_date_condition" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_otday" runat="server" Visible="false" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_end_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <b>รหัสพนักงาน :</b>
                                                        <asp:Label ID="lbl_emp_idx_otday_detail_toemp" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_emp_code_otday_detail_toemp" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ชื่อ-สกุล :</b>
                                                        <asp:Label ID="lbl_emp_name_th_otday_detail_toemp" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ตำแหน่ง :</b>
                                                        <asp:Label ID="lbl_rpos_idx_otday_detail_toemp" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_pos_name_th_otday_detail_toemp" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>หมายเหตุ admin :</b>
                                                        <asp:Label ID="lbl_comment_admin_otday_detail_toemp" runat="server" Text='<%# Eval("comment_admin") %>'></asp:Label>
                                                    </p>

                                                    <%--<p>
                                                        <b>ความเห็น หัวหน้างาน :</b>
                                                        <asp:Label ID="lbl_comment_head_otday_detail_toemp" runat="server" Text='<%# Eval("comment_head") %>'></asp:Label>
                                                    </p>

                                                    <p>
                                                        <b>ความเห็น hr :</b>
                                                        <asp:Label ID="lbl_comment_hr_otday_detail_toemp" runat="server" Text='<%# Eval("comment_hr") %>'></asp:Label>
                                                    </p>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_timestart_detail_toemp" Text='<%# Eval("datetime_start") %>' runat="server"></asp:Label>

                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:Label ID="lbl_shift_timeend_detail_toemp" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                                        <%-- <asp:Label ID="lbl_shift_timeend_uidx" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_timestart_otscan_detail_toemp" runat="server" Text='<%# Eval("datetime_scanot_in") %>'></asp:Label>
                                                        <%--<asp:Label ID="lbl_shift_timestart_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:Label ID="lbl_shift_timeend_otscan_detail_toemp" runat="server" Text='<%# Eval("datetime_scanot_out") %>'></asp:Label>
                                                        <%--<asp:Label ID="lbl_shift_timeend_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีก่อน (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otbefore_detail_toemp" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otbefore_detail_toemp" runat="server" Text='<%# Eval("hours_otbefore") %>'></asp:Label>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีหลัง (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otafter_detail_toemp" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otafter_detail_toemp" runat="server" Text='<%# Eval("hours_otafter") %>'></asp:Label>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีวันหยุด (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otholiday_detail_toemp" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hours_otholiday_detail_toemp" runat="server" Text='<%# Eval("hours_otholiday") %>'></asp:Label>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--<asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_staidx_detail_otday_detail_toemp" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                            <asp:Label ID="lbl_status_name_detail_otday_detail_toemp" runat="server" Text='<%# Eval("status_name") %>' />
                                            <asp:Label ID="lbl_m0_node_idx_detail_otday_detail_toemp" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_node_name_detail_otday_detail_toemp" runat="server" Text='<%# Eval("node_name") %>' />
                                            <br />
                                            โดย<br />
                                            <asp:Label ID="lbl_m0_actor_idx_detail_otday_detail_toemp" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_actor_name_detail_otday_detail_toemp" runat="server" Text='<%# Eval("actor_name") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <!-- Log Detail OT Day -->
                        <br />
                        <div class="panel panel-default" id="div_LogViewDetailDayAdminCreate" runat="server" visible="false">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogViewDetailDayAdminCreate" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log Detail OT Day -->

                    </ContentTemplate>
                </asp:UpdatePanel>



            </div>
        </asp:View>
        <!--View Detail AdminCreate-->

        <!--View Detail Employee by admin Create-->
        <asp:View ID="docDetailEmployeeOTDay" runat="server">
            <div class="col-md-12">
            </div>
        </asp:View>
        <!--View Detail Employee by admin Create-->

        <!-- View HeadUser OT Day Approve -->
        <asp:View ID="docApproveOTDayADminCreate" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelGvWaitApproveOTDayAdminCrate" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-info" id="divheadapprove_otday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">หัวหน้าพิจารณารายการรออนุมัติ OT (กะหมุนเวียน)</h3>
                            </div>
                        </div>

                        <div class="panel panel-info" id="divhrapprove_otday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">เจ้าหน้าที่ฝ่ายทรัพยากรบุคคล พิจารณารายการรออนุมัติ OT (กะหมุนเวียน)</h3>
                            </div>
                        </div>

                        <div class="panel panel-info" id="divadminapprove_otday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">เจ้าหน้าที่ admin แก้ไขรายการรออนุมัติ OT (กะหมุนเวียน)</h3>
                            </div>
                        </div>

                        <div id="div_GvWaitApproveOTDayAdminCrate" style="overflow-x: scroll; width: auto" runat="server">

                            <asp:GridView ID="GvWaitApproveOTDayAdminCrate" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="false" PageSize="10"
                                ShowFooter="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small">

                                        <HeaderTemplate>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:UpdatePanel ID="Pn_HeadApproveAllOtDay" runat="server">
                                                        <ContentTemplate>
                                                            <small>
                                                                <asp:CheckBox ID="chkheadapprove_allotday" runat="server" Text=" เลือก" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                            </small>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>

                                        </HeaderTemplate>

                                        <ItemTemplate>

                                            <%--<asp:Label ID="lbl_no_idx_waitapprove" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>--%>
                                            <asp:CheckBox ID="chkheadapprove_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                            <asp:Label ID="lbl_u0_docday_idx_waitapprove" runat="server" Visible="false" Text='<%# Eval("u0_docday_idx") %>'></asp:Label>
                                            <asp:Label ID="lbl_u1_docday_idx_waitapprove" runat="server" Visible="false" Text='<%# Eval("u1_docday_idx") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_date_waitapprove" runat="server" Text='<%# Eval("shift_date") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_parttime_name_th_waitapprove" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                            </p>
                                            <asp:Label ID="lbl_parttime_day_off_waitapprove" Visible="false" runat="server" Text='<%# Eval("parttime_day_off") %>'></asp:Label>
                                            <asp:Label ID="lbl_parttime_workdays_waitapprove" runat="server" Visible="false" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                            <asp:Label ID="lbl_date_condition_waitapprove" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                            <asp:Label ID="lbl_holiday_idx_waitapprove" runat="server" Visible="false" Text='<%# Eval("holiday_idx") %>'></asp:Label>

                                            <%--<asp:Label ID="lbl_STIDX_otday" runat="server" Visible="true" Text='<%# Eval("STIDX") %>'></asp:Label>
                                                    <asp:Label ID="lbl_m0_parttime_idx" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_workdays" runat="server" Visible="false" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                                    <asp:Label ID="lbl_date_condition" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_otday" runat="server" Visible="false" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_end_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <b>รหัสพนักงาน :</b>
                                                <asp:Label ID="lbl_emp_idx_otday_waitapprove" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_emp_code_otday_waitapprove" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>ชื่อ-สกุล :</b>
                                                <asp:Label ID="lbl_emp_name_th_otday_waitapprove" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>ตำแหน่ง :</b>
                                                <asp:Label ID="lbl_rpos_idx_otday_waitapprove" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_pos_name_th_otday_waitapprove" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                            </p>

                                            <p>
                                                <b>หมายเหตุ admin :</b>
                                                <asp:Label ID="lbl_comment_admin_otday_waitapprove" runat="server" Text='<%# Eval("comment_admin") %>'></asp:Label>
                                            </p>

                                            <%--<p>
                                                <b>ความเห็น หัวหน้างาน :</b>
                                                <asp:Label ID="lbl_comment_head_otday_waitapprove" runat="server" Text='<%# Eval("comment_head") %>'></asp:Label>
                                            </p>

                                            <p>
                                                <b>ความเห็น hr :</b>
                                                <asp:Label ID="lbl_comment_hr_otday_waitapprove" runat="server" Text='<%# Eval("comment_hr") %>'></asp:Label>
                                            </p>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สแกนนิ้วเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_timestart_waitapprove" Text='<%# Eval("datetime_start") %>' runat="server"></asp:Label>

                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สแกนนิ้วออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_shift_timeend_waitapprove" runat="server" Text='<%# Eval("datetime_end") %>'></asp:Label>
                                                <%-- <asp:Label ID="lbl_shift_timeend_uidx" runat="server" Visible="true"></asp:Label>--%>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาสแกนโอทีเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_timestart_otscan_waitapprove" runat="server" Text='<%# Eval("datetime_scanot_in") %>'></asp:Label>
                                                <%--<asp:Label ID="lbl_shift_timestart_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาสแกนโอทีออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_shift_timeend_otscan_waitapprove" runat="server" Text='<%# Eval("datetime_scanot_out") %>'></asp:Label>
                                                <%--<asp:Label ID="lbl_shift_timeend_uidx_otscan" runat="server" Visible="true"></asp:Label>--%>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โอทีก่อน (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_otbefore_waitapprove" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>
                                                <asp:Label ID="lbl_hours_otbefore_waitapprove" runat="server" Text='<%# Eval("hours_otbefore") %>'></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โอทีหลัง (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_otafter_waitapprove" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                                <asp:Label ID="lbl_hours_otafter_waitapprove" runat="server" Text='<%# Eval("hours_otafter") %>'></asp:Label>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="โอทีวันหยุด (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <p>
                                                <asp:Label ID="lbl_shift_otholiday_waitapprove" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                                <asp:Label ID="lbl_hours_otholiday_waitapprove" runat="server" Text='<%# Eval("hours_otholiday") %>'></asp:Label>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_staidx_detail_otday_waitapprove" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                            <asp:Label ID="lbl_status_name_detail_otday_waitapprove" runat="server" Text='<%# Eval("status_name") %>' />
                                            <asp:Label ID="lbl_m0_node_idx_detail_otday_waitapprove" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_node_name_detail_otday_waitapprove" runat="server" Text='<%# Eval("node_name") %>' />
                                            <br />
                                            โดย<br />
                                            <asp:Label ID="lbl_m0_actor_idx_detail_otday_waitapprove" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                            <asp:Label ID="lbl_actor_name_detail_otday_waitapprove" runat="server" Text='<%# Eval("actor_name") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความเห็น" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="txt_comment_waitapprove" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="2" placeholder="ความเห็น ..."></asp:TextBox>

                                            <%--<asp:TextBox ID="txt_remarkhead_shift_waitapprove" runat="server" CssClass="form-control" Text='<%# Eval("comment_head") %>'
                                                TextMode="MultiLine" Rows="2" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>--%>

                                            <%--<asp:TextBox ID="txt_remarkhr_shift_waitapprove" runat="server" CssClass="form-control" Text='<%# Eval("comment_hr") %>'
                                                TextMode="MultiLine" Rows="2" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                            <asp:TextBox ID="txt_remarkadmin_shift_waitapprove" runat="server" CssClass="form-control" Text='<%# Eval("comment_admin") %>'
                                                TextMode="MultiLine" Rows="2" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>

                        <div id="div_btnApproveHeadAdmincreate" runat="server">
                            <label>&nbsp;</label>
                            <div class="clearfix"></div>
                            <div class="form-group pull-left">

                                <asp:Repeater ID="rptBindbtnApproveOtDayAdmin" OnItemDataBound="rptOnRowDataBound" runat="server">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcheck_colerotday_admincreate" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                        <asp:LinkButton ID="btnHeadUserApproveOTDayAdminCreate" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")%>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApproveOTDayAdminCreate"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <%--<asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdAdminCreateOTDay" ValidationGroup="SaveAdminCreateOTDay"></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton3" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="2" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelAdminCreateOTDay"></asp:LinkButton>--%>
                            </div>
                        </div>

                        <div id="div_btnApproveAdminEditOTDay" runat="server">
                            <label>&nbsp;</label>
                            <div class="clearfix"></div>
                            <div class="form-group pull-left">

                                <asp:LinkButton ID="btnSaveApproveOTDayAdminEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการเปลี่ยนแปลง" OnCommand="btnCommand" CommandArgument="17" CommandName="cmdSaveApproveOTDayAdminCreate" ValidationGroup="SaveAdminCreateOTDay"></asp:LinkButton>

                                <%--<asp:LinkButton ID="LinkButton3" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="2" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelAdminCreateOTDay"></asp:LinkButton>--%>
                            </div>
                        </div>



                    </ContentTemplate>
                </asp:UpdatePanel>




            </div>
        </asp:View>
        <!-- View HeadUser Approve -->

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <asp:UpdatePanel ID="_PanelCreateOTDay" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Admin สร้างรายการ OT (กะหมุนเวียน) </h3>
                            </div>

                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>เลือกวันที่สร้างรายการ</label>
                                            <div class="input-group date">
                                                <%--<asp:HiddenField ID="txtSearchDateStart_createHidden" runat="server" CssClass="form-control datetimepicker-search" />--%>
                                                <%--<asp:TextBox ID="txtDateStart_create" runat="server" placeholder="เลือกวันที่..." CssClass="form-control datetimepicker-fromsearch cursor-pointer" />--%>
                                                <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="เลือกวันที่..." CssClass="form-control datetimepicker-fromsearch cursor-pointer" value="" />
                                                <span class="input-group-addon show-fromsearch-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>

                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchDayCreateOT" runat="server" CssClass="btn btn-primary col-md-12" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchDayCreateOT" />
                                        </div>
                                    </div>


                                    <%-- <div class="col-md-12">
                                        <div class="form-group pull-left">
                                            <asp:LinkButton ID="btnSearchDayCreateOT" CssClass="btn btn-primary" runat="server" data-original-title="Search" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSearchDayCreateOT" ValidationGroup="SearchDayCreateOT">
                                                <span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                        </div>
                                    </div>--%>
                                </div>

                            </div>
                        </div>


                        <asp:UpdatePanel ID="_PanelDetailCreateOTDay" runat="server">
                            <ContentTemplate>

                                <div id="div_PanelDetailCreateOTDay" style="overflow: auto" runat="server">

                                    <asp:GridView ID="GvDetailCreateOTDay" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="True" PageSize="31"
                                        ShowFooter="true">
                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:CheckBox ID="chk_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_date" runat="server" Text='<%# Eval("shift_date") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lbl_parttime_name_th" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                                    </p>
                                                    <asp:Label ID="lbl_STIDX_otday" runat="server" Visible="false" Text='<%# Eval("STIDX") %>'></asp:Label>
                                                    <asp:Label ID="lbl_emp_type_idx" runat="server" Visible="false" Text='<%# Eval("emp_type_idx") %>'></asp:Label>
                                                    <%--<asp:Label ID="lbl_m0_parttime_idx" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx") %>'></asp:Label>--%>
                                                    <asp:Label ID="lbl_parttime_day_off" runat="server" Visible="false" Text='<%# Eval("parttime_day_off") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_workdays" runat="server" Visible="false" Text='<%# Eval("parttime_workdays") %>'></asp:Label>
                                                    <asp:Label ID="lbl_date_condition" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                    <asp:Label ID="lbl_holiday_idx_otday" runat="server" Visible="false" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_end_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_start_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_start_time") %>'></asp:Label>
                                                    <asp:Label ID="lbl_parttime_break_end_time" runat="server" Visible="false" Text='<%# Eval("parttime_break_end_time") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียดพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <b>รหัสพนักงาน :</b>
                                                        <asp:Label ID="lbl_emp_idx_otday" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_emp_code_otday" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ชื่อ-สกุล :</b>
                                                        <asp:Label ID="lbl_emp_name_th_otday" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <b>ตำแหน่ง :</b>
                                                        <asp:Label ID="lbl_rpos_idx_otday" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_pos_name_th_otday" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:DropDownList ID="ddl_shifttimestart" runat="server" CssClass="form-control" Style="word-wrap: break-word; white-space: normal; width: 120px; height: 50px;" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        <asp:Label ID="lbl_shift_timestart" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_shift_timestart_uidx" runat="server" Visible="false"></asp:Label>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สแกนนิ้วออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:DropDownList ID="ddl_shifttimeend" runat="server" CssClass="form-control" Style="word-wrap: break-word; white-space: normal; width: 120px; height: 50px;" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        <asp:Label ID="lbl_shift_timeend" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_shift_timeend_uidx" runat="server" Visible="false"></asp:Label>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีเข้า" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <small>
                                                            <asp:DropDownList ID="ddl_shifttimestart_ot" runat="server" Enabled="false" CssClass="form-control" Style="word-wrap: break-word; white-space: normal; width: 120px; height: 50px;" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                            <asp:Label ID="lbl_shift_timestart_otscan" runat="server"></asp:Label>
                                                            <asp:Label ID="lbl_shift_timestart_uidx_otscan" runat="server" Visible="false"></asp:Label>
                                                        </small>
                                                    </p>



                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เวลาสแกนโอทีออก" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:DropDownList ID="ddl_shifttimeend_ot" runat="server" Enabled="false" CssClass="form-control" Style="word-wrap: break-word; white-space: normal; width: 120px; height: 50px;" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        <asp:Label ID="lbl_shift_timeend_otscan" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_shift_timeend_uidx_otscan" runat="server" Visible="false"></asp:Label>
                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีก่อน (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otbefore" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_hours_otbefore" runat="server"></asp:Label>

                                                    </p>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีหลัง (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otafter" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_hours_otafter" runat="server"></asp:Label>

                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โอทีวันหยุด (ชม)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <p>
                                                        <asp:Label ID="lbl_shift_otholiday" runat="server"></asp:Label>
                                                        <asp:Label ID="lbl_hours_otholiday" runat="server"></asp:Label>
                                                    </p>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_remark_shift_otholiday" runat="server" CssClass="form-control" Style="overflow: auto" Rows="2" TextMode="MultiLine" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>

                                <div id="div_btnAdminCreateOTDay" runat="server">
                                    <label>&nbsp;</label>
                                    <div class="clearfix"></div>
                                    <div class="form-group pull-right">
                                        <asp:LinkButton ID="btnAdminCreateOTDay" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdAdminCreateOTDay" ValidationGroup="SaveAdminCreateOTDay"> <i class="fas fa-save"></i> บันทึก</asp:LinkButton>

                                        <asp:LinkButton ID="btnCancelAdminCreateOTDay" CssClass="btn btn-danger" Visible="false" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="2" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelAdminCreateOTDay"></asp:LinkButton>
                                    </div>
                                </div>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelPermissionCreateOTShift2" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-danger ">
                            <div class="panel-heading">
                                <h2 class="panel-title">
                                    <center>--- คุณไม่มีสิทธิ์สร้างรายการ ---</center>
                                </h2>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelCreateDay" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างรายการ OT(แบบรายวัน) </h3>
                            </div>

                            <div class="panel-body">

                                <%--  <div id="div_selected_day" class="panel panel-default" runat="server">
                                    <div class="panel-body">
                                        <div class="col-md-10 col-md-offset-1">--%>

                                <div class="col-md-12">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label>เลือกกลุ่ม</label>
                                            <asp:DropDownList ID="ddlgroup_otday" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchGroupOT" runat="server" CssClass="btn btn-primary col-md-12"
                                                Text="ค้นหากลุ่ม" OnCommand="btnCommand" CommandName="cmdSearchGroupOT" ValidationGroup="SearchGroupDetailCreate" />
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <%--</div>

                                    </div>

                                </div>--%>
                                <asp:UpdatePanel ID="UpdatePanel_CreateOTDay" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>วันที่เริ่มทำ</label>
                                                    <div class="input-group date">

                                                        <asp:TextBox ID="txt_datestart_otday" placeholder="วันที่เริ่ม ..." runat="server" CssClass="form-control datetimepicker-from cursor-pointer" />
                                                        <span class="input-group-addon show-from-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ถึงวันที่</label>
                                                    <div class="input-group date">

                                                        <asp:TextBox ID="txt_dateend_otday" placeholder="ถึงวันที่ ..." runat="server" CssClass="form-control datetimepicker-to cursor-pointer" />
                                                        <span class="input-group-addon show-to-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>เวลาที่เริ่ม</label>
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_timestart_otday" placeholder="เวลา ..." runat="server" ValidationGroup="HeadSaveSetOTMonth"
                                                            CssClass="form-control clockpicker cursor-pointer" value="" />
                                                        <span class="input-group-addon show-time-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                            runat="server"
                                                            ControlToValidate="txt_timestart_form" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกเวลา"
                                                            ValidationGroup="HeadSaveSetOTMonth" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_form" Width="160" PopupPosition="BottomLeft" />--%>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ถึงเวลา</label>
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_timeend_otday" placeholder="เวลา ..." runat="server" ValidationGroup="HeadSaveSetOTMonth"
                                                            CssClass="form-control clockpickerto cursor-pointer" value="" />
                                                        <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                        <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                            runat="server"
                                                            ControlToValidate="txt_timestart_form" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกเวลา"
                                                            ValidationGroup="HeadSaveSetOTMonth" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_form" Width="160" PopupPosition="BottomLeft" />--%>
                                                    </div>
                                                </div>

                                            </div>

                                            <%--<div class="col-md-6">
                                                <div class="form-group">
                                                    <label>เลือกประเภท OT</label>
                                                    <asp:DropDownList ID="ddl_type_otday" runat="server" CssClass="form-control" Visible="false">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>--%>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>รายชื่อพนักงาน</label>
                                                    <asp:GridView ID="GvGroupCreateOTDay" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        AllowPaging="True" PageSize="31"
                                                        ShowFooter="False">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <asp:CheckBox ID="chk_otday" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_m1_group_idx_createotday" runat="server" Visible="false" Text='<%# Eval("m1_group_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_m0_group_idx_createotday" runat="server" Visible="false" Text='<%# Eval("m0_group_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_emp_code_createotday" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อ - สกุล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_emp_name_th_createotday" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_dept_name_th_createotday" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_sec_name_th_createotday" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>

                                                    <div class="form-group pull-right">

                                                        <asp:Repeater ID="rptBindbtnSaveOTDay" OnItemDataBound="rptOnRowDataBound" runat="server">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbcheck_colerotday" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                                                <asp:LinkButton ID="btnSaveOTDay" CssClass="btn btn-success" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")+ ";" + Eval("decision_name") %>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdSaveOTDay"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:Repeater>

                                                        <%-- <asp:LinkButton ID="btnSaveOTDay" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveOTDay" ValidationGroup="SearchGroupDetailCreate"></asp:LinkButton>--%>

                                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="1" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelCreateMonth" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างรายการ OT(ประเภทกะคงที่) </h3>
                            </div>

                            <div class="panel-body">
                                <div id="div_selectedmonth" class="panel panel-default" runat="server">
                                    <div class="panel-body">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="col-md-12">

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <label>เลือกเดือน</label>
                                                        <asp:DropDownList ID="ddl_month_search" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                            <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                            <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                            <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                            <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                            <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                            <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                            <asp:ListItem Value="07">กรกฎาคม</asp:ListItem>
                                                            <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                            <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                            <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                            <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                            <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <%--<p class="help-block"><font color="red"> หมายเหตุ : สามารถเลือกเดือนก่อนหน้า 1 เดือนเท่านั้น!!!</font></p>--%>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>&nbsp;</label>
                                                        <div class="clearfix"></div>
                                                        <asp:LinkButton ID="btnSearchMonth" runat="server" CssClass="btn btn-primary col-md-12"
                                                            Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchMonth" ValidationGroup="SearchMonthDetailCreate" />
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="form-group" id="div_showfile" runat="server" style="color: transparent;">
                                                        <label class="f-s-13"></label>
                                                        <asp:FileUpload ID="FileUpload11" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <asp:UpdatePanel ID="_PanelOTMonth" runat="server">
                                    <ContentTemplate>

                                        <div id="div_scrollCreateOTMonth" style="overflow-x: auto; width: 100%" runat="server">
                                            <asp:GridView ID="GvCreateOTMonth" runat="server"
                                                AutoGenerateColumns="False"
                                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                OnRowDataBound="gvRowDataBound"
                                                AllowPaging="False" PageSize="31"
                                                ShowFooter="true">
                                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                <RowStyle Font-Size="Small" />
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:CheckBox ID="chk_date_ot" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <p>
                                                                <asp:Label ID="lbl_break_start" Visible="false" runat="server" Text='<%# Eval("break_start") %>'></asp:Label>
                                                                <asp:Label ID="lbl_break_end" Visible="false" runat="server" Text='<%# Eval("break_finish") %>'></asp:Label>
                                                                <asp:Label ID="lbl_date_start" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>
                                                                <asp:Label ID="lbl_detailholiday" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                                <asp:Label ID="lbl_detaildayoff" runat="server"><span class="text-danger">*(DH)</span></asp:Label>
                                                                <%-- <label class="col-md-2 control-label"><span class="text-danger">*</span></label>--%>
                                                                <asp:Label ID="lbl_holiday_idx_check" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>


                                                                <asp:Label ID="lbl_day_off" runat="server" Visible="false" Text='<%# Eval("day_off") %>'></asp:Label>
                                                                <asp:Label ID="lbl_date_condition" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>
                                                            </p>
                                                            <p>
                                                                <asp:Label ID="lbl_TypeWork" runat="server" Text='<%# Eval("TypeWork") %>'></asp:Label>
                                                                <asp:Label ID="lbl_work_start" runat="server" Visible="false" Text='<%# Eval("work_start") %>'></asp:Label>
                                                                <asp:Label ID="lbl_work_finish" runat="server" Visible="false" Text='<%# Eval("work_finish") %>'></asp:Label>
                                                            </p>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="งานที่ปฏิบัติ" ItemStyle-Width="12%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txt_job" Visible="false" runat="server" TextMode="MultiLine" Rows="2" Style="overflow: auto" CssClass="form-control" ValidationGroup="SaveCreateEmployeeOTMonth" placeholder="กรอกงานที่ปฏิบัติ ..."></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="Re_txt_job"
                                                                runat="server"
                                                                ControlToValidate="txt_job" Display="None" SetFocusOnError="true"
                                                                ErrorMessage="*กรุณากรอกงานที่ปฏิบัติ"
                                                                ValidationGroup="SaveCreateEmployeeOTMonth" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender61111" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_job" Width="160" PopupPosition="BottomLeft" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-Width="13%" HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                                                <ContentTemplate>
                                                                    <%--<div class="input-group time">

                                                                    <asp:TextBox ID="txt_timestart_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_start") %>' Enabled="false" CssClass="form-control clockpicker cursor-pointer" />
                                                                    <span class="input-group-addon show-time-onclick cursor-pointer">
                                                                        <span class="glyphicon glyphicon-time"></span>
                                                                    </span>
                                                                    
                                                                </div>--%>
                                                                    <asp:HiddenField ID="hfddl_timestart" runat="server" Value='<%# Eval("month_idx") %>' />
                                                                    <asp:DropDownList ID="ddl_timestart" runat="server" Width="160px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-Width="13%" HeaderStyle-Width="13%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="Update_Timeend" runat="server">
                                                                <ContentTemplate>
                                                                    <%--<div class="input-group time">
                                                                    <asp:TextBox ID="txt_timeend_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_end") %>' Enabled="false" CssClass="form-control clockpickerto cursor-pointer" />

                                                                    <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                                        <span class="glyphicon glyphicon-time"></span>
                                                                    </span>
                                                                </div>--%>
                                                                    <asp:HiddenField ID="hfddl_timeend" runat="server" />
                                                                    <asp:DropDownList ID="ddl_timeend" runat="server" Width="160" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                    </asp:DropDownList>

                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: right;">
                                                                <asp:Literal ID="lit_timedetail" runat="server" Text="เวลารวม :"></asp:Literal>
                                                            </div>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OT ก่อน" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:Panel Style="text-align: right; background-color: none;" ID="color_otbefore" runat="server">
                                                                <asp:Label ID="txtot_before" Enabled="false" runat="server" Visible="false"></asp:Label>
                                                                <asp:DropDownList ID="ddl_timebefore" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>

                                                                <asp:Label ID="lblot_before" runat="server" Font-Bold="true"></asp:Label>
                                                            </asp:Panel>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_before" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Panel Style="text-align: right; background-color: none;" ID="color_otholiday" runat="server">
                                                                <asp:Label ID="txtot_holiday" Enabled="false" runat="server" Visible="false"></asp:Label>
                                                                <asp:DropDownList ID="ddl_timeholiday" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblot_holiday" runat="server" Font-Bold="true"></asp:Label>
                                                            </asp:Panel>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_holiday" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OT หลัง" ItemStyle-Width="8%" HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Panel Style="text-align: right; background-color: none;" ID="color_otafter" runat="server">
                                                                <asp:Label ID="txtot_after" Enabled="true" runat="server" Visible="false"></asp:Label>
                                                                <asp:DropDownList ID="ddl_timeafter" runat="server" Width="100px" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblot_after" runat="server" Font-Bold="true"></asp:Label>
                                                            </asp:Panel>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_after" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <div style="text-align: right;">
                                                                <asp:TextBox ID="txt_sum_hour" runat="server" CssClass="form-control" Enabled="false" placeholder="รวม(ชั่วโมง) ..."></asp:TextBox>
                                                            </div>
                                                            <%-- <asp:TextBox ID="txt_sum_hour" runat="server" CssClass="form-control clockpickertotal cursor-pointer" Enabled="false" Text='<%# Eval("sum_time") %>' placeholder="รวม(ชั่วโมง) ..."></asp:TextBox>--%>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursmonth" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="Right" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txt_head_set" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("sum_hours_setotmonth") %>' placeholder="OT หัวหน้า Set ..."></asp:TextBox>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txt_remark" TextMode="MultiLine" Rows="2" Style="overflow: auto" Visible="false" runat="server" CssClass="form-control" placeholder="กรอกหมายเหตุ ..."></asp:TextBox>

                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <div style="text-align: left;">
                                                                <asp:Literal ID="lit_hours_detail" runat="server" Text="ชั่วโมง"></asp:Literal>
                                                            </div>
                                                        </FooterTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>

                                        <div class="form-group" id="div_DetailHolday" runat="server">
                                            <p class="help-block"><font color="red"> หมายเหตุ : *(HH) คือวันหยุดบริษัท / *(DH) คือวันหยุดของพนักงาน</font></p>
                                        </div>


                                        <div class="form-group" id="div_Addfileotmonth" runat="server" visible="false">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                            <label class="f-s-13">แนบไฟล์ขอ OT ย้อนหลัง (*** ต้องแนบไฟล์เนื่องจากสร้าง OT หลังวันที่ 12 ***)</label>

                                            <asp:FileUpload ID="UploadFileMemo" CssClass="control-label multi max-1 accept-png|jpg|pdf maxsize-1024 with-preview"
                                                ValidationGroup="SaveCreateEmployeeOTMonth" runat="server" />
                                            <%--<asp:FileUpload ID="UploadFileMemo" ClientIDMode="Static" ViewStateMode="Enabled" CssClass="control-label multi max-1 accept-png|jpg|pdf maxsize-1024  with-preview"
                                                ValidationGroup="SaveCreateEmployeeOTMonth"
                                                runat="server" />--%>

                                            <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png, pdf</font></p>

                                            <%--<asp:RequiredFieldValidator ID="Re_UploadFileMemo"
                                                runat="server"
                                                ControlToValidate="UploadFileMemo" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาแนบไฟล์ขอ OT ย้อนหลัง"
                                                ValidationGroup="SaveCreateEmployeeOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6sss" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_UploadFileMemo" Width="160" PopupPosition="BottomLeft" />--%>

                                            <%--<asp:RequiredFieldValidator ID="Req_UploadFileMemo"
                                                runat="server" ControlToValidate="UploadFileMemo"
                                                Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาแนบ Memo"
                                                ValidationGroup="SaveCreateEmployeeOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" />--%>


                                            <%--<asp:FileUpload ID="myFileUpload" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" runat="server" />--%>
                                            <%-- <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png</font></p>--%>
                                        </div>
                                        <label>&nbsp;</label>
                                        <div class="clearfix"></div>
                                        <div class="form-group pull-right">
                                            <asp:LinkButton ID="btnSaveOTMonth" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveOTMonth" ValidationGroup="SaveCreateEmployeeOTMonth"></asp:LinkButton>

                                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="2" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                        </div>

                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveOTMonth" />
                                    </Triggers>

                                </asp:UpdatePanel>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

        </asp:View>
        <!--View Create-->

        <!--View Set OT Month-->
        <asp:View ID="docSetOTMonth" runat="server">
            <div class="col-md-12">

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetailSetOTMonth" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <asp:UpdatePanel ID="_PanelSetOTMonth" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">หัวหน้าตั้งค่า OT(แบบรายเดือน) </h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>วันที่เริ่ม</label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_set_datestart" runat="server" ValidationGroup="HeadSaveSetOTMonth" placeholder="วันที่เริ่ม ..." CssClass="form-control datetimepicker-from cursor-pointer" />

                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Re_txt_set_datestart"
                                                    runat="server"
                                                    ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                    ValidationGroup="HeadSaveSetOTMonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงวันที่</label>
                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_set_dateend" runat="server" placeholder="ถึงวันที่..." ValidationGroup="HeadSaveSetOTMonth" CssClass="form-control datetimepicker-to cursor-pointer" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Re_txt_set_dateend"
                                                    runat="server"
                                                    ControlToValidate="txt_set_dateend" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="HeadSaveSetOTMonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_dateend" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เวลาที่เริ่ม</label>
                                            <div class="input-group time">
                                                <asp:TextBox ID="txt_timestart_form" placeholder="เวลา ..." runat="server" ValidationGroup="HeadSaveSetOTMonth"
                                                    CssClass="form-control clockpicker cursor-pointer" value="" />
                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Re_txt_timestart_form"
                                                    runat="server"
                                                    ControlToValidate="txt_timestart_form" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลา"
                                                    ValidationGroup="HeadSaveSetOTMonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_form" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงเวลา</label>

                                            <div class="input-group time">

                                                <asp:TextBox ID="txt_timeend_to" placeholder="เวลา ..." runat="server" ValidationGroup="HeadSaveSetOTMonth"
                                                    CssClass="form-control clockpickerto cursor-pointer" value="" />
                                                <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Re_txt_timeend_to"
                                                    runat="server"
                                                    ControlToValidate="txt_timeend_to" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลา"
                                                    ValidationGroup="HeadSaveSetOTMonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timeend_to" Width="160" PopupPosition="BottomLeft" />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:CheckBox ID="chkAllEmployee" runat="server" Visible="false" Text=" เลือกทั้งหมด" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true" RepeatDirection="Vertical" />
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>รายชื่อพนักงาน</label>
                                            <div id="divGvTopicDetailMa_scroll_x" style="overflow-x: scroll; width: auto" runat="server">
                                                <div id="divGvTopicDetailMa_scroll_y" style="overflow-y: scroll; width: auto; height: auto" runat="server">
                                                    <asp:GridView ID="GvEmployeeSetOTMonth" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        AllowPaging="False" PageSize="5"
                                                        ShowFooter="False">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </small>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="เลือกพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="check_employee" runat="server" Checked="true" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"></asp:CheckBox>
                                                                    <%--<asp:CheckBox ID="check_employee" runat="server" Checked="true" OnCheckedChanged="onSelectedCheckChanged" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"
                                                                        AutoPostBack="true"></asp:CheckBox>--%>
                                                                    <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_empcode_setotmonth" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อ - สกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <p>
                                                                        <b>ชื่อ-สกุล:</b>
                                                                        <asp:Label ID="lbl_emp_name_th_setotmonth" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                                    </p>
                                                                    <p>
                                                                        <b>องค์กร:</b>
                                                                        <asp:Label ID="lbl_org_name_th_setotmonth" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                                    </p>
                                                                    <p>
                                                                        <b>ฝ่าย:</b>
                                                                        <asp:Label ID="lbl_dept_name_th_setotmonth" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                                                    </p>
                                                                    <p>
                                                                        <b>แผนก:</b>
                                                                        <asp:Label ID="lbl_sec_name_th_setotmonth" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                                    </p>
                                                                    <p>
                                                                        <b>ตำแหน่ง:</b>
                                                                        <asp:Label ID="lbl_pos_name_th_setotmonth" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                                                    </p>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <asp:LinkButton ID="btnHeadSaveSetOTMonth" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdHeadSaveSetOTMonth" ValidationGroup="HeadSaveSetOTMonth"></asp:LinkButton>

                                            <asp:LinkButton ID="btnCancelHeadSaveSetOTMonth" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" CommandArgument="2" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View Set OT Month-->

        <!--View Wait Approve -->
        <asp:View ID="docWaitApprove" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelWaitApproveDay" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-info" id="_div_headerwaitapprove_otday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการรออนุมัติ OT รายวัน</h3>
                            </div>
                        </div>

                        <!-- Back To Detail To Day -->
                        <asp:UpdatePanel ID="Update_BackToWaitApproveOTDay" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToWaitApproveOTDay" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToWaitApprove" CommandArgument="1" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail To Day -->

                        <asp:GridView ID="GvWaitDetailOT" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <p>
                                            <asp:Label ID="lbl_create_date_detail_waitotday" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lbl_time_create_date_detail_waitotday" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_u0_document_idx_detail_waitotday" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detail_waitotday" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>องค์กร:</b>
                                            <asp:Label ID="lbl_org_name_th_detail_waitotday" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>ฝ่าย:</b>
                                            <asp:Label ID="lbl_dept_name_th_detail_waitotday" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <p>
                                            <b>ชื่อกลุ่ม:</b>

                                            <asp:Label ID="lbl_group_name_detail_waitotday" Visible="true" runat="server" Text='<%# Eval("group_name") %>' />
                                        </p>
                                        <%--<p>
                                            <b>จำนวน:</b>
                                            <asp:Label ID="lbl_count_emp_detail_waitotday" Visible="true" runat="server" Text='<%# Eval("count_emp")%>' />
                                        </p>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_waitotday" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_waitotday" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_waitotday" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_waitotday" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_waitotday" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_waitotday" runat="server" Text='<%# Eval("actor_name") %>' />

                                        <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:UpdatePanel ID="Update_PnbtnViewDetailWaitApproveOTDay" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnViewDetailWaitApproveOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailWaitApproveOTDay"
                                                    OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                            </ContentTemplate>


                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <asp:FormView ID="FvDetailOTDayWaitapprove" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT รายวัน</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_u0_document_idx_view" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                                    <asp:Label ID="lbl_emp_code_view" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                                    <asp:Label ID="lbl_cemp_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />


                                                </div>

                                                <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_org_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_org_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                                </div>

                                                <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_dept_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_rdept_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_sec_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                                    <asp:Label ID="lbl_rsec_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_pos_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                                    <asp:Label ID="lbl_rpos_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>

                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_email_view" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                                </div>

                                                <%-- <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_costcenter_no_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                                    <asp:Label ID="lbl_costcenter_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                                </div>--%>


                                                <%-- </div>--%>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <asp:GridView ID="GvViewWaitApproveOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            AllowPaging="True" PageSize="31"
                            ShowFooter="False">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lbl_dateot_from_approve" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lbl_detail_job_approve" runat="server" Text='<%# Eval("detail_job") %>'></asp:Label>--%>
                                        <%--<asp:TextBox ID="txt_job" runat="server" CssClass="form-control" Text='<%# Eval("detail_job") %>' ></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายชื่อ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lbl_detail_job_approve" runat="server" Text='<%# Eval("detail_job") %>'></asp:Label>--%>
                                        <%--<asp:TextBox ID="txt_job" runat="server" CssClass="form-control" Text='<%# Eval("detail_job") %>' ></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จำนวนรวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <%--<asp:Label ID="lbl_time_form_approve" runat="server" Text='<%# Eval("time_form") %>'></asp:Label>--%>

                                        <%-- <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">

                                                    <asp:TextBox ID="txt_timestart_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_start") %>' Enabled="false" CssClass="form-control clockpicker cursor-pointer" />
                                                    <span class="input-group-addon show-time-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                 
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                    </ItemTemplate>



                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <%--<asp:Label ID="lbl_time_to_approve" runat="server" Text='<%# Eval("time_to") %>'></asp:Label>--%>

                                        <%--<asp:UpdatePanel ID="Update_Timeend" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">
                                                    <asp:TextBox ID="txt_timeend_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_end") %>' Enabled="false" CssClass="form-control clockpickerto cursor-pointer" />

                                                    <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                    </ItemTemplate>

                                    <%--<FooterTemplate>
                                        <div style="text-align: right;">
                                            <asp:Literal ID="lit_timedetail_approve" runat="server" Text="เวลารวม :"></asp:Literal>
                                        </div>
                                    </FooterTemplate>--%>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lbl_sum_hours_approve" runat="server" Text='<%# Eval("sum_hours") %>'></asp:Label>--%>
                                        <%--<asp:TextBox ID="txt_sum_hour" runat="server" CssClass="form-control clockpickertotal cursor-pointer" Enabled="false" Text='<%# Eval("sum_time") %>' placeholder="รวม(ชั่วโมง) ..."></asp:TextBox>--%>
                                    </ItemTemplate>

                                    <%--<FooterTemplate>
                                        <div style="text-align: right; background-color: chartreuse;">
                                            <asp:Label ID="lit_total_hoursmonth_approve" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </FooterTemplate>--%>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <%--<asp:Label ID="lbl_comment_approve" runat="server" Text='<%# Eval("comment") %>'></asp:Label>--%>
                                    </ItemTemplate>

                                    <%--<FooterTemplate>
                                        <div style="text-align: left;">
                                            <asp:Literal ID="lit_hours_detail_approve" runat="server" Text="ชั่วโมง"></asp:Literal>
                                        </div>
                                    </FooterTemplate>--%>

                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <asp:FormView ID="FvHeadUserApproveOTDay" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <InsertItemTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ส่วนของหัวหน้างานพิจารณารายการ OT (รายวัน)</h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-md-8 col-md-offset-2">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Comment ผู้มีสิทธิ์อนุมัติ</label>
                                                    <asp:TextBox ID="txt_comment_approve_otday" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ผู้มีสิทธิ์อนุมัติ ..." />

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <asp:Repeater ID="rptBindbtnApproveOtDay" OnItemDataBound="rptOnRowDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbcheck_colerotday" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnHeadUserSaveApproveOTDay" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")+ ";" + Eval("decision_name") %>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApproveOTDay"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvHrApproveOTDay" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <InsertItemTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ส่วนของ HR ตรวจสอบรายการ OT (รายวัน)</h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-md-8 col-md-offset-2">

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Comment</label>
                                                    <asp:TextBox ID="txt_comment_hrapprove_otday" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ..." />

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <asp:Repeater ID="rptBindHRbtnApproveOTDay" OnItemDataBound="rptOnRowDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lb_hrcheck_colerotday" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnHRSaveApproveOTDay" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")+ ";" + Eval("decision_name") %>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdHrSaveApproveOTDay"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>


                        <!-- Log Detail OT Wait Day -->
                        <div class="panel panel-info" id="_div_LogViewDetailWaitDay" runat="server" visible="false">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogWaitViewDetailDay" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log Detail OT Wait Day -->



                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="_PanelWaitApproveMonth" runat="server">
                    <ContentTemplate>

                        <div class="form-group" id="div_SearchWaitApproveOTMonth" runat="server">
                            <asp:LinkButton ID="btnSearchWaitApproveOTMonth" CssClass="btn btn-info" runat="server" data-original-title="แสดงแถบเครื่องมือค้นหา" data-toggle="tooltip" CommandName="cmdSearchWaitApproveOTMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> แสดงแถบเครื่องมือค้นหา</asp:LinkButton>

                        </div>

                        <!-- Back To Detail WaitApprove OT Month -->
                        <asp:UpdatePanel ID="_PanelCancelSearchWaitApproveOTMonth" runat="server">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnCancelSearchWaitApproveOTMonth" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                                        CommandName="cmdCancelSearchWaitApproveOTMonth" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>

                                  

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail WaitApprove OT Month -->

                        <asp:UpdatePanel ID="_PanelSearchWaitApprove" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ค้นหารายการรออนุมัติ OT ประเภทกะคงที่</h3>
                                    </div>

                                    <div class="panel-body">

                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>เดือน</label>
                                                    <asp:DropDownList ID="ddlSearchMonth_WaitApprove" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                        <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                        <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                        <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                        <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                        <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                        <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                        <asp:ListItem Value="07">กรกฎาคม</asp:ListItem>
                                                        <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                        <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                        <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                        <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                        <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                    </asp:DropDownList>

                                                   <%-- <asp:RequiredFieldValidator ID="Req_ddlSearchMonth_WaitApprove"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlSearchMonth_WaitApprove" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกเดือน"
                                                        ValidationGroup="SearchWaitApprove" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlSearchMonth_WaitApprove" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ปี</label>
                                                    <asp:DropDownList ID="ddlSearchYear_WaitApprove" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>องค์กร</label>
                                                    <asp:DropDownList ID="ddlSearchorg_WaitApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <%--<p class="help-block"><font color="red">**กรุณาเลือกองค์กร</font></p>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorg" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorg" Width="160" PopupPosition="BottomLeft" />--%>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ฝ่าย</label>
                                                    <asp:DropDownList ID="ddlSearch_rdept_WaitApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>แผนก</label>
                                                    <asp:DropDownList ID="ddlSearch_rsec_WaitApprove" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ชื่อพนักงาน</label>
                                                    <asp:DropDownList ID="ddlSearch_emp_WaitApprove" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="--- เลือกพนักงาน ---" Value="0"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Costcenter</label>
                                                    <asp:TextBox ID="txt_SearchCost_WaitApprove" placeholder="Coscenter" runat="server" CssClass="form-control"> </asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>รหัสพนักงาน</label>
                                                    <asp:TextBox ID="txt_SearchEmpCode_WaitApprove" placeholder="รหัสพนักงาน" runat="server" MaxLength="8" CssClass="form-control"> </asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <asp:LinkButton ID="btnSearchWaitApprove" runat="server" CssClass="btn btn-primary" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchWaitApprove" CommandName="cmdSearchWaitApprove" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                     <asp:LinkButton ID="btnResetSearchWaitApproveOTMonth" runat="server" CssClass="btn btn-default" data-original-title="ล้างค่า" data-toggle="tooltip" ValidationGroup="SearchDetailOTmonth" CommandName="cmdResetSearchWaitApproveOTMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>
				


                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="panel panel-info" id="_div_headerwaitapprove" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการรออนุมัติ OT ประเภทกะคงที่</h3>
                            </div>
                        </div>

                        <!-- Back To Detail To Month -->
                        <asp:UpdatePanel ID="Update_BackToWaitApprove" runat="server" Visible="false">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackToWaitApprove" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackToWaitApprove" CommandArgument="2" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To Detail To Month -->

                        <asp:GridView ID="GvWaitDetailOTMonth" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="False"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <p>
                                            <asp:Label ID="lbl_create_date_detailwait" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lbl_time_create_date_detailwait" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_u0_document_idx_detailwait" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detailwait" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>องค์กร:</b>
                                            <asp:Label ID="lbl_org_name_th_detailwait" runat="server" Text='<%# Eval("org_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>ฝ่าย:</b>
                                            <asp:Label ID="lbl_dept_name_th_detailwait" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <p>
                                            <b>ประจำเดือน:</b>
                                            <asp:Label ID="lbl_month_ot_detailwait" Visible="false" runat="server" Text='<%# Eval("month_ot") %>' />
                                            <asp:Label ID="lbl_month_name_detailwait" Visible="true" runat="server" Text='<%# Eval("month_ot_name") %>' />
                                        </p>
                                        <%--<p>
                                            <b>ชั่วโมงรวม:</b>
                                            <asp:Label ID="lbl_sumhours_detailwait" Visible="true" runat="server" Text='<%# Eval("sumhours") + " " + "ชั่วโมง" %>' />
                                        </p>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detailwait" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detailwait" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detailwait" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detailwait" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detailwait" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detailwait" runat="server" Text='<%# Eval("actor_name") %>' />

                                        <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:UpdatePanel ID="Update_PnbtnViewDetailWaitApprove" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnViewDetailWaitApprove" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailWaitApprove" OnCommand="btnCommand"
                                                    CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                            </ContentTemplate>

                                            <%--<Triggers>
                                                <asp:PostBackTrigger ControlID="btnViewDetail" />
                                            </Triggers>--%>
                                        </asp:UpdatePanel>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>


                        <asp:FormView ID="FvDetailOTMontWaitapprove" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">รายละเอียดรายการ OT ประเภทกะคงที่</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_u0_document_idx_view" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                                    <asp:Label ID="lbl_emp_code_view" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                                    <asp:Label ID="lbl_cemp_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />


                                                </div>

                                                <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_org_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_org_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("org_idx")%>' />
                                                </div>

                                                <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_dept_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th")%>' />
                                                    <asp:Label ID="lbl_cemp_rdept_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rdept_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_sec_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th")%>' />
                                                    <asp:Label ID="lbl_rsec_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rsec_idx")%>' />
                                                </div>

                                                <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_pos_name_th_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th")%>' />
                                                    <asp:Label ID="lbl_rpos_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("rpos_idx")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>

                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_emp_email_view" runat="server" CssClass="control-labelnotop word-wrap col-xs-12" Text='<%# Eval("emp_email")%>' />

                                                </div>

                                                <%-- <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_costcenter_no_view" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no")%>' />
                                                    <asp:Label ID="lbl_costcenter_idx_view" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx")%>' />
                                                </div>--%>


                                                <%-- </div>--%>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div id="divscroll_GvViewWaitApproveOTMont" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvViewWaitApproveOTMont" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="True" PageSize="31"
                                ShowFooter="true"
                                ShowHeaderWhenEmpty="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-Width="15%" HeaderStyle-Width="15%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_u0_doc1_idx_approve" runat="server" Visible="false" Text='<%# Eval("u0_doc1_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_dateot_from_approve" runat="server" Text='<%# Eval("dateot_from") %>'></asp:Label>

                                                <asp:Label ID="lbl_detailholiday_approve" runat="server"><span class="text-danger">*(HH)</span></asp:Label>
                                                <asp:Label ID="lbl_detaildayoff_approve" runat="server"><span class="text-danger">*(DH)</span></asp:Label>

                                                <asp:Label ID="lbl_holiday_idx_approve" Visible="false" runat="server" Text='<%# Eval("holiday_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_day_off_approve" runat="server" Visible="false" Text='<%# Eval("day_off") %>'></asp:Label>
                                                <asp:Label ID="lbl_date_condition_approve" runat="server" Visible="false" Text='<%# Eval("date_condition") %>'></asp:Label>

                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_TypeWork_approve" runat="server" Text='<%# Eval("TypeWork") %>'></asp:Label>
                                                <asp:Label ID="lbl_work_start_approve" runat="server" Visible="false" Text='<%# Eval("work_start") %>'></asp:Label>
                                                <asp:Label ID="lbl_work_finish_approve" runat="server" Visible="false" Text='<%# Eval("work_finish") %>'></asp:Label>
                                            </p>


                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="งานที่ปฏิบัติ" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_detail_job_approve" runat="server" Text='<%# Eval("detail_job") %>'></asp:Label>
                                            <%--<asp:TextBox ID="txt_job" runat="server" CssClass="form-control" Text='<%# Eval("detail_job") %>' ></asp:TextBox>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_time_form_approve" runat="server" Text='<%# Eval("time_form") %>'></asp:Label>

                                            <%-- <asp:UpdatePanel ID="Update_Timestart" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">

                                                    <asp:TextBox ID="txt_timestart_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_start") %>' Enabled="false" CssClass="form-control clockpicker cursor-pointer" />
                                                    <span class="input-group-addon show-time-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                 
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-Width="5%" HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_time_to_approve" runat="server" Text='<%# Eval("time_to") %>'></asp:Label>

                                            <%--<asp:UpdatePanel ID="Update_Timeend" runat="server">
                                            <ContentTemplate>
                                                <div class="input-group time">
                                                    <asp:TextBox ID="txt_timeend_job" placeholder="เวลา ..." runat="server" Text='<%# Eval("time_end") %>' Enabled="false" CssClass="form-control clockpickerto cursor-pointer" />

                                                    <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                        <span class="glyphicon glyphicon-time"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_timedetail_approve" runat="server" Text="เวลารวม :"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT ก่อน" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="left" HeaderStyle-Font-Size="Small">

                                        <HeaderTemplate>

                                            <%--<div class="col-md-10 col-md-offset-1">--%>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <%-- <label>OT ก่อน</label>--%>
                                                    <asp:UpdatePanel ID="UpdatePanel_HeaderOTBefore" runat="server">
                                                        <ContentTemplate>
                                                            <small>
                                                                <asp:CheckBox ID="chk_allotbefore" runat="server" Enabled="false" Text=" OT ก่อน" Font-Size="Small" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true"></asp:CheckBox>
                                                            </small>

                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                        </HeaderTemplate>

                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="txtot_before_approve" Visible="false" runat="server" Text='<%# Eval("ot_before") %>'></asp:Label>

                                                <asp:DropDownList ID="ddl_timebefore_approve" Width="100px" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                <asp:Label ID="lblot_before_approve" runat="server" Text='<%# Eval("hours_ot_before") %>' Font-Bold="true"></asp:Label>
                                            </p>
                                            <p>
                                                <asp:Label ID="lbl_ot_before_status_approve" Visible="false" runat="server" Text='<%# Eval("ot_before_status") %>'></asp:Label>
                                                <asp:CheckBox ID="chkstatus_otbefore" runat="server" Checked="true" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true" Text=" ให้ ot"></asp:CheckBox>
                                            </p>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_before_approve" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชั่วโมงทำงานวันหยุด" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_holiday_approve" Enabled="false" Visible="false" runat="server" Text='<%# Eval("ot_holiday") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timeholiday_approve" Width="100px" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <asp:Label ID="lblot_holiday_approve" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_holiday") %>'></asp:Label>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_holiday_approve" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หลัง" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="right" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="txtot_after_approve" Enabled="false" Visible="false" runat="server" Text='<%# Eval("ot_after") %>'></asp:Label>
                                            <asp:DropDownList ID="ddl_timeafter_approve" Width="100px" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                            <asp:Label ID="lblot_after_approve" runat="server" Font-Bold="true" Text='<%# Eval("hours_ot_after") %>'></asp:Label>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursot_after_approve" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="รวม(ชั่วโมง)" ItemStyle-HorizontalAlign="right" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_sum_hours_approve" runat="server" Text='<%# Eval("sum_hours") %>'></asp:Label>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <asp:Label ID="lit_total_hoursmonth_approve" runat="server" Font-Bold="true"></asp:Label>

                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OT หัวหน้า Set" ItemStyle-HorizontalAlign="center" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_settime_approve" runat="server" Text='<%# Eval("settime") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="lbl_comment_approve" runat="server" Style="overflow: auto" Rows="2" TextMode="MultiLine" CssClass="form-control" Enabled="false" placeholder="หมายเหตุ ..." Text='<%# Eval("comment") %>'></asp:TextBox>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: left;">
                                                <asp:Literal ID="lit_hours_detail_approve" runat="server" Text="ชั่วโมง"></asp:Literal>
                                            </div>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความเห็นจาก HR" ItemStyle-Width="10%" HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:TextBox ID="lbl_comment_hr_approve" Style="overflow: auto" Rows="2" TextMode="MultiLine" runat="server" CssClass="form-control" Enabled="false" placeholder="Comment HR ..." Text='<%# Eval("comment_hr") %>'></asp:TextBox>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="form-group" id="div_GvViewWaitApproveOTMont" runat="server">
                            <p class="help-block"><font color="red"> หมายเหตุ : *(HH) คือวันหยุดบริษัท / *(DH) คือวันหยุดของพนักงาน</font></p>
                        </div>

                        <asp:GridView ID="GvFileMenoWaitOTMonth" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            HeaderStyle-CssClass="info"
                            OnRowDataBound="gvRowDataBound"
                            BorderStyle="None"
                            CellSpacing="2"
                            Font-Size="Small">

                            <Columns>
                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                    <ItemTemplate>
                                        <div class="col-lg-10">
                                            <asp:Literal ID="ltFileName11_permwait" runat="server" Text='<%# Eval("FileName") %>' />
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:HyperLink runat="server" ID="btnDL11_permwait" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                            <asp:HiddenField runat="server" ID="hidFile11_permwait" Value='<%# Eval("Download") %>' />
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <asp:FormView ID="FvHeadUserApprove" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <InsertItemTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ส่วนของหัวหน้างานพิจารณารายการ OT (กะคงที่)</h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-md-8 col-md-offset-2">

                                            <div class="col-md-12" id="ddl_approveheaduser" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>เลือกสถานะการอนุมัติ</label>
                                                    <asp:DropDownList ID="ddlApproveStatus" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlApproveStatus"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlApproveStatus" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานะการอนุมัติ"
                                                        ValidationGroup="SaveApproveHeaduser" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlApproveStatus" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlApproveStatus" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Comment ผู้มีสิทธิ์อนุมัติ</label>
                                                    <asp:TextBox ID="txt_comment_approve" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ผู้มีสิทธิ์อนุมัติ ..." />

                                                </div>
                                            </div>

                                            <div class="col-md-12" id="_divapproveheaduser" runat="server" visible="false">
                                                <div class="form-group pull-right">
                                                    <asp:LinkButton ID="btnSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveApprove" ValidationGroup="SaveApproveHeaduser"></asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToWaitDetail"></asp:LinkButton>

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <asp:Repeater ID="rptBindbtnApprove" OnItemDataBound="rptOnRowDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbcheck_coler" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnHeadUserSaveApprove" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")+ ";" + Eval("decision_name") %>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdSaveApprove"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:FormView ID="FvHrApproveOTMonth" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <InsertItemTemplate>
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">ส่วนของ HR ตรวจสอบรายการ OT (ประเภทกะคงที่)</h3>
                                    </div>
                                    <div class="panel-body">

                                        <div class="col-md-8 col-md-offset-2">

                                            <div class="col-md-12" id="divddlHrApproveStatus" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>เลือกสถานะการอนุมัติ</label>
                                                    <asp:DropDownList ID="ddlHrApproveStatus" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlApproveStatus"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlApproveStatus" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานะการอนุมัติ"
                                                        ValidationGroup="SaveApproveHeaduser" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlApproveStatus" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlApproveStatus" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Comment</label>
                                                    <asp:TextBox ID="txt_comment_hrapprove" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="Comment ..." />

                                                </div>
                                            </div>

                                            <div class="col-md-12" id="divbtnHrSaveApprove" runat="server" visible="false">
                                                <div class="form-group pull-right">
                                                    <asp:LinkButton ID="btnHrSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdHrSaveApprove" ValidationGroup="SaveApproveHeaduser"></asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdBackToWaitDetail"></asp:LinkButton>

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <asp:Repeater ID="rptBindHRbtnApprove" OnItemDataBound="rptOnRowDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lb_hrcheck_coler" runat="server" Visible="false" Text='<%# Eval("decision_idx") %>'></asp:Label>
                                                        <asp:LinkButton ID="btnHRSaveApprove" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("decision_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("decision_idx")+ ";" + Eval("decision_name") %>' OnCommand="btnCommand" Text='<%# Eval("decision_name") %>' CommandName="cmdHrSaveApprove"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>

                        <!-- Log Detail OT Wait Day -->
                        <div class="panel panel-info" id="_div_LogViewDetailWaitMonth" runat="server" visible="false">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogWaitViewDetailMonth" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log Detail OT Wait Day -->


                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View Wait Approve -->

        <!--View Report -->
        <asp:View ID="docReport" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelReportMonth" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-12">
                            <asp:Label ID="adf" runat="server"></asp:Label>
                            <%-------------- BoxSearch Start--------------%>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล Report</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" id="Div1" runat="server">
                                        <asp:FormView ID="Fv_Search_Emp_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                            <InsertItemTemplate>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label38" runat="server" Text="Employee Code" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label39" runat="server" Text="รหัสพนักงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:TextBox ID="txtempcode_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label121" runat="server" Text="Employee Name" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label122" runat="server" Text="ชื่อพนักงาน" /></b></small>
                                                        </h2>
                                                    </label>
                                                    <div class="col-sm-2">
                                                        <asp:TextBox ID="txtempname_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label273" runat="server" Text="DateType" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label274" runat="server" Text="ประเภทวันที่" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddldatetype" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">ประเภทวันที่....</asp:ListItem>
                                                            <asp:ListItem Value="1">วันเริ่มงาน</asp:ListItem>
                                                            <asp:ListItem Value="2">วันลาออก</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <asp:Panel ID="BoxSearch_Date" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label269" runat="server" Text="Start Date" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label270" runat="server" Text="วันที่" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtdatestart" runat="server" CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>

                                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label271" runat="server" Text="End Date" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label272" runat="server" Text="ถึงวันที่" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtdateend" AutoComplete="off" MaxLength="10" CssClass="form-control from-date-datepicker" runat="server"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlorg_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddldep_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label105" runat="server" Text="Section" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label106" runat="server" Text="หน่วยงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlsec_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label107" runat="server" Text="Position" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label108" runat="server" Text="ตำแหน่ง" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlpos_rp" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label220" runat="server" Text="EmpType" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label225" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlemptype_s" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                            <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                            <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label226" runat="server" Text="EmpStatus" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label227" runat="server" Text="สถานะพนักงาน" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlempstatus_s" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="-1">สถานะพนักงาน....</asp:ListItem>
                                                            <asp:ListItem Value="1">พนักงาน</asp:ListItem>
                                                            <asp:ListItem Value="0">พ้นสภาพ</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label42" runat="server" Text="Job Grade" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label43" runat="server" Text="Level" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-1">
                                                        <asp:DropDownList ID="ddljobgrade_start" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือก....</asp:ListItem>
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                            <asp:ListItem Value="9">9</asp:ListItem>
                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                            <asp:ListItem Value="11">11</asp:ListItem>
                                                            <asp:ListItem Value="12">12</asp:ListItem>
                                                            <asp:ListItem Value="13">13</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>
                                                    <div class="col-sm-1 text-center">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label201" runat="server" Text="To" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label279" runat="server" Text="ถึง" /></b></small>
                                                        </h2>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:DropDownList ID="ddljobgrade_end" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือก....</asp:ListItem>
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                            <asp:ListItem Value="9">9</asp:ListItem>
                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                            <asp:ListItem Value="11">11</asp:ListItem>
                                                            <asp:ListItem Value="12">12</asp:ListItem>
                                                            <asp:ListItem Value="13">13</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <label class="col-sm-2 control-label">
                                                        <h2 class="panel-title">
                                                            <asp:Label ID="Label209" runat="server" Text="Affiliation" /><br />
                                                            <small><b>
                                                                <asp:Label ID="Label296" runat="server" Text="สังกัด" /></b></small>
                                                        </h2>
                                                    </label>

                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlAffiliation_s" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">เลือก....</asp:ListItem>
                                                            <asp:ListItem Value="1">MOU-JWS</asp:ListItem>
                                                            <asp:ListItem Value="2">MOU-UDL</asp:ListItem>
                                                            <asp:ListItem Value="3">MOU-FASAI</asp:ListItem>
                                                            <asp:ListItem Value="4">MOU-API</asp:ListItem>
                                                            <asp:ListItem Value="5">UDL</asp:ListItem>
                                                            <asp:ListItem Value="6">FASAI</asp:ListItem>
                                                            <asp:ListItem Value="7">API</asp:ListItem>
                                                            <asp:ListItem Value="8">PN</asp:ListItem>
                                                            <asp:ListItem Value="9">TKN</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <asp:Button ID="btnsearch_report" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" />
                                                        <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                    </div>
                                                </div>

                                                <!--<div class="form-group">
                                                    <div class="col-sm-8">
                                                        <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Button ID="btnexport" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูลทั่วไป)" CommandName="btnExport" OnCommand="btnCommand" title="Export"></asp:Button>
                                                                <asp:Button ID="btnexport_beplus" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูล B-plus)" CommandName="btnExport_bplus" OnCommand="btnCommand" title="Export"></asp:Button>
                                                                <asp:Button ID="btnexport_Visa" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูล วันหมดอายุบัตร visa)" CommandName="btnexport_visa" OnCommand="btnCommand" title="Export"></asp:Button>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnexport" />
                                                                <asp:PostBackTrigger ControlID="btnexport_beplus" />
                                                                <asp:PostBackTrigger ControlID="btnexport_Visa" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>!-->

                                            </InsertItemTemplate>
                                        </asp:FormView>
                                    </div>
                                </div>
                            </div>
                            <%-------------- BoxSearch End--------------%>

                            <div id="BoxExport" runat="server" visible="true">
                                <asp:GridView ID="GvExport" runat="server" AutoGenerateColumns="false" Visible="true" AllowPaging="false" CssClass="table table-striped table-bordered table-hover table-responsive">
                                    <PagerStyle CssClass="PageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="Employee Code">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblCol00" runat="server" Text="Employee Code" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_code" runat="server" Text='<%# Eval("emp_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_type_name" runat="server" Text='<%# Eval("emp_type_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สังกัด">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Affiliation" runat="server" Text='<%# Eval("name_Affiliation") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sex">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sex_name_th" runat="server" Text='<%# Eval("sex_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Name Lastname (TH)">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Name Lastname (EN)">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_name_en" runat="server" Text='<%# Eval("emp_name_en") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nick Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_nickname_th" runat="server" Text='<%# Eval("emp_nickname_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Organization">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_org_name_th" runat="server" Text='<%# Eval("org_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_dept_name_th" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Secton">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sec_name_th" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Position">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_pos_name_th" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Job Level">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_jobgrade_level" runat="server" Text='<%# Eval("jobgrade_level") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cost Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_costcenter_no" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tel home">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_phone_no" runat="server" Text='<%# Eval("emp_phone_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tel mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_mobile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Email">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_email" runat="server" Text='<%# Eval("emp_email") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Birthday">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_birthday" runat="server" Text='<%# Eval("emp_birthday") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nationnal">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_nat_name_th" runat="server" Text='<%# Eval("nat_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Race">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_race_name_th" runat="server" Text='<%# Eval("race_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Rel">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_rel_name_th" runat="server" Text='<%# Eval("rel_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_address" runat="server" Text='<%# Eval("emp_address") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="District">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_dist_name" runat="server" Text='<%# Eval("dist_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Amphone">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_amp_name" runat="server" Text='<%# Eval("amp_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Province">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_prov_name" runat="server" Text='<%# Eval("prov_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PostCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_post_code" runat="server" Text='<%# Eval("post_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_country_name" runat="server" Text='<%# Eval("country_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Married Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_married_name_th" runat="server" Text='<%# Eval("married_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Inhabit Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_inhabit_name" runat="server" Text='<%# Eval("inhabit_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Bank Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bank_name" runat="server" Text='<%# Eval("bank_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Bank No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bank_no" runat="server" Text='<%# Eval("bank_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Emer Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emer_name" runat="server" Text='<%# Eval("emer_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Emer Relation">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emer_relation" runat="server" Text='<%# Eval("emer_relation") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Emer Tel">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emer_tel" runat="server" Text='<%# Eval("emer_tel") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee IN">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_start_date" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Out">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_resign_date" runat="server" Text='<%# Eval("emp_resign_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_status" runat="server" Text='<%# Eval("emp_status") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Probation">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_status" runat="server" Text='<%# Eval("emp_probation_status") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Probation Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_date" runat="server" Text='<%# Eval("emp_probation_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Approver 1">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve1" runat="server" Text='<%# Eval("emp_approve1") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Approve 2">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve2" runat="server" Text='<%# Eval("emp_approve2") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div id="BoxExport_Bplus" runat="server" visible="true">
                                <asp:GridView ID="GvExport_Bplus" runat="server" AutoGenerateColumns="false" Visible="true" AllowPaging="false" CssClass="table table-striped table-bordered table-hover table-responsive">
                                    <PagerStyle CssClass="PageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ชื่อต้น">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_prefix_name_th" runat="server" Text='<%# Eval("prefix_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อตัว">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_firstname_th" runat="server" Text='<%# Eval("emp_firstname_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อสกุล">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_lastname_th" runat="server" Text='<%# Eval("emp_lastname_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่ออังกฤษ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_firstname_en" runat="server" Text='<%# Eval("emp_firstname_en") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันเดือนปีเกิด">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_birthday" runat="server" Text='<%# Eval("emp_birthday") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เพศ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sex_name_th" runat="server" Text='<%# Eval("sex_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานภาพสมรส">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_married_name_th" runat="server" Text='<%# Eval("married_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขบัตรประชาชน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_identity_card" runat="server" Text='<%# Eval("identity_card") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่หมดอายุ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_idate_expired" runat="server" Text='<%# Eval("idate_expired") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานที่ออกบัตร">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_issued_at" runat="server" Text='<%# Eval("issued_at") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ที่อยู่บรรทัด 1">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_address" runat="server" Text='<%# Eval("emp_address") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ที่อยู่บรรทัด 2">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_address_2" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ที่อยู่บรรทัด 3">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_address_3" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ไปรษณีย์">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_post_code" runat="server" Text='<%# Eval("post_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="โทรศัพท์">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_mobile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อีเมล์">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_email" runat="server" Text='<%# Eval("emp_email") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขผู้เสียภาษี">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_nat_name_th" runat="server" Text="" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสพนักงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_code_cus" runat="server" Text='<%# Eval("emp_code_cus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขที่บัตรพนักงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_code_cus_card" runat="server" Text='<%# Eval("emp_code_cus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทการจ้าง">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_type_name" runat="server" Text='<%# Eval("emp_type_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสตำแหน่งงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_position_1" runat="server" Text="01" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสแผนก">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_section_1" runat="server" Text="01" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสสาขา">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_1" runat="server" Text="12345" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วิธีการจ่ายค่าจ้าง">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_check_money" runat="server" Text="2" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสธนาคารโอนเงิน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_BBL" runat="server" Text="BBL" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขที่บัญชีธนาคาร">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bank_no" runat="server" Text='<%# Eval("bank_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขที่กองทุนสำรองฯ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_inhabit_name" runat="server" Text='<%# Eval("inhabit_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สมัครกองทุน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_bank_name" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เลขที่สัญญาเงินกู้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_4" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สัญญาเงินกู้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emer_name" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สมัครปกสค.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emer_relation" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_start_date" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่พ้นทดลองงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_date" runat="server" Text='<%# Eval("emp_probation_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานภาพพนง.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_status" runat="server" Text='<%# Eval("emp_status_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สิ้นสภาพ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_resign_date" runat="server" Text='<%# Eval("emp_resign_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อัตราค่าจ้างปัจจุบัน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_status" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่เริ่มอัตราปัจจุบัน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_date" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อัตราค่าจ้างเดิม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve1" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่เริ่มจ่ายโดยโปรแกรม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve201" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ความถี่การจ่ายค่าจ้าง">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve21" runat="server" Text="1" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วิธีหักเข้ากองทุน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve22" runat="server" Text="0" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดสะสมพนง.ต่อครั้ง">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve23" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดบริษัทสมทบต่อครั้ง">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve24" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดสะสมพนง.ปีก่อน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve25" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดบริษัทสมทบปีก่อน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve26" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดสะสมพนง.ปีนี้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve27" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดบริษัทสมทบปีนี้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve28" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ต้องการหักปกสค.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_approve29" runat="server" Text="Y" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดปกสค.พนักงานปีนี้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl1_emp_approve30" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดปกสค.บริษัทปีนี้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl2_emp_approve31" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดเงินค้ำประกัน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl3_emp_approve32" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค้ำประกันที่เก็บแล้ว">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl4_emp_approve33" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เก็บค้ำประกันครั้งละ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl5_emp_approve34" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดเงินกู้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl6_emp_approve35" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ยอดชำระแล้ว">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl7_emp_approve36" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หักชำระครั้งละ">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl8_emp_approve37" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทการหักภาษี">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl9_emp_approve38" runat="server" Text="3" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายได้ก่อนทำงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl10_emp_approve39" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ภาษีถูกหักก่อนทำงาน">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl11_emp_approve40" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายได้ก่อนโปรแกรม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl12_emp_approve41" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ภาษีถูกหักก่อนโปรแกรม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl13_emp_approve42" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ภาษีบ.จ่ายก่อนโปรแกรม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl14_emp_approve43" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แยกยื่นหรือยื่นรวม">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl15_emp_approve44" runat="server" Text="x" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตัวคูณประมาณรายได้">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl16_emp_approve45" runat="server" Text="12" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนบุตรอัตรา 1">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl17_emp_approve46" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนบุตรอัตรา 2">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl18_emp_approve47" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนบุตรอัตรา 3">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl19_emp_approve48" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จำนวนบุตรอัตรา 4">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl20_emp_approve49" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่าซื้อหน่วยลงทุน RMF">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl21_emp_approve50" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่าซื้อหน่วยลงทุน LTF">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl22_emp_approve51" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เบี้ยประกันชีวิต">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl23_emp_approve52" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="กู้ยืมเพื่อที่อยู่อาศัย">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl24_emp_approve53" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="บริจาค">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl25_emp_approve54" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div id="BoxExport_Exp_Visa" runat="server" visible="true">
                                <asp:GridView ID="Gv_Export_Visa" runat="server" AutoGenerateColumns="false" Visible="true" AllowPaging="false" CssClass="table table-striped table-bordered table-hover table-responsive">
                                    <PagerStyle CssClass="PageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="Employee Code">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblCol00" runat="server" Text="Employee Code" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_code" runat="server" Text='<%# Eval("emp_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_type_name" runat="server" Text='<%# Eval("emp_type_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date Exp. Visa">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_type_nam2e" runat="server" Text='<%# Eval("visa_enddate") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Visa Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_type_wnam2e" runat="server" Text='<%# Eval("DetailProfile") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Sex">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sex_name_th" runat="server" Text='<%# Eval("sex_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Name Lastname (TH)">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Name Lastname (EN)">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_name_en" runat="server" Text='<%# Eval("emp_name_en") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Organization">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_org_name_th" runat="server" Text='<%# Eval("org_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Department">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_dept_name_th" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Secton">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sec_name_th" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Position">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_pos_name_th" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cost Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_costcenter_no" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tel home">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_phone_no" runat="server" Text='<%# Eval("emp_phone_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Tel mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_mobile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Birthday">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_birthday" runat="server" Text='<%# Eval("emp_birthday") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Nationnal">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_nat_name_th" runat="server" Text='<%# Eval("nat_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Race">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_race_name_th" runat="server" Text='<%# Eval("race_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Rel">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_rel_name_th" runat="server" Text='<%# Eval("rel_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Address">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_address" runat="server" Text='<%# Eval("emp_address") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="District">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_dist_name" runat="server" Text='<%# Eval("dist_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Amphone">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_amp_name" runat="server" Text='<%# Eval("amp_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Province">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_prov_name" runat="server" Text='<%# Eval("prov_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PostCode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_post_code" runat="server" Text='<%# Eval("post_code") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_country_name" runat="server" Text='<%# Eval("country_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Married Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_married_name_th" runat="server" Text='<%# Eval("married_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Inhabit Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_inhabit_name" runat="server" Text='<%# Eval("inhabit_name_th") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee IN">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_start_date" runat="server" Text='<%# Eval("emp_start_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Out">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_resign_date" runat="server" Text='<%# Eval("emp_resign_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_status" runat="server" Text='<%# Eval("emp_status") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Probation">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_status" runat="server" Text='<%# Eval("emp_probation_status") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Employee Probation Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_emp_probation_date" runat="server" Text='<%# Eval("emp_probation_date") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div class="panel-heading">
                                <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list" runat="server" /></strong></font></h3>
                            </div>

                            <asp:GridView ID="GvEmployee_Report"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="20"
                                DataKeyNames="emp_idx"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่เริ่มงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblemp_start_date" runat="server" Text='<%# Eval("emp_start_date") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblposition" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Job Grade" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbljoblevel" runat="server" Text='<%# Eval("jobgrade_level") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="1%">
                                        <ItemTemplate>
                                            <small>
                                                <div style="text-align: center; padding-top: 10px;">
                                                    <span class='<%# Eval("emp_status").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                                </div>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View Report -->

        <!--View Add Group OT Day-->
        <asp:View ID="docAddGroupOTDay" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="div_AddGroupOTDay" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnAddGroupOTDay" CssClass="btn btn-primary" data-toggle="tooltip" title="เพิ่มกลุ่ม OT รายวัน" runat="server"
                                CommandName="cmdAddGroupOTDay" CommandArgument="1" OnCommand="btnCommand">+ เพิ่มกลุ่ม</asp:LinkButton>
                        </div>

                        <div class="col-md-1" id="div_showfiletestaddmore1" runat="server" style="color: transparent;">
                            <asp:FileUpload ID="FileUpload1_testaddmore1" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                        </div>
                        <div class="clearfix"></div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnImport" />
                    </Triggers>
                </asp:UpdatePanel>


                <asp:GridView ID="GvDetailGroupOTDay" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True"
                    PageSize="5"
                    BorderStyle="None"
                    CellSpacing="2">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อกลุ่ม" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_m0_group_idx_detail" runat="server" Visible="false" Text='<%# Eval("m0_group_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_group_name_detail" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวน(คน)" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:Label ID="lbl_count_emp_detail" runat="server" Text='<%# Eval("count_emp") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_m0_group_status_detail" runat="server" Visible="false" Text='<%# Eval("m0_group_status") %>'></asp:Label>
                                <asp:Label ID="m0_group_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span></div>
                                </asp:Label>
                                <asp:Label ID="m0_group_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span></div>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDetailGroupOTDay" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewDetailGroupOTDay"
                                    OnCommand="btnCommand" CommandArgument='<%# Eval("m0_group_idx")%>'
                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnDeleteDetailGroupOTDay" CssClass="btn-sm btn-danger" runat="server" CommandName="CmdDeleteDetailGroupOTDay"
                                    OnCommand="btnCommand" CommandArgument='<%# Eval("m0_group_idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-trash"></i></asp:LinkButton>

                                <%--   <asp:LinkButton ID="btnCancelFood" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand"
                                    CommandName="CmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>--%>
                                <%--  <asp:LinkButton ID="btnViewDetailGroupOTDay"" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewDetailGroupOTDay" 
                                            OnCommand="btnCommand" CommandArgument='<%# Eval("m0_group_idx")%>'
                                            data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <!-- Back To Detail To Index -->
                <asp:UpdatePanel ID="Div_BackToDetailGroupOTDay" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetailGroupOTDay" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetailGroupOTDay" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail To Index -->

                <asp:GridView ID="GvViewDetailGroupOTDay" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="True"
                    AllowPaging="True"
                    PageSize="5"
                    BorderStyle="None"
                    CellSpacing="2">
                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <asp:Label ID="lbl_m1_group_idx_viewdetail" runat="server" Visible="false" Text='<%# Eval("m1_group_idx") %>'></asp:Label>
                                <asp:Label ID="lbl_emp_code_viewdetail" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ - สกุล" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left">
                            <ItemTemplate>

                                <asp:Label ID="lbl_emp_name_th_viewdetail" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left">
                            <ItemTemplate>

                                <asp:Label ID="lbl_dept_name_th_viewdetail" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left">
                            <ItemTemplate>

                                <asp:Label ID="lbl_sec_name_th_viewdetail" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


                <asp:UpdatePanel ID="PanelAddGroupOTDay" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่มกลุ่ม OT พนักงานรายวัน  </h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>ชื่อกลุ่ม</label>

                                            <asp:TextBox ID="txt_addnamegroup" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>เลือกไฟล์ Excel</label>
                                            <asp:FileUpload ID="UploadFileExcel" runat="server" />
                                            <%--  <asp:FileUpload ID="UploadFileExcel" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true"
                                                CssClass="control-label multi max-1 accept-png|jpg|pdf maxsize-1024  with-preview"
                                                ValidationGroup="saveInsertPermPerm"
                                                runat="server" />--%>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <asp:LinkButton ID="btnImport" CssClass="btn btn-success" data-toggle="tooltip" title="Import Excel" runat="server" CommandName="cmdImportExcel"
                                                OnCommand="btnCommand">Import Excel</asp:LinkButton>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <asp:Panel ID="Div_GvDetailExcelImport" runat="server">
                                            <asp:GridView ID="GvDetailExcelImport"
                                                runat="server" RowStyle-Font-Size="Small"
                                                HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="25px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex +1) %>
                                                                <%-- <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>--%>
                                       
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbl_emp_code_excel" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbl_emp_name_excel" runat="server" Text='<%# Eval("emp_name")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbl_dept_name_th_excel" runat="server" Text='<%# Eval("dept_name_th")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbl_sec_name_th_excel" runat="server" Text='<%# Eval("sec_name_th")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                            <div class="form-group pull-right">

                                                <asp:LinkButton ID="btnSaveImportExcel" CssClass="btn btn-success" data-toggle="tooltip" title="Import Excel" runat="server" CommandName="cmdSaveImportExcel"
                                                    OnCommand="btnCommand">บันทึก</asp:LinkButton>

                                            </div>

                                        </asp:Panel>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnImport" />
                    </Triggers>

                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View Add Group OT Day-->

        <!--View DetailImport OTDay -->
        <asp:View ID="docDetailImportOTDay" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelDetailImportOTDay" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-primary" id="div_headdetail_otday" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการขอ OT รายวัน</h3>
                            </div>
                        </div>

                        <!-- Back DetailEmployeeImportDay -->
                        <asp:UpdatePanel ID="_PanelBackDetailImportOTDay" runat="server">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnBackDetailImportOTDay" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="cmdBackDetailImportOTDay" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <!-- Back To DetailEmployeeImportDay -->

                        <asp:GridView ID="GvDetailEmployeeImportOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="5"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex +1) %>
                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">

                                    <ItemTemplate>
                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detail_otday" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>วันที่สร้าง:</b>
                                            <asp:Label ID="lbl_create_date_detailimport" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_create_date_detailimport" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="left" ItemStyle-Width="25%" ItemStyle-CssClass="left">

                                    <HeaderTemplate>
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>เลือกโรงงาน</label>
                                                    <asp:UpdatePanel ID="UpdatePanel_Header" runat="server">
                                                        <ContentTemplate>
                                                            <small>
                                                                <asp:DropDownList ID="ddlPlacesFilter" runat="server" CssClass="form-control" Font-Size="Small"
                                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <div class="col-md-1" id="div_showfiletestaddmore" runat="server" style="color: transparent;">
                                                                    <asp:FileUpload ID="FileUpload1_show" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png" />
                                                                </div>
                                                            </small>

                                                        </ContentTemplate>
                                                        <Triggers>

                                                            <asp:AsyncPostBackTrigger ControlID="ddlPlacesFilter" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </HeaderTemplate>

                                    <ItemTemplate>
                                        <p>
                                            <b>โรงงาน:</b>
                                            <asp:Label ID="lbl_LocName_detailimport" Visible="true" runat="server" Text='<%# Eval("LocName") %>' />
                                        </p>
                                        <p>
                                            <b>แผนก:</b>
                                            <asp:Label ID="lbl_SecNameTH_detailimport" Visible="true" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                        </p>
                                        <p>
                                            <b>ประเภทวันทำงาน:</b>
                                            <asp:Label ID="lbl_type_daywork_name_detailimport" Visible="true" runat="server" Text='<%# Eval("type_daywork_name")%>' />

                                        </p>
                                        <p>
                                            <b>ประเภท OT:</b>
                                            <asp:Label ID="lbl_type_ot_name_detailimport" Visible="true" runat="server" Text='<%# Eval("type_ot_name")%>' />
                                        </p>
                                        <p>
                                            <b>วันที่เริ่มทำ:</b>
                                            <asp:Label ID="lbl_dateot_from_detailimport" Visible="true" runat="server" Text='<%# Eval("dateot_from")%>' />
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_form_detailimport" Visible="true" runat="server" Text='<%# Eval("time_form")+ " " + "น."%>' />
                                        </p>
                                        <p>
                                            <b>ถึงวันที่:</b>
                                            <asp:Label ID="lbl_dateot_to_detailimport" Visible="true" runat="server" Text='<%# Eval("dateot_to")%>' />
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_to_detailimport" Visible="true" runat="server" Text='<%# Eval("time_to") + " " + "น."%>' />
                                        </p>
                                        <p>

                                            <b>จำนวนรวม(ชั่วโมง):</b>
                                            <asp:Label ID="lbl_day_all_sum_detailimport_value" Visible="false" runat="server" Text='<%# Eval("day_all_sum")%>' />
                                            <asp:Label ID="lbl_day_all_sum_detailimport" runat="server" /><b> ชั่วโมง</b>
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_detailimport" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_detailimport" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_detailimport" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_detailimport" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_detailimport" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_detailimport" runat="server" Text='<%# Eval("actor_name")%>' />


                                        <%--  <asp:Label ID="lbl_current_status" runat="server" Text='<%# Eval("current_status") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:UpdatePanel ID="_PanelViewEmployeeDetailOTDay" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnViewEmployeeImportOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewEmployeeImportOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                            </ContentTemplate>

                                            <%--<Triggers>
                                                <asp:PostBackTrigger ControlID="btnViewDetail" />
                                            </Triggers>--%>
                                        </asp:UpdatePanel>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <asp:FormView ID="FvViewDetailEmployeeImportDay" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>
                                <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">บันทึกการทำงานล่วงเวลา(สำหรับพนักงานรายวัน)</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">


                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก/ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_u0_document_idx_viewimport" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                                    <asp:Label ID="lbl_dept_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sec_name_th") + " " + "/" + " " +  Eval("dept_name_th")%>' />

                                                </div>

                                                <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_dateot_from_viewimport" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("dateot_from") + " " + "-" + " " +  Eval("dateot_to")%>' />
                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="โรงงาน : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_LocName_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("LocName")%>' />

                                                </div>

                                                <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_time_form_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_form")  + " " + "-" + " " +  Eval("time_to") + " " + "น."%>' />

                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทวันทำงาน : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_daywork_name_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_daywork_name")%>' />

                                                </div>

                                                <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภท OT : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_type_ot_name_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_ot_name")%>' />

                                                </div>
                                                <%-- </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <%--<div class="col-md-12">--%>
                                                <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="จำนวนรวม(ชั่วโมง) : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lbl_day_all_sum_viewimportvalue" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("day_all_sum")%>' />
                                                    <asp:Label ID="lbl_day_all_sum_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" />

                                                </div>


                                                <%-- </div>--%>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>

                        <div id="divGvViewDetailEmployeeImportDay" style="overflow-y: scroll; width: auto; height: auto" runat="server">
                            <asp:GridView ID="GvViewDetailEmployeeImportDay" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="False" PageSize="10"
                                ShowFooter="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <small>
                                                <%# (Container.DataItemIndex +1) %>
                                       
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_emp_code_otday" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            </small>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ - นามสกุล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_emp_name_th_otday" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </small>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_pos_name_th_otday" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                <%-- <asp:Label ID="lbl_sec_name_th_excel_otday" runat="server" Text='<%# Eval("pos_name_th")%>'></asp:Label>--%>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Literal ID="lit_time_import" runat="server" Text="เวลารวม :"></asp:Literal>
                                                </div>

                                            </small>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="26" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_26_excel_otday" runat="server" Text='<%# Eval("ot_day_26")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <%--background-color: chartreuse;--%>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_26_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="27" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_27_excel_otday" runat="server" Text='<%# Eval("ot_day_27")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_27_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="28" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_28_excel_otday" runat="server" Text='<%# Eval("ot_day_28")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_28_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="29" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_29_excel_otday" runat="server" Text='<%# Eval("ot_day_29")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_29_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="30" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_30_excel_otday" runat="server" Text='<%# Eval("ot_day_30")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_30_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="31" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_31_excel_otday" runat="server" Text='<%# Eval("ot_day_31")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_31_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="1" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_1_excel_otday" runat="server" Text='<%# Eval("ot_day_1")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_1_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="2" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_2_excel_otday" runat="server" Text='<%# Eval("ot_day_2")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_2_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="3" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_3_excel_otday" runat="server" Text='<%# Eval("ot_day_3")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_3_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="4" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_4_excel_otday" runat="server" Text='<%# Eval("ot_day_4")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_4_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="5" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_5_excel_otday" runat="server" Text='<%# Eval("ot_day_5")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_5_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="6" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_6_excel_otday" runat="server" Text='<%# Eval("ot_day_6")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_6_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="7" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_7_excel_otday" runat="server" Text='<%# Eval("ot_day_7")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_7_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="8" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_8_excel_otday" runat="server" Text='<%# Eval("ot_day_8")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_8_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="9" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_9_excel_otday" runat="server" Text='<%# Eval("ot_day_9")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_9_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="10" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_10_excel_otday" runat="server" Text='<%# Eval("ot_day_10")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_10_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="11" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_11_excel_otday" runat="server" Text='<%# Eval("ot_day_11")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_11_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="12" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_12_excel_otday" runat="server" Text='<%# Eval("ot_day_12")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_12_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="13" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_13_excel_otday" runat="server" Text='<%# Eval("ot_day_13")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_13_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="14" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_14_excel_otday" runat="server" Text='<%# Eval("ot_day_14")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_14_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="15" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_15_excel_otday" runat="server" Text='<%# Eval("ot_day_15")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_15_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="16" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_16_excel_otday" runat="server" Text='<%# Eval("ot_day_16")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_16_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="17" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_17_excel_otday" runat="server" Text='<%# Eval("ot_day_17")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_17_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="18" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_18_excel_otday" runat="server" Text='<%# Eval("ot_day_18")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_18_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="19" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_19_excel_otday" runat="server" Text='<%# Eval("ot_day_19")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_19_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="20" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_20_excel_otday" runat="server" Text='<%# Eval("ot_day_20")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_20_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="21" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_21_excel_otday" runat="server" Text='<%# Eval("ot_day_21")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_21_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="22" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_22_excel_otday" runat="server" Text='<%# Eval("ot_day_22")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_22_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="23" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_23_excel_otday" runat="server" Text='<%# Eval("ot_day_23")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_23_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="24" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_24_excel_otday" runat="server" Text='<%# Eval("ot_day_24")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_24_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="25" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_ot_day_25_excel_otday" runat="server" Text='<%# Eval("ot_day_25")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_total_ot_day_25_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชั่วโมง(รวม)" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lbl_totalhours_" runat="server"></asp:Label>
                                                <asp:Label ID="lbl_totalhours_sumtotal" runat="server" Visible="false"></asp:Label>
                                            </small>
                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <small>
                                                <div style="text-align: right;">
                                                    <asp:Label ID="lit_totalhours_sumtotal" runat="server" Font-Bold="true"></asp:Label>

                                                </div>
                                            </small>
                                        </FooterTemplate>

                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                        <br />
                        <!-- Log DetailEmployeeImportDay -->
                        <div class="panel panel-info" id="divLogDetailEmployeeImportDay" runat="server">
                            <div class="panel-heading">ประวัติการดำเนินการ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptLogDetailEmployeeImportDay" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <%--<th>ผลการดำเนินการ</th>--%>
                                            <th>เหตุผล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                            <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                            <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                            <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                            <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>
                        </div>
                        <!-- Log DetailEmployeeImportDay -->

                    </ContentTemplate>
                </asp:UpdatePanel>


            </div>
        </asp:View>
        <!--View DetailImport OTDay -->

        <!--View Import OT Day -->
        <asp:View ID="docImportOTDay" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelImportOTDay" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">บันทึกการขอ OT พนักงานรายวัน  </h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>โรงงาน</label>
                                            <asp:DropDownList ID="ddlplace" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay" CssClass="form-control">
                                            </asp:DropDownList>
                                            <p class="help-block"><font color="red">**กรุณาเลือกโรงงาน</font></p>
                                            <asp:RequiredFieldValidator ID="Req_ddlplace"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlplace" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกโรงงาน"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlplace" Width="160" PopupPosition="BottomLeft" />


                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>องค์กร</label>
                                            <asp:DropDownList ID="ddlorg" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <p class="help-block"><font color="red">**กรุณาเลือกองค์กร</font></p>
                                            <asp:RequiredFieldValidator ID="Req_ddlorg"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorg" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorg" Width="160" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ฝ่าย</label>
                                            <asp:DropDownList ID="ddlrdept" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <p class="help-block"><font color="red">**กรุณาเลือกฝ่าย</font></p>
                                            <asp:RequiredFieldValidator ID="Req_ddlrdept"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlrdept" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกฝ่าย"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlrdept" Width="160" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>แผนก</label>
                                            <asp:DropDownList ID="ddlrsec" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทวันทำงาน</label>
                                            <asp:DropDownList ID="ddl_type_daywork" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay" CssClass="form-control">
                                            </asp:DropDownList>
                                            <p class="help-block"><font color="red">**กรุณาเลือกประเภทวันทำงาน</font></p>
                                            <asp:RequiredFieldValidator ID="Req_ddl_type_daywork"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_type_daywork" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทวันทำงาน"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddl_type_daywork" Width="160" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภท OT</label>
                                            <asp:DropDownList ID="ddl_typeot" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay" CssClass="form-control">
                                            </asp:DropDownList>
                                            <p class="help-block"><font color="red">**กรุณาเลือกประเภท OT</font></p>
                                            <asp:RequiredFieldValidator ID="Req_ddl_typeot"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddl_typeot" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภท OT"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddl_typeot" Width="160" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>วันที่เริ่มทำ</label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateot_dayform" placeholder="วันที่เริ่ม ..." runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay"
                                                    CssClass="form-control datetimepicker-from cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_dateot_dayform"
                                                    runat="server"
                                                    ControlToValidate="txt_dateot_dayform" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_dateot_dayform" Width="160" PopupPosition="BottomLeft" />

                                            </div>
                                            <p class="help-block"><font color="red">**กรุณาเลือกวันที่</font></p>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงวันที่</label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateot_dayto" placeholder="ถึงวันที่ ..." runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay"
                                                    CssClass="form-control datetimepicker-to cursor-pointer" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_dateot_dayto"
                                                    runat="server"
                                                    ControlToValidate="txt_dateot_dayto" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่"
                                                    ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_dateot_dayto" Width="160" PopupPosition="BottomLeft" />

                                            </div>
                                            <p class="help-block"><font color="red">**กรุณาเลือกวันที่</font></p>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เวลาที่เริ่ม</label>
                                            <div class="input-group time">
                                                <asp:TextBox ID="txt_time_dayform" placeholder="เวลา ..." runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay"
                                                    CssClass="form-control clockpicker cursor-pointer" value="" />
                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_time_dayform"
                                                    runat="server"
                                                    ControlToValidate="txt_time_dayform" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลา"
                                                    ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_time_dayform" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                            <p class="help-block"><font color="red">**กรุณาเลือกเวลา</font></p>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงเวลา</label>
                                            <div class="input-group time">
                                                <asp:TextBox ID="txt_time_dayto" placeholder="เวลา ..." runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay"
                                                    CssClass="form-control clockpickerto cursor-pointer" value="" />

                                                <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>

                                                <asp:RequiredFieldValidator ID="Req_txt_time_dayto"
                                                    runat="server"
                                                    ControlToValidate="txt_time_dayto" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเวลา"
                                                    ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_time_dayto" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                            <p class="help-block"><font color="red">**กรุณาเลือกเวลา</font></p>
                                        </div>

                                    </div>


                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>เลือกไฟล์ Excel</label>
                                                    <asp:FileUpload ID="UploadFileExcelOTDay" runat="server" />
                                                    <p class="help-block"><font color="red">**กรุณาเลือกไฟล์ เฉพาะนามสกุล xls, xlsx เท่านั้น </font></p>
                                                    <%--  <asp:FileUpload ID="UploadFileExcel" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true"
                                                CssClass="control-label multi max-1 accept-png|jpg|pdf maxsize-1024  with-preview"
                                                ValidationGroup="saveInsertPermPerm"
                                                runat="server" />--%>
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <asp:LinkButton ID="btnImportExcelOTDay" CssClass="btn btn-success" data-toggle="tooltip" title="Import Excel" runat="server" CommandName="cmdImportExcelOTDay"
                                                        OnCommand="btnCommand">Import Excel</asp:LinkButton>

                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnImportExcelOTDay" />
                                        </Triggers>

                                    </asp:UpdatePanel>

                                    <div class="col-md-12">
                                        <asp:Panel ID="Div_GvDetailImportOTDay" runat="server">
                                            <div id="div_scroll_y" style="overflow-y: scroll; width: auto; height: auto" runat="server">
                                                <asp:GridView ID="GvDetailImportOTDay"
                                                    runat="server" RowStyle-Font-Size="Small"
                                                    HeaderStyle-Font-Size="Small"
                                                    AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet"
                                                    OnRowDataBound="gvRowDataBound"
                                                    RowStyle-Wrap="true"
                                                    CssClass="table table-striped table-bordered table-responsive"
                                                    HeaderStyle-Height="25px"
                                                    ShowHeaderWhenEmpty="True"
                                                    ShowFooter="True"
                                                    BorderStyle="None"
                                                    CellSpacing="2">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <%# (Container.DataItemIndex +1) %>
                                       
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_emp_code_excel_otday" runat="server" Text='<%# Eval("emp_code")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อ - นามสกุล" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_emp_name_th_excel_otday" runat="server" Text='<%# Eval("emp_name_th")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_sec_name_th_excel_otday" runat="server" Text='<%# Eval("pos_name_th")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Literal ID="lit_time_import" runat="server" Text="เวลารวม :"></asp:Literal>
                                                                    </div>

                                                                </small>

                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="26" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_26_excel_otday" runat="server" Text='<%# Eval("ot_day_26")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <%--<div style="text-align: right; background-color: chartreuse;">--%>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_26_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="27" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_27_excel_otday" runat="server" Text='<%# Eval("ot_day_27")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_27_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="28" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_28_excel_otday" runat="server" Text='<%# Eval("ot_day_28")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_28_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="29" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_29_excel_otday" runat="server" Text='<%# Eval("ot_day_29")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_29_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="30" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_30_excel_otday" runat="server" Text='<%# Eval("ot_day_30")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_30_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="31" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_31_excel_otday" runat="server" Text='<%# Eval("ot_day_31")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_31_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="1" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_1_excel_otday" runat="server" Text='<%# Eval("ot_day_1")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_1_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="2" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_2_excel_otday" runat="server" Text='<%# Eval("ot_day_2")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_2_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="3" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_3_excel_otday" runat="server" Text='<%# Eval("ot_day_3")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_3_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="4" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_4_excel_otday" runat="server" Text='<%# Eval("ot_day_4")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_4_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="5" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_5_excel_otday" runat="server" Text='<%# Eval("ot_day_5")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_5_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="6" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_6_excel_otday" runat="server" Text='<%# Eval("ot_day_6")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_6_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="7" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_7_excel_otday" runat="server" Text='<%# Eval("ot_day_7")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_7_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="8" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_8_excel_otday" runat="server" Text='<%# Eval("ot_day_8")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_8_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="9" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_9_excel_otday" runat="server" Text='<%# Eval("ot_day_9")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_9_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="10" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_10_excel_otday" runat="server" Text='<%# Eval("ot_day_10")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_10_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="11" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_11_excel_otday" runat="server" Text='<%# Eval("ot_day_11")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_11_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="12" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_12_excel_otday" runat="server" Text='<%# Eval("ot_day_12")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_12_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="13" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_13_excel_otday" runat="server" Text='<%# Eval("ot_day_13")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_13_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="14" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_14_excel_otday" runat="server" Text='<%# Eval("ot_day_14")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_14_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="15" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_15_excel_otday" runat="server" Text='<%# Eval("ot_day_15")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_15_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="16" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_16_excel_otday" runat="server" Text='<%# Eval("ot_day_16")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_16_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="17" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_17_excel_otday" runat="server" Text='<%# Eval("ot_day_17")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_17_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="18" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_18_excel_otday" runat="server" Text='<%# Eval("ot_day_18")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_18_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="19" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_19_excel_otday" runat="server" Text='<%# Eval("ot_day_19")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_19_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="20" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_20_excel_otday" runat="server" Text='<%# Eval("ot_day_20")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_20_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="21" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_21_excel_otday" runat="server" Text='<%# Eval("ot_day_21")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_21_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="22" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_22_excel_otday" runat="server" Text='<%# Eval("ot_day_22")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_22_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="23" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_23_excel_otday" runat="server" Text='<%# Eval("ot_day_23")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_23_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="24" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_24_excel_otday" runat="server" Text='<%# Eval("ot_day_24")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_24_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="25" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbl_ot_day_25_excel_otday" runat="server" Text='<%# Eval("ot_day_25")%>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <small>
                                                                    <div style="text-align: right;">
                                                                        <asp:Label ID="lit_total_ot_day_25_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                                                    </div>
                                                                </small>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>

                                            <br />
                                            <div class="form-group pull-right">

                                                <asp:LinkButton ID="btnSaveImportExcelOTDay" CssClass="btn btn-success" data-toggle="tooltip" title="Save" runat="server" ValidationGroup="SaveImportExcelEmployeeOTDay"
                                                    CommandName="cmdSaveImportExcelOTDay" OnCommand="btnCommand">บันทึก</asp:LinkButton>

                                            </div>

                                        </asp:Panel>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnImportExcelOTDay" />
                    </Triggers>

                </asp:UpdatePanel>

            </div>
        </asp:View>
        <!--View Import OT Day-->

        <!--View Report Import OT Day -->
        <asp:View ID="docReportOTDay" runat="server">
            <div class="col-md-12">


                <!-- Back Detail Report EmployeeImportDay -->
                <asp:UpdatePanel ID="_PanelBackToDetailReportImportOTDay" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetailReportImportOTDay" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetailReportImportOTDay" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back Detail Report EmployeeImportDay -->


                <asp:UpdatePanel ID="_PanelReportDay" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายงาน OT พนักงานรายวัน  </h3>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>โรงงาน</label>
                                            <asp:DropDownList ID="ddlplace_report" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>องค์กร</label>
                                            <asp:DropDownList ID="ddlorg_report" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_typeDayWork" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ฝ่าย</label>
                                            <asp:DropDownList ID="ddlrdept_report" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_typeDayWork" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>แผนก</label>
                                            <asp:DropDownList ID="ddlrsec_report" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_typeDayWork" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทวันทำงาน</label>
                                            <asp:DropDownList ID="ddl_type_daywork_report" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_typeDayWork" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภท OT</label>
                                            <asp:DropDownList ID="ddl_typeot_report" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_typeDayWork" runat="server" ValidationGroup="AddGroupOTDay" placeholder="กรอกชื่อกลุ่ม ..." CssClass="form-control" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>วันที่เริ่มทำ</label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateot_dayform_report" placeholder="วันที่เริ่ม ..." runat="server" CssClass="form-control datetimepicker-from cursor-pointer" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงวันที่</label>
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_dateot_dayto_report" placeholder="ถึงวันที่ ..." runat="server" CssClass="form-control datetimepicker-to cursor-pointer" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เวลาที่เริ่ม</label>
                                            <div class="input-group time">
                                                <asp:TextBox ID="txt_time_dayform_report" placeholder="เวลา ..." runat="server"
                                                    CssClass="form-control clockpicker cursor-pointer" value="" />
                                                <span class="input-group-addon show-time-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
							                    runat="server"
							                    ControlToValidate="txt_timestart_form" Display="None" SetFocusOnError="true"
							                    ErrorMessage="*กรุณาเลือกเวลา"
							                    ValidationGroup="HeadSaveSetOTMonth" />
						                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
							                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_form" Width="160" PopupPosition="BottomLeft" />--%>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ถึงเวลา</label>
                                            <div class="input-group time">
                                                <asp:TextBox ID="txt_time_dayto_report" placeholder="เวลา ..." runat="server"
                                                    CssClass="form-control clockpickerto cursor-pointer" value="" />
                                                <span class="input-group-addon show-timeto-onclick cursor-pointer">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
							                    runat="server"
							                    ControlToValidate="txt_timestart_form" Display="None" SetFocusOnError="true"
							                    ErrorMessage="*กรุณาเลือกเวลา"
							                    ValidationGroup="HeadSaveSetOTMonth" />
						                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
							                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_timestart_form" Width="160" PopupPosition="BottomLeft" />--%>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <asp:LinkButton ID="btnSearchReportDay" CssClass="btn btn-success" data-toggle="tooltip" title="Search" runat="server" CommandName="cmdSearchReportDay"
                                                OnCommand="btnCommand">ค้นหา</asp:LinkButton>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <asp:GridView ID="GvSearchReportOTDay" runat="server"
                            AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound"
                            ShowHeaderWhenEmpty="True"
                            AllowPaging="True"
                            PageSize="5"
                            BorderStyle="None"
                            CellSpacing="2">
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex +1) %>
                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <p>
                                            <b>ชื่อ-สกุลผู้สร้าง:</b>
                                            <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                            <asp:Label ID="lbl_emp_name_th_detail_otday" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </p>
                                        <p>
                                            <b>วันที่สร้าง:</b>
                                            <asp:Label ID="lbl_create_date_detailimport" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_create_date_detailimport" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                        </p>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                    <ItemTemplate>
                                        <p>
                                            <b>โรงงาน:</b>
                                            <asp:Label ID="lbl_LocName_detailimport" Visible="true" runat="server" Text='<%# Eval("LocName") %>' />
                                        </p>
                                        <p>
                                            <b>แผนก:</b>
                                            <asp:Label ID="lbl_SecNameTH_detailimport" Visible="true" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                        </p>
                                        <p>
                                            <b>ประเภทวันทำงาน:</b>
                                            <asp:Label ID="lbl_type_daywork_name_detailimport" Visible="true" runat="server" Text='<%# Eval("type_daywork_name")%>' />

                                        </p>
                                        <p>
                                            <b>ประเภท OT:</b>
                                            <asp:Label ID="lbl_type_ot_name_detailimport" Visible="true" runat="server" Text='<%# Eval("type_ot_name")%>' />
                                        </p>
                                        <p>
                                            <b>วันที่เริ่มทำ:</b>
                                            <asp:Label ID="lbl_dateot_from_detailimport" Visible="true" runat="server" Text='<%# Eval("dateot_from")%>' />
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_form_detailimport" Visible="true" runat="server" Text='<%# Eval("time_form")+ " " + "น."%>' />
                                        </p>
                                        <p>
                                            <b>ถึงวันที่:</b>
                                            <asp:Label ID="lbl_dateot_to_detailimport" Visible="true" runat="server" Text='<%# Eval("dateot_to")%>' />
                                            <b>เวลา:</b>
                                            <asp:Label ID="lbl_time_to_detailimport" Visible="true" runat="server" Text='<%# Eval("time_to") + " " + "น."%>' />
                                        </p>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จำนวนชั่วโมง(รวม)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:Label ID="lbl_day_all_sum_detailimportvalue" Visible="false" runat="server" Text='<%# Eval("day_all_sum")%>' />
                                        <asp:Label ID="lbl_day_all_sum_detailimport" Visible="true" runat="server" />


                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-center" Visible="false">

                                    <ItemTemplate>

                                        <asp:Label ID="lbl_staidx_detail_detailimport" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                        <asp:Label ID="lbl_status_name_detail_detailimport" runat="server" Text='<%# Eval("status_name") %>' />
                                        <asp:Label ID="lbl_m0_node_idx_detail_detailimport" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_node_name_detail_detailimport" runat="server" Text='<%# Eval("node_name") %>' />
                                        <br />
                                        โดย<br />
                                        <asp:Label ID="lbl_m0_actor_idx_detail_detailimport" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                        <asp:Label ID="lbl_actor_name_detail_detailimport" runat="server" Text='<%# Eval("actor_name")%>' />



                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <asp:UpdatePanel ID="_PanelViewEmployeeDetailOTDay" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnSearchReportEmployeeImportOTDay" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdSearchReportEmployeeImportOTDay" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                    data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                            </ContentTemplate>

                                            <%--<Triggers>
                                                <asp:PostBackTrigger ControlID="btnViewDetail" />
                                            </Triggers>--%>
                                        </asp:UpdatePanel>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </ContentTemplate>
                    <%--  <Triggers>
                    <asp:PostBackTrigger ControlID="btnImportExcelOTDay" />
                </Triggers>--%>
                </asp:UpdatePanel>


                <asp:FormView ID="FvReportViewDetailEmployeeImportDay" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">บันทึกการทำงานล่วงเวลา(สำหรับพนักงานรายวัน)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">


                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="lbl_emp_code" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก/ฝ่าย : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_u0_document_idx_viewimport" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                            <asp:Label ID="lbl_dept_name_th_view" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sec_name_th") + " " + "/" + " " +  Eval("dept_name_th")%>' />

                                        </div>

                                        <asp:Label ID="lbl_emp_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_dateot_from_viewimport" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("dateot_from") + " " + "-" + " " +  Eval("dateot_to")%>' />
                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="โรงงาน : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_LocName_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("LocName")%>' />

                                        </div>

                                        <asp:Label ID="lbl_dept_name_th" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_time_form_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_form")  + " " + "-" + " " +  Eval("time_to") + " " + "น."%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทวันทำงาน : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_daywork_name_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_daywork_name")%>' />

                                        </div>

                                        <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภท OT : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_type_ot_name_viewimport" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("type_ot_name")%>' />

                                        </div>
                                        <%-- </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-md-12">--%>
                                        <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="จำนวนรวม(ชั่วโมง) : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_day_all_sum_viewimportvaluereport" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("day_all_sum")%>' />
                                            <asp:Label ID="lbl_day_all_sum_viewimportreport" runat="server" CssClass="control-labelnotop col-xs-12" />

                                        </div>


                                        <%-- </div>--%>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <div id="divReportGvViewDetailEmployeeImportDay" style="overflow-y: scroll; width: auto; height: auto" runat="server">
                    <asp:GridView ID="GvReportEmployeeImportDay" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="False" PageSize="2"
                        ShowFooter="true">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_emp_code_otday" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อ - นามสกุล" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_emp_name_th_otday" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </small>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_pos_name_th_otday" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                        <%-- <asp:Label ID="lbl_sec_name_th_excel_otday" runat="server" Text='<%# Eval("pos_name_th")%>'></asp:Label>--%>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Literal ID="lit_time_import" runat="server" Text="เวลารวม :"></asp:Literal>
                                        </div>

                                    </small>

                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="26" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_26_excel_otday" runat="server" Text='<%# Eval("ot_day_26")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <%--background-color: chartreuse;--%>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_26_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="27" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_27_excel_otday" runat="server" Text='<%# Eval("ot_day_27")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_27_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="28" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_28_excel_otday" runat="server" Text='<%# Eval("ot_day_28")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_28_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="29" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_29_excel_otday" runat="server" Text='<%# Eval("ot_day_29")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_29_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="30" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_30_excel_otday" runat="server" Text='<%# Eval("ot_day_30")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_30_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="31" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_31_excel_otday" runat="server" Text='<%# Eval("ot_day_31")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_31_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="1" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_1_excel_otday" runat="server" Text='<%# Eval("ot_day_1")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_1_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="2" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_2_excel_otday" runat="server" Text='<%# Eval("ot_day_2")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_2_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="3" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_3_excel_otday" runat="server" Text='<%# Eval("ot_day_3")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_3_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="4" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_4_excel_otday" runat="server" Text='<%# Eval("ot_day_4")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_4_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="5" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_5_excel_otday" runat="server" Text='<%# Eval("ot_day_5")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_5_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="6" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_6_excel_otday" runat="server" Text='<%# Eval("ot_day_6")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_6_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="7" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_7_excel_otday" runat="server" Text='<%# Eval("ot_day_7")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_7_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="8" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_8_excel_otday" runat="server" Text='<%# Eval("ot_day_8")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_8_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="9" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_9_excel_otday" runat="server" Text='<%# Eval("ot_day_9")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_9_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="10" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_10_excel_otday" runat="server" Text='<%# Eval("ot_day_10")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_10_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="11" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_11_excel_otday" runat="server" Text='<%# Eval("ot_day_11")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_11_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="12" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_12_excel_otday" runat="server" Text='<%# Eval("ot_day_12")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_12_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="13" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_13_excel_otday" runat="server" Text='<%# Eval("ot_day_13")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_13_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="14" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_14_excel_otday" runat="server" Text='<%# Eval("ot_day_14")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_14_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="15" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_15_excel_otday" runat="server" Text='<%# Eval("ot_day_15")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_15_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="16" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_16_excel_otday" runat="server" Text='<%# Eval("ot_day_16")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_16_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="17" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_17_excel_otday" runat="server" Text='<%# Eval("ot_day_17")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_17_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="18" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_18_excel_otday" runat="server" Text='<%# Eval("ot_day_18")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_18_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="19" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_19_excel_otday" runat="server" Text='<%# Eval("ot_day_19")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_19_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="20" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_20_excel_otday" runat="server" Text='<%# Eval("ot_day_20")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_20_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="21" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_21_excel_otday" runat="server" Text='<%# Eval("ot_day_21")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_21_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="22" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_22_excel_otday" runat="server" Text='<%# Eval("ot_day_22")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_22_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="23" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_23_excel_otday" runat="server" Text='<%# Eval("ot_day_23")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_23_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="24" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_24_excel_otday" runat="server" Text='<%# Eval("ot_day_24")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_24_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="25" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_ot_day_25_excel_otday" runat="server" Text='<%# Eval("ot_day_25")%>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_total_ot_day_25_excel_otday" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชั่วโมง(รวม)" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbl_totalhours_" runat="server"></asp:Label>
                                        <asp:Label ID="lbl_totalhours_sumtotal" runat="server" Visible="false"></asp:Label>
                                    </small>
                                </ItemTemplate>

                                <FooterTemplate>
                                    <small>
                                        <div style="text-align: right;">
                                            <asp:Label ID="lit_totalhours_sumtotal" runat="server" Font-Bold="true"></asp:Label>

                                        </div>
                                    </small>
                                </FooterTemplate>

                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>


            </div>
        </asp:View>
        <!--View Report Import OT Day -->

        <!--View Report OT Month -->
        <asp:View ID="docReportOTMonth" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelReportOTMonth" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายงาน OT ประเภทกะคงที่</h3>
                            </div>

                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เดือน</label>
                                            <asp:DropDownList ID="ddlMonthReport" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="07">กรกฎาคม</asp:ListItem>
                                                <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Req_ddlMonthReport"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlMonthReport" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกเดือน"
                                                ValidationGroup="SearchReportOTmonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5111" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlMonthReport" Width="160" PopupPosition="BottomLeft" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <asp:DropDownList ID="ddlYearReport" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>องค์กร</label>
                                            <asp:DropDownList ID="ddlorgMonthReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<p class="help-block"><font color="red">**กรุณาเลือกองค์กร</font></p>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlorg" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationGroup="SaveImportExcelEmployeeOTDay" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorg" Width="160" PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ฝ่าย</label>
                                            <asp:DropDownList ID="ddlrdeptMonthReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>แผนก</label>
                                            <asp:DropDownList ID="ddlrsecMonthReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ชื่อพนักงาน</label>
                                            <asp:DropDownList ID="ddlempMonthReport" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="--- เลือกพนักงาน ---" Value="0"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Coscenter</label>
                                            <asp:TextBox ID="txtCostcenterReport" placeholder="Coscenter" runat="server" CssClass="form-control"> </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnSearchReportMonthx1" CssClass="btn btn-success" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x1" runat="server" CommandArgument="1" CommandName="cmdSearchReportMonth"
                                                OnCommand="btnCommand">ค่าล่วงเวลา 1 เท่า</asp:LinkButton>

                                            <asp:LinkButton ID="btnSearchReportMonthx2" CssClass="btn btn-success" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x1.5" runat="server" CommandArgument="2" CommandName="cmdSearchReportMonth"
                                                OnCommand="btnCommand">ค่าล่วงเวลา 1.5 เท่า</asp:LinkButton>

                                            <asp:LinkButton ID="btnSearchReportMonthx3" CssClass="btn btn-success" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x3" runat="server" CommandArgument="3" CommandName="cmdSearchReportMonth"
                                                OnCommand="btnCommand">ค่าล่วงเวลา 3 เท่า</asp:LinkButton>

                                            <asp:LinkButton ID="btnResetSearchReport" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchReport" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Export Excel Report Month -->
                <asp:UpdatePanel ID="_PanelExportExcelReportMonth" runat="server">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnExportExcelReportMonth" CssClass="btn btn-success" data-toggle="tooltip" title="Export Excel" runat="server"
                                CommandName="cmdExportExcelReportMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-export"></span> Export Excel</asp:LinkButton>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelReportMonth" />
                    </Triggers>

                </asp:UpdatePanel>
                <!-- Export Excel Report Month -->

                <asp:GridView ID="GvReportOTMonth" runat="server"
                    AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound"
                    ShowHeaderWhenEmpty="False"
                    AllowPaging="True"
                    PageSize="5"
                    BorderStyle="None"
                    CellSpacing="2">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                       
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <p>
                                    <%-- <b>ชื่อ-สกุลผู้สร้าง:</b>--%>
                                    <asp:Label ID="lbl_u0_doc_idx_reportmonth" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>' />
                                    <asp:Label ID="lbl_emp_code_reportmonth" runat="server" Text='<%# Eval("emp_code") %>' />
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทเงินได้" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lbl_condition_otmonth_reportmonth" runat="server" Text='<%# Eval("condition_otmonth") %>' />
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวน ot ก่อน(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <p>
                                    <%--<b>โรงงาน:</b>--%>
                                    <asp:Label ID="lbl_ot_before_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_before") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวน ot หลัง(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <p>
                                    <%--<b>โรงงาน:</b>--%>
                                    <asp:Label ID="lbl_ot_after_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_after") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวน ot วันหยุด(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                            <ItemTemplate>
                                <p>
                                    <%--<b>โรงงาน:</b>--%>
                                    <asp:Label ID="lbl_ot_holiday_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_holiday") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จำนวน ot รวม (ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                            <ItemTemplate>

                                <asp:Label ID="lbl_ot_total_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_total")%>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-CssClass="text-left">
                            <ItemTemplate>

                                <asp:Label ID="lbl_remark_otmonth_reportmonth" Visible="true" runat="server" Text='<%# Eval("remark_otmonth")%>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>



            </div>
        </asp:View>
        <!--View Report OT Month -->

    </asp:MultiView>
    <!--multiview-->

    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>


    <script type="text/javascript">

        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            //stepping: 30,
            ignoreReadonly: true
        });

        $('.clockpicker').on('dp.change', function (e) {
            var dateTo = $('.clockpickerto').data("DateTimePicker").date();
            if (e.date > dateTo) {
                $('.clockpickerto').data("DateTimePicker").date(e.date.format("HH:mm"));
            }

        });

        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            //stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        $('.clockpickerto').on('dp.change', function (e) {
            var dateFrom = $('.clockpicker').data("DateTimePicker").date();
            if (e.date < dateFrom) {
                $('.clockpicker').data("DateTimePicker").date(e.date.format("HH:mm"));
            }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
        });


        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                //stepping: 30,
                ignoreReadonly: true
            });

            $('.clockpicker').on('dp.change', function (e) {
                var dateTo = $('.clockpickerto').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.clockpickerto').data("DateTimePicker").date(e.date.format("HH:mm"));
                }

            });

            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                //stepping: 30,
                ignoreReadonly: true
            });

            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });

            $('.clockpickerto').on('dp.change', function (e) {
                var dateFrom = $('.clockpicker').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.clockpicker').data("DateTimePicker").date(e.date.format("HH:mm"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });


        });


    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                ////minDate: moment().startOf('month'),
                ////maxDate: moment().endOf('month')
                //maxDate: moment().endOf('month').add(30, 'day')
                minDate: moment().add(-7, 'day'),
                maxDate: moment().add(-1, 'day'),
                //ignoreReadonly: true

            });

            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ////minDate: moment().startOf('month'),
                ////maxDate: moment().endOf('month')
                //maxDate: moment().endOf('month').add(1, 'month')
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });

        $(function () {
            ////$('.datetimepicker-fromsearch').datetimepicker({

            ////    format: 'DD/MM/YYYY',
            ////    minDate: moment().add(-7, 'day'),
            ////    maxDate: moment()//.add(-1, 'day'),

            ////});
            ////$('.show-fromsearch-onclick').click(function () {
            ////     alert("4");
            ////    $('.datetimepicker-fromsearch').data("DateTimePicker").show;
            ////});


            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-7, 'day'),
                maxDate: moment().add(-1, 'day'),
                ////minDate: moment().startOf('month'),
                ////maxDate: moment().endOf('month')
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
            });

            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }


                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show;
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ////minDate: moment().startOf('month'),
                ////maxDate: moment().endOf('month')

                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });
        });
    </script>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                var string = $(".datetimepicker-fromsearch").val();
                //alert(string);
                if (string == '') {
                    // alert(1);
                    $('.datetimepicker-fromsearch').datetimepicker({

                        minDate: moment().add(-30, 'day'),
                        maxDate: moment().add(-1, 'day'),
                        format: 'DD/MM/YYYY',
                        ignoreReadonly: true,

                    });

                }
                else {
                    //alert(2);
                    $('.datetimepicker-fromsearch').datetimepicker({

                        minDate: moment().add(-30, 'day'),
                        maxDate: moment().add(-1, 'day'),
                        format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                        //ignoreReadonly: true,

                    }).val(string);
                }

                $('.show-fromsearch-onclick').click(function (e) {

                    //alert("3");
                    $('.datetimepicker-fromsearch').datetimepicker.show;
                });
            });
        });

    </script>

    <script type="text/javascript">
       
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-fromsearch').datetimepicker({

                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-fromsearch').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-tosearch').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-tosearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclicksearch').click(function () {
                $('.datetimepicker-fromsearch').data("DateTimePicker").show();
            });
            $('.datetimepicker-tosearch').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclicksearch').click(function () {
                $('.datetimepicker-tosearch').data("DateTimePicker").show();
            });
            $('.datetimepicker-tosearch').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-fromsearch').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               
            });

           
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

</asp:Content>

