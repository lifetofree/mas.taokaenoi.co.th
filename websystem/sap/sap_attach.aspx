<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="sap_attach.aspx.cs" Inherits="websystem_sap_sap_attach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                รายการไฟล์</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="navCreate" OnCommand="navCommand">
                                อัพโหลดไฟล์</asp:LinkButton>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" runat="server">
                        <li id="li3" runat="server">
                            <asp:HyperLink ID="hlCombine" NavigateUrl="https://combinepdf.com/" Target="_blank" runat="server"><i class="far fa-file-pdf"></i> รวมไฟล์ PDF</asp:HyperLink>
                        </li>
                        <li id="li2" runat="server">
                            <asp:HyperLink ID="hlManual" NavigateUrl="https://docs.google.com/document/d/1wg69EqNE_vGd_3GahFvIUwiduD5eh-SlEe-_udLJIyw/edit?usp=sharing" Target="_blank" runat="server"><i class="fas fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                <div class="row form-group">
                    <label class="col-md-8"></label>
                    <div class="col-md-4" style="padding-right: 0; padding-bottom: 10px;">
                        <div class="input-group">
                            <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control"
                                placeholder="ค้นหาโดย PR No." MaxLength="20">
                            </asp:TextBox>
                            <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="input-group-addon"
                                OnCommand="btnCommand" CommandName="cmdItemSearch"><i class="fas fa-search"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="lbItemReset" runat="server" CssClass="input-group-addon"
                                OnCommand="btnCommand" CommandName="cmdItemReset"><i class="fas fa-sync-alt"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <asp:GridView ID="gvFileList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive" OnRowDataBound="gvRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging" AllowPaging="true" PageSize="20">
                    <HeaderStyle CssClass="info" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก"
                        LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PR No.">
                            <ItemTemplate>
                                <asp:Label ID="lblPrNo" runat="server" Text='<%# Eval("pr_no") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Link">
                            <ItemTemplate>
                                <asp:Literal ID="litFileName" runat="server" Text='<%# "http://drives.taokaenoi.co.th/SAP/" + Eval("file_name") %>'>
                                </asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="File Comment">
                            <ItemTemplate>
                                <asp:Label ID="lblFileComment" runat="server" Text='<%# Eval("file_comment") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View ID="viewCreate" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info" id="div_list_heading" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="far fa-user"></i>&nbsp;<asp:Literal ID="litListHeadingTitle" runat="server"
                                Text="ข้อมูลพนักงาน"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvEmpProfile" runat="server" Width="100%" DefaultMode="ReadOnly">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hfRdeptIdx" runat="server" Value='<%# Eval("rdept_idx")%>'>
                                    </asp:HiddenField>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ชื่อ-นามสกุล :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">บริษัท :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbOrgNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("org_name_th") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ฝ่าย :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbDeptNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("dept_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">แผนก :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbSecNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("sec_name_th") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbPosNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("pos_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <div class="panel panel-primary" id="div_list_heading2" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fas fa-upload"></i>&nbsp;<asp:Literal ID="litListHeadingTitle2" runat="server"
                                Text="อัพโหลดไฟล์"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvUploadFiles" runat="server" Width="100%" DefaultMode="Insert">
                            <InsertItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">PR No. :<span class="text-danger">*</span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbPrNo" runat="server" CssClass="form-control"
                                                MaxLength="20">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">Comment :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbComment" runat="server" CssClass="form-control"
                                                MaxLength="1000">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">File :<span class="text-danger">*</span></label>
                                        <div class="col-md-4">
                                            <asp:FileUpload ID="fuUpload" ViewStateMode="Enabled" AutoPostBack="true"
                                                ClientIDMode="Static" runat="server"
                                                CssClass="btn btn-md multi max-1 accept-pdf" />
                                                <span class="text-danger">*เฉพาะไฟล์ pdf เท่านั้น</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label"></label>
                                        <div class="col-md-4">
                                            <asp:LinkButton ID="lbUpload" runat="server"
                                                CssClass="btn btn-sm btn-success" OnCommand="btnCommand"
                                                CommandName="cmdUpload"><i class="fas fa-upload"></i>&nbsp;Upload
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCancel" runat="server"
                                                CssClass="btn btn-sm btn-danger" OnCommand="btnCommand"
                                                CommandName="cmdCancel"><i class="fas fa-times"></i>&nbsp;Cancel
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">PR No. :</label>
                                        <div class="col-md-4 form-control-static">
                                            <asp:Literal ID="litPrNo" runat="server" Text='<%# Eval("pr_no") %>'>
                                            </asp:Literal>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">Comment :</label>
                                        <div class="col-md-4 form-control-static">
                                            <asp:Literal ID="litComment" runat="server"
                                                Text='<%# Eval("file_comment") %>'></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label">File URL :</label>
                                        <div class="col-md-4 form-control-static">
                                            <asp:Literal ID="litUrl" runat="server" Text='<%# "http://drives.taokaenoi.co.th/SAP/" + Eval("file_name") %>'>
                                            </asp:Literal>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-md-offset-3 control-label"></label>
                                        <div class="col-md-4">
                                            <asp:LinkButton ID="lbCancel" runat="server"
                                                CssClass="btn btn-sm btn-default" OnCommand="btnCommand"
                                                CommandName="cmdCancel"><i class="fas fa-arrow-left"></i></i>&nbsp; Back
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(".multi").MultiFile();
        })
    </script>
</asp:Content>