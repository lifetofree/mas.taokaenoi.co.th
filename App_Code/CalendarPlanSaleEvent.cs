﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CalendarPlanSale
/// </summary>
public class CalendarPlanSaleEvent
{
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public bool allDay { get; set; }
    public string parttime_name_th { get; set; }
    public int u0_empshift_idx { get; set; }
    public int emp_idx { get; set; }
    public int u0_unidx { get; set; }
    public int u0_acidx { get; set; }
    public int doc_decision { get; set; }
    public string ty_name { get; set; }
}