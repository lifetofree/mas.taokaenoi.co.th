﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="emps.aspx.cs" Inherits="websystem_emps_emps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="test" runat="server" />
   <input type="hidden" id="txtEmpIdxLogin" class="txtEmpIdxLogin" runat="server" />
   <!--*** START Menu ***-->
   <div id="divMenu" runat="server" class="col-md-12">
      <nav class="navbar navbar-default">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
               data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">Menu</a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" runat="server">
               <li id="_divMenuLiToDivIndex" runat="server">
                  <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                     CommandName="_divMenuBtnToDivIndex"
                     OnCommand="btnCommand" Text="รายการทั่วไป" />
               </li>
               <li id="_divMenuLiToDivAnnounce" runat="server" class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ประกาศกะการทำงาน <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                     <li>
                        <asp:LinkButton ID="_divMenuBtnToDivAnnounceDiary" runat="server"
                           CommandName="_divMenuBtnToDivAnnounceDiary"
                           OnCommand="btnCommand" Text="รายวัน" />
                     </li>
                     <li role="separator" class="divider"></li>
                     <li>
                        <asp:LinkButton ID="_divMenuBtnToDivAnnounceMonthly" runat="server"
                           CommandName="_divMenuBtnToDivAnnounceMonthly"
                           OnCommand="btnCommand" Text="รายเดือน" />
                     </li>
                  </ul>
               </li>
               <li id="_divMenuLiToDivChangeAnnounce" runat="server">
                  <asp:LinkButton ID="_divMenuBtnToDivChangeAnnounce" runat="server"
                     CommandName="_divMenuBtnToDivChangeAnnounce"
                     OnCommand="btnCommand" Text="ขอเปลี่ยนกะการทำงาน" />
               </li>
               <li id="_divMenuLiToDivHolidaySwap" runat="server" visible="false">
                  <asp:LinkButton ID="_divMenuBtnToDivHolidaySwap" runat="server"
                     CommandName="_divMenuBtnToDivHolidaySwap"
                     OnCommand="btnCommand" Text="ขอสลับวันหยุด" />
               </li>
               <li id="_divMenuLiToDivEditWaiting" runat="server">
                  <asp:LinkButton ID="_divMenuBtnToDivEditWaiting" runat="server"
                     CommandName="_divMenuBtnToDivEditWaiting" OnCommand="btnCommand"
                     Text="รายการที่รอแก้ไข" />
               </li>
               <li id="_divMenuLiToDivPending" runat="server" visible="false">
                  <asp:LinkButton ID="_divMenuBtnToDivPending" runat="server"
                     CommandName="_divMenuBtnToDivPending" OnCommand="btnCommand"
                     Text="รายการที่รออนุมัติ" />
               </li>
               <li id="_divMenuLiToDivHRArea" runat="server" visible="false">
                  <asp:LinkButton ID="_divMenuBtnToDivHRArea" runat="server"
                     CommandName="_divMenuBtnToDivHRArea" OnCommand="btnCommand"
                     Text="ส่วนของ HR" />
               </li>
            </ul>
         </div>
      </nav>
   </div>
   <!--*** END Menu ***-->

   <!--*** START MENU liToIndexEmpShift ZONE ***-->
   <div id="divIndex" runat="server">
      <!-- START Index search -->
      <div id="_divIndexSearch" runat="server" class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ค้นหารายการทั่วไป</div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">องค์กร</label>
                        <asp:DropDownList ID="ddlIndexOrgSearch" runat="server" CssClass="form-control" AutoPostBack="true"
                           OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">ฝ่าย</label>
                        <asp:DropDownList ID="ddlIndexDeptSearch" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทพนักงาน</label>
                        <asp:DropDownList ID="ddlIndexTypesEmployee" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1">รายวัน</asp:ListItem>
                           <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทเอกสาร</label>
                        <div class="form-inline">
                           <asp:DropDownList ID="ddlIndexTypesDocSearch" runat="server" CssClass="form-control">
                              <asp:ListItem Value="1">ตารางกะการทำงาน</asp:ListItem>
                              <asp:ListItem Value="2">ขอเปลี่ยนกะการทำงาน</asp:ListItem>
                           </asp:DropDownList>
                           <asp:Button ID="_divMenuBtnToDivIndexBtnIndexSearch" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="_divMenuBtnToDivIndexBtnIndexSearch" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Index search -->

      <!-- START Index announce diary list -->
      <div id="_divIndexAnnounceDiaryList" runat="server" visible="false" class="col-md-12">
         <asp:GridView ID="gvEmpshiftDiaryIndexList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceDiaryStartDate" runat="server" Text='<%# Eval("announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceDiaryEndDate" runat="server" Text='<%# Eval("announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                     <br />
                     <asp:Label ID="announceDiaryStatus" runat="server" Text='<%# getStatusForAnnouncement((int)Eval("announce_diary_status")) %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="btnViewDescriptionAnnounceDiary" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="btnViewDescriptionAnnounceDiary" CommandArgument='<%# Eval("u0_announce_diary_idx") %>'>
                     <i class="fa fa-file-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END Index announce diary list -->

      <!-- START Index announce diary description -->
      <div id="_divIndexAnnounceDiaryDescription" runat="server" visible="false" class="col-md-12">
         <input type="hidden" runat="server" id="announceDiaryIdxViewDescription" class="announceDiaryIdxViewDescription" />
         <asp:LinkButton ID="btnBackToIndexEmpshiftDiary" runat="server" CssClass="btn btn-danger m-b-10"
            OnCommand="btnCommand" CommandName="btnBackToIndexEmpshift">
            <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
         </asp:LinkButton>
         <div class="clearfix"></div>

         <!-- START View Description Index Calendar Diary -->
         <div class="panel panel-info">
            <div class="panel-heading f-bold">รายละเอียด</div>
            <div class="panel-body">
               <div class="col-md-4">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">รายชื่อพนักงาน</div>
                     <div class="emp-list-diary-pending">
                        <table class="table table-striped f-s-12">
                           <asp:Repeater ID="rpEmployeeDiaryIndexDescription" runat="server">
                              <HeaderTemplate>
                                 <tr>
                                    <th>#</th>
                                    <th>รหัสพนักงาน</th>
                                    <th>ชื่อ - สกุล</th>
                                 </tr>
                              </HeaderTemplate>
                              <ItemTemplate>
                                 <tr>
                                    <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                    <td data-th="รหัสพนักงาน"><%# Eval("EmpCode") %></td>
                                    <td data-th="ชื่อ - สกุล"><%# Eval("FullNameTH") %></td>
                                 </tr>
                              </ItemTemplate>
                           </asp:Repeater>
                        </table>
                     </div>
                  </div>
               </div>
               <div class="col-md-8">
                  <asp:Label ID="lblViewDescriptionIndexCalendarDiary" runat="server" CssClass="lblViewDescriptionIndexCalendarDiary" />
               </div>
            </div>
         </div>
         <!-- END View Description Index Calendar Diary -->

         <!-- START Gridview Log Index Diary -->
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ประวัติการดำเนินการ</div>
            <div class="panel-body">
               <table class="table table-striped table-empshift-responsive f-s-12">
                  <asp:Repeater ID="rpApprovalLogDiary" runat="server">
                     <HeaderTemplate>
                        <tr>
                           <th>วัน / เวลา</th>
                           <th>ผู้ดำเนินการ</th>
                           <th>ดำเนินการ</th>
                           <th>ผลการดำเนินการ</th>
                        </tr>
                     </HeaderTemplate>
                     <ItemTemplate>
                        <tr>
                           <td data-th="วัน / เวลา"><%# Eval("approval_log_date") + " " + Eval("approval_log_time") %> น.</td>
                           <td data-th="ผู้ดำเนินการ"><%# Eval("FullNameTH") %></td>
                           <td data-th="ดำเนินการ"><%# Eval("from_node_name") %></td>
                           <td data-th="ผลการดำเนินการ"><%# Eval("m0_statnode_statnode_ref") %></td>
                        </tr>
                     </ItemTemplate>
                  </asp:Repeater>
               </table>
            </div>
         </div>
         <!-- END Gridview Log Index Diary -->
      </div>
      <!-- END Index announce diary description -->

      <!-- START Index change announce diary list -->
      <div id="_divIndexChangeAnnounceDiaryList" runat="server" visible="false" class="col-md-12">
         <asp:GridView ID="gvChangeEmpshiftDiaryIndexList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("change_announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="changeAnnounceDiaryStartDate" runat="server" Text='<%# Eval("change_announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="changeAnnounceDiaryEndDate" runat="server" Text='<%# Eval("change_announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceDiaryNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                     <br />
                     <asp:Label ID="changeAnnounceDiaryStatus" runat="server" Text='<%# getStatusForAnnouncement((int)Eval("change_announce_diary_status")) %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="btnViewDescriptionChangeAnnounceDiary" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="btnViewDescriptionChangeAnnounceDiary" CommandArgument='<%# Eval("u0_change_announce_diary_idx") + "," + Eval("u0_announce_diary_idx_ref") %>'>
                     <i class="fa fa-file-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END Index change announce diary list -->

      <!-- START Index change announce diary description -->
      <div id="_divIndexChangeAnnounceDiaryDescription" runat="server" visible="false">
         <input type="hidden" runat="server" id="announceDiaryIdxIndexDescriptionOfChange" class="announceDiaryIdxIndexDescriptionOfChange" />
         <input type="hidden" runat="server" id="changeAnnounceDiaryIdxIndexDescriptionOfChange" class="changeAnnounceDiaryIdxIndexDescriptionOfChange" />
         <div class="col-md-12">
            <asp:LinkButton ID="btnBackToIndexChangeEmpshiftDiary" runat="server" CssClass="btn btn-danger m-b-10"
               OnCommand="btnCommand" CommandName="btnBackToIndexChangeEmpshiftDiary">
            <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
            </asp:LinkButton>
         </div>
         <div class="row">
            <div class="row">
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">กะการทำงานเดิม</div>
                     <div class="panel-body">
                        <asp:Label ID="lblViewDescriptionIndexOldCalendarDiary" runat="server" CssClass="lblViewDescriptionIndexOldCalendarDiary" />
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">กะการทำงานใหม่</div>
                     <div class="panel-body">
                        <asp:Label ID="lblViewDescriptionIndexNewCalendarDiary" runat="server" CssClass="lblViewDescriptionIndexNewCalendarDiary" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">ประวัติการดำเนินการ</div>
               <div class="panel-body">
                  <table class="table table-striped table-empshift-responsive f-s-12">
                     <asp:Repeater ID="rpChangeAnnounceDiaryApprovalLog" runat="server">
                        <HeaderTemplate>
                           <tr>
                              <th>วัน / เวลา</th>
                              <th>ผู้ดำเนินการ</th>
                              <th>ดำเนินการ</th>
                              <th>ผลการดำเนินการ</th>
                           </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <tr>
                              <td data-th="วัน / เวลา"><%# Eval("approval_log_date") + " " + Eval("approval_log_time") %> น.</td>
                              <td data-th="ผู้ดำเนินการ"><%# Eval("FullNameTH") %></td>
                              <td data-th="ดำเนินการ"><%# Eval("from_node_name") %></td>
                              <td data-th="ผลการดำเนินการ"><%# Eval("m0_statnode_statnode_ref") %></td>
                           </tr>
                        </ItemTemplate>
                     </asp:Repeater>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END Index change announce diary description -->

      <!-- START Index announce monthly list -->
      <div id="_divIndexAnnounceMonthlyList" runat="server" visible="false" class="col-md-12">
         <asp:GridView ID="gvEmpshiftIndexList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceCreatedAt" runat="server"
                        Text='<%# Eval("announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                     <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                     <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                     <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                     <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                     <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                     <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceStartDate" runat="server" Text='<%# Eval("announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceEndDate" runat="server" Text='<%# Eval("announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                     <br />
                     <asp:Label ID="announceStatus" runat="server" Text='<%# getStatusForAnnouncement((int)Eval("announce_status")) %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="btnViewDescriptionAnnounceMonthly" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="btnViewDescriptionAnnounceMonthly" CommandArgument='<%# Eval("u0_announce_idx") %>'>
                     <i class="fa fa-file-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END Index announce monthly list -->

      <!-- START Index announce monthly description -->
      <div id="_divIndexAnnounceMonthlyDescription" runat="server" visible="false" class="col-md-12">
         <input type="hidden" runat="server" id="announceIdxViewDescription" class="announceIdxViewDescription" />
         <asp:LinkButton ID="btnBackToIndexEmpshift" runat="server" CssClass="btn btn-danger m-b-10"
            OnCommand="btnCommand" CommandName="btnBackToIndexEmpshift">
            <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
         </asp:LinkButton>
         <div class="clearfix"></div>

         <!-- START Employee Data -->
         <div class="row">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">ข้อมูลพนักงาน</div>
               <div class="panel-body emps-p-all-10">
                  <div class="form-group">
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Employee Code</span><br>
                           <small><b>รหัสพนักงาน</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblViewDescriptionIndexEmpCode" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Job Grade</span><br>
                           <small><b>Job Grade</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblViewDescriptionIndexJobLevel" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Full Name</span><br>
                           <small><b>ชื่อ - นามสกุล</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblViewDescriptionIndexFullNameTH" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Position</span><br>
                           <small><b>ตำแหน่ง</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblViewDescriptionIndexPosNameTH" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Department</span><br>
                           <small><b>ฝ่าย</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblViewDescriptionIndexDeptNameTH" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Section</span><br>
                           <small><b>แผนก</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblViewDescriptionIndexSecNameTH" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>First Approver</span><br>
                           <small><b>ผู้อนุมัติที่ 1</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblViewDescriptionIndexFirstApprover" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Second Approver</span><br>
                           <small><b>ผู้อนุมัติที่ 2</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblViewDescriptionIndexSecondApprover" runat="server" CssClass="control-label" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END Employee Data -->

         <!-- START View Description Index Calendar -->
         <div class="panel panel-info">
            <div class="panel-heading f-bold">กะการทำงาน</div>
            <div class="panel-body">
               <div class="col-md-10 col-md-offset-1">
                  <asp:Label ID="lblViewDescriptionIndexCalendar" runat="server" CssClass="lblViewDescriptionIndexCalendar" />
               </div>
            </div>
         </div>
         <!-- END View Description Index Calendar -->

         <!-- START Gridview Log Index -->
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ประวัติการดำเนินการ</div>
            <div class="panel-body">
               <table class="table table-striped table-empshift-responsive f-s-12">
                  <asp:Repeater ID="rpApprovalLog" runat="server">
                     <HeaderTemplate>
                        <tr>
                           <th>วัน / เวลา</th>
                           <th>ผู้ดำเนินการ</th>
                           <th>ดำเนินการ</th>
                           <th>ผลการดำเนินการ</th>
                        </tr>
                     </HeaderTemplate>
                     <ItemTemplate>
                        <tr>
                           <td data-th="วัน / เวลา"><%# Eval("approval_log_date") + " " + Eval("approval_log_time") %> น.</td>
                           <td data-th="ผู้ดำเนินการ"><%# Eval("FullNameTH") %></td>
                           <td data-th="ดำเนินการ"><%# Eval("from_node_name") %></td>
                           <td data-th="ผลการดำเนินการ"><%# Eval("m0_statnode_statnode_ref") %></td>
                        </tr>
                     </ItemTemplate>
                  </asp:Repeater>
               </table>
            </div>
         </div>
         <!-- END Gridview Log Index -->
      </div>
      <!-- END Index announce monthly description -->

      <!-- START Index change announce monthly list -->
      <div id="_divIndexChangeAnnounceMonthlyList" runat="server" visible="false" class="col-md-12">
         <asp:GridView ID="gvChangeEmpshiftIndexList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceCreatedAt" runat="server"
                        Text='<%# Eval("change_announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                     <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                     <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                     <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                     <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                     <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                     <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="changeAnnounceStartDate" runat="server" Text='<%# Eval("change_announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="changeAnnounceEndDate" runat="server" Text='<%# Eval("change_announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceNode" runat="server"
                        Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="btnViewDescriptionChangeAnnounceMonthly" runat="server" CssClass="btn btn-info" OnCommand="btnCommand"
                        CommandName="btnViewDescriptionChangeAnnounceMonthly" CommandArgument='<%# Eval("u0_change_announce_idx") + "," + Eval("u0_announce_idx_ref") %>'>
                     <i class="fa fa-file-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END Index change announce monthly list -->

      <!-- START Index change announce monthly description -->
      <div id="_divIndexChangeAnnounceMonthlyDescription" runat="server" visible="false">
         <input type="hidden" runat="server" id="announceIdxIndexDescription" class="announceIdxIndexDescription" />
         <input type="hidden" runat="server" id="changeAnnounceIdxIndexDescription" class="changeAnnounceIdxIndexDescription" />
         <div class="col-md-12">
            <asp:LinkButton ID="btnBackToIndexChangeEmpshift" runat="server" CssClass="btn btn-danger m-b-10"
               OnCommand="btnCommand" CommandName="btnBackToIndexChangeEmpshift">
            <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
            </asp:LinkButton>
         </div>
         <div class="row">
            <div class="row">
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">กะการทำงานเดิม</div>
                     <div class="panel-body">
                        <asp:Label ID="lblViewDescriptionIndexOldCalendar" runat="server" CssClass="lblViewDescriptionIndexOldCalendar" />
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">กะการทำงานใหม่</div>
                     <div class="panel-body">
                        <asp:Label ID="lblViewDescriptionIndexNewCalendar" runat="server" CssClass="lblViewDescriptionIndexNewCalendar" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">ประวัติการดำเนินการ</div>
               <div class="panel-body">
                  <table class="table table-striped table-empshift-responsive f-s-12">
                     <asp:Repeater ID="rpChangeAnnounceApprovalLog" runat="server">
                        <HeaderTemplate>
                           <tr>
                              <th>วัน / เวลา</th>
                              <th>ผู้ดำเนินการ</th>
                              <th>ดำเนินการ</th>
                              <th>ผลการดำเนินการ</th>
                           </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <tr>
                              <td data-th="วัน / เวลา"><%# Eval("approval_log_date") + " " + Eval("approval_log_time") %> น.</td>
                              <td data-th="ผู้ดำเนินการ"><%# Eval("FullNameTH") %></td>
                              <td data-th="ดำเนินการ"><%# Eval("from_node_name") %></td>
                              <td data-th="ผลการดำเนินการ"><%# Eval("m0_statnode_statnode_ref") %></td>
                           </tr>
                        </ItemTemplate>
                     </asp:Repeater>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END Index change announce monthly description -->
   </div> 
   <!--*** END MENU liToIndexEmpShift ZONE ***-->

   <!--*** START MENU liToAnnounceDiaryEmpShift ZONE ***-->
   <div id="divAnnounceDiaryToDo" runat="server" visible="false">
      <div class="col-md-12">
         <div class="alert alert-info f-s-14 f-bold" role="alert">รายวัน</div>
      </div>
      <!-- START Group Diary List Selection -->
      <div class="row">
         <div class="col-md-6">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">เลือกกลุ่ม</div>
               <div class="input-group">
                  <asp:TextBox ID="txtSearchGroupDiary" runat="server" placeholder="ชื่อกลุ่ม, องค์กร, ฝ่าย, แผนก"
                     CssClass="jstree-search-emps-form-control txtSearchGroupDiary" />
                  <span class="input-group-btn">
                     <button type="button"
                        class="btn-emps-search-parttime btn-primary btnSearchGroupDiary">
                        <i class="fa fa-search"></i>
                     </button>
                  </span>
               </div>
               <div class="img-loading-group-diary-list">
                  <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                     width="30" height="30" />
               </div>
               <div class="group-diary-search-list"></div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">รายการกลุ่มที่เลือก</div>
               <div class="group-diary-add-list">
                  <table class="table table-striped">
                     <thead>
                        <tr class="f-bold f-s-12">
                           <td class="text-center">ชื่อกลุ่ม</td>
                           <td>รายละเอียด</td>
                           <td class="text-danger text-right"><span class="group-diary-announce-added-count">0</span> รายการ</td>
                        </tr>
                     </thead>
                     <tbody class="listDataGroupDiary"></tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END Employee List Selection -->

      <!-- START Calendar Announce Group Diary -->
      <div class="col-md-12">
         <div id="external-events-dragging-area-group-diary" class="panel panel-info">
            <div class="panel-body">
               <div class="row col-md-3">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">ช่วงวันที่ต้องการประกาศกะ</div>
                     <div class="panel-body">
                        <div class='input-group date'>
                           <asp:TextBox ID="txtFromDateAnnounceGroupDiary" runat="server" placeholder="ตั้งแต่วันที่..."
                              CssClass="form-control from-date-announce-group-diary-datepicker libdatepicker" />
                           <span class="input-group-addon show-from-date-announce-group-diary-datepicker-onclick">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <br />
                        <div class='input-group date'>
                           <asp:TextBox ID="txtToDateAnnounceGroupDiary" runat="server" placeholder="ถึงวันที่..."
                              CssClass="form-control to-date-announce-group-diary-datepicker libdatepicker" />
                           <span class="input-group-addon show-to-date-announce-group-diary-datepicker-onclick">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class='panel panel-info external-events-diary'>
                     <div class="panel-heading f-bold">ค้นหากะการทำงาน</div>
                     <div class="input-group">
                        <asp:TextBox ID="txtSearchParttimeGroupDiary" runat="server" placeholder="รหัสกะ, ชื่อกะภาษาไทย"
                           CssClass="jstree-search-emps-form-control txtSearchParttimeGroupDiary" />
                        <span class="input-group-btn">
                           <button type="button"
                              class="btn-emps-search-parttime btn-primary btnSearchParttimeGroupDiary">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div>
                     <div class='panel-body external-events-listing-group-diary'>
                        <div class="img-loading-parttime-search-group-diary text-center m-t-10">
                           <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                              width="30" height="30" />
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-9">
                  <asp:Label ID="lblCalendarAnnounceGroupDiary" runat="server" CssClass="lblCalendarAnnounceGroupDiary" />
                  <button type="button" id="btnAnnounceGroupDiary" class="btn btn-success m-t-10 btnAnnounceGroupDiary pull-right">
                     บันทึกการประกาศกะ
                  </button>
                  <div class="img-loading-announce-group-diary pull-right m-t-10">
                     <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                        width="30" height="30" />
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Calendar Announce -->
   </div>
   <!--*** END MENU liToAnnounceDiaryEmpShift ZONE ***-->

   <!--*** START MENU liToAnnounceEmpShift ZONE ***-->
   <div id="divAnnounceMonthlyToDo" runat="server" visible="false">
      <div class="col-md-12">
         <div class="alert alert-info f-s-14 f-bold" role="alert">รายเดือน</div>
      </div>
      <!-- START Employee List Selection -->
      <div class="row">
         <div class="col-md-6">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">เลือกพนักงาน</div>
               <div class="input-group">
                  <asp:TextBox ID="txtSearchEmployee" runat="server" placeholder="รหัสพนักงาน, ชื่อพนักงาน"
                     CssClass="jstree-search-emps-form-control txtSearchEmployee" />
                  <span class="input-group-btn">
                     <button type="button"
                        class="btn-emps-search-parttime btn-primary btnSearchEmployee">
                        <i class="fa fa-search"></i>
                     </button>
                  </span>
               </div>
               <div class="img-loading-employee-list">
                  <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                     width="30" height="30" />
               </div>
               <div class="employee-search-list"></div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">รายการพนักงานที่เลือก</div>
               <div class="employee-add-list">
                  <table class="table table-striped">
                     <thead>
                        <tr class="f-bold f-s-12">
                           <td>รหัสพนักงาน</td>
                           <td>ชื่อ - สกุล</td>
                           <td class="text-danger text-right"><span class="emp-announce-added-count">0</span> รายการ</td>
                        </tr>
                     </thead>
                     <tbody class="listDataEmp"></tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- END Employee List Selection -->

      <!-- START Calendar Announce -->
      <div class="col-md-12">
         <div id="external-events-dragging-area" class="panel panel-info">
            <div class="panel-body">
               <div class="row col-md-3">
                  <div class="panel panel-info">
                     <div class="panel-heading f-bold">ช่วงวันที่ต้องการประกาศกะ</div>
                     <div class="panel-body">
                        <div class='input-group date'>
                           <asp:TextBox ID="fromDateAnnounce" runat="server" placeholder="ตั้งแต่วันที่..."
                              CssClass="form-control from-date-announce-datepicker libdatepicker" />
                           <span class="input-group-addon show-from-date-announce-datepicker-onclick">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                        <br />
                        <div class='input-group date'>
                           <asp:TextBox ID="toDateAnnounce" runat="server" placeholder="ถึงวันที่..."
                              CssClass="form-control to-date-announce-datepicker libdatepicker" />
                           <span class="input-group-addon show-to-date-announce-datepicker-onclick">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class='panel panel-info external-events'>
                     <div class="panel-heading f-bold">ค้นหากะการทำงาน</div>
                     <div class="input-group">
                        <asp:TextBox ID="txtSearchParttime" runat="server" placeholder="รหัสกะ, ชื่อกะภาษาไทย"
                           CssClass="jstree-search-emps-form-control txtSearchParttime" />
                        <span class="input-group-btn">
                           <button type="button"
                              class="btn-emps-search-parttime btn-primary btnSearchParttime">
                              <i class="fa fa-search"></i>
                           </button>
                        </span>
                     </div>
                     <div class='panel-body external-events-listing'>
                        <div class="img-loading-parttime-search text-center m-t-10">
                           <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                              width="30" height="30" />
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-9 m-b-10">
                  <asp:Label ID="lblCalendarAnnounce" runat="server" CssClass="lblCalendarAnnounce" />
                  <button type="button" id="btnAnnounce" class="btn btn-success m-t-10 f-s-14 btnAnnounce pull-right">
                     บันทึกการประกาศกะ
                  </button>
                  <div class="img-loading-announce pull-right m-t-10">
                     <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                        width="30" height="30" />
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Calendar Announce -->
   </div>
   <!--*** END MENU liToAnnounceEmpShift ZONE ***-->

   <!--*** START MENU liToChangeAnnounceEmpShift ZONE ***-->
   <div id="divChangeAnnounce" runat="server" visible="false">
      <!-- START Search Area Change Announce -->
      <div id="_divChangeAnnounceSearch" runat="server" class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ค้นหาพนักงาน</div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทพนักงาน</label>
                        <asp:DropDownList ID="ddlChangeAnnounceTypesEmployee" runat="server" CssClass="form-control" AutoPostBack="true"
                           OnSelectedIndexChanged="onSelectedIndexChanged">
                           <asp:ListItem Value="1">รายวัน</asp:ListItem>
                           <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-3" id="divDDLChangeAnnounceOrg" runat="server" visible="false">
                     <div class="form-group">
                        <label class="f-s-13">องค์กร</label>
                        <asp:DropDownList ID="ddlChangeAnnounceOrgSearch" runat="server" CssClass="form-control" AutoPostBack="true"
                           OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-3" id="divDDLChangeAnnounceDept" runat="server" visible="false">
                     <div class="form-group">
                        <label class="f-s-13">ฝ่าย</label>
                        <asp:DropDownList ID="ddlChangeAnnounceDeptSearch" runat="server" CssClass="form-control" AutoPostBack="true"
                           OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-3" runat="server" visible="false" id="divChangeAnnounceSecSearch">
                     <div class="form-group">
                        <label class="f-s-13">แผนก</label>
                        <asp:DropDownList ID="ddlChangeAnnounceSecSearch" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-3" runat="server" id="divChangeAnnounceGroupDiarySearch">
                     <div class="form-group">
                        <label class="f-s-13">กลุ่ม</label>
                        <asp:DropDownList ID="ddlChangeAnnounceGroupDiarySearch" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="f-s-13" runat="server" id="lblBlank">&nbsp;</label>
                        <label class="f-s-13" runat="server" visible="false" id="lblKeyword">คำค้น</label>
                        <div class="form-inline">
                           <asp:TextBox ID="txtChangeAnnounceKeywordSearch" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน, ชื่อ-นามสกุล..."
                              Visible="false" />
                           <asp:Button ID="btnChangeAnnounceSearch" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="btnChangeAnnounceSearch" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Search Area Change Announce -->

      <!-- START List Announce Diary Employee Shift -->
      <div id="_divChangeAnnounceDiaryList" runat="server" class="col-md-12">
         <asp:GridView ID="gvEmpshiftChangeAnnounceDiaryList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceDiaryStartDate" runat="server" Text='<%# Eval("announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceDiaryEndDate" runat="server" Text='<%# Eval("announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceDiary" runat="server" OnCommand="btnCommand"
                        CommandName="_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceDiary" CommandArgument='<%# Eval("u0_announce_diary_idx") %>'
                        CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="เปลี่ยนกะการทำงาน">
                        <i class="fa fa-pencil-square-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END List Announce Employee Shift -->

      <!-- START Change Announce Diary Employee Shift -->
      <div id="_divChangeAnnounceDiaryToDo" runat="server" visible="false">
         <input type="hidden" id="txtChangeAnnounceDiaryIdx" runat="server" class="txtChangeAnnounceDiaryIdx" />
         <div class="col-md-12">
            <asp:LinkButton ID="btnBackToChangeAnnounceDiaryList" runat="server" CssClass="btn btn-danger"
               OnCommand="btnCommand" CommandName="btnBackToChangeAnnounceDiaryList">
               <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
            </asp:LinkButton>
         </div>
         <br />
         <!-- START Calendar Old Employee Shift Diary -->
         <div class="col-md-12">
            <div class="panel panel-info m-t-10">
               <div class="panel-heading f-bold">กะการทำงานเดิม</div>
               <div class="panel-body">
                  <div class="col-md-5">
                     <div class="row">
                        <div class="panel panel-default">
                           <div class="panel-heading f-bold">ช่วงวันที่ประกาศกะการทำงาน</div>
                           <div class="panel-body">
                              <div class="col-md-5 text-right m-b-5">
                                 ตั้งแต่วันที่ :
                              </div>
                              <div class="col-md-7 m-b-5">
                                 <asp:Label ID="lblOldFromDateDiary" runat="server" CssClass="lblOldFromDateDiary" />
                              </div>
                              <div class="col-md-5 text-right">
                                 ถึงวันที่ :
                              </div>
                              <div class="col-md-7">
                                 <asp:Label ID="lblOldToDateDiary" runat="server" CssClass="lblOldToDateDiary" />
                              </div>
                           </div>
                        </div>
                        <div class="panel panel-default">
                           <div class="panel-heading f-bold">รายชื่อพนักงาน</div>
                           <div class="emp-list-diary-change-old">
                              <asp:Repeater ID="rpOldEmployeeDiaryList" runat="server">
                                 <HeaderTemplate>
                                    <table class="table table-striped f-s-12">
                                       <thead>
                                          <tr>
                                             <th>#</th>
                                             <th>รหัสพนักงาน</th>
                                             <th>ชื่อ - สกุล</th>
                                             <th></th>
                                          </tr>
                                       </thead>
                                       <tbody class="tbody-emp-list">
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                    <tr>
                                       <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                       <td data-th="รหัสพนักงาน"><%# Eval("EmpCode") %></td>
                                       <td data-th="ชื่อ - สกุล"><%# Eval("FullNameTH") %></td>
                                       <td data-th=""><%# getChangeAnnounceDiary((int)Eval("announce_diary_status")) %></td>
                                    </tr>
                                 </ItemTemplate>
                                 <FooterTemplate>
                                    </tbody>
                                    </table>
                                 </FooterTemplate>
                              </asp:Repeater>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-7">
                     <asp:Label ID="lblCalendarOldEmpShiftDiary" runat="server" CssClass="lblCalendarOldEmpShiftDiary" />
                  </div>
               </div>
            </div>
         </div>
         <!-- END Calendar Old Employee Shift Diary -->

         <!-- START Calendar Change Employee Shift Diary -->
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">เปลี่ยนกะการทำงาน</div>
               <div class="panel-body">
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายชื่อพนักงานในกะนี้</div>
                        <div class="emp-old-announce-diary-list"></div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">รายชื่อพนักงานที่ร้องขอเปลี่ยนกะ</div>
                        <div class="emp-new-announce-diary-list">
                           <table class="table table-striped">
                           <thead>
                              <tr class="f-bold f-s-12">
                                 <td>รหัสพนักงาน</td>
                                 <td>ชื่อ - สกุล</td>
                                 <td class="text-danger text-right"><span class="emp-announce-diary-added-count">0</span> รายการ</td>
                              </tr>
                           </thead>
                           <tbody class="listDataEmpDiary"></tbody>
                        </table>
                        </div>
                     </div>
                  </div>
                  <div id="external-events-dragging-area-change-diary">
                     <div class="col-md-3">
                        <div class="panel panel-info">
                           <div class="panel-heading f-bold">ช่วงวันที่ต้องการประกาศกะ</div>
                           <div class="panel-body">
                              <div class='input-group date'>
                                 <asp:TextBox ID="txtFromDateAnnounceDiaryChange" runat="server" placeholder="ตั้งแต่วันที่..."
                                    CssClass="form-control from-date-announce-datepicker-change-diary libdatepicker" />
                                 <span class="input-group-addon show-from-date-announce-datepicker-change-diary-onclick">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                              </div>
                              <br />
                              <div class='input-group date'>
                                 <asp:TextBox ID="txtToDateAnnounceDiaryChange" runat="server" placeholder="ถึงวันที่..."
                                    CssClass="form-control to-date-announce-datepicker-change-diary libdatepicker" />
                                 <span class="input-group-addon show-to-date-announce-datepicker-change-diary-onclick">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <div class='panel panel-info external-events-change-diary'>
                           <div class="panel-heading f-bold">ค้นหากะการทำงาน</div>
                           <div class="input-group">
                              <asp:TextBox ID="txtSearchParttimeChangeDiary" runat="server" placeholder="รหัสกะ, ชื่อกะภาษาไทย"
                                 CssClass="jstree-search-emps-form-control txtSearchParttimeChangeDiary" />
                              <span class="input-group-btn">
                                 <button type="button"
                                    class="btn-emps-search-parttime btn-primary btnSearchParttimeChangeDiary">
                                    <i class="fa fa-search"></i>
                                 </button>
                              </span>
                           </div>
                           <div class='panel-body external-events-listing-change-diary'>
                              <div class="img-loading-change-announce-diary-search-parttime text-center m-t-10">
                                 <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                                    width="30" height="30" />
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-9">
                        <asp:Label ID="lblCalendarNewEmpShiftDiary" runat="server" CssClass="lblCalendarNewEmpShiftDiary" />
                        <button type="button" id="btnChangeAnnounceDiary" class="btn btn-success m-t-10 btnChangeAnnounceDiary pull-right">
                           บันทึกการเปลี่ยนกะการทำงาน
                        </button>
                        <div class="img-loading-change-announce-diary pull-right m-t-10">
                           <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                              width="30" height="30" />
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END Calendar Change Employee Shift Diary -->
      </div>
      <!-- END Change Announce Diary Employee Shift -->

      <!-- START List Announce Employee Shift -->
      <div id="_divChangeAnnounceMonthlyList" runat="server" class="col-md-12">
         <asp:GridView ID="gvEmpshiftChangeAnnounceList"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            AllowPaging="true"
            PageSize="10"
            OnPageIndexChanging="OnPageIndexChanging">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex +1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceCreatedAt" runat="server"
                        Text='<%# Eval("announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                        <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                        <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                        <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceStartDate" runat="server" Text='<%# Eval("announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceEndDate" runat="server" Text='<%# Eval("announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceMonthly" runat="server" OnCommand="btnCommand"
                        CommandName="_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceMonthly" CommandArgument='<%# Eval("u0_announce_idx") %>'
                        CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="เปลี่ยนกะการทำงาน">
                                <i class="fa fa-pencil-square-o"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END List Announce Employee Shift -->

      <!-- START Change Announce Employee Shift -->
      <div id="_divChangeAnnounceMonthlyToDo" runat="server" class="col-md-12" visible="false">
         <input type="hidden" id="txtChangeAnnounceIdx" runat="server" class="txtChangeAnnounceIdx" />
         <input type="hidden" id="txtChangeAnnounceEmpIdxLogin" runat="server" class="txtChangeAnnounceEmpIdxLogin" />
         <asp:LinkButton ID="btnBackToChangeAnnounceList" runat="server" CssClass="btn btn-danger"
            OnCommand="btnCommand" CommandName="btnBackToChangeAnnounceList">
                <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
         </asp:LinkButton>
         <br />
         <br />
         <!-- START Employee Data -->
         <div class="row">
            <div class="panel panel-info">
               <div class="panel-heading f-bold">ข้อมูลพนักงาน</div>
               <div class="panel-body emps-p-all-10">
                  <div class="form-group">
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Employee Code</span><br>
                           <small><b>รหัสพนักงาน</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblChangeAnnounceEmpCode" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Job Grade</span><br>
                           <small><b>Job Grade</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblChangeAnnounceJobLevel" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Full Name</span><br>
                           <small><b>ชื่อ - นามสกุล</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblChangeAnnounceFullNameTH" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Position</span><br>
                           <small><b>ตำแหน่ง</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblChangeAnnouncePosNameTH" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Department</span><br>
                           <small><b>ฝ่าย</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblChangeAnnounceDeptNameTH" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Section</span><br>
                           <small><b>แผนก</b></small>
                        </h2>
                     </label>
                     <div class="col-md-4">
                        <asp:Label ID="lblChangeAnnounceSecNameTH" runat="server" CssClass="control-label" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="form-group">
                     <label class="col-md-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>First Approver</span><br>
                           <small><b>ผู้อนุมัติที่ 1</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblChangeAnnounceFirstApprover" runat="server" CssClass="control-label" />
                     </div>
                     <label class="col-sm-2 control-labelnotop">
                        <h2 class="panel-title">
                           <span>Second Approver</span><br>
                           <small><b>ผู้อนุมัติที่ 2</b></small>
                        </h2>
                     </label>
                     <div class="col-sm-4">
                        <asp:Label ID="lblChangeAnnounceSecondApprover" runat="server" CssClass="control-label" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END Employee Data -->

         <!-- START Calendar Old Employee Shift -->
         <div class="panel panel-info m-t-10">
            <div class="panel-heading f-bold">กะการทำงานเดิม</div>
            <div class="panel-body">
               <div class="col-md-10 col-md-offset-1">
                  <asp:Label ID="lblCalendarOldEmpShift" runat="server" CssClass="lblCalendarOldEmpShift" />
               </div>
            </div>
         </div>
         <!-- END Calendar Old Employee Shift -->

         <!-- START Calendar Change Employee Shift -->
         <div class="panel panel-info m-t-10">
            <div class="panel-heading f-bold">เปลี่ยนกะการทำงาน</div>
            <div class="panel-body">
               <div id="external-events-dragging-area-change">
                  <div class="row col-md-3">
                     <div class="panel panel-info">
                        <div class="panel-heading f-bold">ช่วงวันที่ต้องการประกาศกะ</div>
                        <div class="panel-body">
                           <div class='input-group date'>
                              <asp:TextBox ID="txtFromDateAnnounceChange" runat="server" placeholder="ตั้งแต่วันที่..."
                                 CssClass="form-control from-date-announce-datepicker-change libdatepicker" />
                              <span class="input-group-addon show-from-date-announce-datepicker-change-onclick">
                                 <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                           </div>
                           <br />
                           <div class='input-group date'>
                              <asp:TextBox ID="txtToDateAnnounceChange" runat="server" placeholder="ตั้งแต่วันที่..."
                                 CssClass="form-control to-date-announce-datepicker-change libdatepicker" />
                              <span class="input-group-addon show-to-date-announce-datepicker-change-onclick">
                                 <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                           </div>
                        </div>
                     </div>
                     <div class='panel panel-info external-events-change'>
                        <div class="panel-heading f-bold">ค้นหากะการทำงาน</div>
                        <div class="input-group">
                           <asp:TextBox ID="txtSearchParttimeChange" runat="server" placeholder="รหัสกะ, ชื่อกะภาษาไทย"
                              CssClass="jstree-search-emps-form-control txtSearchParttimeChange" />
                           <span class="input-group-btn">
                              <button type="button"
                                 class="btn-emps-search-parttime btn-primary btnSearchParttimeChange">
                                 <i class="fa fa-search"></i>
                              </button>
                           </span>
                        </div>
                        <div class='panel-body external-events-listing-change'>
                           <div class="img-loading-change-announce-search-parttime text-center m-t-10">
                              <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                                 width="30" height="30" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-9">
                     <asp:Label ID="lblCalendarNewEmpShift" runat="server" CssClass="lblCalendarNewEmpShift" />
                     <button type="button" id="btnChangeAnnounce" class="btn btn-success m-t-10 btnChangeAnnounce pull-right">
                        บันทึกการเปลี่ยนกะการทำงาน
                     </button>
                     <div class="img-loading-change-announce pull-right m-t-10">
                        <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                           width="30" height="30" />
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
         <!-- END Calendar Change Employee Shift -->
      </div>
      <!-- END Change Announce Employee Shift -->
   </div>
   <!--*** END MENU liToChangeAnnounceEmpShift ZONE ***-->

   <!--*** START MENU liToEditWaiting ZONE ***-->
   <div id="divEditWaiting" runat="server" visible="false">
      <!-- START Search Area Edit Waiting -->
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ค้นหารายการที่รอแก้ไข</div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">องค์กร</label>
                        <asp:DropDownList ID="ddlEditWaitingOrgSearch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">ฝ่าย</label>
                        <asp:DropDownList ID="ddlEditWaitingDeptSearch" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทพนักงาน</label>
                        <asp:DropDownList ID="ddlEditWaitingTypesEmployee" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1">รายวัน</asp:ListItem>
                           <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทเอกสาร</label>
                        <div class="form-inline">
                           <asp:DropDownList ID="ddlEditWaitingTypesDoc" runat="server" CssClass="form-control">
                              <asp:ListItem Value="1">ตารางกะการทำงาน</asp:ListItem>
                              <asp:ListItem Value="2">ขอเปลี่ยนกะการทำงาน</asp:ListItem>
                           </asp:DropDownList>
                           <asp:Button ID="btnEditWaitingSearch" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="btnEditWaitingSearch" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Search Area Edit Waiting -->

      <!-- START List Edit Waiting Employee Shift Diary -->
      <div id="divEditWaitingAnnounceDiary" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvEditWaitingAnnounceDiary"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            PageSize="10">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryIdx" runat="server" Text='<%# Eval("u0_announce_diary_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="u0NodeIdx" runat="server" Text='<%# Eval("u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceDiaryStartDate" runat="server" Text='<%# Eval("announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceDiaryEndDate" runat="server" Text='<%# Eval("announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:LinkButton ID="btnToEditWaiting" runat="server" OnCommand="btnCommand" CommandName="btnToEditWaiting" CommandArgument='<%# Eval("u0_announce_idx") %>'
                        CssClass="btn btn-danger">
                        <i class="fa fa-pencil fa-lg"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
      <!-- END List Edit Waiting Employee Shift Diary -->

      <!-- START List Edit Waiting Employee Shift -->
      <div id="divEditWaitingAnnounce" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvEditWaitingAnnounce"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            PageSize="10">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="announceIdx" runat="server" Text='<%# Eval("u0_announce_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="u0NodeIdx" runat="server" Text='<%# Eval("u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex +1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceCreatedAt" runat="server"
                        Text='<%# Eval("announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                        <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                        <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                        <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceStartDate" runat="server" Text='<%# Eval("announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceEndDate" runat="server" Text='<%# Eval("announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewDesc-<%# Eval("u0_announce_idx") %>' onclick="previewAnnounce(<%# Eval("u0_announce_idx") %>)" class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideDesc-<%# Eval("u0_announce_idx") %>' onclick="previewAnnounce(<%# Eval("u0_announce_idx") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingListCalendar-<%# Eval("u0_announce_idx") %>' style="padding: 0; margin: 0;">
                           <div id='desc-<%# Eval("u0_announce_idx") %>' style="display: none;" class="col-md-8 col-md-offset-2">
                              <span class='lblCalendarPendingAnnounce-<%# Eval("u0_announce_idx") %>'></span>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="btnEditWaitingApproveByHeader" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnEditWaitingApproveByHeader" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="btnEditWaitingApproveByDirector" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnEditWaitingApproveByDirector" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Edit Waiting Employee Shift -->

      <!-- START List Edit Waiting Change Employee Shift -->
      <div id="divEditWaitingChangeAnnounce" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvEditWaitingChangeAnnounce"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            PageSize="10">
            <PagerStyle CssClass="PageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceIdx" runat="server" Text='<%# Eval("u0_change_announce_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="changeU0NodeIdx" runat="server" Text='<%# Eval("change_u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex +1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceCreatedAt" runat="server"
                        Text='<%# Eval("change_announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                        <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                        <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                        <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="changeAnnounceStartDate" runat="server" Text='<%# Eval("change_announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="changeAnnounceEndDate" runat="server" Text='<%# Eval("change_announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewChangeDesc-<%# Eval("u0_announce_idx_ref") %>' onclick="previewChangeAnnounce(<%# Eval("u0_announce_idx_ref") %>)" class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideChangeDesc-<%# Eval("u0_announce_idx_ref") %>' onclick="previewChangeAnnounce(<%# Eval("u0_announce_idx_ref") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingChangeStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingListChangeCalendar-<%# Eval("u0_announce_idx_ref") %>' style="padding: 0; margin: 0;">
                           <div id='descChange-<%# Eval("u0_announce_idx_ref") %>' style="display: none;">
                              <div class="row">
                                 <div class="col-md-6">

                                    <div class="panel panel-info">
                                       <div class="panel-heading f-bold">กะการทำงานเดิม</div>
                                       <div class="panel-body">
                                          <span class='lblCalendarPendingChangeAnnounceOld-<%# Eval("u0_announce_idx_ref") %>'></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="panel panel-info">
                                       <div class="panel-heading f-bold">กะการทำงานใหม่</div>
                                       <div class="panel-body">
                                          <span class='lblCalendarPendingChangeAnnounceNew-<%# Eval("u0_announce_idx_ref") %>'></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="Button6" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeApproveByHeader" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="Button7" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeApproveByDirector" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Edit Waiting Change Employee Shift -->
   </div>
   <!--*** END MENU liToEditWaiting ZONE ***-->

   <!--*** START MENU liToPending ZONE ***-->
   <div id="divPending" runat="server" visible="false">
      <!-- START Search Area Pending -->
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ค้นหารายการที่รออนุมัติ</div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">องค์กร</label>
                        <asp:DropDownList ID="ddlPendingOrgSearch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <label class="f-s-13">ฝ่าย</label>
                        <asp:DropDownList ID="ddlPendingDeptSearch" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทพนักงาน</label>
                        <asp:DropDownList ID="ddlPendingTypesEmployee" runat="server" CssClass="form-control"></asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทเอกสาร</label>
                        <div class="form-inline">
                           <asp:DropDownList ID="ddlPendingTypeDoc" runat="server" CssClass="form-control">
                              <asp:ListItem Value="1">ตารางกะการทำงาน</asp:ListItem>
                              <asp:ListItem Value="2">ขอเปลี่ยนกะการทำงาน</asp:ListItem>
                           </asp:DropDownList>
                           <asp:Button ID="_divMenuBtnToDivPendingBtnPendingSearch" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="_divMenuBtnToDivPendingBtnPendingSearch" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Search Area Pending -->

      <!-- START List Pending Employee Shift Diary -->
      <div id="_divPendingAnnounceDiaryList" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvPendingAnnounceDiary"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            OnRowDataBound="gvOnRowDataBound"
            PageSize="10">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryIdx" runat="server" Text='<%# Eval("u0_announce_diary_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="u0NodeIdx" runat="server" Text='<%# Eval("u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceDiaryStartDate" runat="server" Text='<%# Eval("announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceDiaryEndDate" runat="server" Text='<%# Eval("announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceDiaryNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewDiaryDesc-<%# Eval("u0_announce_diary_idx") %>' onclick="previewAnnounceDiary(<%# Eval("u0_announce_diary_idx") %>)" class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideDiaryDesc-<%# Eval("u0_announce_diary_idx") %>' onclick="previewAnnounceDiary(<%# Eval("u0_announce_diary_idx") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingDiaryStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingDiaryListCalendar-<%# Eval("u0_announce_diary_idx") %>' style="padding: 0; margin: 0;">
                           <div class='col-md-4 table-employee-diary-list-<%# Eval("u0_announce_diary_idx") %>' style="display: none;">
                              <div class="panel panel-info">
                                 <div class="panel-heading f-bold">รายชื่อพนักงาน</div>
                                 <div class="emp-list-diary-pending">
                                    <table class="table table-striped f-s-12">
                                       <asp:Repeater ID="rpEmployeeDiary" runat="server">
                                          <HeaderTemplate>
                                             <tr>
                                                <th>#</th>
                                                <th>รหัสพนักงาน</th>
                                                <th>ชื่อ - สกุล</th>
                                             </tr>
                                          </HeaderTemplate>
                                          <ItemTemplate>
                                             <tr>
                                                <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                                <td data-th="รหัสพนักงาน"><%# Eval("EmpCode") %></td>
                                                <td data-th="ชื่อ - สกุล"><%# Eval("FullNameTH") %></td>
                                             </tr>
                                          </ItemTemplate>
                                       </asp:Repeater>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <div id='descDiary-<%# Eval("u0_announce_diary_idx") %>' style="display: none;" class="col-md-8">
                              <span class='lblCalendarPendingAnnounceDiary-<%# Eval("u0_announce_diary_idx") %>'></span>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="btnPendingDiaryApproveByHeader" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingDiaryApproveByHeader" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="btnPendingDiaryApproveByDirector" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingDiaryApproveByDirector" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Pending Employee Shift Diary -->

      <!-- START List Pending Change Employee Shift Diary -->
      <div id="_divPendingChangeAnnounceDiaryList" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvPendingChangeAnnounceDiary"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            OnRowDataBound="gvOnRowDataBound"
            PageSize="10">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceDiaryIdx" runat="server" Text='<%# Eval("u0_change_announce_diary_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="u0NodeIdx" runat="server" Text='<%# Eval("u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceDiaryCreatedAt" runat="server"
                        Text='<%# Eval("change_announce_diary_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="Description" runat="server">
                        <p><b>กลุ่ม:</b> <%# Eval("group_diary_name") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <b>แผนก:</b> <%# Eval("SecNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="changeAnnounceDiaryStartDate" runat="server" Text='<%# Eval("change_announce_diary_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="changeAnnounceDiaryEndDate" runat="server" Text='<%# Eval("change_announce_diary_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceDiaryNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewChangeDiaryDesc-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>' onclick="previewChangeAnnounceDiary(<%# Eval("u0_announce_diary_idx_ref") %>,<%# Eval("u0_change_announce_diary_idx") %>)" 
                        class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideChangeDiaryDesc-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>' 
                        onclick="previewChangeAnnounceDiary(<%# Eval("u0_announce_diary_idx_ref") %>,<%# Eval("u0_change_announce_diary_idx") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingChangeDiaryStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingChangeDiaryListCalendar-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>' style="padding: 0; margin: 0;">
                           <div class="row">
                              <div class='col-md-6 col-md-offset-3 table-employee-change-diary-list-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>' style="display: none;">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">รายชื่อพนักงาน</div>
                                    <div class="emp-list-change-diary-pending">
                                       <table class="table table-striped f-s-12">
                                          <asp:Repeater ID="rpEmployeeChangeDiary" runat="server">
                                             <HeaderTemplate>
                                                <tr>
                                                   <th>#</th>
                                                   <th>รหัสพนักงาน</th>
                                                   <th>ชื่อ - สกุล</th>
                                                </tr>
                                             </HeaderTemplate>
                                             <ItemTemplate>
                                                <tr>
                                                   <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                                   <td data-th="รหัสพนักงาน"><%# Eval("EmpCode") %></td>
                                                   <td data-th="ชื่อ - สกุล"><%# Eval("FullNameTH") %></td>
                                                </tr>
                                             </ItemTemplate>
                                          </asp:Repeater>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div id='descChangeDiary-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>' style="display: none;">
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">กะการทำงานเดิม</div>
                                    <div class="panel-body">
                                       <span class='lblCalendarPendingChangeAnnounceDiaryOld-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>'></span>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="panel panel-info">
                                    <div class="panel-heading f-bold">กะการทำงานใหม่</div>
                                    <div class="panel-body">
                                       <span class='lblCalendarPendingChangeAnnounceDiaryNew-<%# Eval("u0_announce_diary_idx_ref") %>-<%# Eval("u0_change_announce_diary_idx") %>'></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="btnPendingChangeDiaryApproveByHeader" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeDiaryApproveByHeader" CssClass="btn btn-success pull-right m-b-10" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="btnPendingChangeDiaryApproveByDirector" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeDiaryApproveByDirector" CssClass="btn btn-success pull-right m-b-10" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Pending Change Employee Shift Diary -->

      <!-- START List Pending Employee Shift -->
      <div id="_divPendingAnnounceMonthlyList" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvPendingAnnounce"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            PageSize="10">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="announceIdx" runat="server" Text='<%# Eval("u0_announce_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="u0NodeIdx" runat="server" Text='<%# Eval("u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex +1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceCreatedAt" runat="server"
                        Text='<%# Eval("announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                              <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                              <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                              <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                              <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                              <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                              <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="announceStartDate" runat="server" Text='<%# Eval("announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="announceEndDate" runat="server" Text='<%# Eval("announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="announceNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewDesc-<%# Eval("u0_announce_idx") %>' onclick="previewAnnounce(<%# Eval("u0_announce_idx") %>)" class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideDesc-<%# Eval("u0_announce_idx") %>' onclick="previewAnnounce(<%# Eval("u0_announce_idx") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingListCalendar-<%# Eval("u0_announce_idx") %>' style="padding: 0; margin: 0;">
                           <div id='desc-<%# Eval("u0_announce_idx") %>' style="display: none;" class="col-md-8 col-md-offset-2">
                              <span class='lblCalendarPendingAnnounce-<%# Eval("u0_announce_idx") %>'></span>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="btnPendingApproveByHeader" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingApproveByHeader" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="btnPendingApproveByDirector" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingApproveByDirector" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Pending Employee Shift -->

      <!-- START List Pending Change Employee Shift -->
      <div id="_divPendingChangeAnnounceMonthlyList" class="col-md-12" runat="server" visible="false">
         <asp:GridView ID="gvPendingChangeAnnounce"
            runat="server"
            AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-responsive col-md-12"
            HeaderStyle-CssClass="info"
            HeaderStyle-ForeColor="#31708f"
            OnRowCreated="gvOnRowCreated"
            AllowPaging="true"
            PageSize="10">
            <PagerStyle CssClass="PageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceIdx" runat="server" Text='<%# Eval("u0_change_announce_idx") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" Visible="false">
                  <ItemTemplate>
                     <asp:Label ID="changeU0NodeIdx" runat="server" Text='<%# Eval("change_u0_node_idx_ref") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" Visible="false">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex +1) %>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceCreatedAt" runat="server"
                        Text='<%# Eval("change_announce_created_at_convert") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="EmpDescription" runat="server">
                        <p><b>รหัสพนักงาน:</b> <%# Eval("EmpCode") %></p>
                        <p><b>ชื่อ - สกุล:</b> <%# Eval("FullNameTH") %></p>
                        <p><b>องค์กร:</b> <%# Eval("OrgNameTH") %></p>
                        <p><b>ฝ่าย:</b> <%# Eval("DeptNameTH") %></p>
                        <p><b>แผนก:</b> <%# Eval("SecNameTH") %></p>
                        <b>ตำแหน่ง:</b> <%# Eval("PosNameTH") %>
                     </asp:Label>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ช่วงวันที่ประกาศกะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <p>
                        <asp:Label ID="changeAnnounceStartDate" runat="server" Text='<%# Eval("change_announce_start") %>' />
                     </p>
                     <p>ถึง</p>
                     <asp:Label ID="changeAnnounceEndDate" runat="server" Text='<%# Eval("change_announce_end") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <asp:Label ID="changeAnnounceNode" runat="server" Text='<%# Eval("node_decision_statnode") + " " + Eval("to_node_name") + " โดย " + Eval("to_actor_name") %>' />
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                  HeaderStyle-Font-Size="Small">
                  <ItemTemplate>
                     <button type="button" id='btnViewChangeDesc-<%# Eval("u0_announce_idx_ref") %>' onclick="previewChangeAnnounce(<%# Eval("u0_announce_idx_ref") %>)" class="btn btn-info btn-sm">
                        <i class="fa fa-file-o"></i>
                     </button>
                     <button type="button" style="display: none;" id='btnHideChangeDesc-<%# Eval("u0_announce_idx_ref") %>' onclick="previewChangeAnnounce(<%# Eval("u0_announce_idx_ref") %>)" class="btn btn-danger btn-sm">
                        <i class="fa fa-times"></i>
                     </button>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle">
                  <ItemTemplate>
                     <asp:RadioButtonList ID="pendingChangeStatus" runat="server"
                        OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="True"
                        CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                        <asp:ListItem>อนุมัติ</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                        <asp:ListItem>ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                     </asp:RadioButtonList>
                     <tr>
                        <td colspan="6" class='previewPendingListChangeCalendar-<%# Eval("u0_announce_idx_ref") %>' style="padding: 0; margin: 0;">
                           <div id='descChange-<%# Eval("u0_announce_idx_ref") %>' style="display: none;">
                              <div class="row">
                                 <div class="col-md-6">

                                    <div class="panel panel-info">
                                       <div class="panel-heading f-bold">กะการทำงานเดิม</div>
                                       <div class="panel-body">
                                          <span class='lblCalendarPendingChangeAnnounceOld-<%# Eval("u0_announce_idx_ref") %>'></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="panel panel-info">
                                       <div class="panel-heading f-bold">กะการทำงานใหม่</div>
                                       <div class="panel-body">
                                          <span class='lblCalendarPendingChangeAnnounceNew-<%# Eval("u0_announce_idx_ref") %>'></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </td>
                     </tr>
                  </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
         <asp:Button ID="btnPendingChangeApproveByHeader" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeApproveByHeader" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
         <asp:Button ID="btnPendingChangeApproveByDirector" runat="server" Text="บันทึก" OnCommand="btnCommand"
            CommandName="btnPendingChangeApproveByDirector" CssClass="btn btn-success pull-right" Visible="false"
            OnClientClick="return confirm('คุณต้องการบันทึกใช่หรือไม่ ?')" />
      </div>
      <!-- END List Pending Change Employee Shift -->
   </div>
   <!--*** END MENU liToPending ZONE ***-->

   <!--*** START MENU liToHRArea ZONE ***-->
   <div id="divHRArea" runat="server" visible="false">
      <!-- START Search Area HR Area -->
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading f-bold">ค้นหา</div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทพนักงาน</label>
                        <asp:RequiredFieldValidator ID="requiredDDLTypesEmpHRArea" runat="server"
                           ControlToValidate="ddlTypesEmpHRArea"
                           Display="Dynamic"
                           ForeColor="Red"
                           Font-Size="13px"
                           CssClass="pull-right"
                           ErrorMessage="กรุณาเลือกประเภทพนักงาน"
                           SetFocusOnError="true"
                           ValidationGroup="_divMenuBtnToDivHRAreaBtnSearchHRArea" />
                        <asp:DropDownList ID="ddlTypesEmpHRArea" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                           <asp:ListItem Value="">-- เลือกประเภทพนักงาน --</asp:ListItem>
                           <asp:ListItem Value="1">รายวัน</asp:ListItem>
                           <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">ประเภทเอกสาร</label>
                        <asp:RequiredFieldValidator ID="requiredDDLTypesDocHRArea" runat="server"
                           ControlToValidate="ddlTypesDocHRArea"
                           Display="Dynamic"
                           ForeColor="Red"
                           Font-Size="13px"
                           CssClass="pull-right"
                           ErrorMessage="กรุณาเลือกประเภทเอกสาร"
                           SetFocusOnError="true"
                           ValidationGroup="_divMenuBtnToDivHRAreaBtnSearchHRArea" />
                        <asp:DropDownList ID="ddlTypesDocHRArea" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                           <asp:ListItem Value="">-- เลือกประเภทเอกสาร --</asp:ListItem>
                           <asp:ListItem Value="1">ตารางกะการทำงาน</asp:ListItem>
                           <asp:ListItem Value="2">ขอเปลี่ยนกะการทำงาน</asp:ListItem>
                           <%--<asp:ListItem Value="3">ขอสลับวันหยุด</asp:ListItem>--%>
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group">
                        <label class="f-s-13">สถานะเอกสาร</label>
                        <asp:DropDownList ID="ddlStatusDocHRArea" runat="server" CssClass="form-control">
                           <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                           <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                           <asp:ListItem Value="2">ไม่อนุมัติ (แก้ไข)</asp:ListItem>
                           <asp:ListItem Value="3">ไม่อนุมัติ (ปิดเอกสาร)</asp:ListItem>
                           <asp:ListItem Value="4">รอดำเนินการ</asp:ListItem>
                        </asp:DropDownList>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-4" runat="server" visible="false" id="divDDLOrgHRArea">
                     <div class="form-group">
                        <label class="f-s-13">องค์กร</label>
                        <asp:DropDownList ID="ddlOrgHRArea" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-4" runat="server" visible="false" id="divDDLDeptHRArea">
                     <div class="form-group">
                        <label class="f-s-13">ฝ่าย</label>
                        <asp:DropDownList ID="ddlDeptHRArea" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" />
                     </div>
                  </div>
                  <div class="col-md-4" runat="server" visible="false" id="divDDLSecHRArea">
                     <div class="form-group">
                        <label class="f-s-13">แผนก</label>
                        <asp:DropDownList ID="ddlSecHRArea" runat="server" CssClass="form-control" AutoPostBack="true" />
                     </div>
                  </div>
                  <div class="col-md-4" runat="server" visible="false" id="divDDLGroupDiaryHRArea">
                     <div class="form-group">
                        <label class="f-s-13">กลุ่ม</label>
                        <asp:DropDownList ID="ddlGroupDiaryHRArea" runat="server" CssClass="form-control" />
                     </div>
                  </div>
                  <div class="col-md-4" runat="server" visible="false" id="divTxtKeywordHRArea">
                     <div class="form-group">
                        <label class="f-s-13">คำค้น</label>
                        <asp:TextBox ID="txtKeywordHRArea" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน, ชื่อ-สกุลพนักงาน..." />
                     </div>
                  </div>
                  <div runat="server" id="divSelectionColumnHRArea" visible="false">
                     <div class="col-md-12">
                        <div class="panel panel-default">
                           <div class="panel-heading f-s-12 f-bold">เลือก Columns</div>
                           <div class="panel-body">
                              <div class="col-md-3">
                                 <asp:CheckBox ID="cbAll" runat="server" Value="99" Text="&nbsp;เลือกทั้งหมด" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                              </div>
                              <div class="col-md-3">
                                 <asp:CheckBox ID="cbEmpCode" runat="server" Value="1" Text="&nbsp;รหัสพนักงาน" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbFullNameTH" runat="server" Value="2" Text="&nbsp;ชื่อ-สกุลพนักงาน" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbOrgNameTH" runat="server" Value="3" Text="&nbsp;องค์กร" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbDeptNameTH" runat="server" Value="4" Text="&nbsp;ฝ่าย" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                              </div>
                              <div class="col-md-3">
                                 <asp:CheckBox ID="cbSecNameTH" runat="server" Value="5" Text="&nbsp;แผนก" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbGroupDiary" runat="server" Value="6" Text="&nbsp;กลุ่ม" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbApprover" runat="server" Value="7" Text="&nbsp;ผู้อนุมัติ" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbStatusDoc" runat="server" Value="8" Text="&nbsp;สถานะเอกสาร" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                                 <asp:CheckBox ID="cbAttactFile" runat="server" Value="9" Text="&nbsp;เอกสารแนบ" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" />
                              </div>
                              <div class="col-md-3">
                                 <asp:CheckBox ID="cbRangeDate" runat="server" Value="10" Text="&nbsp;วันที่ทำงาน" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                                 <asp:CheckBox ID="cbParttime" runat="server" Value="11" Text="&nbsp;กะการทำงาน" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                                 <asp:CheckBox ID="cbOldRangeDate" runat="server" Value="12" Text="&nbsp;วันที่ทำงาน (กะเดิม)" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                                 <asp:CheckBox ID="cbOldParttime" runat="server" Value="13" Text="&nbsp;กะการทำงานเดิม" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                                 <asp:CheckBox ID="cbNewRangeDate" runat="server" Value="14" Text="&nbsp;วันที่ทำงาน (กะใหม่)" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                                 <asp:CheckBox ID="cbNewParttime" runat="server" Value="15" Text="&nbsp;กะการทำงานใหม่" CssClass="row f-s-12 cs-pointer"
                                    AutoPostBack="true" OnCheckedChanged="onCheckedChanged" Visible="false" />
                              </div>
                              <div class="clearfix"></div>
                              <asp:CustomValidator ID="customValidatorCheckbox" runat="server"
                                 OnServerValidate="checkboxColumnsSelected"
                                 ErrorMessage="กรุณาเลือก Columns ตั้งแต่ 1 รายการขึ้นไป"
                                 ValidationGroup="_divMenuBtnToDivHRAreaBtnSearchHRArea"
                                 Display="Dynamic"
                                 ForeColor="Red"
                                 Font-Size="13px" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12" runat="server" id="divBtnSearchHRArea" visible="false">
                     <asp:Button ID="_divMenuBtnToDivHRAreaBtnSearchHRArea" runat="server" OnCommand="btnCommand" CommandName="_divMenuBtnToDivHRAreaBtnSearchHRArea"
                        ValidationGroup="_divMenuBtnToDivHRAreaBtnSearchHRArea" Text="ค้นหา" CssClass="btn btn-primary" />
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- END Search Area HR Area -->

      <!-- START HR Area announce diary export excel -->
      <div id="_divHRAreaAnnounceDiaryExportExcel" runat="server" class="col-md-12" visible="false">
         <div class="pull-right m-b-10">
            <asp:Button ID="btnAnnounceDiaryExport" runat="server" CssClass="btn btn-info" Text="Export Excel" OnCommand="btnCommand" CommandName="btnAnnounceDiaryExport" />
         </div>
         <asp:UpdatePanel ID="updatePanelAnnounceDiaryExportHRArea" runat="server">
            <ContentTemplate>
               <asp:GridView ID="gvAnnounceDiaryExportHRArea"
                  runat="server"
                  AutoGenerateColumns="false"
                  CssClass="table table-striped table-bordered table-responsive"
                  HeaderStyle-CssClass="info"
                  HeaderStyle-ForeColor="#31708f"
                  AllowPaging="false"
                  OnRowCreated="gvOnRowCreated"
                  OnPageIndexChanging="OnPageIndexChanging"
                  PageSize="10">
                  <PagerStyle CssClass="pageCustom" />
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                  <EmptyDataTemplate>
                     <div class="text-center">ไม่พบข้อมูล</div>
                  </EmptyDataTemplate>
                  <Columns>
                     <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportEmpCode" runat="server" Text='<%# Eval("EmpCode") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ชื่อ-สกุลพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportFullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="กลุ่ม / แผนก" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportGroupDiaryName" runat="server" Text='<%# Eval("group_diary_name") + " / " + Eval("SecNameTH") %> ' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ผู้อนุมัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportApprover" runat="server" Text='<%# Eval("approver_name") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="วันที่ทำงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportRangeDate" runat="server" Text='<%# Eval("empshift_date_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="กะการทำงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportParttime" runat="server" Text='<%# Eval("parttime_code") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDocStatus" runat="server" Text='<%# Eval("announce_status_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="เอกสารแนบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDeptNameTH" runat="server" Text='' />
                        </ItemTemplate>
                     </asp:TemplateField>
                  </Columns>
               </asp:GridView>
            </ContentTemplate>
            <Triggers>
               <asp:PostBackTrigger ControlID="btnAnnounceDiaryExport" />
            </Triggers>
         </asp:UpdatePanel>
      </div>
      <!-- END HR Area announce diary export excel -->

      <!-- START HR Area announce monthly export excel -->
      <div id="_divHRAreaAnnounceMonthlyExportExcel" class="col-md-12" runat="server" visible="false">
         <div class="pull-right m-b-10">
            <asp:Button ID="btnAnnounceMonthlyExport" runat="server" CssClass="btn btn-info" Text="Export Excel" OnCommand="btnCommand" CommandName="btnAnnounceMonthlyExport" />
         </div>
         <asp:UpdatePanel ID="updatePanelAnnounceExportHRArea" runat="server">
            <ContentTemplate>
               <asp:GridView ID="gvAnnounceExportHRArea"
                  runat="server"
                  AutoGenerateColumns="false"
                  CssClass="table table-striped table-bordered table-responsive"
                  HeaderStyle-CssClass="info"
                  HeaderStyle-ForeColor="#31708f"
                  AllowPaging="false"
                  OnRowCreated="gvOnRowCreated"
                  OnPageIndexChanging="OnPageIndexChanging"
                  PageSize="10">
                  <PagerStyle CssClass="pageCustom" />
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                  <EmptyDataTemplate>
                     <div class="text-center">ไม่พบข้อมูล</div>
                  </EmptyDataTemplate>
                  <Columns>
                     <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportEmpCode" runat="server" Text='<%# Eval("EmpCode") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ชื่อ-สกุลพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportFullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ผู้อนุมัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportApprover" runat="server" Text='<%# Eval("approver_name") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="วันที่ทำงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportRangeDate" runat="server" Text='<%# Eval("empshift_date_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="กะการทำงาน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportParttime" runat="server" Text='<%# Eval("parttime_code") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDocStatus" runat="server" Text='<%# Eval("announce_status_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="เอกสารแนบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDeptNameTH" runat="server" Text='' />
                        </ItemTemplate>
                     </asp:TemplateField>
                  </Columns>
               </asp:GridView>
            </ContentTemplate>
            <Triggers>
               <asp:PostBackTrigger ControlID="btnAnnounceMonthlyExport" />
            </Triggers>
         </asp:UpdatePanel>
      </div>
      <!-- END HR Area announce monthly export excel -->

      <!-- START HR Area change announce monthly export excel -->
      <div id="_divHRAreaChangeAnnounceMonthlyExportExcel" class="col-md-12" runat="server" visible="false">
         <div class="pull-right m-b-10">
            <asp:Button ID="btnChangeAnnounceMonthlyExport" runat="server" CssClass="btn btn-primary" Text="Export Excel" OnCommand="btnCommand" CommandName="btnChangeAnnounceMonthlyExport" />
         </div>
         <asp:UpdatePanel ID="updatePanelChangeAnnounceExportHRArea" runat="server">
            <ContentTemplate>
               <asp:GridView ID="gvChangeAnnounceExportHRArea"
                  runat="server"
                  AutoGenerateColumns="false"
                  CssClass="table table-striped table-bordered table-responsive"
                  HeaderStyle-CssClass="info"
                  HeaderStyle-ForeColor="#31708f"
                  AllowPaging="true"
                  OnRowCreated="gvOnRowCreated"
                  OnPageIndexChanging="OnPageIndexChanging"
                  PageSize="10">
                  <PagerStyle CssClass="pageCustom" />
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                  <EmptyDataTemplate>
                     <div class="text-center">ไม่พบข้อมูล</div>
                  </EmptyDataTemplate>
                  <Columns>
                     <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportEmpCode" runat="server" Text='<%# Eval("EmpCode") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ชื่อ-สกุลพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportFullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ผู้อนุมัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportApprover" runat="server" Text='<%# Eval("new_editor") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="วันที่ทำงานเดิม" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportRangeOldDate" runat="server" Text='<%# Eval("empshift_old_date_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="กะการทำงานเดิม" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportOldParttime" runat="server" Text='<%# Eval("parttime_old") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="วันที่ทำงานใหม่" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportRangeNewDate" runat="server" Text='<%# Eval("empshift_new_date_convert") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="กะการทำงานใหม่" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportNewParttime" runat="server" Text='<%# Eval("parttime_new") %>' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="สถานะเอกสาร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportDocStatus" runat="server" Text='' />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="เอกสารแนบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-VerticalAlign="Middle"
                        ItemStyle-Font-Size="Small" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                           <asp:Label ID="exportAttactFile" runat="server" Text='' />
                        </ItemTemplate>
                     </asp:TemplateField>
                  </Columns>
               </asp:GridView>
            </ContentTemplate>
            <Triggers>
               <asp:PostBackTrigger ControlID="btnChangeAnnounceMonthlyExport" />
            </Triggers>
         </asp:UpdatePanel>
      </div>
      <!-- END HR Area change announce monthly export excel -->
   </div>
   <!--*** END MENU liToHRArea ZONE ***-->

   <!--*** START JS Area ***-->
   <script type="text/javascript">
      var empIds = [[], []];
      var empIdxDiaryArr = [];
      var empCodeDiaryArr = [];
      var empFullNameDiaryArr = [];
      var empIdxArr = [];
      var empCodeArr = [];
      var empFullNameArr = [];
      var empCount = 0;
      var empDiaryCount = 0;
      var groupDiaryIdxArr = [];
      var groupDiaryNameArr = [];
      var groupDiaryDescriptionArr = [];
      var groupDiaryCount = 0;
      function getContrastTextColor(hexcolor) {
         var r = parseInt(hexcolor.substr(1, 2), 16);
         var g = parseInt(hexcolor.substr(3, 2), 16);
         var b = parseInt(hexcolor.substr(5, 2), 16);
         var textColor = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
         return parseInt(textColor) > 255 / 2 ? '#000000' : '#ffffff';
      }

      function addGroupDiaryToListFN(addGroupDiaryIdx) {
         if (groupDiaryIdxArr.indexOf(addGroupDiaryIdx) < 0) {
            groupDiaryIdxArr.push(addGroupDiaryIdx);
            groupDiaryNameArr.push($('#addGroupDiaryName' + addGroupDiaryIdx).val());
            groupDiaryDescriptionArr.push($('#addGroupDiaryDescription' + addGroupDiaryIdx).val());
            $('#addedGroupDiary' + addGroupDiaryIdx).show();
            $('#addGroupDiaryToList' + addGroupDiaryIdx).hide();
            var startGroupDiaryRow = '<tr class="rowGroupDiaryList' + addGroupDiaryIdx + '">';
            var endGroupDiaryRow = '</tr>';
            var addGroupDiaryNameCell = '<input type="hidden" value="' + addGroupDiaryIdx + '"><td class="f-s-12 text-center">' + $('#addGroupDiaryName' + addGroupDiaryIdx).val() + '</td>';
            var addGroupDiaryDescriptionCell = '<td class="f-s-12">' + $('#addGroupDiaryDescription' + addGroupDiaryIdx).val() + '</td>';
            var addRemoveGroupDiaryCell = '<td class="text-right"><button type="button" data-toggle="tooltip" data-placement="left" title="ลบ" onclick="removeGroupDiaryFromListFN(' + addGroupDiaryIdx + ', \'' + $('#addGroupDiaryName' + addGroupDiaryIdx).val() + '\', \'' + $('#addGroupDiaryDescription' + addGroupDiaryIdx).val() + '\')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>';
            $('.listDataGroupDiary').append(startGroupDiaryRow + addGroupDiaryNameCell + addGroupDiaryDescriptionCell + addRemoveGroupDiaryCell + endGroupDiaryRow);
            $('.group-diary-list-notfound').remove();
            $('.group-diary-announce-added-count').html(++groupDiaryCount);
         }
      }

      function removeGroupDiaryFromListFN(removeGroupDiaryIdx, removeGroupDiaryName, removeGroupDiaryDescription) {
         $('.rowGroupDiaryList' + removeGroupDiaryIdx).remove();
         $('#addedGroupDiary' + removeGroupDiaryIdx).hide();
         $('#addGroupDiaryToList' + removeGroupDiaryIdx).show();
         if (groupDiaryIdxArr.indexOf(removeGroupDiaryIdx) > -1) {
            groupDiaryIdxArr.splice(groupDiaryIdxArr.indexOf(removeGroupDiaryIdx), 1);
            groupDiaryNameArr.splice(groupDiaryNameArr.indexOf(removeGroupDiaryName), 1);
            groupDiaryDescriptionArr.splice(groupDiaryDescriptionArr.indexOf(removeGroupDiaryDescription), 1);
            if (groupDiaryIdxArr.length <= 0) {
               $('.listDataGroupDiary').append('<tr class="group-diary-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลกลุ่มในรายการ</td></tr>');
            }
            $('.group-diary-announce-added-count').html(--groupDiaryCount);
         }
      }

      function addEmpToListFN(addEmpIdx) {
         if (empIdxArr.indexOf(addEmpIdx) < 0) {
            empIdxArr.push(addEmpIdx);
            empCodeArr.push($('#addEmpCode' + addEmpIdx).val());
            empFullNameArr.push($('#addFullNameTH' + addEmpIdx).val());
            $('#added' + addEmpIdx).show();
            $('#addEmpToList' + addEmpIdx).hide();
            var startRow = '<tr class="rowList' + addEmpIdx + '">';
            var endRow = '</tr>';
            var addEmpCodeCell = '<input type="hidden" value="' + addEmpIdx + '"><td class="f-s-12">' + $('#addEmpCode' + addEmpIdx).val() + '</td>';
            var addFullNameCell = '<td class="f-s-12">' + $('#addFullNameTH' + addEmpIdx).val() + '</td>';
            var addRemoveCell = '<td class="text-right"><button type="button" data-toggle="tooltip" data-placement="left" title="ลบ"' +
               'onclick="removeEmpFromListFN(' + addEmpIdx + ', \'' + $('#addEmpCode' + addEmpIdx).val() + '\', \'' + $('#addFullNameTH' + addEmpIdx).val() + '\')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td >';
            $('.listDataEmp').append(startRow + addEmpCodeCell + addFullNameCell + addRemoveCell + endRow);
            $('.emp-list-notfound').remove();
            $('.emp-announce-added-count').html(++empCount);
         }
      }

      function removeEmpFromListFN(removeEmpIdx, removeEmpCode, removeFullNameTH) {
         $('.rowList' + removeEmpIdx).remove();
         $('#added' + removeEmpIdx).hide();
         $('#addEmpToList' + removeEmpIdx).show();
         if (empIdxArr.indexOf(removeEmpIdx) > -1) {
            empIdxArr.splice(empIdxArr.indexOf(removeEmpIdx), 1);
            empCodeArr.splice(empCodeArr.indexOf(removeEmpCode), 1);
            empFullNameArr.splice(empFullNameArr.indexOf(removeFullNameTH), 1);
            if (empIdxArr.length <= 0) {
               $('.listDataEmp').append('<tr class="emp-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลพนักงานในรายการ</td></tr>');
            }
            $('.emp-announce-added-count').html(--empCount);
         }
      }

      function addEmpDiaryToListFN(addEmpIdxDiary) {
         if (empIdxDiaryArr.indexOf(addEmpIdxDiary) < 0) {
            empIdxDiaryArr.push(addEmpIdxDiary);
            empCodeDiaryArr.push($('#addEmpCodeDiary' + addEmpIdxDiary).val());
            empFullNameDiaryArr.push($('#addFullNameTHDiary' + addEmpIdxDiary).val());
            $('#addedDiary' + addEmpIdxDiary).show();
            $('#addEmpDiaryToList' + addEmpIdxDiary).hide();
            var startRow = '<tr class="rowListDiary' + addEmpIdxDiary + '">';
            var endRow = '</tr>';
            var addEmpCodeDiaryCell = '<input type="hidden" value="' + addEmpIdxDiary + '"><td class="f-s-12">' + $('#addEmpCodeDiary' + addEmpIdxDiary).val() + '</td>';
            var addFullNameDiaryCell = '<td class="f-s-12">' + $('#addFullNameTHDiary' + addEmpIdxDiary).val() + '</td>';
            var addRemoveDiaryCell = '<td class="text-right"><button type="button" data-toggle="tooltip" data-placement="left" title="ลบ"' +
               'onclick="removeEmpDiaryFromListFN(' + addEmpIdxDiary + ', \'' + $('#addEmpCodeDiary' + addEmpIdxDiary).val() + '\', \'' + $('#addFullNameTHDiary' + addEmpIdxDiary).val() + '\')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td >';
            $('.listDataEmpDiary').append(startRow + addEmpCodeDiaryCell + addFullNameDiaryCell + addRemoveDiaryCell + endRow);
            $('.emp-diary-list-notfound').remove();
            $('.emp-announce-diary-added-count').html(++empDiaryCount);
         }
      }

      function removeEmpDiaryFromListFN(removeEmpIdx, removeEmpCode, removeFullNameTH) {
         $('.rowListDiary' + removeEmpIdx).remove();
         $('#addedDiary' + removeEmpIdx).hide();
         $('#addEmpDiaryToList' + removeEmpIdx).show();
         if (empIdxDiaryArr.indexOf(removeEmpIdx) > -1) {
            empIdxDiaryArr.splice(empIdxDiaryArr.indexOf(removeEmpIdx), 1);
            empCodeDiaryArr.splice(empCodeDiaryArr.indexOf(removeEmpCode), 1);
            empFullNameDiaryArr.splice(empFullNameDiaryArr.indexOf(removeFullNameTH), 1);
            if (empIdxDiaryArr.length <= 0) {
               $('.listDataEmpDiary').append('<tr class="emp-diary-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลพนักงานในรายการ</td></tr>');
            }
            $('.emp-announce-diary-added-count').html(--empDiaryCount);
         }
      }

      function previewAnnounceDiary(obj) {
         var divObjectDiary = document.getElementById("descDiary-" + obj);
         var btnViewObjectDiary = document.getElementById("btnViewDiaryDesc-" + obj);
         var btnHideObjectDiary = document.getElementById("btnHideDiaryDesc-" + obj);
         var tdPreviewDiary = document.getElementsByClassName("previewPendingDiaryListCalendar-" + obj)[0];
         var tablePreviewDiary = document.getElementsByClassName("table-employee-diary-list-" + obj)[0];
         if (divObjectDiary.style.display == "none") {
            tablePreviewDiary.style.display = "inline";
            tdPreviewDiary.style.padding = "8px";
            divObjectDiary.style.display = "inline";
            btnViewObjectDiary.style.display = "none";
            btnHideObjectDiary.style.display = "inline";
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: {
                  announceDiaryIdx: obj,
                  subString: true
               },
               success: function (data) {
                  var announceDiaryRangeStart = Date.parse(data[0].announceDiaryStartDate);
                  var announceDiaryRangeEnd = Date.parse(data[0].announceDiaryEndDate);
                  $('.lblCalendarPendingAnnounceDiary-' + obj).fullCalendar('destroy');
                  $('.lblCalendarPendingAnnounceDiary-' + obj).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingAnnounceDiary-' + obj).fullCalendar('gotoDate', announceDiaryRangeStart);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingAnnounceDiary-' + obj).fullCalendar('gotoDate', announceDiaryRangeStart);
               }
            });
         } else {
            tablePreviewDiary.style.display = "none";
            divObjectDiary.style.display = "none";
            btnViewObjectDiary.style.display = "inline";
            btnHideObjectDiary.style.display = "none";
            tdPreviewDiary.style.padding = "0px";
         }
      }

      function previewChangeAnnounceDiary(obj, objChange) {
         var divChangeObjectDiary = document.getElementById("descChangeDiary-" + obj + "-" + objChange);
         var btnViewChangeObjectDiary = document.getElementById("btnViewChangeDiaryDesc-" + obj + "-" + objChange);
         var btnHideChangeObjectDiary = document.getElementById("btnHideChangeDiaryDesc-" + obj + "-" + objChange);
         var tdChangePreviewDiary = document.getElementsByClassName("previewPendingChangeDiaryListCalendar-" + obj + "-" + objChange)[0];
         var tableChangePreviewDiary = document.getElementsByClassName("table-employee-change-diary-list-" + obj + "-" + objChange)[0];
         if (divChangeObjectDiary.style.display == "none") {
            tableChangePreviewDiary.style.display = "inline";
            tdChangePreviewDiary.style.padding = "8px";
            divChangeObjectDiary.style.display = "inline";
            btnViewChangeObjectDiary.style.display = "none";
            btnHideChangeObjectDiary.style.display = "inline";
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceDiaryIdx: obj,
                  subString: true
               },
               success: function (data) {
                  var announceDiaryOldRangeStart = Date.parse(data[0].announceDiaryStartDate);
                  var announceDiaryOldRangeEnd = Date.parse(data[0].announceDiaryEndDate);
                  $('.lblCalendarPendingChangeAnnounceDiaryOld-' + obj + "-" + objChange).fullCalendar('destroy');
                  $('.lblCalendarPendingChangeAnnounceDiaryOld-' + obj + "-" + objChange).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingChangeAnnounceDiaryOld-' + obj + "-" + objChange).fullCalendar('gotoDate', announceDiaryOldRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingChangeAnnounceDiaryOld-' + obj + '-' + objChange).fullCalendar('gotoDate', announceDiaryOldRangeStart);
               }
            });

            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-change-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceDiaryIdxRef: obj,
                  changeAnnounceDiaryIdx: objChange,
                  subString: true
               },
               success: function (data) {
                  var announceDiaryNewRangeStart = Date.parse(data[0].changeAnnounceDiaryStartDate);
                  var announceDiaryNewRangeEnd = Date.parse(data[0].changeAnnounceDiaryEndDate);
                  $('.lblCalendarPendingChangeAnnounceDiaryNew-' + obj + '-' + objChange).fullCalendar('destroy');
                  $('.lblCalendarPendingChangeAnnounceDiaryNew-' + obj + '-' + objChange).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingChangeAnnounceDiaryNew-' + obj + '-' + objChange).fullCalendar('gotoDate', announceDiaryNewRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingChangeAnnounceDiaryNew-' + obj + '-' + objChange).fullCalendar('gotoDate', announceDiaryNewRangeStart);
               }
            });
         } else {
            tableChangePreviewDiary.style.display = "none";
            divChangeObjectDiary.style.display = "none";
            btnViewChangeObjectDiary.style.display = "inline";
            btnHideChangeObjectDiary.style.display = "none";
            tdChangePreviewDiary.style.padding = "0px";
         }
      }

      function previewAnnounce(obj) {
         var divObject = document.getElementById("desc-" + obj);
         var btnViewObject = document.getElementById("btnViewDesc-" + obj);
         var btnHideObject = document.getElementById("btnHideDesc-" + obj);
         var tdPreview = document.getElementsByClassName("previewPendingListCalendar-" + obj)[0];
         if (divObject.style.display == "none") {
            tdPreview.style.padding = "8px";
            divObject.style.display = "inline";
            btnViewObject.style.display = "none";
            btnHideObject.style.display = "inline";
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: {
                  announceIdx: obj,
                  subString: true
               },
               success: function (data) {
                  var announceRangeStart = Date.parse(data[0].announceStartDate);
                  var announceRangeEnd = Date.parse(data[0].announceEndDate);
                  $('.lblCalendarPendingAnnounce-' + obj).fullCalendar('destroy');
                  $('.lblCalendarPendingAnnounce-' + obj).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingAnnounce-' + obj).fullCalendar('gotoDate', announceRangeStart);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingAnnounce-' + obj).fullCalendar('gotoDate', announceRangeStart);
               }
            });
         } else {
            divObject.style.display = "none";
            btnViewObject.style.display = "inline";
            btnHideObject.style.display = "none";
            tdPreview.style.padding = "0px";
         }
      }

      function previewChangeAnnounce(obj) {
         var divChangeObject = document.getElementById("descChange-" + obj);
         var btnViewChangeObject = document.getElementById("btnViewChangeDesc-" + obj);
         var btnHideChangeObject = document.getElementById("btnHideChangeDesc-" + obj);
         var tdChangePreview = document.getElementsByClassName("previewPendingListChangeCalendar-" + obj)[0];
         if (divChangeObject.style.display == "none") {
            tdChangePreview.style.padding = "8px";
            divChangeObject.style.display = "inline";
            btnViewChangeObject.style.display = "none";
            btnHideChangeObject.style.display = "inline";
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceIdx: obj,
                  subString: true
               },
               success: function (data) {
                  var announceOldRangeStart = Date.parse(data[0].announceStartDate);
                  var announceOldRangeEnd = Date.parse(data[0].announceEndDate);
                  $('.lblCalendarPendingChangeAnnounceOld-' + obj).fullCalendar('destroy');
                  $('.lblCalendarPendingChangeAnnounceOld-' + obj).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingChangeAnnounceOld-' + obj).fullCalendar('gotoDate', announceOldRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingChangeAnnounceOld-' + obj).fullCalendar('gotoDate', announceOldRangeStart);
               }
            });

            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-change-announce-with-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceIdxRef: obj,
                  subString: true
               },
               success: function (data) {
                  var announceNewRangeStart = Date.parse(data[0].changeAnnounceStartDate);
                  var announceNewRangeEnd = Date.parse(data[0].changeAnnounceEndDate);
                  $('.lblCalendarPendingChangeAnnounceNew-' + obj).fullCalendar('destroy');
                  $('.lblCalendarPendingChangeAnnounceNew-' + obj).fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblCalendarPendingChangeAnnounceNew-' + obj).fullCalendar('gotoDate', announceNewRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarPendingChangeAnnounceNew-' + obj).fullCalendar('gotoDate', announceNewRangeStart);
               }
            });
         } else {
            divChangeObject.style.display = "none";
            btnViewChangeObject.style.display = "inline";
            btnHideChangeObject.style.display = "none";
            tdChangePreview.style.padding = "0px";
         }
      }

      var prm = Sys.WebForms.PageRequestManager.getInstance();
      prm.add_endRequest(function () {
         try {
            /** START Check Length Group ID **/
            if (groupDiaryIdxArr.length <= 0) {
               $('.listDataGroupDiary').append('<tr class="group-diary-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลกลุ่มในรายการ</td></tr>');
            } else {
               for (var itemGroupDiaryIdx = 0; itemGroupDiaryIdx < groupDiaryIdxArr.length; itemGroupDiaryIdx++) {
                  var startGroupDiaryRow = '<tr class="rowGroupDiaryList' + groupDiaryIdxArr[itemGroupDiaryIdx] + '">';
                  var endGroupDiaryRow = '</tr>';
                  var addGroupDiaryNameCell = '<input type="hidden" value="' + groupDiaryIdxArr[itemGroupDiaryIdx] + '"><td class="f-s-12 text-center">' + groupDiaryNameArr[itemGroupDiaryIdx] + '</td>';
                  var addGroupDiaryDescriptionCell = '<td class="f-s-12">' + groupDiaryDescriptionArr[itemGroupDiaryIdx] + '</td>';
                  var addRemoveGroupDiaryCell = '<td class="text-right"><button type="button" onclick="removeGroupDiaryFromListFN(' + groupDiaryIdxArr[itemGroupDiaryIdx] + ', \'' + groupDiaryNameArr[itemGroupDiaryIdx] + '\', \'' + groupDiaryDescriptionArr[itemGroupDiaryIdx] + '\')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>';
                  $('.listDataGroupDiary').append(startGroupDiaryRow + addGroupDiaryNameCell + addGroupDiaryDescriptionCell + addRemoveGroupDiaryCell + endGroupDiaryRow);
                  $('.group-diary-list-notfound').remove();
                  $('.group-diary-announce-added-count').html(groupDiaryCount);
               }
            }
            /** END Check Length Group ID **/

            /** START Check Length Emp ID **/
            if (empIdxArr.length <= 0) {
               $('.listDataEmp').append('<tr class="emp-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลพนักงานในรายการ</td></tr>');
            } else {
               for (var itemEmpIdx = 0; itemEmpIdx < empIdxArr.length; itemEmpIdx++) {
                  var startRow = '<tr class="rowList' + empIdxArr[itemEmpIdx] + '">';
                  var endRow = '</tr>';
                  var addEmpCodeCell = '<input type="hidden" value="' + empIdxArr[itemEmpIdx] + '"><td class="f-s-12">' + empCodeArr[itemEmpIdx] + '</td>';
                  var addFullNameCell = '<td class="f-s-12">' + empFullNameArr[itemEmpIdx] + '</td>';
                  var addRemoveCell = '<td class="text-right"><button type="button" onclick="removeEmpFromListFN(' + empIdxArr[itemEmpIdx] + ', \'' + empCodeArr[itemEmpIdx] + '\', \'' + empFullNameArr[itemEmpIdx] + '\')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>';
                  $('.listDataEmp').append(startRow + addEmpCodeCell + addFullNameCell + addRemoveCell + endRow);
                  $('.emp-list-notfound').remove();
                  $('.emp-announce-added-count').html(empCount);
               }
            }
            /** END Check Length Emp ID **/

            /** START Check Length Emp ID Diary **/
            if (empIdxDiaryArr.length <= 0) {
               $('.listDataEmpDiary').append('<tr class="emp-diary-list-notfound"><td class="text-danger text-center f-s-12" colspan="3">ไม่พบข้อมูลพนักงานในรายการ</td></tr>');
            } else {
               empIdxDiaryArr = [];
               empCodeDiaryArr = [];
               empFullNameDiaryArr = [];
            }
            /** END Check Length Emp ID **/

            /** START Add Library Datepicker To Textbox **/
            $('.show-from-date-announce-group-diary-datepicker-onclick').click(function () {
               $('.from-date-announce-group-diary-datepicker').data("DateTimePicker").show();
            });
            $('.from-date-announce-group-diary-datepicker').datetimepicker({
               format: 'DD/MM/YYYY',
               minDate: moment()
            });
            $('.show-to-date-announce-group-diary-datepicker-onclick').click(function () {
               $('.to-date-announce-group-diary-datepicker').data("DateTimePicker").show();
            });
            $('.to-date-announce-group-diary-datepicker').datetimepicker({
               format: 'DD/MM/YYYY'
            });

            $('.show-from-date-announce-datepicker-onclick').click(function () {
               $('.from-date-announce-datepicker').data("DateTimePicker").show();
            });
            $('.from-date-announce-datepicker').datetimepicker({
               format: 'DD/MM/YYYY',
               minDate: moment()
            });
            $('.show-to-date-announce-datepicker-onclick').click(function () {
               $('.to-date-announce-datepicker').data("DateTimePicker").show();
            });
            $('.to-date-announce-datepicker').datetimepicker({
               format: 'DD/MM/YYYY'
            });

            $('.show-from-date-announce-datepicker-change-onclick').click(function () {
               $('.from-date-announce-datepicker-change').data("DateTimePicker").show();
            });
            $('.from-date-announce-datepicker-change').datetimepicker({
               format: 'DD/MM/YYYY',
               minDate: moment()
            });
            $('.show-to-date-announce-datepicker-change-onclick').click(function () {
               $('.to-date-announce-datepicker-change').data("DateTimePicker").show();
            });
            $('.to-date-announce-datepicker-change').datetimepicker({
               format: 'DD/MM/YYYY'
            });

            $('.show-from-date-announce-datepicker-change-diary-onclick').click(function () {
               $('.from-date-announce-datepicker-change-diary').data("DateTimePicker").show();
            });
            $('.from-date-announce-datepicker-change-diary').datetimepicker({
               format: 'DD/MM/YYYY',
               minDate: moment()
            });
            $('.show-to-date-announce-datepicker-change-diary-onclick').click(function () {
               $('.to-date-announce-datepicker-change-diary').data("DateTimePicker").show();
            });
            $('.to-date-announce-datepicker-change-diary').datetimepicker({
               format: 'DD/MM/YYYY'
            });
            /** END Add Library Datepicker To Textbox **/

            /** START Change Month in Calendar After Change Datepicker **/
            $('.from-date-announce-group-diary-datepicker').on('dp.change', function (e) {
               if ($(this).val() != "") {
                  var dateAnnounceGroupDiaryVal = $(this).val();
                  var splitDateAnnounceGroupDiary = dateAnnounceGroupDiaryVal.split("/");
                  var interDateAnnounceGroupDiary = splitDateAnnounceGroupDiary[2] + '-' + splitDateAnnounceGroupDiary[1] + '-' + splitDateAnnounceGroupDiary[0];
                  $('.lblCalendarAnnounceGroupDiary').fullCalendar('gotoDate', interDateAnnounceGroupDiary);
                  $('.to-date-announce-group-diary-datepicker').data("DateTimePicker").minDate(e.date);
               }
            });

            $('.from-date-announce-datepicker').on('dp.change', function (e) {
               if ($(this).val() != "") {
                  var dateAnnounceVal = $(this).val();
                  var splitDateAnnounce = dateAnnounceVal.split("/");
                  var interDateAnnounce = splitDateAnnounce[2] + '-' + splitDateAnnounce[1] + '-' + splitDateAnnounce[0];
                  $('.lblCalendarAnnounce').fullCalendar('gotoDate', interDateAnnounce);
                  $('.to-date-announce-datepicker').data("DateTimePicker").minDate(e.date);
               }
            });

            $('.from-date-announce-datepicker-change').on('dp.change', function (e) {
               if ($(this).val() != "") {
                  var dateChangeAnnounceVal = $(this).val();
                  var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
                  var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '-' + splitDateChangeAnnounce[1] + '-' + splitDateChangeAnnounce[0];
                  $('.lblCalendarNewEmpShift').fullCalendar('gotoDate', interDateChangeAnnounce);
                  $('.to-date-announce-datepicker-change').data("DateTimePicker").minDate(e.date);
               }
            });

            $('.from-date-announce-datepicker-change-diary').on('dp.change', function (e) {
               if ($(this).val() != "") {
                  var dateChangeAnnounceDiaryVal = $(this).val();
                  var splitDateChangeAnnounceDiary = dateChangeAnnounceDiaryVal.split("/");
                  var interDateChangeAnnounceDiary = splitDateChangeAnnounceDiary[2] + '-' + splitDateChangeAnnounceDiary[1] + '-' + splitDateChangeAnnounceDiary[0];
                  $('.lblCalendarNewEmpShiftDiary').fullCalendar('gotoDate', interDateChangeAnnounceDiary);
                  $('.to-date-announce-datepicker-change-diary').data("DateTimePicker").minDate(e.date);
               }
            });
            /** END Change Month in Calendar After Change Datepicker **/

            /** START Render Events Old Calendar Index Change EmpShift **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: {
                  announceIdx: $('.announceIdxIndexDescription').val(),
                  subString: true
               },
               success: function (data) {
                  $('.lblViewDescriptionIndexOldCalendar').fullCalendar({
                     customButtons: {
                        gotoDateAnnounce: {
                           text: 'วันเริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblViewDescriptionIndexOldCalendar').fullCalendar('gotoDate', data[0].announceStartDate);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexOldCalendar').fullCalendar('gotoDate', data[0].announceStartDate);
               }
            });
            /** END Render Events Old Calendar Change EmpShift **/

            /** START Render Events New Calendar Index Change EmpShift **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-change-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: {
                  changeAnnounceIdx: $('.changeAnnounceIdxIndexDescription').val(),
                  subString: true
               },
               success: function (data) {
                  $('.lblViewDescriptionIndexNewCalendar').fullCalendar({
                     customButtons: {
                        gotoDateChangeAnnounce: {
                           text: 'วันเริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblViewDescriptionIndexNewCalendar').fullCalendar('gotoDate', data[0].changeAnnounceStartDate);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'gotoDateChangeAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexNewCalendar').fullCalendar('gotoDate', data[0].changeAnnounceStartDate);
               }
            });
            /** END Render Events Old & New Calendar Change EmpShift **/

            /** START Get calendar for use announce diary **/
            $('.lblCalendarAnnounceGroupDiary').fullCalendar({
               customButtons: {
                  removeAllEventsGroupDiary: {
                     text: 'ล้างกะทั้งหมด',
                     click: function () {
                        $('.lblCalendarAnnounceGroupDiary').fullCalendar('removeEvents');
                     }
                  }
               },
               header: {
                  left: 'title',
                  right: 'removeAllEventsGroupDiary today prev,next',
               },
               editable: true,
               eventDurationEditable: false,
               droppable: true,
               dragRevertDuration: 150,
               eventRender: function (event, element) {
                  element.append("<span class='remove-event'><i class='fa fa-remove'></i></span>");
                  element.find(".remove-event").click(function () {
                     $('.lblCalendarAnnounceGroupDiary').fullCalendar('removeEvents', event._id);
                  });
                  element.append("<div class='clearfix'></div>");
               },
               eventConstraint: {
                  start: moment().format('YYYY-MM-DD'),
                  end: '2200-01-01'
               }
            });
            /** END Get calendar for use announce diary **/

            /** START Get calendar for use announce **/
            $('.lblCalendarAnnounce').fullCalendar({
               customButtons: {
                  removeAllEvents: {
                     text: 'ล้างกะทั้งหมด',
                     click: function () {
                        $('.lblCalendarAnnounce').fullCalendar('removeEvents');
                     }
                  }
               },
               header: {
                  left: 'title',
                  right: 'removeAllEvents today prev,next',
               },
               editable: true,
               eventDurationEditable: false,
               droppable: true,
               dragRevertDuration: 150,
               eventRender: function (event, element) {
                  element.append("<span class='remove-event'><i class='fa fa-remove'></i></span>");
                  element.find(".remove-event").click(function () {
                     $('.lblCalendarAnnounce').fullCalendar('removeEvents', event._id);
                  });
                  element.append("<div class='clearfix'></div>");
               },
               eventConstraint: {
                  start: moment().format('YYYY-MM-DD'),
                  end: '2200-01-01'
               }
            });
            /** END Get calendar for use announce **/

            /** START Search parttime for use announce **/
            $('.txtSearchParttime').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchParttime').click();
                  return false;
               }
            });
            $('.img-loading-parttime-search').hide();
            $('.btnSearchParttime').click(function () {
               if ($.trim($('.txtSearchParttime').val()) != "") {
                  $('.img-loading-parttime-search').show();
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/search-parttime.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchParttime').val() },
                     success: function (data) {
                        $('.external-events-listing').html('');
                        $('.img-loading-parttime-search').hide();
                        var i = 0;
                        var htmlSearchParttime = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              htmlSearchParttime += "<div class='bg-external-event'><div class=\"fc-event\"" +
                                 "data-parttime-idx='" + data[i].m0_parttime_idx + "'" +
                                 "data-title='" + data[i].parttime_code + "'" +
                                 "data-color='" + data[i].parttime_bg_color + "'" +
                                 "data-text-color='" + getContrastTextColor(data[i].parttime_bg_color) + "'" +
                                 "style='color:" + getContrastTextColor(data[i].parttime_bg_color) + ";background-color:" + data[i].parttime_bg_color + ";border: 1px solid " + data[i].parttime_bg_color + "'>" +
                                 "<h4>" + data[i].parttime_code + "</h4>" + data[i].parttime_name_th +
                                 "</div>" +
                                 "</div>";
                              i++;
                           });
                        } else {
                           htmlSearchParttime += "<div class='text-center text-danger f-s-12'>ไม่พบข้อมูลกะการทำงานที่ค้นหา</div>";
                        }
                        $('.external-events-listing').append(htmlSearchParttime);
                        $('.external-events .fc-event').each(function () {
                           $(this).data('event', {
                              parttimeIdx: $(this).attr('data-parttime-idx'),
                              title: $(this).attr('data-title'),
                              color: $(this).attr('data-color'),
                              textColor: $(this).attr('data-text-color'),
                              stick: true
                           });
                           $(this).draggable({
                              zIndex: 999,
                              cursor: 'move',
                              revert: true,
                              revertDuration: 0,
                              containment: '#external-events-dragging-area'
                           });
                        });
                     }
                  });
               }
            });
            /** END Search parttime for use announce **/

            /** START Search group diary for use announce **/
            $('.txtSearchParttimeGroupDiary').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchParttimeGroupDiary').click();
                  return false;
               }
            });
            $('.img-loading-parttime-search-group-diary').hide();
            $('.btnSearchParttimeGroupDiary').click(function () {
               if ($.trim($('.txtSearchParttimeGroupDiary').val()) != "") {
                  $('.img-loading-parttime-search-group-diary').show();
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/search-parttime.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchParttimeGroupDiary').val() },
                     success: function (data) {
                        $('.external-events-listing-group-diary').html('');
                        $('.img-loading-parttime-search-group-diary').hide();
                        var j = 0;
                        var html = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              html += "<div class='bg-external-event'><div class=\"fc-event\"" +
                                 "data-parttime-idx='" + data[j].m0_parttime_idx + "'" +
                                 "data-title='" + data[j].parttime_code + "'" +
                                 "data-color='" + data[j].parttime_bg_color + "'" +
                                 "data-text-color='" + getContrastTextColor(data[j].parttime_bg_color) + "'" +
                                 "style='color:" + getContrastTextColor(data[j].parttime_bg_color) + ";background-color:" + data[j].parttime_bg_color + ";border: 1px solid " + data[j].parttime_bg_color + "'>" +
                                 "<h4>" + data[j].parttime_code + "</h4>" + data[j].parttime_name_th +
                                 "</div>" +
                                 "</div>";
                              j++;
                           });
                        } else {
                           html += "<div class='text-center text-danger f-s-12'>ไม่พบข้อมูลกะการทำงานที่ค้นหา</div>";
                        }
                        $('.external-events-listing-group-diary').append(html);
                        $('.external-events-diary .fc-event').each(function () {
                           $(this).data('event', {
                              parttimeIdx: $(this).attr('data-parttime-idx'),
                              title: $(this).attr('data-title'),
                              color: $(this).attr('data-color'),
                              textColor: $(this).attr('data-text-color'),
                              stick: true
                           });
                           $(this).draggable({
                              zIndex: 999,
                              cursor: 'move',
                              revert: true,
                              revertDuration: 0,
                              containment: '#external-events-dragging-area-group-diary'
                           });
                        });
                     }
                  });
               }
            });
            /** END Search parttime group diary for use announce **/

            /** START Search group diary for use announce **/
            $('.txtSearchGroupDiary').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchGroupDiary').click();
                  return false;
               }
            });
            $('.img-loading-group-diary-list').hide();
            $('.btnSearchGroupDiary').click(function () {
               if ($.trim($('.txtSearchGroupDiary').val()) != "") {
                  $('.img-loading-group-diary-list').show();
                  $('.group-diary-search-list').html('');
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/diary/search-group-diary.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchGroupDiary').val() },
                     success: function (data) {
                        $('.img-loading-group-diary-list').hide();
                        var startTableGroupDiary = '<table class="table table-striped">';
                        var tHeaderGroupDiary = '<thead><tr class="f-bold f-s-12"><td class="text-center">ชื่อกลุ่ม</td><td>รายละเอียด</td><td class="text-right text-danger">' + data.length + ' รายการ</td></tr></thead>';
                        var startTBodyGroupDiary = '<tbody>';
                        var endTBodyGroupDiary = '</tbody>';
                        var endTableGroupDiary = '</table>';
                        var k = 0;
                        var htmlGroupDiary = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              if (groupDiaryIdxArr.length >= 0) {
                                 if (groupDiaryIdxArr.indexOf(data[k].groupDiaryIdx) > -1) {
                                    var addGroupDiaryDisplayNone = "style='display:none'";
                                    var removeGroupDiaryDisplayNone = "";
                                 } else {
                                    var addGroupDiaryDisplayNone = "";
                                    var removeGroupDiaryDisplayNone = "style='display:none'";
                                 }
                              } else {
                                 var addGroupDiaryDisplayNone = "";
                                 var removeGroupDiaryDisplayNone = "style='display:none'";
                              }
                              htmlGroupDiary += "<tr class='f-s-12'>" +
                                 "<td class='text-center'>" + data[k].groupDiaryName + "<input type='hidden' id='addGroupDiaryIdx" + data[k].groupDiaryIdx + "' value='" + data[k].groupDiaryIdx + "'>" +
                                 "<input type='hidden' id='addGroupDiaryName" + data[k].groupDiaryIdx + "' value='" + data[k].groupDiaryName + "'></td>" +
                                 "<td><b>องค์กร:</b> " + data[k].groupDiaryOrgNameTH + "<br><b>ฝ่าย:</b> " + data[k].groupDiaryDeptNameTH + "<br><b>แผนก:</b> " + data[k].groupDiarySecNameTH + "<input type='hidden' id='addGroupDiaryDescription" + data[k].groupDiaryIdx + "' value='<b>องค์กร:</b> " + data[k].groupDiaryOrgNameTH + "<br><b>ฝ่าย:</b> " + data[k].groupDiaryDeptNameTH + "<br><b>แผนก:</b> " + data[k].groupDiarySecNameTH + "'></td>" +
                                 "<td class='text-right'><button data-toggle='tooltip' data-placement='left' title='เพิ่ม' type='button'" + addGroupDiaryDisplayNone + " id='addGroupDiaryToList" + data[k].groupDiaryIdx + "' onclick='addGroupDiaryToListFN(" + data[k].groupDiaryIdx + ");' class='btn btn-success btn-xs'><i class='fa fa-plus'></i></button><button type='button' " + removeGroupDiaryDisplayNone + " disabled id='addedGroupDiary" + data[k].groupDiaryIdx + "' class='btn btn-default btn-xs'><i class='fa fa-check'></i></button></td>" +
                                 "</tr>";
                              k++;
                           });
                           $('.group-diary-search-list').append(startTableGroupDiary + tHeaderGroupDiary + startTBodyGroupDiary + htmlGroupDiary + endTBodyGroupDiary + endTableGroupDiary);
                        } else {
                           htmlGroupDiary += "<tr><td class='text-center text-danger f-s-12' colspan='3'>ไม่พบข้อมูลกลุ่มที่ค้นหา</td></tr>";
                           $('.group-diary-search-list').append(startTableGroupDiary + htmlGroupDiary + endTableGroupDiary);
                        }
                     }
                  });
               }
            });
            /** END Search group diary for use announce **/

            /** START Search employee for use announce **/
            $('.txtSearchEmployee').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchEmployee').click();
                  return false;
               }
            });
            $('.img-loading-employee-list').hide();
            $('.btnSearchEmployee').click(function () {
               if ($.trim($('.txtSearchEmployee').val()) != "") {
                  $('.img-loading-employee-list').show();
                  $('.employee-search-list').html('');
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/search-employee.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchEmployee').val() },
                     success: function (data) {
                        $('.img-loading-employee-list').hide();
                        var startTable = '<table class="table table-striped">';
                        var tHeader = '<thead><tr class="f-bold f-s-12"><td>รหัสพนักงาน</td><td>ชื่อ - สกุล</td><td class="text-right text-danger">' + data.length + ' รายการ</td></tr></thead>';
                        var startTBody = '<tbody>';
                        var endTBody = '</tbody>';
                        var endTable = '</table>';
                        var l = 0;
                        var htmlEmp = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              if (empIdxArr.length >= 0) {
                                 if (empIdxArr.indexOf(data[l].empIdx) > -1) {
                                    var addDisplayNone = "style='display:none'";
                                    var removeDisplayNone = "";
                                 } else {
                                    var addDisplayNone = "";
                                    var removeDisplayNone = "style='display:none'";
                                 }
                              } else {
                                 var addDisplayNone = "";
                                 var removeDisplayNone = "style='display:none'";
                              }
                              htmlEmp += "<tr class='f-s-12'>" +
                                 "<td>" + data[l].empCode + "<input type='hidden' id='addEmpIdx" + data[l].empIdx + "' value='" + data[l].empIdx + "'>" +
                                 "<input type='hidden' id='addEmpCode" + data[l].empIdx + "' value='" + data[l].empCode + "'></td>" +
                                 "<td>" + data[l].fullNameTH + "<input type='hidden' id='addFullNameTH" + data[l].empIdx + "' value='" + data[l].fullNameTH + "'></td>" +
                                 "<td class='text-right'><button data-toggle='tooltip' data-placement='left' title='เพิ่ม' type='button'" + addDisplayNone + " id='addEmpToList" + data[l].empIdx + "' onclick='addEmpToListFN(" + data[l].empIdx + ");' class='btn btn-success btn-xs'><i class='fa fa-plus'></i></button><button type='button' " + removeDisplayNone + " disabled id='added" + data[l].empIdx + "' class='btn btn-default btn-xs'><i class='fa fa-check'></i></button></td>" +
                                 "</tr>";
                              l++;
                           });
                           $('.employee-search-list').append(startTable + tHeader + startTBody + htmlEmp + endTBody + endTable);
                        } else {
                           htmlEmp += "<tr><td class='text-center text-danger f-s-12' colspan='3'>ไม่พบข้อมูลพนักงานที่ค้นหา</td></tr>";
                           $('.employee-search-list').append(startTable + htmlEmp + endTable);
                        }
                     }
                  });
               }
            });
            /** END Search employee for use announce **/

            /** START Create announce onclick use announce **/
            $('.img-loading-announce').hide();
            $('.btnAnnounce').click(function () {
               if (!confirm('คุณต้องการบันทึกใช่หรือไม่')) {
                  return false;
               } else {
                  if (empIdxArr.length <= 0) {
                     alert('กรุณาเลือกพนักงานที่ต้องการประกาศกะ');
                     return false;
                  } else if ($('.from-date-announce-datepicker').val() == '' || $('.to-date-announce-datepicker').val() == '') {
                     alert('กรุณาเลือกช่วงวันที่ต้องการประกาศกะ');
                     return false;
                  } else {
                     var clientEventsAnnounce = $('.lblCalendarAnnounce').fullCalendar('clientEvents');
                     if (clientEventsAnnounce.length <= 0) {
                        alert('กรุณาเลือกกะการทำงานลงที่ปฏิทิน');
                        return false;
                     } else {
                        $('.img-loading-announce').show();
                        $('.btnAnnounce').hide();
                        var txtAnnounceStartDate = $('.from-date-announce-datepicker').val();
                        var txtAnnounceEndDate = $('.to-date-announce-datepicker').val();
                        var announceStartDateSplit = txtAnnounceStartDate.split("/");
                        var announceEndDateSplit = txtAnnounceEndDate.split("/");
                        var announceStartDateInter = announceStartDateSplit[2] + '-' + announceStartDateSplit[1] + '-' + announceStartDateSplit[0];
                        var announceEndDateInter = announceEndDateSplit[2] + '-' + announceEndDateSplit[1] + '-' + announceEndDateSplit[0];
                        for (var lengthEmpIdx = 0; lengthEmpIdx < empIdxArr.length; lengthEmpIdx++) {
                           $.ajax({
                              url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-announce.aspx.ashx") %>',
                              dataType: 'json',
                              data: {
                                 announceStartDate: announceStartDateInter,
                                 announceEndDate: announceEndDateInter,
                                 creator: $('.txtEmpIdxLogin').val(),
                                 empIdx: empIdxArr[lengthEmpIdx]
                              },
                              success: function (data) {
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-announce-log.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       announceIdx: data[0].announceIdx,
                                       nodeIdx: 1,
                                       approveLogTypes: 1, //type announce
                                       creator: $('.txtEmpIdxLogin').val()
                                    }
                                 });
                                 for (var lengthEvents = 0; lengthEvents < clientEventsAnnounce.length; lengthEvents++) {
                                    $.ajax({
                                       url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-emps.aspx.ashx") %>',
                                       dataType: 'json',
                                       async: false,
                                       data: {
                                          announceIdx: data[0].announceIdx,
                                          dateEvent: clientEventsAnnounce[lengthEvents].start.format("YYYY-MM-DD"),
                                          parttimeIdx: clientEventsAnnounce[lengthEvents].parttimeIdx,
                                          creator: $('.txtEmpIdxLogin').val()
                                       }
                                    });
                                 }
                              }
                           });
                        }
                        setTimeout(function () {
                           alert('ประกาศกะการทำงานเรียบร้อยแล้ว');
                           location.reload();
                        }, 3000);
                     }
                  }
               }
            });
            /** END Create announce onclick use announce **/

            /** START Create announce group diary onclick use announce **/
            $('.img-loading-announce-group-diary').hide();
            $('.btnAnnounceGroupDiary').click(function () {
               if (!confirm('คุณต้องการบันทึกใช่หรือไม่')) {
                  return false;
               } else {
                  if (groupDiaryIdxArr.length <= 0) {
                     alert('กรุณาเลือกกลุ่มที่ต้องการประกาศกะ');
                     return false;
                  } else if ($('.from-date-announce-group-diary-datepicker').val() == '' || $('.to-date-announce-group-diary-datepicker').val() == '') {
                     alert('กรุณาเลือกช่วงวันที่ต้องการประกาศกะ');
                     return false;
                  } else {
                     var clientEventsAnnounceGroupDiary = $('.lblCalendarAnnounceGroupDiary').fullCalendar('clientEvents');
                     if (clientEventsAnnounceGroupDiary.length <= 0) {
                        alert('กรุณาเลือกกะการทำงานลงที่ปฏิทิน');
                        return false;
                     } else {
                        $('.img-loading-announce-group-diary').show();
                        $('.btnAnnounceGroupDiary').hide();
                        var txtAnnounceGroupDiaryStartDate = $('.from-date-announce-group-diary-datepicker').val();
                        var txtAnnounceGroupDiaryEndDate = $('.to-date-announce-group-diary-datepicker').val();
                        var announceGroupDiaryStartDateSplit = txtAnnounceGroupDiaryStartDate.split("/");
                        var announceGroupDiaryEndDateSplit = txtAnnounceGroupDiaryEndDate.split("/");
                        var announceGroupDiaryStartDateInter = announceGroupDiaryStartDateSplit[2] + '-' + announceGroupDiaryStartDateSplit[1] + '-' + announceGroupDiaryStartDateSplit[0];
                        var announceGroupDiaryEndDateInter = announceGroupDiaryEndDateSplit[2] + '-' + announceGroupDiaryEndDateSplit[1] + '-' + announceGroupDiaryEndDateSplit[0];
                        for (var lengthGroupDiaryIdxFirst = 0; lengthGroupDiaryIdxFirst < groupDiaryIdxArr.length; lengthGroupDiaryIdxFirst++) {
                           $.ajax({
                              url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emp-by-groupidx.aspx.ashx") %>',
                              dataType: 'json',
                              async: false,
                              data: {
                                 groupDiaryIdx: groupDiaryIdxArr[lengthGroupDiaryIdxFirst]
                              },
                              success: function (data) {
                                 for (var lengthEmp = 0; lengthEmp < data.length; lengthEmp++) {
                                    empIds[lengthGroupDiaryIdxFirst].push(data[lengthEmp].emp_idx);
                                 }
                              }
                           });
                        }
                        for (var lengthGroupDiaryIdxSecond = 0; lengthGroupDiaryIdxSecond < empIds.length; lengthGroupDiaryIdxSecond++) {
                           var empIdLength = empIds[lengthGroupDiaryIdxSecond].length;
                           var empId = empIds[lengthGroupDiaryIdxSecond];
                           $.ajax({
                              url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-announce-group-diary.aspx.ashx") %>',
                              dataType: 'json',
                              data: {
                                 empIdxs: lengthGroupDiaryIdxSecond,
                                 empLengthTemp: empIdLength,
                                 announceGroupDiaryStartDate: announceGroupDiaryStartDateInter,
                                 announceGroupDiaryEndDate: announceGroupDiaryEndDateInter,
                                 creator: $.trim($('.txtEmpIdxLogin').val()),
                                 groupDiaryIdx: groupDiaryIdxArr[lengthGroupDiaryIdxSecond]
                              },
                              success: function (data) {
                                 var empIdxsx = empIds[data[0].empIdxss];
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-announce-group-diary-log.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       announceGroupDiaryIdx: data[0].announceGroupDiaryIdx,
                                       nodeIdx: 1,
                                       approveLogTypes: 1, //type announce
                                       creator: $.trim($('.txtEmpIdxLogin').val())
                                    }
                                 });
                                 for (var lengthEmpGroupDiary = 0; lengthEmpGroupDiary < data[0].empLengthTemps; lengthEmpGroupDiary++) {
                                    $.ajax({
                                       url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-emp-group-diary.aspx.ashx") %>',
                                       dataType: 'json',
                                       async: false,
                                       data: {
                                          announceGroupDiaryIdx: data[0].announceGroupDiaryIdx,
                                          empIdxGroup: parseInt(empIdxsx[lengthEmpGroupDiary]),
                                          creator: $.trim($('.txtEmpIdxLogin').val())
                                       }
                                    });
                                 }
                                 for (var lengthEventsGroupDiary = 0; lengthEventsGroupDiary < clientEventsAnnounceGroupDiary.length; lengthEventsGroupDiary++) {
                                    $.ajax({
                                       url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-emps-group-diary.aspx.ashx") %>',
                                       dataType: 'json',
                                       async: false,
                                       data: {
                                          announceGroupDiaryIdx: data[0].announceGroupDiaryIdx,
                                          dateEvent: clientEventsAnnounceGroupDiary[lengthEventsGroupDiary].start.format("YYYY-MM-DD"),
                                          parttimeIdx: clientEventsAnnounceGroupDiary[lengthEventsGroupDiary].parttimeIdx,
                                          creator: $.trim($('.txtEmpIdxLogin').val())
                                       }
                                    });
                                 }
                              }
                           });
                        }
                        setTimeout(function () {
                           alert('ประกาศกะการทำงานเรียบร้อยแล้ว');
                           location.reload();
                        }, 3000);
                     }
                  }
               }
            });
            /** END Create announce group diary onclick use announce **/

            /** START Render Events Old & New Calendar Change EmpShift Diary **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               data: { announceDiaryIdx: $('.txtChangeAnnounceDiaryIdx').val() },
               success: function (data) {
                  $('.lblCalendarOldEmpShiftDiary').fullCalendar({
                     customButtons: {
                        gotoDateAnnounce: {
                           text: 'วันเริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblCalendarOldEmpShiftDiary').fullCalendar('gotoDate', data[0].announceDiaryStartDate);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     },
                     height: $(window).height() * 0.83
                  });
                  $('.lblCalendarOldEmpShiftDiary').fullCalendar('gotoDate', data[0].announceDiaryStartDate);

                  $('.lblCalendarNewEmpShiftDiary').fullCalendar({
                     customButtons: {
                        removeAllEvents: {
                           text: 'ล้างกะทั้งหมด',
                           click: function () {
                              $('.lblCalendarNewEmpShiftDiary').fullCalendar('removeEvents');
                           }
                        },
                        resetEvents: {
                           text: 'คืนค่า',
                           click: function () {
                              $('.lblCalendarNewEmpShiftDiary').fullCalendar('removeEvents');
                              $('.lblCalendarNewEmpShiftDiary').fullCalendar('addEventSource', data);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'resetEvents removeAllEvents gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: true,
                     eventDurationEditable: false,
                     droppable: true,
                     dragRevertDuration: 150,
                     eventRender: function (event, element) {
                        element.append("<span class='remove-event'><i class='fa fa-remove'></i></span>");
                        element.find(".remove-event").click(function () {
                           $('.lblCalendarNewEmpShiftDiary').fullCalendar('removeEvents', event._id);
                        });
                        element.append("<div class='clearfix'></div>");
                     },
                     eventConstraint: {
                        start: moment().format('YYYY-MM-DD'),
                        end: '2200-01-01'
                     }
                  });
                  $('.lblCalendarNewEmpShiftDiary').fullCalendar('gotoDate', data[0].announceDiaryStartDate);
                  $('.from-date-announce-datepicker-change-diary').val(data[0].newAnnounceDiaryStartDate)
                  $('.to-date-announce-datepicker-change-diary').val(data[0].newAnnounceDiaryEndDate);
               }
            });
            /** END Render Events Old & New Calendar Change EmpShift Diary **/

            /** START Render Events Old & New Calendar Change EmpShift **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: { announceIdx: $('.txtChangeAnnounceIdx').val() },
               success: function (data) {
                  $('.lblCalendarOldEmpShift').fullCalendar({
                     customButtons: {
                        gotoDateAnnounce: {
                           text: 'วันเริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblCalendarOldEmpShift').fullCalendar('gotoDate', data[0].announceStartDate);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblCalendarOldEmpShift').fullCalendar('gotoDate', data[0].announceStartDate);

                  $('.lblCalendarNewEmpShift').fullCalendar({
                     customButtons: {
                        removeAllEvents: {
                           text: 'ล้างกะทั้งหมด',
                           click: function () {
                              $('.lblCalendarNewEmpShift').fullCalendar('removeEvents');
                           }
                        },
                        resetEvents: {
                           text: 'คืนค่า',
                           click: function () {
                              $('.lblCalendarNewEmpShift').fullCalendar('removeEvents');
                              $('.lblCalendarNewEmpShift').fullCalendar('addEventSource', data);
                           }
                        }
                     },
                     header: {
                        left: 'title',
                        right: 'resetEvents removeAllEvents gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: true,
                     eventDurationEditable: false,
                     droppable: true,
                     dragRevertDuration: 150,
                     eventRender: function (event, element) {
                        element.append("<span class='remove-event'><i class='fa fa-remove'></i></span>");
                        element.find(".remove-event").click(function () {
                           $('.lblCalendarNewEmpShift').fullCalendar('removeEvents', event._id);
                        });
                        element.append("<div class='clearfix'></div>");
                     },
                     eventConstraint: {
                        start: moment().format('YYYY-MM-DD'),
                        end: '2200-01-01'
                     }
                  });
                  $('.lblCalendarNewEmpShift').fullCalendar('gotoDate', data[0].announceStartDate);
                  $('.from-date-announce-datepicker-change').val(data[0].newAnnounceStartDate)
                  $('.to-date-announce-datepicker-change').val(data[0].newAnnounceEndDate);
               }
            });
            /** END Render Events Old & New Calendar Change EmpShift **/

            /** START Search Parttime Change EmpShift **/
            $('.txtSearchParttimeChange').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchParttimeChange').click();
                  return false;
               }
            });
            $('.img-loading-change-announce-search-parttime').hide();
            $('.btnSearchParttimeChange').click(function () {
               if ($.trim($('.txtSearchParttimeChange').val()) != "") {
                  $('.img-loading-change-announce-search-parttime').show();
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/search-parttime.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchParttimeChange').val() },
                     success: function (data) {
                        $('.external-events-listing-change').html('');
                        $('.img-loading-change-announce-search-parttime').hide();
                        var m = 0;
                        var htmlSearchParttimeChange = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              htmlSearchParttimeChange += "<div class='bg-external-event'><div class=\"fc-event\"" +
                                 "data-parttime-idx='" + data[m].m0_parttime_idx + "'" +
                                 "data-title='" + data[m].parttime_code + "'" +
                                 "data-color='" + data[m].parttime_bg_color + "'" +
                                 "data-text-color='" + getContrastTextColor(data[m].parttime_bg_color) + "'" +
                                 "style='color:" + getContrastTextColor(data[m].parttime_bg_color) + ";background-color:" + data[m].parttime_bg_color + ";border: 1px solid " + data[m].parttime_bg_color + "'>" +
                                 "<h4>" + data[m].parttime_code + "</h4>" + data[m].parttime_name_th +
                                 "</div>" +
                                 "</div>";
                              m++;
                           });
                        } else {
                           htmlSearchParttimeChange += "<div class='text-center text-danger f-s-12'>ไม่พบข้อมูลกะการทำงานที่ค้นหา</div>";
                        }
                        $('.external-events-listing-change').append(htmlSearchParttimeChange);
                        $('.external-events-change .fc-event').each(function () {
                           $(this).data('event', {
                              parttimeIdx: $(this).attr('data-parttime-idx'),
                              title: $(this).attr('data-title'),
                              color: $(this).attr('data-color'),
                              textColor: $(this).attr('data-text-color'),
                              stick: true
                           });
                           $(this).draggable({
                              zIndex: 999,
                              cursor: 'move',
                              revert: true,
                              revertDuration: 0,
                              containment: '#external-events-dragging-area-change'
                           });
                        });
                     }
                  });
               }
            });
            /** END Search Parttime Change EmpShift **/

            /** START Create change announce onclick use change announce **/
            $('.img-loading-change-announce').hide();
            $('.btnChangeAnnounce').click(function () {
               if (!confirm('คุณต้องการบันทึกใช่หรือไม่')) {
                  return false;
               } else {
                  if ($('.from-date-announce-datepicker-change').val() == '' || $('.to-date-announce-datepicker-change').val() == '') {
                     alert('กรุณาเลือกช่วงวันที่ต้องการประกาศกะ');
                     return false;
                  } else {
                     var clientEventsChangeAnnounce = $('.lblCalendarNewEmpShift').fullCalendar('clientEvents');
                     if (clientEventsChangeAnnounce.length <= 0) {
                        alert('กรุณาเลือกกะการทำงานลงที่ปฏิทิน');
                        return false;
                     } else {
                        $('.btnChangeAnnounce').hide();
                        $('.img-loading-change-announce').show();
                        var txtChangeAnnounceStartDate = $('.from-date-announce-datepicker-change').val();
                        var txtChangeAnnounceEndDate = $('.to-date-announce-datepicker-change').val();
                        var changeAnnounceStartDateSplit = txtChangeAnnounceStartDate.split("/");
                        var changeAnnounceEndDateSplit = txtChangeAnnounceEndDate.split("/");
                        var changeAnnounceStartDateInter = changeAnnounceStartDateSplit[2] + '-' + changeAnnounceStartDateSplit[1] + '-' + changeAnnounceStartDateSplit[0];
                        var changeAnnounceEndDateInter = changeAnnounceEndDateSplit[2] + '-' + changeAnnounceEndDateSplit[1] + '-' + changeAnnounceEndDateSplit[0];
                        $.ajax({
                           url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-change-announce.aspx.ashx") %>',
                           dataType: 'json',
                           data: {
                              announceIdxRef: $('.txtChangeAnnounceIdx').val(),
                              changeAnnounceStartDate: changeAnnounceStartDateInter,
                              changeAnnounceEndDate: changeAnnounceEndDateInter,
                              creator: $('.txtChangeAnnounceEmpIdxLogin').val()
                           },
                           success: function (data) {
                              $.ajax({
                                 url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-announce-log.aspx.ashx") %>',
                                 dataType: 'json',
                                 async: false,
                                 data: {
                                    announceIdx: data[0].changeAnnounceIdx,
                                    nodeIdx: 1,
                                    approveLogTypes: 2, //type change announce
                                    creator: $('.txtChangeAnnounceEmpIdxLogin').val()
                                 }
                              });
                              for (var lengthNewEvents = 0; lengthNewEvents < clientEventsChangeAnnounce.length; lengthNewEvents++) {
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/create-change-emps.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       changeAnnounceIdxRef: data[0].changeAnnounceIdx,
                                       dateEvent: clientEventsChangeAnnounce[lengthNewEvents].start.format("YYYY-MM-DD"),
                                       parttimeIdx: clientEventsChangeAnnounce[lengthNewEvents].parttimeIdx
                                    }
                                 });
                              }
                              $.ajax({
                                 url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/update-announce-status.aspx.ashx") %>',
                                 dataType: 'json',
                                 async: false,
                                 data: {
                                    announceIdx: $('.txtChangeAnnounceIdx').val(),
                                    updator: $('.txtChangeAnnounceEmpIdxLogin').val()
                                 }
                              });
                           }
                        });
                        setTimeout(function () {
                           alert('บันทึกการร้องขอเปลี่ยนกะการทำงานเรียบร้อยแล้ว');
                           location.reload();
                        }, 3000);
                     }
                  }
               }
            });
            /** END Create change announce onclick use change announce **/

            /** START View Description Announce Index **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/monthly/emps-by-announce.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: { announceIdx: $('.announceIdxViewDescription').val() },
               success: function (data) {
                  $('.lblViewDescriptionIndexCalendar').fullCalendar({
                     customButtons: {
                        gotoDateAnnounce: {
                           text: 'วันที่เริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblViewDescriptionIndexCalendar').fullCalendar('gotoDate', data[0].announceStartDate);
                           }
                        }
                     },
                     height: 600,
                     header: {
                        left: 'title',
                        right: 'gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexCalendar').fullCalendar('gotoDate', data[0].announceStartDate);
               }
            });
            /** END View Description Announce Index **/

            /** START View Description Announce Index **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               data: { announceDiaryIdx: $('.announceDiaryIdxViewDescription').val() },
               success: function (data) {
                  $('.lblViewDescriptionIndexCalendarDiary').fullCalendar({
                     customButtons: {
                        gotoDateAnnounce: {
                           text: 'วันที่เริ่มต้นกะทำงาน',
                           click: function () {
                              $('.lblViewDescriptionIndexCalendarDiary').fullCalendar('gotoDate', data[0].announceDiaryStartDate);
                           }
                        }
                     },
                     height: 600,
                     header: {
                        left: 'title',
                        right: 'gotoDateAnnounce prev,next',
                     },
                     events: data,
                     editable: false,
                     eventDurationEditable: false,
                     droppable: false,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexCalendarDiary').fullCalendar('gotoDate', data[0].announceDiaryStartDate);
               }
            });
            /** END View Description Announce Index **/

            /** START Search Parttime Change EmpShift Diary **/
            $('.txtSearchParttimeChangeDiary').keypress(function (e) {
               var key = e.which;
               if (key == 13) {
                  $('.btnSearchParttimeChangeDiary').click();
                  return false;
               }
            });
            $('.img-loading-change-announce-diary-search-parttime').hide();
            $('.btnSearchParttimeChangeDiary').click(function () {
               if ($.trim($('.txtSearchParttimeChangeDiary').val()) != "") {
                  $('.img-loading-change-announce-diary-search-parttime').show();
                  $.ajax({
                     url: '<%= ResolveUrl("~/websystem/emps/handler/search-parttime.aspx.ashx") %>',
                     dataType: 'json',
                     data: { keyword: $('.txtSearchParttimeChangeDiary').val() },
                     success: function (data) {
                        $('.external-events-listing-change-diary').html('');
                        $('.img-loading-change-announce-diary-search-parttime').hide();
                        var n = 0;
                        var htmlSearchParttimeChangeDiary = '';
                        if (data.length > 0) {
                           $.each(data, function () {
                              htmlSearchParttimeChangeDiary += "<div class='bg-external-event'><div class=\"fc-event\"" +
                                 "data-parttime-idx='" + data[n].m0_parttime_idx + "'" +
                                 "data-title='" + data[n].parttime_code + "'" +
                                 "data-color='" + data[n].parttime_bg_color + "'" +
                                 "data-text-color='" + getContrastTextColor(data[n].parttime_bg_color) + "'" +
                                 "style='color:" + getContrastTextColor(data[n].parttime_bg_color) + ";background-color:" + data[n].parttime_bg_color + ";border: 1px solid " + data[n].parttime_bg_color + "'>" +
                                 "<h4>" + data[n].parttime_code + "</h4>" + data[n].parttime_name_th +
                                 "</div>" +
                                 "</div>";
                              n++;
                           });
                        } else {
                           htmlSearchParttimeChangeDiary += "<div class='text-center text-danger f-s-12'>ไม่พบข้อมูลกะการทำงานที่ค้นหา</div>";
                        }
                        $('.external-events-listing-change-diary').append(htmlSearchParttimeChangeDiary);
                        $('.external-events-change-diary .fc-event').each(function () {
                           $(this).data('event', {
                              parttimeIdx: $(this).attr('data-parttime-idx'),
                              title: $(this).attr('data-title'),
                              color: $(this).attr('data-color'),
                              textColor: $(this).attr('data-text-color'),
                              stick: true
                           });
                           $(this).draggable({
                              zIndex: 999,
                              cursor: 'move',
                              revert: true,
                              revertDuration: 0,
                              containment: '#external-events-dragging-area-change-diary'
                           });
                        });
                     }
                  });
               }
            });
            /** END Search Parttime Change EmpShift **/

            /** START Create change announce onclick use change announce **/
            $('.img-loading-change-announce-diary').hide();
            $('.btnChangeAnnounceDiary').click(function () {
               if (!confirm('คุณต้องการบันทึกใช่หรือไม่')) {
                  return false;
               } else {
                  if ($('.from-date-announce-datepicker-change-diary').val() == '' || $('.to-date-announce-datepicker-change-diary').val() == '') {
                     alert('กรุณาเลือกช่วงวันที่ต้องการประกาศกะ');
                     return false;
                  } else {
                     var clientEventsChangeAnnounceDiary = $('.lblCalendarNewEmpShiftDiary').fullCalendar('clientEvents');
                     if (clientEventsChangeAnnounceDiary.length <= 0) {
                        alert('กรุณาเลือกกะการทำงานลงที่ปฏิทิน');
                        return false;
                     } else {
                        $('.btnChangeAnnounceDiary').hide();
                        $('.img-loading-change-announce-diary').show();
                        var txtChangeAnnounceDiaryStartDate = $('.from-date-announce-datepicker-change-diary').val();
                        var txtChangeAnnounceDiaryEndDate = $('.to-date-announce-datepicker-change-diary').val();
                        var changeAnnounceDiaryStartDateSplit = txtChangeAnnounceDiaryStartDate.split("/");
                        var changeAnnounceDiaryEndDateSplit = txtChangeAnnounceDiaryEndDate.split("/");
                        var changeAnnounceDiaryStartDateInter = changeAnnounceDiaryStartDateSplit[2] + '-' + changeAnnounceDiaryStartDateSplit[1] + '-' + changeAnnounceDiaryStartDateSplit[0];
                        var changeAnnounceDiaryEndDateInter = changeAnnounceDiaryEndDateSplit[2] + '-' + changeAnnounceDiaryEndDateSplit[1] + '-' + changeAnnounceDiaryEndDateSplit[0];
                        $.ajax({
                           url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-change-announce-group-diary.aspx.ashx") %>',
                           dataType: 'json',
                           data: {
                              announceDiaryIdxRef: $('.txtChangeAnnounceDiaryIdx').val(),
                              changeAnnounceDiaryStartDate: changeAnnounceDiaryStartDateInter,
                              changeAnnounceDiaryEndDate: changeAnnounceDiaryEndDateInter,
                              creator: $('.txtEmpIdxLogin').val()
                           },
                           success: function (data) {
                              $.ajax({
                                 url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-announce-group-diary-log.aspx.ashx") %>',
                                 dataType: 'json',
                                 async: false,
                                 data: {
                                    announceGroupDiaryIdx: data[0].announceGroupDiaryIdx,
                                    nodeIdx: 1,
                                    approveLogTypes: 2, //type change announce
                                    creator: $('.txtEmpIdxLogin').val()
                                 }
                              });
                              for (var empDiaryLength = 0; empDiaryLength < empIdxDiaryArr.length; empDiaryLength++) {
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-change-emp-group-diary.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       announceGroupDiaryIdx: data[0].announceGroupDiaryIdx,
                                       empIdxGroup: empIdxDiaryArr[empDiaryLength],
                                       creator: $('.txtEmpIdxLogin').val()
                                    }
                                 });
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/diary/update-emp-status.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       empIdxDiary: empIdxDiaryArr[empDiaryLength],
                                       updator: $('.txtEmpIdxLogin').val()
                                    }
                                 });
                              }
                              for (var lengthNewEventsDiary = 0; lengthNewEventsDiary < clientEventsChangeAnnounceDiary.length; lengthNewEventsDiary++) {
                                 $.ajax({
                                    url: '<%= ResolveUrl("~/websystem/emps/handler/diary/create-change-emps-group-diary.aspx.ashx") %>',
                                    dataType: 'json',
                                    async: false,
                                    data: {
                                       changeAnnounceDiaryIdxRef: data[0].announceGroupDiaryIdx,
                                       dateEvent: clientEventsChangeAnnounceDiary[lengthNewEventsDiary].start.format("YYYY-MM-DD"),
                                       parttimeIdx: clientEventsChangeAnnounceDiary[lengthNewEventsDiary].parttimeIdx
                                    }
                                 });
                              }
                           }
                        });
                        setTimeout(function () {
                           alert('บันทึกการร้องขอเปลี่ยนกะการทำงานเรียบร้อยแล้ว');
                           location.reload();
                        }, 3000);
                     }
                  }
               }
            });
            /** END Create change announce onclick use change announce **/

            /** START Get employee for change announce diary **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emp-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               data: { announceDiaryIdx: $('.txtChangeAnnounceDiaryIdx').val() },
               success: function (data) { 
                  var startTable = '<table class="table table-striped">';
                  var tHeader = '<thead><tr class="f-bold f-s-12"><td>รหัสพนักงาน</td><td>ชื่อ - สกุล</td><td class="text-right text-danger">' + data.length + ' รายการ</td></tr></thead>';
                  var startTBody = '<tbody>';
                  var endTBody = '</tbody>';
                  var endTable = '</table>';
                  var n = 0;
                  var htmlEmpDiary = '';
                  if (data.length > 0) {
                     $.each(data, function () {
                        if (empIdxDiaryArr.length >= 0) {
                           if (empIdxDiaryArr.indexOf(data[n].empIdx) > -1) {
                              var addDisplayNone = "style='display:none'";
                              var removeDisplayNone = "";
                           } else {
                              var addDisplayNone = "";
                              var removeDisplayNone = "style='display:none'";
                           }
                        } else {
                           var addDisplayNone = "";
                           var removeDisplayNone = "style='display:none'";
                        }
                        htmlEmpDiary += "<tr class='f-s-12'>" +
                           "<td>" + data[n].empCode + "<input type='hidden' id='addEmpIdxDiary" + data[n].empIdx + "' value='" + data[n].empIdx + "'>" +
                           "<input type='hidden' id='addEmpCodeDiary" + data[n].empIdx + "' value='" + data[n].empCode + "'></td>" +
                           "<td>" + data[n].fullNameTH + "<input type='hidden' id='addFullNameTHDiary" + data[n].empIdx + "' value='" + data[n].fullNameTH + "'></td>" +
                           "<td class='text-right'><button data-toggle='tooltip' data-placement='left' title='เพิ่ม' type='button'" + addDisplayNone + " id='addEmpDiaryToList" + data[n].empIdx + "' onclick='addEmpDiaryToListFN(" + data[n].empIdx + ");' class='btn btn-success btn-xs'><i class='fa fa-plus'></i></button><button type='button' " + removeDisplayNone + " disabled id='addedDiary" + data[n].empIdx + "' class='btn btn-default btn-xs'><i class='fa fa-check'></i></button></td>" +
                           "</tr>";
                        n++;
                     });
                     $('.emp-old-announce-diary-list').append(startTable + tHeader + startTBody + htmlEmpDiary + endTBody + endTable);
                  } else {
                     htmlEmpDiary += "<tr><td class='text-center text-danger f-s-12' colspan='3'>ไม่พบข้อมูลพนักงานในรายการ</td></tr>";
                     $('.emp-old-announce-diary-list').append(startTable + htmlEmpDiary + endTable);
                  }
               }
            });
            /** END Get employee for change announce diary **/

            /** START Render Events Old Calendar Index Change EmpShift **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceDiaryIdx: $('.announceDiaryIdxIndexDescriptionOfChange').val(),
                  subString: true
               },
               success: function (data) {
                  var announceDiaryOldRangeStart = Date.parse(data[0].announceDiaryStartDate);
                  var announceDiaryOldRangeEnd = Date.parse(data[0].announceDiaryEndDate);
                  $('.lblViewDescriptionIndexOldCalendarDiary').fullCalendar('destroy');
                  $('.lblViewDescriptionIndexOldCalendarDiary').fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblViewDescriptionIndexOldCalendarDiary').fullCalendar('gotoDate', announceDiaryOldRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexOldCalendarDiary').fullCalendar('gotoDate', announceDiaryOldRangeStart);
               }
            });
            /** END Render Events Old Calendar Change EmpShift **/

            /** START Render Events New Calendar Index Change EmpShift **/
            $.ajax({
               url: '<%= ResolveUrl("~/websystem/emps/handler/diary/emps-by-change-announce-diary.aspx.ashx") %>',
               dataType: 'json',
               cache: false,
               async: false,
               data: {
                  announceDiaryIdxRef: $('.announceDiaryIdxIndexDescriptionOfChange').val(),
                  changeAnnounceDiaryIdx: $('.changeAnnounceDiaryIdxIndexDescriptionOfChange').val(),
                  subString: true
               },
               success: function (data) {
                  var announceDiaryNewRangeStart = Date.parse(data[0].changeAnnounceDiaryStartDate);
                  var announceDiaryNewRangeEnd = Date.parse(data[0].changeAnnounceDiaryEndDate);
                  $('.lblViewDescriptionIndexNewCalendarDiary').fullCalendar('destroy');
                  $('.lblViewDescriptionIndexNewCalendarDiary').fullCalendar({
                     dayRender: function (date, cell) {
                        cell.css('background-color', '#fff');
                     },
                     customButtons: {
                        dateRangeOfAnnouncer: {
                           text: 'ช่วงวันที่ประกาศกะ',
                           click: function () {
                              $('.lblViewDescriptionIndexNewCalendarDiary').fullCalendar('gotoDate', announceDiaryNewRangeStart);
                           }
                        }
                     },
                     height: 'auto',
                     header: {
                        left: 'title',
                        right: 'dateRangeOfAnnouncer prev,next',
                     },
                     events: data,
                     eventRender: function (event, element) {
                        element.append("<div class='clearfix'></div>");
                     }
                  });
                  $('.lblViewDescriptionIndexNewCalendarDiary').fullCalendar('gotoDate', announceDiaryNewRangeStart);
               }
            });
            /** END Render Events Old & New Calendar Change EmpShift **/
         } catch (e) {
         }
      });
   </script>
   <!--*** END JS Area  ***-->
</asp:Content>
