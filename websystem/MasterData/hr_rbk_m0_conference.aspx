﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_rbk_m0_conference.aspx.cs" Inherits="websystem_MasterData_hr_rbk_m0_conference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server" ></asp:Literal>
    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFile").MultiFile();
            }
        })
    </script>



    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnAddConference" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มอุปกรณ์" data-toggle="tooltip" title="เพิ่มอุปกรณ์" runat="server"
                    CommandName="cmdAddConference" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มอุปกรณ์</asp:LinkButton>
            </div>

            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                        <InsertItemTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">เพิ่มอุปกรณ์</h3>

                                </div>
                                <div class="panel-body">
                                    <div class="panel-heading">

                                        <div class="form-horizontal" role="form">

                                            <div class="panel-heading">

                                                <div class="form-group">
                                                    <asp:Label ID="lblfood_name" runat="server" Text="ชื่ออุปกรณ์" CssClass="col-sm-3 control-label"></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtcon_name" runat="server" CssClass="form-control" placeholder="กรอกชื่ออุปกรณ์ ..." Enabled="true" />
                                                        <asp:RequiredFieldValidator ID="Re_txtcon_name" runat="server"
                                                            ControlToValidate="txtcon_name" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกชื่ออุปกรณ์" ValidationGroup="Save" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txtcon_name" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtcon_name" Width="220" />
                                                    </div>
                                                    <div class="col-sm-3"></div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" Text="อุปกรณ์สำหรับ" CssClass="col-sm-3 control-label"></asp:Label>
                                                    <div class="col-sm-1">
                                                        <asp:CheckBox ID="chkpic" runat="server" CssClass="checkbox-inline" Text="ภาพ" />

                                                    </div>

                                                    <div class="col-sm-1">
                                                        <asp:CheckBox ID="chksound" runat="server" CssClass="checkbox-inline" Text="เสียง" />


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="จำนวนไมค์" />
                                                    <div class="col-sm-5">
                                                        <asp:TextBox ID="txtqtymic" TextMode="Number" runat="server" CssClass="form-control "></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                            ControlToValidate="txtqtymic" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกชื่ออุปกรณ์" ValidationGroup="Save" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="220" />
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ตัว" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="จำนวนผู้เข้าประชุมที่เหมาะสม" />
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="txtpeople_desc" runat="server" CssClass="form-control "></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                            ControlToValidate="txtpeople_desc" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณากรอกชื่ออุปกรณ์" ValidationGroup="Save" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                                    </div>
                                                </div>


                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label10s" class="col-sm-3 control-label" runat="server" Text="Upload File : " />
                                                            <div class="col-sm-6">
                                                                <asp:FileUpload ID="UploadFile" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                    CssClass="btn btn-sm btn-warning  multi max-1 " accept="jpg" />
                                                                <small>
                                                                    <p class="help-block"><font color="red">**Attach File name jpg,png</font></p>
                                                                </small>
                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnSaveConference" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                                <div class="form-group">
                                                    <asp:Label ID="lblcon_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                    <div class="col-sm-6">
                                                        <asp:DropDownList ID="ddlConStatus" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="1" Text="Online" />
                                                            <asp:ListItem Value="0" Text="Offline" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-3"></div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-3">
                                                        <asp:LinkButton ID="btnSaveConference" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="CmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancelConference" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="CmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:GridView ID="GvConferenceInRoom" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_m0_conidx" runat="server" Visible="false" Text='<%# Eval("m0_conidx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m0_conidx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_conidx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbl_con_name_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่ออุปกรณ์" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_con_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("conference_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Re_txt_con_name_edit" runat="server"
                                                ControlToValidate="txt_con_name_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่ออาหารว่าง" ValidationGroup="Edit" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_con_name_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="อุปกรณ์สำหรับ" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtshow_pic" Visible="false" TextMode="Number" runat="server" CssClass="form-control " Text='<%# Eval("pic_check") %>'> </asp:TextBox>

                                            <asp:CheckBox ID="chkpic_edit" runat="server" CssClass="checkbox-inline" Text="ภาพ" />

                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtshow_sound" Visible="false" TextMode="Number" runat="server" CssClass="form-control " Text='<%# Eval("sound_check") %>'> </asp:TextBox>

                                            <asp:CheckBox ID="chksound_edit" runat="server" CssClass="checkbox-inline" Text="เสียง" />


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="จำนวนไมค์" />
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="txtqtymic" TextMode="Number" runat="server" CssClass="form-control " Text='<%# Eval("qty_mic") %>'> </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="txtqtymic" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่ออุปกรณ์" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="220" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ตัว" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="จำนวนผู้เข้าประชุมที่เหมาะสม" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtpeople_desc" runat="server" CssClass="form-control " Text='<%# Eval("people_desc") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                ControlToValidate="txtpeople_desc" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่ออุปกรณ์" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                        </div>
                                    </div>


                                    <%--       <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Label10as" class="col-sm-3 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFile_Edit" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                        CssClass="btn btn-sm btn-warning  multi max-1 " accept="jpg" />
                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg,png</font></p>
                                                    </small>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveConference" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>

                                    <div class="form-group">
                                        <asp:Label ID="lbresult_use_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlcon_status_edit" Text='<%# Eval("constatus") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit"  OnClientClick="return confirm('คุณต้องการยืนยันแก้ไขรายการนี้ใช่หรือไม่ ?')" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="ภาพอุปกรณ์" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">

                                    <asp:Image ID="image" runat="server"></asp:Image>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_conference_name" runat="server"
                                        Text='<%# Eval("conference_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="อุปกรณ์สำหรับ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">


                                    <div class="col-sm-3">
                                        <asp:Label ID="lbl_pic_check" runat="server" Visible="false"
                                            Text='<%# Eval("pic_check") %>'></asp:Label>
                                        <asp:CheckBox ID="chkpic_show" runat="server" CssClass="checkbox-inline" Text="ภาพ" Enabled="false" />

                                    </div>

                                    <div class="col-sm-3">
                                        <asp:Label ID="lbl_sound_check" runat="server" Visible="false"
                                            Text='<%# Eval("sound_check") %>'></asp:Label>

                                        <asp:CheckBox ID="chksound_show" runat="server" CssClass="checkbox-inline" Text="เสียง" Enabled="false" />


                                    </div>

                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จำนวนไมค์" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_qty_mic" runat="server"
                                        Text='<%# Eval("qty_mic") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จำนวนผู้เข้าประชุมที่เหมาะสม" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_people_desc" runat="server"
                                        Text='<%# Eval("people_desc") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_con_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("constatus") %>'></asp:Label>
                                    <asp:Label ID="con_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="con_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="CmdDelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("m0_conidx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>


</asp:Content>

