﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ad-employee-type.aspx.cs" Inherits="websystem_adonline_employee_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <!-- Start Gridview & Update Form -->
   <div id="divIndex" runat="server">
      <div class="col-md-12">
         <asp:LinkButton ID="btnToInsert" CssClass="btn btn-success pull-right f-s-14" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="Create" />
         <div class="form-inline">
            <div class="form-group">
               <label for="keywordSearch">Keyword :</label>
               <asp:TextBox ID="keywordSearch" runat="server" CssClass="form-control" placeholder="Type name" />
            </div>
            <div class="form-group">
               <label for="keywordSearch">Status :</label>
               <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                  <asp:ListItem Value="99">All</asp:ListItem>
                  <asp:ListItem Value="1">Online</asp:ListItem>
                  <asp:ListItem Value="0">Offline</asp:ListItem>
               </asp:DropDownList>
            </div>
            <div class="form-group">
               <asp:LinkButton ID="btnSearch" runat="server" OnCommand="btnCommand" CommandName="btnSearch" CssClass="btn btn-primary"><i class="fa fa-search"></i></asp:LinkButton>
            </div>
         </div>
         <asp:GridView ID="gvEmpType"
            runat="server"
            AutoGenerateColumns="false"
            DataKeyNames="m0_emp_type_idx"
            CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
            HeaderStyle-CssClass="info"
            AllowPaging="true"
            PageSize="10"
            OnRowEditing="Master_RowEditing"
            OnRowUpdating="Master_RowUpdating"
            OnRowCancelingEdit="Master_RowCancelingEdit"
            OnPageIndexChanging="Master_PageIndexChanging"
            OnRowDataBound="Master_RowDataBound">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">No result</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="10%">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="m0_emp_type_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("m0_emp_type_idx")%>' />
                     <div class="col-md-6">
                        <label>Type name : </label>
                        <asp:TextBox ID="txtEmpTypeNameUpdate" runat="server" CssClass="form-control" Text='<%# Eval("emp_type_name") %>' />
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="pull-left">Status</label>
                           <asp:DropDownList ID="ddlEmpTypeStatusUpdate" AutoPostBack="false" runat="server"
                              CssClass="form-control" SelectedValue='<%# Eval("emp_type_status") %>'>
                              <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                              <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                           </asp:DropDownList>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="pull-left">
                           <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                              ValidationGroup="saveEmpTypeUpdate" CommandName="Update"
                              OnClientClick="return confirm('Are you sure to update this item?')">
                              Save change
                           </asp:LinkButton>
                           <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" CommandName="Cancel">
                              Cancel
                           </asp:LinkButton>
                        </div>
                     </div>
                  </EditItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Type name" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="40%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="employeeTypeName" runat="server" Text='<%# Eval("emp_type_name") %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="employeeTypeStatus" runat="server" Text='<%# getStatus((int)Eval("emp_type_status")) %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit"
                        data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                     <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                        title="Remove" CommandName="btnBan" OnCommand="btnCommand"
                        CommandArgument='<%# Eval("m0_emp_type_idx") %>'
                        OnClientClick="return confirm('Do you want to delete this item?')">
                           <i class="fa fa-trash"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
   </div>
   <!-- End Gridview & Update Form -->

   <!-- Start Insert Form -->
   <div id="divInsert" runat="server">
      <div class="row">
         <div class="col-md-12">
            <div class="form-group">
               <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                  CommandName="btnCancel" OnCommand="btnCommand"><i class="fa fa-angle-left fa-lg"></i> Back</asp:LinkButton>
            </div>
         </div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading">Create Employee Type</div>
               <div class="panel-body">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Type name</label>
                        <asp:TextBox ID="txtEmpTypeName" runat="server" CssClass="form-control"
                           placeholder="Type name..." />
                        <asp:RequiredFieldValidator ID="requiredEmpTypeName"
                           ValidationGroup="savePermType" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtEmpTypeName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="Required!" />
                        <asp:CustomValidator ID="customEmpTypeName"
                           ValidationGroup="saveEmpType" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtEmpTypeName"
                           OnServerValidate="checkExistsEmpTypeName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="This type name is already in Database!" />
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Status</label>
                        <asp:DropDownList ID="ddlEmpTypeStatus" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="Save" ValidationGroup="saveEmpType" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Insert Form -->
</asp:Content>
