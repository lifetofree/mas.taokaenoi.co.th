﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_TrnVideo.aspx.cs" Inherits="websystem_el_TrnVideo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script type="text/javascript"> 


        function openModalTraning() {

            $('#DvUpdpnl_modal_training').modal('show');
        }
        function closeModalTraning() {
            $('#DvUpdpnl_modal_training').modal('hide');
        }

    </script>


    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Menu</a>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>
                        <li id="liInsert" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="สร้างวิดีโอ" />
                        </li>

                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

    </asp:Panel>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_ListDataPag" runat="server">

            <asp:Panel ID="pnlListData" runat="server">

                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>เดือน</label>
                                    <asp:DropDownList ID="ddlMonthSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร</label>
                                    <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร..." />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="row">

                    <asp:GridView ID="GvListData"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        DataKeyNames="u0_course_idx"
                        ShowFooter="false">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="12%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_no") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่เปิดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_date") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("course_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("training_group_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("training_branch_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Priority" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getpriority((int)Eval("course_priority")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ไฟล์วิดีโอ" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("zvideo_item") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatus( (int)Eval("course_status")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <asp:TextBox ID="txtapprove_status_GvListData" runat="server" Visible="false" Text='<%# Eval("approve_status") %>' />
                                    <asp:TextBox ID="txtcourse_created_by_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("course_created_by") %>' />

                                    <asp:LinkButton ID="btnDetail_GvListData"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail" CommandArgument='<%# Eval("u0_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                        data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                        ID="btnDelete_GvListData"
                                        Visible="false"
                                        CommandName="btnDelete" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_course_idx") %>'
                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                    <i class="fa fa-trash"></i>
                                    </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>

        <asp:View ID="View_Insert" runat="server">

             <asp:UpdatePanel ID="UpdatePnl_Insert" runat="server"
                UpdateMode="Conditional">
                <ContentTemplate>

            <asp:FormView ID="fvCRUD" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลวิดีโอ</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label4" class="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txttraining_plan_year" runat="server"
                                                    CssClass="form-control"
                                                    Text=""
                                                    Enabled="false"
                                                    Visible="false" />
                                                <asp:TextBox ID="txtcourse_no"
                                                    runat="server" CssClass="form-control" Enabled="false"
                                                    Text="" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right"
                                                    runat="server" Text="" />
                                                <div class="col-md-6">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="Panel5" runat="server" Visible="false">

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlu0_course_idx_ref" runat="server"
                                                    AutoPostBack="true"
                                                    OnSelectedIndexChanged="FvDetail_DataBound"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlu0_course_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    InitialValue="0"
                                                    ErrorMessage="กรุณากรอกชื่อหลักสูตร" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_group_name" runat="server"
                                                        Enabled="false"
                                                        CssClass="form-control">
                                                    </asp:TextBox>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="หัวข้อ :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtvideo_title" runat="server"
                                                    CssClass="form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="txtvideo_title"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกหัวข้อ" />
                                            </div>

                                            <div class="col-md-6">
                                                <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txttraining_branch_name" runat="server"
                                                        Enabled="false"
                                                        CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtvideo_description" runat="server" CssClass="form-control"
                                                    TextMode="MultiLine" MaxLength="250"
                                                    Rows="9"
                                                    Text='<%# Eval("video_description") %>' />
                                            </div>
                                            <div class="col-md-6">

                                                <div class="row">
                                                </div>

                                                <div class="row">
                                                    <div class="form-group" style="padding: 0px"></div>
                                                </div>

                                                <div class="row">
                                                    <asp:Label ID="Label3" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="วันที่เปิดหลักสูตร :" />
                                                    <div class="col-md-6">

                                                        <asp:TextBox ID="txtcourse_date" runat="server"
                                                            CssClass="form-control filter-order-from"
                                                            Enabled="false"
                                                            Text='<%# Eval("course_date") %>' />
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="padding: 0px"></div>
                                                    </div>

                                                    <div class="row">
                                                        <asp:Label ID="Label8" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtlevel_code" runat="server"
                                                                Enabled="false"
                                                                CssClass="form-control">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="padding: 0px"></div>
                                                    </div>

                                                    <div class="row">
                                                        <asp:Label ID="Label9" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtcourse_score" runat="server"
                                                                CssClass="form-control"
                                                                TextMode="Number"
                                                                Enabled="false"
                                                                Text='<%# Eval("course_score") %>'>
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="padding: 0px"></div>
                                                    </div>
                                                    <div class="row">
                                                        <asp:Label ID="Label11" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtscore_through_per" runat="server"
                                                                CssClass="form-control"
                                                                TextMode="Number"
                                                                Text='<%# Eval("score_through_per") %>'>
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลำดับ :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txtvideo_item" runat="server"
                                                        CssClass="form-control" TextMode="Number" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtvideo_item"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกลำดับ" />
                                                </div>

                                                <div class="col-md-6">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ไฟล์วิดีโอ :" />
                                                <div class="col-md-6">

                                                    <asp:FileUpload ID="fldvideo_name"
                                                        ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                        runat="server" CssClass="control-label multi max-1 accept-mp4|mp3|wmv with-preview"></asp:FileUpload>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="fldvideo_name"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณาเลือกไฟล์วิดีโอ" />
                                                </div>

                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="รูปประกอบ :" />
                                                <div class="col-md-6">

                                                    <asp:FileUpload ID="fldvideo_images"
                                                        ViewStateMode="Enabled" AutoPostBack="true"
                                                        Font-Size="small" ClientIDMode="Static" runat="server"
                                                        CssClass="control-label multi max-1 accept-png|jpg|gif|jpeg with-preview"></asp:FileUpload>

                                                </div>

                                                <div class="col-md-4">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel4" runat="server" Visible="true">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlcourse_status" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="1" Text="ใช้งาน" />
                                                            <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                   
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>


            </asp:FormView>

            <%-- End Select --%>

            <asp:Panel ID="pnlSave" runat="server">

                <div class="row">

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-7">


                                <asp:LinkButton CssClass="btn btn-success" runat="server"
                                    CommandName="btnSaveInsert" OnCommand="btnCommand"
                                    Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                    data-toggle="tooltip" title="บันทึก"
                                    ValidationGroup="btnSaveInsert"
                                    ID="btnSaveInsert" />

                                <asp:LinkButton CssClass="btn btn-danger"
                                    data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                    ID="btnCancel"
                                    CommandName="btnCancel" OnCommand="btnCommand" />


                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
              </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveInsert" />

                </Triggers>
            </asp:UpdatePanel>

            <br />
            
        </asp:View>

        <%-- edit --%>

        <asp:View ID="View_Update" runat="server">
            <asp:UpdatePanel ID="UpdatePnl_Update" runat="server"
                UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FormView ID="fv_Update" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>


                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูลวิดีโอ</strong></h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label4" class="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txttraining_plan_year" runat="server"
                                                            CssClass="form-control"
                                                            Text=""
                                                            Enabled="false"
                                                            Visible="false" />
                                                        <asp:TextBox ID="txtcourse_no"
                                                            runat="server" CssClass="form-control" Enabled="false"
                                                            Text='<%# Eval("course_no") %>' />
                                                    </div>

                                                    <div class="col-md-6">
                                                        <asp:Label ID="Label5" CssClass="col-md-4 control-labelnotop text_right"
                                                            runat="server" Text="" />
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="Panel5" runat="server" Visible="false">

                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                        <div class="col-md-4">
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox
                                                            ID="txtu0_course_idx_ref" runat="server"
                                                            CssClass="form-control"
                                                            Visible="false"
                                                            Text='<%# Eval("u0_course_idx_ref") %>' />
                                                        <asp:TextBox
                                                            ID="ddlu0_course_idx_ref" runat="server"
                                                            CssClass="form-control"
                                                            Enabled="false"
                                                            Text='<%# Eval("course_name") %>' />
                                                    </div>

                                                    <div class="col-md-6">
                                                        <asp:Label ID="Label1" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา : " />
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txttraining_group_name" runat="server"
                                                                Enabled="false"
                                                                CssClass="form-control"
                                                                Text='<%# Eval("training_group_name") %>'>
                                                            </asp:TextBox>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="หัวข้อ :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtvideo_title" runat="server"
                                                            CssClass="form-control"
                                                            Text='<%# Eval("video_title") %>' />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                            ValidationGroup="btnSaveUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtvideo_title"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกหัวข้อ" />
                                                    </div>

                                                    <div class="col-md-6">
                                                        <asp:Label ID="Label6" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="สาขาวิชา :" />
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txttraining_branch_name" runat="server"
                                                                Enabled="false"
                                                                Text='<%# Eval("training_branch_name") %>'
                                                                CssClass="form-control">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtvideo_description" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" MaxLength="250"
                                                            Rows="9"
                                                            Text='<%# Eval("video_description") %>' />
                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="row">
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group" style="padding: 0px"></div>
                                                        </div>

                                                        <div class="row">
                                                            <asp:Label ID="Label3" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="วันที่เปิดหลักสูตร :" />
                                                            <div class="col-md-6">

                                                                <asp:TextBox ID="txtcourse_date" runat="server"
                                                                    CssClass="form-control filter-order-from"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("course_date") %>' />
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group" style="padding: 0px"></div>
                                                            </div>

                                                            <div class="row">
                                                                <asp:Label ID="Label8" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="ระดับความยากของหลักสูตร :" />
                                                                <div class="col-md-6">
                                                                    <asp:TextBox ID="txtlevel_code" runat="server"
                                                                        Enabled="false"
                                                                        CssClass="form-control"
                                                                        Text='<%# Eval("level_code") %>'>
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group" style="padding: 0px"></div>
                                                            </div>

                                                            <div class="row">
                                                                <asp:Label ID="Label9" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />
                                                                <div class="col-md-6">
                                                                    <asp:TextBox ID="txtcourse_score" runat="server"
                                                                        CssClass="form-control"
                                                                        TextMode="Number"
                                                                        Enabled="false"
                                                                        Text='<%# Eval("course_score") %>'>
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-group" style="padding: 0px"></div>
                                                            </div>
                                                            <div class="row">
                                                                <asp:Label ID="Label11" CssClass="col-md-4 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />
                                                                <div class="col-md-6">
                                                                    <asp:TextBox ID="txtscore_through_per" runat="server"
                                                                        CssClass="form-control"
                                                                        TextMode="Number"
                                                                        Text='<%# Eval("score_through_per") %>'>
                                                                    </asp:TextBox>
                                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                                        Display="Dynamic"
                                                                        SetFocusOnError="true"
                                                                        ControlToValidate="txtscore_through_per"
                                                                        Font-Size="1em" ForeColor="Red"
                                                                        CssClass="pull-left"
                                                                        ErrorMessage="กรุณากรอกคะแนนผ่าน %" />--%>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลำดับ :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtvideo_item" runat="server"
                                                                CssClass="form-control"
                                                                Enabled="false"
                                                                Text='<%# Eval("video_item") %>' />
                                                        </div>

                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ไฟล์วิดีโอ :" />
                                                        <div class="col-md-4">

                                                            <asp:TextBox ID="txthddfld_video_name" runat="server"
                                                                Visible="false"
                                                                Text='<%# Eval("video_name") %>' />

                                                            <%-- <asp:DropDownList ID="ddlvideo_name"
                                                                runat="server" CssClass="form-control col-md-4"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="FvDetail_DataBound">
                                                                <asp:ListItem Value="0" Text="ไม่เปลี่ยน" Selected="True" />
                                                                <asp:ListItem Value="1" Text="เปลี่ยน" />
                                                            </asp:DropDownList>--%>

                                                            <asp:FileUpload ID="fldvideo_name"
                                                                ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                                runat="server" CssClass="control-label multi max-1 accept-mp4|mp3|wmv with-preview"></asp:FileUpload>

                                                            <video style="" height="150" width="250"
                                                                poster='<%# getImgUrl((string)Eval("course_no"),(string)Eval("video_images")) %>' controls>
                                                                <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/mp4">
                                                                <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/ogg">
                                                                <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/wmv">
                                                                Your browser does not support the video tag.
                                                            </video>

                                                            <%--<label>
                                                                <asp:Label ID="lbRequiredfldvideo_name" runat="server" Text="กรุณากรอกไฟล์วิดีโอ" Visible="false"
                                                                    Font-Size="11px" ForeColor="Red"></asp:Label>
                                                            </label>--%>
                                                        </div>

                                                        <div class="col-md-4">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="รูปประกอบ :" />
                                                        <div class="col-md-4">

                                                            <asp:TextBox ID="txthddfld_video_images" runat="server"
                                                                Visible="false"
                                                                Text='<%# Eval("video_images") %>' />

                                                            <%--<asp:DropDownList ID="ddlvideo_images"
                                                                runat="server" CssClass="form-control col-md-4"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="FvDetail_DataBound">
                                                                <asp:ListItem Value="0" Text="ไม่เปลี่ยน" />
                                                                <asp:ListItem Value="1" Text="เปลี่ยน" Selected="True" />
                                                            </asp:DropDownList>--%>

                                                            <asp:FileUpload ID="fldvideo_images"
                                                                ViewStateMode="Enabled" AutoPostBack="true"
                                                                Font-Size="small" ClientIDMode="Static" runat="server"
                                                                CssClass="control-label multi max-1 accept-png|jpg|gif|jpeg with-preview"></asp:FileUpload>

                                                            <asp:Image ID="imgStoreList1" runat="server"
                                                                ImageUrl='<%# getImgUrl((string)Eval("course_no"),(string)Eval("video_images")) %>'
                                                                CommandName="btnToAddSO" OnCommand="btnCommand"
                                                                CommandArgument='<%# Eval("u6_course_idx") %>'
                                                                Height="150" Width="250" />

                                                        </div>

                                                        <div class="col-md-4">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Panel ID="Panel4" runat="server" Visible="true">

                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlcourse_status" runat="server"
                                                                    CssClass="form-control"
                                                                    SelectedValue='<%# Eval("course_status") %>'>
                                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- End  --%>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>


                    </asp:FormView>

                    <%-- End Select --%>

                    <asp:Panel ID="Panel1" runat="server">


                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-7">

                                        <asp:LinkButton CssClass="btn btn-success" runat="server"
                                            CommandName="btnSaveUpdate" OnCommand="btnCommand"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึกการเปลี่ยนแปลง"
                                            data-toggle="tooltip" title="บันทึกการเปลี่ยนแปลง"
                                            ID="btnSaveUpdate"
                                            ValidationGroup="btnSaveUpdate" />

                                        <asp:LinkButton CssClass="btn btn-danger"
                                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                            ID="btnUpdateCancel"
                                            CommandName="btnUpdateCancel" OnCommand="btnCommand" />

                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="Server" AssociatedUpdatePanelID="UpdatePnl_Update" DisplayAfter="1">
                <ProgressTemplate>
                    <div>
                        <img src="loading.gif" />&nbsp;Please wait ...
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <br />


        </asp:View>


        <asp:View ID="viewList" runat="server">

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <h4>
                            <asp:Label ID="ltTitle" CssClass="text-justify" runat="server" Text="Title"></asp:Label>
                        </h4>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <asp:LinkButton ID="btnToInsertDetail" CssClass="btn btn-primary pull-left" runat="server"
                            data-toggle="tooltip" title="เพิ่ม"
                            CommandName="btnToInsertDetail" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> เพิ่ม</asp:LinkButton>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="row menu-list">
                <asp:Repeater ID="rptList" runat="server"
                    OnItemDataBound="rptItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-4 menu-list-row">
                            <figure class="imghvr-slide-up">
                                <asp:Panel ID="Panel5" runat="server">
                                    <div class="menu-list-box" style="border: 3px solid #D9B4B4; height: 340px; width: 347px;">

                                        <video style="border: 3px solid #FFFAFA;" height="250" width="340"
                                            poster='<%# getImgUrl((string)Eval("course_no"),(string)Eval("video_images")) %>' controls>
                                            <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/mp4">
                                            <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/ogg">
                                            <source src='<%# getVideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>' type="video/wmv">
                                            Your browser does not support the video tag.
                                        </video>

                                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# getImgUrl((string)Eval("course_no"),(string)Eval("video_images")) %>'></asp:Literal>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# getvideoUrl((string)Eval("course_no"),(string)Eval("video_name")) %>'></asp:Literal>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="font-16 font-red">
                                                    <asp:Literal ID="litStoreNameTh" runat="server" Text='<%# Truncate("V "+ Eval("video_item")  +" หัวข้อ "+Eval("video_title"),43) %>'></asp:Literal>

                                                </span>

                                            </div>
                                            <div class="col-md-12">
                                                <span class="font-16 font-red">
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Truncate("รายละเอียด : "+Eval("video_description"),43) %>'></asp:Literal>

                                                </span>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-7 text-left">
                                                <span class="font-16">สถานะ
                                                    <asp:Literal ID="litStorePrice" runat="server"
                                                        Text='<%# getStatusName( (int)Eval("course_status")) %>'>
                                                    </asp:Literal></span>
                                            </div>
                                            <div class="col-md-5 text-right">


                                                <asp:LinkButton ID="btnUpdateDetail" runat="server"
                                                    CssClass="btn btn-warning btn-sm"
                                                    data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                                    OnCommand="btnCommand" CommandName="btnUpdateDetail"
                                                    CommandArgument='<%# Eval("u6_course_idx") %>'>
                                                    <i class="fa fa-pencil-alt"></i>
                                                </asp:LinkButton>


                                                <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                                    data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                    ID="btnDeleteDetail"
                                                    CommandName="btnDeleteDetail" OnCommand="btnCommand"
                                                    CommandArgument='<%# 
                                                    Convert.ToString(Eval("u6_course_idx")) + "|" +  
                                                    Convert.ToString(Eval("course_no")) + "|" +  
                                                    Convert.ToString(Eval("video_name")) + "|" +  
                                                    Convert.ToString(Eval("video_images")) 
                                                    %>'
                                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                                    <i class="fa fa-trash"></i>
                                                </asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>
                            </figure>
                            </p>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>

            </div>

            <div class="row">

                <div class="form-group">
                    <div class="col-md-12">

                        <asp:LinkButton CssClass="btn btn-danger"
                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                            ID="btnCancelDetail"
                            CommandName="btnCancel" OnCommand="btnCommand" />

                    </div>
                </div>
            </div>
            <br />
        </asp:View>


    </asp:MultiView>


    <%-- end search needs --%>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_course_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_course_idx_ref" runat="server" />
    <asp:HiddenField ID="Hddfld_u6_course_idx" runat="server" />
    <style type="text/css">
        ord_p1 {
            word-w word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>


    <script runat="server">
        //IsImage = true = it's an image
        //IsImage = true = it's a video
        //assumption = it's always either an image or a video
        private bool IsImage(string filename)
        {
            bool check = false;
            filename = filename.ToLower();
            if (filename.EndsWith(".png") || filename.EndsWith(".jpg") || filename.EndsWith(".png"))
                check = true;
            else if (filename.EndsWith(".avi") || filename.EndsWith(".mp4"))
                check = false;

            return check;
        }
    </script>
    <script type="text/javascript"> 

        function DivClicked(u0id) {

            var btnHidden = $('.btndiv' + u0id);
            btnHidden.click();
        }


        function openModalRePrint() {
            $('#DvRemarkRePrint').modal('show');

        }

        function openModalUpdateCustomer() {
            $('#DvUpdateCustomer').modal('show');

        }

        function openModal1() {
            $('#ordine1').modal('show');

        }

    </script>

    <asp:HiddenField ID="HiddenField1" runat="server" />
</asp:Content>

