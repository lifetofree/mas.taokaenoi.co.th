<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_fileinterface.aspx.cs" Inherits="websystem_ecommerce_ecom_fileinterface" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .table > thead > tr > td.success,
        .table > tbody > tr > td.success,
        .table > tfoot > tr > td.success,
        .table > thead > tr > th.success,
        .table > tbody > tr > th.success,
        .table > tfoot > tr > th.success,
        .table > thead > tr.success > td,
        .table > tbody > tr.success > td,
        .table > tfoot > tr.success > td,
        .table > thead > tr.success > th,
        .table > tbody > tr.success > th,
        .table > tfoot > tr.success > th {
            white-space: nowrap;
        }

        .btn-fif {
            padding-left: 3px;
            font-size: 16px;
        }

            .btn-fif.success {
                color: #5cb85c;
            }

            .btn-fif.danger {
                color: #d9534f;
            }


        /*///////////////////////////////
         file interface          
///////////////////////////////*/

        .table > thead > tr > td.success,
        .table > tbody > tr > td.success,
        .table > tfoot > tr > td.success,
        .table > thead > tr > th.success,
        .table > tbody > tr > th.success,
        .table > tfoot > tr > th.success,
        .table > thead > tr.success > td,
        .table > tbody > tr.success > td,
        .table > tfoot > tr.success > td,
        .table > thead > tr.success > th,
        .table > tbody > tr.success > th,
        .table > tfoot > tr.success > th {
            /*white-space: nowrap;*/
        }

        table.GvExcel_Show tbody tr td {
            white-space: nowrap;
        }

            table.GvExcel_Show tbody tr td:nth-child(10) /* Address */ {
                min-width: 180px;
                white-space: unset;
            }

            table.GvExcel_Show tbody tr td:nth-child(1) /* BOM */ {
                min-width: 180px;
                white-space: unset;
            }

            table.GvExcel_Show tbody tr td:nth-child(1), table.GvExcel_Show tbody tr th:nth-child(1) {
                position: sticky;
                left: 0;
                -webkit-box-shadow: 1px 0px 0px 0px #dddddd,-1px 0px 0px 0px #dddddd;
                -moz-box-shadow: 1px 0px 0px 0px #dddddd,-1px 0px 0px 0px #dddddd;
                box-shadow: 1px 0px 0px 0px #dddddd,-1px 0px 0px 0px #dddddd;
            }

        table.GvExcel_Show tbody tr:nth-child(even) td:nth-child(1) {
            background-color: #fff;
        }

        table.GvExcel_Show tbody tr:nth-child(odd) td:nth-child(1) {
            background-color: #f9f9f9;
        }

        table.Gv_select_interface tbody tr td {
            white-space: nowrap;
        }

            table.Gv_select_interface tbody tr td:nth-child(15) /* ชื่อสินค้า */ {
                min-width: 180px;
                white-space: unset;
            }

            table.Gv_select_interface tbody tr td:nth-child(43) /* ที่อยู่จัดส่ง */ {
                min-width: 180px;
                white-space: unset;
            }

        table.GV_ecom_u0 > thead > tr > th, .GV_ecom_u0.table > tbody > tr > th, .GV_ecom_u0.table > tfoot > tr > th,
        table.GV_ecom_u0 > thead > tr > td, .GV_ecom_u0.table > tbody > tr > td,
        table.GV_ecom_u0 > tfoot > tr > td {
            vertical-align: middle;
        }

        table.Gv_data_interface_from_base tbody tr td {
            white-space: nowrap;
        }

            table.Gv_data_interface_from_base tbody tr td:nth-child(10) /* ชื่อลูกค้า */ {
                min-width: 180px;
                white-space: unset;
            }

            table.Gv_data_interface_from_base tbody tr td:nth-child(11) /* ที่อยู่ */ {
                min-width: 180px;
                white-space: unset;
            }

        input[type="file"] {
            white-space: nowrap;
            width: 100%;
        }

       

     /*   .row {
            margin-left: -15px !important;
            margin-right: -15px !important;
        }
         .main .row:nth-child(1) {
            padding: 0px;
            margin: 0px !important;
        }*/
    </style>
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>

                </div>
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="Ul1" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="LinkButton0" runat="server" CommandName="cmd_ifif" OnCommand="navCommand" CommandArgument="view_ifif">Import File Interface</asp:LinkButton>
                        </li>


                        <li id="li1" runat="server">
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="cmd_tnkso" OnCommand="navCommand" CommandArgument="view_tnkso">TKN ShopOnline</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="cmd_listfile" OnCommand="navCommand" CommandArgument="view_listfile">List Fileinterface</asp:LinkButton>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="mvSystem" runat="server">






        <asp:View ID="view_ifif" runat="server">

            <div class="col-sm-12">
                <div class="row">
                    <asp:FormView ID="FvAddImport" runat="server" Width="100%" DefaultMode="Insert" Visible="true">
                        <InsertItemTemplate>
                            <div class="col-md-offset-2 col-md-10">

                                <asp:UpdatePanel ID="upActor1Node1Files11" runat="server">
                                    <ContentTemplate>



                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-3 col-md-2 control-label">Channel</label>
                                                <div class="col-sm-9 col-md-3 ">
                                                    <asp:DropDownList ID="m0_channel" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_m0_channel" runat="server"
                                                        ControlToValidate="m0_channel" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือก ข่องทางขาย" Display="None" ValidationGroup="vali_importfile" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_m0_channel" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator_m0_channel" Width="220" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-sm-3 col-md-2  control-label">แนบไฟล์ Excel</label>
                                                <div class="col-sm-9 col-md-5">
                                                    <asp:FileUpload ID="upload" runat="server" AutoPostBack="true" Enabled="true" />
                                                    <asp:RegularExpressionValidator
                                                        runat="server"
                                                        ID="rvaupload"
                                                        ControlToValidate="upload"
                                                        ErrorMessage="Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls) เท่านั้น"
                                                        Display="None"
                                                        ValidationExpression="^.*\.([x|X][l|L][s|S])$" ValidationGroup="vali_importfile" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorplace_upload" runat="server"
                                                        ControlToValidate="upload" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาแนบไฟล์" Display="None" ValidationGroup="vali_importfile" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_rvaupload" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="rvaupload" Width="220" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorplace_upload" Width="220" />

                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnImport" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="upActor1Node1Files1" runat="server">
                                    <ContentTemplate>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label class=" control-label"></label>
                                                </div>

                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="btnImport" runat="server" Text="Import" ValidationGroup="vali_importfile" OnCommand="btnCommand" CommandName="btnImport" CssClass="btn btn-primary" />

                                                    <asp:LinkButton runat="server" ID="btnsavedatafile" Text="Save" OnCommand="btnCommand" CommandName="btnfiletobase" CssClass="btn btn-success"></asp:LinkButton>
                                                    <%--                                                    <asp:LinkButton runat="server" ID="btnExporttosap" Text="Export To SAP" OnCommand="btnCommand" CommandName="btntoSAP" CssClass="btn btn-success"></asp:LinkButton>--%>


                                                    <asp:LinkButton ID="lbDocCancelExcel" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel" CssClass="btn btn-danger" />
                                                </div>
                                            </div>


                                        </div>
                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnImport" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </div>




                            <asp:Panel ID="Save_Excel" runat="server" Visible="false">
                                <div class="form-group" id="div_saveinsert" runat="server">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="btnSaveFileExcel" runat="server" Text="Sagve" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSaveFileExcel" CssClass="btn btn-success" />
                                        <asp:LinkButton ID="lbDocCancelFileExcel" runat="server" Text="Cancel" OnCommand="btnCommand" data-original-title="Cancel" data-toggle="tooltip" CommandName="cmdDocCancel" CssClass="btn btn-danger" />
                                    </div>
                                </div>
                            </asp:Panel>


                        </InsertItemTemplate>
                    </asp:FormView>
                    <div style="max-width: 100%; overflow: auto;">

                        <asp:GridView ID="GvExcel_Show" runat="server" Visible="true" AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-responsive footable col-md-12 GvExcel_Show"
                            HeaderStyle-CssClass="success"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            AutoPostBack="FALSE">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                            <EmptyDataTemplate>
                                <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="BOM" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbOrder_type" runat="server"
                                                    Text='<%# Eval("BOM") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Type" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbOrder_type" runat="server"
                                                    Text='<%# Eval("Order_type") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sale Org." ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbSale_Org" runat="server"
                                                    Text='<%# Eval("Sale_Org") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Channel" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbChannel" runat="server"
                                                    Text='<%# Eval("Channel") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Division" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDivision" runat="server"
                                                    Text='<%# Eval("Division") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales office" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbSales_office" runat="server"
                                                    Text='<%# Eval("Sales_office") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sales group" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbSales_group" runat="server"
                                                    Text='<%# Eval("Sales_group") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer No." ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbCustomer_No" runat="server"
                                                    Text='<%# Eval("Customer_No") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbName" runat="server"
                                                    Text='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Address" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbAddress" runat="server"
                                                    Text='<%# Eval("Address") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Honse number" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbHonse_number" runat="server"
                                                    Text='<%# Eval("Honse_number") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Postal Code" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPostal_Code" runat="server"
                                                    Text='<%# Eval("Postal_Code") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="District" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDistrict" runat="server"
                                                    Text='<%# Eval("District") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="City" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbCitye" runat="server"
                                                    Text='<%# Eval("City") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbCountry" runat="server"
                                                    Text='<%# Eval("Country") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tel" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbTel" runat="server"
                                                    Text='<%# Eval("Tel") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tax number" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbTax_number" runat="server"
                                                    Text='<%# Eval("Tax_number") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PO number" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPO_number" runat="server"
                                                    Text='<%# Eval("PO_number") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delivery Date" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDelivery_Date" runat="server"
                                                    Text='<%# Eval("Delivery_Date") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Head Discount" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDiscount_1" runat="server"
                                                    Text='<%# Eval("Discount_1") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbItem" runat="server"
                                                    Text='<%# Eval("Item") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Material" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbMaterial" runat="server"
                                                    Text='<%# Eval("Material") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbQuantity" runat="server"
                                                    Text='<%# Eval("Quantity") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Un (Sale Unit)" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbusu" runat="server"
                                                    Text='<%# Eval("usu") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Plant" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPlant" runat="server"
                                                    Text='<%# Eval("Plant") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price/Unit" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPrice_Unit" runat="server"
                                                    Text='<%# Eval("Price_Unit") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Currency" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbCurrency" runat="server"
                                                    Text='<%# Eval("Currency") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DiscountType" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDiscountType" runat="server"
                                                    Text='<%# Eval("DiscountType") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DiscountAmount" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbDiscountAmount" runat="server"
                                                    Text='<%# Eval("DiscountAmount") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PromotionID" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPromotionID" runat="server"
                                                    Text='<%# Eval("PromotionID") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PromotionDesc" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbPromotionDesc" runat="server"
                                                    Text='<%# Eval("PromotionDesc") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NetPriceAmount" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbNetPriceAmount" runat="server"
                                                    Text='<%# Eval("NetPriceAmount") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="VATAmount" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbVATAmount" runat="server"
                                                    Text='<%# Eval("VATAmount") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item cate" ItemStyle-HorizontalAlign="left"
                                    HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>
                                        <small>
                                            <div style="padding-top: 5px; text-align: center;">
                                                <asp:Label ID="lbItem_cate" runat="server"
                                                    Text='<%# Eval("Item_cate") %>'></asp:Label>
                                            </div>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>
        </asp:View>
        <asp:View ID="view_tnkso" runat="server">
            <style>
                @media (min-width: 1200px) {
                    .col-lg-1_5 {
                        width: 12.66666667%;
                    }
                }
            </style>
            <div class="col-md-12">
                <div class=" form-group">
                    <div class="col-md-offset-2">


                        <%--<div class="form-horizontal" role="form">--%>
                        <div class="form-group">
                            <div class=" col-md-9">
                                <div class="form-group">
                                    <div style="margin-top: 7px;">
                                        <label class="col-sm-3 col-md-2  col-lg-2 control-label">ตั้งแต่วันที่</label>
                                        <div class="col-sm-6 col-md-4 col-lg-3" style="">
                                            <asp:TextBox ID="tbDateStart" runat="server" CssClass="form-control date-datepicker-date start-date" MaxLength="20" autocomplete="off">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorplace_tbDateStart" runat="server"
                                            ControlToValidate="tbDateStart" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกวันที่" Display="None" ValidationGroup="vali_selectdate" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_tbDateStart" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorplace_tbDateStart" Width="220" />
                                        <div class="col-sm-3 visible-sm-block" style="height: 34px;"></div>
                                    </div>

                                    <div style="margin-top: 7px;">
                                        <label class="col-sm-3 col-md-2  col-lg-col-lg-1_5 control-label text-right">ถึงวันที่</label>


                                        <div class="col-sm-6 col-md-4 col-lg-3">
                                            <asp:TextBox ID="tbDateEnd" runat="server" CssClass="form-control date-datepicker-date end-date" MaxLength="20" autocomplete="off">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                         <div class="clearfix"></div>
                        <%--  
                </div>--%>
                        <div class="form-group" style="margin-top: 7px;">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-6 ">
                                        <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary"
                                            OnCommand="btnCommand" CommandName="cmdSearch" ValidationGroup="vali_selectdate"><i class="fas fa-search"></i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-default"
                                            OnCommand="btnCommand" CommandName="cmdReset"><i class="fas fa-sync-alt"></i>
                                        </asp:LinkButton>
                                        <asp:UpdatePanel ID="updatepanelbutton" runat="server" style="display: inline-block;">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lbExport" runat="server" CssClass="btn btn-success"
                                                    OnCommand="btnCommand" CommandName="cmdExport" title="export excel" data-toggle="tooltip"><i class="far fa-file-excel"></i>
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>

                                                <asp:PostBackTrigger ControlID="lbExport" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">


                <div style="max-width: 100%; overflow: auto;">
                    <asp:GridView ID="Gv_select_interface" runat="server" Visible="true" AutoGenerateColumns="true"
                        CssClass="table table-striped table-bordered table-responsive footable col-md-12 Gv_select_interface"
                        HeaderStyle-CssClass="success"
                        AllowPaging="true"
                        PageSize="10"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        AutoPostBack="FALSE">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                        </EmptyDataTemplate>

                    </asp:GridView>
                </div>
            </div>
        </asp:View>


        <asp:View ID="view_listfile" runat="server">
            <div class="col-sm-12">



                <%--OnRowDataBound="Master_RowDataBound"--%>
                <asp:GridView ID="GV_ecom_u0" runat="server" Visible="true" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-md-12 GV_ecom_u0"
                    HeaderStyle-CssClass="success"
                    AllowPaging="true"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound"
                    PageSize="10"
                    AutoPostBack="FALSE">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <asp:Label ID="GV_ecom_u0_Index_no" runat="server" Visible="false" />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </small>

                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อไฟล์ ที่ Import" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style=" text-align: left;">
                                        <asp:Label ID="lbfile_name_import" runat="server"
                                            Text='<%# Eval("file_name_import") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ช่องทางขาย" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style=" text-align: left;">
                                        <asp:Label ID="lbchannel_name" runat="server"
                                            Text='<%# Eval("channel_name") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อผู้สร้าง" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style=" text-align: left;">
                                        <asp:Label ID="lbemp_name_import" runat="server"
                                            Text='<%# Eval("emp_name_import") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อผู้กดบันทึก SAP" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: left;">
                                        <asp:Label ID="lbemp_name_exsap" runat="server"
                                            Text='<%# Eval("emp_name_exsap") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: left;">
                                        <asp:Label ID="lbcreate_date" runat="server"
                                            Text='<%#  Eval("create_date")  %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่แก้ไข" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style=" text-align: left;">
                                        <asp:Label ID="lbupdate_date" runat="server"
                                            Text='<%#  Eval("update_date")  %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center;">
                                    <asp:LinkButton ID="view_detail" CssClass=" btn btn-primary btn-sm " runat="server" CommandName="btn_view_temp" CommandArgument='<%#Eval("u0_fileinterface_idx") %>'
                                        data-toggle="tooltip" OnCommand="btnCommand" title="ดูรายละเอียด">
                                        <i class="fa fa-list" aria-hidden="true"></i></asp:LinkButton>


                                    <div id="group_edit_GV_ecom_u0" runat="server" class="" style='<%# Eval("export_sap_status").ToString() == "0" ? "display: inline-block;": "display: None;" %>'>
                                        <asp:LinkButton ID="saveToSAP" CssClass=" btn btn-success btn-sm" runat="server" CommandName="btn_saveToSAP"
                                            data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%#Eval("u0_fileinterface_idx") %>' OnClientClick="return confirm('คุณต้องการบันทึกข้อมูลนี้ลง SAP ใช่หรือไม่ ?')" title="บันทึกลง SAP">
                                        <i class="fa fa-save" ></i></asp:LinkButton>
                                        <asp:LinkButton ID="btnTodelete" CssClass=" btn btn-danger btn-sm" runat="server" CommandName="btnTodelete_temp"
                                            data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                            CommandArgument='<%#Eval("u0_fileinterface_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                    </div>


                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>
            </div>
            <div id="div_detail" class="col-sm-12" runat="server">



                <asp:LinkButton ID="btn_backToGv" CssClass="btn btn-primary" runat="server" CommandName="btn_backToGv" Style="margin-bottom: 10px"
                    data-toggle="tooltip" OnCommand="btnCommand" title="ย้อนกลับ">
                    <i class="glyphicon glyphicon-chevron-left"></i> ย้อนกลับ
                    
                </asp:LinkButton>
                <div style="max-width: 100%; overflow: auto;">
                    <asp:GridView ID="Gv_data_interface_from_base" runat="server" Visible="true" AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive footable col-md-12 Gv_data_interface_from_base"
                        HeaderStyle-CssClass="success"
                        AllowPaging="true"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        PageSize="10"
                        AutoPostBack="FALSE">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="text-align: center; padding-top: 5px;">
                                            <asp:Label ID="lbGv_data_interface_from_base0" runat="server" Visible="false" />
                                            <%# (Container.DataItemIndex +1) %>
                                        </div>
                                    </small>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Type" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_order_type")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sale Org." ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_sale_org")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Channel" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_channel")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Division" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_division")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sales office" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_sales_office")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sales group" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_sales_group")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer No." ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_customer_no")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_name")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_address")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Honse number" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_honse_number")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postal Code" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_postal_code")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="District" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_district")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_city")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_country")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tel" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_tel")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tax number" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_tax_number")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PO number" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_po_number")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delivery Date" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_delivery_date")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Head Discount" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label runat="server"
                                                Text='<%#  Eval("u1_discount_1")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_item")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Material" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_material")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_quantity")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Un (Sale Unit)" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_un_sale_unit")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Plant" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_plant")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Price/Unit" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_price_unit")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_currency")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DiscountType" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_discounttype")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DiscountAmount" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_discountAmount")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PromotionID" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_promotionID")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PromotionDesc" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_promotionDesc")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NetPriceAmount" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_netPriceAmount")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VATAmount" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_VATamount")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item cate" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <div style="padding-top: 5px; text-align: left;">
                                            <asp:Label ID="lbupdate_date" runat="server"
                                                Text='<%#  Eval("u1_item_cate")  %>'></asp:Label>
                                        </div>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>



                        </Columns>
                    </asp:GridView>
                </div>

            </div>


        </asp:View>

    </asp:MultiView>

    <script type="text/javascript">

        $(function () {
            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',

            });
            $(".start-date").on("dp.change", function (e) {
                $('.end-date').data("DateTimePicker").minDate(e.date);
            });
            $(".end-date").on("dp.change", function (e) {
                $('.start-date').data("DateTimePicker").maxDate(e.date);
            });


        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
    

            $(function () {
                $('.start-date').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
                $('.end-date').datetimepicker({
                    format: 'YYYY-MM-DD',

                });
                $(".start-date").on("dp.change", function (e) {
                    $('.end-date').data("DateTimePicker").minDate(e.date);
                });
                $(".end-date").on("dp.change", function (e) {
                    $('.start-date').data("DateTimePicker").maxDate(e.date);
                });


            });
        });
    </script>
</asp:Content>
