﻿using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_statal_ipaddress : System.Web.UI.Page
{
   #region Init
   data_statal dataStatal = new data_statal();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string misConn = "conn_mis";
   string misConnReal = "conn_mis_real";
   string statalIPAddressService = "statalIPAddressService";
   string statalConfigService = "statalConfigService";
   string _local_xml = String.Empty;
   int[] empIDX = { 172, 173, 1374, 1413, 3760, 4839 }; //P'Phon, P'Mai, Bonus, P'Toei, Mod, Jame
   #endregion Init

   #region Constant
   public static class Constants
   {
      #region for region Action
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_SEARCH = 22;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int SELECT_WHERE_EXISTS_IPADDRESS_NAME = 28;
      public const int SELECT_WHERE_EXISTS_IPADDRESS_NAME_UPDATE = 29;
      public const string VALID_FALSE = "101";
      #endregion for region Action
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         divIndex.Visible = true;
         divInsert.Visible = false;
         ViewState["empIDX"] = Session["emp_idx"];
         ViewState["keywordSearch"] = "";
         ViewState["statusSearch"] = 99;
         actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
         setConfig();
      }
      visibleButtonConfig();
   }
   #endregion Page Load

   #region Action
   protected void actionIndex(string keywordSearch = "", int statusSearch = 99)
   {
      ipaddress objStatal = new ipaddress();
      dataStatal.statal_ipaddress_action = new ipaddress[1];
      objStatal.keyword_search = keywordSearch;
      objStatal.ipaddress_status = statusSearch;
      dataStatal.statal_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.SELECT_WHERE_SEARCH);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      setGridData(gvIPAddress, dataStatal.statal_ipaddress_action);
   }

   protected void actionCreate()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         ipaddress objStatal = new ipaddress();
         dataStatal.statal_ipaddress_action = new ipaddress[1];
         objStatal.ipaddress_name = txtIPAddressName.Text.Trim();
         objStatal.ipaddress_description = txtIPAddressDescription.Text.Trim();
         objStatal.ipaddress_status = int.Parse(ddlIPAddressStatus.SelectedValue);
         dataStatal.statal_ipaddress_action[0] = objStatal;
         _local_xml = servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.CREATE);
         dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
         if (dataStatal.return_code.ToString() == "0")
         {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
         }
         else
         {
            _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง");
         }
      }
   }

   protected void actionUpdate(int m0_ipaddress_idx, string ipaddress_description, int ipaddress_status)
   {
      ipaddress objStatal = new ipaddress();
      dataStatal.statal_ipaddress_action = new ipaddress[1];
      objStatal.m0_ipaddress_idx = m0_ipaddress_idx;
      objStatal.ipaddress_description = ipaddress_description;
      objStatal.ipaddress_status = ipaddress_status;
      dataStatal.statal_ipaddress_action[0] = objStatal;
      servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.UPDATE);
   }

   protected void actionBan(int id)
   {
      ipaddress objStatal = new ipaddress();
      dataStatal.statal_ipaddress_action = new ipaddress[1];
      objStatal.m0_ipaddress_idx = id;
      dataStatal.statal_ipaddress_action[0] = objStatal;
      servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      ipaddress objStatal = new ipaddress();
      dataStatal.statal_ipaddress_action = new ipaddress[1];
      objStatal.m0_ipaddress_idx = id;
      dataStatal.statal_ipaddress_action[0] = objStatal;
      servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.UNBAN);
   }

   protected void actionChangeConfig()
   {
      config objStatal = new config();
      dataStatal.statal_config_action = new config[1];
      objStatal.m0_config_idx = 1;
      objStatal.config_timeout = int.Parse(txtTimeout.Text.Trim());
      objStatal.config_token = txtToken.Text.Trim();
      objStatal.config_type_msg = rdotypeMessage.SelectedValue;
      objStatal.config_updated_by = int.Parse(ViewState["empIDX"].ToString());
      dataStatal.statal_config_action[0] = objStatal;
      servExec.actionExec(misConn, "data_statal", statalConfigService, dataStatal, Constants.UPDATE);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            divIndex.Visible = false;
            divInsert.Visible = true;
            break;

         case "btnInsert":
            actionCreate();
            break;

         case "btnSearch":
            ViewState["keywordSearch"] = keywordSearch.Text.Trim();
            ViewState["statusSearch"] = ddlStatus.SelectedValue;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;

         case "btnChangeConfig":
            actionChangeConfig();
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online f-bold'>Online</span>";
      }
      else if (status == 0)
      {
         return "<span class='status-offline f-bold'>Offline</span>";
      }
      else
      {
         return "<span class='status-offline f-bold'>Delete</span>";
      }
   }

   protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvIPAddress":
            gvName.PageIndex = e.NewPageIndex;
            gvName.DataBind();
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvIPAddress":
            if (e.Row.RowState.ToString().Contains("Edit"))
            {
               GridView editGrid = sender as GridView;
               int colSpan = editGrid.Columns.Count;
               for (int i = 1; i < colSpan; i++)
               {
                  e.Row.Cells[i].Visible = false;
                  e.Row.Cells[i].Controls.Clear();
               }
               e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
               e.Row.Cells[0].CssClass = "";
            }
            break;
      }
   }

   protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvIPAddress":
            gvName.EditIndex = -1;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvIPAddress":
            int dataKeyId = Convert.ToInt32(gvName.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox txtIPAddressDescriptionUpdate = (TextBox)gvName.Rows[e.RowIndex].FindControl("txtIPAddressDescriptionUpdate");
            DropDownList ddlIPAddressStatusUpdate = (DropDownList)gvName.Rows[e.RowIndex].FindControl("ddlIPAddressStatusUpdate");
            gvName.EditIndex = -1;
            actionUpdate(dataKeyId, txtIPAddressDescriptionUpdate.Text.Trim(), int.Parse(ddlIPAddressStatusUpdate.SelectedValue));
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvIPAddress":
            gvName.EditIndex = e.NewEditIndex;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void checkExistsIPAddressName(object sender, ServerValidateEventArgs e)
   {
      ipaddress objStatal = new ipaddress();
      dataStatal.statal_ipaddress_action = new ipaddress[1];
      objStatal.ipaddress_name = txtIPAddressName.Text.Trim();
      dataStatal.statal_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalIPAddressService, dataStatal, Constants.SELECT_WHERE_EXISTS_IPADDRESS_NAME);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      if (dataStatal.return_code.ToString() == Constants.VALID_FALSE)
      {
         e.IsValid = false;
      }
      else
      {
         e.IsValid = true;
      }
   }

   protected void visibleButtonConfig()
   {
      if (Array.IndexOf(empIDX, int.Parse(ViewState["empIDX"].ToString())) > -1)
      {
         configTypeMSG.Visible = true;
      }
      else
      {
         configTypeMSG.Visible = false;
      }
   }

   protected void setConfig()
   {
      if (Array.IndexOf(empIDX, int.Parse(ViewState["empIDX"].ToString())) > -1)
      {
         configTypeMSG.Visible = true;
         config objStatal = new config();
         dataStatal.statal_config_action = new config[1];
         objStatal.m0_config_idx = 1;
         dataStatal.statal_config_action[0] = objStatal;
         _local_xml = servExec.actionExec(misConn, "data_statal", statalConfigService, dataStatal, Constants.SELECT_ALL);
         dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
         txtTimeout.Text = dataStatal.statal_config_action[0].config_timeout.ToString();
         txtToken.Text = dataStatal.statal_config_action[0].config_token.ToString();
         rdotypeMessage.SelectedValue = dataStatal.statal_config_action[0].config_type_msg;
      }
   }
   #endregion Custom Functions
}
