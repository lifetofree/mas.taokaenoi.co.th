﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

public partial class websystem_visitors_security_board : System.Web.UI.Page
{
    int org_idx;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;
    #region Init
    data_visitors _data_visitors = new data_visitors();
    service_execute servExec = new service_execute();
    function_tool _funcTool = new function_tool();
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlVmGetSecutityBoard = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetSecutityBoard"];
    function_dmu _func_dmu = new function_dmu();

    public object MessageBox { get; private set; }
    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 270;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {


        try
        {
            string splace_id = Request.QueryString["place_id"].ToString().Trim();
            org_idx = int.Parse(splace_id);
        }
        catch
        {
            org_idx = 0;
        }
        checkSessionRedirect();
        if (!IsPostBack)
        {
            hddf_item.Value = "0";
            actionIndex();
        }
    }
    #endregion Page Load

    #region Action
    protected void actionIndex()
    {
        int isplace_id = 0;
        try
        {
            string splace_id = Request.QueryString["place_id"].ToString().Trim();
            isplace_id = int.Parse(splace_id);
        }
        catch
        {

        }
        /*
        if (int.Parse(hddf_item.Value) == 0)
        {
            pnl_GvMaster.Visible = true;
            dvbland.Visible = false;
            pnl_img.Visible = false;
            pnl_text_center.Visible = false;
            
        }
        */

        /*Detail*/
        vm_visitors_board_detail _u0_doc = new vm_visitors_board_detail();
        //_u0_doc.u0_idx = 0;
        //_u0_doc.node_idx = 4; // decision(security)
        //_u0_doc.actor_idx = _actor_idx;
        _data_visitors.vm_visitors_board_list = new vm_visitors_board_detail[1];
        _data_visitors.vm_visitors_board_list[0] = _u0_doc;
        _data_visitors = callServicePostVisitors(_urlVmGetSecutityBoard, _data_visitors);
        ViewState["document_detail"] = _data_visitors.vm_visitors_board_list;
        setGridData(GvMaster, _data_visitors.vm_visitors_board_list);

        setLabel();


        demo_time.Text = "วันที่ : " + DateTime.Now.ToString("dd")+"/"+ DateTime.Now.ToString("MM") + "/" + (int.Parse( DateTime.Now.ToString("yyyy")) + 543).ToString();

        if (hddf_item.Value == "")
        {
            hddf_item.Value = "0";
        }
        hddf_item.Value = (int.Parse(hddf_item.Value) + 1).ToString();


    }
    private void setLabel()
    {
        lbname.Text = "รายการโหลดสินค้า";
        int iPageCount = 0, iPageIndex = 0;
        iPageCount = GvMaster.PageCount;
        iPageIndex = GvMaster.PageIndex+1;
        if (iPageCount > 0)
        {
            lbname.Text += " / " + iPageIndex.ToString() + " of " + iPageCount.ToString();
        }
        
        lbmarquee.Text = "รายการรถที่รอโหลดสินค้าวันที่ "+ DateTime.Now.ToString("dd") + " " + _func_dmu.zMonthTH(int.Parse(DateTime.Now.ToString("MM"))) + " " + (int.Parse(DateTime.Now.ToString("yyyy")) + 543).ToString();
    }
    public string getDatetime(string str)
    {
        string sName = "";
        if (str == "D")
        {
            sName = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        }
        else
        {
            sName = DateTime.Now.ToString("hh:mm", CultureInfo.InstalledUICulture);
        }
        return sName;
    }
    #endregion Action

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void Timer1_Tick(object sender, EventArgs e)
    {

        if (int.Parse(hddf_item.Value) >= 3)
        {
            hddf_item.Value = "0";
            zShowData();
        }
        else
        {
            zShowData();
        }


    }

    public void zShowData()
    {

        int iPageCount = 0, iPageIndex = 0;
        iPageCount = GvMaster.PageCount;
        iPageIndex = GvMaster.PageIndex;
        if (iPageIndex < iPageCount - 1)
        {
            GvMaster.PageIndex = iPageIndex + 1;
            GvMaster.DataBind();
            setLabel();
            actionIndex();
        }
        else
        {
            GvMaster.PageIndex = 0;
            GvMaster.DataBind();
            setLabel();
            actionIndex();
        }

    }

    public string getImgUrl(int AImage = 0)
    {
        string sPathImage = "";
        string sImg = AImage.ToString();

        string filePath_View = ConfigurationManager.AppSettings["path_flie_roombooking"];
        string directoryName = sImg;
        string getfiles = "";
        if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
        {
            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
            // List<ListItem> files = new List<ListItem>();
            foreach (string path in filesPath)
            {
                getfiles = Path.GetFileName(path);
                //getfiles = path;
            }

        }

        if (getfiles != "")
        {
            string path = filePath_View + directoryName + "/" + getfiles;
            sPathImage = path;

        }
        else
        {
            sPathImage = "";
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                setLabel();
                actionIndex();
                break;
        }
    }
    public string getImg_manageUrl(string AImage)
    {
        string sPathImage = "";
        string sImg = AImage.ToString();

        // string filePath_View = ConfigurationManager.AppSettings["path_flie_elearning_http"];
        string filePath_View = ConfigurationManager.AppSettings["path_flie_roombooking"];
        string directoryName = "meetingroom_image";
        string getfiles = "";

        if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
        {
            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
            // List<ListItem> files = new List<ListItem>();
            foreach (string path in filesPath)
            {
                getfiles = Path.GetFileName(path);
                //getfiles = path;
            }

        }
        /*
        Image _image = new Image();
        try
        {
            string path = filePath_View + directoryName + "/" + sImg;
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(path);

            HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse(); // Error 404 right here,

            if (httpRes.StatusCode == HttpStatusCode.NotFound)
            {
                getfiles = "";
            }
            else
            {
                getfiles = path;
            }
            httpRes.Close();
        }
        catch { }
        */
        if (getfiles != "")
        {
            string path = filePath_View + directoryName + "/" + getfiles;
            sPathImage = path;
        }
        else
        {
            sPathImage = "";
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public string getimageProfile()
    {


        string _str = "";
        string filePath_View = ConfigurationManager.AppSettings["upload_place_picture"];
        string directoryName = org_idx.ToString();//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

        if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
        {
            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
            List<ListItem> files = new List<ListItem>();
            int ic = 0;
            foreach (string path in filesPath)
            {
                ic++;
                string getfiles = "";
                getfiles = Path.GetFileName(path);
                _str = filePath_View + directoryName + "/" + getfiles;

            }
        }
        else
        {
            string getfiles = "default/user-default.png";
            _str = filePath_View + getfiles;


        }
        return _str;
    }

    protected data_visitors callServicePostVisitors(string _cmdUrl, data_visitors _data_visitors)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_visitors);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_visitors = (data_visitors)_funcTool.convertJsonToObject(typeof(data_visitors), _local_json);

        return _data_visitors;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_status_flag = (Label)e.Row.FindControl("lb_status_flag");
                    if(lb_status_flag.Text == "1")
                    {
                        e.Row.Attributes.CssStyle.Value = "background-color: #dff0d8;"; //#28a745

                    }
                    else
                    {
                        e.Row.Attributes.CssStyle.Value = "background-color: #f2dede;";
                    }

                }
                break;
            
        }
    }
    #endregion RowDatabound


}