﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_test_type.aspx.cs" Inherits="websystem_MasterData_qa_lab_test_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

      <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="test_type" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster_test_type" runat="server">
        <asp:View ID="view1_test_type" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_add_type" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลห้องปฏิบัติการ" data-toggle="tooltip" title="เพิ่มข้อมูลห้องปฏิบัติการ" runat="server"
                    CommandName="cmdAddtype" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลห้องปฏิบัติการ</asp:LinkButton>
            </div>

            <%--  DIV Start ADD--%>
            <asp:FormView ID="Fv_Insert_Type" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลห้องปฏิบัติการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <asp:Label ID="lanameth" runat="server" Text="ชื่อห้องปฏิบัติการ(TH)" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txttype_nameTH" runat="server" CssClass="form-control" placeholder="กรอกชื่อห้องปฏิบัติการ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatortypenameTh" runat="server"
                                                    ControlToValidate="txttype_nameTH" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องปฏิบัติการ" ValidationGroup="Savetype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendertypenameTh" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatortypenameTh" Width="200" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lanameen" runat="server" Text="ชื่อห้องปฏิบัติการ(EN)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txttype_nameEN" runat="server" CssClass="form-control" placeholder="กรอกชื่อห้องปฏิบัติการ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatortypenameEn" runat="server"
                                                    ControlToValidate="txttype_nameEN" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องปฏิบัติการ" ValidationGroup="Savetype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatortypenameEn" Width="200" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="สัญลักษณ์ของห้องปฏิบัติการ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_symbol" runat="server" CssClass="form-control" placeholder="กรอกสัญลักษณ์ของห้องปฏิบัติการ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorsymbol" runat="server"
                                                    ControlToValidate="txt_symbol" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกสัญลักษณ์ของห้องปฏิบัติการ" ValidationGroup="Savetype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendersysbol" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorsymbol" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lab_statusplace" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddTypeadd" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_type" ValidationGroup="Savetype" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="Save" runat="server" CommandName="Lbtn_submit_type" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_type" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_type" data-toggle="tooltip" title="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <%--  /DIV END ADD--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Test Type--%>
            <asp:GridView ID="Gv_select_Type" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลสถานที่</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                              <div style="text-align: center; padding-top: 5px;">
                            <asp:Label ID="test_type_idx" runat="server" Visible="false" Text='<%# Eval("test_type_idx") %>' />
                            <%# (Container.DataItemIndex +1) %>
                                  </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_type" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("test_type_idx") %>'></asp:TextBox>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อห้องปฏิบัติการ(TH)" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_typeTH" runat="server" CssClass="form-control " Text='<%# Eval("test_type_name_th") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEdittypenameTh" runat="server"
                                                    ControlToValidate="Name_typeTH" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องปฏิบัติการ" ValidationGroup="Edittype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendertypenameTh" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEdittypenameTh" Width="200" />
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อห้องปฏิบัติการ(EN)" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_typeEN" runat="server" CssClass="form-control" Text='<%# Eval("test_type_name_en") %>'></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorEdittypenameEn" runat="server"
                                                    ControlToValidate="Name_typeEN" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องปฏิบัติการ" ValidationGroup="Edittype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEdittypenameEn" Width="200" />
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="สัญลักษณ์ของห้องปฏิบัติการ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="symbol_type" runat="server" CssClass="form-control" Text='<%# Eval("test_type_symbol") %>'></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditsymbol_type" runat="server"
                                                    ControlToValidate="symbol_type" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกสัญลักษณ์ของห้องปฏิบัติการ" ValidationGroup="Edittype" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditsymbol_type" Width="250" />
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                  <%--  <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_set_detail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>--%>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_type" Text='<%# Eval("test_type_status") %>' CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" ValidationGroup="Edittype" CssClass="btn btn-success" runat="server" CommandName="Update" Text="Save" data-original-title="Save" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" data-original-title="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อห้องปฏิบัติการ(TH)" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="type_nameTH" runat="server"
                                     Text='<%# Eval("test_type_name_th") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อห้องปฏิบัติการ(EN)" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="type_nameEN" runat="server"
                                     Text='<%# Eval("test_type_name_en") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สัญลักษณ์ของห้องปฏิบัติการ" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="type_symbol" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("test_type_symbol") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะของห้องปฏิบัติการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbtype_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("test_type_status") %>'></asp:Label>
                                    <asp:Label ID="type_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ห้องปฏิบัติการนี้เปิดใช้งานอยู่"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="type_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ห้องปฏิบัติการนี้ปิดการใช้งานแล้ว" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_type"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบห้องปฏิบัติการนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("test_type_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>
</asp:Content>

