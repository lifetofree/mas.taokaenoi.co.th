﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="rst_m0_exitinterview.aspx.cs" Inherits="websystem_MasterData_rst_m0_exitinterview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .bg-template-resign {
            background-image: url('../../masterpage/images/hr/Cover_Head02.png');
            background-size: 100% 100%;
            width: 100%;
            height: 100%;
            text-align: center;
        }
    </style>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: burlywood" id="bs-example-navbar-collapse-1">
                <%--lemonchiffon--%>
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการ Exit Interview" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="เพิ่มข้อมูล" />
                    </li>

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <asp:UpdatePanel ID="update_viewindex" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 bg-template-resign">
                        <div class="col-lg-12">
                            <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_แบบฟอร์มลาออก_01-06.png" Style="height: 100%; width: 100%;" />
                        </div>

                        <div class="col-lg-12">

                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: gold; text-align: left">
                                    <h3><b>ประเภทชุดแบบฟอร์มลาออก</b></h3>
                                </blockquote>

                            </div>

                            <asp:GridView ID="GvTopic" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info "
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0_toidx"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowDataBound="Master_RowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="องค์กร" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_toidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:Label>
                                            <asp:Label ID="lblorgidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("orgidx") %>'></asp:Label>

                                            <asp:Label ID="lblorgname" runat="server" CssClass="col-sm-10" Visible="true" Text='<%# Eval("OrgNameTH") %>'></asp:Label>

                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">

                                                    <div class="form-group">

                                                        <asp:Label ID="Label18" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lblorgidx_edit" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("orgidx") %>'></asp:Label>
                                                            <asp:DropDownList ID="ddlorgidx_edit" Enabled="false" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="สร้างแบบฟอร์มองค์กร"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlorgidx_edit" Font-Size="11"
                                                                ErrorMessage="สร้างแบบฟอร์มองค์กร"
                                                                ValidationExpression="สร้างแบบฟอร์มองค์กร" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Labesl17" class="col-sm-3 control-label" runat="server" Text="ชื่อแบบฟอร์ม : " />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="txtm0_toidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:Label>
                                                            <asp:TextBox ID="txttopic" TextMode="MultiLine" row="20" CssClass="form-control" runat="server" Text='<%# Eval("topic_name") %>'></asp:TextBox>

                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-3 col-sm-offset-9">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="ชื่อแบบฟอร์ม" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltopic_name" runat="server" CssClass="col-sm-10" Visible="true" Text='<%# Eval("topic_name") %>'></asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ดูรายละเอียด" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnview" CssClass="btn btn-primary btn-sm" runat="server" CommandName="CmdViewDetail" OnCommand="btnCommand" data-toggle="tooltip" title="View" CommandArgument='<%# Eval("m0_toidx") %>'><i class="fa fa-share-square"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnEdit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelete" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบแบบฟอร์มนี้ใช่หรือไม่?')" CommandArgument='<%#  Eval("m0_toidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>


                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <asp:UpdatePanel ID="updatepic" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>
                            <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- แผนก,ตำแหน่ง --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                            <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Form Exit Interview</strong></h3>
                            </div>
                            <asp:FormView ID="fv_insert" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="รายการที่เพิ่ม" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlchooseqs" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกรายการที่เพิ่ม"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ชุดคำถาม"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="คำถามย่อย"></asp:ListItem>

                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveTopic" runat="server" Display="None"
                                                        ControlToValidate="ddlchooseqs" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกรายการที่เพิ่ม"
                                                        ValidationExpression="กรุณาเลือกรายการที่เพิ่ม" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                                </div>

                                            </div>

                                            <div id="div_txttopic" runat="server" visible="false">

                                                <div class="form-group">

                                                    <asp:Label ID="Label18" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlorgidx" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0" Text="สร้างแบบฟอร์มองค์กร"></asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddlorgidx" Font-Size="11"
                                                            ErrorMessage="สร้างแบบฟอร์มองค์กร"
                                                            ValidationExpression="สร้างแบบฟอร์มองค์กร" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">


                                                    <asp:Label ID="Label2" runat="server" Text="ชื่อชุดคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txttopic" ValidationGroup="SaveTopic" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveTopic" runat="server" Display="None" ControlToValidate="txttopic" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกชื่อชุดคำถาม"
                                                            ValidationExpression="กรุณากรอกชื่อชุดคำถาม"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="SaveTopic" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txttopic"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Panel ID="panel_quest" runat="server" Visible="false">
                                                <asp:UpdatePanel ID="update_panelquest" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label19" runat="server" Text="แบบฟอร์มองค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlorgidx_form" ValidationGroup="SaveDataSet" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0" Text="กรุณาเลือกแบบฟอร์มองค์กร"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                    ControlToValidate="ddlorgidx_form" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกแบบฟอร์มองค์กร"
                                                                    ValidationExpression="กรุณาเลือกแบบฟอร์มองค์กร" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label10" runat="server" Text="แบบฟอร์มลาออก" CssClass="col-sm-2 control-label"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddltopic" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0" Text="กรุณาเลือกแบบฟอร์มลาออก"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                    ControlToValidate="ddltopic" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกแบบฟอร์มลาออก"
                                                                    ValidationExpression="กรุณาเลือกแบบฟอร์มลาออก" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddltopic" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" runat="server" Text="ประเภทคำตอบ" CssClass="col-sm-2 control-label"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddltypeans" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำตอบ"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                    ControlToValidate="ddltypeans" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกประเภทคำตอบ"
                                                                    ValidationExpression="กรุณาเลือกประเภทคำตอบ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddltypeans" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                                <div class="form-group">
                                                    <asp:Label ID="Label23" runat="server" Text="ลำดับ" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtcourse_item" OnTextChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"
                                                            TextMode="Number" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                                                            ValidationGroup="SaveDataSet" runat="server"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtcourse_item"
                                                            Font-Size="1em" ForeColor="Red"
                                                            Display="None"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกลำดับ"></asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label11" runat="server" Text="คำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtquest" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtquest" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกคำถาม"
                                                            ValidationExpression="กรุณากรอกคำถาม"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="SaveDataSet" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtquest"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                                    </div>

                                                </div>



                                                <div id="div_choice" runat="server" visible="false">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label15" runat="server" Text="กำหนดคำตอบ" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddltypequest" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กรุณากำหนดคำตอบ"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                ControlToValidate="ddltypeans" Font-Size="11"
                                                                ErrorMessage="กรุณากำหนดคำตอบ"
                                                                ValidationExpression="กรุณากำหนดคำตอบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label12" runat="server" Text="ตัวเลือกคำตอบ" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txtchoice" ValidationGroup="SaveDataSet" row="3" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtchoice" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกคำตอบ"
                                                                ValidationExpression="กรุณากรอกคำตอบ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="SaveDataSet" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtchoice"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" runat="server" Text="ข้อมูลเพิ่มเติม" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddltypechoice" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกข้อมูลเพิ่มเติม"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                ControlToValidate="ddltypechoice" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทคำตอบ"
                                                                ValidationExpression="กรุณาเลือกประเภทคำตอบ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-2">
                                                        <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-primary btn-sm" runat="server" Text="ADD +" ValidationGroup="SaveDataSet" OnCommand="btnCommand" CommandName="CmdAdddataset"></asp:LinkButton>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1">
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <asp:UpdatePanel ID="update1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="GvWrite"
                                                                    runat="server"
                                                                    CssClass="table table-striped table-responsive info"
                                                                    GridLines="None"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    Visible="false"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">


                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>


                                                                        <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbcourse_item" runat="server" Text='<%# Eval("No_Quest") %>' />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbquest" runat="server" Text='<%# Eval("Question") %>' />

                                                                                    <br />
                                                                                    <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteListWrite"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="col-sm-1">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-1">
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <asp:UpdatePanel ID="update2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView ID="GvReportAdd"
                                                                    runat="server"
                                                                    CssClass="table table-striped table-responsive info"
                                                                    GridLines="None"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    Visible="false"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">


                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbcourse_item" runat="server" Text='<%# Eval("No_Quest") %>' />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbquest" runat="server" Text='<%# Eval("Question") %>' />

                                                                                    <br />
                                                                                    <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="กำหนดคำตอบ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbltype_quest" runat="server" Text='<%# Eval("type_quest") %>' />
                                                                                    <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />

                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตัวเลือกคำตอบ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbchoice1" runat="server" Text='<%# Eval("Choice") %>' />

                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ข้อมูลเพิ่มเติม" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <div class="word-wrap">
                                                                                    <asp:Label ID="lbtypechoice" runat="server" Text='<%# Eval("TypeChoice") %>' />
                                                                                    <asp:Label ID="Label14" Visible="false" runat="server" Text='<%# Eval("m0_chidx") %>' />

                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>
                                                                    </Columns>

                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="col-sm-1">
                                                    </div>
                                                </div>

                                            </asp:Panel>


                                            <div class="form-group" id="div_save" runat="server" visible="false">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="SaveTopic" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-lg-12 bg-template-resign">
                <div class="col-lg-12">
                    <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_แบบฟอร์มลาออก_01-06.png" Style="height: 100%; width: 100%;" />

                    <%--<asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_แบบฟอร์มลาออก_01-06.png" Style="height: 500px; width: 100%;" />--%>
                </div>

                <div class="col-lg-12">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="GvTypeAns" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0_taidx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="background-color: #cce6ff;">
                                                    <h4><b>
                                                        <i class="fa fa-edit"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_ans") %>'></asp:Label>
                                                    </b></h4>
                                                </blockquote>
                                            </div>

                                            <asp:Label ID="lblm0_taidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:Label>

                                            <asp:GridView ID="GvQuestion" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="danger small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="m0_quidx"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowCommand="onRowCommand">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ข้อที่" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                            <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                            <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                                            <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-left" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblm0_quidx" Visible="false" runat="server" Text='<%# Eval("m0_quidx") %>' />
                                                            <asp:Label ID="lblchoice" runat="server" Text='<%# Eval("quest_name") %>'></asp:Label>
                                                            <%--    &nbsp;&nbsp; <b style="color:red";>(<asp:Label ID="Label16" runat="server" Text='<%# Eval("type_quest") %>'></asp:Label>)</b>--%>
                                                            <%--<asp:Label ID="lblm0_chidx"  Visible="true" runat="server"  />--%>

                                                            <asp:Panel runat="server" ID="panel_choice_rdo" Visible="false">
                                                                <br />
                                                                <asp:RadioButtonList ID="rdochoice" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                    <%--<input name="txtrdo" runat="server" id="txtrdo" type="text" />--%>
                                                                </asp:RadioButtonList>
                                                                &nbsp;
                                                                <%-- <asp:TextBox ID="txtrdo" ValidationGroup="saveanswer" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                            </asp:Panel>

                                                            <asp:Panel runat="server" ID="panel_choice_chk" Visible="false">
                                                                <br />
                                                                <asp:CheckBoxList ID="chkchoice" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                    <%--<input name="txtchk" runat="server" id="txtchk" type="text" />--%>
                                                                </asp:CheckBoxList>
                                                                &nbsp;
                                                               <%--  <asp:TextBox ID="txtchk" ValidationGroup="saveanswer" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                            </asp:Panel>

                                                            <asp:Panel runat="server" ID="panel_comment" Visible="false">
                                                                <div class="form-group">
                                                                    <div class="col-sm-10">
                                                                        <asp:TextBox ID="txtquest" ValidationGroup="saveanswer" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="saveanswer" runat="server" Display="None" ControlToValidate="txtquest" Font-Size="11"
                                                                            ErrorMessage="กรุณากรอกคำตอบ"
                                                                            ValidationExpression="กรุณากรอกคำตอบ"
                                                                            SetFocusOnError="true" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                            ValidationGroup="saveanswer" Display="None"
                                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                            ControlToValidate="txtquest"
                                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                                            SetFocusOnError="true" />

                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                                                    </div>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="แก้ไขข้อมูล" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnEdit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdViewEdit" OnCommand="btnCommand" data-toggle="tooltip" title="Edit" CommandArgument='<%#  Eval("m0_toidx") + ";" + Eval("m0_quidx") + ";" + Eval("m0_taidx")  %>'><i class="fa fa-edit"></i></asp:LinkButton>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>

                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-11">
                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Back"><i class="fa fa-reply"></i></asp:LinkButton>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewEdit" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 bg-template-resign">
                        <div class="col-lg-12">
                            <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_แบบฟอร์มลาออก_01-06.png" Style="height: 500px; width: 100%;" />
                        </div>

                        <div class="col-lg-12">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: gold; text-align: left">
                                    <h3><b>แก้ไขชุดแบบฟอร์มลาออก</b></h3>
                                </blockquote>
                            </div>


                            <asp:GridView ID="GvEditQuestion" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="danger small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m1_quidx"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowDataBound="Master_RowDataBound">


                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="ข้อที่" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">

                                        <ItemTemplate>
                                            <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                            <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                            <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                            <asp:Label ID="lblm0_chidx" Visible="false" runat="server" Text='<%# Eval("m0_chidx") %>' />
                                            <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm1_quidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_quidx")%>' />
                                                            <asp:TextBox ID="txtm0_quidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_quidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Labesl17" class="col-sm-3 control-label" runat="server" Text="คำถาม : " />
                                                        <div class="col-sm-8">

                                                            <asp:TextBox ID="txtquest_name" TextMode="MultiLine" row="20" CssClass="form-control" runat="server" Text='<%# Eval("quest_name") %>'></asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div id="id_typequest_edit" runat="server">
                                                        <div class="form-group">
                                                            <asp:Label ID="lblm0_tqidx_edit" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                            <asp:Label ID="Labexl15" runat="server" Text="กำหนดคำตอบ" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddltypequest_edit" ValidationGroup="Save_edit" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0" Text="กรุณากำหนดคำตอบ"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6edit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddltypequest_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณากำหนดคำตอบ"
                                                                    ValidationExpression="กรุณากำหนดคำตอบ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6edit" Width="160" />

                                                            </div>
                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Labedl18" runat="server" Text="คำตอบ" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtanswer" TextMode="MultiLine" row="20" CssClass="form-control" runat="server" Text='<%# Eval("choice_name") %>'></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Labdel1" runat="server" Text="ข้อมูลเพิ่มเติม" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <asp:Label ID="lblm0_chidx_edit" Visible="false" runat="server" Text='<%# Eval("m0_chidx") %>' />
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddltypechoice_edit" ValidationGroup="Save_edit" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0" Text="กรุณาเลือกข้อมูลเพิ่มเติม"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1edit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                    ControlToValidate="ddltypechoice_edit" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกประเภทคำตอบ"
                                                                    ValidationExpression="กรุณาเลือกประเภทคำตอบ" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1edit" Width="160" />
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-3 col-sm-offset-9">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-left" ItemStyle-Width="50%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <b>
                                                        <asp:Label ID="Label16" runat="server" Text="คำถาม" CssClass="control-label"></asp:Label></b>

                                                    <asp:Label ID="lblm0_quidx" Visible="false" runat="server" Text='<%# Eval("m0_quidx") %>' />
                                                    <asp:TextBox ID="lblquest_name" Enabled="false" TextMode="MultiLine" row="20" CssClass="form-control" runat="server" Text='<%# Eval("quest_name") %>'></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />

                                            <div class="form-group" id="id_typequest" runat="server">
                                                <div class="col-sm-12">
                                                    <b>
                                                        <asp:Label ID="Label15" runat="server" Text="กำหนดคำตอบ" CssClass="control-label"></asp:Label></b>

                                                    <asp:DropDownList ID="ddltypequest" Enabled="false" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="กรุณากำหนดคำตอบ"></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="ddltypequest" Font-Size="11"
                                                        ErrorMessage="กรุณากำหนดคำตอบ"
                                                        ValidationExpression="กรุณากำหนดคำตอบ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallxoutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />


                                                </div>
                                            </div>
                                        </ItemTemplate>



                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คำตอบ" ItemStyle-CssClass="text-left" ItemStyle-Width="37%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <b>
                                                        <asp:Label ID="Label17" runat="server" Text="คำตอบ" CssClass="control-label"></asp:Label></b>
                                                    <asp:Label ID="lblm1_quidx" Visible="false" runat="server" Text='<%# Eval("m1_quidx") %>' />
                                                    <asp:TextBox ID="lblanswer" Enabled="false" TextMode="MultiLine" row="20" CssClass="form-control" runat="server" Text='<%# Eval("choice_name") %>'></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <b>
                                                        <asp:Label ID="Label1" runat="server" Text="ข้อมูลเพิ่มเติม" CssClass="control-label"></asp:Label></b>

                                                    <asp:DropDownList ID="ddltypechoice" Enabled="false" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกข้อมูลเพิ่มเติม"></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="ddltypechoice" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทคำตอบ"
                                                        ValidationExpression="กรุณาเลือกประเภทคำตอบ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExxtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                </div>
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="8%" Visible="true" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_subquest" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m1_quidx") %>'><i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-11">
                                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel_Detail" data-toggle="tooltip" title="Back"><i class="fa fa-reply"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>
    </asp:MultiView>

</asp:Content>

