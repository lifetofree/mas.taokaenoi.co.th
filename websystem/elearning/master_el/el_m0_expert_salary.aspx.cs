﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_elearning_master_el_el_m0_expert_salary : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_elearning _data_elearning = new data_elearning();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //static string _urlget_course_costcenter = _serviceUrl + ConfigurationManager.AppSettings["urlget_course_costcenter"];
    static string _urlget_expert_salary = _serviceUrl + ConfigurationManager.AppSettings["urlget_expert_salary"];

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            ViewState["txtexpert_detail_search"] = "";
            SelectMasterList();
        }
    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        //dtelearning.CCIDX = txtcusname.Text;
        dtelearning.expert_detail = txtexpert_detail_add.Text;
        dtelearning.expert_num = txtexpert_num_add.Text;
        dtelearning.expert_vat = int.Parse(txtexpert_vat_add.Text);
        dtelearning.ep_create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_expert = 1; //insert

        _data_elearning.m0_expert_salary_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);
    }

    protected void SelectMasterList()
    {
        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        dtelearning.expert_detail = ViewState["txtexpert_detail_search"].ToString(); //select
        dtelearning.type_select_expert = 2; //select

        _data_elearning.m0_expert_salary_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_expert_salary_action);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        dtelearning.EPIDX = int.Parse(ViewState["EPIDX_update"].ToString());
        dtelearning.expert_detail = ViewState["txtexpert_detail_edit"].ToString();
        dtelearning.expert_num = ViewState["txtexpert_num_edit"].ToString();
        dtelearning.expert_vat = int.Parse(ViewState["txtexpert_vat_edit"].ToString());
        //dtelearning.course_status = int.Parse(ViewState["ddStatus_update"].ToString());
        //dtelearning.create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_expert = 3; //update

        _data_elearning.m0_expert_salary_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);
    }

    protected void Delete_Master_List()
    {
        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        dtelearning.EPIDX = int.Parse(ViewState["EPIDX_delete"].ToString());
        dtelearning.type_select_expert = 4; //select

        _data_elearning.m0_expert_salary_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_expert_salary_action);
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _dataEmployee;
    }

    protected data_elearning callServiceElearning(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);
        return _data_elearning;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        //var ddlcost_center_edit = (DropDownList)e.Row.FindControl("ddlcost_center_edit");
                        //var lblcost_center_edit = (Label)e.Row.FindControl("lblcost_center_edit");

                        //Select_Coscenter(ddlcost_center_edit);
                        //ddlcost_center_edit.SelectedValue = lblcost_center_edit.Text;

                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnaddholder.Visible = false;
                div_search.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnaddholder.Visible = true;
                div_search.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int EPIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtexpert_detail_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtexpert_detail_edit");
                var txtexpert_num_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtexpert_num_edit");
                var txtexpert_vat_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtexpert_vat_edit");
                //var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["EPIDX_update"] = EPIDX;
                ViewState["txtexpert_detail_edit"] = txtexpert_detail_edit.Text;
                ViewState["txtexpert_num_edit"] = txtexpert_num_edit.Text;
                ViewState["txtexpert_vat_edit"] = txtexpert_vat_edit.Text;
                //ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":
                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                div_search.Visible = false;
                //Select_Coscenter(ddlcost_center);
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                div_search.Visible = true;
                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int EPIDX = int.Parse(cmdArg);
                ViewState["EPIDX_delete"] = EPIDX;
                Delete_Master_List();
                SelectMasterList();
                break;

            case "CmdSearch":
                ViewState["txtexpert_detail_search"] = txtexpert_detail_search.Text;
                SelectMasterList();
                break;

            case "CmdRefresh":
                ViewState["txtexpert_detail_search"] = "";
                txtexpert_detail_search.Text = String.Empty;
                SelectMasterList();
                break;
        }

    }
    #endregion
}