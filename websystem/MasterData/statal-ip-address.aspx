﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="statal-ip-address.aspx.cs" Inherits="websystem_statal_ipaddress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <!-- Start Gridview & Update Form -->
   <div id="divIndex" runat="server">
      <div class="col-md-12">
         <asp:LinkButton ID="btnToInsert" CssClass="btn btn-success pull-right f-s-14" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="Create" />
         <div id="divJoinGroup" runat="server" class="dropdown m-r-10 pull-right">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
               Join group
               <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
               <li class="text-center">
                  <img src='<%= ResolveUrl("~/images/status-alert/qrcode.jpg") %>' width="220" height="220" />
               </li>
               <li class="text-center f-s-12 text- m-b-10">OR</li>
               <p class="text-center"><a href="http://line.me/ti/g/XSDv9seH5h">Click to join group !</a></p>
            </ul>
         </div>
         <div id="configTypeMSG" runat="server" class="dropdown m-r-10 pull-right" visible="false">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
               Config
               <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" style="width: 350px;">
               <li>
                  <div class="col-md-12">
                     <label>Type message :</label>
                     <asp:RadioButtonList ID="rdotypeMessage" runat="server" CssClass="radio-list-inline-emps">
                        <asp:ListItem Value="11">1 Message - 1 Server</asp:ListItem>
                        <asp:ListItem Value="1m">1 Message - Many server</asp:ListItem>
                     </asp:RadioButtonList>
                  </div>
                  <div class="clearfix"></div>
               </li>
               <li role="separator" class="divider"></li>
               <li>
                  <div class="col-md-12">
                     <label>Time out (ms) :</label>
                     <asp:TextBox ID="txtTimeout" runat="server" CssClass="form-control" />
                     <asp:RequiredFieldValidator runat="server"
                        ValidationGroup="btnSaveChangeConfig"
                        ControlToValidate="txtTimeout"
                        ErrorMessage="Time out Required!"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        Font-Size="12px" ForeColor="Red" />
                     <asp:CompareValidator runat="server"
                        Operator="DataTypeCheck"
                        Type="Integer"
                        ValidationGroup="btnSaveChangeConfig"
                        ControlToValidate="txtTimeout"
                        ErrorMessage="Number only!"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        Font-Size="12px" ForeColor="Red" />
                  </div>
                  <div class="clearfix"></div>
               </li>
               <li role="separator" class="divider"></li>
               <li>
                  <div class="col-md-12">
                     <label>Token :</label>
                     <asp:TextBox ID="txtToken" runat="server" CssClass="form-control txt-token" />
                     <asp:RequiredFieldValidator runat="server"
                        ValidationGroup="btnSaveChangeConfig"
                        ControlToValidate="txtToken"
                        ErrorMessage="Token Required!"
                        Display="Dynamic"
                        CssClass="validator-txt-token"
                        SetFocusOnError="true"
                        Font-Size="12px" ForeColor="Red" />
                  </div>
                  <div class="clearfix"></div>
               </li>
               <li>
                  <div class="col-md-12">
                     <asp:LinkButton ID="btnChangeConfig" runat="server" OnClientClick="return confirm('คุณต้องการบันทึกการเปลี่ยนแปลงการตั้งค่าใช่หรือไม่?')" OnCommand="btnCommand" ValidationGroup="btnSaveChangeConfig" CommandName="btnChangeConfig" Text="Save change" CssClass="btn btn-success f-s-12 m-t-10 m-b-10" />
                  </div>
               </li>
            </ul>
         </div>
         <div class="form-inline">
            <div class="form-group">
               <label for="keywordSearch">Keyword :</label>
               <asp:TextBox ID="keywordSearch" runat="server" CssClass="form-control" placeholder="IP Address" />
            </div>
            <div class="form-group">
               <label for="keywordSearch">Status :</label>
               <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                  <asp:ListItem Value="99">All</asp:ListItem>
                  <asp:ListItem Value="1">Online</asp:ListItem>
                  <asp:ListItem Value="0">Offline</asp:ListItem>
               </asp:DropDownList>
            </div>
            <div class="form-group">
               <asp:LinkButton ID="btnSearch" runat="server" OnCommand="btnCommand" CommandName="btnSearch" CssClass="btn btn-primary"><i class="fa fa-search"></i></asp:LinkButton>
            </div>
         </div>
         <asp:GridView ID="gvIPAddress"
            runat="server"
            AutoGenerateColumns="false"
            DataKeyNames="m0_ipaddress_idx"
            CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
            HeaderStyle-CssClass="info"
            AllowPaging="false"
            PageSize="10"
            OnRowEditing="Master_RowEditing"
            OnRowUpdating="Master_RowUpdating"
            OnRowCancelingEdit="Master_RowCancelingEdit"
            OnPageIndexChanging="Master_PageIndexChanging"
            OnRowDataBound="Master_RowDataBound">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div style="text-align: center">No result</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="10%">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="m0_ipaddress_idx" runat="server" CssClass="form-control"
                        Visible="False" Text='<%# Eval("m0_ipaddress_idx")%>' />
                     <div class="text-center">
                        <label>IP Address : </label>
                        <asp:Label ID="txtIPAddressNameUpdate" runat="server" Text='<%# Eval("ipaddress_name")%>' />
                     </div>
                     <br />
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Description</label>
                           <asp:TextBox ID="txtIPAddressDescriptionUpdate" runat="server" CssClass="form-control" Text='<%# Eval("ipaddress_description")%>'
                              placeholder="Description..."  />
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="pull-left">Status</label>
                           <asp:DropDownList ID="ddlIPAddressStatusUpdate" AutoPostBack="false" runat="server"
                              CssClass="form-control" SelectedValue='<%# Eval("ipaddress_status") %>'>
                              <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                              <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                           </asp:DropDownList>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="pull-left">
                           <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                              ValidationGroup="saveIPAddressUpdate" CommandName="Update"
                              OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                 Save change
                           </asp:LinkButton>
                           <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                              CommandName="Cancel">Cancel</asp:LinkButton>
                        </div>
                     </div>
                  </EditItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="IP Address" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="ipaddressName" runat="server" Text='<%# Eval("ipaddress_name") %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="ipaddressDescription" runat="server" Text='<%# Eval("ipaddress_description") %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <small>
                        <asp:Label runat="server" Text='<%# getStatus((int)Eval("ipaddress_status")) %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                  <ItemTemplate>
                     <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit"
                        data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                     <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                        title="Remove" CommandName="btnBan" OnCommand="btnCommand"
                        CommandArgument='<%# Eval("m0_ipaddress_idx") %>'
                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                           <i class="fa fa-trash"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
   </div>
   <!-- End Gridview & Update Form -->

   <!-- Start Insert Form -->
   <div id="divInsert" runat="server">
      <div class="row">
         <div class="col-md-12">
            <div class="form-group">
               <asp:LinkButton CssClass="btn btn-danger" runat="server" CommandName="btnCancel" OnCommand="btnCommand">
                  <i class="fa fa-angle-left fa-lg"></i> Back
               </asp:LinkButton>
            </div>
         </div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading">Create IP Address</div>
               <div class="panel-body">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>IP Address</label>
                        <asp:TextBox ID="txtIPAddressName" runat="server" CssClass="form-control"
                           placeholder="IP Address..." />
                        <asp:RequiredFieldValidator ID="requiredIPAddressName"
                           ValidationGroup="saveIPAddress" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtIPAddressName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="IP Address is Required!" />
                        <asp:CustomValidator ID="customIPAddressName"
                           ValidationGroup="saveIPAddress" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtIPAddressName"
                           OnServerValidate="checkExistsIPAddressName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="This IP Address is already in database!" />
                        <asp:RegularExpressionValidator ID="regExIPAddressName"
                           ValidationGroup="saveIPAddress" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtIPAddressName"
                           Font-Size="12px" ForeColor="Red"
                           ValidationExpression="\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
                           ErrorMessage="Pattern of IP Address incorrect" />
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>Description</label>
                        <asp:TextBox ID="txtIPAddressDescription" runat="server" CssClass="form-control"
                           placeholder="Description..." />
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <label>Status</label>
                        <asp:DropDownList ID="ddlIPAddressStatus" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:Button ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="Save" ValidationGroup="saveIPAddress" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Insert Form -->

   <!-- Start JS Area -->
   <script type="text/javascript">
      $(function () {
         $(document).on('click', '.dropdown', function (e) {
            e.stopPropagation();
         });
      });
      var prm = Sys.WebForms.PageRequestManager.getInstance();
      prm.add_endRequest(function () {
         $(function () {
            $(document).on('click', '.dropdown', function (e) {
               e.stopPropagation();
            });
         });
      });
   </script>
   <!-- End JS Area -->
</asp:Content>
