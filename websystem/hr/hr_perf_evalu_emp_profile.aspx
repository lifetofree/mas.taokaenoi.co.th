﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_perf_evalu_emp_profile.aspx.cs" Inherits="websystem_hr_perf_evalu_emp_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .bg-ad-profiletemplate-center {
            background: url('../../images/hr_perf_evalu_emp_profile/BG_02-02.png') no-repeat;
            background-size: 100% 100%;
        }

        .bg-ad-profile-templateheader-center {
            background: url('../../images/hr_perf_evalu_emp_profile/BG_02-01.png') no-repeat;
            background-size: 100% 100%;
        }
    </style>




    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <div id="BoxTabMenuIndex" runat="server" visible="false">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnrefresh" OnCommand="btnCommand">ข้อมูลทั่วไป</asp:LinkButton>
                    </li>
                    <li id="ProfileList" runat="server">
                        <asp:LinkButton ID="lbprofile" CssClass="btn_menulist" runat="server" CommandArgument="0" CommandName="ViewProfile" OnCommand="btnCommand">ข้อมูลส่วนตัว</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <%-------- Index Start----------%>

        <asp:View ID="ViewDetail" runat="server">

            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading bg-ad-profile-templateheader-center" style="height: 65pt;">
                    </div>

                    <div class="panel-body bg-ad-profiletemplate-center">

                        <div class="panel-body">

                            <asp:FormView ID="FvProfile" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <ItemTemplate>

                                    <div class="col-lg-12">

                                        <div class="form-horizontal" role="form">

                                            <div class="row">

                                                <div class="panel panel-default">
                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label1" CssClass="col-xs-4 control-labelnotop text_right"
                                                                    runat="server" Text="แบบประเมินผลการปฏิบัติงานประจำปี : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="lb_year" CssClass="control-labelnotop text_left" runat="server" Text='' />

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label2" CssClass="col-xs-4 control-labelnotop text_right"
                                                                    runat="server" Text="เกณฑ์การประเมิน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="lb_evaluation_criteria" CssClass="control-labelnotop text_left" runat="server" Text='' />

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-12" runat="server" visible="false">
                                                <div class="form-group" style="text-align: center">
                                                    <asp:Image ID="images" runat="server" Width="25%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-02.png" />

                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <%--  <div class="col-lg-1">
                                                            style="background-color: white; border-radius: 2%"
                                                    </div>--%>

                                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                        <div class="form-group">

                                                            <asp:Label ID="Label7y" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสพนักงาน : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbEmpCode" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_code") %>' />
                                                                <asp:Label ID="LbEmpidx" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_idx") %>' />

                                                            </div>

                                                            <asp:Label ID="Label18" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อพนักงาน : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label19" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                            </div>

                                                        </div>


                                                        <div class="form-group">

                                                            <asp:Label ID="Label33" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbOrgNameTH" CssClass="control-labelnotop" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                                <asp:Label ID="LbOrgidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_idx") %>' />
                                                            </div>

                                                            <asp:Label ID="Label20" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th") %>' />
                                                                <asp:Label ID="LbDeptidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rdept_idx") %>' />

                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label36" CssClass="control-labelnotop" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                                <asp:Label ID="Lbrsecidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rsec_idx") %>' />
                                                            </div>

                                                            <asp:Label ID="Label31" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th") %>' />
                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label41" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทพนักงาน  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label42" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_type_name") %>' />
                                                                <asp:Label ID="lb_emp_type_idx" CssClass="control-labelnotop" runat="server" Visible="false" Text='<%# Eval("emp_type_idx") %>' />
                                                            </div>

                                                            <asp:Label ID="Label39" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label40" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("LocName") %>' />
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <%--<div class="col-lg-12">
                                                <br>
                                            </div>--%>
                                        </div>
                                    </div>
                                </ItemTemplate>

                            </asp:FormView>

                            <div class="col-lg-12 word-wrap">
                                <div class="panel-heading" style="background-color: #F6B333; text-align: center">
                                    <h3 class="panel-title "><i class="glyphicon glyphicon-file"></i><strong>&nbsp; ผลการปฏิบัติงานตาม KPI (Performance Appraisal)</strong></h3>
                                </div>
                                <%-- <div class="col-lg-12">--%>
                                <asp:GridView ID="gvprofileKPI"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-hover table-responsive col-lg-12 word-wrap"
                                    HeaderStyle-CssClass="info"
                                    RowStyle-BackColor="White"
                                    GridLines="None"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowCreated="grvMergeHeader_RowCreated">

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>


                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="13%">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_ItemIndex" runat="server" Visible="false"
                                                    Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                                <asp:Label ID="lb_data_name" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("data_name") %>'>
                                                                  
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Corporate KPI" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_corp_kpi" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("corp_kpi") %>'>
                                                                  
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TAOKAE-D" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_taokae_d" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("taokae_d") %>'>
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TKN Value" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_tkn_value" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("tkn_value") %>'>
                                                                  
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สาย ขาดงาน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_absence" runat="server" CssClass="col-sm-12"
                                                    Text='<%# Eval("absence") %>'>
                                                                  
                                                </asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="TOTAL" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_total_deduction" runat="server"
                                                    Text='<%# Eval("total_deduction") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="10%">
                                            <ItemTemplate>

                                                <div style="text-align: center;">
                                                    <asp:Label ID="lb_punishment1" runat="server"
                                                        CssClass="col-sm-12"
                                                        Text='<%# Eval("punishment1") %>'>
                                                                  
                                                    </asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10"
                                            HeaderStyle-Width="5%">
                                            <ItemTemplate>

                                                <div style="text-align: center;">
                                                    <asp:Label ID="lb_punishment2"
                                                        runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("punishment2") %>'>
                                                                  
                                                    </asp:Label>
                                                </div>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10">
                                            <ItemTemplate>
                                                <b>
                                                    <asp:Label ID="lb_grade" runat="server"
                                                        Font-Bold="true"
                                                        Font-Size="Medium"
                                                        Text='<%# Eval("grade") %>' />
                                                </b>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-Font-Size="9"
                                            ControlStyle-Font-Size="10">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_grade_name" runat="server"
                                                    Text='<%# Eval("grade_name") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:Label ID="Label12" runat="server"
                                                Text="A - OUTSTANDING มีผลการปฏิบัติงานโดดเด่นอย่างชัดเจน 96-100 %"
                                                ForeColor="Green"
                                                Font-Size="Small">
                                            </asp:Label>
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:Label ID="Label13" runat="server"
                                                Text="B - EXCEEDS EXPECTATIONS เกินกว่าที่คาดหวัง 81-95%"
                                                ForeColor="Green"
                                                Font-Size="Small">
                                            </asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:Label ID="Label14" runat="server"
                                                Text="C - MEET EXPECTATIONS เป็นไปตามความคาดหวัง 61-80 %"
                                                ForeColor="Green"
                                                Font-Size="Small">
                                            </asp:Label>
                                        </div>
                                        <div class="col-lg-6">
                                            <asp:Label ID="Label15" runat="server"
                                                Text="D - IMPROVEMENT NEEDED ต้องปรับปรุง 50-60%"
                                                ForeColor="#cc9900"
                                                Font-Size="Small">
                                            </asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <asp:Label ID="Label16" runat="server"
                                                Text="E - CONSISTENLY BELOW EXPECTATIONS ไม่เป็นไปตามที่คาดหวังอย่างต่อเนื่อง ต่ำกว่า 49%"
                                                ForeColor="Red"
                                                Font-Size="Small">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--  </div>--%>

                            <div class="col-lg-12">
                                <br />
                            </div>

                            <asp:FormView ID="Fv1"
                                DefaultMode="Insert" runat="server" Width="100%"
                                OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="col-lg-12">

                                        <div class="form-horizontal" role="form">

                                            <div class="row">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="background-color: #ffcccc; text-align: center">
                                                        <h3 class="panel-title"><strong>โทษทางวินัย</strong></h3>
                                                    </div>
                                                    <div class="panel-body">

                                                        <div class="form-group">

                                                            <asp:Label ID="Label17" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หนังสือเตือน " />
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="Label21" CssClass="form-control text-center" runat="server" Text='<%# "-" %>' />

                                                            </div>
                                                            <asp:Label ID="Label22" CssClass="col-xs-2 control-labelnotop text-left" runat="server" Text="เรื่อง" />

                                                            <%--<asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="" />--%>
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="lb_punishment1" CssClass="form-control text-center" runat="server" Text='<%# Eval("punishment1") %>' />

                                                            </div>
                                                            <asp:Label ID="Label25" CssClass="col-xs-4 control-labelnotop text-left" runat="server" Text="ครั้ง (ครั้งละ 10 คะแนน)*เกิน 3 ครั้งขึ้นไปไม่ปรับเงินประจำปี" />


                                                        </div>

                                                        <%--  --%>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label26" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="พักงาน " />
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="lb_punishment2" CssClass="form-control text-center" runat="server" Text='<%# Eval("punishment2") %>' />

                                                            </div>
                                                            <asp:Label ID="Label28" CssClass="col-xs-2 control-labelnotop text-left" runat="server" Text="ครั้งไม่ปรับเงินประจำปี" />


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="background-color: #b3e6ff; text-align: center">
                                                        <h3 class="panel-title"><strong>สถิติมาทำงาน</strong></h3>
                                                    </div>
                                                    <div class="panel-body">

                                                        <div class="form-group">

                                                            <asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สาย / กลับก่อน " />
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="Label29" CssClass="form-control text-center" runat="server" Text='<%# "-" %>' />

                                                            </div>
                                                            <asp:Label ID="Label30" CssClass="col-xs-2 control-labelnotop text-left" runat="server" Text="ครั้ง" />

                                                            <%--<asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="" />--%>
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="lb_late_mm" CssClass="form-control text-center" runat="server" Text='<%# Eval("late_mm") %>' />

                                                            </div>
                                                            <asp:Label ID="Label34" CssClass="col-xs-4 control-labelnotop text-left" runat="server" Text="นาที (30 นาที หัก 1 คะแนน)" />


                                                        </div>

                                                        <%--  --%>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label37" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ขาดงาน " />
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="Label38" CssClass="form-control text-center" runat="server" Text='<%# "-" %>' />

                                                            </div>
                                                            <asp:Label ID="Label43" CssClass="col-xs-2 control-labelnotop text-left" runat="server" Text="ครั้ง" />


                                                            <%--<asp:Label ID="Label44" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text=" " />--%>
                                                            <div class="col-xs-2">
                                                                <asp:Label ID="lb_missing_qty" CssClass="form-control text-center" runat="server" Text='<%# Eval("missing_qty") %>' />

                                                            </div>
                                                            <asp:Label ID="Label46" CssClass="col-xs-2 control-labelnotop text-left" runat="server" Text="วัน (วันละ  5 คะแนน)" />


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>

                            </asp:FormView>


                            <%--<div class="col-lg-12">
                                <br />
                            </div>--%>

                            <div class="col-lg-12">
                                <table style="width: 100%; background-color: white"
                                    class="table table-bordered table-hover table-responsive col-lg-12 word-wrap">
                                    <tr style="text-align: center; background-color: #ff9666;">
                                        <td colspan="2" style="width: 70%;"><b>ปัจจัยการประเมิน</b></td>
                                        <td><b>พนักงานประเมินตนเอง</b></td>
                                        <td><b>ผู้บังคับบัญชาประเมินผ่านระบบ</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="background-color: #ffeee6;"><b>TKN VALUE ค่านิยมองค์กร</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lb_1" runat="server">
                                                 <p><b style="color:blue;">O</b>WNERSHIP</p>  
                                            </asp:Label>
                                        </td>
                                        <td>มุ่งมั่น บรรลุผล จนสำเร็จ</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_ownership" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_ownership" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label3" runat="server">
                                                 <p><b style="color:blue;">I</b>MPROVEMENT CONTINUEOUSLY</p>  
                                            </asp:Label>
                                        </td>
                                        <td>เชี่ยวชาญ ต่อยอด ส่งมอบความพึงพอใจ</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_improvement_continueously" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_improvement_continueously" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label4" runat="server">
                                                 <p><b style="color:blue;">C</b>OMMITMENT</p>  
                                            </asp:Label>
                                        </td>
                                        <td>ตั้งใจ รับผิดชอบ ทุ่มเท</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_commitment" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_commitment" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="text-align: right;">
                                        <td colspan="2"><b>คะแนนรวม</b></td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_tkn_value" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_tkn_value" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="background-color: #ffeee6;"><b>LEADERSHIP CAPABILITIES ความเป็นผู้นำ</b></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" runat="server">
                                                 <p><b style="color:red;">T</b>   INNOVA<b style="color:red;"><u>T</u></b>ION</p>  
                                            </asp:Label>
                                        </td>
                                        <td>บทบาทผู้นำในการสร้างนวัตกรรม</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_innovation" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_innovation" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server">
                                                 <p><b style="color:red;">A</b>   <b style="color:red;"><u>A</u></b>CHIEVING  RESULT</p>  
                                            </asp:Label>
                                        </td>
                                        <td>การมุ่งเน้นผลสำเร็จของงาน</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_achieving_result" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_achieving_result" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label7" runat="server">
                                                 <p><b style="color:red;">O</b>   RELATI<b style="color:red;"><u>O</u></b>NS</p>  
                                            </asp:Label>
                                        </td>
                                        <td>การสร้างความสัมพันธ์ในการทำงาน</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_relations" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_relations" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label8" runat="server">
                                                 <p><b style="color:red;">K</b>   JOB  <b style="color:red;"><u>K</u></b>NOWLEDGE</p>  
                                            </asp:Label>
                                        </td>
                                        <td>ความรู้ความสามารถในงาน</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_job_knowledge" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_job_knowledge" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label9" runat="server">
                                                 <p><b style="color:red;">A</b>   <b style="color:red;"><u>A</u></b>RT OF COMMUNICATION</p>  
                                            </asp:Label>
                                        </td>
                                        <td>สื่อสารอย่างมีศิลปะและจูงใจ</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_art_communication" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_art_communication" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label10" runat="server">
                                                 <p><b style="color:red;">E</b>   L<b style="color:red;"><u>E</u></b>ADERSHIP</p>  
                                            </asp:Label>
                                        </td>
                                        <td>ความเป็นผู้นำ</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_leadership" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_leadership" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label11" runat="server">
                                                 <p><b style="color:green;">D</b>   <b style="color:green;"><u>D</u></b>ECISION MAKING & PLANNING</p>  
                                            </asp:Label>
                                        </td>
                                        <td>การแก้ปัญหา การตัดสินใจ และการวางแผน</td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_decision_making_planning" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_decision_making_planning" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="text-align: right; background-color: #ff9666;">
                                        <td colspan="2"><b>คะแนนรวม</b></td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_emp_leadership_capabilities" runat="server">
                                            </asp:Label>
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Label ID="lb_leader_leadership_capabilities" runat="server">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-lg-12">
                                <br />
                            </div>
                            <div class="col-lg-12">
                                <div class="panel-heading" style="background-color: #A3D55D; text-align: center">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                                </div>
                                <%-- <div class="col-lg-12" style="background-color: white">
                                                        <br />--%>

                                <asp:Literal ID="litReportChart" runat="server" />
                                <asp:Literal ID="litReportChart1" runat="server" />

                                <%-- </div>--%>
                            </div>


                            <div class="col-lg-12">
                                <br />
                            </div>
                            <div class="col-lg-12" runat="server" visible="false">

                                <div class="form-group">
                                    <div class="col-sm-11">
                                    </div>
                                    <div class="col-sm-1">
                                        <asp:LinkButton ID="btnprint" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand"
                                            CommandName="btnprint" data-toggle="tooltip" title="Print"
                                            Text="<i class='fa fa-print'></i>"></asp:LinkButton>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <%--</ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

