﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_overtime_report : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_overtime _data_overtime = new data_overtime();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    //-- employee --//


    //-- over time shift rotate --//
    static string _urlGetDetailOTCreateShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTCreateShiftRotate"];
    static string _urlSetCreateOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTShiftRotate"];
    static string _urlGetDetailCreateToAdminShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailCreateToAdminShiftRotate"];
    static string _urlGetDetailCreateToUserShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailCreateToUserShiftRotate"];
    static string _urlGetLogDetailCreateToUserShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetailCreateToUserShiftRotate"];
    static string _urlGetDetailApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailApproveOTShiftRotate"];
    static string _urlSetApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveOTShiftRotate"];
    static string _urlGetCountWaitApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveOTShiftRotate"];
    static string _urlGetStatusSearchOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetStatusSearchOTShiftRotate"];
    static string _urlGetUserSearchOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetUserSearchOTShiftRotate"];

    static string _urlGetReportOvertime = _serviceUrl + ConfigurationManager.AppSettings["urlGetReportOvertime"];
    static string _urlGetM0ShiftTypeRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0ShiftTypeRotate"];
    static string _urlGetViewPrintReportOvertime = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewPrintReportOvertime"];

    static string _path_file_otmonth = ConfigurationManager.AppSettings["path_file_otmonth"];
    //-- over time --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    string _otx1 = "x1";
    string _otx1_5 = "x1.5";
    string _otx2 = "x2";
    string _otx3 = "x3";

    decimal tot_actual_otmonth = 0;
    decimal tot_actual_otday = 0;
    decimal tot_actualoutplan = 0;

    decimal tot_actual_otbefore = 0;
    decimal tot_actual_otafter = 0;
    decimal tot_actual_otholiday = 0;

    decimal totactual_otbefore = 0;
    decimal totactual_otafter = 0;
    decimal totactual_otholiday = 0;

    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;
    string _sumhour = "";
    string _total_floor = "";
    string _total_time = "";
    //set rpos hr tab approve
    string set_rpos_idx_hr = "5903,5906";

    decimal total_otx15 = 0;
    decimal total_otx1 = 0;
    decimal total_otx2 = 0;
    decimal total_otx3 = 0;



    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type

        ViewState["Vs_check_tab_noidx"] = 0;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            //getCountWaitDetailApproveOTAdminCreate();

        }

    }

    #region set/get bind data

    protected void getM0TypeShift(DropDownList ddlName, int _TimeIDX)
    {

        data_overtime data_m0type_shift_detail = new data_overtime();
        ovt_m0type_shift_detail m0type_shift_detail = new ovt_m0type_shift_detail();
        data_m0type_shift_detail.ovt_m0type_shift_list = new ovt_m0type_shift_detail[1];

        data_m0type_shift_detail.ovt_m0type_shift_list[0] = m0type_shift_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0type_shift_detail = callServicePostOvertime(_urlGetM0ShiftTypeRotate, data_m0type_shift_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0type_detail));

        setDdlData(ddlName, data_m0type_shift_detail.ovt_m0type_shift_list, "t_type_name", "TimeIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทกะ ---", "0"));
    }

    protected void getStatusSearchDocument(DropDownList ddlName, int staidx)
    {

        data_overtime data_m0_status_detail = new data_overtime();
        ovt_m0_status_detail m0_status_detail = new ovt_m0_status_detail();
        data_m0_status_detail.ovt_m0_status_list = new ovt_m0_status_detail[1];

        data_m0_status_detail.ovt_m0_status_list[0] = m0_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_status_detail = callServicePostOvertime(_urlGetStatusSearchOTShiftRotate, data_m0_status_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0type_detail));

        setDdlData(ddlName, data_m0_status_detail.ovt_m0_status_list, "status_desc", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะรายการ ---", "0"));
    }

    protected void getWaitApproveDetailOTDayAdminCreate(GridView gvName, int m0_node_idx, int condition)
    {

        data_overtime data_u1doc_wait_detail = new data_overtime();
        ovt_u1doc_rotate_detail u1doc_wait_detail = new ovt_u1doc_rotate_detail();
        data_u1doc_wait_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        u1doc_wait_detail.emp_idx = _emp_idx;
        u1doc_wait_detail.m0_node_idx = m0_node_idx;
        u1doc_wait_detail.condition = condition;
        u1doc_wait_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u1doc_wait_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u1doc_wait_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u1doc_wait_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u1doc_wait_detail.ovt_u1doc_rotate_list[0] = u1doc_wait_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));
        data_u1doc_wait_detail = callServicePostOvertime(_urlGetDetailApproveOTShiftRotate, data_u1doc_wait_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));

        if (data_u1doc_wait_detail.return_code == 0)
        {
            ViewState["Vs_WaitApproveDetailOTShiftRotate"] = data_u1doc_wait_detail.ovt_u1doc_rotate_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }
        else
        {
            ViewState["Vs_WaitApproveDetailOTShiftRotate"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }

    }

    protected void getLogViewDetailOTShiftRotate(Repeater rptName, int _u0doc_idx, int _u1doc_idx)
    {
        data_overtime data_userlog_shiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail userlog_shiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];
        userlog_shiftrotate_detail.u0_doc_idx = _u0doc_idx;
        userlog_shiftrotate_detail.u1_doc_idx = _u1doc_idx;
        userlog_shiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list[0] = userlog_shiftrotate_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_userlog_shiftrotate_detail));
        data_userlog_shiftrotate_detail = callServicePostOvertime(_urlGetLogDetailCreateToUserShiftRotate, data_userlog_shiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["VsLog_DetailShiftRotateToUser"] = data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list;

        setRepeaterData(rptName, ViewState["VsLog_DetailShiftRotateToUser"]);

        //gvName.Visible = true;
        //setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getViewDetailShiftRotateUser(FormView fvName, int _u1doc_idx)
    {
        data_overtime data_user_viewshiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail user_viewshiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        user_viewshiftrotate_detail.u1_doc_idx = _u1doc_idx;
        //user_viewshiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list[0] = user_viewshiftrotate_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_viewshiftrotate_detail));
        data_user_viewshiftrotate_detail = callServicePostOvertime(_urlGetDetailCreateToUserShiftRotate, data_user_viewshiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["Vs_ViewDetailShiftRotateToUser"] = data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list;

        setFormData(fvName, FormViewMode.ReadOnly, ViewState["Vs_ViewDetailShiftRotateToUser"]);

        //gvName.Visible = true;
        //setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getDetailShiftRotateUser(GridView gvName, int _u1doc_idx)
    {
        data_overtime data_user_shiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail user_shiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_user_shiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        user_shiftrotate_detail.u1_doc_idx = _u1doc_idx;
        user_shiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_user_shiftrotate_detail.ovt_u1doc_rotate_list[0] = user_shiftrotate_detail;

        data_user_shiftrotate_detail = callServicePostOvertime(_urlGetDetailCreateToUserShiftRotate, data_user_shiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["Vs_DetailShiftRotateToUser"] = data_user_shiftrotate_detail.ovt_u1doc_rotate_list;

        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));

        ddlName.SelectedValue = ViewState["org_permission"].ToString();

    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกพนักงาน ---", "0"));
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion set/get bind data  

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdCancel":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewAdminShiftRotate":
                setActiveTab("docDetailAdmin", int.Parse(cmdArg.ToString()), 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetailAdmin":
                setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdViewUserShiftRotate":

                string[] arg_viewuser = new string[1];
                arg_viewuser = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewuser = int.Parse(arg_viewuser[0]);
                int _u1_doc_idx_viewuser = int.Parse(arg_viewuser[1]);
                // string _decision_name_saveotday = arg_viewuser[1];

                setActiveTab("docDetail", _u0_doc_idx_viewuser, 0, 0, 0, 0, 0, _u1_doc_idx_viewuser);
                break;
            case "cmdDocCancel":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdSearchReport":

                _PanelExportExcelReportMonth.Visible = false;
                _PanelExportExcelTotalReportMonth.Visible = false;

                GvReportOvertime.Visible = false;
                GvReportTotalOvertime.Visible = false;

                btnSearchReportMonthx0.CssClass = "btn btn-default";
                btnSearchReportMonthx1.CssClass = "btn btn-default";
                btnSearchReportMonthx2.CssClass = "btn btn-default";
                btnSearchReportMonthx3.CssClass = "btn btn-default";
                btnSearchTotalReport.CssClass = "btn btn-default";


                switch (int.Parse(cmdArg))
                {
                    case 0: // ot x1

                        btnSearchReportMonthx0.CssClass = "btn btn-success";

                        data_overtime data_search_reportmonth = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail = new ovt_report_otmonth_detail();
                        data_search_reportmonth.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail.cemp_idx = _emp_idx;
                        search_reportmonth_detail.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail.condition = 1;
                        search_reportmonth_detail.condition_ot = _otx1;
                        search_reportmonth_detail.org_idx = int.Parse(ddlorgReport.SelectedValue);
                        search_reportmonth_detail.rdept_idx = int.Parse(ddlrdeptReport.SelectedValue);
                        search_reportmonth_detail.rsec_idx = int.Parse(ddlrsecReport.SelectedValue);
                        search_reportmonth_detail.emp_idx = int.Parse(ddlempReport.SelectedValue);
                        search_reportmonth_detail.costcenter = txtCostcenterReport.Text;
                        search_reportmonth_detail.time_idx = int.Parse(ddlM0Shift.SelectedValue);
                        search_reportmonth_detail.date_start = txt_start_search.Text;
                        search_reportmonth_detail.date_end = txt_end_search.Text;

                        data_search_reportmonth.ovt_report_otmonth_list[0] = search_reportmonth_detail;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth = callServicePostOvertime(_urlGetReportOvertime, data_search_reportmonth);
                        if (data_search_reportmonth.return_code == 0)
                        {

                            ViewState["Vs_SearchReportOvertime"] = data_search_reportmonth.ovt_report_otmonth_list;

                            data_overtime data_report_detail = new data_overtime();
                            ovt_report_otmonth_detail[] _item_report = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportOvertime"];

                            var _linq_report = (from data in _item_report
                                                where
                                                Convert.ToDouble(data.ot_before) > 0.0
                                                || Convert.ToDouble(data.ot_after) > 0.0
                                                || Convert.ToDouble(data.ot_holiday) > 0.0

                                                select data
                                                    ).ToList();


                            if (_linq_report.Count > 0)//have data
                            {

                                _PanelExportExcelReportMonth.Visible = true;
                                GvReportOvertime.Visible = true;
                                //litDebug.Text = "have data";

                                ViewState["Vs_ReportOvertime"] = _linq_report;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                            }
                            else
                            {
                                //_PanelExportExcelReportMonth.Visible = false;
                                //GvReportOvertime.Visible = false;
                                //litDebug.Text = "no have data";
                                ViewState["Vs_ReportOvertime"] = null;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);


                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                                break;

                            }


                        }
                        else
                        {
                            ViewState["Vs_ReportOvertime"] = null;

                            //GvReportOvertime.Visible = true;
                            setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                        break;
                    case 1: // ot x1.5
                        btnSearchReportMonthx1.CssClass = "btn btn-success";

                        data_overtime data_search_reportmonth_otx1_5 = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail_otx1_5 = new ovt_report_otmonth_detail();
                        data_search_reportmonth_otx1_5.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail_otx1_5.cemp_idx = _emp_idx;
                        search_reportmonth_detail_otx1_5.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.condition = 2;
                        search_reportmonth_detail_otx1_5.condition_ot = _otx1_5;
                        search_reportmonth_detail_otx1_5.org_idx = int.Parse(ddlorgReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.rdept_idx = int.Parse(ddlrdeptReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.rsec_idx = int.Parse(ddlrsecReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.emp_idx = int.Parse(ddlempReport.SelectedValue);
                        search_reportmonth_detail_otx1_5.costcenter = txtCostcenterReport.Text;
                        search_reportmonth_detail_otx1_5.time_idx = int.Parse(ddlM0Shift.SelectedValue);
                        search_reportmonth_detail_otx1_5.date_start = txt_start_search.Text;
                        search_reportmonth_detail_otx1_5.date_end = txt_end_search.Text;

                        data_search_reportmonth_otx1_5.ovt_report_otmonth_list[0] = search_reportmonth_detail_otx1_5;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth_otx1_5));
                        data_search_reportmonth_otx1_5 = callServicePostOvertime(_urlGetReportOvertime, data_search_reportmonth_otx1_5);
                        if (data_search_reportmonth_otx1_5.return_code == 0)
                        {

                            ViewState["Vs_SearchReportOvertime"] = data_search_reportmonth_otx1_5.ovt_report_otmonth_list;

                            data_overtime data_report_detail = new data_overtime();
                            ovt_report_otmonth_detail[] _item_report = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportOvertime"];

                            var _linq_report = (from data in _item_report
                                                where
                                                Convert.ToDouble(data.ot_before) > 0.0
                                                || Convert.ToDouble(data.ot_after) > 0.0
                                                || Convert.ToDouble(data.ot_holiday) > 0.0

                                                select data
                                                    ).ToList();


                            //
                            if (_linq_report.Count > 0)//have data
                            {

                                _PanelExportExcelReportMonth.Visible = true;
                                GvReportOvertime.Visible = true;
                                //litDebug.Text = "have data";

                                ViewState["Vs_ReportOvertime"] = _linq_report;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                            }
                            else
                            {
                                //_PanelExportExcelReportMonth.Visible = false;
                                //GvReportOvertime.Visible = false;
                                //litDebug.Text = "no have data";
                                ViewState["Vs_ReportOvertime"] = null;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);


                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                                break;

                            }


                        }
                        else
                        {
                            ViewState["Vs_ReportOvertime"] = null;

                            //GvReportOvertime.Visible = true;
                            setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                        break;
                    case 2: // ot x2
                        btnSearchReportMonthx2.CssClass = "btn btn-success";

                        data_overtime data_search_reportmonth_otx2 = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail_otx2 = new ovt_report_otmonth_detail();
                        data_search_reportmonth_otx2.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail_otx2.cemp_idx = _emp_idx;
                        search_reportmonth_detail_otx2.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail_otx2.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail_otx2.condition = 4;
                        search_reportmonth_detail_otx2.condition_ot = _otx2;
                        search_reportmonth_detail_otx2.org_idx = int.Parse(ddlorgReport.SelectedValue);
                        search_reportmonth_detail_otx2.rdept_idx = int.Parse(ddlrdeptReport.SelectedValue);
                        search_reportmonth_detail_otx2.rsec_idx = int.Parse(ddlrsecReport.SelectedValue);
                        search_reportmonth_detail_otx2.emp_idx = int.Parse(ddlempReport.SelectedValue);
                        search_reportmonth_detail_otx2.costcenter = txtCostcenterReport.Text;
                        search_reportmonth_detail_otx2.time_idx = int.Parse(ddlM0Shift.SelectedValue);
                        search_reportmonth_detail_otx2.date_start = txt_start_search.Text;
                        search_reportmonth_detail_otx2.date_end = txt_end_search.Text;

                        data_search_reportmonth_otx2.ovt_report_otmonth_list[0] = search_reportmonth_detail_otx2;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth_otx2 = callServicePostOvertime(_urlGetReportOvertime, data_search_reportmonth_otx2);
                        if (data_search_reportmonth_otx2.return_code == 0)
                        {

                            ViewState["Vs_SearchReportOvertime"] = data_search_reportmonth_otx2.ovt_report_otmonth_list;

                            data_overtime data_report_detail = new data_overtime();
                            ovt_report_otmonth_detail[] _item_report = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportOvertime"];

                            var _linq_report = (from data in _item_report
                                                where
                                                Convert.ToDouble(data.ot_before) > 0.0
                                                || Convert.ToDouble(data.ot_after) > 0.0
                                                || Convert.ToDouble(data.ot_holiday) > 0.0

                                                select data
                                                    ).ToList();


                            //
                            if (_linq_report.Count > 0)//have data
                            {

                                _PanelExportExcelReportMonth.Visible = true;
                                GvReportOvertime.Visible = true;
                                //litDebug.Text = "have data";

                                ViewState["Vs_ReportOvertime"] = _linq_report;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                            }
                            else
                            {
                                //_PanelExportExcelReportMonth.Visible = false;
                                //GvReportOvertime.Visible = false;
                                //litDebug.Text = "no have data";
                                ViewState["Vs_ReportOvertime"] = null;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);


                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                                break;

                            }


                        }
                        else
                        {
                            ViewState["Vs_ReportOvertime"] = null;

                            //GvReportOvertime.Visible = true;
                            setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                        break;
                    case 3: // ot x3

                        btnSearchReportMonthx3.CssClass = "btn btn-success";

                        data_overtime data_search_reportmonth_otx3 = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail_otx3 = new ovt_report_otmonth_detail();
                        data_search_reportmonth_otx3.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail_otx3.cemp_idx = _emp_idx;
                        search_reportmonth_detail_otx3.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail_otx3.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail_otx3.condition = 3;
                        search_reportmonth_detail_otx3.condition_ot = _otx3;
                        search_reportmonth_detail_otx3.org_idx = int.Parse(ddlorgReport.SelectedValue);
                        search_reportmonth_detail_otx3.rdept_idx = int.Parse(ddlrdeptReport.SelectedValue);
                        search_reportmonth_detail_otx3.rsec_idx = int.Parse(ddlrsecReport.SelectedValue);
                        search_reportmonth_detail_otx3.emp_idx = int.Parse(ddlempReport.SelectedValue);
                        search_reportmonth_detail_otx3.costcenter = txtCostcenterReport.Text;
                        search_reportmonth_detail_otx3.time_idx = int.Parse(ddlM0Shift.SelectedValue);
                        search_reportmonth_detail_otx3.date_start = txt_start_search.Text;
                        search_reportmonth_detail_otx3.date_end = txt_end_search.Text;

                        data_search_reportmonth_otx3.ovt_report_otmonth_list[0] = search_reportmonth_detail_otx3;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                        data_search_reportmonth_otx3 = callServicePostOvertime(_urlGetReportOvertime, data_search_reportmonth_otx3);
                        if (data_search_reportmonth_otx3.return_code == 0)
                        {

                            ViewState["Vs_SearchReportOvertime"] = data_search_reportmonth_otx3.ovt_report_otmonth_list;

                            data_overtime data_report_detail = new data_overtime();
                            ovt_report_otmonth_detail[] _item_report = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportOvertime"];

                            var _linq_report = (from data in _item_report
                                                where
                                                Convert.ToDouble(data.ot_before) > 0.0
                                                || Convert.ToDouble(data.ot_after) > 0.0
                                                || Convert.ToDouble(data.ot_holiday) > 0.0

                                                select data
                                                    ).ToList();


                            //
                            if (_linq_report.Count > 0)//have data
                            {

                                _PanelExportExcelReportMonth.Visible = true;
                                GvReportOvertime.Visible = true;
                                //litDebug.Text = "have data";

                                ViewState["Vs_ReportOvertime"] = _linq_report;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                            }
                            else
                            {
                                _PanelExportExcelReportMonth.Visible = false;
                                GvReportOvertime.Visible = false;
                                //litDebug.Text = "no have data";
                                ViewState["Vs_ReportOvertime"] = null;
                                setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);


                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                                break;

                            }


                        }
                        else
                        {
                            ViewState["Vs_ReportOvertime"] = null;

                            //GvReportOvertime.Visible = true;
                            setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                        break;
                    case 4:

                        btnSearchTotalReport.CssClass = "btn btn-success";


                        data_overtime data_search_reportmonth_ottotal = new data_overtime();
                        ovt_report_otmonth_detail search_reportmonth_detail_ottotal = new ovt_report_otmonth_detail();
                        data_search_reportmonth_ottotal.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        search_reportmonth_detail_ottotal.cemp_idx = _emp_idx;
                        search_reportmonth_detail_ottotal.month_idx = int.Parse(ddlMonthReport.SelectedValue);
                        search_reportmonth_detail_ottotal.year_idx = int.Parse(ddlYearReport.SelectedValue);
                        search_reportmonth_detail_ottotal.condition = 5;
                        //search_reportmonth_detail_ottotal.condition_ot = _otx3;
                        search_reportmonth_detail_ottotal.org_idx = int.Parse(ddlorgReport.SelectedValue);
                        search_reportmonth_detail_ottotal.rdept_idx = int.Parse(ddlrdeptReport.SelectedValue);
                        search_reportmonth_detail_ottotal.rsec_idx = int.Parse(ddlrsecReport.SelectedValue);
                        search_reportmonth_detail_ottotal.emp_idx = int.Parse(ddlempReport.SelectedValue);
                        search_reportmonth_detail_ottotal.costcenter = txtCostcenterReport.Text;
                        search_reportmonth_detail_ottotal.time_idx = int.Parse(ddlM0Shift.SelectedValue);
                        search_reportmonth_detail_ottotal.date_start = txt_start_search.Text;
                        search_reportmonth_detail_ottotal.date_end = txt_end_search.Text;



                        data_search_reportmonth_ottotal.ovt_report_otmonth_list[0] = search_reportmonth_detail_ottotal;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth_ottotal));

                        data_search_reportmonth_ottotal = callServicePostOvertime(_urlGetReportOvertime, data_search_reportmonth_ottotal);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth_ottotal));
                        if (data_search_reportmonth_ottotal.return_code == 0)
                        {

                            ViewState["Vs_SearchReportTotalOvertime"] = data_search_reportmonth_ottotal.ovt_report_otmonth_list;

                            data_overtime data_report_detail = new data_overtime();
                            ovt_report_otmonth_detail[] _item_report = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportTotalOvertime"];

                            var _linq_report = (from data in _item_report
                                                where
                                                Convert.ToDouble(data.ot_x15) > 0.0
                                                || Convert.ToDouble(data.ot_x1) > 0.0
                                                || Convert.ToDouble(data.ot_x2) > 0.0
                                                || Convert.ToDouble(data.ot_x3) > 0.0

                                                select data
                                                    ).ToList();

                            //
                            if (_linq_report.Count > 0)//have data
                            {

                                _PanelExportExcelTotalReportMonth.Visible = true;
                                GvReportTotalOvertime.Visible = true;
                                //litDebug.Text = "have data";

                                ViewState["Vs_ReportTotalOvertime"] = _linq_report;
                                //setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);

                                ViewState["Vs_DateStart_Report"] = txt_start_search.Text;
                                ViewState["Vs_DateEnd_Report"] = txt_end_search.Text;

                                if (ddlMonthReport.SelectedValue != "0")
                                {
                                    ViewState["VsMonth_Shift_FixPrint"] = ddlMonthReport.SelectedItem.Text;
                                    ViewState["VsYear_Shift_FixPrint"] = ddlYearReport.SelectedItem.Text;
                                }
                                else
                                {
                                    ViewState["VsMonth_Shift_FixPrint"] = "";
                                    ViewState["VsYear_Shift_FixPrint"] = "";
                                }


                                if (txt_start_search.Text != "" && txt_end_search.Text != "")
                                {
                                    ViewState["Vs_ShiftStart_RotatePrint"] = txt_start_search.Text;
                                    ViewState["Vs_ShiftEnd_RotatePrint"] = txt_end_search.Text;
                                }
                                else
                                {
                                    ViewState["Vs_ShiftStart_RotatePrint"] = "";
                                    ViewState["Vs_ShiftEnd_RotatePrint"] = "";
                                }



                                setGridData(GvReportTotalOvertime, ViewState["Vs_ReportTotalOvertime"]);

                            }
                            else
                            {
                                _PanelExportExcelTotalReportMonth.Visible = false;
                                GvReportTotalOvertime.Visible = false;
                                //litDebug.Text = "no have data";
                                ViewState["Vs_ReportTotalOvertime"] = null;
                                //setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                                setGridData(GvReportTotalOvertime, ViewState["Vs_ReportTotalOvertime"]);

                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                                break;

                            }


                        }
                        else
                        {
                            ViewState["Vs_ReportTotalOvertime"] = null;

                            //GvReportOvertime.Visible = true;
                            //setGridData(GvReportOvertime, ViewState["Vs_ReportOvertime"]);
                            setGridData(GvReportTotalOvertime, ViewState["Vs_ReportTotalOvertime"]);
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                        break;
                }
                break;
            case "cmdResetSearchReport":

                setActiveTab("docReport", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToSearchReport":

                setActiveTab("docReport", 0, 0, 0, 0, 0, 0, 0);


                break;

            case "cmdExportExcelReportMonth":
                int count_export = 0;

                ovt_report_otmonth_detail[] _templist_ReportOTMonth = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportOvertime"];

                DataTable tableReportOtMonth = new DataTable();
                tableReportOtMonth.Columns.Add("รหัสพนักงาน", typeof(String));
                tableReportOtMonth.Columns.Add("วันที่จ่ายเงิน", typeof(String));
                tableReportOtMonth.Columns.Add("ประเภทเงินได้", typeof(String));
                tableReportOtMonth.Columns.Add("จำนวนเงิน", typeof(String));
                tableReportOtMonth.Columns.Add("จำนวน", typeof(String));
                tableReportOtMonth.Columns.Add("หน่วย", typeof(String));
                tableReportOtMonth.Columns.Add("อัตรา", typeof(String));
                tableReportOtMonth.Columns.Add("หมายเหตุ", typeof(String));


                var linqRoomDetailCheck = (from _dt_ReportExcelOtMonth in _templist_ReportOTMonth
                                           where
                                               Convert.ToDouble(_dt_ReportExcelOtMonth.ot_before) > 0.0
                                               || Convert.ToDouble(_dt_ReportExcelOtMonth.ot_after) > 0.0
                                               || Convert.ToDouble(_dt_ReportExcelOtMonth.ot_holiday) > 0.0

                                           select _dt_ReportExcelOtMonth
                                           ).ToList();

                foreach (var row_report in linqRoomDetailCheck)
                {
                    DataRow addRowExcel = tableReportOtMonth.NewRow();

                    IFormatProvider culture_ = new CultureInfo("en-US", true);
                    var d_payment = new DateTime();
                    d_payment = DateTime.ParseExact((row_report.date_payment.ToString()), "dd/MM/yyyy", culture_);

                    addRowExcel[0] = row_report.emp_code.ToString();

                    if (d_payment.ToString("dd") == "31")
                    {

                        addRowExcel[1] = d_payment.AddDays(-1).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        addRowExcel[1] = d_payment.ToString("dd/MM/yyyy");
                    }


                    addRowExcel[2] = row_report.condition_otmonth.ToString();
                    addRowExcel[3] = "";
                    addRowExcel[4] = row_report.ot_total.ToString();
                    addRowExcel[5] = "ชั่วโมง";
                    addRowExcel[6] = "";
                    addRowExcel[7] = row_report.remark_otmonth.ToString();

                    tableReportOtMonth.Rows.InsertAt(addRowExcel, count_export++);
                }
                //WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
                WriteExcelWithNPOI(tableReportOtMonth, "xls", "report-otmonth");


                break;

            case "cmdExportExcelTotalReportMonth":
                int count_exporttotal = 0;

                ovt_report_otmonth_detail[] _templist_ReportOTMonthTotal = (ovt_report_otmonth_detail[])ViewState["Vs_SearchReportTotalOvertime"];

                DataTable tableReportOtMonth_Total = new DataTable();
                tableReportOtMonth_Total.Columns.Add("รหัสพนักงาน", typeof(String));
                tableReportOtMonth_Total.Columns.Add("ชื่อพนักงาน", typeof(String));
                tableReportOtMonth_Total.Columns.Add("แผนก", typeof(String));
                tableReportOtMonth_Total.Columns.Add("OTx1.5", typeof(String));
                tableReportOtMonth_Total.Columns.Add("OTx1", typeof(String));
                tableReportOtMonth_Total.Columns.Add("OTx2", typeof(String));
                tableReportOtMonth_Total.Columns.Add("OTx3", typeof(String));
                tableReportOtMonth_Total.Columns.Add("รวมชั่วโมงOT", typeof(String));


                var linqRoomDetailCheck_Total = (from _dt_ReportExcelOtMonth_total in _templist_ReportOTMonthTotal
                                                 where
                                                     Convert.ToDouble(_dt_ReportExcelOtMonth_total.ot_x15) > 0.0
                                                     || Convert.ToDouble(_dt_ReportExcelOtMonth_total.ot_x1) > 0.0
                                                     || Convert.ToDouble(_dt_ReportExcelOtMonth_total.ot_x2) > 0.0
                                                     || Convert.ToDouble(_dt_ReportExcelOtMonth_total.ot_x3) > 0.0

                                                 select _dt_ReportExcelOtMonth_total
                                           ).ToList();

                foreach (var row_report in linqRoomDetailCheck_Total)
                {
                    DataRow addRowExcel = tableReportOtMonth_Total.NewRow();

                    //IFormatProvider culture_ = new CultureInfo("en-US", true);
                    //var d_payment = new DateTime();
                    //d_payment = DateTime.ParseExact((row_report.date_payment.ToString()), "dd/MM/yyyy", culture_);

                    addRowExcel[0] = row_report.emp_code.ToString();

                    ////if (d_payment.ToString("dd") == "31")
                    ////{

                    ////    addRowExcel[1] = d_payment.AddDays(-1).ToString("dd/MM/yyyy");
                    ////}
                    ////else
                    ////{
                    ////    addRowExcel[1] = d_payment.ToString("dd/MM/yyyy");
                    ////}


                    addRowExcel[1] = row_report.emp_name_th.ToString();
                    addRowExcel[2] = row_report.dept_name_th.ToString();
                    addRowExcel[3] = row_report.ot_x15.ToString();
                    addRowExcel[4] = row_report.ot_x1.ToString();
                    addRowExcel[5] = row_report.ot_x2.ToString();
                    addRowExcel[6] = row_report.ot_x3.ToString();
                    addRowExcel[7] = row_report.total_hours.ToString();

                    tableReportOtMonth_Total.Rows.InsertAt(addRowExcel, count_exporttotal++);
                }
                //WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
                WriteExcelWithNPOI(tableReportOtMonth_Total, "xls", "report-otmonth_total");


                break;

            case "cmdViewTotalReportOvt":

                string[] arg_viewreport = new string[1];
                arg_viewreport = e.CommandArgument.ToString().Split(';');
                string _u1_docidxreport = arg_viewreport[0];

                //litDebug.Text = _u1_docidxreport.ToString();

                ViewState["Vs_u1idx_viewreport"] = _u1_docidxreport.ToString();


                setActiveTab("docReport", 1, 0, 0, 0, 0, 0, 0);
                break;


        }

    }
    //endbtn

    #endregion event command

    #region Changed
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {
            case "chkallApprove":
                int chack_all = 0;

                //if (chkallApprove.Checked)
                //{

                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = true;
                //        chack_all++;
                //    }
                //}
                //else
                //{
                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = false;
                //        chack_all++;
                //    }
                //}
                break;

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        //DropDownList ddlorg_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
        //DropDownList ddldep_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
        //DropDownList ddlsec_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");

        switch (ddlName.ID)
        {
            case "ddlorgReport":

                getDepartmentList(ddlrdeptReport, int.Parse(ddlorgReport.SelectedValue));

                ddlrsecReport.Items.Clear();
                ddlrsecReport.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlempReport.Items.Clear();
                ddlempReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlrdeptReport":

                getSectionList(ddlrsecReport, int.Parse(ddlorgReport.SelectedItem.Value), int.Parse(ddlrdeptReport.SelectedItem.Value));


                ddlempReport.Items.Clear();
                ddlempReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

                break;
            case "ddlrsecReport":
                getEmpList(ddlempReport, int.Parse(ddlorgReport.SelectedValue), int.Parse(ddlrdeptReport.SelectedValue), int.Parse(ddlrsecReport.SelectedValue));

                break;
            case "ddlM0Shift":

                div_reportshift_fix.Visible = false;
                div_reportshift_rotate.Visible = false;

                if (ddlM0Shift.SelectedValue == "1") //คงที่
                {
                    div_reportshift_fix.Visible = true;
                }
                else if (ddlM0Shift.SelectedValue == "2") //หมุนเวียน
                {
                    div_reportshift_rotate.Visible = true;
                }

                break;



        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {

            ////case "txt_timeend_job":

            ////    //linkBtnTrigger(btnSaveDetailMA);

            ////    foreach (GridViewRow row in GvCreateOTMonth.Rows)
            ////    {
            ////        CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
            ////        UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
            ////        UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
            ////        TextBox txt_job = (TextBox)row.FindControl("txt_job");
            ////        TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
            ////        TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
            ////        TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
            ////        TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
            ////        TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
            ////        decimal total_hour = 0;


            ////        //TimeSpan diff = secondDate - firstDate;
            ////        //double hours = diff.TotalHours;

            ////        litDebug.Text = "3333";

            ////        if (chk_date_ot.Checked)
            ////        {
            ////            if (txt_timestart_job.Text != "" && txt_timeend_job.Text != "")
            ////            {
            ////                //linkBtnTrigger(btnSaveDetailMA);
            ////                //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
            ////                total_hour = Convert.ToDecimal(txt_timestart_job.Text) * Convert.ToDecimal(txt_timeend_job.Text);
            ////                //litDebug.Text = Convert.ToDecimal(total_price).ToString();
            ////                txt_sum_hour.Text = Convert.ToDecimal(total_hour).ToString();

            ////            }
            ////            else
            ////            {
            ////                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

            ////                break;
            ////            }
            ////        }


            ////    }

            ////    break;




        }

    }

    #endregion Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetailOTShiftRotate":
                //setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);

                break;

        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvCreateShiftRotate":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {



                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {


                }

                break;
            case "gvPrintReport":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_ot_x15_viewreport = (Label)e.Row.FindControl("lbl_ot_x15_viewreport");
                    Label lbl_ot_x1_viewreport = (Label)e.Row.FindControl("lbl_ot_x1_viewreport");
                    Label lbl_ot_x2_viewreport = (Label)e.Row.FindControl("lbl_ot_x2_viewreport");
                    Label lbl_ot_x3_viewreport = (Label)e.Row.FindControl("lbl_ot_x3_viewreport");


                    total_otx15 += Convert.ToDecimal(lbl_ot_x15_viewreport.Text);
                    total_otx1 += Convert.ToDecimal(lbl_ot_x1_viewreport.Text);
                    total_otx2 += Convert.ToDecimal(lbl_ot_x2_viewreport.Text);
                    total_otx3 += Convert.ToDecimal(lbl_ot_x3_viewreport.Text);

                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {

                    Label lit_total_hoursot_x15 = (Label)e.Row.FindControl("lit_total_hoursot_x15");
                    lit_total_hoursot_x15.Text = String.Format("{0:N2}", total_otx15); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_x1 = (Label)e.Row.FindControl("lit_total_hoursot_x1");
                    lit_total_hoursot_x1.Text = String.Format("{0:N2}", total_otx1); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_x2 = (Label)e.Row.FindControl("lit_total_hoursot_x2");
                    lit_total_hoursot_x2.Text = String.Format("{0:N2}", total_otx2); // Convert.ToString(tot_actual);

                    Label lit_total_hoursot_x3 = (Label)e.Row.FindControl("lit_total_hoursot_x3");
                    lit_total_hoursot_x3.Text = String.Format("{0:N2}", total_otx3); // Convert.ToString(tot_actual);


                    ViewState["VsTotal_hour_overtime"] = String.Format("{0:N2}", total_otx15 + total_otx1 + total_otx2 + total_otx3);
                    lbl_total_hourovertime.Text = ViewState["VsTotal_hour_overtime"].ToString();
                }

                break;


        }
    }

    protected void getHourToOT(DropDownList ddlNane, double _hour)
    {
        ddlNane.AppendDataBoundItems = true;
        ddlNane.Items.Clear();



        if (Convert.ToDouble(_hour) >= 0.5)
        {
            double _timescan = Convert.ToDouble(_hour);
            for (double i = 0.0; i <= _timescan; i += 0.5)
            {
                ddlNane.Items.Add((i).ToString());
            }

            ddlNane.Items.FindByValue(Convert.ToString(_timescan)).Selected = true;

        }
        else
        {
            ddlNane.Visible = false;
            ddlNane.SelectedValue = "0";
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docReport", 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_overtime callServicePostOvertime(string _cmdUrl, data_overtime _data_overtime)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_overtime);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_overtime = (data_overtime)_funcTool.convertJsonToObject(typeof(data_overtime), _localJson);

        return _data_overtime;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        //setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        _PanelReportOTMonth.Visible = false;
        ddlMonthReport.ClearSelection();
        txtCostcenterReport.Text = string.Empty;

        div_reportshift_fix.Visible = false;
        div_reportshift_rotate.Visible = false;
        txt_start_search.Text = string.Empty;
        txt_end_search.Text = string.Empty;

        //set tab
        GvReportOvertime.Visible = false;
        setGridData(GvReportOvertime, null);

        GvReportTotalOvertime.Visible = false;
        setGridData(GvReportTotalOvertime, null);

        _PanelExportExcelReportMonth.Visible = false;
        _PanelExportExcelTotalReportMonth.Visible = false;

        btnSearchReportMonthx0.CssClass = "btn btn-default";
        btnSearchReportMonthx1.CssClass = "btn btn-default";
        btnSearchReportMonthx2.CssClass = "btn btn-default";
        btnSearchReportMonthx3.CssClass = "btn btn-default";
        btnSearchTotalReport.CssClass = "btn btn-default";

        //tab print report 
        div_PrintReport.Visible = false;


        switch (activeTab)
        {
            case "docReport":

                switch (uidx)
                {
                    case 0:

                        _PanelReportOTMonth.Visible = true;

                        getddlYear(ddlYearReport);
                        getOrganizationList(ddlorgReport);
                        getDepartmentList(ddlrdeptReport, int.Parse(ddlorgReport.SelectedValue));
                        getSectionList(ddlrsecReport, int.Parse(ddlorgReport.SelectedValue), int.Parse(ddlrdeptReport.SelectedValue));
                        getM0TypeShift(ddlM0Shift, 0);

                        break;
                    case 1:

                        //litDebug.Text = ViewState["Vs_u1idx_viewreport"].ToString();


                        data_overtime data_view_reportmonth = new data_overtime();
                        ovt_report_otmonth_detail view_reportmonth_detail = new ovt_report_otmonth_detail();
                        data_view_reportmonth.ovt_report_otmonth_list = new ovt_report_otmonth_detail[1];

                        view_reportmonth_detail.cemp_idx = _emp_idx;
                        view_reportmonth_detail.condition = int.Parse(ddlM0Shift.SelectedValue);
                        view_reportmonth_detail.u0_doc1_idx_value = ViewState["Vs_u1idx_viewreport"].ToString();


                        data_view_reportmonth.ovt_report_otmonth_list[0] = view_reportmonth_detail;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_view_reportmonth));
                        data_view_reportmonth = callServicePostOvertime(_urlGetViewPrintReportOvertime, data_view_reportmonth);
                        
                        if (data_view_reportmonth.return_code == 0)
                        {
                            div_PrintReport.Visible = true;

                            ViewState["Vs_ViewPrintReportOvertime"] = data_view_reportmonth.ovt_report_otmonth_list;

                            lbl_org_name_th_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].org_name_th.ToString();
                            lbl_parttime_name_th_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].parttime_name_th.ToString();

                            //shift fix
                            if (ViewState["VsMonth_Shift_FixPrint"].ToString() != "")
                            {
                                div_shiftfix.Visible = true;

                                lbl_shift_fixprint_month.Text = ViewState["VsMonth_Shift_FixPrint"].ToString();
                                lbl_shift_fixprint_year.Text = ViewState["VsYear_Shift_FixPrint"].ToString();
                            }
                            else
                            {
                                div_shiftfix.Visible = false;

                                lbl_shift_fixprint_month.Text = "";
                                lbl_shift_fixprint_year.Text = "";
                            }

                            //shift rotate
                            if ((ViewState["Vs_ShiftStart_RotatePrint"].ToString() != "" && ViewState["Vs_ShiftEnd_RotatePrint"].ToString() != "") && ddlM0Shift.SelectedValue == "2")
                            {
                                div_shiftrotate.Visible = true;

                                lbl_shiftstart_rotateprint.Text = ViewState["Vs_ShiftStart_RotatePrint"].ToString();
                                lbl_shiftend_rotateprint.Text = ViewState["Vs_ShiftEnd_RotatePrint"].ToString();
                            }
                            else
                            {
                                div_shiftrotate.Visible = false;

                                lbl_shiftstart_rotateprint.Text = "";
                                lbl_shiftend_rotateprint.Text = "";
                            }


                            lbl_emp_code_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].emp_code.ToString();
                            lbl_emp_name_th_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].emp_name_th.ToString();
                            lbl_pos_name_th_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].pos_name_th.ToString();
                            lbl_dept_name_th_viewreport.Text = data_view_reportmonth.ovt_report_otmonth_list[0].dept_name_th.ToString();

                            lbl_emp_name_th_print.Text = data_view_reportmonth.ovt_report_otmonth_list[0].emp_name_th.ToString();
                            lbl_emp_approve1_print.Text = data_view_reportmonth.ovt_report_otmonth_list[0].emp_approve1.ToString();
                            lbl_emp_approve2_print.Text = data_view_reportmonth.ovt_report_otmonth_list[0].emp_approve2.ToString();


                            setGridData(gvPrintReport, ViewState["Vs_ViewPrintReportOvertime"]);
                        }
                        else
                        {

                        }


                        break;
                }


                setOntop.Focus();
                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docReport":
                li0.Attributes.Add("class", "active");

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                //{
                //    //Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                //    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler_approve");
                //    var btnApprove = (LinkButton)e.Item.FindControl("btnApprove");

                //    for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                //    {
                //        btnApprove.CssClass = ConfigureColors(k);
                //        //Console.WriteLine(i);
                //    }


                //}
                break;

        }
    }

    #endregion reuse

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDay(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDayHR(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    #region data excel

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        ////int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            ////var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            ////sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    #endregion data excel
}