<%@ Page Title="Visitor Management" Language="C#" MasterPageFile="~/masterpage/master_blank.master" AutoEventWireup="true" CodeFile="security_login.aspx.cs" Inherits="websystem_visitors_security_login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="row bg-login-visitor">
        <div class="row vertical-offset-100">
            <div class="col-md-3 col-md-offset-7">
                <!-- <div class="panel panel-default"> -->
                <div>
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Visitor Management</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <asp:TextBox ID="tbUserName" runat="server" CssClass="form-control" placeholder="Username" MaxLength="20" ValidationGroup="formLogin"></asp:textbox>
                                <asp:RequiredFieldValidator ID="rfvEmpCode" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbUserName" ErrorMessage="กรุณากรอก User name" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpCode" TargetControlID="rfvEmpCode" HighlightCssClass="validatorCalloutHighlight" />
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="tbUserPassword" runat="server" CssClass="form-control" placeholder="Password" MaxLength="20" TextMode="Password" ValidationGroup="formLogin"></asp:textbox>
                                <asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formLogin" runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbUserPassword" ErrorMessage="กรุณากรอก Password" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass" TargetControlID="rfvEmpPass" HighlightCssClass="validatorCalloutHighlight" />
                            </div>
                            <asp:Button ID="btnLogin" CssClass="btn btn-lg btn-success btn-block" runat="server" data-original-title="Login" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdLogin" Text="Login" ValidationGroup="formLogin" />
                        </fieldset>
                    </div>
                </div>
                <div ID="divShowError" runat="server" class="alert alert-danger" role="alert" Visible="False">
                    <strong>Error <asp:Literal ID="litErrorCode" runat="server"></asp:Literal> : </strong>
                    Username หรือ Password ไม่ถูกต้อง
                </div>
            </div>
        </div>
    </div>
</asp:Content>