﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int announceGroupDiaryIdx = 0;
      int empIdxGroup = 0;
      int creator = 0;
      int.TryParse(context.Request.QueryString["announceGroupDiaryIdx"], out announceGroupDiaryIdx);
      int.TryParse(context.Request.QueryString["empIdxGroup"], out empIdxGroup);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      string json = this.createEmp(announceGroupDiaryIdx, empIdxGroup, creator);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createEmp(int announceGroupDiaryIdx, int empIdxGroup, int creator)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "INSERT INTO emps_u2_announce_diary (u0_announce_diary_idx_ref, announce_diary_emp_idx_ref, announce_diary_status, announce_diary_updated_at, announce_diary_updated_by) VALUES (@announceGroupDiaryIdx, @empIdxGroup, 1, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@announceGroupDiaryIdx", announceGroupDiaryIdx);
            cmd.Parameters.AddWithValue("@empIdxGroup", empIdxGroup);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.ExecuteNonQuery();
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
