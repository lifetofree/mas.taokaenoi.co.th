﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_product")]
public class data_product
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("M0_ProductType_list")]
    public M0_ProductType_detail[] M0_ProductType_list { get; set; }
    [XmlElement("M0_ProductList_list")]
    public M0_ProductList_detail[] M0_ProductList_list { get; set; }

}

[Serializable]
public class M0_ProductType_detail
{
    [XmlElement("ProTIDX")]
    public int ProTIDX { get; set; }
    [XmlElement("ProTName")]
    public string ProTName { get; set; }
    [XmlElement("ProTStatus")]
    public int ProTStatus { get; set; }

}

[Serializable]
public class M0_ProductList_detail
{
    [XmlElement("ProLIDX")]
    public int ProLIDX { get; set; }
    [XmlElement("ProTIDX")]
    public int ProTIDX { get; set; }
    [XmlElement("ProLName")]
    public string ProLName { get; set; }
    [XmlElement("ProLWeight")]
    public int ProLWeight { get; set; }
    [XmlElement("ProLFlavor")]
    public string ProLFlavor { get; set; }
    [XmlElement("ProLStatus")]
    public int ProLStatus { get; set; }

    [XmlElement("ProTName")]
    public string ProTName { get; set; }

}


