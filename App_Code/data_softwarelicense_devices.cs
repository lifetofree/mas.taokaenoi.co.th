﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_softwarelicense_devices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_softwarelicense_devices")]
public class data_softwarelicense_devices
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("u0_softwarelicense_list")]
    public u0_softwarelicense_detail[] u0_softwarelicense_list { get; set; }

    [XmlElement("u1_softwarelicense_list")]
    public u1_softwarelicense_detail[] u1_softwarelicense_list { get; set; }

    [XmlElement("u2_softwarelicense_list")]
    public u2_softwarelicense_detail[] u2_softwarelicense_list { get; set; }

    [XmlElement("bind_softwarelicense_list")]
    public bind_softwarelicense_detail[] bind_softwarelicense_list { get; set; }

    [XmlElement("log_u0software_list")]
    public log_u0software_detail[] log_u0software_list { get; set; }

    [XmlElement("approve_u0software_list")]
    public approve_u0software_detail[] approve_u0software_list { get; set; }

    [XmlElement("report_softwarelicense_list")]
    public report_softwarelicense_detail[] report_softwarelicense_list { get; set; }

    [XmlElement("bindreport_softwarelicense_list")]
    public bindreport_softwarelicense_detail[] bindreport_softwarelicense_list { get; set; }

    [XmlElement("search_softwarelicense_list")]
    public search_softwarelicense_detail[] search_softwarelicense_list { get; set; }

    [XmlElement("u0_softwarelicenseholder_list")]
    public u0_softwarelicenseholder_detail[] u0_softwarelicenseholder_list { get; set; }

}

[Serializable]
public class u0_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("u0_didx_code")]
    public string u0_didx_code { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    
    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("software_name_where")]
    public string software_name_where { get; set; }

    [XmlElement("software_name_sentemail")]
    public string software_name_sentemail { get; set; }

    [XmlElement("email")]
    public string email { get; set; }

    [XmlElement("u0_holder")]
    public string u0_holder { get; set; }

    [XmlElement("check_holder_tranfer")]
    public int check_holder_tranfer { get; set; }


}

[Serializable]
public class u0_softwarelicenseholder_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("u0_didx_code")]
    public string u0_didx_code { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }


    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("software_name_where")]
    public string software_name_where { get; set; }

    [XmlElement("software_name_sentemail")]
    public string software_name_sentemail { get; set; }

    [XmlElement("email")]
    public string email { get; set; }



}

[Serializable]
public class u1_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u1_software_idx")]
    public int u1_software_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("software_name_emailidx")]
    public string software_name_emailidx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name_idxdataset")]
    public string software_name_idxdataset { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("ghost_status")]
    public int ghost_status { get; set; }


}

[Serializable]
public class u2_softwarelicense_detail
{

    [XmlElement("u2_software_idx")]
    public int u2_software_idx { get; set; }

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("approve_status")]
    public int approve_status { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }


}

[Serializable]
public class bind_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

}

[Serializable]
public class log_u0software_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_des")]
    public string actor_des { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }



}

[Serializable]
public class approve_u0software_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_des")]
    public string actor_des { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }



}

[Serializable]
public class report_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("version")]
    public string version { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("count_software_license")]
    public int count_software_license { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("software_use")]
    public int software_use { get; set; }

    [XmlElement("software_licenseuseasset")]
    public int software_licenseuseasset { get; set; }

    [XmlElement("software_ghost")]
    public int software_ghost { get; set; }

}

[Serializable]
public class search_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

}

[Serializable]
public class bindreport_softwarelicense_detail
{

    [XmlElement("u0_software_idx")]
    public int u0_software_idx { get; set; }

    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("num_license")]
    public string num_license { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_license")]
    public string name_license { get; set; }

    [XmlElement("org_name_th_license")]
    public string org_name_th_license { get; set; }

    [XmlElement("dept_name_th_license")]
    public string dept_name_th_license { get; set; }

    [XmlElement("sec_name_th_license")]
    public string sec_name_th_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("version")]
    public string version { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("u0_serial")]
    public string u0_serial { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("count_software_license")]
    public int count_software_license { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }
    
    [XmlElement("software_use")]
    public int software_use { get; set; }

    [XmlElement("software_licenseuseasset")]
    public int software_licenseuseasset { get; set; }

    [XmlElement("software_ghost")]
    public int software_ghost { get; set; }

}