using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_fingerscan_demo_manage : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    int emp_idx = 0;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        if(!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdFingerPrint":
                // _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewCreate", 0);

                // div_heading.Attributes.Add("class", "panel panel-info");
                // litHeadingTitle.Text = "เพิ่มข้อมูล";
                // rblMasterType.Enabled = false;
                break;
            case "cmdEdit":
                // _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                // setActiveView("viewCreate", _masterType);

                // div_heading.Attributes.Add("class", "panel panel-warning");
                // litHeadingTitle.Text = "แก้ไขข้อมูล";
                // rblMasterType.Enabled = false;
                // switch(_masterType)
                // {
                //     case 1:
                //         LinkButton lbReset = (LinkButton)fvDetail.FindControl("lbReset");
                //         lbReset.Visible = false;
                //         break;
                //     case 2:
                //         LinkButton lbReset2 = (LinkButton)fvDetail.FindControl("lbReset");
                //         lbReset2.Visible = false;
                //         break;
                //     case 3:
                //         LinkButton lbReset3 = (LinkButton)fvDetail.FindControl("lbReset");
                //         lbReset3.Visible = false;
                //         break;
                // }
                break;
            case "cmdSave": case "cmdCancel":
                // _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                // setActiveView("viewList", _masterType);
                initPage();
                break;
            case "cmdReset":
                initPage();
                // _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                // switch(_masterType)
                // {
                //     case 1:
                //         setFormData(fvDetail, FormViewMode.Insert, null);
                //         break;
                //     case 2:
                //         setFormData(fvDetail2, FormViewMode.Insert, null);
                //         break;
                //     case 3:
                //         setFormData(fvDetail3, FormViewMode.Insert, null);
                //         break;
                // }
                break;
            case "cmdSearch":
                table1.Visible = true;
                div_Action.Visible = true;
                break;
            case "cmdTransfer":
                div_transfer.Visible = true;
                div_heading2.Attributes.Add("class", "panel panel-primary");
                litHeadingTitle2.Text = "โอนลายนิ้วมือ";
                lbTransferItem.Visible = true;
                lbDeleteItem.Visible = false;
                break;
            case "cmdDelete":
                div_transfer.Visible = true;
                div_heading2.Attributes.Add("class", "panel panel-danger");
                litHeadingTitle2.Text = "ลบลายนิ้วมือ";
                lbTransferItem.Visible = false;
                lbDeleteItem.Visible = true;
                break;
        }
    }

    protected void rblSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblName = (RadioButtonList)sender;
        // _masterType = _funcTool.convertToInt(rblName.SelectedValue);

        switch (rblName.ID)
        {
            // case "rblMasterType":
            //     setActiveView("viewCreate", _masterType);
            //     break;
            case "rblMasterType2":
                // setActiveView("viewList", _masterType);
                break;
        }
    }
    #endregion event command

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        // _dataEmployee.organization_list = new organization_details[1];
        // organization_details _orgList = new organization_details();
        // _orgList.org_idx = 0;
        // _dataEmployee.organization_list[0] = _orgList;

        // _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        // setDdlData(ddlName, _dataEmployee.organization_list, "org_name_en", "org_idx");
        // ddlName.Items.Insert(0, new ListItem("--- organization ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        // _dataEmployee.department_list = new department_details[1];
        // department_details _deptList = new department_details();
        // _deptList.org_idx = _org_idx;
        // _dataEmployee.department_list[0] = _deptList;

        // _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        // setDdlData(ddlName, _dataEmployee.department_list, "dept_name_en", "rdept_idx");
        // ddlName.Items.Insert(0, new ListItem("--- department ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        // _dataEmployee.section_list = new section_details[1];
        // section_details _secList = new section_details();
        // _secList.org_idx = _org_idx;
        // _secList.rdept_idx = _rdept_idx;
        // _dataEmployee.section_list[0] = _secList;

        // _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        // setDdlData(ddlName, _dataEmployee.section_list, "sec_name_en", "rsec_idx");
        // ddlName.Items.Insert(0, new ListItem("--- section ---", "-1"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        // DropDownList ddlName = (DropDownList)sender;

        // DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
        // DropDownList ddlDept = (DropDownList)fvEmpSearch.FindControl("ddlDept");
        // DropDownList ddlSec = (DropDownList)fvEmpSearch.FindControl("ddlSec");

        // switch (ddlName.ID)
        // {
        //     case "ddlOrg":
        //         getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedItem.Value));
        //         ddlSec.Items.Clear();
        //         ddlSec.Items.Insert(0, new ListItem("--- section ---", "-1"));
        //         break;
        //     case "ddlDept":
        //         getSectionList(ddlSec, int.Parse(ddlOrg.SelectedItem.Value), int.Parse(ddlDept.SelectedItem.Value));
        //         break;
        // }
    }
    #endregion dropdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["state"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setActiveView(string activeTab, int masterType)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch(activeTab)
        {
            case "viewList":
                table1.Visible = false;
                div_Action.Visible = false;
                div_transfer.Visible = false;
                // setGridData(gvList, null);
                // setGridData(gvList2, null);
                // setGridData(gvList3, null);

                // table1.Visible = false;
                // table2.Visible = false;
                // table3.Visible = false;

                // switch(masterType)
                // {
                //     case 1:
                //         table1.Visible = true;
                //         // search_unit_detail _search_units = new search_unit_detail();
                //         // _search_units.s_m0idx = "0";
                //         // _data_services.search_unit_list = new search_unit_detail[1];
                //         // _data_services.search_unit_list[0] = _search_units;
                //         // _data_services = getM0UnitsList(_data_services);
                //         // setGridData(gvList, _data_services.m0_units_list);
                //         break;
                //     case 2:
                //         table2.Visible = true;
                //         // search_brand_detail _search_brands = new search_brand_detail();
                //         // _search_brands.s_m0idx = "0";
                //         // _data_services.search_brand_list = new search_brand_detail[1];
                //         // _data_services.search_brand_list[0] = _search_brands;
                //         // _data_services = getM0BrandsList(_data_services);
                //         // setGridData(gvList2, _data_services.m0_brands_list);
                //         break;
                //     case 3:
                //         table3.Visible = true;
                //         // search_detail_prefix_detail _search_prefix = new search_detail_prefix_detail();
                //         // _search_prefix.s_m0idx = "0";
                //         // _data_services.search_detail_prefix_list = new search_detail_prefix_detail[1];
                //         // _data_services.search_detail_prefix_list[0] = _search_prefix;
                //         // _data_services = getM0DetailPrefixList(_data_services);
                //         // setGridData(gvList3, _data_services.m0_detail_prefix_list);
                //         break;
                // }
                break;
            case "viewCreate":
                // rblMasterType.SelectedValue = rblMasterType2.SelectedValue;
                // setFormData(fvDetail, FormViewMode.ReadOnly, null);
                // setFormData(fvDetail2, FormViewMode.ReadOnly, null);
                // setFormData(fvDetail3, FormViewMode.ReadOnly, null);
                // // litDebug.Text = masterType.ToString();
                // switch(masterType)
                // {
                //     case 1:
                //         setFormData(fvDetail, FormViewMode.Insert, null);
                //         break;
                //     case 2:
                //         setFormData(fvDetail2, FormViewMode.Insert, null);
                //         break;
                //     case 3:
                //         setFormData(fvDetail3, FormViewMode.Insert, null);
                //         break;
                // }
                break;
        }
    }

    // protected data_services getExecuteData(string serviceName, data_services dataIn, int actionType)
    // {
    //     return (data_services)_funcTool.convertXmlToObject(typeof(data_services), _serviceExecute.actionExec(_connString, "data_services", serviceName, dataIn, actionType));
    // }

    // protected void linkBtnTrigger(LinkButton linkBtnID)
    // {
    //     UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
    //     UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
    //     triggerLinkBtn.ControlID = linkBtnID.UniqueID;
    //     updatePanel.Triggers.Add(triggerLinkBtn);
    // }

    // protected data_services getM0UnitsList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceUnit, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    // protected data_services getM0BrandsList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceBrand, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    // protected data_services getM0DetailPrefixList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceDetailPrefix, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    public string getStatusIcon(Int32 inStatus)
    {
        return (inStatus == 1 ? "<i class=\"fas fa-check-circle text-success\"></i>" : "<i class=\"fas fa-times-circle text-danger\"></i>");
    }
    #endregion reuse
}