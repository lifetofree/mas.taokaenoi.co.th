﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_qa_lab_result : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_qa_report _data_qa_report = new data_qa_report();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    //-- employee --//

    //-- qa report result --//
    static string _urlGetTestDetailToResult = _serviceUrl + ConfigurationManager.AppSettings["urlGetTestDetailToResult"];


    //-- qa report result --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;


    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type

 
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();
            getDataResultLabList();
            //getCountWaitDetailApproveOTAdminCreate();

            DataTableTestDetailCreate(0);

           // LoadProducts();


        }

    }

    #region set/get bind data

    protected void AddNewRecord(object sender, EventArgs e)
    {
        gvProducts.ShowFooter = true;
        LoadProducts();
    }


    protected DataSet RetrieveProducts()
    {

        ////fetch the connection string from web.config
        //string connString = ConfigurationManager.ConnectionStrings["Sql"].ConnectionString;

        ////SQL statement to fetch entries from products
        //string sql = @"Select top 10  P.ProductID, P.Name, P.ProductNumber, ListPrice  from tblProduct P";

        DataSet dsProducts = new DataSet();
        ////Open SQL Connection
        //using (SqlConnection conn = new SqlConnection(connString))
        //{
        //    conn.Open();
        //    //Initialize command object
        //    using (SqlCommand cmd = new SqlCommand(sql, conn))
        //    {
        //        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //        //Fill the result set
        //        adapter.Fill(dsProducts);
        //    }
        //}
        return dsProducts;
    }


    protected void LoadProducts()
    {
        //execute the select statement
        DataSet dsProducts = RetrieveProducts();
        DataTable dtProducts = dsProducts.Tables[0];

        //if it is an empty dataset, add a dummy row to the header
        if (dtProducts.Rows.Count == 0)
        {
            FixGridHeader(dtProducts);
        }
        else
        {
            //if the query returned results, just add them to the grid
            gvProducts.DataSource = dtProducts;
            gvProducts.DataBind();
        }
    }

    protected void FixGridHeader(DataTable dataSource)
    {
        //add blank row to the the resultset
        dataSource.Rows.Add(dataSource.NewRow());

        gvProducts.DataSource = dataSource;
        gvProducts.DataBind();

        //hide empty row
        gvProducts.Rows[0].Visible = false;
        gvProducts.Rows[0].Controls.Clear();
    }



    protected void DataTableTestDetailCreate(int m0_test_detail_idx)
    {
        //gv bind create result lab
        data_qa_report _data_lab_m0_testdetail = new data_qa_report();
        _data_lab_m0_testdetail.qa_report_m0_testdetail_list = new qa_report_m0_testdetail_detail[1];
        qa_report_m0_testdetail_detail _m0_testdetail = new qa_report_m0_testdetail_detail();

        _data_lab_m0_testdetail.qa_report_m0_testdetail_list[0] = _m0_testdetail;

        _data_lab_m0_testdetail = callServicePostQALabReport(_urlGetTestDetailToResult, _data_lab_m0_testdetail);

        ViewState["vs_headerTestDetail"] = _data_lab_m0_testdetail.qa_report_m0_testdetail_list;
        litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_lab_m0_testdetail));

        string _header_text = String.Empty;
        int _header_idx;
        int _countrow = 0;
        DataTable table = new DataTable();
        // table.Columns.Add("Approve", typeof(String));
        table.Columns.Add("วันที่รับตัวอย่าง", typeof(String));
        table.Columns.Add("วันทดสอบ", typeof(String));
        table.Columns.Add("Lab Code", typeof(String));
        table.Columns.Add("ชนิดตัวอย่าง", typeof(String));
        table.Columns.Add("MAT", typeof(String));
        table.Columns.Add("ตัวอย่าง", typeof(String));
        table.Columns.Add("Name", typeof(String));
        table.Columns.Add("PI", typeof(String));
        foreach (var _loop_header in _data_lab_m0_testdetail.qa_report_m0_testdetail_list)
        {

            _header_idx = int.Parse(_loop_header.test_detail_idx.ToString());
            _header_text = _loop_header.test_detail_name.ToString();
            table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

            _countrow++;

        }

        //DataRow dr = table.NewRow();
        //foreach (var _loop_footer in _data_lab_m0_testdetail.qa_report_m0_testdetail_list)
        //{

        //    _header_idx = int.Parse(_loop_header.test_detail_idx.ToString());
        //    _header_text = _loop_header.test_detail_name.ToString();
        //    table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

        //    _countrow++;

        //}



        //data_qa _data_lab_resulttest1 = new data_qa();
        //_data_lab_resulttest1.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
        //bind_qa_lab_result_detail _result_lab1 = new bind_qa_lab_result_detail();

        //_result_lab1.condition = 1;
        //_result_lab1.m0_lab = m0_labsup;
        ////_result_lab1.m0_lab = int.Parse(tbValuePlaceSup.Text);

        //_data_lab_resulttest1.bind_qa_lab_result_list[0] = _result_lab1;

        //_data_lab_resulttest1 = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resulttest1);

        //ViewState["vs_listSampleResultLab"] = _data_lab_resulttest1.bind_qa_lab_result_list;

        //table.Columns.Add("Approve", typeof(String));
        //string _text = String.Empty;
        //int _countrow1 = 0;

        //if (_data_lab_resulttest1.bind_qa_lab_result_list == null)
        //{
        //    GvSupervisor.Visible = false;
        //    GvSupervisor_scroll.Visible = false;
        //    EmptyData.Visible = true;

        //}
        //else
        //{

        //    foreach (var _loop_rowText in _data_lab_resulttest1.bind_qa_lab_result_list)
        //    {
        //        //gen label
        //        Label dynamicLabel = new Label();
        //        dynamicLabel.Text = _loop_rowText.sample_code.ToString();
        //        table.Rows.Add(dynamicLabel.Text);
        //        _countrow1++;
        //    }

        //    GvSupervisor.Visible = true;
        //    GvSupervisor_scroll.Visible = true;
        //    EmptyData.Visible = false;


        //}

        ViewState["vsTable"] = table;

        GvSaveResultLab.DataSource = ViewState["vsTable"];
        GvSaveResultLab.DataBind();


        //data_qa _data_lab_resultrow = new data_qa();
        //_data_lab_resultrow.bind_qa_lab_result_list = new bind_qa_lab_result_detail[1];
        //bind_qa_lab_result_detail _result_row = new bind_qa_lab_result_detail();

        //_result_row.condition = 3;
        //_result_row.m0_lab = int.Parse(tbValuePlaceSup.Text);

        //_data_lab_resultrow.bind_qa_lab_result_list[0] = _result_row;
        //_data_lab_resultrow = callServicePostQA(_urlQaGetRecordResultTest, _data_lab_resultrow);

        //ViewState["vs_listResultLab_result"] = _data_lab_resultrow.bind_qa_lab_result_list;

    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกพนักงาน ---", "0"));
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion set/get bind data  

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdCancel":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewAdminShiftRotate":
                setActiveTab("docDetailAdmin", int.Parse(cmdArg.ToString()), 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetailAdmin":
                setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdViewUserShiftRotate":

                string[] arg_viewuser = new string[1];
                arg_viewuser = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewuser = int.Parse(arg_viewuser[0]);
                int _u1_doc_idx_viewuser = int.Parse(arg_viewuser[1]);
                // string _decision_name_saveotday = arg_viewuser[1];

                setActiveTab("docDetail", _u0_doc_idx_viewuser, 0, 0, 0, 0, 0, _u1_doc_idx_viewuser);
                break;
            case "cmdDocCancel":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;
        }

    }
    //endbtn

    #endregion event command

    #region Changed
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {
            case "chkallApprove":
                int chack_all = 0;

                //if (chkallApprove.Checked)
                //{

                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = true;
                //        chack_all++;
                //    }
                //}
                //else
                //{
                //    foreach (GridViewRow row in GvWaitApprove.Rows)
                //    {
                //        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                //        chk_ot_approve.Checked = false;
                //        chack_all++;
                //    }
                //}
                break;

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        //DropDownList ddlorg_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
        //DropDownList ddldep_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
        //DropDownList ddlsec_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");

        switch (ddlName.ID)
        {
            //case "ddlorgReport":

            //    getDepartmentList(ddlrdeptReport, int.Parse(ddlorgReport.SelectedValue));

            //    ddlrsecReport.Items.Clear();
            //    ddlrsecReport.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

            //    ddlempReport.Items.Clear();
            //    ddlempReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

            //    break;
            //case "ddlrdeptReport":

            //    getSectionList(ddlrsecReport, int.Parse(ddlorgReport.SelectedItem.Value), int.Parse(ddlrdeptReport.SelectedItem.Value));


            //    ddlempReport.Items.Clear();
            //    ddlempReport.Items.Insert(0, new ListItem("-- เลือกพนักงาน --", "0"));

            //    break;
            //case "ddlrsecReport":
            //    getEmpList(ddlempReport, int.Parse(ddlorgReport.SelectedValue), int.Parse(ddlrdeptReport.SelectedValue), int.Parse(ddlrsecReport.SelectedValue));

            //    break;



        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {

            ////case "txt_timeend_job":

            ////    //linkBtnTrigger(btnSaveDetailMA);

            ////    foreach (GridViewRow row in GvCreateOTMonth.Rows)
            ////    {
            ////        CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
            ////        UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
            ////        UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
            ////        TextBox txt_job = (TextBox)row.FindControl("txt_job");
            ////        TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
            ////        TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
            ////        TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
            ////        TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
            ////        TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
            ////        decimal total_hour = 0;


            ////        //TimeSpan diff = secondDate - firstDate;
            ////        //double hours = diff.TotalHours;

            ////        litDebug.Text = "3333";

            ////        if (chk_date_ot.Checked)
            ////        {
            ////            if (txt_timestart_job.Text != "" && txt_timeend_job.Text != "")
            ////            {
            ////                //linkBtnTrigger(btnSaveDetailMA);
            ////                //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
            ////                total_hour = Convert.ToDecimal(txt_timestart_job.Text) * Convert.ToDecimal(txt_timeend_job.Text);
            ////                //litDebug.Text = Convert.ToDecimal(total_price).ToString();
            ////                txt_sum_hour.Text = Convert.ToDecimal(total_hour).ToString();

            ////            }
            ////            else
            ////            {
            ////                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

            ////                break;
            ////            }
            ////        }


            ////    }

            ////    break;




        }

    }

    #endregion Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetailOTShiftRotate":
                //setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);

                break;

        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvCreateShiftRotate":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {



                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {


                }

                break;
            case "GvSaveResultLab":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    string buttonNotApprove = "";

                    TextBox _lbNotApprove = new TextBox();
                    _lbNotApprove.ID = "idSupNotApprove";
                    _lbNotApprove.Text = buttonNotApprove;
                    e.Row.Cells[e.Row.Cells.Count].Controls.Add(_lbNotApprove);
                    //litDebug.Text = "2222";

                    //foreach (TableCell cell in e.Row.Cells)
                    //{
                    //    TextBox tbox = new TextBox();
                    //    cell.Controls.Add(tbox);
                    //}
                }

                break;


        }
    }

    protected void getHourToOT(DropDownList ddlNane, double _hour)
    {
        ddlNane.AppendDataBoundItems = true;
        ddlNane.Items.Clear();



        if (Convert.ToDouble(_hour) >= 0.5)
        {
            double _timescan = Convert.ToDouble(_hour);
            for (double i = 0.0; i <= _timescan; i += 0.5)
            {
                ddlNane.Items.Add((i).ToString());
            }

            ddlNane.Items.FindByValue(Convert.ToString(_timescan)).Selected = true;

        }
        else
        {
            ddlNane.Visible = false;
            ddlNane.SelectedValue = "0";
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region set data table
    protected void getDataResultLabList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dstDataResultLabList = new DataSet();
        dstDataResultLabList.Tables.Add("dsDataResultLabListTable");

        dstDataResultLabList.Tables["dsDataResultLabListTable"].Columns.Add("drReceiveDateText", typeof(String));
        dstDataResultLabList.Tables["dsDataResultLabListTable"].Columns.Add("drTestDateText", typeof(String));

        //dstDeadlineList.Tables["dsDataResultLabListTable"].Columns.Add("drSelectedDeadlineText", typeof(String));
        //dstDeadlineList.Tables["dsDataResultLabListTable"].Columns.Add("drSelectedActionListDeadlineText", typeof(String));
        //dstDeadlineList.Tables["dsDataResultLabListTable"].Columns.Add("drSelectedActionListDeadlineIdx", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalStatusText", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusIdx", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusText", typeof(String));
        //drContacts["drSelectedActionListDeadlineIdx"] = Action_detail_idx.ToString();

        ViewState["vsDataResultLabList"] = dstDataResultLabList;
    }

    protected void setDataResultLabList()
    {
        if (ViewState["vsDataResultLabList"] != null)
        {
            //TextBox tb_Office_Action_Date = (TextBox)fvPublicationDetail.FindControl("tb_Office_Action_Date");
            TextBox txt_drReceiveDateText = gvAddResultLabList.FooterRow.FindControl("txt_drReceiveDateText") as TextBox;
            TextBox txt_drTestDateText = gvAddResultLabList.FooterRow.FindControl("txt_drTestDateText") as TextBox;

            //RadioButtonList rdoDeadline = (RadioButtonList)fvPublicationDetail.FindControl("rdoDeadline");
            //CheckBoxList chkActionList = (CheckBoxList)fvPublicationDetail.FindControl("chkActionList");
            ////DropDownList ddlRenewalStatus = (DropDownList)fvPublicationDetail.FindControl("ddlRenewalStatus");
            ////DropDownList ddlRenewalSubStatus = (DropDownList)fvPublicationDetail.FindControl("ddlRenewalSubStatus");
            //GridView gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");


            DataSet dsContacts = (DataSet)ViewState["vsDataResultLabList"];

            foreach (DataRow dr in dsContacts.Tables["dsDataResultLabListTable"].Rows)
            {
                if (dr["drReceiveDateText"].ToString() == txt_drReceiveDateText.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsDataResultLabListTable"].NewRow();
            drContacts["drReceiveDateText"] = txt_drReceiveDateText.Text;
            drContacts["drTestDateText"] = txt_drTestDateText.Text;
            //drContacts["drSelectedText"] = rdoRenewal.SelectedItem.Text;
            //drContacts["drRenewalStatusIdx"] = ddlRenewalStatus.SelectedValue;
            //drContacts["drRenewalStatusText"] = ddlRenewalStatus.SelectedItem.Text;
            //drContacts["drRenewalSubStatusIdx"] = ddlRenewalSubStatus.SelectedValue;
            //drContacts["drRenewalSubStatusText"] = ddlRenewalSubStatus.SelectedItem.Text;

            dsContacts.Tables["dsDataResultLabListTable"].Rows.Add(drContacts);
            ViewState["vsDataResultLabList"] = dsContacts;

            setGridData(gvAddResultLabList, dsContacts.Tables["dsDataResultLabListTable"]);
            gvAddResultLabList.Visible = true;


        }
    }

    protected void CleardataSetDeadlineList()
    {
        //var gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");
        ViewState["vsDataResultLabList"] = null;
        gvAddResultLabList.DataSource = ViewState["vsDataResultLabList"];
        gvAddResultLabList.DataBind();
        getDataResultLabList();
    }

    #endregion set data table

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docCreateResult", 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa_report callServicePostQALabReport(string _cmdUrl, data_qa_report _data_qa_report)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa_report);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa_report = (data_qa_report)_funcTool.convertJsonToObject(typeof(data_qa_report), _localJson);

        return _data_qa_report;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        //setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        switch (activeTab)
        {
            case "docCreateResult":

                //setDataResultLabList();

                //ddlMonthReport.ClearSelection();
                //txtCostcenterReport.Text = string.Empty;

                //getddlYear(ddlYearReport);
                //getOrganizationList(ddlorgReport);
                //getDepartmentList(ddlrdeptReport, int.Parse(ddlorgReport.SelectedValue));
                //getSectionList(ddlrsecReport, int.Parse(ddlorgReport.SelectedValue), int.Parse(ddlrdeptReport.SelectedValue));
                //getM0TypeShift(ddlM0Shift, 0);

                setOntop.Focus();
                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docCreateResult":
                li0.Attributes.Add("class", "active");

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                //{
                //    //Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                //    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler_approve");
                //    var btnApprove = (LinkButton)e.Item.FindControl("btnApprove");

                //    for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                //    {
                //        btnApprove.CssClass = ConfigureColors(k);
                //        //Console.WriteLine(i);
                //    }


                //}
                break;

        }
    }

    #endregion reuse

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDay(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDayHR(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    #region data excel

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        ////int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            ////var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            ////sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    #endregion data excel
}