﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_shift.aspx.cs" Inherits="websystem_hr_employee_shift" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script type="text/javascript">
        function GridSelectAllColumn(spanChk) {
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0]; xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++) {
                if (elm[i].type === 'checkbox' && elm[i].checked != xState)
                    elm[i].click();
            }
        }

        function GridSelectAllColumn_temp(spanChk) {
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0]; xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++) {
                if (elm[i].type === 'checkbox' && elm[i].checked != xState)
                    elm[i].click();
            }
        }

        function GridSelectAllColumn_edit(spanChk) {
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0]; xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++) {
                if (elm[i].type === 'checkbox' && elm[i].checked != xState)
                    elm[i].click();
            }
        }
    </script>

    <script type="text/javascript">
        function CheckAll() {
            var intIndex = 0;
            var rowCount = document.getElementById('YrChkBox').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById('chkAll').checked == true) {
                    if (document.getElementById("YrChkBox" + "_" + i)) {
                        if (document.getElementById("YrChkBox" + "_" + i).disabled != true)
                            document.getElementById("YrChkBox" + "_" + i).checked = true;
                    }
                }
                else {
                    if (document.getElementById("YrChkBox" + "_" + i)) {
                        if (document.getElementById("YrChkBox" + "_" + i).disabled != true)
                            document.getElementById("YrChkBox" + "_" + i).checked = false;
                    }
                }
            }
        }

        function CheckAll_temp() {
            var intIndex = 0;
            var rowCount = document.getElementById('YrChkBox').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById('chkAll_temp').checked == true) {
                    if (document.getElementById("YrChkBox" + "_" + i)) {
                        if (document.getElementById("YrChkBox" + "_" + i).disabled != true)
                            document.getElementById("YrChkBox" + "_" + i).checked = true;
                    }
                }
                else {
                    if (document.getElementById("YrChkBox" + "_" + i)) {
                        if (document.getElementById("YrChkBox" + "_" + i).disabled != true)
                            document.getElementById("YrChkBox" + "_" + i).checked = false;
                    }
                }
            }
        }

        function UnCheckAll() {
            var intIndex = 0;
            var flag = 0;
            var rowCount = document.getElementById('YrChkBox').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById("YrChkBox" + "_" + i)) {
                    if (document.getElementById("YrChkBox" + "_" + i).checked == true) {
                        flag = 1;
                    }
                    else {
                        flag = 0;
                        break;
                    }
                }
            }
            if (flag == 0)
                document.getElementById('chkAll').checked = false;
            else
                document.getElementById('chkAll').checked = true;

        }

    </script>

    <asp:Label ID="fs" runat="server"></asp:Label>
    <%----------- Menu Tab Start---------------%>
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="_divMenuLiToDivIndex" runat="server">
                        <asp:LinkButton ID="lbkindex" runat="server"
                            CommandName="btnIndex" CommandArgument="1"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>
                    <li id="Li1" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ประกาศกะการทำงาน <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="lbkshiftday" runat="server"
                                    CommandName="btnshift" CommandArgument="2"
                                    OnCommand="btnCommand" Text="รายวัน" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="lbkshiftmount" runat="server"
                                    CommandName="btnshift" CommandArgument="3"
                                    OnCommand="btnCommand" Text="รายเดือน" />
                            </li>
                        </ul>
                    </li>
                    <li id="_divMenuLiToDivChangeAnnounce" runat="server" visible="false">
                        <asp:LinkButton ID="lbkshiftchange" runat="server"
                            CommandName="btnshiftchange" CommandArgument="5"
                            OnCommand="btnCommand" Text="ขอเปลี่ยนกะการทำงาน" />
                    </li>
                    <li id="_divMenuLiToDivHolidaySwap" runat="server" visible="false">
                        <asp:LinkButton ID="btnholidayswap" runat="server"
                            CommandName="btnshiftholidayswap" CommandArgument="5"
                            OnCommand="btnCommand" Text="ขอสลับวันหยุด" />
                    </li>
                    <li id="_divMenuLiToDivEditWaiting" runat="server" visible="false">
                        <asp:LinkButton ID="lbkwaiting" runat="server"
                            CommandName="btnwaiting" OnCommand="btnCommand" CommandArgument="6"
                            Text="รายการที่รอแก้ไข" />
                    </li>
                    <li id="_divMenuLiToDivPending" runat="server">
                        <asp:LinkButton ID="lbkapprove" runat="server"
                            CommandName="btnapprove" OnCommand="btnCommand" CommandArgument="4"
                            Text="รายการที่รออนุมัติ" />
                    </li>
                    <li id="_divMenuLiToDivHRArea" runat="server">
                        <asp:LinkButton ID="lbkhr" runat="server" CommandArgument="8"
                            CommandName="btnhr" OnCommand="btnCommand"
                            Text="ส่วนของ HR" />
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <asp:Label ID="sdfa" runat="server"></asp:Label>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div id="boxsearch_shiftime" runat="server" class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="BoxSearch" runat="server">
                            <asp:FormView ID="Fv_Search_Shift_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="Shift Code" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="รหัสกะ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtcode_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label107" runat="server" Text="Shift" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label108" runat="server" Text="กะงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlshift_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกกะงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlIndexOrgSearch" runat="server" CssClass="form-control" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlIndexDeptSearch" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <%--<div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label220" runat="server" Text="EmpType" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label225" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlemptype_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label226" runat="server" Text="EmpStatus" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label227" runat="server" Text="สถานะพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlempstatus_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="-1">สถานะพนักงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">พนักงาน</asp:ListItem>
                                                <asp:ListItem Value="0">พ้นสภาพ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <asp:Button ID="btnsearch_index" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                            <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                    <asp:Panel ID="boxgv_shiftime" runat="server">
                        <asp:GridView ID="GvIndex"
                            DataKeyNames="u0_empshift_idx"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            HeaderStyle-CssClass="table_headCenter"
                            HeaderStyle-Height="40px"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="PageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="1%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <%#(Container.DataItemIndex + 1) %>
                                                <asp:Label ID="lbl_countdown" runat="server" Visible="false" Text='<%# Eval("countdown") %>'></asp:Label>
                                                <asp:Label ID="lblu0_unidx" runat="server" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                                <asp:Label ID="lblu0_acidx" runat="server" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                                <asp:Label ID="lblu0_doc_decision" runat="server" Visible="false" Text='<%# Eval("u0_doc_decision") %>'></asp:Label>
                                    </p>                                                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ข้อมูลกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="lblparttime_code" runat="server" Text="รหัสกะ : " /></b><asp:Label ID="lblparttime_code_" runat="server" Text='<%# Eval("parttime_code") %>'></asp:Label>
                                            </p>                                     
                                    <b>
                                        <asp:Label ID="Label53_1" runat="server" Text="ชื่อกะ : " /></b><asp:Label ID="Label5_9" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                            </p>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่เริ่มใช้" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="fwsse" runat="server" Text="วันที่-เวลาเริ่ม : " /></b><asp:Label ID="lbl_date_start" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_start")) %>'></asp:Label>
                                            </p> 
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="วันที่-เวลาจบ : " /></b><asp:Label ID="Label2" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_end")) %>'></asp:Label>
                                            </p> 
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อผู้สร้างกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="องค์กร : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                            </p>
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>

                                            </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="แผนก : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                            </p>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                        <small>
                                            <span><%# Eval("status_name") %>&nbsp;/ <%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                                    <ItemTemplate>
                                        <small>
                                            <asp:LinkButton ID="btnedit" CssClass="btn btn-primary" runat="server" CommandName="btnedit_detail" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไขข้อมูล/เปลี่ยนกะ" CommandArgument='<%#Eval("u0_empshift_idx") + ";" + Eval("u0_unidx") + ";" + Eval("u0_acidx") + ";" + Eval("u0_doc_decision") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </div>

            <asp:Panel ID="boxedit_shiftime" runat="server" Visible="false">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดกะ
                            <asp:Label ID="Label3" runat="server"></asp:Label></strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListGroup_edit" CssClass="btn_menulist" runat="server" CommandName="btnList_DB_edit" OnCommand="btnCommand" CommandArgument="5">ค้นหารายชื่อ</asp:LinkButton>
                                            </li>
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListEmp_edit" CssClass="btn_menulist" runat="server" CommandName="btnList_DB_edit" OnCommand="btnCommand" CommandArgument="6">ค้นหากลุ่ม</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- GV List -->
                                    <asp:Panel ID="BoxSearchGroup_edit" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">เลือกรายชื่อพนักงาน</div>
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlsec_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlpos_add" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <%--<div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_th_add" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                <asp:ListItem Value="3">นาง</asp:ListItem>
                                                                <asp:ListItem Value="4">อื่นๆ</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-2"></div>--%>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txt_search_name_mount_add" CssClass="form-control" runat="server" placeholder="ชื่อพนักงาน"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="txt_search_empcode_mount_add" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="รหัสพนักงาน Ex. 57000001,57000002" />
                                                        </div>
                                                    </div>

                                                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddlorg_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddldep_add" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <br />
                                                    </asp:Panel>
                                                    <div class="form-group">
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <div class="col-sm-2">
                                                            <asp:Button ID="Button2" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_mount_add" OnCommand="btnCommand" />
                                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:LinkButton ID="btnselect_all_edit" class="btn btn-success" ValidationGroup="Search" runat="server" Text="+ ยืนยันรายการที่เลืือก" data-original-title="search" data-toggle="tooltip" CommandName="btnselect_mount_edit" OnCommand="btnCommand" />
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group" runat="server">
                                                        <div style="width: 100%; height: 450px; overflow: scroll">
                                                            <asp:GridView ID="GvEmployeeDB_add" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="25"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <HeaderStyle CssClass="info" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <%--<asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnSelect_mount" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect_mount_add" OnCommand="btnCommand"
                                                                            CommandArgument='<%#   Eval("emp_idx") + ";" + Eval("emp_code") + ";" + Eval("emp_name_th") %>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="checkAll_edit" runat="server" onclick="GridSelectAllColumn_edit(this);" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox1_edit" runat="server" AutoPostBack="true" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัส" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbemp_idx1" runat="server" Text='<%# Eval("emp_idx") %>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbemp_code1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <!-- GV Group -->
                                    <asp:Panel ID="BoxSearchListEmp_edit" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">เลือกกลุ่ม</div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:TextBox ID="txtgroupname_add" runat="server" CssClass="form-control" placeholder="ชื่อกลุ่ม" />
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <asp:DropDownList ID="ddlgroupname_add" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:Button ID="Button4" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" CommandName="btnsearchtranfer_add" OnCommand="btnCommand" />
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group" runat="server">
                                                        <asp:GridView ID="GvGroupName_add" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="true"
                                                            DataKeyNames="m0_group_diary_idx"
                                                            PageSize="10"
                                                            OnPageIndexChanging="Master_PageIndexChanging">
                                                            <HeaderStyle CssClass="success" />

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnSelect" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect_add" OnCommand="btnCommand"
                                                                            CommandArgument='<%#   Eval("rsec_idx_ref") + ";" + Eval("m0_group_diary_idx") + ";" + Eval("group_diary_name")  %>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListEmp_temp_add" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_add" OnCommand="btnCommand" CommandArgument="9">เพิ่มรายชื่อใหม่</asp:LinkButton>
                                            </li>
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListGroup_temp_add" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_add" OnCommand="btnCommand" CommandArgument="10">เพิ่มกลุ่มใหม่</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- GV Temp Group -->
                                    <asp:Panel ID="BoxSearchListEmp_Temp_add" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">เพิ่มรายชื่อใหม่</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <div style="width: 100%; height: 700px; overflow: scroll">
                                                            <asp:GridView ID="GvEmployeeTemp_add" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="25"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <HeaderStyle CssClass="info" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_mount_add" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>


                                                        <asp:Panel ID="pn_employee_temp_add_edit" runat="server" Visible="false">
                                                            <asp:GridView ID="GvEmployeeTemp_add_Edit" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                OnRowDataBound="Master_RowDataBound">
                                                                <HeaderStyle CssClass="info" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_emp_idx_edit" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1_edit" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name_edit" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_mount" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <!-- GV Temp ListEmp -->
                                    <asp:Panel ID="BoxSearchGroup_Temp_add" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">เพิ่มกลุ่มใหม่</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <asp:GridView ID="GvGroupName_Temp_add" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="true"
                                                            DataKeyNames="m0_group_diary_idx"
                                                            PageSize="25"
                                                            OnPageIndexChanging="Master_PageIndexChanging">
                                                            <HeaderStyle CssClass="success" />

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%#(Container.DataItemIndex + 1) %>
                                                                        <asp:Label ID="lbl_rsec_idx" runat="server" Visible="false" Text='<%# Eval("m0_group_diary_idx") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_add" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("m0_group_diary_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListEmp_temp_edit" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_edit" OnCommand="btnCommand" CommandArgument="7">รายชื่อปัจจุบัน</asp:LinkButton>
                                            </li>
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListGroup_temp_edit" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_edit" OnCommand="btnCommand" CommandArgument="8">กลุ่มปัจจุบัน</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- GV Temp List -->
                                    <asp:Panel ID="BoxSearchListEmp_Temp_edit" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">รายชื่อปัจจุบัน</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <div style="width: 100%; height: 700px; overflow: scroll">
                                                            <asp:GridView ID="GvListDB_edit" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="25"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <HeaderStyle CssClass="info" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_u0_empshift_idx" runat="server" Visible="false" Text='<%# Eval("u0_empshift_idx") %>'></asp:Label>
                                                                            <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัส" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_ListDB_edit" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0_empshift_idx") + ";" + Eval("emp_idx")  %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <!-- GV Temp Group -->
                                    <asp:Panel ID="BoxSearchGroup_Temp_edit" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">กลุ่มปัจจุบัน</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <asp:GridView ID="GvGroupDB_edit" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="true"
                                                            DataKeyNames="m0_group_diary_idx"
                                                            PageSize="25"
                                                            OnPageIndexChanging="Master_PageIndexChanging">
                                                            <HeaderStyle CssClass="success" />

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%#(Container.DataItemIndex + 1) %>
                                                                        <asp:Label ID="lbl_u0_empshift_idx" runat="server" Visible="false" Text='<%# Eval("u0_empshift_idx") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_rsec_idx" runat="server" Visible="false" Text='<%# Eval("m0_group_diary_idx") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_GroupDB_edit" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0_empshift_idx") + ";" + Eval("m0_group_diary_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; แก้ไขข้อมูล / ปฏิทินกะเวลาเข้างาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div id="ca" class="col-md-4">

                                <asp:FormView ID="FvEdit_Detail" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">ช่วงวันที่ต้องการประกาศกะ</div>
                                                <div class="panel-body">
                                                    <div class="col-sm-12">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtstartdate_edit" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_start")) %>' placeholder="ตั้งแต่วันที่..."
                                                                CssClass="form-control from-date-datepicker" />
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                            <asp:RequiredFieldValidator ID="Requirgalidator1" ValidationGroup="btnSave_edit" runat="server" Display="None"
                                                                ControlToValidate="txtstartdate_edit" Font-Size="11"
                                                                ErrorMessage="ช่วงวันที่เริ่ม ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorasCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirgalidator1" Width="160" />
                                                        </div>
                                                        <br />
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtenddate_edit" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_end")) %>' placeholder="ถึงวันที่..."
                                                                CssClass="form-control from-date-datepicker" />
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                            <asp:RequiredFieldValidator ID="RequiredFirdator1" ValidationGroup="btnSave_edit" runat="server" Display="None"
                                                                ControlToValidate="txtenddate_edit" Font-Size="11"
                                                                ErrorMessage="ช่วงวันที่จบ ...." />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFirdator1" Width="160" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='panel panel-info external-events-diary'>
                                                <div class="panel-heading">กะการทำงาน</div>
                                                <div class="panel-body">
                                                    <div class="col-sm-12">
                                                        <asp:Label ID="lbl_partime_idx" runat="server" Visible="false" Text='<%# Eval("m0_parttime_idx_ref") %>' />
                                                        <asp:DropDownList ID="ddlpartime_edit" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="R2etor1" ValidationGroup="btnSave_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlpartime_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกกะทำงาน ...."
                                                            ValidationExpression="กรุณาเลือกกะทำงาน ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R2etor1" Width="160" />
                                                    </div>
                                                </div>
                                            </div>
                                            <asp:LinkButton ID="btnsave_edit" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_empshift_idx") %>' CommandName="btnSave_edit" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')">Save &nbsp;</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnCancel_edit">Cancel &nbsp;</asp:LinkButton>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>

                            </div>
                            <div id="calendar" class="col-md-8">
                                <div id="fullCalModal" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                <h4 id="modalTitle" class="modal-title"></h4>
                                            </div>
                                            <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </asp:View>

        <asp:View ID="ViewShift" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ประกาศกะ -
                            <asp:Label ID="lblshiftype" runat="server"></asp:Label></strong></h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="BoxGroupDay" runat="server" Visible="false">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListGroup" CssClass="btn_menulist" runat="server" CommandName="btnList_DB" OnCommand="btnCommand" CommandArgument="1">ค้นหารายชื่อ</asp:LinkButton>
                                            </li>
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListEmp" CssClass="btn_menulist" runat="server" CommandName="btnList_DB" OnCommand="btnCommand" CommandArgument="2">ค้นหากลุ่ม</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- GV ListEmp -->
                                    <asp:Panel ID="BoxSearchGroup" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">เลือกรายชื่อพนักงาน</div>
                                                <div class="panel-body">
                                                    <div class="form-group">

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddlsec" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddlpos" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <asp:Panel ID="box_hide_ddl_od" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlorg" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddldep" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <br />
                                                    </asp:Panel>

                                                    <div class="form-group">
                                                        <%--<div class="col-sm-1">
                                                            <asp:DropDownList ID="ddl_prefix_th" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                <asp:ListItem Value="3">นาง</asp:ListItem>
                                                                <asp:ListItem Value="4">อื่นๆ</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-sm-1"></div>--%>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_search_name_mount" CssClass="form-control" runat="server" placeholder="ชื่อพนักงาน"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <%--<div class="col-sm-5">
                                                            <asp:DropDownList ID="ddlsec" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>--%>
                                                        <div class="col-sm-10">
                                                            <asp:TextBox ID="txt_search_empcode_mount" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="รหัสพนักงาน Ex. 57000001,57000002" />
                                                        </div>
                                                        <br />

                                                        <%--<asp:Panel ID="box_hide_ddl_p" runat="server" Visible="false">
                                                            <div class="col-sm-5">
                                                                <asp:DropDownList ID="ddlpos" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </asp:Panel>--%>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <%--<div class="col-sm-2">
                                                            <asp:Button ID="Button3" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_mount" OnCommand="btnCommand" />
                                                            <asp:HiddenField ID="TabName" runat="server" />
                                                            </div>--%>
                                                        <div class="col-sm-2">
                                                            <asp:Button ID="Button3" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_mount" OnCommand="btnCommand" />
                                                            <asp:HiddenField ID="TabName" runat="server" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <asp:LinkButton ID="btnSelect_mount" class="btn btn-success" ValidationGroup="Search" runat="server" Text="+ ยืนยันรายการที่เลืือก" data-original-title="search" data-toggle="tooltip" CommandName="btnselect_mount" OnCommand="btnCommand" />
                                                        </div>
                                                    </div>
                                                    <br />

                                                    <br />
                                                    <div class="form-group" runat="server">
                                                        <div style="width: 100%; height: 500px; overflow: scroll">
                                                            <asp:GridView ID="GvEmployeeDB" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="5"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <HeaderStyle CssClass="info" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="checkAll" runat="server" onclick="GridSelectAllColumn(this);" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <%--<asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnSelect_mount" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect_mount" OnCommand="btnCommand"
                                                                            CommandArgument='<%#   Eval("emp_idx") + ";" + Eval("emp_code") + ";" + Eval("emp_name_th") %>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>

                                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbemp_idx" runat="server" Text='<%# Eval("emp_idx") %>' Visible="false"></asp:Label>
                                                                            <asp:Label ID="lbemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <!-- GV Group -->
                                    <asp:Panel ID="BoxSearchListEmp" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">เลือกกลุ่ม</div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtgroupname" runat="server" CssClass="form-control" placeholder="ชื่อกลุ่ม" />
                                                        </div>

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddlgroupname" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <asp:Button ID="Button1" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" CommandName="btnsearchtranfer" OnCommand="btnCommand" />
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <div class="form-group" runat="server">
                                                        <asp:GridView ID="GvGroupName" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="true"
                                                            DataKeyNames="m0_group_diary_idx"
                                                            PageSize="10"
                                                            OnPageIndexChanging="Master_PageIndexChanging">
                                                            <HeaderStyle CssClass="success" />

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="2%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnSelect" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect" OnCommand="btnCommand"
                                                                            CommandArgument='<%#   Eval("rsec_idx_ref") + ";" + Eval("m0_group_diary_idx") + ";" + Eval("group_diary_name")  %>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListGroup_temp" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp" OnCommand="btnCommand" CommandArgument="3">รายชื่อที่ถูกเลือก</asp:LinkButton>
                                            </li>
                                            <li class="nav-item">
                                                <asp:LinkButton ID="lbListEmp_temp" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp" OnCommand="btnCommand" CommandArgument="4">กลุ่มที่ถูกเลือก</asp:LinkButton>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- GV Temp List -->
                                    <asp:Panel ID="BoxSearchGroup_Temp" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">รายชื่อที่ถูกเลือก</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <div style="width: 100%; height: 730px; overflow: scroll">
                                                            <asp:GridView ID="GvEmployeeTemp" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="25"
                                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                                OnRowDataBound="Master_RowDataBound">
                                                                <HeaderStyle CssClass="info" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <%--<asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="checkAll_temp" runat="server" onclick="GridSelectAllColumn_temp(this);" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_mount" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>

                                                        <asp:Panel ID="pn_gvtemp_insert" runat="server" Visible="false">
                                                            <asp:GridView ID="GvEmployeeTemp_Insert" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="emp_idx"
                                                                PageSize="25"
                                                                OnRowDataBound="Master_RowDataBound">
                                                                <HeaderStyle CssClass="info" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_emp_idx_insert" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name1_insert" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbfm_name_insert" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel_mount" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <!-- GV Temp Group -->
                                    <asp:Panel ID="BoxSearchListEmp_Temp" runat="server" Visible="false">
                                        <div class="form-group" runat="server">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">กลุ่มที่เลือก</div>
                                                <div class="panel-body">
                                                    <div class="form-group" runat="server">
                                                        <asp:GridView ID="GvGroupName_Temp" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="true"
                                                            DataKeyNames="m0_group_diary_idx"
                                                            PageSize="25"
                                                            OnPageIndexChanging="Master_PageIndexChanging">
                                                            <HeaderStyle CssClass="success" />

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%#(Container.DataItemIndex + 1) %>
                                                                        <asp:Label ID="lbl_rsec_idx" runat="server" Visible="false" Text='<%# Eval("m0_group_diary_idx") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("m0_group_diary_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>

                </div>
            </div>

            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ปฏิทินกะเวลาเข้างาน</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div id="ca" class="col-md-4">
                            <div class="form-group">
                                <div class="panel panel-info">
                                    <div class="panel-heading">ช่วงวันที่ต้องการประกาศกะ</div>
                                    <div class="panel-body">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txtDate_Start" runat="server" placeholder="ตั้งแต่วันที่..."
                                                CssClass="form-control from-date-datepicker" />
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                            <asp:RequiredFieldValidator ID="Reqator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                ControlToValidate="txtDate_Start" Font-Size="11"
                                                ErrorMessage="ช่วงวันที่เริ่ม ...." />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorasCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqator1" Width="160" />

                                        </div>
                                        <br />
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txtDate_End" runat="server" placeholder="ถึงวันที่..."
                                                CssClass="form-control from-date-datepicker" />
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                            <asp:RequiredFieldValidator ID="Requirwidator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                ControlToValidate="txtDate_End" Font-Size="11"
                                                ErrorMessage="ช่วงวันที่จบ ...." />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirwidator1" Width="160" />

                                        </div>
                                    </div>
                                </div>
                                <div class='panel panel-info external-events-diary'>
                                    <div class="panel-heading">กะการทำงาน</div>
                                    <div class="panel-body">
                                        <div class="col-sm-12">
                                            <asp:DropDownList ID="ddlparttime" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rews4e4ldValidator1" ValidationGroup="InsertData" runat="server" Display="None"
                                                ControlToValidate="ddlparttime" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกกะทำงาน ...."
                                                ValidationExpression="กรุณาเลือกกะทำงาน ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validat4orCallosutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rews4e4ldValidator1" Width="160" />
                                        </div>
                                    </div>
                                </div>
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnSave" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="InsertData">Save &nbsp;</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnCancel_edit">Cancel &nbsp;</asp:LinkButton>
                            </div>
                        </div>
                        <div id="calendar" class="col-md-8">
                            <div id="fullCalModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                            <h4 id="modalTitle" class="modal-title"></h4>
                                        </div>
                                        <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewShiftchange" runat="server">
            Change
        </asp:View>

        <asp:View ID="ViewShiftholidayswap" runat="server">
            Holiday
        </asp:View>

        <asp:View ID="ViewWaiting" runat="server">
            Witing
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="Label17" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="HyperLink2" />
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="sum_approve" runat="server" /></strong></font></h3>
                </div>
                <asp:Panel ID="boxGvApprove" runat="server">
                    <asp:GridView ID="GvApprove"
                        DataKeyNames="u0_empshift_idx"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10"
                        BorderStyle="None"
                        CellSpacing="2"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="PageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="1%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <%#(Container.DataItemIndex + 1) %>
                                            <%--<asp:Label ID="fwsse" runat="server" Text="วันที่-เวลาเริ่ม : " /></b><asp:Label ID="lbldAssweta" runat="server" Text='<%# Eval("startdate_search") %>'></asp:Label>--%>
                                    </p>                                                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ข้อมูลกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="lblparttime_code" runat="server" Text="รหัสกะ : " /></b><asp:Label ID="lblparttime_code_" runat="server" Text='<%# Eval("parttime_code") %>'></asp:Label>
                                        </p>                                     
                                    <b>
                                        <asp:Label ID="Label53_1" runat="server" Text="ชื่อกะ : " /></b><asp:Label ID="Label5_9" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                        </p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่เริ่มใช้" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="fwsse" runat="server" Text="วันที่-เวลาเริ่ม : " /></b><asp:Label ID="lbldAessweta" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_start")) %>'></asp:Label>
                                        </p> 
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="วันที่-เวลาจบ : " /></b><asp:Label ID="Label2" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_end")) %>'></asp:Label>
                                        </p> 
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อผู้สร้างกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                        </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="องค์กร : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                        </p>
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>

                                        </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="แผนก : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                        </p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                                <ItemTemplate>
                                    <small>
                                        <asp:LinkButton ID="btnedit_emp" CssClass="btn btn-primary" runat="server" CommandName="btnapprove_detail" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%#Eval("u0_empshift_idx") + ";" + Eval("u0_unidx") + ";" + Eval("u0_acidx") + ";" + Eval("u0_doc_decision") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>

                <asp:Panel ID="boxviewApprove" runat="server" Visible="false">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการอนุมัติ</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div id="ca" class="col-md-4">
                                        <div class="form-group">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">ผลการอนุมัติ</div>
                                                <div class="panel-body">
                                                    <asp:FormView ID="FvViewApprove" runat="server" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label48" runat="server" Text="Approve" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label57" runat="server" Text="ผลอนุมัติ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <asp:DropDownList ID="ddl_view_Approve" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                                        <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                                                        <asp:ListItem Value="3">ไม่อนุมัติ(แก้ไข)</asp:ListItem>
                                                                        <asp:ListItem Value="4">ไม่อนุมัติ(ยกเลิก)</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="R2equi3redFgdValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                                        ControlToValidate="ddl_view_Approve" Font-Size="11"
                                                                        ErrorMessage="เลือกผลการอนุมัติ ...."
                                                                        ValidationExpression="เลือกผลการอนุมัติ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValiallosutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R2equi3redFgdValidator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label70" runat="server" Text="Comment" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label109" runat="server" Text="หมายเหตุ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-9">
                                                                    <asp:TextBox ID="tbDocRemark" runat="server" CssClass="form-control" TextMode="multiline" Rows="2"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <asp:LinkButton ID="btnApprove" CssClass="btn btn-success" runat="server" CommandName="btn_Approve" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                                    <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" CommandName="btn_Approve_back" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <asp:LinkButton ID="lbListEmp_temp_approve" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_approve" OnCommand="btnCommand" CommandArgument="11">รายชื่อปัจจุบัน</asp:LinkButton>
                                                </li>
                                                <li class="nav-item">
                                                    <asp:LinkButton ID="lbListGroup_temp_approve" CssClass="btn_menulist" runat="server" CommandName="btnList_Temp_approve" OnCommand="btnCommand" CommandArgument="12">กลุ่มปัจจุบัน</asp:LinkButton>
                                                </li>
                                            </ul>
                                        </div>


                                        <!-- GV Temp List -->
                                        <asp:Panel ID="BoxSearchListEmp_Temp_approve" runat="server" Visible="false">
                                            <div class="form-group" runat="server">
                                                <div class="form-group" runat="server">
                                                    <asp:GridView ID="GvListDB_approve" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="primary"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="true"
                                                        DataKeyNames="emp_idx"
                                                        PageSize="10" Width="90%" HorizontalAlign="Center"
                                                        OnPageIndexChanging="Master_PageIndexChanging">
                                                        <HeaderStyle CssClass="info" />

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%#(Container.DataItemIndex + 1) %>
                                                                    <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_u0_empshift_idx" runat="server" Visible="false" Text='<%# Eval("u0_empshift_idx") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัส" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_sec_name" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <!-- GV Temp Group -->
                                        <asp:Panel ID="BoxSearchGroup_Temp_approve" runat="server" Visible="false">
                                            <div class="form-group" runat="server">
                                                <div class="form-group" runat="server">
                                                    <asp:GridView ID="GvGroupDB_approve" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="primary"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="true"
                                                        DataKeyNames="m0_group_diary_idx"
                                                        PageSize="10" Width="90%" HorizontalAlign="Center"
                                                        OnPageIndexChanging="Master_PageIndexChanging">
                                                        <HeaderStyle CssClass="success" />

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%#(Container.DataItemIndex + 1) %>
                                                                    <asp:Label ID="lbl_rsec_idx" runat="server" Visible="false" Text='<%# Eval("m0_group_diary_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_u0_empshift_idx" runat="server" Visible="false" Text='<%# Eval("u0_empshift_idx") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_name1" runat="server" Text='<%# Eval("group_diary_name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%--<asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="btndel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("m0_group_diary_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>


                                    </div>
                                    <div id="calendar" class="col-md-8">
                                        <div id="fullCalModal" class="modal fade">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                        <h4 id="modalTitle" class="modal-title"></h4>
                                                    </div>
                                                    <div id="modalBody" class="modal-body">uteueuetuteu</div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Log / ประวัติการดำเนินการ</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:Repeater ID="rptLog" runat="server">
                                        <HeaderTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2" style="text-align: center;"><small>วัน / เวลา</small></label>
                                                <label class="col-sm-3" style="text-align: left;"><small>ผู้ดำเนินการ</small></label>
                                                <label class="col-sm-2" style="text-align: left;"><small>ดำเนินการ</small></label>
                                                <label class="col-sm-2" style="text-align: left;"><small>ผลการดำเนินการ</small></label>
                                                <label class="col-sm-2" style="text-align: left;"><small>ความคิดเห็น</small></label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <span><small><%#Eval("emp_createdate")%></small></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><small><%# Eval("emp_name_th") %>(<%# Eval("actor_name") %>)</small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("node_name") %></small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("status_name") %></small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("comment_approve") %></small></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </asp:View>

        <asp:View ID="ViewHr" runat="server">

            <div id="Div1" runat="server" class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="Div2" runat="server">
                            <asp:FormView ID="FvSearch_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="Mounth" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="เดือน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_month_report" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เดือน....</asp:ListItem>
                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="5">พฤษภาคม</asp:ListItem>
                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="7">กรกฏาคม</asp:ListItem>
                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requdir0" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_month_report" Font-Size="11"
                                                ErrorMessage="เลือกเดือน ...."
                                                ValidationExpression="เลือกเดือน ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vali1loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requdir0" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label214" runat="server" ForeColor="Red" Text="***" />
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label12" runat="server" Text="Year" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label13" runat="server" Text="ปี" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_year_report" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="2019">2019</asp:ListItem>
                                                <asp:ListItem Value="2020">2020</asp:ListItem>
                                                <asp:ListItem Value="2021">2021</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requirator1" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_year_report" Font-Size="11"
                                                ErrorMessage="เลือกปี ...."
                                                ValidationExpression="เลือกปี ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valiender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirator1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label10" runat="server" ForeColor="Red" Text="***" />
                                        </div>

                                    </div>

                                    <%--<div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label107" runat="server" Text="Shift" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label108" runat="server" Text="กะที่มีการประกาศ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_shift_report" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกกะทำงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Req12or1" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_shift_report" Font-Size="11"
                                                ErrorMessage="เลือกกะทำงาน ...."
                                                ValidationExpression="เลือกกะทำงาน ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validfder3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req12or1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label120" runat="server" ForeColor="Red" Text="***" />
                                        </div>
                                    </div>--%>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_org_report" runat="server" CssClass="form-control" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                            <asp:RequiredFieldValidator ID="Requaedqor1" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_org_report" Font-Size="11"
                                                ErrorMessage="เลือกบริษัท ...."
                                                ValidationExpression="เลือกบริษัท ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requaedqor1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label11" runat="server" ForeColor="Red" Text="***" />
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_dep_report" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Reator1" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_dep_report" Font-Size="11"
                                                ErrorMessage="เลือกฝ่าย ...."
                                                ValidationExpression="เลือกฝ่าย ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reator1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="***" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label6" runat="server" Text="Section" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label7" runat="server" Text="แผนก" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_sec_report" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกแผนก....</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiidator1" ValidationGroup="Search_report" runat="server" Display="None"
                                                ControlToValidate="ddl_sec_report" Font-Size="11"
                                                ErrorMessage="เลือกแผนก ...."
                                                ValidationExpression="เลือกแผนก ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiidator1" Width="160" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="***" />
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label8" runat="server" Text="Position" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label9" runat="server" Text="ตำแหน่ง" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_pos_report" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกตำแหน่ง....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="Empcode" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label5" runat="server" Text="รหัสพนักงาน" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_search_empcode_report" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="รหัสพนักงาน Ex. 57000001,57000002" />
                                        </div>
                                        <br />
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnsearch_report" class="btn btn-primary" ValidationGroup="Search_report" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" />
                                                    <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                    <asp:Button ID="btnexport_general" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูลทั่วไป)" CommandName="btnexport_general" OnCommand="btnCommand" title="Export"></asp:Button>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                                                    <asp:PostBackTrigger ControlID="btnclearsearch_index" />
                                                    <asp:PostBackTrigger ControlID="btnexport_general" />

                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pn_gridview_list" runat="server" Visible="true">
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>
                        <div class="panel-body">

                            <asp:GridView ID="GvEmployee_Report"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table-responsive GridStyle"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-BackColor="#DCF0DA"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                PageSize="20"
                                DataKeyNames="emp_code"
                                OnRowDataBound="Master_RowDataBound">

                                <%--OnPageIndexChanging="Master_PageIndexChanging"--%>
                                <%--CssClass="table table-striped table-bordered table-hover table-responsive"--%>

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <small>
                                                <%# (Container.DataItemIndex +1) %>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันหยุด" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblholiday" runat="server" Text='<%# Eval("holiday") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblshow_holiday" runat="server"></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="1" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_1" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_1" runat="server" Text='<%# Eval("check_1") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_1" runat="server" Text='<%# Eval("day_1") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="2" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_2" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_2" runat="server" Text='<%# Eval("check_2") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_2" runat="server" Text='<%# Eval("day_2") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="3" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_3" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_3" runat="server" Text='<%# Eval("check_3") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_3" runat="server" Text='<%# Eval("day_3") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="4" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_4" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_4" runat="server" Text='<%# Eval("check_4") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_4" runat="server" Text='<%# Eval("day_4") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="5" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_5" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_5" runat="server" Text='<%# Eval("check_5") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_5" runat="server" Text='<%# Eval("day_5") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="6" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_6" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_6" runat="server" Text='<%# Eval("check_6") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_6" runat="server" Text='<%# Eval("day_6") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="7" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_7" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_7" runat="server" Text='<%# Eval("check_7") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_7" runat="server" Text='<%# Eval("day_7") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="8" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_8" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_8" runat="server" Text='<%# Eval("check_8") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_8" runat="server" Text='<%# Eval("day_8") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="9" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_9" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_9" runat="server" Text='<%# Eval("check_9") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_9" runat="server" Text='<%# Eval("day_9") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="10" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_10" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_10" runat="server" Text='<%# Eval("check_10") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_10" runat="server" Text='<%# Eval("day_10") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="11" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_11" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_11" runat="server" Text='<%# Eval("check_11") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_11" runat="server" Text='<%# Eval("day_11") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="12" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_12" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_12" runat="server" Text='<%# Eval("check_12") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_12" runat="server" Text='<%# Eval("day_12") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="13" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_13" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_13" runat="server" Text='<%# Eval("check_13") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_13" runat="server" Text='<%# Eval("day_13") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="14" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_14" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_14" runat="server" Text='<%# Eval("check_14") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_14" runat="server" Text='<%# Eval("day_14") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="15" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_15" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_15" runat="server" Text='<%# Eval("check_15") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_15" runat="server" Text='<%# Eval("day_15") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="16" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_16" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_16" runat="server" Text='<%# Eval("check_16") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_16" runat="server" Text='<%# Eval("day_16") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="17" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_17" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_17" runat="server" Text='<%# Eval("check_17") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_17" runat="server" Text='<%# Eval("day_17") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="18" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_18" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_18" runat="server" Text='<%# Eval("check_18") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_18" runat="server" Text='<%# Eval("day_18") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="19" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_19" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_19" runat="server" Text='<%# Eval("check_19") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_19" runat="server" Text='<%# Eval("day_19") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="20" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_20" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_20" runat="server" Text='<%# Eval("check_20") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_20" runat="server" Text='<%# Eval("day_20") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="21" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_21" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_21" runat="server" Text='<%# Eval("check_21") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_21" runat="server" Text='<%# Eval("day_21") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="22" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_22" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_22" runat="server" Text='<%# Eval("check_22") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_22" runat="server" Text='<%# Eval("day_22") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="23" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_23" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_23" runat="server" Text='<%# Eval("check_23") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_23" runat="server" Text='<%# Eval("day_23") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="24" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_24" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_24" runat="server" Text='<%# Eval("check_24") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_24" runat="server" Text='<%# Eval("day_24") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="25" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_25" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_25" runat="server" Text='<%# Eval("check_25") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_25" runat="server" Text='<%# Eval("day_25") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="26" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_26" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_26" runat="server" Text='<%# Eval("check_26") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_26" runat="server" Text='<%# Eval("day_26") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="27" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_27" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_27" runat="server" Text='<%# Eval("check_27") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_27" runat="server" Text='<%# Eval("day_27") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="28" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_28" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_28" runat="server" Text='<%# Eval("check_28") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_28" runat="server" Text='<%# Eval("day_28") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="29" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_29" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_29" runat="server" Text='<%# Eval("check_29") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_29" runat="server" Text='<%# Eval("day_29") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="30" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_30" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_30" runat="server" Text='<%# Eval("check_30") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_30" runat="server" Text='<%# Eval("day_30") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_col_31" runat="server" Text=""></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcheck_31" runat="server" Text='<%# Eval("check_31") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblday_31" runat="server" Text='<%# Eval("day_31") %>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>

            <div class="col-md-12">
                <div class="panel-body">
                    <div class="form-group">

                        <asp:Button ID="btnview_gridview_show" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="Show Detail List +" data-original-title="Detail List" data-toggle="tooltip" CommandName="btnview_gridview_show" OnCommand="btnCommand" Visible="false" />
                        <asp:Button ID="btnview_gridview_hide" class="btn btn-danger" ValidationGroup="Search" runat="server" Visible="false" Text="Hide Detail List -" data-original-title="Detail List" data-toggle="tooltip" CommandName="btnview_gridview_hide" OnCommand="btnCommand" />
                        <asp:Button ID="btnview_calendar_show" class="btn btn-primary" runat="server" Text="Show Calendar +" data-original-title="Calendar" data-toggle="tooltip" CommandName="btnview_calendar_show" OnCommand="btnCommand" Visible="false" />
                        <asp:Button ID="btnview_calendar_hide" class="btn btn-danger" runat="server" Visible="false" Text="Hide Calendar -" data-original-title="Calendar" data-toggle="tooltip" CommandName="btnview_calendar_hide" OnCommand="btnCommand" />

                    </div>
                </div>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list" runat="server" /></strong></font></h3>
            </div>

            <asp:Panel ID="Box_viewhr_gridview" runat="server" Visible="false">
                <div class="col-md-12">

                    <div class="form-group">
                        <asp:GridView ID="GvIndex_Report"
                            DataKeyNames="u0_empshift_idx"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover"
                            HeaderStyle-CssClass="table_headCenter"
                            HeaderStyle-Height="40px"
                            AllowPaging="True"
                            PageSize="10"
                            BorderStyle="None"
                            CellSpacing="2"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="PageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="1%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <%#(Container.DataItemIndex + 1) %>
                                                <asp:Label ID="lbl_countdown" runat="server" Visible="false" Text='<%# Eval("countdown") %>'></asp:Label>
                                    </p>                                                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ข้อมูลกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="lblparttime_code" runat="server" Text="รหัสกะ : " /></b><asp:Label ID="lblparttime_code_" runat="server" Text='<%# Eval("parttime_code") %>'></asp:Label>
                                            </p>                                     
                                    <b>
                                        <asp:Label ID="Label53_1" runat="server" Text="ชื่อกะ : " /></b><asp:Label ID="Label5_9" runat="server" Text='<%# Eval("parttime_name_th") %>'></asp:Label>
                                            </p>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่เริ่มใช้" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="fwsse" runat="server" Text="วันที่-เวลาเริ่ม : " /></b><asp:Label ID="lbl_date_start" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_start")) %>'></asp:Label>
                                            </p> 
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Text="วันที่-เวลาจบ : " /></b><asp:Label ID="Label2" runat="server" Text='<%# formatDateTime((String)Eval("announce_diary_date_end")) %>'></asp:Label>
                                            </p> 
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อผู้สร้างกะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <small>
                                            <b>
                                                <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="องค์กร : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                            </p>
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="ฝ่าย : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>

                                            </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="แผนก : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                            </p>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                                    <ItemTemplate>
                                        <small>
                                            <span><%# Eval("status_name") %>&nbsp;/ <%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--<asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btnedit" CssClass="btn btn-primary" runat="server" CommandName="btnedit_detail" OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไขข้อมูล/เปลี่ยนกะ" CommandArgument='<%#Eval("u0_empshift_idx") + ";" + Eval("u0_unidx") + ";" + Eval("u0_acidx") + ";" + Eval("u0_doc_decision") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </asp:Panel>

            <asp:Panel ID="Box_viewhr_calendar" runat="server" Visible="false">
                <div class="col-md-8">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ปฏิทินกะเวลาเข้างาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div id="calendar" class="col-md-12">
                                <div id="fullCalModal" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                                <h4 id="modalTitle" class="modal-title"></h4>
                                            </div>
                                            <div id="modalBody" class="modal-body">test</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </asp:View>

    </asp:MultiView>

    <script src='<%= ResolveUrl("~/Scripts/script-dar-shift.js") %>'></script>


    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }
        $(function () {
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script type="text/javascript">

        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            stepping: 15,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                stepping: 15,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },

                    nowIndicator: true,

                    defaultView: 'month',

                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponseShift.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + event.title + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        //var start = moment(event.start).format("YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");
                        var date_start = moment(event.start, "YYYY-MM-DD");

                        //var current = moment().startOf('day');

                        //alert(moment.duration(given.diff(today)).asDays());

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.parttime_name_th, event.start, event.end, event.description),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + event.title + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });

                            //alert(start + ',' + current_date.toLocaleString("en-US") + ',' + dateFrom);
                            //alert(event.start + ',' + event.end + ',' + dataHoje);

                            if (moment.duration(date_start.diff(today)).asDays() < 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                                //alert("1");
                                element.css('background-color', '#D02090');//pink
                                element.find(".fc-event-dot").css('background-color', '#D02090')
                            }
                            else if ((moment.duration(date_start.diff(today)).asDays() <= 1.5 && moment.duration(date_start.diff(today)).asDays() <= 1.5)) {
                                //alert("2");
                                element.css('background-color', '#00CC00');
                                element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                                //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                            }
                            else if (moment.duration(date_start.diff(today)).asDays() > 1.5 && ((moment.duration(date_end.diff(today)).asDays() > 1.5))) {
                                //alert("3");
                                element.css('background-color', '#1E90FF');
                                element.find(".fc-event-dot").css('background-color', '#1E90FF')
                            }

                        }

                    },
                    //defaultView: 'listWeek'

                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },

                //eventColor: '#1E90FF',

                nowIndicator: true,


                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponseShift.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + event.title + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    //var current = moment().startOf('day');



                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.parttime_name_th, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + event.title + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //target: 'mouse',
                                //adjust: {
                                //    mouse: false
                                //}
                                //my: 'bottom left',Panel_AddCarUseHr
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                                //solo: true 
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                        //element.css('background-color', '#00CC00');
                        //element.find(".fc-event-dot").css('background-color', '#00CC00')//green

                        if (moment.duration(date_start.diff(today)).asDays() < 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                            //alert("1");
                            element.css('background-color', '#D02090');//pink
                            element.find(".fc-event-dot").css('background-color', '#D02090')
                        }
                        else if ((moment.duration(date_start.diff(today)).asDays() <= 1.5 && moment.duration(date_start.diff(today)).asDays() <= 1.5)) {
                            //alert("2");
                            element.css('background-color', '#00CC00');
                            element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                            //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                        }
                        else if (moment.duration(date_start.diff(today)).asDays() > 1.5 && ((moment.duration(date_end.diff(today)).asDays() > 1.5))) {
                            //alert("3");
                            element.css('background-color', '#1E90FF');
                            element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        }

                        //alert(moment.duration(date_start.diff(today)).asDays() + ',' + moment.duration(date_end.diff(today)).asDays());
                        //if (moment.duration(date_start.diff(today)).asDays() <= 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                        //    //alert("1");
                        //    element.css('background-color', '#D02090');//pink
                        //    element.find(".fc-event-dot").css('background-color', '#D02090')
                        //}
                        //else if ((moment.duration(date_start.diff(today)).asDays() <= 1 || moment.duration(date_start.diff(today)).asDays() == 0) && ((moment.duration(date_end.diff(today)).asDays() > 0))) {
                        //    //alert("2");
                        //    element.css('background-color', '#00CC00');
                        //    element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                        //    //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be"));
                        //}
                        //else if (moment.duration(date_start.diff(today)).asDays() > 1 && ((moment.duration(date_end.diff(today)).asDays() > 1))) {
                        //    //alert("3");
                        //    element.css('background-color', '#1E90FF');
                        //    element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        //}



                    }
                }
                //defaultView: 'listWeek'


            });
        });

    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('#calendar').fullCalendar({
                    theme: true,

                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },

                    nowIndicator: true,

                    defaultView: 'month',

                    dayCount: 7,
                    timeFormat: 'h:mm a',
                    allDayText: 'all-day', // set replace all-day
                    selectable: false,
                    selectHelper: false,

                    //select: selectDate, //allDay: true,// this decides if the all day slot should be showed at the top
                    editable: false,
                    events: '<%= ResolveUrl("~/websystem/hr/JsonResponseShift.ashx") %>',
                    eventDrop: eventDropped,
                    eventResize: eventResized,
                    eventLimit: true,

                    eventClick: function (event, jsEvent, view) {
                        $('#modalTitle').html(
                            '<strong>' + event.title + '</strong>'
                        );
                        $('#modalBody').html(
                            //event.description
                            '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                            + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                        );
                        $('#eventUrl').attr('href', event.url);
                        $('#fullCalModal').modal();
                    },

                    eventRender: function (event, element) {
                        var today = moment(new Date()).format("YYYY-MM-DD");
                        //var start = moment(event.start).format("YYYY-MM-DD");
                        var date_end = moment(event.end, "YYYY-MM-DD");
                        var date_start = moment(event.start, "YYYY-MM-DD");

                        //var current = moment().startOf('day');

                        //alert(moment.duration(given.diff(today)).asDays());

                        if (event != null) {
                            element.qtip({
                                content: {
                                    text: qTipText(event.parttime_name_th, event.start, event.end, event.description),
                                    //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                    //title: '<strong>' + 'ทะเบียน' + ' : ' + event.car_register + '</strong>'
                                    title: '<strong>' + event.title + '</strong>'
                                    //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                                },
                                position: {
                                    my: 'top left',
                                    at: 'center left'
                                    //my: 'bottom left',
                                    //at: 'left center'
                                },
                                hide: {
                                    event: 'mouseout'
                                },
                                show: {
                                    event: 'mouseover'
                                },
                                style: {
                                    //classes: 'qtip-shadow qtip-rounded qtip-youtube'
                                    classes: 'qtip-tipsy'
                                }

                            });

                            //alert(start + ',' + current_date.toLocaleString("en-US") + ',' + dateFrom);
                            //alert(event.start + ',' + event.end + ',' + dataHoje);

                            if (moment.duration(date_start.diff(today)).asDays() < 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                                //alert("1");
                                element.css('background-color', '#D02090');//pink
                                element.find(".fc-event-dot").css('background-color', '#D02090')
                            }
                            else if ((moment.duration(date_start.diff(today)).asDays() <= 1.5 && moment.duration(date_start.diff(today)).asDays() <= 1.5)) {
                                //alert("2");
                                element.css('background-color', '#00CC00');
                                element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                                //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                            }
                            else if (moment.duration(date_start.diff(today)).asDays() > 1.5 && ((moment.duration(date_end.diff(today)).asDays() > 1.5))) {
                                //alert("3");
                                element.css('background-color', '#1E90FF');
                                element.find(".fc-event-dot").css('background-color', '#1E90FF')
                            }

                        }

                    },
                    //defaultView: 'listWeek'

                });
            });
        });
        $(function () {
            $('#calendar').fullCalendar({
                theme: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },

                //eventColor: '#1E90FF',

                nowIndicator: true,


                defaultView: 'month',
                dayCount: 7,

                timeFormat: 'h:mm a',
                allDayText: 'all-day', // set replace all-day
                //eventClick: updateEvent,
                selectable: false,
                selectHelper: false,

                //allDay: true,// this decides if the all day slot should be showed at the top
                //select: selectDate,
                editable: false,
                events: '<%= ResolveUrl("~/websystem/hr/JsonResponseShift.ashx") %>',
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,

                eventClick: function (event, jsEvent, view) {
                    $('#modalTitle').html(
                        '<strong>' + event.title + '</strong>'
                    );
                    $('#modalBody').html(
                        //event.description
                        '<strong>' + 'ตั้งแต่วันที่ : ' + '</strong>' + event.start.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'ถึงวันที่ : ' + '</strong>' + event.end.format("DD/MM/YYYY h:mm a") + '<br>'
                        + '<strong>' + 'รายละเอียดเพิ่มเติม : ' + '</strong>' + event.description + '<br>'
                    );
                    $('#eventUrl').attr('href', event.url);
                    $('#fullCalModal').modal();
                },

                eventRender: function (event, element) {
                    var today = moment(new Date()).format("YYYY-MM-DD")
                    //var start = moment(event.start).format("YYYY-MM-DD");

                    var date_start = moment(event.start, "YYYY-MM-DD");
                    var date_end = moment(event.end, "YYYY-MM-DD");
                    //var current = moment().startOf('day');



                    if (event != null) {
                        element.qtip({
                            content: {
                                text: qTipText(event.parttime_name_th, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.start, event.end, event.description),
                                //text: qTipText(event.topic_booking, event.place_name, event.room_name_th, event.start, event.end, event.description),//qTipText(event.start, event.end, event.description, event.topic_booking),
                                title: '<strong>' + event.title + '</strong>'
                                //title: '<strong>' + 'หัวข้อ' + ' : ' + event.title + '</strong>'
                            },
                            position: {
                                my: 'top left',
                                at: 'center left'
                                //target: 'mouse',
                                //adjust: {
                                //    mouse: false
                                //}
                                //my: 'bottom left',Panel_AddCarUseHr
                                //at: 'left center'
                            },
                            hide: {
                                event: 'mouseout'
                            },
                            show: {
                                event: 'mouseover'
                                //solo: true 
                            },
                            style: {
                                //classes: 'qtip-shadow  qtip-youtube'
                                classes: 'qtip-tipsy'
                            }

                        });

                        //element.css('background-color', '#00CC00');
                        //element.find(".fc-event-dot").css('background-color', '#00CC00')//green

                        if (moment.duration(date_start.diff(today)).asDays() < 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                            //alert("1");
                            element.css('background-color', '#D02090');//pink
                            element.find(".fc-event-dot").css('background-color', '#D02090')
                        }
                        else if ((moment.duration(date_start.diff(today)).asDays() <= 1.5 && moment.duration(date_start.diff(today)).asDays() <= 1.5)) {
                            //alert("2");
                            element.css('background-color', '#00CC00');
                            element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                            //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be")); 
                        }
                        else if (moment.duration(date_start.diff(today)).asDays() > 1.5 && ((moment.duration(date_end.diff(today)).asDays() > 1.5))) {
                            //alert("3");
                            element.css('background-color', '#1E90FF');
                            element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        }

                        //alert(moment.duration(date_start.diff(today)).asDays() + ',' + moment.duration(date_end.diff(today)).asDays());
                        //if (moment.duration(date_start.diff(today)).asDays() <= 0.5 && ((moment.duration(date_end.diff(today)).asDays() < 0.5))) {
                        //    //alert("1");
                        //    element.css('background-color', '#D02090');//pink
                        //    element.find(".fc-event-dot").css('background-color', '#D02090')
                        //}
                        //else if ((moment.duration(date_start.diff(today)).asDays() <= 1 || moment.duration(date_start.diff(today)).asDays() == 0) && ((moment.duration(date_end.diff(today)).asDays() > 0))) {
                        //    //alert("2");
                        //    element.css('background-color', '#00CC00');
                        //    element.find(".fc-event-dot").css('background-color', '#00CC00')//green
                        //    //element.find(".fc-event-dot").after($("<span class=\"fc-event-icons\"></span>").html("Whatever you want the content of the span to be"));
                        //}
                        //else if (moment.duration(date_start.diff(today)).asDays() > 1 && ((moment.duration(date_end.diff(today)).asDays() > 1))) {
                        //    //alert("3");
                        //    element.css('background-color', '#1E90FF');
                        //    element.find(".fc-event-dot").css('background-color', '#1E90FF')
                        //}



                    }
                }
                //defaultView: 'listWeek'


            });
        });

    </script>

</asp:Content>

