﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for DataReservation
/// </summary>
/// 
[Serializable]
[XmlRoot("DataReservation")]
public class DataReservation
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    public string CountRows { get; set; }
    public string ReturnCount { get; set; }

    [XmlElement("EquipmentDetail")]
    public Equipment[] EquipmentDetail { get; set; }

    [XmlElement("PurchaseDetail")]
    public Purchase[] PurchaseDetail { get; set; }

    [XmlElement("PriceDetail")]
    public Price[] PriceDetail { get; set; }

    [XmlElement("UnitTypeDetail")]
    public UnitType[] UnitTypeDetail { get; set; }

    [XmlElement("StatusDetail")]
    public Status[] StatusDetail { get; set; }

    [XmlElement("StatusTypeDetail")]
    public StatusType[] StatusTypeDetail { get; set; }

    [XmlElement("OrgLocationDetail")]
    public OrgLocation[] OrgLocationDetail { get; set; }

    [XmlElement("ReservationViewDetail")]
    public ReservationView[] ReservationViewDetail { get; set; }

    [XmlElement("AddPurchaseViewDetail")]
    public AddPurchase[] AddPurchaseViewDetail { get; set; }

    [XmlElement("DeviceDetail")]
    public Device[] DeviceDetail { get; set; }

}



[Serializable]
public class Equipment  /*  EquipmentType DataBox ประเภทอุปกรณ์  */
{
    [XmlElement("EqtIDX")]
    public int EqtIDX { get; set; }
    [XmlElement("NameEquipment")]
    private string _NameEquipment;
    public string NameEquipment { get; set; }

    [XmlElement("EqtID")]
    public int EqtID { get; set; }

    [XmlElement("NameEquip")]
    private string _NameEquip;
    public string NameEquip { get; set; }

    [XmlElement("Relation")]
    public int Relation { get; set; }

    [XmlElement("SortName")]
    private string _SortName;
    public string SortName { get; set; }

    [XmlElement("UseType")]
    public int UseType { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("EStatus")]
    public int EStatus { get; set; }

    [XmlElement("EStatusDetail")]
    private string _EStatusDetail;
    public string EStatusDetail { get; set; }
}

[Serializable]
public class Purchase  /*  PurchaseType DataBox ประเภทการซื้อ */
{
    [XmlElement("TpurIDX")]
    public int TpurIDX { get; set; }

    [XmlElement("NamePurchase")]
    private string _NamePurchase;
    public string NamePurchase { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TStatus")]
    public int TStatus { get; set; }

    [XmlElement("TStatusDetail")]
    private string _TStatusDetail;
    public string TStatusDetail { get; set; }
}

[Serializable]
public class Price  /*  Price DataBox ราคา */
{
    [XmlElement("PriIDX")]
    public int PriIDX { get; set; }

    [XmlElement("PriceDetail")]
    private string _PriceDetail;
    public string PriceDetail { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("PStatus")]
    public int PStatus { get; set; }

    [XmlElement("PStatusDetail")]
    private string _PStatusDetail;
    public string PStatusDetail { get; set; }
}

[Serializable]
public class UnitType  /*  UnitType DataBox ประเภทหน่วยนับ */
{
    [XmlElement("UNIDX")]
    public int UNIDX { get; set; }

    [XmlElement("NameTH")]
    private string _NameTH;
    public string NameTH { get; set; }

    [XmlElement("NameEN")]
    private string _NameEN;
    public string NameEN { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("UStatus")]
    public int UStatus { get; set; }

    [XmlElement("UStatusDetail")]
    private string _UStatusDetail;
    public string UStatusDetail { get; set; }
}

[Serializable]
public class Status  /*  Status DataBox สถานะการดำเนินการ */
{
    [XmlElement("StaIDX")]
    public int StaIDX { get; set; }

    [XmlElement("NameStatusAction")]
    private string _NameStatusAction;
    public string NameStatusAction { get; set; }

    [XmlElement("NameStatus")]
    private string _NameStatus;
    public string NameStatus { get; set; }

    [XmlElement("STIDX")]
    public int STIDX { get; set; }
    
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("SStatus")]
    public int SStatus { get; set; }

    [XmlElement("SStatusDetail")]
    private string _SStatusDetail;
    public string SStatusDetail { get; set; }
}

[Serializable]
public class StatusType  /*  Status DataBox ประเภทสถานะการดำเนินการ */
{
    [XmlElement("STIDX")]
    public int STIDX { get; set; }

    [XmlElement("Name")]
    private string _Name;
    public string Name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("STStatus")]
    public int STStatus { get; set; }

    [XmlElement("STStatusDetail")]
    private string _STStatusDetail;
    public string STStatusDetail { get; set; }

}

[Serializable]
public class OrgLocation  /*  OrgLocation DataBox Relation Org-Location */
{
    [XmlElement("OLIDX")]
    public int OLIDX { get; set; }
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }
    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("OStatus")]
    public int OStatus { get; set; }
}

[Serializable]
public class Device  /*  Device DataBox รหัสอุปกรณ์ */
{
    [XmlElement("EqtIDX")]
    public int EqtIDX { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }

    [XmlElement("DeviceCode")]
    private string _DeviceCode;
    public string DeviceCode { get; set; }

}

[Serializable]
public class ReservationView  /*  ReservationView DataBox  */
{
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpCode")]
    private string _EmpCode;
    public string EmpCode { get; set; }

    [XmlElement("FirstNameTH")]
    private string _FirstNameTH;
    public string FirstNameTH { get; set; }

    [XmlElement("LastNameTH")]
    private string _LastNameTH;
    public string LastNameTH { get; set; }

    [XmlElement("FirstNameEN")]
    private string _FirstNameEN;
    public string FirstNameEN { get; set; }

    [XmlElement("LastNameEN")]
    private string _LastNameEN;
    public string LastNameEN { get; set; }

    [XmlElement("PhoneNo")]
    private string _PhoneNo;
    public string PhoneNo { get; set; }

    [XmlElement("MobileNo")]
    private string _MobileNo;
    public string MobileNo { get; set; }

    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }

    [XmlElement("PrefixNameTH")]
    private string _PrefixNameTH;
    public string PrefixNameTH { get; set; }

    [XmlElement("PrefixNameEN")]
    private string _PrefixNameEN;
    public string PrefixNameEN { get; set; }

    [XmlElement("ResIDX")]
    public int ResIDX { get; set; }

    [XmlElement("Year1")]
    private string _Year1;
    public string Year1 { get; set; }

    [XmlElement("Year2")]
    private string _Year2;
    public string Year2 { get; set; }

    [XmlElement("DeptID")]
    public int DeptID { get; set; }

    [XmlElement("IFTypeDate")]
    public int IFTypeDate { get; set; }

    [XmlElement("IFTypeDate1")]
    public int IFTypeDate1 { get; set; }

    [XmlElement("IFSelectDate")]
    public int IFSelectDate { get; set; }

    [XmlElement("NumberPo")]
    private string _NumberPo;
    public string NumberPo { get; set; }

    [XmlElement("AlertDateResovation")]
    private string _AlertDateResovation;
    public string AlertDateResovation { get; set; }

    [XmlElement("AlertDateResovationEnd")]
    private string _AlertDateResovationEnd;
    public string AlertDateResovationEnd { get; set; }

    [XmlElement("ReceiveDateResovation")]
    private string _ReceiveDateResovation;
    public string ReceiveDateResovation { get; set; }

    [XmlElement("Tell")]
    private string _Tell;
    public string Tell { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    private string _DeptNameTH;
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    private string _DeptNameEN;
    public string DeptNameEN { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }
    
    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecNameTH")]
    private string _SecNameTH;
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    private string _SecNameEN;
    public string SecNameEN { get; set; }

    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("PosNameTH")]
    private string _PosNameTH;
    public string PosNameTH { get; set; }

    [XmlElement("PosNameEN")]
    private string _PosNameEN;
    public string PosNameEN { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    private string _OrgNameTH;
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    private string _OrgNameEN;
    public string OrgNameEN { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    private string _LocName;
    public string LocName { get; set; }

    [XmlElement("OLIDX")]
    public int OLIDX { get; set; }

    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }

    [XmlElement("CostNo")]
    private string _CostNo;
    public string CostNo { get; set; }

    [XmlElement("CostName")]
    private string _CostName;
    public string CostName { get; set; }

    [XmlElement("TpurIDX")]
    public int TpurIDX { get; set; }

    [XmlElement("NamePurchase")]
    private string _NamePurchase;
    public string NamePurchase { get; set; }

    [XmlElement("IDApprove")]
    public int IDApprove { get; set; }

    [XmlElement("EqtIDX")]
    public int EqtIDX { get; set; }

    [XmlElement("EqtIDXType")]
    public int EqtIDXType { get; set; }

    [XmlElement("NameEquipment")]
    private string _NameEquipment;
    public string NameEquipment { get; set; }

    [XmlElement("PriIDX")]
    public int PriIDX { get; set; }

    [XmlElement("Price")]
    private string _Price;
    public string Price { get; set; }

    [XmlElement("DetailReason")]
    private string _DetailReason;
    public string DetailReason { get; set; }

    [XmlElement("DetailMore")]
    private string _DetailMore;
    public string DetailMore { get; set; }

    [XmlElement("STAction")]
    public int STAction { get; set; }

    [XmlElement("StaIDX")]
    public int StaIDX { get; set; }

    [XmlElement("NameStatus")]
    private string _NameStatus;
    public string NameStatus { get; set; }

    [XmlElement("STApprove")]
    public int STApprove { get; set; }

    [XmlElement("NameApprove")]
    private string _NameApprove;
    public string NameApprove { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("RStatus")]
    public int RStatus { get; set; }

    [XmlElement("NameStatusAction")]
    private string _NameStatusAction;
    public string NameStatusAction { get; set; }

    [XmlElement("NameTypeAction")]
    private string _NameTypeAction;
    public string NameTypeAction { get; set; }

    [XmlElement("NameStatusApprove")]
    private string _NameStatusApprove;
    public string NameStatusApprove { get; set; }

    [XmlElement("NameTypeApprove")]
    private string _NameTypeApprove;
    public string NameTypeApprove { get; set; }

    [XmlElement("NewReservation")]
    private string _NewReservation;
    public string NewReservation { get; set; }

    [XmlElement("DeviceCode")]
    private string _DeviceCode;
    public string DeviceCode { get; set; }

    [XmlElement("NewTotal")]
    private string _NewTotal;
    public string NewTotal { get; set; }

    [XmlElement("UNIDX")]
    public int UNIDX { get; set; }

    [XmlElement("NameUnitType")]
    private string _NameUnitType;
    public string NameUnitType { get; set; }

    [XmlElement("Unit")]
    private string _Unit;
    public string Unit { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }

    [XmlElement("ReservationCode")]
    private string _ReservationCode;
    public string ReservationCode { get; set; }
    
    [XmlElement("PoCode")]
    private string _PoCode;
    public string PoCode { get; set; }

    [XmlElement("DetailReceiveOrder")]
    private string _DetailReceiveOrder;
    public string DetailReceiveOrder { get; set; }

    [XmlElement("AlertDateCreateOrder")]
    private string _AlertDateCreateOrder;
    public string AlertDateCreateOrder { get; set; }

    [XmlElement("AlertDateOffOrder")]
    private string _AlertDateOffOrder;
    public string AlertDateOffOrder { get; set; }

    [XmlElement("ResID")]
    public int ResID { get; set; }

    [XmlElement("TotalPo")]
    public int TotalPo { get; set; }

    [XmlElement("CountRow")]
    private string _CountRow;
    public string CountRow { get; set; }

    [XmlElement("DocType")]
    public int DocType { get; set; }

    [XmlElement("AlertDateApprove")]
    private string _AlertDateApprove;
    public string AlertDateApprove { get; set; }


}

[Serializable]
public class AddPurchase  /*  AddPurchase DataBox เพิ่มรายการขอซื้อ */
{
    [XmlElement("EqtIDX")]
    public int EqtIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("NameEquipment")]
    private string _NameEquipment;
    public string NameEquipment { get; set; }

    [XmlElement("TpurIDX")]
    public int TpurIDX { get; set; }

    [XmlElement("NamePurchase")]
    private string _NamePurchase;
    public string NamePurchase { get; set; }

    [XmlElement("DetailReason")]
    private string _DetailReason;
    public string DetailReason { get; set; }

    [XmlElement("EmpCode")]
    private string _EmpCode;
    public string EmpCode { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("OLIDX")]
    public int OLIDX { get; set; }

    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }

    [XmlElement("DeviceCode")]
    private string _DeviceCode;
    public string DeviceCode { get; set; }

    [XmlElement("PriIDX")]
    public int PriIDX { get; set; }

    [XmlElement("DatailMore")]
    private string _DatailMore;
    public string DatailMore { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    private string _LocName;
    public string LocName { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    private string _OrgNameTH;
    public string OrgNameTH { get; set; }

    [XmlElement("Tell")]
    private string _Tell;
    public string Tell { get; set; }

    [XmlElement("UNIDX")]
    public int UNIDX { get; set; }

    [XmlElement("NameTH")]
    private string _NameTH;
    public string NameTH { get; set; }

    [XmlElement("NameEN")]
    private string _NameEN;
    public string NameEN { get; set; }

    [XmlElement("Unit")]
    private string _Unit;
    public string Unit { get; set; }

    [XmlElement("CostNo")]
    private string _CostNo;
    public string CostNo { get; set; }

    [XmlElement("CostName")]
    private string _CostName;
    public string CostName { get; set; }

    [XmlElement("EqtID")]
    public int EqtID { get; set; }

    [XmlElement("NameEquip")]
    private string _NameEquip;
    public string NameEquip { get; set; }
}

