﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_kpi")]
public class data_kpi
{

    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- document ---//
    [XmlElement("kpi_details_u0_list")]
    public kpi_u0_detail[] kpi_details_u0_list { get; set; }

    [XmlElement("kpi_details_u1_list")]
    public kpi_u1_detail[] kpi_details_u1_list { get; set; }

    [XmlElement("kpi_details_u2_list")]
    public kpi_u2_detail[] kpi_details_u2_list { get; set; }


    // --- template ---//
    [XmlElement("kpi_search_template_list")]
    public kpi_search_template_detail[] kpi_search_template_list { get; set; }

}


#region template
[Serializable]
public class kpi_search_template_detail
{
    [XmlElement("u0_detail_idx")]
    public int u0_detail_idx { get; set; }

    [XmlElement("s_u0_detail_idx")]
    public string s_u0_detail_idx { get; set; }

    [XmlElement("s_u1_detail_idx")]
    public string s_u1_detail_idx { get; set; }

    [XmlElement("s_condition")]
    public string s_condition { get; set; }

    [XmlElement("s_cemp_idx")]
    public string s_cemp_idx { get; set; }

}
#endregion template


#region document
[Serializable]
public class kpi_u0_detail
{
    [XmlElement("u0_detail_idx")]
    public int u0_detail_idx { get; set; }

    [XmlElement("u1_detail_idx")]
    public int u1_detail_idx { get; set; }

    [XmlElement("details_title")]
    public string details_title { get; set; }

    [XmlElement("details_desc")]
    public string details_desc { get; set; }

    [XmlElement("unit_desc")]
    public string unit_desc { get; set; }

    [XmlElement("unit_name")]
    public string unit_name { get; set; }

    [XmlElement("m0_unit_type_idx")]
    public int m0_unit_type_idx { get; set; }

    [XmlElement("weight")]
    public string weight { get; set; }

    [XmlElement("annual_target")]
    public string annual_target { get; set; }

    [XmlElement("order_no")]
    public int order_no { get; set; }

    [XmlElement("u0_detail_status")]
    public int u0_detail_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class kpi_u1_detail
{
    [XmlElement("u1_detail_idx")]
    public int u1_detail_idx { get; set; }

    [XmlElement("u0_detail_idx")]
    public int u0_detail_idx { get; set; }

    [XmlElement("details_title")]
    public string details_title { get; set; }

    [XmlElement("measurement_detail")]
    public string measurement_detail { get; set; }

    [XmlElement("m0_unit_type_idx")]
    public int m0_unit_type_idx { get; set; }

    [XmlElement("details_desc")]
    public string details_desc { get; set; }

    [XmlElement("unit_desc")]
    public string unit_desc { get; set; }

    [XmlElement("unit_name")]
    public string unit_name { get; set; }

    [XmlElement("weight")]
    public string weight { get; set; }

    [XmlElement("deadline")]
    public string deadline { get; set; }

    [XmlElement("u1_detail_status")]
    public int u1_detail_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class kpi_u2_detail
{
    [XmlElement("u2_detail_idx")]
    public int u2_detail_idx { get; set; }

    [XmlElement("u0_detail_idx")]
    public int u0_detail_idx { get; set; }

    [XmlElement("u1_detail_idx")]
    public int u1_detail_idx { get; set; }

    [XmlElement("target")]
    public string target { get; set; }

    [XmlElement("actual")]
    public string actual { get; set; }

    [XmlElement("month")]
    public string month { get; set; }

    [XmlElement("month_idx")]
    public int month_idx { get; set; }

    [XmlElement("u2_detail_status")]
    public int u2_detail_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("u2_detail_idx_pre")]
    public int u2_detail_idx_pre { get; set; }

    [XmlElement("target_pre")]
    public string target_pre { get; set; }

    [XmlElement("actual_pre")]
    public string actual_pre { get; set; }

    [XmlElement("month_pre")]
    public string month_pre { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}
#endregion document



