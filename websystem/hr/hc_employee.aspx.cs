﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using System.Drawing;
using System.Collections;

public partial class websystem_hr_hc_employee : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    //-- employee --//

    //-- HR HealthCheck --//
    static string _urlGetEmployeeHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeHistory"];
    static string _urlGetHistoryType = _serviceUrl + ConfigurationManager.AppSettings["urlGetHistoryType"];
    static string _urlGetFormTypeHistoryCheck = _serviceUrl + ConfigurationManager.AppSettings["urlGetFormTypeHistoryCheck"];
    static string _urlGetDetailTopicForm = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTopicForm"];
    static string _urlGetTopicFormType = _serviceUrl + ConfigurationManager.AppSettings["urlGetTopicFormType"];
    static string _urlGetEmployeeProfileHistory = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeProfileHistory"];

    //-- HR HealthCheck --//


    int _emp_idx = 0;
    int _default_int = 0;
    int _r1Regis = 0;
    int ExternalID = 2;
    int ExternalCondition = 6;

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["jobgrade_permission"] = _dataEmployee.employee_list[0].jobgrade_level;

    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            getDiseaseRoInjuryList();
            //initPageLoad();

        }

    }

    #region data table

    protected void getDiseaseRoInjuryList()
    {
       
        DataSet dsDiseaseRoInjuryList = new DataSet();
        dsDiseaseRoInjuryList.Tables.Add("dsDiseaseRoInjuryTable");
        dsDiseaseRoInjuryList.Tables["dsDiseaseRoInjuryTable"].Columns.Add("drDiseaseRoInjuryText", typeof(String));
        dsDiseaseRoInjuryList.Tables["dsDiseaseRoInjuryTable"].Columns.Add("drYearText", typeof(String));
        ViewState["vsDiseaseRoInjuryList"] = dsDiseaseRoInjuryList;
    }

    #endregion

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, "0", 0);

    }

    protected void fvOnDataBound(object sender, EventArgs e)
    {
        var fvName = (FormView)sender;

        switch (fvName.ID)
        {
            case "fvInsertHistoryHealthSick":

                if (fvInsertHistoryHealthSick.CurrentMode == FormViewMode.Insert)
                {

                    var tb = (TextBox)fvInsertHistoryHealthSick.FindControl("tb");
                   

                    //ViewState["vsTopic"]
                    hr_healthcheck_history_topic_detail[] _templist_topic = (hr_healthcheck_history_topic_detail[])ViewState["vsTopic"];

                    var _linqhistoryform = (from datahistoryform in _templist_topic
                                            select new
                                            {
                                                datahistoryform.m0_history_topic_idx,
                                                datahistoryform.history_topic_name
                                            }
                                            ).ToList();


                    string idx_topic = "";
                    string history_topic_name = "";

                    for (int idtopic = 0; idtopic < _linqhistoryform.Count(); idtopic++)
                    {
                        idx_topic = _linqhistoryform[idtopic].m0_history_topic_idx.ToString();
                        history_topic_name = _linqhistoryform[idtopic].history_topic_name.ToString();
                        tb.Text += idx_topic.ToString();

                        switch (idx_topic.ToString())
                        {
                            case "1":
                                //pnDiseaseRoInjury.Visible = true;
                                var lb_name_topic_1 = (Label)fvInsertHistoryHealthSick.FindControl("lb_name_topic_1");
                                lb_name_topic_1.Text = history_topic_name.ToString();
                                break;
                            case "2":
                                var lb_name_topic_2 = (Label)fvInsertHistoryHealthSick.FindControl("lb_name_topic_2");
                                lb_name_topic_2.Text = history_topic_name.ToString();
                                break;
                            case "3":
                                var lb_name_topic_3 = (Label)fvInsertHistoryHealthSick.FindControl("lb_name_topic_3");
                                lb_name_topic_3.Text = history_topic_name.ToString();
                                break;
                            case "4":
                                var lb_name_topic_4 = (Label)fvInsertHistoryHealthSick.FindControl("lb_name_topic_4");
                                lb_name_topic_4.Text = history_topic_name.ToString();
                                break;
                            case "5":
                                var lbl_name_topic_5 = (Label)fvInsertHistoryHealthSick.FindControl("lbl_name_topic_5");
                                lbl_name_topic_5.Text = history_topic_name.ToString();
                                break;
                            case "6":
                                var lbl_name_topic_6 = (Label)fvInsertHistoryHealthSick.FindControl("lbl_name_topic_6");
                                lbl_name_topic_6.Text = history_topic_name.ToString();
                                break;
                            case "7":
                                var lbl_name_topic_7 = (Label)fvInsertHistoryHealthSick.FindControl("lbl_name_topic_7");
                                lbl_name_topic_7.Text = history_topic_name.ToString();
                                break;

                            case "11":
                                litDebug.Text = "333333333333333333";
                                break;

                        }

                    }
                  
                }

                break;

        }

    }



    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "gvDiseaseRoInjuryList":


                    break;
            }

        }

    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;

        switch (rdName.ID)
        {
            case "rdoHistoryType":

                var _rdoHistoryType = (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType");
                if (int.Parse(_rdoHistoryType.SelectedValue) != 0)
                {
                    setActiveTab("docCreateList", 0, 0, 0, "0", int.Parse(_rdoHistoryType.SelectedValue));

                }

                break;

            case "rdoCongenitalDisease":
                var txtCongenitalDisease = (TextBox)fvInsertHistoryHealthSick.FindControl("txtCongenitalDisease");
                if (int.Parse(rdName.SelectedValue) != 0)
                {
                    txtCongenitalDisease.Visible = true;
                }
                else
                {
                    txtCongenitalDisease.Visible = false;
                }

                break;

            case "rdoSurgery":

                var txtSurgery = (TextBox)fvInsertHistoryHealthSick.FindControl("txtSurgery");
                if (int.Parse(rdName.SelectedValue) != 0)
                {
                    txtSurgery.Visible = true;
                }
                else
                {
                    txtSurgery.Visible = false;
                }

                break;

            case "rdoImmunity":

                var txtImmunity = (TextBox)fvInsertHistoryHealthSick.FindControl("txtImmunity");
                if (int.Parse(rdName.SelectedValue) != 0)
                {
                    txtImmunity.Visible = true;
                }
                else
                {
                    txtImmunity.Visible = false;
                }

                break;
        }

    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            switch (textbox.ID)
            {
                case "txtEmpNewEmpCode":

                    //litDebug.Text = "99999";

                    //setFormData(fvInsertHistoryHealth, FormViewMode.Insert, null);
                    Panel PnCreate_EmployeeHistory = (Panel)fvInsertHistoryHealth.FindControl("PnCreate_EmployeeHistory");
                    TextBox txtEmpNewEmpCode = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewEmpCode");
                    TextBox txtEmpIDXHidden = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpIDXHidden");
                    TextBox txtEmpNewFullNameTH = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewFullNameTH");
                    TextBox txtEmpNewOrgNameTH = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewOrgNameTH");
                    TextBox txtEmpNewDeptNameTH = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewDeptNameTH");
                    TextBox txtEmpNewSecNameTH = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewSecNameTH");
                    TextBox txtEmpNewPosNameTH = (TextBox)PnCreate_EmployeeHistory.FindControl("txtEmpNewPosNameTH");
                  

                    data_hr_healthcheck _data_history_emp_profile = new data_hr_healthcheck();
                    _data_history_emp_profile.healthcheck_employee_profile_list = new hr_healthcheck_employee_profile_detail[1];
                    hr_healthcheck_employee_profile_detail _hr_emp_profile = new hr_healthcheck_employee_profile_detail();

                    _hr_emp_profile.emp_code = textbox.Text.Trim();

                    _data_history_emp_profile.healthcheck_employee_profile_list[0] = _hr_emp_profile;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_emp_profile));
                    _data_history_emp_profile = callServicePostHRHealthCheck(_urlGetEmployeeProfileHistory, _data_history_emp_profile);
                    litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_history_emp_profile));
                    if (_data_history_emp_profile.return_code == 1)
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);



                        PnCreate_EmployeeHistory.Visible = true;
                        txtEmpNewEmpCode.Text = String.Empty;
                        txtEmpIDXHidden.Text = String.Empty;
                        txtEmpNewFullNameTH.Text = String.Empty;
                        txtEmpNewOrgNameTH.Text = String.Empty;
                        txtEmpNewDeptNameTH.Text = String.Empty;
                        txtEmpNewSecNameTH.Text = String.Empty;
                        txtEmpNewPosNameTH.Text = String.Empty;

                        //btnInsertPermPerm.Visible = false;
                        //if (textbox.Text.Trim() != string.Empty)
                        //{
                        //    litDebug.Text = "0000000";
                        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลพนักงานดังกล่าว กรุณาติดต่อฝ่ายบุคคล');", true);
                        //    break;
                        //}
                    }
                    else
                    {
                        //setFormData(fvInsertHistoryHealth, FormViewMode.Insert, _data_history_emp_profile.healthcheck_employee_profile_list);
                        PnCreate_EmployeeHistory.Visible = true;
                        txtEmpIDXHidden.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].emp_idx.ToString();                       
                        txtEmpNewFullNameTH.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].emp_name_th;                       
                        txtEmpNewOrgNameTH.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].org_name_th;                      
                        txtEmpNewDeptNameTH.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].dept_name_th;                      
                        txtEmpNewSecNameTH.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].sec_name_th;                      
                        txtEmpNewPosNameTH.Text = _data_history_emp_profile.healthcheck_employee_profile_list[0].pos_name_th;                       
                        //btnInsertPermPerm.Visible = true;
                    }
                    break;
            }
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdDocdetailList":

                string idx_DocHistory = cmdArg;

                switch (idx_DocHistory)
                {
                    case "1": //History Employee

                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0);

                        break;
                    case "2":
                        setActiveTab("docDetailList", 0, 0, 0, idx_DocHistory, 0);
                        break;

                }

                break;


        }
    }

    #endregion event command

    #region bind data
    protected void getHistoryEmployee()
    {

        data_hr_healthcheck _data_hr_healthcheck = new data_hr_healthcheck();

        _data_hr_healthcheck.healthcheck_historyemployee_list = new hr_healthcheck_historyemployee_detail[1];
        hr_healthcheck_historyemployee_detail _hr_healthcheck_list = new hr_healthcheck_historyemployee_detail();
        _hr_healthcheck_list.EmpIDX = _emp_idx;//int.Parse(rd.ToString());

        _data_hr_healthcheck.healthcheck_historyemployee_list[0] = _hr_healthcheck_list;

        _data_hr_healthcheck = callServicePostHRHealthCheck(_urlGetEmployeeHistory, _data_hr_healthcheck);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck));

        setFormData(fvHistoryEmployee, FormViewMode.ReadOnly, _data_hr_healthcheck.healthcheck_historyemployee_list);

    }

    protected void getHistoryType(int type_idx_form, RadioButtonList rdoName)
    {
        //setFormData(fvInsertHistoryHealth, FormViewMode.Insert, null);

        //var rdoHistoryType = (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType");

        data_hr_healthcheck _data_hr_healthcheck_type = new data_hr_healthcheck();
        _data_hr_healthcheck_type.healthcheck_historytype_list = new hr_healthcheck_historytype_detail[1];
        hr_healthcheck_historytype_detail _hr_healthcheck_type_list = new hr_healthcheck_historytype_detail();

        _data_hr_healthcheck_type.healthcheck_historytype_list[0] = _hr_healthcheck_type_list;

        _data_hr_healthcheck_type = callServicePostHRHealthCheck(_urlGetHistoryType, _data_hr_healthcheck_type);

        //rdoName.Items.Clear();
        rdoName.AppendDataBoundItems = true;
        // rdoHistoryType.Items.Add(new ListItem("สิทธิ์ทั่วไป", "2"));
        rdoName.DataSource = _data_hr_healthcheck_type.healthcheck_historytype_list;
        rdoName.DataTextField = "type_name";
        rdoName.DataValueField = "m0_type_idx";
        rdoName.DataBind();
        

        //setRadioButtonListData(rdoHistoryType, _data_hr_healthcheck_type.healthcheck_historytype_list, "type_name", "m0_type_idx");

    }

    protected void getHistoryTopic(int type_form, RadioButtonList rdoName)
    {

        data_hr_healthcheck _data_hr_healthcheck_topic = new data_hr_healthcheck();
        _data_hr_healthcheck_topic.healthcheck_history_topic_list = new hr_healthcheck_history_topic_detail[1];
        hr_healthcheck_history_topic_detail _hr_healthcheck_topic = new hr_healthcheck_history_topic_detail();
        _hr_healthcheck_topic.m0_type_idx = int.Parse(type_form.ToString());
        _data_hr_healthcheck_topic.healthcheck_history_topic_list[0] = _hr_healthcheck_topic;

        _data_hr_healthcheck_topic = callServicePostHRHealthCheck(_urlGetTopicFormType, _data_hr_healthcheck_topic);
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_hr_healthcheck_topic));
       
        ViewState["vsTopic"] = _data_hr_healthcheck_topic.healthcheck_history_topic_list;
        
        rdoName.SelectedValue = type_form.ToString();
    }


    #endregion end bind data

    #region reuse

        protected void initPage()
    {
        //clearSession();
        //clearViewState();

        setActiveTab("docDetailList", 0, 0, 0, "1", 0);

    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRadioButtonListData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_hr_healthcheck callServicePostHRHealthCheck(string _cmdUrl, data_hr_healthcheck _data_hr_healthcheck)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_hr_healthcheck);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_hr_healthcheck = (data_hr_healthcheck)_funcTool.convertJsonToObject(typeof(data_hr_healthcheck), _localJson);

        return _data_hr_healthcheck;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, string doc_history_idx, int type_form)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //Create
        divCreate.Visible = false;
        setFormData(fvInsertHistoryHealth, FormViewMode.ReadOnly, null);
        setFormData(fvInsertHistoryHealthSick, FormViewMode.ReadOnly, null);
        

        //detail history
        divDetailHistory.Visible = false;
       
        switch (activeTab)
        {
            case "docDetailList":

                switch (doc_history_idx)
                {
                    case "1":

                        setOntop.Focus();
                        divDetailHistory.Visible = true;
                        getHistoryEmployee();

                        break;
                }
                break;
            case "docCreateList":

                divCreate.Visible = true;
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                setFormData(fvInsertHistoryHealth, FormViewMode.Insert, null);
                //RadioButtonList rdoHistoryType = (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType");
                getHistoryType(0, (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType"));
                Panel PnCreate_EmployeeHistory = (Panel)fvInsertHistoryHealth.FindControl("PnCreate_EmployeeHistory");
                PnCreate_EmployeeHistory.Visible = false;


                switch (type_form)
                {

                    case 1:

                        PnCreate_EmployeeHistory.Visible = true;
                        getHistoryTopic(type_form, (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType"));

                        break;

                    case 2:


                        PnCreate_EmployeeHistory.Visible = true;
                        getHistoryTopic(type_form, (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType"));

                        setFormData(fvInsertHistoryHealthSick, FormViewMode.Insert, ViewState["vsTopic"]);

                        break;
                    case 3:

                        PnCreate_EmployeeHistory.Visible = true;
                        getHistoryTopic(type_form, (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType"));
                        break;

                    case 4:

                        PnCreate_EmployeeHistory.Visible = true;
                        getHistoryTopic(type_form, (RadioButtonList)fvInsertHistoryHealth.FindControl("rdoHistoryType"));
                        break;
                }

                break;

            case "docReportList":

                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, string m0_lab, int type_form)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, m0_lab, type_form);
        switch (activeTab)
        {
            case "docDetailList":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                break;
            case "docCreateList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");

                break;
            case "docReportList":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");

                break;


        }
    }

    #endregion event reuse

}