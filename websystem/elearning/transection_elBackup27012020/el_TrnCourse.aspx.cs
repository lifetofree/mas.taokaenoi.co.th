﻿using AjaxControlToolkit;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_TrnCourse : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];

    static string _urlGetErrorel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training"];
    static string _urlGetel_lu_position = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_position"];
    //
    static string _urlGetel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_course"];
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];
    static string _urlDelel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_course"];
    static string _urlSetUpdel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_course"];

    static string _urlGetel_u1_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u1_training_req"];
    static string _urlGetel_u1_training_req_Dept = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u1_training_req_Dept"];
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;
    string _gPathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];


    string _FromcourseRunNo = "u_course";

    string _Folder_coursetestimg = "course_test_img";

    string _Folder_courseBin = "course_test_img_bin";

    //object
    FormView _FormView;
    DropDownList _ddlcourse_assessment;
    Image _img_proposition;
    HttpPostedFile file_delImage;
    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");

        if (!IsPostBack)
        {
            //  HttpPostedFile file = Request.Files["UploadedImage"];
            HttpPostedFile file_UploadedImage_proposition = Request.Files["UploadedImage_proposition"];
            if (file_UploadedImage_proposition != null && file_UploadedImage_proposition.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_proposition, 1);
            }
            HttpPostedFile file_UploadedImage_a = Request.Files["UploadedImage_a"];
            if (file_UploadedImage_a != null && file_UploadedImage_a.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_a, 2);
            }
            HttpPostedFile file_UploadedImage_b = Request.Files["UploadedImage_b"];
            if (file_UploadedImage_b != null && file_UploadedImage_b.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_b, 3);
            }
            HttpPostedFile file_UploadedImage_c = Request.Files["UploadedImage_c"];
            if (file_UploadedImage_c != null && file_UploadedImage_c.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_c, 4);
            }
            HttpPostedFile file_UploadedImage_d = Request.Files["UploadedImage_d"];
            if (file_UploadedImage_d != null && file_UploadedImage_d.ContentLength > 0)
            {
                imageProcessRequest(file_UploadedImage_d, 5);
            }

            file_delImage = Request.Files["delImage_1"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("1");
            }
            file_delImage = Request.Files["delImage_2"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("2");
            }
            file_delImage = Request.Files["delImage_3"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("3");
            }
            file_delImage = Request.Files["delImage_4"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("4");
            }
            file_delImage = Request.Files["delImage_5"];
            if (file_delImage != null && file_delImage.ContentLength > 0)
            {
                imageupload_del("5");
            }

            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "COURSE-YEAR"
                                );

            _func_dmu.zSetDdlMonth(ddlMonthSearch_TrnNSur);
            ddlMonthSearch_TrnNSur.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_TrnNSur,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-YEAR"
                                );

            _func_dmu.zSetDdlMonth(ddlMonthSearch_TrnNSurDept);
            ddlMonthSearch_TrnNSurDept.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_TrnNSurDept,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-YEAR"
                                );

            getDatasetEmpty();
            DataSet ds = (DataSet)ViewState["vsel_u2_course"];
            _func_dmu.zSetGridData(GvTest, ds.Tables["dsel_u2_course"]);
            //_func_dmu.zSetGridData(GridView1, ds.Tables["dsel_u2_course"]);
            zSetMode(2);

        }

        setTrigger();

    }

    private void setTrigger()
    {
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(btnSearchTrnNeedsSurvey);
        linkBtnTrigger(btnSearchTrnNeedsSurveyDEPT);
        linkBtnTrigger(btncourse);
        linkBtnTrigger(btntest);
        linkBtnTrigger(btnFilter);
        linkBtnTrigger(btnVideoAdd);

        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnClear);
        linkBtnTrigger(btnevaluationform);
        linkBtnTrigger(btntestAdd);
        linkBtnTrigger(btntestClear);
        linkBtnTrigger(btnAdd);
        linkBtnTrigger(btnsearch_TrnNSur);
        linkBtnTrigger(btnsearch_TrnNSurDept);
        linkBtnTrigger(btnSaveUpdate);
        linkBtnTrigger(btnCancel);
        linkBtnTrigger(btnVideoAdd);
        linkBtnTrigger(btnexport1);
        linkBtnTrigger(btnexport2);

        gridViewTrigger(GvTest);
        gridViewTrigger(GvListData);
        gridViewTrigger(GvDeptTraningList);

    }

    #endregion Page Load

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }



    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        pnlSave.Visible = false;
        switch (AiMode)
        {
            case 0:  //insert mode
                {

                    pnlListData.Visible = false;
                    pnlmenu_detail.Visible = true;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = true;
                    fv_Update.Visible = false;
                    fvCRUD.ChangeMode(FormViewMode.Insert);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                    Select_Page1showdata();

                }
                break;
            case 1:  //update mode
                {

                    pnlListData.Visible = false;
                    pnlmenu_detail.Visible = true;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = false;
                    fv_Update.Visible = true;
                    fv_Update.ChangeMode(FormViewMode.Edit);
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                }
                break;
            case 2://preview mode
                {

                    pnlListData.Visible = true;
                    //pnlselect.Visible = false;
                    //pnlSearchTrnNSur.Visible = false;
                    //pnlSearchTrnNSurDept.Visible = false;
                    pnlmenu_detail.Visible = false;
                    //pnladdvideo.Visible = false;
                    setActiveTab("P");
                    CreateDs_dsListData();
                    ShowListData();
                    MultiViewBody.SetActiveView(View_ListDataPag);

                }
                break;
            case 3://preview mode
                {

                    pnlListData.Visible = false;
                    //pnlselect.Visible = false;
                    //pnlSearchTrnNSur.Visible = true;
                    //pnlSearchTrnNSurDept.Visible = false;
                    setActiveTab("SNS");
                    SetViewStateEmpty();
                    MultiViewBody.SetActiveView(View_RptTraining);

                }
                break;
            case 4://preview mode
                {

                    pnlListData.Visible = false;
                    setActiveTab("SNSDEPT");
                    SetViewStateEmpty();
                    MultiViewBody.SetActiveView(View_RptTrainingByDept);

                }
                break;
        }
    }
    #endregion zSetMode

    private void ShowListDataSearch(string sName = "")
    {

        _data_elearning.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.filter_keyword = sName;
        obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_TrnNSur.SelectedValue);
        _data_elearning.el_traning_req_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u1_training_req, _data_elearning);

        _func_dmu.zSetGridData(GvListData_TrnNSur, _data_elearning.el_traning_req_action);

        ViewState["GvListData_TrnNSur"] = _data_elearning.el_traning_req_action;

    }

    private void ShowListDataSearchDept(string sName = "")
    {

        _data_elearning.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.filter_keyword = sName;
        obj.zmonth = int.Parse(ddlMonthSearch_TrnNSurDept.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_TrnNSurDept.SelectedValue);
        _data_elearning.el_traning_req_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u1_training_req_Dept, _data_elearning);

        _func_dmu.zSetGridData(GvListData_TrnNSurDept, _data_elearning.el_traning_req_action);
        ViewState["GvListData_TrnNSurDept"] = _data_elearning.el_traning_req_action;

    }

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        liSearchTrnNeedsSurvey.Attributes.Add("class", "");
        liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "");
        litraining.Attributes.Add("class", "");
        litest.Attributes.Add("class", "");
        lievaluationform.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                litraining.Attributes.Add("class", "active");
                break;
            case "SNS":
                liSearchTrnNeedsSurvey.Attributes.Add("class", "active");
                break;
            case "SNSDEPT":
                liSearchTrnNeedsSurveyDEPT.Attributes.Add("class", "active");
                break;
            case "evaluationform":
                lievaluationform.Attributes.Add("class", "active");
                break;
            case "test":
                litest.Attributes.Add("class", "active");
                break;
        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {
        Select_DetailList();
    }
    protected void Select_DetailList()
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(_data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);

        if (_data_elearning.employeeM_action != null)
        {
            foreach (var item in _data_elearning.employeeM_action)
            {
                ViewState["DocCode"] = item.EmpCode;
                ViewState["CEmpIDX_Create"] = item.EmpIDX;
                ViewState["RPosIDX_J"] = item.RPosIDX_J;
                ViewState["RDeptID"] = item.RDeptID;
            }
        }


    }

    private void SetViewStateEmpty()
    {
        ViewState["DocCode"] = "";
        ViewState["CEmpIDX_Create"] = "";
        ViewState["RPosIDX_J"] = "0";
        ViewState["RDeptID"] = "0";
        getDatasetEmpty();
        DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];


    }
    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    protected void Select_Page1showdata()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj = new course();

        obj.operation_status_id = "U0-EMPTY";
        dataelearning.el_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_course_action);

        zModeData("E");
        Hddfld_course_no.Value = "";
        Hddfld_status.Value = "I";
        Hddfld_u0_course_idx.Value = "";
        TextBox txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        GridView GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");

        txtcourse_date.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _func_dmu.zDropDownList(ddllevel_code,
                                "",
                                "level_code",
                                "level_code",
                                "0",
                                "trainingLoolup",
                                "",
                                "LEVEL"

                                );
        _func_dmu.zClearDataDropDownList(ddm0_training_branch_idx);
        _func_dmu.zDropDownList(ddlm0_training_group_idx_ref,
                                "",
                                "m0_training_group_idx",
                                "training_group_name",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-GROUP"
                                );

        _func_dmu.zGetOrganizationList(ddlOrg_search);
        _func_dmu.zClearDataDropDownList(ddlDept_search);
        _func_dmu.zClearDataDropDownList(ddlSec_search);

        _func_dmu.zGridLookUp(GvCourse, "trainingLoolup", "TRN-COURSE-LIST");
        _func_dmu.zGridLookUp(GvM0_EmpTarget, "trainingLoolup", "EMP-TARGET");

        DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
        _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
        DataSet Dsu2 = (DataSet)ViewState["vsel_u2_course"];
        _func_dmu.zSetGridData(GvTest, Dsu2.Tables["dsel_u2_course"]);
        setGvEmpTarget_searchList();

    }

    protected void getDatasetEmpty() // สร้าง dataset
    {
        CreateDs_dsEmployeeTable();
        CreateDs_dsEmpTraningTable();
        CreateDs_dsEmpTraningTable1();
        CreateDs_dsAddVedio();
        CreateDs_dsevaluation();
        CreateDs_dstest();
        CreateDs_el_u1_course();
        CreateDs_el_u5_course();
    }
    private void CreateDs_dsEmployeeTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmployee");
        ds.Tables["dsEmployee"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RPosIDX_J", typeof(String));
        ViewState["vsEmployeeList"] = ds;
    }
    private void CreateDs_dsEmpTraningTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning");
        ds.Tables["dsEmpTraning"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraningList"] = ds;
    }

    private void CreateDs_dsEmpTraningTable1()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning1");
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraning1List"] = ds;
    }

    private void CreateDs_dsListData()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsListData");
        ds.Tables["dsListData"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsListData"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsListData"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsListData"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Date", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Date_From", typeof(DateTime));
        ds.Tables["dsListData"].Columns.Add("Date_To", typeof(DateTime));
        ds.Tables["dsListData"].Columns.Add("Qty_HH", typeof(float));
        ds.Tables["dsListData"].Columns.Add("Amount_trn", typeof(float));
        ds.Tables["dsListData"].Columns.Add("status", typeof(int));
        ViewState["vsListData"] = ds;
    }

    private void ShowListData()
    {

        _data_elearning.el_course_action = new course[1];
        course obj = new course();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.operation_status_id = "U0";
        _data_elearning.el_course_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetGridData(GvListData, _data_elearning.el_course_action);

    }


    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[11].FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[11].FindControl("btnDelete_GvListData");
                TextBox txtapprove_status = (TextBox)e.Row.Cells[11].FindControl("txttrn_plan_flag_GvListData");
                LinkButton btnDetail = (LinkButton)e.Row.Cells[11].FindControl("btnDetail");
                linkBtnTrigger(btnUpdate_GvListData);
                linkBtnTrigger(btnDelete_GvListData);
                linkBtnTrigger(btnDetail);
                Boolean bBl = true;
                if (txtapprove_status.Text == "1") // 4	อนุมัติ
                {
                    bBl = false;
                }
                // btnUpdate_GvListData.Visible = bBl;
                btnDelete_GvListData.Visible = bBl;
            }
        }
        else if (sGvName == "GvTest")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label btncourse_proposition_img_GvTestx = (Label)e.Row.Cells[1].FindControl("btncourse_proposition_img_GvTest");
                Label btncourse_propos_a_img_GvTest = (Label)e.Row.Cells[2].FindControl("btncourse_propos_a_img_GvTest");
                Label btncourse_propos_b_img_GvTest = (Label)e.Row.Cells[3].FindControl("btncourse_propos_b_img_GvTest");
                Label btncourse_propos_c_img_GvTest = (Label)e.Row.Cells[4].FindControl("btncourse_propos_c_img_GvTest");
                Label btncourse_propos_d_img_GvTest = (Label)e.Row.Cells[5].FindControl("btncourse_propos_d_img_GvTest");
                LinkButton btnRemove_GvTest = (LinkButton)e.Row.Cells[8].FindControl("btnRemove_GvTest");
                //linkBtnTrigger(btncourse_proposition_img_GvTestx);
                //linkBtnTrigger(btncourse_propos_a_img_GvTest);
                //linkBtnTrigger(btncourse_propos_b_img_GvTest);
                //linkBtnTrigger(btncourse_propos_c_img_GvTest);
                //linkBtnTrigger(btncourse_propos_d_img_GvTest);
                linkBtnTrigger(btnRemove_GvTest);


                Label lbcourse_proposition_img_GvTest = (Label)e.Row.Cells[1].FindControl("lbcourse_proposition_img_GvTest");
                Label lbcourse_propos_a_img_GvTest = (Label)e.Row.Cells[2].FindControl("lbcourse_propos_a_img_GvTest");
                Label lbcourse_propos_b_img_GvTest = (Label)e.Row.Cells[3].FindControl("lbcourse_propos_b_img_GvTest");
                Label lbcourse_propos_c_img_GvTest = (Label)e.Row.Cells[4].FindControl("lbcourse_propos_c_img_GvTest");
                Label lbcourse_propos_d_img_GvTest = (Label)e.Row.Cells[5].FindControl("lbcourse_propos_d_img_GvTest");

                setBtnImage(btncourse_proposition_img_GvTestx, lbcourse_proposition_img_GvTest);
                setBtnImage(btncourse_propos_a_img_GvTest, lbcourse_propos_a_img_GvTest);
                setBtnImage(btncourse_propos_b_img_GvTest, lbcourse_propos_b_img_GvTest);
                setBtnImage(btncourse_propos_c_img_GvTest, lbcourse_propos_c_img_GvTest);
                setBtnImage(btncourse_propos_d_img_GvTest, lbcourse_propos_d_img_GvTest);

            }
        }
        else if (sGvName == "GvDeptTraningList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnRemove_GvDeptTraningList = (LinkButton)e.Row.Cells[5].FindControl("btnRemove_GvDeptTraningList");
                linkBtnTrigger(btnRemove_GvDeptTraningList);
            }
        }

    }
    private void setBtnImage(Label _linkButton, Label _Label)
    {
        if (_Label.Text == "")
        {
            _linkButton.Visible = false;
        }
        else
        {
            _linkButton.Visible = true;
        }
        _linkButton.Visible = false;
    }
    public string getImgUrl(string ADocno = "", string AItem = "", string AImage = "")
    {
        string sPathImage = "";

        if (AImage == "")
        {
            sPathImage = "";
        }
        else
        {
            if (ADocno != "")
            {
                sPathImage = _gPathFile + _Folder_courseBin + "/" + ADocno + "/" + AItem + "/" + AImage;
            }
            else
            {
                sPathImage = _gPathFile + _Folder_coursetestimg + "/" + Hddfld_course_no.Value + "/" + AItem + "/" + AImage;
            }
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public static bool UrlExists(string url)
    {
        try
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request == null) return false;
            request.Method = "HEAD";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (UriFormatException)
        {
            //Invalid Url
            return false;
        }
        catch (WebException)
        {
            //Unable to access url
            return false;
        }
    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                // showmodal_traning(txtsearch_modal_training.Text);
                break;
            case "gvModalTraning":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                //showmodal_traning_app(txtsearch_modal_training_app.Text);
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData();
                SETFOCUS.Focus();
                break;
            case "GvListData_TrnNSur":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListDataSearch(txtFilterKeyword_TrnNSur.Text);
                SETFOCUS.Focus();
                break;
            case "GvListData_TrnNSurDept":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListDataSearchDept(txtFilterKeyword_TrnNSurDept.Text);
                SETFOCUS.Focus();
                break;
                
        }
    }
    protected string getStatus(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatusPlan(int status)
    {
        if (status == 1)
        {

            return "<span class='' data-toggle='tooltip' title='แผนการฝึกอบรม'><i class='glyphicon glyphicon-file'></i></span>";
        }
        else
        {
            return "";
        }
    }
    protected string getCourseType(int course_type_etraining, int course_type_elearning)
    {
        string a = "";
        if (course_type_etraining > 0)
        {
            a = "ClassRoom";
        }
        string b = "";
        if (course_type_elearning > 0)
        {
            b = "E-Learning";
        }
        string sValue = "";
        if ((a != "") && (b != ""))
        {
            sValue = a + " / " + b;
        }
        else if (a != "")
        {
            sValue = a;
        }
        else if (b != "")
        {
            sValue = b;
        }
        return sValue;
    }
    protected string getpriority(int status)
    {

        if (status == 1)
        {
            return "<span>Must</span>";
        }
        else if (status == 2)
        {
            return "<span>Need</span>";
        }
        else if (status == 3)
        {
            return "<span>Want</span>";
        }
        else
        {
            return "<span style='font-weight:700;'>-</span>";
        }
    }
    protected string getStatustest(int status)
    {
        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_idx;
        int _cemp_idx;

        m0_training objM0_ProductType = new m0_training();

        switch (cmdName)
        {
            case "btnListData":
                zSetMode(2);
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btncourse":
                pnlmenu_detail.Visible = true;

                litest.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "");
                litraining.Attributes.Add("class", "active");
                MultiViewBody.SetActiveView(View_trainingPage);
                pnlSave.Visible = true;
                setActiveTab("I");
                deleteFileFDImage();
                break;
            case "btnInsert":
                getDatasetEmpty();
                zSetMode(0);
                pnlmenu_detail.Visible = true;
                evaluation_Select();
                MultiViewBody.SetActiveView(View_trainingPage);
                btnSaveInsert.Visible = true;
                btnSaveUpdate.Visible = false;
                pnlSave.Visible = true;
                deleteFileFDImage();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnSaveInsert":
                if (checkError() == false)
                {
                    if (zSaveInsert() == true)
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;
            case "btnDelete":
                _m0_training_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
                zDelete(_m0_training_idx);
                ShowListData();
                break;
            case "btnFilter":
                ShowListData();
                break;

            case "btnVideoAdd":
                addTraning();
                break;


            case "btnSearchTrnNeedsSurvey":
                zSetMode(3);
                txtFilterKeyword_TrnNSur.Text = "";
                ShowListDataSearch("");
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btnsearch_TrnNSur":
                ShowListDataSearch(txtFilterKeyword_TrnNSur.Text);
                break;
            case "btnselGvListData_TrnNSur":
                _m0_training_idx = int.Parse(cmdArg);
                pnlListData.Visible = false;
                setActiveTab("I");
                Select_Page1showdata();
                break;
            case "btnSearchTrnNeedsSurveyDEPT":
                zSetMode(4);
                txtFilterKeyword_TrnNSurDept.Text = "";
                ShowListDataSearchDept("");
                pnlmenu_detail.Visible = false;
                deleteFileFDImage();
                break;
            case "btntest":

                litraining.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "");
                litest.Attributes.Add("class", "active");
                MultiViewBody.SetActiveView(View_testform);
                //linkBtnTrigger(btntestAdd);
                //  linkBtnTrigger(btntestClear);
                // gridViewTrigger(GvTest);
                cleartest();
                break;
            case "btnevaluationform":

                litraining.Attributes.Add("class", "");
                litest.Attributes.Add("class", "");
                lievaluationform.Attributes.Add("class", "active");

                MultiViewBody.SetActiveView(View_evaluationform);
                evaluationList();
                deleteFileFDImage();
                break;
            case "btnImport":

                break;
            case "btntestAdd":
                int item = 0;
                // TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
                if (txtcourse_item.Text != "")
                {
                    try
                    {
                        item = int.Parse(txtcourse_item.Text);
                    }
                    catch { }
                }
                if (item <= 0)
                {
                    showAlert("กรุณากรอกลำดับให้ถูกต้อง");

                }
                else
                {
                    addtest();
                }
                break;
            case "btntestClear":
                cleartest();

                break;

            case "btnAdd":
                addDeptTraning();

                break;

            case "btnUpdate_GvListData":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);

                    zShowdataUpdate(_m0_training_idx, "E");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = true;
                }
                if (Hddfld_course_no.Value == "")
                {
                    zSetMode(2);
                }
                break;
            case "btnSaveUpdate":

                if (checkErrorUpdate() == false)
                {
                    if (Hddfld_course_no.Value == "")
                    {
                        zSetMode(2);
                    }
                    else
                    {
                        if (zSaveUpdate(int.Parse(Hddfld_u0_course_idx.Value)) == true)
                        {
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }

                    }
                }

                break;
            case "btnDetail":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "P");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = false;
                }
                if (Hddfld_course_no.Value == "")
                {
                    zSetMode(2);
                }


                break;
            case "btnsearch_TrnNSurDept":
                ShowListDataSearchDept(txtFilterKeyword_TrnNSurDept.Text);
                break;
            case "btnsearch_GvCourse":
                searchCOURSEList();
                break;
            case "btnexport1":

                GvListData_TrnNSur_Excel.DataSource = ViewState["GvListData_TrnNSur"];
                GvListData_TrnNSur_Excel.DataBind();
                ExportGridToExcel(GvListData_TrnNSur_Excel, "รายงานสรุปหลักสูตรที่ขอ", "รายงานสรุปหลักสูตรที่ขอ");
                break;
            case "btnexport2":
                GvListData_TrnNSurDept_Excel.DataSource = ViewState["GvListData_TrnNSurDept"];
                GvListData_TrnNSurDept_Excel.DataBind();
                ExportGridToExcel(GvListData_TrnNSurDept_Excel, "รายงานสรุปหลักสูตรที่ขอของแต่ละแผนก", "รายงานสรุปหลักสูตรที่ขอของแต่ละแผนก");
                break;

        }
    }
    #endregion btnCommand
    private FormView getFv(string _sMode)
    {
        if (_sMode == "I")
        {
            return fvCRUD;
        }
        else
        {
            return fv_Update;
        }
    }
    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_course_no.Value));
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            if (FvName.ID == "fvCRUD")
            {
                if (FvName.CurrentMode == FormViewMode.Insert)
                {
                    LinkButton btnsearch_GvCourse = (LinkButton)fvCRUD.FindControl("btnsearch_GvCourse");
                    linkBtnTrigger(btnsearch_GvCourse);
                    // setTrigger();
                }
            }
            else if (FvName.ID == "fv_Update")
            {
                if (FvName.CurrentMode == FormViewMode.Edit)
                {
                    LinkButton btnsearch_GvCourse = (LinkButton)fv_Update.FindControl("btnsearch_GvCourse");
                    linkBtnTrigger(btnsearch_GvCourse);
                    //setTrigger();
                }
            }
        }
        else if (sender is DropDownList)
        {

            var FvName = (DropDownList)sender;
            switch (FvName.ID)
            {
                case "ddlm0_training_group_idx_ref":

                    DropDownList ddlm0_training_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_training_group_idx_ref");
                    DropDownList ddm0_training_branch_idx = (DropDownList)_FormView.FindControl("ddm0_training_branch_idx");

                    _func_dmu.zDropDownListWhereID(ddm0_training_branch_idx,
                                                "",
                                                "m0_training_branch_idx",
                                                "training_branch_name",
                                                "0",
                                                "trainingLoolup",
                                                "",
                                                "TRN-BRANCH",
                                                "m0_training_group_idx_ref",
                                                _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue)
                                                );
                    searchCOURSEList();
                    //setTrigger();

                    break;
                case "ddm0_training_branch_idx":

                    searchCOURSEList();
                    //setTrigger();

                    break;

                case "ddlOrg_search":
                    _func_dmu.zGetDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedItem.Value));
                    setGvEmpTarget_search();
                    //setTrigger();
                    break;
                case "ddlDept_search":
                    _func_dmu.zGetSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedItem.Value), int.Parse(ddlDept_search.SelectedItem.Value));
                    setGvEmpTarget_search();
                    //  setTrigger();
                    break;
            }
        }
    }


    #endregion FvDetail_DataBound

    public void showmodal_traning(string sSearch = "")
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "TRN";
        obj_trainingLoolup.fil_Search = sSearch;
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        //  _func_dmu.zSetGridData(gvModalTraning, _data_elearning.trainingLoolup_action);
    }

    public void showmodal_traning_app(string sSearch = "")
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "TRN";
        obj_trainingLoolup.fil_Search = sSearch;
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        // _func_dmu.zSetGridData(gvModalTraning_app, _data_elearning.trainingLoolup_action);
    }



    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void btnuser(object sender, CommandEventArgs e)
    {

    }
    public string gettraining_req_type(int id)
    {
        string sRetrun = "";
        if (id == 0)
        {
            sRetrun = "ฝึกอบรมภายใน";
        }
        else if (id == 1)
        {
            sRetrun = "	ฝึกอบรมภายนอก";
        }
        else
        {
            sRetrun = "ฝึกอบรมภายใน/ฝึกอบรมภายนอก";
        }
        return sRetrun;
    }

    private void CreateDs_dsAddVedio()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsAddVedio");
        ds.Tables["dsAddVedio"].Columns.Add("id", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("item", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("title", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("description", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("file_path", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage", typeof(String));
        ds.Tables["dsAddVedio"].Columns.Add("fileimage_path", typeof(String));
        ViewState["vsAddVedioList"] = ds;

    }
    protected void BtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = this.Page.FindControl("UpdatePanel1") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void FileUploadTrigger(FileUpload _FileUpload)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = _FileUpload.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    private void ClearTraning()
    {
        Select_Page1showdata();
    }

    private void CreateDs_dsevaluation()
    {
        string sDs = "dsEvaluation";
        string sVs = "vsEvaluation";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_group_name", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_f_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("evaluation_f_name", typeof(String));
        ViewState[sVs] = ds;

    }
    private void evaluationList()
    {
        _func_dmu.zDropDownList(ddlm0_evaluation_group_idx_refsel,
                                 "",
                                 "zId",
                                 "zName",
                                 "0",
                                 "trainingLoolup",
                                 "",
                                 "EVALUA-GRP"

                                 );

        txtevaluation_f_name_sel.Text = "";

    }
    private void evaluation_Select()
    {

        data_elearning dataelearning = new data_elearning();
        trainingLoolup obj = new trainingLoolup();
        dataelearning.trainingLoolup_action = new trainingLoolup[1];
        obj.operation_status_id = "EVALUA-GRP-LIST";
        dataelearning.trainingLoolup_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning);

        CreateDs_dsevaluation();
        DataSet Ds = (DataSet)ViewState["vsEvaluation"];
        DataRow dr;
        if (dataelearning.trainingLoolup_action != null)
        {

            foreach (var item in dataelearning.trainingLoolup_action)
            {
                dr = Ds.Tables["dsEvaluation"].NewRow();
                dr["id"] = "1";
                dr["evaluation_group_idx"] = item.zId_G.ToString();
                dr["evaluation_group_name"] = item.zName_G;
                dr["evaluation_f_idx"] = item.zId.ToString();
                dr["evaluation_f_name"] = item.zName;
                Ds.Tables["dsEvaluation"].Rows.Add(dr);
            }

        }
        ViewState["vsEvaluation"] = Ds;
        _func_dmu.zSetGridData(GvEvaluation, Ds.Tables["dsEvaluation"]);
    }

    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }

    private void addTraning()
    {
        //******************  start U3 *********************//
        CreateDs_dsevaluation();
        DataSet Ds = (DataSet)ViewState["vsEvaluation"];
        DataRow dr;
        int ic = 0;
        foreach (var item in GvEvaluation.Rows)
        {
            CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
            Label lbevaluation_group_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_idx_GvEvalu");
            Label lbevaluation_f_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_idx_GvEvalu");
            Label lbevaluation_f_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_name_GvEvalu");
            Label lbevaluation_group_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_name_GvEvalu");

            dr = Ds.Tables["dsEvaluation"].NewRow();
            if (GvEvaluation_cb_sel.Checked == true)
            {
                dr["id"] = "1";
            }
            else
            {
                dr["id"] = "0";
            }

            dr["evaluation_group_idx"] = lbevaluation_group_idx_GvEvalu.Text;
            dr["evaluation_group_name"] = lbevaluation_group_name_GvEvalu.Text;
            dr["evaluation_f_idx"] = lbevaluation_f_idx_GvEvalu.Text;
            dr["evaluation_f_name"] = lbevaluation_f_name_GvEvalu.Text;
            Ds.Tables["dsEvaluation"].Rows.Add(dr);

            ic++;
        }

        ViewState["vsEvaluation"] = Ds;
        //******************  end U3 *********************//

        DataSet dsAddVedio = (DataSet)ViewState["vsEvaluation"];
        DataRow drAddVedio;
        drAddVedio = dsAddVedio.Tables["dsEvaluation"].NewRow();
        drAddVedio["id"] = "1";
        drAddVedio["evaluation_group_idx"] = ddlm0_evaluation_group_idx_refsel.SelectedValue;
        drAddVedio["evaluation_group_name"] = ddlm0_evaluation_group_idx_refsel.Items[ddlm0_evaluation_group_idx_refsel.SelectedIndex].ToString();
        drAddVedio["evaluation_f_name"] = txtevaluation_f_name_sel.Text;
        dsAddVedio.Tables["dsEvaluation"].Rows.Add(drAddVedio);
        ViewState["vsEvaluation"] = dsAddVedio;
        _func_dmu.zSetGridData(GvEvaluation, dsAddVedio.Tables["dsEvaluation"]);
        ddlm0_evaluation_group_idx_refsel.SelectedIndex = 0;
        txtevaluation_f_name_sel.Text = "";

    }

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btnRemove_GvTest":

                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsAddVedio = (DataSet)ViewState["vsel_u2_course"];
                    dsAddVedio.Tables["dsel_u2_course"].Rows[rowIndex].Delete();
                    dsAddVedio.AcceptChanges();
                    _func_dmu.zSetGridData(GvTest, dsAddVedio.Tables["dsel_u2_course"]);
                    deleteFileFDImage();
                    
                    break;
                case "btnRemove_GvDeptTraningList":
                    GridViewRow rowSelectDept = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndexDept = rowSelectDept.RowIndex;
                    DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
                    dsu1.Tables["dsel_u1_course"].Rows[rowIndexDept].Delete();
                    dsu1.AcceptChanges();
                    _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
                    break;

            }
        }
    }
    private void CreateDs_dstest()
    {
        string sDs = "dsel_u2_course";
        string sVs = "vsel_u2_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_answer", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_score", typeof(String));
        ds.Tables[sDs].Columns.Add("docno_bin", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d_img", typeof(String));
        ViewState[sVs] = ds;

    }
    public void deleteFileFDImage()
    {
        string _PathFile1 = ConfigurationSettings.AppSettings["path_flie_elearning"];
        string _PathFile2 = "";
        if ((Hddfld_folderImage.Value == null) || (Hddfld_folderImage.Value == ""))
        {

        }
        else
        {
            for(int i = 1; i <= 5; i++)
            {
                try
                {
                    _PathFile2 = _PathFile1 + _Folder_courseBin + "/" + Hddfld_folderImage.Value+"/"+i.ToString()+".png";
                    File.Delete(Server.MapPath(_PathFile2));
                }
                catch { }
            }
            try
            {
                _PathFile2 = _PathFile1 + _Folder_courseBin + "/" + Hddfld_folderImage.Value;
                Directory.Delete(Server.MapPath(_PathFile2));
            }
            catch { }
        }
    }
    private void addtest()
    {

        //TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
        //TextBox txtcourse_proposition = (TextBox)fvDetail.FindControl("txtcourse_proposition");
        //TextBox txtcourse_propos_a = (TextBox)fvDetail.FindControl("txtcourse_propos_a");
        //TextBox txtcourse_propos_b = (TextBox)fvDetail.FindControl("txtcourse_propos_b");
        //TextBox txtcourse_propos_c = (TextBox)fvDetail.FindControl("txtcourse_propos_c");
        //TextBox txtcourse_propos_d = (TextBox)fvDetail.FindControl("txtcourse_propos_d");
        //DropDownList ddlcourse_propos_answer = (DropDownList)fvDetail.FindControl("ddlcourse_propos_answer");
        //FileUpload UploadImages_proposition = (FileUpload)fvDetail.FindControl("UploadImages_proposition");
        //FileUpload UploadImages_a = (FileUpload)fvDetail.FindControl("UploadImages_a");
        //FileUpload UploadImages_b = (FileUpload)fvDetail.FindControl("UploadImages_b");
        //FileUpload UploadImages_c = (FileUpload)fvDetail.FindControl("UploadImages_c");
        //FileUpload UploadImages_d = (FileUpload)fvDetail.FindControl("UploadImages_d");


        string _sDocNo = "";
        int i = 0, idata = 0;
        idata = int.Parse(txtcourse_item.Text);
        DataSet ds = (DataSet)ViewState["vsel_u2_course"];
        foreach (DataRow item in ds.Tables["dsel_u2_course"].Rows)
        {
            if (item["docno_bin"].ToString() != "")
            {
                _sDocNo = item["docno_bin"].ToString();
            }

            if (_func_dmu.zStringToInt(item["course_item"].ToString()) == idata)
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("ลำดับนี้มีอยู่แล้วกรุณากรอกใหม่");
            deleteFileFDImage();
            txtcourse_item.Focus();
        }
        else
        {
            DataRow dr;

            dr = ds.Tables["dsel_u2_course"].NewRow();
            dr["course_item"] = txtcourse_item.Text;
            dr["course_proposition"] = txtcourse_proposition.Text;
            dr["course_propos_a"] = txtcourse_propos_a.Text;
            dr["course_propos_b"] = txtcourse_propos_b.Text;
            dr["course_propos_c"] = txtcourse_propos_c.Text;
            dr["course_propos_d"] = txtcourse_propos_d.Text;
            dr["course_propos_answer"] = ddlcourse_propos_answer.SelectedValue;

            // _func_dmu.zSetGridData(GridView1, ds.Tables["dsel_u2_course"]);
            if (_sDocNo == "")
            {
                _sDocNo = "emp_idx" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");

            }

            int _int = 0;

            if (zUploadImagesBin(UploadImages_proposition, _sDocNo, int.Parse(txtcourse_item.Text), 1) == 1)
            {
                _int++;
                dr["course_proposition_img"] = "1.png";
            }
            else
            {
                dr["course_proposition_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_a, _sDocNo, int.Parse(txtcourse_item.Text), 2) == 1)
            {
                _int++;
                dr["course_propos_a_img"] = "2.png";
            }
            else
            {
                dr["course_propos_a_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_b, _sDocNo, int.Parse(txtcourse_item.Text), 3) == 1)
            {
                _int++;
                dr["course_propos_b_img"] = "3.png";
            }
            else
            {
                dr["course_propos_b_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_c, _sDocNo, int.Parse(txtcourse_item.Text), 4) == 1)
            {
                _int++;
                dr["course_propos_c_img"] = "4.png";
            }
            else
            {
                dr["course_propos_c_img"] = "";
            }

            if (zUploadImagesBin(UploadImages_d, _sDocNo, int.Parse(txtcourse_item.Text), 5) == 1)
            {
                _int++;
                dr["course_propos_d_img"] = "5.png";
            }
            else
            {
                dr["course_propos_d_img"] = "";
            }
            if (_int > 0)
            {
                dr["docno_bin"] = _sDocNo;
            }
            else
            {
                dr["docno_bin"] = "";
            }

            ds.Tables["dsel_u2_course"].Rows.Add(dr);
            ViewState["vsel_u2_course"] = ds;
            _func_dmu.zSetGridData(GvTest, ds.Tables["dsel_u2_course"]);

            cleartest();
        }
    }
    private void cleartest()
    {
        //TextBox txtcourse_item = (TextBox)fvDetail.FindControl("txtcourse_item");
        //TextBox txtcourse_proposition = (TextBox)fvDetail.FindControl("txtcourse_proposition");
        //TextBox txtcourse_propos_a = (TextBox)fvDetail.FindControl("txtcourse_propos_a");
        //TextBox txtcourse_propos_b = (TextBox)fvDetail.FindControl("txtcourse_propos_b");
        //TextBox txtcourse_propos_c = (TextBox)fvDetail.FindControl("txtcourse_propos_c");
        //TextBox txtcourse_propos_d = (TextBox)fvDetail.FindControl("txtcourse_propos_d");
        //DropDownList ddlcourse_propos_answer = (DropDownList)fvDetail.FindControl("ddlcourse_propos_answer");

        deleteFileFDImage();

        txtcourse_item.Text = "";
        txtcourse_proposition.Text = "";
        txtcourse_propos_a.Text = "";
        txtcourse_propos_b.Text = "";
        txtcourse_propos_c.Text = "";
        txtcourse_propos_d.Text = "";
        ddlcourse_propos_answer.SelectedIndex = 0;

    }

    public string gettest(string id)
    {
        string sNane = "";

        if (id == "1")
        {
            sNane = "ก";
        }
        else if (id == "2")
        {
            sNane = "ข";
        }
        else if (id == "3")
        {
            sNane = "ค";
        }
        else if (id == "4")
        {
            sNane = "ง";
        }

        return sNane;
    }
    private string getbranchcode(int id)
    {
        string sreturn = "";
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup obj = new trainingLoolup();
        obj.idx = id;
        obj.operation_status_id = "TRN-GROUP";
        _data_elearning.trainingLoolup_action[0] = obj;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        if (_data_elearning.trainingLoolup_action != null)
        {
            obj = _data_elearning.trainingLoolup_action[0];
            sreturn = obj.doc_no;
        }

        return sreturn;
    }
    private Boolean zSaveInsert()
    {
        Boolean _Boolean = false;
        // U0
        TextBox txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        CheckBox cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fvCRUD.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fvCRUD.FindControl("ddlcourse_status");
        GridView GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        _ddlcourse_assessment = (DropDownList)fvCRUD.FindControl("ddlcourse_assessment");
        TextBox txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");

        string m0_prefix = "", _docno = "";

        m0_prefix = getbranchcode(int.Parse(ddlm0_training_group_idx_ref.SelectedValue));
        if (m0_prefix == "")
        {
            showAlert("รหัสกลุ่มวิชาไม่ถูกต้อง");

        }
        else
        {
            int idx = 0, icourse_score = 0, u0_course_idx_ref = 0;
            course obj_course = new course();
            _data_elearning.el_course_action = new course[1];
           // obj_course.course_no = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYYY-COURSE", "N", "0000");
            //_docno = obj_course.course_no;

            obj_course.course_date = _func_dmu.zDateToDB(txtcourse_date.Text);
            obj_course.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
            obj_course.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
            obj_course.course_name = txtcourse_name.Text;
            obj_course.course_remark = txtcourse_remark.Text;
            obj_course.level_code = int.Parse(ddllevel_code.SelectedValue);
            obj_course.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
            obj_course.course_status = int.Parse(ddlcourse_status.SelectedValue);
            obj_course.course_assessment = _func_dmu.zStringToInt(_ddlcourse_assessment.SelectedValue);
            obj_course.score_through_per = _func_dmu.zStringToDecimal(txtscore_through_per.Text);

            if (txtcourse_score.Text != "")
            {
                icourse_score = int.Parse(txtcourse_score.Text);
            }
            obj_course.course_score = icourse_score;
            obj_course.course_created_by = emp_idx;
            obj_course.course_updated_by = emp_idx;
            obj_course.course_type_etraining = _func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
            obj_course.course_type_elearning = _func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
            obj_course.course_plan_status = "I";

            //node
            obj_course.approve_status = 0;
            obj_course.u0_idx = 5;
            obj_course.node_idx = 9;
            obj_course.actor_idx = 1;
            obj_course.app_flag = 0;
            obj_course.app_user = 0;

            obj_course.operation_status_id = "U0";
            _data_elearning.el_course_action[0] = obj_course;
            _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning);
            u0_course_idx_ref = _data_elearning.return_code;
            
            if (u0_course_idx_ref > 0)
            {
                _docno = _data_elearning.el_course_action[0].course_no;
                zSaveDetail(u0_course_idx_ref, _docno, "I");

            }
            _Boolean = true;
        }
        return _Boolean;

    }

    private void zSaveDetail(int _id, string _sDocNo, string _sMode)
    {
        if (_id <= 0)
        {
            return;
        }
        TextBox txtcourse_no;
        DropDownList ddlm0_training_group_idx_ref;
        DropDownList ddm0_training_branch_idx;
        TextBox txtcourse_name;
        TextBox txtcourse_date;
        TextBox txtcourse_remark;
        DropDownList ddllevel_code;
        TextBox txtcourse_score;
        CheckBox cbcourse_type_etraining;
        CheckBox cbcourse_type_elearning;
        DropDownList ddlcourse_priority;
        DropDownList ddlcourse_status;
        GridView GvCourse;
        GridView GvM0_EmpTarget;

        string _docno = "";

        // U0
        if (_sMode == "I")
        {
            txtcourse_no = (TextBox)fvCRUD.FindControl("txtcourse_no");
            ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
            txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
            txtcourse_date = (TextBox)fvCRUD.FindControl("txtcourse_date");
            txtcourse_remark = (TextBox)fvCRUD.FindControl("txtcourse_remark");
            ddllevel_code = (DropDownList)fvCRUD.FindControl("ddllevel_code");
            txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
            cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
            cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
            ddlcourse_priority = (DropDownList)fvCRUD.FindControl("ddlcourse_priority");
            ddlcourse_status = (DropDownList)fvCRUD.FindControl("ddlcourse_status");
            GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
            GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        }
        else
        {
            txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
            ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
            txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
            txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
            txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
            ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
            txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
            cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
            cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
            ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
            ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
            GvCourse = (GridView)fv_Update.FindControl("GvCourse");
            GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        }
        _docno = txtcourse_no.Text;
        //showAlert(_sDocNo);
        int ic = 0, icount = 0, irow = 0, u0_course_idx_ref = 0;
        int itemObj = 0;

        u0_course_idx_ref = _id;
        data_elearning dataelearning = new data_elearning();
        //******************  start U1 *********************//
        itemObj = 0;
        DataSet dsU1 = (DataSet)ViewState["vsel_u1_course"];
        course[] objcourse1 = new course[dsU1.Tables["dsel_u1_course"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsel_u1_course"].Rows)
        {
            objcourse1[itemObj] = new course();
            objcourse1[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse1[itemObj].RSecID_ref = _func_dmu.zStringToInt(item["RSecID_ref"].ToString());
            objcourse1[itemObj].RDeptID_ref = _func_dmu.zStringToInt(item["RDeptID_ref"].ToString());
            objcourse1[itemObj].org_idx_ref = _func_dmu.zStringToInt(item["org_idx_ref"].ToString());
            objcourse1[itemObj].TIDX_ref = _func_dmu.zStringToInt(item["TIDX_ref"].ToString());
            objcourse1[itemObj].course_status = 1;
            objcourse1[itemObj].course_updated_by = emp_idx;
            objcourse1[itemObj].operation_status_id = "U1";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse1 = new course[1];
            objcourse1[itemObj] = new course();
            objcourse1[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse1[itemObj].operation_status_id = "U1-DEL";
        }
        dataelearning.el_course_action = objcourse1;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U1 *********************//

        //******************  start U2 *********************//
        itemObj = 0;
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        DataSet dsU2 = (DataSet)ViewState["vsel_u2_course"];
        course[] objcourse2 = new course[dsU2.Tables["dsel_u2_course"].Rows.Count];
        foreach (DataRow item in dsU2.Tables["dsel_u2_course"].Rows)
        {
            objcourse2[itemObj] = new course();
            objcourse2[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse2[itemObj].course_item = _func_dmu.zStringToInt(item["course_item"].ToString());
            objcourse2[itemObj].course_proposition = item["course_proposition"].ToString();
            objcourse2[itemObj].course_propos_a = item["course_propos_a"].ToString();
            objcourse2[itemObj].course_propos_b = item["course_propos_b"].ToString();
            objcourse2[itemObj].course_propos_c = item["course_propos_c"].ToString();
            objcourse2[itemObj].course_propos_d = item["course_propos_d"].ToString();
            objcourse2[itemObj].course_propos_answer = item["course_propos_answer"].ToString();

            //if (item["docno_bin"].ToString() != "")
            //{
            objcourse2[itemObj].course_proposition_img = item["course_proposition_img"].ToString();
            objcourse2[itemObj].course_propos_a_img = item["course_propos_a_img"].ToString();
            objcourse2[itemObj].course_propos_b_img = item["course_propos_b_img"].ToString();
            objcourse2[itemObj].course_propos_c_img = item["course_propos_c_img"].ToString();
            objcourse2[itemObj].course_propos_d_img = item["course_propos_d_img"].ToString();
            //}

            objcourse2[itemObj].course_status = 1;
            objcourse2[itemObj].course_created_by = emp_idx;
            objcourse2[itemObj].course_updated_by = emp_idx;
            objcourse2[itemObj].operation_status_id = "U2";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse2 = new course[1];
            objcourse2[itemObj] = new course();
            objcourse2[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse2[itemObj].operation_status_id = "U2-DEL";
        }
        dataelearning.el_course_action = objcourse2;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        if (itemObj > 0)
        {
            string docno_bin = "";
            foreach (DataRow item in dsU2.Tables["dsel_u2_course"].Rows)
            {
                if (item["docno_bin"].ToString() != "")
                {
                    docno_bin = item["docno_bin"].ToString();
                    zUploadImagesElearning(docno_bin,
                                             _sDocNo,
                                            _func_dmu.zStringToInt(item["course_item"].ToString())
                                             );
                }
            }
            zDelImagesBin(docno_bin);
        }
        else
        {
            zDelImagesBin(_sDocNo);
        }
        //******************  end U2 *********************//

        //******************  start U3 *********************//
        itemObj = 0;
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        foreach (var item in GvEvaluation.Rows)
        {
            CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
            if (GvEvaluation_cb_sel.Checked == true)
            {
                irow++;

            }
            ic++;

        }
        course[] objcourse3 = new course[irow];
        if (irow > 0)
        {

            itemObj = 0;
            ic = 0;
            icount = 0;
            irow = 0;
            ic = 0;
            foreach (var item in GvEvaluation.Rows)
            {
                CheckBox GvEvaluation_cb_sel = (CheckBox)GvEvaluation.Rows[ic].FindControl("GvEvaluation_cb_sel");
                if (GvEvaluation_cb_sel.Checked == true)
                {
                    irow++;
                    Label lbevaluation_group_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_group_idx_GvEvalu");
                    Label lbevaluation_f_idx_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_idx_GvEvalu");
                    Label lbevaluation_f_name_GvEvalu = (Label)GvEvaluation.Rows[ic].FindControl("lbevaluation_f_name_GvEvalu");

                    objcourse3[itemObj] = new course();
                    objcourse3[itemObj].u0_course_idx_ref = u0_course_idx_ref;
                    objcourse3[itemObj].m0_evaluation_f_idx_ref = _func_dmu.zStringToInt(lbevaluation_f_idx_GvEvalu.Text);
                    objcourse3[itemObj].evaluation_f_item = 0;
                    objcourse3[itemObj].m0_evaluation_group_idx_ref = _func_dmu.zStringToInt(lbevaluation_group_idx_GvEvalu.Text);
                    objcourse3[itemObj].course_evaluation_f_name_th = lbevaluation_f_name_GvEvalu.Text;
                    objcourse3[itemObj].course_item = irow;
                    objcourse3[itemObj].course_status = 1;
                    objcourse3[itemObj].course_created_by = emp_idx;
                    objcourse3[itemObj].course_updated_by = emp_idx;
                    objcourse3[itemObj].operation_status_id = "U3";
                    itemObj++;
                }
                ic++;
            }
        }
        else
        {
            objcourse3 = new course[1];
            objcourse3[itemObj] = new course();
            objcourse3[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse3[itemObj].operation_status_id = "U3-DEL";
        }

        dataelearning.el_course_action = objcourse3;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U3 *********************//

        //******************  start U4 *********************//
        ic = 0;
        icount = 0;
        irow = 0;
        ic = 0;
        foreach (var item in GvCourse.Rows)
        {
            CheckBox GvCourse_cb_zId = (CheckBox)GvCourse.Rows[ic].FindControl("GvCourse_cb_zId");
            if (GvCourse_cb_zId.Checked == true)
            {
                irow++;
            }
            ic++;
        }
        itemObj = 0;
        icount = 0;
        course[] objcourse4 = new course[irow];
        ic = 0;
        if (irow > 0)
        {

            foreach (var item in GvCourse.Rows)
            {
                CheckBox GvCourse_cb_zId = (CheckBox)GvCourse.Rows[ic].FindControl("GvCourse_cb_zId");
                if (GvCourse_cb_zId.Checked == true)
                {
                    irow++;
                    Label GvCourse_zId = (Label)GvCourse.Rows[ic].FindControl("GvCourse_zId");
                    //itemObj = 0;
                    objcourse4[itemObj] = new course();
                    objcourse4[itemObj].u0_course_idx_ref = u0_course_idx_ref;
                    objcourse4[itemObj].u0_course_idx_ref_pass = int.Parse(GvCourse_zId.Text);
                    objcourse4[itemObj].course_updated_by = emp_idx;
                    objcourse4[itemObj].operation_status_id = "U4";
                    itemObj++;
                }
                ic++;
            }

        }
        if (itemObj == 0)
        {
            objcourse4 = new course[1];
            objcourse4[itemObj] = new course();
            objcourse4[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse4[itemObj].operation_status_id = "U4-DEL";
        }
        dataelearning.el_course_action = objcourse4;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(dataelearning));
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u_course, dataelearning);
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);

        //******************  end U4 *********************//

        //******************  start U5 *********************//


        ic = 0;
        icount = 0;
        //foreach (var item in GvM0_EmpTarget.Rows)
        //{
        //    CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
        //    if (GvM0_EmpTarget_cb_zId.Checked == true)
        //    {
        //        icount++;
        //    }

        //    ic++;
        //}
        CreateDs_el_u5_course();

        DataSet dsu5 = (DataSet)ViewState["vsel_u5_course"];
        DataRow dru5;
        ic = 0;
        icount = 0;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            dru5 = dsu5.Tables["dsel_u5_course"].NewRow();
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {

                dru5["TIDX_flag"] = "1";

            }
            else
            {
                dru5["TIDX_flag"] = "0";
            }
            Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
            dru5["TIDX_ref"] = GvM0_EmpTarget_zId.Text;
            dsu5.Tables["dsel_u5_course"].Rows.Add(dru5);

            ic++;
        }
        ViewState["vsel_u5_course"] = dsu5;

        itemObj = 0;
        dsu5 = (DataSet)ViewState["vsel_u5_course"];
        course[] objcourse5 = new course[dsu5.Tables["dsel_u5_course"].Rows.Count];
        foreach (DataRow item in dsu5.Tables["dsel_u5_course"].Rows)
        {
            objcourse5[itemObj] = new course();
            objcourse5[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse5[itemObj].TIDX_ref = _func_dmu.zStringToInt(item["TIDX_ref"].ToString());
            objcourse5[itemObj].TIDX_flag = _func_dmu.zStringToInt(item["TIDX_flag"].ToString());
            objcourse5[itemObj].course_status = 1;
            objcourse5[itemObj].course_updated_by = emp_idx;
            objcourse5[itemObj].operation_status_id = "U5";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse5 = new course[1];
            objcourse5[itemObj] = new course();
            objcourse5[itemObj].u0_course_idx_ref = u0_course_idx_ref;
            objcourse5[itemObj].operation_status_id = "U5-DEL";
        }
        dataelearning.el_course_action = objcourse5;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, dataelearning);
        //******************  end U5 *********************//



    }

    private void CreateDs_el_u1_course()
    {
        string sDs = "dsel_u1_course";
        string sVs = "vsel_u1_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RSecID_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("RPosIDX_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("org_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("course_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_ref", typeof(String));

        ds.Tables[sDs].Columns.Add("org_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("dept_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("sec_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("target_name", typeof(String));
        ViewState[sVs] = ds;

    }

    private void addDeptTraning()
    {
        GridView GvM0_EmpTarget = GvM0_EmpTarget_search;  //(GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        int ic = 0, icount = 0;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {
                icount++;
            }

            ic++;
        }
        if ((icount > 0) && (ddlOrg_search.SelectedValue != "0") && (ddlDept_search.SelectedValue != "0") && (ddlSec_search.SelectedValue != "0"))
        {
            DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
            DataRow dru1;
            ic = 0;
            icount = 0;
            foreach (var item in GvM0_EmpTarget.Rows)
            {
                CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
                if (GvM0_EmpTarget_cb_zId.Checked == true)
                {
                    dru1 = dsu1.Tables["dsel_u1_course"].NewRow();
                    dru1["id"] = "1";
                    dru1["org_idx_ref"] = ddlOrg_search.SelectedValue;
                    dru1["org_name_th"] = _func_dmu.zGetDataDropDownList(ddlOrg_search);
                    dru1["RDeptID_ref"] = ddlDept_search.SelectedValue;
                    dru1["dept_name_th"] = _func_dmu.zGetDataDropDownList(ddlDept_search);
                    dru1["RSecID_ref"] = ddlSec_search.SelectedValue;
                    dru1["sec_name_th"] = _func_dmu.zGetDataDropDownList(ddlSec_search);
                    Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
                    Label GvM0_EmpTarget_zName = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zName");
                    dru1["TIDX_ref"] = GvM0_EmpTarget_zId.Text;
                    dru1["target_name"] = GvM0_EmpTarget_zName.Text;
                    dsu1.Tables["dsel_u1_course"].Rows.Add(dru1);

                    icount++;
                }

                ic++;
            }
            ViewState["vsel_u1_course"] = dsu1;
            _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);
            ddlOrg_search.SelectedIndex = 0;
            ddlDept_search.SelectedIndex = 0;
            ddlSec_search.SelectedIndex = 0;
            setGvEmpTarget_searchList();
        }



    }
    public Boolean checkError()
    {
        Boolean _Boolean = false;

        CheckBox cbcourse_type_etraining = (CheckBox)fvCRUD.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fvCRUD.FindControl("cbcourse_type_elearning");
        TextBox txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");
        if ((cbcourse_type_etraining.Checked == false) &&
            (cbcourse_type_elearning.Checked == false))
        {
            _Boolean = true;
            showAlert("กรุณาเลือกประเภท ClassRoom / E-Learning");
        }
        else if((_func_dmu.zStringToDecimal(txtscore_through_per.Text) < 0) ||
            (_func_dmu.zStringToDecimal(txtscore_through_per.Text) > 100)
            )
        {
            _Boolean = true;
            showAlert("กรุณากรอกคะแนนผ่าน % ให้ถูกต้อง");
        }

        return _Boolean;
    }

    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u0_course_idx_ref = id;
        obj_course.course_updated_by = emp_idx;
        _data_elearning.el_course_action[0] = obj_course;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u_course, _data_elearning);
    }

    protected void zSelect_Page1showdata(int id)
    {



    }

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void zModeData(string _Mode)
    {
        _func_dmu.zModePanel(pnlAddDeptTraningList, _Mode);
        _func_dmu.zModePanel(pnlAddtest, _Mode);
        _func_dmu.zModePanel(pnlAddEvaluation, _Mode);
        _func_dmu.zModeGridViewCol(GvDeptTraningList, _Mode, 5);
        //_func_dmu.zModeGridView(GvTest, _Mode, 7);
        _func_dmu.zModeGridViewCol(GvEvaluation, _Mode, 1);
    }
    private void zShowdataUpdate(int id, string _Mode)
    {
        fv_Update.ChangeMode(FormViewMode.Edit);
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_course_action = new course[1];
        course obj = new course();
        obj.u0_course_idx_ref = id;
        obj.operation_status_id = "U0-FULL";
        dataelearning.el_course_action[0] = obj;
        // litDebug.Text = _func_dmu.zJson(_urlGetel_u_course, dataelearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(dataelearning));

        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning);


        _func_dmu.zSetFormViewData(fv_Update, dataelearning.el_course_action);


        TextBox txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
        GridView GvCourse = (GridView)fv_Update.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
        _ddlcourse_assessment = (DropDownList)fv_Update.FindControl("ddlcourse_assessment");

        //Start SetMode

        _func_dmu.zModeTextBox(txtcourse_name, _Mode);
        _func_dmu.zModeTextBox(txtcourse_date, _Mode);
        _func_dmu.zModeTextBox(txtcourse_remark, _Mode);
        _func_dmu.zModeTextBox(txtcourse_score, _Mode);
        _func_dmu.zModeDropDownList(ddlm0_training_group_idx_ref, _Mode);
        _func_dmu.zModeDropDownList(ddm0_training_branch_idx, _Mode);
        _func_dmu.zModeDropDownList(ddllevel_code, _Mode);
        _func_dmu.zModeDropDownList(ddlcourse_priority, _Mode);
        _func_dmu.zModeDropDownList(ddlcourse_status, _Mode);
        _func_dmu.zModeDropDownList(_ddlcourse_assessment, _Mode);

        _func_dmu.zModeCheckBox(cbcourse_type_etraining, _Mode);
        _func_dmu.zModeCheckBox(cbcourse_type_elearning, _Mode);

        _func_dmu.zModeGridViewCol(GvM0_EmpTarget, _Mode, 0);
        _func_dmu.zModeGridViewCol(GvTest, _Mode, 8);

        //_func_dmu.zModeLinkButton(btnSaveUpdate, _Mode);

        _func_dmu.zModeGridViewCol(GvCourse, _Mode, 0);
        zModeData(_Mode);
        //End SetMode

        Hddfld_course_no.Value = "";
        Hddfld_u0_course_idx.Value = "";
        if (dataelearning.el_course_action != null)
        {

            obj = dataelearning.el_course_action[0];
            Hddfld_course_no.Value = obj.course_no;
            Hddfld_u0_course_idx.Value = obj.u0_course_idx.ToString();
            cbcourse_type_etraining.Checked = _func_dmu.zIntToBoolean(obj.course_type_etraining);
            cbcourse_type_elearning.Checked = _func_dmu.zIntToBoolean(obj.course_type_elearning);

            _func_dmu.zDropDownList(ddllevel_code,
                                "",
                                "level_code",
                                "level_code",
                                "0",
                                "trainingLoolup",
                                obj.level_code.ToString(),
                                "LEVEL"

                                );

            _func_dmu.zDropDownList(ddlm0_training_group_idx_ref,
                                    "",
                                    "m0_training_group_idx",
                                    "training_group_name",
                                    "0",
                                    "trainingLoolup",
                                    obj.m0_training_group_idx_ref.ToString(),
                                    "TRN-GROUP"
                                    );
            _func_dmu.zDropDownListWhereID(ddm0_training_branch_idx,
                                                "",
                                                "m0_training_branch_idx",
                                                "training_branch_name",
                                                "0",
                                                "trainingLoolup",
                                                obj.m0_training_branch_idx_ref.ToString(),
                                                "TRN-BRANCH",
                                                "m0_training_group_idx_ref",
                                                _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue)
                                                );

            _func_dmu.zGetOrganizationList(ddlOrg_search);
            _func_dmu.zClearDataDropDownList(ddlDept_search);
            _func_dmu.zClearDataDropDownList(ddlSec_search);

            data_elearning dataelearning_u4 = new data_elearning();
            trainingLoolup obj_u4 = new trainingLoolup();
            dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
            obj_u4.idx = int.Parse(Hddfld_u0_course_idx.Value);
            if (_Mode == "P")
            {
                obj_u4.operation_status_id = "TRN-COURSE";
            }
            else
            {
                obj_u4.operation_status_id = "TRN-COURSE-WHEREID";
                obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
                obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);
            }

            dataelearning_u4.trainingLoolup_action[0] = obj_u4;
            dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
            CreateDs_Clone();
            DataSet Ds = (DataSet)ViewState["vsclone"];
            DataRow dr;
            int ic = 0;
            if (dataelearning_u4.trainingLoolup_action != null)
            {
                foreach (var item in dataelearning_u4.trainingLoolup_action)
                {
                    dr = Ds.Tables["dsclone"].NewRow();
                    dr["id"] = item.zId_G.ToString();
                    dr["zId"] = item.zId.ToString();
                    dr["zName"] = item.zName;
                    Ds.Tables["dsclone"].Rows.Add(dr);
                    ic++;
                }
            }

            ViewState["vsclone"] = Ds;
            _func_dmu.zSetGridData(GvCourse, Ds.Tables["dsclone"]);

            data_elearning dataelearning_u5 = new data_elearning();
            trainingLoolup obj_u5 = new trainingLoolup();
            dataelearning_u5.trainingLoolup_action = new trainingLoolup[1];
            obj_u5.idx = int.Parse(Hddfld_u0_course_idx.Value);
            if (_Mode == "P")
            {
                obj_u5.operation_status_id = "TRN-COURSE-U5-P";
            }
            else
            {
                obj_u5.operation_status_id = "TRN-COURSE-U5";
            }

            dataelearning_u5.trainingLoolup_action[0] = obj_u5;
            dataelearning_u5 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u5);

            CreateDs_Clone();
            Ds = (DataSet)ViewState["vsclone"];
            ic = 0;
            if (dataelearning_u5.trainingLoolup_action != null)
            {
                foreach (var item in dataelearning_u5.trainingLoolup_action)
                {
                    dr = Ds.Tables["dsclone"].NewRow();
                    dr["id"] = item.zId_G.ToString();
                    dr["zId"] = item.zId.ToString();
                    dr["zName"] = item.zName;
                    Ds.Tables["dsclone"].Rows.Add(dr);
                    ic++;
                }
            }

            ViewState["vsclone"] = Ds;
            _func_dmu.zSetGridData(GvM0_EmpTarget, Ds.Tables["dsclone"]);


            setGvEmpTarget_searchList();


            data_elearning dataelearning_u1 = new data_elearning();
            course obj_u1 = new course();
            dataelearning_u1.el_course_action = new course[1];
            obj_u1.u0_course_idx_ref = id;
            obj_u1.operation_status_id = "U1";
            dataelearning_u1.el_course_action[0] = obj_u1;
            dataelearning_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u1);

            CreateDs_el_u1_course();
            DataSet dsu1 = (DataSet)ViewState["vsel_u1_course"];
            DataRow dru1;
            ic = 0;
            int icount = 0;
            if (dataelearning_u1.el_course_action != null)
            {
                foreach (var item in dataelearning_u1.el_course_action)
                {

                    dru1 = dsu1.Tables["dsel_u1_course"].NewRow();
                    dru1["id"] = "1";
                    dru1["org_idx_ref"] = item.org_idx_ref.ToString();
                    dru1["org_name_th"] = item.org_name_th;
                    dru1["RDeptID_ref"] = item.RDeptID_ref.ToString();
                    dru1["dept_name_th"] = item.dept_name_th;
                    dru1["RSecID_ref"] = item.RSecID_ref.ToString();
                    dru1["sec_name_th"] = item.sec_name_th;
                    dru1["TIDX_ref"] = item.TIDX_ref.ToString();
                    dru1["target_name"] = item.target_name;
                    dsu1.Tables["dsel_u1_course"].Rows.Add(dru1);

                    icount++;

                    ic++;
                }
            }

            ViewState["vsel_u1_course"] = dsu1;
            _func_dmu.zSetGridData(GvDeptTraningList, dsu1.Tables["dsel_u1_course"]);

            //แบบสดสอบ
            data_elearning dataelearning_u2 = new data_elearning();
            course obj_u2 = new course();
            dataelearning_u2.el_course_action = new course[1];
            obj_u2.u0_course_idx_ref = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u2.operation_status_id = "U2";
            dataelearning_u2.el_course_action[0] = obj_u2;
            dataelearning_u2 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u2);
            // litDebug.Text = _func_dmu.zJson(_urlGetel_u_course, dataelearning_u2);
            CreateDs_dstest();
            DataSet Dsu2 = (DataSet)ViewState["vsel_u2_course"];
            DataRow dru2;
            ic = 0;
            if (dataelearning_u2.el_course_action != null)
            {
                foreach (var item in dataelearning_u2.el_course_action)
                {
                    dru2 = Dsu2.Tables["dsel_u2_course"].NewRow();
                    dru2["docno_bin"] = "";
                    dru2["course_item"] = item.course_item.ToString();
                    dru2["course_proposition"] = item.course_proposition;
                    dru2["course_propos_a"] = item.course_propos_a;
                    dru2["course_propos_b"] = item.course_propos_b;
                    dru2["course_propos_c"] = item.course_propos_c;
                    dru2["course_propos_d"] = item.course_propos_d;
                    dru2["course_propos_answer"] = item.course_propos_answer;
                    dru2["course_proposition_img"] = item.course_proposition_img;
                    dru2["course_propos_a_img"] = item.course_propos_a_img;
                    dru2["course_propos_b_img"] = item.course_propos_b_img;
                    dru2["course_propos_c_img"] = item.course_propos_c_img;
                    dru2["course_propos_d_img"] = item.course_propos_d_img;
                    Dsu2.Tables["dsel_u2_course"].Rows.Add(dru2);
                    ic++;
                }
            }

            ViewState["vsel_u2_course"] = Dsu2;
            _func_dmu.zSetGridData(GvTest, Dsu2.Tables["dsel_u2_course"]);

            //แบบประเมิน

            data_elearning dataelearning_u3 = new data_elearning();
            course obj_u3 = new course();
            dataelearning_u3.el_course_action = new course[1];
            obj_u3.u0_course_idx_ref = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u3.operation_status_id = "U3";
            dataelearning_u3.el_course_action[0] = obj_u3;
            dataelearning_u3 = _func_dmu.zCallServiceNetwork(_urlGetel_u_course, dataelearning_u3);

            CreateDs_dsevaluation();
            DataSet Dsu3 = (DataSet)ViewState["vsEvaluation"];
            DataRow dru3;
            if (dataelearning_u3.el_course_action != null)
            {

                foreach (var item in dataelearning_u3.el_course_action)
                {
                    dru3 = Dsu3.Tables["dsEvaluation"].NewRow();
                    dru3["id"] = "1";
                    dru3["evaluation_group_idx"] = item.m0_evaluation_group_idx_ref.ToString();
                    dru3["evaluation_group_name"] = item.evaluation_group_name_th;
                    dru3["evaluation_f_idx"] = item.m0_evaluation_f_idx_ref.ToString();
                    dru3["evaluation_f_name"] = item.course_evaluation_f_name_th;
                    Dsu3.Tables["dsEvaluation"].Rows.Add(dru3);
                }

            }
            ViewState["vsEvaluation"] = Dsu3;
            _func_dmu.zSetGridData(GvEvaluation, Dsu3.Tables["dsEvaluation"]);

        }

    }

    private Boolean zSaveUpdate(int id)
    {
        Boolean _Boolean = false;
        // U0
        TextBox txtcourse_no = (TextBox)fv_Update.FindControl("txtcourse_no");
        DropDownList ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
        DropDownList ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");
        TextBox txtcourse_name = (TextBox)fv_Update.FindControl("txtcourse_name");
        TextBox txtcourse_date = (TextBox)fv_Update.FindControl("txtcourse_date");
        TextBox txtcourse_remark = (TextBox)fv_Update.FindControl("txtcourse_remark");
        DropDownList ddllevel_code = (DropDownList)fv_Update.FindControl("ddllevel_code");
        TextBox txtcourse_score = (TextBox)fv_Update.FindControl("txtcourse_score");
        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        DropDownList ddlcourse_priority = (DropDownList)fv_Update.FindControl("ddlcourse_priority");
        DropDownList ddlcourse_status = (DropDownList)fv_Update.FindControl("ddlcourse_status");
        GridView GvCourse = (GridView)fv_Update.FindControl("GvCourse");
        GridView GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        _ddlcourse_assessment = (DropDownList)fv_Update.FindControl("ddlcourse_assessment");
        TextBox txtscore_through_per = (TextBox)fv_Update.FindControl("txtscore_through_per");

        int idx = 0, icourse_score = 0, u0_course_idx_ref = 0;
        u0_course_idx_ref = id;
        course obj_course = new course();
        _data_elearning.el_course_action = new course[1];
        obj_course.u0_course_idx_ref = id;
        obj_course.course_date = _func_dmu.zDateToDB(txtcourse_date.Text);
        obj_course.m0_training_group_idx_ref = int.Parse(ddlm0_training_group_idx_ref.SelectedValue);
        obj_course.m0_training_branch_idx_ref = int.Parse(ddm0_training_branch_idx.SelectedValue);
        obj_course.course_name = txtcourse_name.Text;
        obj_course.course_remark = txtcourse_remark.Text;
        obj_course.level_code = int.Parse(ddllevel_code.SelectedValue);
        obj_course.course_priority = int.Parse(ddlcourse_priority.SelectedValue);
        obj_course.course_status = int.Parse(ddlcourse_status.SelectedValue);
        obj_course.course_assessment = _func_dmu.zStringToInt(_ddlcourse_assessment.SelectedValue);
        obj_course.score_through_per = _func_dmu.zStringToDecimal(txtscore_through_per.Text);

        if (txtcourse_score.Text != "")
        {
            icourse_score = int.Parse(txtcourse_score.Text);
        }
        obj_course.course_score = icourse_score;
        obj_course.course_updated_by = emp_idx;
        obj_course.course_type_etraining = _func_dmu.zBooleanToInt(cbcourse_type_etraining.Checked);
        obj_course.course_type_elearning = _func_dmu.zBooleanToInt(cbcourse_type_elearning.Checked);
        obj_course.course_plan_status = "I";

        //node
        obj_course.approve_status = 0;
        obj_course.u0_idx = 5;
        obj_course.node_idx = 9;
        obj_course.actor_idx = 1;
        obj_course.app_flag = 0;
        obj_course.app_user = 0;

        obj_course.operation_status_id = "U0";
        _data_elearning.el_course_action[0] = obj_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_course, _data_elearning);

        if (u0_course_idx_ref > 0)
        {

            zSaveDetail(u0_course_idx_ref, txtcourse_no.Text, "E");

        }
        _Boolean = true;
        return _Boolean;

    }
    public Boolean checkErrorUpdate()
    {
        Boolean _Boolean = false;

        CheckBox cbcourse_type_etraining = (CheckBox)fv_Update.FindControl("cbcourse_type_etraining");
        CheckBox cbcourse_type_elearning = (CheckBox)fv_Update.FindControl("cbcourse_type_elearning");
        TextBox txtscore_through_per = (TextBox)fv_Update.FindControl("txtscore_through_per");
        if ((cbcourse_type_etraining.Checked == false) &&
            (cbcourse_type_elearning.Checked == false))
        {
            _Boolean = true;
            showAlert("กรุณาเลือกประเภท ClassRoom / E-Learning");
        }
        else if ((_func_dmu.zStringToDecimal(txtscore_through_per.Text) < 0) ||
            (_func_dmu.zStringToDecimal(txtscore_through_per.Text) > 100)
            )
        {
            _Boolean = true;
            showAlert("กรุณากรอกคะแนนผ่าน % ให้ถูกต้อง");
        }
        return _Boolean;
    }

    private void CreateDs_el_u5_course()
    {
        string sDs = "dsel_u5_course";
        string sVs = "vsel_u5_course";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("u5_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("TIDX_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("course_updated_by", typeof(String));
        ViewState[sVs] = ds;

    }
    private void setGvEmpTarget_search()
    {
        GridView GvM0_EmpTarget;
        if ((Hddfld_course_no.Value == "") || (Hddfld_course_no.Value == null))
        {
            GvM0_EmpTarget = (GridView)fvCRUD.FindControl("GvM0_EmpTarget");
        }
        else
        {
            GvM0_EmpTarget = (GridView)fv_Update.FindControl("GvM0_EmpTarget");
        }
        int ic = 0, id = 0;
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        foreach (var item in GvM0_EmpTarget.Rows)
        {
            CheckBox GvM0_EmpTarget_cb_zId = (CheckBox)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_cb_zId");
            if (GvM0_EmpTarget_cb_zId.Checked == true)
            {
                id = 1;
            }
            else
            {
                id = 0;
            }
            dr = Ds.Tables["dsclone"].NewRow();
            dr["id"] = id.ToString();
            Label GvM0_EmpTarget_zId = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zId");
            Label GvM0_EmpTarget_zName = (Label)GvM0_EmpTarget.Rows[ic].FindControl("GvM0_EmpTarget_zName");
            dr["zId"] = GvM0_EmpTarget_zId.Text;
            dr["zName"] = GvM0_EmpTarget_zName.Text;
            Ds.Tables["dsclone"].Rows.Add(dr);
            ic++;
        }
        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvM0_EmpTarget_search, Ds.Tables["dsclone"]);
    }
    private void setGvEmpTarget_searchList()
    {
        data_elearning dataelearning_u4 = new data_elearning();
        trainingLoolup obj_u4 = new trainingLoolup();
        dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
        obj_u4.idx = 0;
        obj_u4.operation_status_id = "EMP-TARGET";
        dataelearning_u4.trainingLoolup_action[0] = obj_u4;
        dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        int ic = 0;
        if (dataelearning_u4.trainingLoolup_action != null)
        {
            foreach (var item in dataelearning_u4.trainingLoolup_action)
            {
                dr = Ds.Tables["dsclone"].NewRow();
                dr["id"] = "0";
                dr["zId"] = item.zId.ToString();
                dr["zName"] = item.zName;
                Ds.Tables["dsclone"].Rows.Add(dr);
                ic++;
            }
        }

        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvM0_EmpTarget_search, Ds.Tables["dsclone"]);
    }
    private void searchCOURSEList()
    {
        GridView GvCourse;
        TextBox txtsearch_GvCourse;
        DropDownList ddlm0_training_group_idx_ref;
        DropDownList ddm0_training_branch_idx;

        data_elearning dataelearning_u4 = new data_elearning();
        trainingLoolup obj_u4 = new trainingLoolup();
        dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
        if ((Hddfld_course_no.Value == "") || (Hddfld_course_no.Value == null))
        {
            GvCourse = (GridView)fvCRUD.FindControl("GvCourse");
            txtsearch_GvCourse = (TextBox)fvCRUD.FindControl("txtsearch_GvCourse");
            ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");

            obj_u4.operation_status_id = "TRN-COURSE-LIST";
            obj_u4.fil_Search = txtsearch_GvCourse.Text;
            obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
            obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);
        }
        else
        {
            GvCourse = (GridView)fv_Update.FindControl("GvCourse");
            txtsearch_GvCourse = (TextBox)fv_Update.FindControl("txtsearch_GvCourse");
            ddlm0_training_group_idx_ref = (DropDownList)fv_Update.FindControl("ddlm0_training_group_idx_ref");
            ddm0_training_branch_idx = (DropDownList)fv_Update.FindControl("ddm0_training_branch_idx");

            obj_u4.operation_status_id = "TRN-COURSE-WHEREID";
            obj_u4.idx = int.Parse(Hddfld_u0_course_idx.Value);
            obj_u4.fil_Search = txtsearch_GvCourse.Text;
            obj_u4.zId_G = _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue);
            obj_u4.idx1 = _func_dmu.zStringToInt(ddm0_training_branch_idx.SelectedValue);

        }

        dataelearning_u4.trainingLoolup_action[0] = obj_u4;
        dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
        CreateDs_Clone();
        DataSet Ds = (DataSet)ViewState["vsclone"];
        DataRow dr;
        int ic = 0;
        if (dataelearning_u4.trainingLoolup_action != null)
        {
            foreach (var item in dataelearning_u4.trainingLoolup_action)
            {
                dr = Ds.Tables["dsclone"].NewRow();
                dr["id"] = item.zId_G.ToString();
                dr["zId"] = item.zId.ToString();
                dr["zName"] = item.zName;
                Ds.Tables["dsclone"].Rows.Add(dr);
                ic++;
            }
        }

        ViewState["vsclone"] = Ds;
        _func_dmu.zSetGridData(GvCourse, Ds.Tables["dsclone"]);
    }
    private int zUploadImagesBin(FileUpload _FileUpload, string sDocNo, int id, int item)
    {
        string _itemExtension = "";
        string _itemNameNew = "";
        string _itemFilePath = "";
        string newFilePath = "";
        string OldFilePath = "";
        int _int = 0;
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

        OldFilePath = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;

        //if (_FileUpload.HasFile)
        //{
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {
            _PathFile = _PathFile + _Folder_courseBin + "/" + sDocNo + "/" + id.ToString() + "/";
            newFilePath = _PathFile;
           
            if ((id > 0) && (item > 0) && (sDocNo != ""))
            {
                
                _itemExtension = ".png";

                //if (_FileUpload.HasFile)
                //{
                    _itemNameNew = item.ToString() + _itemExtension.ToLower();
                //}

                //newFilePath = _PathFile +
                //                    _itemNameNew;

                OldFilePath = OldFilePath + "/" + _itemNameNew;
                newFilePath = newFilePath + "/" + _itemNameNew;
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                try
                {
                    File.Move(Server.MapPath(OldFilePath), Server.MapPath(newFilePath));
                    _int = 1;
                }
                catch { }


                //if (_FileUpload.HasFile)
                //{
                //    try
                //    {
                //        Directory.Delete(Server.MapPath(newFilePath));
                //    }
                //    catch { }
                //    _itemFilePath = Server.MapPath(newFilePath);
                //    _FileUpload.SaveAs(_itemFilePath);
                //    _int = 1;
                //}

            }
        }
        //}


        return _int;

    }

    private void zUploadImagesElearning(string sDocNoBin, string sDocNoEl, int id)
    {

        if ((sDocNoBin != "") && (sDocNoEl != "") && (id > 0))
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            string _itemExtension = "";
            string _itemNameNew = "";
            string _itemFilePath = "";
            string OldFilePath = "";
            string newFilePath = "";
            _itemExtension = ".png";

            OldFilePath = _PathFile + _Folder_courseBin + "/" + sDocNoBin + "/" + id.ToString() + "/";
            //OldFilePath = OldFilePath + _itemNameNew;

            newFilePath = _PathFile + _Folder_coursetestimg + "/" + sDocNoEl + "/" + id.ToString() + "/";
            //newFilePath = newFilePath + _itemNameNew;
            try
            {
                Directory.CreateDirectory(Server.MapPath(newFilePath));
            }
            catch { }
            string _folder = OldFilePath;
            for (int i = 1; i <= 5; i++)
            {
                _itemNameNew = i.ToString() + _itemExtension.ToLower();
                string _OldFilePath, _newFilePath;
                _OldFilePath = OldFilePath + _itemNameNew;
                _newFilePath = newFilePath + _itemNameNew;


                try
                {
                    Directory.Delete(Server.MapPath(_newFilePath));
                }
                catch { }

                try
                {
                    File.Move(Server.MapPath(_OldFilePath), Server.MapPath(_newFilePath));
                }
                catch { }
            }
            try
            {
                Directory.Delete(Server.MapPath(_folder));
            }
            catch { }
        }
    }

    private void zDelImagesBin(string sDocNoBin)
    {
        if (sDocNoBin != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            string OldFilePath = "";
            OldFilePath = _PathFile + _Folder_courseBin + "/" + sDocNoBin + "/";
            try
            {
                Directory.Delete(Server.MapPath(OldFilePath));
            }
            catch { }
            try
            {
                Directory.Delete(OldFilePath);
            }
            catch { }
        }

    }

    private void ExportGridToExcel(GridView _gv, string sfilename = "ExportToExcel", string ATitle = "Export To Excel")
    {
        if (_gv.Rows.Count == 0)
        {
            return;
        }
        //_gv.AllowPaging = false;
        //_gv.DataBind();
        var totalCols = _gv.Rows[0].Cells.Count;
        var totalRows = _gv.Rows.Count;
        var headerRow = _gv.HeaderRow;

        DataTable tableMaterialLog = new DataTable();

        for (var i = 1; i <= totalCols; i++)
        {
            tableMaterialLog.Columns.Add(headerRow.Cells[i - 1].Text, typeof(String));
        }
        for (var j = 1; j <= totalRows; j++)
        {
            DataRow addMaterialLogRow = tableMaterialLog.NewRow();
            for (var i = 1; i <= totalCols; i++)
            {
                string sdata = ((Label)_gv.Rows[j - 1].Cells[i - 1].Controls[1]).Text;
                addMaterialLogRow[i - 1] = sdata;
            }
            tableMaterialLog.Rows.Add(addMaterialLogRow);
        }
        GridView gv = new GridView();
        gv.DataSource = tableMaterialLog;
        gv.DataBind();
        WriteExcelWithNPOI(tableMaterialLog, "xls", sfilename, ATitle);

    }
    public void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");

        IRow row1 = sheet1.CreateRow(0);

        ICell cellTitle = row1.CreateCell(0);
        String columnNameTitle = ATitle;
        cellTitle.SetCellValue(columnNameTitle);


        row1 = sheet1.CreateRow(1);
        cellTitle = row1.CreateCell(0);
        columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        cellTitle.SetCellValue(columnNameTitle);

        row1 = sheet1.CreateRow(3);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 4);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    public void imageProcessRequest(HttpPostedFile _HttpPostedFile, int item)
    {

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        //if ((lbfolderImage.Text == null) || (lbfolderImage.Text == ""))
        //{
        // Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");
        //}
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {

            _PathFile = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;


            if (_HttpPostedFile != null && _HttpPostedFile.ContentLength > 0)
            {
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                string _itemExtension = ".png";
                string _itemNameNew = item.ToString() + _itemExtension.ToLower();
                string _itemFilePath = "";
                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                _itemFilePath = Server.MapPath(newFilePath);
                _HttpPostedFile.SaveAs(_itemFilePath);
                if (item == 1)
                {
                    // _img_proposition = (Image)fvCRUD.FindControl("img_proposition");
                    //img_proposition.ImageUrl = getImgUrlFDBin(newFilePath);
                    //  ImageButton1.ImageUrl = getImgUrlFDBin(newFilePath);
                }
            }
        }

    }
    public string getImgUrlFDBin(string AImage = "")
    {
        string sPathImage = "";
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = AImage;
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }


    public string imageupload_del(string name)
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        string _itemNameNew = "";
        if (name != "")
        {
            _itemNameNew = name + ".png";
        }
        else
        {
            name = "";
        }
        if (name != "")
        {
            //  Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");
            //}
            if (Directory.Exists(Server.MapPath(_PathFile)))
            {

                _PathFile = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;

                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                try
                {
                    File.Delete(Server.MapPath(newFilePath));
                }
                catch { }
            }
        }
        return name;
    }

}