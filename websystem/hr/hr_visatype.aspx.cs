﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_typevisa : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetSelectVisatype = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectVisatype"];
    static string _urlGetInsertVisatype = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertVisatype"];
    static string _urlGetUpdateVisatype = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateVisatype"];
    static string _urlGetDeleteVisatype = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteVisatype"];


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _dataEmployee.BoxTypevisa_list = new Typevisa_List[1];
        Typevisa_List dtemployee = new Typevisa_List();

        dtemployee.name_visa_th = txtNameTH.Text;
        dtemployee.name_visa_en = txtNameEN.Text;
        dtemployee.YearVisa = int.Parse(txtYearVisa.Text);
        dtemployee.VTStatus = int.Parse(ddl_status.SelectedValue);
        dtemployee.CEmapIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmployee.BoxTypevisa_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetInsertVisatype, _dataEmployee);
    }

    protected void SelectMasterList()
    {
        _dataEmployee.BoxTypevisa_list = new Typevisa_List[1];
        Typevisa_List dtemployee = new Typevisa_List();

        _dataEmployee.BoxTypevisa_list[0] = dtemployee;

        _dataEmployee = callService(_urlGetSelectVisatype, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _dataEmployee.BoxTypevisa_list);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.BoxTypevisa_list = new Typevisa_List[1];
        Typevisa_List dtemployee = new Typevisa_List();

        dtemployee.VisaIDX = int.Parse(ViewState["VisaIDX_update"].ToString());
        dtemployee.name_visa_th = ViewState["name_th_update"].ToString();
        dtemployee.name_visa_en = ViewState["name_en_update"].ToString();
        dtemployee.YearVisa = int.Parse(ViewState["YearVisa_update"].ToString());
        dtemployee.VTStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.BoxTypevisa_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetUpdateVisatype, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataEmployee.BoxTypevisa_list = new Typevisa_List[1];
        Typevisa_List dtemployee = new Typevisa_List();

        dtemployee.VisaIDX = int.Parse(ViewState["VisaIDX_delete"].ToString());

        _dataEmployee.BoxTypevisa_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetDeleteVisatype, _dataEmployee);
    }



    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {

                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int VisaIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtNameTH_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtNameTH_update");
                var txtNameEN_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtNameEN_update");
                var txtYearVisa_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtYearVisa");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["VisaIDX_update"] = VisaIDX;
                ViewState["name_th_update"] = txtNameTH_update.Text;
                ViewState["name_en_update"] = txtNameEN_update.Text;
                ViewState["YearVisa_update"] = txtYearVisa_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":

                int CostIDX = int.Parse(cmdArg);
                ViewState["VisaIDX_delete"] = CostIDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion

}