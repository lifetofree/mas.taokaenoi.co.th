﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_result.aspx.cs" Inherits="websystem_qa_qa_lab_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreateResult" runat="server" CommandName="cmdCreateResult" OnCommand="navCommand" CommandArgument="docCreateResult"> บันทึกผลตรวจ</asp:LinkButton>
                        </li>

                        <%-- <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> Admin สร้างรายการ</asp:LinkButton>
                        </li>--%>
                    </ul>

                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Create Result-->
        <asp:View ID="docCreateResult" runat="server">
            <div class="col-md-12">

                <asp:GridView ID="gvProducts" AutoGenerateColumns="False"
                    CssClass="GridViewStyle" GridLines="None" SelectedIndex="0"
                    runat="server" DataKeyNames="ProductID"
                    OnRowCommand="onRowCommand">
                    <RowStyle CssClass="RowStyle" />
                    <FooterStyle CssClass="RowStyle" />
                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                    <Columns>
                        <asp:BoundField DataField="ProductID" HeaderText="Product ID" ReadOnly="true" />
                        <asp:TemplateField HeaderText="Product Number">
                            <ItemTemplate><%#Eval("ProductNumber")%></ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewProductNumber" runat="Server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Product Name">
                            <ItemTemplate><%#Eval("Name")%></ItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewProductName" runat="Server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Price">
                            <ItemTemplate><%#Eval("ListPrice","{0:C}")%></ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewListPrice" runat="Server"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnSelect" runat="server" CommandName="Select" Text="Select" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="btnInsert" runat="Server" Text="Insert" CommandName="Insert" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                <asp:LinkButton ID="btnAdd" runat="server" Text="Add" OnClick="AddNewRecord" />


                <asp:GridView ID="GvSaveResultLab"
                    runat="server"
                    HeaderStyle-CssClass="info"
                    AllowPaging="false" AllowSorting="True"
                    BackColor="White"
                    BorderColor="#DEDFDE"
                    BorderStyle="None" BorderWidth="1px"
                    CssClass="table table-striped table-bordered"
                    Width="100%" CellPadding="4" ForeColor="Black" GridLines="Vertical"
                    OnRowDataBound="gvRowDataBound"
                    ShowFooter="true"
                    ShowHeaderWhenEmpty="True">

                    <EmptyDataTemplate>
                        <div style="text-align: center">-- Data Cannot Be Found --</div>
                        
                       
                    </EmptyDataTemplate>


                </asp:GridView>

                <asp:GridView ID="gvAddResultLabList"
                    runat="server"
                    CssClass="table table-bordered table-striped table-responsive col-md-12"
                    HeaderStyle-CssClass="info"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="gvRowDataBound"
                    AutoGenerateColumns="false"
                    AllowPaging="false"
                    ShowFooter="true"
                    ShowHeaderWhenEmpty="True">
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <%# (Container.DataItemIndex + 1) %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่รับตัวอย่าง" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_drReceiveDateText" runat="server" Text='<%# Eval("drReceiveDateText")%>'></asp:Label>
                            </ItemTemplate>

                            <FooterTemplate>
                                <asp:TextBox ID="txt_drReceiveDateText" runat="server" CssClass="form-control" placeholder="วันที่รับตัวอย่าง ..."></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่ทดสอบ" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_drTestDateText" runat="server" Text='<%# Eval("drTestDateText")%>'></asp:Label>
                            </ItemTemplate>

                            <FooterTemplate>
                                <asp:TextBox ID="txt_drTestDateText" runat="server" CssClass="form-control" placeholder="วันที่ทดสอบ ..."></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                            </ItemTemplate>

                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


            </div>
        </asp:View>
        <!--View Create Result-->

    </asp:MultiView>
    <!--multiview-->
</asp:Content>



