﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_EPN_epn_certificate : System.Web.UI.Page
{

    #region service
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    // ชื่อกล่อง  ตัวแปร = new ชื่อกล่อง
    data_employee _dtEmployee = new data_employee();
 

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    // เรียก service มาใช้ จาก web config
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    int _emp_idx = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            select_empIdx_present();
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

        }
        linkBtnTrigger(_divMenuBtnToDivAdd);
    }

    #region selectEmployee
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["EmpType"] = _dtEmployee.employee_list[0].emp_type_idx;

    }

    #endregion

    #region setDefault
    protected void SetDefault(int page)
    {
        switch (page)
        {
            case 1:
              //  MvMaster.SetActiveView(docCreate);


                break;


        }
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        int type_idx = 0;



        switch (cmdName)
        {

            case "btnCmn":
                //btnshow.Visible = false;
              //  Panel_Add.Visible = true;

                break;

            case "btnCmnA":
               // Insert_MasterSystem();
               // Panel_Add.Visible = false;
               // btnshow.Visible = true;
               // Select_MasterSystem();
              //  GvMaster.Visible = true;
                break;

            case "CmdDel":
                type_idx = int.Parse(cmdArg);
                ViewState["type_idx"] = type_idx;
               // Panel_Add.Visible = false;
               // btnshow.Visible = true;
              //  Delete_MasterSystem();
               // Select_MasterSystem();
                break;

            case "_divMenuBtnToDivAdd":

                SetDefault(1);
                break;

        }

    }


    #endregion

    #region employee  service
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        chkName.Items.Clear();
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion


    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            DropDownList ddllocation_problem = ((DropDownList)FvinsertProblem.FindControl("ddllocation_problem"));
            DropDownList ddlbuilding_problem = ((DropDownList)FvinsertProblem.FindControl("ddlbuilding_problem"));
            DropDownList ddlproduction_problem = ((DropDownList)FvinsertProblem.FindControl("ddlproduction_problem"));
            DropDownList ddltype_problem = ((DropDownList)FvinsertProblem.FindControl("ddltype_problem"));
            DropDownList ddltypeproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem"));

            DropDownList ddlgroupproduct_problem = ((DropDownList)FvinsertProblem.FindControl("ddlgroupproduct_problem"));
            DropDownList ddltypeproduct_problem_m1 = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m1"));
            DropDownList ddltypeproduct_problem_m2 = ((DropDownList)FvinsertProblem.FindControl("ddltypeproduct_problem_m2"));

            DropDownList ddlmat_no = ((DropDownList)FvinsertProblem.FindControl("ddlmat_no"));
            DropDownList ddl_sup = ((DropDownList)FvinsertProblem.FindControl("ddl_sup"));
            Label lblcount_problem = ((Label)FvinsertProblem.FindControl("lblcount_problem"));
            Label lblcount_problem_all = ((Label)FvinsertProblem.FindControl("lblcount_problem_all"));
            TextBox txtmat_name = ((TextBox)FvinsertProblem.FindControl("txtmat_name"));
            DropDownList ddlunit_get = ((DropDownList)FvinsertProblem.FindControl("ddlunit_get"));
            DropDownList ddlunit_problem = ((DropDownList)FvinsertProblem.FindControl("ddlunit_problem"));
            DropDownList ddlunit_random = ((DropDownList)FvinsertProblem.FindControl("ddlunit_random"));


            DropDownList ddllocation_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddllocation_problemedit"));
            DropDownList ddlbuilding_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlbuilding_problemedit"));
            DropDownList ddlproduction_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlproduction_problemedit"));
            DropDownList ddlmat_noedit = ((DropDownList)FvdetailProblem.FindControl("ddlmat_noedit"));
            TextBox txtmat_name_edit = ((TextBox)FvdetailProblem.FindControl("txtmat_name_edit"));
            DropDownList ddlunit_getedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_getedit"));
            DropDownList ddlunit_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit"));
            DropDownList ddlunit_randomedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit"));
            DropDownList ddltype_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltype_problemedit"));

            DropDownList ddlgroupproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlgroupproduct_problemedit"));
            DropDownList ddltypeproduct_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit"));
            DropDownList ddltypeproduct_problemedit_m1 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m1"));
            DropDownList ddltypeproduct_problemedit_m2 = ((DropDownList)FvdetailProblem.FindControl("ddltypeproduct_problemedit_m2"));

            DropDownList ddllocation_exchange = ((DropDownList)FvQA_Comment.FindControl("ddllocation_exchange"));
            Panel panel_accept = ((Panel)FvQA_Comment.FindControl("panel_accept"));
            Panel panel_change = ((Panel)FvQA_Comment.FindControl("panel_change"));
            Control div_approveqa = ((Control)FvQA_Comment.FindControl("div_approveqa"));
            Control div_reason_exchange = ((Control)FvQA_Comment.FindControl("div_reason_exchange"));
            TextBox txtqtyget = ((TextBox)FvinsertProblem.FindControl("txtqtyget"));
            TextBox txtqty_problem = ((TextBox)FvinsertProblem.FindControl("txtqty_problem"));
            TextBox txtqty_random = ((TextBox)FvinsertProblem.FindControl("txtqty_random"));

            Label lblqty_get = ((Label)FvdetailProblem.FindControl("lblqty_get"));
            TextBox txtqtyrollback = ((TextBox)FvQA_Comment.FindControl("txtqtyrollback"));
            Label lblunit_problem_show = ((Label)FvdetailProblem.FindControl("lblunit_problem_show"));

            switch (ddlName.ID)
            {

                case "ddllocation":
                    selectbuilding(ddlbuilding, int.Parse(ddllocation.SelectedValue));

                    break;

                case "ddllocation_problem":
                    selectbuilding(ddlbuilding_problem, int.Parse(ddllocation_problem.SelectedValue));

                    break;


                case "ddlbuilding_problem":
                    selectplace(ddlproduction_problem, int.Parse(ddllocation_problem.SelectedValue), int.Parse(ddlbuilding_problem.SelectedValue));

                    break;

                case "ddlmat_no":
                    selectmatname(txtmat_name, int.Parse(ddlmat_no.SelectedValue));
                    break;

                case "ddlgroupproduct_problem":
                    selecttypeproduct(ddltypeproduct_problem, int.Parse(ddlgroupproduct_problem.SelectedValue));
                    break;

                case "ddltypeproduct_problem":
                    selecttypeproduct_m1(ddltypeproduct_problem_m1, int.Parse(ddltypeproduct_problem.SelectedValue));
                    selecttypeproblem(ddltype_problem, int.Parse(ddltypeproduct_problem.SelectedValue));
                    break;

                case "ddltypeproduct_problem_m1":
                    selecttypeproduct_m2(ddltypeproduct_problem_m2, int.Parse(ddltypeproduct_problem_m1.SelectedValue));
                    break;

                case "ddltype_problem":
                    if (ddltype_problem.SelectedValue == "0")
                    {
                        lblcount_problem.Text = "0";
                        lblcount_problem_all.Text = "0";
                    }
                    else
                    {
                        selectcount_problemwithsupplier(lblcount_problem, lblcount_problem_all, int.Parse(ddltype_problem.SelectedValue), int.Parse(ddl_sup.SelectedValue), int.Parse(ddlmat_no.SelectedValue));
                    }
                    break;

                case "ddlunit_get":
                    ddlunit_problem.SelectedValue = ddlunit_get.SelectedValue;
                    ddlunit_random.SelectedValue = ddlunit_get.SelectedValue;
                    break;

                case "ddllocation_problemedit":
                    selectbuilding(ddlbuilding_problemedit, int.Parse(ddllocation_problemedit.SelectedValue));

                    break;

                case "ddlbuilding_problemedit":
                    selectplace(ddlproduction_problemedit, int.Parse(ddllocation_problemedit.SelectedValue), int.Parse(ddlbuilding_problemedit.SelectedValue));

                    break;

                case "ddlmat_noedit":
                    selectmatname(txtmat_name_edit, int.Parse(ddlmat_noedit.SelectedValue));
                    break;

                case "ddlunit_getedit":
                    ddlunit_problemedit.SelectedValue = ddlunit_getedit.SelectedValue;
                    ddlunit_randomedit.SelectedValue = ddlunit_getedit.SelectedValue;
                    break;

                case "ddlgroupproduct_problemedit":
                    selecttypeproduct(ddltypeproduct_problemedit, int.Parse(ddlgroupproduct_problemedit.SelectedValue));
                    break;

                case "ddltypeproduct_problemedit":
                    selecttypeproduct_m1(ddltypeproduct_problemedit_m1, int.Parse(ddltypeproduct_problemedit.SelectedValue));
                    selecttypeproblem(ddltype_problemedit, int.Parse(ddltypeproduct_problemedit.SelectedValue));
                    break;

                case "ddltypeproduct_problemedit_m1":
                    selecttypeproduct_m2(ddltypeproduct_problemedit_m2, int.Parse(ddltypeproduct_problemedit_m1.SelectedValue));
                    break;

                case "ddl_approve_qa":
                    if (ddl_approve_qa.SelectedValue == "7")
                    {
                        // div_checklist.Visible = true;
                        div_approveqa.Visible = false;
                        panel_accept.Visible = true;
                    }
                    else if (ddl_approve_qa.SelectedValue == "8")
                    {
                        //  div_checklist.Visible = false;
                        panel_accept.Visible = false;
                        // panel_change.Visible = false;
                        div_approveqa.Visible = true;
                        //  rdochange.ClearSelection();
                    }
                    break;

                case "ddlSearchDate":

                    if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                    {
                        AddEndDate.Enabled = true;
                    }
                    else
                    {
                        AddEndDate.Enabled = false;
                        AddEndDate.Text = string.Empty;
                    }

                    break;

                case "ddlrdeptidx_search":
                    getSectionList(ddlrsecidx_search, 1, int.Parse(ddlrdeptidx_search.SelectedValue));
                    break;

                case "ddlgroupproduct_search":
                    selecttypeproduct(ddltypeproduct_search, int.Parse(ddlgroupproduct_search.SelectedValue));
                    break;

                case "ddltypeproduct_search":
                    selecttypeproduct_m1(ddltypeproduct_search_m1, int.Parse(ddltypeproduct_search.SelectedValue));
                    selecttypeproblem(ddlproblem_search, int.Parse(ddltypeproduct_search.SelectedValue));
                    break;
                case "ddltypeproduct_search_m1":
                    selecttypeproduct_m2(ddltypeproduct_search_m2, int.Parse(ddltypeproduct_search_m1.SelectedValue));
                    break;

                case "ddlLocate_search":
                    selectbuilding(ddlBuilding_search, int.Parse(ddlLocate_search.SelectedValue));

                    break;

                case "ddlBuilding_search":
                    selectplace(ddlProduction_name_search, int.Parse(ddlLocate_search.SelectedValue), int.Parse(ddlBuilding_search.SelectedValue));

                    break;

                case "ddltype_addmat":

                    if (ddltype_addmat.SelectedValue == "1")
                    {
                        div_addmanual.Visible = true;
                        div_addimport.Visible = false;
                    }
                    else if (ddltype_addmat.SelectedValue == "2")
                    {
                        div_addmanual.Visible = false;
                        div_addimport.Visible = true;
                    }
                    else
                    {
                        div_addmanual.Visible = false;
                        div_addimport.Visible = false;
                    }

                    break;

                case "ddllocation_exchange":
                    if (ddllocation_exchange.SelectedValue != ViewState["LocRef_Exchange"].ToString())
                    {
                        div_reason_exchange.Visible = true;
                    }
                    else
                    {
                        div_reason_exchange.Visible = false;
                    }
                    break;

                case "ddlactor_search":
                    selectstatus(ddlstatus_search, int.Parse(ddlactor_search.SelectedValue));

                    if (ddlactor_search.SelectedValue != "1")
                    {
                        ddlstatus_search.Items.Insert(1, new ListItem("ดำเนินการ", "1"));
                    }

                    break;

                case "ddlmaster_searchlocation":
                    selectbuilding(ddlmaster_searchbuilding, int.Parse(ddlmaster_searchlocation.SelectedValue));

                    break;

                case "ddlSearchDate_List":

                    if (int.Parse(ddlSearchDate_List.SelectedValue) == 3)
                    {
                        AddEndDate_List.Enabled = true;
                    }
                    else
                    {
                        AddEndDate_List.Enabled = false;
                        AddEndDate_List.Text = string.Empty;
                    }

                    break;

                case "ddltypeproduct_List":
                    selecttypeproduct_m1(ddltypeproduct_list_m1, int.Parse(ddltypeproduct_List.SelectedValue));
                    selecttypeproblem(ddlproblem_List, int.Parse(ddltypeproduct_List.SelectedValue));
                    break;

                case "ddlchoose_addsup":
                    if (ddlchoose_addsup.SelectedValue == "1")
                    {
                        div_addmanualsup.Visible = true;
                        div_addimportsup.Visible = false;
                    }
                    else if (ddlchoose_addsup.SelectedValue == "2")
                    {
                        div_addmanualsup.Visible = false;
                        div_addimportsup.Visible = true;

                    }
                    else
                    {
                        div_addmanualsup.Visible = false;
                        div_addimportsup.Visible = false;
                    }
                    btnshow_supplier.Visible = false;
                    break;

                case "ddlunit_problem":
                    decimal qty_get = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_problem = Convert.ToDecimal(txtqty_problem.Text);

                    if (ddlunit_get.SelectedValue == ddlunit_problem.SelectedValue && qty_get < qty_problem)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        ddlunit_problem.SelectedValue = "0";
                    }
                    break;

                case "ddlunit_random":
                    decimal qty_get1 = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_random = Convert.ToDecimal(txtqty_random.Text);

                    if (ddlunit_get.SelectedValue == ddlunit_random.SelectedValue && qty_get1 < qty_random)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        ddlunit_random.SelectedValue = "0";
                    }
                    break;

                case "ddlunit_rollback":
                    decimal qty_get_edit2 = Convert.ToDecimal(lblqty_get.Text);
                    decimal qty_rollback = Convert.ToDecimal(txtqtyrollback.Text);

                    if (qty_get_edit2 < qty_rollback && lblunit_problem_show.Text == ddlunit_rollback.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนส่งคืนอีกครั้ง!!!');", true);
                        ddlunit_rollback.SelectedValue = "0";
                    }
                    break;


                case "ddlgroupproduct_List":
                    selecttypeproduct(ddltypeproduct_List, int.Parse(ddlgroupproduct_List.SelectedValue));

                    break;

                case "ddltypeproduct_list_m1":
                    selecttypeproduct_m2(ddltypeproduct_list_m2, int.Parse(ddltypeproduct_list_m1.SelectedValue));

                    break;

                case "ddladdgroup_masterm1":
                    selecttypeproduct(ddladdproduct_master, int.Parse(ddladdgroup_masterm1.SelectedValue));

                    break;

                case "ddlsearchgroup_masterm1":
                    selecttypeproduct(ddlsearchproduct_masterm1, int.Parse(ddlsearchgroup_masterm1.SelectedValue));

                    break;

                case "ddlgroupproduct_insertmasterproduct_edit":
                    var ddNameProduct = (DropDownList)sender;
                    var row = (GridViewRow)ddNameProduct.NamingContainer;
                    var ddlgroupproduct_insertmasterproduct_edit = (DropDownList)row.FindControl("ddlgroupproduct_insertmasterproduct_edit");
                    var ddlproduct_insertmasterproductm1_edit = (DropDownList)row.FindControl("ddlproduct_insertmasterproductm1_edit");

                    selecttypeproduct(ddlproduct_insertmasterproductm1_edit, int.Parse(ddlgroupproduct_insertmasterproduct_edit.SelectedValue));
                    break;


                case "ddladdgroup_masterm2":
                    selecttypeproduct(ddladdproduct_master_m2, int.Parse(ddladdgroup_masterm2.SelectedValue));
                    break;

                case "ddladdproduct_master_m2":
                    selecttypeproduct_m1(ddladdproduct_masterm1_m2, int.Parse(ddladdproduct_master_m2.SelectedValue));
                    break;

                case "ddlsearchgroup_masterm2":
                    selecttypeproduct(ddlsearchproduct_masterm2, int.Parse(ddlsearchgroup_masterm2.SelectedValue));
                    break;

                case "ddlsearchproduct_masterm2":
                    selecttypeproduct_m1(ddlsearchproduct_masterm1_m2, int.Parse(ddlsearchproduct_masterm2.SelectedValue));
                    break;

                case "ddlgroupproduct_insertmasterproduct_editm2":
                    var ddNameProduct_groupm2 = (DropDownList)sender;
                    var row_groupm2 = (GridViewRow)ddNameProduct_groupm2.NamingContainer;
                    var ddlgroupproduct_insertmasterproduct_editm2 = (DropDownList)row_groupm2.FindControl("ddlgroupproduct_insertmasterproduct_editm2");
                    var ddlproduct_insertmasterproduct_editm2 = (DropDownList)row_groupm2.FindControl("ddlproduct_insertmasterproduct_editm2");

                    selecttypeproduct(ddlproduct_insertmasterproduct_editm2, int.Parse(ddlgroupproduct_insertmasterproduct_editm2.SelectedValue));

                    break;

                case "ddlproduct_insertmasterproduct_editm2":
                    var ddNameProduct_typem2 = (DropDownList)sender;
                    var row_typem2 = (GridViewRow)ddNameProduct_typem2.NamingContainer;
                    var ddlproduct_insertmasterproduct_editm2_ = (DropDownList)row_typem2.FindControl("ddlproduct_insertmasterproduct_editm2");
                    var ddlproduct_insertmasterproductm1_editm2 = (DropDownList)row_typem2.FindControl("ddlproduct_insertmasterproductm1_editm2");

                    selecttypeproduct_m1(ddlproduct_insertmasterproductm1_editm2, int.Parse(ddlproduct_insertmasterproduct_editm2_.SelectedValue));

                    break;

                case "ddltype_group":
                    selecttypeproduct(ddltype_product, int.Parse(ddltype_group.SelectedValue));
                    break;

                case "ddlmaster_searchgroupproblem":
                    selecttypeproduct(ddlmaster_searchproblem, int.Parse(ddlmaster_searchgroupproblem.SelectedValue));

                    break;

                case "ddlm0_group_tpidx_edit":
                    var ddNameProduct_group_problem = (DropDownList)sender;
                    var row_group_problem = (GridViewRow)ddNameProduct_group_problem.NamingContainer;
                    var ddlm0_group_tpidx_edit = (DropDownList)row_group_problem.FindControl("ddlm0_group_tpidx_edit");
                    var ddlm0_tpidx_edit = (DropDownList)row_group_problem.FindControl("ddlm0_tpidx_edit");

                    selecttypeproduct(ddlm0_tpidx_edit, int.Parse(ddlm0_group_tpidx_edit.SelectedValue));

                    break;

            }
        }
        else if (sender is CheckBox)
        {
            CheckBox chkName = (CheckBox)sender;

            CheckBox chkmat = ((CheckBox)FvinsertProblem.FindControl("chkmat"));
            Control divmat_no = ((Control)FvinsertProblem.FindControl("divmat_no"));
            TextBox txtmat_name = ((TextBox)FvinsertProblem.FindControl("txtmat_name"));
            DropDownList ddlmat_no = ((DropDownList)FvinsertProblem.FindControl("ddlmat_no"));

            CheckBox chkmat_edit = ((CheckBox)FvdetailProblem.FindControl("chkmat_edit"));
            Control divmat_no_edit = ((Control)FvdetailProblem.FindControl("divmat_no_edit"));
            TextBox txtmat_name_edit = ((TextBox)FvdetailProblem.FindControl("txtmat_name_edit"));
            DropDownList ddlmat_noedit = ((DropDownList)FvdetailProblem.FindControl("ddlmat_noedit"));


            switch (chkName.ID)
            {
                case "chkmat":
                    if (chkmat.Checked)
                    {
                        //txt.Text = "1";
                        divmat_no.Visible = true;
                    }
                    else
                    {
                        //txt.Text = "2";
                        divmat_no.Visible = false;
                        txtmat_name.Enabled = true;
                        ddlmat_no.SelectedValue = "0";
                        txtmat_name.Text = String.Empty;
                    }
                    break;

                case "chkmat_edit":
                    if (chkmat_edit.Checked)
                    {
                        //txt.Text = "1";
                        divmat_no_edit.Visible = true;
                    }
                    else
                    {
                        //txt.Text = "2";
                        divmat_no_edit.Visible = false;
                        txtmat_name_edit.Enabled = true;
                        ddlmat_noedit.SelectedValue = "0";
                        txtmat_name_edit.Text = String.Empty;
                    }
                    break;

                case "chkmatrisk_edit":
                    setchk_changed();
                    break;

            }
        }
        else if (sender is TextBox)
        {
            TextBox txtqtyget = ((TextBox)FvinsertProblem.FindControl("txtqtyget"));
            TextBox txtqty_problem = ((TextBox)FvinsertProblem.FindControl("txtqty_problem"));
            TextBox txtqty_random = ((TextBox)FvinsertProblem.FindControl("txtqty_random"));
            DropDownList ddlunit_get = ((DropDownList)FvinsertProblem.FindControl("ddlunit_get"));
            DropDownList ddlunit_problem = ((DropDownList)FvinsertProblem.FindControl("ddlunit_problem"));
            DropDownList ddlunit_random = ((DropDownList)FvinsertProblem.FindControl("ddlunit_random"));


            TextBox txtqtyget_edit = ((TextBox)FvdetailProblem.FindControl("txtqtyget_edit"));
            TextBox txtqty_problemedit = ((TextBox)FvdetailProblem.FindControl("txtqty_problemedit"));
            TextBox txtqty_randomedit = ((TextBox)FvdetailProblem.FindControl("txtqty_randomedit"));
            DropDownList ddlunit_getedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_getedit"));
            DropDownList ddlunit_problemedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_problemedit"));
            DropDownList ddlunit_randomedit = ((DropDownList)FvdetailProblem.FindControl("ddlunit_randomedit"));


            TextBox txtName = (TextBox)sender;

            switch (txtName.ID)
            {
                case "txtqty_problem":
                    decimal qty_get = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_problem = Convert.ToDecimal(txtqty_problem.Text);

                    if (qty_get < qty_problem && ddlunit_get.SelectedValue == ddlunit_problem.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        txtqty_problem.Text = String.Empty;
                        ddlunit_problem.SelectedValue = "0";
                    }

                    break;

                case "txtqty_random":
                    decimal qty_get1 = Convert.ToDecimal(txtqtyget.Text);
                    decimal qty_random = Convert.ToDecimal(txtqty_random.Text);

                    if (qty_get1 < qty_random && ddlunit_get.SelectedValue == ddlunit_random.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        txtqty_random.Text = String.Empty;
                        ddlunit_random.SelectedValue = "0";
                    }

                    break;

                case "txtqty_problemedit":
                    decimal qty_get_edit = Convert.ToDecimal(txtqtyget_edit.Text);
                    decimal qty_problem_edit = Convert.ToDecimal(txtqty_problemedit.Text);

                    if (qty_get_edit < qty_problem_edit && ddlunit_getedit.SelectedValue == ddlunit_problemedit.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนที่พบปัญหาอีกครั้ง!!!');", true);
                        txtqty_problemedit.Text = String.Empty;
                        ddlunit_problemedit.SelectedValue = "0";
                    }
                    break;

                case "txtqty_randomedit":
                    decimal qty_get_edit1 = Convert.ToDecimal(txtqtyget_edit.Text);
                    decimal qty_random_edit = Convert.ToDecimal(txtqty_randomedit.Text);

                    if (qty_get_edit1 < qty_random_edit && ddlunit_getedit.SelectedValue == ddlunit_randomedit.SelectedValue)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาตรวจสอบจำนวนสุ่มอีกครั้ง!!!');", true);
                        txtqty_randomedit.Text = String.Empty;
                        ddlunit_randomedit.SelectedValue = "0";
                    }

                    break;


            }

        }

    }

    protected void setchk_changed()
    {
        GridView Gvedit_Material_risk = ((GridView)FvQA_Comment.FindControl("Gvedit_Material_risk"));

        foreach (GridViewRow row in Gvedit_Material_risk.Rows)
        {
            CheckBox chkmatrisk_edit = (CheckBox)row.Cells[0].FindControl("chkmatrisk_edit");
            Label lbm0_mridx_edit = (Label)row.Cells[1].FindControl("lbm0_mridx_edit");
            TextBox txtmatrisk_edit = (TextBox)row.Cells[2].FindControl("txtmatrisk_edit");


            if (chkmatrisk_edit.Checked == true)
            {
                txtmatrisk_edit.Visible = true;
            }
            else
            {
                txtmatrisk_edit.Visible = false;
                txtmatrisk_edit.Text = string.Empty;
            }

        }

    }
    #endregion




}