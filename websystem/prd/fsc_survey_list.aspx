<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/master_blank.master" AutoEventWireup="true" CodeFile="fsc_survey_list.aspx.cs" Inherits="websystem_prd_fsc_survey_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">
    <style>
        .table-header tr > th {
            background-color: #56c5d0;
            font-size: small;
            text-align: center;
        }

        .btn-fsc-orange:link, .btn-fsc-gray:visited {
            background-color: #f58220;
            border-color: #f58220;
            color: #ffffff;
        }

        .btn-fsc-orange:hover, .btn-fsc-orange:active {
            background-color: #f56400;
            border-color: #f56400;
            color: #ffffff;
        }

        .btn-fsc-gray {
            background-color: #a5a7aa;
            border-color: #a5a7aa;
            color: #ffffff;
        }

        .btn-fsc-gray:hover, .btn-fsc-gray:active {
            background-color: #808083;
            border-color: #808083;
            color: #ffffff;
        }
    </style>
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <img src='<%= ResolveUrl("~/masterpage/images/prd/fsc_header.png") %>' class="img-fluid"
                        width="100%" alt="Food Safety Culture">
                </div>
            </div>
            <div class="row form-group">
                <label class="col-md-8"></label>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control"
                            placeholder="ค้นหาโดยรหัสพนักงาน" MaxLength="8">
                        </asp:TextBox>
                        <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="input-group-addon"
                            OnCommand="btnCommand" CommandName="cmdItemSearch"><i class="fas fa-search"></i>
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbItemReset" runat="server" CssClass="input-group-addon"
                            OnCommand="btnCommand" CommandName="cmdItemReset"><i class="fas fa-sync-alt"></i>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvRandomList" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-responsive table-header" OnPageIndexChanging="gvPageIndexChanging"
                AllowPaging="True" PageSize="50">
                <RowStyle Font-Size="Small" />
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="#">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รหัสพนักงาน">
                        <ItemTemplate>
                            <asp:HiddenField ID="hfEmpIdx" runat="server" Value='<%# Eval("emp_idx") %>'>
                            </asp:HiddenField>
                            <asp:Label ID="lbEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อ">
                        <ItemTemplate>
                            <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ฝ่าย">
                        <ItemTemplate>
                            <asp:Label ID="lblDeptNameTh" runat="server" Text='<%# Eval("dept_name_th") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="แผนก">
                        <ItemTemplate>
                            <asp:Label ID="lblSecNameTh" runat="server" Text='<%# Eval("sec_name_th") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะแบบประเมิน">
                        <ItemTemplate>
                            <asp:Label ID="lblSurveyStatus" runat="server"
                                Text='<%# ((int)Eval("survey_status") == 0) ? "รอดำเนินการ" : "ดำเนินการเรียบร้อย" %>'
                                CssClass='<%# ((int)Eval("survey_status") == 0) ? "text-warning" : "text-success" %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่อัพเดท">
                        <ItemTemplate>
                            <asp:Label ID="lblUpdateDate" runat="server" Text='<%# Eval("update_date") %>'
                                Visible='<%# ((int)Eval("survey_status") == 0) ? false : true %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="link">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbThai" runat="server" CssClass="btn btn-sm btn-fsc-orange"
                                OnCommand="btnCommandLink" CommandName="cmdLinkTh"
                                CommandArgument='<%# Eval("emp_idx") %>'
                                Visible='<%# ((int)Eval("survey_status") == 0) ? true : false %>'>ไทย</asp:LinkButton>
                            <asp:LinkButton ID="lbBurmese" runat="server" CssClass="btn btn-sm btn-fsc-gray"
                                OnCommand="btnCommandLink" CommandName="cmdLinkBm"
                                CommandArgument='<%# Eval("emp_idx") %>'
                                Visible='<%# ((int)Eval("survey_status") == 0) ? true : false %>'>ဗမာ
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>