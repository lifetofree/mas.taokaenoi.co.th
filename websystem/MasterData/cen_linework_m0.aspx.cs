﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_linework_m0 : System.Web.UI.Page
{

    function_tool _functionTool = new function_tool();
    data_cen_master _data_cen_master = new data_cen_master();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    int _emp_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "");

            DropDownList ddlSearchOrgWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
            ddlSearchOrgWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
            showLineWork();
            FormViewS.Visible = true;
        }
      
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {

        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        hlSetTotop.Focus();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        lbCreate.Visible = false;
        gvLinework.Visible = false;
        FvInsertEdit.Visible = false;
        FormViewS.Visible = false;
        switch (cmdName)
        {
            case "cmdCreate":
                
                FvInsertEdit.Visible = true;
                
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                DropDownList ddlInsertOrg = (DropDownList)FvInsertEdit.FindControl("DropdownOrg");
                selectDdl(ddlInsertOrg, "3", "", "", "", "", "");
                DropDownList ddlInsertWg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
                ddlInsertWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                //selectWg(ddlInsertWg, 0);
                break;

            case "cmdSave":
                //lbCreate.Visible = true;
                InsertLineWork(_functionTool.convertToInt(cmdArg));


                break;

            case "cmdEdit":
                FvInsertEdit.Visible = true;
                _search_cen_master_detail.s_org_idx = "";
                _search_cen_master_detail.s_wg_idx = "";
                _search_cen_master_detail.s_lw_idx = cmdArg;
                _data_cen_master.master_mode = "5";
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

                _functionTool.setFvData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_lw_list_m0);


                DropDownList ddlEditorg = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                selectDdl(ddlEditorg, "3", "", "", "", "", "");//select
                ddlEditorg.SelectedValue = _data_cen_master.cen_lw_list_m0[0].org_idx.ToString();//choose

                DropDownList ddlEditWg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
                selectDdl(ddlEditWg, "4", _data_cen_master.cen_lw_list_m0[0].org_idx.ToString(), "", "", "", "");
                ddlEditWg.SelectedValue = _data_cen_master.cen_lw_list_m0[0].wg_idx.ToString();
                FvInsertEdit.Visible = true;
                


                break;

            case "editSave":

                InsertLineWork(_functionTool.convertToInt(cmdArg));


                break;

            case "cmdDelete":

                cmdDelete(int.Parse(cmdArg));

                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddlDeleteOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
                selectDdl(ddlDeleteOrg, "3", "", "", "", "", "");
                DropDownList ddlDeleteWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
                ddlDeleteWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                gvLinework.Visible = true;
                FormViewS.Visible = true;
                lbCreate.Visible = true;
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showLineWork();


                break;

            case "cmdCancel":
                FormViewS.Visible = true;
                gvLinework.Visible = true;
                lbCreate.Visible = true;
                if (ViewState["forSearch"] == null)
                {
                    showLineWork();
                }
                else
                {
                    _functionTool.setGvData(gvLinework, ((data_cen_master)ViewState["forSearch"]).cen_lw_list_m0);

                }
                break;
                

            case "cmdSearch":

                checkBoxSearch();
                lbCreate.Visible = true;
                gvLinework.Visible = true;
                FormViewS.Visible = true;
                break;

            case "cmdReset":
                lbCreate.Visible = true;
                gvLinework.Visible = true;
                FormViewS.Visible = true;
                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddl_s = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
                selectDdl(ddl_s, "3", "", "", "", "", "");
                DropDownList ddlSearchOrgWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
                ddlSearchOrgWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showLineWork();
                break;
        }
    }

    protected void checkBoxSearch()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        DropDownList ddl_searchOrg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList ddl_searchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_lw_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_lw_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_org_idx = ddl_searchOrg.SelectedValue;
        _search_cen_master_detail.s_wg_idx = ddl_searchWg.SelectedValue;
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _data_cen_master.master_mode = "5";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        showLineWork();


    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");

        DropDownList dropD_Org_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList dropD_Wg_s = (DropDownList)FormViewS.FindControl("DropDownWg_s");

        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        data_cen_master _data_cen_master = new data_cen_master();
        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();

        switch (ddlName.ID)
        {
            case "DropDownOrg":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                _functionTool.setDdlData(dropD_Wg, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                break;

            case "DropDownOrg_s":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                _functionTool.setDdlData(dropD_Wg_s, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg_s.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                break;

        }
    }

    protected void InsertLineWork(int id)
    {
        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_lwnameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_lwnameen");
        DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");

        

        //TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
        //litdebug.Text = tex_TH_name.Text;
        //litdebug.Text += tex_EN_name.Text;
        //litdebug.Text += dropD_status.SelectedValue;
        data_cen_master _data_cen_master = new data_cen_master();
        cen_lw_detail_m0 _cen_lw_detail_m0 = new cen_lw_detail_m0();

        _data_cen_master.master_mode = "5";
        _cen_lw_detail_m0.lw_idx = id;
        _cen_lw_detail_m0.lw_name_th = tex_TH_name.Text.Trim();
        _cen_lw_detail_m0.lw_name_en = tex_EN_name.Text.Trim();
        _cen_lw_detail_m0.lw_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_lw_detail_m0.org_idx = _functionTool.convertToInt(dropD_Org.SelectedValue);
        _cen_lw_detail_m0.wg_idx = _functionTool.convertToInt(dropD_Wg.SelectedValue);
        _cen_lw_detail_m0.cemp_idx = _emp_idx;




        _data_cen_master.cen_lw_list_m0 = new cen_lw_detail_m0[1];
        _data_cen_master.cen_lw_list_m0[0] = _cen_lw_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));
        //string msg = _data_cen_master.return_msg;
        //litdebug.Text = msg;

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            gvLinework.Visible = true;
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            FormViewS.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropdownOrg_s");
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "");
            DropDownList ddlSearchOrgWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
            ddlSearchOrgWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
            showLineWork();


        }



    }

    protected void showLineWork()
    {
        //DropDownList searchddl = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        //TextBox searchbox = (TextBox)FormViewS.FindControl("tb_wg_s");
        

        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        if (ViewState["forSearch"] == null)
        {

            _search_cen_master_detail.s_org_idx = "";

            _search_cen_master_detail.s_wg_idx = "";
            _search_cen_master_detail.s_lw_idx = "";
            _data_cen_master.master_mode = "5";

            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

            _functionTool.setGvData(gvLinework, _data_cen_master.cen_lw_list_m0);

        }
        else
        {
            _functionTool.setGvData(gvLinework, ((data_cen_master)ViewState["forSearch"]).cen_lw_list_m0);
        }


    }

    protected void selectDdl(DropDownList ddlName, string masterMode, string _org_idx, string _wg_idx, string _lw_idx, string _dept_idx, string _sec_idx)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        _data_cen_master.master_mode = masterMode;
        _search_cen_master_detail.s_org_idx = _org_idx;
        _search_cen_master_detail.s_wg_idx = _wg_idx;
        _search_cen_master_detail.s_lw_idx = _lw_idx;
        _search_cen_master_detail.s_dept_idx = _dept_idx;
        _search_cen_master_detail.s_sec_idx = _sec_idx;

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        // litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        switch (masterMode)
        {
            case "3":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));


                break;

            case "4":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกกลุ่มงาม---", ""));

                break;

            case "5":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));


                break;

            case "6":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));


                break;
        }

    }

    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_lw_detail_m0 _cen_lw_detail_m0 = new cen_lw_detail_m0();
        _cen_lw_detail_m0.lw_idx = cmdArg;
        _cen_lw_detail_m0.lw_status = 9;
        _cen_lw_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "5";


        _data_cen_master.cen_lw_list_m0 = new cen_lw_detail_m0[1];
        _data_cen_master.cen_lw_list_m0[0] = _cen_lw_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _functionTool.setGvData(gvLinework, _data_cen_master.cen_lw_list_m0);


        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);

        }
        else
        {

        }

    }

    protected void getLinework()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_lw_detail_m0 _cen_lw_detail_m0 = new cen_lw_detail_m0();


        _cen_lw_detail_m0.lw_idx = 0;
        _data_cen_master.master_mode = "5";
  
        _data_cen_master.cen_lw_list_m0 = new cen_lw_detail_m0[1];
        _data_cen_master.cen_lw_list_m0[0] = _cen_lw_detail_m0;
       

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        _functionTool.setGvData(gvLinework, _data_cen_master.cen_lw_list_m0);
        
    }

    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvLinework":
                gvLinework.PageIndex = e.NewPageIndex;
                showLineWork();
                break;

        }
        hlSetTotop.Focus();
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
}