using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_visitors_security_login : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_visitors _data_visitors = new data_visitors();

    int _emp_idx = 0;
    int _temp_idx = 0;
    string _temp_data = "";
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlVmGetSecurityProfile = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetSecurityProfile"];
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Session["se_idx"] = null;
        }
    }

    #region btn command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdLogin":
                string _userName = tbUserName.Text.Trim();
                string _userPassword = _funcTool.getMd5Sum(tbUserPassword.Text.Trim());
                if(_userName != String.Empty && _userPassword != String.Empty)
                {
                    // set data
                    vm_visitors_security_detail_m0 _check_login = new vm_visitors_security_detail_m0();
                    _check_login.username = _userName;
                    _check_login.password = _userPassword;
                    _data_visitors.vm_visitors_security_list_m0 = new vm_visitors_security_detail_m0[1];
                    _data_visitors.vm_visitors_security_list_m0[0] = _check_login;

                    _data_visitors = callServicePostVisitors(_urlVmGetSecurityProfile, _data_visitors);

                    // check return_code
                    if(_data_visitors.return_code == 0)
                    {
                        if(_data_visitors.vm_visitors_security_list_m0 != null)
                        {
                            // create session
                            Session["se_idx"] = _data_visitors.vm_visitors_security_list_m0[0].u0_idx;

                            Response.Redirect(ResolveUrl("~/vm-security"));
                        }
                        else
                        {
                            divShowError.Visible = !divShowError.Visible;
                            litErrorCode.Text = "error : data is null";
                        }
                    }
                    else
                    {
                        divShowError.Visible = !divShowError.Visible;
                        litErrorCode.Text = _data_visitors.return_code.ToString();
                        //litDebug.Text = "error : " + _data_visitors.return_code.ToString() + " - " + _data_visitors.return_msg;
                    }
                }
            break;
        }
    }
    #endregion btn command

    #region reuse
    protected data_visitors callServicePostVisitors(string _cmdUrl, data_visitors _data_visitors)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_visitors);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_visitors = (data_visitors)_funcTool.convertJsonToObject(typeof(data_visitors), _local_json);

        return _data_visitors;
    }
    #endregion reuse
}