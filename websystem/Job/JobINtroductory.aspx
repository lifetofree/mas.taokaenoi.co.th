﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/job.master" AutoEventWireup="true" CodeFile="JobINtroductory.aspx.cs" Inherits="websystem_Sir_TEST_JobINtroductory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        /*เส้น*/
        ประชาชน hr.so_sweethr006 {
            border: 0;
            height: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s;
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0;
            }

            to {
                top: 0;
                opacity: 1;
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0;
            }

            to {
                top: 0;
                opacity: 1;
            }
        }

        /* The Close Button */
        .close {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

        .modal-header {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }

        .modal-body {
            padding: 2px 16px;
        }

        .modal-footer {
            padding: 2px 16px;
            background-color: #5cb85c;
            color: white;
        }
    </style>

    <div class="container">
        <asp:Literal ID="test" runat="server"></asp:Literal>
        <asp:MultiView ID="MvtestMaster" runat="server" ActiveViewIndex="0">
            <asp:View ID="ViewAdd1" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2" data-content="ใบสมัครงาน">
                        <%--<h4 class="list-group-item col-md-3">ตำแหน่งงานที่ต้องการ</h4>--%>
                        <asp:FormView ID="fvExample" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="true">
                                    <ContentTemplate>

                                        <div class="form-horizontal" role="form">


                                            <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                                <h4><span class="label label-default">ประวัติส่วนตัว</span></h4>
                                                <hr class="so_sweethr006" />
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label29" runat="server" ForeColor="DarkBlue" Text="คำนำหน้าชื่อ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Prefix</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddPrefix" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="ddPrefixAL"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddPrefix"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกคำนำหน้าชื่อ"
                                                            ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lblname" runat="server" ForeColor="DarkBlue" Text="ชื่อ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">(ชื่อภาษาไทย)</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbname" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="tbnameAL"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbname"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbname"
                                                            ValidationExpression="[ก-ฮะ-์]{1,}"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lbllastname" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">(นามสกุลภาษาไทย)</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tblastname" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="tblastnameAL"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tblastname"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tblastname"
                                                            ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lblbirthday" runat="server" ForeColor="DarkBlue" Text="อายุ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Age</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="tbAge" runat="server" CssClass="form-control" />
                                                        <asp:RequiredFieldValidator ID="AddbirthdayAL"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbAge"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกอายุ"
                                                            ValidationExpression="เลือกอายุ" InitialValue="00" />
                                                    </div>
                                                    <div class="col-md-1">
                                                        <h5>ปี</h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label1" runat="server" ForeColor="DarkBlue" Text="เบอร์โทรติดต่อ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Phone number</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbnumber" runat="server" CssClass="form-control" placeholder="เบอร์โทร" MaxLength="10" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbnumber"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกเบอร์โทรที่ติดต่อได้"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbnumber"
                                                            ValidationExpression="^[0-9]{9,10}$"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label2" runat="server" ForeColor="DarkBlue" Text="อีเมล์"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Email</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbEmail" runat="server" CssClass="form-control" placeholder="Email" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbEmail"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอก Email"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                            ErrorMessage="&nbsp;* กรุณากรอก E-mail ให้ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbEmail"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                                <h4><span class="label label-default">การศึกษา</span></h4>
                                                <hr class="so_sweethr006" />
                                            </div>
                                            
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                <asp:CheckBox ID="hide_education" runat="server" OnCheckedChanged="Checkbox" AutoPostBack="true" />
                                                    ไม่มีวุฒิการศึกษา
                                                    </label>
                                                </div>
                                            </div>

                                            <asp:Panel ID="NoEdu" runat="server" Visible="true">
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label3" runat="server" ForeColor="DarkBlue" Text="ชื่อสถาบัน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Name of Institute</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbNameInstitute" runat="server" CssClass="form-control" AutoPostBack="true" placeholder="ชื่อสถาบัน" MaxLength="70" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbNameInstitute"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อสถาบัน"
                                                            ValidationExpression="Please check Name" />
                                                    </div>
                                                </div>
                                                </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label4" runat="server" ForeColor="DarkBlue" Text="ระดับการศึกษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Education</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddEducation" runat="server" CssClass="form-control" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddEducation"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกระดับการศึกษา"
                                                            ValidationExpression="กรุณาเลือกระดับการศึกษา" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="Label5" runat="server" ForeColor="DarkBlue" Text="สาขา/วิชาที่ศึกษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Course Taken</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbCourseTaken" runat="server" CssClass="form-control" placeholder="สาขา/วิชาที่ศึกษา" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                            ValidationGroup="formInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbCourseTaken"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกสาขา/วิชาที่ศึกษา"
                                                            ValidationExpression="Please check Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            </asp:Panel>

                                            <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                                <h4><span class="label label-default">ตำแหน่งงานที่สนใจ</span></h4>
                                                <hr class="so_sweethr006" />
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblposition1" runat="server" ForeColor="DarkBlue" Text="ตำแหน่งที่ 1"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Position applied for</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="tbposition1" runat="server" CssClass="form-control" AutoPostBack="true" />
                                                        <asp:RequiredFieldValidator ID="page01"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbposition1"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกตำงานที่ต้องการสมัคร"
                                                            ValidationExpression="เลือกตำแหน่งงานที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblposition2" runat="server" ForeColor="DarkBlue" Text="ตำแหน่งที่ 2"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Position applied for</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="tbposition2" runat="server" CssClass="form-control" placeholder="ตำแหน่งที่ 2" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbposition2"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกตำงานที่ต้องการสมัคร"
                                                            ValidationExpression="เลือกตำแหน่งงานที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblmoney" runat="server" ForeColor="DarkBlue" Text="เงินเดือนที่ต้องการ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Expected starting salary</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbmoney" runat="server" AutoPostBack="true" OnTextChanged="TextBox1_TextChanged" CssClass="form-control" placeholder="เงินเดือนที่ต้องการ" MaxLength="6" />
                                                        <asp:RequiredFieldValidator ID="moneyWant" ValidationGroup="formInsert" runat="server" Display="Dynamic"
                                                            ControlToValidate="tbmoney" Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกเงินเดือน"
                                                            SetFocusOnError="true"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="moneyWantNum" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ ใส่ข้อมู"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbmoney"
                                                            ValidationExpression="[0-9,]{1,10}"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <h5>บาท</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                                <div class="col-md-7 text-right">
                                                    <asp:LinkButton ID="btadd" CssClass="btn btn-primary" runat="server" ValidationGroup="formInsert" CommandName="AddBasic" OnCommand="btnCommand" data-toggle="tooltip" data-original-title="บันทึก"> <i class="glyphicon glyphicon-floppy-saved" aria-hidden="true"></i> Save</asp:LinkButton>
                                                </div>
                                            </div>

                                            <%--<script type="text/javascript">
                                                function btnSubmit_Edit_Click() {

                                                    var T0 = "กรุณาตรวจสอบข้อมูลของท่านก่อนทำการกด 'ตกลง'"  
                                                    var T11 = "ประวัติส่วนตัว";
                                                    var T1 = "ชื่อ-นามสกุล";
                                                    var Prefix = document.getElementById("<%=fvExample.FindControl("ddPrefix").ClientID %>");
                                                    var PrefixR = Prefix.options[Prefix.selectedIndex].text;
                                                    var Name = document.getElementById("<%=fvExample.FindControl("tbname").ClientID %>").value;
                                                    var LastName = document.getElementById("<%=fvExample.FindControl("tblastname").ClientID %>").value;
                                                    var T2 = "อายุ";
                                                    var Age = document.getElementById("<%=fvExample.FindControl("tbAge").ClientID %>").value;
                                                    var T3 = "เบอร์โทรติดต่อ";
                                                    var Phone = document.getElementById("<%=fvExample.FindControl("tbnumber").ClientID %>").value;
                                                    var T4 = "Email";
                                                    var Email = document.getElementById("<%=fvExample.FindControl("tbEmail").ClientID %>").value;

                                                    var T22 = "การศึกษา";
                                                    var T5 = "ชื่อสถาบัน";
                                                    var NameEdu = document.getElementById("<%=fvExample.FindControl("tbNameInstitute").ClientID %>").value;
                                                    var T6 = "ระดับการศึกษา";
                                                    var Education = document.getElementById("<%=fvExample.FindControl("ddEducation").ClientID %>");
                                                    var EducationR = Education.options[Education.selectedIndex].text;
                                                    var T7 = "สาขา/วิชาที่ศึกษา";
                                                    var NameL = document.getElementById("<%=fvExample.FindControl("tbCourseTaken").ClientID %>").value;

                                                    var T33 = "ตำแหน่งงานที่สนใจ";
                                                    var T8 = "ตำแหน่งงานที่ 1";
                                                    var position1 = document.getElementById("<%=fvExample.FindControl("tbposition1").ClientID %>");
                                                    var position1R = position1.options[position1.selectedIndex].text;
                                                    var T9 = "ตำแหน่งงานที่ 2";
                                                    var position2 = document.getElementById("<%=fvExample.FindControl("tbposition2").ClientID %>");
                                                    var position2R = position2.options[position2.selectedIndex].text;
                                                    var T10 = "เงินเดือนที่ต้องการ";
                                                    var money = document.getElementById("<%=fvExample.FindControl("tbmoney").ClientID %>").value;


                                                    var report = T0 + "\n" + "\n" + T11 + "\n" + T1 + " : " + PrefixR + " " + Name + " " + LastName + "\n" + T2 + " : " + Age + "\n" + T3 + " : " + Phone + "\n" + T4 + " : " + Email + "\n" + "\n" + T22 + "\n" + T5 + " : " + NameEdu + "\n" + T6 + " : " + EducationR + "\n" + T7 + " : " + NameL + "\n" + "\n" + T33 + "\n" + T8 + " : " + position1R + "\n" + T9 + " : " + position2R + "\n" + T10 + " : " + money + " " + "บาท" + "\n";
                                                    return confirm(report);

                                                }
                                            </script>--%>
                                        </div>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btadd" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <script type="text/javascript">
                                    function openModal() {
                                        $('#ordine').modal('show');
                                    }
                                </script>
                                <asp:UpdatePanel ID="UpdatePanelModul" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="col-lg-12" runat="server" id="Div2">
                                            <div id="ordine" class="modal open" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <%--<button type="button" class="close" data-dismiss="modal">×</button>--%>
                                                            <h4 class="modal-title" style="margin-left: 3%;">ข้อมูลผู้สมัครงาน</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <asp:Repeater ID="RepeaterEmp" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                                <ItemTemplate>
                                                                    <div class="row">
                                                                        <asp:Literal ID="testmodal" runat="server"></asp:Literal>
                                                                        <br />
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1">
                                                                                <b>
                                                                                    <asp:Label ID="Label27" runat="server" ForeColor="Red" Text="กรุณาตรวจสอบข้อมูลของท่านก่อนทำการกด 'ยืนยัน'"></asp:Label></b>
                                                                            </div>
                                                                        </div>
                                                                        <br />
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label8" runat="server" Text="ชื่อ-นามสกุล :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="prefixS" runat="server" Text='<%# Eval("ddPrefix") %>'></asp:Label>&nbsp;
                                                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("tbname") %>'></asp:Label>&nbsp;
                                                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("tblastname") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label9" runat="server" Text="อายุ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("tbAge") %>'></asp:Label>&nbsp;
                                                                            <asp:Label ID="Label28" runat="server" Text="ปี"></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label11" runat="server" Text="เบอร์โทรติดต่อ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("tbnumber") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label13" runat="server" Text="อีเมล์ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label14" runat="server" Text='<%# Eval("tbEmail") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <hr class="so_sweethr006" style="width: 90%;" />
                                                                        <asp:Panel ID="Panel0" runat="server" Visible="true">
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label15" runat="server" Text="ชื่อสถาบัน :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label16" runat="server" Text='<%# Eval("tbNameInstitute") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label17" runat="server" Text="ระดับการศึกษา :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label18" runat="server" Text='<%# Eval("ddEducation") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label19" runat="server" Text="สาขา/วิชาที่ศึกษา :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label20" runat="server" Text='<%# Eval("tbCourseTaken") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <hr class="so_sweethr006" style="width: 90%;" />
                                                                        </asp:Panel>
                                                                        
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label21" runat="server" Text="ตำแหน่งที่ 1 :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label22" runat="server" Text='<%# Eval("tbposition1") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label23" runat="server" Text="ตำแหน่งที่ 2 :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label24" runat="server" Text='<%# Eval("tbposition2") %>'></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1" style="padding-right: 5%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label25" runat="server" Text="เงินเดือนที่ต้องการ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label26" runat="server" Text='<%# Eval("tbmoney") %>'></asp:Label>&nbsp;
                                                                            <asp:Label ID="Label30" runat="server" Text="บาท"></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:Repeater>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="col-md-12">
                                                                <asp:LinkButton ID="pass" class="btn btn-primary btn-sm" title="ยืนยัน" runat="server" CommandName="pass" OnClientClick="return confirm('คุณต้องการยืนยันใช่หรือไม่ ?')" OnCommand="btnCommand"><i class="fa fa-paper-plane-o"></i>&nbsp;ยืนยัน</asp:LinkButton><%--PostBackUrl="~/websystem/Job/JobINtroductory.aspx"--%>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="pass" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="ViewAdd10" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2">
                        <asp:FormView ID="FormView9" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <br />
                                <div class="form-group text-center">
                                    <h3>ขั้นตอนการสมัครเบื้องต้นเสร็จสิ้นกรุณารอการติดต่อกลับจาก 
                                       <br />
                                        บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด(มหาชน)</h3>
                                </div>
                                <div class="form-group text-center">
                                </div>
                                <meta http-equiv="Refresh" content="9; url=http://www.taokaenoi.co.th/" />
                                <hr class="so_sweethr006" />
                                <div class="form-group text-center">
                                    <h6>กำลังปิดหน้าต่างนี้ในอีก...</h6>
                                    <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="http://www.taokaenoi.co.th/" CssClass="btn btn-default btn-sm" data-original-title="Skip" Target="_parent">Skip</asp:HyperLink>
                                    <img class="img-responsive center-block" width="100" height="100" src='<%= ResolveUrl("~/masterpage/images/countdown.gif") %>' />
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>

