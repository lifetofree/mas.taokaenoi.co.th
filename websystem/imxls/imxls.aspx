﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="imxls.aspx.cs" Inherits="websystem_imxls_imxls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="test" runat="server"></asp:Literal>
   <script>
      $(function () {
         $('.imxls-img-loading').hide();
         $('.imxls-files-loading').on('click', function () {
            $('.imxls-file-area').hide();
            $('.imxls-img-loading').show();
         });
      });
   </script>

   <asp:Label ID="notRules" runat="server" CssClass="text-danger" />
   <div id="fileArea" runat="server" class="imxls-file-area">
      <br />
      <asp:FileUpload ID="upload" runat="server" />
      <br />
      <asp:Button ID="btnImport" runat="server" Text="Import" OnCommand="btnCommand" CommandName="btnImport"
         CssClass="btn btn-success imxls-files-loading" />
      <br />
      <br />
   </div>
   <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>' width="40" height="40" class="imxls-img-loading" />
   <asp:GridView ID="gvXls"
      runat="server"
      AutoGenerateColumns="false"
      CssClass="table table-striped table-bordered table-responsive col-md-12"
      HeaderStyle-CssClass="info">
      <PagerStyle CssClass="PageCustom" />
      <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
      <EmptyDataTemplate>
         <div style="text-align: center">No result</div>
      </EmptyDataTemplate>
      <Columns>
         <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
               <%# (Container.DataItemIndex +1) %>
            </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-CssClass="text-center"
            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
               <%# Eval("imxls_created_at_date")%> <%# Eval("imxls_created_at_time")%> น.
            </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="จำนวนข้อมูล" HeaderStyle-CssClass="text-center"
            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
               <%# Eval("imxls_rows")%> ข้อมูล
            </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderStyle-CssClass="text-center"
            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
               <asp:LinkButton ID="btnDescription" runat="server" CssClass="btn btn-info"
                  OnCommand="btnCommand" CommandName="btnDescription"
                  CommandArgument='<%# Eval("u0_imxls_idx")%>' Text="รายละเอียด" />
            </ItemTemplate>
         </asp:TemplateField>
      </Columns>
   </asp:GridView>

   <asp:LinkButton ID="btnBack" runat="server" CssClass="btn btn-danger" Visible="false"
      OnCommand="btnCommand" CommandName="btnBack" Text="ย้อนกลับ" />
   <div id="divGvXlsWhere" style="overflow-x: scroll; width: 100%" visible="false" runat="server">
      <asp:GridView ID="gvXlsWhere"
         runat="server"
         AutoGenerateColumns="false"
         CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
         HeaderStyle-CssClass="info"
         AllowPaging="true"
         PageSize="100"
         OnPageIndexChanging="OnPageIndexChanging">
         <PagerStyle CssClass="pageCustom" />
         <PagerSettings Mode="Numeric" PageButtonCount="10" />
         <EmptyDataTemplate>
            <div style="text-align: center">No result</div>
         </EmptyDataTemplate>
         <Columns>
            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# (Container.DataItemIndex +1) %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("empcode_new") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="คำนำหน้า" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("prefix") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อภาษาไทย" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("firstname_th") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="นามสกุลภาษาไทย" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("lastname_th") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อภาษาอังกฤษ" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("firstname_en") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="นามสกุลภาษาอังกฤษ" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("lastname_en") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่เริ่มทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("emp_in") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("department") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("section") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("position") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สังกัดค่าใช้จ่าย" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("cost_center") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เพศ" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("sex") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันเกิด" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("birthday") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เลขที่หนังสือเดินทาง" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("passport") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานที่ออกหนังสือเดินทาง" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("passport_issued_at") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกหนังสือเดินทาง" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("issued_date") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันหมดอายุหนังสือเดินทาง" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("passport_exp_date") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="เลขที่ใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("work_permit_no") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("work_permit_issued_at") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("work_permit_issued_date") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("work_permit_exp_date") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("social_id") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("account_no") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("nationality") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("race") %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="วันที่ออกใบอนุญาตทำงาน" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# Eval("religion") %>
               </ItemTemplate>
            </asp:TemplateField>
         </Columns>
      </asp:GridView>
   </div>
</asp:Content>
