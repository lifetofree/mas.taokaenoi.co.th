﻿using AjaxControlToolkit;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class websystem_el_TrnPlan : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan"];
    static string _urlSetInsel_u_plan = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan"];
    static string _urlDelel_u_plan = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan"];
    static string _urlSetUpdel_u_plan = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan"];

    static string _urlsendEmail_Plan_hrtohrd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_Plan_hrtohrd"];
    static string _urlsendEmail_Plan_hrdtomd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_Plan_hrdtomd"];
    static string _urlsendEmail_Plan_hrdtohr_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_Plan_hrdtohr_all"];
    static string _urlsendEmail_Plan_mdtohrd_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_Plan_mdtohrd_all"];


    static string _urlGetel_Report_Plan = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_Plan"];



    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;

    string _FromcourseRunNo = "u_plan";



    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;


    }
    #endregion Constant



    //_object
    FormView _FormView;
    TextBox _txttraining_plan_no;
    TextBox _txttraining_group_name;
    TextBox _txttraining_branch_name;
    DropDownList _ddlu0_course_idx_ref;
    TextBox _txttraining_plan_date;
    TextBox _txttraining_plan_year;
    DropDownList _ddlm0_target_group_idx_ref;
    TextBox _txttraining_plan_qty;
    TextBox _txttraining_plan_amount;
    TextBox _txttraining_plan_model;
    TextBox _txttraining_plan_budget;
    TextBox _txttraining_plan_costperhead;

    RadioButtonList _rdllecturer_type;
    DropDownList _ddlm0_institution_idx_ref;
    GridView _GvMonthList, _GvMonthList_L, _GvMonthList_A;
    DropDownList _ddltraining_plan_status;
    CheckBox _cb_MTT;
    CheckBox _cb_NPW;
    CheckBox _cb_RJN;
    Panel _pnlhistory;
    GridView _GvHistory;

    #endregion Init

    private DateTime _dtMonth;
    private DateTime _selectedDate;
    private bool _specialDaySelected = true;
    private int _currentBindingMonth;

    public void zCrontrollFrom()
    {
        // Page Crontroll
        string _gFromName = ("el_TrnPlan").ToUpper();
        string _gbtnListData = ("btnListData").ToUpper();
        string _gbtnInsert = ("btnInsert").ToUpper();
        string _gbtnHR_Wait = ("btnHR_Wait").ToUpper();
        string _gbtnHR_App = ("btnHR_App").ToUpper();
        string _gbtnMD_Wait = ("btnMD_Wait").ToUpper();
        string _gbtnMD_App = ("btnMD_App").ToUpper();

        //rpos_idx
        //วิจิตรตรา พลอยกระจ่าง	7124
        //วุฒิพงษ์ โพธิ์เจริญ 7124
        //ธัญญกานต์ วรานนท์วนิช   11754
        string _gsHR_rpos_idx = "11754", _gsHR_rpos_idx1 = "5900", _gsHR_rpos_idx2 = "7124"
              , _gsHR_rpos_idx3 = "5964";
        //ธัญญกานต์ วรานนท์วนิช HROD Manager 11754
        string _gsHRD_rpos_idx = "5900"; // .44
        string _gsHRD_rpos_idx28 = "11754"; //.28

        //ปริญญ์ พิชญวิจิตร์ ประธานเจ้าหน้าที่กลุ่มงานทรัพยากรบุคคล 12599
        string _gsMD_rpos_idx = "12599", _gsMD_rpos_idx26 = "12599";

        string _gsAdmin = "210";
        string emp_code_admin = "58000070";

        ViewState["emp_code"] = "";
        ViewState["rpos_idx"] = "";
        ViewState["rsec_idx"] = "";
        ViewState["rdept_idx"] = "";
        ViewState["org_idx"] = "";
        ViewState["jobgrade_idx"] = "";
        ViewState["joblevel_idx"] = "";

        data_elearning dataelearning = new data_elearning();
        dataelearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        dataelearning.employeeM_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, dataelearning);
        if (dataelearning.employeeM_action != null)
        {
            foreach (var item in dataelearning.employeeM_action)
            {
                ViewState["emp_code"] = item.EmpCode;
                ViewState["rpos_idx"] = item.RPosIDX_J;
                ViewState["rsec_idx"] = item.RSecID;
                ViewState["rdept_idx"] = item.RDeptID;
                ViewState["org_idx"] = item.OrgIDX;
                ViewState["jobgrade_idx"] = item.JobGradeIDX;
                ViewState["joblevel_idx"] = item.JobLevel;
            }
        }

        btnListData.Visible = false;
        btnInsert.Visible = false;
        btnHR_Wait.Visible = false;
        btnHR_App.Visible = false;
        btnMD_Wait.Visible = false;
        btnMD_App.Visible = false;
        pnlListData.Visible = false;
        liplanReport.Visible = false;
        int _item = 0;
        if (ViewState["emp_code"].ToString() == "90000001")
        {
            // test 
        }
        else
        {
            if ((ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx) ||
               (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx1) ||
               (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx2) ||
               (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx3) ||
              // (ViewState["rsec_idx"].ToString() == _gsAdmin) ||
               (ViewState["emp_code"].ToString() == emp_code_admin)
               )
            {
                _item++;
                //ผู้จัดการฝ่ายพัฒนาบุคลากร
                btnListData.Visible = true;
                btnInsert.Visible = true;
                pnlListData.Visible = true;
                liplanReport.Visible = true;
            }
            if ((ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx) || 
                (ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx28) ||
                //(ViewState["rsec_idx"].ToString() == _gsAdmin) ||
                (ViewState["emp_code"].ToString() == emp_code_admin)
                )
            {
                _item++;
                //ผู้จัดการฝ่ายบริการงานทรัพยากรบุคคล
                btnHR_Wait.Visible = true;
                btnHR_App.Visible = true;
                zbtnHR_Wait();
            }
            if ((ViewState["rpos_idx"].ToString() == _gsMD_rpos_idx) ||
                (ViewState["rpos_idx"].ToString() == _gsMD_rpos_idx26) ||
                // (ViewState["rsec_idx"].ToString() == _gsAdmin) ||
                (ViewState["emp_code"].ToString() == emp_code_admin)
                )
            {
                _item++;
                //Managing Director
                btnMD_Wait.Visible = true;
                btnMD_App.Visible = true;
                zbtnMD_Wait();
            }
        }
        if (_item == 3)
        {
            zSetMode(2);
        }

    }



    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            //try
            //{
            //  string sGetCurrentTime =  Request.QueryString["GetCurrentTime"];
            //    txtUserName.Text = sGetCurrentTime;
            //}
            //catch
            //{

            //}

            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR"
                                );

            //_func_dmu.zSetDdlMonth(ddlMonthSearch_TrnNSur);
            //ddlMonthSearch_TrnNSur.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            //_func_dmu.zDropDownList(ddlYearSearch_TrnNSur,
            //                    "-",
            //                    "zyear",
            //                    "zyear",
            //                    "0",
            //                    "trainingLoolup",
            //                    DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
            //                    "TRN-YEAR"
            //                    );


            Hddfld_training_plan_no.Value = "";
            Hddfld_u0_training_plan_idx.Value = "";
            Hddfld_status.Value = "P";
            getDatasetEmpty();
            zSetMode(2);

            zCrontrollFrom();

        }
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(btnHR_Wait);
        linkBtnTrigger(btnHR_App);
        linkBtnTrigger(btnFilter);

        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnClear);
        linkBtnTrigger(btnscheduler);
        linkBtnTrigger(btnS_SCHED);
        linkBtnTrigger(btnprint_sched);

        linkBtnTrigger(btnrptscheduler);
        linkBtnTrigger(btnS_rpt_SCHED);
        linkBtnTrigger(btnprint_plan);
        //linkBtnTrigger(btnexportexcel_plan);

    }
    #endregion Page Load


    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        pnlSave.Visible = false;
        switch (AiMode)
        {
            case 0:  //insert mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = true;
                    fv_Update.Visible = false;
                    fvCRUD.ChangeMode(FormViewMode.Insert);
                    setActiveTab("I");
                    actionIndex();
                    Select_Page1showdata();

                }
                break;
            case 1:  //update mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = false;
                    fv_Update.Visible = true;
                    fv_Update.ChangeMode(FormViewMode.Edit);
                    setActiveTab("I");
                    actionIndex();
                }
                break;
            case 2://preview mode
                {
                    pnlListData.Visible = true;
                    setActiveTab("P");
                    ShowListData();
                    MultiViewBody.SetActiveView(View_ListDataPag);

                }
                break;
            case 3://รายการที่รอ HRD อนุมัติ
                {
                    pnlListData.Visible = false;
                    setActiveTab("HR_W");
                    MultiViewBody.SetActiveView(View_HR_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR"
                                );
                    txtFilterKeyword_HR_W.Text = "";
                }
                break;
            case 4://รายการที่ HRD อนุมัติเสร็จแล้ว
                {
                    pnlListData.Visible = false;
                    setActiveTab("HR_A");
                    MultiViewBody.SetActiveView(View_HR_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR"
                                );
                    txtFilterKeyword_HR_A.Text = "";
                }
                break;
            case 5://รายการ HRD Detail
                {
                    pnlListData.Visible = false;
                    setActiveTab("");
                    MultiViewBody.SetActiveView(View_HR_WDetail);
                    fv_preview.Visible = true;
                    fv_preview.ChangeMode(FormViewMode.Edit);
                }
                break;
            case 6://รายการที่รอ MD อนุมัติ
                {
                    pnlListData.Visible = false;
                    setActiveTab("MD_W");
                    MultiViewBody.SetActiveView(View_MD_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR"
                                );
                    txtFilterKeyword_MD_W.Text = "";
                }
                break;
            case 7://รายการที่ MD อนุมัติเสร็จแล้ว
                {
                    pnlListData.Visible = false;
                    setActiveTab("MD_A");
                    MultiViewBody.SetActiveView(View_MD_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR"
                                );
                    txtFilterKeyword_MD_A.Text = "";
                }
                break;
            case 8://ตารางแผนการอบรม
                {
                    pnlListData.Visible = false;
                    setActiveTab("SCHEDULER");
                    MultiViewBody.SetActiveView(View_SCHEDList);
                    _func_dmu.zDropDownList(ddlYearSearch_SCHED,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR-SCHED"
                                );
                    txtFilterKeyword_SCHED.Text = "";
                    _func_dmu.zDropDownList(ddltrn_groupSearch_SCHED,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-GRP-SCHED"
                                );
                }
                break;
            case 9://รายงานแผนการอบรม
                {
                    pnlListData.Visible = false;
                    setActiveTab("RPT_SCHEDULER");
                    MultiViewBody.SetActiveView(View_rpt_SCHEDList);
                    _func_dmu.zDropDownList(ddlYearSearch_rpt_SCHED,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR-SCHED"
                                );
                    txtFilterKeyword_rpt_SCHED.Text = "";
                    _func_dmu.zDropDownList(ddltrn_groupSearch_rpt_SCHED,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-GRP-SCHED"
                                );
                }
                break;
        }
    }
    #endregion zSetMode

    private void ShowListHR_W() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "HR-W";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_plan_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetGridData(GvHR_WList, dataelearning.el_training_plan_action);
        ShowListHR_W_Status();

    }
    private void ShowListHR_W_Status() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "HR-W";
        obj.operation_status_id = "U0-LISTDATA-HR-COUNT";
        dataelearning1.el_training_plan_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_plan_action != null)
        {
            iCount = dataelearning1.el_training_plan_action[0].idx;
        }
        //iCount = 99;
        if (iCount > 0)
        {
            btnHR_Wait.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnHR_Wait.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }
    private void ShowListHR_A() //รายการที่ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.filter_keyword = txtFilterKeyword_HR_A.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_A.SelectedValue);
        obj.zstatus = "HR-A";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_plan_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetGridData(GvHR_AList, dataelearning.el_training_plan_action);
        ShowListHR_W_Status();
    }

    private void ShowListMD_W() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.filter_keyword = txtFilterKeyword_MD_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_plan_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetGridData(GvMD_WList, dataelearning.el_training_plan_action);
        ShowListMD_W_Status();
    }
    private void ShowListMD_W_Status() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR-COUNT";
        dataelearning1.el_training_plan_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_plan_action != null)
        {
            iCount = dataelearning1.el_training_plan_action[0].idx;
        }
        //iCount = 99;
        if (iCount > 0)
        {
            btnMD_Wait.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnMD_Wait.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }
    private void ShowListMD_A() //รายการที่ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.filter_keyword = txtFilterKeyword_MD_A.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_A.SelectedValue);
        obj.zstatus = "MD-A";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_plan_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetGridData(GvMD_AList, dataelearning.el_training_plan_action);
        ShowListMD_W_Status();
    }


    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        liHR_Wait.Attributes.Add("class", "");
        liHR_App.Attributes.Add("class", "");
        liMD_Wait.Attributes.Add("class", "");
        liMD_App.Attributes.Add("class", "");


        liplanReport.Attributes.Add("class", "");
        lischeduler.Attributes.Add("class", "");
        lirptscheduler.Attributes.Add("class", "");

        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                break;
            case "HR_W":
                liHR_Wait.Attributes.Add("class", "active");
                break;
            case "HR_A":
                liHR_App.Attributes.Add("class", "active");
                break;
            case "MD_W":
                liMD_Wait.Attributes.Add("class", "active");
                break;
            case "MD_A":
                liMD_App.Attributes.Add("class", "active");
                break;
            case "SCHEDULER":
                liplanReport.Attributes.Add("class", "active");
                lischeduler.Attributes.Add("class", "active");
                break;
            case "RPT_SCHEDULER":
                liplanReport.Attributes.Add("class", "active");
                lirptscheduler.Attributes.Add("class", "active");
                break;

        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {

    }

    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    protected void Select_Page1showdata()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();

        obj.operation_status_id = "U0-EMPTY";
        dataelearning.el_training_plan_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_training_plan_action);

        _txttraining_plan_no = (TextBox)fvCRUD.FindControl("txttraining_plan_no");
        _txttraining_group_name = (TextBox)fvCRUD.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)fvCRUD.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)fvCRUD.FindControl("ddlu0_course_idx_ref");
        _txttraining_plan_date = (TextBox)fvCRUD.FindControl("txttraining_plan_date");
        _txttraining_plan_year = (TextBox)fvCRUD.FindControl("txttraining_plan_year");
        _ddlm0_target_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_target_group_idx_ref");

        _txttraining_plan_date.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _txttraining_plan_year.Text = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);
        Hddfld_training_plan_no.Value = "";
        Hddfld_u0_training_plan_idx.Value = "";
        Hddfld_status.Value = "I";

        _func_dmu.zDropDownListWhereID(_ddlu0_course_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-COURSE-U0-LOOKUP",
                                "zyear",
                                _func_dmu.zStringToInt(_txttraining_plan_year.Text)
                                );
        _rdllecturer_type = (RadioButtonList)fvCRUD.FindControl("rdllecturer_type");
        setlecturertype(_func_dmu.zStringToInt(_rdllecturer_type.SelectedValue), 0);
        _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-TARGET"
                                );



        zShowDefaultMonth();
        zShowDefaultMonthList();

    }

    protected void getDatasetEmpty() // สร้าง dataset
    {
        CreateDs_el_month();
        CreateDs_el_monthList();
    }

    private void ShowListData()
    {

        _data_elearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.approve_status = int.Parse(ddlStatusapprove_L.SelectedValue);
        obj.operation_status_id = "U0-LISTDATA";
        _data_elearning.el_training_plan_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zSetGridData(GvListData, _data_elearning.el_training_plan_action);

    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        //var chkName = (CheckBoxList)sender;
        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDelete_GvListData");
                TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvListData");
                TextBox txtmd_approve_status = (TextBox)e.Row.Cells[9].FindControl("txtmd_approve_status_GvListData");

                Boolean bBl = true, bBl_a = true;
                //if (txtmd_approve_status.Text == "4")
                //{
                //    bBl = false;
                //}
                //else if (txtmd_approve_status.Text == "6")
                //{
                //    bBl = false;
                //}

                if ((txtapprove_status.Text == "4") || (txtmd_approve_status.Text == "4"))// 4	อนุมัติ
                {
                    bBl = false;
                    bBl_a = true;
                }
                else if ((txtapprove_status.Text == "6") || (txtmd_approve_status.Text == "6"))
                {
                    bBl = false;
                    bBl_a = false;
                }
                if ((txtapprove_status.Text == "5") || (txtmd_approve_status.Text == "5"))// 4	อนุมัติ
                {
                    bBl = true;
                    bBl_a = true;
                }
                btnUpdate_GvListData.Visible = bBl_a;
                btnDelete_GvListData.Visible = bBl;
            }
        }
        else if (sGvName == "GvMonthList_L")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnUpdate_GvListData");
                //LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDelete_GvListData");
                //TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvListData");
                //TextBox txtmd_approve_status = (TextBox)e.Row.Cells[9].FindControl("txtmd_approve_status_GvListData");

                //Boolean bBl = true;
                //btnUpdate_GvListData.Visible = bBl;
                //btnDelete_GvListData.Visible = bBl;
            }

        }
    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                // showmodal_traning(txtsearch_modal_training.Text);
                break;
            case "gvModalTraning":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                //showmodal_traning_app(txtsearch_modal_training_app.Text);
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData();
                SETFOCUS.Focus();
                break;
            case "GvHR_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            case "GvHR_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "GvMD_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "GvMD_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_A();
                SETFOCUS.Focus();
                break;

        }
    }

    protected string getStatus(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatusPlanCourse(int status)
    {
        if (status == 1)
        {

            return "<span class='' data-toggle='tooltip' title='คอร์สอบรม'><i class='glyphicon glyphicon-file'></i></span>";
        }
        else
        {
            return "";
        }
    }

    protected string getStatustest(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    private void zbtnHR_Wait()
    {
        zSetMode(3);
        ShowListHR_W();
    }

    private void zbtnMD_Wait()
    {
        zSetMode(6);
        ShowListMD_W();
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _m0_training_idx;
        int _cemp_idx, _md_app = 1;

        m0_training objM0_ProductType = new m0_training();
        Panel pnlDetailApp;
        Panel pnlapprove_hr;
        Panel pnlApprove_HR_W;
        Panel pnlApprove_HR_A;
        Label lbtitle_preview;

        Panel pnlapprove_md;
        Panel pnlApprove_MD_W;
        Panel pnlApprove_MD_A;



        switch (cmdName)
        {
            case "btnListData":
                zSetMode(2);
                SETFOCUS.Focus();
                break;

            case "btnInsert":
                getDatasetEmpty();
                zSetMode(0);
                MultiViewBody.SetActiveView(View_trainingPage);
                btnSaveInsert.Visible = true;
                btnSaveUpdate.Visible = false;
                pnlSave.Visible = true;
                SETFOCUS.Focus();
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnSaveInsert":
                if (checkError() == false)
                {

                    if (zSave(0) == true)
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;
            case "btnDelete":
                _m0_training_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
                zDelete(_m0_training_idx);
                ShowListData();
                break;
            case "btnFilter":
                // actionIndex();
                ShowListData();
                break;

            case "btnselGvListData_TrnNSur":
                _m0_training_idx = int.Parse(cmdArg);
                pnlListData.Visible = false;
                setActiveTab("I");
                Select_Page1showdata();
                break;

            case "btnImport":

                break;

            case "btnUpdate_GvListData":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "E");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = true;
                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(2);
                }
                SETFOCUS.Focus();
                break;
            case "btnSaveUpdate":
                if (checkError() == false)
                {
                    if (zSave(_func_dmu.zStringToInt(Hddfld_u0_training_plan_idx.Value)) == true)
                    {

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;
            case "btnDetail":
                _m0_training_idx = int.Parse(cmdArg);
                getDatasetEmpty();
                if (_m0_training_idx > 0)
                {

                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "P");
                    pnlSave.Visible = true;
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = false;
                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(2);
                }
                SETFOCUS.Focus();
                break;
            //HR
            case "btnHR_Wait":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_W":
                ShowListHR_W();
                break;
            case "btnDetail_GvHR_WList":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("HR_W");
                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "HR-W", "HR");

                    pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                    pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
                    pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
                    lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

                    pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
                    pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

                    pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
                    pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");

                    pnlDetailApp.Visible = _func_dmu.zIntToBoolean(_md_app);
                    pnlApprove_HR_W.Visible = true;
                    pnlApprove_HR_A.Visible = false;

                    pnlApprove_MD_W.Visible = false;
                    pnlApprove_MD_A.Visible = false;

                    pnlapprove_hr.Visible = true;
                    pnlapprove_md.Visible = false;

                    lbtitle_preview.Text = lbHR_Wait.Text;

                    zcrtl_btnback("HRW");
                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(3);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);

                }
                zSetMode(3);
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnHR_App":
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_A":
                ShowListHR_A();
                break;
            case "btnDetail_GvHR_AList":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("HR_A");
                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "HR-A", "HR");

                    pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                    pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
                    pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
                    lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

                    pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
                    pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

                    pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
                    pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");

                    pnlDetailApp.Visible = _func_dmu.zIntToBoolean(_md_app);
                    pnlApprove_HR_W.Visible = false;
                    pnlApprove_HR_A.Visible = true;

                    pnlApprove_MD_W.Visible = false;
                    pnlApprove_MD_A.Visible = false;

                    pnlapprove_hr.Visible = true;
                    pnlapprove_md.Visible = false;

                    lbtitle_preview.Text = lbHR_App.Text;

                    zcrtl_btnback("HRA");
                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR_A":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);

                }
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            //MD
            case "btnMD_Wait":
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_W":
                ShowListMD_W();
                break;
            case "btnDetail_GvMD_WList":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("MD_W");
                    zShowdataHR_WDetail(_m0_training_idx, "MD-W", "MD");
                    pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                    pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
                    pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
                    lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

                    pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
                    pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

                    pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
                    pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");

                    pnlDetailApp.Visible = true;

                    pnlApprove_HR_W.Visible = false;
                    pnlApprove_HR_A.Visible = false;

                    pnlApprove_MD_W.Visible = true;
                    pnlApprove_MD_A.Visible = false;

                    pnlapprove_hr.Visible = false;
                    pnlapprove_md.Visible = true;

                    lbtitle_preview.Text = lbMD_Wait.Text;

                    zcrtl_btnback("MDW");


                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(6);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnMD_App":
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_A":
                ShowListMD_A();
                break;
            case "btnDetail_GvMD_AList":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("MD_A");
                    zShowdataHR_WDetail(_m0_training_idx, "MD-A", "MD");
                    pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                    pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
                    pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
                    lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

                    pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
                    pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

                    pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
                    pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");

                    pnlDetailApp.Visible = true;

                    pnlApprove_HR_W.Visible = false;
                    pnlApprove_HR_A.Visible = false;

                    pnlApprove_MD_W.Visible = false;
                    pnlApprove_MD_A.Visible = true;

                    pnlapprove_hr.Visible = false;
                    pnlapprove_md.Visible = true;

                    lbtitle_preview.Text = lbMD_App.Text;

                    zcrtl_btnback("MDA");

                }
                if (Hddfld_training_plan_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD_A":
                _m0_training_idx = int.Parse(cmdArg);
                pnlSave.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnscheduler":
                zSetMode(8);
                SETFOCUS.Focus();
                break;
            case "btnAdd_GvMonthList":
                AddMonthList();
                break;
            case "btnrptscheduler":
                zSetMode(9);
                SETFOCUS.Focus();

                break;

        }
    }
    #endregion btnCommand

    private void zcrtl_btnback(string _mode)
    {
        btncancelApp_HR_W.Visible = false;
        btncancelApp_HR.Visible = false;
        btncancelApp_MD_W.Visible = false;
        btncancelApp_MD.Visible = false;
        Boolean _Boolean = true;
        if (_mode == "HRW")
        {
            btncancelApp_HR_W.Visible = _Boolean;
        }
        else if (_mode == "HRA")
        {
            btncancelApp_HR.Visible = _Boolean;
        }
        else if (_mode == "MDW")
        {
            btncancelApp_MD_W.Visible = _Boolean;
        }
        else if (_mode == "MDA")
        {
            btncancelApp_MD.Visible = _Boolean;
        }

    }

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvCRUD":
                    //if (FvName.CurrentMode == FormViewMode.ReadOnly) { }
                    //if (FvName.CurrentMode == FormViewMode.Edit) { }
                    //if (FvName.CurrentMode == FormViewMode.Insert) { }
                    break;
            }
        }
        else if (sender is DropDownList)
        {
            var FvName = (DropDownList)sender;
            if (FvName.ID == "ddlu0_course_idx_ref")
            {
                TextBox txttraining_group_name = (TextBox)fvCRUD.FindControl("txttraining_group_name");
                TextBox txttraining_branch_name = (TextBox)fvCRUD.FindControl("txttraining_branch_name");
                txttraining_group_name.Text = "";
                txttraining_branch_name.Text = "";
                if (_func_dmu.zStringToInt(FvName.SelectedValue) > 0)
                {
                    _data_elearning = _func_dmu.zShowDataDsLookup("TRN-COURSE-U0-LOOKUP",
                                            "idx",
                                            _func_dmu.zStringToInt(FvName.SelectedValue)
                                            );
                    if (_data_elearning.trainingLoolup_action != null)
                    {

                        obj_trainingLoolup = _data_elearning.trainingLoolup_action[0];
                        txttraining_group_name.Text = obj_trainingLoolup.training_group_name;
                        txttraining_branch_name.Text = obj_trainingLoolup.training_branch_name;
                    }
                }
            }
        }
        else if (sender is RadioButtonList)
        {
            var FvName = (RadioButtonList)sender;
            if (FvName.ID == "rdllecturer_type")
            {

                setlecturertype(_func_dmu.zStringToInt(FvName.SelectedValue), 0);
            }
        }
    }

    #endregion FvDetail_DataBound

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void FileUploadTrigger(FileUpload _FileUpload)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = _FileUpload.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    private void ClearTraning()
    {
        Select_Page1showdata();
    }


    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            var GvName = (GridView)sender;
            string sGvName = GvName.ID;
            if (sGvName == "GvMonthList_L")
            {
                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = rowSelect.RowIndex;
                DataSet dsContacts = (DataSet)ViewState["vsel_month_L"];
                dsContacts.Tables["dsel_month_L"].Rows[rowIndex].Delete();
                dsContacts.AcceptChanges();
                _func_dmu.zSetGridData(_GvMonthList_L, dsContacts.Tables["dsel_month_L"]);
            }
        }
    }

    private Boolean zSave(int id)
    {
        Boolean _Boolean = false;
        string sMode = _func_dmu.zGetMode(Hddfld_training_plan_no.Value), sDocno = "";
        string m0_prefix = "TP";
        int _iMonth, idx = 0;
        //
        //if (id > 0)
        //{
        //    sMode = "E";
        //}
        _FormView = getFv(sMode);

        _txttraining_plan_no = (TextBox)_FormView.FindControl("txttraining_plan_no");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        _txttraining_plan_date = (TextBox)_FormView.FindControl("txttraining_plan_date");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        _ddlm0_target_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_target_group_idx_ref");
        _txttraining_plan_qty = (TextBox)_FormView.FindControl("txttraining_plan_qty");
        _txttraining_plan_amount = (TextBox)_FormView.FindControl("txttraining_plan_amount");
        _txttraining_plan_model = (TextBox)_FormView.FindControl("txttraining_plan_model");
        _txttraining_plan_budget = (TextBox)_FormView.FindControl("txttraining_plan_budget");
        _txttraining_plan_costperhead = (TextBox)_FormView.FindControl("txttraining_plan_costperhead");
        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");
        _ddltraining_plan_status = (DropDownList)_FormView.FindControl("ddltraining_plan_status");
        _cb_MTT = (CheckBox)_FormView.FindControl("cb_MTT");
        _cb_NPW = (CheckBox)_FormView.FindControl("cb_NPW");
        _cb_RJN = (CheckBox)_FormView.FindControl("cb_RJN");

        if (sMode == "I")
        {
          //  sDocno = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYMM", "N", "0000");
        }
        else
        {
            sDocno = _txttraining_plan_no.Text;
        }
        training_plan obj_training_plan = new training_plan();
        _data_elearning.el_training_plan_action = new training_plan[1];
        obj_training_plan.u0_training_plan_idx = id;
       // obj_training_plan.training_plan_no = sDocno;
        obj_training_plan.training_plan_date = _func_dmu.zDateToDB(_txttraining_plan_date.Text);
        obj_training_plan.u0_course_idx_ref = _func_dmu.zStringToInt(_ddlu0_course_idx_ref.SelectedValue);
        obj_training_plan.training_plan_year = _func_dmu.zStringToInt(_txttraining_plan_year.Text);
        obj_training_plan.lecturer_type = _func_dmu.zStringToInt(_rdllecturer_type.SelectedValue);
        obj_training_plan.m0_institution_idx_ref = _func_dmu.zStringToInt(_ddlm0_institution_idx_ref.SelectedValue);
        obj_training_plan.m0_target_group_idx_ref = _func_dmu.zStringToInt(_ddlm0_target_group_idx_ref.SelectedValue);
        obj_training_plan.training_plan_qty = _func_dmu.zStringToInt(_txttraining_plan_qty.Text);
        obj_training_plan.training_plan_amount = _func_dmu.zStringToDecimal(_txttraining_plan_amount.Text);
        obj_training_plan.training_plan_model = _func_dmu.zStringToInt(_txttraining_plan_model.Text);
        obj_training_plan.training_plan_budget = _func_dmu.zStringToDecimal(_txttraining_plan_budget.Text);
        obj_training_plan.training_plan_status = int.Parse(_ddltraining_plan_status.SelectedValue);
        obj_training_plan.training_plan_mtt = _func_dmu.zBooleanToInt(_cb_MTT.Checked);
        obj_training_plan.training_plan_npw = _func_dmu.zBooleanToInt(_cb_NPW.Checked);
        obj_training_plan.training_plan_rjn = _func_dmu.zBooleanToInt(_cb_RJN.Checked);

        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _iMonth = 0;
        //for (int i = 1; i <= 12; i++)
        //{
        //    _iMonth = _func_dmu.zStringToInt(((TextBox)_GvMonthList_L.Rows[0].FindControl("txtm" + i.ToString() + "_item_L")).Text);

        //}
        obj_training_plan.training_plan_created_by = emp_idx;
        obj_training_plan.training_plan_updated_by = emp_idx;
        obj_training_plan.operation_status_id = "U0";

        //node
        obj_training_plan.approve_status = 0;
        obj_training_plan.u0_idx = 11;
        obj_training_plan.node_idx = 13;
        obj_training_plan.actor_idx = 1;
        obj_training_plan.app_flag = 0;
        obj_training_plan.app_user = emp_idx;

        _data_elearning.el_training_plan_action[0] = obj_training_plan;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        //_func_dmu.zObjectToXml(_data_elearning);
        if (sMode == "I")
        {
            _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan, _data_elearning);

        }
        else
        {
            _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan, _data_elearning);
        }

        idx = _data_elearning.return_code;
        if (id > 0)
        {
            idx = id;
        }
        if (idx > 0)
        {
            int _iYear = 0, itemObj = 0;
            data_elearning dataelearning = new data_elearning();
            training_plan[] obj_tr_plan_u1 = new training_plan[_GvMonthList_L.Rows.Count];

            _iMonth = 0;
            for (int j = 0; j < _GvMonthList_L.Rows.Count; j++)
            {
                _iYear = _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text);
                obj_tr_plan_u1[itemObj] = new training_plan();
                obj_tr_plan_u1[itemObj].u0_training_plan_idx_ref = idx;
                for (int i = 1; i <= 12; i++)
                {
                    _iMonth = _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtm" + i.ToString() + "_item_L")).Text);
                    if (i == 1)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m1 = _iMonth;
                    }
                    else if (i == 2)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m2 = _iMonth;
                    }
                    else if (i == 3)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m3 = _iMonth;
                    }
                    else if (i == 4)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m4 = _iMonth;
                    }
                    else if (i == 5)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m5 = _iMonth;
                    }
                    else if (i == 6)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m6 = _iMonth;
                    }
                    else if (i == 7)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m7 = _iMonth;
                    }
                    else if (i == 8)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m8 = _iMonth;
                    }
                    else if (i == 9)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m9 = _iMonth;
                    }
                    else if (i == 10)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m10 = _iMonth;
                    }
                    else if (i == 11)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m11 = _iMonth;
                    }
                    else if (i == 12)
                    {
                        obj_tr_plan_u1[itemObj].training_plan_m12 = _iMonth;
                    }
                    obj_tr_plan_u1[itemObj].training_plan_year = _iYear;
                    obj_tr_plan_u1[itemObj].training_plan_status = 1;
                    obj_tr_plan_u1[itemObj].training_plan_updated_by = emp_idx;
                    obj_tr_plan_u1[itemObj].operation_status_id = "U1";

                }
                itemObj++;
            }
            if (itemObj == 0)
            {
                obj_tr_plan_u1 = new training_plan[1];
                obj_tr_plan_u1[itemObj] = new training_plan();
                obj_tr_plan_u1[itemObj].u0_training_plan_idx_ref = idx;
                obj_tr_plan_u1[itemObj].operation_status_id = "U1-DEL";
            }

            dataelearning.el_training_plan_action = obj_tr_plan_u1;
            _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan, dataelearning);
            if (_ddltraining_plan_status.SelectedValue == "1")
            {
                sendEmailhrtohrd(idx);
            }


        }

        _Boolean = true;
        return _Boolean;
    }

    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void zShowdataUpdate(int id, string _Mode)
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.u0_training_plan_idx = id;
        obj.operation_status_id = "U0-FULL";
        dataelearning.el_training_plan_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetFormViewData(fv_Update, dataelearning.el_training_plan_action);

        string sDocno = "";
        string m0_prefix = "";
        int _iMonth;

        _FormView = getFv(_Mode);

        _txttraining_plan_no = (TextBox)_FormView.FindControl("txttraining_plan_no");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        _txttraining_plan_date = (TextBox)_FormView.FindControl("txttraining_plan_date");
        _txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        _ddlm0_target_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_target_group_idx_ref");
        _txttraining_plan_qty = (TextBox)_FormView.FindControl("txttraining_plan_qty");
        _txttraining_plan_amount = (TextBox)_FormView.FindControl("txttraining_plan_amount");
        _txttraining_plan_model = (TextBox)_FormView.FindControl("txttraining_plan_model");
        _txttraining_plan_budget = (TextBox)_FormView.FindControl("txttraining_plan_budget");
        _txttraining_plan_costperhead = (TextBox)_FormView.FindControl("txttraining_plan_costperhead");
        _GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");
        _ddltraining_plan_status = (DropDownList)_FormView.FindControl("ddltraining_plan_status");
        _cb_MTT = (CheckBox)_FormView.FindControl("cb_MTT");
        _cb_NPW = (CheckBox)_FormView.FindControl("cb_NPW");
        _cb_RJN = (CheckBox)_FormView.FindControl("cb_RJN");
        _pnlhistory = (Panel)_FormView.FindControl("pnlhistory");

        //Start SetMode

        _func_dmu.zModeRadioButtonList(_rdllecturer_type, _Mode);
        _func_dmu.zModeDropDownList(_ddlm0_institution_idx_ref, _Mode);
        _func_dmu.zModeTextBox(_txttraining_plan_qty, _Mode);
        _func_dmu.zModeTextBox(_txttraining_plan_model, _Mode);
        _func_dmu.zModeTextBox(_txttraining_plan_budget, _Mode);
        _func_dmu.zModeDropDownList(_ddltraining_plan_status, _Mode);
        _func_dmu.zModeDropDownList(_ddlm0_target_group_idx_ref, _Mode);
        _func_dmu.zModeGridView(_GvMonthList, _Mode);
        _func_dmu.zModeGridView(_GvMonthList_L, _Mode);
        _func_dmu.zModeGridViewCol(_GvMonthList, _Mode, 13);
        _func_dmu.zModeGridViewCol(_GvMonthList_L, _Mode, 13);
        _func_dmu.zModeGridViewVisible(_GvMonthList, _Mode);
        //_func_dmu.zModeGridViewVisible(_GvMonthList_L, _Mode);

        _func_dmu.zModeLinkButton(btnSaveUpdate, _Mode);

        _func_dmu.zModeCheckBox(_cb_MTT, _Mode);
        _func_dmu.zModeCheckBox(_cb_NPW, _Mode);
        _func_dmu.zModeCheckBox(_cb_RJN, _Mode);

        _pnlhistory.Visible = false;
        //End SetMode
        Hddfld_training_plan_no.Value = "";
        Hddfld_u0_training_plan_idx.Value = "";
        Hddfld_status.Value = "E";
        if (dataelearning.el_training_plan_action != null)
        {

            foreach (var item in dataelearning.el_training_plan_action)
            {
                Hddfld_training_plan_no.Value = item.training_plan_no;
                Hddfld_u0_training_plan_idx.Value = item.u0_training_plan_idx.ToString();
                _cb_MTT.Checked = _func_dmu.zIntToBoolean(item.training_plan_mtt);
                _cb_NPW.Checked = _func_dmu.zIntToBoolean(item.training_plan_npw);
                _cb_RJN.Checked = _func_dmu.zIntToBoolean(item.training_plan_rjn);

                _func_dmu.zDropDownList(_ddlu0_course_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                item.u0_course_idx_ref.ToString(),
                                "TRN-COURSE-U0-LOOKUP"
                                );
                setlecturertype(_func_dmu.zStringToInt(_rdllecturer_type.SelectedValue), item.m0_institution_idx_ref);
                _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                        "",
                                        "zId",
                                        "zName",
                                        "0",
                                        "trainingLoolup",
                                        item.m0_target_group_idx_ref.ToString(),
                                        "TRN-TARGET"
                                        );


                CreateDs_el_month();
                zShowDefaultMonth();
                DataSet Ds_a = (DataSet)ViewState["vsel_month"];
                DataRow dr_a;
                ViewState["vsel_month"] = Ds_a;
                _func_dmu.zSetGridData(_GvMonthList, Ds_a.Tables["dsel_month"]);


                data_elearning dataelearning_u1 = new data_elearning();
                dataelearning_u1.el_training_plan_action = new training_plan[1];
                training_plan obj_u1 = new training_plan();
                obj_u1.u0_training_plan_idx = id;
                obj_u1.operation_status_id = "U1-FULL";
                dataelearning_u1.el_training_plan_action[0] = obj_u1;
                dataelearning_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning_u1);

                CreateDs_el_monthList();
                zShowDefaultMonthList();
                DataSet Ds = (DataSet)ViewState["vsel_month_L"];
                DataRow dr;
                if (dataelearning_u1.el_training_plan_action != null)
                {
                    foreach (var item1 in dataelearning_u1.el_training_plan_action)
                    {
                        dr = Ds.Tables["dsel_month_L"].NewRow();
                        dr["id"] = "1";
                        dr["training_plan_year"] = _func_dmu.zIntToEmpty(item1.training_plan_year);
                        for (int i = 1; i <= 12; i++)
                        {
                            if (i == 1)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m1);
                            }
                            else if (i == 2)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m2);
                            }
                            else if (i == 3)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m3);
                            }
                            else if (i == 4)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m4);
                            }
                            else if (i == 5)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m5);
                            }
                            else if (i == 6)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m6);
                            }
                            else if (i == 7)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m7);
                            }
                            else if (i == 8)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m8);
                            }
                            else if (i == 9)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m9);
                            }
                            else if (i == 10)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m10);
                            }
                            else if (i == 11)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m11);
                            }
                            else if (i == 12)
                            {
                                dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m12);
                            }
                        }
                        Ds.Tables["dsel_month_L"].Rows.Add(dr);

                    }
                }
                ViewState["vsel_month_L"] = Ds;
                _func_dmu.zSetGridData(_GvMonthList_L, Ds.Tables["dsel_month_L"]);
                _txttraining_plan_amount.Text = _func_dmu.zFormatfloat(item.training_plan_amount.ToString(), 2);
               
                if (_Mode == "P")
                {
                    _pnlhistory.Visible = true;
                    _GvHistory = (GridView)_FormView.FindControl("GvHistory");
                    //history
                    data_elearning dataelearning_detail;
                    training_plan obj_detail;
                    dataelearning_detail = new data_elearning();
                    dataelearning_detail.el_training_plan_action = new training_plan[1];
                    obj_detail = new training_plan();
                   // litDebug.Text = id.ToString();
                    obj_detail.u0_training_plan_idx = id;
                    obj_detail.operation_status_id = "L0";
                    dataelearning_detail.el_training_plan_action[0] = obj_detail;
                    dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan, dataelearning_detail);
                    _func_dmu.zSetGridData(_GvHistory, dataelearning_detail.el_training_plan_action);

                }

            }
        }

    }

    private void CreateDs_el_month()
    {
        string sDs = "dsel_month";
        string sVs = "vsel_month";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_year", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m1", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m2", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m3", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m4", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m5", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m6", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m7", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m8", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m9", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m10", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m11", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m12", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_monthList()
    {
        string sDs = "dsel_month_L";
        string sVs = "vsel_month_L";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_year", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m1", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m2", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m3", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m4", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m5", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m6", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m7", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m8", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m9", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m10", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m11", typeof(String));
        ds.Tables[sDs].Columns.Add("training_plan_m12", typeof(String));
        ViewState[sVs] = ds;

    }
    private void zShowDefaultMonth()
    {
        CreateDs_el_month();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        GridView GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
        DataSet Ds = (DataSet)ViewState["vsel_month"];
        DataRow dr;
        dr = Ds.Tables["dsel_month"].NewRow();
        dr["id"] = "1";
        dr["training_plan_year"] = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);
        dr["training_plan_m1"] = "";
        dr["training_plan_m2"] = "";
        dr["training_plan_m3"] = "";
        dr["training_plan_m4"] = "";
        dr["training_plan_m5"] = "";
        dr["training_plan_m6"] = "";
        dr["training_plan_m7"] = "";
        dr["training_plan_m8"] = "";
        dr["training_plan_m9"] = "";
        dr["training_plan_m10"] = "";
        dr["training_plan_m11"] = "";
        dr["training_plan_m12"] = "";
        Ds.Tables["dsel_month"].Rows.Add(dr);
        ViewState["vsel_month"] = Ds;
        _func_dmu.zSetGridData(GvMonthList, Ds.Tables["dsel_month"]);
    }
    private void zShowDefaultMonthList()
    {
        CreateDs_el_month();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        GridView GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        DataSet Ds = (DataSet)ViewState["vsel_month_L"];
        DataRow dr;
        _func_dmu.zSetGridData(GvMonthList_L, Ds.Tables["dsel_month_L"]);
    }
    private void setlecturertype(int _type, int _id)
    {

        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");

        if (_type == 0) //ภายใน
        {
            _func_dmu.zDropDownList(_ddlm0_institution_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    _id.ToString(),
                                    "EMPLOYEE"
                                    );
        }
        else
        {
            _func_dmu.zDropDownList(_ddlm0_institution_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    _id.ToString(),
                                    "INSTITUTION"
                                    );
        }
    }

    private Boolean checkError()
    {
        Boolean _Boolean = false;
        string _sError = " ไม่ถูกต้อง";
        int _iMonth = 0, _iYear = 0;
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));

        //_txttraining_plan_no = (TextBox)_FormView.FindControl("txttraining_plan_no");
        //_txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        //_txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        //_ddlu0_course_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_course_idx_ref");
        //_txttraining_plan_date = (TextBox)_FormView.FindControl("txttraining_plan_date");
        //_txttraining_plan_year = (TextBox)_FormView.FindControl("txttraining_plan_year");
        //_ddlm0_target_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_target_group_idx_ref");
        _txttraining_plan_qty = (TextBox)_FormView.FindControl("txttraining_plan_qty");
        _txttraining_plan_amount = (TextBox)_FormView.FindControl("txttraining_plan_amount");
        _txttraining_plan_model = (TextBox)_FormView.FindControl("txttraining_plan_model");
        _txttraining_plan_budget = (TextBox)_FormView.FindControl("txttraining_plan_budget");
        _txttraining_plan_costperhead = (TextBox)_FormView.FindControl("txttraining_plan_costperhead");
        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");

        _iMonth = 0;
        for (int j = 0; j < _GvMonthList_L.Rows.Count; j++)
        {
            for (int i = 1; i <= 12; i++)
            {
                _iMonth = _iMonth + _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtm" + i.ToString() + "_item_L")).Text);
            }
            _iYear = _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text);
            if (_iYear.ToString().Length < 4)
            {
                _iYear = 0;
            }
        }

        if (_func_dmu.zCheckNumber(_txttraining_plan_qty.Text) == false)
        {

            showAlert("จำนวนคนต่อรุ่น" + _sError);
            return true;
        }
        else if (_func_dmu.zCheckAmount(_txttraining_plan_budget.Text) == false)
        {

            showAlert("งบประมาณ" + _sError);
            return true;
        }
        else if (_func_dmu.zCheckNumber(_txttraining_plan_model.Text) == false)
        {

            showAlert("จำนวนรุ่น" + _sError);
            return true;
        }

        else if (_iYear <= 0)
        {
            showAlert("ปีแผนการอบรม" + _sError);
            return true;
        }
        else if (_func_dmu.zStringToInt(_txttraining_plan_model.Text) != _iMonth)
        {

            showAlert("แผนการอบรม" + _sError);
            return true;
        }
        else
        {
            caculate();
            return _Boolean;
        }

    }
    private FormView getFv(string _sMode)
    {
        if (_sMode == "I")
        {
            return fvCRUD;
        }
        else
        {
            return fv_Update;
        }
    }
    private void caculate()
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        _txttraining_plan_qty = (TextBox)_FormView.FindControl("txttraining_plan_qty");
        _txttraining_plan_amount = (TextBox)_FormView.FindControl("txttraining_plan_amount");
        _txttraining_plan_model = (TextBox)_FormView.FindControl("txttraining_plan_model");
        _txttraining_plan_budget = (TextBox)_FormView.FindControl("txttraining_plan_budget");

        int iplan_qty = 0, iplan_model = 0;
        decimal dplan_amount = 0, dplan_budget = 0;

        iplan_qty = _func_dmu.zStringToInt(_txttraining_plan_qty.Text);
        iplan_model = _func_dmu.zStringToInt(_txttraining_plan_model.Text);
        dplan_amount = _func_dmu.zStringToDecimal(_txttraining_plan_budget.Text);
        if (dplan_amount > 0)
        {
            dplan_budget = dplan_amount / (iplan_qty * iplan_model);
        }
        else
        {
            dplan_budget = 0;
        }

        dplan_budget = Math.Round(dplan_budget, 2);
        dplan_budget = _func_dmu.zStringToDecimal(dplan_budget.ToString());
        _txttraining_plan_amount.Text = _func_dmu.zFormatfloat(dplan_budget.ToString(), 2);


    }
    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            if (textbox.ID == "txttraining_plan_qty")
            {
                caculate();
            }
            else if (textbox.ID == "txttraining_plan_amount")
            {
                caculate();
            }
            else if (textbox.ID == "txttraining_plan_model")
            {
                caculate();
            }
            else if (textbox.ID == "txttraining_plan_year")
            {
                _ddlu0_course_idx_ref = (DropDownList)fvCRUD.FindControl("ddlu0_course_idx_ref");
                _txttraining_plan_year = (TextBox)fvCRUD.FindControl("txttraining_plan_year");

                _func_dmu.zDropDownListWhereID(_ddlu0_course_idx_ref,
                                                "",
                                                "zId",
                                                "zName",
                                                "0",
                                                "trainingLoolup",
                                                "",
                                                "TRN-COURSE-U0-LOOKUP",
                                                "zyear",
                                                _func_dmu.zStringToInt(_txttraining_plan_year.Text)
                                                );

            }
        }
    }
    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        training_plan obj_training_plan = new training_plan();
        _data_elearning.el_training_plan_action = new training_plan[1];
        obj_training_plan.u0_training_plan_idx = id;
        obj_training_plan.training_plan_updated_by = emp_idx;
        _data_elearning.el_training_plan_action[0] = obj_training_plan;
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u_plan, _data_elearning);
    }
    public string getTextDoc(string _name,
                             int _status
        )
    {
        string text = string.Empty;
        if(_status == 4)
        {
            text = "<span style='font-size:13px;'>" + _name + "</span>"; //color:#26A65B;
        }
        else if (_status == 5)// 5   กลับไปแก้ไข
        {
            text = "<span style='font-size:13px;'>" + _name + "</span>";//color:#F89406;
        }
        else if (_status == 6)
        {
            text = "<span style='font-size:13px;'>" + _name + "</span>";//color:#F03434;
        }
        else
        {
            text = _name;
        }
        return text;
    }
    public string getTextDoc_old(int hr_approve,
                             int md_approve,
                             string hr_decision_name,
                             string hr_node_name,
                             string hr_actor_name,
                             string md_decision_name,
                             string md_node_name,
                             string md_actor_name
        )
    {
        string text = string.Empty;
        int id = 0;

        if ((hr_approve == 4) && (md_approve == 4))
        {
            text = "<span style='color:#26A65B;'>" + md_decision_name + " จบการดำเนินการ โดย " + md_actor_name + "</span>";
        }
        else if ((hr_approve == 4) && (md_approve != 4))
        {
            id = md_approve;
            if (id == 5) // 5   กลับไปแก้ไข
            {
                text = "<span style='color:#F89406;'>" + md_decision_name + " โดย " + md_actor_name + "</span>";
            }
            else if (id == 6)
            {
                text = "<span style='color:#F03434;'>" + md_decision_name + " โดย " + md_actor_name + "</span>";
            }
            else
            {
                text = "รอการอนุมัติจาก MD";
            }
        }
        else
        {
            id = hr_approve;
            if (id == 5) // 5   กลับไปแก้ไข
            {
                text = "<span style='color:#F89406;'>" + hr_decision_name + " โดย " + hr_actor_name + "</span>";
            }
            else if (id == 6)
            {
                text = "<span style='color:#F03434;'>" + hr_decision_name + " โดย " + hr_actor_name + "</span>";
            }
            else
            {
                text = hr_decision_name + " โดย " + hr_actor_name;
            }
        }

        return text;
    }
    private int zShowdataHR_WDetail(int id, string _zstatus, string _flag)
    {
        int _int = 1;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.u0_training_plan_idx = id;
        obj.operation_status_id = "U0-LISTDATA-HR";
        obj.zstatus = _zstatus;// "HR-W";
        dataelearning.el_training_plan_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        _func_dmu.zSetFormViewData(fv_preview, dataelearning.el_training_plan_action);

        //End SetMode
        Hddfld_training_plan_no.Value = "";
        Hddfld_u0_training_plan_idx.Value = "";
        // Hddfld_status.Value = "E";
        if (dataelearning.el_training_plan_action != null)
        {

            foreach (var item in dataelearning.el_training_plan_action)
            {
                Hddfld_training_plan_no.Value = item.training_plan_no;
                Hddfld_u0_training_plan_idx.Value = item.u0_training_plan_idx.ToString();
                Panel pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                if (_flag == "HR")
                {
                    if ((item.md_approve_status == 4) || (item.md_approve_status == 6))
                    {
                        _int = 0;
                    }
                }

            }
        }

        zShowDataPlanYear(_func_dmu.zStringToInt(Hddfld_u0_training_plan_idx.Value), "A");

        return _int;
    }

    public void zShowDataPlanYear(int id, string sMode)
    {



        data_elearning dataelearning_u1 = new data_elearning();
        dataelearning_u1.el_training_plan_action = new training_plan[1];
        training_plan obj_u1 = new training_plan();
        obj_u1.u0_training_plan_idx = id;
        obj_u1.operation_status_id = "U1-FULL";
        dataelearning_u1.el_training_plan_action[0] = obj_u1;
        dataelearning_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning_u1);

        CreateDs_el_monthList();
        // zShowDefaultMonthList();
        DataSet Ds = (DataSet)ViewState["vsel_month_L"];
        DataRow dr;
        if (dataelearning_u1.el_training_plan_action != null)
        {
            foreach (var item1 in dataelearning_u1.el_training_plan_action)
            {
                dr = Ds.Tables["dsel_month_L"].NewRow();
                dr["id"] = "1";
                dr["training_plan_year"] = _func_dmu.zIntToEmpty(item1.training_plan_year);
                for (int i = 1; i <= 12; i++)
                {
                    if (i == 1)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m1);
                    }
                    else if (i == 2)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m2);
                    }
                    else if (i == 3)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m3);
                    }
                    else if (i == 4)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m4);
                    }
                    else if (i == 5)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m5);
                    }
                    else if (i == 6)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m6);
                    }
                    else if (i == 7)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m7);
                    }
                    else if (i == 8)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m8);
                    }
                    else if (i == 9)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m9);
                    }
                    else if (i == 10)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m10);
                    }
                    else if (i == 11)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m11);
                    }
                    else if (i == 12)
                    {
                        dr["training_plan_m" + i.ToString()] = _func_dmu.zIntToEmpty(item1.training_plan_m12);
                    }
                }
                Ds.Tables["dsel_month_L"].Rows.Add(dr);

            }
        }
        ViewState["vsel_month_L"] = Ds;
        if (sMode == "A") //Approve
        {
            _GvMonthList_A = (GridView)fv_preview.FindControl("GvMonthList_A");
        }
        else if (sMode == "E") //Edit
        {
            _GvMonthList_A = (GridView)fv_Update.FindControl("GvMonthList_L");
        }
        _func_dmu.zSetGridData(_GvMonthList_A, Ds.Tables["dsel_month_L"]);

    }

    public string getlecturer_type(int id, string _a, string _b)
    {
        string _string = "";
        if (id == 0)
        {
            _string = "ภายใน";
        }
        else
        {
            _string = "ภายนอก";
        }
        _string = _string + " " + _a + " " + _b;
        return _string;

    }
    private void zSaveApprove(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove");

        training_plan obj_training_plan = new training_plan();
        //traning_req U1
        _data_elearning.el_training_plan_action = new training_plan[1];
        obj_training_plan.training_plan_updated_by = emp_idx;
        obj_training_plan.u0_training_plan_idx = id;
        obj_training_plan.operation_status_id = "APPROVE-HR";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_plan.approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_plan.approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_plan.u0_idx = 12;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_plan.u0_idx = 13;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_plan.u0_idx = 14;
        }
        obj_training_plan.approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_plan.node_idx = 2;
        obj_training_plan.actor_idx = 6;
        obj_training_plan.app_flag = 1;
        obj_training_plan.app_user = emp_idx;
        _data_elearning.el_training_plan_action[0] = obj_training_plan;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan, _data_elearning);

        if (ddlStatusapprove.SelectedValue == "4")
        {
            sendEmailhrdtomd(id);
        }
        sendEmailhrdtohr_all(id);


    }
    private void zSaveApproveMD(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove_md");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove_md");

        training_plan obj_training_plan = new training_plan();
        //traning_req U1
        _data_elearning.el_training_plan_action = new training_plan[1];
        obj_training_plan.training_plan_updated_by = emp_idx;
        obj_training_plan.u0_training_plan_idx = id;
        obj_training_plan.operation_status_id = "APPROVE-MD";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_plan.md_approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_plan.md_approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_plan.md_u0_idx = 15;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_plan.md_u0_idx = 16;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_plan.md_u0_idx = 17;
        }
        obj_training_plan.md_approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_plan.md_node_idx = 2;
        obj_training_plan.md_actor_idx = 4;
        obj_training_plan.md_app_flag = 1;
        obj_training_plan.md_app_user = emp_idx;
        _data_elearning.el_training_plan_action[0] = obj_training_plan;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan, _data_elearning);

        sendEmailmdtohrd_all(id);

    }

    // start scheduler

    public string MonthTH(int AMonth)
    {

        return _func_dmu.zMonthTH(AMonth);
    }

    public string getHtmlSched()
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj_trn_plan = new training_plan();
        obj_trn_plan.filter_keyword = txtFilterKeyword_SCHED.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj_trn_plan.zyear = _func_dmu.zStringToInt(ddlYearSearch_SCHED.SelectedValue);
        obj_trn_plan.u0_course_idx_ref = _func_dmu.zStringToInt(ddltrn_groupSearch_SCHED.SelectedValue);

        // obj.zstatus = "MD-A";
        obj_trn_plan.operation_status_id = "U0-LISTDATA-SCHED";
        dataelearning.el_training_plan_action[0] = obj_trn_plan;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        //_func_dmu.zSetRepeaterData(repMonths, _data_elearning.el_training_plan_action);
        string sName = "", sHtmlAll = "", sHtml = "", sHtml1 = "", sHtml_Body1 = "", sHtml_Body2 = "";

        sHtml = "";
        int icount = 0;
        if (dataelearning.el_training_plan_action != null)
        {
            foreach (var item in dataelearning.el_training_plan_action)
            {
                sHtml_Body1 = "<td style=\"width: 80px;padding: 5px 0px;\"> </td>";
                sHtml1 = "";
                //im_total = 0;
                //ic = 0;
                int im1 = 0, im2 = 0, im3 = 0, im4 = 0, im5 = 0
            , im6 = 0, im7 = 0, im8 = 0
            , im9 = 0, im10 = 0, im11 = 0, im12 = 0
            , im1_data = 0, im2_data = 0, im3_data = 0
            , im4_data = 0, im5_data = 0
            , im6_data = 0, im7_data = 0, im8_data = 0
            , im9_data = 0, im10_data = 0, im11_data = 0
            , im12_data = 0
            , im_total = 0, ic = 0
            ;
                sName = item.course_name;
                if (item.training_plan_m1 > 0)
                {
                    im1 = 1;
                    im1_data = item.training_plan_m1;
                }
                if (item.training_plan_m2 > 0)
                {
                    im2 = 1;
                    im2_data = item.training_plan_m2;
                }
                if (item.training_plan_m3 > 0)
                {
                    im3 = 1;
                    im3_data = item.training_plan_m3;
                }
                if (item.training_plan_m4 > 0)
                {
                    im4 = 1;
                    im4_data = item.training_plan_m4;
                }
                if (item.training_plan_m5 > 0)
                {
                    im5 = 1;
                    im5_data = item.training_plan_m5;
                }
                if (item.training_plan_m6 > 0)
                {
                    im6 = 1;
                    im6_data = item.training_plan_m6;
                }
                if (item.training_plan_m7 > 0)
                {
                    im7 = 1;
                    im7_data = item.training_plan_m7;
                }
                if (item.training_plan_m8 > 0)
                {
                    im8 = 1;
                    im8_data = item.training_plan_m8;
                }
                if (item.training_plan_m9 > 0)
                {
                    im9 = 1;
                    im9_data = item.training_plan_m9;
                }
                if (item.training_plan_m10 > 0)
                {
                    im10 = 1;
                    im10_data = item.training_plan_m10;
                }
                if (item.training_plan_m11 > 0)
                {
                    im11 = 1;
                    im11_data = item.training_plan_m11;
                }
                if (item.training_plan_m12 > 0)
                {
                    im12 = 1;
                    im12_data = item.training_plan_m12;
                }
                if (im1 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im2 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im3 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im4 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im5 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im6 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im7 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im8 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im9 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im10 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im11 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im12 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im_total > 0)
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                }
                sHtml1 = "<tr>" + sHtml1 + "</tr>";
                sHtml = sHtml + sHtml1;
            }
        }

        return sHtml;
    }
    public string getHtml(int icolspan = 0, string sdata = "", int id = 0, string sColor = "")
    {
        string sName = sdata;
        if (sdata.Length > 8)
        {
            sdata = _func_dmu.zTruncate(sdata, icolspan * 8);
            // sdata = sdata + "...";
        }
        int iwidth = 90 * icolspan;
        //if (icolspan == 3)
        //{
        //    iwidth = 93 * icolspan;
        //}
        if (icolspan >= 4)
        {
            iwidth = 93 * icolspan;
            // iwidth = iwidth + (1 * (icolspan - 1));
        }

        //width: 80px;  width: " + iwidth.ToString() + "px;
        string sHtml_Body = " <td  style=\"padding: 5px 0px;\" colspan=" + icolspan.ToString() + "> " +
                          " <center>" +
            " <input id=\"btn_sched" + id.ToString() + "\" type=\"button\" value=\"" + sdata + "\" class=\"pull-left\" style=\"background-color: " + sColor + ";" +
        " border: none;" +
   "      color: white;" +
    "     text-align: center;" +
    "     text-decoration: none;" +
     "    display: inline-block;" +
     "      padding: 5px 5px;" +
      "   font-size: 14px;" +
    "  width: 100%;   " +
     "    cursor: pointer;\" " +
     " data-toggle=\"tooltip\" title=\"" + sName + "\" " +
     "   onclick=\"ShowCurrentTime1(" + id.ToString() + ")\"  " +
     " />" +

      "</center>" +
                             " </td>";



        //"      padding: 5px 5px;" +
        //"     width: auto;" +
        // " onclick = \"ShowCurrentTime()\" " +
        if (icolspan == 0)
        {
            sHtml_Body = "";
        }
        return sHtml_Body;
    }


    [WebMethod]
    public static string GetCurrentTime(string name)
    {
        // string name = "";
        string sStr = "";
        using (SqlConnection conn = new SqlConnection())
        {

            conn.ConnectionString = ConfigurationManager
                    .ConnectionStrings["conn_mas"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder query = new StringBuilder();
                query.Clear();
                query.AppendLine("SELECT u0_training_plan_idx ");
                query.AppendLine(", training_plan_no ");
                query.AppendLine(", CONVERT(VARCHAR(30), training_plan_date, 103) as training_plan_date ");
                query.AppendLine(", training_plan_year ");
                query.AppendLine(", u0_course_idx_ref ");
                query.AppendLine(", lecturer_type ");
                query.AppendLine(", m0_institution_idx_ref ");
                query.AppendLine(", m0_target_group_idx_ref ");
                query.AppendLine(", training_plan_qty ");
                query.AppendLine(", training_plan_amount ");
                query.AppendLine(", training_plan_model ");
                query.AppendLine(", training_plan_m1 ");
                query.AppendLine(", training_plan_m2 ");
                query.AppendLine(", training_plan_m3 ");
                query.AppendLine(", training_plan_m4 ");
                query.AppendLine(", training_plan_m5 ");
                query.AppendLine(", training_plan_m6 ");
                query.AppendLine(", training_plan_m7 ");
                query.AppendLine(", training_plan_m8 ");
                query.AppendLine(", training_plan_m9 ");
                query.AppendLine(", training_plan_m10 ");
                query.AppendLine(", training_plan_m11 ");
                query.AppendLine(", training_plan_m12 ");
                query.AppendLine(", training_plan_budget ");
                query.AppendLine(", training_plan_costperhead ");
                query.AppendLine(", training_plan_status ");
                query.AppendLine(", training_plan_created_by ");
                query.AppendLine(", training_plan_created_at ");
                query.AppendLine(", training_plan_updated_by ");
                query.AppendLine(", training_plan_updated_at ");
                query.AppendLine(", isnull(a.priority_name, '') + '  ' + isnull(a.course_name, '') course_name ");
                query.AppendLine(", training_group_name ");
                query.AppendLine(", training_branch_name ");
                query.AppendLine(", priority_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(", approve_status ");
                query.AppendLine(", u0_idx ");
                query.AppendLine(", node_idx ");
                query.AppendLine(", actor_idx ");
                query.AppendLine(", approve_remark ");
                query.AppendLine(", decision_name ");
                query.AppendLine(", node_name ");
                query.AppendLine(", actor_name ");
                query.AppendLine(", md_approve_status ");
                query.AppendLine(", md_approve_remark ");
                query.AppendLine(", md_u0_idx ");
                query.AppendLine(", md_node_idx ");
                query.AppendLine(", md_actor_idx ");
                query.AppendLine(", md_decision_name ");
                query.AppendLine(", md_node_name ");
                query.AppendLine(", md_actor_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(",case when lecturer_type = 0 then ");
                query.AppendLine("(select  top 1 emp.FullNameTH from Centralized.dbo.ViewEmployee emp ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and emp.EmpStatus = 1 ");
                query.AppendLine("and emp.EmpIDX = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("else  ");
                query.AppendLine("(select  top 1 m0_t.institution_name from el_m0_institution m0_t ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and m0_t.institution_status = 1 ");
                query.AppendLine("and m0_t.m0_institution_idx = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("end institution_name ");
                query.AppendLine("FROM View_el_u0_training_plan a");
                query.AppendLine(" where 1=1 ");
                query.AppendLine(" and u0_training_plan_idx = " + name);

                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {

                        //customers.Add(string.Format("{0}-{1}", sdr["ContactName"], sdr["CustomerId"]));
                        sStr = sStr + "เลขที่เอกสาร : " + sdr["training_plan_no"].ToString() +
                               Environment.NewLine +
                               " วันที่สร้างเอกสาร : " + sdr["training_plan_date"].ToString() +
                               Environment.NewLine +
                               " ชื่อหลักสูตร : " + sdr["course_name"].ToString() +
                               Environment.NewLine +
                               " กลุ่มวิชา : " + sdr["training_group_name"].ToString() +
                               Environment.NewLine +
                               " สาขาวิชา : " + sdr["training_branch_name"].ToString() +
                               Environment.NewLine +
                               " วิทยากร : " + sdr["institution_name"].ToString();
                        //Environment.NewLine +
                        //" กลุ่มเป้าหมาย : " + sdr["target_group_name"].ToString();
                        //Environment.NewLine +
                        //" จำนวนคนต่อรุ่น : " + sdr["training_plan_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_plan_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_plan_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_plan_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_plan_date"].ToString() +
                    }
                }
                conn.Close();
            }
            // return customers.ToArray();
        }
        return sStr;


        //  return "Hello " + name + Environment.NewLine + "The Current Time is: <br/>"
        // + DateTime.Now.ToString();
        //zShowdataDetail_sched(_func_dmu.zStringToInt(name), "HR-A", "HR");
        //return "Hello " + name;

        //fv_detail_sched.DataSource = dataelearning;

        //dataelearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);


        //dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        //_func_dmu.zSetFormViewData(fv_detail_sched, dataelearning.el_training_plan_action);


    }
    [WebMethod]
    public static string CodebehindMethodName()
    {
        // string name = "";
        return "Hello 9000 " + Environment.NewLine + "The Current Time is: "
             + DateTime.Now.ToString();
    }

    private void zShowdataDetail_sched(int id, string _zstatus, string _flag)
    {
        int _int = 1;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.u0_training_plan_idx = id;
        obj.operation_status_id = "U0-LISTDATA-HR";
        obj.zstatus = _zstatus;// "HR-W";
        dataelearning.el_training_plan_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        //  _func_dmu.zSetFormViewData(fv_detail_sched, dataelearning.el_training_plan_action);


    }

    // end scheduler

    protected void AddMonthList()
    {
        string _sError = " ไม่ถูกต้อง";
        int iError = 0;
        int _iMonth = 0, _iMonth_L = 0, _iYear = 0, _iYear_L = 0;
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_plan_no.Value));
        _txttraining_plan_model = (TextBox)_FormView.FindControl("txttraining_plan_model");
        //* start check error *//

        _GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
        _iYear = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtyear_item")).Text);
        _iMonth = 0;
        for (int i = 1; i <= 12; i++)
        {
            _iMonth = _iMonth + _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtm" + i.ToString() + "_item")).Text);
        }

        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _iMonth_L = 0;
        for (int j = 0; j < _GvMonthList_L.Rows.Count; j++)
        {
            for (int i = 1; i <= 12; i++)
            {
                _iMonth_L = _iMonth_L + _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtm" + i.ToString() + "_item_L")).Text);
            }
            if (_iYear == _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text))
            {
                _iYear_L = _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text);
            }
        }

        if (_iYear <= 0)
        {
            showAlert("ปีแผนการอบรม" + _sError);
            iError++;
        }
        else if (_iYear == _iYear_L)
        {
            showAlert("ปีแผนการอบรมนี้มีแล้ว");
            iError++;
        }
        else if (_iMonth <= 0)
        {

            showAlert("แผนการอบรม" + _sError);
            iError++;
        }
        else if (_func_dmu.zStringToInt(_txttraining_plan_model.Text) < (_iMonth))
        {

            showAlert("แผนการอบรม" + _sError);
            iError++;
        }
        else if (_func_dmu.zStringToInt(_txttraining_plan_model.Text) < (_iMonth + _iMonth_L))
        {

            showAlert("จำนวนรุ่น" + _sError);
            iError++;
        }

        //* start check error *//
        if (iError == 0)
        {

            _GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
            _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");

            DataSet Ds = (DataSet)ViewState["vsel_month_L"];
            DataRow dr;
            dr = Ds.Tables["dsel_month_L"].NewRow();
            dr["id"] = Ds.Tables["dsel_month_L"].Rows.Count + 1;
            _iMonth = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtyear_item")).Text);
            dr["training_plan_year"] = _iMonth.ToString();
            for (int i = 1; i <= 12; i++)
            {
                _iMonth = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtm" + i.ToString() + "_item")).Text);
                if (i == 1)
                {
                    dr["training_plan_m1"] = _iMonth.ToString();
                }
                else if (i == 2)
                {
                    dr["training_plan_m2"] = _iMonth.ToString();
                }
                else if (i == 3)
                {
                    dr["training_plan_m3"] = _iMonth.ToString();
                }
                else if (i == 4)
                {
                    dr["training_plan_m4"] = _iMonth.ToString();
                }
                else if (i == 5)
                {
                    dr["training_plan_m5"] = _iMonth.ToString();
                }
                else if (i == 6)
                {
                    dr["training_plan_m6"] = _iMonth.ToString();
                }
                else if (i == 7)
                {
                    dr["training_plan_m7"] = _iMonth.ToString();
                }
                else if (i == 8)
                {
                    dr["training_plan_m8"] = _iMonth.ToString();
                }
                else if (i == 9)
                {
                    dr["training_plan_m9"] = _iMonth.ToString();
                }
                else if (i == 10)
                {
                    dr["training_plan_m10"] = _iMonth.ToString();
                }
                else if (i == 11)
                {
                    dr["training_plan_m11"] = _iMonth.ToString();
                }
                else if (i == 12)
                {
                    dr["training_plan_m12"] = _iMonth.ToString();
                }
            }
            Ds.Tables["dsel_month_L"].Rows.Add(dr);
            ViewState["vsel_month_L"] = Ds;
            _func_dmu.zSetGridData(_GvMonthList_L, Ds.Tables["dsel_month_L"]);
            zShowDefaultMonth();
        }

    }
    public string getStrformate(string str)
    {
        str = _func_dmu.zFormatfloat(str, 0);
        if (_func_dmu.zStringToInt(str) == 0)
        {
            str = string.Empty;
        }
        return str;
    }
    private void sendEmailhrtohrd(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HR-TO-HRD";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_Plan_hrtohrd, _data_elearning);
        }
        catch { }


    }
    private void sendEmailhrdtomd(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HRD-TO-MD";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_Plan_hrdtomd, _data_elearning);
        }
        catch { }
    }

    private void sendEmailhrdtohr_all(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HRD-TO-HR-ALL";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_Plan_hrdtohr_all, _data_elearning);
        }
        catch { }
    }

    private void sendEmailmdtohrd_all(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-MD-TO-HRD-ALL";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_Plan_mdtohrd_all, _data_elearning);
        }
        catch { }
    }

    //Strat Report
    public string getHtml_rpt_Sched_H()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_report_plan_action = new training_rpt_plan[1];
        training_rpt_plan obj_trn_plan = new training_rpt_plan();
        obj_trn_plan.filter_keyword = txtFilterKeyword_rpt_SCHED.Text;
        obj_trn_plan.zyear = _func_dmu.zStringToInt(ddlYearSearch_rpt_SCHED.SelectedValue);
        obj_trn_plan.idx1 = _func_dmu.zStringToInt(ddltrn_groupSearch_rpt_SCHED.SelectedValue);

        obj_trn_plan.operation_status_id = "PLAN-YEAR-GRP";
        dataelearning.el_report_plan_action[0] = obj_trn_plan;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_Report_Plan, dataelearning);
        string sName = "", sHtml = "", sHtml1 = "", sHtml_Body1 = "", sHtml_Body2 = "";

        sHtml = "";
        sHtml1 = "";
        //20
        //sHtml_Body2 = " <td></td> <td></td> <td></td> <td></td> <td></td> " +
        //              " <td></td> <td></td> <td></td> <td></td> <td></td> " +
        //              " <td></td> <td></td> <td></td> <td></td> <td></td> " +
        //              " <td></td> <td></td> <td></td> <td></td> <td></td> "; 

        sHtml_Body2 = " <td></td> <td></td>  ";

        int ic = 0;
        if (dataelearning.el_report_plan_action != null)
        {
            foreach (var item in dataelearning.el_report_plan_action)
            {
                ic++;
                sName = ic.ToString() + ". " + item.zName;
                sHtml1 = "<td colspan=\"20\" style=\" color: white; font-weight: bold; \"  > " + sName + " </td>";
                //Budget
                sName = _func_dmu.zFormatfloat(item.zbudget.ToString(), 2);
                sHtml1 = sHtml1 + "<td style=\" color: white; text-align: right; \"> " + sName + " </td>";
                //Cost Per Head
                sName = _func_dmu.zFormatfloat(item.zamount.ToString(), 2);
                sHtml1 = sHtml1 + "<td style=\" color: white; text-align: right; \"> " + sName + " </td>";

                sHtml1 = "<tr style=\"background-color: #808080; \" >" + sHtml1 + "</tr>" +
                         getHtml_rpt_Sched_D(item.zId, ic);



                sHtml = sHtml + sHtml1;


            }
        }

        return sHtml;
    }

    public string getHtml_rpt_Sched_D(int id, int zitem)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_report_plan_action = new training_rpt_plan[1];
        training_rpt_plan obj_trn_plan = new training_rpt_plan();
        obj_trn_plan.filter_keyword = txtFilterKeyword_rpt_SCHED.Text;
        obj_trn_plan.zyear = _func_dmu.zStringToInt(ddlYearSearch_rpt_SCHED.SelectedValue);
        obj_trn_plan.idx1 = id;

        obj_trn_plan.operation_status_id = "PLAN-YEAR-DETAIL";
        dataelearning.el_report_plan_action[0] = obj_trn_plan;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_Report_Plan, dataelearning);

        string sName = "", sHtml = "", sHtml1 = "";

        sHtml = "";
        int ic = 0;
        if (dataelearning.el_report_plan_action != null)
        {
            foreach (var item in dataelearning.el_report_plan_action)
            {
                ic++;
                sHtml1 = "";
                //หมวด
                sName = item.zbranch_name;
                sHtml1 = sHtml1 + "<td> " + sName + " </td>";
                //หัวข้อหลักสูตร
                sName = item.zName;
                sHtml1 = sHtml1 + "<td> " + sName + " </td>";
                //กลุ่มเป้าหมาย
                sName = item.zmtt;
                sHtml1 = sHtml1 + "<td><center> " + sName + " </center></td>";
                sName = item.znpw;
                sHtml1 = sHtml1 + "<td><center> " + sName + " </center></td>";
                sName = item.zrjn;
                sHtml1 = sHtml1 + "<td><center> " + sName + " </center></td>";
                sName = item.zgroup_name;
                sHtml1 = sHtml1 + "<td> " + sName + " </td>";
                //จำนวนคน
                sName = _func_dmu.zFormatfloat(item.zqty.ToString(), 0);
                sHtml1 = sHtml1 + "<td style=\" text-align: right; \"> " + sName + " </td>";
                //จำนวน
                sName = _func_dmu.zFormatfloat(item.zmodel.ToString(), 0);
                sHtml1 = sHtml1 + "<td style=\" text-align: right; \"> " + sName + " </td>";
                //แผนการอบรม
                sName = getStrformate(item.zm1.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm1) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm2.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm2) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm3.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm3) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm4.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm4) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm5.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm5) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm6.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm6) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm7.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm7) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm8.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm8) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm9.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm9) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm10.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm10) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm11.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm11) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                sName = getStrformate(item.zm12.ToString());
                sHtml1 = sHtml1 + "<td style=\" " + getbackgroundcolor(item.zm12) + " color: white; font-weight: bold; text-align: center; \"> " + sName + " </td>";
                //Budget
                sName = _func_dmu.zFormatfloat(item.zbudget.ToString(), 2);
                sHtml1 = sHtml1 + "<td style=\" background-color: #00b050; color: white; text-align: right; \"> " + sName + " </td>";
                //Cost Per Head
                sName = _func_dmu.zFormatfloat(item.zamount.ToString(), 2);
                sHtml1 = sHtml1 + "<td style=\" background-color: #00b050; color: white; text-align: right; \"> " + sName + " </td>";

                sHtml1 = "<tr>" + sHtml1 + "</tr>";
                sHtml = sHtml + sHtml1;
            }
        }

        return sHtml;
    }
    public string getbackgroundcolor(int id = 0)
    {
        string sStr = "";
        if (id > 0)
        {
            sStr = "background-color: #92d050;";
        }

        return sStr;
    }
    //end Report


}