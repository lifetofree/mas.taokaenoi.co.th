﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for EventPlanSaleDAO
/// </summary>
public class EventPlanSaleDAO
{
    //change the connection string as per your database connection.
    private static string connectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;

    //this method retrieves all events within range start-end
    //public static List<CalendarEvent> getEvents(DateTime start, DateTime end, int place_idx)
    public static List<CalendarPlanSaleEvent> getEvents(DateTime start, DateTime end, int emp_idx)
    {
        //string sta = "10/09/2018";
        //string end_ = "20/09/2018";

        List<CalendarPlanSaleEvent> events = new List<CalendarPlanSaleEvent>();
        SqlConnection con = new SqlConnection(connectionString);
        /*SqlCommand cmd = new SqlCommand("SELECT a.uidx as ps_u0_idx, CONVERT(datetime, [leave_start],103) as ps_startdate , CONVERT(datetime, leave_end,103) as ps_enddate ,leave_comment as ps_comment,(select lon_shi.TypeWork from [mas].[dbo].[lon_m0_shift_time] as lon_shi WITH(NOLOCK) where a.emp_shift = lon_shi.midx) as TypeWork,emp_idx,m0_node_idx as u0_unidx,m0_actor_idx as u0_acidx,1 as doc_decision, 'ใบลา' as ty_name " + 
            " FROM [mas].[dbo].[lon_u0_document] a where a.leave_status != 9 and (emp_idx = @emp_idx) UNION ALL " +
            " SELECT ps_u0_idx, CONVERT(datetime, ps_startdate,103) as ps_startdate, CONVERT(datetime, ps_enddate,103) as ps_enddate , ps_comment, TypeWork, emp_idx, u0_unidx, u0_acidx, doc_decision, 'ปฏิบัติงาน' as ty_name " + 
            " FROM [mas].[dbo].[view_ps_u0_plansale] where ps_status = 1 and (emp_idx = @emp_idx)", con);*/
        SqlCommand cmd = new SqlCommand("SELECT a.uidx as ps_u0_idx, CONVERT(datetime, [leave_start],103) as ps_startdate , CONVERT(datetime, leave_end,103) as ps_enddate ,leave_comment as ps_comment,(select lon_shi.TypeWork from [mas].[dbo].[lon_m0_shift_time] as lon_shi WITH(NOLOCK) where a.emp_shift = lon_shi.midx) as TypeWork,emp_idx,m0_node_idx as u0_unidx,m0_actor_idx as u0_acidx,leave_status as doc_decision, 'ใบลา' as ty_name " +
        " FROM [mas].[dbo].[lon_u0_document] a where a.leave_status != 9 and (emp_idx = @emp_idx) and m0_node_idx != 3 UNION ALL " +
        " SELECT ps_u0_idx, CONVERT(datetime, ps_startdate,103) as ps_startdate, CONVERT(datetime, ps_enddate,103) as ps_enddate , ps_comment, TypeWork, emp_idx, u0_unidx, u0_acidx, doc_decision, 'ปฏิบัติงาน' as ty_name " +
        " FROM [mas].[dbo].[view_ps_u0_plansale] where ps_status = 1 and (emp_idx = @emp_idx) and doc_decision != 5 and (u0_unidx = 1 or u0_unidx = 2 or u0_unidx = 3) and (u0_acidx = 1 or u0_acidx = 2) and (doc_decision = 1 or doc_decision = 2 or doc_decision = 3)" + "UNION SELECT holiday_idx as ps_u0_idx, holiday_date as ps_startdate, DATEADD(MINUTE,1, holiday_date) as ps_enddate, '' as ps_comment, '' as TypeWork, 0 as emp_idx, 0 as u0_unidx, 0 as u0_acidx, 0 as doc_decision, holiday_name as ty_name FROM[mas].[dbo].[view_emps_holiday] b where(select c.org_idx FROM[Centralized].[dbo].[view_employee] c WITH(NOLOCK) where emp_idx = @emp_idx) = b.org_idx AND(holiday_date >= @start OR holiday_date >= @end) AND u1_manage_status = 1", con);
        cmd.Parameters.Add("@start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@emp_idx", SqlDbType.Int).Value = emp_idx; 

        using (con)
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                events.Add(new CalendarPlanSaleEvent()
                {
                    id = Convert.ToInt32(reader["ps_u0_idx"]),
                    title = Convert.ToString(reader["ty_name"]),
                    description = Convert.ToString(reader["ps_comment"]),
                    start = Convert.ToDateTime(reader["ps_startdate"].ToString()),
                    end = Convert.ToDateTime(reader["ps_enddate"].ToString()),
                    u0_unidx = Convert.ToInt32(reader["u0_unidx"]),
                    u0_acidx = Convert.ToInt32(reader["u0_acidx"]),
                    doc_decision = Convert.ToInt32(reader["doc_decision"]),
                    parttime_name_th = Convert.ToString("กะเข้างาน : " + reader["TypeWork"]),
                    ty_name = Convert.ToString(reader["ty_name"]),

                    //u0_empshift_idx = Convert.ToInt32(reader["u0_empshift_idx"]),
                    //title = Convert.ToString("ชื่อกะ :" + " " + reader["parttime_name_th"]),
                    //parttime_name_th = Convert.ToString(reader["parttime_name_th"]),
                    //start = Convert.ToDateTime(reader["announce_diary_date_start"]),
                    //end = Convert.ToDateTime(reader["announce_diary_date_end"]),

                });
            }
        }
        return events;


        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains an extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }

    //this method updates the event title and description
    public static void updateEvent(int id, String title, String description)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET title=@title, description=@description WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = description;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method updates the event start and end time ... allDay parameter added for FullCalendar 2.x
    public static void updateEventTime(int id, DateTime start, DateTime end, bool allDay)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET event_start=@event_start, event_end=@event_end, all_day=@all_day WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = allDay;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this mehtod deletes event with the id passed in.
    public static void deleteEvent(int id)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("DELETE FROM Event WHERE (event_id = @event_id)", con);
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method adds events to the database
    public static int addEvent(CalendarEvent cevent)
    {
        //add event to the database and return the primary key of the added event row

        //insert
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("INSERT INTO Event(title, description, event_start, event_end, all_day) VALUES(@title, @description, @event_start, @event_end, @all_day)", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

        int key = 0;
        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();

            //get primary key of inserted row
            cmd = new SqlCommand("SELECT max(event_id) FROM Event where title=@title AND description=@description AND event_start=@event_start AND event_end=@event_end AND all_day=@all_day", con);
            cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
            cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
            cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
            cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

            key = (int)cmd.ExecuteScalar();
        }

        return key;
    }
}