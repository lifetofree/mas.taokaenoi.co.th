﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_emps_emps : System.Web.UI.Page
{
   #region Init
   dataODSP_Relation dataODSPReration = new dataODSP_Relation();
   data_emps dataEmps = new data_emps();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string centralizedConn = "conn_centralized";
   string masConn = "conn_mas";
   string empshiftService = "empshiftService";
   string empsParttimeService = "empsParttimeService";
   string empsRewardService = "empsRewardService";
   string empsodspeService = "empsodspeService";
   string empsAnnounceService = "empsAnnounceService";
   string empsChangeAnnounceService = "empsChangeAnnounceService";
   string empsApprovalLogService = "empsApprovalLogService";
   string empsEmployeeService = "empsEmployeeService";
   string empsExportService = "empsExportService";
   string empsGroupDiaryService = "empsGroupDiaryService";
   string empsAnnounceDiaryService = "empsAnnounceDiaryService";
   string empsChangeAnnounceDiaryService = "empsChangeAnnounceDiaryService";
   string _local_xml = String.Empty;
   #endregion Init

   #region Permission for Manage
   //int[] hrAdminPosIdx = { 35, 53, 58, 63, 370, 372, 373, 599, 769, 930, 3044, 3045, 3046, 3047, 3048, 291 };
   //int[] hrHeaderPosIdx = { 135, 150, 290 };
   //int[] hrDirectorPosIdx = { 648, 649, 650, 113, 1019 };
   //int[] pdAdminPosIdx = { 263, 269, 274, 280 };
   //int[] pdHeaderPosIdx = { 261, 267, 272, 278, 861 };
   //int[] pdDirectorPosIdx = { 260, 265, 271 };
   int[] vipEmpIdx = { 4839, 133, 138 };
   int[] vipHeaderEmpIdx = { 1393 };
   int[] vipDirectorEmpIdx = { 1394 };
   int[] misRDeptIdx = { /*20*/ };
   /*
    * 1347 90000001 admin
    * 1393 90000002 header
    * 1394 90000003 director
    */
   #endregion

   #region Constant
   public static class Constants
   {
      public const int ALL = 0;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_ALL = 20;
      public const int SELECT_HR_ADMIN_NODE = 21;
      public const int SELECT_HR_HEADER_NODE = 22;
      public const int SELECT_HR_DIRECTOR_NODE = 23;
      public const int SELECT_ANNOUNCE_FOR_CHANGE = 24;
      public const int SELECT_ANNOUNCE_INDEX = 25;
      public const int SELECT_EMPLOYEE_ANNOUNCE_DIARY = 27;
      public const int SELECT_EMPLOYEE_CHANGE_ANNOUNCE_DIARY = 27;
      public const int SELECT_ANNOUNCE_FOR_EDIT = 28;
      public const int SELECT_WHERE = 21;
      public const int SELECT_LIMIT_FIVE = 22;
      public const int SELECT_WHERE_EMP = 22;
      public const int SELECT_WHERE_SUB = 22;
      public const int SELECT_WHERE_ORG = 22;
      public const int SELECT_WHERE_ORG_DEP = 23;
      public const int SELECT_WHERE_ORG_DEP_SEC = 24;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int NUMBER_NULL = -1;
      public const int U0NODE_ADMIN_CREATED_TO_HEADER_APPROVE = 1;
      public const int U0NODE_HEADER_APPROVE_TO_DIRECTOR_APPROVE = 2;
      public const int U0NODE_HEADER_UNAPPROVE_EDIT_TO_ADMIN_CREATED = 3;
      public const int U0NODE_HEADER_UNAPPROVE_CLOSE_TO_HEADER_FINISH = 4;
      public const int U0NODE_DIRECTOR_APPROVE_TO_DIRECTOR_FINISH = 5;
      public const int U0NODE_DIRECTOR_UNAPPROVE_EDIT_TO_HEADER_EDIT = 6;
      public const int U0NODE_DIRECTOR_UNAPPROVE_CLOSE_TO_DIRECTOR_FINISH = 7;
      public const int TYPEOF_TABLE_U0_ANNOUNCE = 1;
      public const int TYPEOF_TABLE_U0_CHANGE_ANNOUNCE = 2;
      public const int TYPEOF_EMPLOYEE_DIARY = 1;
      public const int TYPEOF_EMPLOYEE_MONTHLY = 2;
      public const int TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE = 1;
      public const int SELECT_TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE = 20;
      public const int SELECT_TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE_DIARY = 30;
      public const int SELECT_GET_EMP_IDX = 27;
      public const int SELECT_COUNT_SCHEDULE_ANNOUNCE = 28;
      public const int TYPEOF_DOCUMENT_CHANGE_ANNOUNCE = 2;
      public const int SELECT_TYPEOF_DOCUMENT_CHANGE_ANNOUNCE_LEFT = 21;
      public const int SELECT_TYPEOF_DOCUMENT_CHANGE_ANNOUNCE_RIGHT = 22;
      public const int SELECT_COUNT_CHANGE_ANNOUNCE = 29;
      public const int TYPEOF_DOCUMENT_SWAP_HOLIDAY = 3;
      public const int STATUSOF_DOCUMENT_APPROVE = 1;
      public const int STATUSOF_DOCUMENT_NOT_APPROVE_EDIT = 2;
      public const int STATUSOF_DOCUMENT_NOT_APPROVE_CLOSED = 3;
      public const int STATUSOF_DOCUMENT_WAITING = 4;
      public const string ALL_NODE_DIARY = "1,2,3,4";
      public const string APPROVE_NODE_DIARY = "2";
      public const string EDIT_NODE_DIARY = "3";
      public const string CLOSED_NODE_DIARY = "4";
      public const string WAITING_NODE_DIARY = "1";
      public const string ALL_NODE = "2,3,4,5,6,7";
      public const string APPROVE_NODE = "2,5";
      public const string EDIT_NODE = "3,6";
      public const string CLOSED_NODE = "4,7";
      public const string WAITING_NODE = "2,3";
      public const string DATA_RETURN_IS_NOT_NULL = "0";
      public const string TEXT_APPROVE = "อนุมัติ";
      public const string TEXT_NOT_APPROVE_EDIT = "ไม่อนุมัติ (แก้ไข)";
      public const string TEXT_NOT_APPROVE_CLOSED = "ไม่อนุมัติ (ปิดเอกสาร)";
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         getEmpLogin(int.Parse(getSessionEmpIdx()));
         visibleIndex();
         getDDLOrgToDeptToSecToPosToEmp(ddlIndexOrgSearch, ddlIndexDeptSearch);
      }
      txtEmpIdxLogin.Value = ViewState["EmpIDX"].ToString();
      visibleMenu();
      generateTableCalendar(2016, 10);
   }
   #endregion Page Load

   #region Action
   protected void actionIndexEmpShiftList()
   {
      if (int.Parse(ddlIndexTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
      {
         if (int.Parse(ddlIndexTypesDocSearch.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            _divIndexAnnounceDiaryList.Visible = true;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            announce_diary objAnnounceDiary = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounceDiary.organization_idx = int.Parse(ddlIndexOrgSearch.SelectedValue);
            objAnnounceDiary.department_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
            dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_ANNOUNCE_INDEX);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvEmpshiftDiaryIndexList, dataEmps.emps_announce_diary_action);
         }
         else if (int.Parse(ddlIndexTypesDocSearch.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = true;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            change_announce_diary objChangeAnnounceDiary = new change_announce_diary();
            dataEmps.emps_change_announce_diary_action = new change_announce_diary[1];
            objChangeAnnounceDiary.organization_idx = int.Parse(ddlIndexOrgSearch.SelectedValue);
            objChangeAnnounceDiary.department_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
            dataEmps.emps_change_announce_diary_action[0] = objChangeAnnounceDiary;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceDiaryService, dataEmps, Constants.SELECT_ANNOUNCE_INDEX);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvChangeEmpshiftDiaryIndexList, dataEmps.emps_change_announce_diary_action);
         }
      }
      else if (int.Parse(ddlIndexTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
      {
         if (int.Parse(ddlIndexTypesDocSearch.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = true;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            announce objAnnounce = new announce();
            dataEmps.emps_announce_action = new announce[1];
            objAnnounce.organization_idx = int.Parse(ddlIndexOrgSearch.SelectedValue);
            objAnnounce.department_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
            dataEmps.emps_announce_action[0] = objAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, Constants.SELECT_ANNOUNCE_INDEX);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvEmpshiftIndexList, dataEmps.emps_announce_action);
         }
         else if (int.Parse(ddlIndexTypesDocSearch.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = true;
            change_announce objChangeAnnounce = new change_announce();
            dataEmps.emps_change_announce_action = new change_announce[1];
            objChangeAnnounce.organization_idx = int.Parse(ddlIndexOrgSearch.SelectedValue);
            objChangeAnnounce.department_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
            dataEmps.emps_change_announce_action[0] = objChangeAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceService, dataEmps, Constants.SELECT_ANNOUNCE_INDEX);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvChangeEmpshiftIndexList, dataEmps.emps_change_announce_action);
         }
      }
   }

   protected void actionChangeAnnounceEmpShift()
   {
      if (int.Parse(ddlChangeAnnounceTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
      {
         announce_diary objAnnounceDiary = new announce_diary();
         dataEmps.emps_announce_diary_action = new announce_diary[1];
         objAnnounceDiary.m0_group_diary_idx_ref = int.Parse(ddlChangeAnnounceGroupDiarySearch.SelectedValue);
         dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
         _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_ANNOUNCE_FOR_CHANGE);
         dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
         setGridData(gvEmpshiftChangeAnnounceDiaryList, dataEmps.emps_announce_diary_action);
         _divChangeAnnounceDiaryList.Visible = true;
         _divChangeAnnounceMonthlyList.Visible = false;
      }
      else if (int.Parse(ddlChangeAnnounceTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
      {
         announce objAnnounce = new announce();
         dataEmps.emps_announce_action = new announce[1];
         objAnnounce.organization_idx = int.Parse(ddlChangeAnnounceOrgSearch.SelectedValue);
         objAnnounce.department_idx = int.Parse(ddlChangeAnnounceDeptSearch.SelectedValue);
         objAnnounce.section_idx = int.Parse(ddlChangeAnnounceSecSearch.SelectedValue);
         objAnnounce.keyword_search = txtChangeAnnounceKeywordSearch.Text.Trim();
         dataEmps.emps_announce_action[0] = objAnnounce;
         _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, Constants.SELECT_ANNOUNCE_FOR_CHANGE);
         dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
         setGridData(gvEmpshiftChangeAnnounceList, dataEmps.emps_announce_action);
         _divChangeAnnounceDiaryList.Visible = false;
         _divChangeAnnounceMonthlyList.Visible = true;
      }
   }

   protected void actionSearchEditWaiting()
   {
      if (int.Parse(ddlEditWaitingTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
      {
         if (int.Parse(ddlEditWaitingTypesDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            //divPendingAnnounceDiary.Visible = true;
            //divPendingAnnounce.Visible = false;
            //divPendingChangeAnnounce.Visible = false;
            announce_diary objAnnounceDiary = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounceDiary.m0_group_diary_idx_ref = int.Parse(ddlChangeAnnounceGroupDiarySearch.SelectedValue);
            dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_ANNOUNCE_FOR_EDIT);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvEmpshiftChangeAnnounceDiaryList, dataEmps.emps_announce_diary_action);
            _divChangeAnnounceDiaryList.Visible = true;
            _divChangeAnnounceMonthlyList.Visible = false;
         }
         else if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {

         }
      }
      else if (int.Parse(ddlEditWaitingTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
      {
         if (int.Parse(ddlEditWaitingTypesDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            //divPendingAnnounceDiary.Visible = false;
            //divPendingAnnounce.Visible = true;
            //divPendingChangeAnnounce.Visible = false;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            announce objAnnounce = new announce();
            dataEmps.emps_announce_action = new announce[1];
            objAnnounce.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objAnnounce.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_announce_action[0] = objAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingAnnounce, dataEmps.emps_announce_action);
         }
         else if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {
            //divPendingAnnounce.Visible = false;
            //divPendingChangeAnnounce.Visible = true;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            change_announce objChangeAnnounce = new change_announce();
            dataEmps.emps_change_announce_action = new change_announce[1];
            objChangeAnnounce.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objChangeAnnounce.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_change_announce_action[0] = objChangeAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingChangeAnnounce, dataEmps.emps_change_announce_action);
         }
      }
   }

   protected void actionSearchPending()
   {
      if (int.Parse(ddlPendingTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
      {
         if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            _divPendingAnnounceDiaryList.Visible = true;
            _divPendingChangeAnnounceDiaryList.Visible = false;
            _divPendingAnnounceMonthlyList.Visible = false;
            _divPendingChangeAnnounceMonthlyList.Visible = false;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            announce_diary objAnnounceDiary = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounceDiary.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objAnnounceDiary.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingAnnounceDiary, dataEmps.emps_announce_diary_action);
         }
         else if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {
            _divPendingAnnounceDiaryList.Visible = false;
            _divPendingChangeAnnounceDiaryList.Visible = true;
            _divPendingAnnounceMonthlyList.Visible = false;
            _divPendingChangeAnnounceMonthlyList.Visible = false;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            change_announce_diary objChangeAnnounceDiary = new change_announce_diary();
            dataEmps.emps_change_announce_diary_action = new change_announce_diary[1];
            objChangeAnnounceDiary.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objChangeAnnounceDiary.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_change_announce_diary_action[0] = objChangeAnnounceDiary;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceDiaryService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingChangeAnnounceDiary, dataEmps.emps_change_announce_diary_action);
         }
      }
      else if (int.Parse(ddlPendingTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
      {
         if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
         {
            _divPendingAnnounceDiaryList.Visible = false;
            _divPendingChangeAnnounceDiaryList.Visible = false;
            _divPendingAnnounceMonthlyList.Visible = true;
            _divPendingChangeAnnounceMonthlyList.Visible = false;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            announce objAnnounce = new announce();
            dataEmps.emps_announce_action = new announce[1];
            objAnnounce.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objAnnounce.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_announce_action[0] = objAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingAnnounce, dataEmps.emps_announce_action);
         }
         else if (int.Parse(ddlPendingTypeDoc.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
         {
            _divPendingAnnounceDiaryList.Visible = false;
            _divPendingChangeAnnounceDiaryList.Visible = false;
            _divPendingAnnounceMonthlyList.Visible = false;
            _divPendingChangeAnnounceMonthlyList.Visible = true;
            int operationStatus = 0;
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_HEADER_NODE;
            }
            else if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               operationStatus = Constants.SELECT_HR_DIRECTOR_NODE;
            }
            change_announce objChangeAnnounce = new change_announce();
            dataEmps.emps_change_announce_action = new change_announce[1];
            objChangeAnnounce.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
            objChangeAnnounce.department_idx = int.Parse(ddlPendingDeptSearch.SelectedValue);
            dataEmps.emps_change_announce_action[0] = objChangeAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceService, dataEmps, operationStatus);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setGridData(gvPendingChangeAnnounce, dataEmps.emps_change_announce_action);
         }
      }
   }

   protected void actionPendingDiaryApproveByHeader()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingAnnounceDiary.Rows.Count; i++)
      {
         RadioButtonList pendingDiaryStatus = (RadioButtonList)gvPendingAnnounceDiary.Rows[i].FindControl("pendingDiaryStatus");
         if (pendingDiaryStatus.SelectedValue != String.Empty)
         {
            if (pendingDiaryStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_HEADER_APPROVE_TO_DIRECTOR_APPROVE;
            }
            else if (pendingDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_EDIT_TO_ADMIN_CREATED;
            }
            else if (pendingDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_CLOSE_TO_HEADER_FINISH;
            }
            var announceDiaryIdx = (gvPendingAnnounceDiary.Rows[i].Cells[0].FindControl("announceDiaryIdx") as Label).Text;
            announce_diary objAnnounceDiary = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounceDiary.u0_announce_diary_idx = int.Parse(announceDiaryIdx);
            objAnnounceDiary.u0_node_idx_ref = nodeIdx;
            objAnnounceDiary.announce_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
            servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(announceDiaryIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_DIARY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingDiaryApproveByDirector()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingAnnounceDiary.Rows.Count; i++)
      {
         RadioButtonList pendingDiaryStatus = (RadioButtonList)gvPendingAnnounceDiary.Rows[i].FindControl("pendingDiaryStatus");
         if (pendingDiaryStatus.SelectedValue != String.Empty)
         {
            if (pendingDiaryStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_APPROVE_TO_DIRECTOR_FINISH;
            }
            else if (pendingDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_EDIT_TO_HEADER_EDIT;
            }
            else if (pendingDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_CLOSE_TO_DIRECTOR_FINISH;
            }
            var announceDiaryIdx = (gvPendingAnnounceDiary.Rows[i].Cells[0].FindControl("announceDiaryIdx") as Label).Text;
            announce_diary objAnnounceDiary = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounceDiary.u0_announce_diary_idx = int.Parse(announceDiaryIdx);
            objAnnounceDiary.u0_node_idx_ref = nodeIdx;
            objAnnounceDiary.announce_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
            servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(announceDiaryIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_DIARY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingChangeDiaryApproveByHeader()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingChangeAnnounceDiary.Rows.Count; i++)
      {
         RadioButtonList pendingChangeDiaryStatus = (RadioButtonList)gvPendingChangeAnnounceDiary.Rows[i].FindControl("pendingChangeDiaryStatus");
         if (pendingChangeDiaryStatus.SelectedValue != String.Empty)
         {
            if (pendingChangeDiaryStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_HEADER_APPROVE_TO_DIRECTOR_APPROVE;
            }
            else if (pendingChangeDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_EDIT_TO_ADMIN_CREATED;
            }
            else if (pendingChangeDiaryStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_CLOSE_TO_HEADER_FINISH;
            }
            var changeAnnounceDiaryIdx = (gvPendingChangeAnnounceDiary.Rows[i].Cells[0].FindControl("changeAnnounceDiaryIdx") as Label).Text;
            change_announce_diary objChangeAnnounceDiary = new change_announce_diary();
            dataEmps.emps_change_announce_diary_action = new change_announce_diary[1];
            objChangeAnnounceDiary.u0_change_announce_diary_idx = int.Parse(changeAnnounceDiaryIdx);
            objChangeAnnounceDiary.u0_node_idx_ref = nodeIdx;
            objChangeAnnounceDiary.change_announce_diary_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_change_announce_diary_action[0] = objChangeAnnounceDiary;
            servExec.actionExec(masConn, "data_emps", empsChangeAnnounceDiaryService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(changeAnnounceDiaryIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_CHANGE_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_DIARY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingApproveByHeader()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingAnnounce.Rows.Count; i++)
      {
         RadioButtonList pendingStatus = (RadioButtonList)gvPendingAnnounce.Rows[i].FindControl("pendingStatus");
         if (pendingStatus.SelectedValue != String.Empty)
         {
            if (pendingStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_HEADER_APPROVE_TO_DIRECTOR_APPROVE;
            }
            else if (pendingStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_EDIT_TO_ADMIN_CREATED;
            }
            else if (pendingStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_CLOSE_TO_HEADER_FINISH;
            }
            var announceIdx = (gvPendingAnnounce.Rows[i].Cells[0].FindControl("announceIdx") as Label).Text;
            announce objAnnounce = new announce();
            dataEmps.emps_announce_action = new announce[1];
            objAnnounce.u0_announce_idx = int.Parse(announceIdx);
            objAnnounce.u0_node_idx_ref = nodeIdx;
            objAnnounce.announce_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_announce_action[0] = objAnnounce;
            servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(announceIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_MONTHLY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingApproveByDirector()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingAnnounce.Rows.Count; i++)
      {
         RadioButtonList pendingStatus = (RadioButtonList)gvPendingAnnounce.Rows[i].FindControl("pendingStatus");
         if (pendingStatus.SelectedValue != String.Empty)
         {
            if (pendingStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_APPROVE_TO_DIRECTOR_FINISH;
            }
            else if (pendingStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_EDIT_TO_HEADER_EDIT;
            }
            else if (pendingStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_CLOSE_TO_DIRECTOR_FINISH;
            }
            var announceIdx = (gvPendingAnnounce.Rows[i].Cells[0].FindControl("announceIdx") as Label).Text;
            announce objAnnounce = new announce();
            dataEmps.emps_announce_action = new announce[1];
            objAnnounce.u0_announce_idx = int.Parse(announceIdx);
            objAnnounce.u0_node_idx_ref = nodeIdx;
            objAnnounce.announce_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_announce_action[0] = objAnnounce;
            servExec.actionExec(masConn, "data_emps", empsAnnounceService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(announceIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_MONTHLY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingChangeApproveByHeader()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingChangeAnnounce.Rows.Count; i++)
      {
         RadioButtonList pendingChangeStatus = (RadioButtonList)gvPendingChangeAnnounce.Rows[i].FindControl("pendingChangeStatus");
         if (pendingChangeStatus.SelectedValue != String.Empty)
         {
            if (pendingChangeStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_HEADER_APPROVE_TO_DIRECTOR_APPROVE;
            }
            else if (pendingChangeStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_EDIT_TO_ADMIN_CREATED;
            }
            else if (pendingChangeStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_HEADER_UNAPPROVE_CLOSE_TO_HEADER_FINISH;
            }
            var changeAnnounceIdx = (gvPendingChangeAnnounce.Rows[i].Cells[0].FindControl("changeAnnounceIdx") as Label).Text;
            change_announce objChangeAnnounce = new change_announce();
            dataEmps.emps_change_announce_action = new change_announce[1];
            objChangeAnnounce.u0_change_announce_idx = int.Parse(changeAnnounceIdx);
            objChangeAnnounce.change_u0_node_idx_ref = nodeIdx;
            objChangeAnnounce.change_announce_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_change_announce_action[0] = objChangeAnnounce;
            servExec.actionExec(masConn, "data_emps", empsChangeAnnounceService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(changeAnnounceIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_CHANGE_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_MONTHLY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionPendingChangeApproveByDirector()
   {
      int nodeIdx = 0;
      for (var i = 0; i < gvPendingChangeAnnounce.Rows.Count; i++)
      {
         RadioButtonList pendingChangeStatus = (RadioButtonList)gvPendingChangeAnnounce.Rows[i].FindControl("pendingChangeStatus");
         if (pendingChangeStatus.SelectedValue != String.Empty)
         {
            if (pendingChangeStatus.SelectedValue == Constants.TEXT_APPROVE)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_APPROVE_TO_DIRECTOR_FINISH;
            }
            else if (pendingChangeStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_EDIT)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_EDIT_TO_HEADER_EDIT;
            }
            else if (pendingChangeStatus.SelectedValue == Constants.TEXT_NOT_APPROVE_CLOSED)
            {
               nodeIdx = Constants.U0NODE_DIRECTOR_UNAPPROVE_CLOSE_TO_DIRECTOR_FINISH;
            }
            var changeAnnounceIdx = (gvPendingChangeAnnounce.Rows[i].Cells[0].FindControl("changeAnnounceIdx") as Label).Text;
            change_announce objChangeAnnounce = new change_announce();
            dataEmps.emps_change_announce_action = new change_announce[1];
            objChangeAnnounce.u0_change_announce_idx = int.Parse(changeAnnounceIdx);
            objChangeAnnounce.change_u0_node_idx_ref = nodeIdx;
            objChangeAnnounce.change_announce_updated_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmps.emps_change_announce_action[0] = objChangeAnnounce;
            servExec.actionExec(masConn, "data_emps", empsChangeAnnounceService, dataEmps, Constants.UPDATE);

            data_emps dataEmpsApprove = new data_emps();
            approval_log objApproveLog = new approval_log();
            dataEmpsApprove.emps_approval_log_action = new approval_log[1];
            objApproveLog.u0_typeof_idx_ref = int.Parse(changeAnnounceIdx);
            objApproveLog.u0_node_idx_ref = nodeIdx;
            objApproveLog.approval_log_types = Constants.TYPEOF_TABLE_U0_CHANGE_ANNOUNCE;
            objApproveLog.approval_log_emp_types = Constants.TYPEOF_EMPLOYEE_MONTHLY;
            objApproveLog.approval_log_created_by = int.Parse(ViewState["EmpIDX"].ToString());
            dataEmpsApprove.emps_approval_log_action[0] = objApproveLog;
            servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmpsApprove, Constants.CREATE);
         }
      }
   }

   protected void actionApprovalLogList(int typeOfIdxRef, int approvalType, int approvalEmpType, Repeater rpID)
   {
      approval_log objApproveLog = new approval_log();
      dataEmps.emps_approval_log_action = new approval_log[1];
      objApproveLog.u0_typeof_idx_ref = typeOfIdxRef;
      objApproveLog.approval_log_types = approvalType;
      objApproveLog.approval_log_emp_types = approvalEmpType;
      dataEmps.emps_approval_log_action[0] = objApproveLog;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsApprovalLogService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setRepeaterData(rpID, dataEmps.emps_approval_log_action);
   }

   protected void actionSearchHRArea()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         if (int.Parse(ddlTypesEmpHRArea.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
         {
            if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
            {
               export objExport = new export();
               dataEmps.emps_export_action = new export[1];
               objExport.m0_group_diary_idx_ref = int.Parse(ddlGroupDiaryHRArea.SelectedValue);
               objExport.keyword_search = txtKeywordHRArea.Text.Trim();
               if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.ALL)
               {
                  objExport.status_doc = Constants.ALL_NODE_DIARY;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_APPROVE)
               {
                  objExport.status_doc = Constants.APPROVE_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_EDIT)
               {
                  objExport.status_doc = Constants.EDIT_NODE_DIARY;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_CLOSED)
               {
                  objExport.status_doc = Constants.CLOSED_NODE_DIARY;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_WAITING)
               {
                  objExport.status_doc = Constants.WAITING_NODE_DIARY;
               }
               dataEmps.emps_export_action[0] = objExport;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmps, Constants.SELECT_TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE_DIARY);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               setGridData(gvAnnounceDiaryExportHRArea, dataEmps.emps_export_action);
               _divHRAreaAnnounceDiaryExportExcel.Visible = true;
               //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
               _divHRAreaAnnounceMonthlyExportExcel.Visible = false;
               _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
               visibleGridViewAnnounceColumns(gvAnnounceDiaryExportHRArea, 1);
               if (dataEmps.return_code.ToString() == Constants.DATA_RETURN_IS_NOT_NULL)
               {
                  _divHRAreaAnnounceDiaryExportExcel.Visible = true;
                  //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
                  _divHRAreaAnnounceMonthlyExportExcel.Visible = false;
                  _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
                  btnAnnounceDiaryExport.Visible = true;
                  btnAnnounceMonthlyExport.Visible = false;
               }
               else
               {
                  _divHRAreaAnnounceDiaryExportExcel.Visible = true;
                  //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
                  _divHRAreaAnnounceMonthlyExportExcel.Visible = false;
                  _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
                  btnAnnounceDiaryExport.Visible = false;
                  btnAnnounceMonthlyExport.Visible = false;
               }
            }
         }
         else if (int.Parse(ddlTypesEmpHRArea.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
         {
            if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
            {
               export objExport = new export();
               dataEmps.emps_export_action = new export[1];
               objExport.organization_idx = int.Parse(ddlOrgHRArea.SelectedValue);
               objExport.department_idx = int.Parse(ddlDeptHRArea.SelectedValue);
               objExport.section_idx = int.Parse(ddlSecHRArea.SelectedValue);
               objExport.keyword_search = txtKeywordHRArea.Text.Trim();
               if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.ALL)
               {
                  objExport.status_doc = Constants.ALL_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_APPROVE)
               {
                  objExport.status_doc = Constants.APPROVE_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_EDIT)
               {
                  objExport.status_doc = Constants.EDIT_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_CLOSED)
               {
                  objExport.status_doc = Constants.CLOSED_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_WAITING)
               {
                  objExport.status_doc = Constants.WAITING_NODE;
               }
               dataEmps.emps_export_action[0] = objExport;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmps, Constants.SELECT_TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               setGridData(gvAnnounceExportHRArea, dataEmps.emps_export_action);
               _divHRAreaAnnounceDiaryExportExcel.Visible = false;
               //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
               _divHRAreaAnnounceMonthlyExportExcel.Visible = true;
               _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
               visibleGridViewAnnounceColumns(gvAnnounceExportHRArea, 2);
               if (dataEmps.return_code.ToString() == Constants.DATA_RETURN_IS_NOT_NULL)
               {
                  _divHRAreaAnnounceDiaryExportExcel.Visible = false;
                  //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
                  _divHRAreaAnnounceMonthlyExportExcel.Visible = true;
                  _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
                  btnAnnounceDiaryExport.Visible = false;
                  btnAnnounceMonthlyExport.Visible = true;
               }
               else
               {
                  _divHRAreaAnnounceDiaryExportExcel.Visible = false;
                  //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
                  _divHRAreaAnnounceMonthlyExportExcel.Visible = true;
                  _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
                  btnAnnounceDiaryExport.Visible = false;
                  btnAnnounceMonthlyExport.Visible = false;
               }
            }
            else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE)
            {
               data_emps dataEmpsGetEmpIdx = new data_emps();
               export objExportGetEmpIdx = new export();
               dataEmpsGetEmpIdx.emps_export_action = new export[1];
               objExportGetEmpIdx.organization_idx = int.Parse(ddlOrgHRArea.SelectedValue);
               objExportGetEmpIdx.department_idx = int.Parse(ddlDeptHRArea.SelectedValue);
               objExportGetEmpIdx.section_idx = int.Parse(ddlSecHRArea.SelectedValue);
               objExportGetEmpIdx.keyword_search = txtKeywordHRArea.Text.Trim();
               if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.ALL)
               {
                  objExportGetEmpIdx.status_doc = Constants.ALL_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_APPROVE)
               {
                  objExportGetEmpIdx.status_doc = Constants.APPROVE_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_EDIT)
               {
                  objExportGetEmpIdx.status_doc = Constants.EDIT_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_CLOSED)
               {
                  objExportGetEmpIdx.status_doc = Constants.CLOSED_NODE;
               }
               else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_WAITING)
               {
                  objExportGetEmpIdx.status_doc = Constants.WAITING_NODE;
               }
               dataEmpsGetEmpIdx.emps_export_action[0] = objExportGetEmpIdx;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmpsGetEmpIdx, Constants.SELECT_GET_EMP_IDX);
               dataEmpsGetEmpIdx = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               string listEmpIdx = dataEmpsGetEmpIdx.return_empidx;
               string[] arraysEmpIdx = listEmpIdx.Split(new string[] { "," }, StringSplitOptions.None);
               for (var i = 0; i < int.Parse(arraysEmpIdx.Length.ToString()); i++)
               {
                  if (arraysEmpIdx[i] != String.Empty)
                  {
                     string _local_xml_export = String.Empty;
                     string _local_xml_announce = String.Empty;
                     string _local_xml_change_announce = String.Empty;
                     var countAnnounce = "-1";
                     var countChangeAnnounce = "-1";
                     data_emps dataEmpsCountAnnounce = new data_emps();
                     export objExportCountAnnounce = new export();
                     dataEmpsCountAnnounce.emps_export_action = new export[1];
                     objExportCountAnnounce.emp_idx_ref = int.Parse(arraysEmpIdx[i]);
                     dataEmpsCountAnnounce.emps_export_action[0] = objExportCountAnnounce;
                     _local_xml_announce = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmpsCountAnnounce, Constants.SELECT_COUNT_SCHEDULE_ANNOUNCE);
                     dataEmpsCountAnnounce = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml_announce);
                     if (dataEmpsCountAnnounce.emps_export_action[0].count_parttime != "0")
                     {
                        countAnnounce = dataEmpsCountAnnounce.emps_export_action[0].count_parttime;
                     }
                     data_emps dataEmpsCountChangeAnnounce = new data_emps();
                     export objExportCountChangeAnnounce = new export();
                     dataEmpsCountChangeAnnounce.emps_export_action = new export[1];
                     objExportCountChangeAnnounce.emp_idx_ref = int.Parse(arraysEmpIdx[i]);
                     dataEmpsCountChangeAnnounce.emps_export_action[0] = objExportCountChangeAnnounce;
                     _local_xml_change_announce = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmpsCountChangeAnnounce, Constants.SELECT_COUNT_CHANGE_ANNOUNCE);
                     dataEmpsCountChangeAnnounce = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml_change_announce);
                     if (dataEmpsCountChangeAnnounce.emps_export_action[0].count_change_parttime != "0")
                     {
                        countChangeAnnounce = dataEmpsCountChangeAnnounce.emps_export_action[0].count_change_parttime;
                     }

                     if (int.Parse(countChangeAnnounce) > int.Parse(countAnnounce))
                     {
                        data_emps dataEmpsBindGV = new data_emps();
                        export objExport = new export();
                        dataEmpsBindGV.emps_export_action = new export[1];
                        objExport.organization_idx = int.Parse(ddlOrgHRArea.SelectedValue);
                        objExport.department_idx = int.Parse(ddlDeptHRArea.SelectedValue);
                        objExport.section_idx = int.Parse(ddlSecHRArea.SelectedValue);
                        if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.ALL)
                        {
                           objExport.status_doc = Constants.ALL_NODE;
                        }
                        else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_APPROVE)
                        {
                           objExport.status_doc = Constants.APPROVE_NODE;
                        }
                        else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_EDIT)
                        {
                           objExport.status_doc = Constants.EDIT_NODE;
                        }
                        else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_NOT_APPROVE_CLOSED)
                        {
                           objExport.status_doc = Constants.CLOSED_NODE;
                        }
                        else if (int.Parse(ddlStatusDocHRArea.SelectedValue) == Constants.STATUSOF_DOCUMENT_WAITING)
                        {
                           objExport.status_doc = Constants.WAITING_NODE;
                        }
                        dataEmpsBindGV.emps_export_action[0] = objExport;
                        _local_xml_export = servExec.actionExec(masConn, "data_emps", empsExportService, dataEmpsBindGV, Constants.SELECT_TYPEOF_DOCUMENT_CHANGE_ANNOUNCE_LEFT);
                        dataEmpsBindGV = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml_export);
                        object dataEmpsBindGVs = dataEmpsBindGV.emps_export_action;
                     }
                     else if (int.Parse(countChangeAnnounce) < int.Parse(countAnnounce))
                     {

                     }
                     else if (int.Parse(countChangeAnnounce) == int.Parse(countAnnounce))
                     {

                     }
                  }
               }
               //setGridData(gvChangeAnnounceExportHRArea, dataEmpsBindGV.emps_export_action);
               //divChangeAnnounceExport.Visible = true;
               //divAnnounceExport.Visible = false;
               //visibleGridViewChangeAnnounceColumns(gvChangeAnnounceExportHRArea);
               //if (dataEmpsBindGV.return_code.ToString() == Constants.DATA_RETURN_IS_NOT_NULL)
               //{
               //   btnChangeAnnounceExport.Visible = true;
               //   btnAnnounceExport.Visible = false;
               //}
               //else
               //{
               //   btnChangeAnnounceExport.Visible = false;
               //   btnAnnounceExport.Visible = false;
               //}
            }
         }
      }
      else
      {
         _divHRAreaAnnounceDiaryExportExcel.Visible = false;
         //_divHRAreaChangeAnnounceDiaryExportExcel.Visible = false;
         _divHRAreaAnnounceMonthlyExportExcel.Visible = false;
         _divHRAreaChangeAnnounceMonthlyExportExcel.Visible = false;
      }
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "_divMenuBtnToDivIndex":
            divIndex.Visible = true;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Add("class", "active");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            getDDLOrgToDeptToSecToPosToEmp(ddlIndexOrgSearch, ddlIndexDeptSearch);
            break;

         case "_divMenuBtnToDivAnnounceDiary":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = true;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Add("class", "active");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            break;

         case "_divMenuBtnToDivIndexBtnIndexSearch":
            actionIndexEmpShiftList();
            break;

         case "_divMenuBtnToDivAnnounceMonthly":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = true;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Add("class", "active");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            break;

         case "_divMenuBtnToDivChangeAnnounce":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = true;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Add("class", "active");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            getDDLOrgToDeptToSecToPosToEmp(ddlChangeAnnounceOrgSearch, ddlChangeAnnounceDeptSearch, ddlChangeAnnounceSecSearch);
            getDDLGroupDiary(ddlChangeAnnounceGroupDiarySearch);
            break;

         case "btnBackToChangeAnnounceList":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = true;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Add("class", "active");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            getDDLOrgToDeptToSecToPosToEmp(ddlChangeAnnounceOrgSearch, ddlChangeAnnounceDeptSearch, ddlChangeAnnounceSecSearch);
            getDDLGroupDiary(ddlChangeAnnounceGroupDiarySearch);
            break;

         case "_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceMonthly":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divChangeAnnounceSearch.Visible = false;
            _divChangeAnnounceDiaryList.Visible = false;
            _divChangeAnnounceDiaryToDo.Visible = false;
            _divChangeAnnounceMonthlyList.Visible = false;
            _divChangeAnnounceMonthlyToDo.Visible = true;
            txtChangeAnnounceIdx.Value = cmdArg.ToString();
            txtChangeAnnounceEmpIdxLogin.Value = ViewState["EmpIDX"].ToString();
            getEmpDataToLabel(int.Parse(cmdArg), lblChangeAnnounceEmpCode, lblChangeAnnounceJobLevel, lblChangeAnnounceFullNameTH, lblChangeAnnouncePosNameTH, lblChangeAnnounceDeptNameTH, lblChangeAnnounceSecNameTH, lblChangeAnnounceFirstApprover, lblChangeAnnounceSecondApprover);
            break;

         case "_divChangeAnnounceMonthlyToDoBtnToChangeAnnounceDiary":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divChangeAnnounceSearch.Visible = false;
            _divChangeAnnounceDiaryList.Visible = false;
            _divChangeAnnounceDiaryToDo.Visible = true;
            _divChangeAnnounceMonthlyList.Visible = false;
            _divChangeAnnounceMonthlyToDo.Visible = false;
            txtChangeAnnounceDiaryIdx.Value = cmdArg.ToString();
            getDataOldAnnounceDiary(int.Parse(cmdArg.ToString()));
            getEmpDataToLabel(int.Parse(cmdArg), lblChangeAnnounceEmpCode, lblChangeAnnounceJobLevel, lblChangeAnnounceFullNameTH, lblChangeAnnouncePosNameTH, lblChangeAnnounceDeptNameTH, lblChangeAnnounceSecNameTH, lblChangeAnnounceFirstApprover, lblChangeAnnounceSecondApprover);
            break;

         case "btnChangeAnnounceSearch":
            actionChangeAnnounceEmpShift();
            break;

         case "_divMenuBtnToDivEditWaiting":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = true;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Add("class", "active");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            getDDLOrgToDeptToSecToPosToEmp(ddlEditWaitingOrgSearch, ddlEditWaitingDeptSearch);
            break;

         case "_divMenuBtnToDivPending":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = true;
            divHRArea.Visible = false;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Add("class", "active");
            _divMenuLiToDivHRArea.Attributes.Remove("class");
            getDDLOrgToDeptToSecToPosToEmp(ddlPendingOrgSearch, ddlPendingDeptSearch);
            visibleDDLEmployeeTypes(ddlPendingTypesEmployee);
            break;

         case "btnEditWaitingSearch":
            actionSearchEditWaiting();
            break;

         case "_divMenuBtnToDivPendingBtnPendingSearch":
            actionSearchPending();
            break;

         case "btnPendingDiaryApproveByHeader":
            actionPendingDiaryApproveByHeader();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingDiaryApproveByDirector":
            actionPendingDiaryApproveByDirector();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingChangeDiaryApproveByHeader":
            actionPendingChangeDiaryApproveByHeader();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingApproveByHeader":
            actionPendingApproveByHeader();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingApproveByDirector":
            actionPendingApproveByDirector();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingChangeApproveByHeader":
            actionPendingChangeApproveByHeader();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnPendingChangeApproveByDirector":
            actionPendingChangeApproveByDirector();
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnViewDescriptionAnnounceDiary":
            divIndex.Visible = true;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divIndexSearch.Visible = false;
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexAnnounceDiaryDescription.Visible = true;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryDescription.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexAnnounceMonthlyDescription.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyDescription.Visible = false;
            actionApprovalLogList(int.Parse(cmdArg), Constants.TYPEOF_TABLE_U0_ANNOUNCE, Constants.TYPEOF_EMPLOYEE_DIARY, rpApprovalLogDiary);
            announceDiaryIdxViewDescription.Value = cmdArg.ToString();
            announce_diary objAnnounce = new announce_diary();
            dataEmps.emps_announce_diary_action = new announce_diary[1];
            objAnnounce.u0_announce_diary_idx = int.Parse(cmdArg);
            dataEmps.emps_announce_diary_action[0] = objAnnounce;
            _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_EMPLOYEE_ANNOUNCE_DIARY);
            dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
            setRepeaterData(rpEmployeeDiaryIndexDescription, dataEmps.emps_announce_diary_action);
            break;

         case "btnViewDescriptionChangeAnnounceDiary":
            divIndex.Visible = true;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divIndexSearch.Visible = false;
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexAnnounceDiaryDescription.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryDescription.Visible = true;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexAnnounceMonthlyDescription.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyDescription.Visible = false;
            var splitCmdArgDiary = cmdArg.ToString().Split(',');
            actionApprovalLogList(int.Parse(splitCmdArgDiary[0]), Constants.TYPEOF_TABLE_U0_CHANGE_ANNOUNCE, Constants.TYPEOF_EMPLOYEE_DIARY, rpChangeAnnounceDiaryApprovalLog);
            announceDiaryIdxIndexDescriptionOfChange.Value = splitCmdArgDiary[1].ToString();
            changeAnnounceDiaryIdxIndexDescriptionOfChange.Value = splitCmdArgDiary[0].ToString();
            break;

         case "btnViewDescriptionAnnounceMonthly":
            divIndex.Visible = true;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divIndexSearch.Visible = false;
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexAnnounceDiaryDescription.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryDescription.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexAnnounceMonthlyDescription.Visible = true;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyDescription.Visible = false;
            getEmpDataToLabel(int.Parse(cmdArg), lblViewDescriptionIndexEmpCode, lblViewDescriptionIndexJobLevel, lblViewDescriptionIndexFullNameTH, lblViewDescriptionIndexPosNameTH, lblViewDescriptionIndexDeptNameTH, lblViewDescriptionIndexSecNameTH, lblViewDescriptionIndexFirstApprover, lblViewDescriptionIndexSecondApprover);
            actionApprovalLogList(int.Parse(cmdArg), Constants.TYPEOF_TABLE_U0_ANNOUNCE, Constants.TYPEOF_EMPLOYEE_MONTHLY, rpApprovalLog);
            announceIdxViewDescription.Value = cmdArg.ToString();
            break;

         case "btnViewDescriptionChangeAnnounceMonthly":
            divIndex.Visible = true;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = false;
            _divIndexSearch.Visible = false;
            _divIndexAnnounceDiaryList.Visible = false;
            _divIndexAnnounceDiaryDescription.Visible = false;
            _divIndexChangeAnnounceDiaryList.Visible = false;
            _divIndexChangeAnnounceDiaryDescription.Visible = false;
            _divIndexAnnounceMonthlyList.Visible = false;
            _divIndexAnnounceMonthlyDescription.Visible = false;
            _divIndexChangeAnnounceMonthlyList.Visible = false;
            _divIndexChangeAnnounceMonthlyDescription.Visible = true;
            var splitCmdArg = cmdArg.ToString().Split(',');
            actionApprovalLogList(int.Parse(splitCmdArg[0]), Constants.TYPEOF_TABLE_U0_CHANGE_ANNOUNCE, Constants.TYPEOF_EMPLOYEE_MONTHLY, rpChangeAnnounceApprovalLog);
            announceIdxIndexDescription.Value = splitCmdArg[1].ToString();
            changeAnnounceIdxIndexDescription.Value = splitCmdArg[0].ToString();
            break;

         case "_divMenuBtnToDivHRArea":
            divIndex.Visible = false;
            divAnnounceDiaryToDo.Visible = false;
            divAnnounceMonthlyToDo.Visible = false;
            divChangeAnnounce.Visible = false;
            divEditWaiting.Visible = false;
            divPending.Visible = false;
            divHRArea.Visible = true;
            _divMenuLiToDivIndex.Attributes.Remove("class");
            _divMenuLiToDivAnnounce.Attributes.Remove("class");
            _divMenuLiToDivChangeAnnounce.Attributes.Remove("class");
            _divMenuLiToDivHolidaySwap.Attributes.Remove("class");
            _divMenuLiToDivEditWaiting.Attributes.Remove("class");
            _divMenuLiToDivPending.Attributes.Remove("class");
            _divMenuLiToDivHRArea.Attributes.Add("class", "active");
            getDDLOrgToDeptToSecToPosToEmp(ddlOrgHRArea, ddlDeptHRArea, ddlSecHRArea);
            break;

         case "_divMenuBtnToDivHRAreaBtnSearchHRArea":
            actionSearchHRArea();
            break;

         case "btnAnnounceMonthlyExport":
            exportGridViewToExcel(gvAnnounceExportHRArea, "announce-export-excel");
            break;

         case "btnChangeAnnounceMonthlyExport":
            exportGridViewToExcel(gvChangeAnnounceExportHRArea, "change-announce-export-excel");
            break;

         case "btnAnnounceDiaryExport":
            exportGridViewToExcel(gvAnnounceDiaryExportHRArea, "announce-diary-export-excel");
            break;

         case "btnBackToIndexEmpshift":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnBackToIndexChangeEmpshift":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnBackToChangeAnnounceDiaryList":
            _divChangeAnnounceSearch.Visible = true;
            _divChangeAnnounceDiaryList.Visible = true;
            _divChangeAnnounceDiaryToDo.Visible = false;
            break;

         case "btnBackToIndexChangeEmpshiftDiary":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Get data from DB
   protected void getEmpLogin(int empId)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      objODSPE.emp_idx = empId;
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ViewState["EmpIDX"] = dataEmps.emps_odspe_action[0].EmpIDX;
      ViewState["EmpCode"] = dataEmps.emps_odspe_action[0].EmpCode;
      ViewState["JobLevel"] = dataEmps.emps_odspe_action[0].JobLevel;
      ViewState["FullNameTH"] = dataEmps.emps_odspe_action[0].FullNameTH;
      ViewState["PosIDX"] = dataEmps.emps_odspe_action[0].RPosIDX_J;
      ViewState["PosNameTH"] = dataEmps.emps_odspe_action[0].PosNameTH;
      ViewState["DeptNameTH"] = dataEmps.emps_odspe_action[0].DeptNameTH;
      ViewState["SecNameTH"] = dataEmps.emps_odspe_action[0].SecNameTH;
      ViewState["NameApprover1"] = dataEmps.emps_odspe_action[0].NameApprover1;
      ViewState["NameApprover2"] = dataEmps.emps_odspe_action[0].NameApprover2;
      ViewState["RDeptIDX"] = dataEmps.emps_odspe_action[0].RDeptID;
   }

   protected void getEmpDataToLabel(int announceIdx, Label empCode, Label jobLevel, Label fullNameTH, Label posNameTH, Label deptNameTH, Label secNameTH, Label firstApproval, Label secondApproval)
   {
      empsemployee objEmployee = new empsemployee();
      dataEmps.emps_employee_data_action = new empsemployee[1];
      objEmployee.u0_announce_idx = announceIdx;
      dataEmps.emps_employee_data_action[0] = objEmployee;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsEmployeeService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      empCode.Text = dataEmps.emps_employee_data_action[0].EmpCode;
      jobLevel.Text = dataEmps.emps_employee_data_action[0].JobLevel;
      fullNameTH.Text = dataEmps.emps_employee_data_action[0].FullNameTH;
      posNameTH.Text = dataEmps.emps_employee_data_action[0].PosNameTH;
      deptNameTH.Text = dataEmps.emps_employee_data_action[0].SecNameTH;
      secNameTH.Text = dataEmps.emps_employee_data_action[0].DeptNameTH;
      firstApproval.Text = dataEmps.emps_employee_data_action[0].NameApprover1;
      secondApproval.Text = dataEmps.emps_employee_data_action[0].NameApprover2;
   }

   protected void getDDLOrgToDeptToSecToPosToEmp(DropDownList ddlOrg, DropDownList ddlDept = null, DropDownList ddlSec = null, DropDownList ddlPos = null, DropDownList ddlEmp = null)
   {
      odspe objODSPE = new odspe();
      dataEmps.emps_odspe_action = new odspe[1];
      dataEmps.emps_odspe_action[0] = objODSPE;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ddlOrg.Items.Clear();
      ddlOrg.AppendDataBoundItems = true;
      ddlOrg.Items.Add(new ListItem("ทั้งหมด", "0"));
      ddlOrg.DataSource = dataEmps.emps_odspe_action;
      ddlOrg.DataTextField = "OrgNameTH";
      ddlOrg.DataValueField = "OrgIDX";
      ddlOrg.DataBind();

      if (ddlDept != null)
      {
         ddlDept.Items.Clear();
         ddlDept.Items.Add(new ListItem("ทั้งหมด", "0"));
      }
      if (ddlSec != null)
      {
         ddlSec.Items.Clear();
         ddlSec.Items.Add(new ListItem("ทั้งหมด", "0"));
      }
      if (ddlPos != null)
      {
         ddlPos.Items.Clear();
         ddlPos.Items.Add(new ListItem("ทั้งหมด", "0"));
      }
      if (ddlEmp != null)
      {
         ddlEmp.Items.Clear();
         ddlEmp.Items.Add(new ListItem("ทั้งหมด", "0"));
      }
   }

   protected void getDDLGroupDiary(DropDownList ddlGroupDiary)
   {
      group_diary objGroupDiary = new group_diary();
      dataEmps.emps_group_diary_action = new group_diary[1];
      objGroupDiary.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
      dataEmps.emps_group_diary_action[0] = objGroupDiary;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsGroupDiaryService, dataEmps, Constants.SELECT_ALL);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      ddlGroupDiary.Items.Clear();
      ddlGroupDiary.AppendDataBoundItems = true;
      ddlGroupDiary.Items.Add(new ListItem("ทั้งหมด", "0"));
      ddlGroupDiary.DataSource = dataEmps.emps_group_diary_action;
      ddlGroupDiary.DataTextField = "text_to_ddl";
      ddlGroupDiary.DataValueField = "m0_group_diary_idx";
      ddlGroupDiary.DataBind();
   }

   protected void getDataOldAnnounceDiary(int groupDiaryIdx)
   {
      announce_diary objAnnounceDiary = new announce_diary();
      dataEmps.emps_announce_diary_action = new announce_diary[1];
      objAnnounceDiary.u0_announce_diary_idx = groupDiaryIdx;
      dataEmps.emps_announce_diary_action[0] = objAnnounceDiary;
      _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_EMPLOYEE_ANNOUNCE_DIARY);
      dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
      setRepeaterData(rpOldEmployeeDiaryList, dataEmps.emps_announce_diary_action);
      lblOldFromDateDiary.Text = dataEmps.emps_announce_diary_action[0].announce_diary_start;
      lblOldToDateDiary.Text = dataEmps.emps_announce_diary_action[0].announce_diary_end;
   }
   #endregion Get data from DB

   #region Custom Functions
   protected string getSessionEmpIdx()
   {
      return Session["emp_idx"] != null ? Session["emp_idx"].ToString() : null;
   }

   protected string getStatusForAnnouncement(int status)
   {
      return status == 2 ? "<span class='text-warning'>(มีการร้องขอเปลี่ยนกะการทำงาน)</i></span>" : null;
   }

   protected string getChangeAnnounceDiary(int status)
   {
      return status == 2 ? "<span data-toggle='tooltip' data-placement='left' title='มีการร้องขอเปลี่ยนกะการทำงาน'><i class='fa fa-retweet text-warning'></i></span>" : null;
   }

   protected void visibleIndex()
   {
      _divMenuLiToDivIndex.Attributes.Add("class", "active");
      divIndex.Visible = true;
      divAnnounceDiaryToDo.Visible = false;
      divAnnounceMonthlyToDo.Visible = false;
      divChangeAnnounce.Visible = false;
      divPending.Visible = false;
      divHRArea.Visible = false;
   }

   protected void visibleDDLEmployeeTypes(DropDownList ddlID)
   {
      ddlID.Items.Clear();
      ddlID.AppendDataBoundItems = true;
      if (Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) <= Constants.NUMBER_NULL) {
         ddlID.Items.Add(new ListItem("รายวัน", "1"));
      }
      ddlID.Items.Add(new ListItem("รายเดือน", "2"));
      ddlID.DataBind();
   }

   protected void visibleMenu()
   {
      if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
      || Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
      || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
      || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
      {
         _divMenuLiToDivPending.Visible = true;
      }
      else
      {
         _divMenuLiToDivPending.Visible = false;
      }

      if (Array.IndexOf(vipEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
      || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
      {
         _divMenuLiToDivHRArea.Visible = true;
      }
      else
      {
         _divMenuLiToDivHRArea.Visible = false;
      }
   }

   protected void visibleGridViewAnnounceColumns(GridView gvName, int empType)
   {
      if (cbAll.Checked)
      {
         visibleColumns(gvName, true);
      }
      else if (!cbAll.Checked)
      {
         if (cbEmpCode.Checked)
         {
            gvName.Columns[0].Visible = true;
         }
         else
         {
            gvName.Columns[0].Visible = false;
         }

         if (cbFullNameTH.Checked)
         {
            gvName.Columns[1].Visible = true;
         }
         else
         {
            gvName.Columns[1].Visible = false;
         }

         if (cbOrgNameTH.Checked)
         {
            gvName.Columns[2].Visible = true;
         }
         else
         {
            gvName.Columns[2].Visible = false;
         }

         if (cbDeptNameTH.Checked)
         {
            gvName.Columns[3].Visible = true;
         }
         else
         {
            gvName.Columns[3].Visible = false;
         }

         if (empType == 2)
         {
            if (cbSecNameTH.Checked)
            {
               gvName.Columns[4].Visible = true;
            }
            else
            {
               gvName.Columns[4].Visible = false;
            }
         }
         else if (empType == 1)
         {
            if (cbGroupDiary.Checked)
            {
               gvName.Columns[4].Visible = true;
            }
            else
            {
               gvName.Columns[4].Visible = false;
            }
         }

         if (cbApprover.Checked)
         {
            gvName.Columns[5].Visible = true;
         }
         else
         {
            gvName.Columns[5].Visible = false;
         }

         if (cbRangeDate.Checked)
         {
            gvName.Columns[6].Visible = true;
         }
         else
         {
            gvName.Columns[6].Visible = false;
         }

         if (cbParttime.Checked)
         {
            gvName.Columns[7].Visible = true;
         }
         else
         {
            gvName.Columns[7].Visible = false;
         }

         if (cbStatusDoc.Checked)
         {
            gvName.Columns[8].Visible = true;
         }
         else
         {
            gvName.Columns[8].Visible = false;
         }

         if (cbAttactFile.Checked)
         {
            gvName.Columns[9].Visible = true;
         }
         else
         {
            gvName.Columns[9].Visible = false;
         }
      }
   }

   protected void visibleGridViewChangeAnnounceColumns(GridView gvName)
   {
      if (cbAll.Checked)
      {
         visibleColumns(gvName, true);
      }
      else if (!cbAll.Checked)
      {
         if (cbEmpCode.Checked)
         {
            gvName.Columns[0].Visible = true;
         }
         else
         {
            gvName.Columns[0].Visible = false;
         }

         if (cbFullNameTH.Checked)
         {
            gvName.Columns[1].Visible = true;
         }
         else
         {
            gvName.Columns[1].Visible = false;
         }

         if (cbOrgNameTH.Checked)
         {
            gvName.Columns[2].Visible = true;
         }
         else
         {
            gvName.Columns[2].Visible = false;
         }

         if (cbDeptNameTH.Checked)
         {
            gvName.Columns[3].Visible = true;
         }
         else
         {
            gvName.Columns[3].Visible = false;
         }

         if (cbSecNameTH.Checked)
         {
            gvName.Columns[4].Visible = true;
         }
         else
         {
            gvName.Columns[4].Visible = false;
         }

         if (cbApprover.Checked)
         {
            gvName.Columns[5].Visible = true;
         }
         else
         {
            gvName.Columns[5].Visible = false;
         }

         if (cbOldRangeDate.Checked)
         {
            gvName.Columns[6].Visible = true;
         }
         else
         {
            gvName.Columns[6].Visible = false;
         }

         if (cbOldParttime.Checked)
         {
            gvName.Columns[7].Visible = true;
         }
         else
         {
            gvName.Columns[7].Visible = false;
         }

         if (cbNewRangeDate.Checked)
         {
            gvName.Columns[8].Visible = true;
         }
         else
         {
            gvName.Columns[8].Visible = false;
         }

         if (cbNewParttime.Checked)
         {
            gvName.Columns[9].Visible = true;
         }
         else
         {
            gvName.Columns[9].Visible = false;
         }

         if (cbStatusDoc.Checked)
         {
            gvName.Columns[10].Visible = true;
         }
         else
         {
            gvName.Columns[10].Visible = false;
         }

         if (cbAttactFile.Checked)
         {
            gvName.Columns[11].Visible = true;
         }
         else
         {
            gvName.Columns[11].Visible = false;
         }
      }
   }

   protected string getWhenDate(string statusDate)
   {
      CultureInfo culture = new CultureInfo("en-US");
      Thread.CurrentThread.CurrentCulture = culture;
      if (statusDate == "1")
      {
         return DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
      }
      else if (statusDate == "2")
      {
         return DateTime.Now.ToString("yyyy-MM-dd");
      }
      else if (statusDate == "3")
      {
         return DateTime.Now.AddDays(+1).ToString("yyyy-MM-dd");
      }
      else
      {
         DateTime date = DateTime.ParseExact(statusDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
         return date.ToString("yyyy-MM-dd");
      }
   }

   protected void checkboxColumnsSelected(object sender, ServerValidateEventArgs e)
   {
      if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
      {
         if (!cbEmpCode.Checked && !cbRangeDate.Checked && !cbParttime.Checked && !cbFullNameTH.Checked && !cbOrgNameTH.Checked && !cbDeptNameTH.Checked && !cbSecNameTH.Checked && !cbAttactFile.Checked && !cbApprover.Checked && !cbStatusDoc.Checked)
         {
            e.IsValid = false;
         }
         else
         {
            e.IsValid = true;
         }
      }
      else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY)
      {
         if (!cbEmpCode.Checked && !cbFullNameTH.Checked && !cbOrgNameTH.Checked && !cbDeptNameTH.Checked && !cbSecNameTH.Checked && !cbAttactFile.Checked && !cbApprover.Checked && !cbGroupDiary.Checked && !cbStatusDoc.Checked && !cbOldRangeDate.Checked && !cbOldParttime.Checked && !cbNewRangeDate.Checked && !cbNewParttime.Checked)
         {
            e.IsValid = false;
         }
         else
         {
            e.IsValid = true;
         }
      }
   }

   protected void onCheckedChanged(object sender, EventArgs e)
   {
      if (sender is CheckBox)
      {
         var checkBox = (CheckBox)sender;
         switch (checkBox.ID)
         {
            case "cbAll":
               if (cbAll.Checked)
               {
                  cbEmpCode.Checked = true;
                  cbFullNameTH.Checked = true;
                  cbOrgNameTH.Checked = true;
                  cbDeptNameTH.Checked = true;
                  cbSecNameTH.Checked = true;
                  cbGroupDiary.Checked = true;
                  cbAttactFile.Checked = true;
                  cbRangeDate.Checked = true;
                  cbParttime.Checked = true;
                  cbOldRangeDate.Checked = true;
                  cbOldParttime.Checked = true;
                  cbNewRangeDate.Checked = true;
                  cbNewParttime.Checked = true;
                  cbApprover.Checked = true;
                  cbStatusDoc.Checked = true;
               }
               else if (!cbAll.Checked)
               {
                  cbEmpCode.Checked = false;
                  cbFullNameTH.Checked = false;
                  cbOrgNameTH.Checked = false;
                  cbDeptNameTH.Checked = false;
                  cbSecNameTH.Checked = false;
                  cbGroupDiary.Checked = false;
                  cbAttactFile.Checked = false;
                  cbRangeDate.Checked = false;
                  cbParttime.Checked = false;
                  cbOldRangeDate.Checked = false;
                  cbOldParttime.Checked = false;
                  cbNewRangeDate.Checked = false;
                  cbNewParttime.Checked = false;
                  cbApprover.Checked = false;
                  cbStatusDoc.Checked = false;
               }
               break;

            case "cbEmpCode":
               if (!cbEmpCode.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbRangeDate":
               if (!cbRangeDate.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbParttime":
               if (!cbParttime.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbFullNameTH":
               if (!cbFullNameTH.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbOrgNameTH":
               if (!cbOrgNameTH.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbDeptNameTH":
               if (!cbDeptNameTH.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbSecNameTH":
               if (!cbSecNameTH.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbGroupDiary":
               if (!cbGroupDiary.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbGroupDiary.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbGroupDiary.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbAttactFile":
               if (!cbAttactFile.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbOldRangeDate":
               if (!cbOldRangeDate.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbOldParttime":
               if (!cbOldParttime.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbNewRangeDate":
               if (!cbNewRangeDate.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbNewParttime":
               if (!cbNewParttime.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY) && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbApprover":
               if (!cbApprover.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY)
               && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked
               && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked
               && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;

            case "cbStatusDoc":
               if (!cbStatusDoc.Checked)
               {
                  cbAll.Checked = false;
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE && cbEmpCode.Checked
               && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked
               && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               else if ((int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE
               || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY)
               && cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked
               && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked
               && cbApprover.Checked && cbStatusDoc.Checked)
               {
                  cbAll.Checked = true;
               }
               break;
         }
      }
   }

   protected void onSelectedIndexChanged(object sender, EventArgs e)
   {
      if (sender is RadioButtonList)
      {
         var radioButtonList = (RadioButtonList)sender;
         switch (radioButtonList.ID)
         {
            case "pendingDiaryStatus":
               if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByHeader.Visible = true;
                  btnPendingApproveByHeader.Visible = false;
                  btnPendingChangeApproveByHeader.Visible = false;
               }
               else if (Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByDirector.Visible = true;
                  btnPendingApproveByDirector.Visible = false;
                  btnPendingChangeApproveByDirector.Visible = false;
               }
               break;

            case "pendingChangeDiaryStatus":
               if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByHeader.Visible = false;
                  btnPendingChangeDiaryApproveByHeader.Visible = true;
                  btnPendingApproveByHeader.Visible = false;
                  btnPendingChangeApproveByHeader.Visible = false;
               }
               else if (Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByDirector.Visible = false;
                  btnPendingChangeDiaryApproveByDirector.Visible = true;
                  btnPendingApproveByDirector.Visible = false;
                  btnPendingChangeApproveByDirector.Visible = false;
               }
               break;

            case "pendingStatus":
               if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByHeader.Visible = false;
                  btnPendingApproveByHeader.Visible = true;
                  btnPendingChangeApproveByHeader.Visible = false;
               }
               else if (Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingApproveByDirector.Visible = true;
               }
               break;

            case "pendingChangeStatus":
               if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingDiaryApproveByHeader.Visible = false;
                  btnPendingApproveByHeader.Visible = false;
                  btnPendingChangeApproveByHeader.Visible = true;
               }
               else if (Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL)
               {
                  btnPendingChangeApproveByDirector.Visible = true;
               }
               break;
         }
      }
      else if (sender is DropDownList)
      {
         var dropDownList = (DropDownList)sender;
         switch (dropDownList.ID)
         {
            case "ddlPendingOrgSearch":
               odspe objODSPE = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objODSPE.organization_idx = int.Parse(ddlPendingOrgSearch.SelectedValue);
               dataEmps.emps_odspe_action[0] = objODSPE;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               ddlPendingDeptSearch.Items.Clear();
               ddlPendingDeptSearch.AppendDataBoundItems = true;
               ddlPendingDeptSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
               ddlPendingDeptSearch.DataSource = dataEmps.emps_odspe_action;
               ddlPendingDeptSearch.DataTextField = "DeptNameTH";
               ddlPendingDeptSearch.DataValueField = "RDeptIDX";
               ddlPendingDeptSearch.DataBind();
               break;

            case "ddlIndexOrgSearch":
               odspe objIndexODSPE = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objIndexODSPE.organization_idx = int.Parse(ddlIndexOrgSearch.SelectedValue);
               dataEmps.emps_odspe_action[0] = objIndexODSPE;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               ddlIndexDeptSearch.Items.Clear();
               ddlIndexDeptSearch.AppendDataBoundItems = true;
               ddlIndexDeptSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
               ddlIndexDeptSearch.DataSource = dataEmps.emps_odspe_action;
               ddlIndexDeptSearch.DataTextField = "DeptNameTH";
               ddlIndexDeptSearch.DataValueField = "RDeptIDX";
               ddlIndexDeptSearch.DataBind();
               break;

            case "ddlChangeAnnounceOrgSearch":
               odspe objChangeAnnounceODSPE = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objChangeAnnounceODSPE.organization_idx = int.Parse(ddlChangeAnnounceOrgSearch.SelectedValue);
               dataEmps.emps_odspe_action[0] = objChangeAnnounceODSPE;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (int.Parse(ddlChangeAnnounceOrgSearch.SelectedValue) == Constants.ALL)
               {
                  ddlChangeAnnounceDeptSearch.Items.Clear();
                  ddlChangeAnnounceDeptSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceDeptSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
                  //
                  ddlChangeAnnounceSecSearch.Items.Clear();
                  ddlChangeAnnounceSecSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceSecSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               else
               {
                  ddlChangeAnnounceDeptSearch.Items.Clear();
                  ddlChangeAnnounceDeptSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceDeptSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
                  ddlChangeAnnounceDeptSearch.DataSource = dataEmps.emps_odspe_action;
                  ddlChangeAnnounceDeptSearch.DataTextField = "DeptNameTH";
                  ddlChangeAnnounceDeptSearch.DataValueField = "RDeptIDX";
                  ddlChangeAnnounceDeptSearch.DataBind();
                  //
                  ddlChangeAnnounceSecSearch.Items.Clear();
                  ddlChangeAnnounceSecSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceSecSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               break;

            case "ddlOrgHRArea":
               odspe objHRAreaO = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objHRAreaO.organization_idx = int.Parse(ddlOrgHRArea.SelectedValue);
               dataEmps.emps_odspe_action[0] = objHRAreaO;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (int.Parse(ddlOrgHRArea.SelectedValue) == Constants.ALL)
               {
                  ddlDeptHRArea.Items.Clear();
                  ddlDeptHRArea.AppendDataBoundItems = true;
                  ddlDeptHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  //
                  ddlSecHRArea.Items.Clear();
                  ddlSecHRArea.AppendDataBoundItems = true;
                  ddlSecHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               else
               {
                  ddlDeptHRArea.Items.Clear();
                  ddlDeptHRArea.AppendDataBoundItems = true;
                  ddlDeptHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  ddlDeptHRArea.DataSource = dataEmps.emps_odspe_action;
                  ddlDeptHRArea.DataTextField = "DeptNameTH";
                  ddlDeptHRArea.DataValueField = "RDeptIDX";
                  ddlDeptHRArea.DataBind();
                  //
                  ddlSecHRArea.Items.Clear();
                  ddlSecHRArea.AppendDataBoundItems = true;
                  ddlSecHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               break;

            case "ddlDeptHRArea":
               odspe objHRAreaOD = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objHRAreaOD.organization_idx = int.Parse(ddlOrgHRArea.SelectedValue);
               objHRAreaOD.department_idx = int.Parse(ddlDeptHRArea.SelectedValue);
               dataEmps.emps_odspe_action[0] = objHRAreaOD;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP_SEC);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (int.Parse(ddlDeptHRArea.SelectedValue) == Constants.ALL)
               {
                  ddlSecHRArea.Items.Clear();
                  ddlSecHRArea.AppendDataBoundItems = true;
                  ddlSecHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               else
               {
                  ddlSecHRArea.Items.Clear();
                  ddlSecHRArea.AppendDataBoundItems = true;
                  ddlSecHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  ddlSecHRArea.DataSource = dataEmps.emps_odspe_action;
                  ddlSecHRArea.DataTextField = "SecNameTH";
                  ddlSecHRArea.DataValueField = "RSecIDX";
                  ddlSecHRArea.DataBind();
               }
               break;

            case "ddlChangeAnnounceDeptSearch":
               odspe objChangeAnnounceOD = new odspe();
               dataEmps.emps_odspe_action = new odspe[1];
               objChangeAnnounceOD.organization_idx = int.Parse(ddlChangeAnnounceOrgSearch.SelectedValue);
               objChangeAnnounceOD.department_idx = int.Parse(ddlChangeAnnounceDeptSearch.SelectedValue);
               dataEmps.emps_odspe_action[0] = objChangeAnnounceOD;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsodspeService, dataEmps, Constants.SELECT_WHERE_ORG_DEP_SEC);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               if (int.Parse(ddlChangeAnnounceDeptSearch.SelectedValue) == Constants.ALL)
               {
                  ddlChangeAnnounceSecSearch.Items.Clear();
                  ddlChangeAnnounceSecSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceSecSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
               }
               else
               {
                  ddlChangeAnnounceSecSearch.Items.Clear();
                  ddlChangeAnnounceSecSearch.AppendDataBoundItems = true;
                  ddlChangeAnnounceSecSearch.Items.Add(new ListItem("ทั้งหมด", "0"));
                  ddlChangeAnnounceSecSearch.DataSource = dataEmps.emps_odspe_action;
                  ddlChangeAnnounceSecSearch.DataTextField = "SecNameTH";
                  ddlChangeAnnounceSecSearch.DataValueField = "RSecIDX";
                  ddlChangeAnnounceSecSearch.DataBind();
               }
               break;

            case "ddlTypesEmpHRArea":
               if (int.Parse(ddlTypesEmpHRArea.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
               {
                  divDDLOrgHRArea.Visible = false;
                  divDDLDeptHRArea.Visible = false;
                  divDDLGroupDiaryHRArea.Visible = true;
                  divDDLSecHRArea.Visible = false;
                  //
                  ddlGroupDiaryHRArea.Items.Clear();
                  ddlGroupDiaryHRArea.AppendDataBoundItems = true;
                  ddlGroupDiaryHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  //
                  divBtnSearchHRArea.Visible = true;
                  //
                  divSelectionColumnHRArea.Visible = true;
                  cbSecNameTH.Visible = false;
                  cbGroupDiary.Visible = true;
                  getDDLGroupDiary(ddlGroupDiaryHRArea);
               }
               else if (int.Parse(ddlTypesEmpHRArea.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
               {
                  divDDLOrgHRArea.Visible = true;
                  divDDLDeptHRArea.Visible = true;
                  divDDLGroupDiaryHRArea.Visible = false;
                  divDDLSecHRArea.Visible = true;
                  //
                  divBtnSearchHRArea.Visible = true;
                  //
                  divSelectionColumnHRArea.Visible = true;
                  cbSecNameTH.Visible = true;
                  cbGroupDiary.Visible = false;
               }
               else if (ddlTypesEmpHRArea.SelectedValue == String.Empty)
               {
                  ddlOrgHRArea.SelectedValue = "0";
                  //
                  ddlDeptHRArea.Items.Clear();
                  ddlDeptHRArea.AppendDataBoundItems = true;
                  ddlDeptHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  //
                  ddlSecHRArea.Items.Clear();
                  ddlSecHRArea.AppendDataBoundItems = true;
                  ddlSecHRArea.Items.Add(new ListItem("ทั้งหมด", "0"));
                  //
                  divDDLOrgHRArea.Visible = false;
                  divDDLDeptHRArea.Visible = false;
                  divDDLGroupDiaryHRArea.Visible = false;
                  divDDLSecHRArea.Visible = false;
                  //
                  divBtnSearchHRArea.Visible = false;
                  //
                  divSelectionColumnHRArea.Visible = true;
               }
               divTxtKeywordHRArea.Visible = true;
               break;

            case "ddlTypesDocHRArea":
               if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SCHEDULE_ANNOUNCE)
               {
                  cbOldRangeDate.Visible = false;
                  cbOldParttime.Visible = false;
                  cbNewRangeDate.Visible = false;
                  cbNewParttime.Visible = false;
                  cbRangeDate.Visible = true;
                  cbParttime.Visible = true;
                  if (cbEmpCode.Checked && cbRangeDate.Checked && cbParttime.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbApprover.Checked && cbStatusDoc.Checked)
                  {
                     cbAll.Checked = true;
                  }
                  else
                  {
                     cbAll.Checked = false;
                  }
               }
               else if (int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_CHANGE_ANNOUNCE || int.Parse(ddlTypesDocHRArea.SelectedValue) == Constants.TYPEOF_DOCUMENT_SWAP_HOLIDAY)
               {
                  cbOldRangeDate.Visible = true;
                  cbOldParttime.Visible = true;
                  cbNewRangeDate.Visible = true;
                  cbNewParttime.Visible = true;
                  cbRangeDate.Visible = false;
                  cbParttime.Visible = false;
                  if (cbEmpCode.Checked && cbFullNameTH.Checked && cbOrgNameTH.Checked && cbDeptNameTH.Checked && cbSecNameTH.Checked && cbAttactFile.Checked && cbOldRangeDate.Checked && cbOldParttime.Checked && cbNewRangeDate.Checked && cbNewParttime.Checked && cbApprover.Checked && cbStatusDoc.Checked)
                  {
                     cbAll.Checked = true;
                  }
                  else
                  {
                     cbAll.Checked = false;
                  }
               }
               break;

            case "ddlChangeAnnounceTypesEmployee":
               if (int.Parse(ddlChangeAnnounceTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_DIARY)
               {
                  divChangeAnnounceSecSearch.Visible = false;
                  divChangeAnnounceGroupDiarySearch.Visible = true;
                  divDDLChangeAnnounceOrg.Visible = false;
                  divDDLChangeAnnounceDept.Visible = false;
                  lblBlank.Visible = true;
                  lblKeyword.Visible = false;
                  txtChangeAnnounceKeywordSearch.Visible = false;
               }
               else if (int.Parse(ddlChangeAnnounceTypesEmployee.SelectedValue) == Constants.TYPEOF_EMPLOYEE_MONTHLY)
               {
                  divChangeAnnounceSecSearch.Visible = true;
                  divChangeAnnounceGroupDiarySearch.Visible = false;
                  divDDLChangeAnnounceOrg.Visible = true;
                  divDDLChangeAnnounceDept.Visible = true;
                  lblBlank.Visible = false;
                  lblKeyword.Visible = true;
                  txtChangeAnnounceKeywordSearch.Visible = true;
               }
               break;
         }
      }
   }

   protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var GvName = (GridView)sender;
      switch (GvName.ID)
      {
         case "gvEmpshiftChangeAnnounceList":
            GvName.PageIndex = e.NewPageIndex;
            GvName.DataBind();
            actionChangeAnnounceEmpShift();
            break;

         case "gvEmpshiftIndexList":
            GvName.PageIndex = e.NewPageIndex;
            GvName.DataBind();
            actionIndexEmpShiftList();
            break;

         case "gvEmpshiftDiaryIndexList":
            GvName.PageIndex = e.NewPageIndex;
            GvName.DataBind();
            actionIndexEmpShiftList();
            break;

         case "gvAnnounceExportHRArea":
            GvName.PageIndex = e.NewPageIndex;
            GvName.DataBind();
            actionSearchHRArea();
            break;

         case "gvAnnounceDiaryExportHRArea":
            GvName.PageIndex = e.NewPageIndex;
            GvName.DataBind();
            actionSearchHRArea();
            break;
      }
   }

   protected void gvOnRowCreated(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPendingDiaryAnnounce":
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               gvName.Columns[8].Visible = true;
            }
            else
            {
               gvName.Columns[8].Visible = false;
            }
            break;

         case "gvPendingAnnounce":
            if (Array.IndexOf(vipHeaderEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(vipDirectorEmpIdx, ViewState["EmpIDX"]) > Constants.NUMBER_NULL
            || Array.IndexOf(misRDeptIdx, ViewState["RDeptIDX"]) > Constants.NUMBER_NULL)
            {
               gvName.Columns[8].Visible = true;
            }
            else
            {
               gvName.Columns[8].Visible = false;
            }
            break;
      }
   }

   protected void gvOnRowDataBound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPendingAnnounceDiary":
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               var announceDiaryIdx = (e.Row.Cells[0].FindControl("announceDiaryIdx") as Label).Text;
               Repeater rpEmployeeDiary = (Repeater)e.Row.Cells[8].FindControl("rpEmployeeDiary");
               announce_diary objAnnounce = new announce_diary();
               dataEmps.emps_announce_diary_action = new announce_diary[1];
               objAnnounce.u0_announce_diary_idx = int.Parse(announceDiaryIdx);
               dataEmps.emps_announce_diary_action[0] = objAnnounce;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsAnnounceDiaryService, dataEmps, Constants.SELECT_EMPLOYEE_ANNOUNCE_DIARY);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               setRepeaterData(rpEmployeeDiary, dataEmps.emps_announce_diary_action);
            }
            break;

         case "gvPendingChangeAnnounceDiary":
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
               var changeAnnounceDiaryIdx = (e.Row.Cells[0].FindControl("changeAnnounceDiaryIdx") as Label).Text;
               Repeater rpEmployeeChangeDiary = (Repeater)e.Row.Cells[8].FindControl("rpEmployeeChangeDiary");
               change_announce_diary objChangeAnnounce = new change_announce_diary();
               dataEmps.emps_change_announce_diary_action = new change_announce_diary[1];
               objChangeAnnounce.u0_change_announce_diary_idx = int.Parse(changeAnnounceDiaryIdx);
               dataEmps.emps_change_announce_diary_action[0] = objChangeAnnounce;
               _local_xml = servExec.actionExec(masConn, "data_emps", empsChangeAnnounceDiaryService, dataEmps, Constants.SELECT_EMPLOYEE_CHANGE_ANNOUNCE_DIARY);
               dataEmps = (data_emps)_funcTool.convertXmlToObject(typeof(data_emps), _local_xml);
               setRepeaterData(rpEmployeeChangeDiary, dataEmps.emps_change_announce_diary_action);
            }
            break;
      }
   }

   protected void exportGridViewToExcel(GridView gvName, string fileNameWithoutExtension)
   {
      Response.Clear();
      Response.Buffer = true;
      Response.AddHeader("content-disposition", "attachment;filename=" + fileNameWithoutExtension + ".xls");
      Response.Charset = "utf-8";
      Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
      Response.ContentType = "application/vnd.ms-excel";
      Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
      Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
      StringWriter sw = new StringWriter();
      HtmlTextWriter hw = new HtmlTextWriter(sw);
      gvName.HeaderRow.BackColor = Color.White;
      foreach (TableCell cell in gvName.HeaderRow.Cells)
      {
         cell.BackColor = gvName.HeaderStyle.BackColor;
      }
      foreach (GridViewRow row in gvName.Rows)
      {
         row.BackColor = Color.White;
         foreach (TableCell cell in row.Cells)
         {
            if (row.RowIndex % 2 == 0)
            {
               cell.BackColor = gvName.AlternatingRowStyle.BackColor;
            }
            else
            {
               cell.BackColor = gvName.RowStyle.BackColor;
            }
            cell.CssClass = "textmode";
         }
      }
      gvName.RenderControl(hw);
      string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
      Response.Write(style);
      Response.Output.Write(sw.ToString());
      Response.Flush();
      Response.End();
   }

   protected void visibleColumns(GridView gvName, bool visible)
   {
      for (var i = 0; i < int.Parse(gvName.Columns.Count.ToString()); i++)
      {
         gvName.Columns[i].Visible = visible;
      }
   }

   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected void setFormData(FormView fvName, Object obj)
   {
      fvName.DataSource = obj;
      fvName.DataBind();
   }

   protected void setRepeaterData(Repeater rpName, Object obj)
   {
      rpName.DataSource = obj;
      rpName.DataBind();
   }

   protected string getMonthTh(int month)
   {
      string[] months = { "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฏาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม" };
      return months[month - 1];
   }

   protected string[] getDescCalendarByYearAndMonth(int year, int month)
   {
      return new string[] {
         DateTime.DaysInMonth(year, month).ToString(),
         getMonthTh(month)
      };
   }

   protected void generateTableCalendar(int year, int month)
   {
      var getDescCalendar = getDescCalendarByYearAndMonth(year, month);
      var startTable = "<table class='table table-bordered f-s-12'>";
      var startTh = "<th style='vertical-align:middle' rowspan='3'>#</th><th style='vertical-align:middle' rowspan='3'>รายชื่อพนักงาน</th><th style='vertical-align:middle' rowspan='3'>รหัสพนักงาน</th><th style='vertical-align:middle' rowspan='3'>วันหยุด</th>";
      var startTr = "<tr>";
      var th = "<th class='text-center' colspan=" + getDescCalendar[0] + ">เดิอน" + getDescCalendar[1] + " " + (year + 543) + "</th>";
      var endTr = "</tr>";
      var tdDateString = String.Empty;
      var tdDateNo = String.Empty;
      for (var i = 0; i < int.Parse(getDescCalendar[0]); i++)
      {
         var bgColor = String.Empty;
         var dateString = new DateTime(year, month, i + 1).ToString("ddd");
         if (dateString == "ส." || dateString == "อา.")
         {
            bgColor = "style='background-color:#ccc;'";
         }
         tdDateString += "<td class='text-center'" + bgColor + ">" + dateString + "</td>";
         tdDateNo += "<td class='text-center'" + bgColor + ">" + (i + 1) + "</td>";
      }
      var endTable = "</table>";
      //test.Text = startTable + startTr + startTh + th + endTr + startTr + tdDateString + endTr + startTr + tdDateNo + endTr + endTr + endTable;
   }

   public override void VerifyRenderingInServerForm(Control control) { }
   #endregion Custom Functions
}