﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Threading;




public partial class websystem_ITPurchase_ITpurchase : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_purchase _data_purchase = new data_purchase();
    data_softwarelicense _data_softwarelicense = new data_softwarelicense();
    service_mail servicemail = new service_mail();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlSetequipment_purchasetype"];
    static string _urlGetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlGetequipment_purchasetype"];
    static string _urlSetstatus_equipmenttype = _serviceUrl + ConfigurationManager.AppSettings["urlSetstatus_equipmenttype"];
    static string _urlSetupdate_equipmenttype = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_equipmenttype"];
    static string _urlSetcreate_equipmentpurchase = _serviceUrl + ConfigurationManager.AppSettings["urlSetcreate_equipmentpurchase"];
    static string _urlGetpurchase_equipmentlist = _serviceUrl + ConfigurationManager.AppSettings["urlGetpurchase_equipmentlist"];
    static string _urlsetapprove_purchase = _serviceUrl + ConfigurationManager.AppSettings["urlsetapprove_purchase"];
    static string _urlGetsearch_purchase = _serviceUrl + ConfigurationManager.AppSettings["urlGetsearch_purchase"];
    static string _urlgetionumber = _serviceUrl + ConfigurationManager.AppSettings["urlgetionumber"];
    static string _urlGetlocation = _serviceUrl + ConfigurationManager.AppSettings["urlGetlocation"];
    static string _urlGetstatus_node = _serviceUrl + ConfigurationManager.AppSettings["urlGetstatus_node"];
    static string _urlGetreport_purchase = _serviceUrl + ConfigurationManager.AppSettings["urlGetreport_purchase"];
    static string _urlGetcostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetcostcenter"];
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlSectionSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSectionSW"];
    static string _urlGetmasterUnit = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasterUnit"];
    static string _urlGetmasterspec = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasterspec"];
    static string _urlSetaddItemspurchase = _serviceUrl + ConfigurationManager.AppSettings["urlSetaddItemspurchase"];
    static string _urlGetitems_purchase = _serviceUrl + ConfigurationManager.AppSettings["urlGetitems_purchase"];
    static string _urlSetpurchaseitems = _serviceUrl + ConfigurationManager.AppSettings["urlSetpurchaseitems"];
    static string _urlGetreportpurchaselist = _serviceUrl + ConfigurationManager.AppSettings["urlGetreportpurchaselist"];
    static string _urlGetposition = _serviceUrl + ConfigurationManager.AppSettings["urlGetposition"];
    static string _urlGetexportexcelPurchase = _serviceUrl + ConfigurationManager.AppSettings["urlGetexportexcelPurchase"];
    static string _urlGetmasternameUnit = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasternameUnit"];
    static string _urlSetstatus_items = _serviceUrl + ConfigurationManager.AppSettings["urlSetstatus_items"];
    static string _urlsetnumberio = _serviceUrl + ConfigurationManager.AppSettings["urlsetnumberio"];
    static string _urlsetnumberasset = _serviceUrl + ConfigurationManager.AppSettings["urlsetnumberasset"];
    static string _urlgetassetnumber = _serviceUrl + ConfigurationManager.AppSettings["urlgetassetnumber"];

    static string _urlGetits_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_lookup"];

    string _localJson = "";
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
  //  string _link = "http://demo.taokaenoi.co.th/it-purchase";
    string _link = "http://mas.taokaenoi.co.th/it-purchase";

    //ผู้เกี่ยวข้อง
    int it_support = 80;
    int budget = 6;
    int asset = 9;
    int assistant_director_mis = 12113;//174;//12113;//174;
    //int assistant_director_mis = 11826;
    int manager = 12112;//3640;//12112;//3640; //คุณบอย
    int hr_director = 12109;//15290;//12109;//127;
    int director_department;
    int head_itsupport = 12114;//178;//12114;//178;


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefult();
            Setpermission();
          
        }

        linkBtnTrigger(btnshowboxcreate);
        Select_tabel_purchaselist();
        select_tabel_iolist();
        select_tabel_assetlist();

    }

    #region Setpermission
    protected void Setpermission()
    {
        ViewState["emp_idx"] = //1345;//12114;//178; //1345;//127;
       int.Parse(Session["emp_idx"].ToString());//3834;

        action_select_information_employee();

        if (int.Parse(ViewState["rsec_idx"].ToString()) == it_support) // IT Support)
        {
            //action_select_purchaselist();
           

            if (int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
            {


                listMenu3.Visible = true;
                btnviewall.Visible = true;
                ViewState["action_type"] = 5;
                ViewState["_node_idx"] = 4;
                _action_select_purchase();
                Panel_approve.Visible = false;
                btnviewall.Visible = true;


            }
            else
            {
                listMenu3.Visible = false;

            }
        }

        else if (int.Parse(ViewState["rsec_idx"].ToString()) != it_support)
        {
            action_select_purchaselist_for_department();
            // listMenu2.Visible = false;
            listMenu3.Visible = false;

        }

        if (int.Parse(ViewState["emp_idx"].ToString()) == hr_director)
        {
            Panel_list_purchase.Visible = true;

            ViewState["action_type"] = 5;
            ViewState["_node_idx"] = 3;
            _action_select_purchase();

        }

        else if (int.Parse(ViewState["emp_idx"].ToString()) == assistant_director_mis)
        {

            Panel_list_purchase.Visible = false;
            btnviewdataofmountly.Visible = true;
            Panel_approve.Visible = true;

            ViewState["action_type"] = 5;
            ViewState["_node_idx"] = 6;
            _action_select_purchase();


        }

        else if (int.Parse(ViewState["rdept_idx"].ToString()) == budget)
        {


            action_select_waiting_budget_approve();
            Panel_list_purchase.Visible = true;
            Panel_approve.Visible = false;

        }

        if (int.Parse(ViewState["rdept_idx"].ToString()) == asset)
        {

            Panel_list_purchase.Visible = true;
            Panel_approve.Visible = false;
            ViewState["action_type"] = 5;
            ViewState["_node_idx"] = 8;
            _action_select_purchase();

        }

        else if (int.Parse(ViewState["emp_idx"].ToString()) == manager)
        {


            Panel_list_purchase.Visible = false;
            btnviewdataofmountly.Visible = true;
            Panel_approve.Visible = true;
            ViewState["action_type"] = 5;
            ViewState["_node_idx"] = 7;
            _action_select_purchase();


        }

        else if (int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport)
        {

            Panel_list_purchase.Visible = false;
            btnviewdataofmountly.Visible = true;
            Panel_approve.Visible = true;
            ViewState["action_type"] = 5;
            ViewState["_node_idx"] = 15;
            _action_select_purchase();
            btnviewall.Visible = true;



        }


    }
    #endregion

    #region action_actor
    protected void action_actor()
    {
        int node_idx = 0;
        int actor_idx = 0;
        int emp_idx_purchase = 0;
        int emp_director = 0;
        int status_action = 0;


        //find hidden value
        HiddenField hidden_m0_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_node");
        HiddenField hidden_m0_actor = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_actor");
        HiddenField hidden_emp_idx = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_emp_idx");
        HiddenField hidden_emp_director = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_emp_director");
        HiddenField hidden_status_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_status_node");

        //linkbutton
        var btnnotapprove_editsheet = (LinkButton)fvmanage_approvepurchase.FindControl("btnnotapprove_editsheet");
        var btnnotapprove = (LinkButton)fvmanage_approvepurchase.FindControl("btnnotapprove");
        var btnsaveapprove = (LinkButton)fvmanage_approvepurchase.FindControl("btnsaveapprove");

        var Panel_alert_warning = (Panel)fvmanage_approvepurchase.FindControl("Panel_alert_warning");
      //  var panelNumberIO = (Panel)fvdetails_purchasequipment.FindControl("panelNumberIO");

        
        var txtcomment_purchase = (TextBox)fvmanage_approvepurchase.FindControl("txtcomment_purchase");
        var lb_warning_action = (Label)Panel_alert_warning.FindControl("lb_warning_action");
        var lb_warning_action_edit_doc = (Label)Panel_alert_warning.FindControl("lb_warning_action_edit_doc");

        node_idx = int.Parse(hidden_m0_node.Value);
        actor_idx = int.Parse(hidden_m0_actor.Value);
        emp_idx_purchase = int.Parse(hidden_emp_idx.Value);
        emp_director = int.Parse(hidden_emp_director.Value);
        status_action = int.Parse(hidden_status_node.Value);

        ViewState["_nodeidx"] = node_idx;
        ViewState["_actoridx"] = actor_idx;

        switch (actor_idx)
        {

            case 1: //ผู้ขอซื้อ

                if (node_idx == 10 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_purchase) //check emp_idx ผู้ขอซื้อ (กรณีแก้ไขเอกสาร)
                {
                    mvMultiview.SetActiveView(viewCreatePurchase);

                    action_edit_details_purchase();
                    action_select_history();
                    btnupdatepurchase.Visible = true;

                   // Literal1.Text = "มีสิทธิ์แก้ไขเอกสาร";

                }
                else
                {

                    mvMultiview.SetActiveView(viewmanage_purchasequipment);
                   // Literal1.Text = "ไม่มีสิทธิ์แก้ไขเอกสาร";
                    fvsummary_itemspurchase.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    Panel_alert_warning.Visible = true;
                    lb_warning_action_edit_doc.Visible = true;
                    lb_warning_action.Visible = false;


                }


                break;

            case 2:  //director ฝ่าย

                if (node_idx == 2 && int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    fvsummary_itemspurchase.Visible = false;
                    Panel_alert_warning.Visible = true;

                  //  Literal1.Text = "it support ดู actor 2 ได้";
                }
                else if (node_idx == 2 && int.Parse(ViewState["rsec_idx"].ToString()) != it_support)
                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                }

                if (node_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == emp_director)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    btnsaveapprove.Visible = true;
                    txtcomment_purchase.Enabled = true;
                    Panel_alert_warning.Visible = false;

                   // Literal1.Text = "director ฝ่าย";
                }


                else if (node_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) != emp_director)

                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;

                  //  Literal1.Text = "ไม่ใช่ director ฝ่าย";

                }

                if (node_idx == 12 && int.Parse(ViewState["emp_idx"].ToString()) == emp_director)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    btnsaveapprove.Visible = true;
                    txtcomment_purchase.Enabled = true;
                    Panel_alert_warning.Visible = false;

                   // Literal1.Text = "director ฝ่าย";


                }

                else if (node_idx == 12 && int.Parse(ViewState["emp_idx"].ToString()) != emp_director)
                {


                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;

                  //  Literal1.Text = "ไม่ใช่ director ฝ่าย";
                }


                content_Summary_items.Visible = false;
                break;


            case 3:  //hr director 

                if (node_idx == 3 && int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "it support ดู actor 3 ได้";
                }

                else if (node_idx == 3 && int.Parse(ViewState["rsec_idx"].ToString()) != it_support)
                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;

                }

                if (node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == hr_director)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    btnsaveapprove.Visible = true;
                    txtcomment_purchase.Enabled = true;
                    Panel_alert_warning.Visible = false;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "hr_director ";
                }

                else if (node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) != hr_director)

                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "ไม่ใช่ hr_director ";

                }
                

                break;

            case 4: //it support

                if (node_idx == 4 && int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    btnsaveapprove.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                }

                else if (node_idx == 4 && int.Parse(ViewState["rsec_idx"].ToString()) != it_support)
                {
                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                }

                break;


            case 5: //budget

             
                /* else*/
                if (node_idx == 5 && int.Parse(ViewState["rdept_idx"].ToString()) == budget)
                {


                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = true;
                    fvsummary_itemspurchase.Visible = false;
                    action_select_result_itsupport();
                  //  Literal1.Text = "BUDGET  NODE 5";

                }

                else if (node_idx == 5 && int.Parse(ViewState["rdept_idx"].ToString()) != budget)

                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                   // Literal1.Text = "ไม่ใช่ BUDGET NODE 5";


                }
                if (node_idx == 11 && int.Parse(ViewState["rdept_idx"].ToString()) == budget)
                {

                 //   Literal1.Text = "BUDGET  NODE 11  ดำเนินการรอบสอง";

                    btnnotapprove.Visible = true;
                    btnnotapprove_editsheet.Visible = false;
                    fvsummary_itemspurchase.Visible = false;
                    action_select_result_itsupport();
                  //  UpdatePanelNoIO.Visible = true;

                }

                else if (node_idx == 11 && int.Parse(ViewState["rdept_idx"].ToString()) != budget)

                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "ไม่ใช่ BUDGET NODE 11  ดำเนินการรอบสอง";

                }

                break;

            case 6: //พี่พน

                if (node_idx == 6 && int.Parse(ViewState["emp_idx"].ToString()) == assistant_director_mis)

                {

                    btnnotapprove.Visible = true;
                    btnnotapprove_editsheet.Visible = false;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "พี่พน NODE 6";
                }
                else

                {


                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "ไม่ใช่พี่พน NODE 6";

                }


                break;

            case 7: //คุณบอย

                if (node_idx == 7 && int.Parse(ViewState["emp_idx"].ToString()) == manager)
                {


                    btnnotapprove.Visible = true;
                    btnnotapprove_editsheet.Visible = false;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "คุณบอย NODE 7";

                }
                else
                {


                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "ไม่ใช่คุณบอย NODE 7";


                }



               
                break;

            case 8: //Asset

            
                if (node_idx == 8 && int.Parse(ViewState["rdept_idx"].ToString()) == asset)
                {

                    btnnotapprove.Visible = true;
                    btnnotapprove_editsheet.Visible = false;
                    fvsummary_itemspurchase.Visible = false;
                 
                    action_select_result_itsupport();

                 //   Literal1.Text = " ASSET node8 ดำเนินการ";

                }

                else

                {

                    btnnotapprove.Visible = false;
                    btnnotapprove_editsheet.Visible = false;
                    btnsaveapprove.Visible = false;
                    txtcomment_purchase.Enabled = false;
                    Panel_alert_warning.Visible = true;
                    fvsummary_itemspurchase.Visible = false;

                  //  Literal1.Text = "ไม่ใช่ ASSET node8 ดำเนินการ";

                }



                break;

            case 9: //พี่เกม

                if (node_idx == 15 && int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport)

                {

                 
                        btnnotapprove.Visible = false;
                        btnnotapprove_editsheet.Visible = true;
                        btnsaveapprove.Visible = true;
                        txtcomment_purchase.Enabled = true;
                        Panel_alert_warning.Visible = false;
                        fvsummary_itemspurchase.Visible = false;

                        //Literal1.Text = "hr_director ";
                      //  Literal1.Text = "พี่เกม node15 ดำเนินการ";



                    }

                else

                {

                        btnnotapprove.Visible = false;
                        btnnotapprove_editsheet.Visible = false;
                        btnsaveapprove.Visible = false;
                        txtcomment_purchase.Enabled = false;
                        Panel_alert_warning.Visible = true;
                        fvsummary_itemspurchase.Visible = false;

                   //     Literal1.Text = "ไม่ใช่พี่เกม node15 ดำเนินการ";

                }




                break;



        }

    }

    #endregion

    #region action_node
    protected void action_node()
    {
        int node_idx = 0;
        int actor_idx = 0;
        int emp_idx = 0;
        int status_node = 0;

        //find hidden value
        HiddenField hidden_m0_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_node");
        HiddenField hidden_m0_actor = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_actor");
        HiddenField hidden_emp_idx = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_emp_idx");
        HiddenField hidden_status = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_status_node");



        node_idx = int.Parse(hidden_m0_node.Value);
        actor_idx = int.Parse(hidden_m0_actor.Value);
        emp_idx = int.Parse(hidden_m0_actor.Value);
        status_node = int.Parse(hidden_status.Value);


        switch (node_idx)
        {
            case 1:
                action_actor();
                break;

            case 2:
                action_actor();
                break;

            case 3:
                action_actor();
                break;

            case 4:
                action_actor();
                break;

            case 5:
                action_actor();
                break;

            case 6:
                action_actor();
                break;

            case 7:
                action_actor();
                break;

            case 8:
                action_actor();
                break;

            case 9:
                action_actor();

                fvmanage_approvepurchase.Visible = false;

                break;

            case 10:
                action_actor();
                break;

            case 11:
                action_actor();
                break;

            case 12:
                action_actor();
                break;

            case 13:
                action_actor();
                break;

            case 14:
                action_actor();
                break;

            case 15:
                action_actor();
                break;

        }

    }
    #endregion

    #region SetDefult
    protected void SetDefult()
    {
      //  UpdatePanel UpdatePanelNoIO = (UpdatePanel)fvmanage_approvepurchase.FindControl("UpdatePanelNoIO");
       // UpdatePanel UpdatePanelcostcenter = (UpdatePanel)fvmanage_approvepurchase.FindControl("UpdatePanelcostcenter");
       // UpdatePanel UpdatePanelNoAsset = (UpdatePanel)fvmanage_approvepurchase.FindControl("UpdatePanelNoAsset");



        listMenu0.BackColor = System.Drawing.Color.LightGray;
        listMenu3.BackColor = System.Drawing.Color.Transparent;


        Panel_approve.Visible = false;
        btnviewdataofmountly.Visible = false;
       // UpdatePanelcostcenter.Visible = false;
      //  UpdatePanelNoIO.Visible = false;
        fvsummary_itemspurchase.Visible = false;
       // UpdatePanelNoAsset.Visible = false;
        btnviewall.Visible = false; 


    }

    #endregion

    #region formview_databound
    protected void formview_databound(object sender, EventArgs e)
    {
        var formviewName = (FormView)sender;

        switch (formviewName.ID)
        {
            case "fvCreatePurchasequipment":

                if (fvCreatePurchasequipment.CurrentMode == FormViewMode.Insert)
                {
                    TextBox txtdetails_purchase = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_purchase");
                    TextBox txtquipmenttype_idx = (TextBox)fvCreatePurchasequipment.FindControl("txtquipmenttype_idx");
                    DropDownList ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
                    DropDownList ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
                    DropDownList ddllocation = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation");
                    GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                    Panel _panel_upload_organization = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_organization");
                    Panel _panel_upload_cutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_cutoff");
                    Panel _panel_upload_refercutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_refercutoff");

                    var btnsavepurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnsavepurchase");
                    var btnaddpurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");

                    linkBtnTrigger(btnsavepurchase);
                    linkBtnTrigger(btnaddpurchase);
                    linkBtnTrigger(btnupdatepurchase);


              

                    action_select_equipmenttype();
                    action_select_purchasetype();
                    action_select_location();
                    action_select_details_employee();
                    action_select_position(0, "0");
                    set_datetime_cutpurchase();
                    action_select_masterunit(1, "0");
                    action_select_masterspec(1, "0");


                    DropDownList _ddlcostCenter = (DropDownList)fvCreatePurchasequipment.FindControl("ddlcostCenter");

                    data_purchase data_purchase_edit = new data_purchase();
                    data_purchase_edit.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details select_costcenter_edit = new u0_purchase_equipment_details();

                    _ddlcostCenter.AppendDataBoundItems = true;
                    _ddlcostCenter.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));

                    data_purchase_edit.u0_purchase_equipment_list[0] = select_costcenter_edit;
                    data_purchase_edit = callServicePurchase(_urlGetcostcenter, data_purchase_edit);

                    _ddlcostCenter.DataSource = data_purchase_edit.u0_purchase_equipment_list;
                    _ddlcostCenter.DataTextField = "costcenter_no";
                    _ddlcostCenter.DataValueField = "costcenter_idx";
                    _ddlcostCenter.DataBind();



                    gvpurchaselist.DataSource = (DataSet)ViewState["Create_purchase_speclist"];
                    gvpurchaselist.DataBind();

                    var _datasetpurchaselist = (DataSet)ViewState["Create_purchase_speclist"];
                    var _datarowpurchaselist = _datasetpurchaselist.Tables[0].NewRow();


                    if (_datasetpurchaselist.Tables[0].Rows.Count == 0)
                    {

                        btnsavepurchase.Visible = false;


                    }
                    else
                    {

                        btnsavepurchase.Visible = true;

                    }
                    foreach (GridViewRow _nrow in gvpurchaselist.Rows)

                    {
                        
                        var idx_location = (Label)_nrow.FindControl("idx_location");
                        var idx_purchase = (Label)_nrow.FindControl("idx_purchase");
                        var idx_costcenter = (Label)_nrow.FindControl("idx_costcenter");
                        var _details_purchase = (Label)_nrow.FindControl("_details_purchase");

                        ddllocation.SelectedValue = idx_location.Text;
                        ddlpurchasetype.SelectedValue = idx_purchase.Text;
                        _ddlcostCenter.SelectedValue = idx_costcenter.Text;
                        txtdetails_purchase.Text = _details_purchase.Text;

                    }
                    if (ddlpurchasetype.Text == "1")
                    {
                        _panel_upload_refercutoff.Visible = false;
                        _panel_upload_cutoff.Visible = false;
                    }
                    
                    else if (ddlpurchasetype.Text == "2")
                    {

                        _panel_upload_organization.Visible = false;
                    }

                    //Panel _panel_upload_organization = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_organization");
                    //Panel _panel_upload_cutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_cutoff");
                    //Panel _panel_upload_refercutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_refercutoff");
                }

                //else if (fvCreatePurchasequipment.CurrentMode == FormViewMode.Insert)
                //{
                //    var btnsavepurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnsavepurchase");
                //    var btnaddpurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");

                //    linkBtnTrigger(btnsavepurchase);
                //    linkBtnTrigger(btnaddpurchase);
                //    linkBtnTrigger(btnupdatepurchase);




                //    // var txtspecidx = (TextBox)fvCreatePurchasequipment.FindControl("txtspecidx"); 
                //    // var ddlspecidx = (DropDownList)fvCreatePurchasequipment.FindControl("ddlspecidx");
                //    // DropDownList ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");


                //    // // RadioButton radioButtonList = new RadioButton();

                //    // _data_purchase.master_spec_list = new master_spec_details[1];
                //    // master_spec_details _selectspec = new master_spec_details();

                //    // //if (ddlequipmenttype.SelectedValue != "0")
                //    // //{

                //    //// _selectspec.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedItem.Value);
                //    // //}



                //    // _data_purchase.master_spec_list[0] = _selectspec;

                //    // //call service
                //    // _data_purchase = callServicePurchase(_urlGetmasterspec, _data_purchase);


                //    // ddlspecidx.DataSource = _data_purchase.master_spec_list;
                //    // ddlspecidx.DataTextField = "name_spec";
                //    // ddlspecidx.DataValueField = "m0_spec_idx";
                //    // ddlspecidx.DataBind();

                //}

                break;

            case "fvresultpurchase":

                if (fvresultpurchase.CurrentMode == FormViewMode.Edit)
                {

                    Panel Panel_costcenter = (Panel)fvresultpurchase.FindControl("Panel_costcenter");
                    Panel_costcenter.Visible = false;
      

                }


                break;

            case "fvmanage_approvepurchase":

                if (fvmanage_approvepurchase.CurrentMode == FormViewMode.Edit)
                {
                    //costcenter
                    var txt_costcenter_idx = (TextBox)fvmanage_approvepurchase.FindControl("txt_costcenter_idx");
                    DropDownList ddllcostcenter = (DropDownList)fvmanage_approvepurchase.FindControl("ddllcostcenter");

                    data_purchase data_purchase_edit = new data_purchase();
                    data_purchase_edit.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details select_costcenter_edit = new u0_purchase_equipment_details();

                    ddllcostcenter.AppendDataBoundItems = true;
                    ddllcostcenter.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));

                    data_purchase_edit.u0_purchase_equipment_list[0] = select_costcenter_edit;
                    data_purchase_edit = callServicePurchase(_urlGetcostcenter, data_purchase_edit);

                    ddllcostcenter.DataSource = data_purchase_edit.u0_purchase_equipment_list;
                    ddllcostcenter.DataTextField = "costcenter_no";
                    ddllcostcenter.DataValueField = "costcenter_idx";
                    ddllcostcenter.DataBind();
                    ddllcostcenter.SelectedValue = txt_costcenter_idx.Text;
                    ddllcostcenter.Enabled = false;
                }

                break;
        }

    }

    #endregion

    #region setGridViewDataBind
    protected void setGridViewDataBind(GridView gridViewName, Object obj)
    {
        gridViewName.DataSource = ViewState["purchase_list"];//obj;
        gridViewName.DataBind();
    }
    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchase_equipmentlist":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients");

                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    Label _nodeidx = (Label)e.Row.FindControl("lb_nodeidx");
                    Label _actoridx = (Label)e.Row.FindControl("lb_actoridx");
                    Label u0_purchase_idx = (Label)e.Row.FindControl("u0_purchase_idx");
                    LinkButton btnmanage_purchase = (LinkButton)e.Row.FindControl("btnmanage_purchase");



                    GridView gvitems = (GridView)e.Row.Cells[2].FindControl("gvitems");


                    data_purchase _reportItemspurchase = new data_purchase();
                    _reportItemspurchase.items_purchase_list = new items_purchase_details[1];


                    items_purchase_details _reportingItems = new items_purchase_details();

                    _reportingItems.u0_purchase_idx = int.Parse(u0_purchase_idx.Text);



                    _reportItemspurchase.items_purchase_list[0] = _reportingItems;

                    _reportItemspurchase = callServicePurchase(_urlGetitems_purchase, _reportItemspurchase);
                    ViewState["itemslist"] = _reportItemspurchase.items_purchase_list;

                    

                    if (_reportItemspurchase.return_code == "0")
                    {
                      

                        gvitems.DataSource = ViewState["itemslist"];
                        gvitems.DataBind();


                    }

                   

                    var rptcommentfromit = (Repeater)e.Row.FindControl("rptcommentfromit");

                    rptcommentfromit.DataSource = ViewState["itemslist"]; ;
                    rptcommentfromit.DataBind();

                    //foreach (GridViewRow row_gvitems in gvitems.Rows)
                    //{

                    //    Label u2_purchaserr_report = (Label)row_gvitems.FindControl("u2_purchaserr_report");

                    //    var rptcommentfromit = (Repeater)e.Row.FindControl("rptcommentfromit"); 


                    //}

                    //linkBtnTrigger(btnmanage_purchase);

                    ViewState["_idnode"] = _nodeidx.Text;
                    ViewState["_idactor"] = _actoridx.Text;

                    if (int.Parse(ViewState["emp_idx"].ToString()) == assistant_director_mis
                        || int.Parse(ViewState["emp_idx"].ToString()) == manager
                       // || int.Parse(ViewState["emp_idx"].ToString()) == hr_director
                        || int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport) //แสดง colums select (checkbox) เมื่อเป็น พี่พน หรือ คุณบอย หรือ hr director หรือพี่เกม
                    {

                        gvpurchase_equipmentlist.Columns[0].Visible = true;

                        if (int.Parse(ViewState["emp_idx"].ToString()) == assistant_director_mis
                            || int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport
                            || int.Parse(ViewState["emp_idx"].ToString()) == manager)
                        {
                            gvpurchase_equipmentlist.Columns[5].Visible = true;

                            if (ViewState["_idnode"].ToString() == "9")
                            {

                                cbrecipients.Visible = false;

                            }

                        }
                        else
                        {

                            gvpurchase_equipmentlist.Columns[5].Visible = false;

                        }


                        if (check_approve_all.Checked || bool.Parse(hfSelected.Value) == true)
                        {
                            cbrecipients.Checked = true;

                            if (check_approve_all.Checked && cbrecipients.Checked)
                            {
                                cbrecipients.Enabled = false;

                            }

                        }
                        else
                        {
                            cbrecipients.Checked = false;
                            cbrecipients.Enabled = true;

                        }

                    }

                    else if (int.Parse(ViewState["emp_idx"].ToString()) != assistant_director_mis
                        || int.Parse(ViewState["emp_idx"].ToString()) != manager
                       // || int.Parse(ViewState["emp_idx"].ToString()) != hr_director
                        || int.Parse(ViewState["emp_idx"].ToString()) != head_itsupport) //ซ่อน colums select (checkbox) เมื่อไม่ใช่ พี่พน หรือ คุณบอย หรือ hr director หรือพี่เกม
                    {

                        gvpurchase_equipmentlist.Columns[0].Visible = false;

                    }

                }


                break;

            case "gvlist_summarypurchase":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    //DropDownList ddlprice_equiptment = (DropDownList)e.Row.Cells[3].FindControl("ddlprice_equiptment");

                    //data_purchase __data_purchase = new data_purchase();
                    //__data_purchase.m0_price_equipment_list = new m0_price_equipment_details[1];
                    //m0_price_equipment_details select_price = new m0_price_equipment_details();

                    //ddlprice_equiptment.AppendDataBoundItems = true;
                    //ddlprice_equiptment.Items.Add(new ListItem("กรุณาเลือกราคาของอุุปกรณ์", "0"));

                    //__data_purchase.m0_price_equipment_list[0] = select_price;
                    //__data_purchase = callServicePurchase(_urlGetprice_equipment, __data_purchase);

                    //ddlprice_equiptment.DataSource = __data_purchase.m0_price_equipment_list;
                    //ddlprice_equiptment.DataTextField = "price_equipment";
                    //ddlprice_equiptment.DataValueField = "m0_price_idx";
                    //ddlprice_equiptment.DataBind();

                }

                break;


            case "gvpurchaselist":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    TextBox txtupdate_purchasetype = (TextBox)e.Row.FindControl("txtupdate_purchasetype");
                    TextBox txtupdate_equipmenttype = (TextBox)e.Row.FindControl("txtupdate_equipmenttype");
                    TextBox txtupdate_spec = (TextBox)e.Row.FindControl("txtupdate_spec");
                    TextBox txtupdate_detailspec = (TextBox)e.Row.FindControl("txtupdate_detailspec");

                    DropDownList ddlpurchasetype_edit = (DropDownList)e.Row.FindControl("ddlpurchasetype_edit");
                    DropDownList ddlequipment_edit = (DropDownList)e.Row.FindControl("ddlequipment_edit");
                    DropDownList ddlspec_edit = (DropDownList)e.Row.FindControl("ddlspec_edit");

                    Label lbtiitlespec = (Label)e.Row.FindControl("lbtiitlespec");

                    //:::::::::::::::::::::: SELECT DATA PURCHASE FOR EDIT BEFORE INSERT TO TABLE :::::::::::::::::://

                    _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
                    m0_type_purchase_equipment_detail select_purchase_edit = new m0_type_purchase_equipment_detail();

                    ddlpurchasetype_edit.AppendDataBoundItems = true;
                    ddlpurchasetype_edit.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

                    select_purchase_edit.status_equipment = 1;

                    _data_purchase.m0_type_purchase_equipment_list[0] = select_purchase_edit;
                    _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);

                    ddlpurchasetype_edit.DataSource = _data_purchase.m0_type_purchase_equipment_list;
                    ddlpurchasetype_edit.DataTextField = "name_purchase_type";
                    ddlpurchasetype_edit.DataValueField = "m0_purchase_type_idx";
                    ddlpurchasetype_edit.DataBind();
                    ddlpurchasetype_edit.SelectedValue = txtupdate_purchasetype.Text;

                    //:::::::::::::::::::::: SELECT DATA EQUIPMENT TYPE FOR EDIT BEFORE INSERT TO TABLE :::::::::::::::::://


                    _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
                    m0_type_purchase_equipment_detail select_equipment_edit = new m0_type_purchase_equipment_detail();

                    ddlequipment_edit.AppendDataBoundItems = true;
                    ddlequipment_edit.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์", "0"));

                    select_equipment_edit.status_equipment = 9;

                    _data_purchase.m0_type_purchase_equipment_list[0] = select_equipment_edit;
                    _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);


                    ddlequipment_edit.DataSource = _data_purchase.m0_type_purchase_equipment_list;
                    ddlequipment_edit.DataTextField = "name_equipment_type";
                    ddlequipment_edit.DataValueField = "m0_equipment_type_idx";
                    ddlequipment_edit.DataBind();
                    ddlequipment_edit.SelectedValue = txtupdate_equipmenttype.Text;

                    //:::::::::::::::::::::: SELECT DATA SPEC TYPE FOR EDIT BEFORE INSERT TO TABLE :::::::::::::::::://


                    if (ddlequipment_edit.SelectedValue == "1" || ddlequipment_edit.SelectedValue == "2" || ddlequipment_edit.SelectedValue == "4")
                    {

                        data_purchase _spec_qpuiment = new data_purchase();
                        _spec_qpuiment.master_spec_list = new master_spec_details[1];
                        master_spec_details select_masterspec = new master_spec_details();

                        _spec_qpuiment.master_spec_list[0] = select_masterspec;

                        _spec_qpuiment = callServicePurchase(_urlGetmasterspec, _spec_qpuiment);

                        ddlspec_edit.DataSource = _spec_qpuiment.master_spec_list;
                        ddlspec_edit.DataTextField = "name_spec";
                        ddlspec_edit.DataValueField = "m0_spec_idx";
                        ddlspec_edit.DataBind();
                        ddlspec_edit.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));
                        ddlspec_edit.SelectedValue = txtupdate_spec.Text;



                    }

                    else
                    {

                        ddlspec_edit.Visible = false;
                        lbtiitlespec.Visible = false;
                        txtupdate_detailspec.Enabled = true;
                    }



                }

                break;


            case "gvEditpurchaselist":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox txtedit_detailspec = (TextBox)e.Row.FindControl("txtedit_detailspec");
                    TextBox txtupdate_m0_spec_idx = (TextBox)e.Row.FindControl("txtupdate_m0_spec_idx");
                    TextBox txtedit_expenditure = (TextBox)e.Row.FindControl("txtedit_expenditure");
                    TextBox txtedit_equipmenttype = (TextBox)e.Row.FindControl("txtedit_equipmenttype");
                    TextBox txtedit_purchasetype = (TextBox)e.Row.FindControl("txtedit_purchasetype");
                    TextBox txteditUnit = (TextBox)e.Row.FindControl("txteditUnit");
                    TextBox txtcostcenter_idxedit = (TextBox)e.Row.FindControl("txtcostcenter_idxedit");
                    TextBox txteditlocation_idx = (TextBox)e.Row.FindControl("txteditlocation_idx");
                    TextBox txteditposition = (TextBox)e.Row.FindControl("txteditposition");

                    Label Labelm0_spec_idx = (Label)e.Row.FindControl("Labelm0_spec_idx");

                    DropDownList ddlequipment_typeedit  = (DropDownList)e.Row.FindControl("ddlequipment_typeedit");
                    DropDownList ddlm0_spec_idx = (DropDownList)e.Row.FindControl("ddlm0_spec_idx");
                    DropDownList ddlpurchase_typeedit = (DropDownList)e.Row.FindControl("ddlpurchase_typeedit");
                    DropDownList ddleditUnit = (DropDownList)e.Row.FindControl("ddleditUnit");
                    DropDownList ddledit_costcenter = (DropDownList)e.Row.FindControl("ddledit_costcenter");
                    DropDownList ddledit_location = (DropDownList)e.Row.FindControl("ddledit_location");
                    DropDownList ddleditposition = (DropDownList)e.Row.FindControl("ddleditposition");




              
                    //:::::::::::::::::::::: SELECT DATA EQUIPMENT TYPE FOR EDIT:::::::::::::::::://

                    data_purchase _data_purchase_edit1 = new data_purchase();

                    _data_purchase_edit1.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
                    m0_type_purchase_equipment_detail select_equipmentype_edit = new m0_type_purchase_equipment_detail();

                 

                    ddlequipment_typeedit.AppendDataBoundItems = true;
                    ddlequipment_typeedit.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์ขอซื้อ", "0"));

                    select_equipmentype_edit.status_equipment = 9;

                    _data_purchase_edit1.m0_type_purchase_equipment_list[0] = select_equipmentype_edit;
                    _data_purchase_edit1 = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase_edit1);

                  //  ViewState["test"] = _data_purchase_edit1.m0_type_purchase_equipment_list[0].m0_unit_idx.ToString();

                    ddlequipment_typeedit.DataSource = _data_purchase_edit1.m0_type_purchase_equipment_list;
                    ddlequipment_typeedit.DataTextField = "name_equipment_type";
                    ddlequipment_typeedit.DataValueField = "m0_equipment_type_idx";
                    ddlequipment_typeedit.DataBind();
                    ddlequipment_typeedit.SelectedValue = txtedit_equipmenttype.Text;
                    ViewState["_UNIT"] = int.Parse(ddlequipment_typeedit.SelectedValue);
                    //ViewState["_UNIT"] = _data_purchase_edit1.m0_type_purchase_equipment_list[0].nameth_unit.ToString();
                    // ViewState["_EQUIPMENTUNIT"] = _data_purchase_edit1.m0_type_purchase_equipment_list[0].nameth_unit;
                    //txteditUnit.Text = _data_purchase_edit1.m0_type_purchase_equipment_list[0].nameth_unit.ToString();


                    //:::::::::::::::::::::: SELECT DATA SPEC EQUIPMENT FOR EDIT:::::::::::::::::://

                    if (ddlequipment_typeedit.SelectedValue == "1" || ddlequipment_typeedit.SelectedValue == "2" || ddlequipment_typeedit.SelectedValue == "4")
                    {


                        data_purchase _data_purchase_edit = new data_purchase();
                        _data_purchase_edit.master_spec_list = new master_spec_details[1];
                        master_spec_details select_masterspec_edit = new master_spec_details();

                        _data_purchase_edit.master_spec_list[0] = select_masterspec_edit;

                        _data_purchase_edit = callServicePurchase(_urlGetmasterspec, _data_purchase_edit);

                        ddlm0_spec_idx.DataSource = _data_purchase_edit.master_spec_list;
                        ddlm0_spec_idx.DataTextField = "name_spec";
                        ddlm0_spec_idx.DataValueField = "m0_spec_idx";
                        ddlm0_spec_idx.DataBind();
                        ddlm0_spec_idx.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));
                        ddlm0_spec_idx.SelectedValue = txtupdate_m0_spec_idx.Text;


                    }

                    else

                    {
                        ddlm0_spec_idx.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));
                        Labelm0_spec_idx.Visible = false;
                        ddlm0_spec_idx.Visible = false;


                    }








                    //:::::::::::::::::::::: SELECT DATA PURCHASE TYPE FOR EDIT:::::::::::::::::://


                    data_purchase _data_purchase_edit2 = new data_purchase();

                    _data_purchase_edit2.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
                    m0_type_purchase_equipment_detail select_purchase_edit = new m0_type_purchase_equipment_detail();

                    ddlpurchase_typeedit.AppendDataBoundItems = true;
                    ddlpurchase_typeedit.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

                    select_purchase_edit.status_equipment = 1;

                    _data_purchase_edit2.m0_type_purchase_equipment_list[0] = select_purchase_edit;
                    _data_purchase_edit2 = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase_edit2);

                    ddlpurchase_typeedit.DataSource = _data_purchase_edit2.m0_type_purchase_equipment_list;
                    ddlpurchase_typeedit.DataTextField = "name_purchase_type";
                    ddlpurchase_typeedit.DataValueField = "m0_purchase_type_idx";
                    ddlpurchase_typeedit.DataBind();
                    ddlpurchase_typeedit.SelectedValue = txtedit_purchasetype.Text;


                    //:::::::::::::::::::::: SELECT DATA COST CENTER FOR EDIT:::::::::::::::::://


                    data_purchase _data_purchase_edit4 = new data_purchase();
                    _data_purchase_edit4.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details select_costcenter_edit1 = new u0_purchase_equipment_details();

                    ddledit_costcenter.AppendDataBoundItems = true;
                    ddledit_costcenter.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));

                    _data_purchase_edit4.u0_purchase_equipment_list[0] = select_costcenter_edit1;
                    _data_purchase_edit4 = callServicePurchase(_urlGetcostcenter, _data_purchase_edit4);

                    ddledit_costcenter.DataSource = _data_purchase_edit4.u0_purchase_equipment_list;
                    ddledit_costcenter.DataTextField = "costcenter_no";
                    ddledit_costcenter.DataValueField = "costcenter_idx";
                    ddledit_costcenter.DataBind();
                    ddledit_costcenter.SelectedValue = txtcostcenter_idxedit.Text;


                    //:::::::::::::::::::::: SELECT DATA LOCATION PURCHASE FOR EDIT:::::::::::::::::://


                    data_purchase _data_purchase_edit5 = new data_purchase();
                    _data_purchase_edit5.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details select_location_edit = new u0_purchase_equipment_details();

                    ddledit_location.AppendDataBoundItems = true;
                    ddledit_location.Items.Add(new ListItem("กรุณาเลือกสถานที่", "0"));

                    _data_purchase_edit5.u0_purchase_equipment_list[0] = select_location_edit;
                    _data_purchase_edit5 = callServicePurchase(_urlGetlocation, _data_purchase_edit5);

                    ddledit_location.DataSource = _data_purchase_edit5.u0_purchase_equipment_list;
                    ddledit_location.DataTextField = "name_location";
                    ddledit_location.DataValueField = "m0_location_idx";
                    ddledit_location.DataBind();
                    ddledit_location.SelectedValue = txteditlocation_idx.Text;

                    /*
                    data_purchase _datapositionEdit = new data_purchase();
                    _datapositionEdit._organization_list = new _organization_details[1];
                    _organization_details edit_position = new _organization_details();


                    edit_position.emp_idx = Int32.Parse(ViewState["emp_idx"].ToString());

                    _datapositionEdit._organization_list[0] = edit_position;
                    //  Literal2.Text = ddlsearch_department.SelectedValue;

                    _datapositionEdit = callServicePurchase(_urlGetposition, _datapositionEdit);

                    ddleditposition.DataSource = _datapositionEdit._organization_list;
                    ddleditposition.DataTextField = "PosNameTH";
                    ddleditposition.DataValueField = "RPosIDX";
                    ddleditposition.DataBind();
                    ddleditposition.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง", "0"));
                    ddleditposition.SelectedValue = txteditposition.Text;
                    */
                    data_itasset _dataitasset = new data_itasset();
                    _dataitasset.its_lookup_action = new its_lookup[1];
                    its_lookup obj_position = new its_lookup();

                    obj_position.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
                    obj_position.operation_status_id = "position";

                    _dataitasset.its_lookup_action[0] = obj_position;
                    _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

                    ddleditposition.DataSource = _dataitasset.its_lookup_action;
                    ddleditposition.DataTextField = "zname";
                    ddleditposition.DataValueField = "idx";
                    ddleditposition.DataBind();
                    ddleditposition.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง", "0"));

                    ddlpurchase_typeedit.Focus();
                }


                break;

            case "gvItemspurchase":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    data_purchase _dataselect_io = new data_purchase();
                    data_purchase _dataselect_asset = new data_purchase();



                    int index = e.Row.RowIndex;
                    Label _nodeidxItems = (Label)e.Row.FindControl("lbnodeidxItems");
                    Label _actoridxItems = (Label)e.Row.FindControl("lbactoridxItems");
                    Label _quantity = (Label)e.Row.FindControl("items_quantity");
                    TextBox txtcommentformITSupport = (TextBox)e.Row.FindControl("txtcommentformITSupport");
                    TextBox txtpriceidx = (TextBox)e.Row.FindControl("txtpriceidx");
                    TextBox txtNOAsset = (TextBox)e.Row.FindControl("txtNOAsset");
                    TextBox txtNoIO = (TextBox)e.Row.FindControl("txtNoIO");
                    DropDownList ddlprice_equipment = (DropDownList)e.Row.FindControl("ddlprice_equipment");
                    GridView gvionumber = (GridView)e.Row.FindControl("gvionumber");
                    GridView gvassetnumber = (GridView)e.Row.FindControl("gvassetnumber");
                    Panel price_comment_complete = (Panel)e.Row.FindControl("price_comment_complete");
                    TextBox txtminprice = (TextBox)e.Row.FindControl("txtminprice");
                    TextBox txtmaxprice = (TextBox)e.Row.FindControl("txtmaxprice");
                    Label lbto = (Label)e.Row.FindControl("lbto");
                    Label _idxnumber = (Label)e.Row.FindControl("_lbItemsnumber");
                    Label lbnumitemsio = (Label)e.Row.FindControl("lbnumitemsio");
                    Label lbnumitemsasset = (Label)e.Row.FindControl("lbnumitemsasset");
                    LinkButton btn_addionumber = (LinkButton)e.Row.FindControl("btn_addionumber");
                    LinkButton btnaddasset = (LinkButton)e.Row.FindControl("btnaddasset");

                    
                    ViewState["_NODEIDX"] = _nodeidxItems.Text;
                    ViewState["_ACTORIDX"] = _actoridxItems.Text;

                    // :::: start select number io ::://
                    _dataselect_io.items_purchase_list = new items_purchase_details[1];
                    items_purchase_details _selectnumberio = new items_purchase_details();
                    _selectnumberio.u2_purchase_idx = int.Parse(_idxnumber.Text);
                    _dataselect_io.items_purchase_list[0] = _selectnumberio;
                    _dataselect_io = callServicePurchase(_urlgetionumber, _dataselect_io);
                    ViewState["data_ionumber"] = _dataselect_io.numberio_list;

                    // :::: end select number io ::://

                    // :::: start select number asset ::://
                    _dataselect_asset.items_purchase_list = new items_purchase_details[1];
                    items_purchase_details _selectnumberasset = new items_purchase_details();
                    _selectnumberasset.u2_purchase_idx = int.Parse(_idxnumber.Text);
                    _dataselect_asset.items_purchase_list[0] = _selectnumberasset;
                    _dataselect_asset = callServicePurchase(_urlgetassetnumber, _dataselect_asset);
                    ViewState["data_assetnumber"] = _dataselect_asset.numberAsset_list;

                    // :::: end select number asset ::://

                    if (ViewState["_NODEIDX"].ToString() == "1" && ViewState["_ACTORIDX"].ToString() == "1"
                        || ViewState["_NODEIDX"].ToString() == "2" && ViewState["_ACTORIDX"].ToString() == "2"
                        || ViewState["_NODEIDX"].ToString() == "3" && ViewState["_ACTORIDX"].ToString() == "3")
                    {

                        gvItemspurchase.Columns[7].Visible = false;
                        gvItemspurchase.Columns[6].Visible = false;
                        gvItemspurchase.Columns[5].Visible = false;


                    }
                    else if (ViewState["_NODEIDX"].ToString() == "4" && int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
                    {

                        txtcommentformITSupport.Enabled = true;
                        gvItemspurchase.Columns[5].Visible = true;
                        txtcommentformITSupport.Visible = true;
                        txtmaxprice.Visible = true;
                        txtminprice.Visible = true;
                        lbto.Visible = true;


                    }
                    if (ViewState["_NODEIDX"].ToString() == "15" && int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport
                       || ViewState["_NODEIDX"].ToString() == "16" && int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport
                       || ViewState["_NODEIDX"].ToString() == "6" && int.Parse(ViewState["emp_idx"].ToString()) == assistant_director_mis
                       || ViewState["_NODEIDX"].ToString() == "7" && int.Parse(ViewState["emp_idx"].ToString()) == manager)
                    {

                        gvItemspurchase.Columns[5].Visible = true;
                        price_comment_complete.Visible = true;



                    }
                    else if (ViewState["_NODEIDX"].ToString() == "5" && int.Parse(ViewState["rdept_idx"].ToString()) == budget)

                    {

                        gvItemspurchase.Columns[5].Visible = true;
                        price_comment_complete.Visible = true;

                    }


                     if (ViewState["_NODEIDX"].ToString() == "11" && int.Parse(ViewState["rdept_idx"].ToString()) == budget)
                    {

                        gvItemspurchase.Columns[6].Visible = true;
                        gvItemspurchase.Columns[5].Visible = true;
                        price_comment_complete.Visible = true;
                        txtNoIO.Visible = true;
                        btn_addionumber.Visible = true;
                        lbnumitemsio.Visible = true;

                    }

                    else if (ViewState["_NODEIDX"].ToString() == "8" && int.Parse(ViewState["rdept_idx"].ToString()) == asset)
                    {

                        txtNOAsset.Enabled = true;
                        gvItemspurchase.Columns[5].Visible = true;
                        gvItemspurchase.Columns[6].Visible = true;
                        gvItemspurchase.Columns[7].Visible = true;
                        price_comment_complete.Visible = true;
                        txtNOAsset.Visible = true;
                        btnaddasset.Visible = true;
                        lbnumitemsasset.Visible = true;

                        gvionumber.DataSource = ViewState["data_ionumber"];//_dataselect_io_asset.numberio_list;
                        gvionumber.DataBind();

                    }

                    else if (ViewState["_NODEIDX"].ToString() == "9")
                    {

                        gvItemspurchase.Columns[5].Visible = true;
                        gvItemspurchase.Columns[6].Visible = true;
                        gvItemspurchase.Columns[7].Visible = true;
                        price_comment_complete.Visible = true;

                        gvassetnumber.DataSource = ViewState["data_assetnumber"];//_dataselect_io_asset.numberAsset_list;
                        gvassetnumber.DataBind();

                        gvionumber.DataSource = ViewState["data_ionumber"];//_dataselect_io_asset.numberio_list;
                        gvionumber.DataBind();




                    }

                }

                    break;

            case "gvionumber":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    var _gvitems = (GridView)sender;
                    var row_showio_number = (GridViewRow)_gvitems.NamingContainer;

                    GridView gvionumber = (GridView)row_showio_number.FindControl("gvionumber");

                    if (ViewState["_NODEIDX"].ToString() == "11" && int.Parse(ViewState["rdept_idx"].ToString()) == budget)
                    {

                        gvionumber.Columns[2].Visible = true;

                    }
                    else
                    {

                        gvionumber.Columns[2].Visible = false;

                    }
                    


                }

       


                break;

            case "gvassetnumber":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    var gvitems = (GridView)sender;
                    var row_showasset_number = (GridViewRow)gvitems.NamingContainer;

                    GridView gvassetnumber = (GridView)row_showasset_number.FindControl("gvassetnumber");

                    if (ViewState["_NODEIDX"].ToString() == "8" && int.Parse(ViewState["rdept_idx"].ToString()) == asset)
                    {

                        gvassetnumber.Columns[2].Visible = true;

                    }
                    else
                    {

                        gvassetnumber.Columns[2].Visible = false;

                    }


                  
                }



                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }


                break;

            case "gvReport":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label u0_purchase_report = (Label)e.Row.Cells[0].FindControl("u0_purchase_report");
                    Label status_purchasereport = (Label)e.Row.Cells[3].FindControl("status_purchasereport");
                    Label idx_status = (Label)e.Row.Cells[3].FindControl("idx_status");

                    GridView gvReportitems = (GridView)e.Row.Cells[2].FindControl("gvReportitems");


                    data_purchase _reportItemspurchase = new data_purchase();
                    _reportItemspurchase.items_purchase_list = new items_purchase_details[1];


                    items_purchase_details _reportingItems = new items_purchase_details();

                    _reportingItems.u0_purchase_idx = int.Parse(u0_purchase_report.Text);



                    _reportItemspurchase.items_purchase_list[0] = _reportingItems;

                    _reportItemspurchase = callServicePurchase(_urlGetitems_purchase, _reportItemspurchase);

                    ViewState["_SESSION_PRINTREPORT"] = _reportItemspurchase.items_purchase_list;

                    gvReportitems.DataSource = ViewState["_SESSION_PRINTREPORT"];//_reportItemspurchase.items_purchase_list;
                    gvReportitems.DataBind();


                    foreach (GridViewRow row_report in gvReportitems.Rows)
                    {
                        var _lbu2idxreport = (Label)row_report.FindControl("_lbu2idxreport");
                        GridView _gvionumber_report = (GridView)row_report.FindControl("gvionumber_report");
                        GridView _gvassetnumber_report = (GridView)row_report.FindControl("gvassetnumber_report");

                        //:::: start select number io::://
                        data_purchase _data_ioreport = new data_purchase();
                        _data_ioreport.items_purchase_list = new items_purchase_details[1];
                        items_purchase_details _selectnumberio = new items_purchase_details();
                        _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                        _data_ioreport.items_purchase_list[0] = _selectnumberio;
                        _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);

                        _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                        _gvionumber_report.DataBind();
                        //:::: end select number io::://


                        // :::: start select number asset ::://
                        data_purchase _data_assetreport = new data_purchase();
                        _data_assetreport.items_purchase_list = new items_purchase_details[1];
                        items_purchase_details _selectnumberasset = new items_purchase_details();
                        _selectnumberasset.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                        _data_assetreport.items_purchase_list[0] = _selectnumberasset;
                        _data_assetreport = callServicePurchase(_urlgetassetnumber, _data_assetreport);

                        _gvassetnumber_report.DataSource = _data_assetreport.numberAsset_list;
                        _gvassetnumber_report.DataBind();

                        // :::: end select number asset::://



                    }
                }



                    break;


            case "GridView1":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }


                break;

            case "gvionumber_report":

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    data_purchase _data_ioreport = new data_purchase();
                //    data_purchase _data_assetreport = new data_purchase();



                //    GridView gvReport = (GridView)viewReport.FindControl("gvReport");

                //    foreach (GridViewRow row_report in gvReport.Rows)
                //    {

                //        GridView gvReportitems = (GridView)row_report.FindControl("gvReportitems");
                //        Label _lbu2idxreport = (Label)row_report.FindControl("_lbu2idxreport");

                //        foreach (GridViewRow row_items in gvReportitems.Rows)
                //        {

                //            GridView _gvionumber_report = (GridView)row_items.FindControl("gvionumber_report");


                //            _data_ioreport.items_purchase_list = new items_purchase_details[1];
                //            items_purchase_details _selectnumberio = new items_purchase_details();
                //            _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                //            _data_ioreport.items_purchase_list[0] = _selectnumberio;
                //            _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);
                //            // ViewState["data_ionumber_report"] = _data_ioreport.numberio_list;

                //            _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                //            _gvionumber_report.DataBind();



                //        }




                        //for (int i = 0; i <= gvReportitems.Rows.Count - 1; i++)

                        //{

                        //    var _gvionumber_report = (gvReportitems.Rows[i].Cells[4].FindControl("gvionumber_report") as GridView);
                        //    var _gvassetnumber_report = (gvReportitems.Rows[i].Cells[5].FindControl("gvassetnumber_report") as GridView);

                        //    //:::: start select number io::://

                        //    _data_ioreport.items_purchase_list = new items_purchase_details[1];
                        //    items_purchase_details _selectnumberio = new items_purchase_details();
                        //    _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                        //    _data_ioreport.items_purchase_list[0] = _selectnumberio;
                        //    _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);
                        //    // ViewState["data_ionumber_report"] = _data_ioreport.numberio_list;

                        //    _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                        //    _gvionumber_report.DataBind();


                        //        // :::: end select number io ::://


                        //}

                //    }

                //}


                    //if (e.Row.RowType == DataControlRowType.DataRow)
                    //{


                    //    data_purchase _data_ioreport = new data_purchase();
                    //    data_purchase _data_assetreport = new data_purchase();


                    //    var gv_report = (GridView)sender;
                    //    var row_show_report = (GridViewRow)gv_report.NamingContainer;

                    //    var _gvReportitems = (GridView)row_show_report.FindControl("gvReportitems");
                    //    Label _lbu2idxreport = (Label)row_show_report.FindControl("_lbu2idxreport");

                    //    var row_show_io_asset = (GridViewRow)_gvReportitems.NamingContainer;


                    //    GridView _gvassetnumber_report = (GridView)row_show_io_asset.FindControl("gvassetnumber_report");
                    //    GridView _gvionumber_report = (GridView)row_show_io_asset.FindControl("gvionumber_report");


                    //    //:::: start select number io::://

                    //    _data_ioreport.items_purchase_list = new items_purchase_details[1];
                    //    items_purchase_details _selectnumberio = new items_purchase_details();
                    //    _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                    //    _data_ioreport.items_purchase_list[0] = _selectnumberio;
                    //    _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);
                    //    // ViewState["data_ionumber_report"] = _data_ioreport.numberio_list;

                    //    _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                    //    _gvionumber_report.DataBind();


                    //    // :::: end select number io ::://



                    //}





                    break;


            case "gvReportitems":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //data_purchase _data_ioreport = new data_purchase();
                    //data_purchase _data_assetreport = new data_purchase();


                    //var gv_report = (GridView)sender;
                    //var row_show_report = (GridViewRow)gv_report.NamingContainer;

                    //var _gvReportitems = (GridView)row_show_report.FindControl("gvReportitems");
                    //Label _lbu2idxreport = (Label)row_show_report.FindControl("_lbu2idxreport");

                    //if (e.Row.RowType == DataControlRowType.DataRow)
                    //{

                    //    GridView _gvassetnumber_report = (GridView)e.Row.Cells[5].FindControl("gvassetnumber_report");
                    //    GridView _gvionumber_report = (GridView)e.Row.Cells[4].FindControl("gvionumber_report");

                        
                    //     //:::: start select number io::://

                    //     _data_ioreport.items_purchase_list = new items_purchase_details[1];
                    //    items_purchase_details _selectnumberio = new items_purchase_details();
                    //    _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                    //    _data_ioreport.items_purchase_list[0] = _selectnumberio;
                    //    _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);
                    //    // ViewState["data_ionumber_report"] = _data_ioreport.numberio_list;

                    //    _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                    //    _gvionumber_report.DataBind();


                    //    // :::: end select number io ::://



                  //  }





                    //var gvitems_report = (GridView)sender;
                    //var row_io_asset = (GridViewRow)gvitems_report.NamingContainer;

                    //GridView _gvionumber_report = (GridView)row_io_asset.FindControl("gvionumber_report");
                    //GridView _gvassetnumber_report = (GridView)row_io_asset.FindControl("gvassetnumber_report");

                    //var _gvReportitems = (GridView)gvitems_report.FindControl("gvReportitems");




                    //Label _lbu2idxreport = (Label)row_showasset_number.FindControl("_lbu2idxreport");
                    //GridView _gvionumber_report = (GridView)row_showasset_number.FindControl("gvionumber_report");
                    //GridView _gvassetnumber_report = (GridView)row_showasset_number.FindControl("gvassetnumber_report");


                    // :::: start select number io ::://

                    // _data_ioreport.items_purchase_list = new items_purchase_details[1];
                    // items_purchase_details _selectnumberio = new items_purchase_details();
                    // _selectnumberio.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                    // _data_ioreport.items_purchase_list[0] = _selectnumberio;
                    // _data_ioreport = callServicePurchase(_urlgetionumber, _data_ioreport);
                    //// ViewState["data_ionumber_report"] = _data_ioreport.numberio_list;

                    // _gvionumber_report.DataSource = _data_ioreport.numberio_list;
                    // _gvionumber_report.DataBind();


                    // // :::: end select number io ::://


                    // // :::: start select number asset ::://

                    // _data_assetreport.items_purchase_list = new items_purchase_details[1];
                    // items_purchase_details _selectnumberasset = new items_purchase_details();
                    // _selectnumberasset.u2_purchase_idx = int.Parse(_lbu2idxreport.Text);
                    // _data_assetreport.items_purchase_list[0] = _selectnumberasset;
                    // _data_assetreport = callServicePurchase(_urlgetassetnumber, _data_assetreport);
                    //// ViewState["data_assetnumber_report"] = _data_assetreport.numberAsset_list;

                    // _gvassetnumber_report.DataSource = _data_assetreport.numberAsset_list;
                    // _gvassetnumber_report.DataBind();

                    // :::: end select number asset ::://
                }

                break;
        }
    }

    #endregion

    #region gvRowCancelingEdit

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchaselist":

                GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");

                gvpurchaselist.EditIndex = -1;
                gvpurchaselist.DataSource = ViewState["Create_purchase_speclist"];
                gvpurchaselist.DataBind();

                break;

            case "gvEditpurchaselist":

                gvEditpurchaselist.EditIndex = -1;
                action_select_purchase_for_edit();
                SETFOCUS_ONTOP.Focus();


                break;
        }
    }

    #endregion

    #region gvRowDeleted

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchaselist":

                GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                var DeletePosisionRow = (DataSet)ViewState["Create_purchase_speclist"];
                var drDriving = DeletePosisionRow.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["Create_purchase_speclist"] = DeletePosisionRow;
                gvpurchaselist.EditIndex = -1;


                gvpurchaselist.DataSource = ViewState["Create_purchase_speclist"];
                gvpurchaselist.DataBind();

                break;

            case "gvEditpurchaselist":

                action_select_purchase_for_edit();


                break;

            case "gvionumber":






                data_purchase _dtpurchase = new data_purchase();

                var gvnumberio = (GridView)sender;
                var row_ionumber = (GridViewRow)gvnumberio.NamingContainer;

                GridView gvionumber = (GridView)row_ionumber.FindControl("gvionumber");

             
                // Literal3.Text = _lbidxnumerid.Text;

                var deleterow_ionumber = (DataSet)ViewState["create_ionumber_list"];
                var drnumberio = deleterow_ionumber.Tables[0].Rows;
                var _delio_number = new numberio_details[deleterow_ionumber.Tables[0].Rows.Count];

                int id = Convert.ToInt32(gvnumberio.DataKeys[e.RowIndex].Value);

                drnumberio.RemoveAt(id);

                ViewState["create_ionumber_list"] = deleterow_ionumber;
                gvionumber.EditIndex = -1;


                gvionumber.DataSource = ViewState["create_ionumber_list"];
                gvionumber.DataBind();


                //foreach (GridViewRow rowCount in gvionumber.Rows)
                //{
                //    Label _lbidxnumerid = (Label)rowCount.FindControl("_lbidxnumerid");
                //    var _deleterow_ionumber = (DataSet)ViewState["create_ionumber_list"];
                ////var _delio_number = new numberio_details[deleterow_ionumber.Tables[0].Rows.Count];

                ////_dtpurchase.numberio_list = _delio_number;

                ////var _linq_iodel = from data_io in _dtpurchase.numberio_list
                ////                  where data_io.idx == 32
                ////                  select data_io;

                ////gvionumber.DataSource = _linq_iodel.ToList();
                ////gvionumber.DataBind();


                //}






                ////_dtpurchase.numberio_list = _delio_number;

                ////var _linq_iodel = from data_io in _dtpurchase.numberio_list
                ////               where data_io.idx == (int.Parse(_lbidxnumerid.ToString()))
                ////               select data_io;

                ////gvionumber.DataSource = _linq_iodel.ToList();
                ////gvionumber.DataBind();

                //////////var gvequipment = (GridView)sender;
                //////////var row_equipment = (GridViewRow)gvequipment.NamingContainer;
                ////////////  Enabled = true :: textbox เมื่อทำการลบข้อมูล
                //////////TextBox txtNoIO = (TextBox)row_equipment.FindControl("txtNoIO");
                //////////Label _lbItemsnumber = (Label)row_equipment.FindControl("_lbItemsnumber");

                //    gvionumber.DataSource = ViewState["create_ionumber_list"];
                //gvionumber.DataBind();


                //////////////txtNoIO.Enabled = true;

                break;

            case "gvassetnumber":

                var _gvnumberasset = (GridView)sender;
                var row_assetnumber = (GridViewRow)_gvnumberasset.NamingContainer;

                GridView gvassetnumber = (GridView)row_assetnumber.FindControl("gvassetnumber");

                var deleterow_assetnumber = (DataSet)ViewState["create_assetnumber_list"];
                var drnumberasset = deleterow_assetnumber.Tables[0].Rows;

                drnumberasset.RemoveAt(e.RowIndex);

                ViewState["create_assetnumber_list"] = deleterow_assetnumber;
                gvassetnumber.EditIndex = -1;


                gvassetnumber.DataSource = ViewState["create_assetnumber_list"];
                gvassetnumber.DataBind();

                var _gvequipment = (GridView)sender;
                var _row_equipment = (GridViewRow)_gvequipment.NamingContainer;
                //  Enabled = true :: textbox เมื่อทำการลบข้อมูล
                TextBox txtNOAsset = (TextBox)_row_equipment.FindControl("txtNOAsset");

                txtNOAsset.Enabled = true;

                break;

        }

    }

#endregion

    #region gvRowUpdating

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchaselist":

                GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");

                var txtupdate_expenditure = (TextBox)gvpurchaselist.Rows[e.RowIndex].FindControl("txtupdate_expenditure");
                var ddlpurchasetype_edit = (DropDownList)gvpurchaselist.Rows[e.RowIndex].FindControl("ddlpurchasetype_edit");
                var ddlequipment_edit = (DropDownList)gvpurchaselist.Rows[e.RowIndex].FindControl("ddlequipment_edit");
                var ddlspec_edit = (DropDownList)gvpurchaselist.Rows[e.RowIndex].FindControl("ddlspec_edit");
                var txtupdate_detailspec = (TextBox)gvpurchaselist.Rows[e.RowIndex].FindControl("txtupdate_detailspec");
                

                var dataset_onUpdate = (DataSet)ViewState["Create_purchase_speclist"];
                var datarow_onUpdate = dataset_onUpdate.Tables[0].Rows;


                //ค่าที่จะเช็คก่อนอัพเดต บน dataset เดิม
                ViewState["purchase_type_idx"] = ddlpurchasetype_edit.SelectedValue;
                ViewState["equipment_idx"] = ddlequipment_edit.SelectedValue;
                ViewState["spec_type_idx"] = ddlspec_edit.SelectedValue;

                int _numrow = dataset_onUpdate.Tables[0].Rows.Count; //จำนวนแถว
                int loop = 0;
                //กรณีมีข้อมูลมากกว่าแถวเดียว
                if (_numrow > 1)
                {
                    //วนหาข้อมูลเพื่อหาเงื่อนไขที่กำหนดไว้
                    foreach (DataRow check in dataset_onUpdate.Tables[0].Rows)
                    {
                        ViewState["check_purchase_type"] = check["purchase_type_idx"];
                        ViewState["check_equipment"] = check["equipment_idx"];
                        ViewState["check_spec"] = check["spec_type_idx"];


                        //กรณิ user เลือกประเภทอุปกรณ์และสเปคตรงกัน
                        if (ViewState["equipment_idx"].ToString() == ViewState["check_equipment"].ToString()
                            && ViewState["spec_type_idx"].ToString() == ViewState["check_spec"].ToString() && ViewState["spec_type_idx"].ToString() != "0")
                        {

                            ViewState["CheckDataset_edit"] = "2";
                            //break;

                        }

                        //กรณิที่ข้อมูลไม่ซ้ำกันจะเข้าการทำงานนี้
                        else
                        {


                            if (ViewState["spec_type_idx"].ToString() == "0")
                            {


                            }

                            else
                            {


                            }

                            ViewState["CheckDataset_edit"] = "1";

                        }



                    }
                    //ทำการ update ข้อมูล
                    if (ViewState["CheckDataset_edit"].ToString() == "1")
                    {
                        if (ViewState["spec_type_idx"].ToString() == "0" || ViewState["spec_type_idx"].ToString() == "" || ViewState["spec_type_idx"].ToString() == null)
                        {
                            datarow_onUpdate[e.RowIndex]["spec_type_idx"] = 0;
                            datarow_onUpdate[e.RowIndex]["spec_type"] = "-";
                        }

                        else
                        {

                            datarow_onUpdate[e.RowIndex]["spec_type_idx"] = int.Parse(ddlspec_edit.SelectedValue);
                            datarow_onUpdate[e.RowIndex]["spec_type"] = ddlspec_edit.SelectedItem.Text;

                        }

                           
                        datarow_onUpdate[e.RowIndex]["equipment_idx"] = ddlequipment_edit.SelectedValue;
                        datarow_onUpdate[e.RowIndex]["purchase_type_idx"] = ddlpurchasetype_edit.SelectedValue;
                        datarow_onUpdate[e.RowIndex]["purchase_type"] = ddlpurchasetype_edit.SelectedItem.Text;
                        datarow_onUpdate[e.RowIndex]["equipment_name"] = ddlequipment_edit.SelectedItem.Text;

                    }

                    else if (ViewState["CheckDataset_edit"].ToString() == "2")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ คุณมีข้อมูลรายการขอซื้อนี้ค่ะ !!!');", true);

                    }

                    //else if (ViewState["CheckDataset_edit"].ToString() == "0")

                    //{

                    //   // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert(' !!!');", true);
                    //    datarow_onUpdate[e.RowIndex]["purchase_type_idx"] = ddlpurchasetype_edit.SelectedValue;
                    //    datarow_onUpdate[e.RowIndex]["purchase_type"] = ddlpurchasetype_edit.SelectedItem.Text;
                    //    datarow_onUpdate[e.RowIndex]["spec_type_idx"] = ddlspec_edit.SelectedValue;
                    //    datarow_onUpdate[e.RowIndex]["equipment_idx"] = ddlequipment_edit.SelectedValue;



                    //}





                }

                //กรณีมีข้อมูลแถวเดียวที่ต้อง สามารถแก้ไขข้อมูลได้
                else if (_numrow == 1)
                {

                    datarow_onUpdate[e.RowIndex]["purchase_type_idx"] = ddlpurchasetype_edit.SelectedValue;
                    datarow_onUpdate[e.RowIndex]["purchase_type"] = ddlpurchasetype_edit.SelectedItem.Text;


                }





                //เก็บค่าที่จะ update ไว้ ใน viewstate
               
                datarow_onUpdate[e.RowIndex]["expenditure"] = txtupdate_expenditure.Text;
                datarow_onUpdate[e.RowIndex]["details_spec"] = txtupdate_detailspec.Text;// txtupdate_expenditure.Text;
              


                ViewState["Create_purchase_speclist"] = dataset_onUpdate;


                gvpurchaselist.EditIndex = -1;

                gvpurchaselist.DataSource = ViewState["Create_purchase_speclist"];
                gvpurchaselist.DataBind();


                break;



            case "gvEditpurchaselist":



                int labelu2purchaseidx = Convert.ToInt32(gvEditpurchaselist.DataKeys[e.RowIndex].Values[0].ToString());
                var _update_purchase_type = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddlpurchase_typeedit");
                var _update_equipment_type = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddlequipment_typeedit");
                var _update_spec = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddlm0_spec_idx");
                var _update_costcenter = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddledit_costcenter");
                var _update_location = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddledit_location");
                var _update_position = (DropDownList)gvEditpurchaselist.Rows[e.RowIndex].FindControl("ddleditposition");

                var _update_expenditure = (TextBox)gvEditpurchaselist.Rows[e.RowIndex].FindControl("txtedit_expenditure");
                var _update_items_quantity = (TextBox)gvEditpurchaselist.Rows[e.RowIndex].FindControl("txtedit_items_quantity");
                var _update_detailspec = (TextBox)gvEditpurchaselist.Rows[e.RowIndex].FindControl("txtedit_detailspec");
                var _update_argumentation = (TextBox)gvEditpurchaselist.Rows[e.RowIndex].FindControl("txteditargumentation");
                var _update_u0purchaseidx = (Label)gvEditpurchaselist.Rows[e.RowIndex].FindControl("labelu0purchaseidx");

                var txtedit_purchasetype = (TextBox)gvEditpurchaselist.Rows[e.RowIndex].FindControl("txtedit_purchasetype");
                
                ViewState["_u0PurchaseIDX"] = _update_u0purchaseidx.Text;
                ViewState["_u2PurchaseIDX"] = labelu2purchaseidx;
                ViewState["_PURCHASETYPE"] = txtedit_purchasetype.Text;


              //  test.Text = ViewState["_PURCHASETYPE"].ToString();

                //:::::::::::::::::::::: START UPDATE DATA PURCHASE FOR EDIT DOCUMENT PURCHASE :::::::::::::::::://

                gvEditpurchaselist.EditIndex = -1;

                //:::::::::::::::: START UPDATE DATA PURCHASE U0_DOCUMENT :::::::::::::::::://

                data_purchase _data_edit_purchase = new data_purchase();
                _data_edit_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                u0_purchase_equipment_details _edit_u0_document_purchase = new u0_purchase_equipment_details();

              

                //:::::::::::::::: END UPDATE DATA PURCHASE U0_DOCUMENT :::::::::::::::::://


                //:::::::::::::::: START UPDATE DATA PURCHASE U2_DOCUMENT :::::::::::::::::://

                //data_purchase _datapurchase_edit = new data_purchase();
                _data_edit_purchase.items_purchase_list = new items_purchase_details[1];
                items_purchase_details _edit_u2_document_purchase = new items_purchase_details();


                //ViewState["_EDIT_u0IDX"] = int.Parse(_update_u0purchaseidx.Text);
                //ViewState["_EDIT_detailspurchase"] = _update_argumentation.Text;
                //ViewState["_EDIT_m0IDX"] = int.Parse(_update_purchase_type.SelectedValue);


                _edit_u0_document_purchase.u0_purchase_idx = int.Parse(_update_u0purchaseidx.Text);
                _edit_u0_document_purchase.details_purchase = _update_argumentation.Text;
                _edit_u0_document_purchase.m0_purchase_idx = int.Parse(_update_purchase_type.SelectedValue);
                _edit_u0_document_purchase.costcenter_idx = int.Parse(_update_costcenter.SelectedValue);



                _edit_u2_document_purchase.u2_purchase_idx = labelu2purchaseidx;
                _edit_u2_document_purchase.m0_equipment_idx = int.Parse(_update_equipment_type.SelectedValue);
                _edit_u2_document_purchase.m0_spec_idx = int.Parse(_update_spec.SelectedValue);
                _edit_u2_document_purchase.location_idx = int.Parse(_update_location.SelectedValue);
                _edit_u2_document_purchase.rpos_idx = int.Parse(_update_position.SelectedValue);
                //_edit_u2_document_purchase.costcenter_idx = int.Parse(_update_costcenter.SelectedValue);



                _edit_u2_document_purchase.items_quantity = int.Parse(_update_items_quantity.Text);
                _edit_u2_document_purchase.items_details = (_update_detailspec.Text);
                _edit_u2_document_purchase.items_expenditure = (_update_expenditure.Text);

                _data_edit_purchase.items_purchase_list[0] = _edit_u2_document_purchase;
                _data_edit_purchase.u0_purchase_equipment_list[0] = _edit_u0_document_purchase;

                _data_edit_purchase = callServicePurchase(_urlSetaddItemspurchase, _data_edit_purchase);

                action_select_purchase_for_edit();
                //:::::::::::::::: END UPDATE DATA PURCHASE U2_DOCUMENT :::::::::::::::::://

                //gvEditpurchaselist.DataSource = _data_edit_purchase.items_purchase_list;
                //gvEditpurchaselist.DataBind();

                break;
        }

    }

    #endregion

    #region GvRowEditing

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchaselist":


                GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                HyperLink Focusset = (HyperLink)fvCreatePurchasequipment.FindControl("Focusset");

                gvpurchaselist.EditIndex = e.NewEditIndex;

                gvpurchaselist.DataSource = ViewState["Create_purchase_speclist"];
                gvpurchaselist.DataBind();

                Focusset.Focus();

                break;

            case "gvEditpurchaselist":


                gvEditpurchaselist.EditIndex = e.NewEditIndex;
                action_select_purchase_for_edit();

           

                break;
        }
    }

    #endregion

    #region table purchaselist

    protected void Select_tabel_purchaselist()
    {
        var _purchaselist = new DataSet();
        _purchaselist.Tables.Add("purchase_speclist");

        _purchaselist.Tables[0].Columns.Add("equipment_idx", typeof(int));
        _purchaselist.Tables[0].Columns.Add("spec_type_idx", typeof(int));
        _purchaselist.Tables[0].Columns.Add("purchase_type_idx", typeof(int));
        _purchaselist.Tables[0].Columns.Add("m0_location_idx", typeof(int));
        _purchaselist.Tables[0].Columns.Add("rpos_idx_purchase", typeof(int));
        _purchaselist.Tables[0].Columns.Add("costcenter_idx", typeof(int));
        _purchaselist.Tables[0].Columns.Add("quantity", typeof(String));
        _purchaselist.Tables[0].Columns.Add("details_spec", typeof(String));
        _purchaselist.Tables[0].Columns.Add("spec_type", typeof(String));
        _purchaselist.Tables[0].Columns.Add("equipment_name", typeof(String));
        _purchaselist.Tables[0].Columns.Add("purchase_type", typeof(String));
        _purchaselist.Tables[0].Columns.Add("expenditure", typeof(String));
        _purchaselist.Tables[0].Columns.Add("details_purchase", typeof(String));


        if (ViewState["Create_purchase_speclist"] == null)
        {
            ViewState["Create_purchase_speclist"] = _purchaselist;
        }

    }


    protected void select_tabel_iolist()
    {
        var _ionumberlist = new DataSet();
        _ionumberlist.Tables.Add("ionumberlist");

        _ionumberlist.Tables[0].Columns.Add("io", typeof(String));
        _ionumberlist.Tables[0].Columns.Add("idx", typeof(int));


        if (ViewState["create_ionumber_list"] == null)
        {
            ViewState["create_ionumber_list"] = _ionumberlist;
        }
    }

    protected void select_tabel_assetlist()
    {
        var _assetnumberlist = new DataSet();
        _assetnumberlist.Tables.Add("AssetnumberList");

        _assetnumberlist.Tables[0].Columns.Add("asset_number", typeof(String));
        _assetnumberlist.Tables[0].Columns.Add("idx_asset", typeof(int));


        if (ViewState["create_assetnumber_list"] == null)
        {
            ViewState["create_assetnumber_list"] = _assetnumberlist;
        }
    }


    #endregion

    #region radio button
    protected void Select_master_spec()
    {

        var ddlspecidx = (DropDownList)fvCreatePurchasequipment.FindControl("ddlspecidx");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");

        _data_purchase.master_spec_list = new master_spec_details[1];
        master_spec_details _spec = new master_spec_details();

        _spec.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedValue);


        data_purchase data_purchase_edit = new data_purchase();
        data_purchase_edit.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_costcenter_edit = new u0_purchase_equipment_details();

        ddlspecidx.AppendDataBoundItems = true;
        ddlspecidx.Items.Add(new ListItem("กรุณาเลือก spec", "0"));


        _data_purchase.master_spec_list[0] = _spec;

        //call service
        _data_purchase = callServicePurchase(_urlGetmasterspec, _data_purchase);



        //ddlspecidx.DataSource = _data_purchase.master_spec_list;
        //ddlspecidx.DataTextField = "name_spec";
        //ddlspecidx.DataValueField = "m0_spec_idx";
        ddlspecidx.DataBind();

    }
    #endregion

    #region textChanged
    protected void textChanged(object sender, EventArgs e)
    {
        TextBox textBox = sender as TextBox;
        TextBox txtquantity = (TextBox)fvCreatePurchasequipment.FindControl("txtquantity");
        var txtexpenditure = (TextBox)fvCreatePurchasequipment.FindControl("txtexpenditure");
        var btnaddpurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");
        DropDownList ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        DropDownList ddlcostCenter = (DropDownList)fvCreatePurchasequipment.FindControl("ddlcostCenter");
        
        string number = string.Empty;

        switch (textBox.ID)
        {

            case "txtquantity":

                txtexpenditure.Focus();

                break;

            case "txtexpenditure":

              

                string decimalValue = string.Format("{0:#,##0}", double.Parse(txtexpenditure.Text));
                txtexpenditure.Text = decimalValue;

                btnaddpurchase.Focus();

                break;

        
        }

    }
    #endregion

    #region gvPageIndexChanging

        protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvpurchase_equipmentlist":

                setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);


                gvpurchase_equipmentlist.PageIndex = e.NewPageIndex;
                gvpurchase_equipmentlist.DataBind();


                SETFOCUS_ONTOP.Focus();

                break;


            case "gvReport":

                setGridViewDataBind(gvReport, ViewState["purchase_list"]);
                gvReport.PageIndex = e.NewPageIndex;
                gvReport.DataBind();

                SETFOCUS_ONTOP.Focus();


                break;

            case "gvitems":



                //int index = e.Row.RowIndex;
                //GridView gvitems = (GridView)[index].FindControl("gvitems");




                //data_purchase _reportItemspurchase = new data_purchase();
                //_reportItemspurchase.items_purchase_list = new items_purchase_details[1];


                //items_purchase_details _reportingItems = new items_purchase_details();

                //_reportingItems.u0_purchase_idx = int.Parse(u0_purchase_idx.Text);



                //_reportItemspurchase.items_purchase_list[0] = _reportingItems;

                //_reportItemspurchase = callServicePurchase(_urlGetitems_purchase, _reportItemspurchase);
                //ViewState["itemslist"] = _reportItemspurchase.items_purchase_list;


                //gvitems.DataSource = ViewState["itemslist"];
                //gvitems.DataBind();









                //   setGridViewDataBind(gvitems, ViewState["itemslist"]);
                //gvitems.DataSource = ViewState["itemslist"];
                //gvitems.DataBind();
                //gvitems.PageIndex = e.NewPageIndex;
                //gvitems.DataBind();

                break;
        }

    }

    #endregion

    #region insert
    protected void action_create_purchasequipment()
    {

       
        var ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        var ddllocation = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation");
        var ddltype_unitequipment = (DropDownList)fvCreatePurchasequipment.FindControl("ddltype_unitequipment");
        var txtdetails_purchase = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_purchase");
        var txtquantity = (TextBox)fvCreatePurchasequipment.FindControl("txtquantity");
        var lb_idx_org_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_idx_org_purchase");
        var ddlcostCenter = (DropDownList)fvCreatePurchasequipment.FindControl("ddlcostCenter");
        var uploadfile_organization = (FileUpload)fvCreatePurchasequipment.FindControl("uploadfile_organization");
        var uploadfile_memo = (FileUpload)fvCreatePurchasequipment.FindControl("uploadfile_memo");
        var uploadfile_cutoff = (FileUpload)fvCreatePurchasequipment.FindControl("uploadfile_cutoff");
        var uploadfile_refercutoff = (FileUpload)fvCreatePurchasequipment.FindControl("uploadfile_refercutoff");

        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details insert_purchaselist = new u0_purchase_equipment_details();


        GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");


        insert_purchaselist.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
        insert_purchaselist.costcenter_idx = int.Parse(ddlcostCenter.SelectedValue);
        insert_purchaselist.m0_purchase_idx = int.Parse(ddlpurchasetype.SelectedValue);

        insert_purchaselist.org_idx_purchase = int.Parse(lb_idx_org_purchase.Text);
        // insert_purchaselist.org_idx_purchase = 1;


        if (int.Parse(ddlpurchasetype.SelectedValue) == 1)
        {
            insert_purchaselist.m0_actor_idx = 1;
            insert_purchaselist.m0_node_idx = 1;
            insert_purchaselist.status_purchase = 1;
           
        }

        else if (int.Parse(ddlpurchasetype.SelectedValue) == 2)
        {
            insert_purchaselist.m0_actor_idx = 1;
            insert_purchaselist.m0_node_idx = 13;
            insert_purchaselist.status_purchase = 1;
           

        }



        _data_purchase.u0_purchase_equipment_list[0] = insert_purchaselist;

        // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase));

        #region sent e mail

        var _dataset_purchasemail = (DataSet)ViewState["Create_purchase_speclist"];
        var AddPur1 = new u0_purchase_equipment_details[_dataset_purchasemail.Tables[0].Rows.Count];

        string _mailequipment = "";

        foreach (DataRow dr in _dataset_purchasemail.Tables[0].Rows)
        {

            _mailequipment += dr["equipment_name"].ToString() + ",";


        }

        insert_purchaselist.name_equipment = _mailequipment;

        ViewState["sentmail_equipment"] = _mailequipment;


        var lb_emps_code_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_emps_code_purchase");
        var lb_emps_fullname_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_emps_fullname_purchase");
        var lb_name_pos_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_name_pos_purchase");

        ////ข้อมูลในการส่ง e-mail
        // insert_purchaselist.name_purchase = (ddlpurchasetype.SelectedItem.Text);

        //// insert_purchaselist.quantity_equipment = int.Parse(txtquantity.Text);
        //insert_purchaselist.fullname_purchase = lb_emps_fullname_purchase.Text;
        //insert_purchaselist.empcode_purchase = lb_emps_code_purchase.Text;
        //insert_purchaselist.pos_name_purchase = lb_name_pos_purchase.Text;
        insert_purchaselist.details_purchase = txtdetails_purchase.Text;


        ViewState["create_fullname_purchase"] = lb_emps_fullname_purchase.Text;
        ViewState["create_empcode_purchase"] = lb_emps_code_purchase.Text;
        ViewState["create_pos_name_purchase"] = lb_name_pos_purchase.Text;
        ViewState["create_details_purchase"] = txtdetails_purchase.Text;
        ViewState["create_name_purchase"] = (ddlpurchasetype.SelectedItem.Text);


        //insert_purchaselist.name_purchase = ViewState["create_name_purchase"].ToString();
        //insert_purchaselist.fullname_purchase = ViewState["create_fullname_purchase"].ToString();
        //insert_purchaselist.empcode_purchase = ViewState["create_empcode_purchase"].ToString();
        //insert_purchaselist.pos_name_purchase = ViewState["create_pos_name_purchase"].ToString();
        //insert_purchaselist.details_purchase = ViewState["create_details_purchase"].ToString();
       
        //insert_purchaselist.link = _link.ToString();

        _data_purchase.u0_purchase_equipment_list[0] = insert_purchaselist;
//
        //check_mail.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase));


        //string emailmove = "msirirak4441@gmail.com";
        //string replyempmove = "sirinyamod@hotmail.com";
        //////string replyempmove = "nipon@taokeanoi.co.th";



        //_mail_subject = "[MIS/IT Purchase] - " + "แจ้งขอซื้ออุปกรณ์";

        //_mail_body = servicemail.create_purchase(_data_purchase.u0_purchase_equipment_list[0]);

        //_data_purchase.u0_purchase_equipment_list[0] = insert_purchaselist;

        // check_XML.Text = (ddlpurchasetype.SelectedItem.Text);


        #endregion


        //call service
        _data_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_purchase);


        var _returncode = _data_purchase.return_code;
        var _returndocumentcode = _data_purchase.return_doccode;
        var _return_emaildirector = _data_purchase.return_email;
        ViewState["_u0idx"] = _data_purchase.return_result;



        if (ViewState["Create_purchase_speclist"] != null)

        {
            var _dataset_purchase = (DataSet)ViewState["Create_purchase_speclist"];
            var _addpurchasrlist = new u2_purchase_equipment_details[_dataset_purchase.Tables[0].Rows.Count];

            int i = 0;

            foreach (DataRow datarowpurchase in _dataset_purchase.Tables[0].Rows)
            {
                _addpurchasrlist[i] = new u2_purchase_equipment_details();

                /* ----------------insert table purchase_u0_document and  table purchase_u1_document--------------*/

                // _addpurchasrlist[i].rpos_idx_purchase = int.Parse(ViewState[""].ToString());
                //
                _addpurchasrlist[i].spec_type_idx = int.Parse(datarowpurchase["spec_type_idx"].ToString());
                _addpurchasrlist[i].equipment_type = int.Parse(datarowpurchase["equipment_idx"].ToString());
                _addpurchasrlist[i].quantity_purchase = int.Parse(datarowpurchase["quantity"].ToString());
                _addpurchasrlist[i].location_idx = int.Parse(datarowpurchase["m0_location_idx"].ToString());
                _addpurchasrlist[i].details_spec = datarowpurchase["details_spec"].ToString();
                _addpurchasrlist[i].expenditure = datarowpurchase["expenditure"].ToString();
                _addpurchasrlist[i].u0_purchasee = int.Parse(ViewState["_u0idx"].ToString());
                _addpurchasrlist[i].rpos_idx_purchase = int.Parse(datarowpurchase["rpos_idx_purchase"].ToString());


                i++;

            }

            if (_dataset_purchase.Tables[0].Rows.Count == 0)

            {


            }

            else
            {

                data_purchase _purchaseItems = new data_purchase();

                _purchaseItems.items_purchase_list = new items_purchase_details[1];
                items_purchase_details condition_insert = new items_purchase_details();


                _purchaseItems.items_purchase_list[0] = condition_insert;
                _purchaseItems.u2_purchase_equipment_list = _addpurchasrlist;
                _purchaseItems = callServicePurchase(_urlSetaddItemspurchase, _purchaseItems);
                //  check_mail.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_purchaseItems));

            }
        }




        if (_data_purchase.return_code == "0")
        {


            ViewState["sentmail_doccode"] = _data_purchase.u0_purchase_equipment_list[0].document_code.ToString();
            ViewState["sentmaildetails_purchase"] = _data_purchase.u0_purchase_equipment_list[0].details_purchase.ToString();
            // ViewState["sentmaildrept_name_purchase"] = _data_purchase.u0_purchase_equipment_list[0].drept_name_purchase.ToString();
            ViewState["sentmailempcode_purchase"] = _data_purchase.u0_purchase_equipment_list[0].empcode_purchase.ToString();
            ViewState["sentmailname_purchase"] = _data_purchase.u0_purchase_equipment_list[0].name_purchase.ToString();
            ViewState["sentmailfullname_purchase"] = _data_purchase.u0_purchase_equipment_list[0].fullname_purchase.ToString();
            //Literal2.Text = ViewState["sentmail_equipment"].ToString();


            data_purchase _data_sentmail = new data_purchase();
            _data_sentmail.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
            u0_purchase_equipment_details create_senmail = new u0_purchase_equipment_details();

            create_senmail.document_code = ViewState["sentmail_doccode"].ToString();
            create_senmail.details_purchase = ViewState["sentmaildetails_purchase"].ToString();
            // create_senmail.drept_name_purchase = ViewState["sentmaildrept_name_purchase"].ToString();
            create_senmail.empcode_purchase = ViewState["sentmailempcode_purchase"].ToString();
            create_senmail.name_purchase = ViewState["sentmailname_purchase"].ToString();
            create_senmail.name_equipment = ViewState["sentmail_equipment"].ToString();
            create_senmail.fullname_purchase = ViewState["sentmailfullname_purchase"].ToString();
            create_senmail.link = _link.ToString();

            string emailmove = "mongkonl.d@taokaenoi.co.th";
            string replyempmove = "msirirak4441@gmail.com";
            //string replyempmove = "nipon@taokeanoi.co.th";

            _data_sentmail.u0_purchase_equipment_list[0] = create_senmail;

            _mail_subject = "[MIS/IT Purchase] - " + "แจ้งขอซื้ออุปกรณ์";

            _mail_body = servicemail.create_purchase(_data_sentmail.u0_purchase_equipment_list[0]);

            _data_sentmail.u0_purchase_equipment_list[0] = create_senmail;


            // check_mail.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
         //   servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail ตอน เทสระบบ


        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ !! ท่านดำเนินการทำรายการขอซื้อสำเร็จไม่สำเร็จ');", true);

        }


      
        //Literal1.Text = _returndocumentcode;

        //uplodefile purchase
        var panel1 = (Panel)fvCreatePurchasequipment.FindControl("panel1");
        var panel2 = (Panel)fvCreatePurchasequipment.FindControl("panel2");



        /*  else*/
        if (uploadfile_memo.HasFile)
        {

            string getPathfile = ConfigurationManager.AppSettings["pathfile_purchase"];
            string document_code = _returndocumentcode;
            string fileName_upload = "[ เอกสาร memo ] " + document_code;
            string filePath_upload = Server.MapPath(getPathfile + document_code);

            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }
            string extension = Path.GetExtension(uploadfile_memo.FileName);

            uploadfile_memo.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + fileName_upload + extension);


        }


        if (uploadfile_organization.HasFile)
        {

            string getPathfileOrg = ConfigurationManager.AppSettings["pathfile_purchase"];
            string document_codeorg = _returndocumentcode;
            string fileName_uploadorg = "[ เอกสาร organization ] " + document_codeorg;
            string filePath_uploadorg = Server.MapPath(getPathfileOrg + document_codeorg);

            if (!Directory.Exists(filePath_uploadorg))
            {
                Directory.CreateDirectory(filePath_uploadorg);
            }
            string extension = Path.GetExtension(uploadfile_organization.FileName);

            uploadfile_organization.SaveAs(Server.MapPath(getPathfileOrg + document_codeorg) + "\\" + fileName_uploadorg + extension);

        }


        if (uploadfile_cutoff.HasFile)
        {

            string getPathfilecutoff = ConfigurationManager.AppSettings["pathfile_purchase"];
            string document_codecutoff = _returndocumentcode;
            string fileName_uploadcutoff = "[ เอกสารการตัดเสีย ] " + document_codecutoff;
            string filePath_uploadcutoff = Server.MapPath(getPathfilecutoff + document_codecutoff);

            if (!Directory.Exists(filePath_uploadcutoff))
            {
                Directory.CreateDirectory(filePath_uploadcutoff);
            }
            string extension = Path.GetExtension(uploadfile_cutoff.FileName);

            uploadfile_cutoff.SaveAs(Server.MapPath(getPathfilecutoff + document_codecutoff) + "\\" + fileName_uploadcutoff + extension);

        }


        if (uploadfile_refercutoff.HasFile)
        {

            string getPathfilerefercutoff = ConfigurationManager.AppSettings["pathfile_purchase"];
            string document_coderefercutoff = _returndocumentcode;
            string fileName_uploadrefercutoff = "[ เอกสารยืนยันการตัดเสีย ] " + document_coderefercutoff;
            string filePath_uploadrefercutoff = Server.MapPath(getPathfilerefercutoff + document_coderefercutoff);

            if (!Directory.Exists(filePath_uploadrefercutoff))
            {
                Directory.CreateDirectory(filePath_uploadrefercutoff);
            }
            string extension = Path.GetExtension(uploadfile_refercutoff.FileName);

            uploadfile_refercutoff.SaveAs(Server.MapPath(getPathfilerefercutoff + document_coderefercutoff) + "\\" + fileName_uploadrefercutoff + extension);

        }





        //ถ้าบันทึกสำเร็จให้ส่ง e-mail
      
       




    }
    #endregion

    #region select report

    protected string action_select_orgreport(int _type, string _selectvalue)
    {

        //ddlsearch_organization.Items.Clear();

        if (_type == 0)
        {

            data_softwarelicense _data_selectorg = new data_softwarelicense();
            _data_selectorg.organization_list = new organization_detail[1];
            organization_detail select_organization = new organization_detail();

            _data_selectorg.organization_list[0] = select_organization;
            _data_selectorg = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_selectorg);

            ddlOrganizationReport.DataSource = _data_selectorg.organization_list;
            ddlOrganizationReport.DataTextField = "OrgNameTH";
            ddlOrganizationReport.DataValueField = "OrgIDX";
            ddlOrganizationReport.DataBind();
            ddlOrganizationReport.Items.Insert(0, new ListItem("กรุณาเลือก organization", "0"));
            // ddlsearch_organization.SelectedValue = _selectvalue;

        }
        else if (_type == 1)
        {
            ddlOrganizationReport.DataBind();
            ddlOrganizationReport.Items.Insert(0, new ListItem("กรุณาเลือก organization", "0"));
        }

        return ddlOrganizationReport.SelectedItem.Value;


    }

    protected string action_select_rdeptreport(int _type, string _selectvalue)
    {

        ddlDepartmentReport.Items.Clear();

        if (_type == 0)
        {

             data_softwarelicense _data_selectrdept = new data_softwarelicense();
            _data_selectrdept.organization_list = new organization_detail[1];
            organization_detail select_depertment = new organization_detail();

            select_depertment.OrgIDX = Int32.Parse(ddlOrganizationReport.SelectedValue);

            _data_selectrdept.organization_list[0] = select_depertment;
            _data_selectrdept = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_selectrdept);

            ddlDepartmentReport.DataSource = _data_selectrdept.organization_list;
            ddlDepartmentReport.DataTextField = "DeptNameTH";
            ddlDepartmentReport.DataValueField = "RDeptIDX";
            ddlDepartmentReport.DataBind();
            ddlDepartmentReport.Items.Insert(0, new ListItem("กรุณาเลือก depertment", "0"));


        }
        else if (_type == 1)
        {
            ddlDepartmentReport.DataBind();
            ddlDepartmentReport.Items.Insert(0, new ListItem("กรุณาเลือก depertment", "0"));
        }

        return ddlDepartmentReport.SelectedItem.Value;

    }


    protected void action_purchase_report()
    {

        ddlPurchaseTypeReport.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail ddlsearch_purchase = new m0_type_purchase_equipment_detail();

        ddlPurchaseTypeReport.AppendDataBoundItems = true;
        ddlPurchaseTypeReport.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

        ddlsearch_purchase.status_equipment = 1;

        __data_purchase.m0_type_purchase_equipment_list[0] = ddlsearch_purchase;
        __data_purchase = callServicePurchase(_urlGetequipment_purchasetype, __data_purchase);

        ddlPurchaseTypeReport.DataSource = __data_purchase.m0_type_purchase_equipment_list;
        ddlPurchaseTypeReport.DataTextField = "name_purchase_type";
        ddlPurchaseTypeReport.DataValueField = "m0_purchase_type_idx";
        ddlPurchaseTypeReport.DataBind();

    }


    protected void action_equipment_report()
    {


        ddlEquipmentTypeReport.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail ddlsearch_equipment = new m0_type_purchase_equipment_detail();

        ddlEquipmentTypeReport.AppendDataBoundItems = true;
        ddlEquipmentTypeReport.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์", "0"));

        ddlsearch_equipment.status_equipment = 9;

        __data_purchase.m0_type_purchase_equipment_list[0] = ddlsearch_equipment;
        __data_purchase = callServicePurchase(_urlGetequipment_purchasetype, __data_purchase);

        ddlEquipmentTypeReport.DataSource = __data_purchase.m0_type_purchase_equipment_list;
        ddlEquipmentTypeReport.DataTextField = "name_equipment_type";
        ddlEquipmentTypeReport.DataValueField = "m0_equipment_type_idx";
        ddlEquipmentTypeReport.DataBind();

    }

    protected void action_location_report()
    {


        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_location = new u0_purchase_equipment_details();

        ddlLocationReport.AppendDataBoundItems = true;
        ddlLocationReport.Items.Add(new ListItem("กรุณาเลือกสถานที่", "0"));

        __data_purchase.u0_purchase_equipment_list[0] = select_location;
        __data_purchase = callServicePurchase(_urlGetlocation, __data_purchase);

        ddlLocationReport.DataSource = __data_purchase.u0_purchase_equipment_list;
        ddlLocationReport.DataTextField = "name_location";
        ddlLocationReport.DataValueField = "m0_location_idx";
        ddlLocationReport.DataBind();
    }

    protected void action_costcenter_report()
    {

        ddlCostcenterReport.Items.Clear();
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_costcenter = new u0_purchase_equipment_details();

        // select_costcenter.action_type = 0; //เรียกข้อมูลการขอซื้อมาทั้งหมด

        _data_purchase.u0_purchase_equipment_list[0] = select_costcenter;
        _data_purchase = callServicePurchase(_urlGetcostcenter, _data_purchase);


        ddlCostcenterReport.AppendDataBoundItems = true;
        ddlCostcenterReport.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));
        ddlCostcenterReport.DataSource = _data_purchase.u0_purchase_equipment_list;
        ddlCostcenterReport.DataTextField = "costcenter_no";
        ddlCostcenterReport.DataValueField = "costcenter_idx";
        ddlCostcenterReport.DataBind();


    }

    protected void action_status_report()
    {

        ddlStatusReport.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details search_status = new u0_purchase_equipment_details();

        ddlStatusReport.AppendDataBoundItems = true;
        ddlStatusReport.Items.Add(new ListItem("กรุณาเลือกสถานะรายการ", "0"));

        __data_purchase.u0_purchase_equipment_list[0] = search_status;
        __data_purchase = callServicePurchase(_urlGetstatus_node, __data_purchase);

        ddlStatusReport.DataSource = __data_purchase.u0_purchase_equipment_list;
        ddlStatusReport.DataTextField = "node_name_purchase";
        ddlStatusReport.DataValueField = "m0_node_idx";
        ddlStatusReport.DataBind();

    }




    #endregion

    #region select

    //#region Directories_File URL 

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        GridView gvFile = (GridView)viewmanage_purchasequipment.FindControl("gvFile");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFile.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFile.DataSource = null;
            gvFile.DataBind();

        }



    }


    public void SearchDirectories_foreditFile(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        GridView GridView1 = (GridView)viewCreatePurchase.FindControl("GridView1");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            GridView1.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            GridView1.DataBind();
            ds1.Dispose();
        }
        else
        {

            GridView1.DataSource = null;
            GridView1.DataBind();

        }



    }


    protected string action_select_masterspec(int _type, string _selectvalue)
    {
        var ddlspecidx = (DropDownList)fvCreatePurchasequipment.FindControl("ddlspecidx");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");

        ddlspecidx.Items.Clear();
        // ddlequipmenttype.Items.Clear();

        if (_type == 0)
        {
            data_purchase data_purchase_ = new data_purchase();

            data_purchase_.master_spec_list = new master_spec_details[1];
            master_spec_details select_masterspec = new master_spec_details();

            // select_masterspec.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedValue);
            //select_masterspec.status_equipment = 2;

            data_purchase_.master_spec_list[0] = select_masterspec;
            // _data_purchase.m0_type_purchase_equipment_list[0] = select_masterunit;

            data_purchase_ = callServicePurchase(_urlGetmasterspec, data_purchase_);

            //var returncode_unit = data_purchase_.return_code;

            ddlspecidx.DataSource = data_purchase_.master_spec_list;
            ddlspecidx.DataTextField = "name_spec";
            ddlspecidx.DataValueField = "m0_spec_idx";
            ddlspecidx.DataBind();
            ddlspecidx.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));
            //ddlspecidx.SelectedValue = returncode_unit;

        }
        else if (_type == 1)
        {
            ddlspecidx.DataBind();
            ddlspecidx.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));
        }

        return ddlspecidx.SelectedItem.Value;


    }
    protected void action_select_costcenter()
    {

      //  var txt_costcenter_idx = (TextBox)fvmanage_approvepurchase.FindControl("txt_costcenter_idx");
        DropDownList _ddlcostCenter = (DropDownList)fvCreatePurchasequipment.FindControl("ddlcostCenter");

        data_purchase data_purchase_edit = new data_purchase();
        data_purchase_edit.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_costcenter_edit = new u0_purchase_equipment_details();

        _ddlcostCenter.AppendDataBoundItems = true;
        _ddlcostCenter.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));

        data_purchase_edit.u0_purchase_equipment_list[0] = select_costcenter_edit;
        data_purchase_edit = callServicePurchase(_urlGetcostcenter, data_purchase_edit);

        _ddlcostCenter.DataSource = data_purchase_edit.u0_purchase_equipment_list;
        _ddlcostCenter.DataTextField = "costcenter_no";
        _ddlcostCenter.DataValueField = "costcenter_idx";
        _ddlcostCenter.DataBind();
        _ddlcostCenter.SelectedValue = ViewState["_value_location"].ToString();
        //ddlcostCenter.Enabled = false;

    }

  


    protected string action_select_masterunit(int _type, string _selectvalue)
    {
        var ddltype_unitequipment = (DropDownList)fvCreatePurchasequipment.FindControl("ddltype_unitequipment");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");

        ddltype_unitequipment.Items.Clear();
       // ddlequipmenttype.Items.Clear();

        if (_type == 0)
        {
            _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
            m0_type_purchase_equipment_detail select_masterunit = new m0_type_purchase_equipment_detail();

            _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
            u0_purchase_equipment_details m0unit = new u0_purchase_equipment_details();

            m0unit.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedValue);
            select_masterunit.status_equipment = 2;

            _data_purchase.u0_purchase_equipment_list[0] = m0unit;
            _data_purchase.m0_type_purchase_equipment_list[0] = select_masterunit;

            _data_purchase = callServicePurchase(_urlGetmasterUnit, _data_purchase);

            var returncode_unit = _data_purchase.return_code;

            ddltype_unitequipment.DataSource = _data_purchase.m0_type_purchase_equipment_list;
            ddltype_unitequipment.DataTextField = "nameth_unit";
            ddltype_unitequipment.DataValueField = "m0_unit_idx";
            ddltype_unitequipment.DataBind();
            ddltype_unitequipment.Items.Insert(0, new ListItem("กรุณาเลือกหน่วยอุปกรณ์", "0"));
            ddltype_unitequipment.SelectedValue = returncode_unit;

        }
        else if (_type == 1)
        {
            ddltype_unitequipment.DataBind();
            ddltype_unitequipment.Items.Insert(0, new ListItem("กรุณาเลือกหน่วยอุปกรณ์", "0"));
        }

        return ddltype_unitequipment.SelectedItem.Value;


    }


    protected void action_select_spec_details()
    {

        var ddlspecidx = (DropDownList)fvCreatePurchasequipment.FindControl("ddlspecidx");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        var txtdetails_spec = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_spec");


        _data_purchase.master_spec_list = new master_spec_details[1];
        master_spec_details select_spec_details = new master_spec_details();

        select_spec_details.m0_spec_idx = int.Parse(ddlspecidx.SelectedValue);
        select_spec_details.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedValue);

        _data_purchase.master_spec_list[0] = select_spec_details;
        _data_purchase = callServicePurchase(_urlGetmasterspec, _data_purchase);

        txtdetails_spec.Text = _data_purchase.master_spec_list[0].details_spec;
        txtdetails_spec.Enabled = false;
    }


    protected void action_select_purchaselist()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchaselist = new u0_purchase_equipment_details();

        select_purchaselist.action_type = 0; //เรียกข้อมูลการขอซื้อมาทั้งหมด

        _data_purchase.u0_purchase_equipment_list[0] = select_purchaselist;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        ViewState["purchase_list"] = _data_purchase.u0_purchase_equipment_list;
        setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);

        //ViewState["_DATAPurchase"] = _data_purchase.u0_purchase_equipment_list;


    }

    protected void action_select_costcenter_search()
    {

        ddl_costcenter.Items.Clear();
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_costcenter = new u0_purchase_equipment_details();

        // select_costcenter.action_type = 0; //เรียกข้อมูลการขอซื้อมาทั้งหมด

        _data_purchase.u0_purchase_equipment_list[0] = select_costcenter;
        _data_purchase = callServicePurchase(_urlGetcostcenter, _data_purchase);


        ddl_costcenter.AppendDataBoundItems = true;
        ddl_costcenter.Items.Add(new ListItem("กรุณาเลือก costcenter", "0"));
        ddl_costcenter.DataSource = _data_purchase.u0_purchase_equipment_list;
        ddl_costcenter.DataTextField = "costcenter_no";
        ddl_costcenter.DataValueField = "costcenter_idx";
        ddl_costcenter.DataBind();


    }

    protected string action_select_organization(int _type, string _selectvalue)
    {

        //ddlsearch_organization.Items.Clear();

        if (_type == 0)
        {

            data_softwarelicense __data_softwarelicense = new data_softwarelicense();
            __data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_organization = new organization_detail();

            __data_softwarelicense.organization_list[0] = select_organization;
            __data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, __data_softwarelicense);

            ddlsearch_organization.DataSource = __data_softwarelicense.organization_list;
            ddlsearch_organization.DataTextField = "OrgNameTH";
            ddlsearch_organization.DataValueField = "OrgIDX";
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new ListItem("กรุณาเลือก organization", "0"));
            // ddlsearch_organization.SelectedValue = _selectvalue;

        }
        else if (_type == 1)
        {
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new ListItem("กรุณาเลือก organization", "0"));
        }

        return ddlsearch_organization.SelectedItem.Value;


    }

    protected string action_select_depertment(int _type, string _selectvalue)
    {

        ddlsearch_department.Items.Clear();

        if (_type == 0)
        {

            // data_softwarelicense __data_softwarelicense = new data_softwarelicense();
            _data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_depertment = new organization_detail();

            select_depertment.OrgIDX = Int32.Parse(ddlsearch_organization.SelectedValue);

            _data_softwarelicense.organization_list[0] = select_depertment;
           // Literal3.Text = (ddlsearch_organization.SelectedValue);
            _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);

            ddlsearch_department.DataSource = _data_softwarelicense.organization_list;
            ddlsearch_department.DataTextField = "DeptNameTH";
            ddlsearch_department.DataValueField = "RDeptIDX";
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new ListItem("กรุณาเลือก depertment", "0"));


        }
        else if (_type == 1)
        {
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new ListItem("กรุณาเลือก depertment", "0"));
        }

        return ddlsearch_department.SelectedItem.Value;

    }

    //protected string action_select_section(int _type, string _selectvalue)
    //{
    //    //ddlsearch_section.Items.Clear();

    //    //if (_type == 0)
    //    //{

    //    //    _data_softwarelicense.organization_list = new organization_detail[1];
    //    //    organization_detail select_section = new organization_detail();

    //    //    select_section.OrgIDX = Int32.Parse(ddlsearch_organization.SelectedValue);
    //    //    select_section.RDeptIDX = Int32.Parse(ddlsearch_department.SelectedValue);

    //    //    _data_softwarelicense.organization_list[0] = select_section;
    //    //    Literal2.Text = ddlsearch_department.SelectedValue;

    //    //    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicense);

    //    //    ddlsearch_section.DataSource = _data_softwarelicense.organization_list;
    //    //    ddlsearch_section.DataTextField = "SecNameTH";
    //    //    ddlsearch_section.DataValueField = "RSecIDX";
    //    //    ddlsearch_section.DataBind();
    //    //    ddlsearch_section.Items.Insert(0, new ListItem("กรุณาเลือก section", "00"));

    //    //}
    //    //else if (_type == 1)
    //    //{
    //    //    ddlsearch_section.DataBind();
    //    //    ddlsearch_section.Items.Insert(0, new ListItem("กรุณาเลือก section", "00"));
    //    //}


    //    //return ddlsearch_section.SelectedItem.Value;
    //}


    protected string action_select_position(int _type, string _selectvalue)
    {

        var ddlposition = (DropDownList)fvCreatePurchasequipment.FindControl("ddlposition");

        ddlposition.Items.Clear();

        if (_type == 0)
        {
            /*
            data_purchase _dataposition = new data_purchase();
            _dataposition._organization_list = new _organization_details[1];
            _organization_details select_position = new _organization_details();


            select_position.emp_idx = Int32.Parse(ViewState["emp_idx"].ToString());
            _dataposition._organization_list[0] = select_position;

            _dataposition = callServicePurchase(_urlGetposition, _dataposition);

            ddlposition.DataSource = _dataposition._organization_list;
            ddlposition.DataTextField = "PosNameTH";
            ddlposition.DataValueField = "RPosIDX";
            ddlposition.DataBind();
            ddlposition.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง", "0"));
            */
            data_itasset _dataitasset = new data_itasset();
            _dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_position = new its_lookup();

            obj_position.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
            obj_position.operation_status_id = "position";

            _dataitasset.its_lookup_action[0] = obj_position;
            _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

            ddlposition.DataSource = _dataitasset.its_lookup_action;
            ddlposition.DataTextField = "zname";
            ddlposition.DataValueField = "idx";
            ddlposition.DataBind();
            ddlposition.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง", "0"));

        }
        else if (_type == 1)
        {
            ddlposition.DataBind();
            ddlposition.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง", "0"));
        }


        return ddlposition.SelectedItem.Value;
    }



    protected void action_select_yearly()
    {
        DateTime datetime = new DateTime(DateTime.Now.Ticks);
        int year = datetime.Year;


        ddl_yearly.AppendDataBoundItems = true;
        ddl_yearly.Items.Add(new ListItem("กรุณาเลือกปีที่ต้องการ....", "0"));
        ddl_yearly.Items.Add(new ListItem("2017", "1"));
        ddl_yearly.Items.Add(new ListItem("2016", "2"));


    }

    protected void action_ddlselect_mountly()
    {
        ddl_mountly.AppendDataBoundItems = true;
        ddl_mountly.Items.Add(new ListItem("กรุณาเลือกเดือนที่ต้องการ....", "0"));
        ddl_mountly.Items.Add(new ListItem("มกราคม", "1"));
        ddl_mountly.Items.Add(new ListItem("กุมภาพันธ์", "2"));
        ddl_mountly.Items.Add(new ListItem("มีนาคม", "3"));
        ddl_mountly.Items.Add(new ListItem("เมษายน", "4"));
        ddl_mountly.Items.Add(new ListItem("พฤษภาคม", "5"));
        ddl_mountly.Items.Add(new ListItem("มิถุนายน", "6"));
        ddl_mountly.Items.Add(new ListItem("กรกฎาคม", "7"));
        ddl_mountly.Items.Add(new ListItem("สิงหาคม", "8"));
        ddl_mountly.Items.Add(new ListItem("กันยายน", "9"));
        ddl_mountly.Items.Add(new ListItem("ตุลาคม", "10"));
        ddl_mountly.Items.Add(new ListItem("พฤษศจิกายน", "11"));
        ddl_mountly.Items.Add(new ListItem("ธันวาคม", "12"));
        ddl_mountly.DataBind();

    }

    protected void _action_select_purchase()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchaselist = new u0_purchase_equipment_details();

        select_purchaselist.action_type = int.Parse(ViewState["action_type"].ToString()); //เรียกข้อมูลเพื่อนุมัติ
        select_purchaselist.m0_node_idx = int.Parse(ViewState["_node_idx"].ToString());
        select_purchaselist.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchaselist;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);


        ViewState["purchase_list"] = _data_purchase.u0_purchase_equipment_list;

        //setGridViewDataBind(gvpurchase_equipmentlist, _data_purchase.u0_purchase_equipment_list);
        setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);

        if (_data_purchase.u0_purchase_equipment_list == null)
        {
            check_approve_all.Visible = false;
            btnapproveall.Visible = false;
            btnnotapprove.Visible = false;
        }



    }


  
    protected void action_select_waiting_budget_approve()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchaselist = new u0_purchase_equipment_details();

        select_purchaselist.action_type = 5; //เรียกข้อมูลเพื่อนุมัติ
                                             // select_purchaselist.m0_node_idx = 5;
        select_purchaselist.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchaselist;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        ViewState["purchase_list"] = _data_purchase.u0_purchase_equipment_list;

        setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);

    }

  
    protected void action_select_purchaselist_for_department()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchasefordepartment = new u0_purchase_equipment_details();

        select_purchasefordepartment.action_type = 4; //เรียกข้อมูลการขอซื้อมาทั้งหมดตามฝ่ายที่ตัวเองอยู่
        select_purchasefordepartment.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchasefordepartment;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

     
        ViewState["purchase_list"] = _data_purchase.u0_purchase_equipment_list;
        setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);

    }

    protected void action_select_information_employee()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_rdept_employee = new u0_purchase_equipment_details();

        select_rdept_employee.action_type = 3; //เรียกข้อมูลพนักงาน
        select_rdept_employee.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
        _data_purchase.u0_purchase_equipment_list[0] = select_rdept_employee;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        ViewState["rdept_idx"] = _data_purchase.u0_purchase_equipment_list[0].dept_idx_purchase;
        ViewState["rsec_idx"] = _data_purchase.u0_purchase_equipment_list[0].sec_idx_purchase;

        setGridViewDataBind(gvpurchase_equipmentlist, _data_purchase.u0_purchase_equipment_list);

    }

    protected void action_select_details_employee()
    {
        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_rdept_employee = new u0_purchase_equipment_details();

        select_rdept_employee.action_type = 3; //เรียกข้อมูลพนักงาน
        select_rdept_employee.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
        __data_purchase.u0_purchase_equipment_list[0] = select_rdept_employee;
        __data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, __data_purchase);

        fvdetails_employee_purchase.DataSource = __data_purchase.u0_purchase_equipment_list;
        fvdetails_employee_purchase.DataBind();

    }

    protected void action_select_purchase()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchase = new u0_purchase_equipment_details();

        select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        fvinformation_purchasequipment.DataSource = _data_purchase.u0_purchase_equipment_list;
        fvinformation_purchasequipment.DataBind();

    }

    protected void action_select_picture_organization()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchase = new u0_purchase_equipment_details();

        select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

       // Repeater_picture_organization.DataSource = _data_purchase.u0_purchase_equipment_list;
      //  Repeater_picture_organization.DataBind();
    }

    protected void action_select_details_purchase()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchase = new u0_purchase_equipment_details();

        select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        fvdetails_purchasequipment.DataSource = _data_purchase.u0_purchase_equipment_list;
        fvdetails_purchasequipment.DataBind();

    }

    protected void action_select_items_purchase()
    {

        ///var gvItemspurchase = (GridView)fvdetails_purchasequipment.FindControl("gvItemspurchase");


        data_purchase _dataItems = new data_purchase();
        
        _dataItems.items_purchase_list = new items_purchase_details[1];
        items_purchase_details select_items = new items_purchase_details();

        //select_items.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_items.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _dataItems.items_purchase_list[0] = select_items;
      

        _dataItems = callServicePurchase(_urlGetitems_purchase, _dataItems);


        if (_dataItems.return_code == "0")
        {


            ViewState["items_purchaselist"] = _dataItems.items_purchase_list;

            //if (_dataItems.items_purchase_list != null)

            //{

            //    ViewState["_nametype_equipment"] += _dataItems.items_purchase_list[0].type_equipment.ToString() + ",";

            //}
            //else

            //{
            //    ViewState["_nametype_equipment"] = "-";
            //}


            gvItemspurchase.DataSource = ViewState["items_purchaselist"];//_dataItems.items_purchase_list;
            gvItemspurchase.DataBind();



        }

        // Literal3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataItems));

    }


    protected void action_select_purchase_for_edit()
    {

        ///var gvItemspurchase = (GridView)fvdetails_purchasequipment.FindControl("gvItemspurchase");


        data_purchase _dataItems_edit = new data_purchase();

        _dataItems_edit.items_purchase_list = new items_purchase_details[1];
        items_purchase_details selectedit_items = new items_purchase_details();

        //select_items.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        selectedit_items.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _dataItems_edit.items_purchase_list[0] = selectedit_items;


        _dataItems_edit = callServicePurchase(_urlGetitems_purchase, _dataItems_edit);

        // Literal3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataItems));

        gvEditpurchaselist.DataSource = _dataItems_edit.items_purchase_list;
        gvEditpurchaselist.DataBind();

    }



    protected void action_select_costcenter_purchase()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchase = new u0_purchase_equipment_details();

        select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        fvmanage_approvepurchase.DataSource = _data_purchase.u0_purchase_equipment_list;
        fvmanage_approvepurchase.DataBind();

    }

    protected void action_select_result_itsupport()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details result_itsupport = new u0_purchase_equipment_details();

        result_itsupport.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        result_itsupport.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = result_itsupport;


        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase));

        fvresultpurchase.DataSource = _data_purchase.u0_purchase_equipment_list;
        fvresultpurchase.DataBind();

    }

    //protected void action_select_result_costcenter()
    //{
    //    _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
    //    u0_purchase_equipment_details result_costcenter = new u0_purchase_equipment_details();

    //    result_costcenter.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
    //    result_costcenter.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

    //    _data_purchase.u0_purchase_equipment_list[0] = result_costcenter;


    //    _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);
    //    // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase));

    //    fvcostcenter.DataSource = _data_purchase.u0_purchase_equipment_list;
    //    fvcostcenter.DataBind();

    //}

    protected void action_edit_details_purchase()
    {
        var gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");

        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_purchase = new u0_purchase_equipment_details();

        select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

      //  ViewState["datapurchase_intoEdit"] = _data_purchase.u0_purchase_equipment_list;

        //ViewState["_datasetedit_equipmentidx"] = _data_purchase.u0_purchase_equipment_list[0].m0_equipment_idx;
        //ViewState["_datasetedit_equipmentname"] = _data_purchase.u0_purchase_equipment_list[0].name_equipment;


        //var _datasetedit_purchase = (DataSet)ViewState["datapurchase_intoEdit"];
        //var _dataroweditpurchase = _datasetedit_purchase.Tables[0].NewRow();

        //_dataroweditpurchase["equipment_name"] = (ViewState["_datasetedit_equipmentidx"].ToString());

        //_datasetedit_purchase.Tables[0].Rows.Add(_dataroweditpurchase);
        //ViewState["datapurchase_intoEdit"] = _datasetedit_purchase;

        //gvpurchaselist.DataSource = _datasetedit_purchase.Tables[0];
        //gvpurchaselist.DataBind();



        // check_mail.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["datapurchase_intoEdit"]));


        //    gvpurchaselist.DataSource = ViewState["data_purchase_intoEdit"];
        //  gvpurchaselist.DataBind();


        //fvCreatePurchasequipment.ChangeMode(FormViewMode.Edit);
        //fvCreatePurchasequipment.DataSource = _data_purchase.u0_purchase_equipment_list;
        // fvCreatePurchasequipment.DataBind();


    }

    protected void action_select_history_purchase()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_history_purchase = new u0_purchase_equipment_details();

        select_history_purchase.action_type = 2; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ (history purchase)
        select_history_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_history_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        history__purchasequipment.DataSource = _data_purchase.u0_purchase_equipment_list;
        history__purchasequipment.DataBind();

    }

    protected void action_select_summary_purchase()
    {
        var gvlist_summarypurchase = (GridView)fvsummary_itemspurchase.FindControl("gvlist_summarypurchase");

        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_history_purchase = new u0_purchase_equipment_details();

        select_history_purchase.action_type = 5; //รายการขอซื้อที่ HR Director อนุมัติแล้ว มาสรุปรายการขอซื้อให้แผนก IT Support ดำเนินการต่อ เพื่อส่งให้กับ Budget
        select_history_purchase.m0_node_idx = 4;
        select_history_purchase.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_history_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        gvlist_summarypurchase.DataSource = _data_purchase.u0_purchase_equipment_list;
        gvlist_summarypurchase.DataBind();

    }

    protected void action_select_history()
    {
        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_history_purchase = new u0_purchase_equipment_details();

        select_history_purchase.action_type = 2; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ (history purchase)
        select_history_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());

        _data_purchase.u0_purchase_equipment_list[0] = select_history_purchase;
        _data_purchase = callServicePurchase(_urlGetpurchase_equipmentlist, _data_purchase);

        history_purchase_action.DataSource = _data_purchase.u0_purchase_equipment_list;
        history_purchase_action.DataBind();

    }


    protected void action_select_location()
    {

        var ddllocation = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation");

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_location = new u0_purchase_equipment_details();

        ddllocation.AppendDataBoundItems = true;
        ddllocation.Items.Add(new ListItem("กรุณาเลือกสถานที่", "0"));

        __data_purchase.u0_purchase_equipment_list[0] = select_location;
        __data_purchase = callServicePurchase(_urlGetlocation, __data_purchase);

        ddllocation.DataSource = __data_purchase.u0_purchase_equipment_list;
        ddllocation.DataTextField = "name_location";
        ddllocation.DataValueField = "m0_location_idx";
        ddllocation.DataBind();
       // ddllocation.SelectedValue = _location;
    }


    protected void action_select_location_search()
    {

      //  var ddllocation_search = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation_search");

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details select_location = new u0_purchase_equipment_details();

        ddllocation_search.AppendDataBoundItems = true;
        ddllocation_search.Items.Add(new ListItem("กรุณาเลือกสถานที่", "0"));

        __data_purchase.u0_purchase_equipment_list[0] = select_location;
        __data_purchase = callServicePurchase(_urlGetlocation, __data_purchase);

        ddllocation_search.DataSource = __data_purchase.u0_purchase_equipment_list;
        ddllocation_search.DataTextField = "name_location";
        ddllocation_search.DataValueField = "m0_location_idx";
        ddllocation_search.DataBind();
    }

    protected void action_select_equipmenttype()
    {

        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        var txtquipmenttype = (TextBox)fvCreatePurchasequipment.FindControl("txtquipmenttype_idx");

        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail select_equipmentype = new m0_type_purchase_equipment_detail();

        ddlequipmenttype.AppendDataBoundItems = true;
        ddlequipmenttype.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์ขอซื้อ", "0"));

        select_equipmentype.status_equipment = 9;

        _data_purchase.m0_type_purchase_equipment_list[0] = select_equipmentype;
        _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);

        ViewState["test"] = _data_purchase.m0_type_purchase_equipment_list[0].m0_unit_idx.ToString();

        ddlequipmenttype.DataSource = _data_purchase.m0_type_purchase_equipment_list;
        ddlequipmenttype.DataTextField = "name_equipment_type";
        ddlequipmenttype.DataValueField = "m0_equipment_type_idx";
        ddlequipmenttype.DataBind();
        //ddlequipmenttype.SelectedValue = "1"; //txtquipmenttype.Text;
    }

    protected void action_select_purchasetype()
    {

        var ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
        var txtpurchasetype = (TextBox)fvCreatePurchasequipment.FindControl("txtpurchasetype_idx");

        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail select_equipmentype = new m0_type_purchase_equipment_detail();

        ddlpurchasetype.AppendDataBoundItems = true;
        ddlpurchasetype.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

        select_equipmentype.status_equipment = 1;

        _data_purchase.m0_type_purchase_equipment_list[0] = select_equipmentype;
        _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);

        ddlpurchasetype.DataSource = _data_purchase.m0_type_purchase_equipment_list;
        ddlpurchasetype.DataTextField = "name_purchase_type";
        ddlpurchasetype.DataValueField = "m0_purchase_type_idx";
        ddlpurchasetype.DataBind();
        // ddlpurchasetype.SelectedValue = "1";//txtpurchasetype.Text;

    }



    protected void action_select_edit_purchasetype()
    {

        var ddlpurchasetype_edit = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
        var txtpurchasetype_edit = (TextBox)fvCreatePurchasequipment.FindControl("txtpurchasetype_idx");

        _data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail select_equipmentype = new m0_type_purchase_equipment_detail();

        ddlpurchasetype_edit.AppendDataBoundItems = true;
        ddlpurchasetype_edit.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

        select_equipmentype.status_equipment = 1;

        _data_purchase.m0_type_purchase_equipment_list[0] = select_equipmentype;
        _data_purchase = callServicePurchase(_urlGetequipment_purchasetype, _data_purchase);

        ddlpurchasetype_edit.DataSource = _data_purchase.m0_type_purchase_equipment_list;
        ddlpurchasetype_edit.DataTextField = "name_purchase_type";
        ddlpurchasetype_edit.DataValueField = "m0_purchase_type_idx";
        ddlpurchasetype_edit.DataBind();
        ddlpurchasetype_edit.SelectedValue = txtpurchasetype_edit.Text;

    }


    #endregion

    #region update
    //protected void action_approve_purchase() // อนุมัติ หรือ ไม่อนุมัติ
    //{

    //    var _lbemail = (Label)fvinformation_purchasequipment.FindControl("lbemail");

    //    _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
    //    u0_purchase_equipment_details u0_approve_purchase = new u0_purchase_equipment_details();

    //    _data_purchase.u1_purchase_equipment_list = new u1_purchase_equipment_details[1];
    //    u1_purchase_equipment_details u1_approve_purchase = new u1_purchase_equipment_details();

      


    //    //กรณีการขอซื้อ อนุมัติ หรือ ไม่อนุมัติ แล้วส่งไปตามผู้ที่เกี่ยวข้อง

    //    u0_approve_purchase.m0_actor_idx = int.Parse(ViewState["actor_idx"].ToString());
    //    u0_approve_purchase.m0_node_idx = int.Parse(ViewState["node_idx"].ToString());
    //    u0_approve_purchase.status_purchase = int.Parse(ViewState["setapprove_purchase"].ToString());
    //    u0_approve_purchase.m0_purchase_idx = int.Parse(ViewState["m0_purchase_idx"].ToString());


    //    u0_approve_purchase.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
    //    u0_approve_purchase.details_purchase = txtcomment_purchase.Text;
    //    u0_approve_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());



    //    //:::::::::::::::: CONDITION IT SUPPORT APPROVE PURCHASE ::::::::::::::::::::::::://

    //    //if (int.Parse(ViewState["rsec_idx"].ToString()) == it_support)
    //    //{

    //    //    GridView gvItemspurchase = (GridView)viewmanage_purchasequipment.FindControl("gvItemspurchase");

    //    //    for (int n = 0; n < gvItemspurchase.Rows.Count; n++)
    //    //    {

    //    //        DropDownList ddlprice_equipment = (DropDownList)gvItemspurchase.Rows[n].FindControl("ddlprice_equipment");
    //    //        Label _lbItemsnumber = (Label)gvItemspurchase.Rows[n].FindControl("_lbItemsnumber");
    //    //        TextBox txtcommentformITSupport = (TextBox)gvItemspurchase.Rows[n].FindControl("txtcommentformITSupport");

    //    //        data_purchase _datapurcahseITapprove = new data_purchase();
    //    //        _datapurcahseITapprove.items_purchase_list = new items_purchase_details[1];
    //    //        items_purchase_details u2_approve_purchase = new items_purchase_details();

    //    //        // u2_approve_purchase.u2_purchase_idx = int.Parse(_lbItemsnumber.Text);
    //    //        u2_approve_purchase.m0_price = int.Parse(ddlprice_equipment.SelectedValue);
    //    //        u2_approve_purchase.commentfromITSupport = txtcommentformITSupport.Text;
    //    //        u2_approve_purchase.u2_purchase_idx = int.Parse(_lbItemsnumber.Text);

    //    //        // _u0tempListpurchase[n].u0_purchase_idx;

    //    //        _datapurcahseITapprove.items_purchase_list[0] = u2_approve_purchase;
    //    //        Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datapurcahseITapprove));

    //    //       // _datapurcahseITapprove = callServicePurchase(_urlSetpurchaseitems, _datapurcahseITapprove);


    //    //    }

    //    //    //action_select_summary_purchase();





    //    //}






    //    //if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "4")
    //    //{
    //    //    //u0_approve_purchase.costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);
    //    //}

    //    //กรณี Budget กรอกเลข IO
    //    if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "11")
    //    {
    //        u0_approve_purchase.number_io = txtNoIO.Text;

    //    }

    //    //กรณี Asset กรอกเลข Asset
    //    else if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "8")
    //    {

    //        u0_approve_purchase.number_asset = txtNoAsset.Text;

    //    }
           

    //    //string _email = "";

    //    ////กรณี sent mail อนุมัติรายการ
    //    //if (ViewState["setapprove_purchase"].ToString() == "5")
    //    //{

    //    //    u0_approve_purchase.node_status_purchase = "อนุมัติรายการขอซื้อ";
    //    //    u0_approve_purchase.costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);

    //    //}

    //    ////กรณี sent mail ไม่อนุมัติรายการ (แก้ไขเอกสารใหม่)
    //    //else if (ViewState["setapprove_purchase"].ToString() == "6")

    //    //{

    //    //    u0_approve_purchase.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสารใหม่)";
    //    //    ViewState["_email"] = _lbemail.Text;




    //    //}

    //    ////กรณี sent mail ไม่อนุมัติรายการ (จบการดำเนินการ)
    //    //else if (ViewState["setapprove_purchase"].ToString() == "7")

    //    //{
    //    //    u0_approve_purchase.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";
    //    //    ViewState["_email"] = _lbemail.Text;
    //    //}


    //    //////ข้อมูลในการส่ง e-mail

    //    ////u0_approve_purchase.document_code = ""; //รหัสเอกสาร
    //    ////u0_approve_purchase.details_purchase = txtcomment_purchase.Text; //comment รายการขอซื้อ

    //    _data_purchase.u0_purchase_equipment_list[0] = u0_approve_purchase;
    //    _data_purchase.u1_purchase_equipment_list[0] = u1_approve_purchase;

    //    //sent e - mail
    //    #region sent e mail

    //   // string emailmove = "msirirak4441@gmail.com";
    //    string replyempmove = "sirinyamod@hotmail.com";
    //    ////string replyempmove = "nipon@taokeanoi.co.th";


    //    _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
    //   //// _mail_body = servicemail.approve_purchase(_data_purchase.u0_purchase_equipment_list[0]);


    //    #endregion

    //     check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase));

    //    //_data_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_purchase);

       
    //    var _returncode = _data_purchase.return_code;

          


    //    ////ถ้าบันทึกสำเร็จให้ส่ง e-mail
    //    //if (_returncode == "0")
    //    //{

    //    //    //call service e - mail
    //    //    Literal1.Text = "sent email ของเลข Node นี้ ==== >" + ViewState["node_idx"] + "ให้กับทาง";


    //    //    //กรณีอนุมัติแล้วส่ง e- mail ต่อให้กับผู้ที่เกี่ยวข้อง
    //    //    if (ViewState["setapprove_purchase"].ToString() == "5")
    //    //    {
    //    //        var _returnmail = _data_purchase.return_email;

    //    //        _email = _returnmail;
    //    //        Literal2.Text = _returnmail;


    //    //    }
    //    //    else
    //    //    {
    //    //        _email = ViewState["_email"].ToString();
    //    //        check_mail.Text = _email;

    //    //    }


    //    //    //   servicemail.SendHtmlFormattedEmailFull(_email, "", replyempmove, _mail_subject, _mail_body);
    //    //    //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
    //    //}

    //    //else
    //    //{


    //    //}







    //}


    protected void action_edit_document_purchasequipment()  //แก้ไขรายการขอซื้อ
    {
        var ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        var ddllocation = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation");
        var txtdetails_purchase = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_purchase");
        var txtquantity = (TextBox)fvCreatePurchasequipment.FindControl("txtquantity");

        _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details editdoc_purchase = new u0_purchase_equipment_details();

        _data_purchase.u1_purchase_equipment_list = new u1_purchase_equipment_details[1];
        u1_purchase_equipment_details insert_comment = new u1_purchase_equipment_details();

        //insert table purchase_u0_document and  table purchase_u1_document
        editdoc_purchase.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
        editdoc_purchase.m0_purchase_idx = int.Parse(ddlpurchasetype.SelectedValue);
        editdoc_purchase.m0_equipment_idx = int.Parse(ddlequipmenttype.SelectedValue);
        editdoc_purchase.m0_location_idx = int.Parse(ddllocation.SelectedValue);
        editdoc_purchase.quantity_equipment = int.Parse(txtquantity.Text);
        editdoc_purchase.details_purchase = txtdetails_purchase.Text;
        editdoc_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());
       

        if (int.Parse(ddlpurchasetype.SelectedValue) == 1)
        {
            editdoc_purchase.status_purchase = 1;
            editdoc_purchase.m0_node_idx = 10;
            editdoc_purchase.m0_actor_idx = 1;

        }
        else if (int.Parse(ddlpurchasetype.SelectedValue) == 2)
        {

            editdoc_purchase.status_purchase = 1;
            editdoc_purchase.m0_node_idx = 14;
            editdoc_purchase.m0_actor_idx = 1;

        }


        editdoc_purchase.details_purchase = txtcomment_purchase.Text;

        _data_purchase.u0_purchase_equipment_list[0] = editdoc_purchase;
        _data_purchase.u1_purchase_equipment_list[0] = insert_comment;

        _data_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_purchase);

        #region sent e mail

        ////ข้อมูลในการส่ง e-mail
        //insert_purchaselist.name_purchase = (ddlpurchasetype.SelectedItem.Text);
        //insert_purchaselist.name_equipment = (ddlequipmenttype.SelectedItem.Text);
        ////insert_purchaselist.quantity_equipment = (txtquantity.Text);
        //insert_purchaselist.fullname_purchase = "ศิริรักษ์ บุญเรืองจักร";
        //insert_purchaselist.empcode_purchase = "59001349";
        //insert_purchaselist.pos_name_purchase = "เจ้าหน้าที่พัฒนาแอพพลิเคชั่น";
        //insert_purchaselist.details_purchase = txtdetails_purchase.Text;

        //check_XML.Text = (ddlpurchasetype.SelectedItem.Text);

        //_data_purchase.u0_purchase_equipment_list[0] = insert_purchaselist;

        //string emailmove = "msirirak4441@gmail.com";
        //string replyempmove = "sirinyamod@hotmail.com";
        //////string replyempmove = "nipon@taokeanoi.co.th";


        //_mail_subject = "[MIS/ITPurchase] - " + "รายการขอซื้อ";
        //_mail_body = servicemail.create_purchase(_data_purchase.u0_purchase_equipment_list[0]);

        ////servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
        //servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

        #endregion


    }

    #endregion

    #region bind dropdrowlist on search

    protected void action_ddlsearch_equipment()
    {

    

        ddlsearch_typequipment.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail ddlsearch_equipment = new m0_type_purchase_equipment_detail();

        ddlsearch_typequipment.AppendDataBoundItems = true;
        ddlsearch_typequipment.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์", "0"));

        ddlsearch_equipment.status_equipment = 9;

        __data_purchase.m0_type_purchase_equipment_list[0] = ddlsearch_equipment;
        __data_purchase = callServicePurchase(_urlGetequipment_purchasetype, __data_purchase);

        ddlsearch_typequipment.DataSource = __data_purchase.m0_type_purchase_equipment_list;
        ddlsearch_typequipment.DataTextField = "name_equipment_type";
        ddlsearch_typequipment.DataValueField = "m0_equipment_type_idx";
        ddlsearch_typequipment.DataBind();

    }


    protected void action_ddlsearch_purchase()
    {

    

        ddlsearch_typepurchase.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
        m0_type_purchase_equipment_detail ddlsearch_purchase = new m0_type_purchase_equipment_detail();

        ddlsearch_typepurchase.AppendDataBoundItems = true;
        ddlsearch_typepurchase.Items.Add(new ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

        ddlsearch_purchase.status_equipment = 1;

        __data_purchase.m0_type_purchase_equipment_list[0] = ddlsearch_purchase;
        __data_purchase = callServicePurchase(_urlGetequipment_purchasetype, __data_purchase);

        ddlsearch_typepurchase.DataSource = __data_purchase.m0_type_purchase_equipment_list;
        ddlsearch_typepurchase.DataTextField = "name_purchase_type";
        ddlsearch_typepurchase.DataValueField = "m0_purchase_type_idx";
        ddlsearch_typepurchase.DataBind();

    }

    protected void action_ddlsearch_status()
    {

        ddlsearch_status.Items.Clear();

        data_purchase __data_purchase = new data_purchase();
        __data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
        u0_purchase_equipment_details search_status = new u0_purchase_equipment_details();

        ddlsearch_status.AppendDataBoundItems = true;
        ddlsearch_status.Items.Add(new ListItem("กรุณาเลือกสถานะรายการ", "0"));

        __data_purchase.u0_purchase_equipment_list[0] = search_status;
        __data_purchase = callServicePurchase(_urlGetstatus_node, __data_purchase);

        ddlsearch_status.DataSource = __data_purchase.u0_purchase_equipment_list;
        ddlsearch_status.DataTextField = "node_name_purchase";
        ddlsearch_status.DataValueField = "m0_node_idx";
        ddlsearch_status.DataBind();

    }




    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var dropdownlistName = (DropDownList)sender;

        var ddltype_unitequipment = (DropDownList)fvCreatePurchasequipment.FindControl("ddltype_unitequipment");
        var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
        var ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
        var ddlposition = (DropDownList)fvCreatePurchasequipment.FindControl("ddlposition");

        

        switch (dropdownlistName.ID)
        {

            case "ddlequipment_edit":
                GridView gvpurchaselist1 = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                foreach (GridViewRow oldrow_move1 in gvpurchaselist1.Rows)
                {
                    DropDownList ddlequipment_edit1 = (DropDownList)oldrow_move1.FindControl("ddlequipment_edit");
                    DropDownList ddlspec_edit1 = (DropDownList)oldrow_move1.FindControl("ddlspec_edit");
                    TextBox txtupdate_detailspec1 = (TextBox)oldrow_move1.FindControl("txtupdate_detailspec");
                    LinkButton btnupdate = (LinkButton)oldrow_move1.FindControl("btnupdate");

                    ddlspec_edit1.SelectedValue = "0";
                    txtupdate_detailspec1.Text = String.Empty;

                    if (int.Parse(ddlequipment_edit1.SelectedValue) == 1 || int.Parse(ddlequipment_edit1.SelectedValue) == 2 || int.Parse(ddlequipment_edit1.SelectedValue) == 4)
                    {
                        ddlspec_edit1.Visible = true;
                      
                    }
                    else
                    {
                        ddlspec_edit1.Visible = false;
                        ddlspec_edit1.SelectedValue = "0";
                    }

                    btnupdate.Focus();
                }

                    break;

            case "ddlspec_edit":

                GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                foreach (GridViewRow oldrow_move in gvpurchaselist.Rows)
                {
                    TextBox txtupdate_detailspec = (TextBox)oldrow_move.FindControl("txtupdate_detailspec");
                    DropDownList ddlspec_edit = (DropDownList)oldrow_move.FindControl("ddlspec_edit");
                    DropDownList ddlequipment_edit = (DropDownList)oldrow_move.FindControl("ddlequipment_edit");
                    LinkButton btnupdate1 = (LinkButton)oldrow_move.FindControl("btnupdate");

                    data_purchase _dtpurchase1 = new data_purchase();
                    _dtpurchase1.master_spec_list = new master_spec_details[1];
                    master_spec_details edit_spec_details1 = new master_spec_details();
                    edit_spec_details1.m0_spec_idx = int.Parse(ddlspec_edit.SelectedValue);
                    edit_spec_details1.m0_equipment_idx = int.Parse(ddlequipment_edit.SelectedValue);
                    _dtpurchase1.master_spec_list[0] = edit_spec_details1;
                    _dtpurchase1 = callServicePurchase(_urlGetmasterspec, _dtpurchase1);

                    if (_dtpurchase1.master_spec_list != null)
                    {
                        txtupdate_detailspec.Text = _dtpurchase1.master_spec_list[0].details_spec;
                        txtupdate_detailspec.Enabled = false;
                    }
                    else
                    {
                        txtupdate_detailspec.Enabled = true;

                    }

                    btnupdate1.Focus();
                }

                break;

            case "ddlsearch_organization":

               action_select_depertment(0, "0");
             //   action_select_section(1, "0");

                break;

            case "ddlsearch_department":

             //   action_select_section(0, "0");

                break;


            case "ddlequipmenttype":



                var panel_spec_equipment = (Panel)fvCreatePurchasequipment.FindControl("panel_spec_equipment");
                var btnaddpurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");

                if (ddlequipmenttype.SelectedValue == "1" || ddlequipmenttype.SelectedValue == "2" || ddlequipmenttype.SelectedValue == "4")
                {
                   
                    panel_spec_equipment.Visible = true;
                    action_select_masterspec(0, "0");

                }

                else

                {

                    var txtdetails_spec = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_spec");
                    panel_spec_equipment.Visible = false;
                    txtdetails_spec.Text = String.Empty;
                    txtdetails_spec.Enabled = true;

                }

                action_select_masterunit(0, "0");
                btnaddpurchase.Focus();

                break;

            case "ddlpurchasetype":

                Panel panel_upload_cutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_cutoff");
                Panel panel_upload_organization = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_organization");
                Panel panel_upload_refercutoff = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_refercutoff");
                Panel panel_title_uploadfile = (Panel)fvCreatePurchasequipment.FindControl("panel_title_uploadfile");
                Panel panel_upload_memo = (Panel)fvCreatePurchasequipment.FindControl("panel_upload_memo");
                var btnaddpurchase1 = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");

                LinkButton btnsavepurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnsavepurchase");

                if (ddlpurchasetype.SelectedValue == "1")
                {

                    panel_upload_cutoff.Visible = false;
                    panel_upload_refercutoff.Visible = false;
                    panel_title_uploadfile.Visible = true;
                    panel_upload_memo.Visible = true;

                   panel_upload_organization.Visible = true;

                 

                }

                else if (ddlpurchasetype.SelectedValue == "2")
                {

                    panel_upload_cutoff.Visible = true;
                    panel_upload_refercutoff.Visible = true;
                    panel_title_uploadfile.Visible = true;
                    panel_upload_memo.Visible = true;

                    panel_upload_organization.Visible = false;

                   // linkBtnTrigger(btnsavepurchase);
                }

                // linkBtnTrigger(btnsavepurchase);

                btnaddpurchase1.Focus();

                break;

            case "ddlspecidx":

                var txtquantity = (TextBox)fvCreatePurchasequipment.FindControl("txtquantity");
                var btnaddpurchase3 = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");
                
                action_select_spec_details();
                btnaddpurchase3.Focus();


                break;


            case "ddlequipment_typeedit":


               // GridView gvEditpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvEditpurchaselist");
                GridViewRow row = gvEditpurchaselist.Rows[gvEditpurchaselist.EditIndex];

                TextBox txteditUnit = (TextBox)row.FindControl("txteditUnit");
              //  TextBox txtedit_detailspec = (TextBox)row.FindControl("txtedit_detailspec");

                DropDownList ddlequipment_typeedit = (DropDownList)row.FindControl("ddlequipment_typeedit");
                DropDownList ddlm0_spec_idx = (DropDownList)row.FindControl("ddlm0_spec_idx");
                Label Labelm0_spec_idx = (Label)row.FindControl("Labelm0_spec_idx");



                //// var txteditUnit = (TextBox)rowUnit.FindControl("txteditUnit");

                data_purchase _data_purchase_edit6 = new data_purchase();

                _data_purchase_edit6.m0_type_purchase_equipment_list = new m0_type_purchase_equipment_detail[1];
                m0_type_purchase_equipment_detail select_equipmentype_edit6 = new m0_type_purchase_equipment_detail();

                select_equipmentype_edit6.m0_equipment_type_idx = int.Parse(ddlequipment_typeedit.SelectedValue);
                // select_equipmentype_edit6.status_purchase = 9;
              

                _data_purchase_edit6.m0_type_purchase_equipment_list[0] = select_equipmentype_edit6;

                

                _data_purchase_edit6 = callServicePurchase(_urlGetmasternameUnit, _data_purchase_edit6);
                // Literal1.Text = _data_purchase_edit6.m0_type_purchase_equipment_list[0].nameth_unit.ToString();//HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase_edit6));

                if (_data_purchase_edit6.m0_type_purchase_equipment_list[0].nameth_unit.ToString() != null)
                {

                    ViewState["_NMAEUNIT"] = _data_purchase_edit6.m0_type_purchase_equipment_list[0].nameth_unit.ToString();

                }
                else
                {

                    ViewState["_NMAEUNIT"] = "-";


                }

                txteditUnit.Text = ViewState["_NMAEUNIT"].ToString();

                if (ddlequipment_typeedit.SelectedValue == "1" || ddlequipment_typeedit.SelectedValue == "2" || ddlequipment_typeedit.SelectedValue == "4")
                {

                    ddlm0_spec_idx.Visible = true;
                    Labelm0_spec_idx.Visible =  true;


                    data_purchase _data_purchase_edit = new data_purchase();
                    _data_purchase_edit.master_spec_list = new master_spec_details[1];
                    master_spec_details select_masterspec_edit = new master_spec_details();

                    _data_purchase_edit.master_spec_list[0] = select_masterspec_edit;

                    _data_purchase_edit = callServicePurchase(_urlGetmasterspec, _data_purchase_edit);

                    ddlm0_spec_idx.DataSource = _data_purchase_edit.master_spec_list;
                    ddlm0_spec_idx.DataTextField = "name_spec";
                    ddlm0_spec_idx.DataValueField = "m0_spec_idx";
                    ddlm0_spec_idx.DataBind();
                    ddlm0_spec_idx.Items.Insert(0, new ListItem("กรุณาเลือกสเปคเครื่อง", "0"));

                }
                else
                {
                    ddlm0_spec_idx.Visible = false;
                    Labelm0_spec_idx.Visible = false;

                }


                break;

            case "ddlm0_spec_idx":


                GridViewRow _row = gvEditpurchaselist.Rows[gvEditpurchaselist.EditIndex];

                DropDownList ddlm0specidx = (DropDownList)_row.FindControl("ddlm0_spec_idx");
                DropDownList ddlequipmenttypeedit = (DropDownList)_row.FindControl("ddlequipment_typeedit");

                TextBox txtedit_detailspec = (TextBox)_row.FindControl("txtedit_detailspec");


                data_purchase _dtpurchase = new data_purchase();
                _dtpurchase.master_spec_list = new master_spec_details[1];
                master_spec_details edit_spec_details = new master_spec_details();

                edit_spec_details.m0_spec_idx = int.Parse(ddlm0specidx.SelectedValue);
                edit_spec_details.m0_equipment_idx = int.Parse(ddlequipmenttypeedit.SelectedValue);

                _dtpurchase.master_spec_list[0] = edit_spec_details;
                _dtpurchase = callServicePurchase(_urlGetmasterspec, _dtpurchase);

                if (_dtpurchase.master_spec_list != null)
                {


                    txtedit_detailspec.Text = _dtpurchase.master_spec_list[0].details_spec;
                    txtedit_detailspec.Enabled = false;



                }
                else
                {

                     txtedit_detailspec.Enabled = true;


                }




                break;

            case "ddlcondition":


             
                if (ddlcondition.SelectedValue == "3")
                {
                    txtenddate.Enabled = true;
                }

                else
                {

                    txtenddate.Enabled = false;

                }

                if (ddlcondition.SelectedValue == "0")

                {
                    txtenddate.Text = String.Empty;
                    txtstartdate.Text = String.Empty;

                }

                else
                {


                }




                break;

            case "ddlconditionReport":

                if (ddlconditionReport.SelectedValue == "3")
                {
                    txtEnddateReport.Enabled = true;
                }

                else
                {

                    txtEnddateReport.Enabled = false;

                }

                if (ddlconditionReport.SelectedValue == "0")

                {
                    txtEnddateReport.Text = String.Empty;
                    txtStartdateReport.Text = String.Empty;

                }

                else
                {


                }

                break;

            case "ddlOrganizationReport":

                action_select_rdeptreport(0, "0");

                break;

            case "ddlposition":
                var btnaddpurchase2 = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");
                btnaddpurchase2.Focus();
                break;

            case "ddltype_unitequipment":
                var btnaddpurchase4 = (LinkButton)fvCreatePurchasequipment.FindControl("btnaddpurchase");
                btnaddpurchase4.Focus();
                break;

            case "ddlcostCenter":
                var Focusset = (HyperLink)fvCreatePurchasequipment.FindControl("Focusset");
                Focusset.Focus();
                break;


        }
    }

    #endregion

    #region checkindexchange 
    protected void checkindexchange(object sender, EventArgs e)
    {
        var checkbox = (CheckBox)sender;

        switch (checkbox.ID)
        {
            case "check_approve_all":

                if (check_approve_all.Checked == true)
                {

                    _action_select_purchase();


                    foreach (GridViewRow gridrow in gvpurchase_equipmentlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                        Label idx = (Label)gridrow.Cells[0].FindControl("idx");
                        Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                        Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");

                        if (ViewState["purchase_list"] != null)
                        {
                            u0_purchase_equipment_details[] _templistpurchase = (u0_purchase_equipment_details[])ViewState["purchase_list"];

                            checklist.Checked = true;
                            checklist.Visible = true;
                            checklist.Enabled = false;
                            ViewState["__actor"] = lb_actoridx.Text;
                            ViewState["__node"] = lb_nodeidx.Text;


                        }

                    }

                }
                else if (check_approve_all.Checked == false)
                {
                    foreach (GridViewRow gridrow in gvpurchase_equipmentlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                        Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                        Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");

                        checklist.Checked = false;
                        checklist.Visible = true;
                        checklist.Enabled = true;

                        ViewState["__actor"] = lb_actoridx.Text;
                        ViewState["__node"] = lb_nodeidx.Text;
                    }

                }

                //    foreach (GridViewRow gridrow in gvpurchase_equipmentlist.Rows)
                //    {
                //        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                //        Label idx = (Label)gridrow.Cells[0].FindControl("idx");
                //        Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                //        Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");
                //        Label lbmailpurchase = (Label)gridrow.Cells[4].FindControl("lbmailpurchase");
                //        Label lb_purchasetype = (Label)gridrow.Cells[3].FindControl("lbnamepurchase");
                //        Label lbactorname = (Label)gridrow.Cells[4].FindControl("lbactorname");
                //        Label lb_documentcode = (Label)gridrow.Cells[0].FindControl("lbdocumentcode");
                //        GridView gv = (GridView)gridrow.Cells[3].FindControl("gvitems");


                //        if (ViewState["purchase_list"] != null)
                //        {
                //            u0_purchase_equipment_details[] _templistpurchase = (u0_purchase_equipment_details[])ViewState["purchase_list"];

                //            checklist.Checked = true;
                //            checklist.Visible = true;
                //            checklist.Enabled = false;
                //            ViewState["__actor"] = lb_actoridx.Text;
                //            ViewState["__node"] = lb_nodeidx.Text;
                //            ViewState["emailpurchase"] = lbmailpurchase.Text;
                //            ViewState["purcahsetype"] = lb_purchasetype.Text;
                //            ViewState["nameactor"] = lbactorname.Text;
                //            ViewState["doccode"] = lb_documentcode.Text;

                //            //foreach (GridViewRow _gridrow in gv.Rows)
                //            //{

                //            //    Label equipment_report = (Label)_gridrow.Cells[1].FindControl("equipment_report");
                //            //    ViewState["equipmentname"] += equipment_report.Text + ",";

                //            //}
                //        }

                //    }

                //}
                //else if (check_approve_all.Checked == false)
                //{
                //    foreach (GridViewRow gridrow in gvpurchase_equipmentlist.Rows)
                //    {
                //        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                //        Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                //        Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");
                //        Label lbmailpurchase = (Label)gridrow.Cells[4].FindControl("lbmailpurchase");
                //        Label lb_purchasetype = (Label)gridrow.Cells[3].FindControl("lbnamepurchase");
                //        Label lbactorname = (Label)gridrow.Cells[4].FindControl("lbactorname");
                //        Label lb_documentcode = (Label)gridrow.Cells[0].FindControl("lbdocumentcode");
                //        GridView gv = (GridView)gridrow.Cells[3].FindControl("gvitems");


                //        checklist.Checked = false;
                //        checklist.Visible = true;
                //        checklist.Enabled = true;

                //        ViewState["__actor"] = lb_actoridx.Text;
                //        ViewState["__node"] = lb_nodeidx.Text;
                //        ViewState["emailpurchase"] = lbmailpurchase.Text;
                //        ViewState["purcahsetype"] = lb_purchasetype.Text;
                //        ViewState["nameactor"] = lbactorname.Text;
                //        ViewState["doccode"] = lb_documentcode.Text;

                //        //foreach (GridViewRow _gridrow in gv.Rows)
                //        //{

                //        //    Label equipment_report = (Label)_gridrow.Cells[1].FindControl("equipment_report");
                //        //    ViewState["equipmentname"] += equipment_report.Text + ",";

                //        //}
                //    }

                //}


                break;


            case "cbrecipients":

                // _action_select_purchase();

                if (ViewState["purchase_list"] != null)
                {

                    u0_purchase_equipment_details[] _templistpurchase = (u0_purchase_equipment_details[])ViewState["purchase_list"];

                    int _checked = int.Parse(checkbox.Text);
                    _templistpurchase[_checked].selected = checkbox.Checked;

                }
                break;

        }


    }
    #endregion

    #region btn_Command

    protected void btn_command(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        switch (cmd_name)
        {
            case "viewmanu1":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "showboxcreate":

                mvMultiview.SetActiveView(viewCreatePurchase);
                ViewState["node_idx"] = int.Parse(cmd_arg);
                fvCreatePurchasequipment.ChangeMode(FormViewMode.Insert);
                fvCreatePurchasequipment.Focus();

                break;

            case "hiddenboxcreate":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "savepurchase":

                var _checkdataset_purchase = (DataSet)ViewState["Create_purchase_speclist"];
                var _checkdatarowpurchase = _checkdataset_purchase.Tables[0].NewRow();

                ///////////////เช็คประเภทการขอซื้อก่อนทำการบันทึกรายการขอซื้อ///////////////////
                if (ViewState["Create_purchase_speclist"] != null)
                {
                  //  check_mail.Text = "มีค่า";

                    int _numrowcheck = _checkdataset_purchase.Tables[0].Rows.Count; //จำนวนแถว

                    if (_numrowcheck > 0)
                    {
                        foreach (DataRow check in _checkdataset_purchase.Tables[0].Rows)
                        {

                            ViewState["check_purchase_type"] = check["purchase_type_idx"];

                            ///////////////กรณีประเภทการขอซื้อไม่ตรงกัน///////////////////
                            if (ViewState["purchase_type_check"].ToString() != ViewState["check_purchase_type"].ToString())
                            {

                                ViewState["CheckDataset_beforeinsert"] = "0";
                                break;

                            }
                            else

                            {

                                ViewState["CheckDataset_beforeinsert"] = "1";


                            }

                        }

                        if (ViewState["CheckDataset_beforeinsert"].ToString() == "1")
                        {
                            if (int.Parse(ViewState["node_idx"].ToString()) == 0)
                            {
                                action_create_purchasequipment();

                              Page.Response.Redirect(Page.Request.Url.ToString(), true);

                            }
                            else if (int.Parse(ViewState["node_idx"].ToString()) == 10)
                            {
                                action_edit_document_purchasequipment();
                                action_select_history();

                            }


                        }

                        else if (ViewState["CheckDataset_beforeinsert"].ToString() == "0")
                        {

                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ประเภทการขอซื้อของท่านไม่ถูกต้อง !!!');", true);

                        }

                    }


                }

                else

                {


                }



                break;

            case "manage_purchase":

                mvMultiview.SetActiveView(viewmanage_purchasequipment);
                string[] argument = new string[6];
                argument = e.CommandArgument.ToString().Split(';');
                int u0_purchase_idx = int.Parse(argument[0]);
                string nodidx = argument[1];
                int emp_idx_purchase = int.Parse(argument[2]);
                int m0_purchase_idx = int.Parse(argument[3]);
                int actor_idx = int.Parse(argument[4]);
                string documentCode = argument[5];

                ViewState["u0_purchase_idx"] = u0_purchase_idx;
                ViewState["node_idx"] = nodidx;
                ViewState["emp_idx_purchase"] = emp_idx_purchase;
                ViewState["m0_purchase_idx"] = m0_purchase_idx;
                ViewState["actor_idx"] = actor_idx;
                ViewState["_DOCUMENTCODE"] = documentCode;

                action_select_purchase();
                action_select_details_purchase();
                action_select_items_purchase();
                action_select_history_purchase();


                if (nodidx == "10")
                {

                    set_datetime_cutpurchase();
                    action_select_details_employee();
                    action_select_purchase_for_edit();
                    paneledituploadfile.Visible = true;

                    if (ViewState["m0_purchase_idx"].ToString() == "1")
                    {

                        panel_upload_refercutoff_edit.Visible = false;
                        panel_upload_cutoff_edit.Visible = false;


                    }
                    else
                    {

                        panel_upload_organization_edit.Visible = false;


                    }


                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_purchase"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString());


                    }
                    catch
                    {

                    }



                }

                else

                {



                }

                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_purchase"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["_DOCUMENTCODE"].ToString());


                }
                catch
                {

                }


                action_actor();
                action_node();

                SETFOCUS_ONTOP.Focus();

                break;

            case "saveapprove":

                ViewState["setapprove_purchase"] = int.Parse(cmd_arg);

                data_purchase _data_purchase_approve = new data_purchase();
                data_purchase _data_numberio_save = new data_purchase();
                data_purchase _data_numberasset_save = new data_purchase();

                ViewState["_chack_status_approve_success"] = "1";

                if (ViewState["setapprove_purchase"].ToString() == "5")
                {

                    if (ViewState["node_idx"].ToString() == "4" || ViewState["node_idx"].ToString() == "8" || ViewState["node_idx"].ToString() == "11")
                    {

                        GridView gvItemspurchase = (GridView)viewmanage_purchasequipment.FindControl("gvItemspurchase");



                        for (int i = 0; i <= gvItemspurchase.Rows.Count - 1; i++)
                        {


                            var _lbItemsnumber = (gvItemspurchase.Rows[i].Cells[0].FindControl("_lbItemsnumber") as Label).Text;
                            var txtcommentformITSupport = (gvItemspurchase.Rows[i].Cells[5].FindControl("txtcommentformITSupport") as TextBox).Text;
                            var txtminprice = (gvItemspurchase.Rows[i].Cells[5].FindControl("txtminprice") as TextBox).Text;
                            var txtmaxprice = (gvItemspurchase.Rows[i].Cells[5].FindControl("txtmaxprice") as TextBox).Text;
                            var txtNOAsset = (gvItemspurchase.Rows[i].Cells[7].FindControl("txtNOAsset") as TextBox).Text;
                            var items_quantity = (gvItemspurchase.Rows[i].Cells[3].FindControl("items_quantity") as Label).Text;
                            var gvionumber = (gvItemspurchase.Rows[i].Cells[6].FindControl("gvionumber") as GridView);
                            var items_type1 = (gvItemspurchase.Rows[i].Cells[1].FindControl("items_type1") as Label).Text;

                         
                            //::: budget approve input No. IO equipment ::://

                            if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "11")
                            {

                               
                                var _dataset_numberio = (DataSet)ViewState["create_ionumber_list"];

                                var _insert_numberio = new numberio_details[_dataset_numberio.Tables[0].Rows.Count];


                                int _numio = 0;

                                foreach (DataRow drnumberio in _dataset_numberio.Tables[0].Rows)
                                {

                                    _insert_numberio[_numio] = new numberio_details();
                                    _insert_numberio[_numio].idx = int.Parse(drnumberio["idx"].ToString());
                                    _insert_numberio[_numio].io = (drnumberio["io"].ToString());

                                    _numio++;

                                }

                                _data_numberio_save.numberio_list = _insert_numberio;

                            }


                            //::: asset approve input No. asset equipment ::://
                            else if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "8")
                            {

                               
                                var _dataset_numberasset = (DataSet)ViewState["create_assetnumber_list"];

                                var _insert_number_asset = new numberAsset_details[_dataset_numberasset.Tables[0].Rows.Count];


                                int num_asset = 0;

                                foreach (DataRow drnumberasset in _dataset_numberasset.Tables[0].Rows)
                                {

                                    _insert_number_asset[num_asset] = new numberAsset_details();
                                    _insert_number_asset[num_asset].idx_asset = int.Parse(drnumberasset["idx_asset"].ToString());
                                    _insert_number_asset[num_asset].asset_number = (drnumberasset["asset_number"].ToString());

                                    num_asset++;

                                }

                                _data_numberasset_save.numberAsset_list = _insert_number_asset;
                            

                            }


                            //::: it support input price , input comment and approve ::://
                            if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "4")
                            {


                                _data_purchase_approve.items_purchase_list = new items_purchase_details[1];
                                items_purchase_details _inputprice_comment = new items_purchase_details();


                                _inputprice_comment.u2_purchase_idx = int.Parse(_lbItemsnumber);
                                _inputprice_comment.min_price = txtminprice;
                                _inputprice_comment.max_price = txtmaxprice;
                                _inputprice_comment.commentfromITSupport = txtcommentformITSupport;
                                //ViewState["_equipment"] += items_type1 + ",";



                                _data_purchase_approve.items_purchase_list[0] = _inputprice_comment;

                                //success ให้ ดำเนินการต่อ
                                ViewState["_chack_status_approve_success"] = 1;


                                //    }
                            }

                            //}

                        }

                        if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "11")
                        {

                            //    Literal1.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_numberio_asset));
                            _data_numberio_save = callServicePurchase(_urlsetnumberio, _data_numberio_save);

                            if (_data_numberio_save.return_code == "0")
                            {
                                //success ให้ ดำเนินการต่อ
                                ViewState["_chack_status_approve_success"] = 1;

                            }
                            else

                            {
                                ViewState["_chack_status_approve_success"] = 0;

                            }

                        }
                        else if (ViewState["setapprove_purchase"].ToString() == "5" && ViewState["node_idx"].ToString() == "8")
                        {

                            _data_numberasset_save = callServicePurchase(_urlsetnumberasset, _data_numberasset_save);

                            if (_data_numberasset_save.return_code == "0")
                            {

                                //success ให้ ดำเนินการต่อ
                                ViewState["_chack_status_approve_success"] = 1;

                            }
                            else

                            {
                                ViewState["_chack_status_approve_success"] = 0;

                            }

                        }

                    }
                }

                else if (ViewState["setapprove_purchase"].ToString() != "5")
                {

                    ViewState["_chack_status_approve_success"] = 1;

                }

                if (ViewState["_chack_status_approve_success"].ToString() == "1")
                {
                  



                    items_purchase_details[] _items = (items_purchase_details[])ViewState["items_purchaselist"];

                    var linq_items = (from data_items in _items
                                      where data_items.u0_purchase_idx == int.Parse(ViewState["u0_purchase_idx"].ToString())
                    select new
                                      {
                                          data_items.type_equipment
                                      }).ToList();
                    string _test1 = "";
                    for (int z = 0; z < linq_items.Count(); z++)
                    {
                        _test1 += linq_items[z].type_equipment.ToString() + ",";
                    }




                    var _lbemail = (Label)fvinformation_purchasequipment.FindControl("lbemail");
                    var lbdocument_code = (Label)fvdetails_purchasequipment.FindControl("lbdocument_code");
                    var lbtypePurchase = (Label)fvdetails_purchasequipment.FindControl("lbtypePurchase");

                    _data_purchase_approve.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details u0_approve_purchase = new u0_purchase_equipment_details();

                    _data_purchase_approve.u1_purchase_equipment_list = new u1_purchase_equipment_details[1];
                    u1_purchase_equipment_details u1_approve_purchase = new u1_purchase_equipment_details();

                    u0_approve_purchase.m0_actor_idx = int.Parse(ViewState["actor_idx"].ToString());
                    u0_approve_purchase.m0_node_idx = int.Parse(ViewState["node_idx"].ToString());
                    u0_approve_purchase.status_purchase = int.Parse(ViewState["setapprove_purchase"].ToString());
                    u0_approve_purchase.m0_purchase_idx = int.Parse(ViewState["m0_purchase_idx"].ToString());


                    u0_approve_purchase.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
                    u0_approve_purchase.details_purchase = txtcomment_purchase.Text;
                    u0_approve_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());
                    ViewState["_chack_status_approve_success"] = 1;

                    //#region sent e mail


                    if (ViewState["setapprove_purchase"].ToString() == "5")
                    {

                        u0_approve_purchase.node_status_purchase = "อนุมัติรายการขอซื้อ";
                        //select ข้อมูลใน base
                       // _data_purchase_approve.u0_purchase_equipment_list[0] = u0_approve_purchase;
                    }
                    else if (ViewState["setapprove_purchase"].ToString() == "6")
                    {

                        u0_approve_purchase.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสารใหม่)";
                        ViewState["_email"] = _lbemail.Text;
                       // _data_purchase_approve.u0_purchase_equipment_list[0] = u0_approve_purchase;
                    }

                    else if (ViewState["setapprove_purchase"].ToString() == "7")
                    {

                        u0_approve_purchase.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";
                        ViewState["_email"] = _lbemail.Text;
                      //  _data_purchase_approve.u0_purchase_equipment_list[0] = u0_approve_purchase;
                    }

                    Label _hidden_name_actor = (Label)fvdetails_purchasequipment.FindControl("hidden_name_actor");
                    Label _lbdrept_name_purchase = (Label)fvinformation_purchasequipment.FindControl("lbdreptnamepurchase");

                    u0_approve_purchase.document_code = lbdocument_code.Text; //รหัสเอกสาร
                    u0_approve_purchase.details_purchase = txtcomment_purchase.Text; //comment รายการขอซื้อ
                    u0_approve_purchase.actor_name_purchase = _hidden_name_actor.Text; //รหัสเอกสาร
                    u0_approve_purchase.name_purchase = lbtypePurchase.Text;
                    u0_approve_purchase.drept_name_purchase = _lbdrept_name_purchase.Text;
                    u0_approve_purchase.name_equipment = _test1;
                    u0_approve_purchase.link = _link.ToString();


                    _data_purchase_approve.u0_purchase_equipment_list[0] = u0_approve_purchase;
                   // _data_purchase_approve.u1_purchase_equipment_list[0] = u1_approve_purchase;

                    string emailmove = "mongkonl.d@taokaenoi.co.th";
                    string replyempmove = "msirirak4441@gmail.com";
                    ////string replyempmove = "nipon@taokeanoi.co.th";

                    if (ViewState["node_idx"].ToString() == "8" && ViewState["setapprove_purchase"].ToString() != "5")
                    {
                        _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                   
                    }

                    else if (ViewState["node_idx"].ToString() != "8")
                    {

                        _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                     

                    }
                    _data_purchase_approve.u0_purchase_equipment_list[0] = u0_approve_purchase;

                    //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_purchase_approve));
                    _mail_body = servicemail.approve_purchase(_data_purchase_approve.u0_purchase_equipment_list[0]);

                    _data_purchase_approve = callServicePurchase(_urlsetapprove_purchase, _data_purchase_approve);



                    if (_data_purchase_approve.return_code == "0")
                    {

                        if (ViewState["node_idx"].ToString() == "8" && ViewState["setapprove_purchase"].ToString() != "5")
                        {

                            var _sentmail = _data_purchase_approve.return_email;
                            check_XML.Text = _sentmail;
                        servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

                        }

                        else if (ViewState["node_idx"].ToString() != "8")
                        {
                            //Literal2.Text = ViewState["_email"].ToString();
                         servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

                        }


                    }

                    #region สรุปการขอซื้อ

                    if (_data_purchase_approve.email_purchase_complete != null)
                    {
                        var comment = _data_purchase_approve.email_purchase_complete[0].comment.ToString();
                        var items = _data_purchase_approve.email_purchase_complete[0].list_items.ToString();
                        var _io_number = _data_purchase_approve.email_purchase_complete[0].io_number.ToString();
                        var _asset_number = _data_purchase_approve.email_purchase_complete[0].asset_number.ToString();
                        var _motive_purchase = _data_purchase_approve.email_purchase_complete[0].motive_purchase.ToString();
                        var _name_purchase = _data_purchase_approve.email_purchase_complete[0].name_purchase.ToString();
                        var _document_code = _data_purchase_approve.email_purchase_complete[0].document_code.ToString();
                        var _name_department = _data_purchase_approve.email_purchase_complete[0].name_department.ToString();

                        data_purchase _datasentemail = new data_purchase();
                        _datasentemail.email_purchase_complete = new email_purchase_details[1];
                        email_purchase_details sentmail = new email_purchase_details();

                        sentmail.motive_purchase = _motive_purchase;
                        sentmail.name_purchase = _name_purchase;
                        sentmail.document_code = _document_code;
                        sentmail.name_department = _name_department;

                        var splitcomment = comment.ToString().Split(',');
                        var splititems = items.ToString().Split(',');
                        var splitionumber = _io_number.ToString().Split(',');
                        var splitassetnumber = _asset_number.ToString().Split(',');


                        foreach (string ToEmail_items in splititems)
                        {
                            if (ToEmail_items != null)
                            {
                                sentmail.list_items += ToEmail_items + "<br />";


                            }
                        }

                        foreach (string ToEmail_comment in splitcomment)
                        {
                            if (ToEmail_comment != null)
                            {
                                sentmail.comment += ToEmail_comment + "<br />";


                            }
                        }

                        foreach (string ToEmail_ionumber in splitionumber)
                        {
                            if (ToEmail_ionumber != null)
                            {
                                sentmail.io_number += ToEmail_ionumber + "<br />";


                            }
                        }

                        foreach (string ToEmail_assetnumber in splitassetnumber)
                        {
                            if (ToEmail_assetnumber != null)
                            {
                                sentmail.asset_number += ToEmail_assetnumber + "<br />";


                            }
                        }

                        string _emailmove = "mongkonl.d@taokaenoi.co.th";
                        string _replyempmove = "msirirak4441@gmail.com";
                        ////string replyempmove = "nipon@taokeanoi.co.th";

                        _datasentemail.email_purchase_complete[0] = sentmail;

                        // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datasentemail));

                        _mail_subject = "[MIS/IT Purchase] - " + "เรื่อง รายการขอซื้ออุปกรณ์ที่ผ่านการอนุมัติ";
                        _mail_body = servicemail.purchase_complete(_datasentemail.email_purchase_complete[0]);
                        servicemail.SendHtmlFormattedEmailFull(_emailmove, "", _replyempmove, _mail_subject, _mail_body); //sent mail ตอน เทสระบบ



                    }

                    #endregion

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }

                else if(ViewState["_chack_status_approve_success"].ToString() == "0")
                { 

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ !!!');", true);

                }


                break;

            case "showboxsummarypurchase":

                mvMultiview.SetActiveView(viewmanage_purchasequipment);

                fvmanage_approvepurchase.Visible = false;
                content_history_purchase.Visible = false;
                action_select_summary_purchase();
                fvsummary_itemspurchase.Visible = true;

                break;

            case "sentlistpurchase":

                GridView gvlist_summarypurchase = (GridView)fvsummary_itemspurchase.FindControl("gvlist_summarypurchase");

                for (int i = 0; i < gvlist_summarypurchase.Rows.Count; i++)
                {

                    DropDownList ddlprice_equiptment = (DropDownList)gvlist_summarypurchase.Rows[i].FindControl("ddlprice_equiptment");
                    Label label_idx_purchase = (Label)gvlist_summarypurchase.Rows[i].FindControl("label_idx_purchase");
                    Label _actor_idx = (Label)gvlist_summarypurchase.Rows[i].FindControl("actor_idx");
                    Label _node_idx = (Label)gvlist_summarypurchase.Rows[i].FindControl("node_idx");


                    _data_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                    u0_purchase_equipment_details itsupport_approve = new u0_purchase_equipment_details();

                    itsupport_approve.m0_actor_idx = int.Parse(_actor_idx.Text);
                    itsupport_approve.m0_node_idx = int.Parse(_node_idx.Text);
                    itsupport_approve.u0_purchase_idx = int.Parse(label_idx_purchase.Text);
                    itsupport_approve.m0_price_idx = int.Parse(ddlprice_equiptment.SelectedValue);
                    itsupport_approve.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());
                    itsupport_approve.status_purchase = 5;


                    _data_purchase.u0_purchase_equipment_list[0] = itsupport_approve;

                    _data_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_purchase);


                }

                action_select_summary_purchase();
                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                SETFOCUS_ONTOP.Focus();

                break;

            case "showBoxsearch":

                panel_search.Visible = true;
                gvpurchase_equipmentlist.Visible = false;
                btnshowBoxsearch.Visible = false;
                btnhiddenBoxsearch.Visible = true;
                Panel_list_purchase.Visible = false;

                action_select_organization(0, "0");
                action_select_depertment(1, "0");
                action_ddlsearch_purchase();
                action_ddlsearch_status();
                action_ddlsearch_equipment();
                action_select_location_search();
                action_select_costcenter_search();

                break;

            case "hiddenBoxsearch":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "searching":

                //ค้นหาข้อมูล

                data_purchase _search_purchase = new data_purchase();

                _search_purchase.search_purchase_list = new search_purchase_details[1];


                search_purchase_details _seaching = new search_purchase_details();

                //ค้นหาตามวันที่
                _seaching.condition_date_type = int.Parse(ddlcondition.SelectedValue);
                _seaching.start_date = txtstartdate.Text;
                _seaching.end_date = txtenddate.Text;

                _seaching.document_code = txtdocumentcode.Text;
                _seaching.fullname_purchase = txtfirstname_lastname.Text;
                _seaching.empcode_purchase = txtempcode.Text;
                _seaching.org_idx_purchase = int.Parse(ddlsearch_organization.SelectedValue);
                _seaching.dept_idx_purchase = int.Parse(ddlsearch_department.SelectedValue);
                _seaching.m0_location_idx = int.Parse(ddllocation_search.SelectedValue);
                _seaching.costcenter_idx = int.Parse(ddl_costcenter.SelectedValue);
                _seaching.m0_purchase_idx = int.Parse(ddlsearch_typepurchase.SelectedValue);
                _seaching.m0_equipment_idx = int.Parse(ddlsearch_typequipment.SelectedValue);
                _seaching.status_purchase = int.Parse(ddlsearch_status.SelectedValue);



                _search_purchase.search_purchase_list[0] = _seaching;

                //  check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_search_purchase));


                _search_purchase = callServicePurchase(_urlGetsearch_purchase, _search_purchase);
                //setGridViewDataBind(gvpurchase_equipmentlist, _search_purchase.u0_purchase_equipment_list);


                var _returnCodealert = _search_purchase.return_code;
                var testreturnmsg = _search_purchase.return_msg;

                if (_returnCodealert == "0")
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ !!! ไม่พบข้อมูลรายการขอซื้อที่ท่านค้นหา');", true);

                }

                else

                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('พบผลลัพธ์รายการขอซื้อ');", true);

                }


                ViewState["purchase_list"] = _search_purchase.search_purchase_list;
                gvpurchase_equipmentlist.DataSource = ViewState["purchase_list"];
                gvpurchase_equipmentlist.DataBind();

                gvpurchase_equipmentlist.Visible = true;





                break;

            #region case confirm_approve
            case "confirm_approve": //ปุ่ม approve all purchase


                u0_purchase_equipment_details[] _u0tempListpurchase_approveall = (u0_purchase_equipment_details[])ViewState["purchase_list"];


                if (check_approve_all.Checked)
                {


                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)

                    {
                        //:::::: select items in gridview sub before sent e-mail :::::::://
                        items_purchase_details[] _itemslist_approveall = (items_purchase_details[])ViewState["itemslist"];

                        var linq_itemslist = (from dataitems in _itemslist_approveall
                                              where dataitems.u0_purchase_idx == _u0tempListpurchase_approveall[i].u0_purchase_idx
                                          select new
                                          {
                                              dataitems.type_equipment

                                          }).ToList();
                        string _listitem_sentmail_all = "";
        
                        for (int z = 0; z < linq_itemslist.Count(); z++)
                        {
                            _listitem_sentmail_all += linq_itemslist[z].type_equipment.ToString() + ",";
                        }


                        data_purchase _dtpurchase_approveall = new data_purchase();
                        _dtpurchase_approveall.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                        u0_purchase_equipment_details u0approve_all = new u0_purchase_equipment_details();

                    // อนุมัติทั้งหมด

                        u0approve_all.m0_actor_idx = _u0tempListpurchase_approveall[i].m0_actor_idx;
                        u0approve_all.m0_node_idx = _u0tempListpurchase_approveall[i].m0_node_idx;
                        u0approve_all.emp_idx_purchase = _u0tempListpurchase_approveall[i].emp_idx_purchase;
                        u0approve_all.status_purchase = 5;
                        u0approve_all.details_purchase = "อนุมัติรายการขอซื้อ";
                        u0approve_all.node_status_purchase = "อนุมัติรายการขอซื้อ";

                        u0approve_all.actor_name_purchase = _u0tempListpurchase_approveall[i].actor_name_purchase;
                        u0approve_all.name_purchase = _u0tempListpurchase_approveall[i].name_purchase;
                        u0approve_all.document_code = _u0tempListpurchase_approveall[i].document_code;
                        u0approve_all.drept_name_purchase = _u0tempListpurchase_approveall[i].drept_name_purchase;
                        u0approve_all.link = _link.ToString();
                        u0approve_all.name_equipment = _listitem_sentmail_all;

                        u0approve_all.u0_purchase_idx = _u0tempListpurchase_approveall[i].u0_purchase_idx;


                        _dtpurchase_approveall.u0_purchase_equipment_list[0] = u0approve_all;

                        string emailmove = "mongkonl.d@taokaenoi.co.th";
                        string replyempmove = "msirirak4441@gmail.com";
                        _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                        _mail_body = servicemail.approve_purchase(_dtpurchase_approveall.u0_purchase_equipment_list[0]);
                        _dtpurchase_approveall.u0_purchase_equipment_list[0] = u0approve_all;
                        //check_XML.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpurchase_approveall));

                        _dtpurchase_approveall = callServicePurchase(_urlsetapprove_purchase, _dtpurchase_approveall);

                        if (_dtpurchase_approveall.return_code == "0")
                        {

                         servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);


                        }

                    }

                    }


                else
                {

                    u0_purchase_equipment_details[] _u0tempListpurchase1 = (u0_purchase_equipment_details[])ViewState["purchase_list"];

                    for (int n = 0; n <= _u0tempListpurchase1.Count() - 1; n++)
                    {



                        if (_u0tempListpurchase1[n].selected)
                        {

                            //:::::: select items in gridview sub before sent e-mail :::::::://
                            items_purchase_details[] _items_approve_somelist = (items_purchase_details[])ViewState["itemslist"];

                            var linq_items_someapprove = (from data_items in _items_approve_somelist
                                              where data_items.u0_purchase_idx == _u0tempListpurchase1[n].u0_purchase_idx
                                              select new
                                              {
                                                  data_items.type_equipment
                                              }).ToList();
                            string _listsomeapprovea = "";
                            for (int z = 0; z < linq_items_someapprove.Count(); z++)
                            {
                                _listsomeapprovea += linq_items_someapprove[z].type_equipment.ToString() + ",";
                            }
                            
                          

                            ////// อนุมัติบางรายการ
                            data_purchase _dtpurchase_approve = new data_purchase();
                            _dtpurchase_approve.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                            u0_purchase_equipment_details u0some_approve = new u0_purchase_equipment_details();

                            u0some_approve.m0_actor_idx = _u0tempListpurchase1[n].m0_actor_idx;
                            u0some_approve.m0_node_idx = _u0tempListpurchase1[n].m0_node_idx;
                            u0some_approve.emp_idx_purchase = _u0tempListpurchase1[n].emp_idx_purchase;
                            u0some_approve.status_purchase = 5;
                            u0some_approve.details_purchase = "อนุมัติรายการขอซื้อ";
                            u0some_approve.node_status_purchase = "อนุมัติรายการขอซื้อ";

                            u0some_approve.actor_name_purchase = _u0tempListpurchase1[n].actor_name_purchase;
                            u0some_approve.name_purchase = _u0tempListpurchase1[n].name_purchase;
                            u0some_approve.document_code = _u0tempListpurchase1[n].document_code;
                            u0some_approve.drept_name_purchase = _u0tempListpurchase1[n].drept_name_purchase;
                            u0some_approve.link = _link.ToString();
                            u0some_approve.name_equipment = _listsomeapprovea;




                            u0some_approve.u0_purchase_idx = _u0tempListpurchase1[n].u0_purchase_idx;
                            _dtpurchase_approve.u0_purchase_equipment_list[0] = u0some_approve;

                            string emailmove = "mongkonl.d@taokaenoi.co.th";
                            string replyempmove = "msirirak4441@gmail.com";
                            _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                            _mail_body = servicemail.approve_purchase(_dtpurchase_approve.u0_purchase_equipment_list[0]);

                            _dtpurchase_approve.u0_purchase_equipment_list[0] = u0some_approve;
                           // Literal1.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpurchase_approve));
                           _dtpurchase_approve = callServicePurchase(_urlsetapprove_purchase, _dtpurchase_approve);

                            if (_dtpurchase_approve.return_code == "0")
                            {
                                servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
                            }
                        }
                      

                    }

                }


                 Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            #endregion

            case "confirm_no_approve":

                u0_purchase_equipment_details[] _u0tempListpurchase_notapproveall = (u0_purchase_equipment_details[])ViewState["purchase_list"];


                if (check_approve_all.Checked)
                {


                    for (int i = 0; i <= _u0tempListpurchase_notapproveall.Count() - 1; i++)

                    {

                        //:::::: select items in gridview sub before sent e-mail :::::::://
                        items_purchase_details[] _itemslist_notapproveall = (items_purchase_details[])ViewState["itemslist"];

                        var linq_itemslist = (from dataitems in _itemslist_notapproveall
                                              where dataitems.u0_purchase_idx == _u0tempListpurchase_notapproveall[i].u0_purchase_idx
                                              select new
                                              {
                                                  dataitems.type_equipment

                                              }).ToList();
                        string _listitem_sentmail_notall = "";

                        for (int z = 0; z < linq_itemslist.Count(); z++)
                        {
                            _listitem_sentmail_notall += linq_itemslist[z].type_equipment.ToString() + ",";
                        }


                        data_purchase _dtpurchase_notapproveall = new data_purchase();
                        _dtpurchase_notapproveall.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                        u0_purchase_equipment_details notapprove_all = new u0_purchase_equipment_details();

                        // อนุมัติทั้งหมด

                        notapprove_all.m0_actor_idx = _u0tempListpurchase_notapproveall[i].m0_actor_idx;
                        notapprove_all.m0_node_idx = _u0tempListpurchase_notapproveall[i].m0_node_idx;
                        notapprove_all.emp_idx_purchase = _u0tempListpurchase_notapproveall[i].emp_idx_purchase;

                        if (_u0tempListpurchase_notapproveall[i].m0_node_idx.ToString() == "15")
                        {
                            notapprove_all.status_purchase = 6;
                            notapprove_all.details_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสาร)";
                            notapprove_all.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสาร)";

                        }
                        else
                        {
                            notapprove_all.status_purchase = 7;
                            notapprove_all.details_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";
                            notapprove_all.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";

                        }


                        notapprove_all.actor_name_purchase = _u0tempListpurchase_notapproveall[i].actor_name_purchase;
                        notapprove_all.name_purchase = _u0tempListpurchase_notapproveall[i].name_purchase;
                        notapprove_all.document_code = _u0tempListpurchase_notapproveall[i].document_code;
                        notapprove_all.drept_name_purchase = _u0tempListpurchase_notapproveall[i].drept_name_purchase;
                        notapprove_all.link = _link.ToString();
                        notapprove_all.name_equipment = _listitem_sentmail_notall;

                        notapprove_all.u0_purchase_idx = _u0tempListpurchase_notapproveall[i].u0_purchase_idx;


                        _dtpurchase_notapproveall.u0_purchase_equipment_list[0] = notapprove_all;

                        string emailmove = "mongkonl.d@taokaenoi.co.th";
                        string replyempmove = "msirirak4441@gmail.com";
                        _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                        _mail_body = servicemail.approve_purchase(_dtpurchase_notapproveall.u0_purchase_equipment_list[0]);

                        _dtpurchase_notapproveall.u0_purchase_equipment_list[0] = notapprove_all;
                        //check_XML.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpurchase_notapproveall));

                        _dtpurchase_notapproveall = callServicePurchase(_urlsetapprove_purchase, _dtpurchase_notapproveall);

                        if (_dtpurchase_notapproveall.return_code == "0")
                        {
                            servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
                        }


                    }

                }

                else
                {


                    u0_purchase_equipment_details[] _u0tempListpurchase_somenotapprove = (u0_purchase_equipment_details[])ViewState["purchase_list"];

                    for (int n = 0; n <= _u0tempListpurchase_somenotapprove.Count() - 1; n++)
                    {



                        if (_u0tempListpurchase_somenotapprove[n].selected)
                        {
                            //:::::: select items in gridview sub before sent e-mail :::::::://
                            items_purchase_details[] _items_approve_notsomelist = (items_purchase_details[])ViewState["itemslist"];

                            var linq_items_notsomeapprove = (from data_items in _items_approve_notsomelist
                                                          where data_items.u0_purchase_idx == _u0tempListpurchase_somenotapprove[n].u0_purchase_idx
                                                          select new
                                                          {
                                                              data_items.type_equipment
                                                          }).ToList();
                            string _listitem_sentmail_notsome = "";
                            for (int z = 0; z < linq_items_notsomeapprove.Count(); z++)
                            {
                                _listitem_sentmail_notsome += linq_items_notsomeapprove[z].type_equipment.ToString() + ",";
                            }



                            ////// อนุมัติบางรายการ
                            data_purchase _dtpurchase_somenotapprove = new data_purchase();
                            _dtpurchase_somenotapprove.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                            u0_purchase_equipment_details u0some_approve = new u0_purchase_equipment_details();

                            u0some_approve.m0_actor_idx = _u0tempListpurchase_somenotapprove[n].m0_actor_idx;
                            u0some_approve.m0_node_idx = _u0tempListpurchase_somenotapprove[n].m0_node_idx;
                            u0some_approve.emp_idx_purchase = _u0tempListpurchase_somenotapprove[n].emp_idx_purchase;

                            if (_u0tempListpurchase_somenotapprove[n].m0_node_idx.ToString() == "15")
                            {
                                u0some_approve.status_purchase = 6;
                                u0some_approve.details_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสาร)";
                                u0some_approve.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (แก้ไขเอกสาร)";

                            }
                            else
                            {
                                u0some_approve.status_purchase = 7;
                                u0some_approve.details_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";
                                u0some_approve.node_status_purchase = "ไม่อนุมัติรายการขอซื้อ (จบการดำเนินการ)";

                            }

                            u0some_approve.actor_name_purchase = _u0tempListpurchase_somenotapprove[n].actor_name_purchase;
                            u0some_approve.name_purchase = _u0tempListpurchase_somenotapprove[n].name_purchase;
                            u0some_approve.document_code = _u0tempListpurchase_somenotapprove[n].document_code;
                            u0some_approve.drept_name_purchase = _u0tempListpurchase_somenotapprove[n].drept_name_purchase;
                            u0some_approve.link = _link.ToString();
                            u0some_approve.name_equipment = _listitem_sentmail_notsome;




                            u0some_approve.u0_purchase_idx = _u0tempListpurchase_somenotapprove[n].u0_purchase_idx;
                            _dtpurchase_somenotapprove.u0_purchase_equipment_list[0] = u0some_approve;

                            string emailmove = "mongkonl.d@taokaenoi.co.th";
                            string replyempmove = "msirirak4441@gmail.com";
                            _mail_subject = "[MIS/IT Purchase] - " + "การอนุมัติรายการขอซื้อ";
                            _mail_body = servicemail.approve_purchase(_dtpurchase_somenotapprove.u0_purchase_equipment_list[0]);
                            _dtpurchase_somenotapprove.u0_purchase_equipment_list[0] = u0some_approve;

                          //  Literal1.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpurchase_somenotapprove));
                            _dtpurchase_somenotapprove = callServicePurchase(_urlsetapprove_purchase, _dtpurchase_somenotapprove);

                            if (_dtpurchase_somenotapprove.return_code == "0")
                            {
                              servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);
                            }


                        }

                    }
                   
                }
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "viewpurchasemountly":

                panel_mountly.Visible = true;
                Panel_list_purchase.Visible = false;
                gvpurchase_equipmentlist.Visible = false;
                action_ddlselect_mountly();
                action_select_yearly();

                break;

            case "submit_mountly":

                data_purchase _dtpurchase = new data_purchase();
                _dtpurchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                u0_purchase_equipment_details select_mountly = new u0_purchase_equipment_details();

                select_mountly.action_type = 0; //แสดงข้อมูลที่ยังไม่ได้อนุมัติเป็นรายเดือน

                select_mountly.yearly = ddl_yearly.SelectedItem.Text;
                select_mountly.mountly = ddl_mountly.SelectedValue;
                select_mountly.m0_node_idx = int.Parse(ViewState["_node_idx"].ToString());

               

                _dtpurchase.u0_purchase_equipment_list[0] = select_mountly;
              //  check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpurchase));
                _dtpurchase = callServicePurchase(_urlGetreport_purchase, _dtpurchase);


                ViewState["purchase_list"] = _dtpurchase.u0_purchase_equipment_list;
                setGridViewDataBind(gvpurchase_equipmentlist, ViewState["purchase_list"]);

                gvpurchase_equipmentlist.Visible = true;

                break;


            case "showbox_report":

                mvMultiview.SetActiveView(viewReport);
                listMenu0.BackColor = System.Drawing.Color.Transparent;
                // listMenu2.BackColor = System.Drawing.Color.Transparent;
                listMenu3.BackColor = System.Drawing.Color.LightGray;



                panelSearchViewreport.Visible = true;


                action_select_orgreport(0, "0");
                action_select_rdeptreport(1, "0");
                action_purchase_report();
                action_equipment_report();
                action_location_report();
                action_costcenter_report();
                action_status_report();

                break;

            case "add_purchase":

                //Select_tabel_purchaselist();
                RequiredFieldValidator _required_spec = (RequiredFieldValidator)fvCreatePurchasequipment.FindControl("RequiredFieldddlspecidx");
                var ddlequipmenttype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlequipmenttype");
                var ddlpurchasetype = (DropDownList)fvCreatePurchasequipment.FindControl("ddlpurchasetype");
                var ddlposition = (DropDownList)fvCreatePurchasequipment.FindControl("ddlposition");
                var ddlspecidx = (DropDownList)fvCreatePurchasequipment.FindControl("ddlspecidx");
                var ddllocation = (DropDownList)fvCreatePurchasequipment.FindControl("ddllocation");
                var txtquantity = (TextBox)fvCreatePurchasequipment.FindControl("txtquantity");
                var txtdetails_spec = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_spec");
                var txtexpenditure = (TextBox)fvCreatePurchasequipment.FindControl("txtexpenditure");
                var txtdetails_purchase = (TextBox)fvCreatePurchasequipment.FindControl("txtdetails_purchase");
                var gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");
                var ddlcostCenter = (DropDownList)fvCreatePurchasequipment.FindControl("ddlcostCenter");

                //   _required_spec.Enabled = true;



                var btnsavepurchase = (LinkButton)fvCreatePurchasequipment.FindControl("btnsavepurchase");

                var _dataset_purchase = (DataSet)ViewState["Create_purchase_speclist"];
                var _datarowpurchase = _dataset_purchase.Tables[0].NewRow();




                //ค่าที่เก็บที่จะเอามาเช็ค
                ViewState["equipment_check"] = (ddlequipmenttype.SelectedValue);
                ViewState["spec_type_check"] = (ddlspecidx.SelectedValue);
                ViewState["purchase_type_check"] = (ddlpurchasetype.SelectedValue);
                ViewState["_value_costcenter"] = (ddlcostCenter.SelectedValue);


                int _numrow = _dataset_purchase.Tables[0].Rows.Count; //จำนวนแถว

                //check ค่าซ้ำของรายการขอซื้อ และ check ความถูกต้องของประเภทขอซื้อ
                //if (_numrow > 0)
                if (_numrow > 0)
                {
                    foreach (DataRow check in _dataset_purchase.Tables[0].Rows)
                    {

                        //check_XML.Text = check["equipment_idx"].ToString();
                        //check_mail.Text = check["spec_type_idx"].ToString();

                        ViewState["check_equipment"] = check["equipment_idx"];
                        ViewState["check_spec"] = check["spec_type_idx"];
                        ViewState["check_purchase_type"] = check["purchase_type_idx"];

                        //กรณิประเภทการขอซื้อไม่ตรงกัน
                        if (ViewState["purchase_type_check"].ToString() != ViewState["check_purchase_type"].ToString())
                        {

                            ViewState["CheckDataset"] = "2";
                            break;

                        }

                        else
                        {
                            //กรณิประเภทอุปกรณ์และสเปคตรงกัน
                            if (ViewState["equipment_check"].ToString() == ViewState["check_equipment"].ToString()
                                && ViewState["spec_type_check"].ToString() == ViewState["check_spec"].ToString())
                            {
                                ViewState["CheckDataset"] = "0";
                                break;

                            }
                            else
                            {
                                ViewState["CheckDataset"] = "1";

                            }

                        }

                    }

                    //ถ้าข้อมูลไม่ซ้ำ จะเข้าการทำงานนี้
                    if (ViewState["CheckDataset"].ToString() == "1")
                    {


                        //ค่าขอมูลที่จะเก็บ
                        _datarowpurchase["spec_type_idx"] = (ddlspecidx.SelectedValue);
                        _datarowpurchase["equipment_idx"] = (ddlequipmenttype.SelectedValue);
                        _datarowpurchase["purchase_type_idx"] = (ddlpurchasetype.SelectedValue);
                        _datarowpurchase["m0_location_idx"] = (ddllocation.SelectedValue);
                        _datarowpurchase["rpos_idx_purchase"] = (ddlposition.SelectedValue);
                        _datarowpurchase["costcenter_idx"] = (ddlcostCenter.SelectedValue);

                       

                        if (ddlequipmenttype.SelectedValue == "1" || ddlequipmenttype.SelectedValue == "2" || ddlequipmenttype.SelectedValue == "4")
                        {

                            _datarowpurchase["spec_type"] = (ddlspecidx.SelectedItem.Text);

                        }
                        else
                        {
                            _datarowpurchase["spec_type"] = "-";

                        }


                        //ข้อความที่แสดง
                        _datarowpurchase["equipment_name"] = (ddlequipmenttype.SelectedItem.Text);
                        //  _datarowpurchase["spec_type"] = (ddlspecidx.SelectedItem.Text);
                        _datarowpurchase["details_spec"] = (txtdetails_spec.Text);
                        _datarowpurchase["quantity"] = ((txtquantity.Text));
                        _datarowpurchase["purchase_type"] = ((ddlpurchasetype.SelectedItem.Text));
                        _datarowpurchase["expenditure"] = ((txtexpenditure.Text));
                        _datarowpurchase["details_purchase"] = ((txtdetails_purchase.Text));

                        //GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");

                        _dataset_purchase.Tables[0].Rows.Add(_datarowpurchase);
                        ViewState["Create_purchase_speclist"] = _dataset_purchase;

                        gvpurchaselist.DataSource = _dataset_purchase.Tables[0];
                        gvpurchaselist.DataBind();


                        btnsavepurchase.Visible = true;

                        fvCreatePurchasequipment.ChangeMode(FormViewMode.Insert);

                        fvCreatePurchasequipment.DataBind();


                    }

                    //ถ้าข้อมูลซ้ำ จะเข้าการทำงานนี้
                    else if (ViewState["CheckDataset"].ToString() == "0")
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ท่านมีการเพิ่มรายการสเปคนี้แล้วค่ะ');", true);
                        ViewState["_check_success_before_reset"] = 0;
                    }
                    //ถ้าข้อมูลประเภทขอซื้อไม่ตรงกัน จะเข้าการทำงานนี้
                    else if (ViewState["CheckDataset"].ToString() == "2")
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ ประเภทการขอซื้อของท่านไม่ถูกต้อง !!!');", true);
                        ViewState["_check_success_before_reset"] = 0;
                    }


                }

                else

                {

                    //ค่าขอมูลที่จะเก็บ
                    _datarowpurchase["equipment_idx"] = (ddlequipmenttype.SelectedValue);

                    _datarowpurchase["purchase_type_idx"] = (ddlpurchasetype.SelectedValue);
                    _datarowpurchase["m0_location_idx"] = (ddllocation.SelectedValue);
                    _datarowpurchase["rpos_idx_purchase"] = (ddlposition.SelectedValue);
                    _datarowpurchase["costcenter_idx"] = (ddlcostCenter.SelectedValue);

                    ViewState["_value_purchasetype"] = (ddlpurchasetype.SelectedValue);
                    ViewState["_value_location"] = (ddllocation.SelectedValue);



                    if (ddlequipmenttype.SelectedValue == "1" || ddlequipmenttype.SelectedValue == "2" || ddlequipmenttype.SelectedValue == "4")
                    {

                        _datarowpurchase["spec_type"] = (ddlspecidx.SelectedItem.Text);
                        _datarowpurchase["spec_type_idx"] = (ddlspecidx.SelectedValue);
                    }
                    else
                    {
                        _datarowpurchase["spec_type"] = "-";
                        _datarowpurchase["spec_type_idx"] = 0;//(ddlspecidx.SelectedValue);

                    }



                    //ข้อความที่แสดง
                    // _datarowpurchase["spec_type"] = (ddlspecidx.SelectedItem.Text);
                    _datarowpurchase["equipment_name"] = (ddlequipmenttype.SelectedItem.Text);
                    _datarowpurchase["details_spec"] = (txtdetails_spec.Text);
                    _datarowpurchase["quantity"] = ((txtquantity.Text));
                    _datarowpurchase["purchase_type"] = ((ddlpurchasetype.SelectedItem.Text));
                    _datarowpurchase["expenditure"] = ((txtexpenditure.Text));
                    _datarowpurchase["details_purchase"] = ((txtdetails_purchase.Text));

                   // GridView gvpurchaselist = (GridView)fvCreatePurchasequipment.FindControl("gvpurchaselist");

                    _dataset_purchase.Tables[0].Rows.Add(_datarowpurchase);
                    ViewState["Create_purchase_speclist"] = _dataset_purchase;

                    gvpurchaselist.DataSource = _dataset_purchase.Tables[0];
                    gvpurchaselist.DataBind();


                    btnsavepurchase.Visible = true;


                    fvCreatePurchasequipment.ChangeMode(FormViewMode.Insert);

                    fvCreatePurchasequipment.DataBind();


                }




               //  ddlpurchasetype.SelectedValue = ViewState["_value_purchasetype"].ToString();
                //ViewState["_value_location"] = (ddllocation.SelectedValue);
                //if (ViewState["_check_success_before_reset"].ToString() == "0")
                //{


                //}

                //else
                //{


                //  }



                //ViewState["Create_purchase_speclist"] = _dataset_purchase;





                // ddlspecidx.ClearSelection();
                //_required_spec.Enabled = false;


                // ddlequipmenttype.SelectedValue = "0";
                //txtdetails_spec.Text = String.Empty;
                //txtquantity.Text = String.Empty;
                //txtexpenditure.Text = String.Empty;



                break;

            case "updatepurchase":


                //:::::::::::::::: START UPDATE DATA PURCHASE U0_DOCUMENT :::::::::::::::::://

                data_purchase _data_edit_purchase = new data_purchase();
                _data_edit_purchase.u0_purchase_equipment_list = new u0_purchase_equipment_details[1];
                u0_purchase_equipment_details _edit_u0_document_purchase = new u0_purchase_equipment_details();

                //  test.Text = ViewState["_UPDATEPURCHASETYPE"].ToString();

                //:::::::::::::::: END UPDATE DATA PURCHASE U0_DOCUMENT :::::::::::::::::://

                //  check_mail.Text =  ViewState["_PURCHASETYPE"].ToString();

                _edit_u0_document_purchase.u0_purchase_idx = int.Parse(ViewState["u0_purchase_idx"].ToString());
                _edit_u0_document_purchase.emp_idx_purchase = int.Parse(ViewState["emp_idx"].ToString());

                //:::::::::::::::: CODINATION BUY NEW :::::::::::::::::://
                if ((ViewState["m0_purchase_idx"].ToString()) == "1")
                {


                    _edit_u0_document_purchase.status_purchase = 1;
                    _edit_u0_document_purchase.m0_node_idx = 10;
                    _edit_u0_document_purchase.m0_actor_idx = 1;

                    // check_mail.Text = "PUR";

                    _data_edit_purchase.u0_purchase_equipment_list[0] = _edit_u0_document_purchase;

                    //_data_edit_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_edit_purchase);
                }

                //:::::::::::::::: CODINATION BUY REPLACE :::::::::::::::::://

                else if ((ViewState["m0_purchase_idx"].ToString()) == "2")

                {


                    _edit_u0_document_purchase.status_purchase = 1;
                    _edit_u0_document_purchase.m0_node_idx = 14;
                    _edit_u0_document_purchase.m0_actor_idx = 1;

                    // check_mail.Text = "PURCHASE";





                    _data_edit_purchase.u0_purchase_equipment_list[0] = _edit_u0_document_purchase;


                }

                #region sent e mail

                var lb_emps_code_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_emps_code_purchase");
                var lb_emps_fullname_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_emps_fullname_purchase");
                var lb_name_pos_purchase = (Label)fvdetails_employee_purchase.FindControl("lb_name_pos_purchase");
                var lbdrept_name_purchase = (Label)fvdetails_employee_purchase.FindControl("lbdrept_name_purchase");

                // gvEditpurchaselist

                // var gvEditpurchaselist = (GridView)sender;
                foreach (GridViewRow _row_equipment in gvEditpurchaselist.Rows)
                {

                    Label items_type_equipment = (Label)_row_equipment.FindControl("items_type_equipment");
                    Label lbargumentation_edit = (Label)_row_equipment.FindControl("lbargumentation_edit");
                    Label lbname_purchase_edit = (Label)_row_equipment.FindControl("lbname_purchase_edit1");
                    Label lbdoccode_edit = (Label)_row_equipment.FindControl("lbdoccode_edit");
                    DropDownList ddlpurchase_typeedit = (DropDownList)_row_equipment.FindControl("ddlpurchase_typeedit");
                    

                    ViewState["name_equipment_edit"] += items_type_equipment.Text + ",";
                    ViewState["editargumentation"] = lbargumentation_edit.Text;
                    ViewState["_purchasetype_edit"] = lbname_purchase_edit.Text;
                    ViewState["doccode_edit"] = lbdoccode_edit.Text;

                }

                //var _row_equipment = (GridViewRow)gvEditpurchaselist.NamingContainer;
                ////  Enabled = true :: textbox เมื่อทำการลบข้อมูล
                //Label ddlpurchase_typeedit = (Label)_row_equipment.FindControl("ddlpurchase_typeedit");




                ////ข้อมูลในการส่ง e-mail
                _edit_u0_document_purchase.name_purchase = ViewState["_purchasetype_edit"].ToString();
                _edit_u0_document_purchase.name_equipment = ViewState["name_equipment_edit"].ToString();
                _edit_u0_document_purchase.document_code = ViewState["doccode_edit"].ToString();
                _edit_u0_document_purchase.fullname_purchase = lb_emps_fullname_purchase.Text;
                _edit_u0_document_purchase.drept_name_purchase = lbdrept_name_purchase.Text;
                _edit_u0_document_purchase.details_purchase = ViewState["editargumentation"].ToString();
                _edit_u0_document_purchase.empcode_purchase = lb_emps_code_purchase.Text;
                _edit_u0_document_purchase.link = _link.ToString(); ;

               // _data_purchase.u0_purchase_equipment_list[0] = insert_purchaselist;
               // check_XML.Text = (ddlpurchasetype.SelectedItem.Text);
//
                string _emailupdate = "mongkonl.d@taokaenoi.co.th";
                string replyeupdate = "msirirak4441@gmail.com";
                ////string replyempmove = "nipon@taokeanoi.co.th";


                _mail_subject = "[MIS/IT Purchase] - " + "แก้ไขเอกสารการขอซื้ออุปกรณ์";
                _mail_body = servicemail.create_purchase(_data_edit_purchase.u0_purchase_equipment_list[0]);


                #endregion


                //  Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_purchase));//ViewState["_DOCUMENTCODE"].ToString();//HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_edit_purchase));


                // _data_edit_purchase = callServicePurchase(_urlSetcreate_equipmentpurchase, _data_edit_purchase);
                _data_edit_purchase = callServicePurchase(_urlsetapprove_purchase, _data_edit_purchase);

                if (_data_edit_purchase.return_code == "0")
                {

                   // var _returnemail = _data_edit_purchase.return_email;
                   // check_XML.Text = _returnemail.ToString();
                 //   servicemail.SendHtmlFormattedEmailFull(_emailupdate, "", replyeupdate, _mail_subject, _mail_body);


                }




                if (uploadfile_memo_edit.HasFile)
                {

                    string getPathfile = ConfigurationManager.AppSettings["pathfile_purchase"];
                    string document_code = ViewState["_DOCUMENTCODE"].ToString();
                    string fileName_upload = "[ เอกสาร memo ] " + document_code;
                    string filePath_upload = Server.MapPath(getPathfile + document_code);

                    if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                    string extension = Path.GetExtension(uploadfile_memo_edit.FileName);

                    uploadfile_memo_edit.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + fileName_upload + extension);


                }


                if (uploadfile_organization_edit.HasFile)
                {

                    string getPathfileOrg = ConfigurationManager.AppSettings["pathfile_purchase"];
                    string document_codeorg = ViewState["_DOCUMENTCODE"].ToString();
                    string fileName_uploadorg = "[ เอกสาร organization ] " + document_codeorg;
                    string filePath_uploadorg = Server.MapPath(getPathfileOrg + document_codeorg);

                    if (!Directory.Exists(filePath_uploadorg))
                    {
                        Directory.CreateDirectory(filePath_uploadorg);
                    }
                    string extension = Path.GetExtension(uploadfile_organization_edit.FileName);

                    uploadfile_organization_edit.SaveAs(Server.MapPath(getPathfileOrg + document_codeorg) + "\\" + fileName_uploadorg + extension);

                }


                if (uploadfile_cutoff_edit.HasFile)
                {

                    string getPathfilecutoff = ConfigurationManager.AppSettings["pathfile_purchase"];
                    string document_codecutoff = ViewState["_DOCUMENTCODE"].ToString();
                    string fileName_uploadcutoff = "[ เอกสารการตัดเสีย ] " + document_codecutoff;
                    string filePath_uploadcutoff = Server.MapPath(getPathfilecutoff + document_codecutoff);

                    if (!Directory.Exists(filePath_uploadcutoff))
                    {
                        Directory.CreateDirectory(filePath_uploadcutoff);
                    }
                    string extension = Path.GetExtension(uploadfile_cutoff_edit.FileName);

                    uploadfile_cutoff_edit.SaveAs(Server.MapPath(getPathfilecutoff + document_codecutoff) + "\\" + fileName_uploadcutoff + extension);

                }


                if (uploadfile_refercutoff_edit.HasFile)
                {

                    string getPathfilerefercutoff = ConfigurationManager.AppSettings["pathfile_purchase"];
                    string document_coderefercutoff = ViewState["_DOCUMENTCODE"].ToString();
                    string fileName_uploadrefercutoff = "[ เอกสารยืนยันการตัดเสีย ] " + document_coderefercutoff;
                    string filePath_uploadrefercutoff = Server.MapPath(getPathfilerefercutoff + document_coderefercutoff);

                    if (!Directory.Exists(filePath_uploadrefercutoff))
                    {
                        Directory.CreateDirectory(filePath_uploadrefercutoff);
                    }
                    string extension = Path.GetExtension(uploadfile_refercutoff_edit.FileName);

                    uploadfile_refercutoff_edit.SaveAs(Server.MapPath(getPathfilerefercutoff + document_coderefercutoff) + "\\" + fileName_uploadrefercutoff + extension);

                }



                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "viewall":

                if (int.Parse(ViewState["rsec_idx"].ToString()) == it_support
                    || int.Parse(ViewState["emp_idx"].ToString()) == head_itsupport) // IT Support)
                {




                    action_select_purchaselist();

                    ViewState["ITViewAll_PageindexChange"] = 0;

                }



                break;

            case "reset_search":


                ddlcondition.SelectedValue = "0";
                txtstartdate.Text = String.Empty;
                txtenddate.Text = String.Empty;
                txtdocumentcode.Text = String.Empty;
                txtempcode.Text = String.Empty;
                txtfirstname_lastname.Text = String.Empty;
                ddlsearch_organization.SelectedValue = "0";
                ddlsearch_department.SelectedValue = "0";
                ddllocation_search.SelectedValue = "0";
                ddl_costcenter.SelectedValue = "0";
                ddlsearch_typepurchase.SelectedValue = "0";
                ddlsearch_typequipment.SelectedValue = "0";
                ddlsearch_status.SelectedValue = "0";


                break;


            case "searchingReport":


                data_purchase _report_purchase = new data_purchase();

                _report_purchase.report_purchase_list = new report_purchase_details[1];


                report_purchase_details _reporting = new report_purchase_details();

                _reporting.condition_date_type = int.Parse(ddlconditionReport.SelectedValue);
                _reporting.start_date = txtStartdateReport.Text;
                _reporting.end_date = txtEnddateReport.Text;

                _reporting.document_code = txtDocumentCodeReport.Text;
                _reporting.fullname_purchase = txtFname_LnameReport.Text;
                _reporting.empcode_purchase = txtEmpCodeReport.Text;
                _reporting.org_idx_purchase = int.Parse(ddlOrganizationReport.SelectedValue);
                _reporting.dept_idx_purchase = int.Parse(ddlDepartmentReport.SelectedValue);
                _reporting.m0_location_idx = int.Parse(ddlLocationReport.SelectedValue);
                _reporting.costcenter_idx = int.Parse(ddlCostcenterReport.SelectedValue);
                _reporting.m0_purchase_idx = int.Parse(ddlPurchaseTypeReport.SelectedValue);
                _reporting.m0_equipment_idx = int.Parse(ddlEquipmentTypeReport.SelectedValue);
                _reporting.status_purchase = int.Parse(ddlStatusReport.SelectedValue);

                _report_purchase.report_purchase_list[0] = _reporting;

                // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_report_purchase));


                _report_purchase = callServicePurchase(_urlGetreportpurchaselist, _report_purchase);

                var _returnCodealertReport = _report_purchase.return_code;


                if (_returnCodealertReport == "0")
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ !!! ไม่พบข้อมูลรายงานการขอซื้อที่ท่านต้องการ');", true);

                }

                else

                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('พบผลลัพธ์รายงานการขอซื้อค่ะ');", true);

                }

                //var linqpur = from p in _report_purchase.report_purchase_list
                //              where p.u0_purchase_idx ==
                //              select p;


                ViewState["purchase_list"] = _report_purchase.report_purchase_list;

             

                gvReport.DataSource = ViewState["purchase_list"];
                gvReport.DataBind();


                gvReport.Visible = true;

                if (_report_purchase.report_purchase_list != null)
                {

                    btnExportExcel.Visible = true;
                    printreport.Visible = true;

                }

                //gvpurchase_equipmentlist.Visible = true;


                break;

            case "resetSearchingReport":

                ddlconditionReport.SelectedValue = "0";
                txtStartdateReport.Text = String.Empty;
                txtEnddateReport.Text = String.Empty;
                txtDocumentCodeReport.Text = String.Empty;
                txtEmpCodeReport.Text = String.Empty;
                txtFname_LnameReport.Text = String.Empty;

                ddlCostcenterReport.SelectedValue = "0";
                ddlPurchaseTypeReport.SelectedValue = "0";
                ddlEquipmentTypeReport.SelectedValue = "0";
                ddlStatusReport.SelectedValue = "0";
                ddlLocationReport.SelectedValue = "0";
                ddlDepartmentReport.SelectedValue = "0";
                ddlOrganizationReport.SelectedValue = "0";


                break;


            case "exportExcell":




                data_purchase _export_purchase = new data_purchase();

                _export_purchase.report_purchase_list = new report_purchase_details[1];


                report_purchase_details _export = new report_purchase_details();

                _export.condition_date_type = int.Parse(ddlconditionReport.SelectedValue);
                _export.start_date = txtStartdateReport.Text;
                _export.end_date = txtEnddateReport.Text;

                _export.document_code = txtDocumentCodeReport.Text;
                _export.fullname_purchase = txtFname_LnameReport.Text;
                _export.empcode_purchase = txtEmpCodeReport.Text;
                _export.org_idx_purchase = int.Parse(ddlOrganizationReport.SelectedValue);
                _export.dept_idx_purchase = int.Parse(ddlDepartmentReport.SelectedValue);
                _export.m0_location_idx = int.Parse(ddlLocationReport.SelectedValue);
                _export.costcenter_idx = int.Parse(ddlCostcenterReport.SelectedValue);
                _export.m0_purchase_idx = int.Parse(ddlPurchaseTypeReport.SelectedValue);
                _export.m0_equipment_idx = int.Parse(ddlEquipmentTypeReport.SelectedValue);
                _export.status_purchase = int.Parse(ddlStatusReport.SelectedValue);


                _export_purchase.report_purchase_list[0] = _export;

                // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_report_purchase));


                _export_purchase = callServicePurchase(_urlGetexportexcelPurchase, _export_purchase);


                //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_export_purchase));

                GvExportExcel.DataSource = _export_purchase.export_purchase_list;
                GvExportExcel.DataBind();

                GvExportExcel.AllowSorting = false;
                GvExportExcel.AllowPaging = false;


                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=ExportExcelPurchase.xls");
                //  Response.Charset = ""; set character
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GvExportExcel.Columns[0].Visible = true;
                GvExportExcel.HeaderRow.BackColor = Color.White;




                foreach (GridViewRow row in GvExportExcel.Rows)
                {

                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {

                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GvExportExcel.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GvExportExcel.RowStyle.BackColor;
                        }

                        cell.CssClass = "textmode";

                    }
                }




                GvExportExcel.RenderControl(hw);



                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                //////Set_Null();


                break;

            case "printreport":



                Session["_REPROTSESION_CONDITION"] = int.Parse(ddlconditionReport.SelectedValue);
                Session["_REPROTSESION_STARTDATE"] = txtStartdateReport.Text;
                Session["_REPROTSESION_ENDDATE"] = txtEnddateReport.Text;

                Session["_REPROTSESION_DOCUMENTCODE"] = txtDocumentCodeReport.Text;
                Session["_REPROTSESION_FULLNAME"] = txtFname_LnameReport.Text;
                Session["_REPROTSESION_EMPCODE"] = txtEmpCodeReport.Text;
                Session["_REPROTSESION_ORGIDX"] = int.Parse(ddlOrganizationReport.SelectedValue);
                Session["_REPROTSESION_RDEPTIDX"] = int.Parse(ddlDepartmentReport.SelectedValue);
                Session["_REPROTSESION_LOCATION"] = int.Parse(ddlLocationReport.SelectedValue);
                Session["_REPROTSESION_COSTCENTER"] = int.Parse(ddlCostcenterReport.SelectedValue);
                Session["_REPROTSESION_PURCHASEIDX"] = int.Parse(ddlPurchaseTypeReport.SelectedValue);
                Session["_REPROTSESION_EQUIPMENTIDX"] = int.Parse(ddlEquipmentTypeReport.SelectedValue);
                Session["_REPROTSESION_STATUSPURCHASE"] = int.Parse(ddlStatusReport.SelectedValue);


                Response.Redirect(ResolveUrl("~/print-purchase"));


                break;

            case "cmddelete":

                ViewState["_u2IDX"] = int.Parse(cmd_arg);


                data_purchase _delete = new data_purchase();
                _delete.items_purchase_list = new items_purchase_details[1];
                items_purchase_details items_delete = new items_purchase_details();

                items_delete.u2_purchase_idx = int.Parse(ViewState["_u2IDX"].ToString());

                _delete.items_purchase_list[0] = items_delete;

                _delete = callServicePurchase(_urlSetstatus_items, _delete);

                action_select_purchase_for_edit();


                break;

            case "btnadd_ionumber":

                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');
                int _idu2 = int.Parse(arg[0]);
                int _qtyitems = int.Parse(arg[1]);
                Literal1.Text = _qtyitems.ToString();
                // Literal1.Text = _idu2.ToString();
                //int _idu2 = int.Parse(cmd_arg);


                GridView gvi = (GridView)viewmanage_purchasequipment.FindControl("gvItemspurchase");
                var _datasetIOList = (DataSet)ViewState["create_ionumber_list"];
                var _datarowIOList = _datasetIOList.Tables[0].NewRow();
              

                foreach (GridViewRow _nrow in gvi.Rows)
                {

                    GridView gvionumber = (GridView)_nrow.FindControl("gvionumber");
                    Label Itemsnumber = (Label)_nrow.FindControl("_lbItemsnumber");
                    RequiredFieldValidator RequitxtNoIO = (RequiredFieldValidator)_nrow.FindControl("RequitxtNoIO");

                    TextBox _input_io_number = ((TextBox)_nrow.FindControl("txtNoIO"));
                    Label _alertsuccess_io = ((Label)_nrow.FindControl("alertsuccess_io"));

                    ViewState["_input_numberIO"] = _input_io_number.Text;


                    data_purchase _data_numberio = new data_purchase();

                    if (int.Parse(Itemsnumber.Text) == _idu2)
                    {

                        _datarowIOList["io"] = ViewState["_input_numberIO"].ToString();
                        _datarowIOList["idx"] = _idu2;

                      

                        _datasetIOList.Tables[0].Rows.Add(_datarowIOList);
                        ViewState["create_ionumber_list"] = _datasetIOList;


                        int number_io = 0;


                        var _addio_number = new numberio_details[_datasetIOList.Tables[0].Rows.Count];

                       // Literal2.Text = _addio_number.ToString();

                        foreach (DataRow drnumberio in _datasetIOList.Tables[0].Rows)
                        {

                            _addio_number[number_io] = new numberio_details();
                            _addio_number[number_io].idx = int.Parse(drnumberio["idx"].ToString());
                            _addio_number[number_io].io = (drnumberio["io"].ToString());

                         

                            number_io++;
                            
                        }
                     

                      //  Literal2.Text = sum_numberio.ToString();

                       _data_numberio.numberio_list = _addio_number;

                        var _linq_io = from data_io in _data_numberio.numberio_list
                                    where data_io.idx == (_idu2)
                                    select data_io;



                        gvionumber.DataSource = _linq_io.ToList();
                        gvionumber.DataBind();

                        RequitxtNoIO.Enabled = false;

                        _input_io_number.Text = String.Empty;


                        int numrowCount = 0;
                        foreach (GridViewRow rowCount in gvionumber.Rows)
                        {
                            numrowCount++;
                            ViewState["CountRow"] = numrowCount;

                            check_XML.Text = ViewState["CountRow"].ToString();


                            if ((int.Parse(ViewState["CountRow"].ToString())) == _qtyitems)
                            {
                                _input_io_number.Enabled = false;


                                break;



                            }
                            else if ((int.Parse(ViewState["CountRow"].ToString())) != _qtyitems)
                            {
                                _input_io_number.Enabled = true;

                            }

                        }

                   


                    }

                   // _input_io_number.Focus();
                }

                gvi.Focus();




                break;

            case "btnadd_assetnumber":

                string[] arg_asset = new string[2];
                arg_asset = e.CommandArgument.ToString().Split(';');
                int _idxu2 = int.Parse(arg_asset[0]);
                int _qtyitem = int.Parse(arg_asset[1]);


               // Literal1.Text = _idxu2.ToString();
                // int _idxu2 = int.Parse(cmd_arg);


                GridView gviitems = (GridView)viewmanage_purchasequipment.FindControl("gvItemspurchase");
                var _dataset_assetlist = (DataSet)ViewState["create_assetnumber_list"];
                var _datarow_assetlist = _dataset_assetlist.Tables[0].NewRow();


                foreach (GridViewRow rowasset in gviitems.Rows)
                {


                    GridView gvassetnumber = (GridView)rowasset.FindControl("gvassetnumber");
                    Label idxtemsnumber = (Label)rowasset.FindControl("_lbItemsnumber");

                    TextBox _input_asset_number = ((TextBox)rowasset.FindControl("txtNOAsset"));
                    RequiredFieldValidator RequitxtNOAsset = (RequiredFieldValidator)rowasset.FindControl("RequitxtNOAsset");

                    ViewState["_input_asset_number"] = _input_asset_number.Text;


                    data_purchase _data_number_asset = new data_purchase();

                    if (int.Parse(idxtemsnumber.Text) == _idxu2)
                    {

                        _datarow_assetlist["asset_number"] = ViewState["_input_asset_number"].ToString();
                        _datarow_assetlist["idx_asset"] = _idxu2;



                        _dataset_assetlist.Tables[0].Rows.Add(_datarow_assetlist);
                        ViewState["create_assetnumber_list"] = _dataset_assetlist;


                        int number_asset = 0;


                        var _addasset_number = new numberAsset_details[_dataset_assetlist.Tables[0].Rows.Count];

                        foreach (DataRow drnumberasset in _dataset_assetlist.Tables[0].Rows)
                        {

                            _addasset_number[number_asset] = new numberAsset_details();
                            _addasset_number[number_asset].idx_asset = int.Parse(drnumberasset["idx_asset"].ToString());
                            _addasset_number[number_asset].asset_number = (drnumberasset["asset_number"].ToString());



                            number_asset++;

                        }


                        _data_number_asset.numberAsset_list = _addasset_number;

                        var _linq_asset = from data_asset in _data_number_asset.numberAsset_list
                                          where data_asset.idx_asset == (_idxu2)
                                       select data_asset;

                        gvassetnumber.DataSource = _linq_asset.ToList();
                        gvassetnumber.DataBind();

                     

                        _input_asset_number.Text = String.Empty;
                        RequitxtNOAsset.Enabled = false;


                        //จำนวนเลข asset ครบ
                        if ((number_asset + 1) == _qtyitem)
                        {
                            _input_asset_number.Enabled = true;
                           

                            break;




                        }
                        else
                        {
                            _input_asset_number.Enabled = false;

                        }


                    }
                    gvassetnumber.Focus();


                }


                    break;
        }
    }

    #endregion

    #region call service
    protected data_purchase callServicePurchase(string _cmdUrl, data_purchase _data_purchase)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_purchase);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_purchase = (data_purchase)_funcTool.convertJsonToObject(typeof(data_purchase), _localJson);

        return _data_purchase;
    }


    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }


    #endregion

    #region path file document



    #endregion

    #region reuse

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion reuse

    #region set datetime cutpurchase
    protected void set_datetime_cutpurchase()
    {
        Label lb_warning_datecutpurchase = (Label)viewCreatePurchase.FindControl("lb_warning_datecutpurchase");

        DateTime present_date = DateTime.Now;
        DateTime cut_date_purchase;

        if (present_date.Day <= 20)
        {
            cut_date_purchase = Convert.ToDateTime(present_date.Year.ToString() + "/" + present_date.Month.ToString() + "/" + "20" + " 23:59:59");
        }
        else
        {
            cut_date_purchase = Convert.ToDateTime(present_date.Year.ToString() + "/" + present_date.AddMonths(1).Month.ToString() + "/" + "20" + " 23:59:59");
        }

        lb_warning_datecutpurchase.Text = ("<strong>"+"ขณะนี้เหลือเวลา &nbsp;" + Math.Floor((cut_date_purchase - present_date).TotalDays).ToString() 
            + "&nbsp; วัน" + " &nbsp;ก่อนทำการตัดรอบ ณ วันที่ &nbsp;" + cut_date_purchase.ToString("dd/MM/yyyy HH:mm:ss")) + "&nbsp;น." + "</strong>";


        //การจัดแบบฟอร์มของวันที่
       // return Math.Floor((cut_date_purchase - present_date).TotalDays).ToString();

    }
    #endregion


    public string FormatTo2Dp(decimal myNumber)
    {
        // Use schoolboy rounding, not bankers.
        myNumber = Math.Round(myNumber, 2, MidpointRounding.AwayFromZero);

        return string.Format("{0:0.00}", myNumber);
    }



    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            check_XML.Text = "error : " + _errorText;
        }
        else
        {
            check_XML.Text = String.Empty;
        }
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    

}