﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_EN_MachineRegister : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();

    function_tool _funcTool = new function_tool();

    data_employee _dtEmployee = new data_employee();
    DataMachine _dtmachine = new DataMachine();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    public string checkfile;
    string nameupload;

    #endregion

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlSelect_Machine_Register = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Machine_Register"];
    static string urlSelect_TypeCode = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeCode_Register"];
    static string urlSelect_GroupCode = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCode_Register"];
    static string urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachine_Register"];
    static string urlSelect_TypeClass = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeClass_Register"];
    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string urlSelect_Room = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Room"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string urlInsert_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Machine_Register"];

    
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            litoindex.Attributes.Add("class", "active");
            select_empIdx_present();
            //SelectMaster_Machine();
        }


    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;


    }

    protected void SelectMaster_Machine()
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxMachine_Register = new Machine_Register[1];

        _dtmachine = callServicePostENMachine(urlSelect_Machine_Register, _dtmachine);
        setGridData(GvViewMachine, _dtmachine.BoxMachine_Register);
    }

    protected void Select_TypeCode(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxMachine_Register = new Machine_Register[1];

        _dtmachine = callServicePostENMachine(urlSelect_TypeCode, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxMachine_Register, "NameTypeCode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("Select TypeCode...", "0"));
    }

    protected void Select_GroupCode(DropDownList ddlName, int TCIDX)
    {
        var ddltypecode = (DropDownList)FvInsertMachine.FindControl("ddltypecode");

        _dtmachine = new DataMachine();
        _dtmachine.BoxMachine_Register = new Machine_Register[1];
        Machine_Register select = new Machine_Register();
        select.TCIDX = int.Parse(ddltypecode.SelectedValue);

        _dtmachine.BoxMachine_Register[0] = select;
        //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENMachine(urlSelect_GroupCode, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxMachine_Register, "NameGroupCode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("Select GroupCode...", "0"));
    }

    protected void Select_TypeMachine(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxMachine_Register = new Machine_Register[1];

        _dtmachine = callServicePostENMachine(urlSelect_TypeMachine, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxMachine_Register, "NameTH", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("Select TypeMachine...", "0"));
    }

    protected void Select_TypeClass(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxMachine_Register = new Machine_Register[1];

        _dtmachine = callServicePostENMachine(urlSelect_TypeClass, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxMachine_Register, "TC_Name", "TCLIDX");
        ddlName.Items.Insert(0, new ListItem("Select TypeClass...", "0"));
    }

    protected void Select_Location(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];

        _dtmachine = callServicePostENMachine(urlSelect_Location, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("Select Location...", "0"));
    }

    protected void Select_Building(DropDownList ddlName, int LocIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail building = new RepairMachineDetail();

        building.LocIDX = LocIDX;
        _dtmachine.BoxRepairMachine[0] = building;

        _dtmachine = callServicePostENMachine(urlSelect_Building, _dtmachine);

        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("Select Building...", "0"));
    }

    protected void Select_Room(DropDownList ddlName, int BuildIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail room = new RepairMachineDetail();

        room.BuildingIDX = BuildIDX;
        _dtmachine.BoxRepairMachine[0] = room;

        _dtmachine = callServicePostENMachine(urlSelect_Room, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "Roomname", "RoomIDX");
        ddlName.Items.Insert(0, new ListItem("Select Room...", "0"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmployee(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("Select Organization...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmployee(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("Select Department...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmployee(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("Select Section...", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dtEmployee.employee_list[0] = _empList;


        _dtEmployee = callServicePostEmployee(urlGetAll, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("Select Employee...", "0"));
    }

    #endregion

    #region GridView

    #region rowdatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvViewMachine":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                break;
        }
    }
    #endregion


    #region Paging
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvViewMachine":

                GvViewMachine.PageIndex = e.NewPageIndex;
                SelectMaster_Machine();

                break;
        }
    }
    #endregion


    #endregion

    #region Call Services

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected DataMachine callServicePostENMachine(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);




        return _dtmachine;
    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();



                }
                break;

            case "FvInsertMachine":
                FormView FvInsertMachine = (FormView)ViewAdd.FindControl("FvInsertMachine");

                if (FvInsertMachine.CurrentMode == FormViewMode.Insert)
                {
                    var ddltypecode = ((DropDownList)FvInsertMachine.FindControl("ddltypecode"));
                    var ddltypemachine = ((DropDownList)FvInsertMachine.FindControl("ddltypemachine"));
                    var ddltypeclass = ((DropDownList)FvInsertMachine.FindControl("ddltypeclass"));
                    var ddllocate = ((DropDownList)FvInsertMachine.FindControl("ddllocate"));
                    var ddlholder_orgidx = ((DropDownList)FvInsertMachine.FindControl("ddlholder_orgidx"));

                    Select_TypeCode(ddltypecode);
                    Select_TypeMachine(ddltypemachine);
                    Select_TypeClass(ddltypeclass);
                    Select_Location(ddllocate);
                    getOrganizationList(ddlholder_orgidx);
                }
                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        var ddltypecode = ((DropDownList)FvInsertMachine.FindControl("ddltypecode"));
        var ddlgroupcode = ((DropDownList)FvInsertMachine.FindControl("ddlgroupcode"));
        var ddllocate = ((DropDownList)FvInsertMachine.FindControl("ddllocate"));
        var ddlbuilding = ((DropDownList)FvInsertMachine.FindControl("ddlbuilding"));
        var ddlroom = ((DropDownList)FvInsertMachine.FindControl("ddlroom"));

        var ddlholder_orgidx = ((DropDownList)FvInsertMachine.FindControl("ddlholder_orgidx"));
        var ddlholder_rdeptidx = ((DropDownList)FvInsertMachine.FindControl("ddlholder_rdeptidx"));
        var ddlholder_rdsecidx = ((DropDownList)FvInsertMachine.FindControl("ddlholder_rdsecidx"));
        var ddlholder_empidx = ((DropDownList)FvInsertMachine.FindControl("ddlholder_empidx"));


        switch (ddName.ID)
        {

            case "ddltypecode":
                Select_GroupCode(ddlgroupcode, int.Parse(ddltypecode.SelectedValue));
                break;

            case "ddllocate":
                Select_Building(ddlbuilding, int.Parse(ddllocate.SelectedValue));
                break;

            case "ddlbuilding":
                Select_Room(ddlroom, int.Parse(ddlbuilding.SelectedValue));
                break;

            case "ddlholder_orgidx":
                getDepartmentList(ddlholder_rdeptidx, int.Parse(ddlholder_orgidx.SelectedValue));


                break;

            case "ddlholder_rdeptidx":
                getSectionList(ddlholder_rdsecidx, int.Parse(ddlholder_orgidx.SelectedValue), int.Parse(ddlholder_rdeptidx.SelectedValue));
           
                break;

            case "ddlholder_rdsecidx":

                getEmpList(ddlholder_empidx, int.Parse(ddlholder_orgidx.SelectedValue), int.Parse(ddlholder_rdeptidx.SelectedValue), int.Parse(ddlholder_rdsecidx.SelectedValue));


                break;

        }
    }
    #endregion

    #region reuse

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region BTN 
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnindex":
                litoindex.Attributes.Add("class", "active");
                litoinsert.Attributes.Remove("class");
                litoapprove.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewIndex);
                break;

            case "btninsert":
                litoindex.Attributes.Remove("class");
                litoinsert.Attributes.Add("class", "active");
                litoapprove.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewAdd);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                FvInsertMachine.ChangeMode(FormViewMode.Insert);
                FvInsertMachine.DataBind();

                break;

            case "btnapprove":
                litoindex.Attributes.Remove("class");
                litoinsert.Attributes.Remove("class");
                litoapprove.Attributes.Add("class", "active");
                break;
        }
    }
    #endregion

}