﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="itswl_m0_software.aspx.cs" Inherits="websystem_MasterData_itswl_m0_software" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>

    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>


    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <%--<asp:TextBox ID="test_del" runat="server"></asp:TextBox>--%>

    <asp:GridView ID="Jon" runat="server"></asp:GridView>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">

            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> ข้อมูลรายละเอียด Software แบ่ง License ตามเลขที่ PO</asp:LinkButton>
                <div id="gridviewindex" runat="server">
                    <asp:GridView ID="GvMaster"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="software_idx"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อ Software " ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbsoftware_nameindex" runat="server" Text='<%# Eval("software_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Version" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbsoftware_versionindex" runat="server" Text='<%# Eval("software_version") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเถท Software" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" Visible="false">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbsoftware_type" runat="server" Text='<%# Eval("type_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="Mobile_No" runat="server" Text='<%# Eval("company_name") %>' />--%>

                                        <asp:Label ID="DetailSoftware" runat="server">

                                        <p><b>&nbsp;&nbsp;&nbsp;เลขที่ PO :</b> <%# Eval("po_code") %></p>   

                                        <p><b>&nbsp;&nbsp;&nbsp;บริษัทที่ซื้อ :</b> <%# Eval("company_name") %></p>                                                                             
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่ซื้อ :</b> <%# Eval("date_purchase") %></p>
                                       
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่หมดประกัน :</b> <%# Eval("date_expire") %></p>

                                        <p><b>&nbsp;&nbsp;&nbsp;เบอร์ติดต่อ :</b> <%# Eval("mobile_no") %></p>
                                     
                                        </asp:Label>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License ทั้งหมด" ItemStyle-HorizontalAlign="left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lbcount_licenseindex" runat="server" Text='<%# Eval("count_license") %>' />--%>

                                        <asp:Label ID="DetailLicense" runat="server">
                                            <p><b>&nbsp;&nbsp;&nbsp;จำนวน License ทั้งหมด :</b> <%# Eval("count_license") %></p> 
                                           <%-- <p><b>&nbsp;&nbsp;&nbsp;จำนวน License คงเหลือ :</b> <%# Eval("count_license") %></p>--%>

                                        </asp:Label>



                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" Visible="false">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbsoftware_statusindex" runat="server" Text='<%# getStatus((int)Eval("software_status")) %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewindex" CssClass="btn btn-primary" Font-Size="11px" runat="server" CommandName="btnViewindex" OnCommand="btnCommand"
                                        data-toggle="tooltip" title="ดูข้อมูล" CommandArgument='<%# Eval("software_idx")%>'><i class="fa fa-file-o"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnEditindex" CssClass="btn btn-warning" Font-Size="11px" runat="server" CommandName="btnEditindex" OnCommand="btnCommand"
                                        data-toggle="tooltip" title="แก้ไขข้อมูล" CommandArgument='<%# Eval("software_idx")%>'><i class="fa fa-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelindex" CssClass="btn btn-danger" Font-Size="11px" runat="server" CommandName="btnDelindex" OnCommand="btnCommand"
                                        data-toggle="tooltip" title="ลบข้อมูล" CommandArgument='<%# Eval("software_idx")%>'><i class="fa fa-minus-square-o"></i></asp:LinkButton>

                                    <%--<asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>--%>
                                    <%-- <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("software_idx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>--%>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

            </div>

        </asp:View>



        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton ID="btnBack" CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnBack" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">กรอกข้อมูล Software แบ่ง License ตามเลขที่ PO</div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อ Software / เลขที่ Po --------------%>
                                        <div class="form-group">
                                            <asp:TextBox ID="lbtype_idx" runat="server" Visible="false"/>
                                            <asp:Label ID="lbddlsoftware_nameinsert" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsoftware_nameinsert" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Requiredtxtsoftware_nameinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="ddlsoftware_nameinsert" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อ Software"
                                                    ValidationExpression="กรุณากรอกชื่อ Software" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtsoftware_nameinsert" Width="160" />

                                            </div>

                                            <asp:Label ID="lbpo_codeinsert" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="เลขที่ PO : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpo_codeinsert" CssClass="form-control" Visible="true" MaxLength="10" placeholder="เลขที่ PO ..." runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredtxtpo_codeinsert" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtpo_codeinsert" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกเลขที่ PO"
                                                    ValidationExpression="กรุณากรอกเลขที่ PO" />

                                                <asp:RegularExpressionValidator ID="Requiredtxtpo_codeinsert1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                    ErrorMessage="กรุณากรอกให้ครบ 10 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtpo_codeinsert"
                                                    ValidationExpression="^[a-zA-Z0-9]{10}$" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender131" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtpo_codeinsert" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender141" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtpo_codeinsert1" Width="160" />
                                            </div>


                                        </div>
                                        <%-------------- ชื่อ Software / เลขที่ Po --------------%>

                                        <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="lbdatepurchase" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <div class='input-group date from-date-datepicker'>
                                                    <asp:TextBox ID="txtdatepurchaseinsert" CssClass="form-control" runat="server" placeholder="วันที่ซื้อ ..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                        ControlToValidate="txtdatepurchaseinsert" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                        ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />
                                                </div>

                                            </div>

                                            <asp:Label ID="lbdateexpire" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="วันที่หมดอายุ : " />
                                            <div class="col-sm-3">
                                                <div id="show_date" class='input-group date from-date-datepickerexpire' runat="server" Visible="false">
                                                    <asp:TextBox ID="txtdateexpireinsert" CssClass="form-control" Visible="false" placeholder="วันที่หมดอายุ ..." runat="server"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="Requiredtxtdateexpireadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                        ControlToValidate="txtdateexpireinsert" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกวันที่หมดอายุ"
                                                        ValidationExpression="กรุณากรอกวันที่หมดอายุ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdateexpireadd" Width="160" />
                                                </div>
                                            </div>

                                        </div>
                                        <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>


                                        <%-------------- บริษัทที่ซื้อ, จำนวน License--------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbcompany_idx" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                <asp:DropDownList ID="ddlcompany_idx" runat="server" CssClass="form-control"></asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Requiredddlcompany_idx" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="ddlcompany_idx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกบริษัทที่ซื้อ"
                                                    ValidationExpression="กรุณาเลือกบริษัทที่ซื้อ" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlcompany_idx" Width="160" />
                                            </div>

                                            <asp:Label ID="lblicense_idx" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtlicense_idx" runat="server" placeholder="จำนวน License ..." CssClass="form-control"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Requiredtxtlicense_idx" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtlicense_idx" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกจำนวน License"
                                                    ValidationExpression="กรุณากรอกจำนวน License" />

                                                <asp:RegularExpressionValidator ID="Requiredtxtlicense_idx2" runat="server" ValidationGroup="saveCompany" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtlicense_idx"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtlicense_idx" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtlicense_idx2" Width="160" />
                                            </div>

                                        </div>
                                        <%-------------- บริษัทที่ซื้อ / จำนวน License--------------%>

                                        <%-------------- ราคา --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="lbsoftware_price" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ราคา : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_price" CssClass="form-control" Visible="true" MaxLength="10" placeholder="ราคา ..." runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredtxtsoftware_price" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtsoftware_price" Font-Size="11"
                                                    ErrorMessage="กรุณาราคา"
                                                    ValidationExpression="กรุณาราคา" />

                                                <asp:RegularExpressionValidator ID="Requiredtxtsoftware_price1" runat="server" ValidationGroup="Saveinsert" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtsoftware_price"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtsoftware_price" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtsoftware_price1" Width="160" />
                                            </div>


                                        </div>
                                        <%-------------- ราคา --------------%>

                                        <%-------------- CheckBox --------------%>
                                        <div class="form-group">
                                            <%--<div class="col-sm-offset-3 col-sm-9">--%>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <asp:CheckBox ID="chk_license" runat="server" Font-Bold="true" Text="แบ่งจำนวน Lisense ตามฝ่าย" OnCheckedChanged="CheckboxChanged" AutoPostBack="true" RepeatDirection="Vertical" />
                                                    <%-- <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
                                                        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>--%>

                                                    <div id="showdetailpermissionsystem" class="form-group" runat="server" visible="false">
                                                        <div class="col-sm-12">
                                                            <asp:Label ID="detailshoe" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px" Text="กรุณาแบ่ง License ตามฝ่าย"></asp:Label>
                                                            <%--<h4>คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices </h4>--%>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                        <%-------------- CheckBox --------------%>

                                        <%-------------- Give จำนวน License to Dept --------------%>
                                        <asp:Panel ID="PaneldetailLicense" runat="server" Visible="false">
                                            <div class="panel panel-info">
                                                <div class="panel-heading f-bold">แบ่งจำนวน License ตามฝ่าย</div>
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">
                                                        <%-------------- องค์กร, ฝ่าย--------------%>
                                                        <div class="form-group">
                                                            <asp:Label ID="lborg_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                            <div class="col-sm-3">
                                                                <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                                <asp:DropDownList ID="ddlorg_idxinsert" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="Requiredddlorg_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                    ControlToValidate="ddlorg_idxinsert" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="00" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlorg_idxinsert" Width="160" />
                                                            </div>

                                                            <asp:Label ID="lbrdept_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddlrdept_idxinsert" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="Requiredddlrdept_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                    ControlToValidate="ddlrdept_idxinsert" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="00" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlrdept_idxinsert" Width="160" />
                                                            </div>

                                                        </div>
                                                        <%-------------- องค์กร / ฝ่าย --------------%>



                                                        <%-------------- Asset No / จำนวน License--------------%>
                                                        <div class="form-group">

                                                            <asp:Label ID="lbasset_code" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ Asset : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtassetcode_dept" runat="server" CssClass="form-control" MaxLength="10" placeholder="เลขที่ Asset ..."></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="Requiredtxtassetcode_dept" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                    ControlToValidate="txtassetcode_dept" Font-Size="11"
                                                                    ErrorMessage="กรุณากรอกเลขที่ Asset"
                                                                    ValidationExpression="กรุณากรอกเลขที่ Asset" />
                                                               <%-- <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_dept1" runat="server" ValidationGroup="saveCompany" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtnumlicense_dept"
                                                                    ValidationExpression="^\d+" />--%>
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtassetcode_dept" Width="160" />
                                                               <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_dept1" Width="160" />--%>

                                                            </div>

                                                            <asp:Label ID="lbnumlicense_dept" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="จำนวน License : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txtnumlicense_dept" runat="server" Visible="false" Enabled="false" CssClass="form-control"></asp:TextBox>

                                                                <%--<asp:RequiredFieldValidator ID="Requiredtxtnumlicense_dept" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                    ControlToValidate="txtnumlicense_dept" Font-Size="11"
                                                                    ErrorMessage="กรุณากรอกจำนวน License"
                                                                    ValidationExpression="กรุณากรอกจำนวน License" />
                                                                <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_dept1" runat="server" ValidationGroup="saveCompany" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                    ControlToValidate="txtnumlicense_dept"
                                                                    ValidationExpression="^\d+" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_dept" Width="160" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_dept1" Width="160" />--%>
                                                            </div>

                                                        </div>
                                                        <%-------------- Asset No/ จำนวน License --------------%>


                                                        <%-------------- เพิ่มรายการ --------------%>
                                                        <div class="form-group">
                                                            <%--<div class="col-sm-2"></div>--%>
                                                            <div class="col-lg-offset-2 col-sm-10">
                                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server" CommandName="btnInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail" title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                                                <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                                            </div>

                                                        </div>
                                                        <%-------------- เพิ่มรายการ --------------%>

                                                        <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                                        <asp:Panel ID="gidviewdetaillicense" runat="server" Visible="false" AutoPostBack="true">
                                                            <div class="col-sm-12">
                                                                <asp:GridView ID="GVShowdetailLicense"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                    HeaderStyle-CssClass="info"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="true"
                                                                    PageSize="10"
                                                                    OnRowEditing="Master_RowEditing"
                                                                    OnRowDeleting="Master_RowDeleting"
                                                                    OnPageIndexChanging="Master_PageIndexChanging">
                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <%# (Container.DataItemIndex +1) %>

                                                                                </small>
                                                                                <%-- <%# (Container.DataItemIndex +1) %>--%>
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("Software_License_dataset") %>'></asp:Label>

                                                                                    <asp:Label ID="lbOrgNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("OrgNameTH_dataset") %>'></asp:Label>
                                                                                    <asp:Label ID="lbOrgIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("OrgIDX_dataset") %>'></asp:Label>
                                                                                </small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbDeptNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("DeptNameTH_dataset") %>'></asp:Label>
                                                                                    <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RDeptIDX_dataset") %>'></asp:Label>
                                                                                </small>

                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เลขที่ Asset" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbAsset_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("Asset_dataset") %>'></asp:Label>
                                                                                    <%--<asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RDeptIDX_dataset") %>'></asp:Label>--%>
                                                                                </small>

                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <small>
                                                                                    <asp:Label ID="lbnumlicense_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("numlicense_dataset") %>'></asp:Label>
                                                                                </small>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="ลบ" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete" />
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate />
                                                                            <FooterTemplate />
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>
                                                            </div>

                                                        </asp:Panel>
                                                        <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย --------------%>


                                                        <asp:TextBox ID="Count_numin_colum" runat="server" Visible="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <%-------------- Give จำนวน License to Dept --------------%>

                                        <%-------------- บันทึก / ยกเลิก --------------%>
                                        <div class="form-group">
                                            <div class="col-lg-offset-10 col-sm-2">
                                                <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="Saveinsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                                            </div>

                                        </div>
                                        <%-------------- บันทึก / ยกเลิก --------------%>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </asp:View>
        <!-- End Insert Form -->

        <asp:View ID="ViewDetail" runat="server">
            <%--<div class="col-md-12">--%>
            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnBackIndex" CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnBackIndex" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" id="panelqrcode" runat="server">
                    <br />
                    <div class="panel panel-info">
                        <div id="divshownamedetailqrcode" runat="server" class="panel-heading f-bold">รายละเอียด Software</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <asp:FormView ID="FvViewDetail" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">

                    <EditItemTemplate>
                        <div class="col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <asp:Label ID="hf_software_idx" runat="server" Visible="false" Text='<%# Eval("software_idx") %>' />
                                    <asp:Label ID="hf_type_idx" runat="server" Visible="false" Text='<%# Eval("type_idx") %>' />

                                    <div class="form-horizontal" role="form">

                                        <%-------------- เลขที่ Asset, เลขที่ PO --------------%>
                                        <div class="form-group">
                                           <%-- <asp:Label ID="lbasset_codeview" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="เลขที่ Asset : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtasset_codeview" Enabled="false" CssClass="form-control" Visible="false" runat="server" Text='<%# Eval("asset_code") %>'></asp:TextBox>
                                            </div>--%>

                                            <asp:Label ID="lbpo_codeview" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="เลขที่ PO : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpo_codeview" Enabled="false" CssClass="form-control" Visible="true" runat="server" Text='<%# Eval("po_code") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- เลขที่ Asset, เลขที่ PO --------------%>


                                        <%-------------- ชื่อ Software, Version --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbsoftware_nameview" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_nameview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_name") %>'></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbsoftware_versionview" CssClass="col-sm-2 control-label" runat="server" Text="Version : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_versionview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_version") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- ชื่อ Software, Version --------------%>

                                        <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbdate_purchaseview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <div class='input-group date from-date-datepicker'>
                                                    <asp:TextBox ID="txtdate_purchaseview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_purchase") %>'></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>

                                            <asp:Label ID="lbdate_expireview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดอายุ : " />
                                            <div class="col-sm-3">
                                                <div class='input-group date from-date-datepicker'>
                                                    <asp:TextBox ID="txtdate_expireview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_expire") %>'></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>

                                        </div>
                                        <%-------------- วันที่ซื้อ, วันที่หมดประกัน --------------%>

                                        <%-------------- บริษัทที่ซื้อ , จำนวน License ทั้งหมด --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="lbcompany_nameviexw" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                            <asp:Label ID="lbcompany_idxdit" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("company_idx") %>' Visible="false" />

                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlcompany_nameedit" AutoPostBack="false" Enabled="false" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>


                                            <asp:Label ID="lbnum_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtnum_licenseview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("count_license") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- บริษัทที่ซื้อ, จำนวน License ทั้งหมด --------------%>

                                        <%-------------- ราคา --------------%>
                                        <div class="form-group">
                                           
                                            <asp:Label ID="lbsoftware_priceview" CssClass="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_priceview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_price") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- ราคา --------------%>



                                        <%-------------- CheckBox --------------%>
                                        <div class="form-group">
                                            <%--<div class="col-sm-offset-3 col-sm-9">--%>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <asp:CheckBox ID="chk_licenseedit" runat="server" Font-Bold="true" Text="แบ่งจำนวน Lisense ตามฝ่ายเพิ่มเติม" OnCheckedChanged="CheckboxChanged" AutoPostBack="true" Visible="false" RepeatDirection="Vertical" />
                                                    <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
                                                        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>
                                                </div>
                                            </div>
                                        </div>

                                        <%-- <div id="showdetailpermissionsystemedit" class="form-group" runat="server" visible="false">
                                                        <div class="col-sm-12">
                                                            <asp:Label ID="detailshoeedit" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="14px" Text="กรุณาแบ่ง License ตามฝ่าย"></asp:Label>--%>
                                        <%--<h4>คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices </h4>--%>
                                        <%--</div>
                                                    </div>

                                                   
                                                </div>
                                            </div>

                                           
                                        </div>--%>
                                        <%-------------- CheckBox --------------%>

                                        <%-------------- Give จำนวน License to Dept Edit --------------%>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:Panel ID="PaneldetailLicenseEdit" runat="server" Visible="false">
                                                    <div class="panel panel-info">
                                                        <div class="panel-heading f-bold">แบ่งจำนวน License ตามฝ่าย(แก้ไข)</div>
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">
                                                                <%-------------- องค์กร, ฝ่าย--------------%>
                                                                <div class="form-group">
                                                                    <asp:Label ID="lborg_idxeditlicense" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                                                    <div class="col-sm-3">
                                                                        <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                                                        <asp:DropDownList ID="ddlorg_idxeditlicense" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        </asp:DropDownList>

                                                                        <asp:RequiredFieldValidator ID="Requireddlorg_idxeditlicense" ValidationGroup="Saveeditdetail" runat="server" Display="None"
                                                                            ControlToValidate="ddlorg_idxeditlicense" Font-Size="11"
                                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                                            ValidationExpression="กรุณาเลือกองค์กร" InitialValue="00" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlorg_idxeditlicense" Width="160" />
                                                                    </div>

                                                                    <asp:Label ID="lbrdept_idxeditlicense" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                                                    <div class="col-sm-3">
                                                                        <asp:DropDownList ID="ddlrdept_idxeditlicense" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>
                                                                        </asp:DropDownList>

                                                                        <asp:RequiredFieldValidator ID="Requiredddlrdept_idxeditlicense" ValidationGroup="Saveeditdetail" runat="server" Display="None"
                                                                            ControlToValidate="ddlrdept_idxeditlicense" Font-Size="11"
                                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                                            ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="00" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlrdept_idxeditlicense" Width="160" />
                                                                    </div>

                                                                </div>
                                                                <%-------------- องค์กร / ฝ่าย --------------%>

                                                                <%-------------- จำนวน License--------------%>
                                                                <div class="form-group">
                                                                    <asp:Label ID="lbnumlicense_depteditlicense" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License : " />
                                                                    <div class="col-sm-3">
                                                                        <asp:TextBox ID="txtnumlicense_depteditlicense" runat="server" placeholder="จำนวน License ..." CssClass="form-control"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="Requiredtxtnumlicense_depteditlicense" ValidationGroup="Saveeditdetail" runat="server" Display="None"
                                                                            ControlToValidate="txtnumlicense_depteditlicense" Font-Size="11"
                                                                            ErrorMessage="กรุณากรอกจำนวน License"
                                                                            ValidationExpression="กรุณากรอกจำนวน License" />
                                                                        <asp:RegularExpressionValidator ID="Requiredtxtnumlicense_depteditlicense1" runat="server" ValidationGroup="saveCompany" Display="None"
                                                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                                            ControlToValidate="txtnumlicense_depteditlicense"
                                                                            ValidationExpression="^\d+" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_depteditlicense" Width="160" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtnumlicense_depteditlicense1" Width="160" />
                                                                    </div>

                                                                </div>
                                                                <%-------------- จำนวน License --------------%>


                                                                <%-------------- เพิ่มรายการ --------------%>
                                                                <div class="form-group">
                                                                    <%--<div class="col-sm-2"></div>--%>
                                                                    <div class="col-lg-offset-2 col-sm-10">
                                                                        <asp:LinkButton ID="btnEditDetail" CssClass="btn btn-default" runat="server" CommandName="btnEditDetail" OnCommand="btnCommand" ValidationGroup="Saveeditdetail" title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                                                        <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                                                    </div>

                                                                </div>
                                                                <%-------------- เพิ่มรายการ --------------%>

                                                                <asp:TextBox ID="Count_numin_columEdit" runat="server" Visible="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <%-------------- Give จำนวน License to Dept --------------%>


                                        <asp:Panel ID="gidviewdetaileditlicense" runat="server" Visible="false" AutoPostBack="true">
                                            <div class="col-sm-12">
                                                <asp:GridView ID="GVShowEditdetailLicense"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="true"
                                                    PageSize="10"
                                                    OnRowEditing="Master_RowEditing"
                                                    OnRowDeleting="Master_RowDeleting"
                                                    OnPageIndexChanging="Master_PageIndexChanging">
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <%--<asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <%# (Container.DataItemIndex +1) %>--%>

                                                        <%-- </small>--%>
                                                        <%-- <%# (Container.DataItemIndex +1) %>--%>
                                                        <%--</ItemTemplate>

                    </asp:TemplateField>--%>

                                                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("SoftwareEdit_License_dataset") %>'></asp:Label>

                                                                    <asp:Label ID="lbOrgNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("OrgNameTHEdit_dataset") %>'></asp:Label>
                                                                    <asp:Label ID="lbOrgIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("OrgIDXEdit_dataset") %>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbDeptNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("DeptNameTHEdit_dataset") %>'></asp:Label>
                                                                    <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RDeptIDXEdit_dataset") %>'></asp:Label>
                                                                </small>

                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>


                                                        <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <asp:Label ID="lbnumlicense_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("numlicenseEdit_dataset") %>'></asp:Label>
                                                                </small>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="ลบ" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete" />
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>

                                        </asp:Panel>
                                        <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย --------------%>

                                        <!-- START history จำนวน License Edit ตามฝ่าย -->
                                        <div class="form-group">
                                            <div class="col-sm-12">

                                                <%-- <br />--%>
                                                <div class="row panel panel-default font_text" visible="true" runat="server" id="panel_detail_softwareedit">
                                                    <div class="panel panel-info">
                                                        <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                                                        <div class="panel-heading f-bold">แก้ไขจำนวน License ตามฝ่ายเดิม</div>
                                                    </div>


                                                    <%-- <asp:TextBox ID="testbind" runat="server"></asp:TextBox>--%>
                                                    <div class="panel-body">
                                                        <table class="table table-striped f-s-12">
                                                            <%--<asp:GridView ID="gvTest" runat="server"></asp:GridView>--%>
                                                            <asp:Repeater ID="rptDetailSoftwareEdit" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <tr>
                                                                        <th>#</th>
                                                                        <th>องค์กร</th>
                                                                        <th>ฝ่าย</th>
                                                                        <th>เลขที่ asset</th>
                                                                        <th runat="server" visible="true">จำนวน License</th>
                                                                        <%--<th>จำนวน License1</th>--%>
                                                                        <%--<th>แก้ไข License</th>--%>
                                                                        <th>แก้ไข</th>
                                                                    </tr>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td data-th="#">
                                                                            <%# (Container.ItemIndex + 1) %>

                                                                        </td>

                                                                        <td data-th="องค์กร">
                                                                            <%--<%# Eval("OrgNameTH") %>--%>
                                                                            <asp:Label ID="lblOrgNameTHedit" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                                            <asp:Label ID="lbllicense_org_idxedit" Visible="false" runat="server" Text='<%# Eval("license_org_idx") %>' />

                                                                            <asp:DropDownList ID="ddlorg_idxedit" runat="server" Visible="false" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            </asp:DropDownList>

                                                                        </td>

                                                                        <td data-th="ฝ่าย">
                                                                            <%-- <%# Eval("DeptNameTH") %>--%>
                                                                            <asp:Label ID="lblDeptNameTHedit" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                                            <asp:Label ID="lblrdept_idxedit" Visible="false" runat="server" Text='<%# Eval("license_rdept_idx") %>' />

                                                                            <asp:DropDownList ID="ddlrdept_idxedit" runat="server" Visible="false" CssClass="form-control">
                                                                                <asp:ListItem Text="กรุณาเลือกฝ่าย ...." Value="00"></asp:ListItem>
                                                                            </asp:DropDownList>

                                                                        </td>

                                                                        <td data-th="เลขที่ asset">
                                                                            <%-- <%# Eval("DeptNameTH") %>--%>
                                                                            <asp:Label ID="lbasset_edit" runat="server" Text='<%# Eval("asset_code") %>' />                                                                          
                                                                            <asp:TextBox ID="txtasset_edit" Enabled="false" CssClass="form-control" runat="server" Width="120" Text='<%# Eval("asset_code") %>' Visible="false" />
                                                                           

                                                                        </td>

                                                                         <td runat="server" visible="true" data-th="จำนวน License">
                                                                            <asp:Label ID="lblnum_licenseedit" runat="server" Visible="true" Text='<%# Eval("count_asset") %>' />
                                                                            <asp:TextBox ID="txtnum_licenseedit" Enabled="false" CssClass="form-control" runat="server" Width="120" Text='<%# Eval("count_asset") %>' Visible="false" />
                                                                        </td>


                                                                   
                                                                       <%-- <td data-th="จำนวน License" >
                                                                            <asp:Label ID="lblnum_licenseedit" runat="server" Text='<%# Eval("num_license_rdept_idx") %>' />
                                                                            <asp:TextBox ID="txtnum_licenseedit" CssClass="form-control" runat="server" Width="120" Text='<%# Eval("num_license_rdept_idx") %>' Visible="false" />
                                                                        </td>--%>
                                                                     
                                                                        <td data-th="แก้ไข">
                                                                            <asp:LinkButton ID="lnkEdit" CssClass="text-edit" data-toggle="tooltip"
                                                                                title="แก้ไข" runat="server" OnClick="OnEdit"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>

                                                                        </td>
                                                                    </tr>




                                                                    <%-- <tr id="Tr1" runat="server" visible='<%# (((IList)((Repeater)Container.Parent).DataSource).Count).ToString() == (Container.ItemIndex + 1).ToString() %>'>
                                 
                                        <td >
                                            <%# (Container.ItemIndex + 1) %>

                                        </td>

                                        <td>
                                            <asp:DropDownList ID="ddlorg_idxedit_add" runat="server"  CssClass="form-control"  Visible="true">

                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlrdept_idxedit_add" runat="server"  Visible="true" CssClass="form-control"></asp:DropDownList>
                                        </td>
                                         <td>
                                            <asp:TextBox ID="txtnum_licenseedit_add" runat="server"  Width="120"  Visible="true" CssClass="form-control"></asp:TextBox>
                                        </td>

                                        <td>
                                            <asp:LinkButton ID="btnShowAdd" runat="server" data-toggle="tooltip"
                                                title="เพิ่ม" OnClick="btnAdd_Click"><i class="fa fa-plus-square"></i></asp:LinkButton>
                                        </td>
                                    </tr>--%>
                                                                </ItemTemplate>

                                                            </asp:Repeater>
                                                        </table>
                                                    </div>
                                                </div>

                                                <%--  <h5>&nbsp;</h5>--%>
                                                <br />
                                            </div>
                                        </div>
                                        <!-- END history จำนวน License Edit ตามฝ่าย -->

                                    </div>

                                </div>
                            </div>

                        </div>

                    </EditItemTemplate>

                    <ItemTemplate>
                        <%--<br />--%>
                        <div class="col-sm-12">
                            <div class="panel panel-info">
                                <div class="panel-body">

                                    <asp:HiddenField ID="hf_software_idx" runat="server" Value='<%# Eval("software_idx") %>' />
                                    <asp:Label ID="hf_type_idx" runat="server" Visible="false" Text='<%# Eval("type_idx") %>' />


                                    <div class="form-horizontal" role="form">


                                        <%-------------- เลขที่ Asset, เลขที่ PO --------------%>
                                        <div class="form-group" id="show_asset" runat="server" >
                                           <%-- <asp:Label ID="lbasset_codeview" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="เลขที่ Asset : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtasset_codeview" Enabled="false" CssClass="form-control" Visible="false" runat="server" Text='<%# Eval("asset_code") %>'></asp:TextBox>
                                            </div>--%>

                                            <asp:Label ID="lbpo_codeview" CssClass="col-sm-2 control-label" runat="server" Visible="true" Text="เลขที่ PO : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpo_codeview" Enabled="false" CssClass="form-control" Visible="true" runat="server" Text='<%# Eval("po_code") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- เลขที่ Asset, เลขที่ PO --------------%>

                                        <%-------------- ชื่อ Software, Version --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbsoftware_nameview" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_nameview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_name") %>'></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbsoftware_versionview" CssClass="col-sm-2 control-label" runat="server" Text="Version : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_versionview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_version") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- ชื่อ Software, Version --------------%>

                                        <%-------------- บริษัทที่ซื้อ , เบอร์ติดต่อ --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbcompany_nameviexw" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcompany_nameview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("company_name") %>'></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbmobile_noview" CssClass="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmobile_noview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("mobile_no") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- บริษัทที่ซื้อ, เบอร์ติดต่อ --------------%>

                                        <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbdate_purchaseview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtdate_purchaseview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_purchase") %>'></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbdate_expireview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดอายุ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtdate_expireview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_expire") %>'></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- วันที่ซื้อ, วันที่หมดประกัน --------------%>

                                        <%-------------- จำนวน License ทั้งหมด / ราคา --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbnum_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtnum_licenseview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("count_license") %>'></asp:TextBox>
                                            </div>

                                             <asp:Label ID="lbsoftware_priceview" CssClass="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_priceview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("software_price") %>'></asp:TextBox>
                                            </div>

                                            <%--<asp:Label ID="lbsoftware_status" Visible="false" CssClass="col-sm-2 control-label" runat="server" Text="สถานะ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsoftware_statusview" Visible="false" Enabled="false" CssClass="form-control" runat="server" Text='<%# getStatusView((int)Eval("software_status")) %>'></asp:TextBox>

                                            </div>--%>
                                        </div>
                                        <%-------------- จำนวน License ทั้งหมด --------------%>

                                        <%-------------- จำนวน License ตามฝ่าย  --------------%>
                                        <%--<div class="form-group">
                                            <div class="col-sm-1"></div>
                                            <asp:Label ID="lbdetail_dept" Font-Bold="true" Font-Size="14px" runat="server" Text=" - จำนวน License ตามฝ่าย" />

                                            <td data-th="วัน / เวลา"><%# Eval("approval_log_date") + " " + Eval("approval_log_time") %> น.</td>

                                        </div>--%>
                                        <%-------------- จำนวน License ตามฝ่าย  --------------%>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>

                </asp:FormView>

            </div>

            <!-- START history จำนวน License ตามฝ่าย -->
            <div class="col-md-12">

                <%-- <br />--%>
                <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_detail_software">
                    <div class="panel panel-info">
                        <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                        <div class="panel-heading f-bold">จำนวน License ตามฝ่าย</div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped f-s-12">
                            <asp:Repeater ID="rptDetailSoftware" runat="server">
                                <HeaderTemplate>
                                    <tr>
                                        <th>#</th>
                                        <th>องค์กร</th>
                                        <th>ฝ่าย</th>
                                        <th>เลขที่ Asset</th>
                                        <th runat="server" visible="true">จำนวน License</th>
                                        <%--<th>จำนวน License</th>--%>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="#"><%# (Container.ItemIndex + 1) %></td>
                                        <td data-th="องค์กร"><%# Eval("OrgNameTH") %></td>
                                        <td data-th="ฝ่าย"><%# Eval("DeptNameTH") %></td>
                                        <%--<td data-th="จำนวน License ทั้งหมก"><%# Eval("num_license_rdept_idx") %></td>--%>
                                        <td data-th="เลขที่ Asset"><%# Eval("asset_code") %></td>
                                        <td runat="server" visible="true" data-th="จำนวน License"><%# Eval("count_asset") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </div>
                </div>

                <%--  <h5>&nbsp;</h5>--%>
                <br />
            </div>
            <!-- END history จำนวน License ตามฝ่าย -->



            <%-------------- บันทึก / ยกเลิก แก้ไข --------------%>
            <div class="form-group" id="show_btnedit" runat="server" visible="false">
                <div class="col-md-12">
                    <%--<div class="col-lg-offset-10 col-sm-2">--%>
                    <div class="pull-right">
                        <asp:LinkButton ID="btn_Saveedit" CssClass="btn btn-success" runat="server" Visible="false" CommandName="btn_Saveedit" OnCommand="btnCommand" ValidationGroup="Saveedit" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                        <asp:LinkButton ID="btn_Canceledit" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btn_Canceledit"> ยกเลิก</asp:LinkButton>

                        <%-------------- Check จำนวน License ทั้งหมด ก่อน แก้ไข --------------%>
                        <asp:TextBox ID="SumLicenseEdit" runat="server" Visible="false"></asp:TextBox>

                    </div>
                </div>
            </div>
            <%-------------- บันทึก / ยกเลิก แก้ไข --------------%>

            <h2>&nbsp;&nbsp;</h2>

            <%--</div>--%>
        </asp:View>


    </asp:MultiView>


</asp:Content>

