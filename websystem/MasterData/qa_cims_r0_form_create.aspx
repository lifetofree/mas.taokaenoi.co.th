﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_r0_form_create.aspx.cs" Inherits="websystem_MasterData_qa_cims_r0_form_create" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Literal ID="litBack" runat="server"></asp:Literal>
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnCreateForm" CssClass="btn btn-primary" runat="server" data-original-title="สร้างฟอร์ม" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="0" title="สร้างฟอร์ม"><i class="fa fa-plus-square" aria-hidden="true"></i> สร้างฟอร์ม</asp:LinkButton>

            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มฟอร์ม</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="ReqtbFormName" runat="server"
                                        ControlToValidate="tbFormName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="saveFormName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormName" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormName" Width="180" />

                                    <label class="col-sm-3 control-label">ชื่อฟอร์ม</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="tbFormName" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์ม ..." Enabled="true" />
                                    </div>


                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ประเภทเครื่องมือที่สอบเทียบ</label>
                                    <div class="col-sm-8">
                                        <asp:RequiredFieldValidator ID="ReddlEquipmentName" runat="server"
                                            ControlToValidate="ddlEquipmentName" Display="None" SetFocusOnError="true" InitialValue="0"
                                            ErrorMessage="*กรุณาเลือกประเภทเครื่องมือที่สอบเทียบ" ValidationGroup="saveFormName" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlEquipmentName" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipmentName" Width="200" />
                                        <asp:DropDownList ID="ddlEquipmentName" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">ฟอร์มรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTopicForm" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>--%>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">สถานะ</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">

                                    <div class="col-sm-11">
                                        <asp:LinkButton ID="btnAddForm" CssClass="btn btn-default pull-right" Visible="false" runat="server" data-original-title="เพิ่ม" data-toggle="tooltip" ValidationGroup="saveFormName"
                                            OnCommand="btnCommand" CommandName="cmdAddForm" title="เพิ่ม"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <asp:Panel ID="panelFormButton" runat="server" Visible="true">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">หัวข้อรายการที่เพิ่ม</label>--%>
                                    <div class="col-sm-9">

                                        <asp:GridView ID="gvCreateFormList"
                                            runat="server" Visible="false"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="success"
                                            HeaderStyle-Height="30px"
                                            OnRowDeleting="gvRowDeleted"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowEditing="gvRowEditing"
                                            OnRowUpdating="gvRowUpdating"
                                            OnRowCancelingEdit="gvRowCancelingEdit"
                                            OnRowDataBound="gvRowDataBound">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbnumber" runat="server" Visible="false" />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ประเภทรายการการที่ตรวจ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblNameEquipmentName" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("nameEquipmentName") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ฟอร์มรายการที่ตรวจ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblNameTopic" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("nameTopic") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:LinkButton ID="btndelete" CssClass="text-trash small" runat="server" CommandName="Delete"
                                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')" title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>


                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r0_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R0FormIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r0_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์ม" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_formname" runat="server" CssClass="form-control" Text='<%# Eval("r0_form_create_name")%>' />
                                            <asp:RequiredFieldValidator ID="Retbupdate_formname" runat="server"
                                                ControlToValidate="tbupdate_formname" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์ม" ValidationGroup="editFormcreate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtbupdate_formname" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retbupdate_formname" Width="220" />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทเครื่องมือที่สอบเทียบ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbtestIDXUpdate" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("equipment_idx")%>' />
                                            <asp:DropDownList ID="ddlEquipmentNameUpdate" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater0status" Text='<%# Eval("r0_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="editFormcreate"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("r0_form_create_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภทเครื่องมือที่สอบเทียบ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbTestType" runat="server" Text='<%# Eval("equipment_name")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r0_form_create_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r0_form_create_idx") + "," + "0" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r0_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r0_form_create_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>



        <asp:View ID="pageCreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAddRoot" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มรายการที่สอบเทียบ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มรายการที่สอบเทียบ</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvformInsertRoot" DefaultMode="Insert" runat="server" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อฟอร์มรายการที่สอบเทียบ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-3 control-label">ชื่อฟอร์ม</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="tbFormNameLevel1" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มรายการที่สอบเทียบ ..." Enabled="false" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ฟอร์มรายการที่สอบเทียบ</label>
                                    <div class="col-sm-8">
                                        <asp:RequiredFieldValidator ID="ReddlTopicForm" runat="server" InitialValue="0"
                                            ControlToValidate="ddlTopicForm" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกฟอร์มรายการที่สอบเทียบ" ValidationGroup="saveFormcreateR1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlTopicForm" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlTopicForm" Width="220" />
                                        <asp:DropDownList ID="ddlTopicForm" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">สถานะ</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlFormStatusLevel2" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormcreateR1" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvR1FormCreate"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormR1idx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <asp:TextBox ID="tbR0idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("r0_form_create_idx")%>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R1FormName" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์ม" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_R0formname" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("r0_form_create_name")%>' />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ฟอร์มรายการที่สอบเทียบ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbFormDetailIDXUpdate" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("form_detail_idx")%>' />
                                            <asp:DropDownList ID="ddlFormDetailUpdate" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReddlFormDetailUpdate" runat="server" InitialValue="0"
                                                ControlToValidate="ddlFormDetailUpdate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกฟอร์มรายการที่สอบเทียบ" ValidationGroup="editFormcreateR1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlFormDetailUpdate" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlFormDetailUpdate" Width="220" />


                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater1status" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="editFormcreateR1"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblR0FormName" runat="server" Text='<%# Eval("r0_form_create_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ฟอร์มรายการที่สอบเทียบ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbR1status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="lbstatus_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="lbstatus_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdViewRoot"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "0" %>' title="view">
                                <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDeleteR1"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

        <asp:View ID="pageRootSubSet" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lnkbtnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="lnkbtnRootSet" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มย่อยรายการที่สอบเทียบ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มย่อยรายการที่สอบเทียบ</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvformInsertSubFormDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มฟอร์มย่อยรายการที่สอบเทียบ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฟอร์มรายการที่สอบเทียบ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSubSetName" runat="server" CssClass="form-control" Enabled="false" />


                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อยรายการที่สอบเทียบ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbSubSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbSubSetName" runat="server"
                                        ControlToValidate="tbSubSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbSubSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbSubSetName" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveSubSetName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSaveRoot" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSub" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterSubSet"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="r1_form_create_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbsubsetidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateSubSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์มย่อยรายการที่สอบเทียบ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetName" runat="server" CssClass="form-control" Text='<%# Eval("text_name_setform")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_sub_setname" runat="server"
                                                ControlToValidate="tbupdateSubSetName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ" ValidationGroup="saveSubRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_ssname" runat="Server" PopupPosition="BottomLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_sub_setname" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateSubSetStatus" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="saveSubRoot"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อฟอร์มย่อยรายการที่สอบเทียบ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblSubSetName" runat="server" Text='<%# Eval("text_name_setform")%>'></asp:Label>
                                <asp:TextBox ID="tbSubSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("r1_form_root_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootSubStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdViewRoot"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "1" %>' title="view">
                                <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelRootSubSet" CssClass="text-trash small" runat="server" CommandName="cmdDeleteR1Root"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

        <asp:View ID="pageRootSubSetLvl4" runat="server">
            <div class="form-group">
                
                <asp:LinkButton ID="lnkbtnBacklvl4" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="lnkbtnRootSetlvl4" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มย่อยรายการที่สอบเทียบ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="3" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มย่อยรายการที่สอบเทียบ</asp:LinkButton>
            </div>
            <!--formview insert Level 4-->
            <asp:FormView ID="fvformInsertSubFormDetaillvl4" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มฟอร์มย่อยรายการที่สอบเทียบ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฟอร์มรายการที่สอบเทียบ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSubSetName" runat="server" CssClass="form-control" Enabled="false" />


                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อยรายการที่สอบเทียบ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbSubSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbSubSetName" runat="server"
                                        ControlToValidate="tbSubSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbSubSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbSubSetName" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveSubSetName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSaveRoot" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSub" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterSubSetlvl4"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="r1_form_create_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbsubsetidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateSubSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์มย่อยรายการที่สอบเทียบ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetName" runat="server" CssClass="form-control" Text='<%# Eval("text_name_setform")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_sub_setname" runat="server"
                                                ControlToValidate="tbupdateSubSetName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่สอบเทียบ" ValidationGroup="saveSubRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_ssname" runat="Server" PopupPosition="BottomLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_sub_setname" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateSubSetStatus" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="saveSubRoot"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อฟอร์มย่อยรายการที่สอบเทียบ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblSubSetName" runat="server" Text='<%# Eval("text_name_setform")%>'></asp:Label>
                                <asp:TextBox ID="tbSubSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("r1_form_root_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootSubStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <%--   <asp:LinkButton ID="btnViewRootSubSet" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("set_idx") + "," + "2" %>' title="view">--%>
                            <%--                 <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelRootSubSet" CssClass="text-trash small" runat="server" CommandName="cmdDeleteR1Root"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

    </asp:MultiView>



</asp:Content>

