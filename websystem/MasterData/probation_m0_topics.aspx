﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="probation_m0_topics.aspx.cs" Inherits="websystem_MasterData_probation_m0_topics_evaluated" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <!-- view Home page -->
        <asp:View ID="viewHomepage" runat="server">
            <div class="col-sm-12" runat="server" id="content_Homepage">
                <label>การจัดการรูปแบบฟอร์มประเมินทดลองงาน</label>
                <!-- gridView type evaluation -->
                <asp:GridView ID="gvEvaluationType" runat="server" AutoGenerateColumns="false" DataKeyNames="m0_type_idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลพนักงาน</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="m0_type_idx" runat="server" Visible="false" Text='<%# Eval("m0_type_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ประเภทแบบฟอร์มการประเมิน" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lb_detailsd" runat="server"
                                            CssClass="col-sm-12" Text='<%# Eval("type_evaluation_name") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะแบบฟอร์ม" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="label_status_evaluation" runat="server" Visible="false"
                                        CssClass="col-sm-12" Text='<%# Eval("type_evaluation_ststus") %>'></asp:Label>
                                    <asp:Label ID="status_offline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ฟอร์มถูกยกเลิกไปแล้ว"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>ไม่ใช้งาน</b></span>
                                    </asp:Label>
                                    <asp:Label ID="status_online" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ใช้งาน" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>ใช้งาน</b></span>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px;">
                                    <asp:LinkButton ID="editform" CssClass="text-edit" runat="server" CommandName="editform_evaluation"
                                        data-toggle="tooltip" OnCommand="btn_command" CommandArgument='<%#Eval("m0_type_idx") %>' title="แก้ไขฟอร์มการประเมิน">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="viewInsertForm" runat="server">
            <div class="col-sm-12" runat="server" id="content_insertData">
                <div class="col-sm-12">
                    <div class="form-group">
                        <asp:LinkButton CssClass="btn btn-primary" data-toggle="tooltip" title="Back" runat="server"
                            CommandName="BackHome" OnCommand="btn_command">ย้อนกลับ</asp:LinkButton>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label>หัวข้อการประเมิน</label>
                        <asp:UpdatePanel ID="paneltopicsName" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txttopicsName" runat="server" CssClass="form-control"
                                    placeholder="กรุณากรอกหัวข้อการประเมิน..." />
                                <%--<asp:RequiredFieldValidator ID="requiredcategoryName"
                              ValidationGroup="save" runat="server"
                              Display="Dynamic"
                              SetFocusOnError="true"
                              ControlToValidate="txtcategoryName"
                              Font-Size="1em" ForeColor="Red"
                              ErrorMessage="กรุณากรอกชื่อชนิดอุปกรณ์" />--%>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label>&nbsp;</label>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton CssClass="btn btn-primary" data-toggle="tooltip" title="เพิ่มหัวข้อ" runat="server"
                                    CommandName="insertList" OnCommand="btn_command"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่มหัวข้อ</asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <h5>&nbsp;</h5>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายการหัวข้อประเมิน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <asp:Repeater ID="listTypeTopic_firstLevel" runat="server">
                                            <HeaderTemplate>
                                                <div class="row">
                                                    <label class="col-sm-1 control-label">ลำดับ</label>
                                                    <label class="col-sm-8 control-label">หัวข้อ</label>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="col-sm-1">
                                                        <small><%# Container.ItemIndex + 1 %></small>
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <small><span><%# Eval("topics_name") %>
                                                            <asp:Label ID="m0_type_idx_" runat="server" Visible="false" Text='<%# Eval("m0_topics_idx") %>' />
                                                            <asp:Label ID="root_m0_Topics" runat="server" Visible="true" Text='<%# Eval("root_m0_topics_idx") %>' />
                                                            <asp:LinkButton ID="btninsertlistlevel2" CssClass="text-success" runat="server" CommandName="insertlistlevel2"
                                                                data-toggle="tooltip" OnCommand="btn_command" CommandArgument='<%#Eval("m0_topics_idx") %>' title="เพิ่มหัวข้อย่อย">
                                                        <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มหัวข้อย่อย</asp:LinkButton>
                                                        </span></small>
                                                        <asp:Repeater ID="listsubTopics" runat="server">
                                                            <ItemTemplate>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <small>-&nbsp;<span><%# Eval("topics_name") %></span></small>

                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:Panel ID="panel_subTopics" runat="server" Visible="false">
                                            <div class="panel panel-info">
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>หัวข้อย่อย..</label>
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtSubtopicsName" runat="server" CssClass="form-control"
                                                                        placeholder="กรุณากรอกหัวข้อการประเมิน..." />
                                                                    <br />
                                                                    <asp:LinkButton CssClass="btn btn-success pull-right" data-toggle="tooltip" title="เพิ่มหัวข้อ" runat="server"
                                                                        CommandName="insertsubTopics" OnCommand="btn_command"><i class="glyphicon glyphicon-plus-sign"></i> เพิ่ม</asp:LinkButton>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

</asp:Content>

