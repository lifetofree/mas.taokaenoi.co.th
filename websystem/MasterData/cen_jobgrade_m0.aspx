﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cen_jobgrade_m0.aspx.cs" Inherits="websystem_MasterData_cen_jobgrade_m0" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvJobgrade" runat="server" ActiveViewIndex="0">
        <asp:View ID="vMaster" runat="server">

            <asp:FormView ID="FormViewS" runat="server" Visible="false" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ค้นหา</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">



                                        <div class="form-group">
                                            <asp:Label ID="lbs_grade" runat="server" CssClass="col-sm-2 control-label">ค้นหาชื่อ Job Grade </asp:Label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tb_grade_s" runat="server" CssClass="form-control" placeholder="กรอกชื่อ Job Grade">

                                                </asp:TextBox>


                                            </div>
                                            
                                            <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                           
                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                           
                                        </div>

                                        <div class="form-group">
                                           <div class="col-sm-10 col-sm-offset-2">
                                                <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="Search">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;Search</asp:LinkButton>

                                                <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;Reset
                                                
                                                </asp:LinkButton>
                                            </div>
                                            <%--<label class="col-sm-3 control-label"></label>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <div class="form-group">
                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
            </div>

            <asp:FormView ID="FvInsertEdit" runat="server" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่ม</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <%--<asp:Label ID="lbnameth" runat="server" Text="ชื่อองค์กร (ภาษาไทย)" CssClass="col-sm-3 control-label"></asp:Label>--%>
                                            <asp:Label ID="labelinsert1" runat="server" class="col-sm-3 control-label">ชื่อ Job Grade
                                            <span class="text-danger">*</span></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="tb_JobGrade" runat="server" CssClass="form-control" placeholder="กรอกชื่อ Job Grade" MaxLength="5">

                                                </asp:TextBox>

                                            </div>
                                            <div class="col-sm-3">
                                                <asp:RequiredFieldValidator ID="RequiredFieldtb_JobGrade" runat="server" ControlToValidate="tb_JobGrade" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                                </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_JobGrade" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_JobGrade" Width="220" />



                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                                </asp:LinkButton>
                                            </div>
                                            <label class="col-sm-3 control-label"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <%--Edit--%>

                    <div class="panel panel-info">
                        <div class="panel-heading">

                            <h3 class="panel-title">แก้ไข</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="labeledit1" runat="server" class="col-sm-3 control-label">ชื่อ Job Grade<span class="text-danger"> *</span></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="tb_JobGrade" runat="server" CssClass="form-control" Text='<%# Eval("jobgrade_name") %>' placeholder="กรอกชื่อ Job Grade" MaxLength="5"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:RequiredFieldValidator ID="RequiredFieldtb_JobGrade" runat="server" ControlToValidate="tb_JobGrade" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" Display="None" SetFocusOnError="true">
                                            </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_JobGrade" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_JobGrade" Width="220" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ControlToValidate="tbNameTH"
                                            ValidationExpression="^[a-zA-Z0-9'@&#.,)(\s]{1,250}$"></asp:RegularExpressionValidator>--%>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="labeledit3" runat="server" class="col-sm-3 control-label" Text="สถานะ"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("jobgrade_status") %>'>
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>





                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-6">
                                            <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="editSave" CommandArgument='<%# Eval("jobgrade_idx") %>'>
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" CausesValidation="false">
                                                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel</asp:LinkButton>
                                        </div>
                                        <label class="col-sm-3 control-label"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvJobgrade" runat="server" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="Master_PageIndexChanging">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>

                    <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">

                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="gvtextjame" runat="server" Visible="false" Text='<%# Eval("jobgrade_idx") %>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Job Grade" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("jobgrade_name") %>'>

                                </asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="Label3" runat="server" Text='<%# convertStatus((int)Eval("jobgrade_status")) %>'
                                    CssClass='<%# convertCss((int)Eval("jobgrade_status")) %>'>
                                

                                </asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="Edit" CommandArgument='<%#Eval("jobgrade_idx") %>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบ Job Grade นี้ใช่หรือไม่ ?')" CommandArgument='<%#Eval("jobgrade_idx") %>' title="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>



                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>
</asp:Content>

