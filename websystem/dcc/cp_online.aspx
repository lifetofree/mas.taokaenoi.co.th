<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cp_online.aspx.cs" Inherits="websystem_dcc_cp_online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <!--tab menu-->
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> แจ้งเรื่อง</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbListNew" runat="server" CommandName="cmdListNew" OnCommand="navCommand" CommandArgument="docListNew"> รายการที่ถูกแจ้งใหม่</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbListDoing" runat="server" CommandName="cmdListDoing" OnCommand="navCommand" CommandArgument="docListDoing">
								รายการที่กำลังดำเนินการ</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbListDone" runat="server" CommandName="cmdListDone" OnCommand="navCommand" CommandArgument="docListDone"> รายการที่ดำเนินการเรียบร้อย</asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--tab menu-->

        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="docCreate" runat="server">
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_en") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_en") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>
                </asp:FormView>
                <asp:HyperLink runat="server" ID="txtfocus_fvActor1Node1" />
                <asp:FormView ID="fvActor1Node1" runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <!--requestor / fill form-->
                        <asp:HiddenField ID="hfNodeIDX" runat="server" Value="1" />
                        <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value="0" />
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทเอกสาร</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlDocType" Font-Size="11"
                                                ErrorMessage="เลือกประเภทเอกสาร"
                                                ValidationExpression="เลือกประเภทเอกสาร" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExten3der3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDocSubType" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ผู้แก้ไขปัญหา</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldV2alidator12" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlOrg" Font-Size="11"
                                                ErrorMessage="เลือกองค์กร"
                                                ValidationExpression="เลือกองค์กร" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldV2alidator12" Width="160" />
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RequiredFie2ldValidator1" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlDept" Font-Size="11"
                                                ErrorMessage="เลือกฝ่าย"
                                                ValidationExpression="เลือกฝ่าย" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFie2ldValidator1" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlSec" runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValid4ator1" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="ddlSec" Font-Size="11"
                                                ErrorMessage="เลือกแผนก"
                                                ValidationExpression="เลือกแผนก" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValid4ator1" Width="160" />
                                        </div>
                                        <label class="col-sm-5 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">หัวข้อปัญหา</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbDocTitle" runat="server" CssClass="form-control" ValidationGroup="Create_Actor1" />
                                            <asp:RequiredFieldValidator ID="RqDocCwreate" ValidationGroup="Create_Actor1" runat="server" Display="None"
                                                ControlToValidate="tbDocTitle" Font-Size="11"
                                                ErrorMessage="หัวข้อปัญหา" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายละเอียดปัญหา</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbDocDetail" runat="server" TextMode="MultiLine" CssClass="tinymce" />
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เอกสารแนบ</label>
                                                <div class="col-sm-10">
                                                    <asp:FileUpload ID="fuAttachFile" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSave" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" ValidationGroup="Create_Actor1" OnCommand="btnCommand" CommandName="cmdDocSave"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfActorIDX" runat="server" Value='<%# Eval("current_actor") %>' />
                        <asp:HiddenField ID="hfNodeIDX" runat="server" Value='<%# Eval("current_node") %>' />

                        <asp:HiddenField ID="hfU0IDX" runat="server" Value='<%# Eval("uidx") %>' />
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทเอกสาร</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDocSubType" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ผู้แก้ไขปัญหา</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlSec" runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-5 control-label"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">หัวข้อปัญหา</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbDocTitle" runat="server" CssClass="form-control" Text='<%# Eval("doc_title") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายละเอียดปัญหา</label>
                                        <div class="col-sm-10 control-label textleft">
                                            <asp:TextBox ID="tbDocDetail" runat="server" Text='<%# Eval("doc_detail") %>' Visible="false" />
                                            <asp:Literal ID="litDocDetail" runat="server" Text='<%# HttpUtility.HtmlDecode((string)Eval("doc_detail")) %>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เอกสารแนบ</label>
                                        <div class="col-sm-10">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">co-share</label>
                                        <div class="col-sm-10 control-label textleft">
                                            <span><%# Eval("coshare_list_name") %></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" Text="Back" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
                <asp:FormView ID="fvActor1" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณาผล โดย ผู้แจ้งปัญหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">พิจารณาผล</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlActor1Approve" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="tbActor1Comment" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <asp:FormView ID="fvActor2" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณาผล โดย QMR</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">พิจารณาผล</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlActor2Approve" runat="server" CssClass="form-control" ValidationGroup="Create_Actor2" />
                                            <asp:RequiredFieldValidator ID="Required4FieldV2alidator12" ValidationGroup="Create_Actor2" runat="server" Display="None"
                                                ControlToValidate="ddlActor2Approve" Font-Size="11"
                                                ErrorMessage="เลือกผลการพิจารณา"
                                                ValidationExpression="เลือกผลการพิจารณา" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required4FieldV2alidator12" Width="160" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="tbActor2Comment" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>

                                    <div id="div_coshare" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">co-share</label>
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlOrg_coshare" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="Create_Actor2_coshare" />
                                            </div>
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlDept_coshare" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="Create_Actor2_coshare" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-5">
                                                <asp:DropDownList ID="ddlSec_coshare" runat="server" CssClass="form-control" ValidationGroup="Create_Actor2_coshare" />
                                                <asp:RequiredFieldValidator ID="Required6FieldValidator1" ValidationGroup="Create_Actor2_coshare" runat="server" Display="None"
                                                    ControlToValidate="ddlSec_coshare" Font-Size="11"
                                                    ErrorMessage="เลือกแผนก"
                                                    ValidationExpression="เลือกแผนก" InitialValue="-1" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required6FieldValidator1" Width="160" />
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="btnAddSection" runat="server" CssClass="btn btn-success" data-original-title="Save" ValidationGroup="Create_Actor2_coshare" data-toggle="tooltip" OnCommand="btnCommand" CommandName="AddCoshare">ADD + </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <asp:GridView ID="GvCoshare"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover"
                                                    HeaderStyle-CssClass="table_headCenter"
                                                    HeaderStyle-Height="40px"
                                                    ShowFooter="False"
                                                    DataKeyNames="RSecIDX"
                                                    ShowHeaderWhenEmpty="True"
                                                    AllowPaging="True"
                                                    PageSize="5"
                                                    BorderStyle="None"
                                                    CellSpacing="2">
                                                    <PagerStyle CssClass="PageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                    <asp:Label ID="lbl_RSecIDX" runat="server" Visible="false" Text='<%# Eval("RSecIDX")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อฝ่าย">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbl_deptname" runat="server" Text='<%# Eval("DeptNameEN")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อแผนก">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbl_secname" runat="server" Text='<%# Eval("SecNameEN")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ลบ">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:LinkButton ID="btn_Delete" runat="server" Text="Delete" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmd_delete_coshare" CommandArgument='<%# Eval("RSecIDX") %>' OnClientClick="return confirm('Do you want delete this item?')" />
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" ValidationGroup="Create_Actor2"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <asp:FormView ID="fvActor3" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณาผล โดย ผู้อนุมัติการแก้ไขปัญหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">พิจารณาผล</label>
                                        <div class="col-sm-5">
                                            <asp:DropDownList ID="ddlActor3Approve" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="tbActor3Comment" runat="server" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-5"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <asp:FormView ID="fvActor4" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ดำเนินการแก้ไข โดย ผู้แก้ไขปัญหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">สาเหตุของปัญหา</label>
                                        <label class="col-sm-10"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Man</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment1" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Machine</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment2" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Material</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment3" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Method</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment4" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Money</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment5" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Environment</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment6" runat="server" CssClass="form-control" Text="-" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">วิธีการและผลที่ได้จากการแก้ไขปัญหา</label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="tbComment7" runat="server" TextMode="MultiLine" CssClass="tinymce" Text=" - " />
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="upActor4Node4Files" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เอกสารแนบ</label>
                                                <div class="col-sm-10">
                                                    <asp:FileUpload ID="fuAttachFileActor4" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="control-label multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSave" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <asp:Repeater ID="rptProcessDetail" runat="server"
                    OnItemDataBound="rptItemDataBound">
                    <HeaderTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                            </div>
                            <div class="panel-body">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfProcessActorIDX" runat="server" Value='<%# Eval("actor_idx") %>' />
                        <asp:HiddenField ID="hfProcessFromNode" runat="server" Value='<%# Eval("from_node") %>' />
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <span>ดำเนินการโดย</span>
                                </div>
                                <div class="col-sm-4 control-label textleft">
                                    <span><%# Eval("actor_name") %></span>
                                </div>
                                <div class="col-sm-2 control-label">
                                    <span>ผลการดำเนินการ</span>
                                </div>
                                <div class="col-sm-4 control-label textleft">
                                    <span><%# Eval("decision_name") %></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <span>โดย</span>
                                </div>
                                <div class="col-sm-4 control-label textleft">
                                    <span><%# Eval("emp_name_th") %></span>
                                </div>
                                <div class="col-sm-2 control-label">
                                    <span>วันที่</span>
                                </div>
                                <div class="col-sm-4 control-label textleft">
                                    <span><%# Eval("create_date") %></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <span>comment</span>
                                </div>
                                <div class="col-sm-10 control-label textleft">
                                    <asp:Label ID="lblComment" runat="server" Text='<%# Eval("comment") %>' />
                                    <asp:FormView ID="fv5M1E" runat="server" Width="100%"
                                        OnDataBound="fvDataBound">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Man</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("M1") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Machine</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("M2") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Material</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("M3") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Method</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("M4") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Money</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("M5") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Environment</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# Eval("E1") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">วิธีการและผลที่ได้จากการแก้ไขปัญหา</label>
                                                <div class="col-sm-10 control-label textleft">
                                                    <span><%# HttpUtility.HtmlDecode((string)Eval("S1")) %></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>
                                </div>
                            </div>
                        </div>
                        <asp:Label ID="fsaa" runat="server"></asp:Label>
                        <%--<div id="BoxgvFileList" runat="server">
                            <asp:GridView ID="gvFileList" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                AllowPaging="True" PageSize="10"
                                OnRowDataBound="Master_RowDataBound">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                        <ItemTemplate>
                                            <div class="col-lg-10">
                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>--%>
                        <hr />
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
				
                    </FooterTemplate>
                </asp:Repeater>
                <div id="BoxgvFileList" runat="server">
                    <asp:Label ID="sf" runat="server"></asp:Label>
                    <asp:GridView ID="gvFileList" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        AllowPaging="True"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        BorderStyle="None"
                        CellSpacing="2"
                        Font-Size="Small">

                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                <ItemTemplate>
                                    <div class="col-lg-10">
                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
            <asp:View ID="docListNew" runat="server">
                new
            </asp:View>
            <asp:View ID="docListDoing" runat="server">
                <asp:GridView ID="gvDoingList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                    DataKeyNames="uidx">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่ร้องเรียน" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# getOnlyDate((string)Eval("create_date")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายการ" HeaderStyle-Width="33%">
                            <ItemTemplate>
                                <strong>เรื่อง :</strong>&nbsp;<asp:Label ID="lblDocTitle" runat="server" Text='<%# Eval("doc_title") %>'></asp:Label><br />
                                <small>
                                    <strong>ประเภทเอกสาร :</strong>&nbsp;<asp:Label ID="lblDocType" runat="server" Text='<%# Eval("doc_type_name") + " - " + Eval("doc_sub_type_name") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ฝ่ายที่ถูกแจ้ง" HeaderStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Label ID="lblResDeptName" runat="server" Text='<%# Eval("res_rdept_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ผู้แจ้งปัญหา" HeaderStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Label ID="lblReqName" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะเอกสาร" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " โดย " + Eval("current_actor_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnView" CssClass="btn-sm btn-info" runat="server" CommandName="cmdDocDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("uidx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
            <asp:View ID="docListDone" runat="server">
                <asp:GridView ID="gvDoneList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging" AllowPaging="True" PageSize="10"
                    DataKeyNames="uidx">
                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocCode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่ร้องเรียน" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# getOnlyDate((string)Eval("create_date")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายการ" HeaderStyle-Width="33%">
                            <ItemTemplate>
                                <strong>เรื่อง :</strong>&nbsp;<asp:Label ID="lblDocTitle" runat="server" Text='<%# Eval("doc_title") %>'></asp:Label><br />
                                <small>
                                    <strong>ประเภทเอกสาร :</strong>&nbsp;<asp:Label ID="lblDocType" runat="server" Text='<%# Eval("doc_type_name") + " - " + Eval("doc_sub_type_name") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ฝ่ายที่ถูกแจ้ง" HeaderStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Label ID="lblResDeptName" runat="server" Text='<%# Eval("res_rdept_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ผู้แจ้งปัญหา" HeaderStyle-Width="12%">
                            <ItemTemplate>
                                <asp:Label ID="lblReqName" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะเอกสาร" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " โดย " + Eval("current_actor_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnView" CssClass="btn-sm btn-info" runat="server" CommandName="cmdDocDetail" OnCommand="btnCommand" CommandArgument='<%# Eval("uidx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:View>
        </asp:MultiView>
        <!--multiview-->
    </div>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>
</asp:Content>
