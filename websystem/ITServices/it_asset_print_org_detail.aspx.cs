﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Data;
using System.Web.UI.HtmlControls;


public partial class websystem_it_asset_print_org_detail : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    function_dmu _func_dmu = new function_dmu();
    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    data_itasset _dtitseet = new data_itasset();

    string _localJson = "";
    int _tempInt = 0;
    int _tempcounter = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //its_u_document
    static string _urlGetits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_document"];

    protected void Page_Load(object sender, EventArgs e)
    {
        string url = Request.QueryString["url"];
        //if (Session["_SESSION_U0DOCIDX"].ToString() != null)
        //{
        //    ViewState["_print_u0_docket_idx"] = Session["_SESSION_U0DOCIDX"].ToString();
        //    ViewState["_print_org_idx"] = Session["_SESSION_ORGIDX"].ToString();

        //}
        //else if (Session["_SESSION_U0DOCIDX"].ToString() == null)
        //{
        //    Response.AddHeader("REFRESH", "3;URL=" + ResolveUrl("~/login?url=" + url));
        //}
        ViewState["_print_u0_docket_idx"] = 7;
        ViewState["_print_org_idx"] = 1;

        PRINT_SAMPLECODE();
        _GETPRINTDATE();
       
    }

    private void PRINT_SAMPLECODE()
    {
       // litDebug.Text = ViewState["_PRINTSAMPLECODE"].ToString();
        int u0_docket_idx = int.Parse(ViewState["_print_u0_docket_idx"].ToString());
        int org_idx = int.Parse(ViewState["_print_org_idx"].ToString());

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.org_idx_its = org_idx;
        select_its.operation_status_id = "docket_u0_org";
        _dtitseet.its_u_document_action[0] = select_its;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        if (_dtitseet.its_u_document_action != null)
        {
            var item = _dtitseet.its_u_document_action[0];
            lb_org_name_th.Text = item.org_name_th;
        }


        _dtitseet.its_u_document_action = new its_u_document[1];
        select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.org_idx_its = org_idx;
        select_its.operation_status_id = "docket_u1_list_print";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        
        if (_dtitseet.its_u_document_action != null)
        {
            var item = _dtitseet.its_u_document_action[0];
            lb_round_month.Text = zsetMonthYear(item.zmonth, item.zyear);
        }
        //ViewState["docket_u1_list"] = _dtitseet.its_u_document_action;
        //_func_dmu.zSetGridData(gvSample, ViewState["docket_u1_list"]);
        DataTable dt = new DataTable();
        dt.Columns.Add("zdevices_name", typeof(string));
        dt.Columns.Add("qty", typeof(Int32));
        dt.Columns.Add("NameTH", typeof(string));
        dt.Columns.Add("spec_remark", typeof(string));
       // DataRow dtrow = dt.NewRow();
        if (_dtitseet.its_u_document_action != null)
        {
            foreach(var item in _dtitseet.its_u_document_action)
            {
                DataRow dtrow = dt.NewRow();
                dtrow["zdevices_name"] = item.zdevices_name;
                dtrow["qty"] = item.qty;
                dtrow["NameTH"] = item.NameTH;
                dtrow["spec_remark"] = item.spec_name;
                dt.Rows.Add(dtrow);
                dtrow = dt.NewRow();
            }
        }
        gvlist_org.DataSource = dt;
        gvlist_org.DataBind();
        tpdf(1);

    }

    public string zsetMonthYear(int Month, int Year)
    {
        return _func_dmu.zMonthTH(Month) + " " + Year.ToString();
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvSample":

                if (e.Row.RowType == DataControlRowType.Header)
                {


                }
                
                break;
        }

    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }


    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void _GETPRINTDATE()
    {
        //lblPrintDate.Text = DateTime.Now.ToString();

        lblPrintDate.Text = DateTime.Now.AddMinutes(55).ToString("dd'/'MM'/'yyyy HH:mm:ss");
    }

    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }
    private void tpdf(int id)
    {
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition", "attachment;filename=Student" + id.ToString() + ".pdf");
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        //gvlist_org.AllowPaging = false;
        //HtmlForm frm = new HtmlForm();
        //gvlist_org.Parent.Controls.Add(frm);
        //frm.Attributes["runat"] = "server";
        //frm.Controls.Add(gvlist_org);
        //frm.RenderControl(hw);
        //gvlist_org.DataBind();
        //StringReader sr = new StringReader(sw.ToString());
        //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //htmlparser.Parse(sr);
        //pdfDoc.Close();
        //Response.Write(pdfDoc);
        //Response.End();
    }

}