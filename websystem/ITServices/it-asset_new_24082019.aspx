﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it-asset_new_24082019.aspx.cs" Inherits="websystem_ITServices_it_asset_new_24082019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:Panel ID="Panel1" runat="server" Visible="false">

        <div class="row">
            <div class="col-md-2">
                <asp:Label ID="Label73" runat="server" Text="send e-mail"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txt_settemplate_asset_create" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnsettemplate_asset_create" runat="server" Text="ok" OnClick="btnsettemplate_asset_create_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <asp:Label ID="Label74" runat="server" Text="flow item"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txt_flow_item" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-2">
            </div>
        </div>

    </asp:Panel>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToDivIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>

                    <li id="_divMenuLiToDivBuyNew" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivBuyNew" runat="server"
                            CommandName="_divMenuBtnToDivBuyNew"
                            OnCommand="btnCommand" Text="ขอซื้อ" />
                    </li>
                    <li id="_divMenuLiToDivDevices" runat="server" visible="false">
                        <asp:LinkButton ID="_divMenuBtnToDivCutDevices" runat="server"
                            CommandName="_divMenuBtnToDivCutDevices"
                            OnCommand="btnCommand" Text="ตัดชำรุดอุปกรณ์" />
                    </li>
                    <li id="_divMenuLiToDivChangeOwn" runat="server" visible="false">
                        <asp:LinkButton ID="_divMenuBtnToDivChangeOwn" runat="server"
                            CommandName="_divMenuBtnToDivChangeOwn"
                            OnCommand="btnCommand" Text="โอนย้ายอุปกรณ์" />
                    </li>
                    <li id="_divMenuLiToDivWaitApprove" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <asp:Label ID="_lbMenuLiToDivWaitApprove" runat="server" Text="รายการรออนุมัติ"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToDivWaitApprove_Buy" runat="server">
                                <asp:LinkButton ID="BtnToDivWaitApprove_Buy" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_Buy"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติซื้ออุปกรณ์" />
                            </li>
                            <li role="separator" class="divider" runat="server" visible="false"
                                id="_liMenuLiToDivWaitApprove_CutDevices"></li>
                            <li id="_divMenuLiToDivWaitApprove_CutDevices" runat="server" visible="false">
                                <asp:LinkButton ID="BtnToDivWaitApprove_CutDevices" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_CutDevices"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติตัดเสียอุปกรณ์" />
                            </li>
                            <li role="separator" class="divider" runat="server" visible="false"
                                id="_liMenuLiToDivWaitApprove_ChangeOwn"></li>
                            <li id="_divMenuLiToDivWaitApprove_ChangeOwn" runat="server" visible="false">
                                <asp:LinkButton ID="BtnToDivWaitApprove_ChangeOwn" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_ChangeOwn"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติโอนย้ายอุปกรณ์" />
                            </li>
                        </ul>
                    </li>

                    <%--                    
                        <li id="_divMenuLiToDivBudget" runat="server">
                        <asp:LinkButton ID="BtnToDivBudget" runat="server"
                            CommandName="_divMenuBtnToDivBudget"
                            OnCommand="btnCommand" Text="Budget" />
                        </li>
                    --%>

                    <li id="_divMenuLiToDivBudget" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                            <asp:Label ID="_lbMenuLiToDivBudget" runat="server" Text="Budget"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_divMenuLiToDivMasterBudget" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivBudget_master" runat="server"
                                    CommandName="_divMenuBtnToDivBudget_master"
                                    OnCommand="btnCommand" Text="Import Data IO" />
                            </li>

                            <li role="separator" id="_divMenuLiToDivBudget_system_separator" runat="server" class="divider"></li>
                            <li id="_divMenuLiToDivBudget_system" runat="server">
                                <asp:LinkButton ID="BtnToDivBudget" runat="server"
                                    CommandName="_divMenuBtnToDivBudget"
                                    OnCommand="btnCommand" Text="อนุมัติรายการขอซื้อ" />
                            </li>
                        </ul>
                    </li>


                    <li id="_divMenuLiToDivAsset" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <asp:Label ID="_lbMenuLiToDivAsset" runat="server" Text="Asset"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_divMenuLiToDivAsset_master" runat="server">
                                <asp:LinkButton ID="BtnToDivAsset_master" runat="server"
                                    CommandName="_divMenuBtnToDivAsset_master"
                                    OnCommand="btnCommand" Text="Master Data Class" />
                            </li>



                            <li role="separator" id="_divMenuLiToDivAsset_system_separator" runat="server" class="divider"></li>
                            <li id="_divMenuLiToDivAsset_system" runat="server">
                                <asp:LinkButton ID="BtnToDivAsset_system" runat="server"
                                    CommandName="_divMenuBtnToDivAsset_system"
                                    OnCommand="btnCommand" Text="ตรวจสอบข้อมูลขอซื้อเพื่อออกเลขทรัพย์สิน" />
                            </li>

                            <li role="separator" id="_liMenuLiToasset_dir1_separator" runat="server" class="divider"></li>
                            <li id="_liMenuLiToasset_dir1" runat="server">
                                <asp:LinkButton ID="btnMenuLiToasset_dir1" runat="server"
                                    CommandName="_divMenuLiToasset_dir1"
                                    OnCommand="btnCommand" Text="อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย Asset" />
                            </li>

                            <li role="separator" id="_divMenuLiToDivDataGiveAsset_separator" runat="server" class="divider"></li>
                            <li id="_divMenuLiToDivDataGiveAsset" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivDataGiveAsset" runat="server"
                                    CommandName="_divMenuBtnToDivDataGiveAsset"
                                    OnCommand="btnCommand" Text="ข้อมูลของบประมาณ" />
                            </li>

                            <li role="separator" id="_divMenuLiToDivDataBuyAsset_separator" runat="server" class="divider"></li>
                            <li id="_divMenuLiToDivDataBuyAsset" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivDataAsset" runat="server"
                                    CommandName="_divMenuBtnToDivDataAsset"
                                    OnCommand="btnCommand" Text="Import Data Asset" />
                            </li>
                        </ul>
                    </li>


                    <li id="_divMenuLiIT" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true"
                            aria-expanded="false">
                            <asp:Label ID="_lbMenuLiIT" runat="server" Text="IT"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_liIT_list1" runat="server">
                                <asp:LinkButton ID="_btnIT" runat="server"
                                    CommandName="_divMenubtnIT"
                                    OnCommand="btnCommand" Text="ข้อมูลขอซื้อ" />
                            </li>

                            <li role="separator" id="_liIT_list1_update_separator" runat="server" class="divider"></li>

                            <li id="_liIT_list1_update" runat="server">

                                <asp:LinkButton ID="_btnIT_update" runat="server"
                                    CommandName="_divMenubtnIT_update"
                                    OnCommand="btnCommand" Text="แก้ไขข้อมูลขอซื้อ" />
                            </li>

                            <li role="separator" id="_liIT_manager_separator" runat="server" class="divider"></li>

                            <li id="_liIT_manager" runat="server">
                                <asp:LinkButton ID="_divMenubtnIT_manager" runat="server"
                                    CommandName="_divMenubtnIT_manager"
                                    OnCommand="btnCommand" Text="อนุมัติรายการขอซื้อโดยผู้จัดการฝ่าย" />
                            </li>

                            <li role="separator" id="_liIT_director_separator" runat="server" class="divider"></li>

                            <li id="_liIT_director" runat="server">
                                <asp:LinkButton ID="_divMenubtnIT_director" runat="server"
                                    CommandName="_divMenubtnIT_director"
                                    OnCommand="btnCommand" Text="อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย" />

                            </li>

                            <li role="separator" id="_liIT_summary_separator" runat="server" class="divider"></li>

                            <li id="_liIT_summary" runat="server">
                                <asp:LinkButton ID="_divMenubtnIT_summary" runat="server"
                                    CommandName="_divMenubtnIT_summary"
                                    OnCommand="btnCommand" Text="สรุปรายการขอซื้อ" />
                            </li>

                            <li role="separator" id="_liIT_print_separator" runat="server" class="divider"></li>

                            <li id="_liIT_print" runat="server">
                                <asp:LinkButton ID="_divMenubtnIT_print" runat="server"
                                    CommandName="_divMenubtnIT_print"
                                    OnCommand="btnCommand" Text="รายงานรายการขอซื้อ" />
                            </li>

                            <li role="separator" id="_liIT_deliver_separator" runat="server" class="divider"></li>

                            <li id="_liIT_deliver" runat="server">
                                <asp:LinkButton ID="_divMenubtnIT_deliver" runat="server"
                                    CommandName="_divMenubtnIT_deliver"
                                    OnCommand="btnCommand" Text="ส่งมอบเครื่องให้กับ User" />
                            </li>

                        </ul>
                    </li>

                    <li id="_divMenuLiMD" runat="server" class="dropdown">
                        <%-- <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true"
                            aria-expanded="false">MD <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_liMD_it" runat="server">--%>
                        <asp:LinkButton ID="_divMenubtnMD_it" runat="server"
                            CommandName="_divMenubtnMD_it"
                            OnCommand="btnCommand" Text="รายการรออนุมัติซื้ออุปกรณ์" />
                        <%--</li>

                        </ul>--%>
                    </li>

                    <li id="_divMenuLipurchase" runat="server" class="dropdown">

                        <asp:LinkButton ID="_divMenubtnpurchase" runat="server"
                            CommandName="_divMenubtnpurchase"
                            OnCommand="btnCommand" Text="จัดซื้อ" />

                    </li>

                </ul>
                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1KUftIg57xPtx-v0uOx419WOBH0ZRsoba8X2--ook_fw/edit" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">

            <div class="col-sm-12" runat="server" id="content_Homepage">
                <div class="form-group">

                    <asp:LinkButton CssClass="btn btn-primary"
                        ID="btnshowboxcreate" data-toggle="tooltip"
                        title="สร้างรายการขอซื้ออุปกรณ์" runat="server"
                        CommandName="showboxcreate"
                        Visible="false"
                        CommandArgument="0" OnCommand="btnCommand"><i class="glyphicon glyphicon-file"></i> สร้างรายการขอซื้ออุปกรณ์</asp:LinkButton>

                    &nbsp;
            <asp:LinkButton CssClass="btn btn-info btn-group-vertical"
                ID="btnshowBoxsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                CommandName="showBoxsearch"
                OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหารายการ</asp:LinkButton>

                    <asp:LinkButton CssClass="btn btn-danger btn-group-vertical"
                        ID="btnhiddenBoxsearch" data-toggle="tooltip" title="ยกเลิกค้นหารายการ"
                        runat="server"
                        CommandName="hiddenBoxsearch" Visible="false"
                        OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ยกเลิกค้นหารายการ</asp:LinkButton>



                    <asp:LinkButton CssClass="btn btn-defult pull-right" ID="btnviewdataofmountly" data-toggle="tooltip"
                        title="แสดงข้อมูลแบบรายเดือน" runat="server"
                        CommandName="viewpurchasemountly" Visible="false"
                        OnCommand="btnCommand"><i class="glyphicon glyphicon-calendar"></i> แสดงข้อมูลแบบรายเดือน</asp:LinkButton>
                    <asp:LinkButton CssClass="btn btn-default pull-right"
                        ID="btnviewall" data-toggle="tooltip" title="แสดงข้อมูลทั้งหมด"
                        runat="server"
                        CommandName="viewall" Visible="false"
                        OnCommand="btnCommand">แสดงข้อมูลทั้งหมด</asp:LinkButton>

                </div>

                <%-- เลือกดูข้อมูลแบบเป็นเดือน--%>
                <div class="form-group">
                    <asp:Panel ID="panel_mountly" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-calendar"></i>&nbsp;
                        <b>Select Mountly</b> (เลือกเดือนเพื่อทำการอนุมัติรายการขอซื้อในแต่ละเดือน)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>เลือกเดือน (select mountly)</label>
                                            <asp:UpdatePanel ID="UpdatePanel_mountly" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_mountly"
                                                        runat="server" CssClass="form-control fa-align-left"
                                                        AutoPostBack="true" Enabled="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>เลือกปี (select yearly)</label>
                                            <asp:UpdatePanel ID="UpdatePanel_yearly" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddl_yearly"
                                                        runat="server" CssClass="form-control fa-align-left"
                                                        AutoPostBack="true" Enabled="true">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>&nbsp;</label>
                                            <asp:UpdatePanel ID="UpdatePanel_submit" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton CssClass="btn btn-primary" ID="btnsubmitview" data-toggle="tooltip" title="แสดงผลลัพธ์" runat="server"
                                                        CommandName="submit_mountly" OnCommand="btnCommand"><i class="glyphicon glyphicon-file"></i> แสดงผลลัพธ์</asp:LinkButton>&nbsp;
                                                <asp:LinkButton CssClass="btn btn-danger" ID="btncancelview" data-toggle="tooltip" title="ยกเลิก" runat="server"
                                                    CommandName="hiddenBoxsearch" OnCommand="btnCommand"><i class="glyphicon glyphicon-share-alt"></i> เลิกทำ</asp:LinkButton>&nbsp;
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <%--  ค้นหา--%>
                <div class="row">
                    <asp:Panel ID="panel_search" runat="server" Visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-search"></i>&nbsp;
                        <b>search</b> (ค้นหารายการ)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <%-- เงื่อนไขการค้นหา--%>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา : " />
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlcondition" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                <asp:ListItem Text="เลือกเงื่อนไข...." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtstartdate" runat="server" placeholder="จากวันที่..."
                                                    CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtenddate" runat="server" Enabled="false" placeholder="ถึงวันที่..."
                                                    CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdocumentcode" runat="server" placeholder="รหัสเอกสาร"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label18" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" runat="server" MaxLength="8" placeholder="รหัสพนักงาน"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ-นามสกุล : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtfirstname_lastname" runat="server" placeholder="ชื่อ-นามสกุล"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label20" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_organization"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_department"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label22" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocation_search"
                                                runat="server" CssClass="form-control fa-align-left"
                                                Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="Cost Center : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_costcenter"
                                                runat="server" CssClass="form-control fa-align-left selectpicker"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label24" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการขอซื้อ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_typepurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_typequipment"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <asp:Label ID="Label38" CssClass="col-sm-2 control-label" runat="server" Text="สถานะรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsearch_status"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <%--   <div class="col-sm-2"></div>--%>
                                        <div class="col-lg-2 col-lg-offset-2">
                                            <asp:LinkButton CssClass="btn btn-info btn-sm" ID="btnsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                CommandName="searching" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                            <%--    </div>
                                        <div class="col-sm-1">--%>
                                            <asp:LinkButton CssClass="btn btn-default btn-sm" ID="btnresetsearch" data-toggle="tooltip" title="รีเซ็ต" runat="server"
                                                CommandName="reset_search" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-refresh"></i> รีเซ็ต</asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-- <div class="row">
                                    <h5>&nbsp;</h5>
                                      
                                </div>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink>
                <asp:Panel ID="Panel_list_purchase" runat="server" Visible="false">


                    <div class="panel panel-default">
                        <!-- Add Panel Heading Here -->
                        <div class="panel-body">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;รายการขอซื้อทั้งหมด
                           
                        </div>
                    </div>

                </asp:Panel>

                <%--  select all อนุมัติทั้งหมด --%>
                <div class="form-horizontal" role="form">
                    <asp:Panel ID="Panel_approve" runat="server" Visible="false">

                        <div class="panel panel-default">
                            <!-- Add Panel Heading Here -->
                            <div class="panel-body">

                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label79" class="col-md-2 control-label"
                                            runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:CheckBox ID="check_approve_all" runat="server" AutoPostBack="true"
                                                RepeatDirection="Vertical" CssClass="textleft"
                                                OnCheckedChanged="checkindexchange" />&nbsp;
                                          <strong>เลือกทั้งหมด</strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label"
                                            runat="server" Text="หมายเหตุ : "
                                            Font-Bold="true" />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark_approve_all" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Requiredtxtremark_approve_all"
                                                ValidationGroup="Saveapprove_allList" runat="server" Display="None"
                                                ControlToValidate="txtremark_approve_all" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Requiredtxtremark_approve_all" Width="160" />

                                            <asp:RegularExpressionValidator ID="RegularExprestxtremark_approve_all" runat="server"
                                                ValidationGroup="Saveapprove_allList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_approve_all"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtremark_approve_all" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RegularExprestxtremark_approve_all" Width="160" />

                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label80" class="col-md-2 control-label" runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="btnapproveall"
                                                data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                                CommandName="confirm_approve" Visible="true"
                                                ValidationGroup="Saveapprove_allList"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand">อนุมัติรายการ</asp:LinkButton>
                                            <asp:LinkButton CssClass="btn btn-warning btn-sm" ID="btnnotapprove"
                                                data-toggle="tooltip" title="กลับไปแก้ไข" runat="server"
                                                CommandName="confirm_no_approve" Visible="true"
                                                ValidationGroup="Saveapprove_allList"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand">กลับไปแก้ไข</asp:LinkButton>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </asp:Panel>
                </div>

                <%--   gridview รายการขอซื้อ--%>
                <asp:GridView ID="Gvits_u0_document_list" Visible="true" runat="server" AutoGenerateColumns="false" DataKeyNames="u0idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%"
                            ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                            Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lb_flow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                <asp:CheckBox ID="cbrecipients" runat="server"
                                    AutoPostBack="true" Visible="true"
                                    OnCheckedChanged="checkindexchange"
                                    Text='<%# Container.DataItemIndex %>'
                                    Style="color: transparent;"></asp:CheckBox>
                                <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />
                                <asp:Label ID="lb_Selected" runat="server" Text="" Visible="false" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_u0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                <asp:Label ID="idx" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>
                                <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                    <p></b> &nbsp;<%# Eval("doccode") %></p>
                                </asp:Label>
                                <asp:Label ID="lbdoccode" runat="server" Visible="false" Text='<%# Eval("doccode") %>' />

                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>
                                <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12">
                    <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emp_code") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emp_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_th") %></p> 
                                </asp:Label>
                                <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_th") %></p>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12 control-label">
                    <p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("zdocdate") %></p>
                                </asp:Label>

                                <asp:Label ID="Label51" runat="server" CssClass="col-sm-12 control-label">
                    <p><b>สถานที่:</b> &nbsp;<%# Eval("place_name") %></p>
                                </asp:Label>

                                <%--                                <asp:Label ID="lb_name_purchase_type" runat="server" CssClass="col-sm-12 control-label">
                    <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase_type") %></p>
                                </asp:Label>--%>
                                <asp:Label ID="lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />

                                <asp:Label ID="lb_remark" runat="server" CssClass="col-sm-12 control-label">
                    <p><b>รายละเอียด:</b> &nbsp;<%# Eval("remark") %></p>
                                </asp:Label>

                                <asp:GridView ID="gvitems" Visible="true" runat="server" AutoGenerateColumns="false"
                                    CssClass="table table-bordered" GridLines="None"
                                    HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                    OnRowDataBound="gvRowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%" ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                            <ItemTemplate>

                                                <asp:Label ID="Gvu1_u1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                <asp:Label ID="idx2reportrr" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ประเภทการขอซื้อ" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="9">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_name_purchase_type" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("name_purchase_type") %>' />


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="9">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_zdevices_name" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("zdevices_name") %>' />


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="9">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_qty" runat="server" Font-Size="Small" Visible="true"
                                                    Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>' />


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="9">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_price" runat="server" Font-Size="Small"
                                                    Visible="true"
                                                    Text='<%# getformatfloat(((decimal)Eval("price")).ToString(),2) %>' />


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_status_node" runat="server" CssClass="col-sm-12">
                            <span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;<br />
                                <b>โดย</b>&nbsp;<br />   <%# Eval("actor_name") %></span>
                                </asp:Label>

                                <%--  <asp:Label ID="lbmailpurchase" runat="server" Visible="false" Text='<%# Eval("email_purchase") %>' />--%>

                                <asp:Label ID="lb_node_idx" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                <asp:Label ID="lb_actor_idx" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />
                                <asp:Label ID="lb_actor_name" runat="server" Visible="false" Text='<%# Eval("actor_name") %>' />
                                <asp:Label ID="lb_doc_status" runat="server" Visible="false" Text='<%# Eval("doc_status") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" ControlStyle-Font-Size="10" HeaderStyle-Width="20%" HeaderStyle-Font-Size="9" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" Visible="false">
                            <ItemTemplate>


                                <asp:Panel ID="pn_price_comment_complete" runat="server" Visible="true">
                                    <asp:Repeater ID="rptcommentfromit" runat="server">
                                        <ItemTemplate>

                                            <asp:Label ID="lbcommentit" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("commentfromITSupport") %>'></asp:Label>
                                            <br />
                                            <label class="bg-success">:: ราคา :: </label>
                                            <br />
                                            <asp:Label ID="_lbprice" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("price_equipment") %>'></asp:Label>
                                            <br />

                                        </ItemTemplate>

                                    </asp:Repeater>
                                </asp:Panel>


                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:UpdatePanel ID="updatebtnsaveEdit" runat="server">
                                    <ContentTemplate>

                                        <asp:LinkButton ID="btnmanage_its" CssClass="btn btn-info btn-sm" runat="server"
                                            data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                            CommandArgument='<%#
                                            Eval("u0idx")+ "|" + 
                                            Eval("node_idx")+ "|" + 
                                            Eval("emp_idx_its")+ "|" + 
                                            Eval("pr_type_idx") + "|" + 
                                            Eval("actor_idx")  + "|" + 
                                            Eval("doccode") + "|" + 
                                            Eval("doc_status")+ "|" + 
                                            Eval("flow_item")+ "|" + 
                                            Eval("place_idx")+ "|" +
                                            Eval("rdept_idx_its")
                                            %>'
                                            CommandName="manage_its">
                                           <i class="fa fa-file"></i>
                                        </asp:LinkButton>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnmanage_its" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>

        <asp:View ID="ViewBuy" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-md-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-md-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-md-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-md-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_createbuynew" runat="server" visible="false">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-shopping-cart"></i><strong>&nbsp; รายการขอซื้ออุปกรณ์
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertBuyNew" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-md-2 control-label" runat="server" Text="ประเภททรัพย์สิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>
                                    <asp:Panel ID="pnl_other" runat="server" Visible="true">
                                        <div class="form-group">
                                            <asp:Label ID="Label23" class="col-md-2 control-label" runat="server" Text="ประเภทอุปกรณ์ขอซื้อ : " />
                                            <div class="col-md-3">

                                                <asp:DropDownList ID="ddlSearchTypeDevices" AutoPostBack="true"
                                                    runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RqddSearchTypeDevices"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlSearchTypeDevices" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทอุปกรณ์ขอซื้อ"
                                                    ValidationExpression="กรุณาเลือกประเภทอุปกรณ์ขอซื้อ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RqddSearchTypeDevices" Width="160" />


                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label24" class="col-md-2 control-label" runat="server" Text="กลุ่มอุปกรณ์ขอซื้อ : " />
                                            <div class="col-md-3">

                                                <asp:DropDownList ID="ddlSearchDevices" AutoPostBack="true"
                                                    runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ..." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddSearchDevices"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlSearchDevices" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ"
                                                    ValidationExpression="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RqddSearchDevices" Width="160" />

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <asp:Label ID="Label25" class="col-md-2 control-label" runat="server" Text="ประเภทการขอซื้อ : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddlsearch_typepurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddsearch_typepurchase"
                                                ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsearch_typepurchase" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการขอซื้อ"
                                                ValidationExpression="กรุณาเลือกประเภทการขอซื้อ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddsearch_typepurchase" Width="160" />

                                        </div>
                                    </div>

                                    <asp:Panel ID="pnl_ionum" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label27" class="col-md-2 control-label" runat="server" Text="เลขที่ IO : " />
                                            <div class="col-md-3">

                                                <asp:TextBox ID="txtionum"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true"
                                                    OnTextChanged="onTextChanged">
                                                </asp:TextBox>

                                            </div>
                                            <div class="col-md-2">

                                                <p class="help-block">
                                                    <font color="red">
                                                            **ต้องเป็นตัวใหญ่ทั้งหมด
                                                        </font>
                                                </p>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnl_ref_itnum" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label28" class="col-md-2 control-label" runat="server" Text="เลขที่ตัดเสีย : " />
                                            <div class="col-md-3">

                                                <asp:TextBox ID="txtref_itnum"
                                                    runat="server" CssClass="form-control fa-align-left">
                                                </asp:TextBox>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <asp:Label ID="Label29" class="col-md-2 control-label" runat="server" Text="ขอซื้อให้พนักงานตำแหน่ง : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddlposition"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlposition" runat="server" InitialValue="0"
                                                ControlToValidate="ddlposition" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกขอซื้อให้พนักงานตำแหน่ง" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldddlposition" Width="180" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label81" class="col-md-2 control-label" runat="server" Text="ประเภทกลุ่มพนักงาน : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddl_target"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddl_target" runat="server" InitialValue="0"
                                                ControlToValidate="ddl_target" Display="None" SetFocusOnError="true"
                                                ErrorMessage="ประเภทกลุ่มพนักงาน" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Requiredddl_target" Width="180" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label39" class="col-md-2 control-label" runat="server" Text="สเปค : " />
                                        <div class="col-md-3">

                                            <div class="input-group col-md-12 pull-left">
                                                <asp:TextBox ID="ddlspecidx"
                                                    runat="server" CssClass="form-control"
                                                    Enabled="false">
                                                </asp:TextBox>

                                                <div class="input-group-btn">
                                                    <asp:LinkButton ID="btn_spec_search" Visible="false"
                                                        CssClass="btn btn-primary pull-left" runat="server"
                                                        data-original-title="ค้นหา" data-toggle="tooltip" Text='<i class="fa fa-search"></i>'
                                                        OnCommand="btnCommand" CommandName="btn_spec_search">
                                                       
                                                    </asp:LinkButton>
                                                </div>
                                            </div>

                                            <asp:TextBox ID="txt_m1_ref_idx"
                                                runat="server" CssClass="form-control"
                                                Visible="false">
                                            </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlspecidx" runat="server" InitialValue=""
                                                ControlToValidate="ddlspecidx" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกเลือกสเปค" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlspecidx" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldddlspecidx" Width="180" />
                                        </div>

                                        <div class="col-md-2">
                                            <asp:CheckBox ID="check_special" runat="server" AutoPostBack="true"
                                                RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                Text="Special"
                                                OnCheckedChanged="checkindexchange" />
                                        </div>

                                    </div>



                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtlist_buynew" CssClass="form-control"
                                                Enabled="false"
                                                TextMode="MultiLine" Rows="5" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_buynew"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-md-2 control-label" runat="server" Text="ราคาต่อเครื่อง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtprice_buynew"
                                                CssClass="form-control"
                                                runat="server"
                                                TextMode="Number"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtprice_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกราคา" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_buynew"
                                                ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                        </div>
                                        <asp:Label ID="Label30" CssClass="col-md-2 control-label" runat="server" Text="สกุลเงิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlcurrency"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlcurrency" runat="server" InitialValue="0"
                                                ControlToValidate="ddlcurrency" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกสกุลเงิน" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlcurrency" Width="180" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-md-2 control-label" runat="server" Text="จำนวน : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtqty_buynew" CssClass="form-control" runat="server"
                                                TextMode="Number"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtqty_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_buynew"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                        <asp:Label ID="Label31" CssClass="col-md-2 control-label" runat="server" Text="หน่วย : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlunit"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlunit" runat="server" InitialValue="0"
                                                ControlToValidate="ddlunit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกหน่วย" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlunit" Width="180" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label33" CssClass="col-md-2 control-label" runat="server" Text="รหัสงบประมาณ : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddlcostcenter"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="false"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlcostcenter" runat="server" InitialValue="0"
                                                ControlToValidate="ddlcostcenter" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกรหัสงบประมาณ" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlcostcenter" Width="180" />

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label32" CssClass="col-md-2 control-label" runat="server" Text="งบประมาณที่ตั้งไว้ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtbudget_set" CssClass="form-control" runat="server"
                                                Enabled="false"
                                                TextMode="Number"></asp:TextBox>

                                            <%--                                            <asp:RequiredFieldValidator ID="Rqtxtbudget_set1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtbudget_set" Font-Size="11"
                                                ErrorMessage="กรุณากรอกงบประมาณที่ตั้งไว้" />
                                            <asp:RegularExpressionValidator ID="Rqtxtbudget_set2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtbudget_set"
                                                ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtbudget_set1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtbudget_set2" Width="160" />--%>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label35" CssClass="col-md-2 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlplace"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlplace" runat="server" InitialValue="0"
                                                ControlToValidate="ddlplace" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกสถานที่" ValidationGroup="SaveAdd_BuyList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlplace" Width="180" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <asp:LinkButton ID="btnAddBuy_Newlist" CssClass="btn btn-warning btn-sm" runat="server"
                                                CommandName="CmdAddBuy_Newlist" OnCommand="btnCommand"
                                                ValidationGroup="SaveAdd" title="Save">
                                                <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <%--<div class="col-md-2">
                                        </div>--%>
                                        <div class="col-md-12">
                                            <asp:GridView ID="GvReportAdd"
                                                runat="server"
                                                HeaderStyle-CssClass="info"
                                                CssClass="table table-striped table-responsive table-bordered word-wrap"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                OnRowDataBound="gvRowDataBound"
                                                AutoGenerateColumns="false">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <asp:Label ID="_lbItemsnumber" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu0idx" runat="server" Text='<%# Eval("u0idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu1idx" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbpr_type_idx" runat="server" Text='<%# Eval("pr_type_idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbm1_ref_idx" runat="server" Text='<%# Eval("m1_ref_idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbposidx" runat="server" Text='<%# Eval("posidx") %>' Visible="false" />

                                                            <asp:Label ID="_lbsysidx" runat="server" Text='<%# Eval("sysidx") %>' Visible="false" />

                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%" HeaderText="ประเภทการขอซื้อ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_purchase_type_idx_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%" HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="Label60" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="items_type2" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_typidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                        </p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("tdidx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label62" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label84" runat="server" CssClass="col-sm-12">
                                                                <p><b>ประเภทกลุ่มพนักงาน:</b> &nbsp;<%# Eval("pos_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label63" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label64" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label65" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label66" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %><%--&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %>--%></p>
                                                            </asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%" HeaderText="จำนวน" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12" Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="ราคา" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_price" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="รวม" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="งบประมาณที่ตั้งไว้" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จัดการ"
                                                        HeaderStyle-Font-Size="Small"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label10" class="col-md-2 control-label" runat="server" Text="แนบเอกสาร Memo : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_memo" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx  with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label36" class="col-md-2 control-label" runat="server" Text="แนบเอกสาร Organization : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_organization" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label37" class="col-md-2 control-label" runat="server" Text="แนบเอกสารตัดเสีย : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_cutoff" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label75" class="col-md-2 control-label" runat="server" Text="แนบเอกสารอื่นๆ : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_other" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx  with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <div class="form-group" id="div_btn_buynew" runat="server" visible="false">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <%-- update --%>

                    <asp:FormView ID="FvUpdateBuyNew" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:HiddenField ID="hidden_m0_node" runat="server" Value='<%# Eval("node_idx") %>' />
                                    <asp:HiddenField ID="hidden_m0_actor" runat="server" Value='<%# Eval("actor_idx") %>' />
                                    <asp:HiddenField ID="hidden_emp_idx" runat="server" Value='<%# Eval("emp_idx_its") %>' />
                                    <asp:HiddenField ID="hidden_emp_director" runat="server" Value='<%# Eval("emp_idx_director") %>' />
                                    <asp:HiddenField ID="hidden_status_node" runat="server" Value='<%# Eval("doc_status") %>' />
                                    <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                    <asp:HiddenField ID="hidden_actor_name" runat="server" Value='<%# Eval("actor_name") %>' />

                                    <div class="form-group">
                                        <asp:Label ID="Label49" class="col-md-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3 control-label textleft">
                                            <asp:TextBox Enabled="false" ID="lbdocument_code"
                                                Text='<%# Eval("doccode") %>' runat="server"
                                                CssClass="form-control fa-align-left"></asp:TextBox>

                                            <asp:Label ID="hidden_name_actor" Visible="false" runat="server" Text='<%# Eval("actor_name") %>' />
                                        </div>
                                        <asp:Label ID="Label50" class="col-md-2 control-label" runat="server" Text="วันที่ขอซื้อ : " />
                                        <div class="col-sm-3 control-label textleft">
                                            <asp:TextBox Enabled="false" ID="Labellld"
                                                Text='<%# Eval("zdocdate") %>' runat="server"
                                                CssClass="form-control fa-align-left"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-md-2 control-label" runat="server" Text="ประเภททรัพย์สิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <asp:Panel ID="pnl_other" runat="server" Visible="true">
                                        <div class="form-group">
                                            <asp:Label ID="Label23" class="col-md-2 control-label" runat="server" Text="ประเภทอุปกรณ์ขอซื้อ : " />
                                            <div class="col-md-3">

                                                <asp:DropDownList ID="ddlSearchTypeDevices" AutoPostBack="true"
                                                    runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RqddSearchTypeDevices"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlSearchTypeDevices" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทอุปกรณ์ขอซื้อ"
                                                    ValidationExpression="กรุณาเลือกประเภทอุปกรณ์ขอซื้อ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RqddSearchTypeDevices" Width="160" />


                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label24" class="col-md-2 control-label" runat="server" Text="กลุ่มอุปกรณ์ขอซื้อ : " />
                                            <div class="col-md-3">

                                                <asp:DropDownList ID="ddlSearchDevices" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ..." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RqddSearchDevices"
                                                    ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlSearchDevices" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ"
                                                    ValidationExpression="กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17"
                                                    runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RqddSearchDevices" Width="160" />

                                            </div>
                                        </div>
                                    </asp:Panel>


                                    <div class="form-group">
                                        <asp:Label ID="Label25" class="col-md-2 control-label" runat="server" Text="ประเภทการขอซื้อ : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddlsearch_typepurchase"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true"
                                                OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddsearch_typepurchase"
                                                ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsearch_typepurchase" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการขอซื้อ"
                                                ValidationExpression="กรุณาเลือกประเภทการขอซื้อ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddsearch_typepurchase" Width="160" />

                                        </div>
                                    </div>

                                    <asp:Panel ID="pnl_ionum" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label27" class="col-md-2 control-label" runat="server" Text="เลขที่ IO : " />
                                            <div class="col-md-3">

                                                <asp:TextBox ID="txtionum"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true"
                                                    OnTextChanged="onTextChanged">
                                                </asp:TextBox>

                                            </div>
                                            <div class="col-md-2">

                                                <p class="help-block">
                                                    <font color="red">
                                                            **ต้องเป็นตัวใหญ่ทั้งหมด
                                                        </font>
                                                </p>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnl_ref_itnum" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label28" class="col-md-2 control-label" runat="server" Text="เลขที่ตัดเสีย : " />
                                            <div class="col-md-3">

                                                <asp:TextBox ID="txtref_itnum"
                                                    runat="server" CssClass="form-control fa-align-left">
                                                </asp:TextBox>

                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <asp:Label ID="Label29" class="col-md-2 control-label" runat="server" Text="ขอซื้อให้พนักงานตำแหน่ง : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddlposition"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlposition" runat="server" InitialValue="0"
                                                ControlToValidate="ddlposition" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกขอซื้อให้พนักงานตำแหน่ง" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldddlposition" Width="180" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label81" class="col-md-2 control-label" runat="server" Text="ประเภทกลุ่มพนักงาน : " />
                                        <div class="col-md-3">

                                            <asp:DropDownList ID="ddl_target"
                                                runat="server" CssClass="form-control fa-align-left"
                                                AutoPostBack="true" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddl_target" runat="server" InitialValue="0"
                                                ControlToValidate="ddl_target" Display="None" SetFocusOnError="true"
                                                ErrorMessage="ประเภทกลุ่มพนักงาน" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Requiredddl_target" Width="180" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label39" class="col-md-2 control-label" runat="server" Text="สเปค : " />
                                        <div class="col-md-3">


                                            <div class="input-group col-md-12 pull-left">
                                                <asp:TextBox ID="ddlspecidx"
                                                    runat="server" CssClass="form-control"
                                                    Enabled="false">
                                                </asp:TextBox>

                                                <div class="input-group-btn">
                                                    <asp:LinkButton ID="btn_spec_search" Visible="false"
                                                        CssClass="btn btn-primary pull-left" runat="server"
                                                        data-original-title="ค้นหา" data-toggle="tooltip" Text='<i class="fa fa-search"></i>'
                                                        OnCommand="btnCommand" CommandName="btn_spec_search">
                                                       
                                                    </asp:LinkButton>
                                                </div>
                                            </div>


                                            <asp:TextBox ID="txt_m1_ref_idx"
                                                runat="server" CssClass="form-control"
                                                Visible="false">
                                            </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlspecidx" runat="server" InitialValue=""
                                                ControlToValidate="ddlspecidx" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกเลือกสเปค" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlspecidx" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldddlspecidx" Width="180" />
                                        </div>

                                        <div class="col-md-2">
                                            <asp:CheckBox ID="check_special" runat="server" AutoPostBack="true"
                                                RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                Text="Special"
                                                OnCheckedChanged="checkindexchange" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtlist_buynew" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_buynew"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-md-2 control-label" runat="server" Text="ราคาต่อเครื่อง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtprice_buynew" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtprice_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกราคา" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_buynew"
                                                ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                        </div>
                                        <asp:Label ID="Label30" CssClass="col-md-2 control-label" runat="server" Text="สกุลเงิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlcurrency"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlcurrency" runat="server" InitialValue="0"
                                                ControlToValidate="ddlcurrency" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกสกุลเงิน" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlcurrency" Width="180" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-md-2 control-label" runat="server" Text="จำนวน : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtqty_buynew" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtqty_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_buynew"
                                                ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                        <asp:Label ID="Label31" CssClass="col-md-2 control-label" runat="server" Text="หน่วย : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlunit"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlunit" runat="server" InitialValue="0"
                                                ControlToValidate="ddlunit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกหน่วย" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlunit" Width="180" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label33" CssClass="col-md-2 control-label" runat="server" Text="รหัสงบประมาณ : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlcostcenter"
                                                Enabled="false"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlcostcenter" runat="server" InitialValue="0"
                                                ControlToValidate="ddlcostcenter" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกรหัสงบประมาณ" ValidationGroup="SaveAdd" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlcostcenter" Width="180" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label32" CssClass="col-md-2 control-label" runat="server" Text="งบประมาณที่ตั้งไว้ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtbudget_set" CssClass="form-control" runat="server"
                                                Enabled="false"></asp:TextBox>

                                            <%--                                            <asp:RequiredFieldValidator ID="Rqtxtbudget_set1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtbudget_set" Font-Size="11"
                                                ErrorMessage="กรุณากรอกงบประมาณที่ตั้งไว้" />
                                            <asp:RegularExpressionValidator ID="Rqtxtbudget_set2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtbudget_set"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtbudget_set1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtbudget_set2" Width="160" />--%>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label35" CssClass="col-md-2 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlplace"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Rqddlplace" runat="server" InitialValue="0"
                                                ControlToValidate="ddlplace" Display="None" SetFocusOnError="true"
                                                ErrorMessage="กรุณาเลือกสถานที่" ValidationGroup="SaveAdd_BuyList" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Rqddlplace" Width="180" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <asp:LinkButton ID="btnAddBuy_Newlist" CssClass="btn btn-warning btn-sm" runat="server"
                                                CommandName="CmdAddBuy_Newlist" OnCommand="btnCommand"
                                                ValidationGroup="SaveAdd" title="Save">
                                                <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <%--<div class="col-md-2">
                                        </div>--%>
                                        <div class="col-md-12">
                                            <asp:GridView ID="GvReportAdd"
                                                runat="server"
                                                HeaderStyle-CssClass="info"
                                                CssClass="table table-striped table-responsive table-bordered word-wrap"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                OnRowDataBound="gvRowDataBound"
                                                AutoGenerateColumns="false">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <asp:Label ID="_lbItemsnumber" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu0idx" runat="server" Text='<%# Eval("u0idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu1idx" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbpr_type_idx" runat="server" Text='<%# Eval("pr_type_idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbm1_ref_idx" runat="server" Text='<%# Eval("m1_ref_idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbposidx" runat="server" Text='<%# Eval("posidx") %>' Visible="false" />
                                                            <asp:Label ID="_lbsysidx" runat="server" Text='<%# Eval("sysidx") %>' Visible="false" />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%" HeaderText="ประเภทการขอซื้อ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_purchase_type_idx_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%" HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="Label60" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="items_type2" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_typidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                        </p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("tdidx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label62" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label84" runat="server" CssClass="col-sm-12">
                                                                <p><b>ประเภทกลุ่มพนักงาน:</b> &nbsp;<%# Eval("pos_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label63" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label64" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label65" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label66" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %><%--&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %>--%></p>
                                                            </asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%" HeaderText="จำนวน" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12" Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="ราคา" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_price" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="รวม" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="งบประมาณที่ตั้งไว้" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จัดการ"
                                                        HeaderStyle-Font-Size="Small"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark" TextMode="multiline"
                                                Rows="5" CssClass="form-control" runat="server"
                                                Text='<%# Eval("remark") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel_memoUpdate" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="lb_memoUpdate" class="col-md-2 control-label" runat="server" Text="แนบเอกสาร Memo : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_memoUpdate" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel_organizationUpdate" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="lb_organizationUpdate" class="col-md-2 control-label" runat="server" Text="แนบเอกสาร Organization : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_organizationUpdate" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel_cutoffUpdate" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="lb_cutoffUpdate" class="col-md-2 control-label" runat="server" Text="แนบเอกสารตัดเสีย : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_cutoffUpdate" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label76" class="col-md-2 control-label" runat="server" Text="แนบเอกสารอื่นๆ : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="uploadfile_otherUpdate" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                        ClientIDMode="Static" runat="server"
                                                        CssClass="btn btn-primary btn-sm multi"
                                                        accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx with-preview" />
                                                    <p class="help-block">
                                                        <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx

                                                        </font>
                                                    </p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <asp:UpdatePanel ID="UpdatePanel_file" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:GridView ID="gvFileUpdate" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="gvRowDataBound">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-10">
                                                                        <asp:Label ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                        <asp:Label ID="lb_Download" runat="server" Visible="false"></asp:Label>
                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />

                                                                        <asp:LinkButton ID="bnDeletefile" runat="server" Text="Delete" CssClass="btn btn-danger btn-sm"
                                                                            OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')"
                                                                            CommandName="bnDeletefile"
                                                                            OnCommand="btnCommand"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="gvFileUpdate" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_buynew" runat="server" visible="true">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </EditItemTemplate>


                    </asp:FormView>

                </div>
            </div>

            <div class="col-lg-12" id="div_createbuyreplace" runat="server" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-shopping-cart"></i><strong>&nbsp; รายการขอซื้ออุปกรณ์ทดแทน
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertBuyReplace" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-md-2 control-label" runat="server" Text="ทรัพย์สิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlsystem_replace" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem_replace" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label11" class="col-md-2 control-label" runat="server" Text="เลขที่ใบตัดชำรุด : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddldoccode" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddldoccode" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลขที่ใบแจ้งซ่อม"
                                                ValidationExpression="กรุณาเลือกเลขที่ใบแจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtlist_buyreplace" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_buyreplace"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-md-2 control-label" runat="server" Text="จำนวน : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtqty_buyreplace" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtqty_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_buyreplace"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-md-2 control-label" runat="server" Text="มูลค่า : " />
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtprice_buyreplace" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtprice_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_buyreplace"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="Label3" CssClass="form-control text-center" Enabled="false" runat="server" Text="บาท " />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAddBuy_Replacelist" OnCommand="btnCommand" ValidationGroup="SaveAdd" title="Save"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">
                                            <asp:GridView ID="GvReportAdd_BuyReplace"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="list_buyreplace" HeaderText="รายละเอียด" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="qty_buyreplace" HeaderText="จำนวน" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="price_buyreplace" HeaderText="ราคา" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark_buyreplace" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_buyreplace"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labesl10" class="col-md-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="UploadImages_BuyReplace" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_buyreplace" runat="server" visible="false">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_createcutdevices" runat="server" visible="false">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-scissors"></i><strong>&nbsp; รายการตัดเสียอุปกรณ์
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertCutDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-md-2 control-label" runat="server" Text="ทรัพย์สิน : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlsystem_cutdevices" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" id="div_doccode" runat="server" visible="false">
                                        <asp:Label ID="Label11" class="col-md-2 control-label" runat="server" Text="เลขที่รายการแจ้งซ่อม : " />
                                        <div class="col-md-2">
                                            <asp:DropDownList ID="ddldoccode_cutdevices" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">IT01610001</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddldoccode_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลขที่ใบแจ้งซ่อม"
                                                ValidationExpression="กรุณาเลือกเลขที่ใบแจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-md-2 control-label" runat="server" Text="รหัสทรัพย์สิน : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtasset_cutdevices" Text="35000002" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label18" CssClass="col-md-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtlist_cutdevices" Text="เครื่องโน๊ตบุ๊ค 16 GB HDD 1 TB " Enabled="false" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" CssClass="col-md-2 control-label" runat="server" Text="Book.Val : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtbook" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="สาเหตุ : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtreason_cutdevices" Text="เนื่องจากระยะเวลาในการใช้งาน และเครื่องโน๊ตบุ๊คเกิดการเสื่อมสภาพ" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtreason_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtreason_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-2">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAddBuy_Cutlist" OnCommand="btnCommand" ValidationGroup="SaveAdd" title="Save"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">หมวดทรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Asset Num.</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Acquls. Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Book Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Ref IT Num</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Action</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>Z1100</td>
                                                        <td>35000002</td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">เครื่องโน๊ตบุ๊ค 16 GB HDD 1 TB</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>35000</td>
                                                        <td>-</td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.549085903</label></td>
                                                        <td>-</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>


                                    <%--  <div class="form-group">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-8">

                                            <asp:GridView ID="GvReportAdd_BuyReplace"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="list_buyreplace" HeaderText="รายละเอียด" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="qty_buyreplace" HeaderText="จำนวน" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="price_buyreplace" HeaderText="ราคา" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>--%>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark_cutdevices" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labeesl10" class="col-md-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="UploadImages_CutDevices" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_cutdevices" runat="server" visible="true">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>


            <%-- History purchase--%>
            <div class="col-sm-12" runat="server" id="content_history_purchase_edit" visible="false">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                                        <b>สถานะดำเนินการขอซื้ออุปกรณ์</b>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="history_purchasequipment_edit" runat="server">
                            <HeaderTemplate>
                                <div class="row">
                                    <label class="col-sm-2 control-label">วันที่ดำเนินการ</label>
                                    <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                    <label class="col-sm-2 control-label">ดำเนินการ</label>
                                    <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                    <label class="col-sm-3 control-label">หมายเหตุ</label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <span><%# Eval("zdate") %></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span><%# Eval("actor_name") %></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><%# Eval("node_name") %></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><%# Eval("status_name") %></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span><%# Eval("approve_remark") %></span>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>



        </asp:View>

        <asp:View ID="ViewCutDevices" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_CutDevieces" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-md-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-md-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-md-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-md-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewBudget" runat="server">
            <div class="col-lg-12" id="gvbudget" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i><strong>&nbsp; รายการรอการจัดการ Budget</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รหัสเอกสาร</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">วันที่สร้าง</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ข้อมูลผู้ทำรายการ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">หมายเหตุ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">สถานะ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รายละเอียด</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">DP610001</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">04/04/2018 12:30:45</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">นางสาว กานต์ธิดา ตระกูลบุญรักษ์</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">ทดสอบระบบ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">พิจารณาผลโดย Budget</label></td>
                                        <td>
                                            <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"><i class="fa fa-file-text-o" ></i></asp:LinkButton>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailbudget" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Budget" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" Text="58000070" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-md-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesdept" Text="การจัดการระบบสารสนเทศ" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-md-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtsec" Text="พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-md-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtpos" Text="เจ้าหน้าที่พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-md-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Text="0924235995" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Text="webmaster@taokaenoi.co.th" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <hr />

                    <asp:FormView ID="FvDetail_ShowBudget" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtlist_buynew" Enabled="false" Text="DP610001" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtreไmark" Text="ทดสอบระบบ" Enabled="false" TextMode="multiline"
                                                Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-9">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">Notebook ram 16 gb hdd 1 tb</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">35000</label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtionum" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">2</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">HR</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">เก้าอี้ตาข่ายดำ 1 ตัว</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1000</label></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label15" class="col-md-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                        <div class="col-md-9">
                                            <asp:TextBox ID="TextBox16" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labeel10" class="col-md-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="UploadImagesBudget" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewbudget" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewAsset" runat="server">
            <div class="col-lg-12" id="gvasset" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i><strong>&nbsp; รายการรอการจัดการ Asset</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%-- start list Asset --%>

                            <%-- <asp:FormView ID="Fv_ListAsset" runat="server" Width="100%"
                                OnDataBound="FvDetail_DataBound">
                                <ItemTemplate>--%>

                            <asp:GridView ID="GvAssetits_u0_document_list" Visible="true" runat="server"
                                AutoGenerateColumns="false" DataKeyNames="u0idx"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
                                OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_u0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                            <asp:Label ID="idx" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>
                                            <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                    <p></b> &nbsp;<%# Eval("doccode") %></p>
                                            </asp:Label>
                                            <asp:Label ID="lbdoccode" runat="server" Visible="false" Text='<%# Eval("doccode") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12">
                    <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emp_code") %></p>
                                            </asp:Label>
                                            <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emp_name_th") %></p>
                                            </asp:Label>
                                            <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_th") %></p>  
                                            </asp:Label>
                                            <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_name_th") %></p>
                                            </asp:Label>
                                            <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_th") %></p>
                                            </asp:Label>
                                            <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12">
                    <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_th") %></p>
                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="30%"
                                        HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left"
                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                        <ItemTemplate>
                                            <div>


                                                <p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("zdocdate") %></p>


                                            </div>
                                            <div>

                                                <p><b>สถานที่:</b> &nbsp;<%# Eval("place_name") %></p>

                                            </div>
                                            <asp:Label ID="lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />
                                            <div>


                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("remark") %></p>


                                            </div>
                                            <asp:GridView ID="gvitems" Visible="true" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-bordered" GridLines="None"
                                                HeaderStyle-CssClass="default" HeaderStyle-Height="10px" AllowPaging="true"
                                                OnRowDataBound="gvRowDataBound">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                                        <ItemTemplate>

                                                            <asp:Label ID="Gvu1_u1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                            <asp:Label ID="idx2reportrr" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ประเภทการขอซื้อ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_name_purchase_type" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("name_purchase_type") %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_zdevices_name" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("zdevices_name") %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_qty" runat="server" Font-Size="Small" Visible="true"
                                                                Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="5%" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_price" runat="server" Font-Size="Small"
                                                                Visible="true"
                                                                Text='<%# getformatfloat(((decimal)Eval("price")).ToString(),2) %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_status_node" runat="server" CssClass="col-sm-12">
                            <span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;<br />
                                <b>โดย</b>&nbsp;<br />   <%# Eval("actor_name") %></span>
                                            </asp:Label>

                                            <%--  <asp:Label ID="lbmailpurchase" runat="server" Visible="false" Text='<%# Eval("email_purchase") %>' />--%>

                                            <asp:Label ID="lb_node_idx" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                            <asp:Label ID="lb_actor_idx" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />
                                            <asp:Label ID="lb_actor_name" runat="server" Visible="false" Text='<%# Eval("actor_name") %>' />
                                            <asp:Label ID="lb_doc_status" runat="server" Visible="false" Text='<%# Eval("doc_status") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ความคิดเห็นจากไอที" ControlStyle-Font-Size="10" HeaderStyle-Width="20%" HeaderStyle-Font-Size="9" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" Visible="false">
                                        <ItemTemplate>


                                            <asp:Panel ID="pn_price_comment_complete" runat="server" Visible="true">
                                                <asp:Repeater ID="rptcommentfromit" runat="server">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lbcommentit" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("commentfromITSupport") %>'></asp:Label>
                                                        <br />
                                                        <label class="bg-success">:: ราคา :: </label>
                                                        <br />
                                                        <asp:Label ID="_lbprice" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("price_equipment") %>'></asp:Label>
                                                        <br />

                                                    </ItemTemplate>

                                                </asp:Repeater>
                                            </asp:Panel>


                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="updatebtnsaveEditAsset" runat="server">
                                                <ContentTemplate>

                                                    <asp:LinkButton ID="btnmanage_itsAsset" CssClass="btn btn-info btn-sm" runat="server"
                                                        data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                                        CommandArgument='<%#
                                            Eval("u0idx")+ "|" + 
                                            Eval("node_idx")+ "|" + 
                                            Eval("emp_idx_its")+ "|" + 
                                            Eval("pr_type_idx") + "|" + 
                                            Eval("actor_idx")  + "|" + 
                                            Eval("doccode") + "|" + 
                                            Eval("doc_status")+ "|" + 
                                            Eval("flow_item")+ "|" + 
                                            Eval("place_idx")
                                            %>'
                                                        CommandName="btndetailasset">
                                           <i class="fa fa-file"></i>
                                                    </asp:LinkButton>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnmanage_itsAsset" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <%--</ItemTemplate>
                            </asp:FormView>--%>

                            <%-- end list Asset --%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailasset" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดข้อมูลผู้ขอซื้ออุปกรณ์</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Asset" runat="server" Width="100%"
                        DefaultMode="Edit">
                        <EditItemTemplate>

                            <div class="panel-body">

                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lb_emps_code_purchase" Text='<%# Eval("emp_code") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล  :</label>
                                        <div class="col-sm-4 control-label textleft">

                                            <asp:TextBox ID="fullname_purchase_approve" Text='<%# Eval("emp_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label7" Text='<%# Eval("org_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbdreptnamepurchase" Text='<%# Eval("dept_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label9" Text='<%# Eval("sec_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label10" Text='<%# Eval("pos_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label11" Text='<%# Eval("emp_mobile_no") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">e-mail :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbemail" Text='<%# Eval("emp_email") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </EditItemTemplate>
                    </asp:FormView>

                </div>

                <asp:FormView ID="FvDetail_ShowAsset" runat="server" Width="100%"
                    OnDataBound="FvDetail_DataBound"
                    DefaultMode="Edit">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการขอซื้ออุปกรณ์</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รหัสเอกสาร : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                            <asp:TextBox ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="hidden_name_actor" runat="server"
                                                Value='<%# Eval("actor_name") %>' />
                                            <asp:HiddenField ID="hidden_flow_item" runat="server"
                                                Value='<%# Eval("flow_item") %>' />

                                            <asp:TextBox ID="txt_hidden_sysidx" Text='<%# Eval("sysidx") %>' runat="server" Visible="false"></asp:TextBox>

                                            <asp:TextBox ID="txt_hidden_node_idx" Text='<%# Eval("node_idx") %>' runat="server" Visible="false"></asp:TextBox>

                                            <asp:TextBox ID="txt_hidden_actor_idx" Text='<%# Eval("actor_idx") %>' runat="server" Visible="false"></asp:TextBox>

                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="วันที่ขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Labellld" Text='<%# Eval("zdocdate") %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtremark" Enabled="false"
                                                TextMode="multiline" Text='<%# Eval("remark") %>'
                                                Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="false">
                                        <asp:Label ID="Label13" class="col-md-2 control-label" runat="server" Text="ความคิดเห็นผู้จัดการฝ่าย : " />
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtdirector_approve_remark" Text='<%# Eval("director_approve_remark") %>'
                                                Enabled="false" TextMode="multiline"
                                                Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="false">
                                        <asp:Label ID="Label14" class="col-md-2 control-label" runat="server" Text="ความคิดเห็นจาก Budget : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtbudget_approve_remark" Text='<%# Eval("budget_approve_remark") %>'
                                                Enabled="false" TextMode="multiline" Rows="2"
                                                CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>



                                    <div class="form-group">

                                        <div class="col-md-12">
                                            <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                                <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                            </blockquote>
                                        </div>
                                        <div class="col-md-12">

                                            <asp:GridView ID="gvItemsIts_Asset" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive word-wrap"
                                                HeaderStyle-CssClass="info"
                                                GridLines="None"
                                                OnRowDataBound="gvRowDataBound">

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                            <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />

                                                            <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                            <asp:Label ID="_lbu2idx" runat="server" Visible="false" Text='<%# Eval("u2idx") %>' />

                                                            <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ประเภทการขอซื้อ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="10%">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_purchase_type_idx_name" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>

                                                            <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                            <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="35%">
                                                        <ItemTemplate>


                                                            <asp:Label ID="Label42" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="items_type1" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label41" runat="server" CssClass="col-sm-12">
                                                                <p><%--<b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                                &nbsp;&nbsp;--%><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("zdevices_name") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label43" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label44" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label40" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label46" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %>&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %></p>
                                                            </asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                                Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="7%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_price" runat="server" CssClass="col-sm-12"
                                                                Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รวม" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="7%"
                                                        Visible="false">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12"
                                                                Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="งบประมาณที่ตั้งไว้" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="7%"
                                                        Visible="false">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12"
                                                                Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Asset No." HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="10%">
                                                        <ItemTemplate>


                                                            <asp:TextBox ID="txtasset_no" MaxLength="50" placeholder="กรอกเลข Asset..."
                                                                CssClass="form-control" runat="server" Width="100%"
                                                                Text='<%# Eval("asset_no") %>'
                                                                Enabled='<%# Convert.ToInt32(Eval("flow_item"))==4?true:false %>'>
                                                            </asp:TextBox>

                                                            <asp:Label ID="_lbid" runat="server" Visible="false" Text='<%# Eval("id") %>' />


                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-12">

                                            <asp:GridView ID="gvFile_Asset" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover "
                                                OnRowDataBound="gvRowDataBound">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                                        <ItemTemplate>
                                                            <div class="col-lg-11">
                                                                <asp:Label ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                            </div>
                                                            <div class="col-lg-1">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:Label ID="lb_Download" runat="server" Visible="false"></asp:Label>
                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <asp:Panel ID="Panel_body_approve" runat="server">

                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-file"></i>&nbsp;
                                                 <b>ดำเนินการขอซื้ออุปกรณ์&nbsp;
                                                <asp:Label ID="lb_title_approve" runat="server" Text=""></asp:Label></b>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">


                                                            <div class="form-group">
                                                                <asp:Label ID="Label29" class="col-md-2 control-label"
                                                                    runat="server" Text="ผลอนุมัติ : " />
                                                                <div class="col-md-3">

                                                                    <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Value="0" Text="ผลการอนุมัติ....." />
                                                                        <asp:ListItem Value="5" Text="อนุมัติ" />
                                                                        <asp:ListItem Value="6" Text="กลับไปแก้ไข" />
                                                                        <%--<asp:ListItem Value="7" Text="ไม่อนุมัติ" />--%>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RqddlStatusapprove" runat="server" InitialValue="0"
                                                                        ControlToValidate="ddlStatusapprove" Display="None"
                                                                        SetFocusOnError="true"
                                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" ValidationGroup="SaveAdd_BuyList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight"
                                                                        TargetControlID="RqddlStatusapprove" Width="180" />

                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-md-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                                                <div class="col-md-8">
                                                                    <asp:TextBox ID="txt_asset_remark" TextMode="multiline" Rows="4"
                                                                        CssClass="form-control" runat="server"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Rqtxt_asset_remark" ValidationGroup="SaveAdd_BuyList"
                                                                        runat="server" Display="None"
                                                                        ControlToValidate="txt_asset_remark" Font-Size="11"
                                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                        SetFocusOnError="true" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7"
                                                                        runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                        TargetControlID="Rqtxt_asset_remark" Width="160" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                                                                        runat="server"
                                                                        ValidationGroup="SaveAdd_BuyList" Display="None"
                                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                        ControlToValidate="txtremark"
                                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                                        SetFocusOnError="true" />

                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert_asset" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewasset" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <%-- History asset--%>
                        <div class="form-group">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                                            <b>สถานะดำเนินการขอซื้ออุปกรณ์</b>
                                </div>
                                <div class="panel-body">
                                    <asp:Repeater ID="rpt_history" runat="server">
                                        <HeaderTemplate>
                                            <div class="row">
                                                <label class="col-sm-2 control-label">วันที่ดำเนินการ</label>
                                                <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                                <label class="col-sm-2 control-label">ดำเนินการ</label>
                                                <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                                <label class="col-sm-3 control-label">หมายเหตุ</label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <span><%# Eval("zdate") %></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><%# Eval("actor_name") %></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><%# Eval("node_name") %></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><%# Eval("status_name") %></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><%# Eval("approve_remark") %></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                        </div>


                    </EditItemTemplate>
                </asp:FormView>



            </div>


        </asp:View>

        <asp:View ID="ViewIT" runat="server">

            <div id="detailIT" runat="server" class="col-lg-12" visible="true">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดข้อมูลผู้ขอซื้ออุปกรณ์</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_IT" runat="server" DefaultMode="Edit" Width="100%">
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lb_emps_code_purchase" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("emp_code") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            ชื่อ - นามสกุล :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="fullname_purchase_approve" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("emp_name_th") %>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label54" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("org_name_th") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            ฝ่าย :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbdreptnamepurchase" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("dept_name_th") %>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            แผนก :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label55" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("sec_name_th") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            ตำแหน่ง :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label56" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("pos_name_th") %>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            เบอร์ติดต่อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label57" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("emp_mobile_no") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">
                                            e-mail :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbemail" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("emp_email") %>' />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>
                </div>





                <asp:FormView ID="FvDetail_ShowIT" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>รายละเอียดรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รหัสเอกสาร : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                            <asp:TextBox ID="lbdocument_code" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("doccode") %>'></asp:TextBox>
                                            <asp:HiddenField ID="hidden_name_actor" runat="server" Value='<%# Eval("actor_name") %>' />
                                            <asp:HiddenField ID="hidden_flow_item" runat="server" Value='<%# Eval("flow_item") %>' />

                                            <asp:TextBox ID="txt_hidden_sysidx" Text='<%# Eval("sysidx") %>' runat="server" Visible="false"></asp:TextBox>

                                            <asp:TextBox ID="txt_hidden_node_idx" Text='<%# Eval("node_idx") %>' runat="server" Visible="false"></asp:TextBox>

                                            <asp:TextBox ID="txt_hidden_actor_idx" Text='<%# Eval("actor_idx") %>' runat="server" Visible="false"></asp:TextBox>

                                            <asp:TextBox ID="txt_rdept_idx_its" Text='<%# Eval("rdept_idx_its") %>' runat="server" Visible="false"></asp:TextBox>


                                        </div>
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="วันที่ขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Labellld" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("zdocdate") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Labedle2" runat="server" class="col-md-2 control-label" Text="หมายเหตุ : " />
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtremark" runat="server" CssClass="form-control" Enabled="false" Rows="5" Text='<%# Eval("remark") %>' TextMode="multiline"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div runat="server" class="form-group" visible="false">
                                        <asp:Label ID="Label58" runat="server" class="col-md-2 control-label" Text="ความคิดเห็นผู้จัดการฝ่าย : " />
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtdirector_approve_remark" runat="server" CssClass="form-control" Enabled="false" Rows="2" Text='<%# Eval("director_approve_remark") %>' TextMode="multiline"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div runat="server" class="form-group" visible="false">
                                        <asp:Label ID="Label59" runat="server" class="col-md-2 control-label" Text="ความคิดเห็นจาก Budget : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtbudget_approve_remark" runat="server" CssClass="form-control" Enabled="false" Rows="2" Text='<%# Eval("budget_approve_remark") %>' TextMode="multiline"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                                <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                            </blockquote>
                                        </div>
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvItemsIts_IT" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-responsive word-wrap" GridLines="None" HeaderStyle-CssClass="info"
                                                OnRowDataBound="gvRowDataBound" Visible="true">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">
                                                        ยังไม่มีรายการขอซื้อ
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%" HeaderText="ลำดับ"
                                                        ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="_lbItemsnumber" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu0idx" runat="server" Text='<%# Eval("u0idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbu1idx" runat="server" Text='<%# Eval("u1idx") %>' Visible="false" />
                                                            <asp:Label ID="_lbpr_type_idx" runat="server" Text='<%# Eval("pr_type_idx") %>' Visible="false" />
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        Visible="false"
                                                        HeaderStyle-Width="7%" HeaderText="ประเภทการขอซื้อ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_purchase_type_idx_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>
                                                            <asp:Label ID="lbnodeidxItems" runat="server" Text='<%# Eval("node_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbactoridxItems" runat="server" Text='<%# Eval("actor_idx") %>' Visible="false" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="40%" HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="Label60" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="items_type2" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label61" runat="server" CssClass="col-sm-12">
                                                                <p><%--<b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                                &nbsp;&nbsp;--%><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("zdevices_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label62" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label63" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label64" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label65" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label66" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %>&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %></p>
                                                            </asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="5%" HeaderText="จำนวน" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%" HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12" Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%" HeaderText="ราคา" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_price" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%" HeaderText="รวม" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="3%" HeaderText="งบประมาณที่ตั้งไว้" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12" Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderStyle-Width="15%"
                                                        HeaderText="Asset No." ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <asp:TextBox ID="txtasset_no" runat="server"
                                                                CssClass="form-control" Visible="false"
                                                                MaxLength="50" placeholder="กรอกเลข Asset..." Text='<%# Eval("asset_no") %>' Width="100%">
                                                            </asp:TextBox>
                                                            <asp:Label ID="_lbid0" runat="server" Text='<%# Eval("id") %>' Visible="false" />

                                                            <asp:GridView ID="gvAssetNo_IT" Visible="true" runat="server"
                                                                AutoGenerateColumns="false"
                                                                GridLines="None"
                                                                HeaderStyle-CssClass="default" HeaderStyle-Height="10px"
                                                                AllowPaging="true"
                                                                ShowHeader="false"
                                                                OnRowDataBound="gvRowDataBound"
                                                                OnPageIndexChanging="gvPageIndexChanging"
                                                                CssClass="table table-striped table-responsive word-wrap">
                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%"
                                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                                        Visible="false">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                                                            <%--<br />--%>
                                                                            <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                                            <asp:Label ID="idx2reportrr" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="assetno" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                                        HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lb_assetno" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("asset_no") %>' />


                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="9"
                                                        ControlStyle-Font-Size="10"
                                                        HeaderText="Spec"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-Width="25%">
                                                        <ItemTemplate>


                                                            <asp:DropDownList ID="ddlspec_idx" runat="server"
                                                                CssClass="form-control" Width="100%"
                                                                Visible="false">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lb_spec_name" runat="server" Font-Size="Small" Visible="false" Text='<%# Eval("spec_remark") %>' />
                                                            <asp:TextBox ID="txt_spec_remark" runat="server"
                                                                Font-Size="Small" Visible="true"
                                                                TextMode="MultiLine"
                                                                CssClass="form-control"
                                                                Rows="7"
                                                                Text='<%# Eval("spec_remark") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:GridView ID="gvFile_IT" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="gvRowDataBound" Visible="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                                        <ItemTemplate>
                                                            <div class="col-lg-11">
                                                                <asp:Label ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                            </div>
                                                            <div class="col-lg-1">
                                                                <asp:HyperLink ID="btnDL11" runat="server" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:Label ID="lb_Download" runat="server" Visible="false"></asp:Label>
                                                                <asp:HiddenField ID="hidFile11" runat="server" Value='<%# Eval("Download") %>' />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel_body_approve" runat="server">
                                        <div class="form-group">

                                            <div class="col-md-12">
                                                <div class="panel panel-success">
                                                    <div class="panel-heading">
                                                        <i class="glyphicon glyphicon-file"></i>&nbsp; <b>ดำเนินการขอซื้ออุปกรณ์&nbsp;
                                                        <asp:Label ID="lb_title_approve" runat="server" Text=""></asp:Label>
                                                        </b>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal" role="form">
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" class="col-md-2 control-label" Text="ผลอนุมัติ : " />
                                                                <div class="col-md-3">
                                                                    <asp:DropDownList ID="ddlStatusapprove" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Text="ผลการอนุมัติ....." Value="0" />
                                                                        <asp:ListItem Text="อนุมัติ" Value="5" />
                                                                        <asp:ListItem Text="กลับไปแก้ไข" Value="6" />
                                                                        <%--<asp:ListItem Value="7" Text="ไม่อนุมัติ" />--%>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RqddlStatusapprove" runat="server" ControlToValidate="ddlStatusapprove" Display="None" ErrorMessage="กรุณากรอกผลอนุมัติ" InitialValue="0" SetFocusOnError="true" ValidationGroup="SaveAdd_BuyList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlStatusapprove" Width="180" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" class="col-md-2 control-label" Text="ความคิดเห็นเพิ่มเติม : " />
                                                                <div class="col-md-8">
                                                                    <asp:TextBox ID="txt_remark" runat="server" CssClass="form-control" Rows="4"
                                                                        TextMode="multiline"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="Rqtxt_remark" runat="server"
                                                                        ControlToValidate="txt_remark" Display="None" ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                        Font-Size="11" SetFocusOnError="true" ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                        ValidationGroup="SaveAdd_BuyList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender224" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxt_remark" Width="160" />
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                                        ControlToValidate="txt_remark" Display="None" ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                                                        Font-Size="11" SetFocusOnError="true" ValidationExpression="^[\s\S]{0,1000}$"
                                                                        ValidationGroup="SaveAdd_BuyList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender225"
                                                                        runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight"
                                                                        TargetControlID="RegularExpressionValidator1"
                                                                        Width="160" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" runat="server" CommandName="CmdInsert_it" CssClass="btn btn-success btn-sm" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" title="Save" ValidationGroup="SaveAdd_BuyList"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" runat="server" CommandName="BtnBack_viewit" CssClass="btn btn-default  btn-sm" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" title="Cancel" ValidationGroup="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- History it--%>
                        <%--<div class="form-group">

                            <div class="col-md-12">--%>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i aria-hidden="true" class="fa fa-list-ul"></i>&nbsp; <b>สถานะดำเนินการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rpt_history" runat="server">
                                    <HeaderTemplate>
                                        <div class="row">
                                            <label class="col-sm-2 control-label">
                                                วันที่ดำเนินการ</label>
                                            <label class="col-sm-3 control-label">
                                                ผู้ดำเนินการ</label>
                                            <label class="col-sm-2 control-label">
                                                ดำเนินการ</label>
                                            <label class="col-sm-2 control-label">
                                                ผลการดำเนินการ</label>
                                            <label class="col-sm-3 control-label">
                                                หมายเหตุ</label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <span><%# Eval("zdate") %></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <span><%# Eval("actor_name") %></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><%# Eval("node_name") %></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><%# Eval("status_name") %></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <span><%# Eval("approve_remark") %></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <%-- </div>
                        </div>--%>
                    </EditItemTemplate>
                </asp:FormView>

            </div>
        </asp:View>


        <asp:View ID="ViewAsset_Master" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i><strong>&nbsp; Class Asset</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">

                            <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Class" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>

                        <%------------------------ Div ADD  ------------------------%>

                        <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Class</strong></h4>
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Class" CssClass="col-md-3 control-label text_right"></asp:Label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveMaster" runat="server" Display="None" ControlToValidate="txtcode" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="SaveMaster" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcode"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="Asset class description" CssClass="col-md-3 control-label text_right"></asp:Label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveMaster" runat="server" Display="None" ControlToValidate="txtname" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveMaster" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtname"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                            <div class="col-md-6">
                                                <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-md-offset-3">
                                                <asp:LinkButton ID="lbladd" ValidationGroup="SaveMaster" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAddMaster" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        </asp:Panel>



                        <asp:GridView ID="GvMaster" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="primary"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            DataKeyNames="clidx"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowUpdating="Master_RowUpdating">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="#">

                                    <ItemTemplate>
                                        <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("clidx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>


                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtclidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("clidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("code_class")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtname_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="Code" CssClass="col-md-3 control-label text_right"></asp:Label>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtcode_edit" runat="server" CssClass="form-control" Text='<%# Eval("code_desc")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcode_edit" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtcode_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-md-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-md-8">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("code_status") %>'>
                                                            <asp:ListItem Value="1" Text="Online" />
                                                            <asp:ListItem Value="0" Text="Offline" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-2 col-md-offset-10">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("code_class") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("code_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("Class_status") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("clidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>

                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12" id="gvapprove" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-check"></i><strong>&nbsp; รายการรอนอุมัติ</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <table id="PanelBody_YrChkBoxsColumns1" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รหัสเอกสาร</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">วันที่สร้าง</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ข้อมูลผู้ทำรายการ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">หมายเหตุ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">สถานะ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รายละเอียด</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">DP610001</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">04/04/2018 12:30:45</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">นางสาว กานต์ธิดา ตระกูลบุญรักษ์</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">ทดสอบระบบ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">พิจารณาผลโดยหัวหน้าผู้สร้าง</label></td>
                                        <td>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailapprove" OnCommand="btnCommand"><i class="fa fa-file-text-o" ></i></asp:LinkButton>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>


                            <%--     <asp:GridView ID="GvListBudget"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="URQIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            
                                                <asp:Literal ID="ltSys1" runat="server" Text="DP610001" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            
                                                <asp:Literal ID="ltSys1" runat="server" Text="IT" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            
                                                <strong>
                                                    <asp:Label ID="Label12" runat="server">วันที่: </asp:Label></strong>
                                                <asp:Literal ID="Literal4" runat="server" Text="04/04/2018" />
                                                </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">เวลา: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text="12:30:45" />
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            
                                                
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                    <strong>
                                                        <asp:Label ID="Label14" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text="พัฒนาแอพพลิเคชั่น" />
                                                    <strong>
                                                        <asp:Label ID="Label16" runat="server">ชื่อผู้ทำรายละเอียด: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text="นางสาว กานต์ธิดา ตระกูลบุญรักษ์" />
                                                
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            
                                                <asp:Label ID="lblsecname" runat="server" Text="ทดสอบระบบ"></asp:Label>
                                            
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            
                                                <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"></asp:LinkButton>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailapprove" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Approve" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" Text="58000070" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-md-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-md-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-md-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtrequesdept" Text="การจัดการระบบสารสนเทศ" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-md-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtsec" Text="พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-md-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtpos" Text="เจ้าหน้าที่พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-md-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Text="0924235995" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-md-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-md-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Text="webmaster@taokaenoi.co.th" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <hr />

                    <asp:FormView ID="FvDetail_ShowApprove" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-md-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtlist_buynew" Enabled="false" Text="DP610001" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div id="div_showreasoncut" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label21" CssClass="col-md-2 control-label" runat="server" Text="เลขที่รายการแจ้งซ่อม : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="TextBox18" Enabled="false" Text="IT01610001" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label22" CssClass="col-md-2 control-label" runat="server" Text="สาเหตุ : " />
                                            <div class="col-md-9">
                                                <asp:TextBox ID="TextBox19" Enabled="false" TextMode="multiline" Rows="3" Text="เนื่องจากระยะเวลาในการใช้งาน และเครื่องโน๊ตบุ๊คเกิดการเสื่อมสภาพ" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtremark" Text="ทดสอบระบบ" Enabled="false" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-9">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">Notebook ram 16 gb hdd 1 tb</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">35000</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.549085903</label></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label20" class="col-md-2 control-label" runat="server" Text="สถานะดำเนินการ : " />
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlapprove" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะดำเนินการ...</asp:ListItem>
                                                <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label16" class="col-md-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                        <div class="col-md-9">
                                            <asp:TextBox ID="TextBox17" TextMode="multiline" Rows="4" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewapprove" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>



        </asp:View>

        <asp:View ID="ViewDataAsset" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-import"></i><strong>&nbsp; Import Data Asset</strong></h3>
                    </div>

                    <div class="panel-body">

                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <%--<div class="col-md-1">--%>
                                    <asp:LinkButton ID="CmdAdd_Import" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Import Asset" runat="server" CommandName="CmdAdd_Import" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                    <asp:LinkButton ID="CmdAdd_SearchAsset" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search Asset" runat="server" CommandName="CmdAdd_SearchAsset" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>

                                    <%-- </div>
                            <div class="col-md-1">
                            </div>--%>
                                </div>
                            </ContentTemplate>

                            <Triggers>
                                <asp:PostBackTrigger ControlID="CmdAdd_Import" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <%------------------------ Div ADD  ------------------------%>

                        <asp:Panel ID="Panel_ImportAdd" runat="server" Visible="false">

                            <asp:UpdatePanel ID="upActor1Node1Files11" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">แนบไฟล์ Excel</label>
                                        <div class="col-md-10">
                                            <asp:FileUpload ID="upload" runat="server" AutoPostBack="false" />

                                            <asp:RequiredFieldValidator ID="requiredFileUpload"
                                                runat="server" ValidationGroup="Create_Actor1file"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="upload"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณาเลือกไฟล์" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredFileUpload" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                    </div>

                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImport" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel ID="upActor1Node1Files1" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="col-md-2  col-md-offset-2">
                                            <asp:LinkButton ID="btnImport" ValidationGroup="SaveMaster" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnImport" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i> Import</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton13" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_Import" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImport" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <asp:Panel ID="Panel_SearchASSET" runat="server" Visible="false">

                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label69" CssClass="col-sm-2 control-label" runat="server" Text="ปี :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_yearasset" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาปี...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label70" class="col-sm-2 control-label" runat="server" Text="Cost Center : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_costcenterasset" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือก Cost Center...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label71" class="col-sm-2 control-label" runat="server" Text="Asset Number : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsearchasset" runat="server" CssClass="form-control"></asp:TextBox>

                                    </div>
                                    <asp:Label ID="Label72" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_locateasset" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานที่...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group">

                                    <div class="col-sm-5 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton4" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_importasset" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton5" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_Import" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>

                                </div>



                            </div>
                        </asp:Panel>
                    </div>

                    <div class="panel-body">
                        <asp:GridView ID="GvAsset_Buy" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            DataKeyNames="asidx"
                            PageSize="15"
                            OnRowDataBound="Master_RowDataBound"
                            OnPageIndexChanging="Master_PageIndexChanging">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">

                                    <ItemTemplate>
                                        <asp:Label ID="lblasidx" runat="server" Visible="false" Text='<%# Eval("asidx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("Asset") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("SNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="CostName" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbCostName" runat="server" Text='<%# Eval("CostCenter") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="LocName" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbLocName" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cap.Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbCapDate" runat="server" Text='<%# Eval("CapDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Asset Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbassetdes" runat="server" Text='<%# Eval("Asset_Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Acquis.Val." ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbacq" runat="server" Text='<%# Eval("AcquisVal") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbstatus" runat="server" Text='<%# getStatus((int)Eval("Asset_Status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>

                    </div>
                </div>


            </div>

        </asp:View>
        <%-- teppanop --%>

        <%--view การจัดการข้อมูลต่างๆ --%>

        <asp:View ID="viewmanage_purchasequipment" runat="server">
            <asp:HyperLink ID="setFocus_viewManage" runat="server"></asp:HyperLink>
            <div class="col-sm-12" runat="server" id="content_manage_purchasequipment">

                <%--                <div class="form-group">

                    <asp:LinkButton ID="LinkButton5" runat="server"
                        CommandName="viewmanu1" CssClass="btn btn-default" data-toggle="tooltip" title="ย้อนกลับ"
                        OnCommand="btn_command"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>&nbsp;ย้อนกลับ</asp:LinkButton>
                </div>--%>

                <asp:FormView ID="fvinformation_purchasequipment" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-user"></i>&nbsp;
                        <b>รายละเอียดข้อมูลผู้ขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lb_emps_code_purchase" Text='<%# Eval("emp_code") %>' runat="server" />
                                        </div>

                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล  :</label>
                                        <div class="col-sm-4 control-label textleft">

                                            <asp:Label ID="fullname_purchase_approve" Text='<%# Eval("emp_name_th") %>' runat="server" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Label7" Text='<%# Eval("org_name_th") %>' runat="server" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lbdreptnamepurchase" Text='<%# Eval("dept_name_th") %>' runat="server" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Label9" Text='<%# Eval("sec_name_th") %>' runat="server" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Label10" Text='<%# Eval("pos_name_th") %>' runat="server" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Label11" Text='<%# Eval("emp_mobile_no") %>' runat="server" />
                                        </div>
                                        <label class="col-sm-2 control-label">e-mail :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lbemail" Text='<%# Eval("emp_email") %>' runat="server" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>

            </div>

            <div class="col-sm-12" runat="server" id="Div1">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>รายละเอียดรายการขอซื้ออุปกรณ์</b>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvdetails_purchasequipment" OnDataBound="FvDetail_DataBound"
                                runat="server" DefaultMode="Edit" Width="100%">
                                <EditItemTemplate>

                                    <asp:HiddenField ID="hidden_m0_node" runat="server" Value='<%# Eval("node_idx") %>' />
                                    <asp:HiddenField ID="hidden_m0_actor" runat="server" Value='<%# Eval("actor_idx") %>' />
                                    <asp:HiddenField ID="hidden_emp_idx" runat="server" Value='<%# Eval("emp_idx_its") %>' />
                                    <asp:HiddenField ID="hidden_emp_director" runat="server" Value='<%# Eval("emp_idx_director") %>' />
                                    <asp:HiddenField ID="hidden_status_node" runat="server" Value='<%# Eval("doc_status") %>' />
                                    <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                    <asp:HiddenField ID="hidden_actor_name" runat="server" Value='<%# Eval("actor_name") %>' />

                                    <asp:TextBox ID="txt_hidden_sysidx" Text='<%# Eval("sysidx") %>' runat="server" Visible="false"
                                        CssClass="form-control"></asp:TextBox>

                                    <asp:TextBox ID="txt_rdept_idx_its" Text='<%# Eval("rdept_idx_its") %>' runat="server" Visible="false"
                                        CssClass="form-control"></asp:TextBox>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสเอกสาร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"></asp:Label>
                                            <asp:Label ID="hidden_name_actor" Visible="false" runat="server" Text='<%# Eval("actor_name") %>' />
                                        </div>
                                        <label class="col-sm-2 control-label">วันที่ขอซื้อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:Label ID="Labellld" Text='<%# Eval("zdocdate") %>' runat="server"></asp:Label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เหตุผลการขอซื้อ :</label>
                                        <div class="col-sm-10 control-label textleft">
                                            <asp:Label ID="Label15" Text='<%# Eval("remark") %>' runat="server"></asp:Label>
                                        </div>
                                    </div>


                                    </div>

                                </EditItemTemplate>
                            </asp:FormView>
                            <br />
                            <%-- Items purchase--%>
                            <div class="form-group">



                                <div class="col-sm-12">

                                    <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                        <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                    </blockquote>
                                    <asp:GridView ID="gvItemsIts" Visible="true" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                                        HeaderStyle-CssClass="info"
                                        GridLines="None"
                                        HeaderStyle-Height="30px" AllowPaging="true"
                                        Font-Size="9"
                                        HeaderStyle-Font-Size="9"
                                        OnRowDataBound="gvRowDataBound">


                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                    <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />
                                                    <%# (Container.DataItemIndex +1) %>
                                                    <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                                    <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                    <asp:Label ID="_lbflow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                                    <asp:Label ID="_lbm1_ref_idx" runat="server" Text='<%# Eval("m1_ref_idx") %>' Visible="false" />
                                                    <asp:Label ID="_lbposidx" runat="server" Text='<%# Eval("posidx") %>' Visible="false" />

                                                    <asp:Label ID="_lbsysidx" runat="server" Text='<%# Eval("sysidx") %>' Visible="false" />

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ประเภทการขอซื้อ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>


                                                    <asp:Label ID="items_type1" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>

                                                    <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                    <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>


                                                    <asp:Label ID="Label42" runat="server" CssClass="col-sm-12">
                                                        <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                        </p>
                                                    </asp:Label>

                                                    <asp:Label ID="lb_typidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                        </p>
                                                    </asp:Label>

                                                    <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12">
                                                        <p><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("zdevices_name") %></p>
                                                    </asp:Label>

                                                    <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                        <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                    </asp:Label>

                                                    <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                        <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                    </asp:Label>

                                                    <asp:Label ID="Label43" runat="server" CssClass="col-sm-12">
                                                        <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                    </asp:Label>

                                                    <asp:Label ID="Label84" runat="server" CssClass="col-sm-12">
                                                                <p><b>ประเภทกลุ่มพนักงาน:</b> &nbsp;<%# Eval("pos_name") %></p>
                                                    </asp:Label>

                                                    <asp:Label ID="Label44" runat="server" CssClass="col-sm-12">
                                                        <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                    </asp:Label>
                                                    <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                                                        <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                    </asp:Label>
                                                    <asp:Label ID="Label40" runat="server" CssClass="col-sm-12">
                                                        <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                    </asp:Label>
                                                    <asp:Label ID="Label46" runat="server" CssClass="col-sm-12">
                                                        <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %>&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %></p>
                                                    </asp:Label>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                        Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="items_price" runat="server" CssClass="col-sm-12"
                                                        Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>
                                                    <%--<asp:TextBox ID="txt_price" runat="server"  TextMode="Number"
                                                         Text='<%# Eval("price") %>' CssClass="form-control pull-left"
                                                        ></asp:TextBox>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รวม" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12"
                                                        Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>
                                                    <%-- <asp:TextBox ID="txt_total_amount" runat="server"  TextMode="Number"
                                                         Text='<%# Eval("total_amount") %>' CssClass="form-control pull-left"
                                                        ></asp:TextBox>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="งบประมาณที่ตั้งไว้" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12"
                                                        Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>
                                                    <%-- <asp:TextBox ID="txt_budget_set" runat="server" TextMode="Number"
                                                         Text='<%# Eval("budget_set") %>' CssClass="form-control pull-left"
                                                        ></asp:TextBox>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderText="Asset No." ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>


                                                    <asp:GridView ID="gvAssetNo" Visible="true" runat="server"
                                                        AutoGenerateColumns="false"
                                                        GridLines="None"
                                                        HeaderStyle-CssClass="default" HeaderStyle-Height="10px"
                                                        AllowPaging="true"
                                                        ShowHeader="false"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        OnRowDataBound="gvRowDataBound"
                                                        CssClass="table table-striped table-responsive word-wrap">
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ยังไม่มีรายการ Asset No</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%"
                                                                ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                                Visible="false">
                                                                <ItemTemplate>


                                                                    <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                                                    <%--<br />--%>
                                                                    <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />


                                                                    <asp:Label ID="idx2reportrr" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="assetno" HeaderStyle-CssClass="text-center"
                                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="5%"
                                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lb_assetno" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("asset_no") %>' />


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderText="Spec"
                                                ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Width="10%">
                                                <ItemTemplate>


                                                    <asp:Label ID="lb_spec_name" runat="server" Font-Size="Small" Visible="true" Text='<%# Eval("spec_remark") %>' />


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                HeaderStyle-Font-Size="Small" HeaderText="การจัดการ"
                                                ItemStyle-HorizontalAlign="Center"
                                                HeaderStyle-Width="5%">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="btnEditPrice" CssClass="btn btn-primary btn-sm" runat="server"
                                                        CommandName="btnEditPrice"
                                                        OnCommand="btnCommand"
                                                        CommandArgument='<%#
                                                                    Eval("u0idx")+ "|" + 
                                                                    Eval("u1idx")
                                                                %>'
                                                        data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">

                                    <asp:GridView ID="gvFile" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="gvRowDataBound">

                                        <Columns>
                                            <asp:TemplateField HeaderText="เอกสารในการขอซื้อ">
                                                <ItemTemplate>
                                                    <div class="col-lg-11">
                                                        <asp:Label ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                    </div>
                                                    <div class="col-lg-1">
                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm pull-right" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                        <asp:Label ID="lb_Download" runat="server" Visible="false"></asp:Label>
                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                    </div>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                            <asp:FormView ID="fvmanage_approvepurchase" OnDataBound="FvDetail_DataBound"
                                runat="server" DefaultMode="Insert" Width="100%">
                                <InsertItemTemplate>

                                    <asp:Panel ID="Panel_body_approve" runat="server">

                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                            <b>ดำเนินการขอซื้ออุปกรณ์&nbsp;
                            <asp:Label ID="lb_title_approve" runat="server" Text=""></asp:Label></b>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <div class="form-group">
                                                        <asp:Label ID="Label29" class="col-md-2 control-label"
                                                            Font-Bold="true"
                                                            runat="server" Text="ผลอนุมัติ : " />
                                                        <div class="col-md-3">
                                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Value="0" Text="ผลการอนุมัติ....." />
                                                                        <asp:ListItem Value="5" Text="อนุมัติ" />
                                                                        <asp:ListItem Value="6" Text="กลับไปแก้ไข" />
                                                                        <%--<asp:ListItem Value="7" Text="ไม่อนุมัติ" />--%>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="RqddlStatusapprove" runat="server" InitialValue="0"
                                                                        ControlToValidate="ddlStatusapprove" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" ValidationGroup="btnApproveDirector" />
                                                                    <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="RqddlStatusapprove" Width="180" />--%>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label47" class="col-md-2 control-label"
                                                            Font-Bold="true"
                                                            runat="server" Text="แสดงความคิดเห็น : " />
                                                        <div class="col-md-10">
                                                            <asp:UpdatePanel ID="panelequipmenttype" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtcomment_its" TextMode="MultiLine" Rows="4" runat="server" CssClass="form-control"
                                                                        placeholder="แสดงความคิดเห็น..." />

                                                                    <asp:RequiredFieldValidator ID="RequiredFieldtxtcomment_its" runat="server"
                                                                        ControlToValidate="txtcomment_its" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาแสดงความคิดเห็น" ValidationGroup="btnApproveDirector" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxtcomment_its" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight"
                                                                        TargetControlID="RequiredFieldtxtcomment_its"
                                                                        PopupPosition="topLeft" Width="180" />


                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">

                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnApproveDirector"
                                                CssClass="btn btn-success" runat="server"
                                                ValidationGroup="btnApproveDirector"
                                                OnCommand="btnCommand" CommandName="btnApproveDirector"
                                                CommandArgument="0"
                                                data-toggle="tooltip" title="ให้ผลการอนุมัติ"
                                                OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')">
                                                <i class="fa fa-save"></i>

                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnApproveDirector_back"
                                                CssClass="btn btn-default" runat="server"
                                                OnCommand="btnCommand" CommandName="BtnBack"
                                                CommandArgument="0"
                                                data-toggle="tooltip" title="Close">
                                                <i class="fa fa-close"></i>

                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>
                </div>
            </div>


            <%-- IT support result purchase--%>
            <div class="col-sm-12" runat="server" id="content_result_itsupport">
                <asp:FormView ID="fvresultpurchase" Visible="false" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>Result purchase</b> (สรุปรายการ)
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <asp:Panel ID="Panel_list_purchase" runat="server" Visible="true">
                                            <div class="alert alert-success" role="alert">
                                                <asp:Label ID="label_result_list" runat="server">
                                            <i class="fa fa-check" aria-hidden="true"></i> &nbsp;<strong>ราคาประมาณ</strong>&nbsp;<%# Eval("price_purchase") %> &nbsp; บาท&nbsp;
                                            <strong>สรุปผลโดย</strong> &nbsp;แผนก IT Support
                                                </asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                    <div class="row">
                                        <asp:Panel ID="Panel_costcenter" runat="server" Visible="true">
                                            <div class="alert alert-success" role="alert">
                                                <asp:Label ID="label3" runat="server">
                                            <i class="fa fa-check" aria-hidden="true"></i> &nbsp;<strong>costcenter</strong> :&nbsp;<%# Eval("costcenter_no") %> &nbsp;
                                            <strong>สรุปผลโดย</strong> &nbsp;ฝ่าย งบประมาณและวิเคราะห์
                                                </asp:Label>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>

            <div class="col-sm-12" runat="server" id="content_Summary_items" visible="false">
                <asp:FormView ID="fvsummary_itemspurchase" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <asp:HiddenField ID="hidden_node" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hidden_actor" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>สรุปรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="row">
                                        <asp:GridView ID="gvlist_summarypurchase" runat="server" CssClass="table table-striped table-hover " HeaderStyle-CssClass="info" GridLines="None"
                                            AutoGenerateColumns="false" Visible="true"
                                            OnRowDataBound="gvRowDataBound">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">

                                                    <div class="alert alert-warning">
                                                        ยังไม่มีรายการขอซื้ออุปกรณ์
                                                    </div>
                                                </div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                        <asp:Label ID="label_idx_purchase" runat="server" Visible="false" Text='<%# Eval("u0_purchase_idx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lb_empCode" runat="server" CssClass="col-sm-12">
                                                <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("empcode_purchase") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_fullName" runat="server" CssClass="col-sm-12">
                                                <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("fullname_purchase") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_orgName" runat="server" CssClass="col-sm-12">
                                                    <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_purchase") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_dpart" runat="server" CssClass="col-sm-12">
                                                <p><b>ฝ่าย:</b> &nbsp;<%# Eval("drept_name_purchase") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_section" runat="server" CssClass="col-sm-12">
                                                <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_purchase") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="lb_position" runat="server" CssClass="col-sm-12">
                                                <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_purchase") %></p>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="รายละเอียดการขอซื้อ" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="actor_idx" runat="server" Visible="true" Text='<%# Eval("m0_actor_idx") %>' />
                                                        <asp:Label ID="node_idx" runat="server" Visible="true" Text='<%# Eval("m0_node_idx") %>' />
                                                        <asp:Label ID="documentcode" runat="server" CssClass="col-sm-12">
                                                <p><b>รหัสเอกสาร:</b> &nbsp;<%# Eval("document_code") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="date_purchase" runat="server" CssClass="col-sm-12">
                                                <p><b>วันที่ขอซื้อ:</b> &nbsp;<%# Eval("date_purchasequipment") %></p>
                                                        </asp:Label>
                                                        <%--  <asp:Label ID="lb_equipment" runat="server" CssClass="col-sm-12">
                                                <p><b>ประเภทอุปกรณ์ที่ซื้อ:</b> &nbsp;<%# Eval("name_equipment") %></p>
                                                    </asp:Label>--%>
                                                        <asp:Label ID="lb_purchase" runat="server" CssClass="col-sm-12">
                                                <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("name_purchase") %></p>
                                                        </asp:Label>
                                                        <%--   <asp:Label ID="quantity_equipment" runat="server" CssClass="col-sm-12">
                                                <p><b>จำนวนเครื่อง:</b> &nbsp;<%# Eval("quantity_equipment") %></p>
                                                    </asp:Label>--%>
                                                        <asp:Label ID="label_details_purchase" runat="server" CssClass="col-sm-12">
                                                <p><b>เหตุผลการขอซื้อ:</b> &nbsp;<%# Eval("details_purchase") %></p>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ราคาของอุปกรณ์" HeaderStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <div class="col-sm-12">
                                                            <asp:DropDownList ID="ddlprice_equiptment"
                                                                runat="server" CssClass="form-control fa-align-left"
                                                                AutoPostBack="true" Enabled="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <asp:LinkButton CssClass="btn btn-success pull-right" Visible="true" ID="btnsaveapprove1" data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                                CommandName="sentlistpurchase" OnCommand="btnCommand"
                                                OnClientClick="return confirm('คุณต้องการบันทึกรายการขอซื้อนี้ใช่หรือไม่ ?')"> ส่งสรุปรายการขอซื้อ</asp:LinkButton>
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>
            <%-- History purchase--%>
            <div class="col-sm-12" runat="server" id="content_history_purchase">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                        <b>สถานะดำเนินการขอซื้ออุปกรณ์</b>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="history_purchasequipment" runat="server">
                            <HeaderTemplate>
                                <div class="row">
                                    <label class="col-sm-2 control-label">วันที่ดำเนินการ</label>
                                    <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                    <label class="col-sm-2 control-label">ดำเนินการ</label>
                                    <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                    <label class="col-sm-3 control-label">หมายเหตุ</label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <span><%# Eval("zdate") %></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span><%# Eval("actor_name") %></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><%# Eval("node_name") %></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><%# Eval("status_name") %></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span><%# Eval("approve_remark") %></span>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="View_docket" runat="server">

            <div class="col-sm-12" runat="server" id="Div2" visible="true">
                <asp:FormView ID="Fv_docket" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>
                        <%-- <asp:HiddenField ID="hidden_node" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hidden_actor" runat="server" Value='<%# Eval("m0_actor_idx") %>' />--%>

                        <%--  ค้นหา--%>
                        <div class="row">
                            <asp:Panel ID="panel_search" runat="server" Visible="true">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <i class="glyphicon glyphicon-search"></i>&nbsp;
                                 <b>search</b> (ค้นหารายการ)
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <%-- เงื่อนไขการค้นหา--%>

                                            <div class="form-group">

                                                <%--                                                                                                <asp:Label ID="Label25" CssClass="col-sm-1 control-label" runat="server" Text="Spec : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlsearch_spec"
                                                        runat="server" CssClass="form-control fa-align-left">
                                                    </asp:DropDownList>
                                                </div>--%>

                                                <asp:Label ID="Label251" CssClass="col-sm-1 control-label" runat="server" Text="เดือน : " />
                                                <div class="col-sm-2">

                                                    <asp:DropDownList ID="ddlMonthSearch" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                </div>

                                                <asp:Label ID="Label52" CssClass="col-sm-1 control-label" runat="server" Text="ปี : " />
                                                <div class="col-sm-2">

                                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                </div>

                                                <div class="col-sm-1">
                                                    <asp:LinkButton CssClass="btn btn-info btn-sm" ID="btnsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                        CommandName="searching_docket" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>

                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>สรุปรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%--  select all อนุมัติทั้งหมด --%>
                                    <asp:Panel ID="Panel_approve" runat="server" Visible="false">

                                        <div class="panel panel-default">
                                            <!-- Add Panel Heading Here -->
                                            <div class="panel-body">

                                                <div class="form-group">
                                                    <asp:Label ID="Label53" class="col-md-2 control-label"
                                                        Font-Bold="true"
                                                        runat="server" Text="" />
                                                    <div class="col-md-10">
                                                        <asp:CheckBox ID="check_approve_docket_all" runat="server" AutoPostBack="true"
                                                            RepeatDirection="Vertical" CssClass="textleft"
                                                            OnCheckedChanged="checkindexchange" />&nbsp;
                                                 <strong>เลือกทั้งหมด</strong>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label47" class="col-md-2 control-label"
                                                        Font-Bold="true"
                                                        runat="server" Text="แสดงความคิดเห็น : " />
                                                    <div class="col-md-8">

                                                        <asp:TextBox ID="txtcomment_docket" TextMode="MultiLine"
                                                            Rows="4"
                                                            runat="server" CssClass="form-control"
                                                            placeholder="แสดงความคิดเห็น..." />


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <asp:LinkButton CssClass="btn btn-success pull-right" ID="btnapproveall"
                                                            data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                                            CommandName="confirm_approve_docket" Visible="true"
                                                            OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')"
                                                            OnCommand="btnCommand"><i class="fa fa-save"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </asp:Panel>
                                    <div class="row">
                                        <asp:GridView ID="gvlist_docket" runat="server"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info" GridLines="None"
                                            AutoGenerateColumns="false" AllowPaging="true" PageSize="15"
                                            OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                            </EmptyDataTemplate>

                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Small"
                                                    Visible="false">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_flow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                                        <asp:CheckBox ID="cbrecipients_docket" runat="server"
                                                            AutoPostBack="true" Visible="true"
                                                            OnCheckedChanged="checkindexchange"
                                                            Text='<%# Container.DataItemIndex %>'
                                                            Style="color: transparent;"></asp:CheckBox>
                                                        <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />
                                                        <asp:Label ID="lb_Selected" runat="server" Text="" Visible="false" />

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("doccode") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่เอกสาร" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("zdocdate") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เดือน / ปี ที่ขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="15%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_month_year" runat="server" CssClass="col-sm-12"
                                                            Text='<%# zsetMonthYear((int)Eval("zmonth"),(int)Eval("zyear")) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="กลุ่มอุปกรณ์" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("zdevices_name") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <%--  --%>

                                                <asp:TemplateField HeaderText="รอบการขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_docket_item" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("docket_item") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_SysNameTH" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("SysNameTH") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--  --%>

                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%" HeaderText="จำนวน"
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%" HeaderText="หน่วย"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_NameTH" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("NameTH") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Spec" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_spec_remark" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("spec_remark") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <%-- ใบปะหน้า --%>




                                                <%-- ส่งมอบ --%>

                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%" HeaderText="ส่งมอบ"
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_out_qty" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((int)Eval("out_qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%" HeaderText="คงเหลือ"
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_balance_qty" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((int)Eval("balance_qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <%-- จำนวนเงิน --%>

                                                <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%" HeaderText="จำนวนเงิน"
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_total_amount" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((decimal)Eval("amount")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="updatepnl_docket" runat="server">
                                                            <ContentTemplate>

                                                                <asp:LinkButton ID="btnmanage_its_docket" CssClass="btn btn-info btn-sm" runat="server"
                                                                    data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                                                    CommandName="manage_its_docket"
                                                                    CommandArgument='<%#
                                                                    Eval("zmonth")+ "|" + 
                                                                    Eval("zyear")+ "|" + 
                                                                    Eval("sysidx")+ "|" + 
                                                                  Eval("typidx")+ "|" + 
                                                                  Eval("tdidx")+ "|" + 
                                                                  Eval("spec_remark")+ "|" + 
                                                                  Eval("zu0idx") + "|" + 
                                                                  Eval("zu1idx")
                                                                %>'>
                                                                    <i class="fa fa-file"></i>
                                                                </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnmanage_its_docket" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="updatepnl_docket_delivery" runat="server">
                                                            <ContentTemplate>

                                                                <asp:LinkButton ID="btnmanage_its_docket_delivery" CssClass="btn btn-info btn-sm" runat="server"
                                                                    data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                                                    CommandName="manage_its_docket_delivery"
                                                                    CommandArgument='<%#
                                                                  Eval("typidx")+ "|" + 
                                                                  Eval("tdidx")+ "|" + 
                                                                  Eval("spec_idx")+ "|" + 
                                                                  Eval("u0_docket_idx") 
                                                                %>'>
                                                                    <i class="fa fa-file"></i>
                                                                </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnmanage_its_docket_delivery" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>
        <asp:View ID="View_docket_list" runat="server">

            <asp:FormView ID="FvDetail_Showdocket_list" runat="server" Width="100%"
                OnDataBound="FvDetail_DataBound"
                DefaultMode="Insert">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการขอซื้ออุปกรณ์</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">



                                <div class="form-group">

                                    <div class="col-md-12">

                                        <asp:GridView ID="gvItemsIts_docketlist" Visible="true" runat="server"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info" GridLines="None"
                                            AutoGenerateColumns="false" AllowPaging="true" PageSize="10"
                                            OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                            </EmptyDataTemplate>

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="3%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                        <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />

                                                        <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                        <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รหัสเอกสาร/วันที่" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="10%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("doccode") %>'></asp:Label>
                                                        <br />

                                                        <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("zdocdate") %>'></asp:Label>

                                                        <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                        <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="35%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="Label42" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                        </asp:Label>

                                                        <asp:Label ID="items_type1" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                        </asp:Label>

                                                        <asp:Label ID="Label41" runat="server" CssClass="col-sm-12">
                                                                <p><b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                                </p>
                                                        </asp:Label>

                                                        <asp:Label ID="Label78" runat="server" CssClass="col-sm-12">
                                                                <p><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("zdevices_name") %></p>
                                                        </asp:Label>

                                                        <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                        </asp:Label>

                                                        <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                        </asp:Label>

                                                        <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                                                                <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emp_name_th") %></p>
                                                        </asp:Label>

                                                        <asp:Label ID="Label43" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                        </asp:Label>

                                                        <asp:Label ID="Label44" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="Label40" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                        </asp:Label>
                                                        <asp:Label ID="Label46" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %>&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="7%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_price" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รวม" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="7%"
                                                    Visible="false">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12"
                                                            Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="งบประมาณที่ตั้งไว้" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="9"
                                                    ControlStyle-Font-Size="10"
                                                    HeaderStyle-Width="7%"
                                                    Visible="true">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12"
                                                            Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2 col-md-offset-10">

                                        <asp:LinkButton ID="AddCancel" runat="server"
                                            CommandName="_divMenubtnIT_summary" CssClass="btn btn-default  btn-sm"
                                            OnCommand="btnCommand" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>

        </asp:View>

        <asp:View ID="View_docket_md" runat="server">

            <div class="col-sm-12" runat="server" id="Div3" visible="true">
                <asp:FormView ID="Fv_docket_md" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Insert" Width="100%">
                    <InsertItemTemplate>

                        <%--  ค้นหา--%>
                        <div class="row">
                            <asp:Panel ID="panel_search" runat="server" Visible="true">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <i class="glyphicon glyphicon-search"></i>&nbsp;
                                        <b>search</b> (ค้นหารายการ)
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <%-- เงื่อนไขการค้นหา--%>

                                            <%--<div class="form-group">
                                                <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา : " />
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlcondition_md" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                        <asp:ListItem Text="เลือกเงื่อนไข...." Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtstartdate" runat="server" placeholder="จากวันที่..."
                                                            CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>


                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtenddate" runat="server" Enabled="false" placeholder="ถึงวันที่..."
                                                            CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>

                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="Label251" CssClass="col-sm-2 control-label" runat="server" Text="เดือน : " />
                                                <div class="col-sm-2">

                                                    <asp:DropDownList ID="ddlMonthSearch" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                </div>

                                                <asp:Label ID="Label52" CssClass="col-sm-1 control-label" runat="server" Text="ปี : " />
                                                <div class="col-sm-2">

                                                    <asp:DropDownList ID="ddlYearSearch" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txtdocumentcode" runat="server" placeholder="รหัสเอกสาร"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>

                                                <%-- <asp:Label ID="Label75" CssClass="col-sm-1 control-label" runat="server" Text="ผลอนุมัติ : " />
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlStatusapproveSearch" runat="server" CssClass="form-control"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        >
                                                        <asp:ListItem Text="รอดำเนินการ....." Value="1" Selected="True" />
                                                        <asp:ListItem Text="อนุมัติ" Value="5" />
                                                        <asp:ListItem Text="กลับไปแก้ไข" Value="6" />
                                                    </asp:DropDownList>
                                                </div>--%>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-lg-2 col-lg-offset-2">
                                                    <asp:LinkButton CssClass="btn btn-info btn-sm" ID="btnsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                        CommandName="searching_md" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton CssClass="btn btn-default btn-sm" ID="btnresetsearch" data-toggle="tooltip" title="รีเซ็ต" runat="server"
                                                        CommandName="reset_search_md" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-refresh"></i> รีเซ็ต</asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                        <b>สรุปรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%--  select all อนุมัติทั้งหมด --%>
                                    <asp:Panel ID="Panel_approve" runat="server" Visible="false">

                                        <div class="panel panel-default">
                                            <!-- Add Panel Heading Here -->
                                            <div class="panel-body">

                                                <div class="row">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label79" class="col-md-2 control-label"
                                                            runat="server" Text="" />
                                                        <div class="col-md-8">
                                                            <asp:CheckBox ID="check_approve_docket_md_all" runat="server" AutoPostBack="true"
                                                                RepeatDirection="Vertical" CssClass="textleft"
                                                                OnCheckedChanged="checkindexchange" />&nbsp;
                                                   <strong>เลือกทั้งหมด</strong>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group">
                                                        <asp:Label ID="Labedle1" class="col-md-2 control-label"
                                                            runat="server" Text="หมายเหตุ : "
                                                            Font-Bold="true" />
                                                        <div class="col-md-8">
                                                            <asp:TextBox ID="txtremark_approve_allmd" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="Requiredtxtremark_approve_allmd"
                                                                ValidationGroup="Saveapprove_allListmd" runat="server" Display="None"
                                                                ControlToValidate="txtremark_approve_allmd" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredtxtremark_approve_allmd" Width="160" />

                                                            <asp:RegularExpressionValidator ID="RegularExprestxtremark_approve_allmd" runat="server"
                                                                ValidationGroup="Saveapprove_allListmd" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtremark_approve_allmd"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtremark_approve_allmd" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="RegularExprestxtremark_approve_allmd" Width="160" />

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label80" class="col-md-2 control-label" runat="server" Text="" />
                                                        <div class="col-md-8">
                                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="btnapproveall"
                                                                data-toggle="tooltip" title="อนุมัติรายการ" runat="server"
                                                                CommandName="confirm_approve_md_docket" Visible="true"
                                                                ValidationGroup="Saveapprove_allListmd"
                                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')"
                                                                OnCommand="btnCommand">อนุมัติรายการ</asp:LinkButton>

                                                            <asp:LinkButton CssClass="btn btn-warning btn-sm" ID="btnnotapprove"
                                                                data-toggle="tooltip" title="กลับไปแก้ไข" runat="server"
                                                                CommandName="confirm_no_approve_md_docket" Visible="true"
                                                                ValidationGroup="Saveapprove_allListmd"
                                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการขอซื้อนี้ใช่หรือไม่ ?')"
                                                                OnCommand="btnCommand">กลับไปแก้ไข</asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </asp:Panel>
                                    <div class="row">
                                        <asp:GridView ID="gvlist_docket_md" runat="server"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info" GridLines="None"
                                            HeaderStyle-Height="30px"
                                            AutoGenerateColumns="false" AllowPaging="true" PageSize="5"
                                            OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                            </EmptyDataTemplate>

                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_itemindex" runat="server" CssClass="col-sm-12"
                                                            Text=' <%# (Container.DataItemIndex +1) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9"
                                                    Visible="false">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_flow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                                        <asp:CheckBox ID="cbrecipients_docket_md" runat="server"
                                                            AutoPostBack="true" Visible="true"
                                                            OnCheckedChanged="checkindexchange"
                                                            Text='<%# Container.DataItemIndex %>'
                                                            Style="color: transparent;"></asp:CheckBox>
                                                        <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />
                                                        <asp:Label ID="lb_Selected" runat="server" Text="" Visible="false" />

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9"
                                                    HeaderStyle-Width="15%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("doccode") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="เดือน / ปี ที่ขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_month_year" runat="server" CssClass="col-sm-12"
                                                            Text='<%# zsetMonthYear((int)Eval("zmonth"),(int)Eval("zyear")) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รอบการขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_docket_item" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("docket_item") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_SysNameTH" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("SysNameTH") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderStyle-CssClass="text-center"
                                                    HeaderText="จำนวนอุปกรณ์"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((int)Eval("total_qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="updatepnl_docket" runat="server">
                                                            <ContentTemplate>

                                                                <asp:LinkButton ID="btnmanage_its_docket_md" CssClass="btn btn-info btn-sm" runat="server"
                                                                    data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                                                    CommandName="manage_its_docket_md"
                                                                    CommandArgument='<%# 
                                                                    Eval("u0_docket_idx")
                                                                         %>'>
                                                                    <i class="fa fa-file"></i>
                                                                </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnmanage_its_docket_md" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center"
                                                    ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="updatepnl_docket_print" runat="server">
                                                            <ContentTemplate>

                                                                <asp:LinkButton ID="btnmanage_its_docket_md_print" CssClass="btn btn-info btn-sm" runat="server"
                                                                    data-toggle="tooltip" title="รายละเอียดการขอซื้อ" OnCommand="btnCommand"
                                                                    CommandName="manage_its_docket_md_print"
                                                                    CommandArgument='<%# 
                                                                    Eval("u0_docket_idx")
                                                                         %>'>
                                                                    <i class="fa fa-file"></i>
                                                                </asp:LinkButton>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnmanage_its_docket_md_print" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>

        <asp:View ID="View_docket_md_detial" runat="server">

            <div class="col-sm-12" id="Div5">
                <asp:FormView ID="Fv_docket_md_detail" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                                <b>รายละเอียดรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group" runat="server" visible="true">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รหัสเอกสาร : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                            <asp:TextBox ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="hidden_name_actor" runat="server"
                                                Value='<%# Eval("actor_name") %>' />
                                            <asp:HiddenField ID="hidden_flow_item" runat="server"
                                                Value='<%# Eval("flow_item") %>' />
                                            <asp:HiddenField ID="hidden_u0_docket_idx" runat="server"
                                                Value='<%# Eval("u0_docket_idx") %>' />
                                            <asp:HiddenField ID="hidden_u1_docket_idx" runat="server"
                                                Value='<%# Eval("u1_docket_idx") %>' />
                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="เดือน / ปี ที่ขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Labellld" Text='<%# zsetMonthYear((int)Eval("zmonth"),(int)Eval("zyear")) %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="true">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รอบการขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox2" Text='<%# getformatfloat(((int)Eval("docket_item")).ToString(),0) %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="ประเภททรัพย์สิน : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox3" Text='<%# Eval("SysNameTH") %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-12" runat="server" visible="false">
                                            <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                                <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                            </blockquote>
                                        </div>
                                        <div class="col-md-12">

                                            <asp:GridView ID="gvlist_docket_md_list" runat="server"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                HeaderStyle-CssClass="info" AllowPaging="true"
                                                AutoGenerateColumns="false" PageSize="5"
                                                HeaderStyle-Height="30px"
                                                OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                                </EmptyDataTemplate>

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%"
                                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                                        <ItemTemplate>

                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="กลุ่มอุปกรณ์" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("zdevices_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                        HeaderStyle-Width="7%" HeaderText="จำนวน"
                                                        ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12"
                                                                Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                        HeaderStyle-Width="10%" HeaderText="หน่วย"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lb_NameTH" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("NameTH") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Spec" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_spec_remark" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("spec_remark") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>

                                        <div class="col-md-12">
                                            <asp:Panel ID="Panel_body_approve" runat="server">
                                                <div class="form-group">

                                                    <div class="col-md-12">
                                                        <div class="panel panel-success">
                                                            <div class="panel-heading">
                                                                <i class="glyphicon glyphicon-file"></i>&nbsp; <b>ดำเนินการขอซื้ออุปกรณ์&nbsp;
                                                        <asp:Label ID="lb_title_approve" runat="server" Text=""></asp:Label>
                                                                </b>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label67" runat="server" class="col-md-2 control-label" Text="ผลอนุมัติ : " />
                                                                        <div class="col-md-3">
                                                                            <asp:DropDownList ID="ddlStatusapprove" runat="server" CssClass="form-control">
                                                                                <asp:ListItem Text="ผลการอนุมัติ....." Value="0" />
                                                                                <asp:ListItem Text="อนุมัติ" Value="5" />
                                                                                <asp:ListItem Text="กลับไปแก้ไข" Value="6" />
                                                                                <%--<asp:ListItem Value="7" Text="ไม่อนุมัติ" />--%>
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="RqddlStatusapprove" runat="server" ControlToValidate="ddlStatusapprove" Display="None" ErrorMessage="กรุณากรอกผลอนุมัติ" InitialValue="0" SetFocusOnError="true" ValidationGroup="SaveAdd_BuyList" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallddlposition" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlStatusapprove" Width="180" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label68" runat="server" class="col-md-2 control-label" Text="ความคิดเห็นเพิ่มเติม : " />
                                                                        <div class="col-md-8">
                                                                            <asp:TextBox ID="txt_remark" runat="server" CssClass="form-control" Rows="4"
                                                                                TextMode="multiline"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="Rqtxt_remark" runat="server"
                                                                                ControlToValidate="txt_remark" Display="None" ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                                Font-Size="11" SetFocusOnError="true" ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                                ValidationGroup="SaveAdd_BuyList" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender224" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxt_remark" Width="160" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                                                ControlToValidate="txt_remark" Display="None" ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                                                                Font-Size="11" SetFocusOnError="true" ValidationExpression="^[\s\S]{0,1000}$"
                                                                                ValidationGroup="SaveAdd_BuyList" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender225"
                                                                                runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                                TargetControlID="RegularExpressionValidator1"
                                                                                Width="160" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-2 col-md-offset-10">
                                                <asp:LinkButton ID="btnAdddata" runat="server" CommandName="Cmdmd_app"
                                                    CssClass="btn btn-success btn-sm"
                                                    OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                                    OnCommand="btnCommand" title="Save"
                                                    ValidationGroup="SaveAdd_BuyList"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="AddCancel" runat="server" CommandName="_divMenubtnMD_it" CssClass="btn btn-default  btn-sm" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" title="Cancel" ValidationGroup="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </EditItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>

        <asp:View ID="View_Delivery_Detail" runat="server">

            <div class="col-sm-12" id="DivDelivery_Detail">

                <asp:FormView ID="FvDetail_ShowDelivery" runat="server" Width="100%"
                    OnDataBound="FvDetail_DataBound"
                    DefaultMode="Edit">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการขอซื้ออุปกรณ์</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รหัสเอกสาร : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                            <asp:TextBox ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="hidden_name_actor" runat="server"
                                                Value='<%# Eval("actor_name") %>' />
                                            <asp:HiddenField ID="hidden_flow_item" runat="server"
                                                Value='<%# Eval("flow_item") %>' />
                                        </div>


                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="เดือน / ปี ที่ขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Labellld" Text='<%# zsetMonthYear((int)Eval("zmonth"),(int)Eval("zyear")) %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="true">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รอบการขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox2" Text='<%# getformatfloat(((int)Eval("docket_item")).ToString(),0) %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="ประเภททรัพย์สิน : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox3" Text='<%# Eval("SysNameTH") %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_sysidx" Text='<%# Eval("sysidx") %>' runat="server" Visible="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <div class="col-md-12">
                                            <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                                <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                            </blockquote>
                                        </div>
                                        <div class="col-md-12">

                                            <asp:GridView ID="gvItemsIts_Delivery" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive word-wrap"
                                                HeaderStyle-CssClass="info"
                                                GridLines="None"
                                                OnRowDataBound="gvRowDataBound">

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="3%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                            <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />

                                                            <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                            <asp:Label ID="_lbu2idx" runat="server" Visible="false" Text='<%# Eval("u2idx") %>' />

                                                            <asp:Label ID="_lbu0_docket_idx" runat="server" Visible="false" Text='<%# Eval("u0_docket_idx") %>' />

                                                            <asp:Label ID="_lbu1_docket_idx" runat="server" Visible="false" Text='<%# Eval("u1_docket_idx") %>' />

                                                            <asp:Label ID="_lbu2_docket_idx" runat="server" Visible="false" Text='<%# Eval("u2_docket_idx") %>' />

                                                            <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />

                                                            <asp:Label ID="_lbu0_didx" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>' />

                                                            <asp:Label ID="_lbsysidx" runat="server" Visible="false" Text='<%# Eval("sysidx") %>' />

                                                            <asp:Label ID="_lbtypidx" runat="server" Visible="false" Text='<%# Eval("typidx") %>' />

                                                            <asp:Label ID="_lbtdidx" runat="server" Visible="false" Text='<%# Eval("tdidx") %>' />

                                                            <asp:Label ID="lb_DataItemIndex" runat="server" Text='<%# (Container.DataItemIndex +1) %>' />

                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รหัสเอกสารขอซื้อ/วันที่เอกสารขอซื้อ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="10%">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><%# Eval("doccode") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><%# Eval("zdocdate") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_purchase_type_idx_name" runat="server" CssClass="col-sm-12" Visible="false"
                                                                Text='<%# Eval("purchase_type_idx_name") %>'></asp:Label>

                                                            <asp:Label ID="lbnodeidxItems" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                                            <asp:Label ID="lbactoridxItems" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="35%">
                                                        <ItemTemplate>


                                                            <asp:Label ID="Label42" runat="server" CssClass="col-sm-12" Visible="false">
                                                                 <p><b>ประเภททรัพย์สิน:</b> &nbsp;<%# Eval("sysidx_name") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="items_type1" runat="server" CssClass="col-sm-12" Visible="true">
                                                                 <p><b>ประเภทการขอซื้อ:</b> &nbsp;<%# Eval("purchase_type_idx_name") %>
                                                                  </p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label41" runat="server" CssClass="col-sm-12">
                                                                <p><%--<b>ประเภทอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("typidx_name") %>
                                                                &nbsp;&nbsp;--%><b>กลุ่มอุปกรณ์ขอซื้อ:</b> &nbsp;<%# Eval("zdevices_name") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ IO:</b> &nbsp;<%# Eval("ionum") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12">
                                                                <p><b>เลขที่ตัดเสีย:</b> &nbsp;<%# Eval("ref_itnum") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label43" runat="server" CssClass="col-sm-12">
                                                                <p><b>ขอซื้อให้พนักงานตำแหน่ง:</b> &nbsp;<%# Eval("rpos_idx_name") %></p>
                                                            </asp:Label>

                                                            <asp:Label ID="Label44" runat="server" CssClass="col-sm-12" Visible="true">
                                                                <p><b>สเปค:</b> &nbsp;<%# Eval("leveldevice") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                                                                <p><b>รายละเอียด:</b> &nbsp;<%# Eval("list_menu") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label40" runat="server" CssClass="col-sm-12" Visible="false">
                                                                <p><b>สกุลเงิน:</b> &nbsp;<%# Eval("currency_idx_name") %></p>
                                                            </asp:Label>
                                                            <asp:Label ID="Label46" runat="server" CssClass="col-sm-12">
                                                                <p><b>รหัสงบประมาณ:</b> &nbsp;<%# Eval("costcenter_idx_name") %>&nbsp;&nbsp;<b>สถานที่:</b> &nbsp;<%# Eval("place_idx_name") %></p>
                                                            </asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                                Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("unidx_name") %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ราคา" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        HeaderStyle-Width="7%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_price" runat="server" CssClass="col-sm-12"
                                                                Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รวม" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        Visible="false">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12"
                                                                Text='<%#  getformatfloat(((string)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="งบประมาณที่ตั้งไว้" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                                                        Visible="false">
                                                        <ItemTemplate>

                                                            <asp:Label ID="items_budget_set" runat="server" CssClass="col-sm-12"
                                                                Text='<%#  getformatfloat(((string)Eval("budget_set")).ToString(),2) %>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Asset No." HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                                        <ItemTemplate>


                                                            <asp:Label ID="Label48" runat="server" Visible="true" Text='<%# Eval("asset_no") %>' />
                                                            <asp:Label ID="_lbid" runat="server" Visible="false" Text='<%# Eval("id") %>' />


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="สถานะการส่งมอบ" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                                        <ItemTemplate>


                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <asp:Label ID="lb_pr_idx" runat="server" Visible="false" Text='<%# Eval("pr_idx") %>' />
                                                                    <asp:DropDownList ID="ddlpr_idx" CssClass="form-control" runat="server"
                                                                        AutoPostBack="true"
                                                                        OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <asp:TextBox ID="txt_pr_remark" TextMode="multiline"
                                                                        Rows="4" CssClass="form-control"
                                                                        Text='<%# Eval("pr_remark") %>'
                                                                        placeholder="หมายเหตุ..."
                                                                        runat="server"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <asp:LinkButton ID="btnDevice" CssClass="btn btn-primary btn-sm" runat="server"
                                                                        OnCommand="btnCommand" CommandName="btnDevice"
                                                                        CommandArgument='<%# Eval("u0idx")+"|"+
                                                                                             Eval("u2idx")+"|"+
                                                                                             Eval("u0_docket_idx")+"|"+
                                                                                             Eval("asset_no")+"|"+
                                                                                             Eval("tdidx")+"|"+ 
                                                                                             Eval("org_idx_its")+"|"+
                                                                                             Eval("rdept_idx_its")+"|"+
                                                                                             Eval("rsec_idx_its")+"|"+
                                                                                             Eval("typidx") %>'
                                                                        Text='<i class="fa fa-link"></i> เพิ่มอุปกรณ์'></asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-md-12">
                                                                    <asp:Label ID="lb_u0_code" runat="server"
                                                                        Visible="true" ForeColor="Blue"
                                                                        Text='<%# getDeviceStatus((string)Eval("u0_code")) %>' />
                                                                </div>
                                                            </div>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-2 col-md-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert_Delivery" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="_divMenubtnIT_deliver" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </EditItemTemplate>
                </asp:FormView>

            </div>

        </asp:View>


        <asp:View ID="View_docket_print_detial" runat="server">

            <div class="col-sm-12" id="Div6">
                <asp:FormView ID="Fv_docket_print_detial" OnDataBound="FvDetail_DataBound"
                    runat="server" DefaultMode="Edit" Width="100%">
                    <EditItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-file"></i>&nbsp;
                                <b>รายละเอียดรายการขอซื้ออุปกรณ์</b>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group" runat="server" visible="true">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รหัสเอกสาร : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                            <asp:TextBox ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                            <asp:HiddenField ID="hidden_name_actor" runat="server"
                                                Value='<%# Eval("actor_name") %>' />
                                            <asp:HiddenField ID="hidden_flow_item" runat="server"
                                                Value='<%# Eval("flow_item") %>' />
                                            <asp:HiddenField ID="hidden_u0_docket_idx" runat="server"
                                                Value='<%# Eval("u0_docket_idx") %>' />
                                            <asp:HiddenField ID="hidden_u1_docket_idx" runat="server"
                                                Value='<%# Eval("u1_docket_idx") %>' />
                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="เดือน / ปี ที่ขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Labellld" Text='<%# zsetMonthYear((int)Eval("zmonth"),(int)Eval("zyear")) %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" visible="true">
                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="รอบการขอซื้อ : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox2" Text='<%# getformatfloat(((int)Eval("docket_item")).ToString(),0) %>' runat="server"
                                                Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>

                                        <asp:Label runat="server" CssClass="col-md-2 control-label" Text="ประเภททรัพย์สิน : "></asp:Label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="TextBox3" Text='<%# Eval("SysNameTH") %>' runat="server" Enabled="false"
                                                CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-12" runat="server" visible="false">
                                            <blockquote class="danger" style="font-size: small; background-color: lavender;">
                                                <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp; รายการขอซื้อ</p>

                                            </blockquote>
                                        </div>
                                        <div class="col-md-12">

                                            <asp:GridView ID="gvlist_org" runat="server"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                HeaderStyle-CssClass="info" AllowPaging="true"
                                                AutoGenerateColumns="false" PageSize="5"
                                                HeaderStyle-Height="30px"
                                                OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูลรายการขอซื้อ</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="บริษัท" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>


                                                            <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("org_name_th") %>'></asp:Label>

                                                            <asp:Label ID="lb_u0_docket_idx" runat="server"
                                                                CssClass="col-sm-12" Text='<%# Eval("u0_docket_idx") %>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lb_org_idx_its" runat="server"
                                                                CssClass="col-sm-12" Text='<%# Eval("org_idx_its") %>'
                                                                Visible="false"></asp:Label>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>


                                                            <asp:GridView ID="gvlist_docket_print_list" runat="server"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                GridLines="None"
                                                                HeaderStyle-CssClass="default" AllowPaging="true"
                                                                AutoGenerateColumns="false" PageSize="10"
                                                                HeaderStyle-Height="30px"
                                                                OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                                                                    FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-Width="5%"
                                                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                                                        ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                                                        <ItemTemplate>

                                                                            <%# (Container.DataItemIndex +1) %>
                                                                            <asp:Label ID="lb_u0_docket_idx" runat="server"
                                                                                CssClass="col-sm-12" Text='<%# Eval("u0_docket_idx") %>'
                                                                                Visible="false"></asp:Label>
                                                                            <asp:Label ID="lb_org_idx_its" runat="server"
                                                                                CssClass="col-sm-12" Text='<%# Eval("org_idx_its") %>'
                                                                                Visible="false"></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="กลุ่มอุปกรณ์" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>


                                                                            <asp:Label ID="lb_tdidx_name" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("zdevices_name") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                                        HeaderStyle-Width="7%" HeaderText="จำนวน"
                                                                        ItemStyle-HorizontalAlign="Right">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                                                        HeaderStyle-Width="10%" HeaderText="หน่วย"
                                                                        ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lb_NameTH" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("NameTH") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="ราคาต่อหน่วย" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>


                                                                            <asp:Label ID="lb_price" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# string.Format("{0:n2}",Eval("price")) %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Spec" HeaderStyle-CssClass="text-center"
                                                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small">
                                                                        <ItemTemplate>


                                                                            <asp:Label ID="lb_spec_remark" runat="server" CssClass="col-sm-12"
                                                                                Text='<%# Eval("spec_remark") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                </Columns>
                                                            </asp:GridView>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                                                        ItemStyle-HorizontalAlign="center"
                                                        ControlStyle-Font-Size="9" HeaderStyle-Font-Size="9">
                                                        <ItemTemplate>

                                                            <asp:UpdatePanel ID="updatepnl_docket_print_detail" runat="server">
                                                                <ContentTemplate>

                                                                    <asp:LinkButton ID="btnmanage_its_docket_md_print_detail" CssClass="btn btn-info btn-sm" runat="server"
                                                                        data-toggle="tooltip" title="Printรายการขอซื้อ" OnCommand="btnCommand"
                                                                        CommandName="manage_its_docket_md_print_detail"
                                                                        CommandArgument='<%# 
                                                                    Eval("u0_docket_idx")+"|"+
                                                                    Eval("org_idx_its")
                                                                         %>'>
                                                                    <i class="fa fa-print"></i>
                                                                    </asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnmanage_its_docket_md_print_detail" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-md-offset-10">

                                                <asp:LinkButton ID="btnAdddata" CssClass="btn btn-primary btn-sm"
                                                    runat="server" CommandName="Cmd_ExportToPdf"
                                                    OnCommand="btnCommand"
                                                    title="Export pdf"
                                                    Visible="false">
                                                <i class="glyphicon glyphicon-export"></i></asp:LinkButton>
                                                <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server"
                                                    CommandName="_divMenubtnIT_print"
                                                    OnCommand="btnCommand" title="Cancel"
                                                    OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </EditItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>


        <asp:View ID="ViewBudget_Master" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-import"></i><strong>&nbsp; Import Data IO</strong></h3>
                    </div>

                    <div class="panel-body">

                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <%--<div class="col-md-1">--%>
                                    <asp:LinkButton ID="CmdAdd_ImportIO" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Import IO" runat="server" CommandName="CmdAdd_ImportIO" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                                    <asp:LinkButton ID="CmdAdd_SearchIO" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search IO" runat="server" CommandName="CmdAdd_SearchIO" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>

                                    <%-- </div>
                            <div class="col-md-1">
                            </div>--%>
                                </div>
                            </ContentTemplate>

                            <Triggers>
                                <asp:PostBackTrigger ControlID="CmdAdd_ImportIO" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <%------------------------ Div ADD  ------------------------%>

                        <asp:Panel ID="Panel_ImportIOAdd" runat="server" Visible="false">

                            <asp:UpdatePanel ID="UpdatePanelIO" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">แนบไฟล์ Excel</label>
                                        <div class="col-md-10">
                                            <asp:FileUpload ID="uploadio" runat="server" AutoPostBack="false" />

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                                runat="server" ValidationGroup="Create_Actor1file"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="uploadio"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณาเลือกไฟล์" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="RequiredFieldValidator9" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                    </div>



                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImportio" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel ID="UpdatePanelIOBTN" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="col-md-4  col-md-offset-2">
                                            <asp:LinkButton ID="btnSearchImportio" ValidationGroup="SaveMaster" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnSearchImportio" OnCommand="btnCommand"><i class="fa fa-search"></i> Search</asp:LinkButton>

                                            <asp:LinkButton ID="btnCancel_Importio" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_Importio" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnSearchImportio" />
                                </Triggers>
                            </asp:UpdatePanel>



                        </asp:Panel>

                        <asp:Panel ID="Panel_SearchIO" runat="server" Visible="false">

                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label47" CssClass="col-sm-2 control-label" runat="server" Text="ปี :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_yeario" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาปี...</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label48" class="col-sm-2 control-label" runat="server" Text="Cost Center : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_costcenterio" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือก Cost Center...</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label49" class="col-sm-2 control-label" runat="server" Text="IO Number : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtsearch_io" runat="server" CssClass="form-control"></asp:TextBox>

                                    </div>
                                    <asp:Label ID="Label50" CssClass="col-sm-2 control-label" runat="server" Text="Status :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsearch_statusio" runat="server" CssClass="form-control">
                                            <asp:ListItem Selected="True" Value="1">ยังไม่ถูกใช้งาน</asp:ListItem>
                                            <asp:ListItem Value="0">ถูกใช้งานแล้ว</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group">

                                    <div class="col-sm-5 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton8" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_importio" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_Importio" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>

                                </div>



                            </div>
                        </asp:Panel>
                    </div>
                    <div class="panel-body">

                        <asp:GridView ID="GvMasterIO" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            DataKeyNames="ioidx"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnRowUpdating="Master_RowUpdating">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="#">

                                    <ItemTemplate>
                                        <asp:Label ID="lblioidx" runat="server" Visible="false" Text='<%# Eval("ioidx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>


                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <asp:TextBox ID="txtioidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("ioidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="Cost" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtcost_update" runat="server" CssClass="form-control" Text='<%# Eval("cost")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="Amount" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtamountio_update" runat="server" CssClass="form-control" Text='<%# Eval("amountio")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label52" runat="server" Text="Budget" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtbudget_update" runat="server" CssClass="form-control" Text='<%# Eval("budget")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label53" runat="server" Text="Variance" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtvariance_update" runat="server" CssClass="form-control" Text='<%# Eval("variance")%>' />
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-10">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Year" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbyear" runat="server" Text='<%# Eval("year_io") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cost Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("costcenter_io") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Department" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("department") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Description" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbCostName" runat="server" Text='<%# Eval("description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียดราคา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <strong>
                                            <asp:Label ID="Label20" runat="server">Cost: </asp:Label></strong>
                                        <asp:Label ID="lbCost" runat="server" Text='<%# Eval("cost") %>' />
                                        </p>

                                                            <strong>
                                                                <asp:Label ID="Label21" runat="server">Unit: </asp:Label></strong>
                                        <asp:Label ID="Lietedral11" runat="server" Text='<%# Eval("unit") %>' />
                                        </p>
                                                            <strong>
                                                                <asp:Label ID="Label22" runat="server">Amount: </asp:Label></strong>
                                        <asp:Label ID="lbAmount" runat="server" Text='<%# Eval("amountio") %>' />

                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label1036" runat="server">Budget: </asp:Label></strong>
                                        <asp:Label ID="lbBudget" runat="server" Text='<%# Eval("budget") %>' />
                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label132" runat="server">Variance: </asp:Label></strong>
                                        <asp:Label ID="lbVariance" runat="server" Text='<%# Eval("variance") %>' />



                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ข้อมูลอุปกรณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>

                                        <strong>
                                            <asp:Label ID="Lasbel20" runat="server">IO Number: </asp:Label></strong>
                                        <asp:Literal ID="Literals210" runat="server" Text='<%# Eval("iono") %>' />
                                        </p>

                                                            <strong>
                                                                <asp:Label ID="Labsel21" runat="server">New: </asp:Label></strong>
                                        <asp:Literal ID="Lietedsral11" runat="server" Text='<%# Eval("newio") %>' />
                                        </p>
                                                            <strong>
                                                                <asp:Label ID="Label2s2" runat="server">Replace: </asp:Label></strong>
                                        <asp:Literal ID="Litersfal13" runat="server" Text='<%# Eval("replaceio") %>' />

                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label1s036" runat="server">Balance: </asp:Label></strong>
                                        <asp:Literal ID="Litersal41" runat="server" Text='<%# Eval("balance") %>' />



                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Approve Status" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbApprove" runat="server" Text='<%# getStatusApprove((int)Eval("status")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbstatus" runat="server" Text='<%# getStatus((int)Eval("usestatus")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                    </ItemTemplate>

                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>

                    </div>
                </div>


            </div>


        </asp:View>


        <%-- start โอนย้ายอุปกรณ์ --%>

        <asp:View ID="View_AssetTransfer_IndexList" runat="server">

            <asp:Panel ID="pnl_trnfcreate" runat="server">
                <div class="row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-4">
                        <div class="form-group">

                            <asp:LinkButton CssClass="btn btn-default"
                                ID="btntrnf_it" data-toggle="tooltip" title="โอนย้ายอุปกรณ์ IT"
                                runat="server"
                                Visible="false"
                                CommandName="btntrnf_it"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ IT</asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-primary "
                                ID="btntrnf_hr" data-toggle="tooltip"
                                title="โอนย้ายอุปกรณ์ HR" runat="server"
                                CommandName="btntrnf_hr"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ HR</asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-success"
                                ID="btntrnf_en" data-toggle="tooltip"
                                title="โอนย้ายอุปกรณ์ EN" runat="server"
                                CommandName="btntrnf_en"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ EN</asp:LinkButton>

                        </div>
                    </div>
                </div>
            </asp:Panel>


            <div class="col-lg-12">

                <%--  select all อนุมัติทั้งหมด --%>
                <div class="form-horizontal" role="form">
                    <asp:Panel ID="Panel_approve_trnf" runat="server" Visible="false">

                        <div class="panel panel-default">
                            <!-- Add Panel Heading Here -->
                            <div class="panel-body">

                                <div class="row">
                                    <div class="form-group">


                                        <asp:Label ID="Label90" class="col-md-2 control-label"
                                            runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:CheckBox ID="check_approve_all_trnf" runat="server" AutoPostBack="true"
                                                RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                Font-Bold="true"
                                                Text="เลือกทั้งหมด "
                                                OnCheckedChanged="checkindexchange" /><%--&nbsp;--%>
                                            <%--<strong>เลือกทั้งหมด</strong>--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label91" class="col-md-2 control-label"
                                            runat="server" Text="หมายเหตุ : "
                                            Font-Bold="true" />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark_approve_all_trnf" TextMode="multiline" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>


                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label92" class="col-md-2 control-label" runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="btnapproveall_trnf"
                                                data-toggle="tooltip" title="อนุมัติ" runat="server"
                                                CommandName="confirm_approve_trnf" Visible="true"
                                                CommandArgument="1"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการโอนย้ายนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand"><i class="fa fa-check-circle"></i> &nbsp; อนุมัติ</asp:LinkButton>
                                            <asp:LinkButton CssClass="btn btn-warning btn-sm" ID="btnnotapprove_trnf"
                                                data-toggle="tooltip" title="ไม่อนุมัติ" runat="server"
                                                CommandName="confirm_no_approve_trnf" Visible="true"
                                                CommandArgument="2"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการโอนย้ายนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand"><i class="fa fa-reply-all"></i> &nbsp; ไม่อนุมัติ</asp:LinkButton>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </asp:Panel>
                </div>


                <asp:GridView ID="GvTrnfIndex" Visible="true" runat="server"
                    AutoGenerateColumns="false" DataKeyNames="u0idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px"
                    AllowPaging="true" PageSize="5"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%"
                            ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                            Visible="false">
                            <ItemTemplate>

                                <asp:Label ID="lb_flow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                <asp:CheckBox ID="cbrecipients_trnf" runat="server"
                                    AutoPostBack="true" Visible="true"
                                    CssClass="checkbox checkbox-primary"
                                    OnCheckedChanged="checkindexchange"
                                    Text='<%# Container.DataItemIndex %>'
                                    Style="color: transparent;"></asp:CheckBox>
                                <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />
                                <asp:Label ID="lb_Selected" runat="server" Text="" Visible="false" />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_u0_transf_idx" runat="server" Visible="false" Text='<%# Eval("u0_transf_idx") %>' />
                                <asp:Label ID="lb_u0_idx" runat="server" Visible="false" Text='<%# Eval("u0_idx") %>' />
                                <asp:Label ID="lb_node_idx" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                <asp:Label ID="lb_actor_idx" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />

                                <asp:Label ID="idx" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>
                                <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                               <p></b> &nbsp;<%# Eval("doccode") %></p>
                                </asp:Label>
                                <asp:Label ID="lbdoccode" runat="server" Visible="false" Text='<%# Eval("doccode") %>' />

                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>
                                <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12">
                                 <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emp_code") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emp_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_th") %></p> 
                                </asp:Label>
                                <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12">
                                  <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_th") %></p>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดการโอนย้าย" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12 control-label">
                                 <p><b>วันที่จัดทำรายการ:</b> &nbsp;<%# Eval("zdocdate") %></p>
                                </asp:Label>


                                <asp:Label ID="lb_sys_name" runat="server" CssClass="col-sm-12">
                                 <p><b>ประเภทอุปกรณ์:</b> &nbsp;<%# Eval("sys_name") %></p>
                                </asp:Label>


                                <asp:Label ID="lb_n_org_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>องค์กร:</b> &nbsp;<%# Eval("n_org_name_th") %></p> 
                                </asp:Label>
                                <asp:Label ID="lb_n_dept_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ฝ่าย:</b> &nbsp;<%# Eval("n_dept_name_th") %></p>
                                </asp:Label>

                                <asp:Label ID="lb_location_name" runat="server" CssClass="col-sm-12">
                                 <p><b>สถานที่:</b> &nbsp;<%# Eval("location_name") %></p>
                                </asp:Label>

                                <%--<asp:Label ID="lb_remark" runat="server" CssClass="col-sm-12 control-label">
                                 <p><b>หมายเหตุ:</b> &nbsp;<%# Eval("remark") %></p>
                                </asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_status_node" runat="server" CssClass="col-sm-12">
                                 <%# Eval("to_name") %>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:UpdatePanel ID="updatebtnsaveEdit" runat="server">
                                    <ContentTemplate>

                                        <asp:LinkButton ID="btnmanage_trnf" CssClass="btn btn-info btn-sm" runat="server"
                                            data-toggle="tooltip" title="รายละเอียด" OnCommand="btnCommand"
                                            CommandArgument='<%#
                                            Eval("u0_transf_idx")
                                            %>'
                                            CommandName="btnmanage_trnf">
                                           <i class="fa fa-file"></i>
                                        </asp:LinkButton>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnmanage_trnf" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>


        <%-- end โอนย้ายอุปกรณ์ --%>
    </asp:MultiView>

    <%-- start modal --%>
    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div4">
        <div id="show_detail" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการขอซื้ออุปกรณ์</strong></h3>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="col-md-12">

                                        <asp:GridView ID="gvListIts_modal" Visible="true" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-responsive word-wrap"
                                            HeaderStyle-CssClass="info"
                                            GridLines="None"
                                            OnRowDataBound="gvRowDataBound">

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ยังไม่มีรายการขอซื้อ</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="3%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                        <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />

                                                        <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                        <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="เลขที่เอกสาร" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="10%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                                                                <p><%# Eval("doccode") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์ขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="15%" Visible="false">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_typidx_name" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("type_name") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="กลุ่มอุปกรณ์ขอซื้อ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="Small" Visible="false"
                                                    HeaderStyle-Width="15%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="Label41" runat="server" CssClass="col-sm-12">
                                                                <p><%# Eval("zdevices_name") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Spec" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="Small" Visible="false"
                                                    HeaderStyle-Width="10%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="Label41" runat="server" CssClass="col-sm-12">
                                                                <p><%# Eval("spec_name") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="40%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="Label45" runat="server" CssClass="col-sm-12">
                                                                <p><%# Eval("list_menu") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_quantity" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((int)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="Small" Visible="true"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_NameTH" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("NameTH") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ราคาต่อหน่วย" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-Font-Size="Small" Visible="true"
                                                    HeaderStyle-Width="10%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_price" runat="server" CssClass="col-sm-12"
                                                            Text='<%# getformatfloat(((decimal)Eval("price")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รวม" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="Small" Visible="true">
                                                    <ItemTemplate>

                                                        <asp:Label ID="items_total_amount" runat="server" CssClass="col-sm-12"
                                                            Text='<%#  getformatfloat(((decimal)Eval("total_amount")).ToString(),2) %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </div>

                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">

                                                    <asp:LinkButton ID="btnCmdCancel_modal" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                        CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>

                                    </asp:UpdatePanel>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div7">
        <div id="showspec_detail" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; รายละเอียดสเปค</strong></h3>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="col-md-12" style="height: 400px; overflow-y: scroll; overflow-x: scroll;">

                                        <asp:GridView ID="gvListspec_modal" Visible="true" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-responsive word-wrap"
                                            HeaderStyle-CssClass="info"
                                            GridLines="None"
                                            OnRowDataBound="gvRowDataBound">

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ยังไม่มีรายการสเปค</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>

                                                        <asp:UpdatePanel ID="updatepnl_select" runat="server">
                                                            <ContentTemplate>

                                                                <asp:LinkButton ID="btn_select" class="btn btn-success btn-sm"
                                                                    data-toggle="tooltip" title="เลือก" runat="server"
                                                                    OnCommand="btnCommand"
                                                                    CommandName="btn_select"
                                                                    CommandArgument='<%# Eval("id")  %>'><i class="glyphicon glyphicon-ok"></i></asp:LinkButton>

                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btn_select" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small"
                                                    Visible="false"
                                                    HeaderStyle-Width="3%">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="_lbItemsnumber" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />
                                                        <asp:Label ID="_lbu0idx" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />

                                                        <asp:Label ID="_lbu1idx" runat="server" Visible="false" Text='<%# Eval("u1idx") %>' />

                                                        <asp:Label ID="_lbpr_type_idx" runat="server" Visible="false" Text='<%# Eval("pr_type_idx") %>' />--%>
                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="สเปค" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="30%">
                                                    <ItemTemplate>


                                                        <asp:Label ID="lb_zname" runat="server" CssClass="col-sm-12">
                                                                <p><%# Eval("zname") %></p>
                                                        </asp:Label>


                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                                                    HeaderStyle-Width="60%">
                                                    <ItemTemplate>

                                                        <asp:Label ID="lb_detail" runat="server" CssClass="col-sm-12"
                                                            Text='<%# Eval("detail") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>

                                    </div>

                                    <asp:UpdatePanel ID="updatepanel7" runat="server">
                                        <ContentTemplate>
                                            <div>
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-10">

                                                        <asp:LinkButton ID="btnCmdCancelspec_modal" class="btn btn-default btn-sm"
                                                            data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                            CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>
    <div class="col-lg-12" runat="server" id="Div8">
        <div id="show_updateprice" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการขอซื้ออุปกรณ์</strong></h3>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="col-md-12">

                                        <asp:FormView ID="fv_show_updateprice"
                                            runat="server" DefaultMode="Edit" Width="100%">
                                            <EditItemTemplate>
                                                <asp:HiddenField ID="hidden_m0_node" runat="server" Value='<%# Eval("node_idx") %>' />
                                                <asp:HiddenField ID="hidden_m0_actor" runat="server" Value='<%# Eval("actor_idx") %>' />
                                                <asp:HiddenField ID="hidden_emp_idx" runat="server" Value='<%# Eval("emp_idx_its") %>' />
                                                <asp:HiddenField ID="hidden_emp_director" runat="server" Value='<%# Eval("emp_idx_director") %>' />
                                                <asp:HiddenField ID="hidden_status_node" runat="server" Value='<%# Eval("doc_status") %>' />
                                                <asp:HiddenField ID="hidden_doccode" runat="server" Value='<%# Eval("doccode") %>' />
                                                <asp:HiddenField ID="hidden_actor_name" runat="server" Value='<%# Eval("actor_name") %>' />
                                                <asp:HiddenField ID="hidden_id" runat="server" />

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">รหัสเอกสาร :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"></asp:Label>
                                                        <asp:Label ID="hidden_name_actor" Visible="false" runat="server" Text='<%# Eval("actor_name") %>' />
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">วันที่ขอซื้อ :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Labellld" Text='<%# Eval("zdocdate") %>' runat="server"></asp:Label>
                                                    </div>
                                                </div>

                                                <%-- <div class="form-group">
                                                    <label class="col-sm-4 control-label">ประเภททรัพย์สิน :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Label42" runat="server">
                                                        <%# Eval("SysNameTH") %>
                                                        </asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">ประเภทอุปกรณ์ขอซื้อ :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Label81" runat="server">
                                                        <%# Eval("type_name") %>
                                                        </asp:Label>
                                                    </div>
                                                </div>--%>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">กลุ่มอุปกรณ์ขอซื้อ :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Label82" runat="server">
                                                        <%# Eval("zdevices_name") %>
                                                        </asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">เลขที่ IO :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Label83" runat="server">
                                                        <%# Eval("ionum") %>
                                                        </asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">รายละเอียด :</label>
                                                    <div class="col-sm-8 control-label textleft">
                                                        <asp:Label ID="Label85" runat="server">
                                                        <%# Eval("list_menu") %>
                                                        </asp:Label>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">ราคา :</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:TextBox ID="txt_price" runat="server" TextMode="Number"
                                                            Text='<%# Eval("price") %>' CssClass="form-control pull-left"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">งบประมาณที่ตั้งไว้ :</label>
                                                    <div class="col-sm-4 control-label textleft">

                                                        <asp:TextBox ID="txt_budget_set" runat="server" TextMode="Number"
                                                            Text='<%# Eval("budget_set") %>' CssClass="form-control pull-left"></asp:TextBox>
                                                    </div>
                                                </div>


                                                </div>

                                                
                                    <asp:UpdatePanel ID="updatepnl_updateprice" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-8">


                                                    <asp:LinkButton ID="btnsaveupdateprice" class="btn btn-success btn-sm"
                                                        data-toggle="tooltip" title="บันทึก" runat="server"
                                                        OnCommand="btnCommand"
                                                        CommandName="btnsaveupdateprice"
                                                        CommandArgument='<%# 
                                                                    Eval("u0idx")+ "|" + 
                                                                    Eval("u1idx")
                                                                    %>'><i class="fa fa-save"></i></asp:LinkButton>


                                                    <asp:LinkButton ID="btnCmdCancelprice_modal" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                        CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnsaveupdateprice" />
                                        </Triggers>

                                    </asp:UpdatePanel>


                                            </EditItemTemplate>
                                        </asp:FormView>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div9">
        <div id="show_spec" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 40%;">
                <!-- Modal content-->
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="form-group">
                                        <asp:Label ID="Label81" class="col-md-2 control-label" runat="server" Text="สเปค : " />
                                        <div class="col-md-9">

                                            <asp:DropDownList ID="ddl_spec_search"
                                                runat="server" CssClass="form-control fa-align-left">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <asp:UpdatePanel ID="updatepnl_spec" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <div class="col-sm-4 col-sm-offset-8">

                                                        <asp:LinkButton ID="btn_okspec" class="btn btn-success btn-sm"
                                                            data-toggle="tooltip" title="บันทึก" runat="server"
                                                            OnCommand="btnCommand"
                                                            CommandName="btn_okspec"><i class="fa fa-check"></i></asp:LinkButton>


                                                        <asp:LinkButton ID="btn_cancelspec" class="btn btn-default btn-sm"
                                                            data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                            CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btn_okspec" />
                                            </Triggers>

                                        </asp:UpdatePanel>


                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12" runat="server" id="Div10">
        <div id="show_importio" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 90%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียดรายการ Import Data IO</strong></h3>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="panel-body" style="height: 500px; overflow-y: scroll; overflow-x: scroll;">


                                        <asp:GridView ID="gv_importio" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info"
                                            HeaderStyle-Height="40px">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="id" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="id" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="CostCenter" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="CostCenter" runat="server" Text='<%# Eval("CostCenter") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Department" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbyDepartmentar" runat="server" Text='<%# Eval("Department") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Description" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Cost" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Cost" runat="server" Text='<%# Eval("Cost") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Unit" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Unit" runat="server" Text='<%# Eval("Unit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Amount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Budget" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Budget" runat="server" Text='<%# Eval("Budget") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Variance" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Variance" runat="server" Text='<%# Eval("Variance") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="IO_Number" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="IO_Number" runat="server" Text='<%# Eval("IO_Number") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="New" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="New" runat="server" Text='<%# Eval("New") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Replace" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Replace" runat="server" Text='<%# Eval("Replace") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="NoCost" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="NoCost" runat="server" Text='<%# Eval("NoCost") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Expenses_Year" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Expenses_Year" runat="server" Text='<%# Eval("Expenses_Year") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Asset_Type" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Asset_Type" runat="server" Text='<%# Eval("Asset_Type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="No" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="No" runat="server" Text='<%# Eval("No") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Status" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Year" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Year" runat="server" Text='<%# Eval("Year") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>


                                    </div>

                                    <div class="col-md-12">

                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <div class="col-sm-4 col-sm-offset-8">

                                                        <asp:LinkButton ID="btnImportio" ValidationGroup="SaveMaster" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save"
                                                            runat="server" CommandName="btnImportio" OnCommand="btnCommand"
                                                            OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')">
                                                             <i class="fa fa-save"></i> Import</asp:LinkButton>

                                                        <asp:LinkButton ID="LinkButton7" class="btn btn-default btn-sm"
                                                            data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                            CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnImportio" />
                                            </Triggers>

                                        </asp:UpdatePanel>


                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%-- end modal --%>

    <script type="text/javascript">
        function openModal_show_detail() {
            $('#show_detail').modal('show');
        }
        function openModal_Spec_detail() {
            $('#showspec_detail').modal('show');
        }
        function openModal_updateprice() {
            $('#show_updateprice').modal('show');
        }
        function openModal_spec() {
            $('#show_spec').modal('show');
        }
        function openModal_importio() {
            $('#show_importio').modal('show');
        }
    </script>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>


    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>

    <script type="text/javascript">

        function SetTarget() {

            document.forms[0].target = "_blank";

        }
        function shwwindow(myurl) {
            window.open(myurl, '_blank');

        }

    </script>

    <style type="text/css">
        .checkbox {
            padding-left: 20px;
        }

            .checkbox label {
                display: inline-block;
                vertical-align: middle;
                position: relative;
                padding-left: 5px;
                font-weight: bold;
            }

                .checkbox label::before {
                    content: "";
                    display: inline-block;
                    position: absolute;
                    width: 17px;
                    height: 17px;
                    eft: 0;
                    cccccc;
                    px;
                    bckgrond-color: #ff;
                    rder 0.15s eas -in-ou, colr 0.15s easein-ou;
                    rder 0.15s ase-inout, olor 0.15s ese-inout;
                    : n-out, coor 0.15s eas -
                }


        er {
            -block ion: ab olute widt : he l ft: 0;
            top: 0;
            : -20px;
            eft: 3px;
            -top: px;
            o color: #5555 5
        }

        nput[typ =" ty: 0;
        z-index: 1;
        }

        input[type= checkbox"]:che

        F



        content: "\f00c";
        }

        primary input[typ ="checkb label::befor {
            c r: #337ab7;
            border-color: #337ab7;
        }

        -prima y inp
        h

        :checked + label::after {
            color: #fff;
    </style>

    <style type="text/css">
        abel {
            /*p d
            
       }

       abel label {
                    line-bloc ;
    al-align: mid le;
            osition: cen er;
            adding-left: 3px;
         
        
                        ont-weight: b l
                       abel::be ore {
       "";
          ne-blo k;
             ition: enter
             widt : 
                 height: 17px;
            eft: 0;
                           
                cccccc;
             px;
                   bckgrond-color: #ff;
                  rder 0.15s eas -in-ou, colr 0.15s easein-ou;
              rder 0.15s ase-inout,  olor 0.15s ese-inout;
                        :

        n-out, color 0.15 ease-in-out
                           .check
            er {
          ne-blo k;
             ition: enter
             widt : 
                he
                       l ft:  0;
            top: 0;
        : -25px;
      eft: 3px;
                 -top: px;
   o

                           color: #555555;
             .
            nput[typ ="
         
        ty: 0;
               z-index: 1;
           }

        

           input[type= checkbox"]:che
            
               F

        
        
                    conent: "\f00c";
               }

        

        .c e
            primary input[typ ="checkb
             label::befor  {
    c

        r: #337ab7;
            order-color: #337ab7;
        
               }

        

        . h
            -prima y inp
        h

        :checked + label::after {
            color: #fff;
        }
    </style>


    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>
    <script type="text/javascript">
        $(".multi").MultiFile();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $(".multi").MultiFile();
        })
    </script>

</asp:Content>

