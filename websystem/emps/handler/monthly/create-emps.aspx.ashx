﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int announceIdx = 0;
      int parttimeIdx = 0;
      int creator = 0;
      string dateEvent = String.Empty;
      int.TryParse(context.Request.QueryString["announceIdx"], out announceIdx);
      int.TryParse(context.Request.QueryString["parttimeIdx"], out parttimeIdx);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      dateEvent = context.Request.QueryString["dateEvent"];
      string json = this.createEmpShift(announceIdx, parttimeIdx, creator, dateEvent);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createEmpShift(int announceIdx, int parttimeIdx, int creator, string dateEvent)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            int statusOnline = 1;
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "INSERT INTO emps_u1_announce (u0_announce_idx_ref, m0_parttime_idx_ref, empshift_date, empshift_status) VALUES (@u0_announce_idx_ref, @m0_parttime_idx_ref, @dateEvent, " + statusOnline + ")";
            cmd.Parameters.AddWithValue("@u0_announce_idx_ref", announceIdx);
            cmd.Parameters.AddWithValue("@m0_parttime_idx_ref", parttimeIdx);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.Parameters.AddWithValue("@dateEvent", dateEvent);
            cmd.ExecuteNonQuery();
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
