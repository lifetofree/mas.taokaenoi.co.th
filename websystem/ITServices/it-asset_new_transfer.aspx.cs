﻿
using NPOI.XWPF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class websystem_ITServices_it_asset_new_transfer : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_itasset _dtitseet = new data_itasset();
    data_purchase _data_purchase = new data_purchase();
    function_dmu _func_dmu = new function_dmu();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();

    List<its_TransferDevice_u0> _its_TransferDevice_u0_selected = new List<its_TransferDevice_u0>();

    string _localJson = String.Empty;
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";

    int emp_idx = 0;
    //ผู้เกี่ยวข้อง
    int it_support = 210;//80;
    int budget = 6;
    int asset = 9;
    int it_admin_rsec = 210; // rsec 210

    int _jobManager = 9, _jobDirector = 10;

    // set object
    FormView _FormView;

    GridView _GvReportAdd,
        _GridView
        ;

    TextBox _txtremark,
        _txtref_itnum,
        _txtionum,
        _txtbudget_set,
        _txtlist_menu,
        _txtqty,
        _txtprice,
        _txtstartdate,
        _txtenddate,
        _txtdocumentcode,
        _txt_remark,
         _ddlspecidx,
        _txt_hidden_sysidx,
        _txt_m1_ref_idx,
        _txtplace_idx,
        _txtplace_trnf,
        _txtplace_idx_hr,
        _txtplace_idx_qa,
        _txtasset_no,
        _txtdesc_name,
        _txtacquis_val
        ;

    DropDownList
        _ddlsystem,
        _ddlSearchTypeDevices,
        _ddlSearchDevices,
        _ddlsearch_typepurchase,
        _ddlposition,
        _ddlcurrency,
        _ddlunit,
        _ddlcostcenter,
        _ddlplace,
        // _ddlspecidx,
        _ddlStatusapprove,
        _ddlcondition,
         _ddlMonthSearch,
        _ddlYearSearch,
        _ddlStatusapproveSearch,
         _ddl_target,
        _ddlAddOrg_trnf,
_ddlAddDep_trnf,
_ddlAddSec_trnf,
_ddlAddemp_idx_trnf,
_ddlSearch_devices_trnf,
_ddlasset_no_trnf,
        _ddlAddOrg_trnf_en,
_ddlAddDep_trnf_en,
_ddllocate_en,
_ddlbuilding_en,
_ddlroom_en,
_ddllocate_en_search,
_ddltypemachine_en_search,
_ddlbuilding_en_search,
_ddlroom_en_search,
_ddlmachine_en_search,
_ddlAddOrg_trnf_hr,
_ddlAddDep_trnf_hr,
_ddlAddemp_idx_trnf_hr,
_ddlAddSec_trnf_hr,
_ddlSearch_devices_trnf_hr,
_ddlasset_no_trnf_hr,
_ddllocate_hr_search,
        _ddllocate_it_search,
        _ddllocate_hr,
        _ddlAddOrg_trnf_qa,
_ddlAddDep_trnf_qa,
_ddlAddemp_idx_trnf_qa,
_ddlAddSec_trnf_qa,
_ddlSearch_devices_trnf_qa,
_ddlasset_no_trnf_qa,
_ddllocate_qa_search,
        _ddllocate_qa,
        _ddlAddOrg_trnf_other,
_ddlAddDep_trnf_other,
_ddllocate_other,
        _ddllocate_other_search,
         _ddlcostcenter_hr,
        _ddlcostcenter_en,
        _ddlAddSec_trnf_en,
        _ddlcostcenter_hr_search ,
        _ddlcostcenter_other_search

        ;
    Panel
        _pnl_ionum,
        _pnl_ref_itnum,
        _Panel_body_approve,
        _Panel_approve,
        _pnl_search,
        _pnl_history,
        _pnl_doc,
        _pnl_system
        ;
    Label
        _lb_title_approve
        ;
    Repeater
        _Repeater,
        _rpt_history
        ;
    LinkButton
        _lbtn,
        _lbtnsave,
        _lbtncancel
        ;
    CheckBox
        _check_approve,
        _CheckBox,
        _check_transfer_flag,
        _check_subno_other,
        _check_transfer_flag_en
        ;
    HiddenField
        _hfu0_transf_idx,
_hfflow_item,
_hfNodeIdx,
_hfActorIdx,
        _hfsysidx,
        _hfdoc_status

        ;

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem"];
    static string _urlInsertClass = _serviceUrl + ConfigurationManager.AppSettings["urlnsert_Master_Class"];
    static string _urlSelectClass = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_Class"];
    static string _urlDeleteClass = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_Class"];
    static string _urlInsertAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_AssetBuy"];
    static string _urlSelectAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_AssetBuy"];
    static string _urlInsert_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_IOBuy"];
    static string _urlSelect_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_IOBuy"];
    static string _urlUpdate_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_IOBuy"];
    static string _urlSelect_Master_YearIOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_YearIOBuy"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlSelect_Master_YEARAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_YEARAssetBuy"];
    static string _urlSelect_Master_LocationAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_LocationAssetBuy"];
    static string _urlSelectPos = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPos"];



    // start Price_Reference

    // static string _urlGetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlGetequipment_purchasetype"];
    static string _urlSelectSystem_TypePrice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_TypePrice"];
    static string _urlSelectTypeDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeDevices_Ref"];
    static string _urlSelectSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSoftware_Ref"];
    static string _urlSelectHROffice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHROffice_Ref"];
    static string _urlSelectENMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectENMachine_Ref"];
    static string _urlGetposition = _serviceUrl + ConfigurationManager.AppSettings["urlGetposition"];

    // end Price_Reference
    static string _urlGetmasterspec = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasterspec"];
    // start it asset 
    static string _urlGetits_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_lookup"];
    //its_u_document
    static string _urlGetits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_document"];
    static string _urlSetInsits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_document"];
    static string _urlSetUpdits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_document"];
    static string _urlDelits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_document"];

    static string _urlGetits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_masterit"];
    static string _urlsendEmailits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailits_u_document"];


    // end it asset
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetlocation = _serviceUrl + ConfigurationManager.AppSettings["urlGetlocation"];

    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    // its_u_transfer_action
    static string _urlGetits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_transfer"];
    static string _urlSetInsits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_transfer"];
    static string _urlSetUpdits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_transfer"];
    static string _urlDelits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_transfer"];

    static string _url_u0_device_update_17 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_17_List"];
    static string urlSetApproveLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveLicense"];

    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string urlSelect_Room = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Room"];
    static string _urlSelectDoc_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_Machine"];
    static string urlSelect_NameMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_NameMachine"];
    static string urlSelectDetailHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetailHolder"];
    static string urlUpdate_ApproveChangeHolder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveChangeHolder"];
    static string urlGetSoftwareShowHolder = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareShowHolder"];

    #endregion

    #endregion

    #region PageLoad
    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

            ViewState["mode"] = "I";
            ViewState["status"] = "preview";
            select_empIdx_present();

            setpermission_admin_dept();
            setActiveTab("preview");
            _liMenuLiTodirector.Visible = false;
            _liMenuLiTodirector_Agency.Visible = false;
            _liMenuLiTodirector_Asset.Visible = false;
            if (_func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()) != it_admin_rsec)
            {
                setpermission_admin_menu();
            }

            ShowListTranfer();
            SETFOCUS.Focus();

        }

        setBtnTrigger();

    }

    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }

    private void setBtnTrigger()
    {
        //menu

        linkBtnTrigger(_divMenuBtnToDivChangeOwn);
        linkBtnTrigger(_divMenuBtnTohr);
        

        linkBtnTrigger(btnTooffice);
        linkBtnTrigger(btnTomanager);
        linkBtnTrigger(btnTodirector);

        linkBtnTrigger(btnTooffice_Agency);
        linkBtnTrigger(btnTomanager_Agency);
        linkBtnTrigger(btnTodirector_Agency);

        linkBtnTrigger(btnTooffice_Asset);
        linkBtnTrigger(btnTomanager_Asset);
        linkBtnTrigger(btnTodirector_Asset);

        linkBtnTrigger(btntrnf_it);
        linkBtnTrigger(btntrnf_hr);
        linkBtnTrigger(btntrnf_en);

        linkBtnTrigger(btnshowBoxsearch);
        linkBtnTrigger(btnhiddenBoxsearch);
        linkBtnTrigger(btnsearch);
        linkBtnTrigger(btnresetsearch);

        GridViewTrigger(GvTrnfIndex);

    }
    #endregion

    #region CallService
    protected data_employee callService(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }


    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        if (_dtEmployee.employee_list != null)
        {
            ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
            ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
            ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

            ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
            ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
            ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
            ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
            ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
            ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

            ViewState["rsec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["rpos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["org_idx"] = _dtEmployee.employee_list[0].org_idx;
            ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
            ViewState["locidx"] = _dtEmployee.employee_list[0].LocIDX;

        }
        else
        {
            ViewState["rdept_name"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["FullName"] = "";
            ViewState["Org_name"] = "";
            ViewState["Org_idx"] = "";

            ViewState["EmpCode"] = "";
            ViewState["Positname"] = "";
            ViewState["Pos_idx"] = "";
            ViewState["Email"] = "";
            ViewState["Tel"] = "";
            ViewState["Secname"] = "";
            ViewState["Sec_idx"] = "";
            ViewState["CostIDX"] = "";

            ViewState["rsec_idx"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["rpos_idx"] = "";
            ViewState["org_idx"] = "";
            ViewState["jobgrade_level"] = "";
            ViewState["locidx"] = "";
        }

        ViewState["emp_idx"] = ViewState["EmpIDX"].ToString();
        ViewState["rdept_idx_dir"] = 0;
        ViewState["emp_idx_dir"] = 0;
        ViewState["rdept_idx_budget"] = 0;
        ViewState["rpos_idx_budget"] = 0;
        ViewState["org_idx_budget"] = 0;
        ViewState["rsec_idx_budget"] = 0;
        ViewState["emp_idx_budget"] = 0;
        ViewState["place_1"] = 0;
        ViewState["place_2"] = 0;
        ViewState["place_3"] = 0;
        ViewState["place_4"] = 0;
        ViewState["place_count"] = 0;

        ViewState["approve1"] = "";
        ViewState["approve2"] = "";
        
    }


    protected void select_system(DropDownList ddlName)
    {
        // tb [Centralized].[dbo].[M0_System]

        data_itasset _dtitseet = new data_itasset();

        _dtitseet.its_lookup_action = new its_lookup[1];
        its_lookup search = new its_lookup();
        search.operation_status_id = "m0_system_tnf";
        _dtitseet.its_lookup_action[0] = search;

        _dtitseet = callServicePostITAsset(_urlGetits_lookup, _dtitseet);
        setDdlData(ddlName, _dtitseet.its_lookup_action, "zname", "idx");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));
    }


    protected void select_YearMasterIO(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxmaster_io = new io_itasset[1];
        io_itasset search = new io_itasset();

        _dtitseet.boxmaster_io[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelect_Master_YearIOBuy, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxmaster_io, "year_io", "year_io");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกปี...", "0"));
    }

    protected void select_CostcenterMasterIO(DropDownList ddlName)
    {

        _dtEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dtEmployee.CostCenterDetail[0] = dtemployee;

        _dtEmployee = callService(_urlGetCostcenterOld, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.CostCenterDetail, "CostNo", "CostIDX");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก Cost Center...", "0"));
    }

    protected void select_YearMasterAsset(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxmaster_asset = new asset_itasset[1];
        asset_itasset search = new asset_itasset();

        _dtitseet.boxmaster_asset[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelect_Master_YEARAssetBuy, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxmaster_asset, "CapDate", "CapDate");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกปี...", "0"));
    }
    
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void GridViewTrigger(GridView gridview)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gridview.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected string getStatusApprove(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-square'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-minus-circle'></i></span>";
        }
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

        }

    }


    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                //GvMaster.PageIndex = e.NewPageIndex;
                //GvMaster.DataBind();

                //SelectMasterList_Class();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                //GvMaster.EditIndex = e.NewEditIndex;
                //SelectMasterList_Class();
                //btnshow.Visible = false;
                break;


        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                //GvMaster.EditIndex = -1;
                //SelectMasterList_Class();
                //btnshow.Visible = true;
                break;


        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                //int clidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());


                break;


        }
    }

    #endregion

    #region FormView

    private void setDefaultPurchase()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        LinkButton btnAddBuy_Newlist = (LinkButton)_FormView.FindControl("btnAddBuy_Newlist");
        LinkButton btnAdddata = (LinkButton)_FormView.FindControl("btnAdddata");
        LinkButton AddCancel = (LinkButton)_FormView.FindControl("AddCancel");
        GridView GvReportAdd = (GridView)_FormView.FindControl("GvReportAdd");

        linkBtnTrigger(btnAddBuy_Newlist);
        linkBtnTrigger(btnAdddata);
        linkBtnTrigger(AddCancel);
        GridViewTrigger(GvReportAdd);
    }
    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;
        switch (FvName.ID)
        {

            case "FvTrnfInsert":
                if ((FvName.CurrentMode == FormViewMode.Edit) || (FvName.CurrentMode == FormViewMode.Insert))
                {
                    setDefaultTrnf();
                }
                break;
        }
    }


    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            int rowIndex = 0;
            GridView _grdView;
            GridViewRow rowSelect;
            DataSet dsContacts;
            switch (cmdName)
            {
                case "btnDeleteList_trnf":
                    _grdView = (GridView)FvTrnfInsert.FindControl("GvTransferDevice");
                    rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    rowIndex = rowSelect.RowIndex;
                    dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
                    dsContacts.Tables["dsits_TransferDevice_u1"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(_grdView, dsContacts.Tables["dsits_TransferDevice_u1"]);
                    if (dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Count < 1)
                    {
                        // _grdView.Visible = false;
                    }
                    break;
            }
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddlAddSec_trnf, ddlAddOrg_trnf
            , ddlAddDep_trnf, ddlAddemp_idx_trnf
            , ddlSearch_devices_trnf, ddlasset_no_trnf,
        ddllocate_en_search, ddltypemachine_en_search,
        ddlbuilding_en_search, ddlroom_en_search,
        ddlmachine_en_search, ddllocate_en, ddlbuilding_en, ddlroom_en
        , ddlplace,ddlcost
        ;


        // _FormView = getFv(ViewState["mode"].ToString());
        switch (ddName.ID)
        {

            case "ddlcondition":
                
                if (ddlcondition.SelectedValue == "3")
                {
                    txtenddate.Enabled = true;
                }
                else
                {
                    txtenddate.Enabled = false;
                }

                if (ddlcondition.SelectedValue == "0")

                {
                    txtenddate.Text = String.Empty;
                    txtstartdate.Text = String.Empty;
                }
                else
                {
                }
                
                break;

            case "ddlAddOrg_trnf":

                select_Trnf_Insert();

                break;
            case "ddlAddDep_trnf":

                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf");
                ddlSearch_devices_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf");

                ddl_Sec(ddlAddSec_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), _func_dmu.zStringToInt(ddlAddDep_trnf.SelectedValue));
                getEmpList(ddlAddemp_idx_trnf, int.Parse(ddlAddOrg_trnf.SelectedValue), int.Parse(ddlAddDep_trnf.SelectedValue), int.Parse(ddlAddSec_trnf.SelectedValue));
                SelectDevices(ddlSearch_devices_trnf);
                break;
            case "ddlAddSec_trnf":

                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf");
                ddlSearch_devices_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf");

                getEmpList(ddlAddemp_idx_trnf, int.Parse(ddlAddOrg_trnf.SelectedValue), int.Parse(ddlAddDep_trnf.SelectedValue), int.Parse(ddlAddSec_trnf.SelectedValue));
                SelectDevices(ddlSearch_devices_trnf);
                break;
            case "ddlAddemp_idx_trnf":
                ddlSearch_devices_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf");

                setLocation(_func_dmu.zStringToInt(ddName.SelectedValue));
                SelectDevices(ddlSearch_devices_trnf);
                break;
            case "ddlSearch_devices_trnf":
                ddlplace = (DropDownList)FvTrnfInsert.FindControl("ddllocate_it_search");
                ddlasset_no_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf");
                ddl_asset_no(ddlasset_no_trnf, _func_dmu.zStringToInt(ddlplace.SelectedValue), _func_dmu.zStringToInt(ddName.SelectedValue));
                break;
            //start transfer en
            case "ddlAddOrg_trnf_en":
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_en");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_en");
                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_en");
                ddl_depertment_en(ddlAddDep_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), 0, 0);
                ddl_Sec(ddlAddSec_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), _func_dmu.zStringToInt(ddlAddDep_trnf.SelectedValue));
                
                break;
            case "ddllocate_en":
                ddllocate_en = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en");
                ddlbuilding_en = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en");
                ddlroom_en = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en");
                Select_Building(ddlbuilding_en, int.Parse(ddllocate_en.SelectedValue));
                Select_Room(ddlroom_en, int.Parse(ddlbuilding_en.SelectedValue));
                break;
            case "ddlbuilding_en":
                ddlbuilding_en = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en");
                ddlroom_en = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en");
                Select_Room(ddlroom_en, int.Parse(ddlbuilding_en.SelectedValue));
                break;
            //start transfer en search
            case "ddllocate_en_search":
                ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
                ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");

                ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
                ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
                ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

                select_typemachine(ddltypemachine_en_search);

                Select_Building(ddlbuilding_en_search, int.Parse(ddllocate_en_search.SelectedValue));
                Select_Room(ddlroom_en_search, int.Parse(ddlbuilding_en_search.SelectedValue));

                Select_NameMachine(ddlmachine_en_search,
                    int.Parse(ddllocate_en_search.SelectedValue),
                    int.Parse(ddlbuilding_en_search.SelectedValue),
                    int.Parse(ddlroom_en_search.SelectedValue),
                    int.Parse(ddltypemachine_en_search.SelectedValue));


                break;
            case "ddltypemachine_en_search":
                ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
                ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");

                ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
                ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
                ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

                Select_Building(ddlbuilding_en_search, int.Parse(ddllocate_en_search.SelectedValue));
                Select_Room(ddlroom_en_search, int.Parse(ddlbuilding_en_search.SelectedValue));

                Select_NameMachine(ddlmachine_en_search,
                    int.Parse(ddllocate_en_search.SelectedValue),
                    int.Parse(ddlbuilding_en_search.SelectedValue),
                    int.Parse(ddlroom_en_search.SelectedValue),
                    int.Parse(ddltypemachine_en_search.SelectedValue));

                break;

            case "ddlbuilding_en_search":
                ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
                ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");

                ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
                ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
                ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

                Select_Room(ddlroom_en_search, int.Parse(ddlbuilding_en_search.SelectedValue));

                Select_NameMachine(ddlmachine_en_search,
                    int.Parse(ddllocate_en_search.SelectedValue),
                    int.Parse(ddlbuilding_en_search.SelectedValue),
                    int.Parse(ddlroom_en_search.SelectedValue),
                    int.Parse(ddltypemachine_en_search.SelectedValue));

                break;
            case "ddlroom_en_search":
                ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
                ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");

                ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
                ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
                ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

                Select_NameMachine(ddlmachine_en_search,
                    int.Parse(ddllocate_en_search.SelectedValue),
                    int.Parse(ddlbuilding_en_search.SelectedValue),
                    int.Parse(ddlroom_en_search.SelectedValue),
                    int.Parse(ddltypemachine_en_search.SelectedValue));

                break;
            // hr //
            case "ddlAddOrg_trnf_hr":
                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_hr");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_hr");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_hr");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_hr");

                ddl_depertment(ddlAddDep_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), 0);
                ddl_Sec(ddlAddSec_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), _func_dmu.zStringToInt(ddlAddDep_trnf.SelectedValue));
                getEmpList(ddlAddemp_idx_trnf, int.Parse(ddlAddOrg_trnf.SelectedValue), int.Parse(ddlAddDep_trnf.SelectedValue), int.Parse(ddlAddSec_trnf.SelectedValue));
                break;
            case "ddlAddDep_trnf_hr":

                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_hr");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_hr");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_hr");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_hr");
                ddl_Sec(ddlAddSec_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), _func_dmu.zStringToInt(ddlAddDep_trnf.SelectedValue));
                getEmpList(ddlAddemp_idx_trnf, int.Parse(ddlAddOrg_trnf.SelectedValue), int.Parse(ddlAddDep_trnf.SelectedValue), int.Parse(ddlAddSec_trnf.SelectedValue));

                break;
            case "ddlAddSec_trnf_hr":

                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_hr");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_hr");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_hr");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf");
                getEmpList(ddlAddemp_idx_trnf, int.Parse(ddlAddOrg_trnf.SelectedValue), int.Parse(ddlAddDep_trnf.SelectedValue), int.Parse(ddlAddSec_trnf.SelectedValue));

                break;
            case "ddlAddemp_idx_trnf_hr":
                ddlSearch_devices_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf_hr");

                break;
            case "ddlSearch_devices_trnf_hr":
                ddlplace = (DropDownList)FvTrnfInsert.FindControl("ddllocate_hr_search");
                ddlasset_no_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf_hr");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_hr");
                ddlcost = (DropDownList)FvTrnfInsert.FindControl("ddlcostcenter_hr_search");
                ddl_asset_no_hr(ddlasset_no_trnf, _func_dmu.zStringToInt(ddName.SelectedValue), _func_dmu.zStringToInt(ddlplace.SelectedValue), _func_dmu.zStringToInt(ddlcost.SelectedValue));
                break;

            case "ddlsystem":

                setMode(_func_dmu.zStringToInt(ddName.SelectedValue));

                break;
            case "ddlSearch_devices_trnf_qa":
                ddlplace = (DropDownList)FvTrnfInsert.FindControl("ddllocate_qa_search");
                ddlasset_no_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf_qa");
                ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_qa");
                ddl_asset_no_qa(ddlasset_no_trnf, _func_dmu.zStringToInt(ddName.SelectedValue), _func_dmu.zStringToInt(ddlplace.SelectedValue));
                break;

            case "ddlsearch_organization":
                action_select_depertment(0, "0");

                break;

            case "ddlAddDep_trnf_en":

                ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_en");
                ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_en");
                ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_en");
                ddl_Sec(ddlAddSec_trnf, _func_dmu.zStringToInt(ddlAddOrg_trnf.SelectedValue), _func_dmu.zStringToInt(ddlAddDep_trnf.SelectedValue));
               
                break;

        }
    }
    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        _FormView = getFv(ViewState["mode"].ToString());
        switch (cmdName)
        {
            case "btnTooffice":
                ViewState["status"] = "office";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTomanager":
                ViewState["status"] = "manager";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTodirector":
                ViewState["status"] = "director";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTooffice_Agency":
                ViewState["status"] = "office_agency";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTomanager_Agency":
                ViewState["status"] = "manager_agency";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTodirector_Agency":
                ViewState["status"] = "director_agency";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTooffice_Asset":
                ViewState["status"] = "office_asset";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTomanager_Asset":
                ViewState["status"] = "manager_asset";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "btnTodirector_Asset":
                ViewState["status"] = "director_asset";
                setActiveTab(ViewState["status"].ToString());
                ShowListTranfer_approve();
                break;

            case "_divMenuBtnToDivChangeOwn":
                ShowListTranfer();
                break;
            case "btntrnf_it":
                setActiveTab("transfer");
                ViewState["mode"] = "trnf_create";
                MvMaster.SetActiveView(View_AssetTransfer_Insert);
                FvTrnfInsert.ChangeMode(FormViewMode.Insert);
                FvTrnfData.ChangeMode(FormViewMode.Insert);
                _func_dmu.zSetFormViewData(FvTrnfInsert, null);
                _func_dmu.zSetFormViewData(FvTrnfData, null);
                setMode(1);

                break;
            case "btntrnf_en":
                setActiveTab("transfer_en");
                ViewState["mode"] = "trnf_create";
                MvMaster.SetActiveView(View_AssetTransfer_Insert);
                FvTrnfInsert.ChangeMode(FormViewMode.Insert);
                FvTrnfData.ChangeMode(FormViewMode.Insert);
                _func_dmu.zSetFormViewData(FvTrnfInsert, null);
                _func_dmu.zSetFormViewData(FvTrnfData, null);
                setMode(3);

                break;
            case "btntrnf_hr":
                setActiveTab("transfer_hr");
                ViewState["mode"] = "trnf_create";
                MvMaster.SetActiveView(View_AssetTransfer_Insert);
                FvTrnfInsert.ChangeMode(FormViewMode.Insert);
                FvTrnfData.ChangeMode(FormViewMode.Insert);
                _func_dmu.zSetFormViewData(FvTrnfInsert, null);
                _func_dmu.zSetFormViewData(FvTrnfData, null);
               // setDefaultTrnf();
                select_system(_ddlsystem);
                setMode(_func_dmu.zStringToInt(_ddlsystem.SelectedValue));
                ViewState["status"] = "preview";
                break;
            case "tbnAddTransferDevice":
                add_trnf_it();
                break;
            case "tbnAddTransferDevice_en":
                add_trnf_en();
                break;
            case "tbnAddTransferDevice_hr":
                add_trnf_hr();
                break;
            case "tbnAddTransferDevice_qa":
                add_trnf_qa();
                break;
            case "tbnAddTransferDevice_other":
                add_trnf_other();
                break;
            case "btnSaveAdd_trnf":
                if (check_error_trnsf_it() == true)
                {
                    setObject_transferIT();
                    if (Savetrnf_it(_func_dmu.zStringToInt(_hfu0_transf_idx.Value), _func_dmu.zStringToInt(cmdArg)) == true)
                    {
                        ShowListTranfer();
                    }
                }

                break;
            case "btnmanage_trnf":
                if(ViewState["status"].ToString() == "preview")
                {
                    setActiveTab("transfer_hr");
                }
                else
                {
                    setActiveTab(ViewState["status"].ToString());
                }
                
                MvMaster.SetActiveView(View_AssetTransfer_Insert);
                FvTrnfInsert.ChangeMode(FormViewMode.Edit);
                FvTrnfData.ChangeMode(FormViewMode.Edit);

                ShowDataUpdate(_func_dmu.zStringToInt(cmdArg));
                checkflowitem();
                break;
            case "btnAddCancel_trnf":
                ShowListTranfer();
                break;
            case "btnSaveAddReject_trnf":
                setObject_transferIT();
                if (Savetrnf_it(_func_dmu.zStringToInt(_hfu0_transf_idx.Value), _func_dmu.zStringToInt(cmdArg)) == true)
                {
                    ShowListTranfer();
                }

                break;
            case "btnSaveAdd_trnf_app":
                setObject_transferIT();
                if (Savetrnf_it(_func_dmu.zStringToInt(_hfu0_transf_idx.Value), _func_dmu.zStringToInt(cmdArg)) == true)
                {
                    ShowListTranfer_approve();
                    setActiveTab(ViewState["status"].ToString());
                }

                break;
            case "btnAddCancel_trnf_app":
                ShowListTranfer_approve();
                setActiveTab(ViewState["status"].ToString());
                break;
            case "confirm_approve_trnf":
                confirm_approve_trnf(_func_dmu.zStringToInt(cmdArg));
                ShowListTranfer_approve();
                break;
            case "confirm_no_approve_trnf":
                confirm_approve_trnf(_func_dmu.zStringToInt(cmdArg));
                ShowListTranfer_approve();
                break;

            case "bnDeletefile":

                try
                {
                    string Pathfile = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
                    string filesLoc = Server.MapPath(Pathfile + "/" + cmdArg);
                    File.Delete(filesLoc);
                }
                catch { }
                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
                    Label lbdocument_code = (Label)FvTrnfInsert.FindControl("lbdocument_code");
                    string filePathLotus = Server.MapPath(getPathLotus + lbdocument_code.Text);
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    GridView gvFile_transfer = (GridView)FvTrnfInsert.FindControl("gvFile_transfer");
                    SearchDirectories_foreditFile(myDirLotus, lbdocument_code.Text, gvFile_transfer);

                }
                catch
                {

                }
                break;
            case "showBoxsearch":
                btnshowBoxsearch.Visible = false;
                btnhiddenBoxsearch.Visible = true;
                panel_search.Visible = true;
                setSearchList();

                break;
            case "hiddenBoxsearch":
                btnshowBoxsearch.Visible = true;
                btnhiddenBoxsearch.Visible = false;
                panel_search.Visible = false;
                setSearchList();

                break;
            case "searching":
                ShowDataIndexTrnf("preview");

                break;
            case "reset_search":
                setSearchList();
                ShowDataIndexTrnf("preview");
                break;

        }
    }

    #endregion

    #region strat teppanop
    // strat teppanop
    protected void select_systemtype(DropDownList ddlName, int sysidx)
    {
        // tb its_m0_typereference

        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();
        system.SysIDX = sysidx;
        _dtitseet.boxprice_reference[0] = system;

        _dtitseet = callServicePostITAsset(_urlSelectSystem_TypePrice, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "type_name", "typidx");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทอุปกรณ์ขอซื้อ...", "0"));

    }

    protected data_purchase callServicePurchase(string _cmdUrl, data_purchase _data_purchase)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_purchase);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_purchase = (data_purchase)_funcTool.convertJsonToObject(typeof(data_purchase), _localJson);

        return _data_purchase;
    }


    protected void select_devices(DropDownList ddlName, int sysidx, int typidx)
    {

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_position = new its_lookup();

        obj_position.sysidx = sysidx;
        obj_position.typidx = typidx;
        obj_position.operation_status_id = "group_devices";

        _dataitasset.its_lookup_action[0] = obj_position;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);
        ddlName.Items.Clear();
        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกกลุ่มอุปกรณ์", "0"));

    }

    protected void select_ddlsearch_purchase(DropDownList ddlName, string status = "")
    {


        // tb purchase_m0_purchasetype

        _FormView = getFv(ViewState["mode"].ToString());
        DropDownList ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
        DropDownList ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        if (ddlsystem != null)
        {
            ddlName.Items.Clear();

            data_itasset dataitasset = new data_itasset();
            dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_m0type = new its_lookup();

            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

            obj_m0type.idx = _func_dmu.zStringToInt(ddlsystem.SelectedValue);
            obj_m0type.typidx = 0;// _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue);
            obj_m0type.operation_status_id = "m0_type";
            dataitasset.its_lookup_action[0] = obj_m0type;
            dataitasset = callServicePostITAsset(_urlGetits_lookup, dataitasset);

            ddlName.DataSource = dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
        }
        else if (status == "index")
        {
            ddlName.Items.Clear();

            data_itasset dataitasset = new data_itasset();
            dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_m0type = new its_lookup();

            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

            obj_m0type.idx = 3;
            obj_m0type.typidx = 3;
            obj_m0type.operation_status_id = "m0_type";
            dataitasset.its_lookup_action[0] = obj_m0type;
            dataitasset = callServicePostITAsset(_urlGetits_lookup, dataitasset);

            ddlName.DataSource = dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
        }

    }

    protected void select_position(DropDownList ddlName, int _type)
    {

        ddlName.Items.Clear();

        if (_type == 0)
        {
            data_itasset _dataitasset = new data_itasset();
            _dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_position = new its_lookup();

            obj_position.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
            obj_position.operation_status_id = "position";

            _dataitasset.its_lookup_action[0] = obj_position;
            // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

            _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

            ddlName.DataSource = _dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกตำแหน่ง", "0"));

        }
        else if (_type == 1)
        {
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกตำแหน่ง", "0"));
        }


    }


    protected void select_costcenter(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

       // obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "costcenter";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกรหัสหน่วยงาน", "0"));

    }

    protected void select_place(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "place";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสถานที่", "0"));

    }
    protected void Select_master_spec(int sysidx, int typidx, int tdidx, int posidx)
    {
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
        _txtlist_menu = (TextBox)_FormView.FindControl("txtlist_buynew");
        _txt_m1_ref_idx = (TextBox)_FormView.FindControl("txt_m1_ref_idx");

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_lookup = new its_u_document();

        obj_lookup.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_lookup.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_lookup.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        obj_lookup.posidx = posidx;
        obj_lookup.sysidx = sysidx;
        obj_lookup.typidx = typidx;
        obj_lookup.tdidx = tdidx;
        obj_lookup.operation_status_id = "its_m1_reference_sel";

        _dataitasset.its_u_document_action[0] = obj_lookup;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        _ddlspecidx.Text = "";
        _txtlist_menu.Text = "";
        _txt_m1_ref_idx.Text = "0";
        if (_dataitasset.its_u_document_action != null)
        {
            foreach (var item in _dataitasset.its_u_document_action)
            {
                _ddlspecidx.Text = item.leveldevice;
                _txtlist_menu.Text = item.detail;
                _txt_m1_ref_idx.Text = item.m1_ref_idx.ToString();
            }
        }


    }

    protected void Select_master_spec_detail(TextBox txtName, TextBox txtprice, int sysidx, int typidx, string leveldevice, int idx)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.sysidx = sysidx;
        obj_lookup.typidx = typidx;
        obj_lookup.leveldevice = leveldevice;
        obj_lookup.idx = idx;
        obj_lookup.operation_status_id = "spec_detail";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text  = _funcTool.convertObjectToJson(_dataitasset);
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);
        if (_dataitasset.its_lookup_action != null)
        {
            string str = _dataitasset.its_lookup_action[0].zname;
            txtName.Text = str;
            str = _dataitasset.its_lookup_action[0].price.ToString();
            //  txtprice.Text = str;
        }
        else
        {
            txtName.Text = "";
            txtprice.Text = "0";
        }

    }

    public string getStrformate(string str, int iformat)
    {
        str = _func_dmu.zFormatfloat(str, iformat);
        return str;
    }
    private void setObject()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        _ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
        _ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
        _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");
        _ddlposition = (DropDownList)_FormView.FindControl("ddlposition");
        _ddlcurrency = (DropDownList)_FormView.FindControl("ddlcurrency");
        _ddlunit = (DropDownList)_FormView.FindControl("ddlunit");
        _ddlcostcenter = (DropDownList)_FormView.FindControl("ddlcostcenter");
        _ddlplace = (DropDownList)_FormView.FindControl("ddlplace");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");

        _txtremark = (TextBox)_FormView.FindControl("txtremark");
        _txtref_itnum = (TextBox)_FormView.FindControl("txtref_itnum");
        _txtionum = (TextBox)_FormView.FindControl("txtionum");

        _pnl_ionum = (Panel)_FormView.FindControl("pnl_ionum");
        _pnl_ref_itnum = (Panel)_FormView.FindControl("pnl_ref_itnum");

        _txtbudget_set = (TextBox)_FormView.FindControl("txtbudget_set");
        _txtlist_menu = (TextBox)_FormView.FindControl("txtlist_buynew");
        _txtqty = (TextBox)_FormView.FindControl("txtqty_buynew");
        _txtprice = (TextBox)_FormView.FindControl("txtprice_buynew");
        _txt_m1_ref_idx = (TextBox)_FormView.FindControl("txt_m1_ref_idx");

        _ddl_target = (DropDownList)_FormView.FindControl("ddl_target");

        _GvReportAdd = (GridView)_FormView.FindControl("GvReportAdd");

    }

    protected void checkindexchange(object sender, EventArgs e)
    {
        var checkbox = (CheckBox)sender;

        switch (checkbox.ID)
        {

            //transfer
            case "cbrecipients_trnf":
                if (ViewState["v_GvTrnfIndex"] != null)
                {

                    its_TransferDevice_u0[] _its_TransferDevice_u0_list = (its_TransferDevice_u0[])ViewState["v_GvTrnfIndex"];

                    int _checked = int.Parse(checkbox.Text);
                    _its_TransferDevice_u0_list[_checked].selected = checkbox.Checked;

                }
                break;
            case "check_approve_all_trnf":

                if (checkbox.Checked == true)
                {
                    GridView grdlist = GvTrnfIndex;
                    Boolean _boolean = true;
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients_trnf");
                        HiddenField hfSelected = (HiddenField)gridrow.Cells[0].FindControl("hfSelected");
                        if (ViewState["v_GvTrnfIndex"] != null)
                        {
                            its_TransferDevice_u0[] _its_TransferDevice_u0_list = (its_TransferDevice_u0[])ViewState["v_GvTrnfIndex"];

                            checklist.Checked = true;
                            checklist.Enabled = false;
                            hfSelected.Value = _boolean.ToString();
                            //int _checked = int.Parse(checkbox.Text);
                            //_its_TransferDevice_u0_list[_checked].selected = checkbox.Checked;
                        }

                    }

                }
                else if (checkbox.Checked == false)
                {
                    GridView grdlist = GvTrnfIndex;
                    Boolean _boolean = false;
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients_trnf");
                        HiddenField hfSelected = (HiddenField)gridrow.Cells[0].FindControl("hfSelected");
                        if (ViewState["v_GvTrnfIndex"] != null)
                        {
                            its_TransferDevice_u0[] _its_TransferDevice_u0_list = (its_TransferDevice_u0[])ViewState["v_GvTrnfIndex"];

                            checklist.Checked = false;
                            checklist.Enabled = true;
                            hfSelected.Value = _boolean.ToString();
                            //int _checked = int.Parse(checkbox.Text);
                            //_its_TransferDevice_u0_list[_checked].selected = checkbox.Checked;
                        }
                    }

                }

                selapprove_all_trnf(checkbox.Checked);

                break;
            case "check_transfer_flag":
                DropDownList ddllocate_hr = (DropDownList)FvTrnfInsert.FindControl("ddllocate_hr");
                if (checkbox.Checked == true)
                {
                    ddllocate_hr.Enabled = true;
                }
                else if (checkbox.Checked == false)
                {
                    ddllocate_hr.SelectedValue = ViewState["locidx"].ToString();
                    ddllocate_hr.Enabled = false;
                }

                break;
            case "check_transfer_flag_en":
                DropDownList ddllocate_en = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en");
                if (checkbox.Checked == true)
                {
                    ddllocate_en.Enabled = true;
                }
                else if (checkbox.Checked == false)
                {
                    ddllocate_en.SelectedValue = ViewState["locidx"].ToString();
                    ddllocate_en.Enabled = false;
                }

                break;

        }


    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {

            case "GvTrnfIndex":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients_trnf");
                    CheckBox check_approve_all = check_approve_all_trnf;

                    //test
                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    //test

                    if (check_approve_all.Checked || bool.Parse(hfSelected.Value) == true) //
                    {
                        cbrecipients.Checked = true;

                        if (check_approve_all.Checked && cbrecipients.Checked)
                        {
                            cbrecipients.Enabled = false;
                        }


                    }

                    else
                    {
                        cbrecipients.Checked = false;
                        cbrecipients.Enabled = true;
                    }

                }

                break;

            case "GvTransferDevice":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    HiddenField hfNodeIdx = (HiddenField)FvTrnfData.FindControl("hfNodeIdx");
                    HiddenField hfActorIdx = (HiddenField)FvTrnfData.FindControl("hfActorIdx");
                    HiddenField hfdoc_status = (HiddenField)FvTrnfData.FindControl("hfdoc_status");
                    Label lb_book_val = (Label)e.Row.FindControl("lb_book_val");
                    TextBox txt_book_val = (TextBox)e.Row.FindControl("txt_book_val");
                    Label lb_price = (Label)e.Row.FindControl("lb_price");
                    TextBox txt_price = (TextBox)e.Row.FindControl("txt_price");
                    lb_book_val.Visible = true;
                    txt_book_val.Visible = false;
                    lb_price.Visible = true;
                    txt_price.Visible = false;
                    if (hfNodeIdx != null)
                    {
                        if (
                            /*
                            (
                            (_funcTool.convertToInt(hfNodeIdx.Value) == 13)
                            &&
                            (_funcTool.convertToInt(hfActorIdx.Value) == 1)
                            &&
                            (_funcTool.convertToInt(hfdoc_status.Value) == 9)
                            )
                            ||
                            (
                            (_funcTool.convertToInt(hfNodeIdx.Value) == 1)
                            &&
                            (_funcTool.convertToInt(hfActorIdx.Value) == 1)
                            &&
                            (_funcTool.convertToInt(hfdoc_status.Value) == 1)
                            )
                            */
                            (
                            (
                            (_funcTool.convertToInt(hfNodeIdx.Value) == 7) || 
                            (_funcTool.convertToInt(hfNodeIdx.Value) == 16) 
                            )
                            &&
                            (_funcTool.convertToInt(hfActorIdx.Value) == 5)
                            &&
                            (_funcTool.convertToInt(hfdoc_status.Value) == 9)
                            )
                            )
                        {
                            if(ViewState["mode"] != null)
                            {
                                if (ViewState["mode"].ToString() == "trnf_approve_list")
                                {
                                    lb_book_val.Visible = false;
                                    txt_book_val.Visible = true;
                                    lb_price.Visible = false;
                                    txt_price.Visible = true;
                                }
                            }
                            
                                
                        }
                    }

                }

                break;
            case "gvFile_transfer":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
                    Label hidden_doccode = (Label)FvTrnfInsert.FindControl("lbdocument_code");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11");
                    LinkButton bnDeletefile = (LinkButton)e.Row.Cells[0].FindControl("bnDeletefile");
                    string LinkHost11 = getPathLotus + hidden_doccode.Text + "/" + ltFileName11.Text;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);
                    bnDeletefile.CommandArgument = hidden_doccode.Text + "/" + ltFileName11.Text;
                    HiddenField hfNodeIdx = (HiddenField)FvTrnfData.FindControl("hfNodeIdx");
                    HiddenField hfemp_idx = (HiddenField)FvTrnfData.FindControl("hfemp_idx");
                    if (hfNodeIdx.Value == "13")
                    {
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            bnDeletefile.Visible = true;
                        }
                        else
                        {
                            bnDeletefile.Visible = false;
                        }
                    }
                    else
                    {
                        bnDeletefile.Visible = false;
                    }
                }
                break;

        }
    }

    private void setdocket_u1_list_print(GridView Gv, int u0_docket_idx, int org_idx_its)
    {
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.org_idx_its = org_idx_its;
        select_its.operation_status_id = "docket_u1_list_print";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        _func_dmu.zSetGridData(Gv, _dtitseet.its_u_document_action);
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {

            //transfer
            case "GvTrnfIndex":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["v_GvTrnfIndex"]);
                SETFOCUS.Focus();

                break;

        }

    }
    private void setu1_document(int id, GridView Gv)
    {
        data_itasset dataitasset = new data_itasset();
        its_u_document obj_detail = new its_u_document();
        dataitasset.its_u_document_action = new its_u_document[1];
        obj_detail.idx = id;
        obj_detail.operation_status_id = "list_index_u1";
        dataitasset.its_u_document_action[0] = obj_detail;
        dataitasset = callServicePostITAsset(_urlGetits_u_document, dataitasset);
        _func_dmu.zSetGridData(Gv, dataitasset.its_u_document_action);

    }
    private void setPanel_doc_number()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _txtref_itnum = (TextBox)_FormView.FindControl("txtref_itnum");
        _txtionum = (TextBox)_FormView.FindControl("txtionum");
        _pnl_ionum = (Panel)_FormView.FindControl("pnl_ionum");
        _pnl_ref_itnum = (Panel)_FormView.FindControl("pnl_ref_itnum");
        _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");

        _pnl_ionum.Visible = false;
        _pnl_ref_itnum.Visible = false;
        if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 1) //ซื้อใหม่
        {
            _pnl_ionum.Visible = true;
            _txtref_itnum.Text = "";
        }
        else if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 2) //ซื้อทดแทน
        {
            _pnl_ionum.Visible = true;
            _pnl_ref_itnum.Visible = true;
        }
        else if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 3) //ต่ออายุ
        {
            _pnl_ionum.Visible = true;
            _txtref_itnum.Text = "";
        }
    }

    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            //
        }
    }

    private void setViewStateDefalut()
    {
        ViewState["u0idx"] = "";
        ViewState["node_idx"] = "";
        ViewState["emp_idx_its"] = "";
        ViewState["pr_type_idx"] = "";
        ViewState["actor_idx"] = "";
        ViewState["_DOCUMENTCODE"] = "";
        ViewState["doc_status"] = "";
        ViewState["flow_item"] = "";
        ViewState["doccode"] = "";
    }


    public void SearchDirectories_foreditFile(DirectoryInfo dir, String target, GridView gridview)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {

                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;

            }

        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gridview.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gridview.DataBind();
            ds1.Dispose();
        }
        else
        {

            gridview.DataSource = null;
            gridview.DataBind();

        }



    }
    
    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        _divMenuLiToDivChangeOwn.Attributes.Remove("class");
        _divMenuLiTohr.Attributes.Remove("class");

        // รายการรออนุมัติ
        _divMenuLiToDivWaitApprove.Attributes.Remove("class");
        _divMenuLiTooffice.Attributes.Remove("class");
        _divMenuLiToDivmanager.Attributes.Remove("class");
        _divMenuLiTodirector.Attributes.Remove("class");

        // รายการรออนุมัติโดยต้นสังกัดที่ดูแล
        _divMenuLiToDivAgency.Attributes.Remove("class");
        _divMenuLiTooffice_Agency.Attributes.Remove("class");
        _divMenuLiTomanager_Agency.Attributes.Remove("class");
        _divMenuLiTodirector_Agency.Attributes.Remove("class");

        // รายการรออนุมัติโดย Asset
        _divMenuLiToDivAsset.Attributes.Remove("class");
        _divMenuLiTooffice_Asset.Attributes.Remove("class");
        _divMenuLiTomanager_Asset.Attributes.Remove("class");
        _divMenuLiTodirector_Asset.Attributes.Remove("class");

        switch (activeTab)
        {

            case "transfer":
                _divMenuLiToDivChangeOwn.Attributes.Add("class", "active");
                break;

            case "preview":
                _divMenuLiToDivChangeOwn.Attributes.Add("class", "active");
                break;

            case "transfer_hr":
                _divMenuLiTohr.Attributes.Add("class", "active");
                break;
            // รายการรออนุมัติ   
            case "office":
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiTooffice.Attributes.Add("class", "active");
                break;

            case "manager":
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivmanager.Attributes.Add("class", "active");
                break;

            case "director":
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiTodirector.Attributes.Add("class", "active");
                break;

            // รายการรออนุมัติโดยต้นสังกัดที่ดูแล

            case "office_agency":
                _divMenuLiToDivAgency.Attributes.Add("class", "active");
                _divMenuLiTooffice_Agency.Attributes.Add("class", "active");
                break;

            case "manager_agency":
                _divMenuLiToDivAgency.Attributes.Add("class", "active");
                _divMenuLiTomanager_Agency.Attributes.Add("class", "active");
                break;

            case "director_agency":
                _divMenuLiToDivAgency.Attributes.Add("class", "active");
                _divMenuLiTodirector_Agency.Attributes.Add("class", "active");
                break;
            // รายการรออนุมัติโดย Asset
            case "office_asset":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiTooffice_Asset.Attributes.Add("class", "active");
                break;

            case "manager_asset":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiTomanager_Asset.Attributes.Add("class", "active");
                break;

            case "director_asset":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiTodirector_Asset.Attributes.Add("class", "active");
                break;


        }


    }
    #endregion setActiveTab

    private FormView getFv(string _sMode)
    {
        return FvTrnfInsert;
        //if (_sMode == "I")
        //{
        //    return FvInsertBuyNew;
        //}
        //else
        //{
        //    return FvUpdateBuyNew;
        //}
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }

    protected void select_costcenter_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "costcenter_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก costcenter", "0"));

    }

    protected void select_type_devices_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "type_devices_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทอุปกรณ์", "0"));

    }

    protected void select_node_status_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "node_status_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสถานะรายการ", "0"));

    }

    public void showMsAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }


    //it

    public string zsetMonthYear(int Month, int Year)
    {
        return _func_dmu.zMonthTH(Month) + " " + Year.ToString();
    }


    private void setpermission_admin_menu()
    {
        Boolean boolean = false;

        
        _liMenuLiTomanager.Visible = boolean;
        _liMenuLiTodirector.Visible = boolean;

        _liMenuLiTooffice_Agency.Visible = boolean;
        _liMenuLiTomanager_Agency.Visible = boolean;
        _liMenuLiTodirector_Agency.Visible = boolean;

        _liMenuLiTooffice_Asset.Visible = boolean;
        _liMenuLiTomanager_Asset.Visible = boolean;
        _liMenuLiTodirector_Asset.Visible = boolean;

        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.emp_idx = emp_idx;
        select_its.operation_status_id = "permission_admin_jobgrade_menu";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        // check manager / director
        if (dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in dtitseet.its_TransferDevice_u0_action)
            {
                if (item.zstatus == "M")
                {
                    if (item.joblevel == _jobManager)
                    {
                        _liMenuLiTomanager.Visible = true;
                    }
                }
                else if (item.zstatus == "D")
                {
                    if (item.joblevel >= _jobDirector)
                    {
                      //  _liMenuLiTodirector.Visible = true;
                    }
                }
            }
        }

        dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        select_its = new its_TransferDevice_u0();
        select_its.emp_idx = emp_idx;
        select_its.operation_status_id = "permission_admin_agency_menu";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        // check asset manager / director
        if (dtitseet.its_TransferDevice_u0_action != null)
        {

            foreach (var item in dtitseet.its_TransferDevice_u0_action)
            {
                if (item.zstatus == "O")
                {
                    if (item.joblevel > 0)
                    {
                        _liMenuLiTooffice_Agency.Visible = true;
                    }
                }
                else if (item.zstatus == "M")
                {

                    if (item.joblevel > 0)
                    {
                        _liMenuLiTomanager_Agency.Visible = true;

                    }
                }
                else if (item.zstatus == "D")
                {
                    if (item.joblevel >= _jobDirector)
                    {
                       // _liMenuLiTodirector_Agency.Visible = true;
                    }
                }
            }
        }

        dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        select_its = new its_TransferDevice_u0();
        select_its.emp_idx = emp_idx;
        select_its.operation_status_id = "permission_admin_asset_menu";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        // check asset manager / director
        if (dtitseet.its_TransferDevice_u0_action != null)
        {
            
            foreach (var item in dtitseet.its_TransferDevice_u0_action)
            {
                if (item.zstatus == "O")
                {
                    if (item.joblevel > 0)
                    {
                        _liMenuLiTooffice_Asset.Visible = true;
                    }
                }
                else if (item.zstatus == "M")
                {
                    
                    if (item.joblevel > 0)
                    {
                        _liMenuLiTomanager_Asset.Visible = true;
                       
                    }
                }
                else if (item.zstatus == "D")
                {
                    if (item.joblevel >= _jobDirector)
                    {
                      //  _liMenuLiTodirector_Asset.Visible = true;
                    }
                }
            }
        }
        
        if (
            (_divMenuLiTooffice.Visible == false) &&
            (_liMenuLiTomanager.Visible == false) &&
            (_liMenuLiTodirector.Visible == false)
            )
        {
            _divMenuLiToDivWaitApprove.Visible = false;
        }
        else
        {
            _divMenuLiToDivWaitApprove.Visible = true;
        }


        if (
            (_liMenuLiTooffice_Agency.Visible == false) &&
            (_liMenuLiTomanager_Agency.Visible == false) &&
            (_liMenuLiTodirector_Agency.Visible == false)
            )
        {
            _divMenuLiToDivAgency.Visible = false;
        }
        else
        {
            _divMenuLiToDivAgency.Visible = true;
        }

        if (
            (_liMenuLiTooffice_Asset.Visible == false) &&
            (_liMenuLiTomanager_Asset.Visible == false) &&
            (_liMenuLiTodirector_Asset.Visible == false)
            )
        {
            _divMenuLiToDivAsset.Visible = false;
        }
        else
        {
            _divMenuLiToDivAsset.Visible = true;
        }
        

    }

    private void setdefaultNotification()
    {

        //if (ViewState["rsec_idx"].ToString() != "210")
        //{
        int icount = 1;
        for (int i = 1; i <= icount; i++)
        {
            setNotification(i, 0);
        }
        //}

    }

    private void setNotification(int _item, int _value)
    {
        data_itasset dataitasset = new data_itasset();
        dataitasset.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        select_its.emp_idx = emp_idx;
        select_its.operation_status_id = "m0_menu";
        select_its.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        select_its.joblevel = _func_dmu.zStringToInt(ViewState["jobgrade_level"].ToString());
        select_its.LocIDX = _func_dmu.zStringToInt(ViewState["locidx"].ToString());

        dataitasset.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
        dataitasset = callServicePostITAsset(_urlGetits_u_transfer, dataitasset);

        if (dataitasset.its_TransferDevice_u0_action != null)
        {
            LinkButton lnkbtn = new LinkButton();
            string sLabel = ""
                , sapprove = ""
                , sapprove_Agency = ""
                 , sapprove_Asset = ""
                ;
            int iapprove = 0
                , iapprove_Agency = 0
                , iapprove_Asset = 0
                , ic_approve = 0
                , ic_approve_Agency = 0
                , ic_approve_Asset = 0
                ;

            foreach (var item in dataitasset.its_TransferDevice_u0_action)
            {

                int ic = 0;
                //**********รายการรออนุมัติ**********//
                //เจ้าหน้าที่
                if ((item.menu_seq == 1) && (item.menu_list_seq == 1))
                {

                    lnkbtn = btnTooffice;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove = item.menu;
                    if (_divMenuLiTooffice.Visible == true)
                    {
                        iapprove += item.zCount;
                        ic_approve++;
                    }

                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้จัดการฝ่าย
                if ((item.menu_seq == 1) && (item.menu_list_seq == 2))
                {

                    lnkbtn = btnTomanager;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove = item.menu;
                    if (_liMenuLiTomanager.Visible == true)
                    {
                        iapprove += item.zCount;
                        ic_approve++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้อำนวยการฝ่าย
                if ((item.menu_seq == 1) && (item.menu_list_seq == 3))
                {

                    lnkbtn = btnTodirector;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove = item.menu;
                    if (_liMenuLiTodirector.Visible == true)
                    {
                        iapprove += item.zCount;
                        ic_approve++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                if (ic_approve > 0)
                {
                    setNotificationMenulabel(_lbMenuLiToDivWaitApprove
                               , sapprove
                               , iapprove
                               );

                }

                //**********รายการรออนุมัติโดยต้นสังกัดที่ดูแล**********//
                //เจ้าหน้าที่
                if ((item.menu_seq == 2) && (item.menu_list_seq == 1))
                {

                    lnkbtn = btnTooffice_Agency;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Agency = item.menu;
                    if (_liMenuLiTooffice_Agency.Visible == true)
                    {
                        iapprove_Agency += item.zCount;
                        ic_approve_Agency++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้จัดการฝ่าย
                if ((item.menu_seq == 2) && (item.menu_list_seq == 2))
                {

                    lnkbtn = btnTomanager_Agency;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Agency = item.menu;
                    if (_liMenuLiTomanager_Agency.Visible == true)
                    {
                        iapprove_Agency += item.zCount;
                        ic_approve_Agency++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้อำนวยการฝ่าย
                if ((item.menu_seq == 2) && (item.menu_list_seq == 3))
                {

                    lnkbtn = btnTodirector_Agency;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Agency = item.menu;
                    if (_liMenuLiTodirector_Agency.Visible == true)
                    {
                        iapprove_Agency += item.zCount;
                        ic_approve_Agency++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                if (ic_approve_Agency > 0)
                {
                    setNotificationMenulabel(_divMenuLiToLbAgency
                               , sapprove_Agency
                               , iapprove_Agency
                               );

                }

                //**********รายการรออนุมัติโดย Asset**********//
                //เจ้าหน้าที่
                if ((item.menu_seq == 3) && (item.menu_list_seq == 1))
                {

                    lnkbtn = btnTooffice_Asset;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Asset = item.menu;
                    if (_liMenuLiTooffice_Asset.Visible == true)
                    {
                        iapprove_Asset += item.zCount;
                        ic_approve_Asset++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้จัดการฝ่าย
                if ((item.menu_seq == 3) && (item.menu_list_seq == 2))
                {

                    lnkbtn = btnTomanager_Asset;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Asset = item.menu;
                    if (_liMenuLiTomanager_Asset.Visible == true)
                    {
                        iapprove_Asset += item.zCount;
                        ic_approve_Asset++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                //ผู้อำนวยการฝ่าย
                if ((item.menu_seq == 3) && (item.menu_list_seq == 3))
                {

                    lnkbtn = btnTodirector_Asset;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    sapprove_Asset = item.menu;
                    if (_liMenuLiTodirector_Asset.Visible == true)
                    {
                        iapprove_Asset += item.zCount;
                        ic_approve_Asset++;
                    }
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }
                if (ic_approve_Asset > 0)
                {
                    setNotificationMenulabel(_divMenuLiToLbAsset
                               , sapprove_Asset
                               , iapprove_Asset
                               );

                }



            }



        }


    }

    private void setNotificationMenubtn(LinkButton _linkbtn, string _label, int _value)
    {
        _linkbtn.Text = _label + " <span class='badge progress-bar-danger' >" + Convert.ToString(_value) + "</span>";
    }
    private void setNotificationMenulabel(Label _linkbtn, string _label, int _value)
    {
        _linkbtn.Text = _label + " <span class='badge progress-bar-danger' >" + Convert.ToString(_value) + "</span>";
    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            if (textbox.ID == "txtionum")
            {
                //setionumber_default();
            }
        }
    }
    protected void select_m0status(DropDownList ddlName, int mode)
    {
        ddlName.Items.Clear();
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.operation_status_id = "m0_status";
        obj_lookup.idx = mode;
        _dataitasset.its_lookup_action[0] = obj_lookup;
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("ผลการอนุมัติ.....", "0"));

    }

    private void select_data(int id)
    {
        TextBox txtremark = (TextBox)_FormView.FindControl("txtlist_buynew");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
        DataSet dsContacts = (DataSet)ViewState["vsits_showmodal"];

        foreach (DataRow dr in dsContacts.Tables["dsits_showmodal"].Rows)
        {
            if (_func_dmu.zStringToInt(dr["id"].ToString()) == id)
            {

                _ddlspecidx.Text = dr["zname"].ToString();
                txtremark.Text = dr["detail"].ToString();
                txtremark.Focus();
                break;
            }
        }

    }
    protected void CreateDs_showmodal()
    {
        string sDs = "dsits_showmodal";
        string sVs = "vsits_showmodal";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zname", typeof(String));
        ds.Tables[sDs].Columns.Add("detail", typeof(String));

        ViewState[sVs] = ds;

    }

    private void setpermission_admin_dept()
    {
        if (ViewState["rdept_idx"].ToString() == "20") // it
        {
            ViewState["sysidx_admin"] = 3;
        }
        else if (ViewState["rdept_idx"].ToString() == "13") // hr
        {
            ViewState["sysidx_admin"] = 9;
        }
        else if (ViewState["rdept_idx"].ToString() == "11") // en
        {
            ViewState["sysidx_admin"] = 11;
        }
        else if (ViewState["rdept_idx"].ToString() == "9") // asset
        {
            ViewState["sysidx_admin"] = 12;
        }
        else
        {
            ViewState["sysidx_admin"] = "";
        }


    }

    private int getu0nodeidx(int sysidx, int idx_it, int idx_hr, int idx_en)
    {
        int u0nodeidx = 0;
        if (sysidx == 11)//en
        {
            u0nodeidx = idx_en;
        }
        else if (sysidx == 9)//hr
        {
            u0nodeidx = idx_hr;
        }
        else //it
        {
            u0nodeidx = idx_it;
        }

        return u0nodeidx;

    }

    protected void select_pos(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference pos = new price_reference();

        _dtitseet.boxprice_reference[0] = pos;

        _dtitseet = callServicePostITAsset(_urlSelectPos, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "pos_name", "posidx");
        ddlName.Items.Insert(0, new ListItem("ประเภทกลุ่มพนักงาน", "0"));

    }


    public string getDeviceStatus(string u0_code)
    {
        string str = "";
        if (u0_code != "")
        {
            str = "ลงทะเบียนเรียบร้อยแล้ว(" + u0_code + ")";
        }
        return str;
    }

    //start transfer devices

    protected string ddl_organization(DropDownList ddlName, int _type)
    {

        ddlName.Items.Clear();

        if (_type == 0)
        {

            data_softwarelicense __data_softwarelicense = new data_softwarelicense();
            __data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_organization = new organization_detail();

            __data_softwarelicense.organization_list[0] = select_organization;
            __data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, __data_softwarelicense);

            ddlName.DataSource = __data_softwarelicense.organization_list;
            ddlName.DataTextField = "OrgNameTH";
            ddlName.DataValueField = "OrgIDX";
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกองค์กร", "0"));
            // ddlsearch_organization.SelectedValue = _selectvalue;

        }
        else
        {
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกองค์กร", "0"));
        }

        return ddlName.SelectedItem.Value;


    }
    protected string ddl_depertment(DropDownList ddlName, int organization_idx, int _type)
    {

        ddlName.Items.Clear();

        if (_type == 0)
        {

            _data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_depertment = new organization_detail();

            select_depertment.OrgIDX = organization_idx;

            _data_softwarelicense.organization_list[0] = select_depertment;
            _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);

            ddlName.DataSource = _data_softwarelicense.organization_list;
            ddlName.DataTextField = "DeptNameTH";
            ddlName.DataValueField = "RDeptIDX";
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกฝ่าย", "0"));

        }
        else if (_type == 1)
        {
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกฝ่าย", "0"));
        }

        return ddlName.SelectedItem.Value;

    }


    protected void ddl_Sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        data_employee _dataEmployee = new data_employee();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        ddlName.Items.Clear();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dtEmployee.employee_list[0] = _empList;


        _dtEmployee = callServicePostEmployee(urlGetAll, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem(" เลือกผู้ถือครอง", "0"));
    }
    protected void SelectDevices(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกอุปกรณ์", "0"));
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_lookup_action = new its_lookup[1];
        its_lookup select_its = new its_lookup();
        select_its.operation_status_id = "sel_devices_trnf";
        dtitseet.its_lookup_action[0] = select_its;

        dtitseet = callServicePostITAsset(_urlGetits_lookup, dtitseet);

        ddlName.DataSource = dtitseet.its_lookup_action;
        ddlName.DataValueField = "idx";
        ddlName.DataTextField = "zname";
        ddlName.DataBind();

    }


    private void setUserMain(int _emp_idx)
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + _emp_idx.ToString());
        //litDebug.Text = _dtEmployee.employee_list[0].emp_name_th;
        FvDetailUser_Main.DataSource = _dtEmployee.employee_list;
        FvDetailUser_Main.DataBind();

        TextBox txt_rsec_idx = (TextBox)FvDetailUser_Main.FindControl("txt_rsec_idx");
        TextBox text_approve1 = (TextBox)FvDetailUser_Main.FindControl("text_approve1");
        TextBox text_approve2 = (TextBox)FvDetailUser_Main.FindControl("text_approve2");

        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.operation_status_id = "emp_approve_mg";
        select_its.emp_idx = _emp_idx;
        select_its.rdept_idx = _func_dmu.zStringToInt(txt_rsec_idx.Text);
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in _dtitseet.its_TransferDevice_u0_action)
            {
                ViewState["approve1"] = item.emp_code + " : "+item.emp_name_th;
            }
        }

        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        select_its = new its_TransferDevice_u0();
        select_its.operation_status_id = "emp_approve_dir";
        select_its.emp_idx = _emp_idx;
        select_its.rdept_idx = _func_dmu.zStringToInt(txt_rsec_idx.Text);
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in _dtitseet.its_TransferDevice_u0_action)
            {
                ViewState["approve2"] = item.emp_code + " : " + item.emp_name_th;
            }
        }
        
        text_approve1.Text = ViewState["approve1"].ToString();
        text_approve2.Text = ViewState["approve2"].ToString();

    }



    private void setTrnf_InsertDefault(int status = 0)
    {
        setObject_transferIT();
        if (status == 0)
        {
            ddl_organization(_ddlAddOrg_trnf, 0);
            ddl_depertment(_ddlAddDep_trnf, 1, 0);
            ddl_Sec(_ddlAddSec_trnf, 0, 0);
            getEmpList(_ddlAddemp_idx_trnf, int.Parse(_ddlAddOrg_trnf.SelectedValue), int.Parse(_ddlAddDep_trnf.SelectedValue), int.Parse(_ddlAddSec_trnf.SelectedValue));
            _txtplace_idx.Text = "";
            _txtplace_trnf.Text = "";
            _txtremark.Text = "";
        }
        // litDebug.Text = ViewState["locidx"].ToString();
        Select_Location_all(_ddllocate_it_search);
        _ddllocate_it_search.SelectedValue = ViewState["locidx"].ToString();
        _ddllocate_it_search.Enabled = false;
        SelectDevices(_ddlSearch_devices_trnf);
        ddl_asset_no(_ddlasset_no_trnf, 0, 0);


    }
    private void select_Trnf_Insert()
    {
        setObject_transferIT();
        ddl_depertment(_ddlAddDep_trnf, _func_dmu.zStringToInt(_ddlAddOrg_trnf.SelectedValue), 0);
        getEmpList(_ddlAddemp_idx_trnf, int.Parse(_ddlAddOrg_trnf.SelectedValue), int.Parse(_ddlAddDep_trnf.SelectedValue), int.Parse(_ddlAddSec_trnf.SelectedValue));
        SelectDevices(_ddlSearch_devices_trnf);
        _txtplace_idx.Text = "";
        _txtplace_trnf.Text = "";
        ddl_asset_no(_ddlasset_no_trnf, 0, 0);

    }

    protected void setLocation(int _idx)
    {
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.emp_idx = _idx;
        select_its.operation_status_id = "sel_location";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        _txtplace_idx = (TextBox)FvTrnfInsert.FindControl("txtplace_idx");
        _txtplace_trnf = (TextBox)FvTrnfInsert.FindControl("txtplace_trnf");
        _txtplace_idx.Text = "";
        _txtplace_trnf.Text = "";
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in _dtitseet.its_TransferDevice_u0_action)
            {
                _txtplace_idx.Text = item.idx.ToString();
                _txtplace_trnf.Text = item.zname;
            }
        }

    }
    protected void ddl_asset_no(DropDownList ddlName, int place_idx, int _idx)
    {
        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.place_idx = place_idx;
        select_its.tdidx = _idx;
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.operation_status_id = "sel_devices_it";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        setDdlData(ddlName, dtitseet.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new ListItem("เลือก Asset No", "0"));
    }

    protected void CreateDsits_TransferDevice_u1()
    {
        string sDs = "dsits_TransferDevice_u1";
        string sVs = "vsits_TransferDevice_u1";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_transf_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_transf_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("tdidx", typeof(String));
        ds.Tables[sDs].Columns.Add("tdidx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_status", typeof(String));
        ds.Tables[sDs].Columns.Add("device_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("asidx", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_no", typeof(String));
        ds.Tables[sDs].Columns.Add("qty", typeof(String));
        ds.Tables[sDs].Columns.Add("acquis_val", typeof(String));
        ds.Tables[sDs].Columns.Add("book_val", typeof(String));
        ds.Tables[sDs].Columns.Add("price", typeof(String));
        ds.Tables[sDs].Columns.Add("ref_itnum", typeof(String));
        ds.Tables[sDs].Columns.Add("ionum", typeof(String));
        ds.Tables[sDs].Columns.Add("desc_name", typeof(String));
        ds.Tables[sDs].Columns.Add("doc_u2idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_org_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_rdept_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_rsec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("o_LocIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("o_BuildingIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("o_RoomIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("subno_flag", typeof(String));
        ViewState[sVs] = ds;

    }

    protected void add_trnf_it()
    {
        setObject_transferIT();

        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr in dsContacts.Tables["dsits_TransferDevice_u1"].Rows)
        {

            if (dr["asset_no"].ToString() == _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf))
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                showMsAlert("มีข้อมูล Asset Num. นี้แล้ว!!!");
                return;
            }

        }

        //ckeck asset no
        its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_m.operation_status_id = "check_dupicat_trnf";
        obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
        obj_m.asset_no = _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf);
        _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            var item = _dtitseet.its_TransferDevice_u0_action[0];
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + item.asset_no + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
            return;
        }

        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.tdidx = _func_dmu.zStringToInt(_ddlSearch_devices_trnf.SelectedValue);
        select_its.device_idx = _func_dmu.zStringToInt(_ddlasset_no_trnf.SelectedValue);
        select_its.emp_idx = _func_dmu.zStringToInt(_ddlAddemp_idx_trnf.SelectedValue);
        select_its.place_idx = _func_dmu.zStringToInt(_ddllocate_it_search.SelectedValue);
        select_its.operation_status_id = "sel_devices_it_all";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);

        if (_GvReportAdd.Rows.Count == 0)
        {
            CreateDsits_TransferDevice_u1();
        }

        dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_TransferDevice_u0_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

                drContacts["id"] = ic;
                drContacts["u0_transf_idx"] = 0;
                drContacts["u1_transf_idx"] = 0;
                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.name_m0_typedevice;
                drContacts["u1_status"] = 1;
                drContacts["device_idx"] = _func_dmu.zStringToInt(_ddlasset_no_trnf.SelectedValue);
                drContacts["asidx"] = vdr.asidx;
                drContacts["asset_no"] = vdr.u0_acc;
                drContacts["qty"] = 1;
                drContacts["acquis_val"] = vdr.acquis_val;
                drContacts["book_val"] = 0;
                drContacts["price"] = 0;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["ionum"] = vdr.ionum;
                drContacts["desc_name"] = vdr.desc_name;
                drContacts["doc_u2idx"] = vdr.doc_u2idx;
                drContacts["o_org_idx"] = vdr.org_idx;
                drContacts["o_rdept_idx"] = vdr.rdept_idx;
                drContacts["o_rsec_idx"] = vdr.rsec_idx;
                drContacts["o_emp_idx"] = vdr.emp_idx;
                drContacts["o_rpos_idx"] = vdr.rpos_idx;

                dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบอุปกรณ์ !!!!')", true);
            return;
        }
        ViewState["vsits_TransferDevice_u1"] = dsContacts;

        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);

    }

    private void setMode(int imode)
    {
        /*
        
        else */
        ///litDebug.Text = imode.ToString();
        if (imode != 37)
        {
            if (imode == 3)
            {
                ViewState["sysidx"] = imode;
                setUserMain(emp_idx);
                setTrnf_InsertDefault();
                CreateDsits_TransferDevice_u1();
                _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);
                set_flowitem_default();
                checkflowitem();

            }
            {
                ViewState["sysidx"] = imode;
                setUserMain(emp_idx);
                setTrnf_InsertDefault_hr(0, imode);
                CreateDsits_TransferDevice_u1();
                _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);
                set_flowitem_default();
                checkflowitem();
            }
            
           
        }
        /*
        else if (imode == 11)
        {
            ViewState["sysidx"] = "11";
            setUserMain(emp_idx);
            _ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_en");
            _ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_en");
            ddl_organization(_ddlAddOrg_trnf, 0);
            ddl_depertment_en(_ddlAddDep_trnf, _func_dmu.zStringToInt(_ddlAddOrg_trnf.SelectedValue), 0, 0);
            DropDownList ddllocate_en = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en");
            DropDownList ddlbuilding_en = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en");
            DropDownList ddlroom_en = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en");
            Select_Location_all(ddllocate_en);
            Select_Building(ddlbuilding_en, int.Parse(ddllocate_en.SelectedValue));
            Select_Room(ddlroom_en, int.Parse(ddlbuilding_en.SelectedValue));

            setTrnf_InsertDefault_en(0, _func_dmu.zStringToInt(ViewState["locidx"].ToString()));
            CreateDsits_TransferDevice_u1();
            _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);
            set_flowitem_default();
            checkflowitem();
        }
        else if (imode == 36)
        {

            ViewState["sysidx"] = "36";
            setUserMain(emp_idx);
            setTrnf_InsertDefault_qa();
            CreateDsits_TransferDevice_u1();
            _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);
            set_flowitem_default();
            checkflowitem();
        }
        */
        
        else 
        {

            ViewState["sysidx"] = imode;
            setUserMain(emp_idx);
            setTrnf_InsertDefault_other();
            CreateDsits_TransferDevice_u1();
            _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);
            set_flowitem_default();
            checkflowitem();
        }
        /*
        else
        {

            ViewState["sysidx"] = "";
            setUserMain(emp_idx);
            setTrnf_InsertDefault();
            
            Panel pnl_it = (Panel)FvTrnfInsert.FindControl("pnl_it");
            Panel pnl_en = (Panel)FvTrnfInsert.FindControl("pnl_en");
            Panel pnl_hr_qa = (Panel)FvTrnfInsert.FindControl("pnl_hr_qa");
            Panel pnl_qa = (Panel)FvTrnfInsert.FindControl("pnl_qa");
            
            Panel pnl_other = (Panel)FvTrnfInsert.FindControl("pnl_other");
            
            Panel pnl_flow_item_1_remark = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_1_remark");
            Panel pnl_flow_item_2_uploadfile = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_2_uploadfile");
            LinkButton btnAdddata = (LinkButton)FvTrnfInsert.FindControl("btnAdddata");
            
            pnl_it.Visible = false;
            pnl_en.Visible = false;
            
            pnl_qa.Visible = false;
            pnl_other.Visible = false;
            setTrnf_InsertDefault_hr(0, 9);
            CreateDsits_TransferDevice_u1();
            _func_dmu.zSetGridData(_GvReportAdd, ViewState["vsits_TransferDevice_u1"]);

            set_flowitem_default();
            checkflowitem();
            pnl_hr_qa.Visible = true;
            pnl_flow_item_1_remark.Visible = false;
            pnl_flow_item_2_uploadfile.Visible = false;
            btnAdddata.Visible = false;
        }
        */
        
    }

    private void setDefaultTrnf()
    {
        // _FormView = getFv(ViewState["mode"].ToString());

        _ddlsystem = (DropDownList)FvTrnfInsert.FindControl("ddlsystem");

        LinkButton tbnAddTransferDevice = (LinkButton)FvTrnfInsert.FindControl("tbnAddTransferDevice");
        LinkButton btnAdddata = (LinkButton)FvTrnfInsert.FindControl("btnAdddata");
        LinkButton btnAddReject = (LinkButton)FvTrnfInsert.FindControl("btnAddReject");
        LinkButton AddCancel = (LinkButton)FvTrnfInsert.FindControl("btnAddCancel");
        LinkButton tbnAddTransferDevice_en = (LinkButton)FvTrnfInsert.FindControl("tbnAddTransferDevice_en");
        LinkButton tbnAddTransferDevice_hr = (LinkButton)FvTrnfInsert.FindControl("tbnAddTransferDevice_hr");
        LinkButton tbnAddTransferDevice_qa = (LinkButton)FvTrnfInsert.FindControl("tbnAddTransferDevice_qa");
        LinkButton tbnAddTransferDevice_other = (LinkButton)FvTrnfInsert.FindControl("tbnAddTransferDevice_other");

        GridView GvTransferDevice = (GridView)FvTrnfInsert.FindControl("GvTransferDevice");

        LinkButton btnAdddata_flowi2 = (LinkButton)FvTrnfInsert.FindControl("btnAdddata_flowi2");
        LinkButton btnAddCancel_flowi2 = (LinkButton)FvTrnfInsert.FindControl("btnAddCancel_flowi2");

        linkBtnTrigger(tbnAddTransferDevice);
        linkBtnTrigger(btnAdddata);
        linkBtnTrigger(btnAddReject);
        linkBtnTrigger(AddCancel);
        linkBtnTrigger(tbnAddTransferDevice_en);
        linkBtnTrigger(tbnAddTransferDevice_hr);
        linkBtnTrigger(tbnAddTransferDevice_qa);
        linkBtnTrigger(tbnAddTransferDevice_other);

        GridViewTrigger(GvTransferDevice);

        linkBtnTrigger(btnAdddata_flowi2);
        linkBtnTrigger(btnAddCancel_flowi2);

    }
    private void setObject_transferIT()
    {
        _ddlsystem = (DropDownList)FvTrnfInsert.FindControl("ddlsystem");
        _pnl_system = (Panel)FvTrnfInsert.FindControl("pnl_system");

        _hfu0_transf_idx = (HiddenField)FvTrnfData.FindControl("hfu0_transf_idx");
        _hfflow_item = (HiddenField)FvTrnfData.FindControl("hfflow_item");
        _hfNodeIdx = (HiddenField)FvTrnfData.FindControl("hfNodeIdx");
        _hfActorIdx = (HiddenField)FvTrnfData.FindControl("hfActorIdx");
        _hfsysidx = (HiddenField)FvTrnfData.FindControl("hfsysidx");
        _hfdoc_status = (HiddenField)FvTrnfData.FindControl("hfdoc_status");

        _ddlAddOrg_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf");
        _ddlAddDep_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf");
        _ddlAddSec_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf");
        _ddlAddemp_idx_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf");
        _ddlSearch_devices_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf");
        _ddlasset_no_trnf = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf");
        _ddllocate_it_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_it_search");

        _txtplace_idx = (TextBox)FvTrnfInsert.FindControl("txtplace_idx");
        _txtplace_trnf = (TextBox)FvTrnfInsert.FindControl("txtplace_trnf");
        _txtremark = (TextBox)FvTrnfInsert.FindControl("txtremark");
        _pnl_search = (Panel)FvTrnfInsert.FindControl("pnl_search");
        _rpt_history = (Repeater)FvTrnfInsert.FindControl("rpt_history");
        _pnl_history = (Panel)FvTrnfInsert.FindControl("pnl_history");
        _pnl_doc = (Panel)FvTrnfInsert.FindControl("pnl_doc");
        _GvReportAdd = (GridView)FvTrnfInsert.FindControl("GvTransferDevice");

        // en //

        _ddlAddOrg_trnf_en = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_en");
        _ddlAddDep_trnf_en = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_en");
        _ddllocate_en = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en");
        _ddlbuilding_en = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en");
        _ddlroom_en = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en");

        _ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
        _ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");
        _ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
        _ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
        _ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

        _ddlAddSec_trnf_en = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_en");
        _ddlcostcenter_en = (DropDownList)FvTrnfInsert.FindControl("ddlcostcenter_en");
        _check_transfer_flag_en = (CheckBox)FvTrnfInsert.FindControl("check_transfer_flag_en");

        //hr//
        _ddlAddOrg_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_hr");
        _ddlAddDep_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_hr");
        _ddlAddemp_idx_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_hr");
        _ddlAddSec_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_hr");
        _ddlSearch_devices_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf_hr");
        _ddlasset_no_trnf_hr = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf_hr");
        _txtplace_idx_hr = (TextBox)FvTrnfInsert.FindControl("txtplace_idx_hr");
        _ddllocate_hr_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_hr_search");
        _ddllocate_hr = (DropDownList)FvTrnfInsert.FindControl("ddllocate_hr");
        _ddlcostcenter_hr = (DropDownList)FvTrnfInsert.FindControl("ddlcostcenter_hr");
        _check_transfer_flag = (CheckBox)FvTrnfInsert.FindControl("check_transfer_flag");
        _ddlcostcenter_hr_search = (DropDownList)FvTrnfInsert.FindControl("ddlcostcenter_hr_search");

        //qa//
        _ddlAddOrg_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_qa");
        _ddlAddDep_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_qa");
        _ddlAddemp_idx_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlAddemp_idx_trnf_qa");
        _ddlAddSec_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlAddSec_trnf_qa");
        _ddlSearch_devices_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlSearch_devices_trnf_qa");
        _ddlasset_no_trnf_qa = (DropDownList)FvTrnfInsert.FindControl("ddlasset_no_trnf_qa");
        _txtplace_idx_qa = (TextBox)FvTrnfInsert.FindControl("txtplace_idx_qa");
        _ddllocate_qa_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_qa_search");
        _ddllocate_qa = (DropDownList)FvTrnfInsert.FindControl("ddllocate_qa");

        //other//
        _ddlAddOrg_trnf_other = (DropDownList)FvTrnfInsert.FindControl("ddlAddOrg_trnf_other");
        _ddlAddDep_trnf_other = (DropDownList)FvTrnfInsert.FindControl("ddlAddDep_trnf_other");
        _ddllocate_other = (DropDownList)FvTrnfInsert.FindControl("ddllocate_other");

        _ddllocate_other_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_other_search");
        _txtasset_no = (TextBox)FvTrnfInsert.FindControl("txtasset_no");
        _txtdesc_name = (TextBox)FvTrnfInsert.FindControl("txtdesc_name");
        _txtacquis_val = (TextBox)FvTrnfInsert.FindControl("txtacquis_val");
        _txtref_itnum = (TextBox)FvTrnfInsert.FindControl("txtref_itnum");
        _txtionum = (TextBox)FvTrnfInsert.FindControl("txtionum");
        _txtionum = (TextBox)FvTrnfInsert.FindControl("txtionum");
        _check_subno_other = (CheckBox)FvTrnfInsert.FindControl("check_subno_other");
        _ddlcostcenter_other_search = (DropDownList)FvTrnfInsert.FindControl("ddlcostcenter_other_search");

    }
    protected Boolean Savetrnf_it(int id, int iArg)
    {
        Boolean _boolean = false;
        setObject_transferIT();
        data_itasset dataitasset = new data_itasset();
        its_TransferDevice_u0 obj_u_document = new its_TransferDevice_u0();
        dataitasset.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_u_document.u0_transf_idx = id;
        obj_u_document.CEmpIDX = emp_idx;
        obj_u_document.UEmpIDX = emp_idx;
        obj_u_document.sysidx = _func_dmu.zStringToInt(_hfsysidx.Value);
        obj_u_document.org_idx = _func_dmu.zStringToInt(ViewState["Org_idx"].ToString());
        obj_u_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_u_document.rpos_idx = _func_dmu.zStringToInt(ViewState["Pos_idx"].ToString());
        obj_u_document.rsec_idx = _func_dmu.zStringToInt(ViewState["Sec_idx"].ToString());
        obj_u_document.emp_idx = emp_idx;
        if (_func_dmu.zStringToInt(_hfsysidx.Value) == 3) // it
        {
            obj_u_document.n_org_idx = _func_dmu.zStringToInt(_ddlAddOrg_trnf.SelectedValue);
            obj_u_document.n_rdept_idx = _func_dmu.zStringToInt(_ddlAddDep_trnf.SelectedValue);
            //obj_u_document.n_rpos_idx = _func_dmu.zStringToInt(_ddlAddSec_trnf.SelectedValue);
            obj_u_document.n_rsec_idx = _func_dmu.zStringToInt(_ddlAddSec_trnf.SelectedValue);
            obj_u_document.n_emp_idx = _func_dmu.zStringToInt(_ddlAddemp_idx_trnf.SelectedValue);
            obj_u_document.n_place_idx = _func_dmu.zStringToInt(_txtplace_idx.Text);
        }
        /*
        else if (_func_dmu.zStringToInt(_hfsysidx.Value) == 11) // en
        {
            obj_u_document.n_org_idx = _func_dmu.zStringToInt(_ddlAddOrg_trnf_en.SelectedValue);
            obj_u_document.n_rdept_idx = _func_dmu.zStringToInt(_ddlAddDep_trnf_en.SelectedValue);
            obj_u_document.n_rsec_idx = 0;
            obj_u_document.n_emp_idx = 0;
            obj_u_document.n_place_idx = _func_dmu.zStringToInt(_ddllocate_en.SelectedValue);
            obj_u_document.n_BuildingIDX = _func_dmu.zStringToInt(_ddlbuilding_en.SelectedValue);
            obj_u_document.n_RoomIDX = _func_dmu.zStringToInt(_ddlroom_en.SelectedValue);

        }*/
        else if (
            (_func_dmu.zStringToInt(_hfsysidx.Value) == 11) ||
            (_func_dmu.zStringToInt(_hfsysidx.Value) == 9) ||
            (_func_dmu.zStringToInt(_hfsysidx.Value) == 36) ||
            (_func_dmu.zStringToInt(_hfsysidx.Value) == 37)
            ) // hr
        {
            obj_u_document.n_org_idx = _func_dmu.zStringToInt(_ddlAddOrg_trnf_hr.SelectedValue);
            obj_u_document.n_rdept_idx = _func_dmu.zStringToInt(_ddlAddDep_trnf_hr.SelectedValue);
            obj_u_document.n_rpos_idx = 0;// _func_dmu.zStringToInt(_ddlAddSec_trnf.SelectedValue);
            obj_u_document.n_rsec_idx = _func_dmu.zStringToInt(_ddlAddSec_trnf_hr.SelectedValue);
            obj_u_document.n_emp_idx = 0;// _func_dmu.zStringToInt(_ddlAddemp_idx_trnf_hr.SelectedValue);
            obj_u_document.n_place_idx = _func_dmu.zStringToInt(_ddllocate_hr.SelectedValue);
            obj_u_document.transfer_flag = _func_dmu.zBooleanToInt(_check_transfer_flag.Checked);
            obj_u_document.n_cost_idx = _func_dmu.zStringToInt(_ddlcostcenter_hr.SelectedValue);

        }
        /*
        else if (_func_dmu.zStringToInt(_hfsysidx.Value) == 36) // qa
        {
            obj_u_document.n_org_idx = _func_dmu.zStringToInt(_ddlAddOrg_trnf_qa.SelectedValue);
            obj_u_document.n_rdept_idx = _func_dmu.zStringToInt(_ddlAddDep_trnf_qa.SelectedValue);
            obj_u_document.n_rpos_idx = 0;// _func_dmu.zStringToInt(_ddlAddSec_trnf.SelectedValue);
            obj_u_document.n_rsec_idx = 0;// _func_dmu.zStringToInt(_ddlAddSec_trnf_hr.SelectedValue);
            obj_u_document.n_emp_idx = 0;// _func_dmu.zStringToInt(_ddlAddemp_idx_trnf_hr.SelectedValue);
            obj_u_document.n_place_idx = _func_dmu.zStringToInt(_ddllocate_qa.SelectedValue);
        }
        
        else if (_func_dmu.zStringToInt(_hfsysidx.Value) == 37) // other
        {
            obj_u_document.n_org_idx = _func_dmu.zStringToInt(_ddlAddOrg_trnf_other.SelectedValue);
            obj_u_document.n_rdept_idx = _func_dmu.zStringToInt(_ddlAddDep_trnf_other.SelectedValue);
            obj_u_document.n_rpos_idx = 0;// _func_dmu.zStringToInt(_ddlAddSec_trnf.SelectedValue);
            obj_u_document.n_rsec_idx = 0;// _func_dmu.zStringToInt(_ddlAddSec_trnf_hr.SelectedValue);
            obj_u_document.n_emp_idx = 0;// _func_dmu.zStringToInt(_ddlAddemp_idx_trnf_hr.SelectedValue);
            obj_u_document.n_place_idx = _func_dmu.zStringToInt(_ddllocate_other.SelectedValue);
        }*/
        string sremark = "";
        if (ViewState["mode"].ToString() == "trnf_approve_list")
        {
            int idoc_status = 0;

            TextBox txtremark;
            DropDownList ddlapprove;
            //if (_func_dmu.zStringToInt(_hfNodeIdx.Value) == 2)
            //{
            txtremark = (TextBox)FvTrnfInsert.FindControl("txtremark_flowi2");
            ddlapprove = (DropDownList)FvTrnfInsert.FindControl("ddlapprove_flowi2");
            sremark = txtremark.Text;
            if (_func_dmu.zStringToInt(_hfsysidx.Value) == 3) //it
            {
                if (_func_dmu.zStringToInt(ddlapprove.SelectedValue) == 2)//ไม่อนุมัติ
                {
                    idoc_status = 8;
                }
                else
                {
                    idoc_status = _func_dmu.zStringToInt(ddlapprove.SelectedValue);
                }
            }
            else if (
                     (_func_dmu.zStringToInt(_hfsysidx.Value) == 11) &&
                     //(_func_dmu.zStringToInt(_hfNodeIdx.Value) == 6) &&
                     //(_func_dmu.zStringToInt(_hfActorIdx.Value) == 4)
                     (_func_dmu.zStringToInt(_hfNodeIdx.Value) == 5) &&
                     (_func_dmu.zStringToInt(_hfActorIdx.Value) == 3)
                     )//en
            {
                if (_func_dmu.zStringToInt(ddlapprove.SelectedValue) == 2)//ไม่อนุมัติ
                {
                    idoc_status = _func_dmu.zStringToInt(ddlapprove.SelectedValue);
                }
                else
                {
                    idoc_status = 12;
                }
            }
            else if (
                     (_func_dmu.zStringToInt(_hfsysidx.Value) == 36) &&
                     //(_func_dmu.zStringToInt(_hfNodeIdx.Value) == 6) &&
                     //(_func_dmu.zStringToInt(_hfActorIdx.Value) == 4)
                     (_func_dmu.zStringToInt(_hfNodeIdx.Value) == 5) &&
                     (_func_dmu.zStringToInt(_hfActorIdx.Value) == 3)
                     )//qa
            {
                if (_func_dmu.zStringToInt(ddlapprove.SelectedValue) == 2)//ไม่อนุมัติ
                {
                    idoc_status = _func_dmu.zStringToInt(ddlapprove.SelectedValue);
                }
                else
                {
                    idoc_status = 13;
                }
            }
            else
            {
                idoc_status = _func_dmu.zStringToInt(ddlapprove.SelectedValue);
            }

            //}
            obj_u_document.remark = sremark;
            obj_u_document.doc_status = idoc_status;
        }
        else
        {

            obj_u_document.remark = _txtremark.Text;
            obj_u_document.doc_status = iArg;
        }

        obj_u_document.node_idx = _func_dmu.zStringToInt(_hfNodeIdx.Value);
        obj_u_document.actor_idx = _func_dmu.zStringToInt(_hfActorIdx.Value);
        dataitasset.its_TransferDevice_u0_action[0] = obj_u_document;

        //******************  start its_u1_document *********************//
        int itemObj = 0;

        DataSet dsU1 = (DataSet)ViewState["vsits_TransferDevice_u1"];
        its_TransferDevice_u1[] objcourse1 = new its_TransferDevice_u1[dsU1.Tables["dsits_TransferDevice_u1"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsits_TransferDevice_u1"].Rows)
        {
            objcourse1[itemObj] = new its_TransferDevice_u1();
            objcourse1[itemObj].u0_transf_idx = _func_dmu.zStringToInt(item["u0_transf_idx"].ToString());
            objcourse1[itemObj].u1_transf_idx = _func_dmu.zStringToInt(item["u1_transf_idx"].ToString());
            objcourse1[itemObj].tdidx = _func_dmu.zStringToInt(item["tdidx"].ToString());
            objcourse1[itemObj].device_idx = _func_dmu.zStringToInt(item["device_idx"].ToString());
            objcourse1[itemObj].asidx = _func_dmu.zStringToInt(item["asidx"].ToString());
            objcourse1[itemObj].asset_no = item["asset_no"].ToString();
            objcourse1[itemObj].qty = _func_dmu.zStringToInt(item["qty"].ToString());
            objcourse1[itemObj].acquis_val = _func_dmu.zStringToDecimal(item["acquis_val"].ToString());
            objcourse1[itemObj].book_val = 0;
            objcourse1[itemObj].price = 0;
            objcourse1[itemObj].ref_itnum = item["ref_itnum"].ToString();
            objcourse1[itemObj].ionum = item["ionum"].ToString();
            objcourse1[itemObj].desc_name = item["desc_name"].ToString();
            objcourse1[itemObj].doc_u2idx = _func_dmu.zStringToInt(item["doc_u2idx"].ToString());
            objcourse1[itemObj].u1_status = 1;
            objcourse1[itemObj].o_org_idx = _func_dmu.zStringToInt(item["o_org_idx"].ToString());
            objcourse1[itemObj].o_rdept_idx = _func_dmu.zStringToInt(item["o_rdept_idx"].ToString());
            objcourse1[itemObj].o_rsec_idx = _func_dmu.zStringToInt(item["o_rsec_idx"].ToString());
            objcourse1[itemObj].o_emp_idx = _func_dmu.zStringToInt(item["o_emp_idx"].ToString());
            objcourse1[itemObj].o_rpos_idx = _func_dmu.zStringToInt(item["o_rpos_idx"].ToString());

            objcourse1[itemObj].o_LocIDX = _func_dmu.zStringToInt(item["o_LocIDX"].ToString());
            objcourse1[itemObj].o_BuildingIDX = _func_dmu.zStringToInt(item["o_BuildingIDX"].ToString());
            objcourse1[itemObj].o_RoomIDX = _func_dmu.zStringToInt(item["o_RoomIDX"].ToString());

            objcourse1[itemObj].subno_flag = _func_dmu.zStringToInt(item["subno_flag"].ToString());

            int i = 0;
            foreach (GridViewRow gridrow in _GvReportAdd.Rows)
            {
                Label lb_asset_no = (Label)gridrow.FindControl("lb_asset_no");
                TextBox txt_book_val = (TextBox)gridrow.FindControl("txt_book_val");
                TextBox txt_price = (TextBox)gridrow.FindControl("txt_price");
                if (item["asset_no"].ToString().ToLower() == lb_asset_no.Text.Trim().ToLower())
                {
                    objcourse1[itemObj].book_val = _func_dmu.zStringToDecimal(txt_book_val.Text);
                    objcourse1[itemObj].price = _func_dmu.zStringToDecimal(txt_price.Text);
                }

                i++;
            }

            itemObj++;

        }
        dataitasset.its_TransferDevice_u1_action = objcourse1;
         // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(dataitasset));
        dataitasset = callServicePostITAsset(_urlSetInsits_u_transfer, dataitasset);
        string _returndocumentcode = "";
        if (dataitasset.ReturnCode == "0")
        {
            if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (iArg == 0))
            {
                _returndocumentcode = dataitasset.ReturnDocCode;
                if (_returndocumentcode != "")
                {
                    if (id == 0)
                    {
                        try
                        {
                            string Pathfile = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
                            Pathfile = Pathfile + _returndocumentcode;
                            int ic = 0;
                            string[] filesLoc = Directory.GetFiles(Server.MapPath(Pathfile + "/"));
                            List<System.Web.UI.WebControls.ListItem> files = new List<System.Web.UI.WebControls.ListItem>();
                            foreach (string file in filesLoc)
                            {
                                try
                                {
                                    File.Delete(file);
                                    ic++;
                                }
                                catch { }
                            }
                        }
                        catch { }
                    }

                    FileUpload uploadfile_trnsf = (FileUpload)FvTrnfInsert.FindControl("uploadfile_trnsf");
                    if (uploadfile_trnsf.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
                        string document_code = _returndocumentcode;
                        string fileName_upload = "[เอกสารโอนย้าย]" + document_code + "-";
                        string filePath_upload = Server.MapPath(getPathfile + document_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }

                        int ic = 1;
                        if (id > 0)
                        {
                            ic = getItemFile(1, document_code);
                            ic++;
                        }
                        foreach (HttpPostedFile uploadedFile in uploadfile_trnsf.PostedFiles)
                        {

                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string filePath12 = ic.ToString();
                                    string _filepathExtension = Path.GetExtension(uploadedFile.FileName);

                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    string fileName = fileName_upload + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + fileName);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }


                    }

                }

            }


            if (dataitasset.its_TransferDevice_u0_action != null)
            {
                foreach (var item in dataitasset.its_TransferDevice_u0_action)
                {
                    if (item.sysidx == 3)
                    {
                        if (item.doc_status == 3)
                        {
                            //Update_Holder(dataitasset);
                            //   litDebug.Text = "1";
                            Update_ApproveChangeHolder(dataitasset, 1, sremark);
                        }
                        else if (item.doc_status == 8)
                        {
                            //  litDebug.Text = "2";
                            Update_ApproveChangeHolder(dataitasset, 2, sremark);
                        }
                    }

                }
            }
            _boolean = true;
        }
        else
        {
            _boolean = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + dataitasset.ReturnCode + ": " + dataitasset.ReturnMsg + "')", true);
        }

        return _boolean;

    }

    protected void ShowDataIndexTrnf(string status)
    {
        // litDebug.Text = "sysidx_admin : " + ViewState["sysidx_admin"].ToString();
        if(status == "preview")
        {
            content_Homepage.Visible = true;
        }
        else
        {
            content_Homepage.Visible = false;
        }
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_document = new its_TransferDevice_u0();
        //GvTrnfIndex.Columns[0].Visible = false;
        select_document.emp_idx = emp_idx;
        select_document.zstatus = status;
        select_document.operation_status_id = "list_index";

        if (status == "preview")
        {
            //ค้นหาตามวันที่
            select_document.condition_date_type = _func_dmu.zStringToInt(ddlcondition.SelectedValue);
            if (txtstartdate.Text.Trim() != "")
            {
                select_document.start_date = _func_dmu.zDateToDB(txtstartdate.Text.Trim());
            }
            if (txtenddate.Text.Trim() != "")
            {
                select_document.end_date = _func_dmu.zDateToDB(txtenddate.Text.Trim());
            }
            select_document.doccode = txtdocumentcode.Text.Trim();
            select_document.emp_code = txtempcode.Text.Trim();
            select_document.emp_name_th = txtfirstname_lastname.Text.Trim();
            select_document.org_idx_search = _func_dmu.zStringToInt(ddlsearch_organization.SelectedValue);
            select_document.rdept_idx_search = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
            select_document.locidx_search = _func_dmu.zStringToInt(ddllocation_search.SelectedValue);
            select_document.sysidx_search = _func_dmu.zStringToInt(ddlsearch_typequipment.SelectedValue);
            select_document.rdept_idx_admin = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());


        }

        select_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_document.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_document.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        select_document.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        select_document.joblevel = _func_dmu.zStringToInt(ViewState["jobgrade_level"].ToString());
        select_document.LocIDX = _func_dmu.zStringToInt(ViewState["locidx"].ToString());

        _dtitseet.its_TransferDevice_u0_action[0] = select_document;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        ViewState["v_GvTrnfIndex"] = _dtitseet.its_TransferDevice_u0_action;
        _func_dmu.zSetGridData(GvTrnfIndex, ViewState["v_GvTrnfIndex"]);
        setdefaultNotification();
        SETFOCUS.Focus();

    }

    private void ShowDataUpdate(int id)
    {
        data_itasset _dtitseet = new data_itasset();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.u0_transf_idx = id;
        select_its.operation_status_id = "sel_data_update";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        //ViewState["v_TransferDevice"] = _dtitseet;
        FvTrnfInsert.DataSource = _dtitseet.its_TransferDevice_u0_action;
        FvTrnfInsert.DataBind();
        FvTrnfData.DataSource = _dtitseet.its_TransferDevice_u0_action;
        FvTrnfData.DataBind();
        setObject_transferIT();
        _pnl_doc.Visible = true;
        _pnl_search.Visible = false;
        HiddenField hfemp_idx = (HiddenField)FvTrnfData.FindControl("hfemp_idx");

        // _txtremark.Enabled = false;
        _GvReportAdd.Columns[10].Visible = false;
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in _dtitseet.its_TransferDevice_u0_action)
            {
                setUserMain(item.emp_idx);
                select_system(_ddlsystem);
                _ddlsystem.SelectedValue = item.sysidx.ToString();
                _pnl_system.Visible = false;
                if (item.sysidx == 3)
                {
                    ddl_organization(_ddlAddOrg_trnf, 0);
                    _ddlAddOrg_trnf.SelectedValue = item.n_org_idx.ToString();
                    _ddlAddOrg_trnf.Enabled = false;
                    ddl_depertment(_ddlAddDep_trnf, 1, 0);
                    _ddlAddDep_trnf.SelectedValue = item.n_rdept_idx.ToString();
                    _ddlAddDep_trnf.Enabled = false;
                    ddl_Sec(_ddlAddSec_trnf, item.n_org_idx, item.n_rdept_idx);
                    _ddlAddSec_trnf.SelectedValue = item.n_rsec_idx.ToString();
                    _ddlAddSec_trnf.Enabled = false;
                    getEmpList(_ddlAddemp_idx_trnf, int.Parse(_ddlAddOrg_trnf.SelectedValue), int.Parse(_ddlAddDep_trnf.SelectedValue), int.Parse(_ddlAddSec_trnf.SelectedValue));
                    _ddlAddemp_idx_trnf.SelectedValue = item.n_emp_idx.ToString();
                    _ddlAddemp_idx_trnf.Enabled = false;
                    if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (_hfdoc_status.Value == "9"))
                    {
                        setTrnf_InsertDefault(1);
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            _GvReportAdd.Columns[10].Visible = true;
                        }
                    }

                }
                /*
                else if (item.sysidx == 11)
                {
                    ddl_organization(_ddlAddOrg_trnf_en, 0);
                    _ddlAddOrg_trnf_en.SelectedValue = item.n_org_idx.ToString();
                    _ddlAddOrg_trnf_en.Enabled = false;
                    ddl_depertment_en(_ddlAddDep_trnf_en, 1, 0, 0);
                    _ddlAddDep_trnf_en.SelectedValue = item.n_rdept_idx.ToString();
                    _ddlAddDep_trnf_en.Enabled = false;
                    Select_Location_all(_ddllocate_en);
                    _ddllocate_en.SelectedValue = item.n_place_idx.ToString();
                    _ddllocate_en.Enabled = false;
                    Select_Building(_ddlbuilding_en, int.Parse(_ddllocate_en.SelectedValue));
                    _ddlbuilding_en.SelectedValue = item.n_BuildingIDX.ToString();
                    _ddlbuilding_en.Enabled = false;
                    Select_Room(_ddlroom_en, int.Parse(_ddlbuilding_en.SelectedValue));
                    _ddlroom_en.SelectedValue = item.n_RoomIDX.ToString();
                    _ddlroom_en.Enabled = false;
                    if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (_hfdoc_status.Value == "9"))
                    {
                        setTrnf_InsertDefault_en(1, item.place_idx);
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            _GvReportAdd.Columns[10].Visible = true;
                        }
                    }

                }
                */
                else if ((item.sysidx == 9) || (item.sysidx == 36) || (item.sysidx == 37) || (item.sysidx == 11))
                {
                    ddl_organization(_ddlAddOrg_trnf_hr, 0);
                    _ddlAddOrg_trnf_hr.SelectedValue = item.n_org_idx.ToString();
                    _ddlAddOrg_trnf_hr.Enabled = false;
                    ddl_depertment(_ddlAddDep_trnf_hr, 1, 0);
                    _ddlAddDep_trnf_hr.SelectedValue = item.n_rdept_idx.ToString();
                    _ddlAddDep_trnf_hr.Enabled = false;
                    
                    ddl_Sec(_ddlAddSec_trnf_hr, item.n_org_idx, item.n_rdept_idx);
                    _ddlAddSec_trnf_hr.SelectedValue = item.n_rsec_idx.ToString();
                    _ddlAddSec_trnf_hr.Enabled = false;
                    select_costcenter(_ddlcostcenter_hr);
                    _ddlcostcenter_hr.SelectedValue = item.n_cost_idx.ToString();
                    _ddlcostcenter_hr.Enabled = false;
                    _check_transfer_flag.Checked = _func_dmu.zIntToBoolean(item.transfer_flag);
                    _check_transfer_flag.Enabled = false;


                    /*
                    getEmpList(_ddlAddemp_idx_trnf_hr, int.Parse(_ddlAddOrg_trnf_hr.SelectedValue), int.Parse(_ddlAddDep_trnf_hr.SelectedValue), int.Parse(_ddlAddSec_trnf_hr.SelectedValue));
                    _ddlAddemp_idx_trnf_hr.SelectedValue = item.n_emp_idx.ToString();
                    _ddlAddemp_idx_trnf_hr.Enabled = false;
                    */
                    setLocation_hr(_ddllocate_hr);
                    _ddllocate_hr.SelectedValue = item.n_place_idx.ToString();
                    _ddllocate_hr.Enabled = false;
                    if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (_hfdoc_status.Value == "9"))
                    {
                        setTrnf_InsertDefault_hr(1, item.sysidx);
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            _GvReportAdd.Columns[10].Visible = true;
                        }
                    }
                }
                /*
                else if (item.sysidx == 36)
                {
                    ddl_organization(_ddlAddOrg_trnf_qa, 0);
                    _ddlAddOrg_trnf_qa.SelectedValue = item.n_org_idx.ToString();
                    _ddlAddOrg_trnf_qa.Enabled = false;
                    ddl_depertment(_ddlAddDep_trnf_qa, 1, 0);
                    _ddlAddDep_trnf_qa.SelectedValue = item.n_rdept_idx.ToString();
                    _ddlAddDep_trnf_qa.Enabled = false;
                    setLocation_hr(_ddllocate_qa);
                    _ddllocate_qa.SelectedValue = item.n_place_idx.ToString();
                    _ddllocate_qa.Enabled = false;
                    if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (_hfdoc_status.Value == "9"))
                    {
                        setTrnf_InsertDefault_qa(1);
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            _GvReportAdd.Columns[10].Visible = true;
                        }
                    }
                }
                */
                /*
                else if (item.sysidx == 37) //other
                {
                    ddl_organization(_ddlAddOrg_trnf_other, 0);
                    _ddlAddOrg_trnf_other.SelectedValue = item.n_org_idx.ToString();
                    _ddlAddOrg_trnf_other.Enabled = false;
                    ddl_depertment(_ddlAddDep_trnf_other, 1, 0);
                    _ddlAddDep_trnf_other.SelectedValue = item.n_rdept_idx.ToString();
                    _ddlAddDep_trnf_other.Enabled = false;
                    setLocation_hr(_ddllocate_other);
                    _ddllocate_other.SelectedValue = item.n_place_idx.ToString();
                    _ddllocate_other.Enabled = false;
                    if (((_hfNodeIdx.Value == "1") || (_hfNodeIdx.Value == "13")) && (_hfdoc_status.Value == "9"))
                    {
                        setTrnf_InsertDefault_other(1);
                        if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                        {
                            _GvReportAdd.Columns[10].Visible = true;
                        }
                    }
                }
                */
            }
        }

        CreateDsits_TransferDevice_u1();
        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        if ((_dtitseet.its_TransferDevice_u0_action != null) && (_dtitseet.its_TransferDevice_u1_action != null))
        {
            int ic = 0;
           // litDebug.Text = _dtitseet.its_TransferDevice_u1_action.Count().ToString();
            foreach (var vdr in _dtitseet.its_TransferDevice_u1_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

                drContacts["id"] = ic;
                drContacts["u0_transf_idx"] = vdr.u0_transf_idx;
                drContacts["u1_transf_idx"] = vdr.u1_transf_idx;
                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.name_m0_typedevice;
                drContacts["u1_status"] = 1;
                drContacts["device_idx"] = vdr.device_idx;
                drContacts["asidx"] = vdr.asidx;
                drContacts["asset_no"] = vdr.asset_no;
                drContacts["qty"] = 1;
                drContacts["acquis_val"] = vdr.acquis_val;
                drContacts["book_val"] = vdr.book_val;
                drContacts["price"] = vdr.price;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["ionum"] = vdr.ionum;
                drContacts["desc_name"] = vdr.desc_name;
                drContacts["doc_u2idx"] = vdr.doc_u2idx;
                drContacts["o_org_idx"] = vdr.o_org_idx;
                drContacts["o_rdept_idx"] = vdr.o_rdept_idx;
                drContacts["o_rsec_idx"] = vdr.o_rsec_idx;
                drContacts["o_emp_idx"] = vdr.o_emp_idx;
                drContacts["o_rpos_idx"] = vdr.o_rpos_idx;

                drContacts["o_LocIDX"] = vdr.o_LocIDX;
                drContacts["o_BuildingIDX"] = vdr.o_BuildingIDX;
                drContacts["o_RoomIDX"] = vdr.o_RoomIDX;
                drContacts["subno_flag"] = vdr.subno_flag;

                dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

                if (_hfsysidx.Value == "3")
                {
                    // litDebug.Text = vdr.device_idx.ToString();
                    SelectDetailHolder(vdr.device_idx);
                    SelectViewSoftware(vdr.device_idx);
                }

            }
        }
        ViewState["vsits_TransferDevice_u1"] = dsContacts;
        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);
        _pnl_history.Visible = false;
        if (id > 0)
        {
            _pnl_history.Visible = true;
            _rpt_history.DataSource = _dtitseet.its_TransferDevice_status_l0_action;
            _rpt_history.DataBind();


        }

        try
        {

            string getPathLotus = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
            GridView gvFile_transfer = (GridView)FvTrnfInsert.FindControl("gvFile_transfer");
            Label lbdocument_code = (Label)FvTrnfInsert.FindControl("lbdocument_code");

            string filePathLotus = Server.MapPath(getPathLotus + lbdocument_code.Text);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
            SearchDirectories_foreditFile(myDirLotus, lbdocument_code.Text, gvFile_transfer);

        }
        catch
        {

        }

    }

    private void set_flowitem_default()
    {
        HiddenField hfu0_transf_idx = (HiddenField)FvTrnfData.FindControl("hfu0_transf_idx");
        HiddenField hfflow_item = (HiddenField)FvTrnfData.FindControl("hfflow_item");
        HiddenField hfNodeIdx = (HiddenField)FvTrnfData.FindControl("hfNodeIdx");
        HiddenField hfActorIdx = (HiddenField)FvTrnfData.FindControl("hfActorIdx");
        HiddenField hfsysidx = (HiddenField)FvTrnfData.FindControl("hfsysidx");
        HiddenField hfdoc_status = (HiddenField)FvTrnfData.FindControl("hfdoc_status");
        if (_func_dmu.zStringToInt(hfu0_transf_idx.Value) == 0)
        {
            hfflow_item.Value = "1";
            hfNodeIdx.Value = "1";
            hfActorIdx.Value = "1";
            hfdoc_status.Value = "1";
            hfsysidx.Value = ViewState["sysidx"].ToString();
        }
    }

    private void checkflowitem()
    {
        HiddenField hfflow_item = (HiddenField)FvTrnfData.FindControl("hfflow_item");
        HiddenField hfu0_transf_idx = (HiddenField)FvTrnfData.FindControl("hfu0_transf_idx");
        HiddenField hfNodeIdx = (HiddenField)FvTrnfData.FindControl("hfNodeIdx");
        HiddenField hfActorIdx = (HiddenField)FvTrnfData.FindControl("hfActorIdx");
        HiddenField hfsysidx = (HiddenField)FvTrnfData.FindControl("hfsysidx");
        HiddenField hfemp_idx = (HiddenField)FvTrnfData.FindControl("hfemp_idx");
        //**********//
        Panel pnl_flow_item_1 = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_1");
        Panel pnl_flow_item_1_remark = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_1_remark");
        LinkButton btnAdddata = (LinkButton)FvTrnfInsert.FindControl("btnAdddata");
        LinkButton btnAddReject = (LinkButton)FvTrnfInsert.FindControl("btnAddReject");
        Panel pnl_flow_item_2 = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_2");
        Panel pnl_flow_item_2_h = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_2_h");
        LinkButton btnAdddata_flowi2 = (LinkButton)FvTrnfInsert.FindControl("btnAdddata_flowi2");
        LinkButton btnAddCancel_flowi2 = (LinkButton)FvTrnfInsert.FindControl("btnAddCancel_flowi2");
        Panel pnl_flow_item_2_uploadfile = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_2_uploadfile");
        Panel pnl_flow_item_2_listfile = (Panel)FvTrnfInsert.FindControl("pnl_flow_item_2_listfile");

        Panel pnl_it = (Panel)FvTrnfInsert.FindControl("pnl_it");
        Panel pnl_en = (Panel)FvTrnfInsert.FindControl("pnl_en");
        Panel pnl_hr_qa = (Panel)FvTrnfInsert.FindControl("pnl_hr_qa");
       // Panel pnl_qa = (Panel)FvTrnfInsert.FindControl("pnl_qa");
        //Panel pnl_other = (Panel)FvTrnfInsert.FindControl("pnl_other");
        Panel pnl_search = (Panel)FvTrnfInsert.FindControl("pnl_search");
        Panel pnl_en_search = (Panel)FvTrnfInsert.FindControl("pnl_en_search");
        Panel pnl_hr_search = (Panel)FvTrnfInsert.FindControl("pnl_hr_search");
        Panel pnl_qa_search = (Panel)FvTrnfInsert.FindControl("pnl_qa_search");
        Panel pnl_other_search = (Panel)FvTrnfInsert.FindControl("pnl_other_search");

        DropDownList ddlapprove_flowi2 = (DropDownList)FvTrnfInsert.FindControl("ddlapprove_flowi2");

        pnl_flow_item_1.Visible = false;
        pnl_flow_item_2.Visible = false;
        pnl_flow_item_2_h.Visible = false;
        btnAdddata_flowi2.Visible = false;

        pnl_search.Visible = false;
        pnl_en_search.Visible = false;
        pnl_hr_search.Visible = false;
        pnl_qa_search.Visible = false;
        pnl_other_search.Visible = false;

        btnAdddata.Visible = false;
        btnAddReject.Visible = false;
        pnl_flow_item_1_remark.Visible = false;
        pnl_flow_item_2_uploadfile.Visible = false;
        pnl_flow_item_2_listfile.Visible = true;

        pnl_it.Visible = false;
        pnl_en.Visible = false;
        pnl_hr_qa.Visible = false;
       // pnl_qa.Visible = false;
        //pnl_other.Visible = false;
        
        if (hfsysidx.Value == "3") //it
        {
            pnl_it.Visible = true;
        }
        else
        {
            pnl_hr_qa.Visible = true;
        }

        //litDebug.Text = ViewState["mode"].ToString();
        //  if (ViewState["status"].ToString() == "trnf_approve_list")
        if (ViewState["mode"].ToString() == "trnf_approve_list")
        {
            CreateDsits_ListApprov();
            int id_count = 0;
            if (hfNodeIdx.Value == "8") //พิจารณาผล(Asset)
            {
                id_count = 1;
            }
            ShowListApprov(id_count);
            ddlListApprov(ddlapprove_flowi2);

            switch (hfNodeIdx.Value)
            {
                case "1":
                    //สร้าง
                    pnl_flow_item_2.Visible = true;
                    btnAdddata_flowi2.Visible = false;
                    pnl_flow_item_1_remark.Visible = true;
                    //  pnl_flow_item_2_listfile.Visible = true;
                    break;
                case "13":
                    //กลับไปแก้ไข
                    pnl_flow_item_2.Visible = true;
                    btnAdddata_flowi2.Visible = false;
                    // pnl_flow_item_2_listfile.Visible = true;
                    break;
                default:
                    pnl_flow_item_2.Visible = true;
                    // pnl_flow_item_2_listfile.Visible = true;
                    if (_func_dmu.zStringToInt(hfNodeIdx.Value) != 9)
                    {
                        // litDebug.Text = ViewState["jobgrade_level"].ToString();

                        data_itasset dtitseet = new data_itasset();
                        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
                        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
                        select_its.emp_idx = emp_idx;
                        select_its.operation_status_id = "permission_jobgrade";
                        dtitseet.its_TransferDevice_u0_action[0] = select_its;
                        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
                        // check asset manager / director
                        int i_O = 0;
                        int i_M = 0;
                        int i_D = 0;
                        if (dtitseet.its_TransferDevice_u0_action != null)
                        {

                            foreach (var item in dtitseet.its_TransferDevice_u0_action)
                            {
                                if (item.zstatus == "O")
                                {
                                    if (item.joblevel > 0)
                                    {
                                        i_O = item.joblevel;
                                    }
                                }
                                else if (item.zstatus == "M")
                                {

                                    if (item.joblevel > 0)
                                    {
                                        i_M = item.joblevel;

                                    }
                                }
                                else if (item.zstatus == "D")
                                {
                                    if (item.joblevel >= _jobDirector)
                                    {
                                        i_D = item.joblevel;
                                    }
                                }
                            }
                        }

                        pnl_flow_item_2_h.Visible = true;
                        btnAdddata_flowi2.Visible = true;
                        int ic = 0;
                        if (
                            //พิจารณาผล(ผู้สร้าง) Manager
                            ((hfNodeIdx.Value == "2") && (hfActorIdx.Value == "3"))
                            ||
                            //พิจารณาผล(ผู้รับ) Manager
                            ((hfNodeIdx.Value == "5") && (hfActorIdx.Value == "3"))
                            )
                        {
                            //litDebug.Text = emp_idx.ToString();
                            if (
                                (i_M == _jobManager)
                                ||
                                (_func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()) == it_admin_rsec)
                                )
                            {
                                ic++;
                            }

                            if (ic > 0)
                            {
                                pnl_flow_item_2_h.Visible = true;
                                btnAdddata_flowi2.Visible = true;
                            }
                            else
                            {
                                pnl_flow_item_2_h.Visible = false;
                                btnAdddata_flowi2.Visible = false;
                            }
                        }
                        else if (
                            //พิจารณาผล(ผู้สร้าง) Director
                            ((hfNodeIdx.Value == "3") && (hfActorIdx.Value == "4"))
                            ||
                            //พิจารณาผล(ผู้รับ) Director
                            ((hfNodeIdx.Value == "6") && (hfActorIdx.Value == "4"))
                            )
                        {
                            if (
                                (i_D >= _jobDirector)
                                 /*||
                               (
                                (i_D == 10)
                                &&
                                 (_func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()) == 20)
                                )*/
                                ||
                                (_func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()) == it_admin_rsec)
                                )
                            {
                                ic++;
                            }
                            if (ic > 0)
                            {
                                pnl_flow_item_2_h.Visible = true;
                                btnAdddata_flowi2.Visible = true;
                            }
                            else
                            {
                                pnl_flow_item_2_h.Visible = false;
                                btnAdddata_flowi2.Visible = false;
                            }
                        }

                        //admin
                        if (_func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()) == it_admin_rsec)
                        {
                            pnl_flow_item_2_h.Visible = true;
                            btnAdddata_flowi2.Visible = true;
                        }

                    }
                    break;

            }
        }
        else
        {
            switch (hfNodeIdx.Value)
            {
                case "1":
                    pnl_flow_item_1.Visible = true;
                    pnl_flow_item_1_remark.Visible = true;
                    pnl_flow_item_2_uploadfile.Visible = true;
                    
                    if (hfsysidx.Value == "3")
                    {
                        pnl_search.Visible = true;
                    }
                    else if (hfsysidx.Value == "37")
                    {
                        pnl_other_search.Visible = true;
                    }
                    else
                    {
                        pnl_hr_search.Visible = true;
                    }
                    if (_func_dmu.zStringToInt(hfu0_transf_idx.Value) == 0)
                    {
                        btnAdddata.Visible = true;
                        btnAddReject.Visible = false;

                    }
                    else
                    {
                        if (_func_dmu.zStringToInt(hfNodeIdx.Value) == 1)
                        {
                            btnAdddata.Visible = true;
                            btnAddReject.Visible = true;
                        }

                    }
                    break;
                case "13":
                    pnl_flow_item_1.Visible = true;
                    if (_func_dmu.zStringToInt(hfemp_idx.Value) == emp_idx)
                    {
                        pnl_flow_item_1.Visible = true;
                        btnAdddata.Visible = true;
                        btnAddReject.Visible = true;
                        pnl_flow_item_1_remark.Visible = true;
                        pnl_flow_item_2_uploadfile.Visible = true;

                        if (hfsysidx.Value == "3")
                        {
                            pnl_search.Visible = true;
                        }
                        else if (hfsysidx.Value == "37")
                        {
                            pnl_other_search.Visible = true;
                        }
                        else
                        {
                            pnl_hr_search.Visible = true;
                        }
                    }

                    break;
                default:
                    pnl_flow_item_1.Visible = true;
                    pnl_flow_item_1_remark.Visible = false;
                    btnAdddata.Visible = false;
                    btnAddReject.Visible = false;
                    // pnl_flow_item_2_listfile.Visible = true;

                    break;

            }
        }

    }

    private void ShowListTranfer()
    {

        setActiveTab("transfer");
        ViewState["mode"] = "trnf_create_list";
        MvMaster.SetActiveView(View_AssetTransfer_IndexList);
        pnl_trnfcreate.Visible = false;
        Panel_approve_trnf.Visible = false;
        GvTrnfIndex.Columns[0].Visible = false;
        ShowDataIndexTrnf("preview");
    }

    private void ShowListTranfer_approve()
    {


        ViewState["mode"] = "trnf_approve_list";
        MvMaster.SetActiveView(View_AssetTransfer_IndexList);
        pnl_trnfcreate.Visible = false;
        Panel_approve_trnf.Visible = true;
        GvTrnfIndex.Columns[0].Visible = true;
        check_approve_all_trnf.Checked = false;
        txtremark_approve_all_trnf.Text = "";
        ShowDataIndexTrnf(ViewState["status"].ToString());
        if (GvTrnfIndex.Rows.Count == 0)
        {
            Panel_approve_trnf.Visible = false;
        }
    }


    private void confirm_approve_trnf(int id)
    {
        its_TransferDevice_u0[] _u0tempList = (its_TransferDevice_u0[])ViewState["v_GvTrnfIndex"];
        if (_u0tempList.Count() > 0)
        {
            foreach (var item in _u0tempList)
            {
                if (item.selected == true)
                {
                    int idoc_status = 0;
                    data_itasset dataitasset = new data_itasset();
                    its_TransferDevice_u0 obj_u_document = new its_TransferDevice_u0();
                    dataitasset.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
                    obj_u_document.u0_transf_idx = item.u0_transf_idx;
                    obj_u_document.emp_idx = emp_idx;
                   // obj_u_document.doc_status = id;

                    if (item.sysidx == 3) //it
                    {
                        if (id == 2)//ไม่อนุมัติ
                        {
                            idoc_status = 8;
                        }
                        else
                        {
                            idoc_status = id;
                        }
                    }
                    else if (
                             (item.sysidx == 11) &&
                             //(item.node_idx == 6) &&
                             //(item.actor_idx == 4)
                             (item.node_idx == 5) &&
                             (item.actor_idx == 3)
                             )//en
                    {
                        if (id == 2)//ไม่อนุมัติ
                        {
                            idoc_status = id;
                        }
                        else
                        {
                            idoc_status = 12;
                        }
                    }
                    else if (
                             (item.sysidx == 36) &&
                             //(item.node_idx == 6) &&
                             //(item.actor_idx == 4)
                             (item.node_idx == 5) &&
                             (item.actor_idx == 3)
                             )//qa
                    {
                        if (id == 2)//ไม่อนุมัติ
                        {
                            idoc_status = id;
                        }
                        else
                        {
                            idoc_status = 13;
                        }
                    }
                    else
                    {
                        idoc_status = id;
                    }
                    
                    obj_u_document.doc_status = idoc_status;
                    obj_u_document.u0_idx = item.u0_idx;
                    obj_u_document.node_idx = item.node_idx;
                    obj_u_document.actor_idx = item.actor_idx;
                    
                    obj_u_document.remark = txtremark_approve_all_trnf.Text;
                    obj_u_document.CEmpIDX = emp_idx;
                    obj_u_document.UEmpIDX = emp_idx;
                    dataitasset.its_TransferDevice_u0_action[0] = obj_u_document;
                    callServicePostITAsset(_urlSetInsits_u_transfer, dataitasset);
                }

            }

        }

    }
    private void selapprove_all_trnf(Boolean _boolean)
    {
        its_TransferDevice_u0[] _u0tempList = (its_TransferDevice_u0[])ViewState["v_GvTrnfIndex"];
        if (_u0tempList.Count() > 0)
        {
            foreach (var item in _u0tempList)
            {
                item.selected = _boolean;

            }
            ViewState["v_GvTrnfIndex"] = _u0tempList;
        }

    }
    //   
    private Boolean check_error_trnsf_it()
    {
        //Boolean _boolean = false;
        setObject_transferIT();
        int iError = 0;
        if (_GvReportAdd.Rows.Count <= 0)
        {
            iError++;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกอุปกรณ์ที่ต้องการโอนย้าย !!!!')", true);
            return false;

        }

        //ckeck asset no
        DataSet dsContacts1 = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr1 in dsContacts1.Tables["dsits_TransferDevice_u1"].Rows)
        {
            its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
            _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
            obj_m.operation_status_id = "check_dupicat_trnf";
            obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
            obj_m.asset_no = dr1["asset_no"].ToString().Trim();
            _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
            _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
            if (_dtitseet.its_TransferDevice_u0_action != null)
            {
                var item = _dtitseet.its_TransferDevice_u0_action[0];
                iError++;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + dr1["asset_no"].ToString().Trim() + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
                return false;

            }
            if (iError > 0)
            {
                break;
            }

        }

        if (iError == 0)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    protected void Update_Holder(data_itasset dataitasset) // กรณีผู้รับถือครอง อนุมัติหรือไม่อนุมัติ
    {

        if (dataitasset.its_TransferDevice_u0_action != null)
        {
            foreach (var v_u0 in dataitasset.its_TransferDevice_u0_action)
            {
                if (dataitasset.its_TransferDevice_u1_action != null)
                {
                    foreach (var v_u1 in dataitasset.its_TransferDevice_u1_action)
                    {
                        try
                        {
                            data_device _data_deviceregister = new data_device();

                            _data_deviceregister.u0_device_list = new u0_device_detail[1];
                            u0_device_detail _data_device_detail = new u0_device_detail();

                            ////dv_u0_device
                            _data_device_detail.u0_didx = v_u1.device_idx; //รหัสอุปกรณ์
                            _data_device_detail.m0_orgidx = v_u0.n_org_idx;
                            _data_device_detail.R0_depidx = v_u0.n_rdept_idx;
                            _data_device_detail.R0_secidx = v_u0.rsec_idx;
                            _data_device_detail.doc_status = "1"; //ใช้งานปกติ
                            _data_device_detail.HDEmpIDX = v_u0.n_emp_idx; //รหัสพนักงาน ผู้ถือครอง ex. 173

                            ////dv_u1_device
                            _data_device_detail.u0_unidx = 2;
                            _data_device_detail.u0_acidx = 1;
                            _data_device_detail.u0_node_dec = 2;
                            _data_device_detail.u0_doc_decision = 1; //ผลการอนุมัติ,ไม่อนุมัติ
                            _data_device_detail.Approve_stauts = 1; //สถานะเอกสาร

                            _data_deviceregister.u0_device_list[0] = _data_device_detail;
                            ////text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));

                            _data_deviceregister = callServicePostDevice(_url_u0_device_update_17, _data_deviceregister);

                            data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                            _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[1]; //chcksoft.Items.Count


                            u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                            u0_softwarelicense_detail_dataset.u0_didx = v_u1.device_idx;
                            u0_softwarelicense_detail_dataset.cemp_idx = emp_idx;
                            u0_softwarelicense_detail_dataset.doc_decision = 5;

                            _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[0] = u0_softwarelicense_detail_dataset;


                            //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                            //text.Text = localJson1;

                            // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                            _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(urlSetApproveLicense, _data_softwarelicense_devicesinsert_);

                        }
                        catch
                        {
                            // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }
                    }
                }
            }
        }



    }

    protected data_device callServicePostDevice(string _cmdUrl, data_device _data_device)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fss.Text = _cmdUrl + _localJson;

        ////// convert json to object
        _data_device = (data_device)_funcTool.convertJsonToObject(typeof(data_device), _localJson);

        return _data_device;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }


    private void setTrnf_InsertDefault_en(int status, int plant_idx)
    {

        setObject_transferIT();
        DropDownList ddllocate_en_search = (DropDownList)FvTrnfInsert.FindControl("ddllocate_en_search");
        DropDownList ddltypemachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypemachine_en_search");
        // DropDownList ddltypecode_en_search = (DropDownList)FvTrnfInsert.FindControl("ddltypecode_en_search");
        // DropDownList ddlgroupmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlgroupmachine_en_search");
        DropDownList ddlbuilding_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlbuilding_en_search");
        DropDownList ddlroom_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlroom_en_search");
        DropDownList ddlmachine_en_search = (DropDownList)FvTrnfInsert.FindControl("ddlmachine_en_search");

        Select_Location_all(ddllocate_en_search);
        ddllocate_en_search.SelectedValue = plant_idx.ToString();
        ddllocate_en_search.Enabled = false;
        select_typemachine(ddltypemachine_en_search);
        //select_typecodemachine(ddltypecode_en_search, int.Parse(ddltypemachine_en_search.SelectedValue));
        //select_groupmachine(ddlgroupmachine_en_search, int.Parse(ddltypecode_en_search.SelectedValue));
        Select_Building(ddlbuilding_en_search, int.Parse(ddllocate_en_search.SelectedValue));
        Select_Room(ddlroom_en_search, int.Parse(ddlbuilding_en_search.SelectedValue));

        ddl_Sec(_ddlAddSec_trnf_en, 0, 0);
        select_costcenter(_ddlcostcenter_en);
        _ddllocate_en.SelectedValue = ViewState["locidx"].ToString();
        _ddllocate_en.Enabled = false;
        _check_transfer_flag_en.Checked = false;

        Select_NameMachine(ddlmachine_en_search,
                    int.Parse(ddllocate_en_search.SelectedValue),
                    int.Parse(ddlbuilding_en_search.SelectedValue),
                    int.Parse(ddlroom_en_search.SelectedValue),
                    int.Parse(ddltypemachine_en_search.SelectedValue));

        if (status == 0)
        {

            _txtplace_idx.Text = "";
            _txtplace_trnf.Text = "";
            _txtremark.Text = "";
        }


    }
    

    protected void Select_Location_all(DropDownList ddlName)
    {

        data_itasset dtitasset = new data_itasset();
        dtitasset.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 locate = new its_TransferDevice_u0();
        locate.operation_status_id = "sel_location_all";
        dtitasset.its_TransferDevice_u0_action[0] = locate;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        dtitasset = callServicePostITAsset(_urlGetits_u_transfer, dtitasset);
        setDdlData(ddlName, dtitasset.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new ListItem("เลือกสถานที่", "0"));
    }

    protected DataMachine callServicePostENRepair(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);




        return _dtmachine;
    }

    protected void select_typemachine(DropDownList ddlName)
    {
        data_en_planning _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan.BoxEN_TypeItemList[0] = typemachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทเครื่องจักร", "0"));

    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        data_en_planning _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกรหัสกลุ่มเครื่องจักร", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        data_en_planning _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกรหัสกลุ่ม", "0"));

    }

    protected void Select_Building(DropDownList ddlName, int LocIDX)
    {

        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail building = new RepairMachineDetail();

        building.LocIDX = LocIDX;
        _dtmachine.BoxRepairMachine[0] = building;

        _dtmachine = callServicePostENRepair(urlSelect_Building, _dtmachine);

        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกอาคาร", "0"));
    }

    protected void Select_Room(DropDownList ddlName, int BuildIDX)
    {

        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail room = new RepairMachineDetail();

        room.BuildingIDX = BuildIDX;
        _dtmachine.BoxRepairMachine[0] = room;

        _dtmachine = callServicePostENRepair(urlSelect_Room, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "Roomname", "RoomIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกห้อง", "0"));
    }
    protected void Select_NameMachine(DropDownList ddlName, int LocIDX, int BuildIDX, int RoomIDX, int TMCIDX)
    {
        /*
        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail machine = new RepairMachineDetail();

        machine.BuildingIDX = BuildIDX;
        machine.RoomIDX = RoomIDX;
        machine.TmcIDX = TMCIDX;
        _dtmachine.BoxRepairMachine[0] = machine;

        _dtmachine = callServicePostENRepair(urlSelect_NameMachine, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "NameMachine", "MCIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกเครื่องจักร", "0"));
        */

        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.LocIDX = LocIDX;
        select_its.BuildingIDX = BuildIDX;
        select_its.RoomIDX = RoomIDX;
        select_its.TmcIDX = TMCIDX;
        select_its.operation_status_id = "sel_devices_en";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        setDdlData(ddlName, dtitseet.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new ListItem("เลือก Asset No", "0"));
    }

    protected void add_trnf_en()
    {
        setObject_transferIT();

        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr in dsContacts.Tables["dsits_TransferDevice_u1"].Rows)
        {

            if (dr["asset_no"].ToString() == _func_dmu.zGetDataDropDownList(_ddlmachine_en_search))
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                showMsAlert("มีข้อมูล Asset Num. นี้แล้ว!!!");
                return;
            }

        }

        //ckeck asset no
        its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_m.operation_status_id = "check_dupicat_trnf";
        obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
        obj_m.asset_no = _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf);
        _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            var item = _dtitseet.its_TransferDevice_u0_action[0];
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + item.asset_no + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
            return;
        }

        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.LocIDX = _func_dmu.zStringToInt(_ddllocate_en_search.SelectedValue);
        select_its.BuildingIDX = _func_dmu.zStringToInt(_ddlbuilding_en_search.SelectedValue);
        select_its.RoomIDX = _func_dmu.zStringToInt(_ddlroom_en_search.SelectedValue);
        select_its.TmcIDX = _func_dmu.zStringToInt(_ddltypemachine_en_search.SelectedValue);
        select_its.device_idx = _func_dmu.zStringToInt(_ddlmachine_en_search.SelectedValue);
        select_its.operation_status_id = "sel_devices_en_all";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);

        if (_GvReportAdd.Rows.Count == 0)
        {
            CreateDsits_TransferDevice_u1();
        }

        dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_TransferDevice_u0_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

                drContacts["id"] = ic;
                drContacts["u0_transf_idx"] = 0;
                drContacts["u1_transf_idx"] = 0;
                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.name_m0_typedevice;
                drContacts["u1_status"] = 1;
                drContacts["device_idx"] = vdr.device_idx;
                drContacts["asidx"] = vdr.asidx;
                drContacts["asset_no"] = vdr.u0_acc;
                drContacts["qty"] = 1;
                drContacts["acquis_val"] = vdr.acquis_val;
                drContacts["book_val"] = 0;
                drContacts["price"] = 0;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["ionum"] = vdr.ionum;
                drContacts["desc_name"] = vdr.desc_name;
                drContacts["doc_u2idx"] = vdr.doc_u2idx;
                drContacts["o_org_idx"] = vdr.org_idx;
                drContacts["o_rdept_idx"] = vdr.rdept_idx;
                drContacts["o_rsec_idx"] = vdr.rsec_idx;
                drContacts["o_emp_idx"] = vdr.emp_idx;
                drContacts["o_rpos_idx"] = vdr.rpos_idx;
                drContacts["o_LocIDX"] = vdr.LocIDX;
                drContacts["o_BuildingIDX"] = vdr.BuildingIDX;
                drContacts["o_RoomIDX"] = vdr.RoomIDX;
                drContacts["subno_flag"] = 0;

                dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบอุปกรณ์ !!!!')", true);
            return;
        }
        ViewState["vsits_TransferDevice_u1"] = dsContacts;

        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);

    }
    private void setTrnf_InsertDefault_hr(int status,int sysidx)
    {
        setObject_transferIT();
        
        if (status == 0)
        {
            ddl_organization(_ddlAddOrg_trnf_hr, 0);
            _ddlAddOrg_trnf_hr.SelectedValue = ViewState["Org_idx"].ToString();
            ddl_depertment(_ddlAddDep_trnf_hr, 1, 0);
            setLocation_hr(_ddllocate_hr);
            ddl_Sec(_ddlAddSec_trnf_hr, 0, 0);
            select_costcenter(_ddlcostcenter_hr);
            _ddllocate_hr.SelectedValue = ViewState["locidx"].ToString();
            _ddllocate_hr.Enabled = false;
            _check_transfer_flag.Checked = false;
            //  getEmpList(_ddlAddemp_idx_trnf_hr, int.Parse(_ddlAddOrg_trnf_hr.SelectedValue), int.Parse(_ddlAddDep_trnf_hr.SelectedValue), int.Parse(_ddlAddSec_trnf_hr.SelectedValue));
            _txtplace_idx.Text = "";
            _txtplace_trnf.Text = "";
            _txtremark.Text = "";
        }
        
           
        /*
        else if(sysidx == 36) // qa
        {
            Select_Location_all(_ddllocate_qa_search);
            _ddllocate_qa_search.SelectedValue = ViewState["locidx"].ToString();
            _ddllocate_qa_search.Enabled = false;
            select_systemtype(_ddlSearch_devices_trnf_qa, 36);
            ddl_asset_no_qa(_ddlasset_no_trnf_qa, 0, 0);
        }
        else*/
        if (sysidx == 37) // other
        {
            Select_Location_all(_ddllocate_other_search);
            _ddllocate_other_search.SelectedValue = ViewState["locidx"].ToString();
            _ddllocate_other_search.Enabled = false;
            select_costcenter(_ddlcostcenter_other_search);
            _ddlcostcenter_other_search.SelectedValue = ViewState["CostIDX"].ToString();
            _txtasset_no.Text = "";
            _txtdesc_name.Text = "";
            _txtacquis_val.Text = "";
            _txtref_itnum.Text = "";
            _txtionum.Text = "";
        }
        else
        {
            Select_Location_all(_ddllocate_hr_search);

            _ddllocate_hr_search.SelectedValue = ViewState["locidx"].ToString();
            _ddllocate_hr_search.Enabled = false;
            select_systemtype_hr(_ddlSearch_devices_trnf_hr);
            select_costcenter(_ddlcostcenter_hr_search);
            _ddlcostcenter_hr_search.SelectedValue = ViewState["CostIDX"].ToString();

            //litDebug.Text = ViewState["locidx"].ToString() + " - " + ViewState["CostIDX"].ToString();

            ddl_asset_no_hr(_ddlasset_no_trnf_hr, 0, _func_dmu.zStringToInt(_ddllocate_hr_search.SelectedValue), _func_dmu.zStringToInt(_ddlcostcenter_hr_search.SelectedValue));

        }



    }
    /*
    private void setTrnf_InsertDefault_qa(int status = 0)
    {
        setObject_transferIT();
        if (status == 0)
        {
            ddl_organization(_ddlAddOrg_trnf_qa, 0);
            ddl_depertment(_ddlAddDep_trnf_qa, 1, 0);
            setLocation_hr(_ddllocate_qa);
            _txtplace_idx.Text = "";
            _txtplace_trnf.Text = "";
            _txtremark.Text = "";
        }
        Select_Location_all(_ddllocate_qa_search);
        _ddllocate_qa_search.SelectedValue = ViewState["locidx"].ToString();
        _ddllocate_qa_search.Enabled = false;
        select_systemtype(_ddlSearch_devices_trnf_qa, 36);
        ddl_asset_no_qa(_ddlasset_no_trnf_qa, 0, 0);


    }
    */
    private void setTrnf_InsertDefault_other(int status = 0)
    {
        setObject_transferIT();
        if (status == 0)
        {
            ddl_organization(_ddlAddOrg_trnf_other, 0);
            ddl_depertment(_ddlAddDep_trnf_other, 1, 0);
            setLocation_hr(_ddllocate_other);
            _txtplace_idx.Text = "";
            _txtplace_trnf.Text = "";
            _txtremark.Text = "";
        }
        Select_Location_all(_ddllocate_other_search);
        _ddllocate_other_search.SelectedValue = ViewState["locidx"].ToString();
        _ddllocate_other_search.Enabled = false;
        select_costcenter(_ddlcostcenter_other_search);
        _ddlcostcenter_other_search.SelectedValue = ViewState["CostIDX"].ToString();

        _txtasset_no.Text = "";
        _txtdesc_name.Text = "";
        _txtacquis_val.Text = "";
        _txtref_itnum.Text = "";
        _txtionum.Text = "";




    }
    protected void setLocation_hr(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        // select_its.emp_idx = _idx;
        select_its.operation_status_id = "sel_location_all";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        setDdlData(ddlName, dtitseet.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("สถานที่", "0"));

        /*
        _txtplace_idx = (TextBox)FvTrnfInsert.FindControl("txtplace_idx_hr");
        _txtplace_trnf = (TextBox)FvTrnfInsert.FindControl("txtplace_trnf_hr");
        _txtplace_idx.Text = "";
        _txtplace_trnf.Text = "";
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            foreach (var item in _dtitseet.its_TransferDevice_u0_action)
            {
                _txtplace_idx.Text = item.idx.ToString();
                _txtplace_trnf.Text = item.zname;
            }
        }
        */

    }
    protected void select_systemtype_hr(DropDownList ddlName)
    {
        // tb its_m0_typereference
        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();

        dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();
        system.SysIDX = 9;
        dtitseet.boxprice_reference[0] = system;

        dtitseet = callServicePostITAsset(_urlSelectSystem_TypePrice, dtitseet);
        setDdlData(ddlName, dtitseet.boxprice_reference, "type_name", "typidx");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกประเภทอุปกรณ์", "0"));

    }

    protected void ddl_asset_no_hr(DropDownList ddlName, int _idx, int plant_idx,int cost_idx)
    {
        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.idx = _idx;
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        //select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.place_idx = plant_idx;
        select_its.cost_idx = cost_idx;
        select_its.operation_status_id = "sel_devices_hr";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        setDdlData(ddlName, dtitseet.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new ListItem("เลือก Asset No", "0"));
    }

    protected void ddl_asset_no_qa(DropDownList ddlName, int _idx, int plant_idx)
    {
        ddlName.Items.Clear();
        data_itasset dtitseet = new data_itasset();
        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.idx = _idx;
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        //select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.place_idx = plant_idx;
        select_its.operation_status_id = "sel_devices_qa";
        dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        setDdlData(ddlName, dtitseet.its_TransferDevice_u0_action, "zname", "idx");
        ddlName.Items.Insert(0, new ListItem("เลือก Asset No", "0"));
    }

    protected void add_trnf_hr()
    {
        setObject_transferIT();

        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr in dsContacts.Tables["dsits_TransferDevice_u1"].Rows)
        {

            if (dr["asset_no"].ToString() == _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf_hr))
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                showMsAlert("มีข้อมูล Asset Num. นี้แล้ว!!!");
                return;
            }

        }
       
        //ckeck asset no
        its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_m.operation_status_id = "check_dupicat_trnf";
        obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
        obj_m.asset_no = _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf_hr);
        _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            var item = _dtitseet.its_TransferDevice_u0_action[0];
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + item.asset_no + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
            return;
        }
 
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
       // select_its.idx = _func_dmu.zStringToInt(_ddlSearch_devices_trnf_hr.SelectedValue);
        select_its.asidx = _func_dmu.zStringToInt(_ddlasset_no_trnf_hr.SelectedValue);
        //select_its.emp_idx = _func_dmu.zStringToInt(_ddlAddemp_idx_trnf_hr.SelectedValue);
        select_its.place_idx = _func_dmu.zStringToInt(_ddllocate_hr_search.SelectedValue);
       select_its.cost_idx = _func_dmu.zStringToInt(_ddlcostcenter_hr_search.SelectedValue);
        select_its.operation_status_id = "sel_devices_hr_all";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
       // litDebug.Text  = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);

        if (_GvReportAdd.Rows.Count == 0)
        {
            CreateDsits_TransferDevice_u1();
        }

        dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_TransferDevice_u0_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

                drContacts["id"] = ic;
                drContacts["u0_transf_idx"] = 0;
                drContacts["u1_transf_idx"] = 0;
                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.name_m0_typedevice;
                drContacts["u1_status"] = 1;
                drContacts["device_idx"] = vdr.device_idx;
                drContacts["asidx"] = vdr.asidx;
                drContacts["asset_no"] = vdr.u0_acc;
                drContacts["qty"] = 1;
                drContacts["acquis_val"] = vdr.acquis_val;
                drContacts["book_val"] = 0;
                drContacts["price"] = 0;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["ionum"] = vdr.ionum;
                drContacts["desc_name"] = vdr.desc_name;
                drContacts["doc_u2idx"] = vdr.doc_u2idx;
                drContacts["o_org_idx"] = vdr.org_idx;
                drContacts["o_rdept_idx"] = vdr.rdept_idx;
                drContacts["o_rsec_idx"] =  vdr.rsec_idx;
                drContacts["o_emp_idx"] = 0;// vdr.emp_idx;
                drContacts["o_rpos_idx"] = 0;// vdr.rpos_idx;
                drContacts["o_LocIDX"] = vdr.place_idx;
                drContacts["subno_flag"] = 0;

                dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบอุปกรณ์ !!!!')", true);
            return;
        }
        ViewState["vsits_TransferDevice_u1"] = dsContacts;

        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);
        
    }


    protected void add_trnf_qa()
    {
        setObject_transferIT();

        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr in dsContacts.Tables["dsits_TransferDevice_u1"].Rows)
        {

            if (dr["asset_no"].ToString() == _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf_qa))
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                showMsAlert("มีข้อมูล Asset Num. นี้แล้ว!!!");
                return;
            }

        }

        //ckeck asset no
        its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_m.operation_status_id = "check_dupicat_trnf";
        obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
        obj_m.asset_no = _func_dmu.zGetDataDropDownList(_ddlasset_no_trnf_qa);
        _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            var item = _dtitseet.its_TransferDevice_u0_action[0];
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + item.asset_no + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
            return;
        }

        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 select_its = new its_TransferDevice_u0();
        select_its.idx = _func_dmu.zStringToInt(_ddlSearch_devices_trnf_qa.SelectedValue);
        select_its.device_idx = _func_dmu.zStringToInt(_ddlasset_no_trnf_qa.SelectedValue);
        //select_its.emp_idx = _func_dmu.zStringToInt(_ddlAddemp_idx_trnf_qa.SelectedValue);
        select_its.operation_status_id = "sel_devices_qa_all";
        _dtitseet.its_TransferDevice_u0_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_transfer, _dtitseet);

        if (_GvReportAdd.Rows.Count == 0)
        {
            CreateDsits_TransferDevice_u1();
        }

        dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_TransferDevice_u0_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

                drContacts["id"] = ic;
                drContacts["u0_transf_idx"] = 0;
                drContacts["u1_transf_idx"] = 0;
                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.name_m0_typedevice;
                drContacts["u1_status"] = 1;
                drContacts["device_idx"] = vdr.device_idx;
                drContacts["asidx"] = vdr.asidx;
                drContacts["asset_no"] = vdr.u0_acc;
                drContacts["qty"] = 1;
                drContacts["acquis_val"] = vdr.acquis_val;
                drContacts["book_val"] = 0;
                drContacts["price"] = 0;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["ionum"] = vdr.ionum;
                drContacts["desc_name"] = vdr.desc_name;
                drContacts["doc_u2idx"] = vdr.doc_u2idx;
                drContacts["o_org_idx"] = vdr.org_idx;
                drContacts["o_rdept_idx"] = vdr.rdept_idx;
                drContacts["o_rsec_idx"] = vdr.rsec_idx;
                drContacts["o_emp_idx"] = 0;// vdr.emp_idx;
                drContacts["o_rpos_idx"] = 0;// vdr.rpos_idx;
                drContacts["o_LocIDX"] = vdr.place_idx;
                drContacts["subno_flag"] = 0;

                dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบอุปกรณ์ !!!!')", true);
            return;
        }
        ViewState["vsits_TransferDevice_u1"] = dsContacts;

        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);

    }

    protected void add_trnf_other()
    {
        setObject_transferIT();

        DataSet dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];
        foreach (DataRow dr in dsContacts.Tables["dsits_TransferDevice_u1"].Rows)
        {

            if (dr["asset_no"].ToString() == _txtasset_no.Text.Trim().ToUpper())
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                showMsAlert("มีข้อมูล Asset Num. นี้แล้ว!!!");
                return;
            }

        }

        //ckeck asset no
        its_TransferDevice_u0 obj_m = new its_TransferDevice_u0();
        _dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        obj_m.operation_status_id = "check_dupicat_trnf";
        obj_m.u0_transf_idx = _func_dmu.zStringToInt(_hfu0_transf_idx.Value);
        obj_m.asset_no = _txtasset_no.Text.Trim().ToUpper();
        _dtitseet.its_TransferDevice_u0_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_u_transfer, _dtitseet);
        if (_dtitseet.its_TransferDevice_u0_action != null)
        {
            var item = _dtitseet.its_TransferDevice_u0_action[0];
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Asset Num. " + item.asset_no + " กำลังอยู่ระหว่างการโอนย้าย (" + item.doccode + ") !!!!')", true);
            return;
        }


        if (_GvReportAdd.Rows.Count == 0)
        {
            CreateDsits_TransferDevice_u1();
        }

        dsContacts = (DataSet)ViewState["vsits_TransferDevice_u1"];

        int ic = 0;

        ic++;
        DataRow drContacts = dsContacts.Tables["dsits_TransferDevice_u1"].NewRow();

        drContacts["id"] = ic;
        drContacts["u0_transf_idx"] = 0;
        drContacts["u1_transf_idx"] = 0;
        drContacts["tdidx"] = 0;
        drContacts["tdidx_name"] = "";
        drContacts["u1_status"] = 1;
        drContacts["device_idx"] = 0;
        drContacts["asidx"] = 0;
        drContacts["asset_no"] = _txtasset_no.Text.Trim().ToUpper();
        drContacts["qty"] = 1;
        drContacts["acquis_val"] = _func_dmu.zStringToDecimal(_txtacquis_val.Text);
        drContacts["book_val"] = 0;
        drContacts["price"] = 0;
        drContacts["ref_itnum"] = _txtref_itnum.Text.Trim();
        drContacts["ionum"] = _txtionum.Text.Trim();
        drContacts["desc_name"] = _txtdesc_name.Text.Trim();
        drContacts["doc_u2idx"] = 0;
        drContacts["o_org_idx"] = ViewState["org_idx"].ToString();
        drContacts["o_rdept_idx"] = ViewState["rdept_idx"].ToString();
        drContacts["o_rsec_idx"] = 0;// vdr.rsec_idx;
        drContacts["o_emp_idx"] = 0;// vdr.emp_idx;
        drContacts["o_rpos_idx"] = 0;// vdr.rpos_idx;
        drContacts["o_LocIDX"] = _ddllocate_other_search.SelectedValue;
        drContacts["subno_flag"] = _func_dmu.zBooleanToInt(_check_subno_other.Checked);

        dsContacts.Tables["dsits_TransferDevice_u1"].Rows.Add(drContacts);

        ViewState["vsits_TransferDevice_u1"] = dsContacts;

        setGridData(_GvReportAdd, dsContacts.Tables["dsits_TransferDevice_u1"]);

    }

    protected void SelectDetailHolder(int _DeviceIDX)
    {
        DataHoldersDevices _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.DeviceIDX = _DeviceIDX;

        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectDetailHolder, _dtholder);

        FvDetailUser_holder_devices.DataSource = _dtholder.BoxHoldersDevices;
        FvDetailUser_holder_devices.DataBind();

        var rt_NEmpIDX = _dtholder.ReturnEmpNode;
        ViewState["rt_NEmpIDX"] = rt_NEmpIDX;

    }

    protected DataHoldersDevices callServicePostHolder(string _cmdUrl, DataHoldersDevices _dtholder)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtholder);
        // text.Text = _cmdUrl + _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  text.Text = _localJson;

        //// convert json to object
        _dtholder = (DataHoldersDevices)_funcTool.convertJsonToObject(typeof(DataHoldersDevices), _localJson);




        return _dtholder;
    }


    protected void Update_ApproveChangeHolder(data_itasset dataitasset, int iapprove, string _txtremark_approve) // ผู้รับโอนย้ายรับทราบการโอนย้าย
    {
        var lblholderidx = (Label)FvDetailUser_holder_devices.FindControl("lblholderidx");
        var lblDeviceIDX = (Label)FvDetailUser_holder_devices.FindControl("lblDeviceIDX");
        var lblrdept = (Label)FvDetailUser_holder_devices.FindControl("lblrdept");
        var lblorg = (Label)FvDetailUser_holder_devices.FindControl("lblorg");
        var lblm0_tdidx = (Label)FvDetailUser_holder_devices.FindControl("lblm0_tdidx");

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser_holder_devices.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailUser_holder_devices.FindControl("hfM0ActoreIDX");

        int node = int.Parse(hfM0NodeIDX.Value);
        int actor = int.Parse(hfM0ActoreIDX.Value);
        int ichangeApprove = 0;
        if (iapprove == 1) //อนุมัติ
        {
            ichangeApprove = 3;
        }
        else if (iapprove == 2) //ไม่อนุมัติ
        {
            ichangeApprove = 4;
        }
        else
        {
            return;
        }

        if (dataitasset.its_TransferDevice_u0_action != null)
        {
            foreach (var v_u0 in dataitasset.its_TransferDevice_u0_action)
            {
                if (dataitasset.its_TransferDevice_u1_action != null)
                {
                    foreach (var v_u1 in dataitasset.its_TransferDevice_u1_action)
                    {
                        DataHoldersDevices _dtholder = new DataHoldersDevices();
                        _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
                        AddHoldersDevices u1search = new AddHoldersDevices();
                        try
                        {
                            u1search.status = ichangeApprove;
                            u1search.acidx = int.Parse(hfM0ActoreIDX.Value);
                            u1search.noidx = int.Parse(hfM0NodeIDX.Value);
                            u1search.EmpIDX_add = emp_idx;
                            u1search.HolderIDX = int.Parse(lblholderidx.Text);
                            u1search.Remark = _txtremark_approve;
                            u1search.DeviceIDX_add = int.Parse(lblDeviceIDX.Text);

                            _dtholder.BoxAddHoldersDevices[0] = u1search;
                            //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtholder));

                            _dtholder = callServicePostHolder(urlUpdate_ApproveChangeHolder, _dtholder);
                        }
                        catch
                        {
                            //
                        }

                        if (ViewState["Software"].ToString() == "2")
                        {

                            if (int.Parse(hfM0NodeIDX.Value) == 8 && int.Parse(hfM0ActoreIDX.Value) == 2 && ichangeApprove == 1 ||
                                int.Parse(hfM0NodeIDX.Value) == 8 && int.Parse(hfM0ActoreIDX.Value) == 2 && ichangeApprove == 3)
                            {
                                try
                                {
                                    data_device _data_deviceregister = new data_device();

                                    _data_deviceregister.u0_device_list = new u0_device_detail[1];
                                    u0_device_detail _data_device_detail = new u0_device_detail();

                                    //dv_u0_device	
                                    _data_device_detail.u0_didx = v_u1.device_idx; //รหัสอุปกรณ์
                                    _data_device_detail.m0_orgidx = int.Parse(ViewState["org_idx"].ToString());
                                    _data_device_detail.R0_depidx = int.Parse(ViewState["rdept_idx"].ToString());
                                    _data_device_detail.doc_status = "1";// ddl_changeApprove.SelectedValue; //ใช้งานปกติ
                                    _data_device_detail.HDEmpIDX = emp_idx; //รหัสพนักงาน ผู้ถือครอง ex. 173

                                    // dv_u1_device
                                    _data_device_detail.u0_unidx = 2;
                                    _data_device_detail.u0_acidx = 1;
                                    _data_device_detail.u0_node_dec = 2;
                                    _data_device_detail.u0_doc_decision = 1; //ผลการอนุมัติ,ไม่อนุมัติ
                                    _data_device_detail.Approve_stauts = 1; //สถานะเอกสาร

                                    _data_deviceregister.u0_device_list[0] = _data_device_detail;

                                    _data_deviceregister = callServicePostDevice(_url_u0_device_update_17, _data_deviceregister);
                                }
                                catch
                                {
                                    //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                                }
                            }
                            else if (ichangeApprove == 2 || ichangeApprove == 4)
                            {
                                if (lblm0_tdidx.Text == "1" || lblm0_tdidx.Text == "2")
                                {
                                    try
                                    {
                                        data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                                        _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[1]; //chcksoft.Items.Count


                                        u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                                        u0_softwarelicense_detail_dataset.u0_didx = v_u1.device_idx;
                                        u0_softwarelicense_detail_dataset.cemp_idx = emp_idx;
                                        u0_softwarelicense_detail_dataset.doc_decision = 5;

                                        _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[0] = u0_softwarelicense_detail_dataset;


                                        //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                                        //text.Text = localJson1;

                                        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                                        _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(urlSetApproveLicense, _data_softwarelicense_devicesinsert_);
                                    }
                                    catch
                                    {
                                        //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }


    }
    protected void SelectViewSoftware(int _DeviceIDX)
    {
        //  text.Text = ViewState["U0IDX"].ToString();

        data_softwarelicense_devices _data_softwarelicense_devicesview = new data_softwarelicense_devices();
        _data_softwarelicense_devicesview.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

        bind_softwarelicense_detail _bind_softwarelicense_detailview = new bind_softwarelicense_detail();

        _bind_softwarelicense_detailview.u0_didx = _DeviceIDX;

        _data_softwarelicense_devicesview.bind_softwarelicense_list[0] = _bind_softwarelicense_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_devicesview = callServiceSoftwareLicenseDevices(urlGetSoftwareShowHolder, _data_softwarelicense_devicesview);

        chcksoft.DataSource = _data_softwarelicense_devicesview.bind_softwarelicense_list;
        if (_data_softwarelicense_devicesview.bind_softwarelicense_list == null)
        {
            //  ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเลือกผู้ถือครองได้ กรุณาเพิ่ม Software');", true);
            ViewState["Software"] = "1";
        }
        else
        {
            chcksoft.DataTextField = "software_name";
            chcksoft.DataValueField = "software_name_idx";
            chcksoft.DataBind();

            for (int i = 0; i < chcksoft.Items.Count; i++)
            {
                chcksoft.Items[i].Selected = true;
            }
            ViewState["Software"] = 2;
        }

    }

    protected string ddl_depertment_en(DropDownList ddlName, int organization_idx, int _type, int rdept_idx)
    {

        ddlName.Items.Clear();

        if (_type == 0)
        {
            data_itasset dataitasset = new data_itasset();
            dataitasset.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
            its_TransferDevice_u0 select_depertment = new its_TransferDevice_u0();

            select_depertment.org_idx = organization_idx;
            select_depertment.rdept_idx = rdept_idx;
            select_depertment.operation_status_id = "sel_department_mc";

            dataitasset.its_TransferDevice_u0_action[0] = select_depertment;
            dataitasset = callServicePostITAsset(_urlGetits_u_transfer, dataitasset);

            ddlName.DataSource = dataitasset.its_TransferDevice_u0_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกฝ่าย", "0"));

        }
        else if (_type == 1)
        {
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("เลือกฝ่าย", "0"));
        }

        return ddlName.SelectedItem.Value;

    }
    #endregion

    private int getItemFile(int type, string doccode)
    {
        int ic = 0, i = 0;
        string stype = "";
        //if (type == 1)
        //{
        stype = "[เอกสารโอนย้าย]" + doccode;
        //}

        try
        {
            string Pathfile = ConfigurationManager.AppSettings["pathfile_asset_transfer"];
            Pathfile = Pathfile + doccode;
            string[] filesLoc = Directory.GetFiles(Server.MapPath(Pathfile + "/"));
            // List<System.Web.UI.WebControls.ListItem> files = new List<System.Web.UI.WebControls.ListItem>();
            foreach (string file in filesLoc)
            {
                try
                {
                    string getfiles = Path.GetFileName(file);
                    if (getfiles.Contains(stype) == true)
                    {
                        int index2 = getfiles.IndexOf('.');
                        i = (index2 - 1) - stype.Length;
                        string sItem = getfiles.Substring(stype.Length + 1, i);
                        i = _func_dmu.zStringToInt(sItem);
                        if (ic < i)
                        {
                            ic = i;
                        }
                    }
                }
                catch { }
            }
        }
        catch { }


        return ic;
    }

    private void setSearchList()
    {
        ddlcondition.SelectedValue = "0";
        txtenddate.Text = String.Empty;
        txtstartdate.Text = String.Empty;
        txtenddate.Enabled = false;
        action_select_organization(0, "0");
        action_select_depertment(0, "0");
        setLocation_hr(ddllocation_search);
        select_system(ddlsearch_typequipment);
        //select_node_status_its(ddlsearch_status);
    }
    protected string action_select_organization(int _type, string _selectvalue)
    {

        //ddlsearch_organization.Items.Clear();

        if (_type == 0)
        {

            data_softwarelicense __data_softwarelicense = new data_softwarelicense();
            __data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_organization = new organization_detail();

            __data_softwarelicense.organization_list[0] = select_organization;
            __data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, __data_softwarelicense);

            ddlsearch_organization.DataSource = __data_softwarelicense.organization_list;
            ddlsearch_organization.DataTextField = "OrgNameTH";
            ddlsearch_organization.DataValueField = "OrgIDX";
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก organization", "0"));
            // ddlsearch_organization.SelectedValue = _selectvalue;

        }
        else if (_type == 1)
        {
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก organization", "0"));
        }

        return ddlsearch_organization.SelectedItem.Value;


    }

    protected string action_select_depertment(int _type, string _selectvalue)
    {

        ddlsearch_department.Items.Clear();

        if (_type == 0)
        {

            _data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_depertment = new organization_detail();

            select_depertment.OrgIDX = Int32.Parse(ddlsearch_organization.SelectedValue);

            _data_softwarelicense.organization_list[0] = select_depertment;
            _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);

            ddlsearch_department.DataSource = _data_softwarelicense.organization_list;
            ddlsearch_department.DataTextField = "DeptNameTH";
            ddlsearch_department.DataValueField = "RDeptIDX";
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก depertment", "0"));

        }
        else if (_type == 1)
        {
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก depertment", "0"));
        }

        return ddlsearch_department.SelectedItem.Value;

    }

    protected string getStatus_subno(string status)
    {
        if (status == "1")
        {

            return "<span><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span ><i class='glyphicon glyphicon-minus'></i></span>";
        }
    }


    protected void CreateDsits_ListApprov()
    {
        string sDs = "dsits_listapprov";
        string sVs = "vsits_listapprov";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("name", typeof(String));
        ViewState[sVs] = ds;

    }

    protected void ShowListApprov(int id)
    {
        DataSet dsContacts = (DataSet)ViewState["vsits_listapprov"];
        DataRow drContacts;
        
        drContacts = dsContacts.Tables["dsits_listapprov"].NewRow();
        drContacts["id"] = 1;
        drContacts["name"] = "อนุมัติ";
        dsContacts.Tables["dsits_listapprov"].Rows.Add(drContacts);

        drContacts = dsContacts.Tables["dsits_listapprov"].NewRow();
        drContacts["id"] = 2;
        drContacts["name"] = "ไม่อนุมัติ";
        dsContacts.Tables["dsits_listapprov"].Rows.Add(drContacts);

        if(id == 1)
        {
            drContacts = dsContacts.Tables["dsits_listapprov"].NewRow();
            drContacts["id"] = 15;
            drContacts["name"] = "กลับไปแก้ไข Book Value";
            dsContacts.Tables["dsits_listapprov"].Rows.Add(drContacts);
        }
        

        ViewState["vsits_listapprov"] = dsContacts;

    }

    protected void ddlListApprov(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        setDdlData(ddlName, ViewState["vsits_listapprov"], "name", "id");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ...", "0"));
    }

}