﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_setname.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_setname" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="test_place" runat="server"></asp:Literal>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_addsetname" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มชุด" data-toggle="tooltip" title="เพิ่มชุด" runat="server"
                    CommandName="cmdAddSetname" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มชุด</asp:LinkButton>
            </div>

            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มชุด</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lblset_name" runat="server" Text="ชื่อ set" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtset_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อ set ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_set_name" runat="server"
                                                    ControlToValidate="txtset_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อ set" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_sname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_set_name" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblset_detail" runat="server" Text="รายละเอียด set" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtset_detail" runat="server" CssClass="form-control" placeholder="กรอกรายละเอียด set ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_set_detail" runat="server"
                                                    ControlToValidate="txtset_detail" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกรายละเอียด set" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_sdetail" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_set_detail" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lab_statussetname" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddSetname" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_setname" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_setname" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_setname" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_setname" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select setname--%>
            <asp:GridView ID="Gv_select_setname" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลชุด</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="setname_idx" runat="server" Visible="false" Text='<%# Eval("setname_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_setname" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("setname_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ set" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_setname" runat="server" CssClass="form-control " Text='<%# Eval("set_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorset_name" runat="server"
                                                ControlToValidate="Name_setname" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อ set" ValidationGroup="Editsetname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_unameth" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorset_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียด set" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Detail_setname" runat="server" CssClass="form-control " Text='<%# Eval("set_detail") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorset_detail" runat="server"
                                                ControlToValidate="Detail_setname" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียด set" ValidationGroup="Editsetname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_unameen" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorset_detail" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_setname" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_setname" Text='<%# Eval("setname_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editsetname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อ set" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbset_name" runat="server"
                                        Text='<%# Eval("set_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียด set" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbset_detail" runat="server"
                                        Text='<%# Eval("set_detail") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbsetname_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("setname_status") %>'></asp:Label>
                                    <asp:Label ID="setname_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="setname_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnview_exa" CssClass="text-read" runat="server" CommandName="cmdview_exa"
                                    data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument='<%#Eval("setname_idx")%>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_setname"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบชุดนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("setname_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>


        <asp:View ID="view_CreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdback" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAddRoot" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSet" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสถานะชุดข้อมูล</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvform_Insert_Root" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มสถานะชุดข้อมูล</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSetNameList" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="false" />

                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlRootSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">

                                    <asp:RequiredFieldValidator ID="ReqtbRootSetName" runat="server"
                                        ControlToValidate="tbRootSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="saveRootSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbRootSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbRootSetName" Width="180" />
                                    <label class="col-sm-2 control-label">ชื่อสถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbRootSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="true" />

                                    </div>

                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชุดข้อมูล</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbDetailRootSet" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชุดข้อมูล ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbDetailRootSet" runat="server"
                                        ControlToValidate="tbDetailRootSet" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="saveRootSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbDetailRootSet" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbDetailRootSet" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="saveRootSetName" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMasterRoot"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="setname_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbrootsetidx" runat="server" CssClass=" font_text text_center" Visible="False" Text='<%# Eval("setname_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateRootSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("setname_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานะชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateRootSetName" runat="server" CssClass="form-control" Text='<%# Eval("set_name")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_set_name" runat="server"
                                                ControlToValidate="tbupdateRootSetName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อสถานะชุดข้อมูล" ValidationGroup="EditRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_sname" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_set_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateRootSetDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_set_detail" runat="server"
                                                ControlToValidate="tbupdateRootSetDetail" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="EditRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_sdetail" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_set_detail" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateRootSetStatus" Text='<%# Eval("setname_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="EditRoot"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblRootSetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>
                                <asp:TextBox ID="tbSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("setroot_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbRootSetDetails" runat="server" Text='<%# Eval("set_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("setname_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnViewRootSet" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("setname_idx") + "," + "1" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash small" runat="server" CommandName="cmdDeleteSet"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("setname_idx") +"," + "1" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

        <asp:View ID="pageRootSubSet" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lnkbtnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="lnkbtnRootSet" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSet" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสถานะชุดข้อมูลย่อย</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvformInsertSubSet" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มสถานะชุดข้อมูลย่อย</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสถานะชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSubSetName" runat="server" CssClass="form-control" Enabled="false" />


                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbSubSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbSubSetName" runat="server"
                                        ControlToValidate="tbSubSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbSubSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbSubSetName" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชุดข้อมูล</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbDetailSubSet" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชุดข้อมูล ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbDetailSubSet" runat="server"
                                        ControlToValidate="tbDetailSubSet" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbDetailSubSet" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbDetailSubSet" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveSubSetName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterSubSet"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="setname_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbsubsetidx" runat="server" CssClass=" font_text text_center" Visible="False" Text='<%# Eval("setname_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateSubSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("setname_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานะชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetName" runat="server" CssClass="form-control" Text='<%# Eval("set_name")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_sub_setname" runat="server"
                                                ControlToValidate="tbupdateSubSetName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อสถานะชุดข้อมูล" ValidationGroup="saveSubRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_ssname" runat="Server" PopupPosition="BottomLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_sub_setname" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                            <asp:RequiredFieldValidator ID="Re_up_sub_setdetail" runat="server"
                                                ControlToValidate="tbupdateSubSetDetail" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="saveSubRoot" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_ssdetail" runat="Server" PopupPosition="BottomLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_up_sub_setdetail" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateSubSetStatus" Text='<%# Eval("setname_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="saveSubRoot"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblSubSetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>
                                <asp:TextBox ID="tbSubSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("setroot_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbSubSetDetails" runat="server" Text='<%# Eval("set_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootSubStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("setname_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <%--   <asp:LinkButton ID="btnViewRootSubSet" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("set_idx") + "," + "2" %>' title="view">--%>
                            <%--                 <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelRootSubSet" CssClass="text-trash small" runat="server" CommandName="cmdDeleteSet"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("setname_idx") +"," + "2" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>
    </asp:MultiView>
</asp:Content>
