﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_itr_m0_CaseRESLV1 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee dtEmployee = new data_employee();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsertCaseLV1RES = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMasterResCaseLV1"];
    static string urlSelectCaseLV1RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV1"];
    static string urlDeleteCaseLV1RES = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMasterResCaseLV1"];



    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_CaseLV1()
    {
        _dtsupport.BoxRESList = new RESList[1];
        RESList insert = new RESList();

        insert.RES1_Name = txtlv.Text;
        insert.RES1Status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxRESList[0] = insert;

        _dtsupport = callServicePostITRepair(urlInsertCaseLV1RES, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxRESList = new RESList[1];
        RESList select = new RESList();

        _dtsupport.BoxRESList[0] = select;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1RES, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxRESList);
    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxRESList = new RESList[1];
        RESList update = new RESList();


        update.RES1IDX = int.Parse(ViewState["RES1IDX"].ToString());
        update.RES1_Name = ViewState["txtname_edit"].ToString();
        update.RES1Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxRESList[0] = update;

        _dtsupport = callServicePostITRepair(urlInsertCaseLV1RES, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.BoxRESList = new RESList[1];
        RESList delete = new RESList();
        delete.RES1IDX = int.Parse(ViewState["RES1IDX"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.BoxRESList[0] = delete;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlDeleteCaseLV1RES, _dtsupport);
    }

  

    #endregion

    #region Gridview


    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int RES1IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["RES1IDX"] = RES1IDX;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }




    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":

                btnadd.Visible = false;
                Panel_Add.Visible = true;
                txtlv.Text = String.Empty;

                break;

            case "btnCancel":
                btnadd.Visible = true;
                Panel_Add.Visible = false;

                break;

            case "btnAdd":
                Insert_CaseLV1();
                SelectMasterList();
                Panel_Add.Visible = false;
                btnadd.Visible = true;

                break;

            case "CmdDel":

                int RES1IDX = int.Parse(cmdArg);
                ViewState["RES1IDX"] = RES1IDX;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion
}