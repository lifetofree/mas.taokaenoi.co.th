﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      int creator = 0;
      int groupDiaryIdx = 0;
      int empLengthTemp = 0;
      int empIdxs = 0;
      string callback = context.Request.QueryString["callback"];
      string announceGroupDiaryStartDate = String.Empty;
      string announceGroupDiaryEndDate = String.Empty;
      int.TryParse(context.Request.QueryString["empIdxs"], out empIdxs);
      int.TryParse(context.Request.QueryString["empLengthTemp"], out empLengthTemp);
      int.TryParse(context.Request.QueryString["groupDiaryIdx"], out groupDiaryIdx);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      announceGroupDiaryStartDate = context.Request.QueryString["announceGroupDiaryStartDate"];
      announceGroupDiaryEndDate = context.Request.QueryString["announceGroupDiaryEndDate"];
      string json = this.createAnnounceDiary(creator, empIdxs, empLengthTemp, groupDiaryIdx, announceGroupDiaryStartDate, announceGroupDiaryEndDate);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createAnnounceDiary(int creator, int empIdxs, int empLengthTemp, int groupDiaryIdx, string announceGroupDiaryStartDate, string announceGroupDiaryEndDate)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            int statusOnline = 1;
            cmd.Connection = conn;
            cmd.CommandText = "INSERT INTO emps_u0_announce_diary (u0_node_idx_ref, m0_group_diary_idx_ref, announce_diary_start_date, announce_diary_end_date, announce_diary_status, announce_diary_created_at, announce_diary_created_by, announce_diary_updated_at, announce_diary_updated_by) OUTPUT Inserted.u0_announce_diary_idx VALUES (1, @groupDiaryIdx, @announceGroupDiaryStartDate, @announceGroupDiaryEndDate, " + statusOnline + ", '" + now + "', @creator, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@announceGroupDiaryStartDate", announceGroupDiaryStartDate);
            cmd.Parameters.AddWithValue("@announceGroupDiaryEndDate", announceGroupDiaryEndDate);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.Parameters.AddWithValue("@groupDiaryIdx", groupDiaryIdx);
            conn.Open();
            var insertedId = cmd.ExecuteScalar();
            emps.Add(new
            {
               empIdxss = empIdxs,
               empLengthTemps = empLengthTemp,
               announceGroupDiaryIdx = insertedId
            });
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
