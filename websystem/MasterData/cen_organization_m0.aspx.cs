﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_organization_m0 : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    data_employee _dataEmployee = new data_employee();

    string _localJson = string.Empty;
    string textstatus = string.Empty;
    string checkSearch = string.Empty;
    string checkSearchnum = string.Empty;
    string checkSearchnum2 = string.Empty;
    int numSearch = 0;
    string msg = string.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];

    data_cen_master _data_cen_master = new data_cen_master();
    //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
    int _emp_idx = 0;


    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            setFormData(FormViewS, FormViewMode.Insert, null);
            ShowOrganization();
            FormViewS.Visible = true;
          
        }

        //ViewState["forSearch"] = null;
    }


    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        lbCreate.Visible = false;
        gvMaster.Visible = false;
        FvInsertEdit.Visible = false;
        FormViewS.Visible = false;
        hlSetTotop.Focus();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        switch (cmdName)
        {
            case "cmdCreate":
                //_functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                FvInsertEdit.Visible = true;
                setFormData(FvInsertEdit, FormViewMode.Insert, null);




                break;
            case "cmdEdit":

                //setFormData(FormViewS, FormViewMode.Insert, null);
                FvInsertEdit.Visible = true;
                //litdebug.Text = cmdArg;
                //data_cen_master _data_cen_master = new data_cen_master();
                //search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

                _search_cen_master_detail.s_org_idx = cmdArg;
                _data_cen_master.master_mode = "3";
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                setFormData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_org_list_m0);
                //FormView2.Visible = true;

                break;
            case "cmdCancel":
                //FormView2.Visible = false;
                FormViewS.Visible = true;
                lbCreate.Visible = true;
                gvMaster.Visible = true;
                //setFormData(FormViewS, FormViewMode.Insert, null);
                //setFormData(FvInsertEdit, FormViewMode.ReadOnly, null);
                if (ViewState["forSearch"] == null)
                {
                    ShowOrganization();
                }
                else
                {
                    _functionTool.setGvData(gvMaster, ((data_cen_master)ViewState["forSearch"]).cen_org_list_m0);

                }
                




                break;

            case "cmdReset":
                //FormView2.Visible = false;
                FormViewS.Visible = true;
                lbCreate.Visible = true;
                gvMaster.Visible = true;
                setFormData(FormViewS, FormViewMode.Insert, null);
                //setFormData(FvInsertEdit, FormViewMode.ReadOnly, null);
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                ShowOrganization();
                break;

            case "cmdDelete":
                setFormData(FormViewS, FormViewMode.Insert, null);

                cmdDelete(int.Parse(cmdArg));
                //if (ViewState["forSearch"] == null)
                //{
                //    ShowOrganization();
                //}
                //else
                //{

                //    checkBoxSearch();
                //}
                gvMaster.Visible = true;
                lbCreate.Visible = true;
                FormViewS.Visible = true;
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                ShowOrganization();



                break;

            case "editSave":
                InsertMaster(_functionTool.convertToInt(cmdArg));
                ////editSave();
                ////data_cen_master _data_cen_master = new data_cen_master();


                ////data_cen_master _data_cen_master2 = new data_cen_master();
                //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
                //DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatusedit");

                ////organization_details _orgDetail = new organization_details();
                //_cen_org_detail_m0.org_idx = int.Parse(cmdArg);
                //_cen_org_detail_m0.org_name_th = ((TextBox)FvInsertEdit.FindControl("tbNameTH")).Text.Trim();
                //_cen_org_detail_m0.org_name_en = ((TextBox)FvInsertEdit.FindControl("tbNameEN")).Text.Trim();
                //_cen_org_detail_m0.org_status = _functionTool.convertToInt(dropD_status.SelectedValue);
                //_cen_org_detail_m0.cemp_idx = _emp_idx;
                //_data_cen_master.master_mode = "3";

                //_data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
                //_data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

                ////litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                //_data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
                ////litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                ////litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));

                //if (_data_cen_master.return_code == 1)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                //    FvInsertEdit.Visible = true;
                //    lbCreate.Visible = false;

                //}
                //else
                //{
                //    //setFormData(FvInsertEdit, FormViewMode.ReadOnly, null);
                //    FvInsertEdit.Visible = false;
                //    lbCreate.Visible = true;
                //    FormViewS.Visible = true;
                //    //FormView2.Visible = false;
                //    gvMaster.Visible = true;
                //    setFormData(FormViewS, FormViewMode.Insert, null);
                //    ViewState["forSearch"] = null;
                //    ViewState["forWordSearch"] = null;
                //    ShowOrganization();
                //    //if (ViewState["forSearch"] == null)
                //    //{
                //    //    ShowOrganization();
                //    //}
                //    //else
                //    //{
                        
                //    //    checkBoxSearch();
                //    //}
                //}

                break;
            case "cmdSave":

                lbCreate.Visible = true;
                InsertMaster(_functionTool.convertToInt(cmdArg));



                break;

            case "cmdSearch":
                TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
                ViewState["forWordSearch"] = searchbox.Text.Trim();
                checkBoxSearch();
                lbCreate.Visible = true;
                gvMaster.Visible = true;
                FormViewS.Visible = true;
                break;
        }
    }

    protected void InsertMaster(int id)
    {
        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_nameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_nameen");
        DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
        TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
        //litdebug.Text = tex_TH_name.Text;
        //litdebug.Text += tex_EN_name.Text;
        //litdebug.Text += dropD_status.SelectedValue;
        data_cen_master _data_cen_master = new data_cen_master();
        cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
        _data_cen_master.master_mode = "3";
        _cen_org_detail_m0.org_idx = id;
        _cen_org_detail_m0.org_name_th = tex_TH_name.Text.Trim();
        _cen_org_detail_m0.org_name_en = tex_EN_name.Text.Trim();
        //_cen_org_detail_m0.org_status = dropD_status.SelectedValue;
        _cen_org_detail_m0.org_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_org_detail_m0.cemp_idx = _emp_idx;

        _data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServicePostMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));
        //string msg = _data_cen_master.return_msg;
        //litdebug.Text = msg;

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            gvMaster.Visible = true;
            setFormData(FormViewS, FormViewMode.Insert, null);
            FormViewS.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            ShowOrganization();

            //if (ViewState["forSearch"] == null)
            //{
            //    ShowOrganization();
            //}
            //else
            //{

            //    checkBoxSearch();
            //}


        }



    }


    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
        _cen_org_detail_m0.org_idx = cmdArg;
        _cen_org_detail_m0.org_status = 9;
        _cen_org_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "3";


        _data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //checkBoxSearch();

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);

        }
        else
        {
            gvMaster.Visible = true;
            lbCreate.Visible = true;
            FormViewS.Visible = true;
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            ShowOrganization();
        }
    }


    protected void checkBoxSearch()
    {
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_org_name = (string)ViewState["forWordSearch"];
        _search_cen_master_detail.s_org_idx = "";
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _data_cen_master.master_mode = "3";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        ShowOrganization();


    }


    protected void ShowOrganization()
    {
        TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
        //data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        //checkSearch = strRearch;
        //data_cen_master _data_cen_master = new data_cen_master();
        //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();
        //ViewState["forSearch"] = ToString();

        //_search_cen_master_detail.s_org_idx = "";

        //checkSearchnum = (string)ViewState["forSearch"];

        //if (ViewState["forSearch"] != null)
        //{
        //    checkSearchnum2 = searchbox.Text.Trim();
        //}

        //if (checkSearchnum == checkSearchnum2)
        //{
        //    _search_cen_master_detail.s_org_name = searchbox.Text.Trim();

        //}
        //if(checkSearchnum != checkSearchnum2 && ViewState["forSearch"] != null)
        //{
        //    _search_cen_master_detail.s_org_name = (string)ViewState["forSearch"];
        //}

        //else if (ViewState["forSearch"] == null)
        //{
        //    _search_cen_master_detail.s_org_name = "";
        //}

        if (ViewState["forSearch"] == null)
        {
            _search_cen_master_detail.s_org_name = "";
            _data_cen_master.master_mode = "3";
            //_cen_org_detail_m0.org_idx = 0;
            //_data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
            //_data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
            //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

            //ViewState["search"] = (data_cen_master)_data_cen_master;
            


            _functionTool.setGvData(gvMaster, _data_cen_master.cen_org_list_m0);
            //ViewState["forSearch"] = null;
        }
        else
        {
            
            _functionTool.setGvData(gvMaster, ((data_cen_master)ViewState["forSearch"]).cen_org_list_m0);
            //ViewState["forSearch"] = null;
        }


    }


    protected void getOrganization()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();

        //data_cen_master _data_cen_master = new data_cen_master();
        //cen_org_detail_m0 _cen_org_detail_m0 = new cen_org_detail_m0();

        _search_cen_master_detail.s_org_idx = "";
        _data_cen_master.master_mode = "3";
        //_cen_org_detail_m0.org_idx = 0;
        //_data_cen_master.cen_org_list_m0 = new cen_org_detail_m0[1];
        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        //_data_cen_master.cen_org_list_m0[0] = _cen_org_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _functionTool.setGvData(gvMaster, _data_cen_master.cen_org_list_m0);

    }


    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected data_cen_master callServicePostMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                //getOrganization();
                //searchOrganization("Search");
                ShowOrganization();


                break;

        }
        hlSetTotop.Focus();
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
}