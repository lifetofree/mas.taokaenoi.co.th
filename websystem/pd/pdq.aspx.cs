﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_pd_pdq : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    // bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        // check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0);
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //txt_document_nosearch.Text = string.Empty;
        //txt_sample_codesearch.Text = string.Empty;
        //txt_mat_search.Text = string.Empty;
        //txt_too_search.Text = string.Empty;
        //txt_customer_search.Text = string.Empty;
        //txt_date_search.Text = string.Empty;
        //txt_customer_search.Text = string.Empty;
        //txt_date_search.Text = string.Empty;

        switch (cmdName)
        {
            case "cmdDocSave":
                string[] cmdArgSave = cmdArg.Split(',');

                int actor_idx = int.TryParse(cmdArgSave[0].ToString(), out _default_int) ? int.Parse(cmdArgSave[0].ToString()) : _default_int;
                int node_idx = int.TryParse(cmdArgSave[1].ToString(), out _default_int) ? int.Parse(cmdArgSave[1].ToString()) : _default_int;
                int to_node_idx = int.TryParse(cmdArgSave[2].ToString(), out _default_int) ? int.Parse(cmdArgSave[2].ToString()) : _default_int;
                int to_actor_idx = int.TryParse(cmdArgSave[3].ToString(), out _default_int) ? int.Parse(cmdArgSave[3].ToString()) : _default_int;
                int decision_idx = int.TryParse(cmdArgSave[4].ToString(), out _default_int) ? int.Parse(cmdArgSave[4].ToString()) : _default_int;

                // switch action by node
                switch (node_idx)
                {
                    case 1:
                        setActiveTab("tab2", 0, 0);
                        break;
                    case 3:
                        switch (actor_idx)
                        {
                            case 1:
                                setActiveTab("tab1", to_node_idx, 91);
                                break;
                            case 2:
                            case 3:
                                if (to_node_idx == 4)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 3)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }

                                else if (to_node_idx == 8)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 1)
                                {
                                    setActiveTab("tab2", to_node_idx, decision_idx);
                                }
                                else
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }
                                break;
                            default:
                                setActiveTab("tab1", to_node_idx, decision_idx);
                                break;
                        }
                        break;
                    case 5:
                        switch (actor_idx)
                        {
                            case 3:
                                if (to_node_idx == 6 && decision_idx == 0)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                    //setActiveTab("tab5", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 6 && decision_idx == 21)
                                {
                                    setActiveTab("tab5", to_node_idx, decision_idx);
                                }
                                else if (to_node_idx == 6 && decision_idx == 1)
                                {
                                    setActiveTab("tab1", to_node_idx, decision_idx);
                                }

                                break;

                        }

                        break;
                    case 10:
                        if (to_node_idx == 12)
                        {
                            setActiveTab("tab2", to_node_idx, decision_idx);
                        }
                        else
                        {
                            setActiveTab("tab1", to_node_idx, decision_idx);
                        }

                        break;
                    case 4:
                    //case 5:
                    case 6:
                    case 8:
                    case 9:
                    //case 10:
                    case 11:
                        setActiveTab("tab1", to_node_idx, decision_idx);
                        break;
                    case 30:

                        break;
                }
                break;
            case "cmdDocCancel":
                setActiveTab("tab2", 0, 0);
                break;

            case "cmdSearchCreate":

                DropDownList ddlFormNo = (DropDownList)fvCreate.FindControl("ddlFormNo");
                Panel Panel_ShowFormFM001_119 = (Panel)fvCreate.FindControl("Panel_ShowFormFM001_119");
                Panel Panel_ShowFormFM001_123 = (Panel)fvCreate.FindControl("Panel_ShowFormFM001_123");
                Panel Panel_ShowFormFM_183 = (Panel)fvCreate.FindControl("Panel_ShowFormFM_183");
                Panel Panel_ShowFormFM_143 = (Panel)fvCreate.FindControl("Panel_ShowFormFM_143");

                Panel Panel_Save = (Panel)fvCreate.FindControl("Panel_Save");

                Panel_ShowFormFM001_119.Visible = false;
                Panel_ShowFormFM_183.Visible = false;
                Panel_ShowFormFM001_123.Visible = false;
                Panel_ShowFormFM_143.Visible = false;
                Panel_Save.Visible = false;


                switch (ddlFormNo.SelectedItem.ToString())
                {
                    case "FM-PD-PD-001/119":
                        Panel_ShowFormFM001_119.Visible = true;
                        Panel_Save.Visible = true;
                        break;
                    case "FM-PD-PD-001/183":
                        Panel_ShowFormFM_183.Visible = true;
                        Panel_Save.Visible = true;

                        break;

                    case "FM-PD-PD-001/123":

                        Panel_ShowFormFM001_123.Visible = true;
                        Panel_Save.Visible = true;
                        break;

                    case "FM-PD-PD-001/143":
                        Panel_ShowFormFM_143.Visible = true;
                        Panel_Save.Visible = true;
                        break;
                }



                break;


        }
    }
    #endregion event command

    #region Checkbox เชคแสดง
    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chk_selected_rdept":

                // actionSystemall();
                FormView fvDocDetail = (FormView)tab1.FindControl("fvDocDetail");
                CheckBox chk_selected_rdept = (CheckBox)fvDocDetail.FindControl("chk_selected_rdept");
                Panel show_selected_rdept = (Panel)fvDocDetail.FindControl("show_selected_rdept");

                if (chk_selected_rdept.Checked)
                {
                    show_selected_rdept.Visible = true;
                }
                else
                {
                    show_selected_rdept.Visible = false;
                }

                break;

            case "chk_other":

                FormView fvDocDetail_chkother = (FormView)tab1.FindControl("fvDocDetail");
                CheckBox chk_other = (CheckBox)fvDocDetail_chkother.FindControl("chk_other");

                DropDownList ddl_mat = (DropDownList)fvDocDetail_chkother.FindControl("ddl_mat");
                TextBox txt_mat_insert = (TextBox)fvDocDetail_chkother.FindControl("txt_mat_insert");

                if (chk_other.Checked)
                {
                    txt_mat_insert.Visible = true;
                    ddl_mat.Visible = false;
                }
                else
                {
                    txt_mat_insert.Visible = false;
                    ddl_mat.Visible = true;
                }

                break;
        }
    }
    #endregion

    #region RadioButton
    protected void RadioButton_CheckedChanged(object sender, System.EventArgs e)
    {
        var rd = (RadioButton)sender;
        switch (rd.ID)
        {

            case "selected_datail":

                //RadioButton selected_datail = (RadioButton)fvDocDetail.FindControl("selected_datail");
                //Panel show_detail_before = (Panel)fvDocDetail.FindControl("show_detail_before");
                //Panel show_alldetail_before1 = (Panel)fvDocDetail.FindControl("show_alldetail_before");
                //Panel show_insert_detail = (Panel)fvDocDetail.FindControl("show_insert_detail");
                //LinkButton btninsert_detail = (LinkButton)show_detail_before.FindControl("btninsert_detail");
                //Panel show_selecteddetail_ex = (Panel)fvDocDetail.FindControl("show_selecteddetail_ex");
                //Panel show_selecteddetail_all = (Panel)fvDocDetail.FindControl("show_selecteddetail_all");


                //if (selected_datail.Checked)
                //{
                //    show_detail_before.Visible = true;
                //    show_alldetail_before1.Visible = false;
                //    show_selecteddetail_all.Visible = false;
                //    //show_insert_detail.Visible = true;
                //}
                //else
                //{
                //    show_detail_before.Visible = false;
                //    show_alldetail_before1.Visible = false;
                //    show_selecteddetail_ex.Visible = false;
                //    //show_insert_detail.Visible = false;
                //}
                ////selected_datail.Focus();
                ////btninsert_detail.Focus();

                break;






        }
    }

    #endregion RadioButton

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;
        FormView fvCreate = (FormView)tab1.FindControl("fvCreate");
        DropDownList ddlOrderNo = (DropDownList)fvCreate.FindControl("ddlOrderNo");
        DropDownList ddlLineNo = (DropDownList)fvCreate.FindControl("ddlLineNo");
        DropDownList ddlRoomNo = (DropDownList)fvCreate.FindControl("ddlRoomNo");
        DropDownList ddlFormNo = (DropDownList)fvCreate.FindControl("ddlFormNo");

        switch (ddName.ID)
        {


            case "ddlOrderNo":

                if (ddlOrderNo.SelectedValue == "0")//Line ย่าง
                {
                    ddlLineNo.AppendDataBoundItems = true;
                    ddlLineNo.Items.Clear();
                    ddlLineNo.Items.Add(new ListItem("--- Line ---", "0"));

                    ddlRoomNo.AppendDataBoundItems = true;
                    ddlRoomNo.Items.Clear();
                    ddlRoomNo.Items.Add(new ListItem("--- Room ---", "0"));
                    //ddlRoomNo.Items.Add(new ListItem("Line ย่าง", "1"));

                }
                else
                {
                    ddlLineNo.AppendDataBoundItems = true;
                    ddlLineNo.Items.Clear();
                    ddlLineNo.Items.Add(new ListItem("--- Line ---", "0"));
                    ddlLineNo.Items.Add(new ListItem("Line ย่าง", "1"));
                    ddlLineNo.Items.Add(new ListItem("Line อบ", "2"));
                    ddlLineNo.Items.Add(new ListItem("Line ทอด", "3"));
                }


                break;

            case "ddlLineNo":
                if (ddlLineNo.SelectedValue == "1")//Line ย่าง
                {
                    ddlRoomNo.AppendDataBoundItems = true;
                    ddlRoomNo.Items.Clear();
                    ddlRoomNo.Items.Add(new ListItem("--- Room ---", "0"));
                    //ddlRoomNo.Items.Add(new ListItem("Line ย่าง", "1"));

                }
                else if (ddlLineNo.SelectedValue == "2")//Line อบ
                {
                    ddlRoomNo.AppendDataBoundItems = true;
                    ddlRoomNo.Items.Clear();
                    ddlRoomNo.Items.Add(new ListItem("--- Room ---", "0"));
                    ddlRoomNo.Items.Add(new ListItem("วัตถุดิบ", "1"));
                    ddlRoomNo.Items.Add(new ListItem("อบสาหร่าย", "2"));
                    ddlRoomNo.Items.Add(new ListItem("บรรจุโหล", "3"));
                }
                else if (ddlLineNo.SelectedValue == "3")//Line ทอด
                {
                    ddlRoomNo.AppendDataBoundItems = true;
                    ddlRoomNo.Items.Clear();
                    ddlRoomNo.Items.Add(new ListItem("--- Room ---", "0"));
                    ddlRoomNo.Items.Add(new ListItem("วัตถุดิบ", "1"));
                    ddlRoomNo.Items.Add(new ListItem("ทอด", "4"));
                    ddlRoomNo.Items.Add(new ListItem("โรยผงปรุง", "5"));
                }
                else
                {
                    ddlRoomNo.Items.Clear();
                    ddlRoomNo.Items.Insert(0, new ListItem("-- Room --", "0"));
                }

                break;
            case "ddlRoomNo":
                if (ddlLineNo.SelectedValue == "2" && ddlRoomNo.SelectedValue == "1")//Line อบ, Room วัตถุดิบ
                {
                    ddlFormNo.AppendDataBoundItems = true;
                    ddlFormNo.Items.Clear();
                    ddlFormNo.Items.Add(new ListItem("--- Form ---", "0"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/119", "1"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/183", "2"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/185", "3"));

                }
                else if (ddlLineNo.SelectedValue == "2" && ddlRoomNo.SelectedValue == "2")//Line อบ, Room อบสาหร่าย
                {
                    ddlFormNo.AppendDataBoundItems = true;
                    ddlFormNo.Items.Clear();
                    ddlFormNo.Items.Add(new ListItem("--- Form ---", "0"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/123", "4"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/124", "5"));

                }
                else if (ddlLineNo.SelectedValue == "2" && ddlRoomNo.SelectedValue == "3")//Line อบ, Room บรรจุโหล
                {
                    ddlFormNo.AppendDataBoundItems = true;
                    ddlFormNo.Items.Clear();
                    ddlFormNo.Items.Add(new ListItem("--- Form ---", "0"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/140", "5"));
                    ddlFormNo.Items.Add(new ListItem("FM-PD-PD-001/143", "6"));
                }
                else
                {
                    ddlFormNo.Items.Clear();
                    ddlFormNo.Items.Insert(0, new ListItem("-- Form --", "0"));
                }
                break;


        }
    }

    #endregion

    #region TextBoxIndexChanged
    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;
        //var searchingToolID = (TextBox)fvInsertLabCal.FindControl("searchingToolID");
        //var paneldetailTool = (Panel)fvInsertLabCal.FindControl("paneldetailTool");
        //var btnInsertDetail = (LinkButton)fvInsertLabCal.FindControl("btnInsertDetail");
        switch (txtName.ID)
        {
            case "txt_samplecode_result":

                //TextBox txt_samplecode_result = (TextBox)Fv_Insert_Result.FindControl("txt_samplecode_result");
                //Panel result_insert = (Panel)Fv_Insert_Result.FindControl("result_insert");

                //if (txt_samplecode_result.Text == "7G07001")
                //{

                //    result_insert.Visible = true;

                //}
                //else
                //{
                //    result_insert.Visible = false;


                //}

                break;



        }

    }
    #endregion

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("tab1", 0, 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int doc_decision)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        //tab create
        setFormData(fvCreate, FormViewMode.ReadOnly, null);

        // clear other formview, repeater
        //setFormData(fvDocDetail, FormViewMode.ReadOnly, null);

        //save_allprocess.Visible = false;  
        // clear other formview, repeater
        switch (activeTab)
        {
            case "tab1":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);


                switch (uidx)
                {
                    case 0:
                        //setFormData(fvDocDetail, FormViewMode.Insert, null);
                        setFormData(fvCreate, FormViewMode.Insert, null);

                        Panel Panel_ShowFormFM001_119 = (Panel)fvCreate.FindControl("Panel_ShowFormFM001_119");
                        Panel Panel_ShowFormFM_183 = (Panel)fvCreate.FindControl("Panel_ShowFormFM_183");
                        Panel Panel_ShowFormFM001_123 = (Panel)fvCreate.FindControl("Panel_ShowFormFM001_123");
                        Panel Panel_ShowFormFM_143 = (Panel)fvCreate.FindControl("Panel_ShowFormFM_143");

                        Panel Panel_Save = (Panel)fvCreate.FindControl("Panel_Save");

                        //Set Form FM
                        Panel_ShowFormFM001_119.Visible = false;
                        Panel_ShowFormFM_183.Visible = false;
                        Panel_ShowFormFM001_123.Visible = false;
                        Panel_ShowFormFM_143.Visible = false;
                        Panel_Save.Visible = false;


                        //ETFOCUS.Focus();
                        break;

                }
                break;

            case "tab3":

                setFormData(fvEmpLogin, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(fvApprove, FormViewMode.Insert, null);

                Panel Panel_ShowFormFM001_119_approve = (Panel)fvApprove.FindControl("Panel_ShowFormFM001_119");
                Panel Panel_ShowFormFM_183_approve = (Panel)fvApprove.FindControl("Panel_ShowFormFM_183");
                Panel Panel_ShowFormFM001_123_approve = (Panel)fvApprove.FindControl("Panel_ShowFormFM001_123");
                Panel Panel_ShowFormFM_143_approve = (Panel)fvApprove.FindControl("Panel_ShowFormFM_143");

                Panel Panel_Save_approve = (Panel)fvApprove.FindControl("Panel_Save");

                //Set Form FM
                Panel_ShowFormFM001_119_approve.Visible = false;
                Panel_ShowFormFM_183_approve.Visible = false;
                Panel_ShowFormFM001_123_approve.Visible = false;
                Panel_ShowFormFM_143_approve.Visible = false;
                Panel_Save_approve.Visible = false;

                break;
                //case "tab2":
                //    switch (uidx)
                //    {
                //        case 0:


                //            show_data.Visible = true;
                //            SETFOCUS.Focus();

                //            break;
                //        case 1:
                //            switch (doc_decision)
                //            {
                //                case 29:
                //                    show_data.Visible = true;
                //                    SETFOCUS.Focus();
                //                    break;
                //            }
                //            //if (txtsearch_index.Text == "QA600003")
                //            //{
                //            //    show_search_doc.Visible = true;
                //            //}
                //            //else if (txtsearch_index.Text == "7G07003")
                //            //{
                //            //    show_search_sample.Visible = true;
                //            //}
                //            //else
                //            //{
                //            //    show_data.Visible = true;
                //            //}
                //            //show_search_doc.Visible = true;

                //            break;

                //        case 12:
                //            switch (doc_decision)
                //            {

                //                case 12:

                //                    show_sucess.Visible = true;
                //                    SETFOCUS.Focus();
                //                    break;

                //            }

                //            break;
                //    }
                //    break;
                //case "tab3":
                //    switch (uidx)
                //    {
                //        case 0:
                //            switch (doc_decision)
                //            {
                //                // set default show  maseter data
                //                case 0:

                //                    master_meterail.Visible = true;
                //                    master_data_microbiological.Visible = false;
                //                    master_data_chemical.Visible = false;
                //                    break;
                //                case 1:
                //                    master_data_microbiological.Visible = true;
                //                    master_data_chemical.Visible = false;
                //                    master_meterail.Visible = false;
                //                    break;
                //                case 2:
                //                    master_data_chemical.Visible = true;
                //                    master_data_microbiological.Visible = false;
                //                    master_meterail.Visible = false;
                //                    break;

                //            }

                //            break;
                //    }

                //    break;
                //case "tab4":
                //    break;
                //case "tab5":
                //    switch (uidx)
                //    {
                //        case 0:
                //            setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //            break;
                //        case 6:
                //            switch (doc_decision)
                //            {
                //                case 21:
                //                    setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //                    break;
                //            }
                //            //txt_samplecode_result.Visible = string.Empty;
                //            break;
                //    }


                //    break;
                //case "tab6":
                //    setFormData(fv_supervisor_detail, FormViewMode.Insert, null);
                //    break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int doc_decision)
    {
        setActiveView(activeTab, uidx, doc_decision);
        switch (activeTab)
        {
            case "tab1":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                break;
            case "tab2":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                break;
            case "tab3":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");

                break;
                //case "tab3":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "active");
                //    li3.Attributes.Add("class", "");
                //    li4.Attributes.Add("class", "");
                //    li5.Attributes.Add("class", "");
                //    break;
                //case "tab4":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "");
                //    li3.Attributes.Add("class", "active");
                //    li4.Attributes.Add("class", "");
                //    li5.Attributes.Add("class", "");
                //    break;
                //case "tab5":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "");
                //    li3.Attributes.Add("class", "");
                //    li4.Attributes.Add("class", "active");
                //    li5.Attributes.Add("class", "");
                //    break;
                //case "tab6":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "");
                //    li3.Attributes.Add("class", "");
                //    li4.Attributes.Add("class", "");
                //    li5.Attributes.Add("class", "active");
                //    break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }
    #endregion reuse



}