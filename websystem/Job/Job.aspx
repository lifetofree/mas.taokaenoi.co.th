﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/job.master" AutoEventWireup="true" CodeFile="Job.aspx.cs" Inherits="WebSystem_Sir_TEST_Default3" Debug="true"
    MaintainScrollPositionOnPostback="true" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .hideMessage {
            display: none;
        }
    </style>

    <script>
        $(function () {
            var dp = $(".datepickerbasicLOK");
            dp.datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                minDate: moment(),
                maxDate: moment().add(3600, 'days'),
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                var dp = $(".datepickerbasicLOK");
                dp.datetimepicker({
                    useCurrent: false,
                    format: 'DD/MM/YYYY',
                    minDate: moment(),
                    maxDate: moment().add(3600, 'days'),
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.datepickerbasic').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                minDate: moment().add(-25200, 'days'),
                maxDate: moment(),
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.datepickerbasic').datetimepicker({
                    useCurrent: false,
                    format: 'DD/MM/YYYY',
                    minDate: moment().add(-25200, 'days'),
                    maxDate: moment(),
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.datepickerNo').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.datepickerNo').datetimepicker({
                    useCurrent: false,
                    format: 'DD/MM/YYYY',
                });
            });
        });

    </script>

    <style type="text/css">
        /*เส้น*/ ประชาชน hr.so_sweethr006 {
            border: 0;
            height: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        function addbox() {
            var b = document.getElementById('inputboxes');
            var el1 = document.createElement("br");
            var el2 = document.createElement("input");
            el2.name = 'f1[]';
            el2.type = 'text';
            el2.value = '';
            b.appendChild(el1);
            b.appendChild(el2);
        }
    </script>

    <script type="text/javascript">
        function autoTab(obj) {
            /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย 
            หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น รูปแบบเลขที่บัตรประชาชน 
            4-2215-54125-6-12 ก็สามารถกำหนดเป็น _-____-_____-_-__ 
            รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____ 
            หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__ 
            ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบรหัสสินค้า
            รหัสสินค้า 11-BRID-Y1207 
            */
            var pattern = new String("_-____-_____-__-_"); // กำหนดรูปแบบในนี้ 
            var pattern_ex = new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้ 
            var returnText = new String("");
            var obj_l = obj.value.length;
            var obj_l2 = obj_l - 1;
            for (i = 0; i < pattern.length; i++) {
                if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                    returnText += obj.value + pattern_ex;
                    obj.value = returnText;
                }
            }
            if (obj_l >= pattern.length) {
                obj.value = obj.value.substr(0, pattern.length);
            }
        }
    </script>
    <script type="text/javascript">
        function autoTab2(obj) {
            /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย 
            หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น รูปแบบเลขที่บัตรประชาชน 
            4-2215-54125-6-12 ก็สามารถกำหนดเป็น _-____-_____-_-__ 
            รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____ 
            หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__ 
            ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบรหัสสินค้า
            รหัสสินค้า 11-BRID-Y1207 
            */
            var pattern = new String("_.__"); // กำหนดรูปแบบในนี้ 
            var pattern_ex = new String("."); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้ 
            var returnText = new String("");
            var obj_l = obj.value.length;
            var obj_l2 = obj_l - 1;
            for (i = 0; i < pattern.length; i++) {
                if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                    returnText += obj.value + pattern_ex;
                    obj.value = returnText;
                }
            }
            if (obj_l >= pattern.length) {
                obj.value = obj.value.substr(0, pattern.length);
            }
        }
    </script>

    <asp:Literal ID="text" runat="server"></asp:Literal>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    <asp:Literal ID="test" runat="server"></asp:Literal>
    <asp:Literal ID="Literal2" runat="server"></asp:Literal>
    <div class="container">
        <asp:MultiView ID="MvtestMaster" runat="server" ActiveViewIndex="0">
            <asp:View ID="ViewAdd1" runat="server">
                <asp:Panel ID="ChLink" runat="server" Visible="true">
                    <div class="col-md-12" role="main" style="margin-top: -2%;">
                        <h4 class="list-group-item col-md-4">ตำแหน่งงานที่ต้องการ <small>Job Requirements</small></h4>
                        <asp:FormView ID="fvExample" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="true">
                                    <ContentTemplate>

                                        <div class="form-horizontal" role="form">

                                            <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblposition1" runat="server" ForeColor="DarkBlue" Text="ตำแหน่งที่ 1"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Position applied for</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="tbposition1" runat="server" CssClass="form-control" AutoPostBack="true" />
                                                        <asp:RequiredFieldValidator ID="page01"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbposition1"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกตำงานที่ต้องการสมัคร"
                                                            ValidationExpression="เลือกตำแหน่งงานที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblposition2" runat="server" ForeColor="DarkBlue" Text="ตำแหน่งที่ 2"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Position applied for</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="tbposition2" runat="server" CssClass="form-control" placeholder="ตำแหน่งที่ 2" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                            ValidationGroup="formInsert"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbposition2"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกตำงานที่ต้องการสมัคร"
                                                            ValidationExpression="เลือกตำแหน่งงานที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblmoney" runat="server" ForeColor="DarkBlue" Text="เงินเดือนที่ต้องการ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Expected starting salary</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-5">
                                                        <asp:TextBox ID="tbmoney" runat="server" AutoPostBack="true" OnTextChanged="TextBox1_TextChanged" CssClass="form-control" placeholder="เงินเดือนที่ต้องการ" MaxLength="6" />
                                                        <asp:RequiredFieldValidator ID="moneyWant" ValidationGroup="formInsert" runat="server" Display="Dynamic"
                                                            ControlToValidate="tbmoney" Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกเงินเดือน"
                                                            SetFocusOnError="true"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="moneyWantNum" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น/ กรอกจำนวนเงินไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbmoney"
                                                            ValidationExpression="[0-9,]{1,6}"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: -3%;">
                                                        <h5>บาท</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lbldate" runat="server" ForeColor="DarkBlue" Text="วันที่พร้อมปฏิบัติงาน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Start working</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <div class='input-group date datepickerbasicLOK'>
                                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="formInsert" runat="server" Display="Dynamic"
                                                            ControlToValidate="AddStartdate" Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกวันที่"
                                                            ValidationExpression="Please check Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                                <div class="col-md-7 text-right">
                                                    <asp:LinkButton ID="btadd" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" ValidationGroup="formInsert" CommandName="Cmdnext" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btadd" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </asp:Panel>
                <asp:Panel ID="EndAge" runat="server" Visible="false">
                    <div class="form-group text-center">
                        <div class="col-md-12">
                            <b>
                                <h2>
                                    <asp:Label ID="End" runat="server" Text="ขออภัยลิงก์ของท่านได้หมดอายุในการเข้าใช้งานแล้ว"></asp:Label></h2>
                            </b>
                            <h4>
                                <asp:Label ID="Label49" runat="server" Text="กรุณาติดต่อกลับหากเกิดปัญหาหรือมีข้อสงสัยในการสมัคร"></asp:Label></h4>
                        </div>
                    </div>
                </asp:Panel>
            </asp:View>
            ส

            <asp:View ID="ViewAdd2" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <%--<div class="bs-docs-example2" data-content="ประวัติส่วนตัว">--%>
                    <h4 class="list-group-item col-md-4">ประวัติส่วนตัว <small>Profile</small></h4>
                    <%--<h4 class="list-group-item col-md-3">ประวัติส่วนตัว</h4>--%>
                    <asp:FormView ID="FormView1" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional"
                                ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label29" runat="server" ForeColor="DarkBlue" Text="คำนำหน้าชื่อ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Prefix</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddPrefix" runat="server" OnSelectedIndexChanged="selectchang_dd" AutoPostBack="true" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddPrefixAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddPrefix"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกคำนำหน้าชื่อ"
                                                        ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblname" runat="server" ForeColor="DarkBlue" Text="ชื่อ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">(ชื่อภาษาไทย)</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbname" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tbnameAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbname"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbname"
                                                        ValidationExpression="[ก-ฮะ-์]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lbllastname" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">(นามสกุลภาษาไทย)</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tblastname" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tblastnameAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tblastname"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tblastname"
                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label37" runat="server" ForeColor="DarkBlue" Text="ชื่อเล่น"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">(ชื่อเล่นภาษาไทย)</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbNickname" runat="server" CssClass="form-control" placeholder="ชื่อเล่น" MaxLength="20" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbNickname"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อเล่น(ไทย)"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbNickname"
                                                        ValidationExpression="[ก-ฮะ-์]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label30" runat="server" ForeColor="DarkBlue" Text="Prefix"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text"></p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddPrefixEng" OnSelectedIndexChanged="selectchang_dd" AutoPostBack="true" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddPrefixEngAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddPrefixEng"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกคำนำหน้าชื่อ"
                                                        ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblnameeng" runat="server" ForeColor="DarkBlue" Text="First name"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text"></p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbnameeng" runat="server" CssClass="form-control" placeholder="First name" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tbnameengAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbnameeng"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* Please check First name"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาอังกฤษเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbnameeng"
                                                        ValidationExpression="^[A-Za-z]+$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lbllastnameeng" runat="server" ForeColor="DarkBlue" Text="Last name"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text"></p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tblastnameeng" runat="server" CssClass="form-control" placeholder="Last name" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tblastnameengAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tblastnameeng"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* Please check Last name"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาอังกฤษเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tblastnameeng"
                                                        ValidationExpression="^[A-Za-z ]+$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label1" runat="server" ForeColor="DarkBlue" Text="Nick Name"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text"></p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbNickEng" runat="server" CssClass="form-control" placeholder="Nick Name" MaxLength="20" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbNickEng"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อเล่น(อังกฤษ)"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator37" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาอังกฤษเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbNickEng"
                                                        ValidationExpression="[a-zA-Z]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblbirthday" runat="server" ForeColor="DarkBlue" Text="วัน/เดือน/ปีเกิด"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Date of birth</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class='input-group date datepickerbasic'>

                                                        <asp:TextBox ID="Addbirthday" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="AddbirthdayAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="Addbirthday"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกวันเกิด"
                                                        ValidationExpression="Please check Name" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lbllocationstart" runat="server" ForeColor="DarkBlue" Text="จังหวัดที่เกิด"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Birth province</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddlocationstart" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddlocationstartAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlocationstart"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกจังหวัด"
                                                        ValidationExpression="กรุณาเลือกจังหวัด" InitialValue="00" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label31" runat="server" ForeColor="DarkBlue" Text="ประเทศ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Country</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddCountry" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddCountryAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddCountry"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกประเทศ"
                                                        ValidationExpression="กรุณาเลือกประเทศ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label25" runat="server" ForeColor="DarkBlue" Text="จังหวัดที่อยู่ปัจจุบัน"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">province</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddlocationHere" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddlocationHereAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlocationHere"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกจังหวัด"
                                                        ValidationExpression="กรุณาเลือกจังหวัด" InitialValue="00" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label26" runat="server" ForeColor="DarkBlue" Text="อำเภอ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Amphur</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddAmphur" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddAmphurAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddAmphur"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกอำเภอ"
                                                        ValidationExpression="กรุณาเลือกอำเภอ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label27" runat="server" ForeColor="DarkBlue" Text="ตำบล"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">District</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddDistrict" runat="server" CssClass="form-control fa-align-left" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddDistrictAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddDistrict"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกตำบล"
                                                        ValidationExpression="กรุณาเลือกตำบล" InitialValue="00" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label28" runat="server" ForeColor="DarkBlue" Text="รหัสไปรษณีย์"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Postcode</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="Postcode" runat="server" CssClass="form-control fa-align-left" MaxLength="5"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="PostcodeAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="Postcode"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกรหัสไปรษณีย์"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="PostcodeALNum" runat="server"
                                                        ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="Postcode"
                                                        ValidationExpression="^[0-9]{5,5}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblnationality" runat="server" ForeColor="DarkBlue" Text="สัญชาติ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Nationality</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddnationality" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddnationalityAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddnationality"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกสัญชาติ"
                                                        ValidationExpression="กรุณาเลือกสัญชาติ" InitialValue="00" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblrace" runat="server" ForeColor="DarkBlue" Text="เชื้อชาติ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Race</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddrace" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddraceAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddrace"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกเชื้อชาติ"
                                                        ValidationExpression="กรุณาเลือกเชื้อชาติ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblsex" runat="server" ForeColor="DarkBlue" Text="เพศ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Sex</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ddsex" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang_dd"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddsexAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddsex"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกเพศ"
                                                        ValidationExpression="กรุณาเลือกเพศ" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblweight" runat="server" ForeColor="DarkBlue" Text="น้ำหนัก"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Weight</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbweight" runat="server" CssClass="form-control fa-align-left" placeholder="น้ำหนัก" MaxLength="3"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="tbweightAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbweight"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกน้ำหนัก"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="tbweightALNum" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกน้ำหนักไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbweight"
                                                        ValidationExpression="^[0-9]{2,3}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                                <div class="col-md-2" style="margin-left: -3%;">
                                                    <h5>กก.</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label32" runat="server" ForeColor="DarkBlue" Text="ส่วนสูง"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Height</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbheight" runat="server" CssClass="form-control" placeholder="ส่วนสูง" MaxLength="3" />
                                                    <asp:RequiredFieldValidator ID="tbheightAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbheight"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกส่วนสูง"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="tbheightNum" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกส่วนสูงไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbheight"
                                                        ValidationExpression="^[0-9]{3,3}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                                <div class="col-md-2" style="margin-left: -3%;">
                                                    <h5>ซม.</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblscar" runat="server" ForeColor="DarkBlue" Text="ตำหนิ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Scar</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbscar" runat="server" CssClass="form-control" placeholder="ตำหนิ" MaxLength="30" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbscar"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกตำหนิหากไม่มีให้ใส่ไม่มี/ -"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorScar" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ - เท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbscar"
                                                        ValidationExpression="[0-9ก-ฮะ-์- ]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label33" runat="server" ForeColor="DarkBlue" Text="กรุ๊ปเลือด"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Blood group</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:DropDownList ID="ddblood" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="ddbloodAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddblood"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกกรุ๊ปเลือด"
                                                        ValidationExpression="กรุณาเลือกกรุ๊ปเลือด" InitialValue="00" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblreligon" runat="server" ForeColor="DarkBlue" Text="ศาสนา"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Religon</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:DropDownList ID="ddreligon" runat="server" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="ddreligonAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddreligon"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกศาสนา"
                                                        ValidationExpression="กรุณาเลือกศาสนา" InitialValue="00" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-9">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblnumber" runat="server" ForeColor="DarkBlue" Text="เบอร์โทรศัพท์(มือถือ)"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Mobie</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbnumberphone" runat="server" CssClass="form-control" placeholder="เบอร์โทรศัพท์(มือถือ)" MaxLength="10" />
                                                    <asp:RequiredFieldValidator ID="tbnumberphoneAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbnumberphone"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกเบอร์โทรศัพท์มือถือ"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="tbscarNum" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbnumberphone"
                                                        ValidationExpression="^[0-9]{10,10}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-9">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblnumberhome" runat="server" ForeColor="DarkBlue" Text="เบอร์โทรศัพท์(บ้าน)"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Telephone</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbnumberhome" runat="server" CssClass="form-control" placeholder="เบอร์โทรศัพท์(บ้าน)" MaxLength="9" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                        ErrorMessage="&nbsp; &nbsp; &nbsp;*เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbnumberhome"
                                                        ValidationExpression="^[0-9]{9,9}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-9">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="Label20" runat="server" ForeColor="DarkBlue" Text="อีเมล์"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Email</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbemail" runat="server" CssClass="form-control" placeholder="Email" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbemail"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอก Email"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator31" runat="server"
                                                        ErrorMessage="&nbsp;* กรุณรากรอก E-mail ให้ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbemail"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-12">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbladdresspre" runat="server" ForeColor="DarkBlue" Text="ที่อยู่ปัจจุบันที่ติดต่อได้สะดวก"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Present address</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox TextMode="MultiLine" ID="tbaddresspre" runat="server" CssClass="form-control" placeholder="ที่อยู่ปัจจุบัน" MaxLength="500" Height="90" />
                                                    <asp:RequiredFieldValidator ID="tbaddresspreAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbaddresspre"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกที่อยู่ปัจจุบัน"
                                                        ValidationExpression="Please check Name" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-12">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lblperaddress" runat="server" ForeColor="DarkBlue" Text="ที่อยู่ตามทะเบียนบ้าน"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Permanent address</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox TextMode="MultiLine" ID="tbperaddress" runat="server" CssClass="form-control" placeholder="ที่อยู่ตามทะเบียนบ้าน" MaxLength="500" Height="90" />
                                                    <asp:RequiredFieldValidator ID="tbperaddressAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbperaddress"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกที่อยู่ตามทะเบียนบ้าน"
                                                        ValidationExpression="Please check Name" />
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblidcard" runat="server" ForeColor="DarkBlue" Text="บัตรประชาชน"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">ID.Card No.</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbidcard" runat="server" CssClass="form-control" AutoPostBack="true" onkeypress="autoTab(this)" placeholder="บัตรประชาชน" MaxLength="17" OnTextChanged="TextBox1_TextChanged" />
                                                    <asp:RequiredFieldValidator ID="tbidcardAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbidcard"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกเลขบัตรประชาชน"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbidcard"
                                                        ValidationExpression="^[0-9-]{17,17}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblissued_at" runat="server" ForeColor="DarkBlue" Text="ออกให้ ณ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Issued at</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbissued_at" runat="server" CssClass="form-control" placeholder="ออกให้ ณ" MaxLength="60" />
                                                    <asp:RequiredFieldValidator ID="tbissued_atAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbissued_at"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกออก ณ ที่"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbissued_at"
                                                        ValidationExpression="[ก-ฮะ-์. ]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblissued_date" runat="server" ForeColor="DarkBlue" Text="วันที่ออกบัตร"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Issued date</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class='input-group date datepickerbasic'>

                                                        <asp:TextBox ID="tbissued_date" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="tbissued_dateAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbissued_date"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกวันที่ออกบัตร"
                                                        ValidationExpression="Please check Name" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblexpired_date" runat="server" ForeColor="DarkBlue" Text="บัตรหมดอายุ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Expired date</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class='input-group date datepickerbasicLOK'>
                                                        <asp:TextBox ID="tbexpired_date" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="tbexpired_dateAL"
                                                        ValidationGroup="formInsert1" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbexpired_date"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกวันที่หมอายุ"
                                                        ValidationExpression="Please check Name" />
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lblsocialcard" runat="server" ForeColor="DarkBlue" Text="บัตรรับรองสิทธิประกันสังคมเลขที่"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Social.Securrity Card.No</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbsocialcard" runat="server" CssClass="form-control" placeholder="บัตรรับรองสิทธิประกันสังคมเลขที่" MaxLength="13" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbsocialcard"
                                                        ValidationExpression="[0-9-]{1,13}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblissued_atsocial" runat="server" ForeColor="DarkBlue" Text="ออกให้ ณ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Issued at</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbissued_atsocial" runat="server" CssClass="form-control" placeholder="ออกให้ ณ" MaxLength="100" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp; &nbsp; &nbsp;*เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbissued_atsocial"
                                                        ValidationExpression="[ก-ฮะ-์-. ]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblexpired_date_social" runat="server" ForeColor="DarkBlue" Text="บัตรหมดอายุ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Expired date</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class='input-group date datepickerbasicLOK'>
                                                        <asp:TextBox ID="tbexpired_date_social" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbltaxcard" runat="server" ForeColor="DarkBlue" Text="บัตรประจำตัวผู้เสียภาษีเลขที่"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Tax ID.Card No.</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbtaxcard" runat="server" CssClass="form-control" placeholder="บัตรประจำตัวผู้เสียภาษีเลขที่" MaxLength="13" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                        ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbtaxcard"
                                                        ValidationExpression="^[0-9-]{1,13}$"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblissued_tax" runat="server" ForeColor="DarkBlue" Text="ออกให้ ณ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Issued at</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbissued_tax" runat="server" CssClass="form-control" placeholder="ออกให้ ณ" MaxLength="100" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp; &nbsp; &nbsp;*เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbissued_tax"
                                                        ValidationExpression="[ก-ฮะ-์-]{1,}"
                                                        ValidationGroup="formInsert1" />
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <label class="col-md-2 list-group-item-heading">
                                                    <asp:Label ID="lbllive" runat="server" ForeColor="DarkBlue" Text="สถานะความเป็นอยู่"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Living status</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="Living_status" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;บ้านส่วนตัว (Own home)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;บ้านเช่า (Rent home)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;อาศัยอยู่กับบิดามารดา (Live with Parents)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="Living_statusAL"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="Living_status"
                                                        Font-Size="11" ForeColor="Red" CssClass=""
                                                        ErrorMessage="* กรุณาเลือกสถานะความเป็นอยู่"
                                                        ValidationExpression="กรุณาเลือกสถานะความเป็นอยู่" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-12">
                                                <label class="col-md-2 list-group-item-heading">
                                                    <asp:Label ID="lblmarital" runat="server" ForeColor="DarkBlue" Text="สถานะครอบครัว"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Marital Status</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="Marital_Status" runat="server" OnSelectedIndexChanged="chkSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="1" Text="&nbsp;โสด (Single)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;แต่งงาน (Married)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;หย่า (Live with Parents)"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="&nbsp;หม้าย (Widowed)"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="&nbsp;แยกกันอยู่ (Separated)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="formInsert1"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="Marital_Status"
                                                        Font-Size="11" ForeColor="Red"
                                                        CssClass=""
                                                        ErrorMessage="* กรุณาเลือกสถานะครอบครัว"
                                                        ValidationExpression="กรุณาเลือกสถานะครอบครัว" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="ifmarried" runat="server" OnCheckedChanged="chkSelectedIndexChanged" Visible="false" AutoPostBack="true">
                                            <hr class="so_sweethr006" />
                                            <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                                <div class="col-md-12">
                                                    <label class="col-md-2 list-group-item-heading">
                                                        <asp:Label ID="lblifmarried" runat="server" ForeColor="DarkBlue" Text="กรณีแต่งงาน"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">If Married</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:RadioButtonList ID="If_Married" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;จดทะเบียนสมรส (Registered)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ไม่ได้จดทะเบียนสมรส (Non Registered)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-12">
                                                    <label class="col-md-2 list-group-item-heading">
                                                        <asp:Label ID="lblspouseincome" runat="server" ForeColor="DarkBlue" Text="คู่สมรสมีเงินได้หรือไม่"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">or not is the spouse has income?</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:RadioButtonList ID="spouseincome" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;มี (Yes)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ไม่มี (No)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lblnamespouse" runat="server" ForeColor="DarkBlue" Text="ชื่อคู่สมรส"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Name</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbnamespouse" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                                            ValidationGroup="formInsert1" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbnamespouse"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbnamespouse"
                                                            ValidationExpression="[ก-ฮะ-์]{1,}"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lbllastnamespouse" runat="server" ForeColor="DarkBlue" Text="นามสกุลคู่สมรส"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Lastname</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tblastnamespouse" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                                                            ValidationGroup="formInsert1" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tblastnamespouse"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator35" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tblastnamespouse"
                                                            ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lbloccupation" runat="server" ForeColor="DarkBlue" Text="อาชีพ"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Occupation</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tboccupation" runat="server" CssClass="form-control" placeholder="อาชีพ" MaxLength="30" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator36" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tboccupation"
                                                            ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lblfirmaddress" runat="server" ForeColor="DarkBlue" Text="สถานที่ทำงาน"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Firm Address</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbfirmaddress" runat="server" CssClass="form-control" placeholder="สถานที่ทำงาน" MaxLength="500" />
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="so_sweethr006" style="margin-bottom: 3%;" />
                                        </asp:Panel>
                                        <asp:Panel ID="Chind" runat="server" Visible="true">
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-6 list-group-item-heading">
                                                        <asp:Label ID="lblchildren" runat="server" ForeColor="DarkBlue" Text="จำนวนบุตร"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">No. of Children</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbchildren" runat="server" CssClass="form-control fa-align-left" placeholder="จำนวนบุตร" MaxLength="2"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server"
                                                            ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbchildren"
                                                            ValidationExpression="^[0-9-]{1,2}$"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: -3%;">
                                                        <h5>คน</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-6 list-group-item-heading">
                                                        <asp:Label ID="lblchildrenschool" runat="server" ForeColor="DarkBlue" Text="จำนวนบุตรที่กำลังศึกษา"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Children of School</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbchildrenschool" runat="server" CssClass="form-control fa-align-left" placeholder="จำนวนบุตร" MaxLength="2"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server"
                                                            ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbchildrenschool"
                                                            ValidationExpression="^[0-9-]{1,2}$"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: -3%;">
                                                        <h5>คน</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-6 list-group-item-heading">
                                                        <asp:Label ID="lblchildren21" runat="server" ForeColor="DarkBlue" Text="จำนวนบุตรอายุเกิน 21 ปี"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Children over 21 years</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbchildren21" runat="server" CssClass="form-control fa-align-left" placeholder="จำนวนบุตร" MaxLength="2"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator34" runat="server"
                                                            ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbchildren21"
                                                            ValidationExpression="^[0-9-]{1,2}$"
                                                            ValidationGroup="formInsert1" />
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: -3%;">
                                                        <h5>คน</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <hr class="so_sweethr006" />
                                        <asp:Panel ID="hideMilitary_Service" runat="server">
                                            <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                                <div class="col-md-12">
                                                    <label class="col-md-2 list-group-item-heading">
                                                        <asp:Label ID="lblmilitary" runat="server" ForeColor="DarkBlue" Text="สถานะทางทหาร"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Military Service</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:RadioButtonList ID="Military_Service" runat="server" OnSelectedIndexChanged="chkSelectedIndexChanged">
                                                            <asp:ListItem Value="1" Text="&nbsp;ผ่านการเกณฑ์ทหาร (Excepted)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;เรียนรักษาดินแดน (Military Studied)"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="&nbsp;ได้รับข้อยกเว้น (Discharged)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                            ValidationGroup="formInsert1"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="Military_Service"
                                                            Font-Size="11" ForeColor="Red"
                                                            CssClass=""
                                                            ErrorMessage="* กรุณาเลือกสถานะทางทหาร"
                                                            ValidationExpression="กรุณาเลือกสถานะทางทหาร" />
                                                    </div>
                                                </div>
                                            </div>
                                            <%--<div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-7">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lblMilitaryother" runat="server" ForeColor="DarkBlue" Text="อื่นๆ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Other</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbMilitaryother" runat="server" CssClass="form-control" placeholder="อื่นๆ"  OnTextChanged="TextChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>--%>
                                        </asp:Panel>

                                        <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                            <div class="col-md-6 text-right">
                                                <asp:LinkButton ID="btbackadd1" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd1" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                            </div>

                                            <div class="col-md-6 text-left">
                                                <asp:LinkButton ID="btadd1" CssClass="btn btn-primary" runat="server" ValidationGroup="formInsert1" OnClientClick="javascript:scroll(0,0);" CommandName="Cmdnext1" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i><a href="#Focus"></a></asp:LinkButton><%--ValidationGroup="formInsert1"--%>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btadd1" />
                                    <asp:PostBackTrigger ControlID="btbackadd1" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd3" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <h4 class="list-group-item col-md-4">ประวัติครอบครัว <small>Family history</small></h4>
                    <%--<div class="bs-docs-example2" data-content="ประวัติครอบครัว">--%>
                    <%--<h4 class="list-group-item" style="width: 20%;">ประวัติครอบครัว</h4>--%>
                    <asp:FormView ID="FormView2" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>
                            <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional"
                                ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblnamefather" runat="server" ForeColor="DarkBlue" Text="ชื่อบิดา"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Name of Father</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbnamefather" runat="server" CssClass="form-control" placeholder="ชื่อบิดา" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tbnamefather_AL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbnamefather"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อบิดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator133" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbnamefather"
                                                        ValidationExpression="[ก-ฮะ-์]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbllastnamefather" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Lastname</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tblastnamefather" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tblastnamefather_AL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tblastnamefather"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกนามสกุลบิดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tblastnamefather"
                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblage" runat="server" ForeColor="DarkBlue" Text="อายุ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Age</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddage" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-2" style="margin-left: -3%;">
                                                    <h5>ปี</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbloccupationfather" runat="server" ForeColor="DarkBlue" Text="อาชีพ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Occupation</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tboccupationfather" runat="server" CssClass="form-control" placeholder="อาชีพ" MaxLength="30" />
                                                    <asp:RequiredFieldValidator ID="tboccupationfatherAL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tboccupationfather"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกอาชีพบิดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator155" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tboccupationfather"
                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblstatuslift" runat="server" ForeColor="DarkBlue" Text="สถานะ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Status</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:RadioButtonList ID="statuslift" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;มีชีวิต (Alive)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ถึงแก่กรรม (Passed away)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="statusliftAL"
                                                        ValidationGroup="formInsert2"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="statuslift"
                                                        Font-Size="11" ForeColor="Red"
                                                        CssClass=""
                                                        ErrorMessage="&nbsp;* กรุณาเลือกสถานะ"
                                                        ValidationExpression="กรุณาเลือกสถานะ" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="namemother" runat="server" ForeColor="DarkBlue" Text="ชื่อมารดา"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Name of Mother</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbnamemother" runat="server" CssClass="form-control" placeholder="ชื่อมารดา" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tbnamemotherAL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tbnamemother"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อมารดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tbnamemother"
                                                        ValidationExpression="[ก-ฮะ-์]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbllastnamemother" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Lastname</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tblastnamemother" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                    <asp:RequiredFieldValidator ID="tblastnamemotherAL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tblastnamemother"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกนามสกุลมารดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tblastnamemother"
                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblagemother" runat="server" ForeColor="DarkBlue" Text="อายุ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Age</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddahemother" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-2" style="margin-left: -3%;">
                                                    <h5>ปี</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-3 list-group-item-heading">
                                                    <asp:Label ID="lbloccupationmother" runat="server" ForeColor="DarkBlue" Text="อาชีพ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Occupation</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tboccupationmother" runat="server" CssClass="form-control" placeholder="อาชีพ" MaxLength="30" />
                                                    <asp:RequiredFieldValidator ID="tboccupationmotherAL"
                                                        ValidationGroup="formInsert2" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="tboccupationmother"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณากรอกอาชีพมารดา"
                                                        ValidationExpression="Please check Name" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        ControlToValidate="tboccupationmother"
                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                        ValidationGroup="formInsert2" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lblstatusmother" runat="server" ForeColor="DarkBlue" Text="สถานะ"></asp:Label>
                                                    <span style="color: red;"><b>*</b></span>
                                                    <small>
                                                        <p class="list-group-item-text">Status</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-6">
                                                    <asp:RadioButtonList ID="statusmother" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;มีชีวิต (Alive)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ถึงแก่กรรม (Passed away)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="statusmotherAL"
                                                        ValidationGroup="formInsert2"
                                                        runat="server" Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="statusmother"
                                                        Font-Size="11" ForeColor="Red"
                                                        CssClass=""
                                                        ErrorMessage="&nbsp;* กรุณาเลือกสถานะ"
                                                        ValidationExpression="กรุณาเลือกสถานะ" />
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <h4 class="list-group-item col-md-4">ชื่อ-นามสกุลพี่น้องร่วมบิดามารดาเดียวกัน <small>Name of the relatives shares parents</small></h4>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 5%; margin-top: 3%;">
                                            <asp:CheckBox ID="hide_parent" runat="server" OnCheckedChanged="Checkbox" AutoPostBack="true" />
                                            ไม่มีพี่น้อง
                                        </div>
                                        <asp:Panel ID="hide_parent_1" runat="server" Visible="true">
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblnameparent1" runat="server" ForeColor="DarkBlue" Text="ชื่อ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Name</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbnameparent1" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="datamore11"
                                                            ValidationGroup="more1" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbnameparent1"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbnameparent1"
                                                            ValidationExpression="[ก-ฮะ-์]{1,}"
                                                            ValidationGroup="more1" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lbllastnameparent1" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Lastname</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tblastnameparent1" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="40" />
                                                        <asp:RequiredFieldValidator ID="datamore12"
                                                            ValidationGroup="more1" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tblastnameparent1"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tblastnameparent1"
                                                            ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                            ValidationGroup="more1" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblageparent1" runat="server" ForeColor="DarkBlue" Text="อายุ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Age</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddageparent1" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="datamore13"
                                                            ValidationGroup="more1"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddageparent1"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกอายุ"
                                                            ValidationExpression="เลือกอายุ" InitialValue="00" />
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: -3%;">
                                                        <h5>ปี</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-4 list-group-item-heading">
                                                        <asp:Label ID="lbloccupationparent1" runat="server" ForeColor="DarkBlue" Text="อาชีพ"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Occupation</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tboccupationparent1" runat="server" CssClass="form-control" placeholder="อาชีพ" MaxLength="30" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* เฉพาะภาษาไทย/อังกฤษเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tboccupationparent1"
                                                            ValidationExpression="[ก-ฮะ-์a-zA-Z- ]{1,}"
                                                            ValidationGroup="more1" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblfirmaddressparent1" runat="server" ForeColor="DarkBlue" Text="สถานที่ทำงาน"></asp:Label>
                                                        <small>
                                                            <p id="test" class="list-group-item-text" style="visibility: hidden">Firm Address</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbfirmaddressparent1" runat="server" CssClass="form-control" placeholder="สถานที่ทำงาน" MaxLength="500" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblnumberparent1" runat="server" ForeColor="DarkBlue" Text="เบอร์โทรศัพท์"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Telephone</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbnumberparent1" runat="server" CssClass="form-control" placeholder="เบอร์โทรศัพท์" MaxLength="10" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator100" runat="server"
                                                            ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbnumberparent1"
                                                            ValidationExpression="^[0-9-]{9,10}$"
                                                            ValidationGroup="more1" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <div class="col-md-5">
                                                        <asp:LinkButton ID="btmore1" runat="server" ValidationGroup="more1" CssClass="btn btn-success" CommandName="more1" OnCommand="btnCommand" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"> เพิ่ม</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>


                                    <asp:Panel ID="hide_parent_2" runat="server" Visible="true">
                                        <div class="col-md-12" style="margin-top: 3%; margin-left: 0.5%">
                                            <div>
                                                <h4>สรุปรายการ<small> (List Brethren)</small></h4>
                                            </div>
                                            <div id="ViewModelReportAddparents1" visible="True" runat="server">
                                                <asp:GridView ID="GvReportAddparent"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover"
                                                    HeaderStyle-CssClass="table_headCenter"
                                                    HeaderStyle-Height="40px"
                                                    ShowFooter="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    AllowPaging="True"
                                                    PageSize="5"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    OnRowEditing="Gvbuyequipment_Editing"
                                                    OnRowUpdating="Gvbuyequipment_RowUpdating"
                                                    OnRowDataBound="Gvbuyequipment_RowDataBound"
                                                    OnRowDeleting="Gvbuyequipment_RowDeleting"
                                                    OnRowCancelingEdit="Gvbuyequipment_RowCancelingEdit">

                                                    <PagerStyle CssClass="PageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </div>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-12">
                                                                                <h3>Brethren FormEdit<small> (แบบฟอร์มแก้ไขรายการพี่น้อง)</small></h3>
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label19" runat="server" Text="ชื่อ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <asp:TextBox ID="tbnameparent1_edit" class="form-control" runat="server" Text='<%# Eval("tbnameparent1") %>' MaxLength="40" />
                                                                                    <asp:RequiredFieldValidator ID="potetparentedit"
                                                                                        ValidationGroup="formbrethren" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbnameparent1_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                                                        ValidationExpression="Please check Name" />
                                                                                    <asp:RegularExpressionValidator ID="potetparentedit1" runat="server"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        ControlToValidate="tbnameparent1_edit"
                                                                                        ValidationExpression="[ก-ฮะ-์]{1,}"
                                                                                        ValidationGroup="formbrethren" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label6" runat="server" Text="นามสกุล"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <asp:TextBox ID="tblastnameparent1_edit" class="form-control" runat="server" Text='<%# Eval("tblastnameparent1") %>' MaxLength="40" />
                                                                                    <asp:RequiredFieldValidator ID="tblastnameAL"
                                                                                        ValidationGroup="formbrethren" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tblastnameparent1_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                                                        ValidationExpression="Please check Name" />
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        ControlToValidate="tblastnameparent1_edit"
                                                                                        ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                                                        ValidationGroup="formbrethren" />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label7" runat="server" Text="อายุ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-4">
                                                                                    <asp:DropDownList ID="ddageparent1_edit" class="form-control" runat="server" />
                                                                                    <asp:HiddenField ID="ddageparent1_edit_" runat="server" Value='<%# Eval("ddageparent1") %>' />
                                                                                    <asp:RequiredFieldValidator ID="AddbirthdayAL2"
                                                                                        ValidationGroup="formbrethren"
                                                                                        runat="server" Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="ddageparent1_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณาเลือกอายุ"
                                                                                        ValidationExpression="เลือกอายุ" InitialValue="00" />
                                                                                </div>
                                                                                <div class="col-md-2" style="margin-left: -3%;">
                                                                                    <h5>ปี</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label8" runat="server" Text="อาชีพ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <asp:TextBox ID="tboccupationparent1_edit" class="form-control" runat="server" Text='<%# Eval("tboccupationparent1") %>' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label9" runat="server" Text="สถานที่ทำงาน"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <asp:TextBox ID="tbfirmaddressparent1_edit" class="form-control" runat="server" Text='<%# Eval("tbfirmaddressparent1") %>' MaxLength="60" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-4 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label10" runat="server" Text="เบอร์โทร"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-8">
                                                                                    <asp:TextBox ID="tbnumberparent1_edit" class="form-control" runat="server" Text='<%# Eval("tbnumberparent1") %>' MaxLength="10" />
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ControlToValidate="tbnumberparent1_edit"
                                                                                        ValidationExpression="^[0-9]{9,10}$"
                                                                                        ValidationGroup="formbrethren" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <hr />
                                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="formbrethren" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="รายการ" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbPage" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อ" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbnameparent" runat="server" Text='<%# Eval("tbnameparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="นามสกุล" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lblastnameparent" runat="server" Text='<%# Eval("tblastnameparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="อายุ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbageparent" runat="server" Text='<%# Eval("ddageparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="อาชีพ" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lboccupationparent" runat="server" Text='<%# Eval("tboccupationparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="สถานที่ทำงาน" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbfirmaddressparent" runat="server" Text='<%# Eval("tbfirmaddressparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="เบอร์โทร" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbnumberparent" runat="server" Text='<%# Eval("tbnumberparent1")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Width="11%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDrivingEdit" runat="server" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" CommandName="Delete" />
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group" style="margin-top: 7%; margin-bottom: -2%;">
                                        <div class="col-md-6 text-right">
                                            <asp:LinkButton ID="btbackadd2" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd2" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>

                                        <div class="col-md-6 text-left">
                                            <asp:LinkButton ID="btadd2" ValidationGroup="formInsert2" OnClientClick="javascript:scroll(0,0);" CssClass="btn btn-primary" runat="server" CommandName="Cmdnext2" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btadd2" />
                                    <asp:PostBackTrigger ControlID="btbackadd2" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd4" runat="server">
                <asp:HyperLink runat="server" ID="HyperLink3" />
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <h4 class="list-group-item col-md-4">ประวัติการศึกษา <small>History of education</small></h4>
                    <%-- <div class="bs-docs-example2" data-content="ประวัติการศึกษา">--%>
                    <%--<h4 class="list-group-item" style="width: 20%;">ประวัติครอบครัว</h4>--%>
                    <asp:FormView ID="FormView3" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>
                            <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional"
                                ChildrenAsTriggers="true">
                                <ContentTemplate>

                                    <div class="form-horizontal" role="form">
                                        <div class="form-group" style="margin-left: 5%; margin-top: 3%;">
                                            <asp:CheckBox ID="hide_education" runat="server" OnCheckedChanged="Checkbox" AutoPostBack="true" />
                                            ไม่มีประวัติการศึกษา
                                        </div>
                                        <asp:Panel ID="hide_education_1" runat="server" Visible="true">
                                            <div class="form-group" style="margin-left: 4%;">
                                                <div class="col-md-5">
                                                    <label style="color: red">***กรุณาใส่ข้อมูลตั้งแต่ปัจจุบันจนถึงเก่าสุด***</label>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblnameinstitute" runat="server" ForeColor="DarkBlue" Text="ชื่อสถาบัน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Name of Institute</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbinstitute" runat="server" CssClass="form-control" placeholder="ชื่อสถาบัน" MaxLength="70" />
                                                        <asp:RequiredFieldValidator ID="tbnameAL1"
                                                            ValidationGroup="formHisEdu" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbinstitute"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อสถาบัน"
                                                            ValidationExpression="Please check Name" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lbleducation" runat="server" ForeColor="DarkBlue" Text="ระดับการศึกษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Education</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddeducation" runat="server" CssClass="form-control" placeholder="ระดับการศึกษา" />
                                                        <asp:RequiredFieldValidator ID="AddbirthdayAL1"
                                                            ValidationGroup="formHisEdu"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddeducation"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกระดับการศึกษา"
                                                            ValidationExpression="กรุณาเลือกระดับการศึกษา" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblprovinceeducation" runat="server" ForeColor="DarkBlue" Text="จังหวัด"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Province</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddprovinceeducation" runat="server" CssClass="form-control" MaxLength="500" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                                                            ValidationGroup="formHisEdu"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddprovinceeducation"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกจังหวัด"
                                                            ValidationExpression="กรุณาเลือกจังหวัด" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblyearattended" runat="server" ForeColor="DarkBlue" Text="ปีการศึกษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Year Attended</p>
                                                        </small>
                                                    </label>
                                                    <label class="col-md-1 list-group-item-heading">
                                                        <asp:Label ID="lblfromattended" runat="server" ForeColor="DarkBlue" Text="จาก"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">From</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="Addyearattendedstart" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12"
                                                            ValidationGroup="formHisEdu"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="Addyearattendedstart"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกปี"
                                                            ValidationExpression="เลือกปีที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <label class="col-md-1 list-group-item-heading">
                                                        <asp:Label ID="lbltoattended" runat="server" ForeColor="DarkBlue" Text="ถึง"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">To</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="tbtoattended" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13"
                                                            ValidationGroup="formHisEdu"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbtoattended"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกปี"
                                                            ValidationExpression="เลือกปีที่ต้องการ" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="Label21" runat="server" ForeColor="DarkBlue" Text="คณะ"></asp:Label>
                                                        <small>
                                                            <p class="list-group-item-text">Faculty</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:DropDownList ID="ddFaculty" runat="server" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lblcourse" runat="server" ForeColor="DarkBlue" Text="วิชาที่ศึกษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Course Taken</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbcourse" runat="server" CssClass="form-control" placeholder="วิชาที่ศึกษา" MaxLength="50" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                                                            ValidationGroup="formHisEdu" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbcourse"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกวิชาที่ศึกษา"
                                                            ValidationExpression="Please check Name" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-3 list-group-item-heading">
                                                        <asp:Label ID="lblcompleted" runat="server" ForeColor="DarkBlue" Text="วุฒิที่ได้รับ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Completed</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-6">
                                                        <asp:DropDownList ID="ddcompleted" runat="server" CssClass="form-control" placeholder="วุฒิที่ได้รับ" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                                                            ValidationGroup="formHisEdu"
                                                            runat="server" Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddcompleted"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณาเลือกวุฒิที่ได้รับ"
                                                            ValidationExpression="กรุณาเลือกวุฒิการศึกษา" InitialValue="00" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="Label22" runat="server" ForeColor="DarkBlue" Text="เกรดเฉลี่ย"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">GPA</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbGpa" runat="server" onkeypress="autoTab2(this)" CssClass="form-control" placeholder="เกรดเฉลี่ย" MaxLength="4" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16"
                                                            ValidationGroup="formHisEdu" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="tbGpa"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp;* กรุณากรอกเกรดเฉลี่ย"
                                                            ValidationExpression="Please check Name" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1001" runat="server"
                                                            ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            Font-Size="11" ForeColor="Red"
                                                            ControlToValidate="tbGpa"
                                                            ValidationExpression="^[0-9.]{4,4}$"
                                                            ValidationGroup="formHisEdu" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <asp:LinkButton ID="btmore2" runat="server" CssClass="btn btn-success" CommandName="more2" ValidationGroup="formHisEdu" OnCommand="btnCommand" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"> เพิ่ม</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="hide_education_2" runat="server" Visible="true">
                                            <div class="form-group col-md-12" style="margin-left: 0.5%; margin-top: 3%;">
                                                <div>
                                                    <h4>สรุปรายการ<small> (List Education)</small></h4>
                                                </div>
                                                <div id="ViewModelReportAddparents2" visible="True" runat="server">
                                                    <asp:GridView ID="GvReportAdd_Education"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        ShowFooter="False"
                                                        ShowHeaderWhenEmpty="True"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowEditing="Gvbuyequipment_Editing"
                                                        OnRowUpdating="Gvbuyequipment_RowUpdating"
                                                        OnRowDataBound="Gvbuyequipment_RowDataBound"
                                                        OnRowDeleting="Gvbuyequipment_RowDeleting"
                                                        OnRowCancelingEdit="Gvbuyequipment_RowCancelingEdit">

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal" role="form">
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12">
                                                                                    <h3>Education FormEdit<small> (แบบฟอร์มแก้ไขรายการประวัติการศึกษา)</small></h3>
                                                                                </div>
                                                                            </div>
                                                                            <hr />
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label19" runat="server" Text="ชื่อสถาบัน"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:TextBox ID="tbinstitute_edit" class="form-control" runat="server" Text='<%# Eval("tbinstitute") %>' />
                                                                                        <asp:RequiredFieldValidator ID="tbnameAL12"
                                                                                            ValidationGroup="formHisEduEdit" runat="server"
                                                                                            Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="tbinstitute_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณากรอกชื่อสถาบัน"
                                                                                            ValidationExpression="Please check Name" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label6" runat="server" Text="ระดับการศึกษา"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="ddeducation_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="ddeducation_edit_" runat="server" Value='<%# Eval("ddeducation") %>' />
                                                                                        <asp:RequiredFieldValidator ID="AddbirthdayAL12"
                                                                                            ValidationGroup="formHisEduEdit"
                                                                                            runat="server" Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="ddeducation_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณาเลือกระดับการศึกษา"
                                                                                            ValidationExpression="กรุณาเลือกระดับการศึกษา" InitialValue="00" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label7" runat="server" Text="จังหวัด"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="ddprovinceeducation_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="ddprovinceeducation_edit_" runat="server" Value='<%# Eval("ddprovinceeducation") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator112"
                                                                                            ValidationGroup="formHisEduEdit"
                                                                                            runat="server" Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="ddprovinceeducation_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณาเลือกจังหวัด"
                                                                                            ValidationExpression="กรุณาเลือกจังหวัด" InitialValue="00" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label8" runat="server" Text="ปีการศึกษา"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="Addyearattendedstart_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="Addyearattendedstart_edit_" runat="server" Value='<%# Eval("Addyearattendedstart") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator122"
                                                                                            ValidationGroup="formHisEduEdit"
                                                                                            runat="server" Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="Addyearattendedstart_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณาเลือกปี"
                                                                                            ValidationExpression="เลือกปีที่ต้องการ" InitialValue="00" />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label9" runat="server" Text="ถึง"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="tbtoattended_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="tbtoattended_edit_" runat="server" Value='<%# Eval("tbtoattended") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator132"
                                                                                            ValidationGroup="formHisEduEdit"
                                                                                            runat="server" Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="tbtoattended_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณาเลือกปี"
                                                                                            ValidationExpression="เลือกปีที่ต้องการ" InitialValue="00" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label10" runat="server" Text="วิชาที่ศึกษา"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:TextBox ID="tbcourse_edit" class="form-control" runat="server" Text='<%# Eval("tbcourse") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator142"
                                                                                            ValidationGroup="formHisEduEdit" runat="server"
                                                                                            Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="tbcourse_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณากรอกวิชาที่ศึกษา"
                                                                                            ValidationExpression="Please check Name" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label11" runat="server" Text="วุฒิที่ได้รับ"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="ddcompleted_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="ddcompleted_edit_" runat="server" Value='<%# Eval("ddcompleted") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator152"
                                                                                            ValidationGroup="formHisEduEdit"
                                                                                            runat="server" Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="ddcompleted_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณาเลือกวุฒิที่ได้รับ"
                                                                                            ValidationExpression="กรุณาเลือกวุฒิการศึกษา" InitialValue="00" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label23" runat="server" Text="คณะ"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="ddFaculty_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="ddFaculty_edit_" runat="server" Value='<%# Eval("ddFaculty") %>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label24" runat="server" Text="เกรดเฉลี่ย"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:TextBox ID="tbGpa_edit" class="form-control" runat="server" onkeypress="autoTab2(this)" Text='<%# Eval("tbGpa") %>' />
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator162"
                                                                                            ValidationGroup="formHisEduEdit" runat="server"
                                                                                            Display="Dynamic"
                                                                                            SetFocusOnError="true"
                                                                                            ControlToValidate="tbGpa_edit"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ErrorMessage="&nbsp;* กรุณากรอกเกรดเฉลี่ย"
                                                                                            ValidationExpression="Please check Name" />
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10012" runat="server"
                                                                                            ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                                                            SetFocusOnError="true"
                                                                                            Display="Dynamic"
                                                                                            Font-Size="11" ForeColor="Red"
                                                                                            ControlToValidate="tbGpa_edit"
                                                                                            ValidationExpression="^[0-9.]{4,4}$"
                                                                                            ValidationGroup="formHisEduEdit" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <hr />
                                                                                <div class="col-sm-4 col-sm-offset-8">
                                                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="formHisEduEdit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายการ" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbPage" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ชื่อสถาบัน" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbnameeducation" runat="server" Text='<%# Eval("tbinstitute")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ระดับการศึกษา" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbeducation" runat="server" Text='<%# Eval("ddeducation_txt")%>'></asp:Label>
                                                                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("ddprovinceeducation")%>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("Addyearattendedstart")%>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("tbtoattended")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="วิชาที่ศึกษา" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbcourse" runat="server" Text='<%# Eval("tbcourse")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="วุฒิที่ได้รับ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbcompleted" runat="server" Text='<%# Eval("ddcompleted_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDrivingEdit" runat="server" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                    <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" CommandName="Delete" />
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                            <div class="col-md-6 text-right">
                                                <asp:LinkButton ID="btbackadd3" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd3" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                            </div>

                                            <div class="col-md-6 text-left">
                                                <asp:LinkButton ID="btadd3" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdnext3" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btadd3" />
                                    <asp:PostBackTrigger ControlID="btbackadd3" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd5" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <%--<div class="bs-docs-example2" data-content="ประวัติการทำงาน">--%>
                    <h4 class="list-group-item col-md-4">ประวัติการทำงาน <small>Career History</small></h4>
                    <%--<h4 class="list-group-item" style="width: 20%;">ประวัติครอบครัว</h4>--%>
                    <asp:FormView ID="FormView4" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>

                            <div class="form-horizontal" role="form">
                                <div class="form-group" style="margin-left: 5%; margin-top: 3%;">
                                    <asp:CheckBox ID="hide_Emp" runat="server" OnCheckedChanged="Checkbox" AutoPostBack="true" />
                                    ไม่มีประวัติการทำงาน
                                </div>
                                <asp:Panel ID="hide_Emp_1" runat="server" Visible="true">
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-5">
                                            <label class="col-md-5 list-group-item-heading">
                                                <asp:Label ID="lblnameemployed" runat="server" ForeColor="DarkBlue" Text="ชื่อสถานประกอบการ"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">List of Employed</p>
                                                </small>
                                            </label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="tbnameemployed" runat="server" CssClass="form-control" placeholder="ชื่อสถานประกอบการ" MaxLength="50" />
                                                <asp:RequiredFieldValidator ID="FormHiswork11"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbnameemployed"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกชื่อสถานประกอบการ"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-md-4 list-group-item-heading">
                                                <asp:Label ID="lbltelbusiness" runat="server" ForeColor="DarkBlue" Text="เบอร์โทร"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">Tel.</p>
                                                </small>
                                            </label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbtelbusiness" runat="server" CssClass="form-control" placeholder="เบอร์โทร" MaxLength="10" />
                                                <asp:RegularExpressionValidator ID="PostcodeALNum" runat="server"
                                                    ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    Font-Size="11" ForeColor="Red"
                                                    ControlToValidate="tbtelbusiness"
                                                    ValidationExpression="^[0-9]{9,10}$"
                                                    ValidationGroup="formInsert" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-5">
                                            <label class="col-md-5 list-group-item-heading">
                                                <asp:Label ID="lbltypeBusiness" runat="server" ForeColor="DarkBlue" Text="ประเภทธุรกิจ"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Type Business</p>
                                                </small>
                                            </label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="tbtypeBusiness" runat="server" CssClass="form-control" placeholder="ประเภทธุรกิจ" MaxLength="40" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbtypeBusiness"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกประเภทธุรกิจ"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-md-4 list-group-item-heading">
                                                <asp:Label ID="lblpositionbusiness" runat="server" ForeColor="DarkBlue" Text="ตำแหน่ง"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Position</p>
                                                </small>
                                            </label>
                                            <div class="col-md-6">
                                                <asp:TextBox ID="tbpositionbusiness" runat="server" CssClass="form-control" placeholder="ตำแหน่ง" MaxLength="30" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbpositionbusiness"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกตำแหน่ง"
                                                    ValidationExpression="Please check Name" />
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1po" runat="server"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* เฉพาะภาษาไทย/อังกฤษเท่านั้น"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    ControlToValidate="tbpositionbusiness"
                                                    ValidationExpression="[a-zA-Zก-ฮะ-์. ]{1,}"
                                                    ValidationGroup="formInsert" />--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-12">
                                            <label class="col-md-2 list-group-item-heading">
                                                <asp:Label ID="lbladdressbusiness" runat="server" ForeColor="DarkBlue" Text="ที่อยู่"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">Address</p>
                                                </small>
                                            </label>
                                            <div class="col-md-5">
                                                <asp:TextBox TextMode="MultiLine" ID="tbaddressbusiness" runat="server" CssClass="form-control" placeholder="ที่อยู่" MaxLength="100" Height="90" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-5">
                                            <label class="col-md-5 list-group-item-heading">
                                                <asp:Label ID="lblyearattended" runat="server" ForeColor="DarkBlue" Text="ปีการทำงาน"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Year Attended</p>
                                                </small>
                                            </label>
                                            <label class="col-md-1 list-group-item-heading">
                                                <asp:Label ID="lblfromattended" runat="server" ForeColor="DarkBlue" Text="จาก"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">From</p>
                                                </small>
                                            </label>
                                            <div class="col-md-6">
                                                <div class='input-group date datepickerbasic'>
                                                    <asp:TextBox ID="Addyearattendedstart_emp" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="Addyearattendedstart_emp"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณาเลือกวันที่"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <label class="col-md-1 list-group-item-heading">
                                                <asp:Label ID="lbltoattended" runat="server" ForeColor="DarkBlue" Text="ถึง"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">To</p>
                                                </small>
                                            </label>
                                            <div class="col-md-6">
                                                <div class='input-group date datepickerNo'>
                                                    <asp:TextBox ID="tbtoattended_emp" runat="server" CssClass="form-control" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbtoattended_emp"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณาเลือกวันที่"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-5">
                                            <label class="col-md-5 list-group-item-heading">
                                                <asp:Label ID="lblsupervisor" runat="server" ForeColor="DarkBlue" Text="ผู้บังคับบัญชา"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">Supervisor</p>
                                                </small>
                                            </label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="tbsupervisor" runat="server" CssClass="form-control" placeholder="ผู้บังคับบัญชา" MaxLength="40" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ ภาษาอังกฤษเท่านั้น"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    ControlToValidate="tbsupervisor"
                                                    ValidationExpression="[ก-ฮะ-์. ]{1,}"
                                                    ValidationGroup="formInsert" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-5">
                                            <label class="col-md-5 list-group-item-heading">
                                                <asp:Label ID="lblsalary" runat="server" ForeColor="DarkBlue" Text="เงินเดือน"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Salary</p>
                                                </small>
                                            </label>
                                            <div class="col-md-5">
                                                <asp:TextBox ID="tbsalary_emp" runat="server" CssClass="form-control" placeholder="เงินเดือน" MaxLength="6" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator21"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbsalary_emp"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกเงินเดือน"
                                                    ValidationExpression="Please check Name" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                    ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเงินเดือนไม่ถูกต้อง"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    Font-Size="11" ForeColor="Red"
                                                    ControlToValidate="tbsalary_emp"
                                                    ValidationExpression="^[0-9]{1,10}$"
                                                    ValidationGroup="formInsert" />
                                            </div>
                                            <div class="col-md-1" style="margin-left: -3%;">
                                                <h5>บาท</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-md-4 list-group-item-heading">
                                                <asp:Label ID="lblrevenue" runat="server" ForeColor="DarkBlue" Text="เงินเดือนอื่นๆ"></asp:Label>
                                                <small>
                                                    <p class="list-group-item-text">Revenue is Other</p>
                                                </small>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbrevenue" runat="server" CssClass="form-control" placeholder="เงินเดือนอื่นๆ" MaxLength="6" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                                    ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเงินเดือนไม่ถูกต้อง"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    Font-Size="11" ForeColor="Red"
                                                    ControlToValidate="tbrevenue"
                                                    ValidationExpression="^[0-9]{1,10}$"
                                                    ValidationGroup="formInsert" />
                                            </div>
                                            <div class="col-md-1" style="margin-left: -3%;">
                                                <h5>บาท</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-12">
                                            <label class="col-md-2 list-group-item-heading">
                                                <asp:Label ID="lblwork" runat="server" ForeColor="DarkBlue" Text="ลักษณะงานโดยย่อ"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Work charcter briefly</p>
                                                </small>
                                            </label>
                                            <div class="col-md-5">
                                                <asp:TextBox TextMode="MultiLine" ID="tbwork" runat="server" CssClass="form-control" placeholder="ลักษณะงานโดยย่อ" MaxLength="70" Height="90" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbwork"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกลักษณะงานโดยย่อ"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-12">
                                            <label class="col-md-2 list-group-item-heading">
                                                <asp:Label ID="Label34" runat="server" ForeColor="DarkBlue" Text="สาเหตุการลาออก"></asp:Label>
                                                <span style="color: red;"><b>*</b></span>
                                                <small>
                                                    <p class="list-group-item-text">Resignation</p>
                                                </small>
                                            </label>
                                            <div class="col-md-5">
                                                <asp:TextBox TextMode="MultiLine" ID="tbResignation" runat="server" CssClass="form-control" placeholder="สาเหตุการลาออก" MaxLength="70" Height="90" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator23"
                                                    ValidationGroup="formwork" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="tbResignation"
                                                    Font-Size="11" ForeColor="Red"
                                                    ErrorMessage="&nbsp;* กรุณากรอกสาเหตุการลาออก"
                                                    ValidationExpression="Please check Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 2%;">
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <asp:LinkButton ID="btmore3" runat="server" CssClass="btn btn-success" ValidationGroup="formwork" CommandName="more3" OnCommand="btnCommand" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"> เพิ่ม</i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <asp:Panel ID="hide_Emp_2" runat="server" Visible="true">
                                <div class="row">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group col-md-12" style="margin-left: 0.5%; margin-top: 3%;">
                                            <div>
                                                <h4>สรุปรายการ<small> (List Employed)</small></h4>
                                            </div>
                                            <div id="ViewModelReportAddparents3" visible="True" runat="server">
                                                <asp:GridView ID="GvReportAdd_Employee"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover"
                                                    HeaderStyle-CssClass="table_headCenter"
                                                    HeaderStyle-Height="40px"
                                                    ShowFooter="False"
                                                    ShowHeaderWhenEmpty="True"
                                                    AllowPaging="True"
                                                    PageSize="5"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    OnRowEditing="Gvbuyequipment_Editing"
                                                    OnRowUpdating="Gvbuyequipment_RowUpdating"
                                                    OnRowDataBound="Gvbuyequipment_RowDataBound"
                                                    OnRowDeleting="Gvbuyequipment_RowDeleting"
                                                    OnRowCancelingEdit="Gvbuyequipment_RowCancelingEdit">

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate>
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-12">
                                                                                <h3>Work History FormEdit<small> (แบบฟอร์มแก้ไขรายการประวัติการทำงาน)</small></h3>
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label19" runat="server" Text="ชื่อสถานประกอบการ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <asp:TextBox ID="tbnameemployed_edit" class="form-control" runat="server" Text='<%# Eval("tbnameemployed") %>' />
                                                                                    <asp:RequiredFieldValidator ID="FormHiswork2"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbnameemployed_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกชื่อสถานประกอบการ"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label6" runat="server" Text="เบอร์โทร"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <asp:TextBox ID="tbtelbusiness_edit" class="form-control" runat="server" Text='<%# Eval("tbtelbusiness") %>' />
                                                                                    <asp:RegularExpressionValidator ID="PostcodeALNum2" runat="server"
                                                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ControlToValidate="tbtelbusiness_edit"
                                                                                        ValidationExpression="^[0-9]{9,10}$"
                                                                                        ValidationGroup="formworkEdit" />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label7" runat="server" Text="ประเภทธุรกิจ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <asp:TextBox ID="tbtypeBusiness_edit" class="form-control" runat="server" Text='<%# Eval("tbtypeBusiness") %>' />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator172"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbtypeBusiness_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกประเภทธุรกิจ"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label8" runat="server" Text="ตำแหน่ง"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <asp:TextBox ID="tbpositionbusiness_edit" runat="server" class="form-control" Value='<%# Eval("tbpositionbusiness") %>' />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator182"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbpositionbusiness_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกตำแหน่ง"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-6">
                                                                                <label class="col-sm-5 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label9" runat="server" Text="ที่อยู่"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-7">
                                                                                    <asp:TextBox TextMode="MultiLine" ID="tbaddressbusiness_edit" class="form-control" runat="server" Text='<%# Eval("tbaddressbusiness") %>' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label10" runat="server" Text="ปีการทำงาน"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <div class='input-group date datepickerbasic'>
                                                                                        <asp:TextBox ID="Addyearattendedstart_emp_edit" CssClass="form-control" class="form-control" runat="server" Text='<%# Eval("Addyearattendedstart_emp") %>' />
                                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                    </div>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator192"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="Addyearattendedstart_emp_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณาเลือกวันที่"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label15" runat="server" Text="ถึง"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <div class='input-group date datepickerNo'>
                                                                                        <asp:TextBox ID="tbtoattended_emp_edit" class="form-control" CssClass="form-control" runat="server" Text='<%# Eval("tbtoattended_emp") %>' />
                                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                    </div>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbtoattended_emp_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณาเลือกวันที่"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label11" runat="server" Text="ผู้บังคับบัญชา"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-6">
                                                                                    <asp:TextBox ID="tbsupervisor_edit" class="form-control" runat="server" Text='<%# Eval("tbsupervisor") %>' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label16" runat="server" Text="เงินเดือน"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-5">
                                                                                    <asp:TextBox ID="tbsalary_emp_edit" class="form-control" runat="server" Text='<%# Eval("tbsalary_emp") %>' />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator212"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbsalary_emp_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกเงินเดือน"
                                                                                        ValidationExpression="Please check Name" />
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator152" runat="server"
                                                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเงินเดือนไม่ถูกต้อง"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ControlToValidate="tbsalary_emp_edit"
                                                                                        ValidationExpression="^[0-9]{1,10}$"
                                                                                        ValidationGroup="formworkEdit" />
                                                                                </div>
                                                                                <div class="col-md-1" style="margin-left: -3%;">
                                                                                    <h5>บาท</h5>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-5">
                                                                                <label class="col-sm-6 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label17" runat="server" Text="เงินเดือนอื่นๆ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-5">
                                                                                    <asp:TextBox ID="tbrevenue_edit" class="form-control" runat="server" Text='<%# Eval("tbrevenue") %>' />
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator222" runat="server"
                                                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเงินเดือนไม่ถูกต้อง"
                                                                                        SetFocusOnError="true"
                                                                                        Display="Dynamic"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ControlToValidate="tbrevenue_edit"
                                                                                        ValidationExpression="^[0-9]{1,10}$"
                                                                                        ValidationGroup="formworkEdit" />
                                                                                </div>
                                                                                <div class="col-md-1" style="margin-left: -3%;">
                                                                                    <h5>บาท</h5>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <div class="col-md-6">
                                                                                <label class="col-sm-5 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label18" runat="server" Text="ลักษณะงานโดยย่อ"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-7">
                                                                                    <asp:TextBox ID="tbwork_edit" TextMode="MultiLine" class="form-control" runat="server" Text='<%# Eval("tbwork") %>' />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator223"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbwork_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกลักษณะงานโดยย่อ"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6">
                                                                                <label class="col-sm-5 control-label">
                                                                                    <h2 class="panel-title">
                                                                                        <asp:Label ID="Label35" runat="server" Text="สาเหตุการลาออก"></asp:Label>
                                                                                    </h2>
                                                                                </label>
                                                                                <div class="col-sm-7">
                                                                                    <asp:TextBox ID="tbResignation_edit" TextMode="MultiLine" class="form-control" runat="server" Text='<%# Eval("tbResignation") %>' />
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator232"
                                                                                        ValidationGroup="formworkEdit" runat="server"
                                                                                        Display="Dynamic"
                                                                                        SetFocusOnError="true"
                                                                                        ControlToValidate="tbResignation_edit"
                                                                                        Font-Size="11" ForeColor="Red"
                                                                                        ErrorMessage="&nbsp;* กรุณากรอกสาเหตุการลาออก"
                                                                                        ValidationExpression="Please check Name" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <hr />
                                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="formworkEdit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="รายการ" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbPage" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อสถานประกอบการ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbnameEmployed" runat="server" Text='<%# Eval("tbnameemployed")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ประเภทธุรกิจ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbtypeBusiness" runat="server" Text='<%# Eval("tbtypeBusiness")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="เบอร์โทร" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbTelemp" runat="server" Text='<%# Eval("tbtelbusiness")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbPosition" runat="server" Text='<%# Eval("tbpositionbusiness")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="เงินเดือน" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbSalaryemp" runat="server" Text='<%# Eval("tbsalary_emp")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ลักษณะงานโดยย่อ" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lbWorkemp" runat="server" Text='<%# Eval("tbwork")%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDrivingEdit" runat="server" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" CommandName="Delete" />
                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                <div class="col-md-6 text-right">
                                    <asp:LinkButton ID="btbackadd4" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd4" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                </div>

                                <div class="col-md-6 text-left">
                                    <asp:LinkButton ID="btadd4" CssClass="btn btn-primary" runat="server" OnClientClick="javascript:scroll(0,0);" CommandName="Cmdnext4" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></asp:LinkButton>
                                </div>
                            </div>

                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd6" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <h4 class="list-group-item col-md-4">ความสามารถพิเศษ <small>Talent</small></h4>
                    <%--<div class="bs-docs-example2" data-content="ความสามารถพิเศษ">--%>
                    <%--<h4 class="list-group-item" style="width: 20%;">ประวัติครอบครัว</h4>--%>
                    <asp:FormView ID="FormView5" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional"
                                ChildrenAsTriggers="true">
                                <ContentTemplate>

                                    <div class="form-horizontal" role="form">
                                        <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-5 list-group-item-heading">
                                                    <asp:Label ID="lblcomprogram" runat="server" ForeColor="DarkBlue" Text="คอมพิวเตอร์/โปรแกรม"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Computer/Program</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-7">
                                                    <asp:TextBox TextMode="MultiLine" ID="tbcom_pro" runat="server" CssClass="form-control" placeholder="คอมพิวเตอร์/โปรแกรม" MaxLength="500" Height="90" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-5 list-group-item-heading">
                                                    <asp:Label ID="lbltypingth" runat="server" ForeColor="DarkBlue" Text="พิมพ์ดีดไทย"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Typing Thai</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbtypingth" runat="server" CssClass="form-control" placeholder="จำนวนคำใน 1 นาที" MaxLength="3" />
                                                    <asp:RegularExpressionValidator ID="PostcodeALNum2" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbtypingth"
                                                        ValidationExpression="^[0-9]{1,3}$"
                                                        ValidationGroup="formInsert" />
                                                </div>
                                                <div class="col-md-2">
                                                    <h5>คำ/นาที</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-md-4 list-group-item-heading">
                                                    <asp:Label ID="lbltypingeng" runat="server" ForeColor="DarkBlue" Text="พิมพ์ดีดอังกฤษ"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Typing English</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-5">
                                                    <asp:TextBox ID="tbtypingeng" runat="server" CssClass="form-control" placeholder="จำนวนคำใน 1 นาที" MaxLength="3" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                                                        ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbtypingeng"
                                                        ValidationExpression="^[0-9]{1,3}$"
                                                        ValidationGroup="formInsert" />
                                                </div>
                                                <div class="col-md-2">
                                                    <h5>คำ/นาที</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <h4 class="list-group-item col-md-4">ความสามารถทางภาษา <small>(Type Language)</small></h4>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 3%; margin-top: 3%;">
                                            <h9 class="col-md-8" style="color: red;"> ## บังคับเลือกภาษาไทยและภาษาอังกฤษ ##</h9>
                                        </div>

                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-5">
                                                <div class="col-md-4">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label38" runat="server" ForeColor="DarkBlue" Text="ภาษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Language</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-7">
                                                    <b>
                                                        <asp:Label ID="thai" runat="server" Font-Size="Medium" Text="ไทย"></asp:Label>
                                                    </b>
                                                    <small>(Thai)</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-10">
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label39" runat="server" ForeColor="DarkBlue" Text="การพูด"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Speaking</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList ID="LthaiSP" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RqChoice" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LthaiSP" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label40" runat="server" ForeColor="DarkBlue" Text="ความเข้าใจ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Understanding</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="LthaiUST" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LthaiUST" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-10">
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label41" runat="server" ForeColor="DarkBlue" Text="การอ่าน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Reading</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList ID="LthaiRD" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator27" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LthaiRD" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label42" runat="server" ForeColor="DarkBlue" Text="การเขียน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Wrinting</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="LthaiWT" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator28" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LthaiWT" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-5">
                                                <div class="col-md-4">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label43" runat="server" ForeColor="DarkBlue" Text="ภาษา"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Language</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-7">
                                                    <b>
                                                        <asp:Label ID="Label44" runat="server" Font-Size="Medium" Text="อังกฤษ"></asp:Label>
                                                    </b>
                                                    <small>(English)</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-10">
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label45" runat="server" ForeColor="DarkBlue" Text="การพูด"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Speaking</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList ID="LEngSP" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LEngSP" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label46" runat="server" ForeColor="DarkBlue" Text="ความเข้าใจ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Understanding</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="LEngUST" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LEngUST" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-10">
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label47" runat="server" ForeColor="DarkBlue" Text="การอ่าน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Reading</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:RadioButtonList ID="LEngRD" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator31" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LEngRD" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                                <div class="col-md-2">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label48" runat="server" ForeColor="DarkBlue" Text="การเขียน"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Wrinting</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:RadioButtonList ID="LEngWT" runat="server">
                                                        <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator32" ValidationGroup="formRBTThai" runat="server" Display="Dynamic"
                                                        ControlToValidate="LEngWT" Font-Size="11"
                                                        ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกคำตอบ"
                                                        SetFocusOnError="true"
                                                        ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                </div>
                                            </div>
                                        </div>

                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2.3%;">
                                            <div class="col-md-5">
                                                <div class="col-md-4">
                                                    <label class="list-group-item-heading">
                                                        <asp:Label ID="Label36" runat="server" ForeColor="DarkBlue" Text="ภาษาอื่นๆ"></asp:Label>
                                                        <%--<span style="color: red;"><b>*</b></span>--%>
                                                        <small>
                                                            <p class="list-group-item-text">(Language Other)</p>
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="tbotherLanguage" runat="server" CssClass="form-control" placeholder="ภาษา" OnSelectedIndexChanged="selectchang_dd" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="hindLOther" runat="server" Visible="false">
                                            <div class="form-group" style="margin-left: 2.3%;">
                                                <div class="col-md-10">
                                                    <div class="col-md-2">
                                                        <label class="list-group-item-heading">
                                                            <asp:Label ID="Label2" runat="server" ForeColor="DarkBlue" Text="การพูด"></asp:Label>
                                                            <%--<span style="color: red;"><b>*</b></span>--%>
                                                            <small>
                                                                <p class="list-group-item-text">Speaking</p>
                                                            </small>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:RadioButtonList ID="status_speaking_other" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="list-group-item-heading">
                                                            <asp:Label ID="Label3" runat="server" ForeColor="DarkBlue" Text="ความเข้าใจ"></asp:Label>
                                                            <%--<span style="color: red;"><b>*</b></span>--%>
                                                            <small>
                                                                <p class="list-group-item-text">Understanding</p>
                                                            </small>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:RadioButtonList ID="status_Understanding_other" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-left: 2.3%;">
                                                <div class="col-md-10">
                                                    <div class="col-md-2">
                                                        <label class="list-group-item-heading">
                                                            <asp:Label ID="Label4" runat="server" ForeColor="DarkBlue" Text="การอ่าน"></asp:Label>
                                                            <%--<span style="color: red;"><b>*</b></span>--%>
                                                            <small>
                                                                <p class="list-group-item-text">Reading</p>
                                                            </small>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:RadioButtonList ID="status_Reading_other" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="list-group-item-heading">
                                                            <asp:Label ID="Label5" runat="server" ForeColor="DarkBlue" Text="การเขียน"></asp:Label>
                                                            <%--<span style="color: red;"><b>*</b></span>--%>
                                                            <small>
                                                                <p class="list-group-item-text">Wrinting</p>
                                                            </small>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:RadioButtonList ID="status_Wrinting_other" runat="server">
                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="margin-left: 0;">
                                                    <div class="col-md-6">
                                                        <div class="col-md-6">
                                                            <asp:LinkButton ID="btmore4" runat="server" CssClass="btn btn-success" CommandName="more4" OnCommand="btnCommand" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"> เพิ่ม</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12" style="margin-top: 1%; margin-left: 0.5%; margin-bottom: 1%;">
                                                <div>
                                                    <h4>สรุปรายการ<small> (List Other Languages)</small></h4>
                                                </div>
                                                <div id="ViewModelReportAddparents4" visible="True" runat="server">
                                                    <asp:GridView ID="GvReportAddOtherLanguages"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        ShowFooter="False"
                                                        ShowHeaderWhenEmpty="True"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowEditing="Gvbuyequipment_Editing"
                                                        OnRowUpdating="Gvbuyequipment_RowUpdating"
                                                        OnRowDataBound="Gvbuyequipment_RowDataBound"
                                                        OnRowDeleting="Gvbuyequipment_RowDeleting"
                                                        OnRowCancelingEdit="Gvbuyequipment_RowCancelingEdit">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate>
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal" role="form">
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12">
                                                                                    <h3>Languages FormEdit<small> (แบบฟอร์มแก้ไขรายการภาษาอื่นๆ)</small></h3>
                                                                                </div>
                                                                            </div>
                                                                            <hr />
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-label">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label19" runat="server" Text="ภาษาอื่นๆ"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="tbotherLanguage_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="tbotherLanguage_edit_" runat="server" Value='<%# Eval("tbotherLanguage") %>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br />
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-3 control-labelnotop1" style="margin-left: 10%;">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label6" runat="server" Text="การพูด"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-5">
                                                                                        <asp:RadioButtonList ID="status_speaking_other_edit" runat="server">
                                                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:HiddenField ID="status_speaking_other_edit_" runat="server" Value='<%# Eval("status_speaking_other") %>' />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-labelnotop1">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label7" runat="server" Text="ความเข้าใจ"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-5">
                                                                                        <asp:RadioButtonList ID="status_Understanding_other_edit" runat="server">
                                                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:HiddenField ID="status_Understanding_other_" runat="server" Value='<%# Eval("status_Understanding_other") %>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-3 control-labelnotop1" style="margin-left: 10%;">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label8" runat="server" Text="การอ่าน"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-5">
                                                                                        <asp:RadioButtonList ID="status_Reading_other_edit" runat="server">
                                                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:HiddenField ID="status_Reading_other_" runat="server" Value='<%# Eval("status_Reading_other") %>' />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-labelnotop1">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label9" runat="server" Text="การเขียน"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-5">
                                                                                        <asp:RadioButtonList ID="status_Wrinting_other_edit" runat="server">
                                                                                            <asp:ListItem Value="1" Text="&nbsp;ดี (Good)"></asp:ListItem>
                                                                                            <asp:ListItem Value="2" Text="&nbsp;ดีมาก (Excellent)"></asp:ListItem>
                                                                                            <asp:ListItem Value="3" Text="&nbsp;พอใช้ (Fair)"></asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                        <asp:HiddenField ID="status_Wrinting_other_" runat="server" Value='<%# Eval("status_Wrinting_other") %>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <hr />
                                                                                <div class="col-sm-4 col-sm-offset-8">
                                                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายการ" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbPage" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ภาษา" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbnameparent" runat="server" Text='<%# Eval("tbotherLanguage_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="การพูด" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblastnameparent" runat="server" Text='<%# Eval("status_speaking_other_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ความเข้าใจ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbageparent" runat="server" Text='<%# Eval("status_Understanding_other_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="การอ่าน" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lboccupationparent" runat="server" Text='<%# Eval("status_Reading_other_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="การเขียน" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbfirmaddressparent" runat="server" Text='<%# Eval("status_Wrinting_other_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDrivingEdit" runat="server" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                    <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" CommandName="Delete" />
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <h7>&nbsp;</h7>
                                        <hr class="so_sweethr006" />
                                        <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                            <div class="col-md-12">
                                                <h4 class="list-group-item col-md-4">ความสามารถในการขับขี่ <small>(The Driving Skills)</small></h4>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-left: 5%; margin-top: 3%;">
                                            <asp:CheckBox ID="hide_drive" runat="server" OnCheckedChanged="Checkbox" AutoPostBack="true" />
                                            ไม่สามารถขับขี่ได้
                                        </div>
                                        <asp:Panel ID="hide_Drive_1" runat="server" Visible="true">
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lbcar" runat="server" ForeColor="DarkBlue" Text="ประเภทรถต่างๆ"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Various types of car</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ddTypesOfCar" class="form-control" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-5">
                                                    <label class="col-md-5 list-group-item-heading">
                                                        <asp:Label ID="lbldriving" runat="server" ForeColor="DarkBlue" Text="ประเภทใบขับขี่"></asp:Label>
                                                        <span style="color: red;"><b>*</b></span>
                                                        <small>
                                                            <p class="list-group-item-text">Driving license kind</p>
                                                        </small>
                                                    </label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="tbdriving" runat="server" CssClass="form-control" placeholder="ประเภทใบขับขี่" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1hinddrive" runat="server"
                                                            Font-Size="11" ForeColor="Red"
                                                            ErrorMessage="&nbsp; &nbsp; &nbsp;*เฉพาะภาษาไทยเท่านั้น"
                                                            SetFocusOnError="true"
                                                            Display="Dynamic"
                                                            ControlToValidate="tbdriving"
                                                            ValidationExpression="[ก-ฮะ-์0-9 ]{1,}"
                                                            ValidationGroup="formInsert" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-left: 2%;">
                                                <div class="col-md-6">
                                                    <div class="col-md-6">
                                                        <asp:LinkButton ID="btmore3" runat="server" CssClass="btn btn-success" CommandName="more5" OnCommand="btnCommand" data-toggle="tooltip"><i class="glyphicon glyphicon-plus-sign" aria-hidden="true"> เพิ่ม</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="hide_Drive_2" runat="server" Visible="true">
                                            <div class="col-md-12" style="margin-top: 1%; margin-left: 0.5%; margin-bottom: 1%;">
                                                <div>
                                                    <h4>สรุปรายการ<small> (List Other Type Of Car)</small></h4>
                                                </div>
                                                <div id="Div1" visible="True" runat="server">
                                                    <asp:GridView ID="GvReportAdd_Car"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        ShowFooter="False"
                                                        ShowHeaderWhenEmpty="True"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowEditing="Gvbuyequipment_Editing"
                                                        OnRowUpdating="Gvbuyequipment_RowUpdating"
                                                        OnRowDataBound="Gvbuyequipment_RowDataBound"
                                                        OnRowDeleting="Gvbuyequipment_RowDeleting"
                                                        OnRowCancelingEdit="Gvbuyequipment_RowCancelingEdit">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate>
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal" role="form">
                                                                            <div class="form-group">
                                                                                <div class="col-lg-12">
                                                                                    <h3>Job Order FormEdit<small> (แบบฟอร์มแก้ไขรายการ Other Type Of Car)</small></h3>
                                                                                </div>
                                                                            </div>
                                                                            <hr />
                                                                            <div class="form-group">
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-labelnotop1">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label19" runat="server" Text="ประเภทรถ"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:DropDownList ID="ddTypeOfCar_edit" class="form-control" runat="server" />
                                                                                        <asp:HiddenField ID="ddTypeOfCar_edit_" runat="server" Value='<%# Eval("ddTypesOfCar") %>' />
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5">
                                                                                    <label class="col-sm-4 control-labelnotop1">
                                                                                        <h2 class="panel-title">
                                                                                            <asp:Label ID="Label6" runat="server" Text="ประเภทใบขับขี่"></asp:Label>
                                                                                        </h2>
                                                                                    </label>
                                                                                    <div class="col-sm-8">
                                                                                        <asp:TextBox ID="tbdriving_edit" class="form-control" runat="server" Text='<%# Eval("tbdriving") %>' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <hr />
                                                                                <div class="col-sm-4 col-sm-offset-8">
                                                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รายการ" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbPage" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ประเภทรถ" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lbnameparent" runat="server" Text='<%# Eval("ddTypesOfCar_txt")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ประเภทใบขับขี่" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblastnameparent" runat="server" Text='<%# Eval("tbdriving")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDrivingEdit" runat="server" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                    <asp:LinkButton ID="btnDrivingDelete" runat="server" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" CommandName="Delete" />
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                            <div class="col-md-6 text-right">
                                                <asp:LinkButton ID="btbackadd5" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd5" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                            </div>

                                            <div class="col-md-6 text-left">
                                                <asp:LinkButton ID="btadd5" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" ValidationGroup="formRBTThai" CommandName="Cmdnext5" OnCommand="btnCommand" data-toggle="tooltip" title="Next">Next <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btadd5" />
                                    <asp:PostBackTrigger ControlID="btbackadd5" />
                                    <asp:PostBackTrigger ControlID="btmore4" />
                                    <asp:PostBackTrigger ControlID="btmore3" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd7" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <h4 class="list-group-item col-md-4">ข้อมูลเพิ่มเติม <small>More information</small></h4>
                    <%--<div class="bs-docs-example2" data-content="ข้อมูลเพิ่มเติม">--%>
                    <%--<h4 class="list-group-item" style="width: 20%;">ประวัติครอบครัว</h4>--%>
                    <asp:FormView ID="FormView6" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                        <InsertItemTemplate>
                            <div class="form-horizontal" role="form">
                                <div class="form-group" style="margin-top: 2%; margin-left: 2%;">
                                    <div class="col-md-12">
                                        <h4 class="col-md-7">บุคคลที่ไม่ใช่ญาติซึ่งทราบประวัติของท่าน และบริษัทฯ สามารถสอบถามได้ <small>(Persons other than relatives can be contaced)</small></h4>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblnamepersons" runat="server" ForeColor="DarkBlue" Text="ชื่อ"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Name</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbnamepersons" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLengt="40" />

                                            <asp:RequiredFieldValidator ID="tbnamepersonsAL"
                                                ValidationGroup="formInsert4" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbnamepersons"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกชื่อ"
                                                ValidationExpression="Please check Name" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1name" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น/ กรอกข้อมูลไม่ถูกต้อง"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbnamepersons"
                                                ValidationExpression="[ก-ฮะ-์]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lbllastnamepersons" runat="server" ForeColor="DarkBlue" Text="นามสกุล"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Lastname</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tblastnamepersons" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLengt="40" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                                ValidationGroup="formInsert4" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tblastnamepersons"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกนามสกุล"
                                                ValidationExpression="Please check Name" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tblastnamepersons"
                                                ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblrelationship" runat="server" ForeColor="DarkBlue" Text="ความสัมพันธ์"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Relationship</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbrelationship" runat="server" CssClass="form-control" placeholder="ความสัมพันธ์" MaxLength="30" />
                                            <asp:RequiredFieldValidator ID="tbrelationshipAL"
                                                ValidationGroup="formInsert4" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbrelationship"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกความสัมพันธ์"
                                                ValidationExpression="Please check Name" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbrelationship"
                                                ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblfirmaddresspersons" runat="server" ForeColor="DarkBlue" Text="สถานที่ทำงาน"></asp:Label>
                                            <small>
                                                <p class="list-group-item-text">Firm Address</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbfirmaddresspersons" runat="server" CssClass="form-control" placeholder="สถานที่ทำงาน" MaxLength="400" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblpositionpersons" runat="server" ForeColor="DarkBlue" Text="ตำแหน่ง"></asp:Label>
                                            <small>
                                                <p class="list-group-item-text">Position</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbpositionpersons" runat="server" CssClass="form-control" placeholder="ตำแหน่ง" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator30" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทย/อังกฤษเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbpositionpersons"
                                                ValidationExpression="[a-zA-Zก-ฮะ-์- ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lbltelpersons" runat="server" ForeColor="DarkBlue" Text="โทรศัพท์"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Telephone</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbtelpersons" runat="server" CssClass="form-control" placeholder="โทรศัพท์" MaxLength="10" />
                                            <asp:RequiredFieldValidator ID="tbtelpersonsAL"
                                                ValidationGroup="formInsert4" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbtelpersons"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกเบอร์โทรศัพท์"
                                                ValidationExpression="Please check Name" />
                                            <asp:RegularExpressionValidator ID="tbtelpersonsNum" runat="server"
                                                ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                Font-Size="11" ForeColor="Red"
                                                ControlToValidate="tbtelpersons"
                                                ValidationExpression="^[0-9]{9,10}$"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                </div>
                                <hr class="so_sweethr006" />
                                <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lblwhoadvise" runat="server" ForeColor="DarkBlue" Text="ใครแนะนำให้ท่านมาสมัครงานที่นี้/หรือทราบได้อย่างไร"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Who advise come to apply for a job that or know get?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddadvise" runat="server" CssClass="form-control" placeholder="รู้จากไหน" />
                                            <asp:RequiredFieldValidator ID="ddadviseAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddadvise"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือกแหล่งข้อมูล"
                                                ValidationExpression="กรุณาเลือกแหล่งข้อมูล" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lbleverwork" runat="server" ForeColor="DarkBlue" Text="ท่านเคยยื่นใบสมัครหรือเคยทำงานกับบริษัทนี้มาก่อนหรือไม่?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">You ever work with or not is this company comes to before?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddworkcompany" runat="server" CssClass="form-control" placeholder="เคยสัมครมาไหม" />
                                            <asp:RequiredFieldValidator ID="ddworkcompanyAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddworkcompany"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือก เคย/ไม่เคย"
                                                ValidationExpression="กรุณาเลือก เคย/ไม่เคย" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lblcaught" runat="server" ForeColor="DarkBlue" Text="ท่านเคยถูกจับหรือต้องคดีอาญาบ้างหรือไม่?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">You had evered to caught or , accuse criminal?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddcriminal" runat="server" CssClass="form-control" placeholder="เคยถูกจับหรือไม่" />
                                            <asp:RequiredFieldValidator ID="ddcriminalAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddcriminal"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือก เคย/ไม่เคย"
                                                ValidationExpression="กรุณาเลือก เคย/ไม่เคย" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lblallergy" runat="server" ForeColor="DarkBlue" Text="ท่านเคยมีประวัติเป็นโรคภูมิแพ้/แพ้สารหรือยาอะไรบ้าง?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">You have evered to have the history is the disease the allergy?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddallergy" runat="server" CssClass="form-control" placeholder="แพ้ยา" />
                                            <asp:RequiredFieldValidator ID="ddallergyAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddallergy"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือก เคย/ไม่เคย"
                                                ValidationExpression="กรุณาเลือก เคย/ไม่เคย" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lblcongenital" runat="server" ForeColor="DarkBlue" Text="ท่านมีโรคประจำตัวหรือไม่?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Or not are you have the congenital disease?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="dddisease" runat="server" CssClass="form-control" placeholder="โรคประจำตัว" />
                                            <asp:RequiredFieldValidator ID="dddiseaseAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="dddisease"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือก เคย/ไม่เคย"
                                                ValidationExpression="กรุณาเลือก เคย/ไม่เคย" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-12">
                                        <label class="col-md-8 list-group-item-heading">
                                            <asp:Label ID="lblsick" runat="server" ForeColor="DarkBlue" Text="ท่านเคยป่วยหนัก,ได้รับบาดแผลจากอุบัติเหตุ,หรือได้รับการผ่าตัดหรือไม่?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">You have evered to violently sick, receive a wound from an accident or have operate on?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddsick" runat="server" CssClass="form-control" placeholder="ป่วยหนัก" />
                                            <asp:RequiredFieldValidator ID="ddsickAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddsick"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณาเลือก เคย/ไม่เคย"
                                                ValidationExpression="กรุณาเลือก เคย/ไม่เคย" InitialValue="00" />
                                        </div>
                                    </div>
                                </div>
                                <hr class="so_sweethr006" />
                                <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                    <div class="col-md-12">
                                        <label class="col-md-6 list-group-item-heading">
                                            <asp:Label ID="lblaccident" runat="server" ForeColor="DarkBlue" Text="ในกรณีเกิดเหตุฉุกเฉินกรุณาติดต่อใคร?"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">In case of there in an accident the emergency pleases to contact with who?</p>
                                            </small>
                                        </label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="tbaccident" runat="server" CssClass="form-control" placeholder="ชื่อ-นามสกุล" MaxLength="90" />
                                            <asp:RequiredFieldValidator ID="tbaccidentAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbaccident"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกชื่อ-นามสกุล"
                                                ValidationExpression="กรุณากรอกชื่อ-นามสกุล" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbaccident"
                                                ValidationExpression="[ก-ฮะ-์ ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblrelationaccident" runat="server" ForeColor="DarkBlue" Text="ความสัมพันธ์"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">The relation is</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbrelation" runat="server" CssClass="form-control" placeholder="ความสัมพันธ์" MaxLength="30" />
                                            <asp:RequiredFieldValidator ID="tbrelationAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbrelation"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกความสัมพันธ์"
                                                ValidationExpression="กรุณากรอกความสัมพันธ์" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbrelation"
                                                ValidationExpression="[ก-ฮะ-์a-zA-z ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lbltelaccident" runat="server" ForeColor="DarkBlue" Text="โทรศัพท์"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Telephone</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbtelaccident" runat="server" CssClass="form-control" placeholder="โทรศัพท์" MaxLength="10" />
                                            <asp:RequiredFieldValidator ID="tbtelaccidentAL"
                                                ValidationGroup="formInsert4"
                                                runat="server" Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbtelaccident"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกเบอร์โทรศัพท์"
                                                ValidationExpression="กรุณากรอกเบอร์โทรศัพท์" />
                                            <asp:RegularExpressionValidator ID="PostcodeALNum" runat="server"
                                                ErrorMessage="&nbsp;* เฉพาะตัวเลขเท่านั้น/ กรอกเบอร์โทรไม่ถูกต้อง"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                Font-Size="11" ForeColor="Red"
                                                ControlToValidate="tbtelaccident"
                                                ValidationExpression="^[0-9]{9,10}$"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lbladdresspre" runat="server" ForeColor="DarkBlue" Text="ที่อยู่"></asp:Label>
                                            <span style="color: red;"><b>*</b></span>
                                            <small>
                                                <p class="list-group-item-text">Address</p>
                                            </small>
                                        </label>
                                        <div class="col-md-7">
                                            <asp:TextBox TextMode="MultiLine" ID="tbaddresspreAccident" runat="server" CssClass="form-control" placeholder="ที่อยู่" MaxLength="500" Height="90" />
                                            <asp:RequiredFieldValidator ID="tbaddresspreAL"
                                                ValidationGroup="formInsert4" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="tbaddresspreAccident"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* กรุณากรอกที่อยู่"
                                                ValidationExpression="Please check Name" />
                                        </div>
                                    </div>
                                </div>

                                <hr class="so_sweethr006" />
                                <div class="form-group" style="margin-left: 2%; margin-top: 3%;">
                                    <div class="col-md-12">
                                        <label class="col-md-6 list-group-item-heading">
                                            <asp:Label ID="lblpersoncompany" runat="server" ForeColor="DarkBlue" Text="ให้เขียนชื่อคนรู้จักที่ทำงานในบริษัทนี้"></asp:Label>
                                            <small>
                                                <p class="list-group-item-text">Give sing a person knows the office in this company</p>
                                            </small>
                                        </label>
                                        <div class="col-md-5">
                                            <asp:TextBox ID="tbpersoncompany" runat="server" CssClass="form-control" placeholder="ชื่อ-นามสกุล" MaxLength="90" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp;* เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbpersoncompany"
                                                ValidationExpression="[ก-ฮะ-์- ]{1,}"
                                                ValidationGroup="formInsert4" />

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-4 list-group-item-heading">
                                            <asp:Label ID="lblrelationpersoncompany" runat="server" ForeColor="DarkBlue" Text="ความสัมพันธ์"></asp:Label>
                                            <small>
                                                <p class="list-group-item-text">The relation is</p>
                                            </small>
                                        </label>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="tbrelationpersoncompany" runat="server" CssClass="form-control" placeholder="ความสัมพันธ์" MaxLength="30" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator29" runat="server"
                                                Font-Size="11" ForeColor="Red"
                                                ErrorMessage="&nbsp; &nbsp; &nbsp;*เฉพาะภาษาไทยเท่านั้น"
                                                SetFocusOnError="true"
                                                Display="Dynamic"
                                                ControlToValidate="tbrelationpersoncompany"
                                                ValidationExpression="[ก-ฮะ-์a-zA-Z- ]{1,}"
                                                ValidationGroup="formInsert4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                    <div class="col-md-6 text-right">
                                        <asp:LinkButton ID="btbackadd6" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="Cmdbackadd6" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>

                                    <div class="col-md-6 text-left">
                                        <asp:LinkButton ID="saveEmp" CssClass="btn btn-primary" runat="server" ValidationGroup="formInsert4" OnClientClick="return confirm('กรุณาตรวจสอบข้อมูลก่อนทำการกด ตกลง')" CommandName="NextImmage" OnCommand="btnCommand" data-toggle="tooltip" title="Save"><i class="glyphicon glyphicon-floppy-saved" aria-hidden="true"> Save</i></asp:LinkButton>
                                    </div>

                                </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd8" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2" data-content="อัพโหลดรูปภาพ">
                        <asp:FormView ID="FormView7" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <div class="panel panel-success">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <div class="col-md-12 custom-row content-room-list" style="margin-left: 7%; margin-top: -2%;">
                                                <div class="form-group" style="margin-top: 5%;">
                                                    <div class="col-md-12">
                                                        <asp:Repeater ID="Repeater0" runat="server" OnItemDataBound="R1_ItemDataBound" Visible="false">
                                                            <ItemTemplate>
                                                                <div class="row">
                                                                    <div class="form-group" style="margin-left: -15%;">
                                                                        <div class="col-md-12">
                                                                            <center><asp:Image ID="showIM" runat="server" class="img-responsive img-circle" ImageUrl='<%# retrunpat((int)Eval("EmpIDX"))%>' style="width: 200px; height: 200px" /></center>
                                                                        </div>
                                                                        <div class="col-md-12" style="margin-left: 0%;">
                                                                            <center><font color="#1A5276 "><small class="text_right"><b>Picture</b><p>รูปถ่าย</small></font></center>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <div class="form-group" style="margin-left: -15%;">
                                                            <asp:Panel ID="hiImNo" runat="server" Visible="true">
                                                                <div class="col-md-12">
                                                                    <center><asp:Image ID="Image1" runat="server" class="img-responsive img-circle" ImageUrl='<%# ResolveUrl("~/masterpage/images/Profile.png") %>' style="width: 200px; height: 200px" /></center>
                                                                </div>
                                                                <div class="col-md-12" style="margin-left: 0%;">
                                                                    <center><font color="#1A5276 "><small class="text_right"><b>Picture</b><p>รูปถ่าย</small></font></center>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="margin-left: 0%; margin-top: -20%;">
                                                    <div class="col-md-12">
                                                        <h4 class="list-group-item col-md-4">อัพโหลดรูปโปรไฟล์</h4>
                                                    </div>
                                                </div>
                                                <asp:UpdatePanel ID="UpdatePanelimmage" runat="server" UpdateMode="Conditional"
                                                    ChildrenAsTriggers="true">
                                                    <ContentTemplate>
                                                        <div class="form-group" style="margin-top: 3%; margin-left: 5%;">
                                                            <div class="col-md-10">
                                                                <label class="col-md-2 list-group-item-heading">
                                                                    <asp:Label ID="LabelUp" runat="server" ForeColor="DarkBlue" Text="Choose file"></asp:Label>
                                                                    <small>
                                                                        <p class="list-group-item-text">Upload File</p>
                                                                    </small>
                                                                </label>
                                                                <div class="col-md-6">
                                                                    <asp:FileUpload ID="UploadImages" ClientIDMode="Static" runat="server" CssClass="control-label" accept="jpg" />
                                                                    <p class="help-block"><font color="red">**รองรับไฟล์ภาพนามสกุล jpg เท่านั้น</font></p>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorup"
                                                                        ValidationGroup="formInsert" runat="server"
                                                                        Display="Dynamic"
                                                                        SetFocusOnError="true"
                                                                        ControlToValidate="UploadImages"
                                                                        Font-Size="11" ForeColor="Red"
                                                                        ErrorMessage="&nbsp;* กรุณาเลือกไฟล์"
                                                                        ValidationExpression="Please check File" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="margin-top: -1%; margin-left: 4%;">
                                                            <div class="col-md-3 text-right">
                                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" ValidationGroup="formInsert" CommandName="save" OnCommand="btnCommand" data-toggle="tooltip" title="อัพโหลด">Upload</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <br />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="LinkButton1" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                        <div class="col-md-6 text-right">
                                            <%--<asp:LinkButton ID="btbackadd7" CssClass="btn btn-primary" runat="server" CommandName="btbackadd7" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>--%>
                                        </div>

                                        <div class="col-md-6 text-left">
                                            <asp:LinkButton ID="save" CssClass="btn btn-primary" runat="server" CommandName="nextDoc" OnCommand="btnCommand" data-toggle="tooltip" title="Next"><i class="fa fa-arrow-circle-right" aria-hidden="true"> Next</i></asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd9" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2" data-content="อัพโหลดเอกสาร">
                        <asp:FormView ID="FormView8" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <div class="col-md-12">
                                    <h4 class="list-group-item col-md-4">อัพโหลดเอกสาร</h4>
                                </div>
                                <br />
                                <br />
                                <div class="form-group" style="margin-top: 4%; margin-left: 2%;">
                                    <div class="col-md-6">
                                        <label class="col-md-5 list-group-item-heading">
                                            <asp:Label ID="lblposition1" runat="server" ForeColor="DarkBlue" Text="ชื่อเอกสาร"></asp:Label>
                                            <small>
                                                <p class="list-group-item-text">Name Document</p>
                                            </small>
                                        </label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="DDTypeDoc" runat="server" CssClass="form-control" />
                                        </div>
                                        <%--<asp:RequiredFieldValidator ID="pageDoc"
                                            ValidationGroup="formInsert01"
                                            runat="server" Display="Dynamic"
                                            SetFocusOnError="true"
                                            ControlToValidate="DDTypeDoc"
                                            Font-Size="11" ForeColor="Red"
                                            ErrorMessage="&nbsp; &nbsp; &nbsp;* กรุณาเลือกประเภทเอกสาร"
                                            ValidationExpression="เลือกประเภทเอกสาร" InitialValue="00" />--%>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <asp:UpdatePanel ID="UpdatePanelDoc" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                        <div class="form-group" style="margin-top: 3%; margin-left: 2%;">
                                            <div class="col-md-6">
                                                <label class="col-md-5 list-group-item-heading">
                                                    <asp:Label ID="LabelUpFileDoc" runat="server" ForeColor="DarkBlue" Text="Choose file"></asp:Label>
                                                    <small>
                                                        <p class="list-group-item-text">Upload File</p>
                                                    </small>
                                                </label>
                                                <div class="col-md-7">
                                                    <asp:FileUpload ID="UploadDocfile" ClientIDMode="Static" runat="server" CssClass="control-label" accept="jpg" />
                                                    <p class="help-block"><font color="red">**รองรับไฟล์ภาพนามสกุล jpg เท่านั้น</font></p>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredDoc"
                                                        ValidationGroup="formInsert01" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="UploadDocfile"
                                                        Font-Size="11" ForeColor="Red"
                                                        ErrorMessage="&nbsp;* กรุณาเลือกไฟล์"
                                                        ValidationExpression="Please check File" />--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-offset-3">
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-success" runat="server" ValidationGroup="formInsert01" CommandName="savefile" OnCommand="btnCommand" data-toggle="tooltip" title="Save"><i class="glyphicon glyphicon-ok"></i> Save</asp:LinkButton>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="LinkButton2" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <hr class="so_sweethr006" />

                                <div class="form-horizontal" role="form">
                                    <div style="margin-top: 3%;">
                                        <h4>สรุปรายการ<small> (List Document)</small></h4>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10" style="margin-left: auto; margin-right: auto;">
                                        <asp:GridView ID="gvDocumentFile"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-CssClass="table_headCenter"
                                            DataKeyNames="File_IDX"
                                            HeaderStyle-Height="40px"
                                            ShowFooter="False"
                                            ShowHeaderWhenEmpty="True"
                                            AllowPaging="True"
                                            PageSize="5"
                                            BorderStyle="None"
                                            CellSpacing="2"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowDataBound="Gvbuyequipment_RowDataBound">
                                            <PagerStyle CssClass="PageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ชื่อเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <div class="panel-heading">
                                                            <asp:Label ID="TypeISO" runat="server" CssClass="col-sm-10" Text='<%# Eval("Name_Doc") %>'></asp:Label>
                                                            <asp:TextBox ID="EmpIDFile" runat="server" Visible="false" Text='<%# Eval("EmpIDX") %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ไฟล์" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" ID="btnDLX" CssClass="btn btn-default btn-sm" Target="_blank" data-original-title="เอกสาร" NavigateUrl='<%#  retrunpatFile((string)Eval("NameDoc")) %>'><i class="fa fa-eye"></i>&nbsp;ดูไฟล์</asp:HyperLink>
                                                        <asp:LinkButton ID="DeleteFile" runat="server" data-toggle="tooltip" OnCommand="btnCommand" CssClass="btn btn-danger" CommandName="Deletefile" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("File_IDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 4%; margin-bottom: -2%;">
                                    <div class="col-md-6 text-right">
                                        <asp:LinkButton ID="btbackadd1" CssClass="btn btn-primary" OnClientClick="javascript:scroll(0,0);" runat="server" CommandName="BackImmage" OnCommand="btnCommand" data-toggle="tooltip" title="back"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>

                                    <div class="col-md-6 text-left">
                                        <asp:LinkButton ID="btadd1" CssClass="btn btn-primary" OnClientClick="return confirm('คุณต้องการยืนยันใช่หรือไม่ ?')" runat="server" CommandName="Confirme" OnCommand="btnCommand" data-toggle="tooltip" title="ยืนยัน"><i class="glyphicon glyphicon-ok-sign" aria-hidden="true"> Confirm</i></asp:LinkButton>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd10" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2">
                        <asp:FormView ID="FormView9" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <br />
                                <div class="form-group text-center">
                                    <h3>ขั้นตอนการสมัครเสร็จสิ้นกรุณารอการติดต่อกลับจาก 
                                       <br />
                                        บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด(มหาชน)</h3>
                                </div>
                                <meta http-equiv="Refresh" content="9; url=http://www.taokaenoi.co.th/" />
                                <hr class="so_sweethr006" />
                                <div class="form-group text-center">
                                    <h6>กำลังปิดหน้าต่างนี้ในอีก...</h6>
                                    <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="http://www.taokaenoi.co.th/" CssClass="btn btn-default btn-sm" data-original-title="Skip" Target="_parent">Skip</asp:HyperLink>
                                    <img class="img-responsive center-block" width="100" height="100" src='<%= ResolveUrl("~/masterpage/images/countdown.gif") %>' />
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>


            <asp:View ID="ViewAdd11" runat="server">
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2">
                        <asp:FormView ID="FormView10" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <br />
                                <div class="form-group">
                                    <div class="text-center">
                                        <marquee direction="right" behavior="alternate" width="50%">
                                            <asp:Image ID="showIM" runat="server" ImageUrl='<%# ResolveUrl("~/masterpage/images/Head.png") %>' style="width: 280px; height: 120px"></asp:Image>
                                        </marquee>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <img src='<%= ResolveUrl("~/masterpage/images/RHB-1.png") %>' style="width: 90%; height: 120px; margin-top: -3%;" />
                                </div>
                                <br />
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <div class="panel panel-default" style="margin-left: 8%; margin-right: 8%;">
                                            <img src='<%= ResolveUrl("~/masterpage/images/header.png") %>' style="width: 100%; height: 100px;" />
                                            <h5>&nbsp;</h5>
                                            <div class=" col-md-offset-2 text-left">
                                                <h4>1. ข้อสอบมีทั้งหมด 35 ข้อ</h4>
                                                <h4>2. มีเวลาจำกัดในการทำ 30 นาที</h4>
                                                <h4 style="color: red; margin-left: 2.1%;">(หากทำข้อสอบไม่ทันตามเวลาที่กำหนด ระบบจะทำการยกเลิกการทำข้อสอบทันที)</h4>
                                                <h4>3. ข้อสอบจะปรากฏทีละข้อ เมื่อผู้สมัครงานเลือกคำตอบเสร็จสิ้นให้ทำการกดปุ่ม Next เพื่อข้อต่อไป</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="form-group">
                                    <div class="text-center">
                                        <asp:LinkButton ID="btnLogic" CssClass="btn btn-success btn-lg" runat="server" CommandName="btnLogic" Width="20%" OnCommand="btnCommand" data-toggle="tooltip" title="Start"><i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i>&nbsp; เริ่มต้นทำข้อสอบ</asp:LinkButton>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>

            <asp:View ID="ViewAdd12" runat="server">
                <script type="text/javascript">
                    var isNS = (navigator.appName == "Netscape") ? 1 : 0;

                    if (navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);

                    function mischandler() {
                        return false;
                    }

                    function mousehandler(e) {
                        var myevent = (isNS) ? e : event;
                        var eventbutton = (isNS) ? myevent.which : myevent.button;
                        if ((eventbutton == 2) || (eventbutton == 3)) return false;
                    }
                    document.oncontextmenu = mischandler;
                    document.onmousedown = mousehandler;
                    document.onmouseup = mousehandler;
                </script>
                <div class="col-md-12" role="main" style="margin-top: -2%;">
                    <div class="bs-docs-example2">
                        <asp:Literal ID="Literal3" runat="server"></asp:Literal><br />
                        <asp:Literal ID="Literal4" runat="server"></asp:Literal><br />
                        <asp:Literal ID="Literal5" runat="server"></asp:Literal><br />
                        <asp:TextBox ID="Sum" runat="server" Visible="false"></asp:TextBox>
                        <asp:FormView ID="FormView11" runat="server" DefaultMode="Insert" CssClass="table table-borderless">
                            <InsertItemTemplate>
                                <br />
                                <br />
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-9">
                                        </div>
                                        <div class="col-md-3 text-right">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                <ContentTemplate>
                                                    <div class="panel panel-default" style="margin-bottom: -8.5%; border-color: darkorange; background-color: darkorange; color: aliceblue">
                                                        <div style="margin-right: 5%;">
                                                            <b>
                                                                <h3>
                                                                    <asp:Panel ID="hindTime" runat="server" Visible="false">
                                                                        <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                                                                        </asp:Timer>
                                                                    </asp:Panel>
                                                                    <asp:Label ID="A1" runat="server" Text=""></asp:Label><asp:Label ID="TimCount" runat="server" />&nbsp;<asp:Label ID="Labelminite" runat="server" Text="นาที" />
                                                                </h3>
                                                            </b>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="Timer1" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <hr class="so_sweethr006" style="border-color: darkorange;" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="panel panel-default" style="margin-bottom: -0%; border-color: orange; background-color: orange;">
                                                <div style="margin-left: 3%;">
                                                    <b>
                                                        <h2>
                                                            <asp:Label ID="A2" runat="server" Text="Logic Test"></asp:Label>&nbsp; <i class="glyphicon glyphicon-pencil"></i></h2>
                                                    </b>
                                                    <h5>
                                                        <asp:Label ID="Label1" runat="server" Text="แบบทดสอบตรรกะ"></asp:Label></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:UpdatePanel ID="LogicOnline" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                        <ContentTemplate>
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <div class="panel panel-default">
                                                <h5>&nbsp;</h5>
                                                <h5>&nbsp;</h5>
                                                <div class="col-md-12">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-2 text-right">
                                                        <b>
                                                            <h4>
                                                                <asp:Label ID="A3" runat="server" Text="ข้อที่:"></asp:Label>&nbsp;<asp:Label ID="HChoin" runat="server" Text="0" />
                                                            </h4>
                                                        </b>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <asp:Repeater ID="RepeaterEmpMain1" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                            <ItemTemplate>
                                                                <asp:Panel ID="HindLogic" runat="server" Visible="false">
                                                                    <asp:Image ID="showIM" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpat((string)Eval("name_Logic"))%>' Style="width: 400px; height: 150px" />
                                                                </asp:Panel>
                                                                <asp:Panel ID="gg" runat="server" Visible="true">
                                                                    <asp:Image ID="Image5" runat="server" class="img-responsive img-thumbnail" ImageUrl="" Style="width: 500px; height: 150px" />
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <h5>&nbsp;</h5>
                                                <br />
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-1">
                                                            <asp:RadioButton ID="A" runat="server" Text="&nbsp;ก." GroupName="A" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="HindChoice" runat="server" Visible="false">
                                                                        <asp:Image ID="Image1" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpatA((string)Eval("name_Choice"))%>' Style="width: 90px; height: 90px" />
                                                                    </asp:Panel>
                                                                    <asp:TextBox ID="ATrue" runat="server" Text='<%# Eval("status_true") %>' Visible="false"></asp:TextBox>
                                                                    <asp:Panel ID="Panel1" runat="server" Visible="true">
                                                                        <asp:Image ID="Image6" runat="server" class="img-responsive img-thumbnail" ImageUrl="" Style="width: 110px; height: 110px" />
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-1">
                                                            <asp:RadioButton ID="B" runat="server" Text="&nbsp;ข." GroupName="A" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                                                                        <asp:Image ID="Image1" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpatB((string)Eval("name_Choice"))%>' Style="width: 90px; height: 90px" />
                                                                    </asp:Panel>
                                                                    <asp:TextBox ID="BTrue" runat="server" Text='<%# Eval("status_true") %>' Visible="false"></asp:TextBox>
                                                                    <asp:Panel ID="Panel3" runat="server" Visible="true">
                                                                        <asp:Image ID="Image7" runat="server" class="img-responsive img-thumbnail" ImageUrl="" Style="width: 110px; height: 110px" />
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6>&nbsp;</h6>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-1">
                                                            <asp:RadioButton ID="C" runat="server" Text="&nbsp;ค." GroupName="A" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Repeater ID="Repeater3" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="Panel5" runat="server" Visible="false">
                                                                        <asp:Image ID="Image1" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpatC((string)Eval("name_Choice"))%>' Style="width: 90px; height: 90px" />
                                                                    </asp:Panel>
                                                                    <asp:TextBox ID="CTrue" runat="server" Text='<%# Eval("status_true") %>' Visible="false"></asp:TextBox>
                                                                    <asp:Panel ID="Panel6" runat="server" Visible="true">
                                                                        <asp:Image ID="Image7" runat="server" class="img-responsive img-thumbnail" ImageUrl="" Style="width: 110px; height: 110px" />
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-1">
                                                            <asp:RadioButton ID="D" runat="server" Text="&nbsp;ง." GroupName="A" />
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Repeater ID="Repeater4" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                                <ItemTemplate>
                                                                    <asp:Panel ID="Panel7" runat="server" Visible="false">
                                                                        <asp:Image ID="Image1" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpatD((string)Eval("name_Choice"))%>' Style="width: 90px; height: 90px" />
                                                                    </asp:Panel>
                                                                    <asp:TextBox ID="DTrue" runat="server" Text='<%# Eval("status_true") %>' Visible="false"></asp:TextBox>
                                                                    <asp:Panel ID="Panel8" runat="server" Visible="true">
                                                                        <asp:Image ID="Image7" runat="server" class="img-responsive img-thumbnail" ImageUrl="" Style="width: 110px; height: 110px" />
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6>&nbsp;</h6>
                                                <br />
                                                <div class="form-group text-center">
                                                    <div class="col-md-12">
                                                        <div class="col-md-5"></div>
                                                        <div class="col-md-1">
                                                            <asp:Panel ID="hindback" runat="server" Visible="true">
                                                                <asp:LinkButton ID="BHChoin" CssClass="btn btn-success" runat="server" CommandName="BHChoin" OnCommand="btnCommand" data-toggle="tooltip" title="ย้อนกลับ"><i class="glyphicon glyphicon-chevron-left" aria-hidden="true"></i>&nbsp;Back</asp:LinkButton>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <asp:LinkButton ID="NHChoin" CssClass="btn btn-success" runat="server" CommandName="NHChoin" OnCommand="btnCommand" data-toggle="tooltip" title="ข้อต่อไป">Next&nbsp;<i class="glyphicon glyphicon-chevron-right" aria-hidden="true"></i></asp:LinkButton>
                                                        </div>
                                                        <div class="col-md-5"></div>
                                                    </div>
                                                </div>
                                                <h6>&nbsp;</h6>
                                            </div>
                                        </div>
                                    </div>
                                    </ContentTemplate>
                                        <Triggers>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </asp:View>

        </asp:MultiView>
    </div>
</asp:Content>

