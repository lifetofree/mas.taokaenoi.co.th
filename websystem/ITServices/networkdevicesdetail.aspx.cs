﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_networkdevicesdetail : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_network _data_network = new data_network();
    data_networkdevices _data_networkdevices = new data_networkdevices();

    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    static string _keynetworkdevices = ConfigurationManager.AppSettings["keynetworkdevices"];
    static string _urlGetNetworkIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetNetworkIndex"];

    static string _urlGetNetworkView = _serviceUrl + ConfigurationManager.AppSettings["urlGetNetworkView"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    int u0_devicenetwork_idx = 0;

    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        actionIndexEmp(); // เช็คคนเข้าระบบ


        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        Label lblPanelTitle = (Label)Master.FindControl("lblPanelTitle");
        if (lblPanelTitle != null)
        {

            lblPanelTitle.Visible = false;
        }
        else
        {
            ////lblTest.Text = "123123213123";
        }


        if (ViewState["emp_idx_emp"].ToString() == "1413" || ViewState["emp_idx_emp"].ToString() == "178" || ViewState["emp_idx_emp"].ToString() == "174" || ViewState["emp_idx_emp"].ToString() == "1394" || ViewState["emp_idx_emp"].ToString() == "1347" || ViewState["emp_idx_emp"].ToString() == "172" || ViewState["emp_idx_emp"].ToString() == "173" || ViewState["emp_idx_emp"].ToString() == "3760" || ViewState["emp_idx_emp"].ToString() == "1374" || ViewState["emp_idx_emp"].ToString() == "20140" || ViewState["emp_idx_emp"].ToString() == "3593")
        {

            emp_idx = int.Parse(Session["emp_idx"].ToString());

            //u0_devicenetwork_idx = int.Parse(Request.Form["emp_idx"].ToString());

            string _key_u0_devicenetwork_idx = Page.RouteData.Values["u0_devicenetwork_idx"].ToString().ToLower();

            string _u0_devicenetwork_idx = _funcTool.getDecryptRC4(_key_u0_devicenetwork_idx.ToString(), _keynetworkdevices);

            ViewState["_u0_devicenetwork_idx"] = _u0_devicenetwork_idx;

            lblTest.Text = _u0_devicenetwork_idx.ToString();

            showdetailsystem.Visible = true;

            // SelectQrCode();

            data_network _data_networkview = new data_network();
            _data_networkview.bindnetwork_list = new bindnetwork_detail[1];

            bindnetwork_detail _bindnetwork_detailview = new bindnetwork_detail();

            _bindnetwork_detailview.u0_devicenetwork_idx = int.Parse(_u0_devicenetwork_idx);

            _data_networkview.bindnetwork_list[0] = _bindnetwork_detailview;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

            _data_networkview = callServiceDataNetwork(_urlGetNetworkView, _data_networkview);


            FvDetailShowQRCode.DataSource = _data_networkview.bindnetwork_list;
            FvDetailShowQRCode.DataBind();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์เข้าดูรายละเอียด Network Devices');", true);


            showdetailpermissionsystem.Visible = true;

            //litShowDetail.Text = "คุณไม่มีสิทธิ์เข้าระบบ Network Devices";

            FvDetailShowQRCode.Visible = false;
        }
     
    
    }

    protected void SelectQrCode()
    {
        data_network _data_networkview = new data_network();
        _data_networkview.bindnetwork_list = new bindnetwork_detail[1];

        bindnetwork_detail _bindnetwork_detailview = new bindnetwork_detail();

        _bindnetwork_detailview.u0_devicenetwork_idx = int.Parse(ViewState["_u0_devicenetwork_idx"].ToString());

        _data_networkview.bindnetwork_list[0] = _bindnetwork_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networkview = callServiceDataNetwork(_urlGetNetworkView, _data_networkview);


        FvDetailShowQRCode.DataSource = _data_networkview.bindnetwork_list;
        FvDetailShowQRCode.DataBind();
    }

    #region selected Emp System   
    protected void actionIndexEmp() //เอาไว้ใช้กัน Link ตรง
    {

        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

        ViewState["emp_idx_emp"] = _data_employee.employee_list[0].emp_idx;
        ViewState["rdept_idx_emp"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx_emp"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected Emp System  

    #region get
    protected string getRoomname(string roomname_move)
    {
        if (roomname_move != null)
        {

            //FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");
            TextBox txtroom_nameqrcode = (TextBox)FvDetailShowQRCode.FindControl("txtroom_nameqrcode");

            return txtroom_nameqrcode.Text = roomname_move;


            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            //FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");
            TextBox txtroom_nameqrcode = (TextBox)FvDetailShowQRCode.FindControl("txtroom_nameqrcode");

            return txtroom_nameqrcode.Text = "-";//ViewState[roomname_move].ToString();
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getPass(String passwordview)
    {
        if (passwordview != null)
        {

            //FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            TextBox txtpass_wordqrcode = (TextBox)FvDetailShowQRCode.FindControl("txtpass_wordqrcode");

            string pass = _funcTool.getDecryptRC4(passwordview.ToString(), _keynetworkdevices);

            return txtpass_wordqrcode.Text = pass;
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            //FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            TextBox txtpass_wordqrcode = (TextBox)FvDetailShowQRCode.FindControl("txtpass_wordqrcode");

            //string pass = _funcTool.getDecryptRC4(txtpass_wordview.ToString(), "password");

            return txtpass_wordqrcode.Text = "-";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion get

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailShowQRCode":

                FormView FvDetailShowQRCode = (FormView)FindControl("FvDetailShowQRCode");
            break;

        }

    }


    #endregion FvDetail_DataBound

    #region bind data
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {

            case "FvDetailShowCut":


                //setFormData(FvDetailShowCut, FormViewMode.Insert, _data_networkdetailcut.bindcutnetwork_list, 0);

                break;

        }
    }


    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_network callServiceDataNetwork(string _cmdUrl, data_network _data_network)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_network);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_network = (data_network)_funcTool.convertJsonToObject(typeof(data_network), _localJson);

        return _data_network;
    }

    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _data_networkdevices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_networkdevices);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_networkdevices = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _data_networkdevices;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    #endregion reuse
}