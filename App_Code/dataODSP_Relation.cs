﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using System.Xml.Serialization;
/// <summary>
/// Summary description for dataODSP_Reration
/// </summary>
public class dataODSP_Relation
{
    [XmlElement("AlertMessage")]
    public string AlertMessage { get; set; }
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    public string CountRows { get; set; }
    public string ReturnCount { get; set; }

    [XmlElement("detailRelation_ODSP")]
    public Relation_ODSP[] detailRelation_ODSP { get; set; }


}

[Serializable]
public class Relation_ODSP  /*  Relation_ODSP DataBox  */
{
    [XmlElement("K1")]
    public int K1 { get; set; }
    [XmlElement("K2")]
    public int K2 { get; set; }

    [XmlElement("emp_idx_position")]
    public int emp_idx_position { get; set; }
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }
    [XmlElement("OrgNameTH")]
    private string _OrgNameTH;
    public string OrgNameTH { get; set; }
    [XmlElement("OrgNameEN")]
    private string _OrgNameEN;
    public string OrgNameEN { get; set; }


    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }
    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }
    [XmlElement("DeptNameTH")]
    private string _DeptNameTH;
    public string DeptNameTH { get; set; }
    [XmlElement("DeptNameEN")]
    private string _DeptNameEN;
    public string DeptNameEN { get; set; }


    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }
    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }
    [XmlElement("SecNameTH")]
    private string _SecNameTH;
    public string SecNameTH { get; set; }
    [XmlElement("SecNameEN")]
    private string _SecNameEN;
    public string SecNameEN { get; set; }



    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }
    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }
    [XmlElement("PosNameTH")]
    private string _PosNameTH;
    public string PosNameTH { get; set; }
    [XmlElement("PosNameEN")]
    private string _PosNameEN;
    public string PosNameEN { get; set; }

    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("EmpIDXUser")]
    public int EmpIDXUser { get; set; }
    [XmlElement("EStatus")]
    public int EStatus { get; set; }
    [XmlElement("CreateDate")]
    private string _CreateDate;
    public string CreateDate { get; set; }
    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }

    [XmlElement("JobGradeIDX1")]
    public int JobGradeIDX1 { get; set; }

    [XmlElement("JobGradeIDX2")]
    public int JobGradeIDX2 { get; set; }

    [XmlElement("FullNameTH")]
    private string _FullNameTH;
    public string FullNameTH { get; set; }


}
