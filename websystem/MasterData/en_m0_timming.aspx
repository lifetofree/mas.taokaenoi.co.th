﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="en_m0_timming.aspx.cs" Inherits="websystem_MasterData_en_m0_timming" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="text" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; Time (ระยะเวลาที่ตรวจสอบ)</strong></h3>
                        </div>

                        <div class="panel-body">

                            <div id="SETBoxAllSearch" runat="server">
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="Time :" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsearchtime" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-5">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnRefresh" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>


                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddTime" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Time</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="ความถี่ในการเข้า PM" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtnameth" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtnameth" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูลไทย"
                                                    ValidationExpression="กรุณากรอกข้อมูลไทย"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtnameth"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="คำย่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtnameen" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtnameen" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูลอังกฤษ"
                                                    ValidationExpression="กรุณากรอกข้อมูลอังกฤษ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtnameen"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="ความถี่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtqty" CssClass="form-control" runat="server"> </asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="txtqty" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" />
                                                    <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtqty"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="คำนวณรอบแบบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlunit" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                        <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                        <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                        <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                    </asp:DropDownList>
                                                    <%--   <asp:TextBox ID="txtunit" runat="server" CssClass="form-control" PlaceHolder="........" />--%>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunit" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกข้อมูล"
                                                    ValidationExpression="กรุณากรอกข้อมูล"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="แจ้งเตือนกรณีดำเนินการ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlunitday" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                        <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                        <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                        <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitday" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlday" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlday" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label6" runat="server" Text="แจ้งเตือนให้ Director" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:DropDownList ID="ddlunitdirector" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                        <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                        <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                        <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitdirector" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddldirec" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddldirec" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label7" runat="server" Text="แจ้งเตือนให้ COO" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:DropDownList ID="ddlunitcoo" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                        <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                        <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                        <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitcoo" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlcoo" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlcoo" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0tidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblm0tidx" runat="server" Visible="false" Text='<%# Eval("m0tidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtm0tidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0tidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ความถี่ในการเข้า PM" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtnameth_edit" runat="server" CssClass="form-control" Text='<%# Eval("time_desc")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtnameth_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูลภาษาไทย"
                                                            ValidationExpression="กรุณากรอกข้อมูลภาษาไทย"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtnameth_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="คำย่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtnameen_edit" runat="server" CssClass="form-control" Text='<%# Eval("time_short")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtnameen_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูลอังกฤษ"
                                                            ValidationExpression="กรุณากรอกข้อมูลอังกฤษ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtnameen_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                    </div>



                                                    <div class="form-group">
                                                        <asp:Label ID="Label5" runat="server" Text="ความถี่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txteditqty" CssClass="form-control" runat="server" Text='<%# Eval("qty")%>'> </asp:TextBox>

                                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="txteditqty" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" />
                                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                ControlToValidate="txteditqty"
                                                                ValidationExpression="^[0-9]{1,10}$" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" Text="คำนวณรอบแบบ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txteditunit" runat="server" Text='<%# Eval("unit")%>' CssClass="form-control" PlaceHolder="........" />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txteditunit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกข้อมูล"
                                                            ValidationExpression="กรุณากรอกข้อมูล"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                            ValidationGroup="Save" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txteditunit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label4" runat="server" Text="แจ้งเตือนกรณีดำเนินการ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                             <asp:Label ID="lblunitday_edit" runat="server" Text='<%# Bind("unit_doing") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlunitday_edit" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                                <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                                <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                                <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                                <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitday_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                        </div>
                                                        <div class="col-sm-3">
                                                               <asp:Label ID="lblday_edit" runat="server" Text='<%# Bind("count_doing") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlday_edit" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlday_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="แจ้งเตือนให้ Director" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                              <asp:Label ID="lblunitdirector_edit" runat="server" Text='<%# Bind("unit_director") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlunitdirector_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                                <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                                <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                                <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitdirector_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                        </div>
                                                        <div class="col-sm-3">
                                                             <asp:Label ID="lbldirec_edit" runat="server" Text='<%# Bind("count_director") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddldirec_edit" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddldirec_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                        </div>

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="แจ้งเตือนให้ COO" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                               <asp:Label ID="lblunitcoo_edit" runat="server" Text='<%# Bind("unit_coo") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlunitcoo_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                                                                <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                                                                <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                                                                <asp:ListItem Text="Year" Value="Year"></asp:ListItem>

                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlunitcoo_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                        </div>
                                                        <div class="col-sm-3">
                                                              <asp:Label ID="lblcoo_edit" runat="server" Text='<%# Bind("count_coo") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlcoo_edit" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="ddlcoo_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("time_status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ความถี่ในการเข้า PM" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("time_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คำย่อ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("time_short") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความถี่" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbqty" runat="server" Text='<%# Eval("qty") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คำนวณรอบแบบ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbunit" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="แจ้งเตือนกรณีดำเนินการ" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbalertdoing" runat="server" Text='<%# Eval("alertdoing") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="แจ้งเตือนให้กับ Director" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbalertdirector" runat="server" Text='<%# Eval("alertdirector") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="แจ้งเตือนให้กับ COO" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbalertcoo" runat="server" Text='<%# Eval("alertcoo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("TimingStatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0tidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

