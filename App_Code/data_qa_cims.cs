﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_qa_cims
/// </summary>
/// 

[Serializable]
[XmlRoot("data_qa_cims")]

public class data_qa_cims
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // master data //
    [XmlElement("qa_cims_m0_unit_list")]
    public qa_cims_m0_unit_detail[] qa_cims_m0_unit_list { get; set; }
    [XmlElement("qa_cims_m0_equipment_type_list")]
    public qa_cims_m0_equipment_type_detail[] qa_cims_m0_equipment_type_list { get; set; }
    [XmlElement("qa_cims_m0_place_list")]
    public qa_cims_m0_place_detail[] qa_cims_m0_place_list { get; set; }
    [XmlElement("qa_cims_m0_cal_type_list")]
    public qa_cims_m0_cal_type_detail[] qa_cims_m0_cal_type_list { get; set; }
    [XmlElement("qa_cims_m0_equipment_result_list")]
    public qa_cims_m0_equipment_result_detail[] qa_cims_m0_equipment_result_list { get; set; }
    [XmlElement("qa_cims_m0_brand_list")]
    public qa_cims_m0_brand_detail[] qa_cims_m0_brand_list { get; set; }
    [XmlElement("qa_cims_m0_equipment_name_list")]
    public qa_cims_m0_equipment_name_detail[] qa_cims_m0_equipment_name_list { get; set; }
    [XmlElement("qa_cims_m0_investigate_list")]
    public qa_cims_m0_investigate_detail[] qa_cims_m0_investigate_list { get; set; }
    [XmlElement("qa_cims_m0_setname_list")]
    public qa_cims_m0_setname_detail[] qa_cims_m0_setname_list { get; set; }
    [XmlElement("qa_cims_m0_form_detail_list")]
    public qa_cims_m0_form_detail_detail[] qa_cims_m0_form_detail_list { get; set; }
    [XmlElement("qa_cims_m0_lab_list")]
    public qa_cims_m0_lab_detail[] qa_cims_m0_lab_list { get; set; }
    [XmlElement("qa_cims_m1_lab_list")]
    public qa_cims_m1_lab_detail[] qa_cims_m1_lab_list { get; set; }
    [XmlElement("qa_cims_r0_form_create_list")]
    public qa_cims_r0_form_create_detail[] qa_cims_r0_form_create_list { get; set; }
    [XmlElement("qa_cims_r1_form_create_list")]
    public qa_cims_r1_form_create_detail[] qa_cims_r1_form_create_list { get; set; }
    [XmlElement("qa_cims_log_registration_device_list")]
    public qa_cims_log_registration_device_detail[] qa_cims_log_registration_device_list { get; set; }
    [XmlElement("qa_cims_search_registration_device_list")]
    public qa_cims_search_registration_device[] qa_cims_search_registration_device_list { get; set; }

    [XmlElement("qa_cims_m0_resolution_list")]
    public qa_cims_m0_resulution_detail[] qa_cims_m0_resolution_list { get; set; }

    [XmlElement("qa_cims_m0_frequency_list")]
    public qa_cims_m0_frequency_detail[] qa_cims_m0_frequency_list { get; set; }

    [XmlElement("qa_cims_m0_rangeuse_list")]
    public qa_cims_m0_rangeuse_detail[] qa_cims_m0_rangeuse_list { get; set; }

    [XmlElement("qa_cims_m0_measuring_list")]
    public qa_cims_m0_measuring_detail[] qa_cims_m0_measuring_list { get; set; }

    [XmlElement("qa_cims_m0_acceptance_list")]
    public qa_cims_m0_acceptance_detail[] qa_cims_m0_acceptance_list { get; set; }

    [XmlElement("qa_cims_m0_location_list")]
    public qa_cims_m0_location_detail[] qa_cims_m0_location_list { get; set; }

    [XmlElement("qa_cims_m0_statusdevices_list")]
    public qa_cims_m0_statusdevices_detail[] qa_cims_m0_statusdevices_list { get; set; }

    // master data registration device //

    [XmlElement("qa_cims_m0_registration_device_list")]
    public qa_cims_m0_registration_device[] qa_cims_m0_registration_device_list { get; set; }

    [XmlElement("qa_cims_m1_registration_device_list")]
    public qa_cims_m1_registration_device[] qa_cims_m1_registration_device_list { get; set; }

    [XmlElement("qa_cims_m2_registration_device_list")]
    public qa_cims_m2_registration_device[] qa_cims_m2_registration_device_list { get; set; }

    [XmlElement("qa_cims_m3_registration_device_list")]
    public qa_cims_m3_registration_device[] qa_cims_m3_registration_device_list { get; set; }

    [XmlElement("qa_cims_m4_registration_device_list")]
    public qa_cims_m4_registration_device[] qa_cims_m4_registration_device_list { get; set; }

    [XmlElement("qa_cims_m5_registration_device_list")]
    public qa_cims_m5_registration_device[] qa_cims_m5_registration_device_list { get; set; }

    [XmlElement("qa_cims_m6_registration_device_list")]
    public qa_cims_m6_registration_device[] qa_cims_m6_registration_device_list { get; set; }

    //dacument data //

    [XmlElement("qa_cims_bindnode_decision_list")]
    public qa_cims_bindnode_decision_detail[] qa_cims_bindnode_decision_list { get; set; }

    [XmlElement("qa_cims_u0_document_device_list")]
    public qa_cims_u0_document_device_detail[] qa_cims_u0_document_device_list { get; set; }

    [XmlElement("qa_cims_u1_document_device_list")]
    public qa_cims_u1_document_device_detail[] qa_cims_u1_document_device_list { get; set; }

    [XmlElement("qa_cims_u2_document_device_list")]
    public qa_cims_u2_document_device_detail[] qa_cims_u2_document_device_list { get; set; }

    //document calibation equipment
    [XmlElement("qa_cims_u0_calibration_document_list")]
    public qa_cims_u0_calibration_document_details[] qa_cims_u0_calibration_document_list { get; set; }

    [XmlElement("qa_cims_u1_calibration_document_list")]
    public qa_cims_u1_calibration_document_details[] qa_cims_u1_calibration_document_list { get; set; }

    [XmlElement("qa_cims_u2_calibration_document_list")]
    public qa_cims_u2_calibration_document_details[] qa_cims_u2_calibration_document_list { get; set; }

    [XmlElement("qa_cims_m0_calibration_registration_list")]
    public qa_cims_m0_calibration_registration_details[] qa_cims_m0_calibration_registration_list { get; set; }

    [XmlElement("qa_cims_calibration_document_list")]
    public qa_cims_calibration_document_details[] qa_cims_calibration_document_list { get; set; }

    [XmlElement("qa_cims_u1_search_document_device_list")]
    public qa_cims_u1_search_document_device_detail[] qa_cims_u1_search_document_device_list { get; set; }


    [XmlElement("qa_cims_bind_topics_form_list")]
    public qa_cims_bind_topics_form_details[] qa_cims_bind_topics_form_list { get; set; }

    //record result to form by form || record by value

    [XmlElement("qa_cims_record_topics_list")]
    public qa_cims_record_topics_details[] qa_cims_record_topics_list { get; set; }

    [XmlElement("qa_cims_record_result_by_form_list")]
    public qa_cims_record_result_by_form_details[] qa_cims_record_result_by_form_list { get; set; }

    //master form name :: New !
    [XmlElement("qa_cims_m0_form_calculate_list")]
    public qa_cims_m0_form_calculate_details[] qa_cims_m0_form_calculate_list { get; set; }

    [XmlElement("qa_cims_m1_form_calculate_list")]
    public qa_cims_m1_form_calculate_details[] qa_cims_m1_form_calculate_list { get; set; }

    [XmlElement("qa_cims_m2_form_calculate_list")]
    public qa_cims_m2_form_calculate_details[] qa_cims_m2_form_calculate_list { get; set; }

    [XmlElement("qa_cims_m0_per_management_list")]
    public qa_cims_m0_per_management_detail[] qa_cims_m0_per_management_list { get; set; }

    //Report Devices Tool
    [XmlElement("qa_cims_reportdevices_list")]
    public qa_cims_reportdevices_detail[] qa_cims_reportdevices_list { get; set; }

    //per qa
    [XmlElement("qa_cims_m0_management_list")]
    public qa_cims_m0_management_detail[] qa_cims_m0_management_list { get; set; }

    [XmlElement("qa_cims_m1_management_list")]
    public qa_cims_m1_management_detail[] qa_cims_m1_management_list { get; set; }

    [XmlElement("qa_cims_m2_management_list")]
    public qa_cims_m2_management_detail[] qa_cims_m2_management_list { get; set; }



}

[Serializable]
public class qa_cims_m0_management_detail
{
    [XmlElement("m0_management_idx")]
    public int m0_management_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m0_management_status")]
    public int m0_management_status { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("pos_name_th_per")]
    public string pos_name_th_per { get; set; }

    [XmlElement("sec_name_th_per")]
    public string sec_name_th_per { get; set; }


    [XmlElement("dept_name_th_per")]
    public string dept_name_th_per { get; set; }

    [XmlElement("org_name_th_per")]
    public string org_name_th_per { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx_check_insertregis")]
    public string place_idx_check_insertregis { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }


}

[Serializable]
public class qa_cims_m1_management_detail
{
    [XmlElement("m0_management_idx")]
    public int m0_management_idx { get; set; }

    [XmlElement("m1_management_idx")]
    public int m1_management_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m1_management_status")]
    public int m1_management_status { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }


}

[Serializable]
public class qa_cims_m2_management_detail
{
    [XmlElement("m0_management_idx")]
    public int m0_management_idx { get; set; }

    [XmlElement("m0_per_management_idx")]
    public int m0_per_management_idx { get; set; }

    [XmlElement("m2_management_idx")]
    public int m2_management_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m2_management_status")]
    public int m2_management_status { get; set; }

    [XmlElement("per_management_name")]
    public string per_management_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}



[Serializable]
public class qa_cims_m0_per_management_detail
{
    [XmlElement("m0_per_management_idx")]
    public int m0_per_management_idx { get; set; }

    [XmlElement("per_management_name")]
    public string per_management_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("per_management_status")]
    public int per_management_status { get; set; }


}

[Serializable]
public class qa_cims_reportdevices_detail
{
    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m1_device_idx")]
    public int m1_device_idx { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("device_org_idx")]
    public int device_org_idx { get; set; }

    [XmlElement("device_rdept_idx")]
    public int device_rdept_idx { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("device_cal_date")]
    public string device_cal_date { get; set; }

    [XmlElement("device_due_date")]
    public string device_due_date { get; set; }

    [XmlElement("device_status")]
    public int device_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("brand_idx")]
    public int brand_idx { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("range_of_use_min")]
    public decimal range_of_use_min { get; set; }

    [XmlElement("range_of_use_max")]
    public decimal range_of_use_max { get; set; }

    [XmlElement("unit_idx_range_of_use")]
    public int unit_idx_range_of_use { get; set; }

    [XmlElement("equipment_type_idx")]
    public int equipment_type_idx { get; set; }

    [XmlElement("resolution_detail")]
    public decimal resolution_detail { get; set; }

    [XmlElement("unit_idx_resolution")]
    public int unit_idx_resolution { get; set; }

    [XmlElement("measuring_range_min")]
    public decimal measuring_range_min { get; set; }

    [XmlElement("measuring_range_max")]
    public decimal measuring_range_max { get; set; }

    [XmlElement("unit_idx_measuring_range")]
    public int unit_idx_measuring_range { get; set; }

    [XmlElement("acceptance_criteria")]
    public decimal acceptance_criteria { get; set; }

    [XmlElement("calibration_frequency")]
    public decimal calibration_frequency { get; set; }

    [XmlElement("calibration_frequency_unit")]
    public int calibration_frequency_unit { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("device_rsec")]
    public string device_rsec { get; set; }

    [XmlElement("acceptance_criteria_unit_idx")]
    public int acceptance_criteria_unit_idx { get; set; }

    [XmlElement("device_details")]
    public string device_details { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("device_org")]
    public string device_org { get; set; }

    [XmlElement("device_rdept")]
    public string device_rdept { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("unit_name_Res")]
    public string unit_name_Res { get; set; }

    [XmlElement("unit_name_ROU")]
    public string unit_name_ROU { get; set; }

    [XmlElement("unit_name_CF")]
    public string unit_name_CF { get; set; }

    [XmlElement("unit_name_MR")]
    public string unit_name_MR { get; set; }

    [XmlElement("unit_name_CP")]
    public string unit_name_CP { get; set; }

    [XmlElement("unit_name_AC")]
    public string unit_name_AC { get; set; }

    [XmlElement("calibration_point")]
    public string calibration_point { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("list_m0_device")]
    public string list_m0_device { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("device_place_idx")]
    public int device_place_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("resolution_name_value")]
    public string resolution_name_value { get; set; }

    [XmlElement("detail_frequency")]
    public string detail_frequency { get; set; }

    [XmlElement("range_value")]
    public string range_value { get; set; }

    [XmlElement("measuring_value")]
    public string measuring_value { get; set; }

    [XmlElement("acceptance_value")]
    public string acceptance_value { get; set; }

    [XmlElement("equipment_frequency_idx")]
    public int equipment_frequency_idx { get; set; }

    [XmlElement("equipment_resolution_idx")]
    public int equipment_resolution_idx { get; set; }

    [XmlElement("m3_device_frequency_idx")]
    public int m3_device_frequency_idx { get; set; }

    [XmlElement("receive_devices")]
    public string receive_devices { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("m0_status_idx")]
    public int m0_status_idx { get; set; }

    [XmlElement("m1_location_idx")]
    public int m1_location_idx { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("location_zone")]
    public string location_zone { get; set; }

    [XmlElement("certificate_detail")]
    public string certificate_detail { get; set; }

    [XmlElement("count_devices")]
    public int count_devices { get; set; }

}

[Serializable]
public class qa_cims_m0_statusdevices_detail
{
    [XmlElement("m0_status_idx")]
    public int m0_status_idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}


[Serializable]
public class qa_cims_m0_unit_detail
{
    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("unit_name_th")]
    public string unit_name_th { get; set; }

    [XmlElement("unit_name_en")]
    public string unit_name_en { get; set; }

    [XmlElement("unit_symbol_th")]
    public string unit_symbol_th { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("unit_status")]
    public int unit_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class qa_cims_m0_acceptance_detail
{
    [XmlElement("m6_device_acceptance_idx")]
    public int m6_device_acceptance_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("acceptance_criteria")]
    public string acceptance_criteria { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }

}

[Serializable]
public class qa_cims_m0_frequency_detail
{
    [XmlElement("equipment_frequency_idx")]
    public int equipment_frequency_idx { get; set; }

    [XmlElement("frequency_count")]
    public string frequency_count { get; set; }

    [XmlElement("frequency_unit")]
    public int frequency_unit { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("frequency_status")]
    public int frequency_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("detail_frequency")]
    public string detail_frequency { get; set; }

    [XmlElement("unit_frequency")]
    public string unit_frequency { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

}

[Serializable]
public class qa_cims_m0_rangeuse_detail
{
    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m4_device_range_idx")]
    public int m4_device_range_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("range_start")]
    public string range_start { get; set; }

    [XmlElement("range_end")]
    public string range_end { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }


}

[Serializable]
public class qa_cims_m0_measuring_detail
{
    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m5_device_measuring_idx")]
    public int m5_device_measuring_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("measuring_start")]
    public string measuring_start { get; set; }

    [XmlElement("measuring_end")]
    public string measuring_end { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }


}

[Serializable]
public class qa_cims_m0_resulution_detail
{
    [XmlElement("equipment_resolution_idx")]
    public int equipment_resolution_idx { get; set; }

    [XmlElement("resolution_name")]
    public string resolution_name { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("resolution_status")]
    public int resolution_status { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("resolution_value")]
    public string resolution_value { get; set; }

    [XmlElement("m2_device_resolution_idx")]
    public int m2_device_resolution_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }


}

[Serializable]
public class qa_cims_m0_equipment_type_detail
{
    [XmlElement("equipment_type_idx")]
    public int equipment_type_idx { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("equipment_type_status")]
    public int equipment_type_status { get; set; }
}

[Serializable]
public class qa_cims_m0_place_detail
{
    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("place_code")]
    public string place_code { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("place_status")]
    public int place_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("place_idx_check_insert_regis")]
    public string place_idx_check_insert_regis { get; set; }
}

[Serializable]
public class qa_cims_m0_cal_type_detail
{
    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("cal_type_name")]
    public string cal_type_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cal_type_status")]
    public int cal_type_status { get; set; }
}

[Serializable]
public class qa_cims_m0_equipment_result_detail
{
    [XmlElement("equipment_result_idx")]
    public int equipment_result_idx { get; set; }

    [XmlElement("equipment_result")]
    public string equipment_result { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("equipment_result_status")]
    public int equipment_result_status { get; set; }
}

[Serializable]
public class qa_cims_m0_brand_detail
{
    [XmlElement("brand_idx")]
    public int brand_idx { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("brand_status")]
    public int brand_status { get; set; }
}

[Serializable]
public class qa_cims_m0_equipment_name_detail
{
    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("equipment_status")]
    public int equipment_status { get; set; }
}

[Serializable]
public class qa_cims_m0_investigate_detail
{
    [XmlElement("investigate_idx")]
    public int investigate_idx { get; set; }

    [XmlElement("investigate_detail")]
    public string investigate_detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("investigate_status")]
    public int investigate_status { get; set; }
}

[Serializable]
public class qa_cims_m0_setname_detail
{
    [XmlElement("setname_idx")]
    public int setname_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("set_detail")]
    public string set_detail { get; set; }

    [XmlElement("setroot_idx")]
    public int setroot_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("setname_status")]
    public int setname_status { get; set; }
}

[Serializable]
public class qa_cims_m0_form_detail_detail
{
    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("form_detail")]
    public string form_detail { get; set; }

    [XmlElement("form_detail_root_idx")]
    public int form_detail_root_idx { get; set; }

    [XmlElement("set_idx")]
    public int set_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

    [XmlElement("option_idx")]
    public int option_idx { get; set; }

    [XmlElement("option_name")]
    public string option_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("form_detail_status")]
    public int form_detail_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }
}

[Serializable]
public class qa_cims_m0_lab_detail
{
    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("lab_status")]
    public int lab_status { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("cal_type_name")]
    public string cal_type_name { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

}

[Serializable]
public class qa_cims_m1_lab_detail
{
    [XmlElement("m1_lab_idx")]
    public int m1_lab_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("status")]
    public string status { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

}

[Serializable]
public class qa_cims_r0_form_create_detail
{
    [XmlElement("r0_form_create_idx")]
    public int r0_form_create_idx { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("r0_form_create_status")]
    public int r0_form_create_status { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("r1_form_root_idx")]
    public int r1_form_root_idx { get; set; }
}

[Serializable]
public class qa_cims_r1_form_create_detail
{
    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("r0_form_create_idx")]
    public int r0_form_create_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("r1_form_create_status")]
    public int r1_form_create_status { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("r1_form_root_idx")]
    public int r1_form_root_idx { get; set; }

    [XmlElement("text_name_setform")]
    public string text_name_setform { get; set; }

}

[Serializable]
public class qa_cims_m0_registration_device
{
    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m1_device_idx")]
    public int m1_device_idx { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("device_org_idx")]
    public int device_org_idx { get; set; }

    [XmlElement("device_rdept_idx")]
    public int device_rdept_idx { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("device_cal_date")]
    public string device_cal_date { get; set; }

    [XmlElement("device_due_date")]
    public string device_due_date { get; set; }

    [XmlElement("device_status")]
    public int device_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("brand_idx")]
    public int brand_idx { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("range_of_use_min")]
    public decimal range_of_use_min { get; set; }

    [XmlElement("range_of_use_max")]
    public decimal range_of_use_max { get; set; }

    [XmlElement("unit_idx_range_of_use")]
    public int unit_idx_range_of_use { get; set; }

    [XmlElement("equipment_type_idx")]
    public int equipment_type_idx { get; set; }

    [XmlElement("resolution_detail")]
    public decimal resolution_detail { get; set; }

    [XmlElement("unit_idx_resolution")]
    public int unit_idx_resolution { get; set; }

    [XmlElement("measuring_range_min")]
    public decimal measuring_range_min { get; set; }

    [XmlElement("measuring_range_max")]
    public decimal measuring_range_max { get; set; }

    [XmlElement("unit_idx_measuring_range")]
    public int unit_idx_measuring_range { get; set; }

    [XmlElement("acceptance_criteria")]
    public decimal acceptance_criteria { get; set; }

    [XmlElement("calibration_frequency")]
    public decimal calibration_frequency { get; set; }

    [XmlElement("calibration_frequency_unit")]
    public int calibration_frequency_unit { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("device_rsec")]
    public string device_rsec { get; set; }

    [XmlElement("acceptance_criteria_unit_idx")]
    public int acceptance_criteria_unit_idx { get; set; }

    [XmlElement("device_details")]
    public string device_details { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("device_org")]
    public string device_org { get; set; }

    [XmlElement("device_rdept")]
    public string device_rdept { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("unit_name_Res")]
    public string unit_name_Res { get; set; }

    [XmlElement("unit_name_ROU")]
    public string unit_name_ROU { get; set; }

    [XmlElement("unit_name_CF")]
    public string unit_name_CF { get; set; }

    [XmlElement("unit_name_MR")]
    public string unit_name_MR { get; set; }

    [XmlElement("unit_name_CP")]
    public string unit_name_CP { get; set; }

    [XmlElement("unit_name_AC")]
    public string unit_name_AC { get; set; }

    [XmlElement("calibration_point")]
    public string calibration_point { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("list_m0_device")]
    public string list_m0_device { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("device_place_idx")]
    public int device_place_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("resolution_name_value")]
    public string resolution_name_value { get; set; }

    [XmlElement("detail_frequency")]
    public string detail_frequency { get; set; }

    [XmlElement("range_value")]
    public string range_value { get; set; }

    [XmlElement("measuring_value")]
    public string measuring_value { get; set; }

    [XmlElement("acceptance_value")]
    public string acceptance_value { get; set; }

    [XmlElement("equipment_frequency_idx")]
    public int equipment_frequency_idx { get; set; }

    [XmlElement("equipment_resolution_idx")]
    public int equipment_resolution_idx { get; set; }

    [XmlElement("m3_device_frequency_idx")]
    public int m3_device_frequency_idx { get; set; }

    [XmlElement("receive_devices")]
    public string receive_devices { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("m0_status_idx")]
    public int m0_status_idx { get; set; }

    [XmlElement("m1_location_idx")]
    public int m1_location_idx { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("location_zone")]
    public string location_zone { get; set; }

    [XmlElement("certificate_detail")]
    public string certificate_detail { get; set; }

}

[Serializable]
public class qa_cims_m1_registration_device
{

    [XmlElement("m1_device_idx")]
    public int m1_device_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("calibration_point")]
    public string calibration_point { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("unit_symbol_en")]
    public string unit_symbol_en { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("device_serial_no")]
    public string device_serial_no { get; set; }

}

[Serializable]
public class qa_cims_m2_registration_device
{
    [XmlElement("equipment_resolution_idx")]
    public int equipment_resolution_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m2_device_resolution_idx")]
    public int m2_device_resolution_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

}

[Serializable]
public class qa_cims_m3_registration_device
{
    [XmlElement("equipment_frequency_idx")]
    public int equipment_frequency_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }
}

[Serializable]
public class qa_cims_m4_registration_device
{

    [XmlElement("m4_device_range_idx")]
    public int m4_device_range_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("range_start")]
    public string range_start { get; set; }

    [XmlElement("range_end")]
    public string range_end { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m5_registration_device
{

    [XmlElement("m5_device_measduring_ix")]
    public int m5_device_measduring_ix { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("measuring_start")]
    public string measuring_start { get; set; }

    [XmlElement("measuring_end")]
    public string measuring_end { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class qa_cims_m6_registration_device
{

    [XmlElement("m6_device_acceptance_idx")]
    public int m6_device_acceptance_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("acceptance_criteria")]
    public string acceptance_criteria { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("unit_idx")]
    public int unit_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class qa_cims_bindnode_decision_detail
{

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("decision_status")]
    public int decision_status { get; set; }

    [XmlElement("nodidx")]
    public int nodidx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("decision_type")]
    public int decision_type { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }
}

[Serializable]
public class qa_cims_u0_document_device_detail
{
    [XmlElement("u0_device_idx")]
    public int u0_device_idx { get; set; }

    [XmlElement("document_type_idx")]
    public int document_type_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("org_idx_tranfer")]
    public int org_idx_tranfer { get; set; }

    [XmlElement("rdept_idx_tranfer")]
    public int rdept_idx_tranfer { get; set; }

    [XmlElement("rsec_idx_tranfer")]
    public int rsec_idx_tranfer { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("document_type_name")]
    public string document_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("emp_email_create")]
    public string emp_email_create { get; set; }

    [XmlElement("emp_name_th_create")]
    public string emp_name_th_create { get; set; }

    [XmlElement("org_name_th_create")]
    public string org_name_th_create { get; set; }

    [XmlElement("dept_name_th_create")]
    public string dept_name_th_create { get; set; }

    [XmlElement("sec_name_th_create")]
    public string sec_name_th_create { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("org_tranfer")]
    public string org_tranfer { get; set; }

    [XmlElement("dept_tranfer")]
    public string dept_tranfer { get; set; }

    [XmlElement("sec_tranfer")]
    public string sec_tranfer { get; set; }

    [XmlElement("head_email_tranfer")]
    public string head_email_tranfer { get; set; }

    [XmlElement("email_tranfer")]
    public string email_tranfer { get; set; }

    [XmlElement("decision_status")]
    public string decision_status { get; set; }

    [XmlElement("headqa_email_tranfer")]
    public string headqa_email_tranfer { get; set; }

    [XmlElement("place_idx_tranfer")]
    public int place_idx_tranfer { get; set; }

    [XmlElement("place_name_tranfer")]
    public string place_name_tranfer { get; set; }



}

[Serializable]
public class qa_cims_u1_document_device_detail
{

    [XmlElement("u1_device_idx")]
    public int u1_device_idx { get; set; }

    [XmlElement("u0_device_idx")]
    public int u0_device_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("tranfer_name")]
    public string tranfer_name { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("device_status")]
    public int device_status { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("range_of_use")]
    public string range_of_use { get; set; }

    [XmlElement("point_of_use")]
    public string point_of_use { get; set; }

    [XmlElement("place_idx_tranfer")]
    public int place_idx_tranfer { get; set; }

    [XmlElement("place_name_tranfer")]
    public string place_name_tranfer { get; set; }

}

[Serializable]
public class qa_cims_u1_search_document_device_detail
{
    [XmlElement("u0_device_idx")]
    public int u0_device_idx { get; set; }

    [XmlElement("document_type_idx")]
    public int document_type_idx { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("org_idx_tranfer")]
    public int org_idx_tranfer { get; set; }

    [XmlElement("rdept_idx_tranfer")]
    public int rdept_idx_tranfer { get; set; }

    [XmlElement("rsec_idx_tranfer")]
    public int rsec_idx_tranfer { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("document_type_name")]
    public string document_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }



}

[Serializable]
public class qa_cims_u2_document_device_detail
{
    [XmlElement("u2_device_idx")]
    public int u2_device_idx { get; set; }

    [XmlElement("u0_device_idx")]
    public int u0_device_idx { get; set; }

    [XmlElement("u1_device_idx")]
    public int u1_device_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_name")]
    public string cemp_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("decision_desc")]
    public string decision_desc { get; set; }

    [XmlElement("create_time")]
    public string create_time { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

}

[Serializable]
public class qa_cims_log_registration_device_detail
{

    [XmlElement("log_device_idx")]
    public int log_device_idx { get; set; }

    [XmlElement("log_detalis")]
    public string log_detalis { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("create_time")]
    public string create_time { get; set; }

    [XmlElement("action_type")]
    public int action_type { get; set; }

    [XmlElement("action_name")]
    public string action_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

}

[Serializable]
public class qa_cims_u0_calibration_document_details
{
    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("u0_cal_doc_status")]
    public int u0_cal_doc_status { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("equipment_type")]
    public int equipment_type { get; set; }

    [XmlElement("document_reference")]
    public int document_reference { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("check_other_name")]
    public int check_other_name { get; set; }

}

[Serializable]
public class qa_cims_u1_calibration_document_details
{
    [XmlElement("u1_cal_idx")]
    public int u1_cal_idx { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("u1_cal_doc_status")]
    public int u1_cal_doc_status { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("other_details")]
    public string other_details { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("received_calibration_date")]
    public string received_calibration_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("lab_name")]
    public string lab_name { get; set; }

    [XmlElement("equipment_type")]
    public int equipment_type { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("form_detail_name")]
    public string form_detail_name { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("r0_form_create_idx")]
    public int r0_form_create_idx { get; set; }

    [XmlElement("r0_form_create_name")]
    public string r0_form_create_name { get; set; }

    [XmlElement("r1_form_create_idx")]
    public int r1_form_create_idx { get; set; }

    [XmlElement("r1_form_root_idx")]
    public int r1_form_root_idx { get; set; }

    [XmlElement("form_detail_idx")]
    public int form_detail_idx { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("table_result_name")]
    public string table_result_name { get; set; }

    [XmlElement("m0_formcal_name")]
    public string m0_formcal_name { get; set; }

    [XmlElement("device_rsec")]
    public string device_rsec { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }


}

[Serializable]
public class qa_cims_u2_calibration_document_details
{

    [XmlElement("u2_cal_idx")]
    public int u2_cal_idx { get; set; }

    [XmlElement("u1_cal_idx")]
    public int u1_cal_idx { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

}

[Serializable]
public class qa_cims_m0_calibration_registration_details
{
    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m1_device_idx")]
    public int m1_device_idx { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("device_org_idx")]
    public int device_org_idx { get; set; }

    [XmlElement("device_rdept_idx")]
    public int device_rdept_idx { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("device_cal_date")]
    public string device_cal_date { get; set; }

    [XmlElement("device_due_date")]
    public string device_due_date { get; set; }

    [XmlElement("device_status")]
    public int device_status { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("brand_idx")]
    public int brand_idx { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("range_of_use_min")]
    public decimal range_of_use_min { get; set; }

    [XmlElement("range_of_use_max")]
    public decimal range_of_use_max { get; set; }

    [XmlElement("unit_idx_range_of_use")]
    public int unit_idx_range_of_use { get; set; }

    [XmlElement("equipment_type_idx")]
    public int equipment_type_idx { get; set; }

    [XmlElement("resolution_detail")]
    public decimal resolution_detail { get; set; }

    [XmlElement("unit_idx_resolution")]
    public int unit_idx_resolution { get; set; }

    [XmlElement("measuring_range_min")]
    public decimal measuring_range_min { get; set; }

    [XmlElement("measuring_range_max")]
    public decimal measuring_range_max { get; set; }

    [XmlElement("unit_idx_measuring_range")]
    public int unit_idx_measuring_range { get; set; }

    [XmlElement("acceptance_criteria")]
    public decimal acceptance_criteria { get; set; }

    [XmlElement("calibration_frequency")]
    public decimal calibration_frequency { get; set; }

    [XmlElement("calibration_frequency_unit")]
    public int calibration_frequency_unit { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("device_rsec")]
    public string device_rsec { get; set; }

    [XmlElement("acceptance_criteria_unit_idx")]
    public int acceptance_criteria_unit_idx { get; set; }

    [XmlElement("device_details")]
    public string device_details { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("device_org")]
    public string device_org { get; set; }

    [XmlElement("device_rdept")]
    public string device_rdept { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("unit_name_resolution")]
    public string unit_name_resolution { get; set; }

    [XmlElement("unit_name_range_of_use")]
    public string unit_name_range_of_use { get; set; }

    [XmlElement("unit_name_calibration_frequency")]
    public string unit_name_calibration_frequency { get; set; }

    [XmlElement("unit_name_measuring")]
    public string unit_name_measuring { get; set; }

    [XmlElement("unit_name_calibration_point")]
    public string unit_name_calibration_point { get; set; }

    [XmlElement("unit_name_acceptance_criteria")]
    public string unit_name_acceptance_criteria { get; set; }

    [XmlElement("calibration_point")]
    public decimal calibration_point { get; set; }

    [XmlElement("calibration_point_list")]
    public string calibration_point_list { get; set; }

    [XmlElement("calibration_point_idx_list")]
    public string calibration_point_idx_list { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("count_point_list")]
    public int count_point_list { get; set; }

    [XmlElement("count_point_unit_idx")]
    public int count_point_unit_idx { get; set; }


}

[Serializable]
public class qa_cims_calibration_document_details
{

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

    [XmlElement("u1_cal_idx")]
    public int u1_cal_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("cemp_org_idx")]
    public int cemp_org_idx { get; set; }

    [XmlElement("cemp_rdept_idx")]
    public int cemp_rdept_idx { get; set; }

    [XmlElement("cemp_rsec_idx")]
    public int cemp_rsec_idx { get; set; }

    [XmlElement("received_date")]
    public string received_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("u0_cal_doc_status")]
    public int u0_cal_doc_status { get; set; }

    [XmlElement("document_code")]
    public string document_code { get; set; }

    [XmlElement("admin_emp_idx")]
    public int admin_emp_idx { get; set; }

    [XmlElement("admin_org_idx")]
    public int admin_org_idx { get; set; }

    [XmlElement("admin_rdept_idx")]
    public int admin_rdept_idx { get; set; }

    [XmlElement("admin_rsec_idx")]
    public int admin_rsec_idx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("equipment_type")]
    public int equipment_type { get; set; }

    [XmlElement("document_reference")]
    public int document_reference { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

}

[Serializable]
public class qa_cims_search_registration_device
{

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m1_device_idx")]
    public int m1_device_idx { get; set; }

    [XmlElement("device_serial")]
    public string device_serial { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("device_org_idx")]
    public int device_org_idx { get; set; }

    [XmlElement("device_rdept_idx")]
    public int device_rdept_idx { get; set; }

    [XmlElement("device_rsec_idx")]
    public int device_rsec_idx { get; set; }

    [XmlElement("device_cal_date")]
    public string device_cal_date { get; set; }

    [XmlElement("device_due_date")]
    public string device_due_date { get; set; }

    [XmlElement("device_status")]
    public int device_status { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("emp_idx_create")]
    public int emp_idx_create { get; set; }

    [XmlElement("rdept_idx_create")]
    public int rdept_idx_create { get; set; }

    [XmlElement("org_idx_create")]
    public int org_idx_create { get; set; }

    [XmlElement("rsec_idx_create")]
    public int rsec_idx_create { get; set; }

    [XmlElement("brand_idx")]
    public int brand_idx { get; set; }

    [XmlElement("equipment_idx")]
    public int equipment_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("range_of_use_min")]
    public decimal range_of_use_min { get; set; }

    [XmlElement("range_of_use_max")]
    public decimal range_of_use_max { get; set; }

    [XmlElement("unit_idx_range_of_use")]
    public int unit_idx_range_of_use { get; set; }

    [XmlElement("equipment_type_idx")]
    public int equipment_type_idx { get; set; }

    [XmlElement("resolution_detail")]
    public decimal resolution_detail { get; set; }

    [XmlElement("unit_idx_resolution")]
    public int unit_idx_resolution { get; set; }

    [XmlElement("measuring_range_min")]
    public decimal measuring_range_min { get; set; }

    [XmlElement("measuring_range_max")]
    public decimal measuring_range_max { get; set; }

    [XmlElement("unit_idx_measuring_range")]
    public int unit_idx_measuring_range { get; set; }

    [XmlElement("acceptance_criteria")]
    public decimal acceptance_criteria { get; set; }

    [XmlElement("calibration_frequency")]
    public decimal calibration_frequency { get; set; }

    [XmlElement("calibration_frequency_unit")]
    public int calibration_frequency_unit { get; set; }

    [XmlElement("equipment_name")]
    public string equipment_name { get; set; }

    [XmlElement("equipment_type_name")]
    public string equipment_type_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("device_rsec")]
    public string device_rsec { get; set; }

    [XmlElement("acceptance_criteria_unit_idx")]
    public int acceptance_criteria_unit_idx { get; set; }

    [XmlElement("device_details")]
    public string device_details { get; set; }

    [XmlElement("cal_type_idx")]
    public int cal_type_idx { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

    [XmlElement("device_org")]
    public string device_org { get; set; }

    [XmlElement("device_rdept")]
    public string device_rdept { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("unit_name_Res")]
    public string unit_name_Res { get; set; }

    [XmlElement("unit_name_ROU")]
    public string unit_name_ROU { get; set; }

    [XmlElement("unit_name_CF")]
    public string unit_name_CF { get; set; }

    [XmlElement("unit_name_MR")]
    public string unit_name_MR { get; set; }

    [XmlElement("unit_name_CP")]
    public string unit_name_CP { get; set; }

    [XmlElement("unit_name_AC")]
    public string unit_name_AC { get; set; }

    [XmlElement("calibration_point")]
    public string calibration_point { get; set; }

    [XmlElement("device_place_idx")]
    public int device_place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("m0_status_idx")]
    public int m0_status_idx { get; set; }
}

[Serializable]
public class qa_cims_bind_topics_form_details
{
    [XmlElement("topic_form_idx")]
    public int topic_form_idx { get; set; }

    [XmlElement("topic_form_name")]
    public string topic_form_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("value")]
    public string value { get; set; }

}

[Serializable]
public class qa_cims_record_topics_details
{
    [XmlElement("topic_form_idx")]
    public int topic_form_idx { get; set; }

    [XmlElement("topic_form_name")]
    public string topic_form_name { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("u1_cal_idx")]
    public int u1_cal_idx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("certificate")]
    public int certificate { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_device_idx")]
    public int m0_device_idx { get; set; }

    [XmlElement("m0_lab_idx")]
    public int m0_lab_idx { get; set; }

    [XmlElement("value")]
    public string value { get; set; }

    [XmlElement("u1_val1")]
    public string u1_val1 { get; set; }

    [XmlElement("u1_val2")]
    public string u1_val2 { get; set; }

    [XmlElement("u1_val3")]
    public string u1_val3 { get; set; }

    [XmlElement("u1_val4")]
    public string u1_val4 { get; set; }

    [XmlElement("u1_val5")]
    public string u1_val5 { get; set; }

    [XmlElement("u1_val6")]
    public string u1_val6 { get; set; }

    [XmlElement("u1_val7")]
    public string u1_val7 { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("u0_cal_idx")]
    public int u0_cal_idx { get; set; }

}

[Serializable]
public class qa_cims_record_result_by_form_details
{

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("u1_cal_idx")]
    public int u1_cal_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("device_id_no")]
    public string device_id_no { get; set; }

    [XmlElement("table_result_name")]
    public string table_result_name { get; set; }

    [XmlElement("m0_formcal_name")]
    public string m0_formcal_name { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("value")]
    public string value { get; set; }

    [XmlElement("val1")]
    public string val1 { get; set; }

    [XmlElement("val2")]
    public string val2 { get; set; }

    [XmlElement("val3")]
    public string val3 { get; set; }

    [XmlElement("val4")]
    public string val4 { get; set; }

    [XmlElement("val5")]
    public string val5 { get; set; }

    [XmlElement("val6")]
    public string val6 { get; set; }

    [XmlElement("val7")]
    public string val7 { get; set; }

    [XmlElement("val8")]
    public string val8 { get; set; }

    [XmlElement("val9")]
    public string val9 { get; set; }

    [XmlElement("val10")]
    public string val10 { get; set; }

    [XmlElement("val11")]
    public string val11 { get; set; }

    [XmlElement("val12")]
    public string val12 { get; set; }

    [XmlElement("val13")]
    public string val13 { get; set; }

    [XmlElement("val14")]
    public string val14 { get; set; }

    [XmlElement("val15")]
    public string val15 { get; set; }

    [XmlElement("val16")]
    public string val16 { get; set; }

    [XmlElement("val17")]
    public string val17 { get; set; }

    [XmlElement("val18")]
    public string val18 { get; set; }

    [XmlElement("val19")]
    public string val19 { get; set; }

    [XmlElement("val20")]
    public string val20 { get; set; }

    [XmlElement("val21")]
    public string val21 { get; set; }

}

[Serializable]
public class qa_cims_m0_form_calculate_details
{
    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("m2_formcal_idx")]
    public int m2_formcal_idx { get; set; }

    [XmlElement("m0_formcal_name")]
    public string m0_formcal_name { get; set; }

    [XmlElement("m0_form_status")]
    public int m0_form_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("note_detail")]
    public string note_detail { get; set; }

    [XmlElement("m2_signature_name")]
    public string m2_signature_name { get; set; }

    [XmlElement("m2_position_signature")]
    public string m2_position_signature { get; set; }

    [XmlElement("m2_datetime_signature")]
    public string m2_datetime_signature { get; set; }

    [XmlElement("m2_select_detail")]
    public string m2_select_detail { get; set; }

    [XmlElement("m2_formcal_status")]
    public int m2_formcal_status { get; set; }

}

[Serializable]
public class qa_cims_m1_form_calculate_details
{
    [XmlElement("m1_formcal_idx")]
    public int m1_formcal_idx { get; set; }

    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("table_result_name")]
    public string table_result_name { get; set; }

    [XmlElement("table_decription")]
    public string table_decription { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
}

[Serializable]
public class qa_cims_m2_form_calculate_details
{
    [XmlElement("m2_formcal_idx")]
    public int m2_formcal_idx { get; set; }

    [XmlElement("m0_formcal_idx")]
    public int m0_formcal_idx { get; set; }

    [XmlElement("m2_signature_name")]
    public string m2_signature_name { get; set; }

    [XmlElement("m0_formcal_name")]
    public string m0_formcal_name { get; set; }

    [XmlElement("m2_position_signature")]
    public string m2_position_signature { get; set; }

    [XmlElement("m2_datetime_signature")]
    public string m2_datetime_signature { get; set; }

    [XmlElement("m2_select_detail")]
    public int m2_select_detail { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m2_formcal_status")]
    public int m2_formcal_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
}

[Serializable]
public class qa_cims_m0_location_detail
{
    [XmlElement("m0_location_idx")]
    public int m0_location_idx { get; set; }

    [XmlElement("m1_location_idx")]
    public int m1_location_idx { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("location_status")]
    public int location_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("zone_status")]
    public int zone_status { get; set; }

    [XmlElement("location_zone")]
    public string location_zone { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }
}
