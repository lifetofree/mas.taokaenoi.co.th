﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_adonline_permission_type : System.Web.UI.Page
{
   #region Init
   data_adonline dataADOnline = new data_adonline();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string misConn = "conn_mis";
   string misConnReal = "conn_mis_real";
   string adPermissionTypeService = "adPermissionTypeService";
   string adPermissionCategoryService = "adPermissionCategoryService";
   string adEmployeeLifeTimeTypeService = "adEmployeeLifeTimeTypeService";
   string _local_xml = String.Empty;
   int[] empIDX = { 172, 173, 1374, 1413, 3760, 4839 }; //P'Phon, P'Mai, Bonus, P'Toei, Mod, Jame
   #endregion Init

   #region Constant
   public static class Constants
   {
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_SEARCH = 22;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int SELECT_WHERE_EXISTS_NAME = 28;
      public const int SELECT_WHERE_EXISTS_NAME_UPDATE = 29;
      public const string VALID_FALSE = "101";
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         divIndex.Visible = true;
         divInsert.Visible = false;
         ViewState["empIDX"] = Session["emp_idx"];
         ViewState["keywordSearch"] = "";
         ViewState["statusSearch"] = 99;
         actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex(string keywordSearch = "", int statusSearch = 99)
   {
      permission_type objADOnline = new permission_type();
      dataADOnline.ad_permission_type_action = new permission_type[1];
      objADOnline.keyword_search_perm_type = keywordSearch;
      objADOnline.perm_type_status = statusSearch;
      dataADOnline.ad_permission_type_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.SELECT_WHERE_SEARCH);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      setGridData(gvPermType, dataADOnline.ad_permission_type_action);
   }

   protected void actionCreate()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         var permCateSelected = String.Empty;
         foreach (ListItem itemPermCate in chkPermissionCategory.Items)
         {
            if (itemPermCate.Selected)
            {
               permCateSelected += itemPermCate.Value.ToString() + ",";
            }
         }
         var empLifeTimeTypeSelected = String.Empty;
         foreach (ListItem EmpType in chkEmployeeLifeTimeType.Items)
         {
            if (EmpType.Selected)
            {
               empLifeTimeTypeSelected += EmpType.Value.ToString() + ",";
            }
         }
         permission_type objADOnline = new permission_type();
         dataADOnline.ad_permission_type_action = new permission_type[1];
         objADOnline.perm_type_name = txtPermTypeName.Text.Trim();
         objADOnline.perm_type_status = int.Parse(ddlPermTypeStatus.SelectedValue);
         objADOnline.m0_perm_cate_idx_ref = permCateSelected.Remove(permCateSelected.Length - 1);
         objADOnline.m0_emp_type_idx_ref = empLifeTimeTypeSelected.Remove(empLifeTimeTypeSelected.Length - 1);
         dataADOnline.ad_permission_type_action[0] = objADOnline;
         _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.CREATE);
         dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
         if (dataADOnline.return_code == 0)
         {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
         }
         else
         {
            _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง");
         }
      }
   }

   protected void actionUpdate(int idx, string name, int status, string permCate, string empLifeTimeType)
   {
      permission_type objADOnline = new permission_type();
      dataADOnline.ad_permission_type_action = new permission_type[1];
      objADOnline.m0_perm_type_idx = idx;
      objADOnline.perm_type_name = name;
      objADOnline.perm_type_status = status;
      objADOnline.m0_perm_cate_idx_ref = permCate;
      objADOnline.m0_emp_type_idx_ref = empLifeTimeType;
      dataADOnline.ad_permission_type_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.UPDATE);
   }

   protected void actionBan(int id)
   {
      permission_type objADOnline = new permission_type();
      dataADOnline.ad_permission_type_action = new permission_type[1];
      objADOnline.m0_perm_type_idx = id;
      dataADOnline.ad_permission_type_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      permission_type objADOnline = new permission_type();
      dataADOnline.ad_permission_type_action = new permission_type[1];
      objADOnline.m0_perm_type_idx = id;
      dataADOnline.ad_permission_type_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.UNBAN);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            divIndex.Visible = false;
            divInsert.Visible = true;
            getChkPermisionCategory(chkPermissionCategory);
            getChkEmployeeLifeTimeType(chkEmployeeLifeTimeType);
            break;

         case "btnInsert":
            actionCreate();
            break;

         case "btnSearch":
            ViewState["keywordSearch"] = keywordSearch.Text.Trim();
            ViewState["statusSearch"] = ddlStatus.SelectedValue;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected void getChkPermisionCategory(CheckBoxList chkList)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.SELECT_ALL);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      chkList.DataSource = dataADOnline.ad_permission_category_action;
      chkList.DataTextField = "perm_cate_name";
      chkList.DataValueField = "m0_perm_cate_idx";
      chkList.DataBind();
   }

   protected void getChkEmployeeLifeTimeType(CheckBoxList chkList)
   {
      employee_lifetime_type objADOnline = new employee_lifetime_type();
      dataADOnline.ad_employee_lifetime_type_action = new employee_lifetime_type[1];
      dataADOnline.ad_employee_lifetime_type_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adEmployeeLifeTimeTypeService, dataADOnline, Constants.SELECT_ALL);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      chkList.DataSource = dataADOnline.ad_employee_lifetime_type_action;
      chkList.DataTextField = "emp_type_name";
      chkList.DataValueField = "m0_emp_type_idx";
      chkList.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online f-bold'>Online</span>";
      }
      else if (status == 0)
      {
         return "<span class='status-offline f-bold'>Offline</span>";
      }
      else
      {
         return "<span class='status-offline f-bold'>Delete</span>";
      }
   }

   protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermType":
            gvName.PageIndex = e.NewPageIndex;
            gvName.DataBind();
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermType":
            if (e.Row.RowState.ToString().Contains("Edit"))
            {
               GridView editGrid = sender as GridView;
               int colSpan = editGrid.Columns.Count;
               for (int i = 1; i < colSpan; i++)
               {
                  e.Row.Cells[i].Visible = false;
                  e.Row.Cells[i].Controls.Clear();
               }
               e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
               e.Row.Cells[0].CssClass = "";
               CheckBoxList chkPermissionCategoryUpdate = (CheckBoxList)e.Row.FindControl("chkPermissionCategoryUpdate");
               getChkPermisionCategory(chkPermissionCategoryUpdate);
               TextBox txtPermissionCategoryUpdate = (TextBox)e.Row.FindControl("txtPermissionCategoryUpdate");
               string[] m0PermCateIdxRef = txtPermissionCategoryUpdate.Text.Split(',');
               var j = 0;
               foreach (ListItem itemPermCateUpdate in chkPermissionCategoryUpdate.Items)
               {
                  if (Array.IndexOf(m0PermCateIdxRef, itemPermCateUpdate.Value) > -1)
                  {
                     itemPermCateUpdate.Selected = true;
                  }
                  j++;
               }

               CheckBoxList chkEmployeeLifeTimeTypeUpdate = (CheckBoxList)e.Row.FindControl("chkEmployeeLifeTimeTypeUpdate");
               getChkEmployeeLifeTimeType(chkEmployeeLifeTimeTypeUpdate);
               TextBox txtEmployeeLifeTimeTypeUpdate = (TextBox)e.Row.FindControl("txtEmployeeLifeTimeTypeUpdate");
               string[] m0EmpLifeTimeTypeIdxRef = txtEmployeeLifeTimeTypeUpdate.Text.Split(',');
               var k = 0;
               foreach (ListItem itemEmpLifeTimeTypeUpdate in chkEmployeeLifeTimeTypeUpdate.Items)
               {
                  if (Array.IndexOf(m0EmpLifeTimeTypeIdxRef, itemEmpLifeTimeTypeUpdate.Value) > -1)
                  {
                     itemEmpLifeTimeTypeUpdate.Selected = true;
                  }
                  k++;
               }
            }
            break;
      }
   }

   protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermType":
            gvName.EditIndex = -1;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermType":
            int dataKeyId = Convert.ToInt32(gvName.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox txtPermTypeNameUpdate = (TextBox)gvName.Rows[e.RowIndex].FindControl("txtPermTypeNameUpdate");
            DropDownList ddlPermTypeStatusUpdate = (DropDownList)gvName.Rows[e.RowIndex].FindControl("ddlPermTypeStatusUpdate");
            CheckBoxList chkPermissionCategoryUpdate = (CheckBoxList)gvName.Rows[e.RowIndex].FindControl("chkPermissionCategoryUpdate");
            var permCateSelected = String.Empty;
            foreach (ListItem itemPermCate in chkPermissionCategoryUpdate.Items)
            {
               if (itemPermCate.Selected)
               {
                  permCateSelected += itemPermCate.Value.ToString() + ",";
               }
            }
            permCateSelected = permCateSelected.Remove(permCateSelected.Length - 1);

            CheckBoxList chkEmpLifeTimeTypeUpdate = (CheckBoxList)gvName.Rows[e.RowIndex].FindControl("chkEmployeeLifeTimeTypeUpdate");
            var itemEmpLifeTimeTypeSelected = String.Empty;
            foreach (ListItem itemEmpLifeTimeType in chkEmpLifeTimeTypeUpdate.Items)
            {
               if (itemEmpLifeTimeType.Selected)
               {
                  itemEmpLifeTimeTypeSelected += itemEmpLifeTimeType.Value.ToString() + ",";
               }
            }
            itemEmpLifeTimeTypeSelected = itemEmpLifeTimeTypeSelected.Remove(itemEmpLifeTimeTypeSelected.Length - 1);
            gvName.EditIndex = -1;
            actionUpdate(dataKeyId, txtPermTypeNameUpdate.Text.Trim(), int.Parse(ddlPermTypeStatusUpdate.SelectedValue), permCateSelected.ToString(), itemEmpLifeTimeTypeSelected.ToString());
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermType":
            gvName.EditIndex = e.NewEditIndex;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void checkExistsPermTypeName(object sender, ServerValidateEventArgs e)
   {
      permission_type objADOnline = new permission_type();
      dataADOnline.ad_permission_type_action = new permission_type[1];
      objADOnline.perm_type_name = txtPermTypeName.Text.Trim();
      dataADOnline.ad_permission_type_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionTypeService, dataADOnline, Constants.SELECT_WHERE_EXISTS_NAME);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      if (dataADOnline.return_code.ToString() == Constants.VALID_FALSE)
      {
         e.IsValid = false;
      }
      else
      {
         e.IsValid = true;
      }
   }
   #endregion Custom Functions
}
