<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="websystem_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
	<div class="col-md-10 col-md-offset-1">
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				<li data-target="#carousel-example-generic" data-slide-to="3"></li>
				<!-- <li data-target="#carousel-example-generic" data-slide-to="4"></li> -->
				<!-- <li data-target="#carousel-example-generic" data-slide-to="5"></li> -->
				<!-- <li data-target="#carousel-example-generic" data-slide-to="6"></li> -->
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="100%"
						alt="Christmas" />
				</div>
				<div class="item">
					<img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' class="img-fluid" width="100%"
						alt="Car Booking" />
				</div>
				<div class="item">
					<img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' class="img-fluid"
						width="100%" alt="Reset Password" />
				</div>
				<div class="item">
					<img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' class="img-fluid" width="100%"
						alt="Room Booking" />
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</asp:Content>