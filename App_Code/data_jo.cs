﻿using System;
using System.Xml.Serialization;

#region data_jo
[Serializable]
[XmlRoot("data_jo")]
public class data_jo
{
   [XmlElement("return_code")]
   public int  return_code { get; set; }
   [XmlElement("return_msg")]
   public string return_msg { get; set; }
    [XmlElement("return_mailmis")]
    public string return_mailmis { get; set; }

    //[XmlElement("to_m0_node")]
    //public int to_m0_node { get; set; }

    //[XmlElement("to_m0_actor")]
    //public int to_m0_actor { get; set; }

    //[XmlElement("id_statework")]
    //public int id_statework { get; set; }

    //[XmlElement("m0_actor_idx")]
    //public int m0_actor_idx { get; set; }

    //[XmlElement("m0_node_idx")]
    //public int m0_node_idx { get; set; }


    [XmlElement("job_order_overview_action")]
   public job_order_overview[] job_order_overview_action { get; set; }

    [XmlElement("jo_add_datalist_action")]
    public jo_add_datalist[] jo_add_datalist_action { get; set; }

    [XmlElement("jo_log_action")]
    public jo_log[] jo_log_action { get; set; }

    [XmlElement("job_user_action")]
    public job_user[] job_user_action { get; set; }
}
#endregion data_jo

#region job_user
[Serializable]
public class job_user
{
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
   
    [XmlElement("emp_login")]
    public int emp_login { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
}
#endregion

#region jo_log
[Serializable]
public class jo_log
{
    [XmlElement("jo_idx_ref")]
   public int jo_idx_ref { get; set; }

    [XmlElement("l0_jo_idx")]
    public int l0_jo_idx { get; set; }

    [XmlElement("l0_jo_idx_ref")]
    public int l0_jo_idx_ref { get; set; }

    [XmlElement("m0_actor_idx_lo")]
   public int m0_actor_idx_lo { get; set; }

   [XmlElement("m0_node_idx_lo")]
   public int m0_node_idx_lo { get; set; }

   [XmlElement("id_statework_lo")]
   public int id_statework_lo { get; set; }

   [XmlElement("oakja")]
   public int oakja { get; set; }

   [XmlElement("from_table")]
   public string from_table { get; set; }

    [XmlElement("from_table_l0")]
    public string from_table_l0 { get; set; }

    [XmlElement("requried_old")]
   public string requried_old { get; set; }

   [XmlElement("day_create")]
   public string day_create { get; set; }

    [XmlElement("u1_day_sent_time")]
    public string u1_day_sent_time { get; set; }

    [XmlElement("idx_create")]
   public int idx_create { get; set; }

   [XmlElement("m0_actor_ref")]
   public int m0_actor_ref { get; set; }

   [XmlElement("day_actor")]
   public string day_actor { get; set; }

   [XmlElement("m0_node_ref")]
   public int m0_node_ref { get; set; }

   [XmlElement("m0_status_ref")]
   public int m0_status_ref { get; set; }

    [XmlElement("idx_create_lo")]
    public int idx_create_lo { get; set; }

    [XmlElement("name_emp")]
    public string name_emp { get; set; }

    [XmlElement("l0_codeemp")]
    public int l0_codeemp { get; set; }

    [XmlElement("AdminMain")]
    public string AdminMain { get; set; }

    [XmlElement("statenode_name_l0")]
    public string statenode_name_l0 { get; set; }

    [XmlElement("node_name_l0")]
    public string node_name_l0 { get; set; }

    [XmlElement("name_description")]
    public string name_description { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }
    
}


#endregion jo_log

#region jo_add_datalist
[Serializable]
public class jo_add_datalist
{
    [XmlElement("u1_emp_sent_single")]
    public int u1_emp_sent_single { get; set; }

    [XmlElement("u1_jo_idx")]
    public int u1_jo_idx { get; set; }

    [XmlElement("u1_title")]
    public string u1_title { get; set; }

    [XmlElement("u1_day_sent")]
    public string u1_day_sent { get; set; }
    
    [XmlElement("u1_day_receive")]
    public string u1_day_receive { get; set; }

    [XmlElement("u1_emp_receive")]
    public int u1_emp_receive { get; set; }

    [XmlElement("u1_emp_sent")]
    public int u1_emp_sent { get; set; }

    [XmlElement("u0_jo_idx_ref")]
    public int u0_jo_idx_ref { get; set; }

    [XmlElement("wait")]
    public int wait { get; set; }

    [XmlElement("u1_status")]
    public int u1_status { get; set; }

    [XmlElement("u1_ov_useonly")]
    public int u1_ov_useonly { get; set; }

    [XmlElement("u1_day_loop")]
    public string u1_day_loop { get; set; }

    [XmlElement("jolog_m0_node")]
    public int jolog_m0_node { get; set; }

    [XmlElement("jolog_m0_actor")]
    public int jolog_m0_actor { get; set; }

    [XmlElement("jolog_m0_status")]
    public int jolog_m0_status { get; set; }

    [XmlElement("u1_codeemp")]
    public int u1_codeemp { get; set; }

    [XmlElement("name_emp_look")]
    public string name_emp_look { get; set; }

    [XmlElement("createmp")]
    public string createmp { get; set; }

    [XmlElement("receive_emp_name")]
    public string receive_emp_name { get; set; }

    [XmlElement("u1_day_receive_time")]
    public string u1_day_receive_time { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("day_created")]
    public string day_created { get; set; }

    [XmlElement("no_invoice")]
    public string no_invoice { get; set; }

    [XmlElement("statenode_name")]
    public string statenode_name { get; set; }

    [XmlElement("id_statework")]
    public int id_statework { get; set; }

    [XmlElement("name_description")]
    public string name_description { get; set; }

    [XmlElement("end_send")]
    public string end_send { get; set; }

    [XmlElement("sec_mis")]
    public int sec_mis { get; set; }

    [XmlElement("title_jo")]
    public string title_jo { get; set; }

    [XmlElement("require_jo")]
    public string require_jo { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }
}


#endregion jo_add_datalist

#region job_order_overview
[Serializable]
public class job_order_overview
{
    //[XmlElement("year_check")]
    //private string _year_check;
    //public string year_check { get; set; }
    [XmlElement("u0_day_sent_time")]
    public string u0_day_sent_time { get; set; }

    [XmlElement("emp_dep_job")]
    public int emp_dep_job { get; set; }

    [XmlElement("emp_org_job")]
    public int emp_org_job { get; set; }

    [XmlElement("name_emp_look")]
  
    public string name_emp_look { get; set; }

    [XmlElement("AdminMain")]
    public string AdminMain { get; set; }

    [XmlElement("name_description")]
    public string name_description { get; set; }

    [XmlElement("Organize")]
    public string Organize { get; set; }

    [XmlElement("org_idx")]
    public string org_idx { get; set; }

    [XmlElement("Deptname")]
    public string Deptname { get; set; }

    [XmlElement("Secname")]
    public string Secname { get; set; }

    [XmlElement("Posname")]
    public string Posname { get; set; }

    [XmlElement("Codeemp")]
    public int Codeemp { get; set; }

    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("rsec_creator")]
    public int rsec_creator { get; set; }

    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    
    [XmlElement("rdep_num")]
    public int rdep_num { get; set; }

    [XmlElement("job_grade_cre")]
    public int job_grade_cre { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("u0_jo_idx")]
    public int u0_jo_idx { get; set; }

    [XmlElement("no_invoice")]
    public string no_invoice { get; set; }

    [XmlElement("day_start")]
    public string day_start { get; set; }

    [XmlElement("day_finish")]
    public string day_finish { get; set; }

    [XmlElement("day_amount")]
    public string day_amount { get; set; }

    [XmlElement("title_jo")]
    public string title_jo { get; set; }

    [XmlElement("require_jo")]
    public string require_jo { get; set; }

    [XmlElement("id_statework")]
    public int id_statework { get; set; }

    [XmlElement("emp_id_creator")]
    public int emp_id_creator { get; set; }

    [XmlElement("day_created")]
    public string day_created { get; set; }

    [XmlElement("emp_id_receive")]
    public int emp_id_receive { get; set; }

    [XmlElement("day_receive")]
    public string day_receive { get; set; }

    [XmlElement("emp_id_sent")]
    public int emp_id_sent { get; set; }

    [XmlElement("day_sent")]
    public string day_sent { get; set; }

    [XmlElement("to_m0_node")]
    public int to_m0_node { get; set; }

    [XmlElement("to_m0_actor")]
    public int to_m0_actor { get; set; }

    [XmlElement("statenode_name")]
    public string statenode_name { get; set; }

    [XmlElement("u1_jo_idx")]
    public string u1_jo_idx { get; set; }

    [XmlElement("u1_title")]
    public string u1_title { get; set; }

    [XmlElement("u1_day_sent")]
    public string u1_day_sent { get; set; }

    [XmlElement("u0_jo_idx_ref")]
    public int u0_jo_idx_ref { get; set; }

// /////////เพิ่มมาใหม่////////////////////////////////

    [XmlElement("name_actor")]
    public string name_actor { get; set; }

    [XmlElement("node_name")]
    public string node_name  { get; set; }

    [XmlElement("fromac")]
    public string fromac  { get; set; }

    [XmlElement("jo_idx_ref")]
    public int jo_idx_ref { get; set; }
   
    [XmlElement("node_status")]
    public int node_status { get; set; }
    
    [XmlElement("m0_actor_idx")]
    public int  m0_actor_idx { get; set; }
    
    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("org_idx_cre")] 
    public int org_idx_cre { get; set; }

    [XmlElement("statenode_state")]
    public int statenode_state { get; set; }

    [XmlElement("requried_old")]
    public string requried_old { get; set; }

    // [XmlElement("noinvoice_search")]
    // private string _noinvoice_search;
    // public string noinvoice_search { get; set; }


    // //เพิ่มมาวันที่ 6.07.2560 ///
    [XmlElement("noinvoice_search")]
    public string noinvoice_search { get; set; }

    [XmlElement("day_search")]
    public string day_search { get; set; }

    [XmlElement("day_searchto")]
    public string day_searchto { get; set; }


    [XmlElement("if_date")]
    public int if_date { get; set; }


    [XmlElement("Org_search")]
    public int Org_search { get; set; }

    [XmlElement("Dept_search")]
    public int Dept_search { get; set; }

    [XmlElement("Sec_search")]
    public int Sec_search { get; set; }

    [XmlElement("ep_search")]
    public string ep_search { get; set; }

    [XmlElement("from_table")]
    public string from_table { get; set; }

    [XmlElement("nameActor")]
    public string nameActor { get; set; }

    [XmlElement("end_send")]
    public string end_send { get; set; }

    [XmlElement("day_created_time_log")]
    public string day_created_time_log { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("degree_idx")]
    public int degree_idx { get; set; }

    [XmlElement("sec_mis")]
    public int sec_mis { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

}


#endregion job_order_overview

