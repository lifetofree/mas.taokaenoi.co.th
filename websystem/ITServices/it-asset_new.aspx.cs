﻿
using NPOI.XWPF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class websystem_ITServices_it_asset_new : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_itasset _dtitseet = new data_itasset();
    data_purchase _data_purchase = new data_purchase();
    function_dmu _func_dmu = new function_dmu();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();

    List<its_TransferDevice_u0> _its_TransferDevice_u0_selected = new List<its_TransferDevice_u0>();

    string _localJson = String.Empty;
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";
    string _path_assetfile = "ASSETFILE/";

    //string _link = "http://demo.taokaenoi.co.th/it-asset";
    //static string _baseUrl = "http://172.16.11.26/taokaenoi.co.th/MAS/RegisterMachine";

    string _link = "http://mas.taokaenoi.co.th/it-asset";
    static string _baseUrl = "http://www.taokaenoi.co.th/MAS/RegisterMachine";

    int emp_idx = 0;
    //ผู้เกี่ยวข้อง
    int it_support = 210;//80;
    int budget = 6;
    int asset = 9;
    int it_admin = 25270, Admin_Office_emp_idx = 2488, md_emp_idx = 3640; // 25270 //24359
    int md_rpos_idx = 780;


    // set object
    FormView _FormView;

    GridView _GvReportAdd,
        _GridView
        ;

    TextBox _txtremark,
        _txtref_itnum,
        _txtionum,
        _txtbudget_set,
        _txtlist_menu,
        _txtqty,
        _txtprice,
        _txtstartdate,
        _txtenddate,
        _txtdocumentcode,
        _txt_remark,
         _ddlspecidx,
        _txt_hidden_sysidx,
        _txt_m1_ref_idx,
        _txtplace_idx,
_txtplace_trnf,
         _txtplace_idx_hr,
        _txt_hidden_actor_idx,
        _txt_hidden_node_idx
        ;

    DropDownList
        _ddlsystem,
        _ddlSearchTypeDevices,
        _ddlSearchDevices,
        _ddlsearch_typepurchase,
        _ddlposition,
        _ddlcurrency,
        _ddlunit,
        _ddlcostcenter,
        _ddlplace,
        // _ddlspecidx,
        _ddlStatusapprove,
        _ddlcondition,
         _ddlMonthSearch,
        _ddlYearSearch,
        _ddlStatusapproveSearch,
         _ddl_target,
        _ddlAddOrg_trnf,
_ddlAddDep_trnf,
_ddlAddSec_trnf,
_ddlAddemp_idx_trnf,
_ddlSearch_devices_trnf,
_ddlasset_no_trnf,
        _ddlAddOrg_trnf_en,
_ddlAddDep_trnf_en,
_ddllocate_en,
_ddlbuilding_en,
_ddlroom_en,
_ddllocate_en_search,
_ddltypemachine_en_search,
_ddlbuilding_en_search,
_ddlroom_en_search,
_ddlmachine_en_search,
_ddlAddOrg_trnf_hr,
_ddlAddDep_trnf_hr,
_ddlAddemp_idx_trnf_hr,
_ddlAddSec_trnf_hr,
_ddlSearch_devices_trnf_hr,
_ddlasset_no_trnf_hr,
_ddllocate_hr_search,
        _ddllocate_it_search,
        _ddllocate_hr
        ;
    Panel
        _pnl_ionum,
        _pnl_ref_itnum,
        _Panel_body_approve,
        _Panel_approve,
        _pnl_search,
        _pnl_history,
        _pnl_doc,
        _pnl_other
        ;
    Label
        _lb_title_approve
        ;
    Repeater
        _Repeater,
        _rpt_history
        ;
    LinkButton
        _lbtn,
        _lbtnsave,
        _lbtncancel,
         _btn_spec_search
        ;
    CheckBox
        _check_approve,
        _CheckBox,
        _check_special
        ;
    HiddenField
        _hfu0_transf_idx,
_hfflow_item,
_hfNodeIdx,
_hfActorIdx,
        _hfsysidx,
        _hfdoc_status

        ;

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem"];
    static string _urlInsertClass = _serviceUrl + ConfigurationManager.AppSettings["urlnsert_Master_Class"];
    static string _urlSelectClass = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_Class"];
    static string _urlDeleteClass = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_Class"];
    static string _urlInsertAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_AssetBuy"];
    static string _urlSelectAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_AssetBuy"];
    static string _urlInsert_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_IOBuy"];
    static string _urlSelect_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_IOBuy"];
    static string _urlUpdate_Master_IOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_IOBuy"];
    static string _urlSelect_Master_YearIOBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_YearIOBuy"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlSelect_Master_YEARAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_YEARAssetBuy"];
    static string _urlSelect_Master_LocationAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_LocationAssetBuy"];
    static string _urlSelectPos = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPos"];



    // start Price_Reference

    // static string _urlGetequipment_purchasetype = _serviceUrl + ConfigurationManager.AppSettings["urlGetequipment_purchasetype"];
    static string _urlSelectSystem_TypePrice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_TypePrice"];
    static string _urlSelectTypeDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeDevices_Ref"];
    static string _urlSelectSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSoftware_Ref"];
    static string _urlSelectHROffice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHROffice_Ref"];
    static string _urlSelectENMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectENMachine_Ref"];
    static string _urlGetposition = _serviceUrl + ConfigurationManager.AppSettings["urlGetposition"];

    // end Price_Reference
    static string _urlGetmasterspec = _serviceUrl + ConfigurationManager.AppSettings["urlGetmasterspec"];
    // start it asset 
    static string _urlGetits_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_lookup"];
    //its_u_document
    static string _urlGetits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_document"];
    static string _urlSetInsits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_document"];
    static string _urlSetUpdits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_document"];
    static string _urlDelits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_document"];

    static string _urlGetits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_masterit"];
    static string _urlsendEmailits_u_document = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmailits_u_document"];


    // end it asset
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetlocation = _serviceUrl + ConfigurationManager.AppSettings["urlGetlocation"];

    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    // its_u_transfer_action
    static string _urlGetits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_transfer"];
    static string _urlSetInsits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_u_transfer"];
    static string _urlSetUpdits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_u_transfer"];
    static string _urlDelits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_u_transfer"];

    static string _url_u0_device_update_17 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_17_List"];
    static string urlSetApproveLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveLicense"];

    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string urlSelect_Room = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Room"];
    static string _urlSelectDoc_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_Machine"];
    static string urlSelect_NameMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_NameMachine"];
    static string urlSelectDetailHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetailHolder"];
    static string urlUpdate_ApproveChangeHolder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveChangeHolder"];
    static string urlGetSoftwareShowHolder = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareShowHolder"];

    #endregion

    #endregion

    #region PageLoad
    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

            ViewState["mode"] = "I";
            select_empIdx_present();

            setpermission_admin_dept();
            setActiveTab("p");
            if (ViewState["rsec_idx"].ToString() != "210")
            {
                setpermission_admin_menu();
            }

            // CreateDsits_u1_document();
            //SetViewState_BuyReplace();
            // ShowDataIndex();
            if (ViewState["mode"].ToString() == "md_approve")
            {
                settab_md();
            }
            else
            {
                CreateDsits_u1_document();
                SetViewState_BuyReplace();
                ShowDataIndex();
            }
            SETFOCUS.Focus();

        }

        setBtnTrigger();

    }

    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }

    private void setBtnTrigger()
    {
        //menu
        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivBuyNew);
        linkBtnTrigger(_divMenuBtnToDivCutDevices);
        linkBtnTrigger(_divMenuBtnToDivChangeOwn);
        linkBtnTrigger(BtnToDivWaitApprove_Buy);
        linkBtnTrigger(BtnToDivWaitApprove_CutDevices);
        linkBtnTrigger(BtnToDivWaitApprove_ChangeOwn);
        linkBtnTrigger(BtnToDivBudget);
        linkBtnTrigger(BtnToDivAsset_master);
        linkBtnTrigger(BtnToDivAsset_system);
        linkBtnTrigger(_divMenuBtnToDivDataGiveAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_btnIT);
        linkBtnTrigger(_btnIT_update);
        linkBtnTrigger(_divMenubtnIT_manager);
        linkBtnTrigger(btnMenuLiToasset_dir1);
        linkBtnTrigger(_divMenubtnIT_director);
        linkBtnTrigger(_divMenubtnMD_it);
        linkBtnTrigger(_divMenubtnIT_deliver);
        linkBtnTrigger(_divMenuBtnToDivBudget_master);
        linkBtnTrigger(_divMenubtnIT_print);
        linkBtnTrigger(_divMenubtnpurchase);

        //detail 
        linkBtnTrigger(btnshowboxcreate);
        linkBtnTrigger(btnshowBoxsearch);
        linkBtnTrigger(btnhiddenBoxsearch);
        linkBtnTrigger(btnviewdataofmountly);
        linkBtnTrigger(btnviewall);
        linkBtnTrigger(btnsubmitview);
        linkBtnTrigger(btncancelview);
        linkBtnTrigger(btnsearch);
        linkBtnTrigger(btnresetsearch);
        linkBtnTrigger(btnapproveall);
        linkBtnTrigger(btnnotapprove);
        //linkBtnTrigger(btnapprove_all);



        /*
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        */
        linkBtnTrigger(CmdAdd_Import);
        //model
        linkBtnTrigger(btnCmdCancel_modal);

        linkBtnTrigger(btntrnf_it);
        linkBtnTrigger(btntrnf_hr);
        linkBtnTrigger(btntrnf_en);

        linkBtnTrigger(btn_okspec);
        linkBtnTrigger(btn_cancelspec);

        GridViewTrigger(GvTrnfIndex);

    }
    #endregion

    #region CallService
    protected data_employee callService(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }


    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        if (_dtEmployee.employee_list != null)
        {
            ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
            ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
            ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

            ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
            ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
            ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
            ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
            ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
            ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

            ViewState["rsec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
            ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
            ViewState["rpos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
            ViewState["org_idx"] = _dtEmployee.employee_list[0].org_idx;
            ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
            ViewState["locidx"] = _dtEmployee.employee_list[0].LocIDX;

        }
        else
        {
            ViewState["rdept_name"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["FullName"] = "";
            ViewState["Org_name"] = "";
            ViewState["Org_idx"] = "";

            ViewState["EmpCode"] = "";
            ViewState["Positname"] = "";
            ViewState["Pos_idx"] = "";
            ViewState["Email"] = "";
            ViewState["Tel"] = "";
            ViewState["Secname"] = "";
            ViewState["Sec_idx"] = "";
            ViewState["CostIDX"] = "";

            ViewState["rsec_idx"] = "";
            ViewState["rdept_idx"] = "";
            ViewState["rpos_idx"] = "";
            ViewState["org_idx"] = "";
            ViewState["jobgrade_level"] = "";
            ViewState["locidx"] = "";

        }

        ViewState["emp_idx"] = ViewState["EmpIDX"].ToString();
        ViewState["rdept_idx_dir"] = 0;
        ViewState["emp_idx_dir"] = 0;
        ViewState["rdept_idx_budget"] = 0;
        ViewState["rpos_idx_budget"] = 0;
        ViewState["org_idx_budget"] = 0;
        ViewState["rsec_idx_budget"] = 0;
        ViewState["emp_idx_budget"] = 0;
        ViewState["place_1"] = 0;
        ViewState["place_2"] = 0;
        ViewState["place_3"] = 0;
        ViewState["place_4"] = 0;
        ViewState["place_count"] = 0;
        ViewState["jobgrade_level_dir"] = 0;
        ViewState["rdept_idx_dir"] = 0;

        _dtitseet.its_lookup_action = new its_lookup[1];
        its_lookup select_its = new its_lookup();
        select_its.rdept_idx = 0;
        select_its.idx = emp_idx;
        select_its.operation_status_id = "jobgrade_director";

        _dtitseet.its_lookup_action[0] = select_its;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlGetits_lookup, _dtitseet);

        if (_dtitseet.its_lookup_action != null)
        {
            foreach (var item in _dtitseet.its_lookup_action)
            {
                //litDebug.Text = item.jobgrade_level.ToString() + "-" + item.rdept_idx.ToString();
                ViewState["jobgrade_level_dir"] = item.jobgrade_level;
                ViewState["rdept_idx_dir"] = item.rdept_idx;
            }
        }



        _dtitseet.its_lookup_action = new its_lookup[1];
        select_its = new its_lookup();
        select_its.operation_status_id = "budget_approve";
        select_its.emp_idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());

        _dtitseet.its_lookup_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_lookup, _dtitseet);
        if (_dtitseet.its_lookup_action != null)
        {
            foreach (var item in _dtitseet.its_lookup_action)
            {
                ViewState["rdept_idx_budget"] = item.rdept_idx;
                ViewState["emp_idx_budget"] = item.emp_idx;
                ViewState["place_count"] = item.item_count;

                ViewState["rpos_idx_budget"] = item.rpos_idx;
                ViewState["org_idx_budget"] = item.org_idx;
                ViewState["rsec_idx_budget"] = item.rsec_idx;

            }
        }

        ViewState["RSec_JobGradeIDX"] = "";
        ViewState["RDep_Position"] = "";

        _dtitseet.its_lookup_action = new its_lookup[1];
        select_its = new its_lookup();
        select_its.idx = _func_dmu.zStringToInt(ViewState["emp_idx"].ToString());
        select_its.operation_status_id = "sel_view_employee_position";
        _dtitseet.its_lookup_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_lookup, _dtitseet);

        if (_dtitseet.its_lookup_action != null)
        {
            foreach (var item in _dtitseet.its_lookup_action)
            {
                ViewState["RSec_JobGradeIDX"] = item.jobgrade_level;
                ViewState["RDep_Position"] = item.rdept_idx;
            }
        }


        ViewState["Approve_Name"] = "";

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its1 = new its_u_document();
        select_its1.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its1.operation_status_id = "permission_send_email_dir";
        _dtitseet.its_u_document_action[0] = select_its1;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        if (_dtitseet.its_u_document_action != null)
        {
            int ic = 0;
            foreach (var item in _dtitseet.its_u_document_action)
            {
                if (ic == 0)
                {
                    ViewState["Approve_Name"] = item.emp_name_th;
                }
                else
                {
                    ViewState["Approve_Name"] = ViewState["Approve_Name"].ToString() + ',' + item.emp_name_th;
                }
                ic = ic + 1;
            }
        }


    }


    protected void select_system(DropDownList ddlName)
    {
        // tb [Centralized].[dbo].[M0_System]

        _dtitseet = new data_itasset();

        _dtitseet.boxdata_u0document_itasset = new u0_document_itasset[1];
        u0_document_itasset search = new u0_document_itasset();

        _dtitseet.boxdata_u0document_itasset[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelectSystem, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxdata_u0document_itasset, "SysNameEN", "SysIDX");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภททรัพย์สิน...", "0"));
    }

    protected void SelectMasterList_Class()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset dtasset = new class_itasset();

        _dtitseet.boxmaster_class[0] = dtasset;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
        //txt.Text = _localJson1;
        _dtitseet = callServicePostITAsset(_urlSelectClass, _dtitseet);
        setGridData(GvMaster, _dtitseet.boxmaster_class);
    }

    protected void SelectMasterList_AssetBuy()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxmaster_asset = new asset_itasset[1];
        asset_itasset search = new asset_itasset();

        search.LocIDX = int.Parse(ddlsearch_locateasset.SelectedValue);
        search.CostIDX = int.Parse(ddlsearch_costcenterasset.SelectedValue);
        search.Asset = txtsearchasset.Text;
        search.CapDate = ddlsearch_yearasset.SelectedValue;
        search.Asset_Status = int.Parse(ViewState["status_searchasset"].ToString());

        _dtitseet.boxmaster_asset[0] = search;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlSelectAssetBuy, _dtitseet);

        setGridData(GvAsset_Buy, _dtitseet.boxmaster_asset);
    }

    protected void SelectMasterList_IO()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxmaster_io = new io_itasset[1];
        io_itasset search = new io_itasset();

        search.year_io = int.Parse(ddlsearch_yeario.SelectedValue);
        search.CostIDX = int.Parse(ddlsearch_costcenterio.SelectedValue);
        search.iono = txtsearch_io.Text;
        search.usestatus = int.Parse(ddlsearch_statusio.SelectedValue);
        search.status = int.Parse(ViewState["status_searchio"].ToString());
        search.CEmpIDX = emp_idx;

        _dtitseet.boxmaster_io[0] = search;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlSelect_Master_IOBuy, _dtitseet);
        setGridData(GvMasterIO, _dtitseet.boxmaster_io);
    }

    protected void select_YearMasterIO(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxmaster_io = new io_itasset[1];
        io_itasset search = new io_itasset();

        _dtitseet.boxmaster_io[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelect_Master_YearIOBuy, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxmaster_io, "year_io", "year_io");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกปี...", "0"));
    }

    protected void select_CostcenterMasterIO(DropDownList ddlName)
    {

        _dtEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dtEmployee.CostCenterDetail[0] = dtemployee;

        _dtEmployee = callService(_urlGetCostcenterOld, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.CostCenterDetail, "CostNo", "CostIDX");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก Cost Center...", "0"));
    }

    protected void select_YearMasterAsset(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxmaster_asset = new asset_itasset[1];
        asset_itasset search = new asset_itasset();

        _dtitseet.boxmaster_asset[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelect_Master_YEARAssetBuy, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxmaster_asset, "CapDate", "CapDate");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกปี...", "0"));
    }

    protected void select_LocateMasterAsset(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxmaster_asset = new asset_itasset[1];
        asset_itasset search = new asset_itasset();

        _dtitseet.boxmaster_asset[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelect_Master_LocationAssetBuy, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxmaster_asset, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสถานที่...", "0"));
    }



    #endregion

    #region Insert
    protected void Insert_Master()
    {
        string text = String.Empty;
        text = txtname.Text.Replace('&', ' ');

        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset insert = new class_itasset();

        insert.code_class = txtcode.Text;
        insert.code_desc = text;
        insert.code_status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxmaster_class[0] = insert;

        //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
        //txt.Text = _localJson1;// HttpUtility.HtmlEncode(_funcTool.(_localJson1));
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlInsertClass, _dtitseet);

        if (_dtitseet.ReturnCode == "102")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้ในระบบแล้ว');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);
        }

    }

    protected void ImportFileBuyDevices(string filePath, string Extension, string isHDR, string fileName, string module)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();


        string idxCreated = String.Empty;
        int j = 0;

        switch (module)
        {

            case "asset":

                for (var i = 0; i < dt.Rows.Count - 1; i++)
                {
                    if (dt.Rows[j][5].ToString().Trim() != "Asset description" && dt.Rows[j][5].ToString().Trim() != "")
                    {
                        asset_itasset import = new asset_itasset();
                        _dtitseet.boxmaster_asset = new asset_itasset[1];

                        import.Asset = dt.Rows[j][0].ToString().Trim();
                        import.SNo = dt.Rows[j][1].ToString().Trim();
                        import.CostCenter = dt.Rows[j][2].ToString().Trim();
                        import.LocIDX = int.Parse(dt.Rows[j][3].ToString());
                        import.CapDate = _func_dmu.zDateToDB(dt.Rows[j][4].ToString().Trim());
                        import.Asset_Description = dt.Rows[j][5].ToString().Trim();
                        import.AcquisVal = dt.Rows[j][6].ToString();
                        import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                        _dtitseet.boxmaster_asset[0] = import;
                        //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
                        //txt.Text = _localJson1;// HttpUtility.HtmlEncode(_funcTool.(_localJson1));
                        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                        _dtitseet = callServicePostITAsset(_urlInsertAssetBuy, _dtitseet);


                    }
                    j++;
                }
                break;

            case "budget":
                string cost = "0";
                string amountio = "0";
                string budget = "0";
                string variance = "0";

                for (var i = 0; i < dt.Rows.Count - 1; i++)
                {
                    if (dt.Rows[j][0].ToString().Trim() != "")
                    {
                        io_itasset import = new io_itasset();
                        _dtitseet.boxmaster_io = new io_itasset[1];

                        cost = dt.Rows[j][3].ToString().Replace(",", "");
                        amountio = dt.Rows[j][5].ToString().Replace(",", "");
                        budget = dt.Rows[j][6].ToString().Replace(",", "");
                        variance = dt.Rows[j][7].ToString().Replace(",", "");

                        import.costcenter_io = dt.Rows[j][0].ToString().Trim();
                        import.department = dt.Rows[j][1].ToString().Trim();
                        import.description = dt.Rows[j][2].ToString().Trim();


                        import.cost = _func_dmu.zStringToDecimal(cost);
                        import.unit = int.Parse(dt.Rows[j][4].ToString());
                        import.amountio = _func_dmu.zStringToDecimal(amountio);

                        if (dt.Rows[j][6].ToString() == "")
                        {
                            import.budget = 0;

                        }
                        else
                        {
                            import.budget = _func_dmu.zStringToDecimal(budget);

                        }

                        if (dt.Rows[j][7].ToString() == "")
                        {
                            import.variance = 0;

                        }
                        else
                        {
                            import.variance = _func_dmu.zStringToDecimal(variance);

                        }

                        import.iono = dt.Rows[j][8].ToString().Trim();

                        if (dt.Rows[j][9].ToString() == "")
                        {
                            import.newio = 0;

                        }
                        else
                        {
                            import.newio = int.Parse(dt.Rows[j][9].ToString());

                        }

                        if (dt.Rows[j][10].ToString() == "")
                        {
                            import.replaceio = 0;

                        }
                        else
                        {
                            import.replaceio = int.Parse(dt.Rows[j][10].ToString());

                        }

                        import.no_cost = dt.Rows[j][11].ToString().Trim();
                        import.expenses_year = dt.Rows[j][12].ToString().Trim();
                        import.asset_type = dt.Rows[j][13].ToString().Trim();
                        import.no_order = dt.Rows[j][14].ToString().Trim();

                        if (dt.Rows[j][15].ToString() == "" || dt.Rows[j][15].ToString() == "N")
                        {
                            import.status = 0;

                        }
                        else
                        {
                            import.status = 1;

                        }

                        import.year_io = int.Parse(dt.Rows[j][16].ToString());


                        import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                        _dtitseet.boxmaster_io[0] = import;
                        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                        _dtitseet = callServicePostITAsset(_urlInsert_Master_IOBuy, _dtitseet);

                    }
                    j++;
                }
                break;
        }






    }

    #endregion

    #region Update
    protected void Update_Master_List()
    {
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset update = new class_itasset();

        update.code_class = ViewState["txtname_edit"].ToString();
        update.code_desc = ViewState["txtcode_edit"].ToString();
        update.code_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.clidx = int.Parse(ViewState["clidx"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxmaster_class[0] = update;

        _dtitseet = callServicePostITAsset(_urlInsertClass, _dtitseet);



    }

    protected void Delete_Master_List()
    {
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset delete = new class_itasset();

        delete.clidx = int.Parse(ViewState["clidx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtitseet.boxmaster_class[0] = delete;

        _dtitseet = callServicePostITAsset(_urlDeleteClass, _dtitseet);
    }

    protected void UpdateIO_Master_List()
    {
        _dtitseet.boxmaster_io = new io_itasset[1];
        io_itasset updateio = new io_itasset();

        updateio.cost = int.Parse(ViewState["txtcost_update"].ToString());
        updateio.amountio = int.Parse(ViewState["txtamountio_update"].ToString());
        updateio.budget = int.Parse(ViewState["txtbudget_update"].ToString());
        updateio.variance = int.Parse(ViewState["txtvariance_update"].ToString());
        updateio.ioidx = int.Parse(ViewState["ioidx"].ToString());
        updateio.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxmaster_io[0] = updateio;

        _dtitseet = callServicePostITAsset(_urlUpdate_Master_IOBuy, _dtitseet);



    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        txtcode.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void GridViewTrigger(GridView gridview)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = gridview.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected string getStatusApprove(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-square'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-minus-circle'></i></span>";
        }
    }

    #region SetDefault BuyNew
    protected void CreateDsits_u1_document()
    {
        string sDs = "dsits_u1_document";
        string sVs = "vsits_u1_document";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("list_buynew", typeof(String));
        ds.Tables[sDs].Columns.Add("qty_buynew", typeof(int));
        ds.Tables[sDs].Columns.Add("price_buynew", typeof(int));
        ds.Tables[sDs].Columns.Add("u1idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0idx", typeof(String));
        ds.Tables[sDs].Columns.Add("clidx", typeof(String));
        ds.Tables[sDs].Columns.Add("asidx", typeof(String));
        ds.Tables[sDs].Columns.Add("sysidx", typeof(String));
        ds.Tables[sDs].Columns.Add("sysidx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("typidx", typeof(String));
        ds.Tables[sDs].Columns.Add("typidx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("tdidx", typeof(String));
        ds.Tables[sDs].Columns.Add("tdidx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("pr_type_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("purchase_type_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("ionum", typeof(String));
        ds.Tables[sDs].Columns.Add("ref_itnum", typeof(String));
        ds.Tables[sDs].Columns.Add("rpos_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("rpos_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("list_menu", typeof(String));
        ds.Tables[sDs].Columns.Add("price", typeof(String));
        ds.Tables[sDs].Columns.Add("currency_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("currency_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("qty", typeof(String));
        ds.Tables[sDs].Columns.Add("unidx", typeof(String));
        ds.Tables[sDs].Columns.Add("unidx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("costidx", typeof(String));
        ds.Tables[sDs].Columns.Add("costcenter_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("budget_set", typeof(String));
        ds.Tables[sDs].Columns.Add("place_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("place_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("acquls_val", typeof(String));
        ds.Tables[sDs].Columns.Add("book_val", typeof(String));
        ds.Tables[sDs].Columns.Add("action", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_status", typeof(String));
        ds.Tables[sDs].Columns.Add("CEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("CreateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("UEmpIDX", typeof(String));
        ds.Tables[sDs].Columns.Add("UpdateDate", typeof(String));
        ds.Tables[sDs].Columns.Add("spec_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("spec_idx_name", typeof(String));
        ds.Tables[sDs].Columns.Add("leveldevice", typeof(String));
        ds.Tables[sDs].Columns.Add("total_amount", typeof(String));

        ds.Tables[sDs].Columns.Add("node_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("actor_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("zdevices_name", typeof(String));

        ds.Tables[sDs].Columns.Add("u2idx", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_no", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_item", typeof(String));

        ds.Tables[sDs].Columns.Add("asset_emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_approve_status", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_remark", typeof(String));

        ds.Tables[sDs].Columns.Add("asset_dir_emp_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_dir_approve_status", typeof(String));
        ds.Tables[sDs].Columns.Add("asset_dir_approve_remark", typeof(String));

        ds.Tables[sDs].Columns.Add("operation_status_id", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("flow_item", typeof(String));
        ds.Tables[sDs].Columns.Add("doc_status", typeof(String));
        ds.Tables[sDs].Columns.Add("spec_name", typeof(String));
        ds.Tables[sDs].Columns.Add("zu0idx", typeof(String));
        ds.Tables[sDs].Columns.Add("selected", typeof(Boolean));

        ds.Tables[sDs].Columns.Add("doccode", typeof(String));
        ds.Tables[sDs].Columns.Add("zdocdate", typeof(String));
        ds.Tables[sDs].Columns.Add("pr_idx", typeof(String));

        ds.Tables[sDs].Columns.Add("u0_docket_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_docket_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_docket_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("spec_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_name_th", typeof(String));
        ds.Tables[sDs].Columns.Add("pr_remark", typeof(String));

        ds.Tables[sDs].Columns.Add("posidx", typeof(String));
        ds.Tables[sDs].Columns.Add("pos_name", typeof(String));
        ds.Tables[sDs].Columns.Add("m1_ref_idx", typeof(String));

        ds.Tables[sDs].Columns.Add("org_idx_its", typeof(String));
        ds.Tables[sDs].Columns.Add("rdept_idx_its", typeof(String));
        ds.Tables[sDs].Columns.Add("rsec_idx_its", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_didx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_code", typeof(String));

        ds.Tables[sDs].Columns.Add("special_flag", typeof(String));


        ViewState[sVs] = ds;

    }
    protected void setAddList_BuyNew()
    {
        decimal _total_amount = 0;
        if (ViewState["vsits_u1_document"] != null)
        {
            setObject();
            if (
                //( _func_dmu.zStringToInt(_ddlsystem.SelectedValue) != 3 )
                // &&
                // (_func_dmu.zStringToInt(_ddlSearchTypeDevices.SelectedValue) != 10)

                _ddlspecidx.Enabled == false

                )
            {
                Select_master_spec(
                         _func_dmu.zStringToInt(_ddlsystem.SelectedValue)
                        , _func_dmu.zStringToInt(_ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zStringToInt(_ddlSearchDevices.SelectedValue)
                        , _func_dmu.zStringToInt(_ddl_target.SelectedValue)
                        );
            }


            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];

            foreach (DataRow dr in dsContacts.Tables["dsits_u1_document"].Rows)
            {
                if (
                       dr["list_menu"].ToString() == _txtlist_menu.Text &&
                       dr["qty"].ToString() == _txtqty.Text &&
                       dr["price"].ToString() == _txtprice.Text &&
                       dr["ionum"].ToString().ToUpper() == _txtionum.Text.ToUpper() &&
                       _func_dmu.zStringToInt(dr["pr_type_idx"].ToString()) == _func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue)

                       )
                {
                    showMsAlert("มีข้อมูล ประเภทการขอซื้อ,เลขที่ IO,รายละเอียด,ราคา,จำนวน นี้แล้ว!!!");
                    return;
                }
                else if (_func_dmu.zStringToInt(dr["sysidx"].ToString()) != _func_dmu.zStringToInt(_ddlsystem.SelectedValue))
                {
                    showMsAlert("กรุณาเลือกประเภททรัพย์สินที่ขอซื้อให้เป็นประเภททรัพย์สินเดียวกัน !!!");
                    return;
                }

            }
            if (
                (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 2)
                     &&
                     (_txtref_itnum.Text.Trim() == "")
                )
            {
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกเลขที่ตัดเสีย!!!');", true);
                showMsAlert("กรุณากรอกเลขที่ตัดเสีย!!!");
                return;
            }
            else if ((_ddlspecidx.Text.Trim() == "") && (_func_dmu.zStringToInt(_ddlsystem.SelectedValue) == 3))
            {
                // ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกเลขที่ตัดเสีย!!!');", true);
                showMsAlert("กรุณากรอกสเปค!!!");
                return;
            }
            //ckeck io no

            its_masterit obj_m = new its_masterit();
            _dtitseet.its_masterit_action = new its_masterit[1];
            obj_m.pr_type_idx = _func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue);

            if (ViewState["mode"] != null)
            {
                if (ViewState["mode"].ToString() == "E")
                {
                    if (ViewState["u0idx"] != null)
                    {
                        obj_m.u0idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
                    }
                    else
                    {
                        obj_m.u0idx = 0;
                    }
                }
                else
                {
                    obj_m.u0idx = 0;
                }
            }
            else
            {
                obj_m.u0idx = 0;
            }

            obj_m.iono = _txtionum.Text.Trim();
            obj_m.costcenter_idx = _func_dmu.zStringToInt(_ddlcostcenter.SelectedValue);
            obj_m.operation_status_id = "its_m0_io_budget";
            _dtitseet.its_masterit_action[0] = obj_m;
            _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_masterit, _dtitseet);

            int _qty = 0, _out_qty = 0;
            string _iono = "";
            if (_dtitseet.its_masterit_action != null)
            {
                foreach (var item in _dtitseet.its_masterit_action)
                {
                    _qty = _qty + item.balance_qty;
                    _iono = item.iono;
                }
            }
            if (_qty == 0)
            {

                showMsAlert("ไม่อนุญาตให้ใช้ IO " + _txtionum.Text.Trim() +
                    " เนื่องจากมีการนำเลขที่ IO " + _txtionum.Text.Trim() +
                    " ไปใช้ในการออกใบขอซื้อครบตามจำนวนที่ขอใน IO เรียบร้อยแล้ว " +
                    " หากท่านต้องการทำรายการขอซื้อกรุณาติดต่อฝ่ายงบประมาณ " +
                    " !!!");
                return;
            }
            else
            {
                int iError = 0;
                if (_dtitseet.its_masterit_action != null)
                {

                    foreach (var item in _dtitseet.its_masterit_action)
                    {
                        decimal price = 0, budget = 0;
                        price = _func_dmu.zStringToDecimal(_txtprice.Text);
                        budget = _func_dmu.zStringToDecimal(_txtbudget_set.Text);
                        if (price <= item.cost)
                        {
                            //ขอซื้อได้
                        }
                        else
                        {
                            iError++;
                            showMsAlert("ราคาเกินงบของเลขที่ IO : " + _txtionum.Text + " !!!");
                            return;
                        }
                        if (budget <= item.budget)
                        {
                            //ขอซื้อได้
                        }
                        else
                        {
                            iError++;
                            showMsAlert("งบประมาณที่ตั้งไว้เกินงบของเลขที่ IO : " + _txtionum.Text + " !!!");
                            return;
                        }
                    }
                }

                if (iError == 0)
                {
                    dsContacts = (DataSet)ViewState["vsits_u1_document"];
                    _out_qty = _func_dmu.zStringToInt(_txtqty.Text);
                    foreach (DataRow dr in dsContacts.Tables["dsits_u1_document"].Rows)
                    {
                        if (
                            (_func_dmu.zStringToInt(dr["pr_type_idx"].ToString()) ==
                            _func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue)
                            )
                            &&
                            (dr["ionum"].ToString().ToUpper() ==
                             _txtionum.Text.Trim().ToUpper()
                            )
                           )
                        {
                            _out_qty = _out_qty + _func_dmu.zStringToInt(dr["qty"].ToString());
                        }

                    }
                    //  litDebug.Text = _out_qty.ToString()+"=" + _qty.ToString();
                    if (_out_qty > _qty)
                    {
                        // showMsAlert("เลขที่ IO : " + _txtionum.Text.Trim() + " ไม่พอ กรุณาตรวจสอบอีกครั้ง !!!");
                        showMsAlert("เลขที่ IO : " + _txtionum.Text.Trim() + " งบประมาณที่ตั้งไว้ไม่เพียงพอรบกวนติดต่อฝ่ายงบประมาณ !!!");
                        return;
                    }
                }

            }


            DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

            drContacts["sysidx"] = _func_dmu.zStringToInt(_ddlsystem.SelectedValue);
            drContacts["sysidx_name"] = _func_dmu.zGetTextDropDownList(_ddlsystem);

            drContacts["typidx"] = _func_dmu.zStringToInt(_ddlSearchTypeDevices.SelectedValue);
            drContacts["typidx_name"] = _func_dmu.zGetTextDropDownList(_ddlSearchTypeDevices);

            drContacts["tdidx"] = _func_dmu.zStringToInt(_ddlSearchDevices.SelectedValue);
            drContacts["tdidx_name"] = _func_dmu.zGetTextDropDownList(_ddlSearchDevices);

            drContacts["pr_type_idx"] = _func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue);
            drContacts["purchase_type_idx_name"] = _func_dmu.zGetTextDropDownList(_ddlsearch_typepurchase);

            drContacts["ionum"] = _txtionum.Text.ToUpper();
            drContacts["ref_itnum"] = _txtref_itnum.Text;

            drContacts["rpos_idx"] = _func_dmu.zStringToInt(_ddlposition.SelectedValue);
            drContacts["rpos_idx_name"] = _func_dmu.zGetTextDropDownList(_ddlposition);

            drContacts["list_menu"] = _txtlist_menu.Text;

            drContacts["price"] = _func_dmu.zStringToDecimal(_txtprice.Text);

            drContacts["currency_idx"] = _func_dmu.zStringToInt(_ddlcurrency.SelectedValue);
            drContacts["currency_idx_name"] = _func_dmu.zGetTextDropDownList(_ddlcurrency);

            drContacts["qty"] = _func_dmu.zStringToInt(_txtqty.Text);

            drContacts["unidx"] = _func_dmu.zStringToInt(_ddlunit.SelectedValue);
            drContacts["unidx_name"] = _func_dmu.zGetTextDropDownList(_ddlunit);

            drContacts["costidx"] = _func_dmu.zStringToInt(_ddlcostcenter.SelectedValue);
            drContacts["costcenter_idx_name"] = _func_dmu.zGetTextDropDownList(_ddlcostcenter);

            drContacts["budget_set"] = _func_dmu.zStringToDecimal(_txtbudget_set.Text);

            drContacts["place_idx"] = _func_dmu.zStringToInt(_ddlplace.SelectedValue);
            drContacts["place_idx_name"] = _func_dmu.zGetTextDropDownList(_ddlplace);

            drContacts["spec_idx"] = 0;//_func_dmu.zStringToInt(ddlspecidx.SelectedValue);
            drContacts["spec_idx_name"] = "";// _func_dmu.zGetTextDropDownList(ddlspecidx);

            drContacts["leveldevice"] = _ddlspecidx.Text;

            _total_amount = _func_dmu.zStringToDecimal(_txtprice.Text) * _func_dmu.zStringToInt(_txtqty.Text);
            drContacts["total_amount"] = _total_amount;

            drContacts["posidx"] = _func_dmu.zStringToInt(_ddl_target.SelectedValue);
            drContacts["pos_name"] = _func_dmu.zGetTextDropDownList(_ddl_target);

            drContacts["m1_ref_idx"] = _func_dmu.zStringToInt(_txt_m1_ref_idx.Text);
            drContacts["special_flag"] = getspecial(_check_special);

            dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);
            ViewState["vsits_u1_document"] = dsContacts;
            _GvReportAdd.Visible = true;
            setGridData(_GvReportAdd, dsContacts.Tables["dsits_u1_document"]);
            setInsertPurchas_RqDefault("add");
        }
    }
    private int getspecial(CheckBox checkbox)
    {
        int ispecial = 0;
        if (checkbox.Checked == true)
        {
            ispecial = 1;
        }

        return ispecial;

    }
    protected void CleardataSetBuyNewList()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        GridView GvReportAdd = (GridView)_FormView.FindControl("GvReportAdd");
        ViewState["vsits_u1_document"] = null;
        GvReportAdd.DataSource = ViewState["vsits_u1_document"];
        GvReportAdd.DataBind();
        CreateDsits_u1_document();

    }

    #endregion

    #region SetDefault BuyReplace
    protected void SetViewState_BuyReplace()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable_BuyReplace");
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("list_buyreplace", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("qty_buyreplace", typeof(int));
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("price_buyreplace", typeof(int));
        ViewState["vsBuyequipment_BuyReplace"] = dsFoodMaterialList;

    }
    protected void setAddList_BuyReplace()
    {
        if (ViewState["vsBuyequipment_BuyReplace"] != null)
        {
            TextBox txtlist_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtlist_buyreplace");
            TextBox txtqty_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtqty_buyreplace");
            TextBox txtprice_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtprice_buyreplace");
            GridView GvReportAdd_BuyReplace = (GridView)FvInsertBuyReplace.FindControl("GvReportAdd_BuyReplace");


            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment_BuyReplace"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_BuyReplace"].Rows)
            {
                if (dr["list_buyreplace"].ToString() == txtlist_buyreplace.Text &&
                       dr["qty_buyreplace"].ToString() == txtqty_buyreplace.Text &&
                       dr["price_buyreplace"].ToString() == txtprice_buyreplace.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsAddListTable_BuyReplace"].NewRow();
            drContacts["list_buyreplace"] = txtlist_buyreplace.Text;
            drContacts["qty_buyreplace"] = int.Parse(txtqty_buyreplace.Text);
            drContacts["price_buyreplace"] = int.Parse(txtprice_buyreplace.Text);

            dsContacts.Tables["dsAddListTable_BuyReplace"].Rows.Add(drContacts);
            ViewState["vsBuyequipment_BuyReplace"] = dsContacts;
            GvReportAdd_BuyReplace.Visible = true;
            setGridData(GvReportAdd_BuyReplace, dsContacts.Tables["dsAddListTable_BuyReplace"]);

        }
    }
    protected void CleardataSetBuyReplaceList()
    {
        var GvReportAdd_BuyReplace = (GridView)FvInsertBuyReplace.FindControl("GvReportAdd_BuyReplace");
        ViewState["vsBuyequipment_BuyReplace"] = null;
        GvReportAdd_BuyReplace.DataSource = ViewState["vsBuyequipment_BuyReplace"];
        GvReportAdd_BuyReplace.DataBind();
        SetViewState_BuyReplace();
    }

    #endregion

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvMasterIO":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMasterIO.EditIndex != e.Row.RowIndex)
                    {
                        var lbCost = ((Label)e.Row.FindControl("lbCost"));
                        var lbAmount = ((Label)e.Row.FindControl("lbAmount"));
                        var lbBudget = ((Label)e.Row.FindControl("lbBudget"));
                        var lbVariance = ((Label)e.Row.FindControl("lbVariance"));

                        if (lbCost.Text != "")
                        {
                            lbCost.Text = string.Format("{0:N2}", Convert.ToDecimal(lbCost.Text));

                        }
                        if (lbAmount.Text != "")
                        {
                            lbAmount.Text = string.Format("{0:N2}", Convert.ToDecimal(lbAmount.Text));

                        }
                        if (lbBudget.Text != "")
                        {
                            lbBudget.Text = string.Format("{0:N2}", Convert.ToDecimal(lbBudget.Text));

                        }
                        if (lbVariance.Text != "")
                        {
                            lbVariance.Text = string.Format("{0:N2}", Convert.ToDecimal(lbVariance.Text));

                        }

                    }

                }
                break;

            case "GvAsset_Buy":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvAsset_Buy.EditIndex != e.Row.RowIndex)
                    {
                        var lbacq = ((Label)e.Row.FindControl("lbacq"));

                        if (lbacq.Text != "")
                        {
                            lbacq.Text = string.Format("{0:N2}", Convert.ToDecimal(lbacq.Text));

                        }
                    }

                }
                break;

            case "GvMastercost":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
        }

    }


    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList_Class();

                break;

            case "GvAsset_Buy":
                GvAsset_Buy.PageIndex = e.NewPageIndex;
                GvAsset_Buy.DataBind();
                SelectMasterList_AssetBuy();
                break;

            case "GvMasterIO":
                GvMasterIO.PageIndex = e.NewPageIndex;
                GvMasterIO.DataBind();
                SelectMasterList_IO();
                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList_Class();
                btnshow.Visible = false;
                break;

            case "GvMasterIO":

                GvMasterIO.EditIndex = e.NewEditIndex;
                CmdAdd_ImportIO.Visible = false;
                CmdAdd_SearchIO.Visible = false;
                SelectMasterList_IO();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList_Class();
                btnshow.Visible = true;
                break;

            case "GvMasterIO":
                GvMasterIO.EditIndex = -1;
                CmdAdd_ImportIO.Visible = true;
                CmdAdd_SearchIO.Visible = true;
                SelectMasterList_IO();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int clidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["clidx"] = clidx;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList_Class();

                break;

            case "GvMasterIO":
                int lblioidx = Convert.ToInt32(GvMasterIO.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcost_update = (TextBox)GvMasterIO.Rows[e.RowIndex].FindControl("txtcost_update");
                var txtamountio_update = (TextBox)GvMasterIO.Rows[e.RowIndex].FindControl("txtamountio_update");
                var txtbudget_update = (TextBox)GvMasterIO.Rows[e.RowIndex].FindControl("txtbudget_update");
                var txtvariance_update = (TextBox)GvMasterIO.Rows[e.RowIndex].FindControl("txtvariance_update");

                GvMasterIO.EditIndex = -1;

                ViewState["ioidx"] = lblioidx;
                ViewState["txtcost_update"] = txtcost_update.Text;
                ViewState["txtamountio_update"] = txtamountio_update.Text;
                ViewState["txtbudget_update"] = txtbudget_update.Text;
                ViewState["txtvariance_update"] = txtvariance_update.Text;


                UpdateIO_Master_List();
                SelectMasterList_IO();
                CmdAdd_ImportIO.Visible = true;
                CmdAdd_SearchIO.Visible = true;

                break;

        }
    }

    #endregion

    #region FormView

    private void setDefaultPurchase()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        LinkButton btnAddBuy_Newlist = (LinkButton)_FormView.FindControl("btnAddBuy_Newlist");
        LinkButton btnAdddata = (LinkButton)_FormView.FindControl("btnAdddata");
        LinkButton AddCancel = (LinkButton)_FormView.FindControl("AddCancel");
        LinkButton btn_spec_search = (LinkButton)_FormView.FindControl("AddCancel");
        GridView GvReportAdd = (GridView)_FormView.FindControl("GvReportAdd");

        linkBtnTrigger(btnAddBuy_Newlist);
        linkBtnTrigger(btnAdddata);
        linkBtnTrigger(AddCancel);
        linkBtnTrigger(btn_spec_search);
        GridViewTrigger(GvReportAdd);
    }
    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;
        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewBuy.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));
                    var txt_ApproveDir = ((TextBox)FvDetailUser.FindControl("txt_ApproveDir"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                    txt_ApproveDir.Text = ViewState["Approve_Name"].ToString();
                }
                break;
            case "FvInsertBuyNew":
                if (FvName.CurrentMode == FormViewMode.Insert)
                {

                    setDefaultPurchase();
                    setInsertPurchas_RqDefault("empty");

                }
                break;

            case "FvInsertBuyReplace":
                FormView FvInsertBuyReplace = (FormView)ViewBuy.FindControl("FvInsertBuyReplace");
                DropDownList ddlsystem_replace = (DropDownList)FvInsertBuyReplace.FindControl("ddlsystem_replace");
                if (FvInsertBuyReplace.CurrentMode == FormViewMode.Insert)
                {
                    select_system(ddlsystem_replace);
                }
                break;

            case "FvInsertCutDevices":
                FormView FvInsertCutDevices = (FormView)ViewBuy.FindControl("FvInsertCutDevices");
                DropDownList ddlsystem_cutdevices = (DropDownList)FvInsertCutDevices.FindControl("ddlsystem_cutdevices");
                if (FvInsertCutDevices.CurrentMode == FormViewMode.Insert)
                {
                    select_system(ddlsystem_cutdevices);
                }
                break;
            case "FvDetailUser_Approve":
                if (FvDetailUser_Approve.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetailUser_Budget":
                if (FvDetailUser_Budget.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetail_ShowBudget":
                if (FvDetail_ShowBudget.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetailUser_Asset":
                if (FvDetailUser_Asset.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetail_ShowAsset":
                if (FvName.CurrentMode == FormViewMode.Edit)
                {
                    DropDownList dll = (DropDownList)FvName.FindControl("ddlStatusapprove");
                    int imode = 0;
                    if (ViewState["mode"].ToString() == "asset_dir1")
                    {
                        imode = 1;
                    }
                    else
                    {
                        imode = 0;
                    }
                    select_m0status(dll, imode);

                }
                break;
            case "fvmanage_approvepurchase":

                LinkButton btnApproveDirector = (LinkButton)FvName.FindControl("btnApproveDirector");
                LinkButton btnApproveDirector_back = (LinkButton)FvName.FindControl("btnApproveDirector_back");

                linkBtnTrigger(btnApproveDirector);
                linkBtnTrigger(btnApproveDirector_back);
                if (FvName.CurrentMode == FormViewMode.Insert)
                {
                    DropDownList dll = (DropDownList)FvName.FindControl("ddlStatusapprove");
                    int imode = 0;
                    if (ViewState["flow_item"] != null)
                    {
                        if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 14)
                        {
                            imode = 2;
                        }
                        else
                        {
                            imode = 0;
                        }
                    }
                    else
                    {
                        imode = 0;
                    }
                    select_m0status(dll, imode);
                }

                break;

        }
    }


    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            int rowIndex = 0;
            GridView _grdView;
            GridViewRow rowSelect;
            DataSet dsContacts;
            switch (cmdName)
            {
                case "bnDeleteList":
                    _FormView = getFv(ViewState["mode"].ToString());
                    _grdView = (GridView)_FormView.FindControl("GvReportAdd");
                    rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    rowIndex = rowSelect.RowIndex;
                    dsContacts = (DataSet)ViewState["vsits_u1_document"];
                    dsContacts.Tables["dsits_u1_document"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(_grdView, dsContacts.Tables["dsits_u1_document"]);
                    if (dsContacts.Tables["dsits_u1_document"].Rows.Count < 1)
                    {
                        _grdView.Visible = false;
                    }
                    break;

            }
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        DropDownList ddlsystem, ddlSearchTypeDevices, ddlSearchDevices, ddl_target;
        TextBox ddlspecidx, txtlist_buynew, txtprice_buynew;
        LinkButton btnDevice;
        Label lbu0_didx;
        Panel pnl_other;

        _FormView = getFv(ViewState["mode"].ToString());
        switch (ddName.ID)
        {


            case "ddlcondition":



                if (ddlcondition.SelectedValue == "3")
                {
                    txtenddate.Enabled = true;
                }

                else
                {

                    txtenddate.Enabled = false;

                }

                if (ddlcondition.SelectedValue == "0")

                {
                    txtenddate.Text = String.Empty;
                    txtstartdate.Text = String.Empty;

                }

                else
                {


                }




                break;

            case "ddlsystem_cutdevices":
                DropDownList ddlsystem_cutdevices = (DropDownList)FvInsertCutDevices.FindControl("ddlsystem_cutdevices");
                Control div_doccode = (Control)FvInsertCutDevices.FindControl("div_doccode");
                TextBox txtreason_cutdevices = (TextBox)FvInsertCutDevices.FindControl("txtreason_cutdevices");
                TextBox txtasset_cutdevices = (TextBox)FvInsertCutDevices.FindControl("txtasset_cutdevices");
                if (ddlsystem_cutdevices.SelectedValue != "9")
                {
                    div_doccode.Visible = true;
                    txtreason_cutdevices.Enabled = false;
                    txtasset_cutdevices.Enabled = false;

                }
                else
                {
                    div_doccode.Visible = false;
                    txtreason_cutdevices.Enabled = true;
                    txtasset_cutdevices.Enabled = true;
                }

                break;
            case "ddlsystem":
                ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
                ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
                ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
                _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");

                select_systemtype(ddlSearchTypeDevices, _func_dmu.zStringToInt(ddName.SelectedValue));
                select_devices(ddlSearchDevices, _func_dmu.zStringToInt(ddName.SelectedValue), _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue));

                select_ddlsearch_purchase(_ddlsearch_typepurchase);
                ddl_target = (DropDownList)_FormView.FindControl("ddl_target");

                Select_master_spec(
                         _func_dmu.zStringToInt(ddName.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddl_target.SelectedValue)
                        );


                break;
            case "ddlSearchTypeDevices":
                ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
                ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
                ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
                ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
                _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");

                select_devices(ddlSearchDevices, _func_dmu.zStringToInt(ddlsystem.SelectedValue), _func_dmu.zStringToInt(ddName.SelectedValue));
                //ddlspecidx.Text = "";
                select_ddlsearch_purchase(_ddlsearch_typepurchase);
                ddl_target = (DropDownList)_FormView.FindControl("ddl_target");
                //ddlspecidx.Text = "";
                Select_master_spec(
                         _func_dmu.zStringToInt(ddlsystem.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddl_target.SelectedValue)
                        );
                break;


            case "ddlSearchDevices":
                ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
                ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
                ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
                ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
                ddl_target = (DropDownList)_FormView.FindControl("ddl_target");
                //ddlspecidx.Text = "";
                Select_master_spec(
                         _func_dmu.zStringToInt(ddlsystem.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddl_target.SelectedValue)
                        );
                break;

            case "ddlspecidx":
                ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
                ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
                ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
                txtlist_buynew = (TextBox)_FormView.FindControl("txtlist_buynew");
                txtprice_buynew = (TextBox)_FormView.FindControl("txtprice_buynew");

                Select_master_spec_detail(txtlist_buynew, txtprice_buynew
                        , _func_dmu.zStringToInt(ddlsystem.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zGetDataDropDownList(ddName)
                        , _func_dmu.zStringToInt(ddlSearchDevices.SelectedValue)
                        );
                break;

            case "ddlsearch_typepurchase":
                setPanel_doc_number();

                break;
            case "ddlsearch_organization":
                action_select_depertment(0, "0");

                break;
            case "ddlcondition_md":
                setObject_it_docket_md();
                if (ddName.SelectedValue == "3")
                {
                    _txtenddate.Enabled = true;
                }

                else
                {
                    _txtenddate.Enabled = false;
                }

                if (ddName.SelectedValue == "0")
                {
                    _txtenddate.Text = String.Empty;
                    _txtstartdate.Text = String.Empty;
                }

                else
                {

                }

                break;
            case "ddlStatusapproveSearch":

                setStatusApprove();

                break;
            case "ddlcostcenter":

                setionumber_default();

                break;
            case "ddl_target":
                ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
                ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
                ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");

                Select_master_spec(
                         _func_dmu.zStringToInt(ddlsystem.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddlSearchDevices.SelectedValue)
                        , _func_dmu.zStringToInt(ddName.SelectedValue)
                        );
                break;
            case "ddlpr_idx":
                GridViewRow rowSelect = (GridViewRow)ddName.NamingContainer;
                btnDevice = (LinkButton)rowSelect.FindControl("btnDevice");
                lbu0_didx = (Label)rowSelect.FindControl("_lbu0_didx");
                TextBox txt_sysidx = (TextBox)FvDetail_ShowDelivery.FindControl("txt_sysidx");
                Label lbsysidx = (Label)rowSelect.FindControl("_lbsysidx");
                Label lbtypidx = (Label)rowSelect.FindControl("_lbtypidx");
                if (ddName.SelectedValue == "1")
                {

                    if (
                        ((lbsysidx.Text == "3") && ((lbtypidx.Text == "1") || (lbtypidx.Text == "10")))
                        ||
                        (lbsysidx.Text == "11")
                        )
                    {
                        btnDevice.Visible = true;
                    }
                    else
                    {
                        btnDevice.Visible = false;
                    }
                }
                else
                {
                    btnDevice.Visible = false;
                }
                if (_func_dmu.zStringToInt(lbu0_didx.Text) > 0)
                {
                    btnDevice.Visible = false;
                }
                if (txt_sysidx.Text == "9")
                {
                    btnDevice.Visible = false;
                }
                break;
            
            case "ddlSearchReport":
                setddlSearchReport(ddlSearchReport.SelectedValue);
                break;

        }
    }
    #endregion

    private void setddlSearchReport(string Status )
    {
        if (Status == "1")
        {
            pnl_asset_report.Visible = false;
            pnlsearch_asset2.Visible = false;
            pnl_print_plan_io.Visible = true;
            pnl_print_plan.Visible = false;
            pnl_BalanceIO.Visible = true;
            pnl_seach_asset2.Visible = false;
            ddlcondition.SelectedValue = "0";
            txtstartdate.Enabled = true;
            txtenddate.Enabled = false;
            txtstartdate.Text = "";
            txtenddate.Text = "";
            pnl_seach_asset1.Visible = false;
        }
        else
        {
            pnl_asset_report.Visible = true;
            pnl_seach_asset2.Visible = true;
            pnlsearch_asset2.Visible = true;
            pnl_print_plan_io.Visible = false;
            pnl_print_plan.Visible = true;
            pnl_BalanceIO.Visible = false;
            ddlcondition.SelectedValue = "3";
            txtstartdate.Enabled = true;
            txtenddate.Enabled = true;
            txtstartdate.Text = System.DateTime.Now.ToString("1/MM/yyyy");
            txtenddate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            pnl_seach_asset1.Visible = true;
        }
    }


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        int _u0_idx = 0;
        int _U0IDX = 0;
        HiddenField hidden_m0_node, hidden_m0_actor;
        _FormView = getFv(ViewState["mode"].ToString());
        switch (cmdName)
        {

            case "CmdAdd_ImportIO":
                Panel_ImportIOAdd.Visible = true;
                Panel_SearchIO.Visible = false;
                CmdAdd_ImportIO.Visible = false;
                CmdAdd_SearchIO.Visible = false;

                break;

            case "CmdAdd_SearchIO":
                Panel_ImportIOAdd.Visible = false;
                Panel_SearchIO.Visible = true;
                CmdAdd_ImportIO.Visible = false;
                CmdAdd_SearchIO.Visible = false;
                select_YearMasterIO(ddlsearch_yeario);
                select_CostcenterMasterIO(ddlsearch_costcenterio);
                txtsearch_io.Text = "";
                break;

            case "btnsearch_importio":
                ViewState["status_searchio"] = 1;
                SelectMasterList_IO();
                break;

            case "btnSearchImportio":
                if (uploadio.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(uploadio.PostedFile.FileName);
                    string extension = Path.GetExtension(uploadio.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_asset_importio"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        uploadio.SaveAs(filePath);
                        //ImportFileBuyDevices(filePath, extension, "Yes", FileName, "budget");
                        ImportFileIO(filePath, extension, "Yes", FileName, "budget");
                        // File.Delete(filePath);
                        //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        showMsAlert("Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                }
                /*
                Panel_ImportIOAdd.Visible = false;
                Panel_SearchIO.Visible = false;
                CmdAdd_ImportIO.Visible = true;
                CmdAdd_SearchIO.Visible = true;
                ViewState["status_searchio"] = 1;
                SelectMasterList_IO();
                */
                setActiveTab("budget_io");
                ViewState["mode"] = "budget";
                ViewState["status_searchio"] = 0;
                MvMaster.SetActiveView(ViewBudget_Master);
                ViewState["status_searchio"] = 1;
                SelectMasterList_IO();
                break;

            case "btnImportio":
                zsave_ImportFileIO();
                Panel_ImportIOAdd.Visible = false;
                Panel_SearchIO.Visible = false;
                CmdAdd_ImportIO.Visible = true;
                CmdAdd_SearchIO.Visible = true;
                ViewState["status_searchio"] = 1;
                SelectMasterList_IO();

                break;

            case "btnCancel_Importio":
                Panel_ImportIOAdd.Visible = false;
                Panel_SearchIO.Visible = false;
                CmdAdd_ImportIO.Visible = true;
                CmdAdd_SearchIO.Visible = true;
                ViewState["status_searchio"] = 0;
                SelectMasterList_IO();
                break;

            case "_divMenuBtnToDivIndex":
                setActiveTab("p");
                ViewState["mode"] = "P";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "_divMenuBtnToDivBuyNew":
                setActiveTab("insert");
                ViewState["mode"] = "I";

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = true;
                div_createbuyreplace.Visible = false;
                div_createcutdevices.Visible = false;
                FvInsertBuyNew.ChangeMode(FormViewMode.Insert);
                FvInsertBuyNew.DataBind();
                CleardataSetBuyNewList();
                setObject();
                setPanel_doc_number();
                FvInsertBuyNew.Visible = true;
                FvUpdateBuyNew.Visible = false;
                content_history_purchase_edit.Visible = false;
                break;
            case "_divMenuBtnToDivBuyReplace":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                // _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = false;
                div_createbuyreplace.Visible = true;
                div_createcutdevices.Visible = false;
                FvInsertBuyReplace.ChangeMode(FormViewMode.Insert);
                FvInsertBuyReplace.DataBind();
                CleardataSetBuyReplaceList();



                break;
            case "_divMenuBtnToDivCutDevices":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Add("class", "active");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                //_divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = false;
                div_createbuyreplace.Visible = false;
                div_createcutdevices.Visible = true;

                FvInsertCutDevices.ChangeMode(FormViewMode.Insert);
                FvInsertCutDevices.DataBind();


                break;

            case "_divMenuBtnToDivWaitApprove_Buy":

                ViewState["cmdArg_typesys"] = "Approve_Buy";
                setActiveTab("dir_user_approve");
                ViewState["mode"] = "dir_user_approve";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;

            case "_divMenuBtnToDivWaitApprove_CutDevices":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                //_divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                ViewState["cmdArg_typesys"] = "Approve_CutDevices";

                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;

                break;

            case "_divMenuBtnToDivWaitApprove_ChangeOwn":
                ViewState["cmdArg_typesys"] = "Approve_ChangeOwn";

                //ShowListTranfer_approve();
                break;

            case "_divMenuBtnToDivBudget":

                setActiveTab("budget");
                ViewState["mode"] = "budget";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();


                break;

            case "_divMenuBtnToDivAsset_system":

                setActiveTab("asset");
                ViewState["mode"] = "asset";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                break;

            case "_divMenuBtnToDivAsset_master":
                setActiveTab("master_class");
                MvMaster.SetActiveView(ViewAsset_Master);
                SelectMasterList_Class();
                break;

            case "_divMenuBtnToDivDataAsset":
                setActiveTab("import_asset");

                MvMaster.SetActiveView(ViewDataAsset);
                ViewState["status_searchasset"] = 0;
                SelectMasterList_AssetBuy();
                break;

            case "_divMenuBtnToDivDataGiveAsset":

                setActiveTab("data_asset");
                break;


            case "CmdAddBuy_Newlist":
                Control div_btn_buynew = (Control)_FormView.FindControl("div_btn_buynew");
                div_btn_buynew.Visible = true;
                setAddList_BuyNew();


                break;

            case "CmdAddBuy_Replacelist":
                Control div_btn_buyreplace = (Control)FvInsertBuyReplace.FindControl("div_btn_buyreplace");
                div_btn_buyreplace.Visible = true;
                setAddList_BuyReplace();
                break;

            case "BtnBack":

                if (ViewState["mode"].ToString() == "budget")
                {
                    setActiveTab("budget");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if (ViewState["mode"].ToString() == "dir_user_approve")
                {
                    setActiveTab("dir_user_approve");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                SETFOCUS.Focus();
                break;

            case "btndetailbudget":
                gvbudget.Visible = false;
                detailbudget.Visible = true;

                FvDetailUser_Budget.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Budget.DataBind();

                FvDetail_ShowBudget.ChangeMode(FormViewMode.Insert);
                FvDetail_ShowBudget.DataBind();


                break;

            case "BtnBack_viewbudget":
                MvMaster.SetActiveView(ViewBudget);
                gvbudget.Visible = true;
                detailbudget.Visible = false;
                SETFOCUS.Focus();
                break;

            case "btndetailasset": // รายละเอียด หน้า budget
                gvasset.Visible = false;
                detailasset.Visible = true;
                action_select_its_Asset(e.CommandArgument.ToString());

                break;

            case "BtnBack_viewasset":

                if (ViewState["mode"].ToString() == "asset")
                {
                    setActiveTab("asset");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if ((ViewState["mode"].ToString() == "asset_dir1")
                    ||
                    (ViewState["mode"].ToString() == "asset_dir2")
                    ||
                    (ViewState["mode"].ToString() == "asset_dir3")
                    ||
                    (ViewState["mode"].ToString() == "asset_dir4")
                    ||
                    (ViewState["mode"].ToString() == "attach_flie_asset")
                    ||
                    (ViewState["mode"].ToString() == "attach_flie_asset_show")
                    )
                {
                    setActiveTab(ViewState["mode"].ToString());
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                SETFOCUS.Focus();
                break;

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "CmdAddBuy_Cutlist":

                break;

            case "btndetailapprove":

                gvapprove.Visible = false;
                detailapprove.Visible = true;
                FvDetailUser_Approve.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Approve.DataBind();

                FvDetail_ShowApprove.ChangeMode(FormViewMode.Insert);
                FvDetail_ShowApprove.DataBind();

                Control div_showreasoncut = (Control)FvDetail_ShowApprove.FindControl("div_showreasoncut");

                if (ViewState["cmdArg_typesys"].ToString() == "Approve_CutDevices")
                {
                    div_showreasoncut.Visible = true;
                }
                else
                {
                    div_showreasoncut.Visible = false;
                }

                break;

            case "BtnBack_viewapprove":
                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;
                break;

            case "btnAddMaster":
                Insert_Master();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList_Class();
                break;

            case "CmdDel":
                int clidx = int.Parse(cmdArg);
                ViewState["clidx"] = clidx;
                Delete_Master_List();
                SelectMasterList_Class();
                break;

            case "CmdAdd_Import":
                Panel_ImportAdd.Visible = true;
                CmdAdd_Import.Visible = false;
                CmdAdd_SearchAsset.Visible = false;
                break;

            case "btnCancel_Import":
                Panel_ImportAdd.Visible = false;
                Panel_SearchASSET.Visible = false;
                CmdAdd_Import.Visible = true;
                CmdAdd_SearchAsset.Visible = true;
                ViewState["status_searchasset"] = 0;
                SelectMasterList_AssetBuy();
                break;

            case "btnImport":
                if (upload.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(upload.PostedFile.FileName);
                    string extension = Path.GetExtension(upload.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_asset_importasset"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        upload.SaveAs(filePath);
                        ImportFileBuyDevices(filePath, extension, "Yes", FileName, "asset");
                        //File.Delete(filePath);

                        //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        showMsAlert("Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                    Panel_ImportAdd.Visible = false;
                    CmdAdd_Import.Visible = true;
                    CmdAdd_SearchAsset.Visible = true;
                    SelectMasterList_AssetBuy();
                }
                break;

            case "CmdAdd_SearchAsset":
                CmdAdd_Import.Visible = false;
                CmdAdd_SearchAsset.Visible = false;
                Panel_SearchASSET.Visible = true;
                select_CostcenterMasterIO(ddlsearch_costcenterasset);
                select_YearMasterAsset(ddlsearch_yearasset);
                select_LocateMasterAsset(ddlsearch_locateasset);
                txtsearchasset.Text = "";
                break;


            case "btnsearch_importasset":
                ViewState["status_searchasset"] = 1;
                SelectMasterList_AssetBuy();
                break;

            case "CmdInsert":

                if (checkErrorInsert() == false)
                {
                    if (ViewState["mode"].ToString() == "E")
                    {
                        zSaveUpdate(_func_dmu.zStringToInt(ViewState["u0idx"].ToString()));
                    }
                    else
                    {
                        zSave(0);
                    }
                    SETFOCUS.Focus();
                }
                break;

            case "showBoxsearch":
                panel_search.Visible = true;
                Gvits_u0_document_list.Visible = false;
                btnshowBoxsearch.Visible = false;
                btnhiddenBoxsearch.Visible = true;
                Panel_list_purchase.Visible = false;
                setSearchList();

                break;
            case "hiddenBoxsearch":

                sethiddenBoxsearch();
                ShowDataIndex();
                break;
            case "manage_its": // list ข้อมูล / approve
                setActiveTab("approve");
                string[] argument = new string[10];
                argument = e.CommandArgument.ToString().Split('|');
                int u0idx = int.Parse(argument[0]);
                string nodidx = argument[1];
                int emp_idx_its = int.Parse(argument[2]);
                int pr_type_idx = int.Parse(argument[3]);
                int actor_idx = int.Parse(argument[4]);
                string documentCode = argument[5];
                string doc_status = argument[6];
                int flow_item = int.Parse(argument[7]);
                int place_idx_its = int.Parse(argument[8]);
                int rdept_idx_its = int.Parse(argument[9]);

                ViewState["u0idx"] = u0idx;
                ViewState["node_idx"] = nodidx;
                ViewState["emp_idx_its"] = emp_idx_its;
                ViewState["pr_type_idx"] = pr_type_idx;
                ViewState["actor_idx"] = actor_idx;
                ViewState["_DOCUMENTCODE"] = documentCode;
                ViewState["doccode"] = documentCode;
                ViewState["doc_status"] = doc_status;
                ViewState["place_idx_its"] = place_idx_its;

                if ((ViewState["mode"].ToString() == "dir_user_approve") ||
                    (ViewState["mode"].ToString() == "budget"))
                {
                    if ((ViewState["mode"].ToString() == "dir_user_approve"))
                    {
                        setActiveTab("dir_user_approve");
                    }
                    else
                    {
                        setActiveTab("budget");
                    }
                    FvInsertBuyNew.Visible = false;
                    FvUpdateBuyNew.Visible = true;
                    action_select_its(u0idx, flow_item);
                    Ms_purchase(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()), rdept_idx_its);

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFile);

                    }
                    catch
                    {

                    }
                }
                else if (ViewState["mode"].ToString() == "asset")
                {
                    setActiveTab("asset");
                    MvMaster.SetActiveView(ViewAsset);
                    gvasset.Visible = false;
                    detailasset.Visible = true;
                    action_select_its_Asset(e.CommandArgument.ToString());
                    SETFOCUS.Focus();
                }
                else if ((ViewState["mode"].ToString() == "asset_dir1")
                      ||
                      (ViewState["mode"].ToString() == "asset_dir2")
                      ||
                      (ViewState["mode"].ToString() == "asset_dir3")
                      ||
                      (ViewState["mode"].ToString() == "asset_dir4")
                    )
                {
                    setActiveTab(ViewState["mode"].ToString());
                    MvMaster.SetActiveView(ViewAsset);
                    gvasset.Visible = false;
                    detailasset.Visible = true;
                    action_select_its_Asset(e.CommandArgument.ToString());
                    SETFOCUS.Focus();
                }
                else if (ViewState["mode"].ToString() == "it")
                {

                    setActiveTab("it");
                    MvMaster.SetActiveView(ViewIT);
                    action_select_its_IT(e.CommandArgument.ToString());
                }
                else if (ViewState["mode"].ToString() == "it_update")
                {

                    setActiveTab("it_update");
                    MvMaster.SetActiveView(ViewIT);
                    action_select_its_IT(e.CommandArgument.ToString());
                }
                else if (ViewState["mode"].ToString() == "it-manager")
                {

                    setActiveTab("it-manager");
                    MvMaster.SetActiveView(ViewIT);
                    action_select_its_IT(e.CommandArgument.ToString());
                }
                else if (ViewState["mode"].ToString() == "it-dir")
                {
                    setActiveTab("it-dit");
                    MvMaster.SetActiveView(ViewIT);
                    action_select_its_IT(e.CommandArgument.ToString());
                }
                else if (ViewState["mode"].ToString() == "pur")
                {

                    setActiveTab("pur");
                    MvMaster.SetActiveView(ViewIT);
                    action_select_its_IT(e.CommandArgument.ToString());

                }
                else if (
                    (ViewState["mode"].ToString() == "attach_flie_asset")
                    ||
                    (ViewState["mode"].ToString() == "attach_flie_asset_show")
                    )
                {
                    setActiveTab(ViewState["mode"].ToString());
                    MvMaster.SetActiveView(ViewAsset);
                    gvasset.Visible = false;
                    detailasset.Visible = true;
                    action_select_its_Asset(e.CommandArgument.ToString());
                    SETFOCUS.Focus();

                }
                else
                {

                    ViewState["mode"] = "E";
                    FvInsertBuyNew.Visible = false;
                    FvUpdateBuyNew.Visible = true;
                    action_select_its(u0idx, flow_item);
                    Ms_purchase(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()), rdept_idx_its);

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFile);

                    }
                    catch
                    {

                    }
                    // File Upload

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        GridView gvFileUpdate = (GridView)FvUpdateBuyNew.FindControl("gvFileUpdate");
                        SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFileUpdate);

                    }
                    catch
                    {

                    }

                }

                break;

            case "btnApproveDirector":
                int ic = 0;
                _u0_idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
                hidden_m0_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_node");
                hidden_m0_actor = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_actor");
                if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 2)
                {
                    if (
                        (_func_dmu.zStringToInt(hidden_m0_node.Value) == 2) &&
                        (_func_dmu.zStringToInt(hidden_m0_actor.Value) == 2)
                        )
                    {
                        if (approve_director(_u0_idx, _func_dmu.zStringToInt(hidden_m0_node.Value), _func_dmu.zStringToInt(hidden_m0_actor.Value)) == true)
                        {
                            ic++;
                        }
                    }
                    else
                    {
                        ic++;
                    }
                }
                else if (//(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 3)
                         (_func_dmu.zStringToInt(hidden_m0_node.Value) == 3) &&
                         (_func_dmu.zStringToInt(hidden_m0_actor.Value) == 5)
                        )
                {
                    if (ViewState["mode_approve"].ToString() == "budget_3")
                    {
                        if (approve_budget(_u0_idx, _func_dmu.zStringToInt(hidden_m0_node.Value), _func_dmu.zStringToInt(hidden_m0_actor.Value)) == true)
                        {
                            ic++;
                        }
                    }
                    else
                    {
                        ic++;
                    }
                }
                else if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 14)
                {

                    if (zSaveUser_updateIO(_u0_idx) == true)
                    {
                        ic++;
                    }
                    else
                    {
                        ic++;
                    }
                }
                if (ic > 0)
                {
                    if (ViewState["mode"].ToString() == "budget")
                    {
                        setActiveTab("budget");
                        MvMaster.SetActiveView(ViewIndex);
                        ShowDataIndex();
                    }
                    else if (ViewState["mode"].ToString() == "dir_user_approve")
                    {
                        setActiveTab("dir_user_approve");
                        MvMaster.SetActiveView(ViewIndex);
                        sethiddenBoxsearch();
                        ShowDataIndex();
                    }
                    else
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }

                }
                SETFOCUS.Focus();
                break;

            case "reset_search":
                ddlcondition.SelectedValue = "0";
                txtstartdate.Text = String.Empty;
                txtenddate.Text = String.Empty;
                txtdocumentcode.Text = String.Empty;
                txtempcode.Text = String.Empty;
                txtfirstname_lastname.Text = String.Empty;
                ddlsearch_organization.SelectedValue = "0";
                ddlsearch_department.SelectedValue = "0";
                ddllocation_search.SelectedValue = "0";
                ddl_costcenter.SelectedValue = "0";
                ddlsearch_typepurchase.SelectedValue = "0";
                ddlsearch_typequipment.SelectedValue = "0";
                ddlsearch_status.SelectedValue = "0";


                break;

            case "searching":
                if (ViewState["mode"].ToString() == "asset_report")
                {
                    hidden_showasset.Value = "1";
                    if(ddlSearchReport.SelectedValue == "1")
                    {
                        getHtml_ReportAsset_io();
                    }
                    else
                    {
                        getHtml_ReportAsset();
                    }
                    
                }
                else
                {
                    hidden_showasset.Value = "0";
                    SearchDataIndex();
                }


                break;
            case "CmdInsert_asset":
                if (ViewState["mode"].ToString() == "attach_flie_asset")
                {
                    if (_func_dmu.zStringToInt(ViewState["u0idx"].ToString()) > 0)
                    {
                        TextBox txt_m0_node = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_node_idx");
                        TextBox txt_m0_actor = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_actor_idx");
                        zSaveUpdate_asset(_func_dmu.zStringToInt(ViewState["u0idx"].ToString()), _func_dmu.zStringToInt(txt_m0_node.Text), _func_dmu.zStringToInt(txt_m0_actor.Text));

                        MvMaster.SetActiveView(ViewIndex);
                        sethiddenBoxsearch();
                        ShowDataIndex();
                        SETFOCUS.Focus();
                    }
                }
                else
                {
                    if (checkErrorInsert_asset() == false)
                    {
                        if (
                            (
                            (ViewState["mode"].ToString() == "asset")
                            ||
                            (ViewState["mode"].ToString() == "asset_dir1")
                            ||
                            (ViewState["mode"].ToString() == "asset_dir2")
                            ||
                            (ViewState["mode"].ToString() == "asset_dir3")
                            ||
                            (ViewState["mode"].ToString() == "asset_dir4")
                            )
                            &&
                            (_func_dmu.zStringToInt(ViewState["u0idx"].ToString()) > 0)
                            )
                        {
                            TextBox txt_m0_node = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_node_idx");
                            TextBox txt_m0_actor = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_actor_idx");
                            zSaveUpdate_asset(_func_dmu.zStringToInt(ViewState["u0idx"].ToString()), _func_dmu.zStringToInt(txt_m0_node.Text), _func_dmu.zStringToInt(txt_m0_actor.Text));
                            if (ViewState["mode"].ToString() == "asset")
                            {
                                setActiveTab("asset");
                            }
                            else
                            {
                                setActiveTab(ViewState["mode"].ToString());
                            }

                            MvMaster.SetActiveView(ViewIndex);
                            sethiddenBoxsearch();
                            ShowDataIndex();
                            SETFOCUS.Focus();
                        }
                    }
                }
                break;
            case "_divMenubtnIT":
                setActiveTab("it");
                ViewState["mode"] = "it";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "_divMenubtnIT_update":
                setActiveTab("it_update");
                ViewState["mode"] = "it_update";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "_divMenubtnIT_manager":
                setActiveTab("it-manager");
                ViewState["mode"] = "it-manager";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "_divMenubtnIT_director":
                setActiveTab("it-dir");
                ViewState["mode"] = "it-dir";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "CmdInsert_it":

                if (checkErrorInsert_it() == false)
                {
                    if (
                       ((ViewState["mode"].ToString() == "it") ||
                        (ViewState["mode"].ToString() == "it_update") ||
                         (ViewState["mode"].ToString() == "it-manager") ||
                         (ViewState["mode"].ToString() == "it-dir") ||
                         (ViewState["mode"].ToString() == "pur")
                         )
                        &&
                        (_func_dmu.zStringToInt(ViewState["u0idx"].ToString()) > 0)
                        )
                    {
                        if (ViewState["mode"].ToString() == "it_update")
                        {
                            zSaveUpdate_it_update(_func_dmu.zStringToInt(ViewState["u0idx"].ToString()));
                        }
                        else
                        {
                            zSaveUpdate_it(_func_dmu.zStringToInt(ViewState["u0idx"].ToString()));
                        }

                        SETFOCUS.Focus();
                    }
                }
                break;
            case "BtnBack_viewit":
                if (ViewState["mode"].ToString() == "it")
                {
                    setActiveTab("it");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if (ViewState["mode"].ToString() == "it_update")
                {
                    setActiveTab("it_update");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if (ViewState["mode"].ToString() == "it-manager")
                {
                    setActiveTab("it-manager");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if (ViewState["mode"].ToString() == "it-dir")
                {
                    setActiveTab("it-dir");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                else if (ViewState["mode"].ToString() == "pur")
                {
                    setActiveTab("pur");
                    MvMaster.SetActiveView(ViewIndex);
                    sethiddenBoxsearch();
                    ShowDataIndex();
                }
                SETFOCUS.Focus();
                break;
            case "_divMenuLiToasset_dir1":

                setActiveTab("asset_dir1");
                ViewState["mode"] = "asset_dir1";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "btnM_SeniorManager":

                setActiveTab("asset_dir2");
                ViewState["mode"] = "asset_dir2";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "btnM_Dir_Asset":

                setActiveTab("asset_dir3");
                ViewState["mode"] = "asset_dir3";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "btnM_CFO":
                //
                setActiveTab("asset_dir4");
                ViewState["mode"] = "asset_dir4";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "confirm_approve":
                confirm_approve();
                sethiddenBoxsearch();
                ShowDataIndex();
                break;
            case "confirm_no_approve":
                confirm_no_approve();
                sethiddenBoxsearch();
                ShowDataIndex();
                break;
            case "_divMenubtnIT_summary":

                setActiveTab("it_summary");
                ViewState["mode"] = "it_summary";
                MvMaster.SetActiveView(View_docket);
                Fv_docket.ChangeMode(FormViewMode.Insert);
                setObject_it_docket();
                _func_dmu.zSetDdlMonth(_ddlMonthSearch);
                _ddlMonthSearch.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
                select_yeardocket(_ddlYearSearch);

                action_select_its_docket("");

                break;
            case "manage_its_docket":

                action_select_its_docket_detail(e.CommandArgument.ToString());

                break;
            case "searching_docket":

                action_select_its_docket("");

                break;
            case "confirm_approve_docket":
                setObject_it_docket();
                confirm_approve_docket(_check_approve.Checked);
                action_select_its_docket("");

                break;
            case "_divMenubtnMD_it":
                ViewState["mode"] = "md_approve";
                settab_md();

                break;
            case "manage_its_docket_md":

                MvMaster.SetActiveView(View_docket_md_detial);
                Fv_docket_md_detail.ChangeMode(FormViewMode.Edit);
                ViewState["flow_item"] = "10";
                action_select_its_docket_md_detail(e.CommandArgument.ToString());

                break;
            case "Cmdmd_app":
                zSaveUpdate_md(_func_dmu.zStringToInt(e.CommandArgument.ToString()));
                setActiveTab("md_summary");
                MvMaster.SetActiveView(View_docket_md);
                Fv_docket_md.ChangeMode(FormViewMode.Insert);
                setObject_it_docket_md();
                action_select_its_docket_md("");
                break;

            case "confirm_approve_md_docket":
                confirm_approve_md_docket();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                // setObject_it_docket_md();
                //action_select_its_docket_md("");
                break;
            case "confirm_no_approve_md_docket":
                confirm_no_approve_md_docket();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "reset_search_md":
                setObject_it_docket_md();
                _func_dmu.zSetDdlMonth(_ddlMonthSearch);
                _ddlMonthSearch.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
                select_yeardocket(_ddlYearSearch, 2);
                _txtdocumentcode.Text = String.Empty;
                //_ddlStatusapproveSearch.SelectedValue = "1";
                setStatusApprove();
                action_select_its_docket_md("");
                break;
            case "searching_md":
                setStatusApprove();
                action_select_its_docket_md("");

                break;
            case "_divMenubtnIT_deliver":

                setActiveTab("it_deliver");
                ViewState["mode"] = "it_deliver";
                MvMaster.SetActiveView(View_docket);
                Fv_docket.ChangeMode(FormViewMode.Insert);
                setObject_it_docket();

                _func_dmu.zSetDdlMonth(_ddlMonthSearch);
                _ddlMonthSearch.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
                select_yeardocket(_ddlYearSearch, 1);
                action_select_its_docket("");
                SETFOCUS.Focus();
                break;
            case "manage_its_docket_delivery": // รายละเอียด หน้า delivery
                MvMaster.SetActiveView(View_Delivery_Detail);
                action_select_its_Delivery(e.CommandArgument.ToString());

                break;
            case "CmdInsert_Delivery":

                if (zSaveUpdate_Delivery(0) == true)
                {
                    setActiveTab("it_deliver");
                    ViewState["mode"] = "it_deliver";
                    MvMaster.SetActiveView(View_docket);
                    Fv_docket.ChangeMode(FormViewMode.Insert);
                    setObject_it_docket();

                    action_select_its_docket("");
                    SETFOCUS.Focus();
                }

                break;

            case "_divMenuBtnToDivBudget_master":
                setActiveTab("budget_io");
                ViewState["mode"] = "budget";
                ViewState["status_searchio"] = 0;
                MvMaster.SetActiveView(ViewBudget_Master);
                //SelectMasterList_IO();
                ViewState["status_searchio"] = 1;
                SelectMasterList_IO();

                break;
            case "manage_its_docket_md_print":
                setActiveTab("it_print");
                ViewState["mode"] = "it_print";
                _U0IDX = int.Parse(cmdArg);
                MvMaster.SetActiveView(View_docket_print_detial);
                Fv_docket_print_detial.ChangeMode(FormViewMode.Edit);
                showdata_print_detail(_U0IDX);
                break;
            case "_divMenubtnIT_print":

                ViewState["mode"] = "it_print";
                settab_md();
                setActiveTab("it_print");
                break;
            case "manage_its_docket_md_print_detail":
                //  _U0IDX = int.Parse(cmdArg);
                string[] _argument = new string[2];
                argument = e.CommandArgument.ToString().Split('|');
                Session["_SESSION_U0DOCIDX"] = argument[0];
                Session["_SESSION_ORGIDX"] = argument[1];
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('it-asset-print', '', '');", true);

                break;
            case "Cmd_ExportToPdf":
                ExportToPdf();

                break;
            case "btnSearch_ddlspecidx":
                showModal_Spec();

                break;
            case "btn_select":
                select_data(int.Parse(cmdArg));

                break;
            case "btnEditPrice":
                showModal_updateprice(cmdArg);
                break;
            case "btnsaveupdateprice":
                updateprice(cmdArg);
                break;
            case "btnDevice":
                getDevice(cmdArg);
                break;
            case "_divMenubtnpurchase":
                setActiveTab("pur");
                ViewState["mode"] = "pur";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();
                SETFOCUS.Focus();
                break;
            case "btn_spec_search":
                Search_spec();
                break;
            case "btn_okspec":
                setspec();
                break;
            case "bnDeletefile":

                try
                {
                    string Pathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                    string filesLoc = Server.MapPath(Pathfile + "/" + cmdArg);
                    File.Delete(filesLoc);
                }
                catch { }
                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    GridView gvFileUpdate = (GridView)FvUpdateBuyNew.FindControl("gvFileUpdate");
                    SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFileUpdate);

                }
                catch
                {

                }
                break;
            case "btnUploadFileAsset":
                zShowUploadFileAsset(cmdArg);

                break;
            case "btn_uploadfile_Asset":
                if (ViewState["mode"].ToString() == "attach_flie_asset")
                {
                    zAddUploadFileAsset1(ViewState["v_uploadfile_asset"].ToString());
                }
                else
                {
                    zAddUploadFileAsset(ViewState["v_uploadfile_asset"].ToString());
                }
                break;
            case "bnDeletefile_AssetFile":
                zDeleteUploadFileAsset(cmdArg);

                break;

            case "btnAttachFlieAsset":
                setActiveTab("attach_flie_asset");
                ViewState["mode"] = "attach_flie_asset";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "btnAttachFlieAsset_show":
                setActiveTab("attach_flie_asset_show");
                ViewState["mode"] = "attach_flie_asset_show";
                MvMaster.SetActiveView(ViewIndex);
                sethiddenBoxsearch();
                ShowDataIndex();

                break;
            case "btnAsset_Report":
                setActiveTab("asset_report");
                ViewState["mode"] = "asset_report";
                MvMaster.SetActiveView(ViewIndex);
                panel_search.Visible = true;
                Gvits_u0_document_list.Visible = false;
                btnshowBoxsearch.Visible = false;
                btnhiddenBoxsearch.Visible = false;
                Panel_list_purchase.Visible = false;
                setSearchList();
                div_asset.Visible = true;
                pnlsearch_asset1.Visible = true;
                setddlSearchReport(ddlSearchReport.SelectedValue);
                ShowDataIndex();
               // pnl_asset_report.Visible = true;
                break;
            case "btnUploadFileAsset1":
                zShowUploadFileAsset(cmdArg);

                break;
            //case "btn_uploadfile_Asset1":
            //    zAddUploadFileAsset1(ViewState["v_uploadfile_asset"].ToString());

            //    break;
            case "bnDeletefile_AssetFile1":
                zDeleteUploadFileAsset1(cmdArg);

                break;
        }
    }

    private void settab_md()
    {
        setActiveTab("md_summary");

        MvMaster.SetActiveView(View_docket_md);
        Fv_docket_md.ChangeMode(FormViewMode.Insert);
        setObject_it_docket_md();

        _func_dmu.zSetDdlMonth(_ddlMonthSearch);
        _ddlMonthSearch.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
        select_yeardocket(_ddlYearSearch, 2);
        setStatusApprove();
        action_select_its_docket_md("");
        setdefaultNotification();
    }

    private void sethiddenBoxsearch()
    {
        panel_search.Visible = false;
        btnhiddenBoxsearch.Visible = false;
        Gvits_u0_document_list.Visible = true;
        btnshowBoxsearch.Visible = true;
        Panel_list_purchase.Visible = false;
        setSearchList();
    }

    #endregion

    #region strat teppanop
    // strat teppanop
    protected void select_systemtype(DropDownList ddlName, int sysidx)
    {
        // tb its_m0_typereference

        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();
        system.SysIDX = sysidx;
        _dtitseet.boxprice_reference[0] = system;

        _dtitseet = callServicePostITAsset(_urlSelectSystem_TypePrice, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "type_name", "typidx");
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทอุปกรณ์ขอซื้อ...", "0"));

    }

    protected data_purchase callServicePurchase(string _cmdUrl, data_purchase _data_purchase)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_purchase);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_purchase = (data_purchase)_funcTool.convertJsonToObject(typeof(data_purchase), _localJson);

        return _data_purchase;
    }


    protected void select_devices(DropDownList ddlName, int sysidx, int typidx)
    {

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_position = new its_lookup();

        obj_position.sysidx = sysidx;
        obj_position.typidx = typidx;
        obj_position.operation_status_id = "group_devices";

        _dataitasset.its_lookup_action[0] = obj_position;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);
        ddlName.Items.Clear();
        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกกลุ่มอุปกรณ์", "0"));


        /*
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();

        if (typeidx == 1)
        {
            //tb 
            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectTypeDevices, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "m0_tdidx");
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกอุปกรณ์...", "0"));
        }
        else if (typeidx == 2)
        {
            // tb 

            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectSoftware, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "software_name_idx");
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกซอฟแวร์...", "0"));
        }
        else if (typeidx == 4)
        {
            // tb 
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;

            // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlSelectHROffice, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสำนักงาน...", "0"));
        }
        else if (typeidx == 11 || typeidx == 12)
        {
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectENMachine, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกเครื่องจักร...", "0"));
        }
        else
        {
            ddlName.Items.Clear();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกกลุ่มอุปกรณ์ขอซื้อ...", "0"));
        }
        */
    }

    protected void select_ddlsearch_purchase(DropDownList ddlName, string status = "")
    {


        // tb purchase_m0_purchasetype

        _FormView = getFv(ViewState["mode"].ToString());
        DropDownList ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
        DropDownList ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        if (ddlsystem != null)
        {
            ddlName.Items.Clear();

            data_itasset dataitasset = new data_itasset();
            dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_m0type = new its_lookup();

            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

            obj_m0type.idx = _func_dmu.zStringToInt(ddlsystem.SelectedValue);
            obj_m0type.typidx = _func_dmu.zStringToInt(ddlSearchTypeDevices.SelectedValue);
            obj_m0type.operation_status_id = "m0_type";
            dataitasset.its_lookup_action[0] = obj_m0type;
            dataitasset = callServicePostITAsset(_urlGetits_lookup, dataitasset);

            ddlName.DataSource = dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
        }
        else if (status == "index")
        {
            ddlName.Items.Clear();

            data_itasset dataitasset = new data_itasset();
            dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_m0type = new its_lookup();

            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Add(new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทการขอซื้อ", "0"));

            obj_m0type.idx = 3;
            obj_m0type.typidx = 3;
            obj_m0type.operation_status_id = "m0_type";
            dataitasset.its_lookup_action[0] = obj_m0type;
            dataitasset = callServicePostITAsset(_urlGetits_lookup, dataitasset);

            ddlName.DataSource = dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
        }

    }

    protected void select_position(DropDownList ddlName, int _type)
    {

        ddlName.Items.Clear();

        if (_type == 0)
        {
            data_itasset _dataitasset = new data_itasset();
            _dataitasset.its_lookup_action = new its_lookup[1];
            its_lookup obj_position = new its_lookup();

            obj_position.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
            obj_position.operation_status_id = "position";

            _dataitasset.its_lookup_action[0] = obj_position;
            // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

            _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

            ddlName.DataSource = _dataitasset.its_lookup_action;
            ddlName.DataTextField = "zname";
            ddlName.DataValueField = "idx";
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกตำแหน่ง", "0"));

        }
        else if (_type == 1)
        {
            ddlName.DataBind();
            ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกตำแหน่ง", "0"));
        }


    }

    protected void select_currency(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        obj_lookup.operation_status_id = "currency";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสกุลเงิน", "0"));

    }

    protected void select_unittype(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        obj_lookup.operation_status_id = "unittype";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกหน่วย", "0"));

    }

    protected void select_yeardocket(DropDownList ddlName, int _item = 0)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        if (_item == 0)
        {
            obj_lookup.operation_status_id = "year_docket";
        }
        else if (_item == 1)
        {
            obj_lookup.operation_status_id = "year_docket_delivery";
        }
        else if (_item == 2)
        {
            obj_lookup.operation_status_id = "year_docket_md";
        }

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zyear";
        ddlName.DataValueField = "zyear";
        ddlName.DataBind();
        //ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกปี", "0"));

    }

    protected void select_costcenter(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        // obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "costcenter";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกรหัสงบประมาณ", "0"));

    }

    protected void select_place(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "place";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสถานที่", "0"));

    }
    protected void Select_master_spec(int sysidx, int typidx, int tdidx, int posidx)
    {
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
        _txtlist_menu = (TextBox)_FormView.FindControl("txtlist_buynew");
        _txt_m1_ref_idx = (TextBox)_FormView.FindControl("txt_m1_ref_idx");
        _pnl_other = (Panel)_FormView.FindControl("pnl_other");
        _check_special = (CheckBox)_FormView.FindControl("check_special");
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_lookup = new its_u_document();

        obj_lookup.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_lookup.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_lookup.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        obj_lookup.posidx = posidx;
        obj_lookup.sysidx = sysidx;
        obj_lookup.typidx = typidx;
        obj_lookup.tdidx = tdidx;
        obj_lookup.operation_status_id = "its_m1_reference_sel";

        _dataitasset.its_u_document_action[0] = obj_lookup;
        ///litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_u_document, _dataitasset);
        _ddlspecidx.Text = "";
        _txtlist_menu.Text = "";
        _txt_m1_ref_idx.Text = "0";

        _check_special.Checked = false;
        if (((sysidx == 3) && (typidx == 10)) || (sysidx == 37))
        {
            _ddlspecidx.Enabled = true;
            _txtlist_menu.Enabled = true;
            _check_special.Visible = false;
        }
        else
        {
            _ddlspecidx.Enabled = false;
            _txtlist_menu.Enabled = false;
            _check_special.Visible = true;
        }
        if (sysidx == 37)
        {
            _pnl_other.Visible = false;
        }
        else
        {
            _pnl_other.Visible = true;
        }

        if (_dataitasset.its_u_document_action != null)
        {
            foreach (var item in _dataitasset.its_u_document_action)
            {
                _ddlspecidx.Text = item.leveldevice;
                _txtlist_menu.Text = item.detail;
                _txt_m1_ref_idx.Text = item.m1_ref_idx.ToString();
            }
        }


    }

    protected void Select_master_spec_detail(TextBox txtName, TextBox txtprice, int sysidx, int typidx, string leveldevice, int idx)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.sysidx = sysidx;
        obj_lookup.typidx = typidx;
        obj_lookup.leveldevice = leveldevice;
        obj_lookup.idx = idx;
        obj_lookup.operation_status_id = "spec_detail";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text  = _funcTool.convertObjectToJson(_dataitasset);
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataitasset));
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);
        if (_dataitasset.its_lookup_action != null)
        {
            string str = _dataitasset.its_lookup_action[0].zname;
            txtName.Text = str;
            str = _dataitasset.its_lookup_action[0].price.ToString();
            //  txtprice.Text = str;
        }
        else
        {
            txtName.Text = "";
            txtprice.Text = "0";
        }

    }

    public string getStrformate(string str, int iformat)
    {
        str = _func_dmu.zFormatfloat(str, iformat);
        return str;
    }
    private void setObject()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        _ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
        _ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
        _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");
        _ddlposition = (DropDownList)_FormView.FindControl("ddlposition");
        _ddlcurrency = (DropDownList)_FormView.FindControl("ddlcurrency");
        _ddlunit = (DropDownList)_FormView.FindControl("ddlunit");
        _ddlcostcenter = (DropDownList)_FormView.FindControl("ddlcostcenter");
        _ddlplace = (DropDownList)_FormView.FindControl("ddlplace");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");

        _txtremark = (TextBox)_FormView.FindControl("txtremark");
        _txtref_itnum = (TextBox)_FormView.FindControl("txtref_itnum");
        _txtionum = (TextBox)_FormView.FindControl("txtionum");

        _pnl_ionum = (Panel)_FormView.FindControl("pnl_ionum");
        _pnl_ref_itnum = (Panel)_FormView.FindControl("pnl_ref_itnum");

        _txtbudget_set = (TextBox)_FormView.FindControl("txtbudget_set");
        _txtlist_menu = (TextBox)_FormView.FindControl("txtlist_buynew");
        _txtqty = (TextBox)_FormView.FindControl("txtqty_buynew");
        _txtprice = (TextBox)_FormView.FindControl("txtprice_buynew");
        _txt_m1_ref_idx = (TextBox)_FormView.FindControl("txt_m1_ref_idx");

        _ddl_target = (DropDownList)_FormView.FindControl("ddl_target");

        _check_special = (CheckBox)_FormView.FindControl("check_special");

        _GvReportAdd = (GridView)_FormView.FindControl("GvReportAdd");

    }
    private Boolean zSave(int id)
    {

        int idx;
        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "TRQ";
        setObject();
        data_itasset dataitasset = new data_itasset();
        its_u_document obj_u_document = new its_u_document();
        dataitasset.its_u_document_action = new its_u_document[1];
        obj_u_document.u0idx = id;
        obj_u_document.CEmpIDX = emp_idx;
        obj_u_document.UEmpIDX = emp_idx;
        obj_u_document.operation_status_id = "U0";

        obj_u_document.org_idx_its = _func_dmu.zStringToInt(ViewState["Org_idx"].ToString());
        obj_u_document.rdept_idx_its = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_u_document.rpos_idx_its = _func_dmu.zStringToInt(ViewState["Pos_idx"].ToString());
        obj_u_document.rsec_idx_its = _func_dmu.zStringToInt(ViewState["Sec_idx"].ToString());

        //detail
        obj_u_document.emp_idx_its = emp_idx;
        obj_u_document.remark = _txtremark.Text;
        obj_u_document.place_idx = _func_dmu.zStringToInt(_ddlplace.SelectedValue);
        obj_u_document.sysidx = _func_dmu.zStringToInt(_ddlsystem.SelectedValue);


        //node
        obj_u_document.approve_status = 0;
        obj_u_document.its_status = 1;
        obj_u_document.flow_item = 2;
        obj_u_document.node_idx = 1;
        obj_u_document.actor_idx = 1;
        obj_u_document.doc_status = 1;

        dataitasset.its_u_document_action[0] = obj_u_document;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
        int iFile = 0;
        if (uploadfile_memo.HasFile)
        {
            iFile++;
        }
        if (uploadfile_organization.HasFile)
        {
            iFile++;
        }
        if (uploadfile_cutoff.HasFile)
        {
            iFile++;
        }
        if (uploadfile_other.HasFile)
        {
            iFile++;
        }
        iFile++;
        if (iFile == 0)
        {
            //showMsAlert("กรุณาแนบไฟล์เอกสาร.!");
        }

        if (iFile > 0)
        {



            dataitasset = callServicePostITAsset(_urlSetInsits_u_document, dataitasset);
            idx = _func_dmu.zStringToInt(dataitasset.ReturnCode);
            string _returndocumentcode = "";
            if (idx > 0)
            {
                data_itasset dtitseet1 = new data_itasset();
                dtitseet1.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                select_its.idx = idx;
                select_its.operation_status_id = "list_u0";

                dtitseet1.its_u_document_action[0] = select_its;
                dtitseet1 = callServicePostITAsset(_urlGetits_u_document, dtitseet1);
                _returndocumentcode = "";
                if (dtitseet1.its_u_document_action != null)
                {
                    foreach (var item in dtitseet1.its_u_document_action)
                    {
                        _returndocumentcode = item.doccode;
                    }
                }
                if (_returndocumentcode != "")
                {
                    string sfile_other = "", sfile_memo = "", sfile_organization = "", sfile_cutoff = "";

                    string getPathfile1 = ConfigurationManager.AppSettings["pathfile_itasset"];
                    string document_code1 = _returndocumentcode;
                    string filePath_upload1 = Server.MapPath(getPathfile1 + document_code1);

                    if (!Directory.Exists(filePath_upload1))
                    {
                        Directory.CreateDirectory(filePath_upload1);
                    }


                    if (uploadfile_memo.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_code = _returndocumentcode;
                        string fileName_upload = "[เอกสาร memo]" + document_code;
                        string filePath_upload = Server.MapPath(getPathfile + document_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }

                        int ic = 1;
                        foreach (HttpPostedFile uploadedFile in uploadfile_memo.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_memo = fileName_upload + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + sfile_memo);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }


                    }

                    if (uploadfile_organization.HasFile)
                    {

                        string getPathfileOrg = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_codeorg = _returndocumentcode;
                        string fileName_uploadorg = "[เอกสาร organization]" + document_codeorg;
                        string filePath_uploadorg = Server.MapPath(getPathfileOrg + document_codeorg);

                        if (!Directory.Exists(filePath_uploadorg))
                        {
                            Directory.CreateDirectory(filePath_uploadorg);
                        }

                        int ic = 1;
                        foreach (HttpPostedFile uploadedFile in uploadfile_organization.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_organization = fileName_uploadorg + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfileOrg + document_codeorg) + "\\" + sfile_organization);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }

                    }

                    if (uploadfile_cutoff.HasFile)
                    {

                        string getPathfilecutoff = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_codecutoff = _returndocumentcode;
                        string fileName_uploadcutoff = "[เอกสารการตัดเสีย]" + document_codecutoff;
                        string filePath_uploadcutoff = Server.MapPath(getPathfilecutoff + document_codecutoff);

                        if (!Directory.Exists(filePath_uploadcutoff))
                        {
                            Directory.CreateDirectory(filePath_uploadcutoff);
                        }

                        int ic = 1;
                        foreach (HttpPostedFile uploadedFile in uploadfile_cutoff.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_cutoff = fileName_uploadcutoff + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfilecutoff + document_codecutoff) + "\\" + sfile_cutoff);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }


                    }

                    if (uploadfile_other.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_code = _returndocumentcode;
                        string fileName_upload = "[เอกสาร อื่นๆ]" + document_code;
                        string filePath_upload = Server.MapPath(getPathfile + document_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        int ic = 1;
                        foreach (HttpPostedFile uploadedFile in uploadfile_other.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_other = fileName_upload + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + sfile_other);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }

                    }

                }

            }

            if (id > 0)
            {
                idx = id;

            }
            if (idx > 0)
            {
                zSaveDetail(idx);
                settemplate_asset_create(idx, 2, 0);

                //showMsAlert("บันทึกข้อมูลเสร็จแล้ว.");

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }

            _Boolean = true;
        }
        else
        {
            _Boolean = false;
        }
        return _Boolean;
    }

    private void zSaveDetail(int _id)
    {
        if (_id <= 0)
        {
            return;
        }

        int ic = 0, icount = 0, irow = 0, u0_idx = 0;
        int itemObj = 0;

        u0_idx = _id;
        data_itasset dataitasset = new data_itasset();
        //******************  start its_u1_document *********************//
        itemObj = 0;
        setObject();
        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
        its_u_document[] objcourse1 = new its_u_document[dsU1.Tables["dsits_u1_document"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsits_u1_document"].Rows)
        {
            objcourse1[itemObj] = new its_u_document();
            objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(item["u1idx"].ToString());
            objcourse1[itemObj].u0idx = u0_idx;
            objcourse1[itemObj].sysidx = _func_dmu.zStringToInt(_ddlsystem.SelectedValue);
            objcourse1[itemObj].typidx = _func_dmu.zStringToInt(item["typidx"].ToString());
            objcourse1[itemObj].pr_type_idx = _func_dmu.zStringToInt(item["pr_type_idx"].ToString());
            objcourse1[itemObj].tdidx = _func_dmu.zStringToInt(item["tdidx"].ToString());
            objcourse1[itemObj].ionum = item["ionum"].ToString();
            objcourse1[itemObj].ref_itnum = item["ref_itnum"].ToString();
            objcourse1[itemObj].rpos_idx = _func_dmu.zStringToInt(item["rpos_idx"].ToString());
            objcourse1[itemObj].list_menu = item["list_menu"].ToString();
            objcourse1[itemObj].price = _func_dmu.zStringToDecimal(item["price"].ToString());
            objcourse1[itemObj].currency_idx = _func_dmu.zStringToInt(item["currency_idx"].ToString());
            objcourse1[itemObj].qty = _func_dmu.zStringToInt(item["qty"].ToString());
            objcourse1[itemObj].unidx = _func_dmu.zStringToInt(item["unidx"].ToString());
            objcourse1[itemObj].costidx = _func_dmu.zStringToInt(item["costidx"].ToString());
            objcourse1[itemObj].budget_set = _func_dmu.zStringToDecimal(item["budget_set"].ToString());
            objcourse1[itemObj].place_idx = _func_dmu.zStringToInt(_ddlplace.SelectedValue);//_func_dmu.zStringToInt(item["place_idx"].ToString());
            objcourse1[itemObj].acquls_val = _func_dmu.zStringToInt(item["acquls_val"].ToString());
            objcourse1[itemObj].book_val = _func_dmu.zStringToInt(item["book_val"].ToString());
            objcourse1[itemObj].action = item["action"].ToString();
            objcourse1[itemObj].spec_idx = _func_dmu.zStringToInt(item["spec_idx"].ToString());
            objcourse1[itemObj].leveldevice = item["leveldevice"].ToString();
            objcourse1[itemObj].total_amount = _func_dmu.zStringToDecimal(item["total_amount"].ToString());
            objcourse1[itemObj].posidx = _func_dmu.zStringToInt(item["posidx"].ToString());
            objcourse1[itemObj].m1_ref_idx = _func_dmu.zStringToInt(item["m1_ref_idx"].ToString());
            objcourse1[itemObj].special_flag = _func_dmu.zStringToInt(item["special_flag"].ToString());
            objcourse1[itemObj].u1_status = 1;
            objcourse1[itemObj].CEmpIDX = emp_idx;
            objcourse1[itemObj].UEmpIDX = emp_idx;
            objcourse1[itemObj].operation_status_id = "U1";
            itemObj++;

        }

        dataitasset.its_u_document_action = objcourse1;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
        if (ViewState["mode"].ToString() == "E")
        {
            callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
        }
        else
        {
            callServicePostITAsset(_urlSetInsits_u_document, dataitasset);
        }
        //******************  end its_u1_document *********************//


    }

    protected void ShowDataIndex()
    {
        // view View_its_u0_document
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_document = new its_u_document();
        Gvits_u0_document_list.Columns[0].Visible = false;
        txtremark_approve_all.Text = "";
        check_approve_all.Checked = false;
        if (ViewState["mode"].ToString() == "it-manager")
        {
            Gvits_u0_document_list.Columns[0].Visible = true;
        }
        else if (ViewState["mode"].ToString() == "it-dir")
        {
            Gvits_u0_document_list.Columns[0].Visible = true;

        }
        //else if (ViewState["mode"].ToString() == "asset_dir3")
        //{
        //    Gvits_u0_document_list.Columns[0].Visible = true;

        //}
        //else if (ViewState["mode"].ToString() == "asset_dir4")
        //{
        //    Gvits_u0_document_list.Columns[0].Visible = true;

        //}
        select_document.zstatus = zModeStatus();
        select_document.rdept_idx_admin = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());

         if (_func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()) != 20)
        {
            select_document.rdept_idx_its = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        }
        else if (ViewState["mode"].ToString() == "dir_user_approve")
        {
            select_document.rdept_idx_its = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            // select_document.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        }
        // litDebug.Text = ViewState["sysidx_admin"].ToString();
        select_document.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        select_document.locidx = _func_dmu.zStringToInt(ViewState["locidx"].ToString());
        select_document.emp_idx = emp_idx;
        select_document.operation_status_id = "list_index";
        _dtitseet.its_u_document_action[0] = select_document;
        // litDebug.Text = ViewState["locidx"].ToString();
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

        ViewState["its_u0_document_list"] = _dtitseet.its_u_document_action;

        _func_dmu.zSetGridData(Gvits_u0_document_list, ViewState["its_u0_document_list"]);

        Panel_approve.Visible = false;
        check_approve_all.Checked = false;
        check_approve_all.Visible = false;
        btnapproveall.Visible = false;
        btnnotapprove.Visible = false;
        if (_dtitseet.its_u_document_action == null)
        {

        }
        else
        {
            if ((ViewState["mode"].ToString() == "it-manager") ||
                (ViewState["mode"].ToString() == "it-dir") ||
                (ViewState["mode"].ToString() == "asset_dir2") ||
                (ViewState["mode"].ToString() == "asset_dir3") ||
                (ViewState["mode"].ToString() == "asset_dir4")
                )
            {
                _dtitseet.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                //select_its.emp_idx = emp_idx;
                select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());

                if (ViewState["mode"].ToString() == "asset_dir2")
                {
                    select_its.flow_item = 50;
                    select_its.place_idx = 0;

                }
                else if (ViewState["mode"].ToString() == "asset_dir3")
                {
                    select_its.flow_item = 51;
                    select_its.place_idx = 0;

                }
                else if (ViewState["mode"].ToString() == "asset_dir4")
                {
                    select_its.flow_item = 52;
                    select_its.place_idx = 0;

                }
                else if (ViewState["mode"].ToString() == "it-manager")
                {
                    select_its.flow_item = 7;
                }
                else
                {
                    select_its.flow_item = 8;
                }
                select_its.operation_status_id = "permission_admin_select";
                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                if (_dtitseet.its_u_document_action != null)
                {

                    if (_dtitseet.its_u_document_action.Count() > 0)
                    {
                        Panel_approve.Visible = true;
                        check_approve_all.Visible = true;
                        btnapproveall.Visible = true;
                        btnnotapprove.Visible = true;
                        Gvits_u0_document_list.Columns[0].Visible = true;
                    }
                }

                if (_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                {
                    Panel_approve.Visible = true;
                    check_approve_all.Visible = true;
                    btnapproveall.Visible = true;
                    btnnotapprove.Visible = true;
                    Gvits_u0_document_list.Columns[0].Visible = true;
                }

            }
            if (ViewState["mode"].ToString() == "it-dir")
            {
                if (_func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString()) >= 10)
                {
                    Panel_approve.Visible = true;
                    check_approve_all.Visible = true;
                    btnapproveall.Visible = true;
                    btnnotapprove.Visible = true;
                }
            }
        }
        setdefaultNotification();


    }

    private string zModeStatus()
    {
        string zstatus = "";

        if (ViewState["mode"].ToString() == "dir_user_approve")
        {
            zstatus = "dir_user_approve";
        }
        else if (ViewState["mode"].ToString() == "budget")
        {
            zstatus = "budget";
        }
        else if (ViewState["mode"].ToString() == "asset")
        {
            zstatus = "asset";
        }
        else if (ViewState["mode"].ToString() == "asset_dir1")
        {
            zstatus = "asset_dir1";
        }
        else if (ViewState["mode"].ToString() == "asset_dir2")
        {
            zstatus = "asset_dir2";
        }
        else if (ViewState["mode"].ToString() == "asset_dir3")
        {
            zstatus = "asset_dir3";
        }
        else if (ViewState["mode"].ToString() == "asset_dir4")
        {
            zstatus = "asset_dir4";
        }
        else if (ViewState["mode"].ToString() == "it")
        {
            zstatus = "it";
        }
        else if (ViewState["mode"].ToString() == "it_update")
        {
            zstatus = "it_update";
        }
        else if (ViewState["mode"].ToString() == "it-manager")
        {
            zstatus = "it_manager";
        }
        else if (ViewState["mode"].ToString() == "it-dir")
        {
            zstatus = "it_dir";

        }
        else if (ViewState["mode"].ToString() == "pur")
        {
            zstatus = "pur";

        }
        else if (ViewState["mode"].ToString() == "attach_flie_asset")
        {
            zstatus = "attach_flie_asset";

        }
        else if (ViewState["mode"].ToString() == "attach_flie_asset_show")
        {
            zstatus = "attach_flie_asset_show";

        }
        else
        {
            zstatus = "preview";
        }


        return zstatus;
    }

    protected void checkindexchange(object sender, EventArgs e)
    {
        var checkbox = (CheckBox)sender;

        switch (checkbox.ID)
        {
            case "check_approve_all":

                if (check_approve_all.Checked == true)
                {

                    //ShowDataIndex();
                    if ((ViewState["mode"].ToString() == "it-manager") ||
                           (ViewState["mode"].ToString() == "it-dir") ||
                           (ViewState["mode"].ToString() == "asset_dir3") ||
                           (ViewState["mode"].ToString() == "asset_dir4")
                           )
                    {

                        foreach (GridViewRow gridrow in Gvits_u0_document_list.Rows)
                        {
                            CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                            Label lb_flow_item = (Label)gridrow.Cells[0].FindControl("lb_flow_item");
                            Label idx = (Label)gridrow.Cells[0].FindControl("idx");
                            Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                            Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");

                            if (ViewState["its_u0_document_list"] != null)
                            {
                                its_u_document[] _its_u0_document_list = (its_u_document[])ViewState["its_u0_document_list"];

                                checklist.Checked = true;
                                checklist.Enabled = false;
                            }

                        }
                    }

                }
                else if (check_approve_all.Checked == false)
                {
                    foreach (GridViewRow gridrow in Gvits_u0_document_list.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[0].FindControl("cbrecipients");
                        Label lb_nodeidx = (Label)gridrow.Cells[4].FindControl("lb_nodeidx");
                        Label lb_actoridx = (Label)gridrow.Cells[4].FindControl("lb_actoridx");

                        checklist.Checked = false;
                        checklist.Visible = true;
                        checklist.Enabled = true;

                        //ViewState["__actor"] = lb_actoridx.Text;
                        //ViewState["__node"] = lb_nodeidx.Text;
                    }

                }

                break;


            case "cbrecipients":

                // _action_select_purchase();

                if (ViewState["its_u0_document_list"] != null)
                {

                    its_u_document[] _templistpurchase = (its_u_document[])ViewState["its_u0_document_list"];

                    int _checked = int.Parse(checkbox.Text);
                    _templistpurchase[_checked].selected = checkbox.Checked;

                }
                break;

            case "check_approve_docket_all":

                if (checkbox.Checked == true)
                {
                    GridView grdlist = (GridView)Fv_docket.FindControl("gvlist_docket");
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[1].FindControl("cbrecipients_docket");
                        Label lb_flow_item = (Label)gridrow.Cells[1].FindControl("lb_flow_item");

                        if (ViewState["its_u0_document_list"] != null)
                        {
                            its_u_document[] _its_u0_document_list = (its_u_document[])ViewState["its_u0_document_list"];

                            checklist.Checked = true;
                            checklist.Enabled = false;
                        }

                    }

                }
                else if (checkbox.Checked == false)
                {
                    GridView grdlist = (GridView)Fv_docket.FindControl("gvlist_docket");
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[1].FindControl("cbrecipients_docket");

                        checklist.Checked = false;
                        checklist.Visible = true;
                        checklist.Enabled = true;

                    }

                }

                break;
            case "cbrecipients_docket":

                if (ViewState["its_u0_document_list"] != null)
                {

                    its_u_document[] _templistpurchase = (its_u_document[])ViewState["its_u0_document_list"];

                    int _checked = int.Parse(checkbox.Text);
                    _templistpurchase[_checked].selected = checkbox.Checked;

                }
                break;
            case "check_approve_docket_md_all":

                if (checkbox.Checked == true)
                {
                    GridView grdlist = (GridView)Fv_docket_md.FindControl("gvlist_docket_md");
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[1].FindControl("cbrecipients_docket_md");
                        Label lb_flow_item = (Label)gridrow.Cells[1].FindControl("lb_flow_item");

                        if (ViewState["its_u0_document_list"] != null)
                        {
                            its_u_document[] _its_u0_document_list = (its_u_document[])ViewState["its_u0_document_list"];

                            checklist.Checked = true;
                            checklist.Enabled = false;
                        }

                    }

                }
                else if (checkbox.Checked == false)
                {
                    GridView grdlist = (GridView)Fv_docket_md.FindControl("gvlist_docket_md");
                    foreach (GridViewRow gridrow in grdlist.Rows)
                    {
                        CheckBox checklist = (CheckBox)gridrow.Cells[1].FindControl("cbrecipients_docket_md");

                        checklist.Checked = false;
                        checklist.Visible = true;
                        checklist.Enabled = true;

                    }

                }

                break;
            case "cbrecipients_docket_md":

                if (ViewState["its_u0_document_list"] != null)
                {

                    its_u_document[] _templistpurchase = (its_u_document[])ViewState["its_u0_document_list"];

                    int _checked = int.Parse(checkbox.Text);
                    _templistpurchase[_checked].selected = checkbox.Checked;

                }
                break;
            case "check_special":
                setSpecial();
                break;


        }


    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "Gvits_u0_document_list":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_u0idx = (Label)e.Row.Cells[1].FindControl("lb_u0idx");
                    GridView gvitems = (GridView)e.Row.Cells[3].FindControl("gvitems");
                    setu1_document(_func_dmu.zStringToInt(lb_u0idx.Text), gvitems);

                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients");

                    //test
                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    //test
                    Label lb_Selected = (Label)e.Row.FindControl("lb_Selected");
                    lb_Selected.Text = hfSelected.Value.ToString();
                    if (check_approve_all.Checked || bool.Parse(hfSelected.Value) == true) //
                    {
                        cbrecipients.Checked = true;

                        if (check_approve_all.Checked && cbrecipients.Checked)
                        {
                            cbrecipients.Enabled = false;
                        }


                    }

                    else
                    {
                        cbrecipients.Checked = false;
                        cbrecipients.Enabled = true;
                    }

                }

                break;
            case "GvReportAdd":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[0].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");

                    Label _lbsysidx = (Label)e.Row.Cells[0].FindControl("_lbsysidx");
                    Label lb_typidx_name = (Label)e.Row.Cells[2].FindControl("lb_typidx_name");
                    Label lb_tdidx_name = (Label)e.Row.Cells[2].FindControl("lb_tdidx_name");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }
                    if (_lbsysidx.Text == "37")
                    {
                        lb_typidx_name.Visible = false;
                        lb_tdidx_name.Visible = false;
                    }
                    else
                    {
                        lb_typidx_name.Visible = true;
                        lb_tdidx_name.Visible = true;
                    }
                }

                break;
            case "gvItemsIts":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[2].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }

                    Label _lbu0idx = (Label)e.Row.Cells[0].FindControl("_lbu0idx");
                    Label _lbu1idx = (Label)e.Row.Cells[0].FindControl("_lbu1idx");
                    Label _lbflow_item = (Label)e.Row.Cells[0].FindControl("_lbflow_item");
                    GridView gvAssetNo = (GridView)e.Row.Cells[8].FindControl("gvAssetNo");
                    _dtitseet.its_u_document_action = new its_u_document[1];
                    its_u_document select_its = new its_u_document();
                    select_its.idx = _func_dmu.zStringToInt(_lbu0idx.Text);
                    select_its.u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                    select_its.operation_status_id = "list_u2";
                    _dtitseet.its_u_document_action[0] = select_its;
                    _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                    _func_dmu.zSetGridData(gvAssetNo, _dtitseet.its_u_document_action);

                    Label _lbsysidx = (Label)e.Row.Cells[0].FindControl("_lbsysidx");
                    Label lb_typidx_name = (Label)e.Row.Cells[2].FindControl("lb_typidx_name");
                    Label lb_tdidx_name = (Label)e.Row.Cells[2].FindControl("lb_tdidx_name");
                    if (_lbsysidx.Text == "37")
                    {
                        lb_typidx_name.Visible = false;
                        lb_tdidx_name.Visible = false;
                    }
                    else
                    {
                        lb_typidx_name.Visible = true;
                        lb_tdidx_name.Visible = true;
                    }

                }

                break;
            case "gvItemsIts_Asset":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[0].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");
                    LinkButton btnUploadFileAsset1 = (LinkButton)e.Row.Cells[9].FindControl("btnUploadFileAsset1");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }
                    try
                    {
                        TextBox txt_m0_node = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_node_idx");
                        TextBox txt_m0_actor = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_actor_idx");

                        if (
                            ((txt_m0_node.Text == "29") && (txt_m0_actor.Text == "1"))
                            ||
                            ((txt_m0_node.Text == "19") && (txt_m0_actor.Text == "12"))
                            )
                        {
                            btnUploadFileAsset1.Visible = true;
                        }
                        else
                        {
                            btnUploadFileAsset1.Visible = false;
                        }
                    }
                    catch { }
                    try
                    {

                        Label _lb_doccode = (Label)e.Row.Cells[1].FindControl("lb_doccode");
                        Label lb_asset_no = (Label)e.Row.Cells[1].FindControl("lb_asset_no");
                        GridView _gvFile_AssetFile = (GridView)e.Row.Cells[9].FindControl("gvFile_AssetFile1");

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                        getPathLotus = getPathLotus + "/" + _path_assetfile;
                        string filePathLotus = Server.MapPath(getPathLotus + _lb_doccode.Text + "/" + lb_asset_no.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_foreditFile(myDirLotus, _lb_doccode.Text, lb_asset_no.Text, _gvFile_AssetFile);

                    }
                    catch
                    {

                    }

                }

                break;
            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    HiddenField hidden_doccode = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_doccode");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11");
                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + hidden_doccode.Value + "/" + ltFileName11.Text;// string.Format("http://{0}", Request.Url.Host);
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                }

                break;
            case "gvFileUpdate":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    HiddenField hidden_doccode = (HiddenField)FvUpdateBuyNew.FindControl("hidden_doccode");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11");
                    LinkButton bnDeletefile = (LinkButton)e.Row.Cells[0].FindControl("bnDeletefile");
                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + hidden_doccode.Value + "/" + ltFileName11.Text;// string.Format("http://{0}", Request.Url.Host);
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                    bnDeletefile.CommandArgument = hidden_doccode.Value + "/" + ltFileName11.Text; ;
                }

                break;
            case "GvAssetits_u0_document_list":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_u0idx = (Label)e.Row.Cells[1].FindControl("lb_u0idx");

                    GridView gvitems = (GridView)e.Row.Cells[2].FindControl("gvitems");
                    setu1_document(_func_dmu.zStringToInt(lb_u0idx.Text), gvitems);

                }
                break;
            case "gvFile_Asset":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    HiddenField hidden_doccode = (HiddenField)FvDetail_ShowAsset.FindControl("hidden_doccode");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11");
                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + hidden_doccode.Value + "/" + ltFileName11.Text;// string.Format("http://{0}", Request.Url.Host);
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                }
                break;

            case "gvFile_IT":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    HiddenField hidden_doccode = (HiddenField)FvDetail_ShowIT.FindControl("hidden_doccode");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11");
                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + hidden_doccode.Value + "/" + ltFileName11.Text;// string.Format("http://{0}", Request.Url.Host);
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                }
                break;
            case "gvItemsIts_IT":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[0].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }

                    Label _lbu0idx = (Label)e.Row.Cells[0].FindControl("_lbu0idx");
                    Label _lbu1idx = (Label)e.Row.Cells[0].FindControl("_lbu1idx");
                    GridView gvAssetNo_IT = (GridView)e.Row.Cells[8].FindControl("gvAssetNo_IT");
                    _dtitseet.its_u_document_action = new its_u_document[1];
                    its_u_document select_its = new its_u_document();
                    select_its.idx = _func_dmu.zStringToInt(_lbu0idx.Text);
                    select_its.u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                    select_its.operation_status_id = "list_u2";
                    _dtitseet.its_u_document_action[0] = select_its;
                    _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                    _func_dmu.zSetGridData(gvAssetNo_IT, _dtitseet.its_u_document_action);

                    TextBox txt_spec_remark = (TextBox)e.Row.Cells[9].FindControl("txt_spec_remark");

                    txt_spec_remark.Enabled = false;
                    if ((ViewState["mode"].ToString() == "it") ||
                        (ViewState["mode"].ToString() == "it_update"))
                    {

                        txt_spec_remark.Enabled = true;
                        LinkButton linkbtn = (LinkButton)FvDetail_ShowIT.FindControl("btnAdddata");
                        if (linkbtn.Visible == false)
                        {
                            txt_spec_remark.Enabled = false;
                        }

                    }
                    else
                    {


                        //lb_spec_name.Visible = true;
                        txt_spec_remark.Enabled = false;
                    }


                }

                break;
            case "gvlist_docket":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients_docket");
                    CheckBox check_approve_docket_all = (CheckBox)Fv_docket.FindControl("check_approve_docket_all");

                    //test
                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    //test

                    if (check_approve_docket_all.Checked || bool.Parse(hfSelected.Value) == true) //
                    {
                        cbrecipients.Checked = true;

                        if (check_approve_docket_all.Checked && cbrecipients.Checked)
                        {
                            cbrecipients.Enabled = false;
                        }


                    }

                    else
                    {
                        cbrecipients.Checked = false;
                        cbrecipients.Enabled = true;
                    }

                }

                break;
            case "gvlist_docket_md":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lb_itemindex = (Label)e.Row.Cells[0].FindControl("lb_itemindex");
                    Label lb_u0_docket_idx = (Label)e.Row.Cells[4].FindControl("lb_u0_docket_idx");

                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients_docket_md");
                    CheckBox check_approve_docket_md_all = (CheckBox)Fv_docket_md.FindControl("check_approve_docket_md_all");

                    //test
                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    //test

                    if (check_approve_docket_md_all.Checked || bool.Parse(hfSelected.Value) == true) //
                    {
                        cbrecipients.Checked = true;

                        if (check_approve_docket_md_all.Checked && cbrecipients.Checked)
                        {
                            cbrecipients.Enabled = false;
                        }


                    }

                    else
                    {
                        cbrecipients.Checked = false;
                        cbrecipients.Enabled = true;
                    }


                }

                break;
            case "gvlist_docket_md_list":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }

                break;
            case "gvItemsIts_Delivery":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[0].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");
                    DropDownList ddlpr_idx = (DropDownList)e.Row.Cells[7].FindControl("ddlpr_idx");
                    Label lb_pr_idx = (Label)e.Row.Cells[7].FindControl("lb_pr_idx");
                    LinkButton btnDevice = (LinkButton)e.Row.Cells[7].FindControl("btnDevice");
                    Label _lbu0_didx = (Label)e.Row.Cells[7].FindControl("_lbu0_didx");

                    Label lbnodeidxItems = (Label)e.Row.Cells[1].FindControl("lbnodeidxItems");
                    Label lbactoridxItems = (Label)e.Row.Cells[1].FindControl("lbactoridxItems");
                    LinkButton btnUploadFileAsset = (LinkButton)e.Row.Cells[10].FindControl("btnUploadFileAsset");

                    DropDownList _ddlpr_idx = (DropDownList)e.Row.Cells[9].FindControl("ddlpr_idx");
                    TextBox _txt_pr_remark = (TextBox)e.Row.Cells[9].FindControl("txt_pr_remark");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }
                    select_device_pr_status(ddlpr_idx, _func_dmu.zStringToInt(lb_pr_idx.Text));
                    if (ddlpr_idx.SelectedValue == "1")
                    {
                        btnDevice.Visible = true;

                    }
                    else
                    {
                        btnDevice.Visible = false;
                    }
                    if (_func_dmu.zStringToInt(_lbu0_didx.Text) > 0)
                    {
                        btnDevice.Visible = false;
                    }

                    TextBox txt_sysidx = (TextBox)FvDetail_ShowDelivery.FindControl("txt_sysidx");

                    if (txt_sysidx.Text == "9")
                    {
                        btnDevice.Visible = false;
                    }


                    try
                    {

                        Label _lb_doccode = (Label)e.Row.Cells[1].FindControl("lb_doccode");
                        Label lb_asset_no = (Label)e.Row.Cells[1].FindControl("lb_asset_no");
                        GridView _gvFile_AssetFile = (GridView)e.Row.Cells[10].FindControl("gvFile_AssetFile");

                        string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                        getPathLotus = getPathLotus + "/" + _path_assetfile;
                        string filePathLotus = Server.MapPath(getPathLotus + _lb_doccode.Text + "/" + lb_asset_no.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories_foreditFile(myDirLotus, _lb_doccode.Text, lb_asset_no.Text, _gvFile_AssetFile, lbnodeidxItems.Text, lbactoridxItems.Text);

                    }
                    catch
                    {

                    }

                    if ((lbnodeidxItems.Text == "4003") && (lbactoridxItems.Text == "10"))
                    {
                        btnUploadFileAsset.Visible = false;
                        _ddlpr_idx.Enabled = false;
                        _txt_pr_remark.Enabled = false;
                    }
                    else
                    {
                        btnUploadFileAsset.Visible = true;
                        _ddlpr_idx.Enabled = true;
                        _txt_pr_remark.Enabled = true;
                    }

                }

                break;
            case "gvItemsIts_docketlist":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _lbpr_type_idx = (Label)e.Row.Cells[0].FindControl("_lbpr_type_idx");
                    Label lb_ref_itnum = (Label)e.Row.Cells[2].FindControl("lb_ref_itnum");

                    lb_ref_itnum.Visible = false;
                    if (_func_dmu.zStringToInt(_lbpr_type_idx.Text) == 2)
                    {
                        lb_ref_itnum.Visible = true;
                    }
                }

                break;
            case "gvlist_org":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lb_u0_docket_idx = (Label)e.Row.Cells[0].FindControl("lb_u0_docket_idx");
                    Label lb_org_idx_its = (Label)e.Row.Cells[0].FindControl("lb_org_idx_its");
                    GridView gvlist_docket_print_list = (GridView)e.Row.Cells[1].FindControl("gvlist_docket_print_list");
                    setdocket_u1_list_print(gvlist_docket_print_list
                        , _func_dmu.zStringToInt(lb_u0_docket_idx.Text)
                        , _func_dmu.zStringToInt(lb_org_idx_its.Text)
                        );

                }

                break;
            case "GvTrnfIndex":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients_trnf");
                    CheckBox check_approve_all = check_approve_all_trnf;

                    //test
                    HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
                    //test

                    if (check_approve_all.Checked || bool.Parse(hfSelected.Value) == true) //
                    {
                        cbrecipients.Checked = true;

                        if (check_approve_all.Checked && cbrecipients.Checked)
                        {
                            cbrecipients.Enabled = false;
                        }


                    }

                    else
                    {
                        cbrecipients.Checked = false;
                        cbrecipients.Enabled = true;
                    }

                }

                break;

            case "gvFile_AssetFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    Label lb_doccode = (Label)e.Row.Cells[0].FindControl("lb_doccode");
                    Label lb_asset_no = (Label)e.Row.Cells[0].FindControl("lb_asset_no");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11_AssetFile");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11_AssetFile");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download_AssetFile");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11_AssetFile");
                    LinkButton bnDeletefile = (LinkButton)e.Row.Cells[0].FindControl("bnDeletefile_AssetFile");
                    try
                    {
                        Label txt_m0_node = (Label)e.Row.Cells[0].FindControl("lbnodeidxItems");
                        Label txt_m0_actor = (Label)e.Row.Cells[0].FindControl("lbactoridxItems");

                        if ((txt_m0_node.Text == "4003") && (txt_m0_actor.Text == "10"))
                        {
                            bnDeletefile.Visible = false;
                        }
                        else
                        {
                            bnDeletefile.Visible = true;
                        }
                    }
                    catch { }

                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + _path_assetfile + lb_doccode.Text + "/" + lb_asset_no.Text + "/" + ltFileName11.Text;// string.Format("http://{0}", Request.Url.Host);
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                    bnDeletefile.CommandArgument = lb_doccode.Text + "/" + lb_asset_no.Text + "/" + ltFileName11.Text; ;



                }
                break;
            case "gvFile_AssetFile1":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];
                    Label lb_doccode = (Label)e.Row.Cells[0].FindControl("lb_doccode1");
                    Label lb_asset_no = (Label)e.Row.Cells[0].FindControl("lb_asset_no1");
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11_AssetFile1");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11_AssetFile1");
                    Label lb_Download = (Label)e.Row.Cells[0].FindControl("lb_Download_AssetFile1");
                    Label ltFileName11 = (Label)e.Row.Cells[0].FindControl("ltFileName11_AssetFile1");
                    LinkButton bnDeletefile = (LinkButton)e.Row.Cells[0].FindControl("bnDeletefile_AssetFile1");
                    //// Display the company name in italics.
                    string LinkHost11 = getPathLotus + _path_assetfile + lb_doccode.Text + "/" + lb_asset_no.Text + "/" + ltFileName11.Text;
                    //lb_Download.Text = LinkHost11;
                    btnDL11.NavigateUrl = ResolveUrl(LinkHost11);// MapURL(hidFile11.Value);
                    bnDeletefile.CommandArgument = lb_doccode.Text + "/" + lb_asset_no.Text + "/" + ltFileName11.Text;

                    TextBox txt_m0_node = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_node_idx");
                    TextBox txt_m0_actor = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_actor_idx");

                    if (
                            ((txt_m0_node.Text == "29") && (txt_m0_actor.Text == "1"))
                            ||
                            ((txt_m0_node.Text == "19") && (txt_m0_actor.Text == "12"))
                            )
                    {
                        bnDeletefile.Visible = true;
                    }
                    else
                    {
                        bnDeletefile.Visible = false;
                    }

                }

                break;
        }
    }

    private void setdocket_u1_list_print(GridView Gv, int u0_docket_idx, int org_idx_its)
    {
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.org_idx_its = org_idx_its;
        select_its.operation_status_id = "docket_u1_list_print";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        _func_dmu.zSetGridData(Gv, _dtitseet.its_u_document_action);
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "Gvits_u0_document_list":

                Gvits_u0_document_list.PageIndex = e.NewPageIndex;
                Gvits_u0_document_list.DataBind();
                _func_dmu.zSetGridData(Gvits_u0_document_list, ViewState["its_u0_document_list"]);
                SETFOCUS.Focus();

                break;

            case "gvitems":

                //int index = e.Row.RowIndex;
                //GridView gvitems = (GridView)[index].FindControl("gvitems");

                //data_purchase _reportItemspurchase = new data_purchase();
                //_reportItemspurchase.items_purchase_list = new items_purchase_details[1];


                //items_purchase_details _reportingItems = new items_purchase_details();

                //_reportingItems.u0_purchase_idx = int.Parse(u0_purchase_idx.Text);

                //_reportItemspurchase.items_purchase_list[0] = _reportingItems;

                //_reportItemspurchase = callServicePurchase(_urlGetitems_purchase, _reportItemspurchase);
                //ViewState["itemslist"] = _reportItemspurchase.items_purchase_list;


                //gvitems.DataSource = ViewState["itemslist"];
                //gvitems.DataBind();

                //   setGridViewDataBind(gvitems, ViewState["itemslist"]);
                //gvitems.DataSource = ViewState["itemslist"];
                //gvitems.DataBind();
                //gvitems.PageIndex = e.NewPageIndex;
                //gvitems.DataBind();

                break;
            case "gvlist_docket":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["its_u0_document_list"]);
                SETFOCUS.Focus();

                break;
            case "gvlist_docket_md":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["its_u0_document_list"]);
                SETFOCUS.Focus();

                break;
            case "gvlist_docket_list":
                Label lb_u0_docket_idx = (Label)gridViewName.Rows[0].Cells[0].FindControl("lb_u0_docket_idx");
                int u0_docket_idx = _func_dmu.zStringToInt(lb_u0_docket_idx.Text);
                _dtitseet.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                select_its.u0_docket_idx = u0_docket_idx;
                select_its.operation_status_id = "docket_u1_list";
                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, _dtitseet.its_u_document_action);
                break;
            case "gvlist_docket_md_list":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["docket_u1_list"]);
                SETFOCUS.Focus();
                break;
            case "gvItemsIts_docketlist":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["its_u1_document"]);
                SETFOCUS.Focus();
                break;
            case "gvlist_org":

                Gvits_u0_document_list.PageIndex = e.NewPageIndex;
                Gvits_u0_document_list.DataBind();
                setGridData(_GridView, ViewState["docket_u0_org_list"]);
                SETFOCUS.Focus();

                break;
            case "gvlist_docket_print_list":
                Label _lb_u0_docket_idx = (Label)gridViewName.Rows[0].Cells[0].FindControl("lb_u0_docket_idx");
                Label _lb_org_idx_its = (Label)gridViewName.Rows[0].Cells[0].FindControl("lb_org_idx_its");

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                setdocket_u1_list_print(gridViewName
                        , _func_dmu.zStringToInt(_lb_u0_docket_idx.Text)
                        , _func_dmu.zStringToInt(_lb_org_idx_its.Text)
                        );


                break;
            //transfer
            case "GvTrnfIndex":

                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, ViewState["v_GvTrnfIndex"]);
                SETFOCUS.Focus();

                break;
            case "gvAssetNo_IT":

                Label _lbu0idx = (Label)gridViewName.Rows[0].Cells[0].FindControl("_lbu0idx");
                Label _lbu1idx = (Label)gridViewName.Rows[0].Cells[0].FindControl("_lbu1idx");
                data_itasset _dtitseet1 = new data_itasset();
                _dtitseet1.its_u_document_action = new its_u_document[1];
                its_u_document select_its1 = new its_u_document();
                select_its1.idx = _func_dmu.zStringToInt(_lbu0idx.Text);
                select_its1.u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                select_its1.operation_status_id = "list_u2";
                _dtitseet1.its_u_document_action[0] = select_its1;
                _dtitseet1 = callServicePostITAsset(_urlGetits_u_document, _dtitseet1);
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, _dtitseet1.its_u_document_action);

                break;
            case "gvAssetNo":

                Label _lbu0idx_ass = (Label)gridViewName.Rows[0].Cells[0].FindControl("_lbu0idx");
                Label _lbu1idx_ass = (Label)gridViewName.Rows[0].Cells[0].FindControl("_lbu1idx");
                data_itasset _dtitseet1_ass = new data_itasset();
                _dtitseet1_ass.its_u_document_action = new its_u_document[1];
                its_u_document select_its1_ass = new its_u_document();
                select_its1_ass.idx = _func_dmu.zStringToInt(_lbu0idx_ass.Text);
                select_its1_ass.u1idx = _func_dmu.zStringToInt(_lbu1idx_ass.Text);
                select_its1_ass.operation_status_id = "list_u2";
                _dtitseet1_ass.its_u_document_action[0] = select_its1_ass;
                _dtitseet1_ass = callServicePostITAsset(_urlGetits_u_document, _dtitseet1_ass);
                gridViewName.PageIndex = e.NewPageIndex;
                gridViewName.DataBind();
                _func_dmu.zSetGridData(gridViewName, _dtitseet1_ass.its_u_document_action);

                break;

        }

    }
    private void setu1_document(int id, GridView Gv)
    {
        data_itasset dataitasset = new data_itasset();
        its_u_document obj_detail = new its_u_document();
        dataitasset.its_u_document_action = new its_u_document[1];
        obj_detail.idx = id;
        obj_detail.operation_status_id = "list_index_u1";
        dataitasset.its_u_document_action[0] = obj_detail;
        dataitasset = callServicePostITAsset(_urlGetits_u_document, dataitasset);
        _func_dmu.zSetGridData(Gv, dataitasset.its_u_document_action);

    }
    private void setPanel_doc_number()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _txtref_itnum = (TextBox)_FormView.FindControl("txtref_itnum");
        _txtionum = (TextBox)_FormView.FindControl("txtionum");
        _pnl_ionum = (Panel)_FormView.FindControl("pnl_ionum");
        _pnl_ref_itnum = (Panel)_FormView.FindControl("pnl_ref_itnum");
        _ddlsearch_typepurchase = (DropDownList)_FormView.FindControl("ddlsearch_typepurchase");

        _pnl_ionum.Visible = false;
        _pnl_ref_itnum.Visible = false;
        if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 1) //ซื้อใหม่
        {
            _pnl_ionum.Visible = true;
            _txtref_itnum.Text = "";
        }
        else if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 2) //ซื้อทดแทน
        {
            _pnl_ionum.Visible = true;
            _pnl_ref_itnum.Visible = true;
        }
        else if (_func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue) == 3) //ต่ออายุ
        {
            _pnl_ionum.Visible = true;
            _txtref_itnum.Text = "";
        }
    }

    private void setInsertPurchas_RqDefault(string _empty)
    {
        setObject();
        _txtionum.Text = "";
        _txtref_itnum.Text = "";
        _txtbudget_set.Text = "";
        _txtlist_menu.Text = "";
        _txtqty.Text = "";
        _txtprice.Text = "";

        if (_empty != "add")
        {
            select_system(_ddlsystem);
            _ddlsystem.SelectedIndex = 0;

        }

        select_systemtype(_ddlSearchTypeDevices, _func_dmu.zStringToInt(_ddlsystem.SelectedValue));
        select_devices(_ddlSearchDevices, _func_dmu.zStringToInt(_ddlsystem.SelectedValue), _func_dmu.zStringToInt(_ddlSearchTypeDevices.SelectedValue));
        select_ddlsearch_purchase(_ddlsearch_typepurchase);
        select_position(_ddlposition, 0);
        select_pos(_ddl_target);
        select_currency(_ddlcurrency);
        _ddlcurrency.SelectedValue = "1";
        select_unittype(_ddlunit);

        select_costcenter(_ddlcostcenter);
        if (ViewState["CostIDX"] != null)
        {
            // _ddlcostcenter.SelectedValue = ViewState["CostIDX"].ToString();
        }
        if (_empty != "add")
        {
            select_place(_ddlplace);
        }
        _ddlspecidx.Text = "";


    }
    private Boolean checkErrorInsert()
    {
        Boolean _Boolean = false;
        setObject();
        if (_txtremark.Text.Trim() == "")
        {
            _Boolean = true;
        }
        else if (_func_dmu.zStringToInt(_ddlplace.SelectedValue) == 0)
        {
            _Boolean = true;
        }
        else
        {
            int itemObj = 0;
            DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
            if (dsU1.Tables["dsits_u1_document"].Rows.Count == 0)
            {
                showMsAlert("กรุณากรอกรายการขอซื้ออุปกรณ์.!");
                _Boolean = true;

            }
            else
            {
                int iError = 0;

                //ckeck io no

                DataSet dsContacts1 = (DataSet)ViewState["vsits_u1_document"];
                foreach (DataRow dr1 in dsContacts1.Tables["dsits_u1_document"].Rows)
                {
                    its_masterit obj_m = new its_masterit();
                    _dtitseet.its_masterit_action = new its_masterit[1];

                    obj_m.operation_status_id = "its_m0_io_budget";
                    if (ViewState["mode"].ToString() == "E")
                    {
                        obj_m.u0idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
                    }
                    else
                    {
                        obj_m.u0idx = 0;
                    }
                    obj_m.costcenter_idx = _func_dmu.zStringToInt(dr1["costidx"].ToString());
                    obj_m.pr_type_idx = _func_dmu.zStringToInt(dr1["pr_type_idx"].ToString());
                    obj_m.iono = dr1["ionum"].ToString().Trim();

                    _dtitseet.its_masterit_action[0] = obj_m;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                    _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_masterit, _dtitseet);

                    if (_dtitseet.its_masterit_action != null)
                    {

                        foreach (var item in _dtitseet.its_masterit_action)
                        {
                            decimal price = 0, budget = 0;
                            price = _func_dmu.zStringToDecimal(_txtprice.Text);
                            budget = _func_dmu.zStringToDecimal(_txtbudget_set.Text);
                            if (price <= item.cost)
                            {
                                //ขอซื้อได้
                            }
                            else
                            {
                                iError++;
                                showMsAlert("ราคาเกินงบของเลขที่ IO : " + dr1["ionum"].ToString().Trim() + " !!!");
                                _Boolean = true;
                                break;
                            }
                            if (budget <= item.budget)
                            {
                                //ขอซื้อได้
                            }
                            else
                            {
                                iError++;
                                showMsAlert("งบประมาณที่ตั้งไว้เกินงบของเลขที่ IO : " + dr1["ionum"].ToString().Trim() + " !!!");
                                _Boolean = true;
                                break;
                            }
                        }
                    }

                    if (iError == 0)
                    {
                        int _qty = 0, _out_qty = 0;
                        string _iono = "";
                        if (_dtitseet.its_masterit_action != null)
                        {
                            foreach (var item in _dtitseet.its_masterit_action)
                            {
                                _qty = _qty + item.balance_qty;
                                _iono = item.iono;
                            }
                        }
                        DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
                        foreach (DataRow dr in dsContacts.Tables["dsits_u1_document"].Rows)
                        {
                            if (
                                (_func_dmu.zStringToInt(dr["pr_type_idx"].ToString()) ==
                                _func_dmu.zStringToInt(dr1["pr_type_idx"].ToString())
                                )
                                &&
                                (dr["ionum"].ToString().ToUpper() ==
                                 dr1["ionum"].ToString().ToUpper()
                                )
                               )
                            {
                                _out_qty = _out_qty + _func_dmu.zStringToInt(dr["qty"].ToString());
                            }

                        }
                        //  litDebug.Text = _out_qty.ToString()+"=" + _qty.ToString();
                        if (_out_qty > _qty)
                        {
                            iError++;
                            // showMsAlert("เลขที่ IO " + dr1["ionum"].ToString() + " ไม่พอ กรุณาตรวจสอบอีกครั้ง !!!");
                            showMsAlert("ไม่อนุญาตให้ใช้ IO " + dr1["ionum"].ToString() +
                    " เนื่องจากมีการนำเลขที่ IO " + dr1["ionum"].ToString() +
                    " ไปใช้ในการออกใบขอซื้อครบตามจำนวนที่ขอใน IO เรียบร้อยแล้ว " +
                    " หากท่านต้องการทำรายการขอซื้อกรุณาติดต่อฝ่ายงบประมาณ " +
                    " !!!");
                            _Boolean = true;
                            break;
                        }
                    }

                }

                if (iError == 0)
                {
                    int buy_replacement = 0, buy_new = 0;
                    foreach (DataRow item in dsU1.Tables["dsits_u1_document"].Rows)
                    {
                        if (_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 1) //ซื้อใหม่
                        {
                            buy_new = 1;
                        }
                        else if ((_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 2) || (_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 3))//ซื้อทดแทน
                        {
                            buy_replacement = 1;
                        }
                    }
                    FileUpload _uploadfile_memo;
                    FileUpload _uploadfile_organization;
                    FileUpload _uploadfile_cutoff;

                    _FormView = getFv(ViewState["mode"].ToString());
                    if (ViewState["mode"].ToString() == "E")
                    {
                        _uploadfile_memo = uploadfile_memoUpdate;
                        _uploadfile_organization = uploadfile_organizationUpdate;
                        _uploadfile_cutoff = uploadfile_cutoffUpdate;

                    }
                    else
                    {
                        _uploadfile_memo = uploadfile_memo;
                        _uploadfile_organization = uploadfile_organization;
                        _uploadfile_cutoff = uploadfile_cutoff;
                    }

                    string doccode = "";
                    if (ViewState["mode"].ToString() == "E")
                    {
                        HiddenField hidden_doccode = (HiddenField)FvUpdateBuyNew.FindControl("hidden_doccode");
                        doccode = hidden_doccode.Value;
                    }

                    if ((buy_replacement + buy_new) == 2)
                    {

                        int ic = 0;
                        string sMsError = "";
                        if (_uploadfile_memo.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(1, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , Memo";
                                }
                                else
                                {
                                    sMsError += "Memo";
                                }
                                ic++;
                            }

                        }
                        if (_uploadfile_organization.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(2, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , Organization";
                                }
                                else
                                {
                                    sMsError += "Organization";
                                }
                                ic++;
                            }

                        }
                        if (_uploadfile_cutoff.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(3, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , ตัดเสีย";
                                }
                                else
                                {
                                    sMsError += "ตัดเสีย";
                                }
                                ic++;
                            }

                        }
                        if (ic > 0)
                        {
                            showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                            _Boolean = true;
                        }
                    }
                    else if (buy_new > 0) //ซื้อใหม่
                    {
                        int ic = 0;
                        string sMsError = "";
                        if (_uploadfile_memo.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(1, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , Memo";
                                }
                                else
                                {
                                    sMsError += "Memo";
                                }
                                ic++;
                            }
                        }
                        if (_uploadfile_organization.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(2, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , Organization";
                                }
                                else
                                {
                                    sMsError += "Organization";
                                }
                                ic++;
                            }

                        }
                        if (ic > 0)
                        {
                            showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                            _Boolean = true;
                        }
                    }
                    else if (buy_replacement > 0) //ซื้อทดแทน
                    {
                        int ic = 0;
                        string sMsError = "";
                        if (_uploadfile_memo.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(1, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , Memo";
                                }
                                else
                                {
                                    sMsError += "Memo";
                                }
                                ic++;
                            }
                        }

                        if (_uploadfile_cutoff.HasFile == false)
                        {
                            int icount = 0;
                            if (ViewState["mode"].ToString() == "E")
                            {
                                icount = getItemFile(3, doccode);
                            }
                            if (icount == 0)
                            {
                                if (ic > 0)
                                {
                                    sMsError += " , ตัดเสีย";
                                }
                                else
                                {
                                    sMsError += "ตัดเสีย";
                                }
                                ic++;
                            }

                        }
                        if (ic > 0)
                        {
                            showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                            _Boolean = true;
                        }
                    }
                }

            }

        }

        return _Boolean;
    }
    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            //
        }
    }

    private void setViewStateDefalut()
    {
        ViewState["u0idx"] = "";
        ViewState["node_idx"] = "";
        ViewState["emp_idx_its"] = "";
        ViewState["pr_type_idx"] = "";
        ViewState["actor_idx"] = "";
        ViewState["_DOCUMENTCODE"] = "";
        ViewState["doc_status"] = "";
        ViewState["flow_item"] = "";
        ViewState["doccode"] = "";
    }

    protected void action_select_its(int id, int item)
    {
        content_history_purchase_edit.Visible = false;
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();

        //select_purchase.action_type = 1; //เรียกข้อมูลการขอซื้อมาเฉพาะ ID นั้นๆ
        select_its.idx = id;
        select_its.operation_status_id = "list_u0";

        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        if (_dtitseet.its_u_document_action != null)
        {
            foreach (var item_its in _dtitseet.its_u_document_action)
            {
                ViewState["u0idx"] = item_its.u0idx;
                ViewState["node_idx"] = item_its.node_idx;
                ViewState["emp_idx_its"] = item_its.emp_idx_its;
                ViewState["pr_type_idx"] = item_its.pr_type_idx;
                ViewState["actor_idx"] = item_its.actor_idx;
                ViewState["_DOCUMENTCODE"] = item_its.doccode;
                ViewState["doc_status"] = item_its.doc_status;
                ViewState["flow_item"] = item_its.flow_item;
                ViewState["doccode"] = item_its.doccode;
                ViewState["place_idx"] = item_its.place_idx;
                ViewState["sysidx"] = item_its.sysidx;
            }
        }
        else
        {
            setViewStateDefalut();
        }

        int ic = 0, iemp_idx_its = 0;
        if ((ViewState["mode"].ToString() != "dir_user_approve") &&
            (ViewState["mode"].ToString() != "budget"))
        {
            if (item == 1) // แก้ไขเอกสาร
            {
                if (_dtitseet.its_u_document_action != null)
                {
                    foreach (var item1 in _dtitseet.its_u_document_action)
                    {
                        if (item1.emp_idx_its == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
                        {
                            ic++;
                        }
                    }
                }
            }
            else if (item == 14) // แก้ไขราคา IO
            {
                ic = 0;
                if (_dtitseet.its_u_document_action != null)
                {
                    foreach (var item1 in _dtitseet.its_u_document_action)
                    {
                        if (item1.emp_idx_its == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
                        {
                            iemp_idx_its = item1.emp_idx_its;
                        }
                    }
                }
            }
            else
            {
                ic = 0;
            }
        }
        if (ic > 0) // มีสิทธิ์แก้ไข
        {
            MvMaster.SetActiveView(ViewBuy);
            FvDetailUser.ChangeMode(FormViewMode.Insert);
            FvDetailUser.DataBind();
            div_createbuynew.Visible = true;
            div_createbuyreplace.Visible = false;
            div_createcutdevices.Visible = false;
            FvUpdateBuyNew.ChangeMode(FormViewMode.Edit);
            FvUpdateBuyNew.DataBind();
            FvUpdateBuyNew.DataSource = _dtitseet.its_u_document_action;
            FvUpdateBuyNew.DataBind();
            CleardataSetBuyNewList();
            setDefaultPurchase();
            setInsertPurchas_RqDefault("update");
            setObject();
            setPanel_doc_number();
            if (ViewState["place_idx"] != null)
            {
                _ddlplace.SelectedValue = ViewState["place_idx"].ToString();
            }
            if (ViewState["sysidx"] != null)
            {
                _ddlsystem.SelectedValue = ViewState["sysidx"].ToString();
                setInsertPurchas_RqDefault("add");
            }


            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = id;
            select_its.operation_status_id = "list_u1";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
            if (_dtitseet.its_u_document_action != null)
            {
                foreach (var vdr in _dtitseet.its_u_document_action)
                {

                    DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

                    drContacts["u1idx"] = vdr.u1idx;

                    drContacts["sysidx"] = vdr.sysidx;
                    drContacts["sysidx_name"] = vdr.SysNameTH;

                    drContacts["typidx"] = vdr.typidx;
                    drContacts["typidx_name"] = vdr.type_name;

                    drContacts["tdidx"] = vdr.tdidx;
                    drContacts["tdidx_name"] = vdr.zdevices_name;
                    drContacts["zdevices_name"] = vdr.zdevices_name;

                    drContacts["pr_type_idx"] = vdr.pr_type_idx;
                    drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                    drContacts["ionum"] = vdr.ionum;
                    drContacts["ref_itnum"] = vdr.ref_itnum;

                    drContacts["rpos_idx"] = vdr.rpos_idx;
                    drContacts["rpos_idx_name"] = vdr.pos_name_th;

                    drContacts["list_menu"] = vdr.list_menu;

                    drContacts["price"] = vdr.price;

                    drContacts["currency_idx"] = vdr.currency_idx;
                    drContacts["currency_idx_name"] = vdr.currency_th;

                    drContacts["qty"] = vdr.qty;

                    drContacts["unidx"] = vdr.unidx;
                    drContacts["unidx_name"] = vdr.NameTH;

                    drContacts["costidx"] = vdr.costidx;
                    drContacts["costcenter_idx_name"] = vdr.CostNo;

                    drContacts["budget_set"] = vdr.budget_set;

                    drContacts["place_idx"] = vdr.place_idx;
                    drContacts["place_idx_name"] = vdr.place_name;

                    drContacts["spec_idx"] = 0;
                    drContacts["spec_idx_name"] = "";

                    drContacts["leveldevice"] = vdr.leveldevice;
                    drContacts["total_amount"] = vdr.total_amount;

                    drContacts["posidx"] = vdr.posidx;
                    drContacts["pos_name"] = vdr.pos_name;

                    drContacts["m1_ref_idx"] = vdr.m1_ref_idx;
                    drContacts["special_flag"] = vdr.special_flag;

                    dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);
                }
            }

            ViewState["vsits_u1_document"] = dsContacts;
            _GvReportAdd.Visible = true;

            setGridData(_GvReportAdd, dsContacts.Tables["dsits_u1_document"]);

            content_history_purchase_edit.Visible = true;
            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = id;
            select_its.operation_status_id = "list_l0";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            // Repeater history_purchasequipment_edit = (Repeater)FvUpdateBuyNew.FindControl("history_purchasequipment_edit");
            history_purchasequipment_edit.DataSource = _dtitseet.its_u_document_action;
            history_purchasequipment_edit.DataBind();



        }
        else
        {
            MvMaster.SetActiveView(viewmanage_purchasequipment);
            fvinformation_purchasequipment.DataSource = _dtitseet.its_u_document_action;
            fvinformation_purchasequipment.DataBind();

            fvdetails_purchasequipment.DataSource = _dtitseet.its_u_document_action;
            fvdetails_purchasequipment.DataBind();


            fvmanage_approvepurchase.ChangeMode(FormViewMode.Insert);
            fvmanage_approvepurchase.DataBind();

            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = id;
            select_its.operation_status_id = "list_u1";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            //gvItemsIts.DataSource = _dtitseet.its_u_document_action;
            //gvItemsIts.DataBind();
            CreateDsits_u1_document();
            DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
            if (_dtitseet.its_u_document_action != null)
            {
                foreach (var vdr in _dtitseet.its_u_document_action)
                {

                    DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

                    drContacts["u0idx"] = vdr.u0idx;
                    drContacts["u1idx"] = vdr.u1idx;

                    drContacts["sysidx"] = vdr.sysidx;
                    drContacts["sysidx_name"] = vdr.SysNameTH;

                    drContacts["typidx"] = vdr.typidx;
                    drContacts["typidx_name"] = vdr.type_name;

                    drContacts["tdidx"] = vdr.tdidx;
                    drContacts["tdidx_name"] = vdr.zdevices_name;
                    drContacts["zdevices_name"] = vdr.zdevices_name;

                    drContacts["pr_type_idx"] = vdr.pr_type_idx;
                    drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                    drContacts["ionum"] = vdr.ionum;
                    drContacts["ref_itnum"] = vdr.ref_itnum;

                    drContacts["rpos_idx"] = vdr.rpos_idx;
                    drContacts["rpos_idx_name"] = vdr.pos_name_th;

                    drContacts["list_menu"] = vdr.list_menu;

                    drContacts["price"] = vdr.price;

                    drContacts["currency_idx"] = vdr.currency_idx;
                    drContacts["currency_idx_name"] = vdr.currency_th;

                    drContacts["qty"] = vdr.qty;

                    drContacts["unidx"] = vdr.unidx;
                    drContacts["unidx_name"] = vdr.NameTH;

                    drContacts["costidx"] = vdr.costidx;
                    drContacts["costcenter_idx_name"] = vdr.CostNo;

                    drContacts["budget_set"] = vdr.budget_set;

                    drContacts["place_idx"] = vdr.place_idx;
                    drContacts["place_idx_name"] = vdr.place_name;

                    drContacts["spec_idx"] = 0;
                    drContacts["spec_idx_name"] = "";

                    drContacts["leveldevice"] = vdr.leveldevice;

                    drContacts["total_amount"] = vdr.total_amount;

                    drContacts["spec_remark"] = vdr.spec_remark;

                    drContacts["posidx"] = vdr.posidx;
                    drContacts["pos_name"] = vdr.pos_name;

                    drContacts["m1_ref_idx"] = vdr.m1_ref_idx;

                    dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);
                }
            }

            ViewState["vsits_u1_document"] = dsContacts;
            setGridData(gvItemsIts, dsContacts.Tables["dsits_u1_document"]);

            gvItemsIts.Columns[8].Visible = false;
            gvItemsIts.Columns[9].Visible = false;
            if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) > 4)
            {
                gvItemsIts.Columns[8].Visible = true;
            }
            if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) > 6)
            {
                gvItemsIts.Columns[9].Visible = true;
            }

            if ((_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 14) &&
                (iemp_idx_its == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
                )
            {
                gvItemsIts.Columns[10].Visible = true;
            }
            else
            {
                gvItemsIts.Columns[10].Visible = false;
            }

            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = id;
            select_its.operation_status_id = "list_l0";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            history_purchasequipment.DataSource = _dtitseet.its_u_document_action;
            history_purchasequipment.DataBind();

            HiddenField hidden_actor_name = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_actor_name");
            Label lb_title_approve = (Label)fvmanage_approvepurchase.FindControl("lb_title_approve");

            lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;



        }


    }


    public void SearchDirectories_foreditFile(DirectoryInfo dir, String target, String sAssetNo, GridView gridview, String _node_idx = "0", String _actor_idx = "0")
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dc1 = new DataColumn("doccode", typeof(string));
        dc1 = new DataColumn("asset_no", typeof(string));
        dc1 = new DataColumn("asset_no1", typeof(string));
        dc1 = new DataColumn("actor_idx", typeof(string));
        dc1 = new DataColumn("node_idx", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");
        dt1.Columns.Add("doccode");
        dt1.Columns.Add("asset_no");
        dt1.Columns.Add("asset_no1");
        dt1.Columns.Add("actor_idx");
        dt1.Columns.Add("node_idx");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {

                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                dt1.Rows[i][2] = target;
                dt1.Rows[i][3] = sAssetNo;
                dt1.Rows[i][5] = _actor_idx;
                dt1.Rows[i][6] = _node_idx;

                i++;

            }

        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gridview.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gridview.DataBind();
            ds1.Dispose();
        }
        else
        {

            gridview.DataSource = null;
            gridview.DataBind();

        }



    }

    protected void action_actor()
    {
        int node_idx = 0;
        int actor_idx = 0;
        int emp_idx_purchase = 0;
        int emp_director = 0;
        int status_action = 0;


        //find hidden value
        HiddenField hidden_m0_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_node");
        HiddenField hidden_m0_actor = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_m0_actor");
        HiddenField hidden_emp_idx = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_emp_idx");
        HiddenField hidden_emp_director = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_emp_director");
        HiddenField hidden_status_node = (HiddenField)fvdetails_purchasequipment.FindControl("hidden_status_node");

        //linkbutton
        var btnnotapprove_editsheet = (LinkButton)fvmanage_approvepurchase.FindControl("btnnotapprove_editsheet");
        var btnnotapprove = (LinkButton)fvmanage_approvepurchase.FindControl("btnnotapprove");
        var btnsaveapprove = (LinkButton)fvmanage_approvepurchase.FindControl("btnsaveapprove");

        var Panel_alert_warning = (Panel)fvmanage_approvepurchase.FindControl("Panel_alert_warning");
        //  var panelNumberIO = (Panel)fvdetails_purchasequipment.FindControl("panelNumberIO");


        var txtcomment_purchase = (TextBox)fvmanage_approvepurchase.FindControl("txtcomment_purchase");
        var lb_warning_action = (Label)Panel_alert_warning.FindControl("lb_warning_action");
        var lb_warning_action_edit_doc = (Label)Panel_alert_warning.FindControl("lb_warning_action_edit_doc");

        node_idx = int.Parse(hidden_m0_node.Value);
        actor_idx = int.Parse(hidden_m0_actor.Value);
        emp_idx_purchase = int.Parse(hidden_emp_idx.Value);
        emp_director = int.Parse(hidden_emp_director.Value);
        status_action = int.Parse(hidden_status_node.Value);

        ViewState["_nodeidx"] = node_idx;
        ViewState["_actoridx"] = actor_idx;

        switch (actor_idx)
        {

            case 1: //ผู้ขอซื้อ

                if (node_idx == 10 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_purchase) //check emp_idx ผู้ขอซื้อ (กรณีแก้ไขเอกสาร)
                {

                }
                else
                {

                }


                break;


        }

    }

    private Boolean approve_director(int id, int node_idx, int actor_idx)
    {
        Boolean _Boolean = false;
        if (id > 0)
        {
            data_itasset dataitasset = new data_itasset();
            its_u_document obj_u_document = new its_u_document();
            dataitasset.its_u_document_action = new its_u_document[1];
            obj_u_document.u0idx = id;
            obj_u_document.operation_status_id = "approve_director";

            //detail
            obj_u_document.director_emp_idx = emp_idx;
            obj_u_document.director_approve_remark = txtcomment_its.Text;
            obj_u_document.director_approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

            if (_func_dmu.zStringToInt(ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                //obj_u_document.u0_idx = 3;
                obj_u_document.flow_item = 3;
            }
            else //กลับไปแก้ไข
            {
                //obj_u_document.u0_idx = 2;
                obj_u_document.flow_item = 1; //กับไปแก้ไข
            }

            obj_u_document.node_idx = node_idx;
            obj_u_document.actor_idx = actor_idx;
            obj_u_document.doc_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

            dataitasset.its_u_document_action[0] = obj_u_document;
            //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_create(id, 3, 0);
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 2);
            }

            _Boolean = true;
        }

        return _Boolean;
    }

    private Boolean approve_budget(int id, int node_idx, int actor_idx)
    {
        Boolean _Boolean = false;
        if (id > 0)
        {
            data_itasset dataitasset = new data_itasset();
            its_u_document obj_u_document = new its_u_document();
            dataitasset.its_u_document_action = new its_u_document[1];
            obj_u_document.u0idx = id;
            obj_u_document.operation_status_id = "approve_budget";

            //detail
            obj_u_document.budget_emp_idx = emp_idx;
            obj_u_document.budget_approve_remark = txtcomment_its.Text;
            obj_u_document.budget_approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);
            obj_u_document.doc_status = 1; //ดำเนินการ
            if (_func_dmu.zStringToInt(ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                obj_u_document.flow_item = 4;
            }
            else //กลับไปแก้ไข
            {
                obj_u_document.flow_item = 1; //กับไปแก้ไข
            }

            obj_u_document.node_idx = node_idx;
            obj_u_document.actor_idx = actor_idx;
            obj_u_document.doc_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

            dataitasset.its_u_document_action[0] = obj_u_document;
            //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_create(id, 4, 0);
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 3);
            }

            _Boolean = true;
        }

        return _Boolean;
    }

    private void Ms_purchase(int iMs_node, int irdept_idx)
    {
        ViewState["mode_approve"] = "";
        int iPanel = 0;
        Boolean _Boolean = false;
        Panel Panel_body_approve = (Panel)fvmanage_approvepurchase.FindControl("Panel_body_approve");
        Panel Panel_alert_warning = (Panel)fvmanage_approvepurchase.FindControl("Panel_alert_warning");
        Label lb_warning_action = (Label)fvmanage_approvepurchase.FindControl("lb_warning_action");
        Label lb_warning_action_edit_doc = (Label)fvmanage_approvepurchase.FindControl("lb_warning_action_edit_doc");
        _lbtn = (LinkButton)fvmanage_approvepurchase.FindControl("btnApproveDirector");
        //TextBox txt_rdept_idx_its = (TextBox)fvdetails_purchasequipment.FindControl("txt_rdept_idx_its");

        setjobgrade_director(irdept_idx);
        _lbtn.Visible = false;
        Panel_body_approve.Visible = false;
        if (iMs_node == 0) // ยังไม่เข้า process approve insert
        {
            _lbtn.Visible = false;
            Panel_body_approve.Visible = false;
        }
        else if (iMs_node == 1) // ยังไม่เข้า process approve edit
        {
            _lbtn.Visible = false;
            Panel_body_approve.Visible = false;
        }
        else if ((iMs_node == 2) && (ViewState["mode"].ToString() == "dir_user_approve")) // wait  approve by director
        {
            if (
                 (_func_dmu.zStringToInt(ViewState["set_jobgrade_level_dir"].ToString())
                 >= 9
                 )
                 &&
                 (_func_dmu.zStringToInt(ViewState["set_rdept_idx_dir"].ToString())
                 ==
                // _func_dmu.zStringToInt(txt_rdept_idx_its.Text)
                irdept_idx
                 )
                 )
            {
                _Boolean = true;
                ViewState["mode_approve"] = "dir_2";

            }
            else
            {
                _Boolean = false;
            }
            if (emp_idx == it_admin)
            {
                _Boolean = true;
                ViewState["mode_approve"] = "dir_2";
            }
            if ((_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                    &&
                     (ViewState["mode"].ToString() != "P")
                     &&
                     (ViewState["mode"].ToString() != "I")
                     &&
                     (ViewState["mode"].ToString() != "E")
                    )
            {
                _Boolean = true;
                ViewState["mode_approve"] = "dir_2";
            }

            _lbtn.Visible = _Boolean;
            Panel_body_approve.Visible = _Boolean;
        }
        else if ((iMs_node == 3) && (ViewState["mode"].ToString() == "budget")) // wait approve by budget 
        {
            if (
               (_func_dmu.zStringToInt(ViewState["rdept_idx_budget"].ToString())
               ==
               _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString())
               )
               &&
               (_func_dmu.zStringToInt(ViewState["org_idx_budget"].ToString())
               ==
               _func_dmu.zStringToInt(ViewState["org_idx"].ToString())
               )
               &&
               (_func_dmu.zStringToInt(ViewState["rsec_idx_budget"].ToString())
               ==
               _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString())
               )
               &&
               (_func_dmu.zStringToInt(ViewState["rpos_idx_budget"].ToString())
               ==
               _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString())
               )
               )
            {
                int icount = 0;

                _dtitseet.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                //select_its.emp_idx = emp_idx;
                select_its.flow_item = iMs_node;
                select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
                select_its.operation_status_id = "permission_admin_select";

                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                if (_dtitseet.its_u_document_action != null)
                {
                    icount = _dtitseet.its_u_document_action.Count();
                }
                if (icount > 0)
                {
                    _Boolean = true;
                    ViewState["mode_approve"] = "budget_3";
                }
                else
                {
                    _Boolean = false;
                }
            }
            else
            {
                _Boolean = false;
            }

            if ((_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                    &&
                     (ViewState["mode"].ToString() != "P")
                     &&
                     (ViewState["mode"].ToString() != "I")
                     &&
                     (ViewState["mode"].ToString() != "E")
                    )
            {
                _Boolean = true;
                ViewState["mode_approve"] = "budget_3";
            }

            _lbtn.Visible = _Boolean;
            Panel_body_approve.Visible = _Boolean;
        }
        else if (iMs_node == 14)
        {
            int iemp_idx_its = 0;
            _dtitseet.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();

            select_its.idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
            select_its.operation_status_id = "list_u0";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            _lbtn.Visible = false;
            Panel_body_approve.Visible = false;
            if (_dtitseet.its_u_document_action != null)
            {
                foreach (var item1 in _dtitseet.its_u_document_action)
                {
                    if (item1.emp_idx_its == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
                    {
                        iemp_idx_its = item1.emp_idx_its;
                    }
                }
                if (iemp_idx_its == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
                {
                    _lbtn.Visible = true;
                    Panel_body_approve.Visible = true;
                }
            }

        }
        else if (iMs_node == 44)
        {
            _lbtn.Visible = false;
            Panel_body_approve.Visible = false;
        }
        else
        {
            _lbtn.Visible = false;
            Panel_body_approve.Visible = false;
        }

        if ((_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                    &&
                     (ViewState["mode"].ToString() != "P")
                     &&
                     (ViewState["mode"].ToString() != "I")
                     &&
                     (ViewState["mode"].ToString() != "E")
                    )
        {
            _lbtn.Visible = true;
            Panel_body_approve.Visible = true;
            //_lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
        }


    }

    private Boolean CheckJobgradeApprove(int iMs_node)
    {
        Boolean _Boolean = false;
        if (
                (_func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString())
                ==
                _func_dmu.zStringToInt(ViewState["jobgrade_level"].ToString())
                )
                &&
                (_func_dmu.zStringToInt(ViewState["rdept_idx_dir"].ToString())
                ==
                _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString())
                )
                )
        {
            //if (iMs_node == 2) // director user
            //{

            //}
            _Boolean = true;
        }
        else
        {
            _Boolean = false;
        }

        return _Boolean;

    }


    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        _divMenuLiToDivIndex.Attributes.Remove("class");
        _divMenuLiToDivBuyNew.Attributes.Remove("class");
        _divMenuLiToDivDevices.Attributes.Remove("class");
        _divMenuLiToDivChangeOwn.Attributes.Remove("class");
        _divMenuLiToDivWaitApprove.Attributes.Remove("class");
        _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
        _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
        _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
        _divMenuLiToDivBudget.Attributes.Remove("class");
        _divMenuLiToDivAsset.Attributes.Remove("class");
        _divMenuLiToDivAsset_system.Attributes.Remove("class");
        _divMenuLiToDivAsset_master.Attributes.Remove("class");
        //_divMenuLiToDivDataAsset.Attributes.Remove("class");
        _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
        _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");
        _divMenuLiIT.Attributes.Remove("class");
        _liIT_list1.Attributes.Remove("class");
        _liIT_manager.Attributes.Remove("class");
        _liIT_director.Attributes.Remove("class");
        _liMenuLiToasset_dir1.Attributes.Remove("class");
        _liIT_summary.Attributes.Remove("class");
        _divMenuLiMD.Attributes.Remove("class");
        // _liMD_it.Attributes.Remove("class");
        _liIT_deliver.Attributes.Remove("class");
        _divMenuLiToDivMasterBudget.Attributes.Remove("class");
        _divMenuLiToDivBudget_system.Attributes.Remove("class");
        _liIT_list1_update.Attributes.Remove("class");
        _liIT_print.Attributes.Remove("class");
        _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
        _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");
        _divMenuLiToDivAsset_master.Attributes.Remove("class");
        _divMenuLipurchase.Attributes.Remove("class");


        _libtnM_SeniorManager.Attributes.Remove("class");


        _libtnM_Dir_Asset.Attributes.Remove("class");

        _libtnM_CFO.Attributes.Remove("class");

        _libtnAttachFlieAsset.Attributes.Remove("class");

        _libtnAttachFlieAsset_show.Attributes.Remove("class");

        switch (activeTab)
        {
            case "p":
                _divMenuLiToDivIndex.Attributes.Add("class", "active");
                break;
            case "insert":
                _divMenuLiToDivBuyNew.Attributes.Add("class", "active");
                break;
            case "update":
                _divMenuLiToDivBuyNew.Attributes.Add("class", "active");
                break;
            case "approve":
                _divMenuLiToDivBuyNew.Attributes.Add("class", "active");
                break;
            case "asset":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiToDivAsset_system.Attributes.Add("class", "active");
                break;
            case "asset_dir1":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _liMenuLiToasset_dir1.Attributes.Add("class", "active");
                break;
            case "asset_dir2":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnM_SeniorManager.Attributes.Add("class", "active");
                break;
            case "asset_dir3":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnM_Dir_Asset.Attributes.Add("class", "active");
                break;
            case "asset_dir4":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnM_CFO.Attributes.Add("class", "active");
                break;
            case "it":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_list1.Attributes.Add("class", "active");
                break;
            case "it_update":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_list1_update.Attributes.Add("class", "active");
                break;
            case "it-manager":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_manager.Attributes.Add("class", "active");
                break;
            case "it-dir":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_director.Attributes.Add("class", "active");

                break;
            case "it_summary":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_summary.Attributes.Add("class", "active");
                break;
            case "budget":
                _divMenuLiToDivBudget.Attributes.Add("class", "active");
                _divMenuLiToDivBudget_system.Attributes.Add("class", "active");
                break;
            case "budget_io":
                _divMenuLiToDivBudget.Attributes.Add("class", "active");
                _divMenuLiToDivMasterBudget.Attributes.Add("class", "active");
                break;
            case "dir_user_approve":
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Add("class", "active");
                break;
            case "md_summary":
                _divMenuLiMD.Attributes.Add("class", "active");
                //_liMD_it.Attributes.Add("class", "active");
                break;
            case "it_deliver":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_deliver.Attributes.Add("class", "active");
                break;
            case "it_print":
                _divMenuLiIT.Attributes.Add("class", "active");
                _liIT_print.Attributes.Add("class", "active");
                break;
            case "import_asset":
                _divMenuLiToDivDataBuyAsset.Attributes.Add("class", "active");
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                break;
            case "master_class":
                _divMenuLiToDivAsset_master.Attributes.Add("class", "active");
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                break;
            case "data_asset":
                _divMenuLiToDivDataGiveAsset.Attributes.Add("class", "active");
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                break;
            case "transfer":
                _divMenuLiToDivChangeOwn.Attributes.Add("class", "active");
                break;
            case "transfer_approve":
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Add("class", "active");
                break;
            case "pur":
                _divMenuLipurchase.Attributes.Add("class", "active");
                break;
            case "attach_flie_asset":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnAttachFlieAsset.Attributes.Add("class", "active");
                break;
            case "attach_flie_asset_show":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnAttachFlieAsset_show.Attributes.Add("class", "active");
                break;
            case "asset_report":
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _libtnAsset_Report.Attributes.Add("class", "active");
                break;

        }


    }
    #endregion setActiveTab

    private FormView getFv(string _sMode)
    {
        if (_sMode == "I")
        {
            return FvInsertBuyNew;
        }
        else
        {
            return FvUpdateBuyNew;
        }
    }

    private Boolean zSaveUpdate(int id)
    {
        int idx;
        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "TRQ";

        setObject();

        HiddenField hidden_m0_node = (HiddenField)FvUpdateBuyNew.FindControl("hidden_m0_node");
        HiddenField hidden_m0_actor = (HiddenField)FvUpdateBuyNew.FindControl("hidden_m0_actor");

        data_itasset dataitasset = new data_itasset();
        its_u_document obj_u_document = new its_u_document();
        dataitasset.its_u_document_action = new its_u_document[1];
        obj_u_document.u0idx = id;
        obj_u_document.CEmpIDX = emp_idx;
        obj_u_document.UEmpIDX = emp_idx;
        obj_u_document.operation_status_id = "U0";

        obj_u_document.org_idx_its = _func_dmu.zStringToInt(ViewState["Org_idx"].ToString());
        obj_u_document.rdept_idx_its = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_u_document.rpos_idx_its = _func_dmu.zStringToInt(ViewState["Pos_idx"].ToString());
        obj_u_document.rsec_idx_its = _func_dmu.zStringToInt(ViewState["Sec_idx"].ToString());

        //detail
        obj_u_document.emp_idx_its = emp_idx;
        obj_u_document.remark = _txtremark.Text;
        obj_u_document.place_idx = _func_dmu.zStringToInt(_ddlplace.SelectedValue);
        obj_u_document.sysidx = _func_dmu.zStringToInt(_ddlsystem.SelectedValue);
        obj_u_document.doc_status = 1;
        obj_u_document.node_idx = _func_dmu.zStringToInt(hidden_m0_node.Value);
        obj_u_document.actor_idx = _func_dmu.zStringToInt(hidden_m0_actor.Value);
        //obj_u_document.u0_idx = 23;

        //node
        obj_u_document.approve_status = 0;
        obj_u_document.its_status = 1;
        obj_u_document.flow_item = 2;

        dataitasset.its_u_document_action[0] = obj_u_document;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));

        int iFile = 0;
        if (uploadfile_memoUpdate.HasFile)
        {
            iFile++;
        }
        if (uploadfile_organizationUpdate.HasFile)
        {
            iFile++;
        }
        if (uploadfile_cutoffUpdate.HasFile)
        {
            iFile++;
        }
        if (uploadfile_otherUpdate.HasFile)
        {
            iFile++;
        }
        iFile++;
        if (iFile == 0)
        {
            //  showMsAlert("กรุณาแนบไฟล์เอกสาร.!");
        }

        if (iFile > 0)
        {



            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
            idx = _func_dmu.zStringToInt(dataitasset.ReturnCode);
            string _returndocumentcode = "";

            if (idx > 0)
            {
                _dtitseet.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                select_its.idx = idx;
                select_its.operation_status_id = "list_u0";

                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                _returndocumentcode = "";
                if (_dtitseet.its_u_document_action != null)
                {
                    foreach (var item in _dtitseet.its_u_document_action)
                    {
                        _returndocumentcode = item.doccode;
                    }
                }
                if (_returndocumentcode != "")
                {
                    string sfile_other = "", sfile_memo = "", sfile_organization = "", sfile_cutoff = "";

                    /*
                    try
                    {
                        string Pathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                        Pathfile = Pathfile + _returndocumentcode;
                        int ic = 0;
                        string[] filesLoc = Directory.GetFiles(Server.MapPath(Pathfile + "/"));
                        List<System.Web.UI.WebControls.ListItem> files = new List<System.Web.UI.WebControls.ListItem>();
                        foreach (string file in filesLoc)
                        {
                            try
                            {
                                File.Delete(file);
                                ic++;
                            }
                            catch { }
                        }
                    }
                    catch { }
                    */

                    if (uploadfile_memoUpdate.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_code = _returndocumentcode;
                        string fileName_upload = "[เอกสาร memo]" + document_code;
                        string filePath_upload = Server.MapPath(getPathfile + document_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }

                        int ic = 0;
                        ic = getItemFile(1, document_code);
                        ic++;
                        foreach (HttpPostedFile uploadedFile in uploadfile_memoUpdate.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_memo = fileName_upload + "-" + ic.ToString() + extension;
                                    // sfile_memo = fileName_upload;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + sfile_memo);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }


                    }

                    if (uploadfile_organizationUpdate.HasFile)
                    {

                        string getPathfileOrg = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_codeorg = _returndocumentcode;
                        string fileName_uploadorg = "[เอกสาร organization]" + document_codeorg;
                        string filePath_uploadorg = Server.MapPath(getPathfileOrg + document_codeorg);

                        if (!Directory.Exists(filePath_uploadorg))
                        {
                            Directory.CreateDirectory(filePath_uploadorg);
                        }

                        int ic = 0;
                        ic = getItemFile(2, document_codeorg);
                        ic++;
                        foreach (HttpPostedFile uploadedFile in uploadfile_organizationUpdate.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_organization = fileName_uploadorg + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfileOrg + document_codeorg) + "\\" + sfile_organization);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }

                    }

                    if (uploadfile_cutoffUpdate.HasFile)
                    {

                        string getPathfilecutoff = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_codecutoff = _returndocumentcode;
                        string fileName_uploadcutoff = "[เอกสารการตัดเสีย]" + document_codecutoff;
                        string filePath_uploadcutoff = Server.MapPath(getPathfilecutoff + document_codecutoff);

                        if (!Directory.Exists(filePath_uploadcutoff))
                        {
                            Directory.CreateDirectory(filePath_uploadcutoff);
                        }

                        int ic = 0;
                        ic = getItemFile(3, document_codecutoff);
                        ic++;
                        foreach (HttpPostedFile uploadedFile in uploadfile_cutoffUpdate.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_cutoff = fileName_uploadcutoff + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfilecutoff + document_codecutoff) + "\\" + sfile_cutoff);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }


                    }

                    if (uploadfile_otherUpdate.HasFile)
                    {

                        string getPathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                        string document_code = _returndocumentcode;
                        string fileName_upload = "[เอกสาร อื่นๆ]" + document_code;
                        string filePath_upload = Server.MapPath(getPathfile + document_code);

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        int ic = 0;
                        ic = getItemFile(4, document_code);
                        ic++;
                        foreach (HttpPostedFile uploadedFile in uploadfile_otherUpdate.PostedFiles)
                        {
                            try
                            {
                                if (uploadedFile.ContentLength > 0)
                                {
                                    string extension = Path.GetExtension(uploadedFile.FileName);
                                    sfile_other = fileName_upload + "-" + ic.ToString() + extension;
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + document_code) + "\\" + sfile_other);

                                }
                            }
                            catch (Exception Ex)
                            {

                            }
                            ic++;
                        }

                    }

                }

            }

            if (id > 0)
            {
                idx = id;

            }
            if (idx > 0)
            {
                zSaveDetail(idx);
                settemplate_asset_create(idx, 2, 0);

                // showMsAlert("บันทึกข้อมูลเสร็จแล้ว.");

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }

            _Boolean = true;
        }
        else
        {
            _Boolean = false;
        }
        return _Boolean;
    }

    private void setSearchList()
    {
        action_select_organization(0, "0");
        action_select_depertment(0, "0");
        select_place(ddllocation_search);
        select_costcenter_its(ddl_costcenter);
        select_ddlsearch_purchase(ddlsearch_typepurchase, "index");
        select_type_devices_its(ddlsearch_typequipment);
        select_node_status_its(ddlsearch_status);
        select_YearMasterIO(ddlsearch_yeario1);
        div_asset.Visible = false;
        pnlsearch_asset1.Visible = false;
        pnlsearch_asset2.Visible = true;
        pnl_seach_asset1.Visible = true;
        pnl_seach_asset2.Visible = false;

        if (ViewState["mode"] == null)
        {
            pnl_asset_report.Visible = false;
            pnl_asset_io_report.Visible = false;
        }
        else if (ViewState["mode"].ToString() == "asset_report")
        {
            if (ddlSearchReport.SelectedValue == "1")
            {
               // pnlsearch_asset2.Visible = false;
                pnl_asset_io_report.Visible = true;
                pnl_asset_report.Visible = false;
            }
            else
            {
               // pnlsearch_asset2.Visible = true;
                pnl_asset_io_report.Visible = false;
                pnl_print_plan.Visible = true;
            }

        }
        else
        {
            pnl_asset_report.Visible = false;
            pnl_asset_io_report.Visible = false;
        }
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }
    protected string action_select_organization(int _type, string _selectvalue)
    {

        //ddlsearch_organization.Items.Clear();

        if (_type == 0)
        {

            data_softwarelicense __data_softwarelicense = new data_softwarelicense();
            __data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_organization = new organization_detail();

            __data_softwarelicense.organization_list[0] = select_organization;
            __data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, __data_softwarelicense);

            ddlsearch_organization.DataSource = __data_softwarelicense.organization_list;
            ddlsearch_organization.DataTextField = "OrgNameTH";
            ddlsearch_organization.DataValueField = "OrgIDX";
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก organization", "0"));
            // ddlsearch_organization.SelectedValue = _selectvalue;

        }
        else if (_type == 1)
        {
            ddlsearch_organization.DataBind();
            ddlsearch_organization.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก organization", "0"));
        }

        return ddlsearch_organization.SelectedItem.Value;


    }

    protected string action_select_depertment(int _type, string _selectvalue)
    {

        ddlsearch_department.Items.Clear();

        if (_type == 0)
        {

            _data_softwarelicense.organization_list = new organization_detail[1];
            organization_detail select_depertment = new organization_detail();

            select_depertment.OrgIDX = Int32.Parse(ddlsearch_organization.SelectedValue);

            _data_softwarelicense.organization_list[0] = select_depertment;
            _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);

            ddlsearch_department.DataSource = _data_softwarelicense.organization_list;
            ddlsearch_department.DataTextField = "DeptNameTH";
            ddlsearch_department.DataValueField = "RDeptIDX";
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก depertment", "0"));

        }
        else if (_type == 1)
        {
            ddlsearch_department.DataBind();
            ddlsearch_department.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก depertment", "0"));
        }

        return ddlsearch_department.SelectedItem.Value;

    }

    protected void select_costcenter_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "costcenter_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือก costcenter", "0"));

    }

    protected void select_type_devices_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "type_devices_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกประเภทอุปกรณ์", "0"));

    }

    protected void select_node_status_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();

        //obj_lookup.idx = Int32.Parse(ViewState["rsec_idx"].ToString());
        obj_lookup.operation_status_id = "node_status_its";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        // litDebug.Text   _funcTool.convertObjectToJson(_dataemployee);

        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("กรุณาเลือกสถานะรายการ", "0"));

    }

    protected void SearchDataIndex()
    {
        // view View_its_u0_document
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_document = new its_u_document();

        //ค้นหาตามวันที่
        select_document.condition_date_type = _func_dmu.zStringToInt(ddlcondition.SelectedValue);
        if (txtstartdate.Text.Trim() != "")
        {
            select_document.start_date = _func_dmu.zDateToDB(txtstartdate.Text.Trim());
        }
        if (txtenddate.Text.Trim() != "")
        {
            select_document.end_date = _func_dmu.zDateToDB(txtenddate.Text.Trim());
        }

        select_document.doccode = txtdocumentcode.Text.Trim();
        select_document.emp_name_th = txtfirstname_lastname.Text.Trim();
        select_document.emp_code = txtempcode.Text.Trim();
        select_document.org_idx_its = _func_dmu.zStringToInt(ddlsearch_organization.SelectedValue);
        select_document.rdept_idx_its = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
        select_document.rdept_idx_search = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
        select_document.place_idx = _func_dmu.zStringToInt(ddllocation_search.SelectedValue);
        select_document.costidx = _func_dmu.zStringToInt(ddl_costcenter.SelectedValue);
        select_document.pr_type_idx = _func_dmu.zStringToInt(ddlsearch_typepurchase.SelectedValue);
        select_document.typidx = _func_dmu.zStringToInt(ddlsearch_typequipment.SelectedValue);
        select_document.doc_status = _func_dmu.zStringToInt(ddlsearch_status.SelectedValue);
        select_document.rdept_idx_admin = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());

        select_document.zstatus = zModeStatus();
        select_document.emp_idx = emp_idx;
        select_document.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        select_document.locidx = _func_dmu.zStringToInt(ViewState["locidx"].ToString());
        select_document.operation_status_id = "list_index";
        _dtitseet.its_u_document_action[0] = select_document;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

        ViewState["its_u0_document_list"] = _dtitseet.its_u_document_action;

        _func_dmu.zSetGridData(Gvits_u0_document_list, ViewState["its_u0_document_list"]);
        Gvits_u0_document_list.Visible = true;

        //if (_dtitseet.its_u_document_action == null)
        //{
        //    check_approve_all.Visible = false;
        //    btnapproveall.Visible = false;
        //    btnnotapprove.Visible = false;
        //}

    }
    protected void SearchAssetDataIndex()
    {
        // view View_its_u0_document
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_document = new its_u_document();

        select_document.operation_status_id = "asset_list_index";
        _dtitseet.its_u_document_action[0] = select_document;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);


        ViewState["assetits_u0_document_list"] = _dtitseet.its_u_document_action;

        _func_dmu.zSetGridData(GvAssetits_u0_document_list, ViewState["assetits_u0_document_list"]);

    }


    protected void action_select_its_Asset(string _sCommandArgument)
    {

        string[] argument = new string[9];
        argument = _sCommandArgument.ToString().Split('|');
        int u0idx = int.Parse(argument[0]);
        string nodidx = argument[1];
        int emp_idx_its = int.Parse(argument[2]);
        int pr_type_idx = int.Parse(argument[3]);
        int actor_idx = int.Parse(argument[4]);
        string documentCode = argument[5];
        string doc_status = argument[6];
        int flow_item = int.Parse(argument[7]);
        int place_idx_its = int.Parse(argument[8]);

        ViewState["u0idx"] = u0idx;
        ViewState["node_idx"] = nodidx;
        ViewState["emp_idx_its"] = emp_idx_its;
        ViewState["pr_type_idx"] = pr_type_idx;
        ViewState["actor_idx"] = actor_idx;
        ViewState["_DOCUMENTCODE"] = documentCode;
        ViewState["doccode"] = documentCode;
        ViewState["doc_status"] = doc_status;
        ViewState["place_idx_its"] = place_idx_its;
        ViewState["flow_item"] = flow_item;
        // ViewState["mode"] = "update_asset_no";


        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.flow_item = 4;
        select_its.operation_status_id = "permission_admin_dept";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        int rdept_idx = 0;

        if (_dtitseet.its_u_document_action != null)
        {
            rdept_idx = _dtitseet.its_u_document_action[0].rdept_idx;
        }


        if (((_func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()) == rdept_idx) && (rdept_idx > 0))
            ||
            (_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
            )
        {
            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = u0idx;
            select_its.operation_status_id = "list_u0";
            _dtitseet.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            FvDetailUser_Asset.DataSource = _dtitseet.its_u_document_action;
            FvDetailUser_Asset.DataBind();
            FvDetail_ShowAsset.DataSource = _dtitseet.its_u_document_action;
            FvDetail_ShowAsset.DataBind();


            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = u0idx;
            select_its.u1idx = 0;
            if ((ViewState["mode"].ToString() == "attach_flie_asset")
                ||
                (ViewState["mode"].ToString() == "attach_flie_asset_show")
                )
            {
                select_its.operation_status_id = "list_u2";
            }
            else
            {
                if (flow_item == 4)
                {
                    if (_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 21)
                    {
                        select_its.operation_status_id = "list_u2";
                    }
                    else
                    {
                        select_its.operation_status_id = "list_u1";
                    }

                }
                else
                {
                    select_its.operation_status_id = "list_u2";
                }
            }

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            CreateDsits_u1_document();
            GridView gvItemsIts_Asset = (GridView)FvDetail_ShowAsset.FindControl("gvItemsIts_Asset");
            gvItemsIts_Asset.Columns[9].Visible = false;
            if ((ViewState["mode"].ToString() == "attach_flie_asset")
                ||
                (ViewState["mode"].ToString() == "attach_flie_asset_show")
                )
            {
                gvItemsIts_Asset.Columns[9].Visible = true;
            }
            DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
            if (_dtitseet.its_u_document_action != null)
            {
                foreach (var vdr in _dtitseet.its_u_document_action)
                {
                    int ic = 0, icount = vdr.qty;
                    if (
                        (flow_item != 4)
                        ||
                        (_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 21)
                        ||
                        ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 8) &&
                        (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 4001))
                        )
                    {
                        icount = 1;
                    }
                    for (ic = 1; ic <= icount; ic++)
                    {
                        DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

                        drContacts["u1idx"] = vdr.u1idx;
                        drContacts["u0idx"] = vdr.u0idx;
                        drContacts["id"] = ic;

                        drContacts["sysidx"] = vdr.sysidx;
                        drContacts["sysidx_name"] = vdr.SysNameTH;

                        drContacts["typidx"] = vdr.typidx;
                        drContacts["typidx_name"] = vdr.type_name;

                        drContacts["tdidx"] = vdr.tdidx;
                        drContacts["tdidx_name"] = vdr.zdevices_name;

                        drContacts["pr_type_idx"] = vdr.pr_type_idx;
                        drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                        drContacts["ionum"] = vdr.ionum;
                        drContacts["ref_itnum"] = vdr.ref_itnum;

                        drContacts["rpos_idx"] = vdr.rpos_idx;
                        drContacts["rpos_idx_name"] = vdr.pos_name_th;

                        drContacts["list_menu"] = vdr.list_menu;

                        drContacts["price"] = vdr.price;

                        drContacts["currency_idx"] = vdr.currency_idx;
                        drContacts["currency_idx_name"] = vdr.currency_th;

                        drContacts["qty"] = 1;// vdr.qty;

                        drContacts["unidx"] = vdr.unidx;
                        drContacts["unidx_name"] = vdr.NameTH;

                        drContacts["costidx"] = vdr.costidx;
                        drContacts["costcenter_idx_name"] = vdr.CostNo;

                        drContacts["budget_set"] = vdr.budget_set;

                        drContacts["place_idx"] = vdr.place_idx;
                        drContacts["place_idx_name"] = vdr.place_name;

                        drContacts["spec_idx"] = 0;
                        drContacts["spec_idx_name"] = "";

                        drContacts["leveldevice"] = vdr.leveldevice;

                        drContacts["total_amount"] = vdr.total_amount;

                        drContacts["zdevices_name"] = vdr.zdevices_name;
                        drContacts["ionum"] = vdr.ionum;
                        drContacts["ref_itnum"] = vdr.ref_itnum;
                        drContacts["asset_no"] = "";
                        drContacts["u2idx"] = 0;
                        drContacts["flow_item"] = flow_item;

                        if (vdr.doccode == null)
                        {
                            drContacts["doccode"] = "";
                        }
                        else
                        {
                            drContacts["doccode"] = vdr.doccode;
                        }
                        if (((flow_item != 4)
                            ||
                            (_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 21))
                            ||
                            (ViewState["mode"].ToString() == "attach_flie_asset")
                            ||
                            ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 8) &&
                             (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 4001))
                            )
                        {
                            drContacts["asset_no"] = vdr.asset_no;
                            drContacts["u2idx"] = vdr.u2idx;
                        }


                        dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);
                    }
                }
            }

            ViewState["vsits_u1_document"] = dsContacts;

            setGridData(gvItemsIts_Asset, dsContacts.Tables["dsits_u1_document"]);

            setObject_asset();
            HiddenField hidden_actor_name = (HiddenField)FvDetail_ShowAsset.FindControl("hidden_name_actor");
            _Panel_body_approve = (Panel)FvDetail_ShowAsset.FindControl("Panel_body_approve");
            _lb_title_approve = (Label)FvDetail_ShowAsset.FindControl("lb_title_approve");
            _lbtn = (LinkButton)FvDetail_ShowAsset.FindControl("btnAdddata");

            _lbtn.Visible = false;
            _Panel_body_approve.Visible = false;

            if (//(flow_item == 4) && 
                (ViewState["mode"].ToString() == "asset"))
            {
                _lbtn.Visible = true;
                _Panel_body_approve.Visible = true;
                _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
            }
            else if ((//(flow_item == 5) && 
                (ViewState["mode"].ToString() == "asset_dir1"))
                ||
                (ViewState["mode"].ToString() == "asset_dir2")
                ||
                (ViewState["mode"].ToString() == "asset_dir3")
                ||
                (ViewState["mode"].ToString() == "asset_dir4")
                ||
                (ViewState["mode"].ToString() == "attach_flie_asset")
                )
            {
                _dtitseet.its_u_document_action = new its_u_document[1];
                select_its = new its_u_document();
                //select_its.emp_idx = emp_idx;
                select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());

                if (
                     ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 29) &&
                     (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 1))
                     ||
                     ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 19) &&
                     (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 12))
                    )
                {
                    select_its.place_idx = 0;
                    select_its.flow_item = 49;
                }
                else if ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 8) &&
                     (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 4001)
                    )
                {
                    select_its.place_idx = 0;
                    select_its.flow_item = 51;
                }
                else if ((_func_dmu.zStringToInt(ViewState["node_idx"].ToString()) == 8) &&
                     (_func_dmu.zStringToInt(ViewState["actor_idx"].ToString()) == 4002)
                    )
                {
                    select_its.place_idx = 0;
                    select_its.flow_item = 52;
                }
                else
                {
                    select_its.place_idx = place_idx_its;
                    select_its.flow_item = 5;
                }


                select_its.operation_status_id = "permission_admin_select";
                _dtitseet.its_u_document_action[0] = select_its;
                //litDebug.Text = ViewState["rdept_idx"].ToString() + "org_idx " +
                //                   ViewState["org_idx"].ToString() + "rsec_idx " +
                //                   ViewState["rsec_idx"].ToString() + "rpos_idx " +
                //                   ViewState["rpos_idx"].ToString() + "place_idx_its " +
                //                   place_idx_its.ToString()+ " node_idx " +
                //                   ViewState["node_idx"].ToString() + " actor_idx " +
                //                   ViewState["actor_idx"].ToString()
                //                   ;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                if (_dtitseet.its_u_document_action != null)
                {
                    var rowdt = _dtitseet.its_u_document_action[0];

                    if (
                       (rowdt.rdept_idx
                       ==
                       _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString())
                       )
                       &&
                       (rowdt.org_idx
                       ==
                       _func_dmu.zStringToInt(ViewState["org_idx"].ToString())
                       )
                       &&
                       (rowdt.rsec_idx
                       ==
                       _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString())
                       )
                       &&
                       (rowdt.rpos_idx
                       ==
                       _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString())
                       )
                       )
                    {
                        _lbtn.Visible = true;
                        _Panel_body_approve.Visible = true;
                        _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                    }
                }
                if ((_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                    &&
                     (ViewState["mode"].ToString() != "P")
                     &&
                     (ViewState["mode"].ToString() != "I")
                     &&
                     (ViewState["mode"].ToString() != "E")
                    )
                {
                    _lbtn.Visible = true;
                    _Panel_body_approve.Visible = true;
                    _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                }
                if (ViewState["mode"].ToString() == "attach_flie_asset")
                {
                    _Panel_body_approve.Visible = false;
                }

            }
            try
            {

                string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                GridView gvFile_Asset = (GridView)FvDetail_ShowAsset.FindControl("gvFile_Asset");
                SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFile_Asset);

            }
            catch
            {

            }

            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            select_its.idx = u0idx;
            select_its.operation_status_id = "list_l0";

            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            _Repeater = (Repeater)FvDetail_ShowAsset.FindControl("rpt_history");
            _Repeater.DataSource = _dtitseet.its_u_document_action;
            _Repeater.DataBind();


        }
        else
        {
            MvMaster.SetActiveView(ViewIndex);
            ShowDataIndex();
        }


    }
    private void setObject_asset()
    {
        _txtremark = (TextBox)FvDetail_ShowAsset.FindControl("txt_asset_remark");
        _GridView = (GridView)FvDetail_ShowAsset.FindControl("gvItemsIts_Asset");
        _ddlStatusapprove = (DropDownList)FvDetail_ShowAsset.FindControl("ddlStatusapprove");
        _Panel_body_approve = (Panel)FvDetail_ShowAsset.FindControl("Panel_body_approve");
        _lb_title_approve = (Label)FvDetail_ShowAsset.FindControl("lb_title_approve");
        _txt_hidden_sysidx = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_sysidx");
        _txt_hidden_actor_idx = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_actor_idx");
        _txt_hidden_node_idx = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_node_idx");

    }
    private Boolean checkErrorInsert_asset()
    {
        Boolean _Boolean = false;
        setObject_asset();
        if (_txtremark.Text.Trim() == "")
        {
            //  showMsAlert( "ความคิดเห็นเพิ่มเติม.!");
            _Boolean = true;
        }
        else if ((_GridView.Rows.Count == 0) && (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) != 6))
        {
            showMsAlert("ไม่มีรายการขอซื้อ.!");
            _Boolean = true;
        }
        else
        {
            int irow = 0;
            int iErrer = 0;
            if ((_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) != 6) &&
                (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) != 12)
                )
            {
                foreach (var item in _GridView.Rows)
                {

                    TextBox txtasset_no = (TextBox)_GridView.Rows[irow].FindControl("txtasset_no");
                    if (txtasset_no.Text == null)
                    {
                        iErrer++;
                    }
                    else if (txtasset_no.Text.Trim() == "")
                    {
                        iErrer++;
                    }
                    if (iErrer > 0)
                    {
                        _Boolean = true;
                        showMsAlert("กรุณากรอกเลข Asset No.!");
                        txtasset_no.Focus();
                        break;
                    }
                    irow++;
                }
                if (iErrer == 0)
                {
                    irow = 0;
                    GridView _GridView1 = (GridView)FvDetail_ShowAsset.FindControl("gvItemsIts_Asset");
                    foreach (var item in _GridView.Rows)
                    {
                        TextBox txtasset_no = (TextBox)_GridView.Rows[irow].FindControl("txtasset_no");

                        for (int irow1 = irow + 1; irow1 < _GridView.Rows.Count; irow1++)
                        {
                            TextBox txtasset_no1 = (TextBox)_GridView1.Rows[irow1].FindControl("txtasset_no");
                            if (txtasset_no.Text.Trim().ToLower() == txtasset_no1.Text.Trim().ToLower())
                            {
                                _Boolean = true;
                                showMsAlert("มีเลข Asset No : " + txtasset_no1.Text + " นี้แล้ว กรุณากรอกเใหม่!");
                                txtasset_no1.Focus();
                                break;
                            }
                        }

                        irow++;
                    }
                }
            }

        }

        return _Boolean;
    }
    public void showMsAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }


    private Boolean zSaveUpdate_asset(int id, int node_idx, int actor_idx)
    {

        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "";
        setObject_asset();

        if (ViewState["mode"].ToString() == "attach_flie_asset")
        {
            if (
                ((node_idx == 29) && (actor_idx == 1))
                ||
                ((node_idx == 19) && (actor_idx == 12))
                )
            {
                data_itasset dataitasset = new data_itasset();
                dataitasset.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                select_its.u0idx = id;
                //detail
                select_its.asset_dir_emp_idx = emp_idx;
                select_its.asset_dir_approve_remark = _txtremark.Text;
                select_its.asset_dir_approve_status = 5;

                select_its.approve_status = 5;
                select_its.node_idx = 4004;
                select_its.actor_idx = 10;
                select_its.sysidx = _func_dmu.zStringToInt(_txt_hidden_sysidx.Text);

                select_its.operation_status_id = "approve_dir_asset_5";
                dataitasset.its_u_document_action[0] = select_its;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);


            }
        }
        else
        {
            if (//(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 4)
                (node_idx == 17) && (actor_idx == 10) ||
                (node_idx == 21) && (actor_idx == 10)
            )
            {
                data_itasset dataitasset = new data_itasset();
                int itemObj = 0;
                dataitasset = new data_itasset();
                //******************  start its_document_assetno_u2 *********************//
                itemObj = 0;
                CreateDsits_u1_document();
                DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                its_u_document[] objcourse1 = new its_u_document[_GridView.Rows.Count];

                foreach (var item in _GridView.Rows)
                {
                    Label _lbu0idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu0idx");
                    Label _lbu1idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu1idx");
                    Label _lbu2idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu2idx");
                    Label _lbid = (Label)_GridView.Rows[itemObj].FindControl("_lbid");
                    TextBox txtasset_no = (TextBox)_GridView.Rows[itemObj].FindControl("txtasset_no");

                    objcourse1[itemObj] = new its_u_document();

                    //detail
                    objcourse1[itemObj].asset_emp_idx = emp_idx;
                    objcourse1[itemObj].asset_remark = _txtremark.Text;
                    objcourse1[itemObj].asset_approve_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);

                    objcourse1[itemObj].approve_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                    objcourse1[itemObj].node_idx = _func_dmu.zStringToInt(_txt_hidden_node_idx.Text);
                    objcourse1[itemObj].actor_idx = _func_dmu.zStringToInt(_txt_hidden_actor_idx.Text);

                    if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                    {
                        objcourse1[itemObj].flow_item = 5;
                    }
                    else //กลับไปแก้ไข
                    {
                        objcourse1[itemObj].flow_item = 1; //กับไปแก้ไข
                    }
                    objcourse1[itemObj].doc_status = 1; //ดำเนินการ

                    objcourse1[itemObj].u2idx = _func_dmu.zStringToInt(_lbu2idx.Text);
                    objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                    objcourse1[itemObj].u0idx = _func_dmu.zStringToInt(_lbu0idx.Text);
                    objcourse1[itemObj].asset_item = _func_dmu.zStringToInt(_lbid.Text);
                    objcourse1[itemObj].asset_no = txtasset_no.Text;

                    objcourse1[itemObj].u2_status = 1;
                    objcourse1[itemObj].CEmpIDX = emp_idx;
                    objcourse1[itemObj].UEmpIDX = emp_idx;
                    objcourse1[itemObj].operation_status_id = "U2";
                    itemObj++;

                }

                dataitasset.its_u_document_action = objcourse1;
                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                callServicePostITAsset(_urlSetInsits_u_document, dataitasset);

                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                {
                    settemplate_asset_create(id, 5, 0);
                }
                else //กลับไปแก้ไข
                {
                    settemplate_asset_approve(id, 4);
                }

                //******************  end its_document_assetno_u2 *********************//
            }
            else if (//(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 5)
                ((node_idx == 8) && (actor_idx == 11))
                ||
                ((node_idx == 8) && (actor_idx == 4000))
                ||
                ((node_idx == 8) && (actor_idx == 4001))
                ||
                ((node_idx == 8) && (actor_idx == 4002))
                )
            {
                data_itasset dataitasset = new data_itasset();
                dataitasset.its_u_document_action = new its_u_document[1];
                its_u_document select_its = new its_u_document();
                select_its.u0idx = id;
                //detail
                select_its.asset_dir_emp_idx = emp_idx;
                select_its.asset_dir_approve_remark = _txtremark.Text;
                select_its.asset_dir_approve_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);

                select_its.approve_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                select_its.node_idx = _func_dmu.zStringToInt(_txt_hidden_node_idx.Text);
                select_its.actor_idx = _func_dmu.zStringToInt(_txt_hidden_actor_idx.Text);
                select_its.sysidx = _func_dmu.zStringToInt(_txt_hidden_sysidx.Text);

                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                {
                    // select_its.flow_item = 6;
                }
                else if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 12) //กลับไปแก้ไขเลขที่ Asset
                {
                    // select_its.flow_item = 4;
                }
                else //กลับไปแก้ไข
                {
                    select_its.sysidx = 0;
                    //  select_its.flow_item = 1; //กับไปแก้ไข
                }

                select_its.operation_status_id = "approve_dir_asset_5";
                dataitasset.its_u_document_action[0] = select_its;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
                TextBox txt_hidden_place_idx = (TextBox)FvDetail_ShowAsset.FindControl("txt_hidden_place_idx");

                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                {

                    //manager 
                    if ((node_idx == 8) && (actor_idx == 11))
                    {
                        if (txt_hidden_place_idx.Text == "6")
                        {
                            settemplate_asset_create(id, 50, 0);
                        }
                        else
                        {
                            settemplate_asset_create(id, 51, 0);
                        }

                    }
                    //Senior manager Tobi
                    else if ((node_idx == 8) && (actor_idx == 4000))
                    {
                        settemplate_asset_create(id, 52, 0);
                    }
                    //Director
                    else if ((node_idx == 8) && (actor_idx == 4001))
                    {
                        settemplate_asset_create(id, 52, 0);
                    }
                    //CFO
                    else if ((node_idx == 8) && (actor_idx == 4002))
                    {
                        settemplate_asset_approve(id, 5); //ส่งแจ้ง user ว่าถูกส่งให้ it แล้ว
                        if ((_txt_hidden_sysidx.Text != "9") && (_txt_hidden_sysidx.Text != "37")) //HR,OTHER
                        {
                            settemplate_asset_create(id, 6, _func_dmu.zStringToInt(_txt_hidden_sysidx.Text));
                        }
                    }

                }
                else if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 12) //กลับไปแก้ไขเลขที่ Asset
                {
                    settemplate_asset_create(id, 4, 0, "edit_assetno");
                }
                else //กลับไปแก้ไข
                {
                    settemplate_asset_approve(id, 5);
                }


            }

        }
        _Boolean = true;

        return _Boolean;
    }

    //it

    private void setObject_it()
    {
        _txtremark = (TextBox)FvDetail_ShowIT.FindControl("txt_remark");
        _GridView = (GridView)FvDetail_ShowIT.FindControl("gvItemsIts_IT");
        _ddlStatusapprove = (DropDownList)FvDetail_ShowIT.FindControl("ddlStatusapprove");
        _Panel_body_approve = (Panel)FvDetail_ShowIT.FindControl("Panel_body_approve");
        _lb_title_approve = (Label)FvDetail_ShowIT.FindControl("lb_title_approve");
        _txt_hidden_sysidx = (TextBox)FvDetail_ShowIT.FindControl("txt_hidden_sysidx");
        _txt_hidden_node_idx = (TextBox)FvDetail_ShowIT.FindControl("txt_hidden_node_idx");
        _txt_hidden_actor_idx = (TextBox)FvDetail_ShowIT.FindControl("txt_hidden_actor_idx");
    }

    protected void action_select_its_IT(string _sCommandArgument)
    {

        string[] argument = new string[9];
        argument = _sCommandArgument.ToString().Split('|');
        int u0idx = int.Parse(argument[0]);
        string nodidx = argument[1];
        int emp_idx_its = int.Parse(argument[2]);
        int pr_type_idx = int.Parse(argument[3]);
        int actor_idx = int.Parse(argument[4]);
        string documentCode = argument[5];
        string doc_status = argument[6];
        int flow_item = int.Parse(argument[7]);
        int place_idx_its = int.Parse(argument[8]);

        ViewState["u0idx"] = u0idx;
        ViewState["node_idx"] = nodidx;
        ViewState["emp_idx_its"] = emp_idx_its;
        ViewState["pr_type_idx"] = pr_type_idx;
        ViewState["actor_idx"] = actor_idx;
        ViewState["_DOCUMENTCODE"] = documentCode;
        ViewState["doccode"] = documentCode;
        ViewState["doc_status"] = doc_status;
        ViewState["place_idx_its"] = place_idx_its;
        ViewState["flow_item"] = flow_item;
        int ierror = 0;
        string sMode = ViewState["mode"].ToString();
        if (
            (sMode == "it") ||
            (sMode == "it_update") ||
            (sMode == "it-manager") ||
            (sMode == "it-dir") ||
            (sMode == "pur")
            )
        {
            _dtitseet.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();
            select_its.idx = u0idx;
            select_its.operation_status_id = "it_list_u0";
            _dtitseet.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            FvDetailUser_IT.DataSource = _dtitseet.its_u_document_action;
            FvDetailUser_IT.DataBind();
            FvDetail_ShowIT.DataSource = _dtitseet.its_u_document_action;
            FvDetail_ShowIT.DataBind();

            if (_dtitseet.its_u_document_action != null)
            {
                _dtitseet.its_u_document_action = new its_u_document[1];
                select_its = new its_u_document();
                select_its.idx = u0idx;
                select_its.operation_status_id = "list_u1";
                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                CreateDsits_u1_document();
                GridView gvItemsIts_IT = (GridView)FvDetail_ShowIT.FindControl("gvItemsIts_IT");
                gvItemsIts_IT.Columns[9].Visible = true;

                DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
                if (_dtitseet.its_u_document_action != null)
                {
                    foreach (var vdr in _dtitseet.its_u_document_action)
                    {
                        int ic = 0, icount = vdr.qty;

                        DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

                        drContacts["u1idx"] = vdr.u1idx;
                        drContacts["u0idx"] = vdr.u0idx;
                        drContacts["id"] = ic;

                        drContacts["sysidx"] = vdr.sysidx;
                        drContacts["sysidx_name"] = vdr.SysNameTH;

                        drContacts["typidx"] = vdr.typidx;
                        drContacts["typidx_name"] = vdr.type_name;

                        drContacts["tdidx"] = vdr.tdidx;
                        drContacts["tdidx_name"] = vdr.zdevices_name;

                        drContacts["pr_type_idx"] = vdr.pr_type_idx;
                        drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                        drContacts["ionum"] = vdr.ionum;
                        drContacts["ref_itnum"] = vdr.ref_itnum;

                        drContacts["rpos_idx"] = vdr.rpos_idx;
                        drContacts["rpos_idx_name"] = vdr.pos_name_th;

                        drContacts["list_menu"] = vdr.list_menu;

                        drContacts["price"] = vdr.price;

                        drContacts["currency_idx"] = vdr.currency_idx;
                        drContacts["currency_idx_name"] = vdr.currency_th;

                        drContacts["qty"] = vdr.qty;

                        drContacts["unidx"] = vdr.unidx;
                        drContacts["unidx_name"] = vdr.NameTH;

                        drContacts["costidx"] = vdr.costidx;
                        drContacts["costcenter_idx_name"] = vdr.CostNo;

                        drContacts["budget_set"] = vdr.budget_set;

                        drContacts["place_idx"] = vdr.place_idx;
                        drContacts["place_idx_name"] = vdr.place_name;

                        drContacts["spec_idx"] = 0;
                        drContacts["spec_idx_name"] = "";

                        drContacts["leveldevice"] = vdr.leveldevice;

                        drContacts["total_amount"] = vdr.total_amount;

                        drContacts["zdevices_name"] = vdr.zdevices_name;
                        drContacts["ionum"] = vdr.ionum;
                        drContacts["ref_itnum"] = vdr.ref_itnum;
                        drContacts["asset_no"] = vdr.asset_no;
                        drContacts["u2idx"] = vdr.u2idx;
                        drContacts["flow_item"] = flow_item;
                        drContacts["spec_name"] = vdr.spec_name;
                        drContacts["spec_remark"] = vdr.spec_remark;

                        dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);

                    }
                }

                ViewState["vsits_u1_document"] = dsContacts;

                setGridData(gvItemsIts_IT, dsContacts.Tables["dsits_u1_document"]);

                setObject_it();
                HiddenField hidden_actor_name = (HiddenField)FvDetail_ShowIT.FindControl("hidden_name_actor");
                _Panel_body_approve = (Panel)FvDetail_ShowIT.FindControl("Panel_body_approve");
                _lb_title_approve = (Label)FvDetail_ShowIT.FindControl("lb_title_approve");
                _lbtn = (LinkButton)FvDetail_ShowIT.FindControl("btnAdddata");

                _lbtn.Visible = false;
                _Panel_body_approve.Visible = false;
                if ((sMode == "it"))
                {
                    if ((flow_item == 6)) //6 เจ้าหน้าที่ it support
                    {
                        _dtitseet.its_u_document_action = new its_u_document[1];
                        select_its = new its_u_document();
                        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                        select_its.flow_item = flow_item;
                        select_its.operation_status_id = "permission_admin_sec";
                        _dtitseet.its_u_document_action[0] = select_its;
                        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                        if (_dtitseet.its_u_document_action != null)
                        {
                            if (_dtitseet.its_u_document_action[0].rsec_idx > 0)
                            {
                                _lbtn.Visible = true;
                                _Panel_body_approve.Visible = true;
                                _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                            }
                        }
                    }
                }
                else if ((sMode == "it_update"))
                {
                    if ((flow_item == 7)) //7 รอพี่เกมส์อนุมัติ
                    {
                        _dtitseet.its_u_document_action = new its_u_document[1];
                        select_its = new its_u_document();
                        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                        select_its.flow_item = 6;
                        select_its.operation_status_id = "permission_admin_sec";
                        _dtitseet.its_u_document_action[0] = select_its;
                        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                        if (_dtitseet.its_u_document_action != null)
                        {
                            if (_dtitseet.its_u_document_action[0].rsec_idx > 0)
                            {
                                _lbtn.Visible = true;
                                _Panel_body_approve.Visible = true;

                                _dtitseet.its_u_document_action = new its_u_document[1];
                                select_its = new its_u_document();
                                select_its.u0_idx = 8;
                                select_its.operation_status_id = "v_its_u0_node";
                                _dtitseet.its_u_document_action[0] = select_its;
                                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                                if (_dtitseet.its_u_document_action != null)
                                {
                                    foreach (var item in _dtitseet.its_u_document_action)
                                    {
                                        _lb_title_approve.Text = "โดย&nbsp;" + item.to_actor_name;
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        _dtitseet.its_u_document_action = new its_u_document[1];
                        select_its = new its_u_document();
                        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                        select_its.flow_item = 6;
                        select_its.operation_status_id = "permission_admin_sec";
                        _dtitseet.its_u_document_action[0] = select_its;
                        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                        if (_dtitseet.its_u_document_action != null)
                        {
                            if (_dtitseet.its_u_document_action[0].rsec_idx > 0)
                            {
                                _ddlStatusapprove.SelectedValue = "5";
                                _txtremark.Text = "แก้ไข";
                                // _Panel_body_approve.Visible = true;
                                _lbtn.Visible = true;
                            }
                        }
                    }

                }
                else if (sMode == "it-manager") //พี่ เกมส์
                {
                    if (flow_item == 7)
                    {   //30072020
                        _dtitseet.its_u_document_action = new its_u_document[1];
                        select_its = new its_u_document();
                        select_its.emp_idx = emp_idx;
                        select_its.flow_item = flow_item;
                        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                        select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
                        select_its.operation_status_id = "permission_admin_select";

                        _dtitseet.its_u_document_action[0] = select_its;
                        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                        if (_dtitseet.its_u_document_action != null)
                        {
                            int icount = 0;
                            foreach (var item in _dtitseet.its_u_document_action)
                            {
                                icount++;
                            }
                            if (icount > 0)
                            {
                                _lbtn.Visible = true;
                                _Panel_body_approve.Visible = true;
                                _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                            }
                        }
                    }
                }
                else if (sMode == "it-dir") //พี่ พน
                {
                    if (flow_item == 8)
                    {
                        if (_txt_hidden_sysidx.Text == "3") //it
                        {
                            _dtitseet.its_u_document_action = new its_u_document[1];
                            select_its = new its_u_document();
                            // select_its.emp_idx = emp_idx;
                            select_its.flow_item = flow_item;
                            select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                            select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                            select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                            select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());

                            select_its.operation_status_id = "permission_admin_select";
                            _dtitseet.its_u_document_action[0] = select_its;
                            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                            if (_dtitseet.its_u_document_action != null)
                            {
                                int icount = 0;
                                foreach (var item in _dtitseet.its_u_document_action)
                                {
                                    icount++;
                                }
                                if (icount > 0)
                                {
                                    _lbtn.Visible = true;
                                    _Panel_body_approve.Visible = true;
                                    _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                                }
                            }
                        }
                        else
                        {
                            int icount = 0;
                            TextBox txt_rdept_idx_its = (TextBox)FvDetail_ShowIT.FindControl("txt_rdept_idx_its");

                            setjobgrade_director(_func_dmu.zStringToInt(txt_rdept_idx_its.Text));

                            if (
                                (_func_dmu.zStringToInt(ViewState["set_jobgrade_level_dir"].ToString())
                                >= 10
                                 )
                                 &&
                                 (_func_dmu.zStringToInt(ViewState["set_rdept_idx_dir"].ToString())
                                  ==
                                 _func_dmu.zStringToInt(txt_rdept_idx_its.Text)
                                 )
                                )
                            {
                                icount++;

                            }
                            /*
                            else if (
                                (_func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString())
                                ==
                                _func_dmu.zStringToInt(ViewState["jobgrade_level"].ToString())
                                )
                                &&
                                (_func_dmu.zStringToInt(ViewState["rdept_idx_dir"].ToString())
                                ==
                                _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString())
                                )
                                )
                            {
                                icount++;

                            }*/

                            if (icount > 0)
                            {
                                _lbtn.Visible = true;
                                _Panel_body_approve.Visible = true;
                                _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                            }

                        }
                    }
                }
                else if ((sMode == "pur"))
                {
                    if ((flow_item == 6)) //6 เจ้าหน้าที่ จัดซื้อ
                    {
                        GridView gvItemsIts_IT1 = (GridView)FvDetail_ShowIT.FindControl("gvItemsIts_IT");
                        gvItemsIts_IT1.Columns[9].Visible = false;
                        _dtitseet.its_u_document_action = new its_u_document[1];
                        select_its = new its_u_document();
                        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
                        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
                        select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
                        select_its.flow_item = flow_item;
                        select_its.operation_status_id = "permission_admin_dept_pur";
                        _dtitseet.its_u_document_action[0] = select_its;
                        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
                        if (_dtitseet.its_u_document_action != null)
                        {
                            if (_dtitseet.its_u_document_action[0].rsec_idx > 0)
                            {
                                _lbtn.Visible = true;
                                _Panel_body_approve.Visible = true;
                                _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                            }
                        }
                    }
                }

                if ((_func_dmu.zStringToInt(ViewState["EmpIDX"].ToString()) == it_admin)
                    &&
                     (ViewState["mode"].ToString() != "P")
                     &&
                     (ViewState["mode"].ToString() != "I")
                     &&
                     (ViewState["mode"].ToString() != "E")
                    )
                {
                    _lbtn.Visible = true;
                    _Panel_body_approve.Visible = true;
                    _lb_title_approve.Text = "โดย&nbsp;" + hidden_actor_name.Value;
                }

                setGridData(gvItemsIts_IT, dsContacts.Tables["dsits_u1_document"]);
                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["pathfile_itasset"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["_DOCUMENTCODE"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                    GridView gvFile_IT = (GridView)FvDetail_ShowIT.FindControl("gvFile_IT");
                    SearchDirectories_foreditFile(myDirLotus, ViewState["_DOCUMENTCODE"].ToString(), "", gvFile_IT);

                }
                catch
                {

                }

                _dtitseet.its_u_document_action = new its_u_document[1];
                select_its = new its_u_document();
                select_its.idx = u0idx;
                select_its.operation_status_id = "list_l0";

                _dtitseet.its_u_document_action[0] = select_its;
                _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

                _Repeater = (Repeater)FvDetail_ShowIT.FindControl("rpt_history");
                _Repeater.DataSource = _dtitseet.its_u_document_action;
                _Repeater.DataBind();
            }
            else
            {
                ierror++;
            }

        }
        else
        {
            ierror++;
        }
        if (ierror > 0)
        {
            if (ViewState["mode"].ToString() == "it")
            {
                setActiveTab("it");
            }
            else
            {
                setActiveTab("it_update");
            }

            MvMaster.SetActiveView(ViewIndex);
            ShowDataIndex();
        }

    }
    private Boolean checkErrorInsert_it()
    {
        Boolean _Boolean = false;
        setObject_it();
        if (_txtremark.Text.Trim() == "")
        {
            _Boolean = true;
        }
        else if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 0)
        {
            _Boolean = true;
        }
        else if ((_GridView.Rows.Count == 0) && (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) != 6))
        {
            showMsAlert("ไม่มีรายการขอซื้อ.!");
            _Boolean = true;
        }
        else if ((ViewState["mode"].ToString() == "it") ||
                 (ViewState["mode"].ToString() == "it_update")
                 )
        {
            int irow = 0;
            int iErrer = 0;
            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) != 6)
            {
                foreach (var item in _GridView.Rows)
                {

                    //DropDownList ddlspec_idx = (DropDownList)_GridView.Rows[irow].FindControl("ddlspec_idx");
                    TextBox txt_spec_remark = (TextBox)_GridView.Rows[irow].FindControl("txt_spec_remark");
                    if (txt_spec_remark.Text.Trim() == "")
                    {
                        iErrer++;
                    }
                    if (iErrer > 0)
                    {
                        _Boolean = true;
                        showMsAlert("กรุณาเลือก Spec.!");
                        txt_spec_remark.Focus();
                        break;
                    }
                    irow++;
                }
            }

        }

        return _Boolean;
    }
    private Boolean zSaveUpdate_it(int id)
    {

        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "";
        setObject_it();

        int node_idx = _func_dmu.zStringToInt(_txt_hidden_node_idx.Text);
        int actor_idx = _func_dmu.zStringToInt(_txt_hidden_actor_idx.Text);

        if (//(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 6) &&
            (ViewState["mode"].ToString() == "it") &&
            (node_idx == 30)

            ) // เจ้าหน้าที่ it support
        {
            data_itasset dataitasset = new data_itasset();
            //******************  start its_document *********************//
            int itemObj = 0;
            CreateDsits_u1_document();
            DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
            its_u_document[] objcourse1 = new its_u_document[_GridView.Rows.Count];

            foreach (var item in _GridView.Rows)
            {
                Label _lbu0idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu0idx");
                Label _lbu1idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu1idx");
                Label _lbid = (Label)_GridView.Rows[itemObj].FindControl("_lbid");
                TextBox txt_spec_remark = (TextBox)_GridView.Rows[itemObj].FindControl("txt_spec_remark");
                DropDownList ddlspec_idx = (DropDownList)_GridView.Rows[itemObj].FindControl("ddlspec_idx");

                objcourse1[itemObj] = new its_u_document();

                //detail
                objcourse1[itemObj].itsp_emp_idx = emp_idx;
                objcourse1[itemObj].itsp_remark = _txtremark.Text;
                objcourse1[itemObj].itsp_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                {
                    //objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 10, 910, 1110, 3610);//10
                    objcourse1[itemObj].flow_item = 7;
                }
                else //กลับไปแก้ไข
                {
                    //objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 11, 911, 1111, 3611); //11
                    objcourse1[itemObj].flow_item = 1; //กับไปแก้ไข
                }
                objcourse1[itemObj].doc_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                objcourse1[itemObj].node_idx = node_idx;
                objcourse1[itemObj].actor_idx = actor_idx;
                objcourse1[itemObj].spec_idx = _func_dmu.zStringToInt(ddlspec_idx.SelectedValue);
                objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                objcourse1[itemObj].u0idx = id;// _func_dmu.zStringToInt(_lbu0idx.Text);
                objcourse1[itemObj].UEmpIDX = emp_idx;
                objcourse1[itemObj].spec_remark = txt_spec_remark.Text.Trim();
                objcourse1[itemObj].operation_status_id = "approve_it_6";
                itemObj++;

            }

            dataitasset.its_u_document_action = objcourse1;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
            //******************  end its_document *********************//

            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_create(id, 7, _func_dmu.zStringToInt(_txt_hidden_sysidx.Text));
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 6);
            }

            setActiveTab("it");
        }
        else if ((_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 6) &&
                  (ViewState["mode"].ToString() == "pur")) // เจ้าหน้าที่ จัดซื้อ
        {
            data_itasset dataitasset = new data_itasset();
            dataitasset.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();
            select_its.u0idx = id;
            //detail
            select_its.itsp_emp_idx = emp_idx;
            select_its.itsp_remark = _txtremark.Text;
            select_its.itsp_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);

            select_its.sysidx = _func_dmu.zStringToInt(_txt_hidden_sysidx.Text);
            select_its.actor_idx = _func_dmu.zStringToInt(_txt_hidden_actor_idx.Text);
            select_its.node_idx = _func_dmu.zStringToInt(_txt_hidden_node_idx.Text);
            select_its.approve_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);

            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                select_its.flow_item = 10;
            }
            else //กลับไปแก้ไข
            {
                select_its.flow_item = 1; //กับไปแก้ไข
            }
            select_its.doc_status = 1; //ดำเนินการ

            select_its.operation_status_id = "approve_pur_mg_6";
            dataitasset.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_approve(id, 10);
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 7);
            }

            setActiveTab("pur");
        }
        else if ( //(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 7) &&
                  (ViewState["mode"].ToString() == "it-manager") &&
                  (node_idx == 15)

                  ) // เจ้าหน้าที่ it manager
        {
            data_itasset dataitasset = new data_itasset();
            dataitasset.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();
            select_its.u0idx = id;
            //detail
            select_its.itsp_mg_emp_idx = emp_idx;
            select_its.itsp_mg_remark = _txtremark.Text;
            select_its.itsp_mg_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                //  select_its.u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 12, 912, 1112, 3612); //12
                select_its.flow_item = 8;
            }
            else //กลับไปแก้ไข
            {
                // select_its.u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 13, 913, 1113, 3613);//13
                select_its.flow_item = 1; //กับไปแก้ไข
            }
            select_its.doc_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
            select_its.node_idx = node_idx;
            select_its.actor_idx = actor_idx;

            select_its.operation_status_id = "approve_it_mg_7";
            dataitasset.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_create(id, 8, _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()));
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 7);
            }

            setActiveTab("it-manager");
        }
        else if ( //(_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 8) &&
                  (ViewState["mode"].ToString() == "it-dir") &&
                  (node_idx == 12)

                  ) // เจ้าหน้าที่ it manager
        {
            data_itasset dataitasset = new data_itasset();
            dataitasset.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();
            select_its.u0idx = id;
            //detail
            select_its.itsp_dir_emp_idx = emp_idx;
            select_its.itsp_dir_remark = _txtremark.Text;
            select_its.itsp_dir_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                //select_its.u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 14, 914, 1114, 3614);//14
                select_its.flow_item = 9;
            }
            else //กลับไปแก้ไข
            {
                //select_its.u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 15, 915, 1115, 3615);//15
                select_its.flow_item = 1; //กับไปแก้ไข
            }
            select_its.doc_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
            select_its.node_idx = node_idx;
            select_its.actor_idx = actor_idx;

            select_its.operation_status_id = "approve_it_dir_8";
            dataitasset.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                settemplate_asset_create(id, 9, _func_dmu.zStringToInt(_txt_hidden_sysidx.Text));
            }
            else //กลับไปแก้ไข
            {
                settemplate_asset_approve(id, 8);
            }

            setActiveTab("it-dir");
        }

        MvMaster.SetActiveView(ViewIndex);
        ShowDataIndex();

        _Boolean = true;

        return _Boolean;
    }

    private Boolean zSaveUpdate_it_update(int id)
    {

        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "";
        setObject_it();
        int node_idx = _func_dmu.zStringToInt(_txt_hidden_node_idx.Text);
        int actor_idx = _func_dmu.zStringToInt(_txt_hidden_actor_idx.Text);

        if ((ViewState["mode"].ToString() == "it_update")) // เจ้าหน้าที่ it support
        {
            data_itasset dataitasset = new data_itasset();
            //******************  start its_document *********************//
            int itemObj = 0;
            CreateDsits_u1_document();
            DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
            its_u_document[] objcourse1 = new its_u_document[_GridView.Rows.Count];

            foreach (var item in _GridView.Rows)
            {
                Label _lbu0idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu0idx");
                Label _lbu1idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu1idx");
                Label _lbid = (Label)_GridView.Rows[itemObj].FindControl("_lbid");
                TextBox txt_spec_remark = (TextBox)_GridView.Rows[itemObj].FindControl("txt_spec_remark");
                DropDownList ddlspec_idx = (DropDownList)_GridView.Rows[itemObj].FindControl("ddlspec_idx");

                objcourse1[itemObj] = new its_u_document();

                //detail
                objcourse1[itemObj].itsp_emp_idx = emp_idx;
                objcourse1[itemObj].itsp_remark = _txtremark.Text;
                objcourse1[itemObj].itsp_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
                {
                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 29, 929, 1129, 3629); //29

                    objcourse1[itemObj].flow_item = 7;
                }
                else //กลับไปแก้ไข
                {
                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(_txt_hidden_sysidx.Text), 11, 911, 1111, 3611);//11
                    objcourse1[itemObj].flow_item = 1; //กับไปแก้ไข
                }

                objcourse1[itemObj].node_idx = node_idx;
                objcourse1[itemObj].actor_idx = actor_idx;
                objcourse1[itemObj].spec_idx = _func_dmu.zStringToInt(ddlspec_idx.SelectedValue);
                objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                objcourse1[itemObj].u0idx = id;// _func_dmu.zStringToInt(_lbu0idx.Text);
                objcourse1[itemObj].UEmpIDX = emp_idx;
                objcourse1[itemObj].spec_remark = txt_spec_remark.Text.Trim();
                objcourse1[itemObj].operation_status_id = "approve_it_6_update";
                itemObj++;

            }

            dataitasset.its_u_document_action = objcourse1;
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
            //******************  end its_document *********************//

            setActiveTab("it_update");
        }


        MvMaster.SetActiveView(ViewIndex);
        ShowDataIndex();

        _Boolean = true;

        return _Boolean;
    }

    protected void select_spec_its(DropDownList ddlName)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.operation_status_id = "spec_it";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("...", "0"));

    }

    private void confirm_approve()
    {
        if (txtremark_approve_all.Text != "")
        {
            if (check_approve_all.Checked == true)
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    data_itasset dataitasset = new data_itasset();
                    int itemObj = 0;
                    CreateDsits_u1_document();
                    DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                    its_u_document[] objcourse1 = new its_u_document[_u0tempListpurchase_approveall.Count()];
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        objcourse1[itemObj] = new its_u_document();
                        //detail

                        if (ViewState["mode"].ToString() == "it-manager")
                        {
                            objcourse1[itemObj].itsp_mg_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_mg_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_mg_status = 5;
                            objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 12, 912, 1112, 3612);//12
                            objcourse1[itemObj].flow_item = 8;
                            objcourse1[itemObj].operation_status_id = "approve_it_mg_7_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir2")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 5;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4000;
                            objcourse1[itemObj].flow_item = 50;
                            objcourse1[itemObj].operation_status_id = "asset_dir2_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir3")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 5;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4001;
                            objcourse1[itemObj].flow_item = 51;
                            objcourse1[itemObj].operation_status_id = "asset_dir3_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir4")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 5;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4002;
                            objcourse1[itemObj].flow_item = 52;
                            objcourse1[itemObj].operation_status_id = "asset_dir4_all";
                        }
                        else
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 5;
                            objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 14, 914, 1114, 3614);
                            objcourse1[itemObj].flow_item = 9;
                            objcourse1[itemObj].operation_status_id = "approve_it_dir_8_all";
                        }
                        objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                        objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                        objcourse1[itemObj].UEmpIDX = emp_idx;
                        itemObj++;
                    }
                    dataitasset.its_u_document_action = objcourse1;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                    callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        int id = _u0tempListpurchase_approveall[i].u0idx;
                        int _sysidx = _u0tempListpurchase_approveall[i].sysidx;
                        if (ViewState["mode"].ToString() == "it-manager")
                        {
                            settemplate_asset_create(id, 8, _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()));
                        }
                        //Senior manager Tobi
                        else if (ViewState["mode"].ToString() == "asset_dir2")
                        {
                            settemplate_asset_create(id, 52, 0);

                        }
                        //Director
                        else if (ViewState["mode"].ToString() == "asset_dir3")
                        {
                            settemplate_asset_create(id, 52, 0);

                        }
                        //CFO
                        else if (ViewState["mode"].ToString() == "asset_dir4")
                        {
                            settemplate_asset_approve(id, 5); //ส่งแจ้ง user ว่าถูกส่งให้ it แล้ว
                            if ((_sysidx != 9) && (_sysidx != 37)) //HR,OTHER
                            {
                                settemplate_asset_create(id, 6, _sysidx);
                            }

                        }
                        else
                        {
                            settemplate_asset_create(id, 9, _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()));
                        }
                    }

                }
            }
            else
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    int irow = 0;
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        if (_u0tempListpurchase_approveall[i].selected == true)
                        {
                            irow++;
                        }
                    }
                    if (irow > 0)
                    {
                        data_itasset dataitasset = new data_itasset();
                        int itemObj = 0;
                        CreateDsits_u1_document();
                        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                        its_u_document[] objcourse1 = new its_u_document[irow];
                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                objcourse1[itemObj] = new its_u_document();
                                //detail

                                if (ViewState["mode"].ToString() == "it-manager")
                                {
                                    objcourse1[itemObj].itsp_mg_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_mg_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_mg_status = 5;
                                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 12, 912, 1112, 3612);//12
                                    objcourse1[itemObj].flow_item = 8;
                                    objcourse1[itemObj].operation_status_id = "approve_it_mg_7_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir2")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 5;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4000;
                                    objcourse1[itemObj].flow_item = 50;
                                    objcourse1[itemObj].operation_status_id = "asset_dir2_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir3")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 5;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4001;
                                    objcourse1[itemObj].flow_item = 51;
                                    objcourse1[itemObj].operation_status_id = "asset_dir3_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir4")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 5;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4002;
                                    objcourse1[itemObj].flow_item = 52;
                                    objcourse1[itemObj].operation_status_id = "asset_dir4_all";
                                }
                                else
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 5;
                                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 14, 914, 1114, 3614);//14
                                    objcourse1[itemObj].flow_item = 9;
                                    objcourse1[itemObj].operation_status_id = "approve_it_dir_8_all";
                                }
                                objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                                objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                                objcourse1[itemObj].UEmpIDX = emp_idx;
                                itemObj++;
                            }
                        }
                        dataitasset.its_u_document_action = objcourse1;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                        callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                int id = _u0tempListpurchase_approveall[i].u0idx;
                                int _sysidx = _u0tempListpurchase_approveall[i].sysidx;
                                if (ViewState["mode"].ToString() == "it-manager")
                                {
                                    settemplate_asset_create(id, 8, _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()));
                                }
                                //Senior manager Tobi
                                else if (ViewState["mode"].ToString() == "asset_dir2")
                                {
                                    settemplate_asset_create(id, 52, 0);

                                }
                                //Director
                                else if (ViewState["mode"].ToString() == "asset_dir3")
                                {
                                    settemplate_asset_create(id, 52, 0);

                                }
                                //CFO
                                else if (ViewState["mode"].ToString() == "asset_dir4")
                                {
                                    settemplate_asset_approve(id, 5); //ส่งแจ้ง user ว่าถูกส่งให้ it แล้ว
                                    if ((_sysidx != 9) && (_sysidx != 37)) //HR,OTHER
                                    {
                                        settemplate_asset_create(id, 6, _sysidx);
                                    }

                                }
                                else
                                {
                                    settemplate_asset_create(id, 9, _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()));
                                }
                            }

                        }

                    }
                }

            }

        }

    }
    private void confirm_no_approve()
    {
        if (txtremark_approve_all.Text != "")
        {

            if (check_approve_all.Checked == true)
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    data_itasset dataitasset = new data_itasset();
                    int itemObj = 0;
                    CreateDsits_u1_document();
                    DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                    its_u_document[] objcourse1 = new its_u_document[_u0tempListpurchase_approveall.Count()];
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        objcourse1[itemObj] = new its_u_document();
                        //detail

                        if (ViewState["mode"].ToString() == "it-manager")
                        {
                            objcourse1[itemObj].itsp_mg_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_mg_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_mg_status = 6;
                            objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 13, 913, 1113, 3613);//13
                            objcourse1[itemObj].operation_status_id = "approve_it_mg_7_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir2")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 6;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4000;
                            objcourse1[itemObj].flow_item = 50;
                            objcourse1[itemObj].operation_status_id = "asset_dir2_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir3")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 6;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4001;
                            objcourse1[itemObj].flow_item = 51;
                            objcourse1[itemObj].operation_status_id = "asset_dir3_all";
                        }
                        else if (ViewState["mode"].ToString() == "asset_dir4")
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 6;
                            objcourse1[itemObj].node_idx = 8;
                            objcourse1[itemObj].actor_idx = 4002;
                            objcourse1[itemObj].flow_item = 52;
                            objcourse1[itemObj].operation_status_id = "asset_dir4_all";
                        }
                        else
                        {
                            objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                            objcourse1[itemObj].itsp_dir_status = 6;
                            objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 15, 915, 1115, 3615);//15
                            objcourse1[itemObj].operation_status_id = "approve_it_dir_8_all";
                        }

                        objcourse1[itemObj].flow_item = 1;
                        objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                        objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                        objcourse1[itemObj].UEmpIDX = emp_idx;

                        itemObj++;
                    }
                    dataitasset.its_u_document_action = objcourse1;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                    callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        int id = _u0tempListpurchase_approveall[i].u0idx;
                        if (ViewState["mode"].ToString() == "it-manager")
                        {
                            settemplate_asset_approve(id, 7);
                        }
                        else
                        {
                            settemplate_asset_approve(id, 8);
                        }
                    }

                }
            }
            else
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    int irow = 0;
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        if (_u0tempListpurchase_approveall[i].selected == true)
                        {
                            irow++;
                        }
                    }
                    if (irow > 0)
                    {
                        data_itasset dataitasset = new data_itasset();
                        int itemObj = 0;
                        CreateDsits_u1_document();
                        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                        its_u_document[] objcourse1 = new its_u_document[irow];
                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                objcourse1[itemObj] = new its_u_document();
                                //detail

                                if (ViewState["mode"].ToString() == "it-manager")
                                {
                                    objcourse1[itemObj].itsp_mg_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_mg_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_mg_status = 6;
                                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 13, 913, 1113, 3613);//13
                                    objcourse1[itemObj].operation_status_id = "approve_it_mg_7_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir2")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 6;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4000;
                                    objcourse1[itemObj].flow_item = 50;
                                    objcourse1[itemObj].operation_status_id = "asset_dir2_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir3")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 6;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4001;
                                    objcourse1[itemObj].flow_item = 51;
                                    objcourse1[itemObj].operation_status_id = "asset_dir3_all";
                                }
                                else if (ViewState["mode"].ToString() == "asset_dir4")
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 6;
                                    objcourse1[itemObj].node_idx = 8;
                                    objcourse1[itemObj].actor_idx = 4002;
                                    objcourse1[itemObj].flow_item = 52;
                                    objcourse1[itemObj].operation_status_id = "asset_dir4_all";
                                }
                                else
                                {
                                    objcourse1[itemObj].itsp_dir_emp_idx = emp_idx;
                                    objcourse1[itemObj].itsp_dir_remark = txtremark_approve_all.Text;
                                    objcourse1[itemObj].itsp_dir_status = 6;
                                    objcourse1[itemObj].u0_idx = getu0nodeidx(_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()), 15, 915, 1115, 3615);//15
                                    objcourse1[itemObj].operation_status_id = "approve_it_dir_8_all";
                                }
                                objcourse1[itemObj].flow_item = 1;
                                objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                                objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                                objcourse1[itemObj].UEmpIDX = emp_idx;
                                itemObj++;
                            }
                        }
                        dataitasset.its_u_document_action = objcourse1;
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                        callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                int id = _u0tempListpurchase_approveall[i].u0idx;
                                if (ViewState["mode"].ToString() == "it-manager")
                                {
                                    settemplate_asset_approve(id, 7);
                                }
                                else
                                {
                                    settemplate_asset_approve(id, 8);
                                }
                            }

                        }

                    }
                }
            }

        }


    }

    private void setObject_it_docket()
    {
        //_txtremark = (TextBox)Fv_docket.FindControl("txt_remark");
        _GridView = (GridView)Fv_docket.FindControl("gvlist_docket");
        // _ddlspecidx = (DropDownList)Fv_docket.FindControl("ddlsearch_spec");
        _ddlMonthSearch = (DropDownList)Fv_docket.FindControl("ddlMonthSearch");
        _ddlYearSearch = (DropDownList)Fv_docket.FindControl("ddlYearSearch");
        _Panel_approve = (Panel)Fv_docket.FindControl("Panel_approve");
        _check_approve = (CheckBox)Fv_docket.FindControl("check_approve_docket_all");
        _txt_remark = (TextBox)Fv_docket.FindControl("txtcomment_docket");
    }
    public string zsetMonthYear(int Month, int Year)
    {
        return _func_dmu.zMonthTH(Month) + " " + Year.ToString();
    }

    protected void action_select_its_docket(string _sCommandArgument)
    {
        setObject_it_docket();
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.idx = 0;
        // select_its.spec_idx = _func_dmu.zStringToInt(_ddlspecidx.SelectedValue);
        select_its.zmonth = _func_dmu.zStringToInt(_ddlMonthSearch.SelectedValue);
        select_its.zyear = _func_dmu.zStringToInt(_ddlYearSearch.SelectedValue);
        if (ViewState["mode"].ToString() == "it_deliver")
        {
            select_its.operation_status_id = "docket_u0_select_delivery";
        }
        else
        {
            select_its.operation_status_id = "docket_u0_select";
        }

        select_its.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        ViewState["its_u0_document_list"] = _dtitseet.its_u_document_action;
        setGridData(_GridView, ViewState["its_u0_document_list"]);

        _Panel_approve.Visible = false;
        _GridView.Columns[1].Visible = false;

        _GridView.Columns[2].Visible = false;
        _GridView.Columns[3].Visible = false;

        _GridView.Columns[4].Visible = false;
        _GridView.Columns[5].Visible = false;
        _GridView.Columns[6].Visible = false;
        _GridView.Columns[7].Visible = false;
        _GridView.Columns[8].Visible = false;

        _GridView.Columns[9].Visible = false;
        _GridView.Columns[10].Visible = false;
        _GridView.Columns[11].Visible = false;
        //summary
        _GridView.Columns[12].Visible = false;
        //delivery
        _GridView.Columns[13].Visible = false;
        _GridView.Columns[14].Visible = false;
        _GridView.Columns[15].Visible = false;

        if (ViewState["mode"].ToString() == "it_deliver")
        {
            _GridView.Columns[2].Visible = true;
            _GridView.Columns[4].Visible = true;
            _GridView.Columns[6].Visible = true;
            _GridView.Columns[7].Visible = true;
            _GridView.Columns[8].Visible = true;
            _GridView.Columns[11].Visible = true;
            _GridView.Columns[12].Visible = true;

            _GridView.Columns[15].Visible = true;
            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            //select_its.emp_idx = emp_idx;
            select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
            select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
            select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
            select_its.flow_item = 11;
            select_its.operation_status_id = "permission_admin_select";
            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            if (_dtitseet.its_u_document_action != null)
            {
                if (_dtitseet.its_u_document_action.Count() > 0)
                {
                    // _Panel_approve.Visible = true;
                    // _check_approve.Checked = false;
                    //  _GridView.Columns[1].Visible = true;
                }
            }
        }
        else
        {

            _GridView.Columns[4].Visible = true;
            _GridView.Columns[5].Visible = true;
            _GridView.Columns[8].Visible = true;
            _GridView.Columns[9].Visible = true;
            _GridView.Columns[10].Visible = true;

            _GridView.Columns[14].Visible = true;
            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            //select_its.emp_idx = emp_idx;
            select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
            select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
            select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
            select_its.flow_item = 9;
            select_its.operation_status_id = "permission_admin_select";
            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

            if (_dtitseet.its_u_document_action != null)
            {
                if (_dtitseet.its_u_document_action.Count() > 0)
                {
                    _Panel_approve.Visible = true;
                    _check_approve.Checked = false;
                    _txt_remark.Text = "";
                    _GridView.Columns[1].Visible = true;
                }
            }
        }
        setdefaultNotification();

    }
    protected void action_select_its_docket_detail(string _sCommandArgument)
    {
        string[] argument = new string[9];
        argument = _sCommandArgument.ToString().Split('|');
        int zmonth = int.Parse(argument[0]);
        int zyear = int.Parse(argument[1]);
        int sysidx = int.Parse(argument[2]);
        int typidx = int.Parse(argument[3]);
        int tdidx = int.Parse(argument[4]);
        string spec_remark = argument[5];
        string zu0idx = argument[6];
        string zu1idx = argument[7];

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        //select_its.typidx = typidx;
        //select_its.tdidx = tdidx;
        select_its.spec_remark = spec_remark;
        select_its.zsql = zu0idx;
        select_its.zu1idx = zu1idx;
        select_its.operation_status_id = "docket_u1_select";
        _dtitseet.its_u_document_action[0] = select_its;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        MvMaster.SetActiveView(View_docket_list);
        FvDetail_Showdocket_list.ChangeMode(FormViewMode.Insert);
        _GridView = (GridView)FvDetail_Showdocket_list.FindControl("gvItemsIts_docketlist");
        CreateDsits_u1_document();
        DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
        if (_dtitseet.its_u_document_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_u_document_action)
            {

                DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();
                ic++;
                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["id"] = ic;

                drContacts["sysidx"] = vdr.sysidx;
                drContacts["sysidx_name"] = vdr.SysNameTH;

                drContacts["typidx"] = vdr.typidx;
                drContacts["typidx_name"] = vdr.type_name;

                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.zdevices_name;

                drContacts["pr_type_idx"] = vdr.pr_type_idx;
                drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                drContacts["ionum"] = vdr.ionum;
                drContacts["ref_itnum"] = vdr.ref_itnum;

                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["rpos_idx_name"] = vdr.pos_name_th;

                drContacts["list_menu"] = vdr.list_menu;

                drContacts["price"] = vdr.price;

                drContacts["currency_idx"] = vdr.currency_idx;
                drContacts["currency_idx_name"] = vdr.currency_th;

                drContacts["qty"] = 1;// vdr.qty;

                drContacts["unidx"] = vdr.unidx;
                drContacts["unidx_name"] = vdr.NameTH;

                drContacts["costidx"] = vdr.costidx;
                drContacts["costcenter_idx_name"] = vdr.CostNo;

                drContacts["budget_set"] = vdr.budget_set;

                drContacts["place_idx"] = vdr.place_idx;
                drContacts["place_idx_name"] = vdr.place_name;

                drContacts["spec_idx"] = 0;
                drContacts["spec_idx_name"] = "";

                drContacts["leveldevice"] = vdr.leveldevice;

                drContacts["total_amount"] = vdr.total_amount;

                drContacts["zdevices_name"] = vdr.zdevices_name;
                drContacts["ionum"] = vdr.ionum;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["asset_no"] = "";
                drContacts["u2idx"] = 0;
                drContacts["flow_item"] = vdr.flow_item;
                drContacts["asset_no"] = vdr.asset_no;
                drContacts["u2idx"] = vdr.u2idx;
                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["emp_name_th"] = vdr.emp_name_th;

                dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);

            }
        }

        ViewState["vsits_u1_document"] = dsContacts;
        ViewState["its_u1_document"] = dsContacts;

        setGridData(_GridView, ViewState["its_u1_document"]);

        // ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_show_detail();", true);

    }
    private void confirm_approve_docket(Boolean check)
    {
        if (check == true)
        {
            its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
            if (_u0tempListpurchase_approveall.Count() > 0)
            {
                data_itasset dataitasset = new data_itasset();
                int itemObj = 0;
                CreateDsits_u1_document();
                DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                its_u_document[] objcourse1 = new its_u_document[_u0tempListpurchase_approveall.Count()];
                for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                {
                    objcourse1[itemObj] = new its_u_document();
                    //detail
                    objcourse1[itemObj].typidx = _u0tempListpurchase_approveall[i].typidx;
                    objcourse1[itemObj].tdidx = _u0tempListpurchase_approveall[i].tdidx;
                    objcourse1[itemObj].spec_idx = _u0tempListpurchase_approveall[i].spec_idx;
                    objcourse1[itemObj].qty = _u0tempListpurchase_approveall[i].qty;
                    objcourse1[itemObj].amount = _u0tempListpurchase_approveall[i].amount;
                    objcourse1[itemObj].zu0idx = _u0tempListpurchase_approveall[i].zu0idx;
                    objcourse1[itemObj].zu1idx = _u0tempListpurchase_approveall[i].zu1idx;
                    objcourse1[itemObj].spec_remark = _u0tempListpurchase_approveall[i].spec_remark;
                    objcourse1[itemObj].CEmpIDX = emp_idx;
                    objcourse1[itemObj].UEmpIDX = emp_idx;
                    objcourse1[itemObj].itsp_summ_emp_idx = emp_idx;
                    objcourse1[itemObj].itsp_summ_status = 5;
                    objcourse1[itemObj].flow_item = 10;
                    objcourse1[itemObj].doc_status = 1;
                    objcourse1[itemObj].u0_idx = getu0nodeidx(_u0tempListpurchase_approveall[i].sysidx, 16, 916, 1116, 3616);//16
                    objcourse1[itemObj].itsp_summ_remark = "";
                    objcourse1[itemObj].sysidx = _u0tempListpurchase_approveall[i].sysidx;
                    objcourse1[itemObj].unidx = _u0tempListpurchase_approveall[i].unidx;
                    objcourse1[itemObj].docket_remark = _txt_remark.Text.Trim();
                    objcourse1[itemObj].operation_status_id = "docket";
                    itemObj++;
                }
                dataitasset.its_u_document_action = objcourse1;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                dataitasset = callServicePostITAsset(_urlSetInsits_u_document, dataitasset);

                if (_func_dmu.zStringToInt(dataitasset.ReturnCode) > 0)
                {
                    int id = _func_dmu.zStringToInt(dataitasset.ReturnCode);
                    settemplate_asset_docket_create(id);
                }

            }
        }
        else
        {
            its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
            if (_u0tempListpurchase_approveall.Count() > 0)
            {
                int irow = 0;
                for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                {
                    if (_u0tempListpurchase_approveall[i].selected == true)
                    {
                        irow++;
                    }
                }
                if (irow > 0)
                {
                    data_itasset dataitasset = new data_itasset();
                    int itemObj = 0;
                    CreateDsits_u1_document();
                    DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                    its_u_document[] objcourse1 = new its_u_document[irow];
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        if (_u0tempListpurchase_approveall[i].selected == true)
                        {
                            objcourse1[itemObj] = new its_u_document();
                            //detail
                            objcourse1[itemObj].typidx = _u0tempListpurchase_approveall[i].typidx;
                            objcourse1[itemObj].tdidx = _u0tempListpurchase_approveall[i].tdidx;
                            objcourse1[itemObj].spec_idx = _u0tempListpurchase_approveall[i].spec_idx;
                            objcourse1[itemObj].qty = _u0tempListpurchase_approveall[i].qty;
                            objcourse1[itemObj].amount = _u0tempListpurchase_approveall[i].amount;
                            objcourse1[itemObj].zu0idx = _u0tempListpurchase_approveall[i].zu0idx;
                            objcourse1[itemObj].zu1idx = _u0tempListpurchase_approveall[i].zu1idx;
                            objcourse1[itemObj].spec_remark = _u0tempListpurchase_approveall[i].spec_remark;
                            objcourse1[itemObj].CEmpIDX = emp_idx;
                            objcourse1[itemObj].UEmpIDX = emp_idx;
                            objcourse1[itemObj].itsp_summ_emp_idx = emp_idx;
                            objcourse1[itemObj].itsp_summ_status = 5;
                            objcourse1[itemObj].flow_item = 10;
                            objcourse1[itemObj].doc_status = 1;
                            objcourse1[itemObj].u0_idx = getu0nodeidx(_u0tempListpurchase_approveall[i].sysidx, 16, 916, 1116, 3616);//16
                            objcourse1[itemObj].itsp_summ_remark = "";
                            objcourse1[itemObj].sysidx = _u0tempListpurchase_approveall[i].sysidx;
                            objcourse1[itemObj].unidx = _u0tempListpurchase_approveall[i].unidx;
                            objcourse1[itemObj].docket_remark = _txt_remark.Text.Trim();
                            objcourse1[itemObj].operation_status_id = "docket";
                            itemObj++;
                        }
                    }
                    dataitasset.its_u_document_action = objcourse1;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                    dataitasset = callServicePostITAsset(_urlSetInsits_u_document, dataitasset);
                    if (_func_dmu.zStringToInt(dataitasset.ReturnCode) > 0)
                    {
                        int id = _func_dmu.zStringToInt(dataitasset.ReturnCode);
                        settemplate_asset_docket_create(id);
                    }
                }
            }

        }

    }


    private void setObject_it_docket_md()
    {

        _GridView = (GridView)Fv_docket_md.FindControl("gvlist_docket_md");
        _ddlspecidx = (TextBox)Fv_docket_md.FindControl("ddlsearch_spec");
        _Panel_approve = (Panel)Fv_docket_md.FindControl("Panel_approve");
        _check_approve = (CheckBox)Fv_docket_md.FindControl("check_approve_docket_md_all");

        _ddlMonthSearch = (DropDownList)Fv_docket_md.FindControl("ddlMonthSearch");
        _ddlYearSearch = (DropDownList)Fv_docket_md.FindControl("ddlYearSearch");
        _txtdocumentcode = (TextBox)Fv_docket_md.FindControl("txtdocumentcode");
        _txt_remark = (TextBox)Fv_docket_md.FindControl("txtremark_approve_allmd");

    }

    protected void action_select_its_docket_md(string _sCommandArgument)
    {

        setObject_it_docket_md();
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.idx = 0;
        //ค้นหาตามวันที่
        /*
        select_its.condition_date_type = _func_dmu.zStringToInt(_ddlcondition.SelectedValue);
        if (_txtstartdate.Text.Trim() != "")
        {
            select_its.start_date = _func_dmu.zDateToDB(_txtstartdate.Text.Trim());
        }
        if (_txtenddate.Text.Trim() != "")
        {
            select_its.end_date = _func_dmu.zDateToDB(_txtenddate.Text.Trim());
        }
        */
        select_its.zmonth = _func_dmu.zStringToInt(_ddlMonthSearch.SelectedValue);
        select_its.zyear = _func_dmu.zStringToInt(_ddlYearSearch.SelectedValue);
        // _txt_remark.Text = "";
        if (ViewState["mode"].ToString() == "md_approve")
        {
            select_its.u0_status = 1;
            select_its.sysidx_admin = 0;
        }
        else
        {
            select_its.u0_status = 5;
            select_its.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        }
        select_its.doccode = _txtdocumentcode.Text.Trim();
        select_its.operation_status_id = "docket_u0_list";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        ViewState["its_u0_document_list"] = _dtitseet.its_u_document_action;
        setGridData(_GridView, ViewState["its_u0_document_list"]);

        _Panel_approve.Visible = false;
        _GridView.Columns[1].Visible = false;
        if (ViewState["mode"].ToString() == "md_approve")
        {
            _dtitseet.its_u_document_action = new its_u_document[1];
            select_its = new its_u_document();
            //select_its.emp_idx = emp_idx;
            select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
            select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
            select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
            select_its.flow_item = 10;
            select_its.operation_status_id = "permission_admin_select";
            _dtitseet.its_u_document_action[0] = select_its;
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            if (_dtitseet.its_u_document_action != null)
            {
                if ((_dtitseet.its_u_document_action.Count() > 0) &&
                    (ViewState["mode"].ToString() == "md_approve") &&
                    (_func_dmu.zStringToInt(ViewState["rpos_idx"].ToString()) == md_rpos_idx)
                    )
                {
                    _Panel_approve.Visible = true;
                    _check_approve.Checked = false;
                    _GridView.Columns[1].Visible = true;
                }
            }
        }

        setdefaultNotification();


    }
    private void setObject_it_docket_md_detail()
    {
        _txtremark = (TextBox)Fv_docket_md_detail.FindControl("txt_remark");
        _GridView = (GridView)Fv_docket_md_detail.FindControl("gvlist_docket_md_list");
        //_ddlspecidx = (DropDownList)Fv_docket_md.FindControl("ddlsearch_spec");
        _Panel_approve = (Panel)Fv_docket_md_detail.FindControl("Panel_body_approve");
        _ddlStatusapprove = (DropDownList)Fv_docket_md_detail.FindControl("ddlStatusapprove");
        _lb_title_approve = (Label)Fv_docket_md_detail.FindControl("lb_title_approve");
        _lbtn = (LinkButton)Fv_docket_md_detail.FindControl("btnAdddata");

    }
    protected void action_select_its_docket_md_detail(string _sCommandArgument)
    {

        // int u0_docket_idx = _func_dmu.zStringToInt(_sCommandArgument);
        string[] argument = new string[5];
        argument = _sCommandArgument.ToString().Split('|');
        int u0_docket_idx = int.Parse(argument[0]);
        //int u1_docket_idx = int.Parse(argument[1]);
        //string zu0idx = argument[2];
        //string zu1idx = argument[3];
        //string zu0_docket_idx = argument[4];
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.operation_status_id = "docket_u0_list";
        _dtitseet.its_u_document_action[0] = select_its;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        _func_dmu.zSetFormViewData(Fv_docket_md_detail, _dtitseet.its_u_document_action);

        setObject_it_docket_md_detail();
        _dtitseet.its_u_document_action = new its_u_document[1];
        select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        //select_its.u1_docket_idx = u1_docket_idx;
        select_its.operation_status_id = "docket_u1_list";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        ViewState["docket_u1_list"] = _dtitseet.its_u_document_action;
        _func_dmu.zSetGridData(_GridView, ViewState["docket_u1_list"]);

        _dtitseet.its_u_document_action = new its_u_document[1];
        select_its = new its_u_document();
        // select_its.emp_idx = emp_idx;
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());
        select_its.flow_item = 10;
        select_its.operation_status_id = "permission_admin_select";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        _Panel_approve.Visible = false;
        _lbtn.Visible = false;
        if (_dtitseet.its_u_document_action != null)
        {
            if (
                (_dtitseet.its_u_document_action.Count() > 0) &&
                (_func_dmu.zStringToInt(ViewState["rpos_idx"].ToString()) == md_rpos_idx)
                    )
            {
                _Panel_approve.Visible = true;
                _lbtn.Visible = true;
            }
        }

    }


    private void confirm_approve_md_docket()
    {
        setObject_it_docket_md();
        if (_txt_remark.Text != "")
        {
            if (_check_approve.Checked == true)
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    data_itasset dataitasset = new data_itasset();
                    int itemObj = 0;
                    CreateDsits_u1_document();
                    DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                    its_u_document[] objcourse1 = new its_u_document[_u0tempListpurchase_approveall.Count()];
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        objcourse1[itemObj] = new its_u_document();
                        //detail

                        objcourse1[itemObj].md_emp_idx = emp_idx;
                        objcourse1[itemObj].md_remark = _txt_remark.Text;
                        objcourse1[itemObj].md_status = 5;
                        objcourse1[itemObj].u0_idx = 18;
                        objcourse1[itemObj].flow_item = 11;
                        objcourse1[itemObj].operation_status_id = "approve_md_all";

                        objcourse1[itemObj].doc_status = 5; //ดำเนินการ
                        objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                        objcourse1[itemObj].u0_docket_idx = _u0tempListpurchase_approveall[i].u0_docket_idx;
                        objcourse1[itemObj].UEmpIDX = emp_idx;
                        itemObj++;
                    }
                    dataitasset.its_u_document_action = objcourse1;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                    callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        int id = _u0tempListpurchase_approveall[i].u0_docket_idx;
                        settemplate_asset_docket_approve(id);
                        settemplate_asset_docket_approvetouser(id);
                    }

                }
            }
            else
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    int irow = 0;
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        if (_u0tempListpurchase_approveall[i].selected == true)
                        {
                            irow++;
                        }
                    }
                    if (irow > 0)
                    {
                        data_itasset dataitasset = new data_itasset();
                        int itemObj = 0;
                        CreateDsits_u1_document();
                        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                        its_u_document[] objcourse1 = new its_u_document[irow];
                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                objcourse1[itemObj] = new its_u_document();
                                //detail

                                objcourse1[itemObj].md_emp_idx = emp_idx;
                                objcourse1[itemObj].md_remark = _txt_remark.Text;
                                objcourse1[itemObj].md_status = 5;
                                objcourse1[itemObj].u0_idx = 18;
                                objcourse1[itemObj].flow_item = 11;
                                objcourse1[itemObj].operation_status_id = "approve_md_all";

                                objcourse1[itemObj].doc_status = 5; //ดำเนินการ
                                objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                                objcourse1[itemObj].u0_docket_idx = _u0tempListpurchase_approveall[i].u0_docket_idx;
                                objcourse1[itemObj].UEmpIDX = emp_idx;
                                itemObj++;
                            }
                        }
                        dataitasset.its_u_document_action = objcourse1;
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                        callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                int id = _u0tempListpurchase_approveall[i].u0_docket_idx;
                                settemplate_asset_docket_approve(id);
                                settemplate_asset_docket_approvetouser(id);
                            }

                        }


                    }
                }

            }
        }


    }
    private void confirm_no_approve_md_docket()
    {
        setObject_it_docket_md();
        if (_txt_remark.Text != "")
        {
            if (_check_approve.Checked == true)
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    data_itasset dataitasset = new data_itasset();
                    int itemObj = 0;
                    CreateDsits_u1_document();
                    DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                    its_u_document[] objcourse1 = new its_u_document[_u0tempListpurchase_approveall.Count()];
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        objcourse1[itemObj] = new its_u_document();
                        //detail

                        objcourse1[itemObj].md_emp_idx = emp_idx;
                        objcourse1[itemObj].md_remark = _txt_remark.Text;
                        objcourse1[itemObj].md_status = 6;
                        objcourse1[itemObj].u0_idx = 19;
                        objcourse1[itemObj].operation_status_id = "approve_md_all";

                        objcourse1[itemObj].flow_item = 1;
                        objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                        objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                        objcourse1[itemObj].u0_docket_idx = _u0tempListpurchase_approveall[i].u0_docket_idx;
                        objcourse1[itemObj].UEmpIDX = emp_idx;

                        itemObj++;
                    }
                    dataitasset.its_u_document_action = objcourse1;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                    dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        int id = _u0tempListpurchase_approveall[i].u0_docket_idx;
                        settemplate_asset_docket_approve(id);
                        settemplate_asset_docket_approvetouser(id);
                    }


                }
            }
            else
            {
                its_u_document[] _u0tempListpurchase_approveall = (its_u_document[])ViewState["its_u0_document_list"];
                if (_u0tempListpurchase_approveall.Count() > 0)
                {
                    int irow = 0;
                    for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                    {
                        if (_u0tempListpurchase_approveall[i].selected == true)
                        {
                            irow++;
                        }
                    }
                    if (irow > 0)
                    {
                        data_itasset dataitasset = new data_itasset();
                        int itemObj = 0;
                        CreateDsits_u1_document();
                        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
                        its_u_document[] objcourse1 = new its_u_document[irow];
                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                objcourse1[itemObj] = new its_u_document();
                                //detail

                                objcourse1[itemObj].md_emp_idx = emp_idx;
                                objcourse1[itemObj].md_remark = _txt_remark.Text;
                                objcourse1[itemObj].md_status = 6;
                                objcourse1[itemObj].u0_idx = 19;
                                objcourse1[itemObj].operation_status_id = "approve_md_all";

                                objcourse1[itemObj].flow_item = 1;
                                objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                                objcourse1[itemObj].u0idx = _u0tempListpurchase_approveall[i].u0idx;
                                objcourse1[itemObj].u0_docket_idx = _u0tempListpurchase_approveall[i].u0_docket_idx;
                                objcourse1[itemObj].UEmpIDX = emp_idx;
                                itemObj++;
                            }
                        }
                        dataitasset.its_u_document_action = objcourse1;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
                        dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

                        for (int i = 0; i <= _u0tempListpurchase_approveall.Count() - 1; i++)
                        {
                            if (_u0tempListpurchase_approveall[i].selected == true)
                            {
                                int id = _u0tempListpurchase_approveall[i].u0_docket_idx;
                                settemplate_asset_docket_approve(id);
                                settemplate_asset_docket_approvetouser(id);
                            }

                        }

                    }
                }
            }

        }


    }

    private Boolean zSaveUpdate_md(int id)
    {

        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "";
        setObject_it_docket_md_detail();
        HiddenField hidden_u0_docket_idx = (HiddenField)Fv_docket_md_detail.FindControl("hidden_u0_docket_idx");
        HiddenField hidden_u1_docket_idx = (HiddenField)Fv_docket_md_detail.FindControl("hidden_u1_docket_idx");
        if (_func_dmu.zStringToInt(ViewState["flow_item"].ToString()) == 10)
        {
            data_itasset dataitasset = new data_itasset();
            dataitasset.its_u_document_action = new its_u_document[1];
            its_u_document select_its = new its_u_document();
            select_its.u0_docket_idx = _func_dmu.zStringToInt(hidden_u0_docket_idx.Value);
            select_its.u1_docket_idx = _func_dmu.zStringToInt(hidden_u1_docket_idx.Value);
            //detail
            select_its.md_emp_idx = emp_idx;
            select_its.md_remark = _txtremark.Text;
            select_its.md_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 5) //อนุมัติ
            {
                select_its.u0_idx = 18;
                select_its.flow_item = 11;
                select_its.doc_status = 5; //ดำเนินการ
            }
            else //กลับไปแก้ไข
            {
                select_its.u0_idx = 19;
                select_its.flow_item = 1; //กับไปแก้ไข
                select_its.doc_status = 1; //ดำเนินการ
            }


            select_its.operation_status_id = "approve_md";
            dataitasset.its_u_document_action[0] = select_its;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            dataitasset = callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);

            if (_func_dmu.zStringToInt(hidden_u0_docket_idx.Value) > 0)
            {
                int idx = _func_dmu.zStringToInt(hidden_u0_docket_idx.Value);
                settemplate_asset_docket_approve(idx);
                settemplate_asset_docket_approvetouser(idx);
            }

        }

        // Page.Response.Redirect(Page.Request.Url.ToString(), true);

        _Boolean = true;

        return _Boolean;
    }
    protected void action_select_its_Delivery(string _sCommandArgument)
    {

        string[] argument = new string[9];
        argument = _sCommandArgument.ToString().Split('|');
        int typidx = int.Parse(argument[0]);
        int tdidx = int.Parse(argument[1]);
        int spec_idx = int.Parse(argument[2]);
        int u0_docket_idx = int.Parse(argument[3]);

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.operation_status_id = "list_u0_docket";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        FvDetail_ShowDelivery.DataSource = _dtitseet.its_u_document_action;
        FvDetail_ShowDelivery.DataBind();

        _dtitseet.its_u_document_action = new its_u_document[1];
        select_its = new its_u_document();
        //select_its.typidx = typidx;
        //select_its.tdidx = tdidx;
        //select_its.spec_idx = spec_idx;
        select_its.u0_docket_idx = u0_docket_idx;
        select_its.operation_status_id = "list_u2_docket";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        CreateDsits_u1_document();
        GridView gvItemsIts_Delivery = (GridView)FvDetail_ShowDelivery.FindControl("gvItemsIts_Delivery");
        DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
        if (_dtitseet.its_u_document_action != null)
        {
            int ic = 0;
            foreach (var vdr in _dtitseet.its_u_document_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_u1_document"].NewRow();

                drContacts["u1idx"] = vdr.u1idx;
                drContacts["u0idx"] = vdr.u0idx;
                drContacts["id"] = ic;

                drContacts["sysidx"] = vdr.sysidx;
                drContacts["sysidx_name"] = vdr.SysNameTH;

                drContacts["typidx"] = vdr.typidx;
                drContacts["typidx_name"] = vdr.type_name;

                drContacts["tdidx"] = vdr.tdidx;
                drContacts["tdidx_name"] = vdr.zdevices_name;

                drContacts["pr_type_idx"] = vdr.pr_type_idx;
                drContacts["purchase_type_idx_name"] = vdr.name_purchase_type;

                drContacts["ionum"] = vdr.ionum;
                drContacts["ref_itnum"] = vdr.ref_itnum;

                drContacts["rpos_idx"] = vdr.rpos_idx;
                drContacts["rpos_idx_name"] = vdr.pos_name_th;

                drContacts["list_menu"] = vdr.list_menu;

                drContacts["price"] = vdr.price;

                drContacts["currency_idx"] = vdr.currency_idx;
                drContacts["currency_idx_name"] = vdr.currency_th;

                drContacts["qty"] = 1;// vdr.qty;

                drContacts["unidx"] = vdr.unidx;
                drContacts["unidx_name"] = vdr.NameTH;

                drContacts["costidx"] = vdr.costidx;
                drContacts["costcenter_idx_name"] = vdr.CostNo;

                drContacts["budget_set"] = vdr.budget_set;

                drContacts["place_idx"] = vdr.place_idx;
                drContacts["place_idx_name"] = vdr.place_name;

                drContacts["spec_idx"] = 0;
                drContacts["spec_idx_name"] = "";

                drContacts["leveldevice"] = vdr.leveldevice;

                drContacts["total_amount"] = vdr.total_amount;

                drContacts["zdevices_name"] = vdr.zdevices_name;
                drContacts["ionum"] = vdr.ionum;
                drContacts["ref_itnum"] = vdr.ref_itnum;
                drContacts["flow_item"] = vdr.flow_item;
                drContacts["asset_no"] = vdr.asset_no;
                drContacts["u2idx"] = vdr.u2idx;

                drContacts["doccode"] = vdr.doccode;
                drContacts["zdocdate"] = vdr.zdocdate;
                drContacts["pr_idx"] = vdr.pr_idx;
                drContacts["pr_remark"] = vdr.pr_remark;

                drContacts["u0_docket_idx"] = vdr.u0_docket_idx;
                drContacts["u1_docket_idx"] = vdr.u1_docket_idx;
                drContacts["u2_docket_idx"] = vdr.u2_docket_idx;

                drContacts["org_idx_its"] = vdr.org_idx_its;
                drContacts["rdept_idx_its"] = vdr.rdept_idx_its;
                drContacts["rsec_idx_its"] = vdr.rsec_idx_its;
                drContacts["u0_didx"] = vdr.u0_didx;
                drContacts["u0_code"] = vdr.u0_code;

                drContacts["actor_idx"] = vdr.actor_idx;
                drContacts["node_idx"] = vdr.node_idx;

                dsContacts.Tables["dsits_u1_document"].Rows.Add(drContacts);

            }
        }

        ViewState["vsits_u1_document"] = dsContacts;

        setGridData(gvItemsIts_Delivery, dsContacts.Tables["dsits_u1_document"]);


    }
    protected void select_device_pr_status(DropDownList ddlName, int id)
    {

        ddlName.Items.Clear();

        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.operation_status_id = "device_pr_status";

        _dataitasset.its_lookup_action[0] = obj_lookup;
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("...", "0"));

        ddlName.SelectedValue = id.ToString();

    }

    private void setObject_it_Delivery()
    {

        _GridView = (GridView)FvDetail_ShowDelivery.FindControl("gvItemsIts_Delivery");

    }

    private Boolean zSaveUpdate_Delivery(int id)
    {

        Boolean _Boolean = false;
        string sMode = "", sDocno = "";
        string m0_prefix = "";
        int iu0_docket_idx = 0;
        setObject_it_Delivery();

        if (ViewState["mode"].ToString() == "it_deliver") // เจ้าหน้าที่ it support
        {
            data_itasset dataitasset = new data_itasset();
            //******************  start its_document *********************//
            int itemObj = 0;
            CreateDsits_u1_document();
            DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
            its_u_document[] objcourse1 = new its_u_document[_GridView.Rows.Count];

            foreach (var item in _GridView.Rows)
            {
                Label _lbu0idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu0idx");
                Label _lbu1idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu1idx");
                Label _lbu2idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu2idx");

                Label _lbu0_docket_idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu0_docket_idx");
                Label _lbu1_docket_idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu1_docket_idx");
                Label _lbu2_docket_idx = (Label)_GridView.Rows[itemObj].FindControl("_lbu2_docket_idx");

                Label _lbid = (Label)_GridView.Rows[itemObj].FindControl("_lbid");
                DropDownList ddlpr_idx = (DropDownList)_GridView.Rows[itemObj].FindControl("ddlpr_idx");
                TextBox txt_pr_remark = (TextBox)_GridView.Rows[itemObj].FindControl("txt_pr_remark");
                iu0_docket_idx = _func_dmu.zStringToInt(_lbu0_docket_idx.Text);

                objcourse1[itemObj] = new its_u_document();

                //detail
                objcourse1[itemObj].pr_emp_idx = emp_idx;
                objcourse1[itemObj].pr_idx = _func_dmu.zStringToInt(ddlpr_idx.SelectedValue);
                objcourse1[itemObj].u0idx = _func_dmu.zStringToInt(_lbu0idx.Text);
                objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(_lbu1idx.Text);
                objcourse1[itemObj].u2idx = _func_dmu.zStringToInt(_lbu2idx.Text);
                objcourse1[itemObj].u0_docket_idx = _func_dmu.zStringToInt(_lbu0_docket_idx.Text);
                objcourse1[itemObj].u1_docket_idx = _func_dmu.zStringToInt(_lbu1_docket_idx.Text);
                objcourse1[itemObj].u2_docket_idx = _func_dmu.zStringToInt(_lbu2_docket_idx.Text);
                objcourse1[itemObj].UEmpIDX = emp_idx;
                objcourse1[itemObj].pr_remark = txt_pr_remark.Text;
                objcourse1[itemObj].operation_status_id = "update_device_pr_status";
                /*
                if(_func_dmu.zStringToInt(ddlpr_idx.SelectedValue) == 3) //ผู้ขอซื้อกลับไปแก้ไขราคา
                {
                    objcourse1[itemObj].flow_item = 14;
                }
                else
                {
                    objcourse1[itemObj].flow_item = 13;
                }
                */
                itemObj++;

            }

            dataitasset.its_u_document_action = objcourse1;
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
            //******************  end its_document *********************//
            settemplate_asset_delivery(iu0_docket_idx, 3);
            settemplate_asset_delivery(iu0_docket_idx, 1);

            //แจ้งเพิ่ม price io

            settemplate_asset_delivery(iu0_docket_idx, 14);

        }


        _Boolean = true;

        return _Boolean;
    }
    //send email
    private void settemplate_asset_create(int id, int flow_item, int sysidx_admin, string status = "create")
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_create";
        obj_document.u0idx = id;
        obj_document.flow_item = flow_item;
        obj_document.sysidx_admin = sysidx_admin;
        obj_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_document.jobgrade_level_dir = 0;// _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        obj_document.zstatus = status;
        _dataitasset.its_u_document_action[0] = obj_document;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataitasset));
        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }


    }
    private void settemplate_asset_approve(int id, int flow_item)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_approve";
        obj_document.u0idx = id;
        obj_document.flow_item = flow_item;
        obj_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_document.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        _dataitasset.its_u_document_action[0] = obj_document;

        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }
    }
    private void settemplate_asset_docket_create(int id)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_create";
        obj_document.u0_docket_idx = id;
        obj_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_document.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        _dataitasset.its_u_document_action[0] = obj_document;

        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }
    }
    private void settemplate_asset_docket_approve(int id)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_approve";
        obj_document.u0_docket_idx = id;
        obj_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_document.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        _dataitasset.its_u_document_action[0] = obj_document;

        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }
    }
    private void settemplate_asset_docket_approvetouser(int id)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_docket_approvetouser";
        obj_document.u0_docket_idx = id;
        obj_document.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        obj_document.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        obj_document.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        _dataitasset.its_u_document_action[0] = obj_document;

        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }
    }


    #endregion


    protected void btnsettemplate_asset_create_Click(object sender, EventArgs e)
    {
        int id = _func_dmu.zStringToInt(txt_settemplate_asset_create.Text.Trim());
        int flow_item = _func_dmu.zStringToInt(txt_flow_item.Text.Trim());
        //  settemplate_asset_create(id, flow_item);
        // settemplate_asset_approve(id, flow_item);
        // settemplate_asset_docket_create(id);
        try
        {
            settemplate_asset_docket_approvetouser(id);
        }
        catch { }


    }

    private void setpermission_admin_menu()
    {
        Boolean boolean = false;
        //main menu
        _divMenuLiToDivIndex.Visible = true; //รายการทั่วไป
        _divMenuLiToDivBuyNew.Visible = false; //ขอซื้อ
        _divMenuLiToDivDevices.Visible = false; //ตัดชำรุดอุปกรณ์
        _divMenuLiToDivChangeOwn.Visible = false; //โอนย้ายอุปกรณ์
        _divMenuLiToDivWaitApprove.Visible = false; //รายการรออนุมัติ
        _divMenuLiToDivBudget.Visible = false; //Budget
        _divMenuLiToDivAsset.Visible = false; //Asset
        _divMenuLiIT.Visible = false; //it en hr
        _divMenuLiMD.Visible = false; //MD

        // lis menu it/hr/en
        _liIT_list1.Visible = false; //ข้อมูลขอซื้อ
        _liIT_list1_update.Visible = false; //แก้ไขข้อมูลขอซื้อ
        _liIT_manager.Visible = false; //อนุมัติรายการขอซื้อโดยผู้จัดการฝ่าย
        _liIT_director.Visible = false; //อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย
        _liIT_summary.Visible = false; //สรุปรายการขอซื้อ
        _liIT_print.Visible = false; //Print รายการขอซื้อ
        _liIT_deliver.Visible = false; //ส่งมอบเครื่องให้กับ User


        _liIT_list1_update_separator.Visible = false;
        _liIT_manager_separator.Visible = false;
        _liIT_director_separator.Visible = false;
        _liIT_summary_separator.Visible = false;
        _liIT_print_separator.Visible = false;
        _liIT_deliver_separator.Visible = false;

        //lis menu asset
        _divMenuLiToDivAsset_master.Visible = true; //Master Data Class

        _divMenuLiToDivAsset_system.Visible = false; //ตรวจสอบข้อมูลขอซื้อเพื่อออกเลขทรัพย์สิน
        _liMenuLiToasset_dir1.Visible = false; //อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย Asset

        _divMenuLiToDivDataGiveAsset.Visible = false; //ข้อมูลของบประมาณ
        _divMenuLiToDivDataBuyAsset.Visible = false; //ข้อมูลขอซื้อ Asset

        _divMenuLiToDivAsset_system_separator.Visible = false;
        _liMenuLiToasset_dir1_separator.Visible = false;

        _divMenuLiToDivDataGiveAsset_separator.Visible = false;
        _divMenuLiToDivDataBuyAsset_separator.Visible = false;

        //lis menu budget
        _divMenuLiToDivBudget_system.Visible = false; //จัดการระบบ Budget

        _divMenuLiToDivBudget_system_separator.Visible = false;
        //จัดซื้อ	
        _divMenuLipurchase.Visible = false;

        //ยังไม่ใช้
        _liMenuLiToDivWaitApprove_CutDevices.Visible = false;
        _divMenuLiToDivWaitApprove_CutDevices.Visible = false; //รายการรออนุมัติตัดเสียอุปกรณ์
        _liMenuLiToDivWaitApprove_ChangeOwn.Visible = false;
        _divMenuLiToDivWaitApprove_ChangeOwn.Visible = false; //รายการรออนุมัติโอนย้ายอุปกรณ์

        _liSeniorManager.Visible = false;
        _liDir_Asset.Visible = false;
        _liCFO.Visible = false;
        _liAttachFlieAsset.Visible = false;

        if ((Admin_Office_emp_idx != emp_idx) && (md_emp_idx != emp_idx))
        {

        
            if (_func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()) == 20) // it
        {
            boolean = true;
            _divMenuLiToDivBuyNew.Visible = boolean; //ขอซื้อ

        }
        else
        {
            boolean = true;
            _divMenuLiToDivBuyNew.Visible = boolean; //ขอซื้อ
        }
        // สิทธ์การอนุมัติของ director ฝ่าย

        if (_func_dmu.zStringToInt(ViewState["RSec_JobGradeIDX"].ToString()) >= 9) //ผู้ช่วยขึ้นไป มีหรือไม่
        {


            boolean = true;
            _divMenuLiToDivBuyNew.Visible = boolean; //ขอซื้อ
            _divMenuLiToDivWaitApprove.Visible = boolean; //รายการรออนุมัติ


        }

        boolean = true;
        int ic_menu_it_en_hr = 0;
        int ic_asset = 0;
        int ic_budget = 0;
        int ic_md = 0;
        int ic_pur = 0;

        if (_func_dmu.zStringToInt(ViewState["RSec_JobGradeIDX"].ToString()) >= 10) //ผู้ช่วยขึ้นไป มีหรือไม่
        {
            data_itasset _dtitseet1 = new data_itasset();
            _dtitseet1.its_lookup_action = new its_lookup[1];
            its_lookup select_its1 = new its_lookup();
            select_its1.idx = emp_idx;
            select_its1.operation_status_id = "setjobgrade_director_admin_menu";
            _dtitseet1.its_lookup_action[0] = select_its1;
            _dtitseet1 = callServicePostITAsset(_urlGetits_lookup, _dtitseet1);

            if (_dtitseet1.its_lookup_action != null)
            {
                _liIT_director_separator.Visible = boolean;
                _liIT_director.Visible = boolean;
                ic_menu_it_en_hr++;
            }
        }


        // สิทธ์การอนุมัติของ md

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.emp_idx = emp_idx;
        select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        // select_its.org_idx = _func_dmu.zStringToInt(ViewState["org_idx"].ToString());
        // select_its.rsec_idx = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        //select_its.rpos_idx = _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString());

        select_its.operation_status_id = "permission_admin_dept_menu";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        if (_dtitseet.its_u_document_action != null)
        {
            //30072020
            foreach (var item in _dtitseet.its_u_document_action)
            {
                // litDebug.Text = ViewState["rpos_idx"].ToString();
                /*
                if (
                    (item.org_idx == _func_dmu.zStringToInt(ViewState["org_idx"].ToString()))
                    &&
                    (item.rdept_idx == _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()))
                  &&
                  (item.rsec_idx == _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()))
                  &&
                  (item.rpos_idx == _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString()))
                  )
                {*/

                    ////md 
                    if (item.flow_item == 10)
                    {
                        _divMenuLiMD.Visible = boolean;
                        ic_md++;
                    }
                    //จัดซื้อ
                    else if ((item.flow_item == 6) && (item.sysidx_admin == 37))
                    {
                        _divMenuLipurchase.Visible = boolean;
                        ic_pur++;
                    }
                    else
                    {
                        /******* start menu budget ********/
                        //จัดการระบบ Budget
                        if (item.flow_item == 3)
                        {
                            _divMenuLiToDivBudget_system_separator.Visible = boolean;
                            _divMenuLiToDivBudget_system.Visible = boolean;
                            ic_budget++;
                        }
                        /******* end menu budget ********/
                        /******* start menu asset ********/
                        //อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย Asset
                        else if (item.flow_item == 4)
                        {
                            _divMenuLiToDivAsset_system_separator.Visible = boolean;
                            _divMenuLiToDivAsset_system.Visible = boolean;
                            ic_asset++;
                        }
                        /******* end menu asset ********/
                        /******* start menu asset ********/
                        //อนุมัติรายการขอซื้อโดยผู้จัดการฝ่าย Asset
                        else if (item.flow_item == 5)
                        {
                            _liMenuLiToasset_dir1_separator.Visible = boolean;
                            _liMenuLiToasset_dir1.Visible = boolean;
                            ic_asset++;
                        }
                        else if (item.flow_item == 49)
                        {
                            _liAttachFlieAsset.Visible = boolean;
                            ic_asset++;
                        }
                        //Senior Manager
                        else if (item.flow_item == 50)
                        {

                            _liSeniorManager.Visible = boolean;
                            ic_asset++;
                        }
                        //อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย Asset
                        else if (item.flow_item == 51)
                        {

                            _liDir_Asset.Visible = boolean;
                            ic_asset++;
                        }
                        //อนุมัติรายการขอซื้อโดย CFO
                        else if (item.flow_item == 52)
                        {

                            _liCFO.Visible = boolean;
                            ic_asset++;
                        }
                        /******* end menu asset ********/

                        /******* start menu it hr en ********/
                        //อนุมัติรายการขอซื้อโดยผู้จัดการฝ่าย
                        else if ((item.flow_item == 7)
                            )
                        {
                            _liIT_manager_separator.Visible = boolean;
                            _liIT_manager.Visible = boolean;
                            ic_menu_it_en_hr++;
                        }
                        //อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย
                        else if ((item.flow_item == 88)
                            )
                        {
                            _liIT_director_separator.Visible = boolean;
                            _liIT_director.Visible = boolean;
                            ic_menu_it_en_hr++;
                            // litDebug.Text = "3-อนุมัติรายการขอซื้อโดยผู้อำนวยการฝ่าย";
                        }
                        //สรุปรายการขอซื้อ
                        else if (item.flow_item == 9)
                        {
                            _liIT_summary_separator.Visible = boolean;
                            _liIT_summary.Visible = boolean;
                            _liIT_print_separator.Visible = boolean;
                            _liIT_print.Visible = boolean;
                            ic_menu_it_en_hr++;
                            //litDebug.Text = "4-สรุปรายการขอซื้อ";
                        }
                        /******* end menu it hr en ********/



                    }

               /* }
                else
                {*/
                    if (
                        (item.rdept_idx == _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()))
                         &&
                        (item.rsec_idx == _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString()))
                        &&
                        (4396 != _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString())) //พี่เกมส์
                        &&
                        (949 != _func_dmu.zStringToInt(ViewState["rpos_idx"].ToString())) // พี่พน
                       )
                    {
                        /******* start menu it hr en ********/
                        //ใส่ spec
                        if ((item.flow_item == 6) && (item.sysidx_admin != 37))
                        {
                            _liIT_list1.Visible = boolean;
                            _liIT_list1_update_separator.Visible = boolean;
                            _liIT_list1_update.Visible = boolean;
                            ic_menu_it_en_hr++;
                        }
                        //ส่งมอบเครื่องให้กับ User
                        else if (item.flow_item == 11)
                        {
                            _liIT_deliver_separator.Visible = boolean;
                            _liIT_deliver.Visible = boolean;
                            ic_menu_it_en_hr++;
                        }
                        /******* end menu it hr en ********/
                    }
                    else if (
                        (item.rdept_idx == _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString()))
                       )
                    {
                        ////
                    }


               // }


            }

            /******* menu it hr en ********/
            if (ic_menu_it_en_hr > 0)
            {
                _divMenuLiIT.Visible = boolean; //it en hr
            }
            /******* menu asset ********/
            if (ic_asset > 0)
            {
                _divMenuLiToDivAsset.Visible = boolean;
            }
            /******* menu budget ********/
            if (ic_budget > 0)
            {
                _divMenuLiToDivBudget.Visible = boolean;
            }
            /******* จัดซื้อ ********/
            if (ic_pur > 0)
            {
                _divMenuLipurchase.Visible = boolean;
            }

            if (ic_md > 0)
            {
                ViewState["mode"] = "md_approve";
                settab_md();
                _divMenuLiToDivIndex.Visible = false; //รายการทั่วไป
                _divMenuLiToDivBuyNew.Visible = false; //ขอซื้อ
                _divMenuLiToDivWaitApprove.Visible = false;
                _divMenuLiIT.Visible = false;
                _divMenuLiToDivBudget.Visible = false;
                _divMenuLiToDivDataBuyAsset.Visible = false; //ข้อมูลขอซื้อ Asset

            }

        }
        else
        {
            /******* menu it hr en ********/
            if (ic_menu_it_en_hr > 0)
            {
                _divMenuLiIT.Visible = boolean; //it en hr
            }
        }
        _divMenuLiToDivDataGiveAsset.Visible = true; //ข้อมูลของบประมาณ
        _divMenuLiToDivDataBuyAsset.Visible = true; //ข้อมูลขอซื้อ Asset

        _divMenuLiToDivDataGiveAsset_separator.Visible = true;
        _divMenuLiToDivDataBuyAsset_separator.Visible = true;
        }
        else
        {
            _divMenuLiMD.Visible = true;

            ViewState["mode"] = "md_approve";
            //settab_md();
            _divMenuLiToDivIndex.Visible = false; //รายการทั่วไป
            _divMenuLiToDivBuyNew.Visible = false; //ขอซื้อ
            _divMenuLiToDivWaitApprove.Visible = false;
            _divMenuLiIT.Visible = false;
            _divMenuLiToDivBudget.Visible = false;
            _divMenuLiToDivDataBuyAsset.Visible = false; //ข้อมูลขอซื้อ Asset

            if (Admin_Office_emp_idx == emp_idx)
            {
                _divMenuLiToDivIndex.Visible = true; //รายการทั่วไป
                _divMenuLiToDivBuyNew.Visible = true; //ขอซื้อ
            }
        }
    }

    private void setStatusApprove()
    {
        setObject_it_docket_md();
        _GridView.Columns[7].Visible = false;
        _GridView.Columns[8].Visible = false;
        ViewState["its_u0_document_list"] = null;
        setGridData(_GridView, ViewState["its_u0_document_list"]);
        if (ViewState["mode"].ToString() == "md_approve")
        {
            _Panel_approve.Visible = true;
            _GridView.Columns[7].Visible = true;
        }
        else
        {
            _Panel_approve.Visible = false;
            _GridView.Columns[8].Visible = true;
        }

    }

    private void showdata_print(int id)
    {
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = id;
        select_its.operation_status_id = "docket_u0_org";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

        if (_dtitseet.its_u_document_action != null)
        {
            int ic = 0;
            foreach (var item in _dtitseet.its_u_document_action)
            {
                ic++;
                //  litDebug.Text = ic.ToString();
                Session["_SESSION_U0DOCIDX"] = id.ToString();
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('it-asset-print', '', '');", true);

            }
        }
    }


    private void showdata_print_detail(int id)
    {
        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0_docket_idx = id;
        select_its.operation_status_id = "docket_u0_list";
        _dtitseet.its_u_document_action[0] = select_its;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        _func_dmu.zSetFormViewData(Fv_docket_print_detial, _dtitseet.its_u_document_action);

        _dtitseet.its_u_document_action = new its_u_document[1];
        select_its = new its_u_document();
        select_its.u0_docket_idx = id;
        select_its.operation_status_id = "docket_u0_org";
        _dtitseet.its_u_document_action[0] = select_its;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        ViewState["docket_u0_org_list"] = _dtitseet.its_u_document_action;
        _GridView = (GridView)Fv_docket_print_detial.FindControl("gvlist_org");
        setGridData(_GridView, ViewState["docket_u0_org_list"]);

    }

    private void setdefaultNotification()
    {

        //if (ViewState["rsec_idx"].ToString() != "210")
        //{
        int icount = 1;
        for (int i = 1; i <= icount; i++)
        {
            setNotification(i, 0);
        }
        //}

    }

    private void setNotification(int _item, int _value)
    {
        data_itasset dataitasset = new data_itasset();
        dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.rdept_idx_its = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.emp_idx = emp_idx;
        select_its.operation_status_id = "m0_menu";
        select_its.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
        // select_its.jobgrade_level_dir = _func_dmu.zStringToInt(ViewState["jobgrade_level_dir"].ToString());
        //litDebug.Text = ViewState["locidx"].ToString();
        if ((_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()) == 11) || (_func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString()) == 36))
        {
            select_its.locidx = _func_dmu.zStringToInt(ViewState["locidx"].ToString());
        }
        else
        {
            select_its.locidx = 0;
        }
        dataitasset.its_u_document_action[0] = select_its;
        //litDebug.Text = emp_idx.ToString() + " : " + ViewState["rdept_idx"].ToString() + " : " + ViewState["sysidx_admin"].ToString(); ;  //HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        dataitasset = callServicePostITAsset(_urlGetits_u_document, dataitasset);
        if (dataitasset.its_u_document_action != null)
        {
            LinkButton lnkbtn = new LinkButton();
            string sLabel = ""
                , sflow_item_2 = ""
                , sBudget = ""
                 , sAsset = ""
                 , sIT = ""
                ;
            int flow_item_2 = 0
                , iBudget = 0
                , iAsset = 0
                , iIT = 0
                ;
            foreach (var item in dataitasset.its_u_document_action)
            {
                int ic = 0;
                if (item.flow_item == 2)
                {
                    //โอนย้ายอุปกรณ์
                    if ((item.menu_seq == 5) && (item.menu_list_seq == 3))
                    {
                        lnkbtn = BtnToDivWaitApprove_ChangeOwn;
                        sLabel = item.menu_list;
                        _value = item.zCount;
                        flow_item_2 += item.zCount;
                        sflow_item_2 = item.menu;
                        ic++;
                    }
                    else
                    {
                        lnkbtn = BtnToDivWaitApprove_Buy;
                        sLabel = item.menu_list;
                        _value = item.zCount;
                        flow_item_2 += item.zCount;
                        sflow_item_2 = item.menu;
                        ic++;
                    }

                }
                else if (item.flow_item == 3)
                {
                    lnkbtn = BtnToDivBudget;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    iBudget += item.zCount;
                    sBudget = item.menu;
                    ic++;
                }
                else if (item.flow_item == 4)
                {
                    lnkbtn = BtnToDivAsset_system;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 5)
                {
                    lnkbtn = btnMenuLiToasset_dir1;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 49)
                {
                    lnkbtn = btnAttachFlieAsset;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 50)
                {
                    lnkbtn = btnM_SeniorManager;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 51)
                {
                    lnkbtn = btnM_Dir_Asset;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 52)
                {
                    lnkbtn = btnM_CFO;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if ((item.flow_item == 6) && (item.menu_seq != 10))
                {
                    lnkbtn = _btnIT;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if ((item.flow_item == 6) && (item.menu_seq == 10))
                {
                    lnkbtn = _divMenubtnpurchase;
                    sLabel = item.menu;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if (item.flow_item == 7)
                {
                    lnkbtn = _divMenubtnIT_manager;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if (item.flow_item == 8)
                {
                    lnkbtn = _divMenubtnIT_director;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if (item.flow_item == 9)
                {
                    lnkbtn = _divMenubtnIT_summary;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if (item.flow_item == 11)
                {
                    lnkbtn = _divMenubtnIT_deliver;
                    sLabel = item.menu_list;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iIT += item.zCount;
                    }
                    sIT = item.menu;
                    ic++;
                }
                else if (item.flow_item == 10)
                {
                    lnkbtn = _divMenubtnMD_it;
                    sLabel = item.menu;
                    _value = item.zCount;
                    ic++;
                }
                else if (item.flow_item == 51)
                {
                    lnkbtn = btnM_SeniorManager;
                    sLabel = item.menu;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 52)
                {
                    lnkbtn = btnM_Dir_Asset;
                    sLabel = item.menu;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                else if (item.flow_item == 53)
                {
                    lnkbtn = btnM_CFO;
                    sLabel = item.menu;
                    _value = item.zCount;
                    if (lnkbtn.Visible == true)
                    {
                        iAsset += item.zCount;
                    }
                    sAsset = item.menu;
                    ic++;
                }
                if (ic > 0)
                {
                    setNotificationMenubtn(lnkbtn
                               , sLabel
                               , _value
                               );

                }

            }

            setNotificationMenulabel(_lbMenuLiToDivWaitApprove
                               , sflow_item_2
                               , flow_item_2
                               );
            setNotificationMenulabel(_lbMenuLiToDivBudget
                               , sBudget
                               , iBudget
                               );
            setNotificationMenulabel(_lbMenuLiToDivAsset
                               , sAsset
                               , iAsset
                               );

            setNotificationMenulabel(_lbMenuLiIT
                               , ViewState["sysidx_remark"].ToString()
                               , iIT
                               );

        }

    }

    private void setNotificationMenubtn(LinkButton _linkbtn, string _label, int _value)
    {
        _linkbtn.Text = _label + " <span class='badge progress-bar-danger' >" + Convert.ToString(_value) + "</span>";
    }
    private void setNotificationMenulabel(Label _linkbtn, string _label, int _value)
    {
        _linkbtn.Text = _label + " <span class='badge progress-bar-danger' >" + Convert.ToString(_value) + "</span>";
    }

    protected void ExportToPdf()
    {

        HiddenField hidden_u0_docket_idx = (HiddenField)Fv_docket_print_detial.FindControl("hidden_u0_docket_idx");
        //litDebug.Text = hidden_u0_docket_idx.Value;
        string pageurl = "it-asset-print-org?u0_docket_idx = " + hidden_u0_docket_idx.Value;
        //Response.Write("<script> window.open('" + pageurl + "',''); </script>");
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('" + pageurl + "', '', '');", true);
    }

    private void settemplate_asset_delivery(int id, int flow_item)
    {
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_u_document_action = new its_u_document[1];
        its_u_document obj_document = new its_u_document();
        obj_document.operation_status_id = "template_asset_delivery";
        obj_document.u0_docket_idx = id;
        obj_document.flow_item = flow_item;
        _dataitasset.its_u_document_action[0] = obj_document;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataitasset));

        try
        {
            callServicePostITAsset(_urlsendEmailits_u_document, _dataitasset);
        }
        catch { }

    }

    protected void setionumber_default()
    {
        setObject();
        //ckeck io no
        its_masterit obj_m = new its_masterit();
        _dtitseet.its_masterit_action = new its_masterit[1];
        obj_m.pr_type_idx = _func_dmu.zStringToInt(_ddlsearch_typepurchase.SelectedValue);
        if (ViewState["mode"] != null)
        {
            if (ViewState["mode"].ToString() == "E")
            {
                if (ViewState["u0idx"] != null)
                {
                    obj_m.u0idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
                }
                else
                {
                    obj_m.u0idx = 0;
                }
            }
            else
            {
                obj_m.u0idx = 0;
            }
        }
        else
        {
            obj_m.u0idx = 0;
        }

        obj_m.iono = _txtionum.Text.Trim();
        obj_m.costcenter_idx = _func_dmu.zStringToInt(_ddlcostcenter.SelectedValue);
        obj_m.operation_status_id = "its_m0_io_budget";
        _dtitseet.its_masterit_action[0] = obj_m;
        _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_masterit, _dtitseet);
        if (_dtitseet.its_masterit_action != null)
        {
            foreach (var item in _dtitseet.its_masterit_action)
            {
                _txtqty.Text = "1";
                _txtprice.Text = item.cost.ToString();
                _txtbudget_set.Text = item.budget.ToString();
                _ddlcostcenter.SelectedValue = item.costcenter_idx.ToString();

            }
        }
        else
        {
            _ddlcostcenter.SelectedValue = "0";
        }


    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            if (textbox.ID == "txtionum")
            {
                setionumber_default();
            }
        }
    }
    protected void select_m0status(DropDownList ddlName, int mode)
    {
        ddlName.Items.Clear();
        data_itasset _dataitasset = new data_itasset();
        _dataitasset.its_lookup_action = new its_lookup[1];
        its_lookup obj_lookup = new its_lookup();
        obj_lookup.operation_status_id = "m0_status";
        obj_lookup.idx = mode;
        _dataitasset.its_lookup_action[0] = obj_lookup;
        _dataitasset = callServicePostITAsset(_urlGetits_lookup, _dataitasset);

        ddlName.DataSource = _dataitasset.its_lookup_action;
        ddlName.DataTextField = "zname";
        ddlName.DataValueField = "idx";
        ddlName.DataBind();
        ddlName.Items.Insert(0, new System.Web.UI.WebControls.ListItem("ผลการอนุมัติ.....", "0"));

    }

    protected void showModal_Spec()
    {

        _FormView = getFv(ViewState["mode"].ToString());
        _ddlSearchTypeDevices = (DropDownList)_FormView.FindControl("ddlSearchTypeDevices");
        _ddlSearchDevices = (DropDownList)_FormView.FindControl("ddlSearchDevices");
        _ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");

        data_itasset dtitaseet = new data_itasset();
        dtitaseet.its_lookup_action = new its_lookup[1];
        its_lookup select_its = new its_lookup();
        select_its.sysidx = _func_dmu.zStringToInt(_ddlsystem.SelectedValue);
        select_its.typidx = _func_dmu.zStringToInt(_ddlSearchTypeDevices.SelectedValue);
        select_its.idx = _func_dmu.zStringToInt(_ddlSearchDevices.SelectedValue);
        select_its.operation_status_id = "spec";
        dtitaseet.its_lookup_action[0] = select_its;
        // 
        dtitaseet = callServicePostITAsset(_urlGetits_lookup, dtitaseet);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dtitaseet));
        CreateDs_showmodal();
        DataSet dsContacts = (DataSet)ViewState["vsits_showmodal"];
        if (dtitaseet.its_lookup_action != null)
        {
            int ic = 0;
            foreach (var vdr in dtitaseet.its_lookup_action)
            {
                ic++;
                DataRow drContacts = dsContacts.Tables["dsits_showmodal"].NewRow();

                drContacts["id"] = ic;
                drContacts["zname"] = vdr.zname;
                drContacts["detail"] = vdr.detail;

                dsContacts.Tables["dsits_showmodal"].Rows.Add(drContacts);

            }
        }
        ViewState["vsits_showmodal"] = dsContacts;

        setGridData(gvListspec_modal, dsContacts.Tables["dsits_showmodal"]);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_Spec_detail();", true);

    }
    private void select_data(int id)
    {
        TextBox txtremark = (TextBox)_FormView.FindControl("txtlist_buynew");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
        DataSet dsContacts = (DataSet)ViewState["vsits_showmodal"];

        foreach (DataRow dr in dsContacts.Tables["dsits_showmodal"].Rows)
        {
            if (_func_dmu.zStringToInt(dr["id"].ToString()) == id)
            {

                _ddlspecidx.Text = dr["zname"].ToString();
                txtremark.Text = dr["detail"].ToString();
                txtremark.Focus();
                break;
            }
        }

    }
    protected void CreateDs_showmodal()
    {
        string sDs = "dsits_showmodal";
        string sVs = "vsits_showmodal";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);

        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zname", typeof(String));
        ds.Tables[sDs].Columns.Add("detail", typeof(String));

        ViewState[sVs] = ds;

    }

    protected void showModal_updateprice(string _string)
    {
        string[] _argument = new string[3];
        _argument = _string.Split('|');
        int u0idx = _func_dmu.zStringToInt(_argument[0]);
        int u1idx = _func_dmu.zStringToInt(_argument[1]);
        // int id = _func_dmu.zStringToInt(_argument[2]);

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        select_its.u0idx = u0idx;
        select_its.u1idx = u1idx;
        select_its.operation_status_id = "list_u1_update_price";

        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);

        fv_show_updateprice.DataSource = _dtitseet.its_u_document_action;
        fv_show_updateprice.DataBind();

        // HiddenField hidden_id = (HiddenField)fv_show_updateprice.FindControl("hidden_id");
        // hidden_id.Value = id.ToString();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_updateprice();", true);

    }

    protected void updateprice(string _string)
    {

        string[] _argument = new string[3];
        _argument = _string.Split('|');
        int u0idx = _func_dmu.zStringToInt(_argument[0]);
        int u1idx = _func_dmu.zStringToInt(_argument[1]);
        // HiddenField hidden_id = (HiddenField)fv_show_updateprice.FindControl("hidden_id");
        TextBox txt_price = (TextBox)fv_show_updateprice.FindControl("txt_price");
        TextBox txt_budget_set = (TextBox)fv_show_updateprice.FindControl("txt_budget_set");

        DataSet dsContacts = (DataSet)ViewState["vsits_u1_document"];
        foreach (DataRow dr in dsContacts.Tables["dsits_u1_document"].Rows)
        {
            if ((_func_dmu.zStringToInt(dr["u0idx"].ToString()) == u0idx) &&
                (_func_dmu.zStringToInt(dr["u1idx"].ToString()) == u1idx)
                )
            {
                its_masterit obj_m = new its_masterit();
                _dtitseet.its_masterit_action = new its_masterit[1];
                obj_m.pr_type_idx = _func_dmu.zStringToInt(dr["pr_type_idx"].ToString());
                if (ViewState["mode"] != null)
                {
                    if (ViewState["mode"].ToString() == "E")
                    {
                        if (ViewState["u0idx"] != null)
                        {
                            obj_m.u0idx = _func_dmu.zStringToInt(ViewState["u0idx"].ToString());
                        }
                        else
                        {
                            obj_m.u0idx = 0;
                        }
                    }
                    else
                    {
                        obj_m.u0idx = 0;
                    }
                }
                else
                {
                    obj_m.u0idx = 0;
                }
                obj_m.iono = dr["ionum"].ToString();
                obj_m.costcenter_idx = _func_dmu.zStringToInt(dr["costidx"].ToString());
                obj_m.operation_status_id = "its_m0_io_budget";
                _dtitseet.its_masterit_action[0] = obj_m;
                _dtitseet = _func_dmu.zCallServicePostITAsset(_urlGetits_masterit, _dtitseet);

                int _qty = 0;
                string _iono = "";
                if (_dtitseet.its_masterit_action != null)
                {
                    foreach (var item in _dtitseet.its_masterit_action)
                    {
                        _qty = _qty + item.balance_qty;
                        _iono = item.iono;
                    }
                }
                if (_qty == 0)
                {

                    showMsAlert("ไม่อนุญาตให้ใช้ IO " + dr["ionum"].ToString() +
                        " เนื่องจากมีการนำเลขที่ IO " + dr["ionum"].ToString() +
                        " ไปใช้ในการออกใบขอซื้อครบตามจำนวนที่ขอใน IO เรียบร้อยแล้ว " +
                        " หากท่านต้องการทำรายการขอซื้อกรุณาติดต่อฝ่ายงบประมาณ " +
                        " !!!");
                    return;
                }
                else
                {
                    int iError = 0;
                    if (_dtitseet.its_masterit_action != null)
                    {

                        foreach (var item in _dtitseet.its_masterit_action)
                        {
                            decimal price = 0, budget = 0;
                            price = _func_dmu.zStringToDecimal(txt_price.Text);
                            budget = _func_dmu.zStringToDecimal(txt_budget_set.Text);
                            if (price <= item.cost)
                            {
                                //ขอซื้อได้
                            }
                            else
                            {
                                iError++;
                                showMsAlert("ราคาเกินงบของเลขที่ IO : " + dr["ionum"].ToString() + " !!!");
                                return;
                            }
                            if (budget <= item.budget)
                            {
                                //ขอซื้อได้
                            }
                            else
                            {
                                iError++;
                                showMsAlert("งบประมาณที่ตั้งไว้เกินงบของเลขที่ IO : " + dr["ionum"].ToString() + " !!!");
                                return;
                            }
                        }
                    }
                    if (iError == 0)
                    {
                        decimal _total_amount = 0;
                        dr["price"] = txt_price.Text;
                        dr["budget_set"] = txt_budget_set.Text;
                        _total_amount = _func_dmu.zStringToDecimal(txt_price.Text) * _func_dmu.zStringToInt(dr["qty"].ToString());
                        dr["total_amount"] = _total_amount;
                    }

                }

                break;
            }
        }
        ViewState["vsits_u1_document"] = dsContacts;
        //setObject();//
        //GridView gvItemsIts = (GridView)fvdetails_purchasequipment.FindControl("gvItemsIts"); 
        setGridData(gvItemsIts, dsContacts.Tables["dsits_u1_document"]);

    }

    private Boolean zSaveUser_updateIO(int id)
    {

        Boolean _Boolean = false;
        setObject();

        if ((ViewState["mode"].ToString() == "E") &&
            (_func_dmu.zStringToInt(ViewState["emp_idx_its"].ToString()) == _func_dmu.zStringToInt(ViewState["emp_idx"].ToString()))
            )
        {
            data_itasset dataitasset = new data_itasset();
            //******************  start its_document *********************//
            int itemObj = 0;

            DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
            its_u_document[] objcourse1 = new its_u_document[dsU1.Tables["dsits_u1_document"].Rows.Count];

            _txtremark = (TextBox)fvmanage_approvepurchase.FindControl("txtcomment_its");
            _ddlStatusapprove = (DropDownList)fvmanage_approvepurchase.FindControl("ddlStatusapprove");
            foreach (DataRow dr in dsU1.Tables["dsits_u1_document"].Rows)
            {

                objcourse1[itemObj] = new its_u_document();

                //detail
                objcourse1[itemObj].UEmpIDX = emp_idx;
                objcourse1[itemObj].user_remark = _txtremark.Text;
                objcourse1[itemObj].user_status = _func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue);
                if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 25) //แก้ไขเลขที่ IO และราคาใหม่เส็จแล้ว
                {
                    objcourse1[itemObj].u0_idx = 34;
                    objcourse1[itemObj].flow_item = 12;
                }
                else //ดำเนินการขอเลขที่ IO และราคาใหม่
                {
                    objcourse1[itemObj].u0_idx = 33;
                    objcourse1[itemObj].flow_item = 14;
                }
                objcourse1[itemObj].doc_status = 1; //ดำเนินการ
                objcourse1[itemObj].u1idx = _func_dmu.zStringToInt(dr["u1idx"].ToString());
                objcourse1[itemObj].u0idx = _func_dmu.zStringToInt(dr["u0idx"].ToString());// _func_dmu.zStringToInt(_lbu0idx.Text);
                objcourse1[itemObj].price = _func_dmu.zStringToDecimal(dr["price"].ToString());
                objcourse1[itemObj].total_amount = _func_dmu.zStringToDecimal(dr["total_amount"].ToString());
                objcourse1[itemObj].budget_set = _func_dmu.zStringToDecimal(dr["budget_set"].ToString());
                objcourse1[itemObj].UEmpIDX = emp_idx;
                objcourse1[itemObj].operation_status_id = "user_update_priceio";
                itemObj++;
                //user_update_priceio
                //approve_it_6_update
            }

            dataitasset.its_u_document_action = objcourse1;
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataitasset));
            callServicePostITAsset(_urlSetUpdits_u_document, dataitasset);
            //******************  end its_document *********************//
            if (_func_dmu.zStringToInt(_ddlStatusapprove.SelectedValue) == 25) //แก้ไขเลขที่ IO และราคาใหม่เส็จแล้ว
            {
                TextBox txt_hidden_sysidx = (TextBox)fvdetails_purchasequipment.FindControl("txt_hidden_sysidx");
                settemplate_asset_create(id, 12, _func_dmu.zStringToInt(txt_hidden_sysidx.Text), "create_ionew");
            }


            setActiveTab("P");
        }


        MvMaster.SetActiveView(ViewIndex);
        ShowDataIndex();

        _Boolean = true;

        return _Boolean;
    }


    private void setpermission_admin_dept()
    {

        _dtitseet.its_u_document_action = new its_u_document[1];
        its_u_document select_its = new its_u_document();
        //select_its.rdept_idx = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
        select_its.idx = emp_idx;
        select_its.operation_status_id = "its_m0_setpermission_admin_dept";
        _dtitseet.its_u_document_action[0] = select_its;
        _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
        if (_dtitseet.its_u_document_action != null)
        {
            foreach (var item in _dtitseet.its_u_document_action)
            {
                ViewState["sysidx_admin"] = item.sysidx_admin;
                ViewState["sysidx_remark"] = item.remark;
                //_lbMenuLiIT.Text = item.remark;
                //litDebug.Text = item.remark;
            }

        }
        else
        {
            ViewState["sysidx_admin"] = 0;
            ViewState["sysidx_remark"] = "";

        }
        // litDebug.Text = ViewState["sysidx_admin"].ToString();
    }

    private int getu0nodeidx(int sysidx, int idx_it, int idx_hr, int idx_en, int idx_qa)
    {
        int u0nodeidx = 0;
        if (sysidx == 11)//en
        {
            u0nodeidx = idx_en;
        }
        else if (sysidx == 9)//hr
        {
            u0nodeidx = idx_hr;
        }
        else if (sysidx == 36)//hr
        {
            u0nodeidx = idx_qa;
        }
        else //it
        {
            u0nodeidx = idx_it;
        }

        return u0nodeidx;

    }

    protected void select_pos(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference pos = new price_reference();

        _dtitseet.boxprice_reference[0] = pos;

        _dtitseet = callServicePostITAsset(_urlSelectPos, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "pos_name", "posidx");
        ddlName.Items.Insert(0, new ListItem("ประเภทกลุ่มพนักงาน", "0"));

    }

    private void getDevice(string cmdArg)
    {
        TextBox lbdocument_code = (TextBox)FvDetail_ShowDelivery.FindControl("lbdocument_code");
        TextBox txt_sysidx = (TextBox)FvDetail_ShowDelivery.FindControl("txt_sysidx");
        string[] _argument = new string[8];
        _argument = cmdArg.ToString().Split('|');


        string url = "";
        if (txt_sysidx.Text == "3") // IT
        {
            Session["_sesion_u0idx"] = _argument[0];
            Session["_sesion_u2idx"] = _argument[1];
            Session["_sesion_u0_docket_idx"] = _argument[2];
            Session["_sesion_asset_no"] = _argument[3];
            Session["_sesion_tdidx"] = _argument[4];
            Session["_sesion_org_idx_its"] = _argument[5];
            Session["_sesion_rdept_idx_its"] = _argument[6];
            Session["_sesion_rsec_idx_its"] = _argument[7];
            //Session["_sesion_devicestype"] = _argument[8];

            Session["_sesion_pono"] = "";// lbdocument_code.Text;
            Session["_sesion_devices"] = "create";


            if (_argument[8] == "10")
            {
                url = "network-devices";

                //"networkdevices.aspx";

            }
            else
            {
                url = "devices-register";
            }


        }
        else if (txt_sysidx.Text == "11") // EN
        {
            // url = "RegisterMachine";
            string _ss_u0idx = _argument[0];
            string _ss_u2idx = _argument[1];
            string _ss_u0_docket_idx = _argument[2];
            string _ss_asset_no = _argument[3];
            string _ss_tdidx = _argument[4];
            string _ss_org_idx_its = _argument[5];
            string _ss_rdept_idx_its = _argument[6];
            string _ss_rsec_idx_its = _argument[7];
            string _ss_typidx = _argument[8];

            string _ss_pono = "";// lbdocument_code.Text;
            string _ss_devices = "create";

            string emp_ = Session["emp_idx"].ToString();
            string rdept_ = ViewState["rdept_idx"].ToString();
            string rsec_ = ViewState["rsec_idx"].ToString();
            string rpos_ = ViewState["rpos_idx"].ToString();
            if (_ss_typidx == "11") //Utilities Support
            {
                _ss_typidx = "2";
            }
            else if (_ss_typidx == "12") //Machine Process
            {
                _ss_typidx = "1";
            }
            url = _baseUrl + "?_emp_idx=" + emp_ +
                "&rdept_idx=" + rdept_ +
                "&rsec_idx=" + rsec_ +
                "&rpos_idx=" + rpos_ +
               "&_ss_asset_no=" + _ss_asset_no +
               "&_ss_pono=" + _ss_pono +
               "&_ss_devices=" + _ss_devices +
               "&_ss_typidx=" + _ss_typidx +
               "&_ss_tdidx=" + _ss_tdidx
               ;


        }
        if (url != null)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('" + url + "', '', '');", true);

        }

    }

    public string getDeviceStatus(string u0_code)
    {
        string str = "";
        if (u0_code != "")
        {
            str = "ลงทะเบียนเรียบร้อยแล้ว(" + u0_code + ")";
        }
        return str;
    }

    private void setSpecial()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _ddlsystem = (DropDownList)_FormView.FindControl("ddlsystem");
        _pnl_other = (Panel)_FormView.FindControl("pnl_other");
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");
        _txtlist_menu = (TextBox)_FormView.FindControl("txtlist_buynew");
        _check_special = (CheckBox)_FormView.FindControl("check_special");
        _btn_spec_search = (LinkButton)_FormView.FindControl("btn_spec_search");

        _ddlspecidx.Text = "";
        _txtlist_menu.Text = "";
        if (_check_special.Checked == true) // special
        {
            // _pnl_other.Visible = false;
            _ddlspecidx.Enabled = true;
            _txtlist_menu.Enabled = true;
            _btn_spec_search.Visible = true;
        }
        else
        {
            // _pnl_other.Visible = true;
            _ddlspecidx.Enabled = false;
            _txtlist_menu.Enabled = false;
            _btn_spec_search.Visible = false;
        }

        if (_func_dmu.zStringToInt(_ddlsystem.SelectedValue) == 37)
        {
            _pnl_other.Visible = false;
        }
        else
        {
            _pnl_other.Visible = true;
        }

    }

    private void setjobgrade_director(int rdept_idx)
    {

        data_itasset _dtitseet = new data_itasset();
        _dtitseet.its_lookup_action = new its_lookup[1];
        its_lookup select_its = new its_lookup();
        select_its.operation_status_id = "setjobgrade_director";
        select_its.rdept_idx = rdept_idx;
        select_its.idx = emp_idx;
        _dtitseet.its_lookup_action[0] = select_its;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlGetits_lookup, _dtitseet);

        if (_dtitseet.its_lookup_action != null)
        {
            foreach (var item in _dtitseet.its_lookup_action)
            {
                //litDebug.Text = item.jobgrade_level.ToString() + "-" + item.rdept_idx.ToString();
                ViewState["set_jobgrade_level_dir"] = item.jobgrade_level;
                ViewState["set_rdept_idx_dir"] = item.rdept_idx;
            }
        }
        else
        {
            ViewState["set_jobgrade_level_dir"] = "";
            ViewState["set_rdept_idx_dir"] = "";
        }
    }

    private void Search_spec()
    {
        select_spec_its(ddl_spec_search);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_spec();", true);
    }
    private void setspec()
    {
        _FormView = getFv(ViewState["mode"].ToString());
        _ddlspecidx = (TextBox)_FormView.FindControl("ddlspecidx");

        if (_func_dmu.zStringToInt(ddl_spec_search.SelectedValue) > 0)
        {
            _ddlspecidx.Text = _func_dmu.zGetDataDropDownList(ddl_spec_search);
        }
        _ddlspecidx.Focus();

    }

    private int getItemFile(int type, string doccode)
    {
        int ic = 0, i = 0;
        string stype = "";
        if (type == 1)
        {
            stype = "[เอกสาร memo]" + doccode;
        }
        else if (type == 2)
        {
            stype = "[เอกสาร organization]" + doccode;
        }
        else if (type == 3)
        {
            stype = "[เอกสารการตัดเสีย]" + doccode;
        }
        else if (type == 4)
        {
            stype = "[เอกสาร อื่นๆ]" + doccode;
        }

        try
        {
            string Pathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
            Pathfile = Pathfile + doccode;
            string[] filesLoc = Directory.GetFiles(Server.MapPath(Pathfile + "/"));
            // List<System.Web.UI.WebControls.ListItem> files = new List<System.Web.UI.WebControls.ListItem>();
            foreach (string file in filesLoc)
            {
                try
                {
                    string getfiles = Path.GetFileName(file);
                    if (getfiles.Contains(stype) == true)
                    {
                        int index2 = getfiles.IndexOf('.');
                        i = (index2 - 1) - stype.Length;
                        string sItem = getfiles.Substring(stype.Length + 1, i);
                        i = _func_dmu.zStringToInt(sItem);
                        if (ic < i)
                        {
                            ic = i;
                        }
                    }
                }
                catch { }
            }
        }
        catch { }


        return ic;
    }

    private Boolean check_fileupload()
    {
        Boolean _Boolean = false;
        int itemObj = 0;
        DataSet dsU1 = (DataSet)ViewState["vsits_u1_document"];
        if (dsU1.Tables["dsits_u1_document"].Rows.Count == 0)
        {
            showMsAlert("กรุณากรอกรายการขอซื้ออุปกรณ์.!");
            _Boolean = true;

        }
        else
        {

            int buy_replacement = 0, buy_new = 0;
            foreach (DataRow item in dsU1.Tables["dsits_u1_document"].Rows)
            {
                if (_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 1) //ซื้อใหม่
                {
                    buy_new = 1;
                }
                else if ((_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 2) || (_func_dmu.zStringToInt(item["pr_type_idx"].ToString()) == 3))//ซื้อทดแทน
                {
                    buy_replacement = 1;
                }
            }
            FileUpload _uploadfile_memo;
            FileUpload _uploadfile_organization;
            FileUpload _uploadfile_cutoff;

            _FormView = getFv(ViewState["mode"].ToString());
            if (ViewState["mode"].ToString() == "E")
            {
                _uploadfile_memo = uploadfile_memoUpdate;
                _uploadfile_organization = uploadfile_organizationUpdate;
                _uploadfile_cutoff = uploadfile_cutoffUpdate;

            }
            else
            {
                _uploadfile_memo = uploadfile_memo;
                _uploadfile_organization = uploadfile_organization;
                _uploadfile_cutoff = uploadfile_cutoff;
            }

            string doccode = "";
            if (ViewState["mode"].ToString() == "E")
            {
                HiddenField hidden_doccode = (HiddenField)FvUpdateBuyNew.FindControl("hidden_doccode");
                doccode = hidden_doccode.Value;
            }

            if ((buy_replacement + buy_new) == 2)
            {

                int ic = 0;
                string sMsError = "";
                if (_uploadfile_memo.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(1, doccode);
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , Memo";
                        }
                        else
                        {
                            sMsError += "Memo";
                        }
                        ic++;
                    }

                }
                if (_uploadfile_organization.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(2, doccode);
                    }
                    else
                    {
                        icount = 1;
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , Organization";
                        }
                        else
                        {
                            sMsError += "Organization";
                        }
                        ic++;
                    }

                }
                if (_uploadfile_cutoff.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(3, doccode);
                    }
                    else
                    {
                        icount = 1;
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , ตัดเสีย";
                        }
                        else
                        {
                            sMsError += "ตัดเสีย";
                        }
                        ic++;
                    }

                }
                if (ic > 0)
                {
                    showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                    _Boolean = true;
                }
            }
            else if (buy_new > 0) //ซื้อใหม่
            {
                int ic = 0;
                string sMsError = "";
                if (_uploadfile_memo.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(1, doccode);
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , Memo";
                        }
                        else
                        {
                            sMsError += "Memo";
                        }
                        ic++;
                    }
                }
                if (_uploadfile_organization.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(2, doccode);
                    }
                    else
                    {
                        icount = 1;
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , Organization";
                        }
                        else
                        {
                            sMsError += "Organization";
                        }
                        ic++;
                    }

                }
                if (ic > 0)
                {
                    showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                    _Boolean = true;
                }
            }
            else if (buy_replacement > 0) //ซื้อทดแทน
            {
                int ic = 0;
                string sMsError = "";
                if (_uploadfile_memo.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(1, doccode);
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , Memo";
                        }
                        else
                        {
                            sMsError += "Memo";
                        }
                        ic++;
                    }
                }

                if (_uploadfile_cutoff.HasFile == false)
                {
                    int icount = 0;
                    if (ViewState["mode"].ToString() == "E")
                    {
                        icount = getItemFile(3, doccode);
                    }
                    else
                    {
                        icount = 1;
                    }
                    if (icount == 0)
                    {
                        if (ic > 0)
                        {
                            sMsError += " , ตัดเสีย";
                        }
                        else
                        {
                            sMsError += "ตัดเสีย";
                        }
                        ic++;
                    }

                }
                if (ic > 0)
                {
                    showMsAlert("กรุณาแนบเอกสาร " + sMsError + ".!");
                    _Boolean = true;
                }
            }

        }

        return _Boolean;

    }


    protected string valid(OleDbDataReader myreader, int stval)//if any columns are found null then they are replaced by zero
    {
        object val = myreader[stval];
        if (val != DBNull.Value)
            return val.ToString();
        else
            return "";
    }

    protected string valid_test(OleDbDataReader myreader, int stval)//if any columns are found null then they are replaced by zero
    {
        object val = myreader[stval];
        if (val != DBNull.Value)
            return val.ToString();
        else
            return "0";
    }

    protected void ImportFileIO(string filePath, string Extension, string isHDR, string fileName, string module)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();

        OleDbCommand ocmd = new OleDbCommand("select * from [" + SheetName + "]", connExcel);
        connExcel.Close();
        connExcel.Open();
        OleDbDataReader odr = ocmd.ExecuteReader();

        //connExcel.Close();
        //connExcel.Open();
        //cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        //oda.SelectCommand = cmdExcel;
        //oda.Fill(dt);
        //connExcel.Close();
        CreateDsits_importio();
        DataSet dsContacts = (DataSet)ViewState["vsits_m0io"];
        dsContacts = (DataSet)ViewState["vsits_m0io"];
        int ic = 0;
        while (odr.Read())
        {

            string costcenter_no = valid(odr, 0).Trim();
            if (_func_dmu.zStringToInt(costcenter_no) > 0)
            {
                string cost = "0";
                string amountio = "0";
                string budget = "0";
                string variance = "0";

                DataRow drContacts = dsContacts.Tables["dsits_m0io"].NewRow();

                ic++;

                drContacts["id"] = ic;
                drContacts["CostCenter"] = valid(odr, 0).Trim();
                drContacts["Department"] = valid(odr, 1).Trim();
                drContacts["Description"] = valid(odr, 2).Trim();
                drContacts["Cost"] = valid(odr, 3).Replace(",", "");
                drContacts["Unit"] = valid(odr, 4);
                drContacts["Amount"] = valid(odr, 5).Replace(",", "");
                drContacts["Budget"] = valid(odr, 6).Replace(",", "");
                drContacts["Variance"] = valid(odr, 7).Replace(",", "");
                drContacts["IO_Number"] = valid(odr, 8).Trim();
                drContacts["New"] = valid(odr, 9);
                drContacts["Replace"] = valid(odr, 10);

                drContacts["NoCost"] = valid(odr, 11).Trim();
                drContacts["Expenses_Year"] = valid(odr, 12).Trim();
                drContacts["Asset_Type"] = valid(odr, 13).Trim();
                drContacts["No"] = valid(odr, 14).Trim();

                if (valid(odr, 15) == "" || valid(odr, 15) == "N")
                {
                    drContacts["Status"] = 0;

                }
                else
                {
                    drContacts["Status"] = 1;

                }

                drContacts["Year"] = valid(odr, 16);
                dsContacts.Tables["dsits_m0io"].Rows.Add(drContacts);
            }



        }
        connExcel.Close();
        ViewState["vsits_m0io"] = dsContacts;

        setGridData(gv_importio, dsContacts.Tables["dsits_m0io"]);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_importio();", true);

    }

    protected void zsave_ImportFileIO()
    {
        DataSet dsU1 = (DataSet)ViewState["vsits_m0io"];
        its_TransferDevice_u1[] objcourse1 = new its_TransferDevice_u1[dsU1.Tables["dsits_m0io"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsits_m0io"].Rows)
        {
            io_itasset import = new io_itasset();
            _dtitseet.boxmaster_io = new io_itasset[1];

            import.costcenter_io = item["CostCenter"].ToString().Trim();
            import.department = item["Department"].ToString().Trim();
            import.description = item["Description"].ToString().Trim();
            import.cost = _func_dmu.zStringToDecimal(item["Cost"].ToString());
            import.unit = _func_dmu.zStringToInt(item["Unit"].ToString());
            import.amountio = _func_dmu.zStringToDecimal(item["Amount"].ToString());
            import.budget = _func_dmu.zStringToDecimal(item["Budget"].ToString());
            import.variance = _func_dmu.zStringToDecimal(item["Variance"].ToString());
            import.iono = item["IO_Number"].ToString().Trim();
            import.newio = _func_dmu.zStringToInt(item["New"].ToString());
            import.replaceio = _func_dmu.zStringToInt(item["Replace"].ToString());
            import.no_cost = item["NoCost"].ToString().Trim();
            import.expenses_year = item["Expenses_Year"].ToString();
            import.asset_type = item["Asset_Type"].ToString().Trim();
            import.no_order = item["No"].ToString().Trim();
            import.status = _func_dmu.zStringToInt(item["Status"].ToString());
            import.year_io = _func_dmu.zStringToInt(item["Year"].ToString());


            import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            _dtitseet.boxmaster_io[0] = import;
            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlInsert_Master_IOBuy, _dtitseet);

        }

    }



    protected void CreateDsits_importio()
    {
        string sDs = "dsits_m0io";
        string sVs = "vsits_m0io";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("CostCenter", typeof(String));
        ds.Tables[sDs].Columns.Add("Department", typeof(String));
        ds.Tables[sDs].Columns.Add("Description", typeof(String));
        ds.Tables[sDs].Columns.Add("Cost", typeof(String));
        ds.Tables[sDs].Columns.Add("Unit", typeof(String));
        ds.Tables[sDs].Columns.Add("Amount", typeof(String));
        ds.Tables[sDs].Columns.Add("Budget", typeof(String));
        ds.Tables[sDs].Columns.Add("Variance", typeof(String));
        ds.Tables[sDs].Columns.Add("IO_Number", typeof(String));
        ds.Tables[sDs].Columns.Add("New", typeof(String));
        ds.Tables[sDs].Columns.Add("Replace", typeof(String));
        ds.Tables[sDs].Columns.Add("NoCost", typeof(String));
        ds.Tables[sDs].Columns.Add("Expenses_Year", typeof(String));
        ds.Tables[sDs].Columns.Add("Asset_Type", typeof(String));
        ds.Tables[sDs].Columns.Add("No", typeof(String));
        ds.Tables[sDs].Columns.Add("Status", typeof(String));
        ds.Tables[sDs].Columns.Add("Year", typeof(String));
        ViewState[sVs] = ds;

    }

    protected void zShowUploadFileAsset(string _str)
    {
        ViewState["v_uploadfile_asset"] = _str;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_UploadFile_Asset();", true);
    }


    protected void zAddUploadFileAsset(string _str)
    {
        TextBox lbdocument_code = (TextBox)FvDetail_ShowDelivery.FindControl("lbdocument_code");
        TextBox txt_sysidx = (TextBox)FvDetail_ShowDelivery.FindControl("txt_sysidx");
        string _returndocumentcode = "", _returnAssetNo = "";
        string[] _argument = new string[8];
        _argument = _str.ToString().Split('|');

        _returndocumentcode = _argument[0];
        _returnAssetNo = _argument[3];
        if (_returndocumentcode != "")
        {
            string sfile_other = "";

            string getPathfile1 = ConfigurationManager.AppSettings["pathfile_itasset"];
            getPathfile1 = getPathfile1 + "/" + _path_assetfile;
            string document_code1 = _returndocumentcode;
            string filePath_upload1 = Server.MapPath(getPathfile1 + document_code1 + "/" + _returnAssetNo);

            if (!Directory.Exists(filePath_upload1))
            {
                Directory.CreateDirectory(filePath_upload1);
            }


            if (uploadfile_Asset.HasFile)
            {

                //string getPathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
                string document_code = _returnAssetNo;
                string fileName_upload = "[เอกสารทรัพย์สิน]" + document_code;
                // string filePath_upload = Server.MapPath(filePath_upload1 );

                int ic = 1;
                foreach (HttpPostedFile uploadedFile in uploadfile_Asset.PostedFiles)
                {
                    try
                    {
                        if (uploadedFile.ContentLength > 0)
                        {
                            string extension = Path.GetExtension(uploadedFile.FileName);
                            sfile_other = fileName_upload + "-" + ic.ToString() + extension;
                            uploadedFile.SaveAs(filePath_upload1 + "\\" + sfile_other);

                        }
                    }
                    catch (Exception Ex)
                    {

                    }
                    ic++;
                }


            }

            GridView gvItemsIts_Delivery = (GridView)FvDetail_ShowDelivery.FindControl("gvItemsIts_Delivery");
            setGridData(gvItemsIts_Delivery, ViewState["vsits_u1_document"]);

        }
    }
    protected void zDeleteUploadFileAsset(string _str)
    {
        try
        {
            string Pathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
            string filesLoc = Server.MapPath(Pathfile + "/" + _path_assetfile + "/" + _str);
            File.Delete(filesLoc);
        }
        catch { }
        try
        {

            GridView gvItemsIts_Delivery = (GridView)FvDetail_ShowDelivery.FindControl("gvItemsIts_Delivery");
            setGridData(gvItemsIts_Delivery, ViewState["vsits_u1_document"]);
        }
        catch
        {

        }
    }
    /******* Upload file asset *******/

    protected void zAddUploadFileAsset1(string _str)
    {
        string _returndocumentcode = "", _returnAssetNo = "";
        string[] _argument = new string[8];
        _argument = _str.ToString().Split('|');
        _returndocumentcode = _argument[0];
        _returnAssetNo = _argument[3];
        if (_returndocumentcode != "")
        {
            string sfile_other = "";
            string getPathfile1 = ConfigurationManager.AppSettings["pathfile_itasset"];
            getPathfile1 = getPathfile1 + "/" + _path_assetfile;
            string document_code1 = _returndocumentcode;
            string filePath_upload1 = Server.MapPath(getPathfile1 + document_code1 + "/" + _returnAssetNo);
            if (!Directory.Exists(filePath_upload1))
            {
                Directory.CreateDirectory(filePath_upload1);
            }
            if (uploadfile_Asset.HasFile)
            {
                string document_code = _returnAssetNo;
                string fileName_upload = "[เอกสารทรัพย์สิน]" + document_code;
                int ic = 1;
                foreach (HttpPostedFile uploadedFile in uploadfile_Asset.PostedFiles)
                {
                    try
                    {
                        if (uploadedFile.ContentLength > 0)
                        {
                            string extension = Path.GetExtension(uploadedFile.FileName);
                            sfile_other = fileName_upload + "-" + ic.ToString() + extension;
                            uploadedFile.SaveAs(filePath_upload1 + "\\" + sfile_other);

                        }
                    }
                    catch (Exception Ex)
                    {

                    }
                    ic++;
                }


            }

            GridView gvItemsIts_Delivery = (GridView)FvDetail_ShowAsset.FindControl("gvItemsIts_Asset");
            setGridData(gvItemsIts_Delivery, ViewState["vsits_u1_document"]);

        }
    }
    protected void zDeleteUploadFileAsset1(string _str)
    {
        try
        {
            string Pathfile = ConfigurationManager.AppSettings["pathfile_itasset"];
            string filesLoc = Server.MapPath(Pathfile + "/" + _path_assetfile + "/" + _str);
            File.Delete(filesLoc);
        }
        catch { }
        try
        {

            GridView gvItemsIts_Delivery = (GridView)FvDetail_ShowAsset.FindControl("gvItemsIts_Asset");
            setGridData(gvItemsIts_Delivery, ViewState["vsits_u1_document"]);
        }
        catch
        {

        }
    }

    protected string getHtml_ReportAsset()
    {
        if (ddlSearchReport.SelectedValue == "2")
        {
            //if (hidden_showasset.Value == "1")
            //{
            // view View_its_u0_document
            hidden_showasset.Value = "0";

            _dtitseet.its_u_document_action = new its_u_document[1];
            its_u_document select_document = new its_u_document();

            //ค้นหาตามวันที่
            select_document.condition_date_type = _func_dmu.zStringToInt(ddlcondition.SelectedValue);
            if (txtstartdate.Text.Trim() != "")
            {
                select_document.start_date = _func_dmu.zDateToDB(txtstartdate.Text.Trim());
            }
            if (txtenddate.Text.Trim() != "")
            {
                select_document.end_date = _func_dmu.zDateToDB(txtenddate.Text.Trim());
            }

            select_document.doccode = txtdocumentcode.Text.Trim();
            select_document.emp_name_th = txtfirstname_lastname.Text.Trim();
            select_document.emp_code = txtempcode.Text.Trim();
            select_document.org_idx_its = _func_dmu.zStringToInt(ddlsearch_organization.SelectedValue);
            select_document.rdept_idx_its = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
            select_document.rdept_idx_search = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
            select_document.place_idx = _func_dmu.zStringToInt(ddllocation_search.SelectedValue);
            select_document.costidx = _func_dmu.zStringToInt(ddl_costcenter.SelectedValue);
            select_document.pr_type_idx = _func_dmu.zStringToInt(ddlsearch_typepurchase.SelectedValue);
            select_document.typidx = _func_dmu.zStringToInt(ddlsearch_typequipment.SelectedValue);
            select_document.doc_status = _func_dmu.zStringToInt(ddlsearch_status.SelectedValue);

            select_document.ionum = txtsearch_ionum.Text.Trim();
            select_document.asset_no = txtsearch_asset_no.Text.Trim();

            //select_document.rdept_idx_admin = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());

            select_document.zstatus = zModeStatus();
            select_document.emp_idx = emp_idx;
            //select_document.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
            //select_document.locidx = _func_dmu.zStringToInt(ViewState["locidx"].ToString());
            select_document.operation_status_id = "asset_report";
            _dtitseet.its_u_document_action[0] = select_document;
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            string sHtml = "", sHtml_tr = "", sHtml_td = "";
            if (_dtitseet.its_u_document_action != null)
            {
                int ic = 0;
                foreach (var item in _dtitseet.its_u_document_action)
                {
                    ic++;
                    sHtml_td = "";
                    sHtml_td = sHtml_td + "<td>" + ic + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.zyear + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.doccode + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.zdocdate + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.zapp_remark + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.emp_code + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.emp_name_th + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.SysNameTH + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.type_name + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.zdevices_name + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.name_purchase_type + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.ionum + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.dept_name_th + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.pos_name + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.leveldevice + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.list_menu + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.qty + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.NameTH + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.price + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.currency_th + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.total_amount + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.budget_set + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.CostNo + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.place_name + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.asset_no + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.spec_remark + "</td>";

                    //sHtml_td = sHtml_td + "<td>" + item.total_amount + "</td>";
                    sHtml_tr = "<tr>" + sHtml_td + "</tr>";
                    sHtml = sHtml + sHtml_tr;
                }


            }
            return sHtml;
            //}
            //else
            //{
            //    return "";
            //}
        }
        else
        {
            return "<tr></tr>";
        }
    }

    protected string getHtml_ReportAsset_io()
    {
        //if (hidden_showasset.Value == "1")
        //{
        // view View_its_u0_document
        if (ddlSearchReport.SelectedValue == "1")
        {
            hidden_showasset.Value = "0";

            _dtitseet.its_u_document_action = new its_u_document[1];
            its_u_document select_document = new its_u_document();

            //ค้นหาตามวันที่
            select_document.condition_date_type = _func_dmu.zStringToInt(ddlcondition.SelectedValue);
            if (txtstartdate.Text.Trim() != "")
            {
                select_document.start_date = _func_dmu.zDateToDB(txtstartdate.Text.Trim());
            }
            if (txtenddate.Text.Trim() != "")
            {
                select_document.end_date = _func_dmu.zDateToDB(txtenddate.Text.Trim());
            }

            //select_document.doccode = txtdocumentcode.Text.Trim();
            //select_document.emp_name_th = txtfirstname_lastname.Text.Trim();
            //select_document.emp_code = txtempcode.Text.Trim();
            //select_document.org_idx_its = _func_dmu.zStringToInt(ddlsearch_organization.SelectedValue);
            //select_document.rdept_idx_its = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
            //select_document.rdept_idx_search = _func_dmu.zStringToInt(ddlsearch_department.SelectedValue);
            select_document.place_idx = _func_dmu.zStringToInt(ddlBalanceIO.SelectedValue);
            select_document.zyear = _func_dmu.zStringToInt(ddlsearch_yeario1.SelectedValue);
            select_document.costidx = _func_dmu.zStringToInt(ddl_costcenter.SelectedValue);
            select_document.pr_type_idx = _func_dmu.zStringToInt(ddlsearch_typepurchase.SelectedValue);
            //select_document.typidx = _func_dmu.zStringToInt(ddlsearch_typequipment.SelectedValue);
            //select_document.doc_status = _func_dmu.zStringToInt(ddlsearch_status.SelectedValue);

            select_document.ionum = txtsearch_ionum.Text.Trim();
            //  select_document.asset_no = txtsearch_asset_no.Text.Trim();

            //select_document.rdept_idx_admin = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());

            select_document.zstatus = zModeStatus();
            select_document.emp_idx = emp_idx;
            //select_document.sysidx_admin = _func_dmu.zStringToInt(ViewState["sysidx_admin"].ToString());
            //select_document.locidx = _func_dmu.zStringToInt(ViewState["locidx"].ToString());
            select_document.operation_status_id = "asset_io_balance_report";
            _dtitseet.its_u_document_action[0] = select_document;
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlGetits_u_document, _dtitseet);
            string sHtml = "", sHtml_tr = "", sHtml_td = "";
            if (_dtitseet.its_u_document_action != null)
            {
                int ic = 0;
                foreach (var item in _dtitseet.its_u_document_action)
                {
                    ic++;
                    sHtml_td = "";
                    sHtml_td = sHtml_td + "<td>" + ic + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.zyear + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.CostNo + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.department + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.description + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.cost + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.unit + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.amountio + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.budget + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.variance + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.ionum + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.name_purchase_type + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.qty + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.out_qty + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.balance_qty + "</td>";
                    sHtml_td = sHtml_td + "<td>" + item.doccode + "</td>";

                    //sHtml_td = sHtml_td + "<td>" + item.total_amount + "</td>";
                    sHtml_tr = "<tr>" + sHtml_td + "</tr>";
                    sHtml = sHtml + sHtml_tr;
                }


            }
            return sHtml;
            //}
            //else
            //{
            //    return "";
            //}

        }
        else
        {
            return "<tr></tr>";
        }
    }
}