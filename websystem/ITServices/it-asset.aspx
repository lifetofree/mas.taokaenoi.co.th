﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it-asset.aspx.cs" Inherits="websystem_ITServices_it_asset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToDivIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>
                    <li id="_divMenuLiToDivBuy" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ขอซื้ออุปกรณ์ <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToDivBuyNew" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivBuyNew" runat="server"
                                    CommandName="_divMenuBtnToDivBuyNew"
                                    OnCommand="btnCommand" Text="ขอซื้อใหม่" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivBuyReplace" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivBuyReplace" runat="server"
                                    CommandName="_divMenuBtnToDivBuyReplace"
                                    OnCommand="btnCommand" Text="ขอซื้อทดแทน" />
                            </li>
                        </ul>
                    </li>
                    <li id="_divMenuLiToDivDevices" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivCutDevices" runat="server"
                            CommandName="_divMenuBtnToDivCutDevices"
                            OnCommand="btnCommand" Text="ตัดชำรุดอุปกรณ์" />
                    </li>
                    <li id="_divMenuLiToDivChangeOwn" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivChangeOwn" runat="server"
                            CommandName="_divMenuBtnToDivChangeOwn"
                            OnCommand="btnCommand" Text="โอนย้ายอุปกรณ์" />
                    </li>
                    <li id="_divMenuLiToDivWaitApprove" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการรออนุมัติ <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToDivWaitApprove_Buy" runat="server">
                                <asp:LinkButton ID="LinkButton7" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_Buy"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติซื้ออุปกรณ์" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivWaitApprove_CutDevices" runat="server">
                                <asp:LinkButton ID="LinkButton8" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_CutDevices"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติตัดเสียอุปกรณ์" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivWaitApprove_ChangeOwn" runat="server">
                                <asp:LinkButton ID="LinkButton9" runat="server"
                                    CommandName="_divMenuBtnToDivWaitApprove_ChangeOwn"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติโอนย้ายอุปกรณ์" />
                            </li>
                        </ul>
                    </li>



                    <li id="_divMenuLiToDivBudget" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ส่วนจัดการ Budget <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToDivMasterBudget" runat="server">
                                <asp:LinkButton ID="LinkButton10" runat="server"
                                    CommandName="_divMenuBtnToDivBudget_master"
                                    OnCommand="btnCommand" Text="Master Data Class" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivBudget_system" runat="server">
                                <asp:LinkButton ID="LinkButton11" runat="server"
                                    CommandName="_divMenuBtnToDivBudget_system"
                                    OnCommand="btnCommand" Text="จัดการระบบ Asset" />
                            </li>
                        </ul>
                    </li>



                    <li id="_divMenuLiToDivAsset" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ส่วนจัดการ Asset <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToDivAsset_master" runat="server">
                                <asp:LinkButton ID="LinkButton5" runat="server"
                                    CommandName="_divMenuBtnToDivAsset_master"
                                    OnCommand="btnCommand" Text="Master Data Class" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivAsset_system" runat="server">
                                <asp:LinkButton ID="LinkButton6" runat="server"
                                    CommandName="_divMenuBtnToDivAsset_system"
                                    OnCommand="btnCommand" Text="จัดการระบบ Asset" />
                            </li>
                        </ul>
                    </li>

                    <li id="_divMenuLiToDivDataAsset" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ข้อมูล Asset <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_divMenuLiToDivDataGiveAsset" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivDataGiveAsset" runat="server"
                                    CommandName="_divMenuBtnToDivDataGiveAsset"
                                    OnCommand="btnCommand" Text="ข้อมูลของบประมาณ" />
                            </li>

                            <li role="separator" class="divider"></li>
                            <li id="_divMenuLiToDivDataBuyAsset" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivDataAsset" runat="server"
                                    CommandName="_divMenuBtnToDivDataAsset"
                                    OnCommand="btnCommand" Text="ข้อมูลขอซื้อ Asset" />
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">
        </asp:View>

        <asp:View ID="ViewBuy" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_createbuynew" runat="server" visible="false">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-shopping-cart"></i><strong>&nbsp; รายการขอซื้ออุปกรณ์ใหม่
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertBuyNew" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="ทรัพย์สิน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รายการ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtlist_buynew" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_buynew"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtqty_buynew" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtqty_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_buynew"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="มูลค่า : " />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtprice_buynew" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtprice_buynew" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_buynew"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="Label3" CssClass="form-control text-center" Enabled="false" runat="server" Text="บาท " />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAddBuy_Newlist" OnCommand="btnCommand" ValidationGroup="SaveAdd" title="Save"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:GridView ID="GvReportAdd"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="list_buynew" HeaderText="รายการ" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="qty_buynew" HeaderText="จำนวน" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="price_buynew" HeaderText="ราคา" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImagesSAP" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_buynew" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_createbuyreplace" runat="server" visible="false">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-shopping-cart"></i><strong>&nbsp; รายการขอซื้ออุปกรณ์ทดแทน
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertBuyReplace" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="ทรัพย์สิน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsystem_replace" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem_replace" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="เลขที่ใบตัดชำรุด : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddldoccode" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddldoccode" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลขที่ใบแจ้งซ่อม"
                                                ValidationExpression="กรุณาเลือกเลขที่ใบแจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รายการ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtlist_buyreplace" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_buyreplace"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtqty_buyreplace" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtqty_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtqty_buyreplace"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="มูลค่า : " />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtprice_buyreplace" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txtprice_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice_buyreplace"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="Label3" CssClass="form-control text-center" Enabled="false" runat="server" Text="บาท " />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAddBuy_Replacelist" OnCommand="btnCommand" ValidationGroup="SaveAdd" title="Save"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:GridView ID="GvReportAdd_BuyReplace"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="list_buyreplace" HeaderText="รายการ" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="qty_buyreplace" HeaderText="จำนวน" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="price_buyreplace" HeaderText="ราคา" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtremark_buyreplace" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark_buyreplace" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_buyreplace"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labesl10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImages_BuyReplace" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_buyreplace" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12" id="div_createcutdevices" runat="server" visible="false">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-scissors"></i><strong>&nbsp; รายการตัดเสียอุปกรณ์
                        </strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertCutDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="ทรัพย์สิน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsystem_cutdevices" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlsystem_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภททรัพย์สิน"
                                                ValidationExpression="กรุณาเลือกประเภททรัพย์สิน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" id="div_doccode" runat="server" visible="false">
                                        <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="เลขที่รายการแจ้งซ่อม : " />
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddldoccode_cutdevices" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">IT01610001</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddldoccode_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกเลขที่ใบแจ้งซ่อม"
                                                ValidationExpression="กรุณาเลือกเลขที่ใบแจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รหัสทรัพย์สิน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtasset_cutdevices" Text="35000002" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label18" CssClass="col-sm-2 control-label" runat="server" Text="รายการ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtlist_cutdevices" Text="เครื่องโน๊ตบุ๊ค 16 GB HDD 1 TB " Enabled="false" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="Book.Val : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbook" CssClass="form-control" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtlist_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtlist_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุ : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtreason_cutdevices" Text="เนื่องจากระยะเวลาในการใช้งาน และเครื่องโน๊ตบุ๊คเกิดการเสื่อมสภาพ" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtreason_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายการ"
                                                ValidationExpression="กรุณากรอกรายการ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtreason_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAddBuy_Cutlist" OnCommand="btnCommand" ValidationGroup="SaveAdd" title="Save"><i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-8">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">หมวดทรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Asset Num.</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Acquls. Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Book Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Ref IT Num</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Action</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>Z1100</td>
                                                        <td>35000002</td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">เครื่องโน๊ตบุ๊ค 16 GB HDD 1 TB</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>35000</td>
                                                        <td>-</td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.549085903</label></td>
                                                        <td>-</td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>


                                    <%--  <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-8">

                                            <asp:GridView ID="GvReportAdd_BuyReplace"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="list_buyreplace" HeaderText="รายการ" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="qty_buyreplace" HeaderText="จำนวน" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="price_buyreplace" HeaderText="ราคา" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>


                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </div>--%>

                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtremark_cutdevices" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd_BuyList" runat="server" Display="None" ControlToValidate="txtremark_cutdevices" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd_BuyList" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_cutdevices"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labeesl10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImages_CutDevices" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group" id="div_btn_cutdevices" runat="server" visible="true">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewCutDevices" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_CutDevieces" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewBudget" runat="server">
            <div class="col-lg-12" id="gvbudget" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i><strong>&nbsp; รายการรอการจัดการ Budget</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รหัสเอกสาร</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">วันที่สร้าง</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ข้อมูลผู้ทำรายการ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">หมายเหตุ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">สถานะ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รายละเอียด</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">DP610001</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">04/04/2018 12:30:45</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">นางสาว กานต์ธิดา ตระกูลบุญรักษ์</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">ทดสอบระบบ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">พิจารณาผลโดย Budget</label></td>
                                        <td>
                                            <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"><i class="fa fa-file-text-o" ></i></asp:LinkButton>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>


                            <%--     <asp:GridView ID="GvListBudget"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="URQIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </small>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="DP610001" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="IT" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label12" runat="server">วันที่: </asp:Label></strong>
                                                <asp:Literal ID="Literal4" runat="server" Text="04/04/2018" />
                                                </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">เวลา: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text="12:30:45" />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                    <strong>
                                                        <asp:Label ID="Label14" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text="พัฒนาแอพพลิเคชั่น" />
                                                    <strong>
                                                        <asp:Label ID="Label16" runat="server">ชื่อผู้ทำรายการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text="นางสาว กานต์ธิดา ตระกูลบุญรักษ์" />
                                                </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblsecname" runat="server" Text="ทดสอบระบบ"></asp:Label>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailbudget" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Budget" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" Text="58000070" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Text="การจัดการระบบสารสนเทศ" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Text="พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Text="เจ้าหน้าที่พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Text="0924235995" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Text="webmaster@taokaenoi.co.th" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <hr />

                    <asp:FormView ID="FvDetail_ShowBudget" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtlist_buynew" Enabled="false" Text="DP610001" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Labeไdle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtreไmark" Text="ทดสอบระบบ" Enabled="false" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-9">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">Notebook ram 16 gb hdd 1 tb</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">35000</label></td>
                                                        <td>
                                                            <asp:TextBox ID="txtionum" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">2</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">HR</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">เก้าอี้ตาข่ายดำ 1 ตัว</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1000</label></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox16" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labeel10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImagesBudget" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewbudget" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewAsset" runat="server">
            <div class="col-lg-12" id="gvasset" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i><strong>&nbsp; รายการรอการจัดการ Asset</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <table id="PanelBody_YrChkBoxColumns1" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รหัสเอกสาร</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">วันที่สร้าง</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ข้อมูลผู้ทำรายการ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">หมายเหตุ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">สถานะ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รายละเอียด</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">DP610001</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">04/04/2018 12:30:45</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">นางสาว กานต์ธิดา ตระกูลบุญรักษ์</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">ทดสอบระบบ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">พิจารณาผลโดย Asset</label></td>
                                        <td>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailasset" OnCommand="btnCommand"><i class="fa fa-file-text-o" ></i></asp:LinkButton>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>


                            <%--     <asp:GridView ID="GvListBudget"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="URQIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </small>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="DP610001" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="IT" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label12" runat="server">วันที่: </asp:Label></strong>
                                                <asp:Literal ID="Literal4" runat="server" Text="04/04/2018" />
                                                </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">เวลา: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text="12:30:45" />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                    <strong>
                                                        <asp:Label ID="Label14" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text="พัฒนาแอพพลิเคชั่น" />
                                                    <strong>
                                                        <asp:Label ID="Label16" runat="server">ชื่อผู้ทำรายการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text="นางสาว กานต์ธิดา ตระกูลบุญรักษ์" />
                                                </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblsecname" runat="server" Text="ทดสอบระบบ"></asp:Label>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailasset" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Asset" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" Text="58000070" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Text="การจัดการระบบสารสนเทศ" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Text="พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Text="เจ้าหน้าที่พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Text="0924235995" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Text="webmaster@taokaenoi.co.th" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <hr />

                    <asp:FormView ID="FvDetail_ShowAsset" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtlist_buynew" Enabled="false" Text="DP610001" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" Text="ทดสอบระบบ" Enabled="false" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="ความคิดเห็นหัวหน้าแผนก : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox12" Text="ผ่านอนุมัติ หัวหน้าแผนก" Enabled="false" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="ความคิดเห็นผู้จัดการฝ่าย : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox13" Text="ผ่านอนุมัติ ผู้จัดการฝ่าย" Enabled="false" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-9">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">หมวดทรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Asset Num.</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Acquls. Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Book Val</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Ref IT Num</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddltype" CssClass="form-control">
                                                                <asp:ListItem Value="1">Z1100</asp:ListItem>
                                                                <asp:ListItem Value="2">Z1200</asp:ListItem>
                                                                <asp:ListItem Value="1">Z1300</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2100</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2200</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2300</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">Notebook ram 16 gb hdd 1 tb</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox14" runat="server" Text="35,000.00" CssClass="form-control" /></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" /></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.549085903</label></td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">2</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">HR</label></td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddltype1" CssClass="form-control">
                                                                <asp:ListItem Value="1">Z1100</asp:ListItem>
                                                                <asp:ListItem Value="2">Z1200</asp:ListItem>
                                                                <asp:ListItem Value="1">Z1300</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2100</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2200</asp:ListItem>
                                                                <asp:ListItem Value="1">Z2300</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">เก้าอี้ตาข่ายดำ 1 ตัว</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox7" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox8" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox15" runat="server" Text="1,000.00" CssClass="form-control" /></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox9" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.5357868578</label></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox17" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Labefl10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImagesAsset" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewasset" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewAsset_Master" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-book"></i><strong>&nbsp; Class Asset</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">

                            <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Class" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                        </div>

                        <%------------------------ Div ADD  ------------------------%>

                        <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Class</strong></h4>
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" Text="Class" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveMaster" runat="server" Display="None" ControlToValidate="txtcode" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="SaveMaster" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcode"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="Asset class description" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtname" runat="server" CssClass="form-control" PlaceHolder="........" />
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveMaster" runat="server" Display="None" ControlToValidate="txtname" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูล"
                                                ValidationExpression="กรุณากรอกข้อมูล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveMaster" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtname"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="lbladd" ValidationGroup="SaveMaster" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAddMaster" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        </asp:Panel>

                        <asp:GridView ID="GvMaster" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="primary"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            DataKeyNames="clidx"
                            PageSize="10"
                            OnRowDataBound="Master_RowDataBound"
                            OnRowEditing="Master_RowEditing"
                            OnRowCancelingEdit="Master_RowCancelingEdit"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowUpdating="Master_RowUpdating">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="#">

                                    <ItemTemplate>
                                        <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("clidx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>


                                    <EditItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="panel-heading">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                        <asp:TextBox ID="txtclidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("clidx")%>' />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("code_class")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtname_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtcode_edit" runat="server" CssClass="form-control" Text='<%# Eval("code_desc")%>' />
                                                    </div>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcode_edit" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกข้อมูล"
                                                        ValidationExpression="กรุณากรอกข้อมูล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        ValidationGroup="Save_edit" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtcode_edit"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                    <div class="col-sm-8">
                                                        <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("code_status") %>'>
                                                            <asp:ListItem Value="1" Text="Online" />
                                                            <asp:ListItem Value="0" Text="Offline" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-10">
                                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </EditItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("code_class") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("code_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("Class_status") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("clidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>

                                    <EditItemTemplate />
                                    <FooterTemplate />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12" id="gvapprove" runat="server" visible="false">
                <div class="panel panel-warning ">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-check"></i><strong>&nbsp; รายการรอนอุมัติ</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">


                            <table id="PanelBody_YrChkBoxsColumns1" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รหัสเอกสาร</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">วันที่สร้าง</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">ข้อมูลผู้ทำรายการ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">หมายเหตุ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">สถานะ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_0">รายละเอียด</label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">DP610001</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">04/04/2018 12:30:45</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">นางสาว กานต์ธิดา ตระกูลบุญรักษ์</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">ทดสอบระบบ</label></td>
                                        <td>
                                            <label for="PanelBody_YrChkBoxColumns_1">พิจารณาผลโดยหัวหน้าผู้สร้าง</label></td>
                                        <td>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailapprove" OnCommand="btnCommand"><i class="fa fa-file-text-o" ></i></asp:LinkButton>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>


                            <%--     <asp:GridView ID="GvListBudget"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="URQIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                            </small>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="DP610001" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภททรัพย์สิน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text="IT" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label12" runat="server">วันที่: </asp:Label></strong>
                                                <asp:Literal ID="Literal4" runat="server" Text="04/04/2018" />
                                                </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">เวลา: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text="12:30:45" />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                    <strong>
                                                        <asp:Label ID="Label14" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text="พัฒนาแอพพลิเคชั่น" />
                                                    <strong>
                                                        <asp:Label ID="Label16" runat="server">ชื่อผู้ทำรายการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text="นางสาว กานต์ธิดา ตระกูลบุญรักษ์" />
                                                </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblsecname" runat="server" Text="ทดสอบระบบ"></asp:Label>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btndetailbudget" runat="server" CssClass="btn btn-info btn-xs" CommandName="btndetailbudget" OnCommand="btnCommand"></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>--%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="detailapprove" runat="server" visible="false">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser_Approve" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" Text="58000070" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" Text="เถ้าแก่น้อย ฟู๊ด แอนด์ มาร์เก็ตติ้ง จำกัด(มหาชน)" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Text="การจัดการระบบสารสนเทศ" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Text="พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Text="เจ้าหน้าที่พัฒนาแอพพลิเคชั่น" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Text="0924235995" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Text="webmaster@taokaenoi.co.th" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                    <hr />

                    <asp:FormView ID="FvDetail_ShowApprove" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtlist_buynew" Enabled="false" Text="DP610001" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>
                                    </div>

                                    <div id="div_showreasoncut" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label21" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่รายการแจ้งซ่อม : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="TextBox18" Enabled="false" Text="IT01610001" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label22" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุ : " />
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="TextBox19" Enabled="false" TextMode="multiline" Rows="3" Text="เนื่องจากระยะเวลาในการใช้งาน และเครื่องโน๊ตบุ๊คเกิดการเสื่อมสภาพ" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" Text="ทดสอบระบบ" Enabled="false" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-9">

                                            <table id="PanelBody_YrChkBoxColumns" class="table table-striped table-bordered table-hover table-responsive col-lg-12" style="width: 100%; text-align: center">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ลำดับ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">ประเภททรัพย์สิน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">รายการ</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">จำนวน</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">Price</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_0">IO.Num</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IT</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">Notebook ram 16 gb hdd 1 tb</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">1</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">35000</label></td>
                                                        <td>
                                                            <label for="PanelBody_YrChkBoxColumns_1">IO.549085903</label></td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label20" class="col-sm-2 control-label" runat="server" Text="สถานะดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlapprove" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะดำเนินการ...</asp:ListItem>
                                                <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox17" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd_BuyList" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack_viewapprove" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>



        </asp:View>

        <asp:View ID="ViewDataAsset" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-import"></i><strong>&nbsp; Import Data Asset</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <%--<div class="col-sm-1">--%>
                            <asp:LinkButton ID="CmdAdd_Import" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add Import Asset" runat="server" CommandName="CmdAdd_Import" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            <asp:LinkButton ID="CmdAdd_Search" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Search Asset" runat="server" CommandName="CmdAdd_Search" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>

                            <%-- </div>
                            <div class="col-sm-1">
                            </div>--%>
                        </div>

                        <%------------------------ Div ADD  ------------------------%>

                        <asp:Panel ID="Panel_ImportAdd" runat="server" Visible="false">

                            <asp:UpdatePanel ID="upActor1Node1Files11" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แนบไฟล์ Excel</label>
                                        <div class="col-sm-10">
                                            <asp:FileUpload ID="upload" runat="server" AutoPostBack="false" />

                                            <asp:RequiredFieldValidator ID="requiredFileUpload"
                                                runat="server" ValidationGroup="Create_Actor1file"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="upload"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณาเลือกไฟล์" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredFileUpload" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                    </div>

                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImport" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel ID="upActor1Node1Files1" runat="server">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <div class="col-sm-2  col-sm-offset-2">
                                            <asp:LinkButton ID="btnImport" ValidationGroup="SaveMaster" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnImport" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i> Import</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton13" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_Import" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </ContentTemplate>

                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImport" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <asp:Panel ID="Panel_SearchImport" runat="server">
                        </asp:Panel>
                    </div>
                    <div class="panel-body">
                        <asp:GridView ID="GvAsset_Buy" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="primary"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            DataKeyNames="asidx"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="#">

                                    <ItemTemplate>
                                        <asp:Label ID="lblasidx" runat="server" Visible="false" Text='<%# Eval("asidx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Asset" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("Asset") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SNo" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("SNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="CostName" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbCostName" runat="server" Text='<%# Eval("CostCenter") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="LocName" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbLocName" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cap.Date" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbCapDate" runat="server" Text='<%# Eval("CapDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Asset Description" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbassetdes" runat="server" Text='<%# Eval("Asset_Description") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Acquis.Val." ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbacq" runat="server" Text='<%# Eval("AcquisVal") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("AssetDetailStatus") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </div>
                </div>


            </div>

        </asp:View>
    </asp:MultiView>

</asp:Content>

