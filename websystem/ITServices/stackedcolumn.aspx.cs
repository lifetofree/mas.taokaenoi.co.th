﻿using System;using System.Collections.Generic;using System.Data;using System.Data.SqlClient;using System.Linq;using System.Text;using System.Web;using System.Web.UI;using System.Web.UI.DataVisualization.Charting;using System.Web.UI.WebControls;

public partial class websystem_ITServices_stackedcolumn : System.Web.UI.Page
{

    StringBuilder str = new StringBuilder();
    //Get connection string from web.config
    //SqlConnection conn = new SqlConnection("Data source=172.16.11.5; Initial catalog=MIS; Persist Security Info=True;User ID=webdev;Password=Y$PP9Z6hCw");

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    // dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();
    data_chr datachr = new data_chr();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    string sum;

    protected void Page_Load(object sender, EventArgs e)
    {

        BindChart();

    }

    private void Stack()    {        var dsEquipment_cpu = new DataSet();        dsEquipment_cpu.Tables.Add("device_cpu");        dsEquipment_cpu.Tables[0].Columns.Add("Dep_Name", typeof(string));        dsEquipment_cpu.Tables[0].Columns.Add("Dep", typeof(string));        dsEquipment_cpu.Tables[0].Columns.Add("Poin", typeof(string));        ViewState["vsTemp"] = dsEquipment_cpu;        DataTable dt = new DataTable();        dt.Clear();        dt.Columns.Add("Dep");        dt.Columns.Add("Dep_Name");        dt.Columns.Add("RDep");        dt.Columns.Add("Poin");        DataRow _ravi1 = dt.NewRow();        _ravi1["Dep"] = "FM";        _ravi1["Dep_Name"] = "MIS";        _ravi1["RDep"] = "20";        _ravi1["Poin"] = "2";        dt.Rows.Add(_ravi1);        DataRow _ravi = dt.NewRow();        _ravi["Dep"] = "FM";        _ravi1["Dep_Name"] = "HR";        _ravi["RDep"] = "10";        _ravi["Poin"] = "1";        dt.Rows.Add(_ravi);        DataRow _ravi2 = dt.NewRow();        _ravi2["Dep"] = "FM";        _ravi1["Dep_Name"] = "QA";        _ravi2["RDep"] = "30";        _ravi2["Poin"] = "3";        dt.Rows.Add(_ravi2);        string Loop_temp = "20,10"; //เลข Rdept
        string temp = "";        string name = "";        string[] ToId = Loop_temp.Split(',');        foreach (string Loop in ToId)        {            foreach (DataRow dr in dt.Rows)            {                name = dr["Dep_Name"].ToString();                if (Loop == dr["RDep"].ToString())                {                    temp += dr["Poin"].ToString() + ",";                    ViewState["Po"] = temp;                    var dsEquiment_etc = (DataSet)ViewState["vsTemp"];                    var drEquiment_etc = dsEquiment_etc.Tables[0].NewRow();                    drEquiment_etc["Dep_Name"] = Loop;//dr["Dep_Name"].ToString();
                    drEquiment_etc["Dep"] = dr["Dep"].ToString();                    drEquiment_etc["Poin"] = ViewState["Po"].ToString();                    dsEquiment_etc.Tables[0].Rows.Add(drEquiment_etc);                    ViewState["vsTemp"] = dsEquiment_etc;                }            }            temp = "";            name = "";        }        if (ViewState["vsTemp"] != null)        {
            //fsg.Text = ViewState["Po"].ToString();

            GvMaster.DataSource = ViewState["vsTemp"];            GvMaster.DataBind();        }        else        {        }    }    private DataTable bindITRepair()    {        DataTable table = new DataTable();        table.Columns.Add("Name", typeof(string));        table.Columns.Add("CO_1", typeof(string));        table.Columns.Add("CO_2", typeof(string));
        table.Columns.Add("CO_3", typeof(string));        table.Columns.Add("CO_4", typeof(string));
        table.Columns.Add("CO_5", typeof(string));        table.Columns.Add("CO_6", typeof(string));
        table.Columns.Add("CO_7", typeof(string));        table.Columns.Add("CO_8", typeof(string));
        table.Columns.Add("CO_9", typeof(string));
        // table.Columns.Add("CO_10", typeof(string));

        // Here we add five DataRows.
        //table.Rows.Add("Indocin", 2, 4,5,6,7);
        //table.Rows.Add("Enebrel", 30, 40,50,60,70);

        DataSupportIT datasupportdept = new DataSupportIT();
        UserRequestList searchdept = new UserRequestList();
        datasupportdept.BoxUserRequest = new UserRequestList[1];

        datasupportdept.BoxUserRequest[0] = searchdept;

        if (Session["Choose"].ToString() == "Sum")
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupportdept, 265);
            datasupportdept = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);
        }        else if(Session["Choose"].ToString() == "Top5")
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupportdept, 266);
            datasupportdept = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);
        }        string Loop_temp = datasupportdept.ReturnName;

        //string Loop_temp = "20,1,2"; //เลข Rdept
        string[] ToId = Loop_temp.Split(',');        foreach (string Loop in ToId)        {            DataSupportIT datasupport = new DataSupportIT();
            UserRequestList searchlv1 = new UserRequestList();
            datasupport.BoxUserRequest = new UserRequestList[1];

            searchlv1.RDeptID = int.Parse(Loop);
            searchlv1.DC_YEAR = Session["DC_YEAR"].ToString();
            searchlv1.DC_Mount = Session["DC_Mount"].ToString();
            searchlv1.AdminIDX = int.Parse(Session["AdminIDX"].ToString());
            searchlv1.AdminDoingIDX = int.Parse(Session["AdminDoingIDX"].ToString());

            datasupport.BoxUserRequest[0] = searchlv1;


            // fsg.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 259);
            datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);            table.Rows.Add(datasupport.ReturnName, datasupport.ReturnCount, datasupport.ReturnCount2, datasupport.ReturnCount3, datasupport.ReturnCount4, datasupport.ReturnCount5,                datasupport.ReturnCount6, datasupport.ReturnCount7, datasupport.ReturnCount8, datasupport.ReturnCount9);
            //fsg.Text = datasupport.ReturnCount + "," + datasupport.ReturnCount2 + "," + datasupport.ReturnCount3 + "," + datasupport.ReturnCount4 + "," + datasupport.ReturnCount5;
        }        return table;    }


    private DataTable bindCHR()    {        DataTable table = new DataTable();        table.Columns.Add("Name", typeof(string));        table.Columns.Add("CO_1", typeof(string));        table.Columns.Add("CO_2", typeof(string));
        table.Columns.Add("CO_3", typeof(string));        table.Columns.Add("CO_4", typeof(string));
        table.Columns.Add("CO_5", typeof(string));        table.Columns.Add("CO_6", typeof(string));
        table.Columns.Add("CO_7", typeof(string));        table.Columns.Add("CO_8", typeof(string));
        table.Columns.Add("CO_9", typeof(string));
        // table.Columns.Add("CO_10", typeof(string));

        // Here we add five DataRows.
        //table.Rows.Add("Indocin", 2, 4,5,6,7);
        //table.Rows.Add("Enebrel", 30, 40,50,60,70);



        data_chr datachr = new data_chr();
        CHRList searchdept = new CHRList();
        datachr.BoxCHRList = new CHRList[1];

        datachr.BoxCHRList[0] = searchdept;
        _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", datachr, 226);
        datachr = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);        string Loop_temp = "";        if (Session["RDeptIDX"].ToString() == "0")
        {
            Loop_temp = datachr.ReturnName;
        }        else
        {
            Loop_temp = Session["RDeptIDX"].ToString();
        }



        // fsg.Text = Session["RDeptIDX"].ToString();
        //string Loop_temp = "20,1,2"; //เลข Rdept
        string[] ToId = Loop_temp.Split(',');        foreach (string Loop in ToId)        {
            data_chr datachrbind = new data_chr();
            CHRList searchlv1 = new CHRList();
            datachrbind.BoxCHRList = new CHRList[1];

            searchlv1.RDeptIDX = int.Parse(Loop);
            searchlv1.IFSearch = int.Parse(Session["IFSearch"].ToString());
            searchlv1.IFSearchbetween = int.Parse(Session["IFSearchbetween"].ToString());
            searchlv1.DategetJob = Session["AddStartdate"].ToString();
            searchlv1.DateCloseJob = Session["DateCloseJob"].ToString();
            searchlv1.AEmpIDX = int.Parse(Session["AEmpIDX"].ToString());
            searchlv1.AEmpIDX1 = int.Parse(Session["AEmpIDX1"].ToString());

            datachrbind.BoxCHRList[0] = searchlv1;


            //   fsg.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datachrbind));
            _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", datachrbind, 224);
            datachrbind = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);            table.Rows.Add(datachrbind.ReturnName, datachrbind.ReturnCount, datachrbind.ReturnCount2, datachrbind.ReturnCount3, datachrbind.ReturnCount4, datachrbind.ReturnCount5,                datachrbind.ReturnCount6, datachrbind.ReturnCount7, datachrbind.ReturnCount8, datachrbind.ReturnCount9);
            //fsg.Text = datasupport.ReturnCount + "," + datasupport.ReturnCount2 + "," + datasupport.ReturnCount3 + "," + datasupport.ReturnCount4 + "," + datasupport.ReturnCount5;
        }        return table;

    }


    private void BindChart()    {        DataTable dt = new DataTable();        try        {            if (Session["System"].ToString() == "ITREPAIR")
            {
                dt = bindITRepair();

            }
            else if (Session["System"].ToString() == "CHR")
            {
                //  fsg.Text = Session["System"].ToString();
                dt = bindCHR();

            }
            //จำนวนชั้น
            str.Append(@"<script type=text/javascript> google.load( *visualization*, *1*, {packages:[*corechart*]});            google.setOnLoadCallback(drawChart);            function drawChart() {            var data = new google.visualization.DataTable();            data.addColumn('string', 'Name');            data.addColumn('number', 'MM-Material Managments');            data.addColumn('number', 'FI-Financial Accounting');            data.addColumn('number', 'CO-Controlling Accounting');            data.addColumn('number', 'PP-Production Planning');            data.addColumn('number', 'SD-Sales  Distribution');            data.addColumn('number', 'QM-Quality Management');            data.addColumn('number', 'BS-BASIS ');            data.addColumn('number', 'ABAP-Programming ');            data.addColumn('number', 'ZZ-แจ้งผิด');                       data.addRows(" + dt.Rows.Count + ");");



            for (int i = 0; i <= dt.Rows.Count - 1; i++)            {                str.Append("data.setValue( " + i + "," + 0 + "," + "'" + dt.Rows[i]["Name"].ToString() + "');");                str.Append("data.setValue(" + i + "," + 1 + "," + dt.Rows[i]["CO_1"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 2 + "," + dt.Rows[i]["CO_2"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 3 + "," + dt.Rows[i]["CO_3"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 4 + "," + dt.Rows[i]["CO_4"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 5 + "," + dt.Rows[i]["CO_5"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 6 + "," + dt.Rows[i]["CO_6"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 7 + "," + dt.Rows[i]["CO_7"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 8 + "," + dt.Rows[i]["CO_8"].ToString() + ") ;");                str.Append("data.setValue(" + i + "," + 9 + "," + dt.Rows[i]["CO_9"].ToString() + ") ;");
                //str.Append("data.setValue(" + i + "," + 10 + "," + dt.Rows[i]["CO_10"].ToString() + ") ;");
            }            str.Append(" var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));");            str.Append(" chart.draw(data,{isStacked:true, width:1024, height:550, hAxis: {showTextEvery:1, slantedText:true}});}");            str.Append("</script>");            lt.Text = str.ToString().TrimEnd(',').Replace('*', '"');        }        catch        {        }    }
}