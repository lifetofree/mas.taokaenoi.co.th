﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="Mockup_CHR.aspx.cs" Inherits="websystem_ITServices_Mockup_CHR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script>

        function DivClicked() {
            var btnHidden = $('#<%= btnHidden.ClientID %>');
            btnHidden.click();
        }

    </script>


    <style>
        #grad1 {
            background-image: -webkit-gradient(linear,left top,left bottom,from( #ffffff ),to( #5dafd3 ));
            background-image: -webkit-linear-gradient( #ffffff,#F4EFAF );
            background-image: -moz-linear-gradient( #ffffff,#F4EFAF );
            background-image: -ms-linear-gradient( #ffffff,#F4EFAF );
            background-image: -o-linear-gradient( #ffffff,#F4EFAF );
            background-image: linear-gradient( #ffffff,#F4EFAF );
        }
    </style>

    <asp:Literal ID="text" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <asp:View ID="ViewIndex" runat="server">
            <div id="grad1">


                <div id="PanelBody_BoxGVIndex" class="panel-body font_text">
                    <%--  <div class="form-horizontal" role="form">--%>
                    <div id="PanelBody_gridviewindex">
                        <div>

                            <%--   <asp:GridView ID="GvMaster" runat="server"
                        AutoGenerateColumns="true"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="primary"
                        HeaderStyle-Height="40px"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <p />
                                        <strong>
                                            <asp:Label ID="Label9" runat="server">รหัสเอกสาร: </asp:Label></strong>
                                        <asp:Literal ID="Literal11" runat="server" Text="SC600082" />
                                        <br />
                                        <strong>
                                            <asp:Label ID="Label15" runat="server">ระบบ: </asp:Label></strong>
                                        <asp:Literal ID="Literal3" runat="server" Text="SAP" />
                                        <br />
                                        <strong>
                                            <asp:Label ID="Label13" runat="server">เงื่อนไขในการแจ้ง: </asp:Label></strong>
                                        <asp:Literal ID="Literal1" runat="server" Text="เพิ่มสิทธิ์" />
                                        <br />
                                        <strong>
                                            <asp:Label ID="Label2" runat="server">ระบบใหม่: </asp:Label></strong>
                                        <asp:Literal ID="Literal4" runat="server" Text="ทดสอบระบบใหม่" />
                                        <br />
                                        <strong>
                                            <asp:Label ID="Label3" runat="server">วันที่สร้าง: </asp:Label></strong>
                                        <asp:Literal ID="Literal5" runat="server" Text="24/07/2017" />
                                        <br />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <small>
                                        <asp:Literal ID="Literal12" runat="server" Text="ดำเนินการ พิจารณาผล โดย ผู้อนุมัติลำดับที่ 2" />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>


                           <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("stidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>--%>

                            <table class="table  table-bordered table-hover table-responsive col-lg-12" cellspacing="0" rules="all" border="1" id="PanelBody_GvMaster" style="border-collapse: collapse;">
                                <tbody>



                                    <td align="left" style="width: 70%">
                                        <small>
                                            <asp:Button runat="server" ID="btnHidden" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการดูรายละเอียดรายการนี้ใช่หรือไม่ ?')" CommandName="btnhide" Style="display: none" />

                                            <div id="div_detail" runat="server" onclick="javascript:DivClicked(); return true;">

                                                <div class="col-lg-3">
                                                    <strong>

                                                        <asp:Image ID="images" runat="server" Width="50%" ImageUrl="~/masterpage/images/CHR/bi_logo.png" />
                                                    </strong>

                                                </div>
                                                <div class="col-lg-9">
                                                    <strong>
                                                        <asp:Label ID="Label9" runat="server">รหัสเอกสาร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal11" runat="server" Text="SC600082" />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server">ระบบ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text="SAP" />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label13" runat="server">เงื่อนไขในการแจ้ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text="เพิ่มสิทธิ์" />
                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label2" runat="server">ระบบใหม่: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text="ทดสอบระบบใหม่" />
                                                    <br />


                                                    <strong>
                                                        <asp:Label ID="Label3" runat="server">วันที่สร้าง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text="24/07/2017" />
                                                    </p>
                                        <strong>
                                            <asp:Label ID="Label23" runat="server">ข้อมูลผู้แจ้ง </asp:Label></strong>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label25" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label26" runat="server">ชื่อ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal25" runat="server" Text="นายผู้อนุมัติที่ 2  (8-9) ทดสอบ MIS" />
                                                    <br />
                                                </div>
                                            </div>

                                        </small>
                                    </td>
                                    <td align="left" style="width: 28%">
                                        <small>
                                            <asp:Literal ID="Literal12" runat="server" Text="ดำเนินการ พิจารณาผล โดย ผู้อนุมัติลำดับที่ 2" />
                                        </small>
                                    </td>

                                    </tr>

                                    <tr>

                                        <td align="left" style="width: 70%">
                                            <small>
                                                <asp:Button runat="server" ID="Button1" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการดูรายละเอียดรายการนี้ใช่หรือไม่ ?')" CommandName="btnhide" Style="display: none" />

                                                <div id="div1" runat="server" onclick="javascript:DivClicked(); return true;">


                                                    <div class="col-lg-3">
                                                        <strong>

                                                            <asp:Image ID="image1" runat="server" Width="50%" ImageUrl="~/masterpage/images/CHR/sap1600.png" />
                                                        </strong>

                                                    </div>
                                                    <div class="col-lg-9">
                                                        <strong>
                                                            <asp:Label ID="Label10" runat="server">รหัสเอกสาร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal13" runat="server" Text="SC600081" />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label11" runat="server">ระบบ: </asp:Label></strong>
                                                        <asp:Literal ID="Literal14" runat="server" Text="SAP" />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label12" runat="server">เงื่อนไขในการแจ้ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal15" runat="server" Text="แก้ไข" />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label16" runat="server">ระบบใหม่: </asp:Label></strong>
                                                        <asp:Literal ID="Literal17" runat="server" Text="ข้อมูลระบบใหม่" />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label17" runat="server">วันที่สร้าง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal18" runat="server" Text="21/07/2017" />
                                                        <br />

                                                        </p>
                                        <strong>
                                            <asp:Label ID="Label6" runat="server">ข้อมูลผู้แจ้ง </asp:Label></strong>
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label7" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal7" runat="server" Text="การจัดการระบบสารสนเทศ" />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label8" runat="server">ชื่อ: </asp:Label></strong>
                                                        <asp:Literal ID="Literal8" runat="server" Text="นางสาวกานต์ธิดา ตระกูลบุญรักษ์" />
                                                        <br />
                                                    </div>
                                                </div>
                                            </small>
                                        </td>

                                        <td align="left" style="width: 28%">
                                            <small>
                                                <asp:Literal ID="Literal24" runat="server" Text="ดำเนินการ พิจารณาผล โดย ผู้อนุมัติลำดับที่ 1" />
                                            </small>
                                        </td>


                                    </tr>

                                
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <%--  </div>--%>
                </div>


            </div>

        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="panel-body">
                <div class="form-horizontal" role="form">
                    <div class="col-lg-offset-9">
                        <asp:LinkButton ID="LinkButton8" CssClass="btn-sm btn-success" runat="server"
                            data-toggle="tooltip" title="อนุมัติ" OnClientClick="return confirm('คุณต้องการอนุมัติข้อมูลนี้ใช่หรือไม่')">
                                            <i class="glyphicon glyphicon-ok"></i> อนุมัติ</asp:LinkButton>

                        <asp:LinkButton ID="LinkButton9" CssClass="btn-sm btn-danger" runat="server" data-toggle="tooltip"
                            title="ไม่อนุมัติ" OnClientClick="return confirm('คุณต้องการไม่อนุมัติข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-remove"></i> ไม่อนุมัติ</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton10" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="btnback" OnCommand="btnCommand"><i class="glyphicon glyphicon-home"></i></asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i><strong>&nbsp; รายละเอียดรายการ Change Request</strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:FormView ID="FvDetailUser" runat="server" Width="100%" DefaultMode="ReadOnly">
                            <InsertItemTemplate>
                                <div class="form-horizontal" role="form">

                                    <div class="col-lg-12">

                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label23" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="รหัสเอกสาร :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label22" CssClass="control-labelnotop " runat="server" Text="SC600082" />
                                                    </div>
                                                    <%-- <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right " runat="server" Text="ระบบ :" />
                                            <div class="col-xs-4">
                                                <asp:Label ID="Label17" CssClass="control-labelnotop " runat="server" Text="SAP" />
                                            </div>--%>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label7" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="ระบบ :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label17" CssClass="control-labelnotop " runat="server" Text="SAP" />
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label30" CssClass="col-xs-5 control-labelnotop text_right" runat="server" Text="ฝ่าย :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label34" CssClass=" control-labelnotop" runat="server" Text="การจัดการระบบสารสนเทศ" />

                                                    </div>
                                                    <%-- <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้แจ้ง :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label36" CssClass=" control-labelnotop" runat="server" Text="นายผู้อนุมัติที่ 2 (8-9) ทดสอบ MIS" />

                                                    </div>--%>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label1" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="ชื่อผู้แจ้ง :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label4" CssClass="control-labelnotop " runat="server" Text="นายผู้อนุมัติที่ 2 (8-9) ทดสอบ MIS" />
                                                    </div>

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label15" CssClass="col-xs-5 control-labelnotop text_right" runat="server" Text="เงื่อนไขในการแจ้ง :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="lbltel" CssClass="control-labelnotop" runat="server" Text="แก้ไข" />
                                                    </div>
                                                    <%--   <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แจ้งเรื่อง :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label18" CssClass=" control-labelnotop " runat="server" Text="ทดสอบระบบ" />
                                                    </div>--%>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label5" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="แจ้งเรื่อง :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label28" CssClass="control-labelnotop " runat="server" Text="ทดสอบระบบ" />
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label29" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="ระบบเดิม :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label31" CssClass="control-labelnotop " runat="server" Text="ทดสอบระบบเดิม" />
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label32" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="ระบบใหม่ :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label33" CssClass="control-labelnotop " runat="server" Text="ทดสอบระบบใหม่" />
                                                    </div>

                                                </div>
                                            </div>


                                            <%--                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระบบเดิม :" />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="lbledit_sysidx" CssClass="control-labelnotop" runat="server" Text="ทดสอบระบบเดิม" />
                                                    </div>

                                                    <asp:Label ID="Label77" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระบบใหม่ :" />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="txtedittopic" CssClass="control-labelnotop" runat="server" Text="ทดสอบระบบใหม่" />

                                                    </div>
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label35" CssClass="col-xs-5 control-labelnotop text_right " runat="server" Text="วันที่สร้าง :" />
                                                    <div class="col-xs-7">
                                                        <asp:Label ID="Label36" CssClass="control-labelnotop " runat="server" Text="24/07/2017" />
                                                    </div>

                                                </div>
                                            </div>

                                            <br />

                                            <div class="form-group">
                                                <div class="col-lg-2 col-lg-offset-10">

                                                    <asp:LinkButton ID="BoxButtonSearchShow" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="More Detail" runat="server" CommandName="BtnHideSETBoxAllSearchShow" OnCommand="btnCommand"><i class="glyphicon glyphicon-list"></i> แสดงข้อมูลผู้อนุมัติ </asp:LinkButton>

                                                    <asp:LinkButton ID="BoxButtonSearchHide" Visible="false" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Close Detail" runat="server" CommandName="BtnHideSETBoxAllSearchHide" OnCommand="btnCommand"><i class="glyphicon glyphicon-list"></i> ย่อข้อมูลผู้อนุมัติ</asp:LinkButton>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6" id="div_p1" runat="server" visible="false">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-check"></i><strong>&nbsp; User SAP ขอใช้สิทธิ์</strong></h3>
                                            <table class="table table-striped table-bordered table-hover table-responsive col-lg-12" cellspacing="0" rules="all" border="1" id="PanselBody_GvMaster" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="success" style="height: 40px;">
                                                        <th class="text-center" scope="col" style="font-size: Small;">ลำดับที่</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้ใช้งานระบบ</th>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 30%">
                                                            <small>
                                                                <asp:Literal ID="Literal38" runat="server" Text="1" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 70%">
                                                            <small>
                                                                <asp:Literal ID="Literal39" runat="server" Text="Support" />

                                                            </small>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td align="left" style="width: 30%">
                                                            <small>
                                                                <asp:Literal ID="Literal40" runat="server" Text="2" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 70%">
                                                            <small>
                                                                <asp:Literal ID="Literal41" runat="server" Text="N038" />

                                                            </small>
                                                        </td>

                                                    </tr>


                                                </tbody>
                                            </table>

                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ผู้อนุมัติ</strong></h3>
                                            <table class="table table-striped table-bordered table-hover table-responsive col-lg-12" cellspacing="0" rules="all" border="1" id="PanselBody_GvMaster" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="success" style="height: 40px;">
                                                        <th class="text-center" scope="col" style="font-size: Small;">ลำดับที่</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้อนุมัติ</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้อนุมัติลำดับที่</th>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal42" runat="server" Text="1" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal43" runat="server" Text="นายผู้อนุมัติที่ 1 (6-7) ทดสอบ QMR" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal44" runat="server" Text="ผู้อนุมัติลำดับที่ 1" />

                                                            </small>
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal45" runat="server" Text="2" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal46" runat="server" Text="นายผู้อนุมัติที่ 2 (8-9) ทดสอบ MIS" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal47" runat="server" Text="ผู้อนุมัติลำดับที่ 2" />

                                                            </small>
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal48" runat="server" Text="3" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal49" runat="server" Text="นายผู้อนุมัติที่ 3 (10-12) ทดสอบ" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal50" runat="server" Text="ผู้อนุมัติลำดับที่ 3" />

                                                            </small>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>


                                    </div>


                                    <%--<div class="col-lg-12" id="div_personal" runat="server" visible="false">
                                        <div class="col-lg-6">

                                            <h3 class="panel-title"><i class="glyphicon glyphicon-check"></i><strong>&nbsp; User SAP ขอใช้สิทธิ์</strong></h3>
                                            <table class="table table-striped table-bordered table-hover table-responsive col-lg-12" cellspacing="0" rules="all" border="1" id="PanselBody_GvMaster" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="success" style="height: 40px;">
                                                        <th class="text-center" scope="col" style="font-size: Small;">ลำดับที่</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้ใช้งานระบบ</th>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 30%">
                                                            <small>
                                                                <asp:Literal ID="Literal31" runat="server" Text="1" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 70%">
                                                            <small>
                                                                <asp:Literal ID="Literal23" runat="server" Text="Support" />

                                                            </small>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                        <td align="left" style="width: 30%">
                                                            <small>
                                                                <asp:Literal ID="Literal26" runat="server" Text="2" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 70%">
                                                            <small>
                                                                <asp:Literal ID="Literal27" runat="server" Text="N038" />

                                                            </small>
                                                        </td>

                                                    </tr>


                                                </tbody>
                                            </table>

                                        </div>

                                        <div class="col-lg-6">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ผู้อนุมัติ</strong></h3>
                                            <table class="table table-striped table-bordered table-hover table-responsive col-lg-12" cellspacing="0" rules="all" border="1" id="PanselBody_GvMaster" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="success" style="height: 40px;">
                                                        <th class="text-center" scope="col" style="font-size: Small;">ลำดับที่</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้อนุมัติ</th>
                                                        <th class="text-center" scope="col" style="font-size: Small;">ผู้อนุมัติลำดับที่</th>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal28" runat="server" Text="1" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal29" runat="server" Text="นายผู้อนุมัติที่ 1 (6-7) ทดสอบ QMR" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal33" runat="server" Text="ผู้อนุมัติลำดับที่ 1" />

                                                            </small>
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal30" runat="server" Text="2" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal32" runat="server" Text="นายผู้อนุมัติที่ 2 (8-9) ทดสอบ MIS" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal34" runat="server" Text="ผู้อนุมัติลำดับที่ 2" />

                                                            </small>
                                                        </td>
                                                    </tr>

                                                    <tr>

                                                        <td align="left" style="width: 20%">
                                                            <small>
                                                                <asp:Literal ID="Literal35" runat="server" Text="3" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal36" runat="server" Text="นายผู้อนุมัติที่ 3 (10-12) ทดสอบ" />

                                                            </small>
                                                        </td>
                                                        <td align="left" style="width: 40%">
                                                            <small>
                                                                <asp:Literal ID="Literal37" runat="server" Text="ผู้อนุมัติลำดับที่ 3" />

                                                            </small>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>--%>
                                </div>

                            </InsertItemTemplate>

                        </asp:FormView>

                    </div>


                </div>
            </div>

            <div class="panel-body">
                <div class="form-horizontal" role="form">
                    <div class="col-lg-offset-9">
                        <asp:LinkButton ID="LinkButton6" CssClass="btn-sm btn-success" runat="server"
                            data-toggle="tooltip" title="อนุมัติ" OnClientClick="return confirm('คุณต้องการอนุมัติข้อมูลนี้ใช่หรือไม่')">
                                            <i class="glyphicon glyphicon-ok"></i> อนุมัติ</asp:LinkButton>

                        <asp:LinkButton ID="LinkButton7" CssClass="btn-sm btn-danger" runat="server" data-toggle="tooltip"
                            title="ไม่อนุมัติ" OnClientClick="return confirm('คุณต้องการไม่อนุมัติข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-remove"></i> ไม่อนุมัติ</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton3" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="btnback" OnCommand="btnCommand"><i class="glyphicon glyphicon-home"></i></asp:LinkButton>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>

