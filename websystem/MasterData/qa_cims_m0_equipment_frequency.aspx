﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_equipment_frequency.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_equipment_frequency" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewResolution" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_InsertFrequency" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลความถี่ในการสอบเทียบ" data-toggle="tooltip" title="เพิ่มข้อมูลความถี่ในการสอบเทียบ" runat="server" CommandName="cmdInsertFrequency" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลความถี่ในการสอบเทียบ</asp:LinkButton>
            </div>

            <asp:GridView ID="GvFrequency" runat="server" Visible="true"
                AutoGenerateColumns="false"
                DataKeyNames="equipment_frequency_idx"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูล</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_equipment_frequency_idx" runat="server" Visible="false" Text='<%# Eval("equipment_frequency_idx") %>' />

                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_equipment_frequency_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("equipment_frequency_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_frequency_count" CssClass="col-sm-3 control-label" runat="server" Text="ความถี่ในการสอบเทียบ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_frequency_count_edit" runat="server" CssClass="form-control " Text='<%# Eval("frequency_count") %>'></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Re_txt_frequency_count_edit" runat="server"
                                                ControlToValidate="txt_frequency_count_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกความถี่ในการสอบเทียบ" ValidationGroup="SaveEdit" />

                                            <asp:RegularExpressionValidator ID="Rq_txt_frequency_count_edit" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลขเท่านั้น" Font-Size="11"
                                                ControlToValidate="txt_frequency_count_edit"
                                                ValidationExpression="^-?([0-9]{0,2}(\.[0-5])?|100(\.00?)?)$" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_frequency_count_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_frequency_count_edit" Width="220" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorRe_txt_frequency_count_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rq_txt_frequency_count_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="หน่วย" />
                                        <div class="col-sm-6">
                                            <%--<asp:Label ID="lbl_frequency_unit_edit" runat="server" Text='<%# Bind("frequency_unit") %>' Visible="false" />--%>
                                            <asp:DropDownList ID="ddlunit_frequency_edit" AutoPostBack="true" runat="server" CssClass="form-control" SelectedValue='<%# Eval("frequency_unit") %>'>
                                                <asp:ListItem Value="0" Text="-- เลือกหน่วย --" />
                                                <asp:ListItem Value="1" Text="สัปดาห์/ครั้ง" />
                                                <asp:ListItem Value="2" Text="เดือน/ครั้ง" />
                                                <asp:ListItem Value="3" Text="ปี/ครั้ง" />

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Re_ddlunit_frequency_edit" runat="server" InitialValue="0" ControlToValidate="ddlunit_frequency_edit" Display="None"
                                                SetFocusOnError="true" ErrorMessage="*กรุณาเลือกหน่วย" ValidationGroup="SaveEdit" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Val_Re_ddlunit_frequency_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlunit_frequency_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_cal_type" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">

                                            <asp:DropDownList ID="ddl_frequency_status_edit" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("frequency_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="SaveEdit" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ความถี่ในการสอบเทียบ" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbfrequency_count" runat="server" Text='<%# Eval("frequency_count") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbfrequency_unit" Visible="false" runat="server" Text='<%# Eval("frequency_unit") %>'></asp:Label>

                                    <asp:Label ID="lbl_unit_frequency" runat="server" Text='<%# Eval("unit_frequency") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_frequency_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("frequency_status") %>'></asp:Label>

                                    <asp:Label ID="lbl_frequency_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_frequency_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteFrequency" CssClass="text-trash" runat="server" CommandName="cmdDelFrequency"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบความถี่ในการสอบเทียบนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("equipment_frequency_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnCancelInsert" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server" CommandName="cmdCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่มความถี่ในการสอบเทียบ (Calibration Frequency)</h3>

                </div>

                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                <div class="form-group">
                                    <asp:Label ID="lbl_frequency_count" runat="server" Text="ความถี่ในการสอบเทียบ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_frequency_count" runat="server" CssClass="form-control" placeholder="กรอกความถี่ในการสอบเทียบ ..." Enabled="true" />

                                        <asp:RequiredFieldValidator ID="Re_txt_frequency_count" runat="server"
                                            ControlToValidate="txt_frequency_count" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรอกความถี่ในการสอบเทียบ" ValidationGroup="Save" />

                                        <asp:RegularExpressionValidator ID="Rq_txt_frequency_count" runat="server" ValidationGroup="Save" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลขเท่านั้น" Font-Size="11"
                                            ControlToValidate="txt_frequency_count"
                                            ValidationExpression="^-?([0-9]{0,2}(\.[0-5])?|100(\.00?)?)$" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_frequency_count" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_frequency_count" Width="220" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_frequency_count" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rq_txt_frequency_count" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_frequency_unit" CssClass="col-sm-3 control-label" runat="server" Text="หน่วย" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_frequency_unit" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0" Text="-- เลือกหน่วย --" />
                                            <asp:ListItem Value="1" Text="สัปดาห์/ครั้ง" />
                                            <asp:ListItem Value="2" Text="เดือน/ครั้ง" />
                                            <asp:ListItem Value="3" Text="ปี/ครั้ง" />
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Requireddl_frequency_unit" runat="server" InitialValue="0"
                                            ControlToValidate="ddl_frequency_unit" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกหน่วย" ValidationGroup="Save" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatorddl_frequency_unit" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddl_frequency_unit" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_frequency_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_frequency_status" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btn_SaveFrequency" ValidationGroup="Save" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSaveFrequency" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btn_CancelFrequency" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->

    </asp:MultiView>



</asp:Content>

