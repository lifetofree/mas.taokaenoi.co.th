﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_MasterPriority : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelectPriority = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPriority"];
    static string urlInsertPriority = _serviceUrl + ConfigurationManager.AppSettings["urlInsertPriority"];
    static string urlUpdatePriority = _serviceUrl + ConfigurationManager.AppSettings["urlUpdatePriority"];
    static string urlDeletePriority = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePriority"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Priority()
    {
        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP dtsupport = new PrioritySAP();

        dtsupport.Priority_name = txtpriority.Text;
        dtsupport.baseline = txtbase.Text;
        dtsupport.Priority_status = int.Parse(ddStatusadd.SelectedValue);
        dtsupport.CEmpIDX_PIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxStatusPriority[0] = dtsupport;

        _dtsupport = callServiceDevices(urlInsertPriority, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP dtsupport = new PrioritySAP();

        _dtsupport.BoxStatusPriority[0] = dtsupport;

        _dtsupport = callServiceDevices(urlSelectPriority, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxStatusPriority);
    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP dtsupport = new PrioritySAP();

        dtsupport.PIDX = int.Parse(ViewState["PTIDX_Update"].ToString());
        dtsupport.Priority_name = ViewState["Priority_name_edit_Update"].ToString();
        dtsupport.baseline = ViewState["BASELINE_edit_Update"].ToString();
        dtsupport.Priority_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dtsupport.CEmpIDX_PIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxStatusPriority[0] = dtsupport;

        _dtsupport = callServiceDevices(urlUpdatePriority, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP dtsupport = new PrioritySAP();
        dtsupport.PIDX = int.Parse(ViewState["PTIDX_Update"].ToString());
        dtsupport.CEmpIDX_PIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.BoxStatusPriority[0] = dtsupport;

        _dtsupport = callServiceDevices(urlDeletePriority, _dtsupport);
    }

    protected DataSupportIT callServiceDevices(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }



    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int PIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtpriority_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtpriority_edit");
                var txtbase_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtbase_edit");

                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["PTIDX_Update"] = PIDX;
                ViewState["Priority_name_edit_Update"] = txtpriority_edit.Text;
                ViewState["BASELINE_edit_Update"] = txtbase_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                txtpriority.Text = String.Empty;

                break;

            case "btnAdd":
                Insert_Priority();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int PIDX = int.Parse(cmdArg);
                ViewState["PTIDX_Update"] = PIDX;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion


}