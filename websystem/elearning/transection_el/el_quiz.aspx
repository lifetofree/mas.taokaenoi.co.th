﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_quiz.aspx.cs" Inherits="websystem_elearning_transection_el_el_quiz" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
        <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

     <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="color: black;">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: #f0f0f0" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">

                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex" ForeColor="black"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>



                    <li id="_divMenuLiToViewApprove" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove" ForeColor="black"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ / รายการรอแก้ไข">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>


                    <li id="_divMenuLiToViewMaster" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" style="color: black;" aria-haspopup="true" aria-expanded="false">Master Data 
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="_divMenuLiToViewMaster_GroupProduct" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterGroupProduct" runat="server"
                                    CommandName="_divMenuBtnToDivMasterModule" ForeColor="black"
                                    OnCommand="btnCommand" Text="Module SAP" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Product" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct" ForeColor="black"
                                    OnCommand="btnCommand" Text="ประเภทวัตถุดิบ" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Product_M1" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct_M1" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct_M1" ForeColor="black"
                                    OnCommand="btnCommand" Text="โครงสร้างวัตถุดิบ" />
                            </li>


                            <li id="_divMenuLiToViewMaster_Product_M2" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterProduct_M2" runat="server"
                                    CommandName="_divMenuBtnToDivMasterProduct_M2" ForeColor="black"
                                    OnCommand="btnCommand" Text="โครงสร้างย่อยวัตถุดิบ" />
                            </li>

                            <li id="_divMenuLiToViewMaster_Unit" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterUnit" runat="server"
                                    CommandName="_divMenuBtnToDivMasterUnit" ForeColor="black"
                                    OnCommand="btnCommand" Text="หน่วยวัด" />
                            </li>
                            <li id="_divMenuLiToViewMaster_Place" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivMasterPlace" runat="server"
                                    CommandName="_divMenuBtnToDivMasterPlace" ForeColor="black"
                                    OnCommand="btnCommand" Text="สถานที่" />
                            </li>
                        </ul>
                    </li>


                  


                </ul>

               <%-- <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1FgvVEkpyIPkI9wnyo13-sjcoZHpCjdyrcCuOkxR8hBw/edit?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>


                </ul>--%>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
   


            </asp:View>

         <asp:View ID="ViewMasterModule" runat="server">
   


            </asp:View>
        </asp:MultiView>
    

</asp:Content>

