﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataMachine")]
public class DataMachine
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("ReturnIDX")]
    public string ReturnIDX { get; set; }

    [XmlElement("BoxRepairMachine")]
    public RepairMachineDetail[] BoxRepairMachine { get; set; }

    [XmlElement("BoxStatusList")]
    public StatusListDetail[] BoxStatusList { get; set; }

    [XmlElement("BoxTypeMachine")]
    public TypeMachineDetail[] BoxTypeMachine { get; set; }

    [XmlElement("BoxQuestionMachine")]
    public QuestionDetail[] BoxQuestionMachine { get; set; }

    [XmlElement("TypeClassDetail")]
    public TypeClass[] TypeClassDetail { get; set; }

    [XmlElement("BoxMachine_Register")]
    public Machine_Register[] BoxMachine_Register { get; set; }

    [XmlElement("BoxRepairMachine_Export")]
    public RepairMachine_Export[] BoxRepairMachine_Export { get; set; }

}

[Serializable]
public class RepairMachineDetail
{
    [XmlElement("RPIDX")]
    public int RPIDX { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("m0tidx")]
    public int m0tidx { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("datestart")]
    public string datestart { get; set; }

    [XmlElement("NameTypecode")]
    public string NameTypecode { get; set; }

    [XmlElement("NameGroupcode")]
    public string NameGroupcode { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }


    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }


    [XmlElement("u1idx_planning")]
    public int u1idx_planning { get; set; }

    [XmlElement("DateManage")]
    public string DateManage { get; set; }


    [XmlElement("countbuild")]
    public int countbuild { get; set; }

    [XmlElement("downtime")]
    public string downtime { get; set; }


    [XmlElement("RECode")]
    public string RECode { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("UserEmpIDX")]
    public int UserEmpIDX { get; set; }

    [XmlElement("CTime")]
    public string CTime { get; set; }

    [XmlElement("CommentAuto")]
    public string CommentAuto { get; set; }

    [XmlElement("CDate")]
    public string CDate { get; set; }


    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("CMIDX")]
    public int CMIDX { get; set; }

    [XmlElement("FileUser")]
    public int FileUser { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }


    [XmlElement("CommentUser")]
    public string CommentUser { get; set; }

    [XmlElement("DateNotify")]
    public string DateNotify { get; set; }

    [XmlElement("TimeNotify")]
    public string TimeNotify { get; set; }


    [XmlElement("AdminEmpIDX")]
    public int AdminEmpIDX { get; set; }

    [XmlElement("FileAM")]
    public int FileAM { get; set; }

    [XmlElement("AdminApproveEmpIDX")]
    public int AdminApproveEmpIDX { get; set; }

    [XmlElement("DateApprove")]
    public string DateApprove { get; set; }

    [XmlElement("TimeApprove")]
    public string TimeApprove { get; set; }


    [XmlElement("CommentApprove")]
    public string CommentApprove { get; set; }

    [XmlElement("DateReceive")]
    public string DateReceive { get; set; }

    [XmlElement("DateClose")]
    public string DateClose { get; set; }

    [XmlElement("CommentReciveAdmin")]
    public string CommentReciveAdmin { get; set; }

    [XmlElement("DategetJobSearch")]
    public string DategetJobSearch { get; set; }

    [XmlElement("DatecloseJobSearch")]
    public string DatecloseJobSearch { get; set; }

    [XmlElement("Price")]
    public int Price { get; set; }

    [XmlElement("CCAIDX")]
    public int CCAIDX { get; set; }

    [XmlElement("STAppIDX")]
    public int STAppIDX { get; set; }

    [XmlElement("RoomIDX")]
    public int RoomIDX { get; set; }

    [XmlElement("Roomname")]
    public string Roomname { get; set; }


    [XmlElement("BuildingIDX")]
    public int BuildingIDX { get; set; }

    [XmlElement("BuildingName")]
    public string BuildingName { get; set; }


    [XmlElement("AdminReciveEmpIDX")]
    public int AdminReciveEmpIDX { get; set; }

    [XmlElement("ACIDX")]
    public int ACIDX { get; set; }

    [XmlElement("IFSearch")]
    public int IFSearch { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("nameclosejobID")]
    public int nameclosejobID { get; set; }

    [XmlElement("ClasssearchID")]
    public int ClasssearchID { get; set; }

    [XmlElement("MCCode")]
    public string MCCode { get; set; }

    [XmlElement("NameMachine")]
    public string NameMachine { get; set; }

    [XmlElement("MCIDX")]
    public int MCIDX { get; set; }

    [XmlElement("EMail")]
    public string EMail { get; set; }

    [XmlElement("NameUser")]
    public string NameUser { get; set; }

    [XmlElement("UserOrgNameTH")]
    public string UserOrgNameTH { get; set; }

    [XmlElement("UserSecNameTH")]
    public string UserSecNameTH { get; set; }

    [XmlElement("UserDeptNameTH")]
    public string UserDeptNameTH { get; set; }

    [XmlElement("UserPosNameTH")]
    public string UserPosNameTH { get; set; }

    [XmlElement("nodeidx")]
    public int nodeidx { get; set; }

    [XmlElement("actoridx")]
    public int actoridx { get; set; }

    [XmlElement("node_status")]
    public int node_status { get; set; }

    [XmlElement("status_name1")]
    public string status_name1 { get; set; }


    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("TypeNameTH")]
    public string TypeNameTH { get; set; }

    [XmlElement("MCNameTH")]
    public string MCNameTH { get; set; }

    [XmlElement("Abbreviation")]
    public string Abbreviation { get; set; }

    [XmlElement("DateAlertJob")]
    public string DateAlertJob { get; set; }

    [XmlElement("TimeAlertJob")]
    public string TimeAlertJob { get; set; }

    [XmlElement("NameAdminClose")]
    public string NameAdminClose { get; set; }

    [XmlElement("DateCloseJob")]
    public string DateCloseJob { get; set; }

    [XmlElement("TimeCloseJob")]
    public string TimeCloseJob { get; set; }

    [XmlElement("CommentAdmin")]
    public string CommentAdmin { get; set; }

    [XmlElement("NameAdminRe")]
    public string NameAdminRe { get; set; }

    [XmlElement("UserEmpCode")]
    public string UserEmpCode { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("DateReciveJob")]
    public string DateReciveJob { get; set; }

    [XmlElement("TimeReciveJob")]
    public string TimeReciveJob { get; set; }

    [XmlElement("sumtime")]
    public string sumtime { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("RPIDXQuest")]
    public int RPIDXQuest { get; set; }


    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }


    [XmlElement("DC_Month")]
    public string DC_Month { get; set; }

    [XmlElement("zSysIDX_name")]
    public string zSysIDX_name { get; set; }

}

[Serializable]
public class StatusListDetail
{
    [XmlElement("stidx")]
    public int stidx { get; set; }

    [XmlElement("dtidx")]
    public int dtidx { get; set; }

    [XmlElement("status_state")]
    public int status_state { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("downtime_name")]
    public string downtime_name { get; set; }


}

[Serializable]
public class TypeMachineDetail
{
    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("TStatus")]
    public int TStatus { get; set; }

    [XmlElement("NameTH")]
    public string NameTH { get; set; }

    [XmlElement("CodeTM")]
    public string CodeTM { get; set; }

    [XmlElement("NameEN")]
    public string NameEN { get; set; }
}

[Serializable]
public class QuestionDetail
{
    [XmlElement("TFBIDX")]
    public int TFBIDX { get; set; }

    [XmlElement("TFBIDX1")]
    public int TFBIDX1 { get; set; }

    [XmlElement("TFBIDX2")]
    public int TFBIDX2 { get; set; }

    [XmlElement("TFBIDX3")]
    public int TFBIDX3 { get; set; }

    [XmlElement("POIDX")]
    public int POIDX { get; set; }

    [XmlElement("UMFB2")]
    public int UMFB2 { get; set; }

    [XmlElement("Point")]
    public int Point { get; set; }

    [XmlElement("QuestionSet")]
    public string QuestionSet { get; set; }

    [XmlElement("Topic")]
    public string Topic { get; set; }

    [XmlElement("Question")]
    public string Question { get; set; }

    [XmlElement("TIDX")]
    public int TIDX { get; set; }

    [XmlElement("RPIDX")]
    public int RPIDX { get; set; }

    [XmlElement("Other")]
    public string Other { get; set; }

    [XmlElement("SumPoint")]
    public int SumPoint { get; set; }

    [XmlElement("WFIDX")]
    public int WFIDX { get; set; }

    [XmlElement("STAIDX")]
    public int STAIDX { get; set; }

    [XmlElement("U1IDX")]
    public int U1IDX { get; set; }

    [XmlElement("U0AQS")]
    public int U0AQS { get; set; }

    [XmlElement("PO_TFBIDX2")]
    public int PO_TFBIDX2 { get; set; }

}

[Serializable]
public class TypeClass
{
    [XmlElement("TCLIDX")]
    public int TCLIDX { get; set; }

    [XmlElement("TC_Name")]
    public string TC_Name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }

    [XmlElement("TCLStatus")]
    public int TCLStatus { get; set; }

    [XmlElement("TCStatus1")]
    public string TCStatus1 { get; set; }
}

[Serializable]
public class Machine_Register
{
    [XmlElement("MCIDX")]
    public int MCIDX { get; set; }

    [XmlElement("MCCode")]
    public string MCCode { get; set; }

    [XmlElement("Abbreviation")]
    public string Abbreviation { get; set; }

    [XmlElement("MCNameTH")]
    public string MCNameTH { get; set; }

    [XmlElement("MCNameEN")]
    public string MCNameEN { get; set; }

    [XmlElement("MCGenerate")]
    public string MCGenerate { get; set; }

    [XmlElement("MCSerial")]
    public string MCSerial { get; set; }

    [XmlElement("MCInsurance")]
    public string MCInsurance { get; set; }

    [XmlElement("MCMachineNo")]
    public string MCMachineNo { get; set; }

    [XmlElement("AlertPurchaseDate")]
    public string AlertPurchaseDate { get; set; }

    [XmlElement("AlertOutInsureDate")]
    public string AlertOutInsureDate { get; set; }

    [XmlElement("TC_Name")]
    public string TC_Name { get; set; }

    [XmlElement("NameMachine")]
    public string NameMachine { get; set; }

    [XmlElement("NameTypeTH")]
    public string NameTypeTH { get; set; }
    [XmlElement("NameTypeEN")]

    public string NameTypeEN { get; set; }

    [XmlElement("ADate")]
    public string ADate { get; set; }

    [XmlElement("DetailEtc")]
    public string DetailEtc { get; set; }

    [XmlElement("AssetsCode")]
    public string AssetsCode { get; set; }

    [XmlElement("Partpic")]
    public string Partpic { get; set; }

    [XmlElement("MCStatusDetail")]
    public string MCStatusDetail { get; set; }

    [XmlElement("PlName")]
    public string PlName { get; set; }

    [XmlElement("STApprove")]
    public string STApprove { get; set; }

    [XmlElement("RoomName")]
    public string RoomName { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("RDeptName")]
    public string RDeptName { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("TCIDX")]
    public int TCIDX { get; set; }

    [XmlElement("NameTypeCode")]
    public string NameTypeCode { get; set; }

    [XmlElement("GCIDX")]
    public int GCIDX { get; set; }

    [XmlElement("NameGroupCode")]
    public string NameGroupCode { get; set; }

    [XmlElement("TmcIDX")]
    public int TmcIDX { get; set; }

    [XmlElement("CodeTM")]
    public string CodeTM { get; set; }

    [XmlElement("NameTH")]
    public string NameTH { get; set; }

    [XmlElement("TCLIDX")]
    public int TCLIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

}

[Serializable]
public class RepairMachine_Export
{
    [XmlElement("ระบบ")]
    public string ระบบ { get; set; }

    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("ประเภทเครื่องจักร")]
    public string ประเภทเครื่องจักร { get; set; }

    [XmlElement("ชื่อเครื่องจักร")]
    public string ชื่อเครื่องจักร { get; set; }

    [XmlElement("รหัสใหม่")]
    public string รหัสใหม่ { get; set; }

    [XmlElement("รหัสเก่า")]
    public string รหัสเก่า { get; set; }

    [XmlElement("สถานที่")]
    public string สถานที่ { get; set; }

    [XmlElement("อาคาร")]
    public string อาคาร { get; set; }

    [XmlElement("ห้อง")]
    public string ห้อง { get; set; }

    [XmlElement("วันที่แจ้ง")]
    public string วันที่แจ้ง { get; set; }

    [XmlElement("ชื่อผู้แจ้ง")]
    public string ชื่อผู้แจ้ง { get; set; }

    [XmlElement("ฝ่ายผู้แจ้ง")]
    public string ฝ่ายผู้แจ้ง { get; set; }

    [XmlElement("รายละเอียดรายการ")]
    public string รายละเอียดรายการ { get; set; }

    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }

    [XmlElement("เจ้าหน้าที่รับงาน")]
    public string เจ้าหน้าที่รับงาน { get; set; }

    [XmlElement("ความคิดเห็นเจ้าหน้าที่รับงาน")]
    public string ความคิดเห็นเจ้าหน้าที่รับงาน { get; set; }

    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }

    [XmlElement("เจ้าหน้าที่ปิดงาน")]
    public string เจ้าหน้าที่ปิดงาน { get; set; }

    [XmlElement("ความคิดเห็นเจ้าหน้าที่ปิดงาน")]
    public string ความคิดเห็นเจ้าหน้าที่ปิดงาน { get; set; }


    [XmlElement("เวลาในการดำเนินการ")]
    public string เวลาในการดำเนินการ { get; set; }


    [XmlElement("สถานะดำเนินการ")]
    public string สถานะดำเนินการ { get; set; }

}