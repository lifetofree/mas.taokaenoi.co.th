﻿using AjaxControlToolkit;
using MessagingToolkit.QRCode.Codec;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class websystem_el_Trn_plan_course : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();
    data_employee _dtEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetInsel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan_course"];
    static string _urlDelel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    static string _urlsendEmail_plan_course_hrtohrd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrtohrd"];
    static string _urlsendEmail_plan_course_hrdtomd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtomd"];
    static string _urlsendEmail_plan_course_hrdtohr_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtohr_all"];
    static string _urlsendEmail_plan_course_mdtohrd_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_mdtohrd_all"];
    static string _urlsendEmail_outplan_course_usertoleader = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_outplan_course_usertoleader"];
    static string _urlsendemail_outplan_leadertohr = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertohr"];
    static string _urlsendemail_outplan_leadertouser_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertouser_all"];


    static string _urlGetel_Report_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_plan_course"];

    //หลักสูตร 
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];

    static string _urlget_course_costcenter = _serviceUrl + ConfigurationManager.AppSettings["urlget_course_costcenter"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlget_expert_salary = _serviceUrl + ConfigurationManager.AppSettings["urlget_expert_salary"];
    static string urlget_training_location = _serviceUrl + ConfigurationManager.AppSettings["urlget_training_location"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetMasterQuiz = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_Quiz"];
    static string _urlGetel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_manage"];


    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;
    DateTime _datenow = DateTime.Now;

    string _FromcourseRunNo = "u_plan_course";
    string _Folder_plan_course = "plan_course_file";
    string _Folder_plan_course_bin = "plan_course_file_bin";
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];
    string _folder_plan_course_qrcode = "plan_course_qrcode";
    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;


    }
    #endregion Constant



    //start_object
    FormView _FormView;
    TextBox
        _txttraining_course_no
    , _txttraining_group_name
    , _txttraining_branch_name
    , _txttraining_course_date
    , _txttraining_course_year
    , _txttraining_course_qty
    , _txttraining_course_amount
    , _txttraining_course_model
    , _txttraining_course_budget
    , _txttraining_course_costperhead
    , _txttraining_course_description
    , _txtdatestart_create
    , _txt_timestart_create
    , _txt_timeend_create
        , _txtexpenses_description
        , _txtamount
        , _txtvat
        , _txtwithholding_tax
        , _txttraining_course_total
        , _txttraining_course_total_avg
        , _txttraining_course_reduce_tax
        , _txttraining_course_net_charge
        , _txttraining_course_net_charge_tax
        , _txttraining_course_planbudget_total
        , _txttraining_course_budget_total
        , _txttraining_course_budget_balance
        , _txttraining_course_budget_total_per
        , _txtpass_test_per
        , _txthour_training_per
        , _txtpublish_training_description
        , _txtother_description
        , _txthrd_follow_day
        , _txttraining_course_remark
        , _txttraining_course_file_name
        , _txtcourse_name
        , _txtsuper_app_status
        , _txthr_status
        , _txttraining_course_date_qty
        , _txtcourse_score
        , _txtscore_through_per
        , _txtcourse_assessment
        , _txtother

        ;
    RadioButtonList
        _rdllecturer_type
        , _rdotraining_course_type
        , _rdotraining_course_planbudget_type
        , _rdolecturer_type_place

        ;
    CheckBoxList
        _chkchoice_file;
    DropDownList
        _ddltraining_course_status
        , _ddlm0_institution_idx_ref
        , _ddlm0_objective_idx_ref
        , _ddlm0_target_group_idx_ref
        , _ddlu0_training_plan_idx_ref
        , _ddlemp_idx_ref
        , _ddlcostcenter_idx_ref
        , _ddlRDeptID_ref
        , _ddlcourse_plan_status
        , _ddlplace_idx_ref
        , _ddl_training_location
        , _ddlm0_training_group_idx_ref
        , _ddm0_training_branch_idx
        , _ddl_training_name_add
        , _ddlOrgID
        ;
    GridView
        _GvMonthList
        , _GvMonthList_L
        , _GvMonthList_A
        , _Gvinstitution
        , _Gvobjective
        , _Gvemp
        , _Gvtrn_expenses
        , _Gvu8trncoursedate
        , _GvHistory
        ;
    CheckBox
         _cbpass_test_flag
        , _cbhour_training_flag
        , _cbwrite_report_training_flag
        , _cbpublish_training_flag
        , _cbcourse_lecturer_flag
        , _cbother_flag
        , _cbhrd_nofollow_flag
        , _cbhrd_follow_flag
        ;

    Panel
        _pnl_institution
        , _pnl_objective
        , _pnl_emp
        , _pnl_expenses
        , _pnl_file
        , _pnl_file_btn
        , _pnladd_emp_resulte
        , _pnlcourse_plan_status
        , _pnlcourse_outplan_status
        , _pnlplace
        , _pnlcourse_plan_status1
        , _pnlcourse_outplan_status2
        , _pnl_date
        , _pnltarget_group
        , _pnlhistory
        ;
    Label
        _lbFileName
        , _nav_approve
        ;
    RequiredFieldValidator
        _rqf_ddlu0_training_plan_idx_ref
        , _rqf_txtcourse_name
        , _rqf_ddm0_training_group_idx_ref
        , _rqf_ddm0_training_branch_idx
        , _rqf_place_idx_ref
        , _rqf_ddlm0_target_group_idx_ref
        ;
    FileUpload
        _UploadFileCourse
        ;
    Control
        _div_otherreason
        ;
    #endregion Init

    private DateTime _dtMonth;
    private DateTime _selectedDate;
    private bool _specialDaySelected = true;
    private int _currentBindingMonth;
    public string checkfile;

    public void zCrontrollFrom()
    {
        // Page Crontroll
        string _gFromName = ("el_TrnPlan").ToUpper();
        string _gbtnListData = ("btnListData").ToUpper();
        string _gbtnInsert = ("btnInsert").ToUpper();
        string _gbtnHR_Wait = ("btnHR_Wait").ToUpper();
        string _gbtnHR_App = ("btnHR_App").ToUpper();
        string _gbtnMD_Wait = ("btnMD_Wait").ToUpper();
        string _gbtnMD_App = ("btnMD_App").ToUpper();

        //rpos_idx
        ////นางสาวลลดา  นุกูลเอื้ออำรุง ผู้จัดการฝ่ายพัฒนาบุคลากร 5900
        //string _gsHR_rpos_idx = "11754";
        ////นางสาวกมลชนก  บุญกูล ผู้จัดการฝ่ายบริการงานทรัพยากรบุคคล  5899
        //string _gsHRD_rpos_idx = "5899"; //.44
        //string _gsHRD_rpos_idx28 = "8150"; //.28
        ////นายณัชชัชพงศ์  พีระเดชาพันธ์ Managing Director 780
        //string _gsMD_rpos_idx = "780";

        //วิจิตรตรา พลอยกระจ่าง	7124
        //วุฒิพงษ์ โพธิ์เจริญ 7124
        //ธัญญกานต์ วรานนท์วนิช   11754
        string _gsHR_rpos_idx = "11754", _gsHR_rpos_idx1 = "7124", _gsHR_rpos_idx2 = "5964", _gsHR_rpos_idx3 = "5900";
        //ธัญญกานต์ วรานนท์วนิช HROD Manager 11754
        string _gsHRD_rpos_idx = "5900"; // .44
        string _gsHRD_rpos_idx28 = "11754"; //.28

        //ปริญญ์ พิชญวิจิตร์ ประธานเจ้าหน้าที่กลุ่มงานทรัพยากรบุคคล 12599
        string _gsMD_rpos_idx = "12599";

        string _gsAdmin = "210", emp_code_admin = "58000070";

        ViewState["emp_code"] = "";
        ViewState["rpos_idx"] = "";
        ViewState["rsec_idx"] = "";
        ViewState["rdept_idx"] = "";
        ViewState["org_idx"] = "";
        ViewState["jobgrade_idx"] = "";
        ViewState["joblevel_idx"] = "";
        ViewState["CostIDX"] = "";
        ViewState["CostNo"] = "";

        data_elearning dataelearning = new data_elearning();
        dataelearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        dataelearning.employeeM_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_employee, dataelearning);
        if (dataelearning.employeeM_action != null)
        {
            foreach (var item in dataelearning.employeeM_action)
            {
                ViewState["emp_code"] = item.EmpCode;
                ViewState["rpos_idx"] = item.RPosIDX_J;
                ViewState["rsec_idx"] = item.RSecID;
                ViewState["rdept_idx"] = item.RDeptID;
                ViewState["RDeptName"] = item.RDeptName;
                ViewState["org_idx"] = item.OrgIDX;
                ViewState["jobgrade_idx"] = item.JobGradeIDX;
                ViewState["joblevel_idx"] = item.JobLevel;
                ViewState["CostIDX"] = item.CostIDX;
                if (item.CostNo != null)
                {
                    ViewState["CostNo"] = item.CostNo;
                }

            }
        }

        btnListData.Visible = false;
        //  btnInsert.Visible = false;
        btnHR_Wait.Visible = false;
        btnHR_App.Visible = false;
        btnMD_Wait.Visible = false;
        btnMD_App.Visible = false;
        pnlListData.Visible = false;
        liplanReport.Visible = false;
        lisummary.Visible = false;
        liLeaderList.Visible = false;
        liLeaderApproveList.Visible = false;
        // licourse_out_plan.Visible = false;
        pnlScourse_plan_status.Visible = true;

        //  liInsert.Visible = false;

        int _item = 0;

        if (
            (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx) ||
            (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx1) ||
            (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx2) ||
             (ViewState["rpos_idx"].ToString() == _gsHR_rpos_idx3) ||
            (ViewState["emp_code"].ToString() == emp_code_admin)

            )

        // || (ViewState["rsec_idx"].ToString() == _gsAdmin)
        {
            _item++;
            //ผู้จัดการฝ่ายพัฒนาบุคลากร
            Hddfld_permission.Value = "HR";
            btnListData.Visible = true;
            // btnInsert.Visible = true;
            //  liInsert.Visible = true;
            pnlListData.Visible = true;
            liplanReport.Visible = true;
            lisummary.Visible = true;
            //  licourse_out_plan.Visible = true;
            if ((_func_dmu.zStringToInt(ViewState["joblevel_idx"].ToString()) >= 7) ||
                (ViewState["emp_code"].ToString() == emp_code_admin)
                )
            {
                liLeaderList.Visible = false;
                liLeaderApproveList.Visible = false;
            }
            zSetMode(2);
            ShowHRsummary();
            ShowLeaderW();

            if ((ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx28) ||
                 (ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx) ||
                 (ViewState["emp_code"].ToString() == emp_code_admin)
                 ) //|| (ViewState["rsec_idx"].ToString() == _gsAdmin)
            {

                _item++;
                //ผู้จัดการฝ่ายบริการงานทรัพยากรบุคคล
                Hddfld_permission.Value = "HRD";
                btnHR_Wait.Visible = false;
                btnHR_App.Visible = false;
                //zbtnHR_Wait();
            }
            if (ViewState["emp_code"].ToString() == emp_code_admin)
            {
                _item++;
                //Managing Director
                Hddfld_permission.Value = "MD";
                btnMD_Wait.Visible = false;
                btnMD_App.Visible = false;
                // zbtnMD_Wait();

            }

        }
        //else if ((ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx28) ||
        //         (ViewState["rpos_idx"].ToString() == _gsHRD_rpos_idx)
        //         ) //|| (ViewState["rsec_idx"].ToString() == _gsAdmin)
        //{

        //    _item++;
        //    //ผู้จัดการฝ่ายบริการงานทรัพยากรบุคคล
        //    Hddfld_permission.Value = "HRD";
        //    btnHR_Wait.Visible = true;
        //    btnHR_App.Visible = true;
        //    zbtnHR_Wait();
        //}
        else if ((ViewState["rpos_idx"].ToString() == _gsMD_rpos_idx)
            )// || (ViewState["rsec_idx"].ToString() == _gsAdmin)
        {
            _item++;
            //Managing Director
            Hddfld_permission.Value = "MD";
            btnMD_Wait.Visible = false;
            btnMD_App.Visible = false;
            // zbtnMD_Wait();
        }
        else
        {

            Hddfld_permission.Value = "USER";
            btnListData.Visible = true;
            // licourse_out_plan.Visible = true;
            pnlScourse_plan_status.Visible = false;
            if (_func_dmu.zStringToInt(ViewState["joblevel_idx"].ToString()) >= 7)
            {
                // Hddfld_permission.Value = "LEADER";
                liLeaderList.Visible = false;
                liLeaderApproveList.Visible = false;
            }
            zSetMode(2);
            //ShowLeaderW();
        }
        // litkeg.Text = liInsert.Visible.ToString()+ licourse_out_plan.Visible.ToString();

        liplanReport.Visible = false;

        //if ((liInsert.Visible == false) &&
        //    (licourse_out_plan.Visible == false))
        //{
        //    li_courseplan.Visible = false;
        //}
        //else
        //{
        //    li_courseplan.Visible = true;
        //}

    }
    public string getImg()
    {
        _txttraining_course_no = (TextBox)fvCRUD.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fvCRUD.FindControl("txttraining_course_file_name");
        TextBox lbFileName_1 = (TextBox)fvCRUD.FindControl("lbFileName_1");
        //Image imgnofile1 = (Image)fvCRUD.FindControl("imgnofile1");
        //Image imgfile1 = (Image)fvCRUD.FindControl("imgfile1");

        string sPathImage = "", sFileName = "";
        string strFinalFileName = "", sReturn = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFileName = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }
        if (sFileName == "")
        {
            if (_txttraining_course_no.Text != "")
            {
                string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course + "/" + _txttraining_course_no.Text;
                try
                {
                    if (_PathFile != "")
                    {
                        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string file in filesLoc)
                        {
                            try
                            {
                                strFinalFileName = Path.GetFileName(file);
                                sFileName = Path.GetExtension(file);
                            }
                            catch { }
                        }
                    }
                }
                catch { }
                if (_txttraining_course_file_name.Text != "")
                {
                    if (_txttraining_course_file_name.Text == strFinalFileName)
                    {

                    }
                    else
                    {
                        sFileName = "";
                    }
                }
            }

        }
        if (sFileName == "")
        {
            //imgfile1.Visible = false;
            //imgnofile1.Visible = true;
            sReturn = _PathFileimage + _func_dmu._IconnoFile;
            // lbFileName_1.Text = "";
        }
        else
        {
            //imgfile1.Visible = true;
            //imgnofile1.Visible = false;
            // lbFileName_1.Text = sFileName;
            sReturn = _PathFileimage + _func_dmu._IconFile;
        }
        sReturn = ResolveUrl(sReturn);
        return sReturn;
    }
    public string getImgFileType()
    {
        _txttraining_course_no = (TextBox)fvCRUD.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fvCRUD.FindControl("txttraining_course_file_name");

        string sPathImage = "", sFileName = "";
        string strFinalFileName = "", sReturn = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFileName = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }
        if (sFileName == "")
        {
            if (_txttraining_course_no.Text != "")
            {
                string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course + "/" + _txttraining_course_no.Text;
                try
                {
                    if (_PathFile != "")
                    {
                        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string file in filesLoc)
                        {
                            try
                            {
                                strFinalFileName = Path.GetFileName(file);
                                sFileName = Path.GetExtension(file);
                            }
                            catch { }
                        }
                    }
                }
                catch { }
                if (_txttraining_course_file_name.Text != "")
                {
                    if (_txttraining_course_file_name.Text == strFinalFileName)
                    {

                    }
                    else
                    {
                        sFileName = "";
                    }
                }
            }

        }
        if (sFileName == "")
        {
            sReturn = "";
        }
        else
        {
            sReturn = sFileName;
        }
        return sReturn;
    }
    public void zDeleteFileMAS()
    {
        _txttraining_course_no = (TextBox)fvCRUD.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fvCRUD.FindControl("txttraining_course_file_name");
        if (_txttraining_course_no.Text == "")
        {
            return;
        }
        if (_txttraining_course_file_name.Text == "")
        {
            return;
        }
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course + "/" + _txttraining_course_no.Text;
        int ic = 0;
        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
        List<ListItem> files = new List<ListItem>();
        foreach (string file in filesLoc)
        {
            try
            {
                File.Delete(file);
                ic++;
            }
            catch { }
        }
        if (ic > 0)
        {
            _txttraining_course_file_name.Text = "";
            showAlert("ลบไฟล์เอกสารเสร็จแล้ว");
            try
            {
                Directory.Delete(Server.MapPath(_PathFile + "/"));
            }
            catch { }

        }

        //try
        //{
        //    File.Delete(_PathFile);
        //    _txttraining_course_file_name.Text = "";
        //    showAlert("ลบไฟล์เอกสารเสร็จแล้ว");
        //}
        //catch { }


    }

    private void zUploadTOElearning(string sDocNoEl)
    {
        _txttraining_course_file_name = (TextBox)fvCRUD.FindControl("txttraining_course_file_name");
        string sFile = "", sFileName = "";
        string _itemExtension = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFile = Path.GetFileName(file);
                        _itemExtension = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }

        if ((sFile != "") && (sDocNoEl != ""))
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

            string _itemNameNew = "";
            string OldFilePath = "";
            string newFilePath = "";

            OldFilePath = sFile;
            newFilePath = _PathFile + _Folder_plan_course + "/" + sDocNoEl + "/";
            try
            {
                Directory.CreateDirectory(Server.MapPath(newFilePath));
            }
            catch { }
            // Clear File Old In Folder El Mas
            try
            {

                if (newFilePath != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(newFilePath));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }
            }
            catch { }

            string _folder = OldFilePath;
            for (int i = 1; i <= 1; i++)
            {
                _itemNameNew = sDocNoEl + _itemExtension.ToLower();
                string _OldFilePath, _newFilePath;
                _OldFilePath = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _OldFilePath = _OldFilePath + _Folder_plan_course_bin + "/" + Hddfld_folder.Value + "/" + sFile;
                //  _OldFilePath = OldFilePath;
                _newFilePath = newFilePath + _itemNameNew;


                try
                {
                    Directory.Delete(Server.MapPath(_newFilePath));
                }
                catch { }

                try
                {
                    File.Move(Server.MapPath(_OldFilePath), Server.MapPath(_newFilePath));
                    _txttraining_course_file_name.Text = _itemNameNew;
                }
                catch { }
            }
            try
            {
                _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
                Directory.Delete(Server.MapPath(_PathFile));
            }
            catch { }
        }
    }

    public void zDeleteFileBin()
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
        try
        {
            if (_PathFile != "")
            {
                string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                List<ListItem> files = new List<ListItem>();
                foreach (string file in filesLoc)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch { }
                }
            }
        }
        catch { }
    }
    public void importProcessRequest(HttpPostedFile _HttpPostedFile, int item)
    {

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            zDeleteFileBin();
            if (_HttpPostedFile != null && _HttpPostedFile.ContentLength > 0)
            {
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                string _itemExtension = Path.GetExtension(_HttpPostedFile.FileName);
                string _itemNameNew = item.ToString() + _itemExtension.ToLower();
                string _itemFilePath = "";
                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                _itemFilePath = Server.MapPath(newFilePath);
                _HttpPostedFile.SaveAs(_itemFilePath);
            }
        }
        getImg();

    }

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        Hddfld_folder.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");

        if (!IsPostBack)
        {

            zDeleteFileBin();
            zDeleteFileBinEmployee();
            HttpPostedFile file_Uploaded_txtfileimport = Request.Files["Uploaded_txtfileimport"];
            if (file_Uploaded_txtfileimport != null && file_Uploaded_txtfileimport.ContentLength > 0)
            {
                importProcessRequest(file_Uploaded_txtfileimport, 1);
            }
            HttpPostedFile file_Uploaded_file_import_employee = Request.Files["Uploaded_file_import_employee"];
            if (file_Uploaded_file_import_employee != null && file_Uploaded_file_import_employee.ContentLength > 0)
            {
                importEmployeeProcessRequest(file_Uploaded_file_import_employee, 1);
            }
            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
            Hddfld_training_course_no.Value = "";
            Hddfld_u0_training_course_idx.Value = "";
            Hddfld_status.Value = "P";


            // zCrontrollFrom();
            // zSetMode(2);
            zCrontrollFrom();
            //ShowListData();
            SETFOCUS.Focus();
            _dtEmployee = callServiceEmpProfile(urlGetMyProfile + emp_idx.ToString());
            ViewState["cost_no"] = _dtEmployee.employee_list[0].costcenter_idx;
            //ViewState["OrgIDX"] = _dtEmployee.employee_list[0].org_idx;
            //ViewState["RDeptIDX"] = _dtEmployee.employee_list[0].rdept_idx;
            select_admin(int.Parse(_dtEmployee.employee_list[0].rsec_idx.ToString()));
            ViewState["ddl_location_type"] = "1";

        }
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(lbApprove);
        linkBtnTrigger(btnHR_Wait);
        linkBtnTrigger(btnHR_App);
        linkBtnTrigger(btnFilter);

        linkBtnTrigger(btnSaveInsert);
        linkBtnTrigger(btnClear);
        linkBtnTrigger(btnscheduler);
        linkBtnTrigger(btnS_SCHED);
        linkBtnTrigger(btnprint_sched);

        linkBtnTrigger(btnrptscheduler);

        linkBtnTrigger(btnLeaderList);
        linkBtnTrigger(btnLeaderApproveList);
        linkBtnTrigger(btnFilterLeader);
        linkBtnTrigger(btnFilterLeader_app);


    }
    #endregion Page Load


    /* Start Std*/
    #region Std

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_elearning callServiceElearning(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);
        return _data_elearning;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected void select_ddl_expenses_description(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกรายละเอียดค่าใช้จ่าย...", "0"));

        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        dtelearning.type_select_expert = 2; //select

        _data_elearning.m0_expert_salary_action[0] = dtelearning;
        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);

        ddlName.DataSource = _data_elearning.m0_expert_salary_action;
        ddlName.DataTextField = "expert_detail";
        ddlName.DataValueField = "EPIDX";
        ddlName.DataBind();
    }

    protected void select_admin(int rsecidx)
    {
        _data_elearning = new data_elearning();

        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost _select = new m0_course_cost();

        _select.type_select_course = 8;
        _select.rsec_idx = rsecidx;
        _data_elearning.m0_course_cost_action[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        ViewState["return_admin"] = _data_elearning.return_code.ToString();

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกองค์กร...", "0"));


        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmployee(_urlGetOrganizationList, _dtEmployee);

        ddlName.DataSource = _dtEmployee.organization_list;
        ddlName.DataTextField = "org_name_th";
        ddlName.DataValueField = "org_idx";
        ddlName.DataBind();

        //setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        //ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกฝ่าย...", "0"));

        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmployee(_urlGetDepartmentList, _dtEmployee);

        ddlName.DataSource = _dtEmployee.department_list;
        ddlName.DataTextField = "dept_name_th";
        ddlName.DataValueField = "rdept_idx";
        ddlName.DataBind();

        //setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        // ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }
    protected void select_ddl_costcenter(DropDownList ddlName, int rdeptidx)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter", "0"));

        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.type_select_course = 9; //select
        dtelearning.rdept_idx = rdeptidx;

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        ddlName.DataSource = _data_elearning.m0_course_cost_action;
        ddlName.DataTextField = "cost_name";
        ddlName.DataValueField = "cost_idx";
        ddlName.DataBind();
    }

    protected void select_sum_approve(int jobgradeidx, int rsecidx, int cempidx, int rposidx)
    {
        _nav_approve = (Label)panelMenu.FindControl("nav_approve");
        _data_elearning = new data_elearning();

        _data_elearning.el_training_course_action = new training_course[1];
        training_course _select = new training_course();

        _select.operation_status_id = "ApproveList";
        _select.JobLevel = jobgradeidx;
        _select.rsec_idx = rsecidx;
        _select.training_course_created_by = cempidx;
        _select.rpos_idx_ref = rposidx;

        _data_elearning.el_training_course_action[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _data_elearning = callServiceElearning(_urlGetel_u_manage, _data_elearning);
        _nav_approve.Text = "<span class='badge progress-bar-danger' >" + _data_elearning.return_msg.ToString() + "</span>";

    }

    protected void select_approve_list(GridView Gvname, int jobgradeidx, int rsecidx, int cempidx, int rposidx)
    {
        _data_elearning = new data_elearning();

        _data_elearning.el_training_course_action = new training_course[1];
        training_course _select = new training_course();

        _select.operation_status_id = "ApproveList";
        _select.JobLevel = jobgradeidx;
        _select.rsec_idx = rsecidx;
        _select.training_course_created_by = cempidx;
        _select.rpos_idx_ref = rposidx;

        _data_elearning.el_training_course_action[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _data_elearning = callServiceElearning(_urlGetel_u_manage, _data_elearning);
        _func_dmu.zSetGridData(Gvname, _data_elearning.el_training_course_action);

    }

    protected void selectstatus(DropDownList ddlName, int unidx)
    {
        _data_elearning = new data_elearning();

        _data_elearning.el_training_course_action = new training_course[1];
        training_course _select = new training_course();

        _select.operation_status_id = "DDLApprove";
        _select.node_idx = unidx;

        _data_elearning.el_training_course_action[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _data_elearning = callServiceElearning(_urlGetel_u_manage, _data_elearning);
        _func_dmu.zSetDdlData(ddlName, _data_elearning.el_training_course_action, "status_name", "staidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะอนุมัติ...", "0"));

    }

    protected void selectdetail_print(FormView fvname, int u0_trainingcourse)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = u0_trainingcourse;
        obj.operation_status_id = "U0-FULL";
        dataelearning.el_training_course_action[0] = obj;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        ViewState["m0_course_plan_status"] = dataelearning.el_training_course_action[0].course_plan_status.ToString();
        ViewState["m0_fileidx_ref_print"] = dataelearning.el_training_course_action[0].m0_fileidx_ref.ToString();

        _func_dmu.zSetFormViewData(fvname, dataelearning.el_training_course_action);


    }

    protected void selectinstitution_print(Label lblname, int u0_trainingcourse)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = u0_trainingcourse;
        obj.operation_status_id = "U1-FULL";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        if (dataelearning.return_code.ToString() == "0")
        {
            int i = 0;

            foreach (var data in dataelearning.el_training_course_action)
            {
                lblname.Text += data.institution_name.ToString() + ",";
                i++;
            }
        }
    }

    protected void selectobjective_print(Label lblqty, Literal lbltestper, Literal lithour, int u0_trainingcourse)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = u0_trainingcourse;
        obj.operation_status_id = "U2-FULL";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        if (dataelearning.return_code.ToString() == "0")
        {
            int i = 0;

            foreach (var data in dataelearning.el_training_course_action)
            {
                lblqty.Text += data.objective_name.ToString() + ",";
                lbltestper.Text = data.pass_test_per.ToString();
                lithour.Text = data.hour_training_per.ToString();

                ViewState["check_passtest"] = data.pass_test_flag.ToString();
                ViewState["check_hour"] = data.hour_training_flag.ToString();
                ViewState["check_writereport"] = data.write_report_training_flag.ToString();
                ViewState["check_publish"] = data.publish_training_flag.ToString();
                ViewState["check_publish_des"] = data.publish_training_description.ToString();
                ViewState["check_course_lec"] = data.course_lecturer_flag.ToString();
                ViewState["check_otherflag"] = data.other_flag.ToString();
                ViewState["check_other_des"] = data.other_description.ToString();

                ViewState["check_hrd_nofollow_flag"] = data.hrd_nofollow_flag.ToString();
                ViewState["check_hrd_follow_flag"] = data.hrd_follow_flag.ToString();
                ViewState["check_hrd_follow_day"] = data.hrd_follow_day.ToString();

                i++;
            }
        }
    }

    protected void selectemployee_print(Literal litempcode, RadioButton rdoempfile, RadioButton rdoempform, Control divname, GridView gvName, Label lblname, int u0_trainingcourse)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = u0_trainingcourse;
        obj.operation_status_id = "U3-FULL";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        int count = int.Parse(dataelearning.el_training_course_action.Length.ToString());

        if (count > 3)
        {
            rdoempfile.Checked = true;
            rdoempform.Checked = false;

            _func_dmu.zSetGridData(gvName, dataelearning.el_training_course_action);
            divname.Visible = true;
        }
        else
        {
            rdoempfile.Checked = false;
            rdoempform.Checked = true;
            divname.Visible = false;

            int i = 0;
            int no = 1;
            foreach (var data in dataelearning.el_training_course_action)
            {
                litempcode.Text += "&nbsp;&nbsp;" + no.ToString() + ".รหัสพนักงาน   " + dataelearning.el_training_course_action[i].EmpCode.ToString() + "&nbsp;&nbsp;&nbsp;" +
                    "ชื่อ-สกุล   " + dataelearning.el_training_course_action[i].FullNameTH.ToString() + "&nbsp;&nbsp;&nbsp;" +
                    "ตำแหน่ง   " + dataelearning.el_training_course_action[i].PosNameTH.ToString() + "&nbsp;&nbsp;&nbsp;" +
                    "Cost Center   " + dataelearning.el_training_course_action[i].CostNo.ToString() + "&nbsp;&nbsp;&nbsp;" +
                    "เบอร์ติดต่อ   " + dataelearning.el_training_course_action[i].MobileNo.ToString() + "<br />";
                i++;
                no++;
            }
        }
        lblname.Text = dataelearning.el_training_course_action.Length.ToString();

    }

    protected void selectexpenses_print(GridView gvName, int u0_trainingcourse)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = u0_trainingcourse;
        obj.operation_status_id = "U4-FULL";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        _func_dmu.zSetGridData(gvName, dataelearning.el_training_course_action);

    }

    protected void select_text_costbalance(TextBox txtname, int costcenter)
    {

        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.type_select_course = 5; //select
        dtelearning.cost_idx = costcenter;

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        if (_data_elearning.return_code.ToString() == "0" && _data_elearning.m0_course_cost_action[0].cost_balance.ToString() != "0.00")
        {
            txtname.Text = _data_elearning.m0_course_cost_action[0].cost_balance.ToString();
            btnSaveInsert.Visible = true;

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีงบกรุณาทำเรื่องของบประมาณในการอบรม!!!');", true);
            txtname.Text = "0";
            btnSaveInsert.Visible = false;
        }
    }

    protected void Update_System(int unidx, int acidx, int staidx, int u0_training_course_idx, string comment, int empidx, int type_request, int cost_idx_ref, decimal training_course_budget_total)
    {

        _data_elearning = new data_elearning();

        _data_elearning.el_training_course_action = new training_course[1];
        training_course _select = new training_course();

        _select.operation_status_id = "UpdateApprove";
        _select.node_idx = unidx;
        _select.actor_idx = acidx;
        _select.app_flag = staidx;
        _select.u0_training_course_idx = u0_training_course_idx;
        _select.approve_remark = comment;
        _select.training_course_created_by = empidx;
        _select.type_request = type_request;
        _select.costcenter_idx_ref = cost_idx_ref;
        _select.training_course_budget_total = training_course_budget_total;

        _data_elearning.el_training_course_action[0] = _select;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _data_elearning = callServiceElearning(_urlSetInsel_u_plan_course, _data_elearning);

    }


    protected void selectcourse_train(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("กรุณาเลือกคอร์สอบรม...", "0"));


        _data_elearning.Boxu0_QuizDocument = new U0_QuizDocument[1];
        U0_QuizDocument _select = new U0_QuizDocument();

        _select.condition = 1;

        _data_elearning.Boxu0_QuizDocument[0] = _select;
        //    litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtelerning));

        _data_elearning = callServiceElearning(_urlGetMasterQuiz, _data_elearning);

        ddlName.DataSource = _data_elearning.Boxu0_QuizDocument;
        ddlName.DataTextField = "course_name";
        ddlName.DataValueField = "u0_course_idx";
        ddlName.DataBind();

    }

    protected void select_chklist_file(CheckBoxList chklistname)
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.type_select_course = 10; //select

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        chklistname.DataSource = _data_elearning.m0_course_cost_action;
        chklistname.DataTextField = "file_name";
        chklistname.DataValueField = "m0_fileidx";
        chklistname.DataBind();
    }

    protected void select_wherescore(int u0_course_idx, TextBox score, TextBox score_per)
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.type_select_course = 11; //select
        dtelearning.u0_course_idx = u0_course_idx;
        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);

        if (_data_elearning.return_code.ToString() == "0")
        {
            score.Text = _data_elearning.el_u0_course_list[0].course_score.ToString();
            score_per.Text = _data_elearning.el_u0_course_list[0].score_through_per.ToString();
        }
        else

        {
            score.Text = "0";
            score_per.Text = "0";
        }

        //chklistname.DataSource = _data_elearning.m0_course_cost_action;
        //chklistname.DataTextField = "file_name";
        //chklistname.DataValueField = "m0_fileidx";
        //chklistname.DataBind();
    }
    protected void select_ddl_expenses_description_detail(int EPIDX)
    {
        _data_elearning.m0_expert_salary_action = new m0_expert_salary[1];
        m0_expert_salary dtelearning = new m0_expert_salary();

        dtelearning.EPIDX = EPIDX;
        dtelearning.type_select_expert = 2; //select

        _data_elearning.m0_expert_salary_action[0] = dtelearning;
        _data_elearning = callServiceElearning(_urlget_expert_salary, _data_elearning);

        //ViewState["expert_detail_ddl"] = _data_elearning.m0_expert_salary_action[0].expert_detail;
        ViewState["expert_num_ddl"] = _data_elearning.m0_expert_salary_action[0].expert_num;
        ViewState["expert_vat_ddl"] = _data_elearning.m0_expert_salary_action[0].expert_vat;
    }

    protected void select_ddl_training_location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกสถานที่อบรม...", "0"));

        _data_elearning.m0_training_location_action = new m0_training_location[1];
        m0_training_location dtelearning = new m0_training_location();

        dtelearning.location_type = int.Parse(ViewState["ddl_location_type"].ToString()); //select
        dtelearning.type_select_location = 2; //select

        _data_elearning.m0_training_location_action[0] = dtelearning;

        _data_elearning = callServiceElearning(urlget_training_location, _data_elearning);
        ddlName.DataSource = _data_elearning.m0_training_location_action;
        ddlName.DataTextField = "location_name";
        ddlName.DataValueField = "LOIDX";
        ddlName.DataBind();
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {

            DropDownList ddName = (DropDownList)sender;
            TextBox txtamount = (TextBox)fvCRUD.FindControl("txtamount");
            TextBox txtvat = (TextBox)fvCRUD.FindControl("txtvat");
            DropDownList ddlexpenses_description = (DropDownList)fvCRUD.FindControl("ddlexpenses_description");
            DropDownList ddlOrgID = (DropDownList)fvCRUD.FindControl("ddlOrgID");
            DropDownList ddlRDeptID_ref = (DropDownList)fvCRUD.FindControl("ddlRDeptID_ref");
            DropDownList ddlcostcenter_idx_ref = (DropDownList)fvCRUD.FindControl("ddlcostcenter_idx_ref");
            TextBox txttraining_course_planbudget_total = (TextBox)fvCRUD.FindControl("txttraining_course_planbudget_total");
            TextBox txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
            TextBox txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");
            Control divother_expense = (Control)fvCRUD.FindControl("divother_expense");
            TextBox txtexpense_other = (TextBox)fvCRUD.FindControl("txtexpense_other");
            TextBox txtshow_planbudget_total = (TextBox)fvCRUD.FindControl("txtshow_planbudget_total");


            switch (ddName.ID)
            {
                case "ddlexpenses_description":
                    select_ddl_expenses_description_detail(int.Parse(ddlexpenses_description.SelectedValue));
                    txtamount.Text = ViewState["expert_num_ddl"].ToString();
                    txtvat.Text = ViewState["expert_vat_ddl"].ToString();

                    if (ddlexpenses_description.SelectedValue == "5")
                    {
                        divother_expense.Visible = true;
                        txtamount.Enabled = true;
                        txtvat.Enabled = true;
                        txtexpense_other.Text = String.Empty;
                    }
                    else
                    {
                        divother_expense.Visible = false;
                        txtamount.Enabled = false;
                        txtvat.Enabled = false;
                    }
                    break;

                case "ddl_type_plan":
                    if (ddl_type_plan.SelectedValue == "1")
                    {
                        Hddfld_in_out_plan.Value = "IP";
                        showdata();
                    }
                    else if (ddl_type_plan.SelectedValue == "2")
                    {
                        Hddfld_in_out_plan.Value = "OP";
                        showdata();
                        zCalculateCosts();
                    }
                    else
                    {
                        fvCRUD.Visible = false;
                        btnSaveInsert.Visible = false;
                    }

                    linkFvTrigger(fvCRUD);

                    break;

                case "ddlOrgID":
                    getDepartmentList(ddlRDeptID_ref, int.Parse(ddlOrgID.SelectedValue));
                    break;

                case "ddlRDeptID_ref":
                    select_ddl_costcenter(ddlcostcenter_idx_ref, int.Parse(ddlRDeptID_ref.SelectedValue));
                    break;

                case "ddlcostcenter_idx_ref":
                    select_text_costbalance(txttraining_course_planbudget_total, int.Parse(ddlcostcenter_idx_ref.SelectedValue));
                    txtshow_planbudget_total.Text = string.Format("{0:N2}", Convert.ToDecimal(txttraining_course_planbudget_total.Text));

                    break;

                case "ddlu0_training_plan_idx_ref":
                case "ddl_training_name_add":
                    select_wherescore(int.Parse(ddName.SelectedValue), txtcourse_score, txtscore_through_per);
                    break;

            }
        }

        else if (sender is RadioButtonList)
        {
            RadioButtonList rdoName = (RadioButtonList)sender;
            RadioButtonList rdolecturer_type_place = (RadioButtonList)fvCRUD.FindControl("rdolecturer_type_place");
            DropDownList _ddl_training_location = (DropDownList)fvCRUD.FindControl("ddl_training_location");
            RadioButtonList rdllecturer_type = (RadioButtonList)fvCRUD.FindControl("rdllecturer_type");

            switch (rdoName.ID)
            {
                case "rdolecturer_type_place":
                    if (rdolecturer_type_place.SelectedValue == "0")
                    {
                        ViewState["ddl_location_type"] = "1";
                    }
                    else
                    {
                        ViewState["ddl_location_type"] = "2";
                    }
                    select_ddl_training_location(_ddl_training_location);
                    break;
            }
        }
    }

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        btnSaveInsert.Visible = false;
        switch (AiMode)
        {
            case 0:  //insert mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_trainingPage);
                    fvCRUD.Visible = true;
                    fvCRUD.ChangeMode(FormViewMode.Insert);

                    if (Hddfld_in_out_plan.Value == "OP")
                    {
                        setActiveTab("IOP");
                    }
                    else
                    {
                        setActiveTab("I");
                    }

                    actionIndex();
                    Select_Page1showdata();

                }
                break;
            case 1:  //update mode
                {
                    pnlListData.Visible = false;
                    MultiViewBody.SetActiveView(View_trainingPage);

                    //  fvCRUD.Visible = false;
                    if (Hddfld_in_out_plan.Value == "OP")
                    {
                        ddl_type_plan.SelectedValue = "2";
                    }
                    else
                    {
                        ddl_type_plan.SelectedValue = "1";
                    }
                    ddl_type_plan.Enabled = false;
                    fvCRUD.Visible = true;
                    fvCRUD.ChangeMode(FormViewMode.Edit);
                    setActiveTab("I");
                    actionIndex();

                }
                break;
            case 2://preview mode
                {
                    pnlListData.Visible = true;
                    setActiveTab("P");
                    ShowListData();
                    MultiViewBody.SetActiveView(View_ListDataPag);
                    select_sum_approve(int.Parse(ViewState["joblevel_idx"].ToString()), int.Parse(ViewState["rsec_idx"].ToString()), emp_idx, int.Parse(ViewState["rpos_idx"].ToString()));
                }
                break;
            case 3://รายการที่รอ HRD อนุมัติ
                {
                    pnlListData.Visible = false;
                    setActiveTab("HR_W");
                    MultiViewBody.SetActiveView(View_HR_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_HR_W.Text = "";
                }
                break;
            case 4://รายการที่ HRD อนุมัติเสร็จแล้ว
                {
                    pnlListData.Visible = false;
                    setActiveTab("HR_A");
                    MultiViewBody.SetActiveView(View_HR_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_HR_A.Text = "";
                }
                break;
            case 5://รายการ HRD Detail
                {
                    pnlListData.Visible = false;
                    setActiveTab("");
                    MultiViewBody.SetActiveView(View_HR_WDetail);
                    fv_preview.Visible = true;
                    fv_preview.ChangeMode(FormViewMode.Edit);
                }
                break;
            case 6://รายการที่รอ MD อนุมัติ
                {
                    pnlListData.Visible = false;
                    setActiveTab("MD_W");
                    MultiViewBody.SetActiveView(View_MD_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_MD_W.Text = "";
                }
                break;
            case 7://รายการที่ MD อนุมัติเสร็จแล้ว
                {
                    pnlListData.Visible = false;
                    setActiveTab("MD_A");
                    MultiViewBody.SetActiveView(View_MD_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_MD_A.Text = "";
                }
                break;
            case 8://ตารางแผนการอบรม
                {
                    pnlListData.Visible = false;
                    setActiveTab("SCHEDULER");
                    MultiViewBody.SetActiveView(View_SCHEDList);
                    _func_dmu.zDropDownList(ddlYearSearch_SCHED,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR-SCHED"
                                );
                    txtFilterKeyword_SCHED.Text = "";
                    _func_dmu.zDropDownList(ddltrn_groupSearch_SCHED,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-GRP-SCHED"
                                );
                }
                break;
            case 10://ผลการฝึกอบรม
                {

                    setActiveTab("summary");
                    MultiViewBody.SetActiveView(View_summaryList);
                    _func_dmu.zDropDownList(ddlYearSearch_summary,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_summary.Text = "";
                }
                break;
            case 11://ผลการฝึกอบรม
                {
                    setActiveTab("summary");
                    MultiViewBody.SetActiveView(View_summarycourse);
                }
                break;
            case 12://preview mode รายการที่รออนุมัติ
                {
                    setActiveTab("liLeaderList");
                    MultiViewBody.SetActiveView(View_ListDataLeader);
                    _func_dmu.zDropDownList(ddlYearSearch_Leader,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_Leader.Text = "";
                    ddlStatusapprove_Leader.SelectedIndex = 0;
                }
                break;
            case 13://รายการที่อนุมัติเสร็จแล้ว
                {
                    setActiveTab("liLeaderApproveList");
                    MultiViewBody.SetActiveView(View_ListDataLeader);
                    _func_dmu.zDropDownList(ddlYearSearch_Leader,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_Leader.Text = "";
                    ddlStatusapprove_Leader.SelectedIndex = 0;
                }
                break;

        }


    }
    #endregion zSetMode

    private void ShowListHR_W() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "HR-W";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvHR_WList, dataelearning.el_training_course_action);
        ShowListHR_W_Status();
    }
    private void ShowListHR_W_Status() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "HR-W";
        obj.operation_status_id = "U0-LISTDATA-HR-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        //iCount = 99;
        if (iCount > 0)
        {
            btnHR_Wait.Text = "รายการที่รอ HROD Manager อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnHR_Wait.Text = "รายการที่รอ HROD Manager อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }
    private void ShowListHR_A() //รายการที่ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_HR_A.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_A.SelectedValue);
        obj.zstatus = "HR-A";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvHR_AList, dataelearning.el_training_course_action);
        ShowListHR_W_Status();
    }

    private void ShowListMD_W() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_MD_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvMD_WList, dataelearning.el_training_course_action);
        ShowListMD_W_Status();
    }
    private void ShowListMD_W_Status() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        //iCount = 99;
        if (iCount > 0)
        {
            btnMD_Wait.Text = "รายการที่รอ Chief People Officer อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnMD_Wait.Text = "รายการที่รอ Chief People Officer อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }
    private void ShowListMD_A() //รายการที่ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_MD_A.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_A.SelectedValue);
        obj.zstatus = "MD-A";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvMD_AList, dataelearning.el_training_course_action);
        ShowListMD_W_Status();
    }


    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        //  liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        liHR_Wait.Attributes.Add("class", "");
        liHR_App.Attributes.Add("class", "");
        liMD_Wait.Attributes.Add("class", "");
        liMD_App.Attributes.Add("class", "");
        liApprove.Attributes.Add("class", "");
        li_courseplan.Attributes.Add("class", "");
        liplanReport.Attributes.Add("class", "");
        lischeduler.Attributes.Add("class", "");
        lirptscheduler.Attributes.Add("class", "");
        lisummary.Attributes.Add("class", "");
        //  licourse_out_plan.Attributes.Add("class", "");
        liLeaderList.Attributes.Add("class", "");
        liLeaderApproveList.Attributes.Add("class", "");

        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "PLAN":
            case "IOP":
            case "I":
                li_courseplan.Attributes.Add("class", "active");
                break;
            case "HR_W":
                liHR_Wait.Attributes.Add("class", "active");
                break;
            case "HR_A":
                liHR_App.Attributes.Add("class", "active");
                break;
            case "MD_W":
                liMD_Wait.Attributes.Add("class", "active");
                break;
            case "MD_A":
                liMD_App.Attributes.Add("class", "active");
                break;
            case "SCHEDULER":
                liplanReport.Attributes.Add("class", "active");
                lischeduler.Attributes.Add("class", "active");
                break;
            case "RPT_SCHEDULER":
                liplanReport.Attributes.Add("class", "active");
                lirptscheduler.Attributes.Add("class", "active");
                break;
            case "summary":
                lisummary.Attributes.Add("class", "active");
                break;

            case "liLeaderList":
                liLeaderList.Attributes.Add("class", "active");
                break;
            case "liLeaderApproveList":
                liLeaderApproveList.Attributes.Add("class", "active");
                break;

            case "Approve":
                liApprove.Attributes.Add("class", "active");
                break;
        }

    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {

    }

    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    protected void Select_Page1showdata()
    {
        ViewState["zUpdate"] = "";
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();

        obj.operation_status_id = "U0-EMPTY";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_training_course_action);

        _txttraining_course_no = (TextBox)fvCRUD.FindControl("txttraining_course_no");
        _txttraining_group_name = (TextBox)fvCRUD.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)fvCRUD.FindControl("txttraining_branch_name");
        _ddlu0_training_plan_idx_ref = (DropDownList)fvCRUD.FindControl("ddlu0_training_plan_idx_ref");
        _ddl_training_name_add = (DropDownList)fvCRUD.FindControl("ddl_training_name_add");
        _txttraining_course_date = (TextBox)fvCRUD.FindControl("txttraining_course_date");
        _txttraining_course_year = (TextBox)fvCRUD.FindControl("txttraining_course_year");
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        _ddlemp_idx_ref = (DropDownList)fvCRUD.FindControl("ddlemp_idx_ref");
        _ddlcourse_plan_status = (DropDownList)fvCRUD.FindControl("ddlcourse_plan_status");

        _txtdatestart_create = (TextBox)fvCRUD.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)fvCRUD.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)fvCRUD.FindControl("txt_timeend_create");
        _txttraining_course_date_qty = (TextBox)fvCRUD.FindControl("txttraining_course_date_qty");
        _txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        _txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");

        Label Label67 = (Label)fvCRUD.FindControl("Label67");
        //Label67.Text = dataelearning.el_training_course_action[0].CostIDX.ToString();
        //_txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");

        _txttraining_course_date.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _txttraining_course_year.Text = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);

        _txtdatestart_create.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _txt_timestart_create.Text = "09.00";
        _txt_timeend_create.Text = "16.00";
        _txttraining_course_date_qty.Text = "6";

        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        Hddfld_status.Value = "I";
        if (Hddfld_in_out_plan.Value == "OP")
        {
            setObject();
            selectcourse_train(_ddl_training_name_add);

            _func_dmu.zClearDataDropDownList(_ddm0_training_branch_idx);
            _func_dmu.zDropDownList(_ddlm0_training_group_idx_ref,
                                "",
                                "m0_training_group_idx",
                                "training_group_name",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-GROUP"
                                );
            _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-TARGET"
                                );
            _txtcourse_score.Enabled = false;
            _txtscore_through_per.Enabled = false;
        }
        else
        {
            setObject();
            selectcourse_train(_ddlu0_training_plan_idx_ref);
            _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-TARGET"
                                );
            //_func_dmu.zDropDownListWhereID(_ddlu0_training_plan_idx_ref,
            //                    "",
            //                    "zId",
            //                    "zName",
            //                    "0",
            //                    "trainingLoolup",
            //                    "",
            //                    "TRN-PLAN-U0-LOOKUP",
            //                    "zId_G",
            //                    getcourse_plan_status(_ddlcourse_plan_status.SelectedValue)
            //                    );
            _txtcourse_score.Enabled = false;
            _txtscore_through_per.Enabled = false;
            // litDebug.Text = _ddlcourse_plan_status.SelectedValue;
        }
        setdatadetail_default();
        Panel pnladd_emp_resulte = (Panel)fvCRUD.FindControl("pnladd_emp_resulte");
        pnladd_emp_resulte.Visible = false;
        setplace();

    }

    private void setdatadetail_default()
    {
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        _ddlemp_idx_ref = (DropDownList)fvCRUD.FindControl("ddlemp_idx_ref");

        _rdllecturer_type = (RadioButtonList)fvCRUD.FindControl("rdllecturer_type");
        setlecturertype(_func_dmu.zStringToInt(_rdllecturer_type.SelectedValue), 0);
        CreateDs_el_u1_training_course_lecturer();
        setinstitutionDefault();

        _func_dmu.zDropDownList(_ddlm0_objective_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "M-OBJECTIVE"
                                );
        CreateDs_el_u2_training_course_objective();
        setObjectiveDefault();
        //if ((Hddfld_in_out_plan.Value == "OP") && (ViewState["rpos_idx"].ToString() != "5900"))
        if (ViewState["return_admin"].ToString() == "0")
        {
            _func_dmu.zDropDownList(_ddlemp_idx_ref,
                                   "",
                                   "zId",
                                   "zName",
                                   "0",
                                   "trainingLoolup",
                                   "",
                                   "EMPLOYEE"
                                   );

        }
        else
        {
            _func_dmu.zDropDownListWhereID(_ddlemp_idx_ref,
                                  "",
                                  "zId",
                                  "zName",
                                  "0",
                                  "trainingLoolup",
                                  "",
                                  "EMPLOYEE_OP",
                                  "DeptIDX",
                                  _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString())
                                  );
        }
        CreateDs_el_u3_training_course_employee();
        setEmployeeDefault();

        CreateDs_el_u4_training_course_expenses();
        setExpensesDefault();
        //setCourseSel();

        CreateDs_el_u8_training_course_date();
        setTrainingDateDefault();


    }

    private int getcourse_plan_status(string value)
    {
        if (value == "I")
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    private void ShowListData(string status = "")
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_L.Text;
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.approve_status = int.Parse(ddlStatusapprove_L.SelectedValue);
        obj.operation_status_id = "U0-LISTDATA";
        obj.zstatus = Hddfld_permission.Value;
        obj.emp_idx_ref = emp_idx;
        obj.course_plan_status = ddlcourse_plan_status_L.SelectedValue;
        obj.JobLevel = _func_dmu.zStringToInt(ViewState["joblevel_idx"].ToString());
        obj.RSecID = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());

        if (Hddfld_permission_approve.Value == "LEADER")
        {
            obj.zstatus = "LEADER";
        }
        // litDebug.Text ="ccccccccc"+ obj.zstatus;

        dataelearning.el_training_course_action[0] = obj;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        _func_dmu.zSetGridData(GvListData, dataelearning.el_training_course_action);
        // litDebug.Text = Hddfld_permission.Value;
    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        //var chkName = (CheckBoxList)sender;
        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }
        else if (sender is CheckBoxList)
        {
            var rdName = (CheckBoxList)sender;

            CheckBoxList chkchoice_file = ((CheckBoxList)fvCRUD.FindControl("chkchoice_file"));
            Control div_otherreason = ((Control)fvCRUD.FindControl("div_otherreason"));


            switch (rdName.ID)
            {
                case "chkchoice_file":

                    ViewState["ChkAns"] = null;
                    List<String> AddoingList = new List<string>();
                    foreach (ListItem item in chkchoice_file.Items)
                    {
                        if (item.Selected)
                        {
                            AddoingList.Add(item.Value);
                        }
                    }

                    String Addoing = String.Join(",", AddoingList.ToArray());
                    ViewState["ChkAns"] = Addoing;

                    //litDebug.Text = ViewState["ChkAns"].ToString();

                    string[] ToId = ViewState["ChkAns"].ToString().Split(',');


                    foreach (string To_check in ToId)
                    {

                        if (To_check == "5")
                        {
                            div_otherreason.Visible = true;

                        }
                        else
                        {
                            div_otherreason.Visible = false;
                        }

                    }

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.FindControl("btnDelete_GvListData");
                TextBox txtapprove_status = (TextBox)e.Row.FindControl("txtapprove_status_GvListData");
                TextBox txtmd_approve_status = (TextBox)e.Row.FindControl("txtmd_approve_status_GvListData");
                HyperLink btnDownloadFile = (HyperLink)e.Row.FindControl("btnDownloadFile");
                TextBox txtGvtraining_course_no = (TextBox)e.Row.FindControl("txtGvtraining_course_no");
                TextBox txtGvsuper_app_status = (TextBox)e.Row.FindControl("txtGvsuper_app_status");
                LinkButton btnqrcode = (LinkButton)e.Row.FindControl("btnqrcode");
                Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");
                Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                Label lblCempIDX = (Label)e.Row.FindControl("lblCempIDX");
                Boolean bBl = true, bBl_a = true;
                LinkButton btnDetail = (LinkButton)e.Row.FindControl("btnDetail");


                linkBtnTrigger(btnDetail);
                btnDownloadFile.NavigateUrl = DownloadFile(txtGvtraining_course_no.Text);
                if (btnDownloadFile.NavigateUrl == "")
                {
                    btnDownloadFile.Visible = false;
                }
                else
                {
                    btnDownloadFile.Visible = true;
                }

                if ((txtapprove_status.Text == "4") || (txtmd_approve_status.Text == "4")
                     || (txtGvsuper_app_status.Text == "4")
                    )// 4	อนุมัติ
                {
                    bBl = false;
                    bBl_a = true;

                }
                else if ((txtapprove_status.Text == "6") || (txtmd_approve_status.Text == "6")
                    || (txtGvsuper_app_status.Text == "6")
                    )
                {
                    bBl = false;
                    bBl_a = false;
                }
                if ((txtapprove_status.Text == "5") || (txtmd_approve_status.Text == "5")
                    || (txtGvsuper_app_status.Text == "5")
                    )// 4	อนุมัติ
                {
                    bBl = true;
                    bBl_a = true;
                }
                if ((txtapprove_status.Text == "4")
                     || (txtmd_approve_status.Text == "4")
                     || (txtGvsuper_app_status.Text == "4")
                     )// 4	อนุมัติ
                {
                    if (Hddfld_permission.Value == "USER")
                    {
                        bBl = false;
                        bBl_a = false;
                    }
                    else if (Hddfld_permission.Value == "HR")
                    {
                        bBl = false;
                        bBl_a = true;
                    }
                }
                if (
                    (txtapprove_status.Text == "4")
                     &&
                     (txtmd_approve_status.Text == "4")
                     )// 4	อนุมัติ
                {
                    btnqrcode.Visible = true;
                }
                else
                {
                    btnqrcode.Visible = false;
                }
                btnqrcode.Visible = false;
                //btnUpdate_GvListData.Visible = bBl_a;
                //btnDelete_GvListData.Visible = bBl;


                switch (int.Parse(lblunidx.Text))
                {
                    case 1:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                        if (int.Parse(lblCempIDX.Text) == emp_idx)
                        {
                            btnDelete_GvListData.Visible = true;
                        }
                        else
                        {
                            btnDelete_GvListData.Visible = false;
                        }
                        break;
                    case 2:

                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                        if (int.Parse(lblCempIDX.Text) == emp_idx)
                        {
                            btnDelete_GvListData.Visible = true;
                        }
                        else
                        {
                            btnDelete_GvListData.Visible = false;
                        }
                        break;
                    case 3:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                        btnDelete_GvListData.Visible = false;
                        break;
                    case 4:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");
                        btnDelete_GvListData.Visible = false;
                        break;
                    case 5:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0099ff");
                        btnDelete_GvListData.Visible = false;
                        break;
                    case 6:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#4F77B9");
                        btnDelete_GvListData.Visible = false;
                        break;
                    case 7:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#803DA9");
                        btnDelete_GvListData.Visible = false;
                        break;
                    case 9:
                        if (lblstaidx.Text == "2")
                        {
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        }

                        else
                        {
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                        }


                        btnDelete_GvListData.Visible = false;
                        break;

                }
                btnUpdate_GvListData.Visible = false;

            }
        }
        else if (sGvName == "GvMonthList_L")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnUpdate_GvListData");
                //LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDelete_GvListData");
                //TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvListData");
                //TextBox txtmd_approve_status = (TextBox)e.Row.Cells[9].FindControl("txtmd_approve_status_GvListData");

                //Boolean bBl = true;
                //btnUpdate_GvListData.Visible = bBl;
                //btnDelete_GvListData.Visible = bBl;
            }

        }
        else if (sGvName == "Gvemp")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnUpdate_GvListData");

                //Boolean bBl = true;
                //btnUpdate_GvListData.Visible = bBl;
                //btnDelete_GvListData.Visible = bBl;
            }

        }
        else if (sGvName == "GvsummaryList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtGvtraining_course_no = (TextBox)e.Row.Cells[6].FindControl("txtGvtraining_course_no");
                TextBox txtGvemp_idx_ref = (TextBox)e.Row.Cells[6].FindControl("txtGvemp_idx_ref");
                HyperLink btnDownloadFile = (HyperLink)e.Row.Cells[6].FindControl("btnDownloadFile");
                Label lbu0_training_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_training_course_idx");
                Label lbu0_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_course_idx");

                btnDownloadFile.NavigateUrl = DownloadFile(txtGvtraining_course_no.Text + "_emp" + txtGvemp_idx_ref.Text, "plan_course_summ_file");
                if (btnDownloadFile.NavigateUrl == "")
                {
                    btnDownloadFile.Visible = false;
                }
                else
                {
                    btnDownloadFile.Visible = true;
                }

                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[4].FindControl("rptu8trncoursedate");
                setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);

            }

        }
        else if (sGvName == "GvApprovelist")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");
                Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                LinkButton btnDetail = (LinkButton)e.Row.FindControl("btnDetail");

                linkBtnTrigger(btnDetail);


                switch (int.Parse(lblunidx.Text))
                {
                    case 1:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                        break;
                    case 2:

                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                        break;
                    case 3:

                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                        break;
                    case 4:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");

                        break;
                    case 5:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0099ff");
                        break;
                    case 6:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#4F77B9");

                        break;
                    case 7:
                        lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#803DA9");

                        break;
                    case 9:
                        if (lblstaidx.Text != "3" && lblstaidx.Text != "6" && lblstaidx.Text != "8")
                        {
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        }
                        else if (lblstaidx.Text == "6")
                        {
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#737373");
                        }
                        else
                        {
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                        }
                        break;
                }

            }
        }
        else if (sGvName == "Gvexpenses_Print")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (GvName.EditIndex != e.Row.RowIndex)
                {
                    var litamount = (Literal)e.Row.FindControl("litamount");
                    var litamount_comma = (Literal)e.Row.FindControl("litamount_comma");

                    litamount_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(litamount.Text));
                }

            }
        }
        else if (sGvName == "gvFile")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                // Display the company name in italics.
                string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
            }
        }

    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                // showmodal_traning(txtsearch_modal_training.Text);
                break;
            case "gvModalTraning":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                //showmodal_traning_app(txtsearch_modal_training_app.Text);
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData();
                SETFOCUS.Focus();
                break;
            case "GvHR_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            case "GvHR_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "GvMD_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "GvMD_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "GvListDataLeader":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData_WApp_Leader(Hddfld_leader_status.Value);
                SETFOCUS.Focus();
                break;
            case "GvApprovelist":
                GvName.PageIndex = e.NewPageIndex;
                select_approve_list(GvName, int.Parse(ViewState["joblevel_idx"].ToString()), int.Parse(ViewState["rsec_idx"].ToString()), emp_idx, int.Parse(ViewState["rpos_idx"].ToString()));
                SETFOCUS.Focus();
                break;

        }
    }

    protected string getStatus(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatustest(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    private void zbtnHR_Wait()
    {
        zSetMode(3);
        ShowListHR_W();
    }

    private void zbtnMD_Wait()
    {
        zSetMode(6);
        ShowListMD_W();
    }
    public void add_institution()
    {
        _ddlm0_institution_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_institution_idx_ref");
        if (_func_dmu.zStringToInt(_ddlm0_institution_idx_ref.SelectedValue) <= 0)
        {
            showAlert("กรุณาเลือกวิทยากร");
        }
        else
        {
            add_institution_data();
        }
    }
    private void add_institution_data()
    {
        int i = 0, idata = 0;
        _ddlm0_institution_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_institution_idx_ref");
        _rdllecturer_type = (RadioButtonList)fvCRUD.FindControl("rdllecturer_type");
        _Gvinstitution = (GridView)fvCRUD.FindControl("Gvinstitution");

        DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
        foreach (DataRow item in ds.Tables["dsel_u1_training_course_lecturer"].Rows)
        {
            if (
                (_func_dmu.zStringToInt(item["lecturer_type"].ToString()) == _func_dmu.zStringToInt(_rdllecturer_type.SelectedValue))
                &&
                (_func_dmu.zStringToInt(item["m0_institution_idx_ref"].ToString()) == _func_dmu.zStringToInt(_ddlm0_institution_idx_ref.SelectedValue))
                )
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("วิทยากรนี้มีอยู่แล้วกรุณากรอกใหม่");
            _ddlm0_institution_idx_ref.Focus();
        }
        else
        {
            DataRow dr;

            dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
            dr["lecturer_type"] = _rdllecturer_type.SelectedValue;
            dr["m0_institution_idx_ref"] = _ddlm0_institution_idx_ref.SelectedValue;
            dr["zName"] = _ddlm0_institution_idx_ref.Items[_ddlm0_institution_idx_ref.SelectedIndex].Text;
            ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
            ViewState["vsel_u1_training_course_lecturer"] = ds;
            _func_dmu.zSetGridData(_Gvinstitution, ds.Tables["dsel_u1_training_course_lecturer"]);

        }
    }
    public void add_objective()
    {
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        if (_func_dmu.zStringToInt(_ddlm0_objective_idx_ref.SelectedValue) <= 0)
        {
            showAlert("กรุณาเลือกวัตถุประสงค์");
        }
        else
        {
            add_objective_data();
        }
    }
    private void add_objective_data()
    {
        int i = 0, irow = 0;
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        _Gvobjective = (GridView)fvCRUD.FindControl("Gvobjective");

        DataSet ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
        foreach (DataRow item in ds.Tables["dsel_u2_training_course_objective"].Rows)
        {
            if (
                (_func_dmu.zStringToInt(item["m0_objective_idx_ref"].ToString()) == _func_dmu.zStringToInt(_ddlm0_objective_idx_ref.SelectedValue))
                )
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("วัตถุประสงค์นี้มีอยู่แล้วกรุณากรอกใหม่");
            _ddlm0_objective_idx_ref.Focus();
        }
        else
        {

            add_objective_dataToDataset(0);

        }
    }
    public void add_objective_dataToDataset(int iSatus)
    {
        int i = 0, irow = 0;
        _ddlm0_objective_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_objective_idx_ref");
        _Gvobjective = (GridView)fvCRUD.FindControl("Gvobjective");
        _txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");

        DataSet ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
        irow = 0;
        CreateDs_el_u2_training_course_objective();
        ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
        DataRow dr;
        foreach (var item in _Gvobjective.Rows)
        {
            Label lbm0_objective_idx_ref = (Label)_Gvobjective.Rows[irow].FindControl("lbm0_objective_idx_ref");
            Label txtobjective_name_item_L = (Label)_Gvobjective.Rows[irow].FindControl("txtobjective_name_item_L");
            _cbpass_test_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbpass_test_flag");
            _txtpass_test_per = (TextBox)_Gvobjective.Rows[irow].FindControl("txtpass_test_per");
            _cbhour_training_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbhour_training_flag");
            _txthour_training_per = (TextBox)_Gvobjective.Rows[irow].FindControl("txthour_training_per");
            _cbwrite_report_training_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbwrite_report_training_flag");
            _cbpublish_training_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbpublish_training_flag");
            _txtpublish_training_description = (TextBox)_Gvobjective.Rows[irow].FindControl("txtpublish_training_description");
            _cbcourse_lecturer_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbcourse_lecturer_flag");
            _cbother_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbother_flag");
            _txtother_description = (TextBox)_Gvobjective.Rows[irow].FindControl("txtother_description");
            _cbhrd_nofollow_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbhrd_nofollow_flag");
            _cbhrd_follow_flag = (CheckBox)_Gvobjective.Rows[irow].FindControl("cbhrd_follow_flag");
            _txthrd_follow_day = (TextBox)_Gvobjective.Rows[irow].FindControl("txthrd_follow_day");

            dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
            dr["m0_objective_idx_ref"] = lbm0_objective_idx_ref.Text;
            dr["zName"] = txtobjective_name_item_L.Text;
            dr["pass_test_flag"] = _func_dmu.zBooleanToInt(_cbpass_test_flag.Checked).ToString();
            dr["pass_test_per"] = _txtpass_test_per.Text;
            dr["hour_training_flag"] = _func_dmu.zBooleanToInt(_cbhour_training_flag.Checked).ToString();
            dr["hour_training_per"] = _txthour_training_per.Text;
            dr["write_report_training_flag"] = _func_dmu.zBooleanToInt(_cbwrite_report_training_flag.Checked).ToString();
            dr["publish_training_flag"] = _func_dmu.zBooleanToInt(_cbpublish_training_flag.Checked).ToString();
            dr["publish_training_description"] = _txtpublish_training_description.Text;
            dr["course_lecturer_flag"] = _func_dmu.zBooleanToInt(_cbcourse_lecturer_flag.Checked).ToString();
            dr["other_flag"] = _func_dmu.zBooleanToInt(_cbother_flag.Checked).ToString();
            dr["other_description"] = _txtother_description.Text;
            dr["hrd_nofollow_flag"] = _func_dmu.zBooleanToInt(_cbhrd_nofollow_flag.Checked).ToString();
            dr["hrd_follow_flag"] = _func_dmu.zBooleanToInt(_cbhrd_follow_flag.Checked).ToString();
            dr["hrd_follow_day"] = _txthrd_follow_day.Text;
            ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);

            irow++;
        }
        if (iSatus == 0)
        {

            dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
            dr["m0_objective_idx_ref"] = _ddlm0_objective_idx_ref.SelectedValue;
            dr["zName"] = _ddlm0_objective_idx_ref.Items[_ddlm0_objective_idx_ref.SelectedIndex].Text;

            dr["pass_test_flag"] = "0";
            dr["pass_test_per"] = "";
            dr["hour_training_flag"] = "0";
            dr["hour_training_per"] = "";
            dr["write_report_training_flag"] = "0";
            dr["publish_training_flag"] = "0";
            dr["publish_training_description"] = "";
            dr["course_lecturer_flag"] = "0";
            dr["other_flag"] = "0";
            dr["other_description"] = "";
            dr["hrd_nofollow_flag"] = "0";
            dr["hrd_follow_flag"] = "0";
            dr["hrd_follow_day"] = "";
            if (_func_dmu.zStringToDecimal(_txtscore_through_per.Text) > 0)
            {
                dr["pass_test_per"] = _txtscore_through_per.Text;
                dr["pass_test_flag"] = "1";
            }

            ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
        }

        ViewState["vsel_u2_training_course_objective"] = ds;
        if (iSatus == 0)
        {
            _func_dmu.zSetGridData(_Gvobjective, ds.Tables["dsel_u2_training_course_objective"]);
        }
    }
    public void add_employee()
    {
        _ddlemp_idx_ref = (DropDownList)fvCRUD.FindControl("ddlemp_idx_ref");
        if (_func_dmu.zStringToInt(_ddlemp_idx_ref.SelectedValue) <= 0)
        {
            showAlert("กรุณาเลือกรหัสพนักงานที่เข้าร่วมอบรม");
        }
        else
        {
            add_employee_data();
            setCourseSel();
            zCalculateCosts();
        }
    }
    private void add_employee_data(int item_imp = 0, int item_data = 0)
    {
        int i = 0;
        if (item_imp == 0)
        {
            _ddlemp_idx_ref = (DropDownList)fvCRUD.FindControl("ddlemp_idx_ref");
            item_data = _func_dmu.zStringToInt(_ddlemp_idx_ref.SelectedValue);
        }
        else
        {

        }
        _Gvemp = (GridView)fvCRUD.FindControl("Gvemp");

        DataSet ds = (DataSet)ViewState["vsel_u3_training_course_employee"];
        foreach (DataRow item in ds.Tables["dsel_u3_training_course_employee"].Rows)
        {/*_func_dmu.zStringToInt(_ddlemp_idx_ref.SelectedValue)*/
            if (
                (_func_dmu.zStringToInt(item["emp_idx_ref"].ToString()) == item_data)
                )
            {
                i++;
            }
        }
        if (i > 0)
        {
            if (item_imp == 0)
            {
                showAlert("รหัสพนักงานที่เข้าร่วมอบรมนี้มีอยู่แล้วกรุณากรอกใหม่");
                _ddlemp_idx_ref.Focus();
            }
        }
        else
        {
            DataRow dr;

            data_elearning dataelearning = new data_elearning();
            dataelearning.employeeM_action = new employeeM[1];
            employeeM obj = new employeeM();
            obj.EmpIDX = item_data;// _func_dmu.zStringToInt(_ddlemp_idx_ref.SelectedValue);
            if ((Hddfld_in_out_plan.Value == "OP") && (ViewState["rpos_idx"].ToString() != "5900"))
            {
                obj.zstatus = "OP";
                obj.RDeptID = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
            }
            dataelearning.employeeM_action[0] = obj;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
            dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_employee, dataelearning);
            if (dataelearning.employeeM_action != null)
            {
                foreach (var item in dataelearning.employeeM_action)
                {
                    dr = ds.Tables["dsel_u3_training_course_employee"].NewRow();
                    dr["emp_idx_ref"] = item.EmpIDX.ToString();
                    dr["empcode"] = item.EmpCode;
                    dr["zName"] = item.FullNameTH;
                    dr["RDeptID"] = item.RDeptID.ToString();
                    dr["RDeptName"] = item.RDeptName;
                    dr["zPostName"] = item.PosNameTH;
                    dr["zCostId"] = item.CostIDX.ToString();
                    dr["zCostCenter"] = item.CostNo;
                    dr["zTel"] = item.MobileNo;
                    dr["register_status"] = "0";
                    dr["register_date"] = "";
                    dr["zregister_date"] = "";
                    dr["register_user"] = "0";
                    dr["register_remark"] = "";
                    dr["u3_training_course_idx"] = "0";


                    dr["signup_status"] = "0";
                    dr["signup_user"] = "";
                    dr["signup_date"] = "";
                    dr["zsignup_date"] = "";
                    dr["signup_remark"] = "";

                    dr["test_scores"] = "0";
                    dr["test_scores_status"] = "0";
                    dr["test_scores_user"] = "";
                    dr["test_scores_date"] = "";
                    dr["ztest_scores_date"] = "";
                    dr["test_scores_remark"] = "";

                    dr["zstatus_name"] = "อยู่ระหว่างดำเนิดการ";
                    dr["grade_status"] = "0";


                    ds.Tables["dsel_u3_training_course_employee"].Rows.Add(dr);

                }
            }


            ViewState["vsel_u3_training_course_employee"] = ds;
            _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);

        }
    }

    public void add_expenses()
    {
        _txtexpenses_description = (TextBox)fvCRUD.FindControl("txtexpenses_description");
        DropDownList _ddlexpenses_description = (DropDownList)fvCRUD.FindControl("ddlexpenses_description");
        if (_ddlexpenses_description.SelectedValue == "0")
        {
            showAlert("กรุณากรอกรายการค่าใช้จ่ายฝึกอบรม");
        }
        else
        {
            add_expenses_data();
            zCalculateCosts();
        }
    }
    private void add_expenses_data()
    {
        int i = 0;
        _txtexpenses_description = (TextBox)fvCRUD.FindControl("txtexpenses_description");
        DropDownList _ddlexpenses_description = (DropDownList)fvCRUD.FindControl("ddlexpenses_description");
        _txtamount = (TextBox)fvCRUD.FindControl("txtamount");
        _txtvat = (TextBox)fvCRUD.FindControl("txtvat");
        _txtwithholding_tax = (TextBox)fvCRUD.FindControl("txtwithholding_tax");
        _Gvtrn_expenses = (GridView)fvCRUD.FindControl("Gvtrn_expenses");
        TextBox txtexpense_other = (TextBox)fvCRUD.FindControl("txtexpense_other");
        Control divother_expense = (Control)fvCRUD.FindControl("divother_expense");

        DataSet ds = (DataSet)ViewState["vsel_u4_training_course_expenses"];
        foreach (DataRow item in ds.Tables["dsel_u4_training_course_expenses"].Rows)
        {
            if (
                (item["expenses_description"].ToString().Trim() == _ddlexpenses_description.SelectedItem.Text)
                )
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("รายการค่าใช้จ่ายฝึกอบรมนี้มีอยู่แล้วกรุณากรอกใหม่");
            _txtexpenses_description.Focus();
        }
        else
        {
            DataRow dr;

            dr = ds.Tables["dsel_u4_training_course_expenses"].NewRow();

            if (_ddlexpenses_description.SelectedValue != "5")
            {
                dr["expenses_description"] = _ddlexpenses_description.SelectedItem.Text; //_txtexpenses_description.Text;
            }
            else
            {
                dr["expenses_description"] = txtexpense_other.Text;
            }

            dr["amount"] = _txtamount.Text;//_func_dmu.zStringToInt(_txtamount.Text).ToString();
            dr["vat"] = _func_dmu.zStringToInt(_txtvat.Text).ToString();
            dr["withholding_tax"] = _func_dmu.zStringToInt(_txtwithholding_tax.Text).ToString();
            ds.Tables["dsel_u4_training_course_expenses"].Rows.Add(dr);
            ViewState["vsel_u4_training_course_expenses"] = ds;
            _func_dmu.zSetGridData(_Gvtrn_expenses, ds.Tables["dsel_u4_training_course_expenses"]);

            _txtexpenses_description.Text = "";
            _txtamount.Text = "";
            _txtvat.Text = "";
            _txtwithholding_tax.Text = "";
            txtexpense_other.Text = "";
            _ddlexpenses_description.SelectedValue = "0";
            divother_expense.Visible = false;
            _txtamount.Enabled = false;
            _txtvat.Enabled = false;

        }
    }
    private void setCalculateExpenses()
    {

        _txttraining_course_total = (TextBox)fvCRUD.FindControl("txttraining_course_total");
        _txttraining_course_total_avg = (TextBox)fvCRUD.FindControl("txttraining_course_total_avg");
        _txttraining_course_reduce_tax = (TextBox)fvCRUD.FindControl("txttraining_course_reduce_tax");
        _txttraining_course_net_charge = (TextBox)fvCRUD.FindControl("txttraining_course_net_charge");
        _txttraining_course_net_charge_tax = (TextBox)fvCRUD.FindControl("txttraining_course_net_charge_tax");

        decimal total = 0,
                 total_avg = 0,
                 reduce_tax = 0,
                 net_charge = 0,
                 net_charge_tax = 0;

        DataSet ds = (DataSet)ViewState["vsel_u4_training_course_expenses"];
        foreach (DataRow item in ds.Tables["dsel_u4_training_course_expenses"].Rows)
        {

        }
    }

    private void setinstitutionDefault()
    {
        _Gvinstitution = (GridView)fvCRUD.FindControl("Gvinstitution");
        CreateDs_el_u1_training_course_lecturer();
        _func_dmu.zSetGridData(_Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);

    }
    private void setObjectiveDefault()
    {
        _Gvobjective = (GridView)fvCRUD.FindControl("Gvobjective");
        CreateDs_el_u2_training_course_objective();
        _func_dmu.zSetGridData(_Gvobjective, ViewState["vsel_u2_training_course_objective"]);

    }
    private void setEmployeeDefault()
    {
        _Gvemp = (GridView)fvCRUD.FindControl("Gvemp");
        CreateDs_el_u3_training_course_employee();
        _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);

    }
    private void setExpensesDefault()
    {
        _Gvtrn_expenses = (GridView)fvCRUD.FindControl("Gvtrn_expenses");
        CreateDs_el_u4_training_course_expenses();
        _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u4_training_course_expenses"]);

    }
    public void showdata()
    {
        zSetMode(0);

        //btnSaveInsert.Visible = true;
        zDeleteFileBin();
        zDeleteFileBinEmployee();
        SETFOCUS.Focus();

    }
    /*
    protected void setStatusData(string sValue)
    {
        if (sValue == "btnLeaderList")
        {
            Hddfld_permission_approve.Value = "LEADER";
        }
        else if (sValue == "btnLeaderApproveList")
        {
            Hddfld_permission_approve.Value = "LEADER";
        }
        else if (sValue == "btnDetailWaitApprove")
        {
            Hddfld_permission_approve.Value = "LEADER";
        }
        else if (sValue == "btnupdateApprove_HR")
        {
            Hddfld_permission_approve.Value = "LEADER";
        }
        else if (sValue == "btnFilter") 
        {

        }
        else
        {
            Hddfld_permission_approve.Value = "";
        }

    }
    */

    #region bind Actor

    protected void setFormDataActor(int m0_actor, int m0_node, int m0_staidx, int history_detail, int emp_create, int u0_training_course_idx)
    {


        //litDebug.Text = m0_actor + "," + m0_node + "," + m0_staidx + "," + history_detail;

        switch (m0_actor)
        {

            case 1: // User

                if (m0_node == 1 && emp_create == emp_idx)
                {
                    zSetMode(1);
                    zShowdataUpdate(u0_training_course_idx, "E");
                    div_approve.Visible = true;
                    btnCancel.Visible = false;
                    btnSaveInsert.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                btnprint_course.Visible = false;
                break;

            case 2: // Head User
                if (history_detail == 2 && m0_node == 2)
                {
                    div_approve.Visible = true;
                    btnCancel.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                btnprint_course.Visible = false;

                break;

            case 3: // HRD Admin
                if (history_detail == 2 && (m0_node == 3 || m0_node == 7))
                {
                    zSetMode(1);
                    zShowdataUpdate(u0_training_course_idx, "E");
                    div_approve.Visible = true;
                    btnCancel.Visible = false;
                    btnSaveInsert.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                btnprint_course.Visible = false;

                break;
            case 4: // HRD Manager
                if (history_detail == 2 && m0_node == 4)
                {
                    div_approve.Visible = true;
                    btnCancel.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                btnprint_course.Visible = false;

                break;

            case 5: // HR Director
                if (history_detail == 2 && m0_node == 5)
                {
                    div_approve.Visible = true;
                    btnCancel.Visible = false;
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                if (m0_node == 9 && m0_staidx == 2)
                {
                    btnprint_course.Visible = true;

                }
                else
                {
                    btnprint_course.Visible = false;
                }

                break;

            case 6: // MD
                if (history_detail == 2 && m0_node == 6)
                {
                    if (ViewState["rpos_idx"].ToString() == "780") // K'Boy
                    {
                        div_approve.Visible = true;
                        btnCancel.Visible = false;
                    }
                    else // Secretary k'boy
                    {
                        div_approve.Visible = false;
                        btnCancel.Visible = true;
                    }
                }
                else
                {
                    div_approve.Visible = false;

                    if (history_detail == 1)
                    {
                        btnCancel.Visible = true;
                    }
                    else
                    {
                        btnCancel.Visible = false;
                    }
                }
                if (m0_node == 9 && m0_staidx == 2)
                {
                    btnprint_course.Visible = true;

                }
                else
                {
                    btnprint_course.Visible = false;
                }
                break;
        }

    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _m0_training_idx, _costcenter_idx, _DeptID;
        int _cemp_idx, _md_app = 1;
        string[] _calc = new string[4];

        m0_training objM0_ProductType = new m0_training();
        _ddlcostcenter_idx_ref = (DropDownList)fvCRUD.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)fvCRUD.FindControl("ddlRDeptID_ref");

        switch (cmdName)
        {
            case "btnListData":

                zSetMode(2);
                SETFOCUS.Focus();
                break;

            case "btnInsert":
                MultiViewBody.SetActiveView(View_trainingPage);
                setActiveTab("PLAN");
                btnprint_course.Visible = false;
                ddl_type_plan.SelectedValue = "0";
                ddl_type_plan.Enabled = true;
                fvCRUD.Visible = false;


                break;

            case "btnRefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnApprove":
            case "btnBack_Approve":
                setActiveTab("Approve");
                MultiViewBody.SetActiveView(ViewApprove);
                select_approve_list(GvApprovelist, int.Parse(ViewState["joblevel_idx"].ToString()), int.Parse(ViewState["rsec_idx"].ToString()), emp_idx, int.Parse(ViewState["rpos_idx"].ToString()));
                SETFOCUS.Focus();

                break;

            //case "btncourse_out_plan":
            //    Hddfld_in_out_plan.Value = "OP";
            //    showdata();
            //    zCalculateCosts();
            //    break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnAdd_lecturer":
                add_institution();
                break;
            case "btnAdd_objective":
                add_objective();
                break;
            case "btnAdd_emp":
                Hddfld_costno.Value = _ddlcostcenter_idx_ref.SelectedValue;
                Hddfld_rdept.Value = _ddlRDeptID_ref.SelectedValue;
                add_employee();
                if (_func_dmu.zStringToInt(Hddfld_costno.Value) > 0)
                {
                    //_ddlcostcenter_idx_ref.SelectedValue = Hddfld_costno.Value;
                    //_ddlRDeptID_ref.SelectedValue = Hddfld_rdept.Value;
                }
                break;
            case "btnAdd_trn_expen":
                add_expenses();
                break;
            case "btnClearfile":
                zDeleteFileBin();
                zDeleteFileBinEmployee();
                break;
            case "btnDeletfileMAS":
                zDeleteFileMAS();
                break;
            case "btnDownloadFile":
                DownloadFile(cmdArg);
                break;
            case "btnimport_employee":
                ImportEmployee();
                break;

            case "btnSaveInsert":
                if (checkError() == false)
                {
                    if (zSave(_func_dmu.zStringToInt(Hddfld_u0_training_course_idx.Value), 1, 1, 1, int.Parse(ddl_type_plan.SelectedValue), txtremark_approve.Text) == true)
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;
            case "btnDelete":
                _m0_training_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
                zDelete(_m0_training_idx);
                ShowListData();
                zDeleteFileBin();
                zDeleteFileBinEmployee();
                break;
            case "btnFilter":
                // actionIndex();
                ShowListData();
                break;

            case "btnselGvListData_TrnNSur":
                _m0_training_idx = int.Parse(cmdArg);
                pnlListData.Visible = false;
                setActiveTab("I");
                Select_Page1showdata();
                break;

            case "btnImport":

                break;

            case "btnUpdate_GvListData":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "E");
                    if ((Hddfld_in_out_plan.Value == "OP") && (
                        (Hddfld_permission.Value == "USER") ||
                        (Hddfld_permission.Value == "LEADER") ||
                        (Hddfld_permission.Value == "HR")
                        )
                        )
                    {
                        setActiveTab("IOP");
                    }
                    else
                    {
                        setActiveTab("I");
                    }
                    getTilteCourse();
                    setplace();
                    btnSaveInsert.Visible = true;
                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(2);
                }
                zDeleteFileBin();
                zDeleteFileBinEmployee();
                SETFOCUS.Focus();
                break;
            case "btnSaveUpdate":
                if (checkError() == false)
                {
                    if (zSave(_func_dmu.zStringToInt(Hddfld_u0_training_course_idx.Value), 0, 0, 0, 0, "") == true)
                    {

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
                break;
            case "btnDetail":
                string[] arg1_detail = new string[10];
                arg1_detail = e.CommandArgument.ToString().Split(';');

                _m0_training_idx = int.Parse(arg1_detail[0]);
                ViewState["u0_training_course_idx"] = _m0_training_idx.ToString();
                ViewState["History_Detail"] = int.Parse(arg1_detail[1]);
                ViewState["m0_node"] = int.Parse(arg1_detail[2]);
                ViewState["m0_actor"] = int.Parse(arg1_detail[3]);
                ViewState["m0_status"] = int.Parse(arg1_detail[4]);
                ViewState["type_request"] = int.Parse(arg1_detail[5]);
                ViewState["CEmpIDX_u0"] = int.Parse(arg1_detail[6]);
                ViewState["costcenter_idx_ref"] = int.Parse(arg1_detail[7]);
                ViewState["training_course_budget_total"] = arg1_detail[8];
                ViewState["training_course_no"] = arg1_detail[9];

                linkFvTrigger(fvCRUD);

                if (_m0_training_idx > 0)
                {

                    zSetMode(1);
                    zShowdataUpdate(_m0_training_idx, "P");
                    if ((Hddfld_in_out_plan.Value == "OP") && (
                        (Hddfld_permission.Value == "USER") ||
                        (Hddfld_permission.Value == "LEADER") ||
                        (Hddfld_permission.Value == "HR")

                        )
                        )
                    {
                        setActiveTab("IOP");
                    }
                    else
                    {
                        setActiveTab("I");
                    }
                    getTilteCourse();
                    setplace();
                    btnSaveInsert.Visible = false;
                    //ddl_approve.SelectedValue = "0";
                    txtremark_approve.Text = String.Empty;

                    setFormDataActor(int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_status"].ToString()), int.Parse(ViewState["History_Detail"].ToString()), int.Parse(ViewState["CEmpIDX_u0"].ToString()), int.Parse(ViewState["u0_training_course_idx"].ToString()));
                    selectstatus(ddl_approve, int.Parse(ViewState["m0_node"].ToString()));

                    try
                    {

                        string getPathLotus = ConfigurationSettings.AppSettings["path_elearning_plan"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["training_course_no"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories(myDirLotus, ViewState["training_course_no"].ToString());
                    }
                    catch
                    {

                    }


                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(2);
                }

                FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);
                selectdetail_print(FvTemplate_print, _m0_training_idx);
                FvTemplate_print.DataBind();
                zDeleteFileBin();
                zDeleteFileBinEmployee();
                SETFOCUS.Focus();
                break;

            case "CmdUpdateApprove":

                if ((ViewState["m0_node"].ToString() == "1" && ViewState["m0_actor"].ToString() == "1") || ViewState["m0_actor"].ToString() == "3")
                {
                    zSave(int.Parse(ViewState["u0_training_course_idx"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue), int.Parse(ViewState["type_request"].ToString()), txtremark_approve.Text);
                }
                else
                {
                    Update_System(int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue), int.Parse(ViewState["u0_training_course_idx"].ToString()), txtremark_approve.Text, emp_idx, int.Parse(ViewState["type_request"].ToString()), int.Parse(ViewState["costcenter_idx_ref"].ToString()), Convert.ToDecimal(ViewState["training_course_budget_total"].ToString()));

                }
                setActiveTab("Approve");
                MultiViewBody.SetActiveView(ViewApprove);
                select_sum_approve(int.Parse(ViewState["joblevel_idx"].ToString()), int.Parse(ViewState["rsec_idx"].ToString()), emp_idx, int.Parse(ViewState["rpos_idx"].ToString()));
                select_approve_list(GvApprovelist, int.Parse(ViewState["joblevel_idx"].ToString()), int.Parse(ViewState["rsec_idx"].ToString()), emp_idx, int.Parse(ViewState["rpos_idx"].ToString()));
                SETFOCUS.Focus();
                break;

            //HR
            case "btnHR_Wait":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_W":
                ShowListHR_W();
                break;
            case "btnDetail_GvHR_WList":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("HR_W");
                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "HR-W", "HR");
                    setModeApprove("btnDetail_GvHR_WList", _md_app, 0);
                    zcrtl_btnback("HRW");
                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(3);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);

                }
                zSetMode(3);
                SETFOCUS.Focus();
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnHR_App":
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_A":
                ShowListHR_A();
                break;
            case "btnDetail_GvHR_AList":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSetMode(5);
                    setActiveTab("HR_A");
                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "HR-A", "HR");
                    setModeApprove("btnDetail_GvHR_AList", _md_app, 0);
                    zcrtl_btnback("HRA");
                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR_A":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);

                }
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            //MD
            case "btnMD_Wait":
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_W":
                ShowListMD_W();
                break;
            case "btnDetail_GvMD_WList":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("MD_W");
                    zShowdataHR_WDetail(_m0_training_idx, "MD-W", "MD");
                    setModeApprove("btnDetail_GvMD_WList", 1, 0);
                    zcrtl_btnback("MDW");


                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(6);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnMD_App":
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_A":
                ShowListMD_A();
                break;
            case "btnDetail_GvMD_AList":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSetMode(5);
                    setActiveTab("MD_A");
                    zShowdataHR_WDetail(_m0_training_idx, "MD-A", "MD");
                    setModeApprove("btnDetail_GvMD_AList", 1, 0);
                    zcrtl_btnback("MDA");
                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD_A":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnscheduler":
                zSetMode(8);
                SETFOCUS.Focus();
                break;
            case "btnAdd_GvMonthList":
                AddMonthList();
                break;
            case "btnrptscheduler":
                zSetMode(9);
                SETFOCUS.Focus();

                break;
            case "btnGvemp_register":
                _m0_training_idx = int.Parse(cmdArg);
                zShowdata_register(_m0_training_idx);
                // SETFOCUS.Focus();
                break;
            case "lbCmdUpdate":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSave_register(_m0_training_idx);

                }
                break;
            case "btnGvemp_signup":
                _m0_training_idx = int.Parse(cmdArg);
                zShowdata_signup(_m0_training_idx);

                break;
            case "lbCmdUpdate_signup":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSave_signup(_m0_training_idx);

                }
                break;
            case "btnGvemp_test_scores":
                _m0_training_idx = int.Parse(cmdArg);
                zShowdata_test_scores(_m0_training_idx);
                // SETFOCUS.Focus();
                break;
            case "lbCmdUpdate_test_scores":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSave_test_scores(_m0_training_idx);

                }
                break;
            case "btnadd_emp_resulte":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zShowdata_EmpResulte(_m0_training_idx);

                }
                break;
            case "btnCmdUpdate_emp_resulte":
                zSave_EmpResulte();

                break;
            case "btnsummary":
                zSetMode(10);
                ShowListsummary();
                SETFOCUS.Focus();
                break;
            case "btnDetail_GvsummaryList":
                //_m0_training_idx = int.Parse(cmdArg);
                string su0_idx = "", su6_idx = "";
                string[] calc = new string[4];
                if (cmdArg != "0")
                {
                    calc = cmdArg.Split('|');
                }
                else
                {
                    calc = new string[4] { "", "", "", "" };
                }
                su0_idx = calc[0];
                su6_idx = calc[1];
                if ((_func_dmu.zStringToInt(su0_idx) > 0) && (_func_dmu.zStringToInt(su6_idx) > 0))
                {
                    zSetMode(11);
                    zShowdata_SummaryDetail(_func_dmu.zStringToInt(su0_idx), _func_dmu.zStringToInt(su6_idx), "open_course");

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(10);
                }
                SETFOCUS.Focus();
                break;
            case "btnS_summary":
                ShowListsummary();
                break;
            case "btnSave_summary":
                zSave_summary();
                zSetMode(10);
                ShowListsummary();
                SETFOCUS.Focus();
                break;
            case "btnqrcode":
                //_m0_training_idx = int.Parse(cmdArg);
                if (cmdArg != "0")
                {
                    calc = cmdArg.Split('|');
                }
                else
                {
                    calc = new string[4] { "", "", "", "" };
                }
                genQrCode_Test(_func_dmu.zStringToInt(calc[0]), calc[1]);
                break;
            case "btnLeaderList":
                zSetMode(12);
                pnlStatusapprove_Leader.Visible = false;
                btnFilterLeader.Visible = true;
                btnFilterLeader_app.Visible = false;
                ShowListData_WApp_Leader("LEADER_W");
                Hddfld_leader_status.Value = "LEADER_W";
                SETFOCUS.Focus();
                break;
            case "btnFilterLeader":
                ShowListData_WApp_Leader("LEADER_W");
                Hddfld_leader_status.Value = "LEADER_W";
                SETFOCUS.Focus();
                break;
            case "btnDetailWaitApprove":
                _m0_training_idx = 0;// int.Parse(cmdArg);
                btnSaveInsert.Visible = false;

                string idx = "", isuper_app = "", iapp = "", ihr_status = "";
                string[] calc1 = new string[4];
                if (cmdArg != "0")
                {
                    calc = cmdArg.Split('|');
                }
                else
                {
                    calc = new string[4] { "", "", "", "" };
                }
                idx = calc[0];
                isuper_app = calc[1];
                iapp = calc[2];
                ihr_status = calc[3];
                _m0_training_idx = _func_dmu.zStringToInt(idx);
                if (_m0_training_idx > 0)
                {
                    zSetMode(5);
                    if ((_func_dmu.zStringToInt(isuper_app) == 0))
                    {
                        setActiveTab("liLeaderList");
                        _md_app = zShowdataHR_WDetail(_m0_training_idx, "LEADER-W", "LEADER");
                        setModeApprove("btnDetailWaitApprove", _md_app, _func_dmu.zStringToInt(ihr_status));
                        zcrtl_btnback("LEADERW");

                    }
                    else if ((_func_dmu.zStringToInt(isuper_app) == 4) ||
                             (_func_dmu.zStringToInt(isuper_app) == 6)
                        )
                    {
                        setActiveTab("liLeaderApproveList");
                        _md_app = zShowdataHR_WDetail(_m0_training_idx, "LEADER-A", "LEADER");
                        setModeApprove("liLeaderApproveList", _md_app, _func_dmu.zStringToInt(ihr_status));
                        zcrtl_btnback("LEADERA");

                    }
                    else
                    {
                        Hddfld_training_course_no.Value = "";
                    }

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(12);
                }
                else
                {

                }
                SETFOCUS.Focus();
                break;
            case "btnLEADER_Wait":
                zSetMode(12);
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_leader":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveLeader(_m0_training_idx);

                }
                zSetMode(12);
                ShowListData_WApp_Leader("LEADER_W");
                Hddfld_leader_status.Value = "LEADER_W";
                SETFOCUS.Focus();
                break;
            case "btnLeaderApproveList":
                zSetMode(13);
                pnlStatusapprove_Leader.Visible = true;
                btnFilterLeader.Visible = false;
                btnFilterLeader_app.Visible = true;
                ShowListData_WApp_Leader("LEADER_A");
                Hddfld_leader_status.Value = "LEADER_A";
                SETFOCUS.Focus();
                break;
            case "btnFilterLeader_app":
                ShowListData_WApp_Leader("LEADER_A");
                Hddfld_leader_status.Value = "LEADER_A";
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_leader_A":
                _m0_training_idx = int.Parse(cmdArg);
                btnSaveInsert.Visible = false;
                if (_m0_training_idx > 0)
                {
                    zSaveApproveLeader(_m0_training_idx);

                }
                zSetMode(13);
                ShowListData_WApp_Leader("LEADER_A");
                Hddfld_leader_status.Value = "LEADER_A";
                SETFOCUS.Focus();
                break;
            case "btnAdd_u8date":
                add_TrainingDate();
                break;


        }

    }
    #endregion btnCommand

    private void zcrtl_btnback(string _mode)
    {
        btncancelApp_HR_W.Visible = false;
        btncancelApp_HR.Visible = false;
        btncancelApp_MD_W.Visible = false;
        btncancelApp_MD.Visible = false;

        btncancelApp_LEADER_W.Visible = false;
        btncancelApp_LEADER.Visible = false;
        Boolean _Boolean = true;
        if (_mode == "HRW")
        {
            btncancelApp_HR_W.Visible = _Boolean;
        }
        else if (_mode == "HRA")
        {
            btncancelApp_HR.Visible = _Boolean;
        }
        else if (_mode == "MDW")
        {
            btncancelApp_MD_W.Visible = _Boolean;
        }
        else if (_mode == "MDA")
        {
            btncancelApp_MD.Visible = _Boolean;
        }
        else if (_mode == "LEADERW")
        {
            btncancelApp_LEADER_W.Visible = _Boolean;
        }
        else if (_mode == "LEADERA")
        {
            btncancelApp_LEADER.Visible = _Boolean;
        }

    }

    public void zshowdetailtraining_plan()
    {

    }

    #region FvDetail_DataBound


    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _txttraining_course_description = (TextBox)_FormView.FindControl("txttraining_course_description");
        _ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
        _rdotraining_course_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_type");
        _txttraining_course_planbudget_total = (TextBox)_FormView.FindControl("txttraining_course_planbudget_total");
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _Gvinstitution = (GridView)_FormView.FindControl("Gvinstitution");
        _ddlu0_training_plan_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_training_plan_idx_ref");
        TextBox txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        TextBox txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");

        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvCRUD":
                    if (
                        (FvName.CurrentMode == FormViewMode.Insert) ||
                        (FvName.CurrentMode == FormViewMode.Edit)
                        )
                    {
                        LinkButton btnAdd_lecturer = (LinkButton)fvCRUD.FindControl("btnAdd_lecturer");
                        linkBtnTrigger(btnAdd_lecturer);
                        LinkButton btnAdd_objective = (LinkButton)fvCRUD.FindControl("btnAdd_objective");
                        linkBtnTrigger(btnAdd_objective);
                        LinkButton btnAdd_emp = (LinkButton)fvCRUD.FindControl("btnAdd_emp");
                        linkBtnTrigger(btnAdd_emp);
                        LinkButton btnimport_employee = (LinkButton)fvCRUD.FindControl("btnimport_employee");
                        linkBtnTrigger(btnimport_employee);
                        LinkButton btnAdd_trn_expen = (LinkButton)fvCRUD.FindControl("btnAdd_trn_expen");
                        linkBtnTrigger(btnAdd_trn_expen);
                        LinkButton btnClearfile = (LinkButton)fvCRUD.FindControl("btnClearfile");
                        linkBtnTrigger(btnClearfile);
                        LinkButton btnDeletfileMAS = (LinkButton)fvCRUD.FindControl("btnDeletfileMAS");
                        linkBtnTrigger(btnDeletfileMAS);
                        LinkButton btninvestigatefile = (LinkButton)fvCRUD.FindControl("btninvestigatefile");
                        linkBtnTrigger(btninvestigatefile);

                        TextBox _txttraining_ = (TextBox)fvCRUD.FindControl("txttraining_course_planbudget_total");
                        DropDownList _ddl_training_name_add = (DropDownList)fvCRUD.FindControl("ddl_training_name_add");
                        DropDownList _ddlexpenses_description = (DropDownList)fvCRUD.FindControl("ddlexpenses_description");
                        DropDownList _ddl_training_location = (DropDownList)fvCRUD.FindControl("ddl_training_location");
                        DropDownList ddlcostcenter_idx_ref = (DropDownList)fvCRUD.FindControl("ddlcostcenter_idx_ref");
                        DropDownList ddlOrgID = (DropDownList)fvCRUD.FindControl("ddlOrgID");
                        DropDownList ddlRDeptID_ref = (DropDownList)fvCRUD.FindControl("ddlRDeptID_ref");
                        TextBox txttraining_course_planbudget_total = (TextBox)fvCRUD.FindControl("txttraining_course_planbudget_total");
                        TextBox txttraining_course_budget_balance = (TextBox)fvCRUD.FindControl("txttraining_course_budget_balance");
                        Panel _pnlplace = (Panel)fvCRUD.FindControl("pnlplace");
                        CheckBoxList chkchoice_file = (CheckBoxList)fvCRUD.FindControl("chkchoice_file");
                        TextBox txtorgidx_cost = (TextBox)fvCRUD.FindControl("txtorgidx_cost");
                        TextBox txtdept_cost = (TextBox)fvCRUD.FindControl("txtdept_cost");
                        TextBox txtcostcenter = (TextBox)fvCRUD.FindControl("txtcostcenter");
                        TextBox txtplace_idx_ref = (TextBox)fvCRUD.FindControl("txtplace_idx_ref");
                        RadioButtonList rdolecturer_type_place = (RadioButtonList)fvCRUD.FindControl("rdolecturer_type_place");
                        TextBox txtm0_target_group_idx_ref = (TextBox)fvCRUD.FindControl("txtm0_target_group_idx_ref");
                        DropDownList ddlm0_target_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_target_group_idx_ref");
                        TextBox txtshow_planbudget_total = (TextBox)fvCRUD.FindControl("txtshow_planbudget_total");
                        TextBox _txttraining_course_budget_balance = (TextBox)fvCRUD.FindControl("txttraining_course_budget_balance");
                        TextBox txttraining_course_budget_balance_show = (TextBox)fvCRUD.FindControl("txttraining_course_budget_balance_show");


                        getOrganizationList(ddlOrgID);


                        if (FvName.CurrentMode == FormViewMode.Insert)
                        {
                            //litDebug.Text = "1 Insert";
                            ddlOrgID.SelectedValue = ViewState["org_idx"].ToString();
                            getDepartmentList(ddlRDeptID_ref, int.Parse(ddlOrgID.SelectedValue));
                            ddlRDeptID_ref.SelectedValue = ViewState["rdept_idx"].ToString();
                            select_ddl_costcenter(ddlcostcenter_idx_ref, int.Parse(ddlRDeptID_ref.SelectedValue));
                            ddlcostcenter_idx_ref.SelectedValue = ViewState["cost_no"].ToString();
                            ViewState["ddl_location_type"] = "1";
                            select_ddl_training_location(_ddl_training_location);
                            txttraining_course_planbudget_total.Enabled = false;
                            select_text_costbalance(txttraining_course_planbudget_total, int.Parse(ddlcostcenter_idx_ref.SelectedValue));
                            _txttraining_course_budget_balance.Text = string.Format("{0:N2}", Convert.ToDecimal(txttraining_course_planbudget_total.Text));



                            if (ViewState["return_admin"].ToString() == "1")
                            {
                                ddlOrgID.Enabled = false;
                                ddlRDeptID_ref.Enabled = false;
                                ddlcostcenter_idx_ref.Enabled = false;
                            }
                            else
                            {
                                ddlOrgID.Enabled = true;
                                ddlRDeptID_ref.Enabled = true;
                                ddlcostcenter_idx_ref.Enabled = true;

                            }

                        }
                        else if (FvName.CurrentMode == FormViewMode.Edit)
                        {
                            if (int.Parse(txtplace_idx_ref.Text) <= 3)
                            {
                                rdolecturer_type_place.SelectedValue = "0";
                                ViewState["ddl_location_type"] = "1";
                                select_ddl_training_location(_ddl_training_location);
                            }
                            else
                            {
                                rdolecturer_type_place.SelectedValue = "1";
                                ViewState["ddl_location_type"] = "2";
                                select_ddl_training_location(_ddl_training_location);
                            }

                            //litDebug.Text = "2 Edit";
                            ddlOrgID.SelectedValue = txtorgidx_cost.Text;
                            getDepartmentList(ddlRDeptID_ref, int.Parse(ddlOrgID.SelectedValue));
                            ddlRDeptID_ref.SelectedValue = txtdept_cost.Text;
                            select_ddl_costcenter(ddlcostcenter_idx_ref, int.Parse(txtdept_cost.Text));
                            ddlcostcenter_idx_ref.SelectedValue = txtcostcenter.Text;
                            _ddl_training_location.SelectedValue = txtplace_idx_ref.Text;
                            ddlm0_target_group_idx_ref.SelectedValue = txtm0_target_group_idx_ref.Text;


                            _txttraining_course_budget_balance.Text = string.Format("{0:N2}", Convert.ToDecimal(txttraining_course_budget_balance_show.Text));


                            ////txttraining_course_planbudget_total.Enabled = false;
                            ////ddlOrgID.Enabled = false;
                            ////ddlRDeptID_ref.Enabled = false;
                            ////ddlcostcenter_idx_ref.Enabled = false;
                        }





                        txtshow_planbudget_total.Text = string.Format("{0:N2}", Convert.ToDecimal(txttraining_course_planbudget_total.Text));
                        select_ddl_expenses_description(_ddlexpenses_description);
                        _pnlplace.Visible = false;
                        select_chklist_file(chkchoice_file);
                       
                        
                    }
                    break;

                case "FvTemplate_print":
                    if (FvTemplate_print.CurrentMode == FormViewMode.ReadOnly)
                    {
                        var lbldate = (Label)FvTemplate_print.FindControl("lbldate");
                        var rdoplan = (RadioButtonList)FvTemplate_print.FindControl("rdoplan");
                        var lblinsitution = (Label)FvTemplate_print.FindControl("lblinsitution");
                        var chkfilelist = (CheckBoxList)FvTemplate_print.FindControl("chkfilelist");
                        var divfile_reason = (Control)FvTemplate_print.FindControl("divfile_reason");
                        var lblobjective = (Label)FvTemplate_print.FindControl("lblobjective");
                        var lblcountqty = (Label)FvTemplate_print.FindControl("lblcountqty");
                        var GvEmployee_Print = (GridView)FvTemplate_print.FindControl("GvEmployee_Print");
                        var litEmpCode = (Literal)FvTemplate_print.FindControl("litEmpCode");
                        var Gvexpenses_Print = (GridView)FvTemplate_print.FindControl("Gvexpenses_Print");
                        var litpass = (Literal)FvTemplate_print.FindControl("litpass");
                        var lithour = (Literal)FvTemplate_print.FindControl("lithour");
                        var div_inplan = (Control)FvTemplate_print.FindControl("div_inplan");
                        var div_outplan = (Control)FvTemplate_print.FindControl("div_outplan");
                        var rdoreport = (RadioButton)FvTemplate_print.FindControl("rdoreport");
                        var rdoshare = (RadioButton)FvTemplate_print.FindControl("rdoshare");
                        var litpublish_des = (Literal)FvTemplate_print.FindControl("litpublish_des");
                        var rdocourse = (RadioButton)FvTemplate_print.FindControl("rdocourse");
                        var rdoother = (RadioButton)FvTemplate_print.FindControl("rdoother");
                        var litother = (Literal)FvTemplate_print.FindControl("litother");
                        var rdopass = (RadioButton)FvTemplate_print.FindControl("rdopass");
                        var rdohour = (RadioButton)FvTemplate_print.FindControl("rdohour");
                        var rdohrnofollow = (RadioButton)FvTemplate_print.FindControl("rdohrnofollow");
                        var rdohrfollow = (RadioButton)FvTemplate_print.FindControl("rdohrfollow");
                        var lithrdate = (Literal)FvTemplate_print.FindControl("lithrdate");
                        var nameapprove_hrdmana = (Control)FvTemplate_print.FindControl("nameapprove_hrdmana");
                        var nameapprove2to3_hrdmanager = (Control)FvTemplate_print.FindControl("nameapprove2to3_hrdmanager");
                        var nameapprove_md = (Control)FvTemplate_print.FindControl("nameapprove_md");
                        var nameapprove3to4_md = (Control)FvTemplate_print.FindControl("nameapprove3to4_md");
                        var rdoempfile = (RadioButton)FvTemplate_print.FindControl("rdoempfile");
                        var rdoempform = (RadioButton)FvTemplate_print.FindControl("rdoempform");
                        var div_page2 = (Control)FvTemplate_print.FindControl("div_page2");
                        var littraining_course_total = (Literal)FvTemplate_print.FindControl("littraining_course_total");
                        var littraining_course_total_comma = (Literal)FvTemplate_print.FindControl("littraining_course_total_comma");
                        var littraining_course_total_avg = (Literal)FvTemplate_print.FindControl("littraining_course_total_avg");
                        var littraining_course_total_avg_comma = (Literal)FvTemplate_print.FindControl("littraining_course_total_avg_comma");
                        var littraining_course_budget_total = (Literal)FvTemplate_print.FindControl("littraining_course_budget_total");
                        var littraining_course_budget_total_comma = (Literal)FvTemplate_print.FindControl("littraining_course_budget_total_comma");
                        var littraining_course_budget_balance = (Literal)FvTemplate_print.FindControl("littraining_course_budget_balance");
                        var littraining_course_budget_balance_comma = (Literal)FvTemplate_print.FindControl("littraining_course_budget_balance_comma");
                        var littraining_course_planbudget_total = (Literal)FvTemplate_print.FindControl("littraining_course_planbudget_total");
                        var training_course_planbudget_total_comma = (Literal)FvTemplate_print.FindControl("training_course_planbudget_total_comma");

                        lbldate.Text = _datenow.ToString("dd/MM/yyyy");
                        rdoplan.SelectedValue = ViewState["m0_course_plan_status"].ToString();

                        selectinstitution_print(lblinsitution, int.Parse(ViewState["u0_training_course_idx"].ToString()));

                        select_chklist_file(chkfilelist);

                        string[] ToId = ViewState["m0_fileidx_ref_print"].ToString().Split(',');

                        foreach (ListItem li in chkfilelist.Items)
                        {
                            foreach (string To_check in ToId)
                            {

                                if (li.Value == To_check)
                                {
                                    li.Selected = true;

                                    if (To_check == "5")
                                    {
                                        divfile_reason.Visible = true;

                                    }
                                    else
                                    {
                                        divfile_reason.Visible = false;
                                    }

                                    break;
                                }
                                else
                                {
                                    li.Selected = false;
                                }
                            }
                        }
                        selectobjective_print(lblobjective, litpass, lithour, int.Parse(ViewState["u0_training_course_idx"].ToString()));
                        selectemployee_print(litEmpCode, rdoempfile, rdoempform, div_page2, GvEmployee_Print, lblcountqty, int.Parse(ViewState["u0_training_course_idx"].ToString()));
                        selectexpenses_print(Gvexpenses_Print, int.Parse(ViewState["u0_training_course_idx"].ToString()));
                        littraining_course_total_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(littraining_course_total.Text));
                        littraining_course_total_avg_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(littraining_course_total_avg.Text));
                        littraining_course_budget_total_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(littraining_course_budget_total.Text));
                        littraining_course_budget_balance_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(littraining_course_budget_balance.Text));
                        training_course_planbudget_total_comma.Text = string.Format("{0:N2}", Convert.ToDecimal(littraining_course_planbudget_total.Text));

                        if (ViewState["type_request"].ToString() == "1")
                        {
                            div_inplan.Visible = true;
                            div_outplan.Visible = false;

                            if (ViewState["check_passtest"].ToString() == "1")
                            {
                                rdopass.Checked = true;
                            }
                            else
                            {
                                rdopass.Checked = false;
                                litpass.Text = "...................";
                            }

                            if (ViewState["check_hour"].ToString() == "1")
                            {
                                rdohour.Checked = true;
                            }
                            else
                            {
                                rdohour.Checked = false;
                                lithour.Text = "...................";
                            }

                            nameapprove_hrdmana.Visible = true;
                            nameapprove2to3_hrdmanager.Visible = false;
                            nameapprove_md.Visible = true;
                            nameapprove3to4_md.Visible = false;

                        }
                        else if (ViewState["type_request"].ToString() == "2")
                        {
                            div_inplan.Visible = false;
                            div_outplan.Visible = true;

                            if (ViewState["check_writereport"].ToString() == "1")
                            {
                                rdoreport.Checked = true;
                            }
                            else
                            {
                                rdoreport.Checked = false;
                            }

                            if (ViewState["check_publish"].ToString() == "1")
                            {
                                rdoshare.Checked = true;
                                litpublish_des.Text = ViewState["check_publish_des"].ToString();
                            }
                            else
                            {
                                rdoshare.Checked = false;
                                litpublish_des.Text = ".........................................................................................................................";
                            }

                            if (ViewState["check_course_lec"].ToString() == "1")
                            {
                                rdocourse.Checked = true;
                            }
                            else
                            {
                                rdocourse.Checked = false;
                            }

                            if (ViewState["check_otherflag"].ToString() == "1")
                            {
                                rdoother.Checked = true;
                                litother.Text = ViewState["check_other_des"].ToString();
                            }
                            else
                            {
                                rdoother.Checked = false;
                                litother.Text = ".........................................................................................................................";
                            }

                            nameapprove_hrdmana.Visible = false;
                            nameapprove2to3_hrdmanager.Visible = true;
                            nameapprove_md.Visible = false;
                            nameapprove3to4_md.Visible = true;
                        }

                        if (ViewState["check_hrd_nofollow_flag"].ToString() == "1")
                        {
                            rdohrnofollow.Checked = true;
                        }
                        else
                        {
                            rdohrnofollow.Checked = false;
                        }

                        if (ViewState["check_hrd_follow_flag"].ToString() == "1")
                        {
                            rdohrfollow.Checked = true;
                            lithrdate.Text = ViewState["check_hrd_follow_day"].ToString();
                        }
                        else
                        {
                            rdohrfollow.Checked = false;
                            lithrdate.Text = "..................";

                        }


                    }
                    break;
            }
        }
        else if (sender is DropDownList)
        {
            var FvName = (DropDownList)sender;
            /*  if (FvName.ID == "ddlu0_training_plan_idx_ref")
             {

                 txttraining_group_name.Text = "";
                 txttraining_branch_name.Text = "";
                 _txttraining_course_description.Text = "";
                 if (_func_dmu.zStringToInt(FvName.SelectedValue) > 0)
                 {
                     _data_elearning = _func_dmu.zShowDataDsLookup("TRN-PLAN-U0-LOOKUP",
                                             "idx",
                                             _func_dmu.zStringToInt(FvName.SelectedValue)
                                             );
                     if (_data_elearning.trainingLoolup_action != null)
                     {

                         obj_trainingLoolup = _data_elearning.trainingLoolup_action[0];
                         txttraining_group_name.Text = obj_trainingLoolup.training_group_name;
                         txttraining_branch_name.Text = obj_trainingLoolup.training_branch_name;
                         _txttraining_course_description.Text = obj_trainingLoolup.zRemark;
                         _txttraining_course_planbudget_total.Text = obj_trainingLoolup.amount.ToString();
                         _rdllecturer_type.SelectedValue = obj_trainingLoolup.lecturer_type.ToString();
                         _txtcourse_score.Text = obj_trainingLoolup.course_score.ToString();
                         _txtscore_through_per.Text = obj_trainingLoolup.score_through_per.ToString();

                         setlecturertype(_func_dmu.zStringToInt(_rdllecturer_type.SelectedValue), 0);
                         setinstitutionDefault();

                         CreateDs_el_u1_training_course_lecturer();
                         int i = 0;
                         DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
                         DataRow dr;
                         dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                         dr["lecturer_type"] = _rdllecturer_type.SelectedValue;
                         dr["m0_institution_idx_ref"] = obj_trainingLoolup.zId_G.ToString();
                         dr["zName"] = obj_trainingLoolup.zName_G;
                         ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
                         ViewState["vsel_u1_training_course_lecturer"] = ds;
                         _func_dmu.zSetGridData(_Gvinstitution, ds.Tables["dsel_u1_training_course_lecturer"]);

                         _data_elearning = _func_dmu.zShowDataDsLookup("EMP-COURSE",
                                            "idx",
                                            _func_dmu.zStringToInt(FvName.SelectedValue)
                                            );
                         if (_data_elearning.trainingLoolup_action != null)
                         {
                             _Gvemp = (GridView)fvCRUD.FindControl("Gvemp");
                             CreateDs_el_u3_training_course_employee();
                             setEmployeeDefault();
                             // ViewState["View_emp_course"] = _data_elearning.trainingLoolup_action;
                             // DataSet Ds = (DataSet)ViewState["View_emp_course"];
                             foreach (var dtrow in _data_elearning.trainingLoolup_action)
                             {
                                 int iEmp_Idx = dtrow.zId; //_func_dmu.zStringToInt(dtrow["zId"].ToString());
                                 if (iEmp_Idx > 0)
                                 {
                                     add_employee_data(1, iEmp_Idx);

                                 }
                             }
                             _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);
                             setCourseSel();
                             zCalculateCosts();
                         }

                     }
                 }
             }
             else if (FvName.ID == "ddlcostcenter_idx_ref")
              {

                  string scount = "0";
                  if (_rdotraining_course_type.SelectedValue == "1")
                  {
                      _ddlRDeptID_ref.SelectedIndex = _ddlcostcenter_idx_ref.SelectedIndex;
                  }
                  else
                  {
                      DataSet Ds = (DataSet)ViewState["vsel_coursebudget_dept"];
                      foreach (DataRow dtrow in Ds.Tables["dsel_coursebudget_dept"].Rows)
                      {
                          if (_ddlcostcenter_idx_ref.SelectedValue == dtrow["id"].ToString().Trim())
                          {
                              scount = dtrow["RDeptID"].ToString().Trim();

                          }
                      }
                      _ddlRDeptID_ref.SelectedValue = scount;
                  }


              }*/
            if (FvName.ID == "ddlcourse_plan_status")
            {
                _func_dmu.zDropDownListWhereID(_ddlu0_training_plan_idx_ref,
                                "",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-U0-LOOKUP",
                                "zId_G",
                                getcourse_plan_status(FvName.SelectedValue)
                                );
                txttraining_group_name.Text = "";
                txttraining_branch_name.Text = "";
                _txttraining_course_description.Text = "";

            }
            else if ((FvName.ID == "ddlm0_training_group_idx_ref") && (Hddfld_in_out_plan.Value == "OP"))
            {
                DropDownList ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
                DropDownList ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");

                _func_dmu.zDropDownListWhereID(ddm0_training_branch_idx,
                                            "",
                                            "m0_training_branch_idx",
                                            "training_branch_name",
                                            "0",
                                            "trainingLoolup",
                                            "",
                                            "TRN-BRANCH",
                                            "m0_training_group_idx_ref",
                                            _func_dmu.zStringToInt(ddlm0_training_group_idx_ref.SelectedValue)
                                            );

            }

        }
        else if (sender is RadioButtonList)
        {
            var FvName = (RadioButtonList)sender;
            if (FvName.ID == "rdllecturer_type")
            {

                setlecturertype(_func_dmu.zStringToInt(FvName.SelectedValue), 0);
                setinstitutionDefault();
            }
            else if (FvName.ID == "rdotraining_course_type")
            {
                setCourseSel();
            }
            //else if ((FvName.ID == "rdolecturer_type_place") && (Hddfld_in_out_plan.Value == "OP"))
            //{
            //    //setplace();
            //    RadioButtonList _rdolecturer_type_place = (RadioButtonList)fvCRUD.FindControl("rdolecturer_type_place");
            //    DropDownList _ddl_training_location = (DropDownList)fvCRUD.FindControl("ddl_training_location");

            //    if (_rdolecturer_type_place.SelectedValue == "0")
            //    {
            //        ViewState["ddl_location_type"] = "1";
            //    }
            //    else
            //    {
            //        ViewState["ddl_location_type"] = "2";
            //    }

            //    select_ddl_training_location(_ddl_training_location);
            //}
        }
        else if (sender is TextBox)
        {

        }
    }
    #endregion FvDetail_DataBound

    public void setCourseSel(string status = "")
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        _ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
        _rdotraining_course_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_type");


        if (_func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue) == 1)
        {
            setCourseBudget(_func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue), status);
        }
        else if (
            (_func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue) == 2)
            ||
            (_func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue) == 3)
            )
        {
            add_course_as_dept();
            setCourseBudget(_func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue), status);
        }




    }

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void linkFvTrigger(FormView linkFvID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerFv = new PostBackTrigger();
        triggerFv.ControlID = linkFvID.UniqueID;
        updatePanel.Triggers.Add(triggerFv);
    }

    protected void FileUploadTrigger(FileUpload _FileUpload)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = _FileUpload.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    private void ClearTraning()
    {
        Select_Page1showdata();
    }


    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _Gvinstitution = (GridView)_FormView.FindControl("Gvinstitution");
        _Gvobjective = (GridView)_FormView.FindControl("Gvobjective");
        _Gvemp = (GridView)_FormView.FindControl("Gvemp");
        _Gvtrn_expenses = (GridView)_FormView.FindControl("Gvtrn_expenses");
        _Gvu8trncoursedate = (GridView)_FormView.FindControl("Gvu8trncoursedate");
        //_ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
        //_ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");

        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            var GvName = (GridView)sender;
            string sGvName = GvName.ID;
            if (sGvName == "GvMonthList_L")
            {
                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = rowSelect.RowIndex;
                DataSet dsContacts = (DataSet)ViewState["vsel_month_L"];
                dsContacts.Tables["dsel_month_L"].Rows[rowIndex].Delete();
                dsContacts.AcceptChanges();
                _func_dmu.zSetGridData(_GvMonthList_L, dsContacts.Tables["dsel_month_L"]);
            }
            else if (sGvName == "Gvinstitution")
            {
                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = rowSelect.RowIndex;
                DataSet dsContacts = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
                dsContacts.Tables["dsel_u1_training_course_lecturer"].Rows[rowIndex].Delete();
                dsContacts.AcceptChanges();
                _func_dmu.zSetGridData(_Gvinstitution, dsContacts.Tables["dsel_u1_training_course_lecturer"]);
            }
            else if (sGvName == "Gvobjective")
            {
                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = rowSelect.RowIndex;
                DataSet dsContacts = (DataSet)ViewState["vsel_u2_training_course_objective"];
                dsContacts.Tables["dsel_u2_training_course_objective"].Rows[rowIndex].Delete();
                dsContacts.AcceptChanges();
                _func_dmu.zSetGridData(_Gvobjective, dsContacts.Tables["dsel_u2_training_course_objective"]);
            }
            else if (sGvName == "Gvemp")
            {
                if (cmdName == "btnDel_emp")
                {
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsel_u3_training_course_employee"];
                    dsContacts.Tables["dsel_u3_training_course_employee"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    _func_dmu.zSetGridData(_Gvemp, dsContacts.Tables["dsel_u3_training_course_employee"]);
                    //Hddfld_costno.Value = _ddlcostcenter_idx_ref.SelectedValue;
                    //Hddfld_rdept.Value = _ddlRDeptID_ref.SelectedValue;
                    setCourseSel();
                    zCalculateCosts();
                    //if (_func_dmu.zStringToInt(Hddfld_costno.Value) > 0)
                    //{
                    //    _ddlcostcenter_idx_ref.SelectedValue = Hddfld_costno.Value;
                    //    _ddlRDeptID_ref.SelectedValue = Hddfld_rdept.Value;
                    //}
                }

            }
            else if (sGvName == "Gvtrn_expenses")
            {
                GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int rowIndex = rowSelect.RowIndex;
                DataSet dsContacts = (DataSet)ViewState["vsel_u4_training_course_expenses"];
                dsContacts.Tables["dsel_u4_training_course_expenses"].Rows[rowIndex].Delete();
                dsContacts.AcceptChanges();
                _func_dmu.zSetGridData(_Gvtrn_expenses, dsContacts.Tables["dsel_u4_training_course_expenses"]);
                zCalculateCosts();
            }
            else if (sGvName == "Gvu8trncoursedate")
            {
                if (cmdName == "btnDel_Gvu8")
                {
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsel_u8_training_course_date"];
                    dsContacts.Tables["dsel_u8_training_course_date"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    _func_dmu.zSetGridData(_Gvu8trncoursedate, dsContacts.Tables["dsel_u8_training_course_date"]);
                }
            }
        }
    }

    private Boolean zSave(int id, int node_idx, int actor_idx, int app_flag, int type_request, string comment)
    {
        int idx;
        Boolean _Boolean = false;
        string sMode = _func_dmu.zGetMode(Hddfld_training_course_no.Value), sDocno = "";// 
        string m0_prefix = "TRQ";
        int check;

        if (id > 0)
        {
            sMode = "E";
        }
        _FormView = getFv(sMode);

        _txttraining_course_date = (TextBox)_FormView.FindControl("txttraining_course_date");
        _txttraining_course_no = (TextBox)_FormView.FindControl("txttraining_course_no");
        _ddlcourse_plan_status = (DropDownList)_FormView.FindControl("ddlcourse_plan_status");
        _ddlu0_training_plan_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_training_plan_idx_ref");
        _txttraining_course_description = (TextBox)_FormView.FindControl("txttraining_course_description");
        _rdotraining_course_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_type");
        _ddlplace_idx_ref = (DropDownList)_FormView.FindControl("ddlplace_idx_ref");
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _txtdatestart_create = (TextBox)_FormView.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)_FormView.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)_FormView.FindControl("txt_timeend_create");
        _txttraining_course_remark = (TextBox)_FormView.FindControl("txttraining_course_remark");
        _rdotraining_course_planbudget_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_planbudget_type");
        _txttraining_course_total = (TextBox)_FormView.FindControl("txttraining_course_total");
        _txttraining_course_total_avg = (TextBox)_FormView.FindControl("txttraining_course_total_avg");
        _txttraining_course_reduce_tax = (TextBox)_FormView.FindControl("txttraining_course_reduce_tax");
        _txttraining_course_net_charge = (TextBox)_FormView.FindControl("txttraining_course_net_charge");
        _txttraining_course_net_charge_tax = (TextBox)_FormView.FindControl("txttraining_course_net_charge_tax");
        _ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
        _txttraining_course_planbudget_total = (TextBox)_FormView.FindControl("txttraining_course_planbudget_total");
        _txttraining_course_budget_total = (TextBox)_FormView.FindControl("txttraining_course_budget_total");
        _txttraining_course_budget_balance = (TextBox)_FormView.FindControl("txttraining_course_budget_balance");
        _txttraining_course_budget_total_per = (TextBox)_FormView.FindControl("txttraining_course_budget_total_per");
        _ddltraining_course_status = (DropDownList)_FormView.FindControl("ddltraining_course_status");
        _txtsuper_app_status = (TextBox)_FormView.FindControl("txtsuper_app_status");
        _ddlm0_target_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_target_group_idx_ref");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");
        _ddl_training_name_add = (DropDownList)_FormView.FindControl("ddl_training_name_add");
        _ddl_training_location = (DropDownList)_FormView.FindControl("ddl_training_location");
        _chkchoice_file = (CheckBoxList)_FormView.FindControl("chkchoice_file");
        _txtother = (TextBox)_FormView.FindControl("txtother");
        FileUpload UploadFileCourse = (FileUpload)_FormView.FindControl("UploadFileCourse");


        setObject();
        int iretrun_code = 9999;
        if ((id == 0) && (Hddfld_in_out_plan.Value == "OP"))
        {
            //Gen หลักสูตร อัตโนมัติ
            iretrun_code = _func_dmu.zStringToInt(_ddl_training_name_add.SelectedValue);//genSyllabusAuto();
        }

        if (iretrun_code == 0)
        {

        }
        else
        {
            if (sMode == "I")
            {
                // sDocno = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYMM", "N", "0000");
            }
            else
            {
                sDocno = _txttraining_course_no.Text;
            }

            decimal tot = Convert.ToDecimal(_txttraining_course_planbudget_total.Text);
            decimal cost_tot = Convert.ToDecimal(_txttraining_course_budget_total.Text);


            if (tot >= cost_tot)
            {

                training_course obj_training_course = new training_course();
                _data_elearning.el_training_course_action = new training_course[1];
                obj_training_course.u0_training_course_idx = id;
                // obj_training_course.training_course_no = sDocno;
                obj_training_course.training_course_date = _func_dmu.zDateToDB(_txttraining_course_date.Text);
                obj_training_course.course_plan_status = _ddlcourse_plan_status.SelectedValue;
                obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);

                obj_training_course.training_course_description = _txttraining_course_description.Text;
                obj_training_course.training_course_type = _func_dmu.zStringToInt(_rdotraining_course_type.SelectedValue);
                obj_training_course.place_idx_ref = _func_dmu.zStringToInt(_ddl_training_location.SelectedValue);
                obj_training_course.lecturer_type = _func_dmu.zStringToInt(_rdllecturer_type.SelectedValue);
                obj_training_course.training_course_date_strat = _func_dmu.zDateToDB(_txtdatestart_create.Text.Trim()) + " " +
                                                                 _txt_timestart_create.Text.Trim();

                obj_training_course.training_course_remark = _txttraining_course_remark.Text;
                obj_training_course.training_course_planbudget_type = _func_dmu.zStringToInt(_rdotraining_course_planbudget_type.SelectedValue);
                obj_training_course.training_course_total = _func_dmu.zStringToDecimal(_txttraining_course_total.Text);
                obj_training_course.training_course_total_avg = _func_dmu.zStringToDecimal(_txttraining_course_total_avg.Text);
                obj_training_course.training_course_reduce_tax = _func_dmu.zStringToDecimal(_txttraining_course_reduce_tax.Text);
                obj_training_course.training_course_net_charge = _func_dmu.zStringToDecimal(_txttraining_course_net_charge.Text);
                obj_training_course.training_course_net_charge_tax = _func_dmu.zStringToDecimal(_txttraining_course_net_charge_tax.Text);
                obj_training_course.costcenter_idx_ref = _func_dmu.zStringToInt(_ddlcostcenter_idx_ref.SelectedValue);
                obj_training_course.RDeptID_ref = _func_dmu.zStringToInt(_ddlRDeptID_ref.SelectedValue);
                obj_training_course.training_course_planbudget_total = _func_dmu.zStringToDecimal(_txttraining_course_planbudget_total.Text);
                obj_training_course.training_course_budget_total = _func_dmu.zStringToDecimal(_txttraining_course_budget_total.Text);
                obj_training_course.training_course_budget_balance = _func_dmu.zStringToDecimal(_txttraining_course_budget_balance.Text);
                obj_training_course.training_course_budget_total_per = _func_dmu.zStringToDecimal(_txttraining_course_budget_total_per.Text);
                obj_training_course.training_course_status = _func_dmu.zStringToInt(_ddltraining_course_status.SelectedValue);
                obj_training_course.m0_target_group_idx_ref = _func_dmu.zStringToInt(_ddlm0_target_group_idx_ref.SelectedValue);

                obj_training_course.org_idx = int.Parse(ViewState["org_idx"].ToString());
                obj_training_course.rdept_idx = int.Parse(ViewState["rdept_idx"].ToString());
                obj_training_course.rsec_idx = int.Parse(ViewState["rsec_idx"].ToString());
                obj_training_course.remark = comment;

                List<String> FileList = new List<string>();
                foreach (ListItem item in _chkchoice_file.Items)
                {
                    if (item.Selected)
                    {
                        FileList.Add(item.Value);

                    }
                }

                String file = String.Join(",", FileList.ToArray());

                obj_training_course.m0_fileidx_ref = file;
                obj_training_course.file_reason = _txtother.Text;


                if ((id > 0) && (Hddfld_in_out_plan.Value == "OP"))
                {
                    //obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddl_training_name_add.SelectedValue);
                    obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);
                    obj_training_course.u0_course_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);
                }
                else if ((id > 0) && (Hddfld_in_out_plan.Value != "OP"))
                {
                    obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);
                    obj_training_course.u0_course_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);

                }

                obj_training_course.training_course_created_by = emp_idx;
                obj_training_course.training_course_updated_by = emp_idx;
                obj_training_course.operation_status_id = "U0";
                obj_training_course.place_type = _func_dmu.zStringToInt(_rdolecturer_type_place.SelectedValue);
                if (Hddfld_in_out_plan.Value == "OP")
                {
                    obj_training_course.io_page_flag = "OP";
                    obj_training_course.course_plan_status = "O";
                }
                else
                {
                    obj_training_course.io_page_flag = "IP";
                }
                if (id == 0)
                {
                    if (Hddfld_in_out_plan.Value == "OP")
                    {
                        ////Gen หลักสูตร อัตโนมัติ
                        //obj_training_course.u0_training_plan_idx_ref = iretrun_code;
                        //obj_training_course.u0_course_idx_ref = iretrun_code;
                        obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddl_training_name_add.SelectedValue);
                        obj_training_course.u0_course_idx_ref = _func_dmu.zStringToInt(_ddl_training_name_add.SelectedValue);
                    }
                    else
                    {
                        obj_training_course.u0_training_plan_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);
                        obj_training_course.u0_course_idx_ref = _func_dmu.zStringToInt(_ddlu0_training_plan_idx_ref.SelectedValue);
                        obj_training_course.place_type = 0;
                    }
                }

                obj_training_course.approve_status = 0;
                obj_training_course.u0_idx = 1;
                obj_training_course.node_idx = node_idx;
                obj_training_course.actor_idx = actor_idx;
                obj_training_course.app_flag = app_flag;
                obj_training_course.app_user = 0;
                obj_training_course.hr_status = 0;
                obj_training_course.type_request = type_request;

                //node
                //if ((Hddfld_in_out_plan.Value == "OP") && (Hddfld_permission.Value != "HR"))
                //{
                //    obj_training_course.approve_status = 0;
                //    obj_training_course.u0_idx = 25;
                //    obj_training_course.node_idx = 14;
                //    obj_training_course.actor_idx = 2;
                //    obj_training_course.app_flag = 0;
                //    obj_training_course.app_user = 0;
                //    obj_training_course.hr_status = 0;

                //    if ((_txtsuper_app_status.Text != "4") && (_txtsuper_app_status.Text != "6"))
                //    {
                //        obj_training_course.zstatus = "user_update";
                //        obj_training_course.super_actor_idx = 0;
                //        obj_training_course.super_app_remark = "";
                //        obj_training_course.super_node_idx = 0;
                //        obj_training_course.super_app_status = 0;
                //        obj_training_course.super_app_user = 0;
                //        obj_training_course.super_u0_idx = 0;
                //    }
                //}
                //else
                //{

                //    obj_training_course.approve_status = 0;
                //    obj_training_course.u0_idx = 18;
                //    obj_training_course.node_idx = 14;
                //    obj_training_course.actor_idx = 1;
                //    obj_training_course.app_flag = 0;
                //    obj_training_course.app_user = 0;
                //    obj_training_course.hr_status = 1;
                //    obj_training_course.hr_user = emp_idx;
                //}

                obj_training_course.course_score = _func_dmu.zStringToInt(_txtcourse_score.Text);
                obj_training_course.score_through_per = _func_dmu.zStringToDecimal(_txtscore_through_per.Text);

                _data_elearning.el_training_course_action[0] = obj_training_course;
                // 
                //_func_dmu.zObjectToXml(_data_elearning);

                if (sMode == "I")
                {
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, _data_elearning);
                    if (_data_elearning.el_training_course_action != null)
                    {
                        sDocno = _data_elearning.el_training_course_action[0].training_course_no;
                    }

                    check = 0;
                }
                else
                {
                    check = 1;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);
                }
                
                

                idx = _data_elearning.return_code;
                if (id > 0)
                {
                    idx = id;

                }
                if (idx > 0)
                {
                    // zUploadTOElearning(sDocno);

                    if (check == 0)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["path_elearning_plan"];
                        string fileName_upload = sDocno;// idx.ToString();
                        string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                        int x = 0;
                        if (UploadFileCourse.HasFile)
                        {
                            if (!Directory.Exists(filePath_upload))
                            {
                                Directory.CreateDirectory(filePath_upload);
                            }
                            string extension = Path.GetExtension(UploadFileCourse.FileName);

                            foreach (HttpPostedFile uploadedFile in UploadFileCourse.PostedFiles)
                            {
                                if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                                {
                                    uploadedFile.SaveAs(Server.MapPath(getPathfile + sDocno) + "\\" + fileName_upload + x + extension);
                                }
                                x++;
                            }

                        }
                    }
                    else
                    {
                        string getPath = ConfigurationSettings.AppSettings["path_elearning_plan"];
                        string filePath = Server.MapPath(getPath + _txttraining_course_no.Text);
                        DirectoryInfo dir = new DirectoryInfo(filePath);
                        HttpFileCollection hfc = Request.Files;
                        int ocount = 0;

                        SearchDirectories(dir, _txttraining_course_no.Text);

                        if (checkfile != "11")
                        {
                            ocount = dir.GetFiles().Length;
                        }
                        int x = ocount;

                        if (UploadFileCourse.HasFile)
                        {
                            if (!Directory.Exists(filePath))
                            {
                                Directory.CreateDirectory(filePath);
                            }
                            string extension = Path.GetExtension(UploadFileCourse.FileName);

                            foreach (HttpPostedFile uploadedFile in UploadFileCourse.PostedFiles)
                            {
                                if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                                {
                                    uploadedFile.SaveAs(Server.MapPath(getPath + _txttraining_course_no.Text) + "\\" + _txttraining_course_no.Text + x + extension);
                                }
                                x++;
                            }
                        }
                    }

                    //obj_training_course = new training_course();
                    //_data_elearning.el_training_course_action = new training_course[1];
                    //obj_training_course.u0_training_course_idx = idx;
                    //obj_training_course.operation_status_id = "U0_updatefile";
                    //obj_training_course.training_course_file_name = _txttraining_course_file_name.Text;
                    //_data_elearning.el_training_course_action[0] = obj_training_course;
                    //_data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);

                    zSaveDetail(idx,node_idx,actor_idx,app_flag,type_request);

                }
                //if (idx > 0)
                //{
                //    if (_ddltraining_course_status.SelectedValue == "1")
                //    {
                //        if ((Hddfld_in_out_plan.Value == "OP") && (Hddfld_permission.Value == "USER"))
                //        {
                //            sendEmailusertoleader(idx);
                //        }
                //        else
                //        {
                //            sendEmailhrtohrd(idx);
                //        }

                //    }
                //}

                
                _Boolean = true;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณมีงบประมาณไม่พอสำหรับการอบรม กรุณาทำเรื่องของบประมาณ!!');", true);
                _Boolean = false;
            }
        }
        return _Boolean;
    }

    public int getValueInt(string Str)
    {
        int i = _func_dmu.zStringToInt(Str);

        return i;
    }

    public string getValueStr(string Str, string sValue)
    {
        string _string = "";
        if ((Str == null) || Str == "")
        {
            _string = sValue;
        }
        else
        {
            _string = Str;
        }
        return _string;
    }

    public string getValueIntTOStr(Int32 Str, string sValue)
    {
        string _string = "";
        if ((Str == null))
        {
            _string = sValue;
        }
        else
        {
            _string = Str.ToString();
        }
        return _string;
    }

    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void zShowdataUpdate(int id, string _Mode)
    {
        string valuecourse = String.Empty;
        string valueplace = String.Empty;
        string valuefile = String.Empty;
        string valuefile_reason = String.Empty;


        ViewState["zUpdate"] = "";
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = id;
        obj.operation_status_id = "U0-FULL";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fvCRUD, dataelearning.el_training_course_action);

        valuecourse = dataelearning.el_training_course_action[0].u0_training_plan_idx_ref.ToString();
        valueplace = dataelearning.el_training_course_action[0].place_idx_ref.ToString();
        valuefile = dataelearning.el_training_course_action[0].m0_fileidx_ref.ToString();
        valuefile_reason = dataelearning.el_training_course_action[0].file_reason.ToString();

        setdatadetail_default();

        string sDocno = "";
        string m0_prefix = "";
        int _iMonth;

        _FormView = getFv(_Mode);

        _txttraining_course_date = (TextBox)_FormView.FindControl("txttraining_course_date");
        _txttraining_course_no = (TextBox)_FormView.FindControl("txttraining_course_no");
        _ddlcourse_plan_status = (DropDownList)_FormView.FindControl("ddlcourse_plan_status");
        _ddlu0_training_plan_idx_ref = (DropDownList)_FormView.FindControl("ddlu0_training_plan_idx_ref");
        _txttraining_group_name = (TextBox)_FormView.FindControl("txttraining_group_name");
        _txttraining_branch_name = (TextBox)_FormView.FindControl("txttraining_branch_name");
        _txttraining_course_description = (TextBox)_FormView.FindControl("txttraining_course_description");
        _rdotraining_course_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_type");
        _ddlplace_idx_ref = (DropDownList)_FormView.FindControl("ddlplace_idx_ref");
        _rdllecturer_type = (RadioButtonList)_FormView.FindControl("rdllecturer_type");
        _txtdatestart_create = (TextBox)_FormView.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)_FormView.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)_FormView.FindControl("txt_timeend_create");
        _txttraining_course_date_qty = (TextBox)_FormView.FindControl("txttraining_course_date_qty");

        _txttraining_course_remark = (TextBox)_FormView.FindControl("txttraining_course_remark");
        _rdotraining_course_planbudget_type = (RadioButtonList)_FormView.FindControl("rdotraining_course_planbudget_type");
        _txttraining_course_total = (TextBox)_FormView.FindControl("txttraining_course_total");
        _txttraining_course_total_avg = (TextBox)_FormView.FindControl("txttraining_course_total_avg");
        _txttraining_course_reduce_tax = (TextBox)_FormView.FindControl("txttraining_course_reduce_tax");
        _txttraining_course_net_charge = (TextBox)_FormView.FindControl("txttraining_course_net_charge");
        _txttraining_course_net_charge_tax = (TextBox)_FormView.FindControl("txttraining_course_net_charge_tax");
        _ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
        _txttraining_course_planbudget_total = (TextBox)_FormView.FindControl("txttraining_course_planbudget_total");
        _txttraining_course_budget_total = (TextBox)_FormView.FindControl("txttraining_course_budget_total");
        _txttraining_course_budget_balance = (TextBox)_FormView.FindControl("txttraining_course_budget_balance");
        _txttraining_course_budget_total_per = (TextBox)_FormView.FindControl("txttraining_course_budget_total_per");
        _ddltraining_course_status = (DropDownList)_FormView.FindControl("ddltraining_course_status");
        _pnl_institution = (Panel)_FormView.FindControl("pnl_institution");
        _pnl_objective = (Panel)_FormView.FindControl("pnl_objective");
        _pnl_emp = (Panel)_FormView.FindControl("pnl_emp");
        _pnl_expenses = (Panel)_FormView.FindControl("pnl_expenses");
        _pnl_file = (Panel)_FormView.FindControl("pnl_file");
        _pnl_file_btn = (Panel)_FormView.FindControl("pnl_file_btn");
        _pnladd_emp_resulte = (Panel)_FormView.FindControl("pnladd_emp_resulte");
        _pnl_date = (Panel)_FormView.FindControl("pnl_date");
        _rdolecturer_type_place = (RadioButtonList)_FormView.FindControl("rdolecturer_type_place");


        _ddlm0_target_group_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_target_group_idx_ref");
        _pnlcourse_outplan_status2 = (Panel)_FormView.FindControl("pnlcourse_outplan_status2");
        _pnlhistory = (Panel)_FormView.FindControl("pnlhistory");
        _txtcourse_score = (TextBox)_FormView.FindControl("txtcourse_score");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");
        _ddl_training_location = (DropDownList)_FormView.FindControl("ddl_training_location");
        _ddlOrgID = (DropDownList)_FormView.FindControl("ddlOrgID");
        _ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
        _chkchoice_file = (CheckBoxList)_FormView.FindControl("chkchoice_file");
        _UploadFileCourse = (FileUpload)_FormView.FindControl("UploadFileCourse");
        _div_otherreason = (Control)_FormView.FindControl("div_otherreason");
        _txtother = (TextBox)_FormView.FindControl("txtother");
        //Start SetMode 
        _func_dmu.zModeTextBox(_txttraining_course_date, _Mode);
        _func_dmu.zModeDropDownList(_ddlcourse_plan_status, "P");
        _func_dmu.zModeDropDownList(_ddlu0_training_plan_idx_ref, "P");
        _func_dmu.zModeTextBox(_txttraining_course_description, _Mode);
        _func_dmu.zModeRadioButtonList(_rdotraining_course_type, _Mode);
        _func_dmu.zModeDropDownList(_ddlplace_idx_ref, _Mode);
        _func_dmu.zModeDropDownList(_ddl_training_location, _Mode);
        _func_dmu.zModeDropDownList(_ddlOrgID, _Mode);
        _func_dmu.zModeDropDownList(_ddlRDeptID_ref, _Mode);
        _func_dmu.zModeRadioButtonList(_rdolecturer_type_place, _Mode);
        _func_dmu.zModeRadioButtonList(_rdllecturer_type, _Mode);
        _func_dmu.zModePanel(_pnl_institution, _Mode);
        _func_dmu.zModeGridViewCol(_Gvinstitution, _Mode, 2);
        _func_dmu.zModeTextBox(_txtdatestart_create, _Mode);
        _func_dmu.zModeTextBox(_txt_timestart_create, _Mode);
        _func_dmu.zModeTextBox(_txt_timeend_create, _Mode);
        _func_dmu.zModePanel(_pnl_objective, _Mode);
        _func_dmu.zModeTextBox(_txttraining_course_remark, _Mode);
        _func_dmu.zModeGridViewCol(_Gvobjective, _Mode, 2);
        _func_dmu.zModeGridView(_Gvobjective, _Mode);
        _func_dmu.zModePanel(_pnl_emp, _Mode);
        _func_dmu.zModeCheckboxList(_chkchoice_file, _Mode);
        _func_dmu.zModeFileUpload(_UploadFileCourse, _Mode);
        _func_dmu.zModeRadioButtonList(_rdotraining_course_planbudget_type, _Mode);
        _func_dmu.zModePanel(_pnl_expenses, _Mode);
        _func_dmu.zModeGridViewCol(_Gvtrn_expenses, _Mode, 5);
        _func_dmu.zModeTextBox(_txttraining_course_reduce_tax, _Mode);
        _func_dmu.zModeDropDownList(_ddlcostcenter_idx_ref, _Mode);
        _func_dmu.zModeTextBox(_txttraining_course_planbudget_total, _Mode);
        _func_dmu.zModeDropDownList(_ddltraining_course_status, _Mode);
        _func_dmu.zModePanel(_pnl_file, _Mode);
        _func_dmu.zModePanel(_pnl_file_btn, _Mode);
        _func_dmu.zModeGridViewCol(_Gvemp, _Mode, 9);
        _func_dmu.zModePanel(_pnl_date, _Mode);
        _func_dmu.zModeGridViewCol(_Gvu8trncoursedate, _Mode, 4);
        _func_dmu.zModeDropDownList(_ddlm0_target_group_idx_ref, _Mode);
        _func_dmu.zModeTextBox(_txtother, _Mode);


        _txtdatestart_create.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _txt_timestart_create.Text = "09.00";
        _txt_timeend_create.Text = "16.00";
        _txttraining_course_date_qty.Text = "6";

        _pnladd_emp_resulte.Visible = false;
        _pnlhistory.Visible = false;
        //End SetMode
        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        Hddfld_status.Value = "E";
        Hddfld_mode.Value = _Mode;
        if (dataelearning.el_training_course_action != null)
        {

            foreach (var item in dataelearning.el_training_course_action)
            {
                Hddfld_training_course_no.Value = item.training_course_no;
                Hddfld_u0_training_course_idx.Value = item.u0_training_course_idx.ToString();


                ddl_type_plan.SelectedValue = ViewState["type_request"].ToString();

                if (item.zcount_resulte > 0)
                {
                    _pnladd_emp_resulte.Visible = true;
                }
                Hddfld_in_out_plan.Value = item.io_page_flag;
                if (Hddfld_in_out_plan.Value == "OP")
                {
                    //_func_dmu.zDropDownListWhereID(_ddlu0_training_plan_idx_ref,
                    //           "",
                    //           "zId",
                    //           "zName",
                    //           "0",
                    //           "trainingLoolup",
                    //            item.u0_course_idx_ref.ToString(),
                    //           "TRN-PLAN-U0-LOOKUP-OP",
                    //           "idx",
                    //           item.u0_course_idx_ref
                    //           );
                    _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    item.m0_target_group_idx_ref.ToString(),
                                    "TRN-TARGET"
                                    );
                    _txtcourse_score.Enabled = true;
                    _txtscore_through_per.Enabled = true;
                    _func_dmu.zModeTextBox(_txtcourse_score, _Mode);
                    _func_dmu.zModeTextBox(_txtscore_through_per, _Mode);
                }
                else
                {
                    //_func_dmu.zDropDownListWhereID(_ddlu0_training_plan_idx_ref,
                    //           "",
                    //           "zId",
                    //           "zName",
                    //           "0",
                    //           "trainingLoolup",
                    //            item.u0_training_plan_idx_ref.ToString(),
                    //           "TRN-PLAN-U0-LOOKUP",
                    //           "idx",
                    //           item.u0_training_plan_idx_ref
                    //           );
                    _func_dmu.zDropDownList(_ddlm0_target_group_idx_ref,
                                  "",
                                  "zId",
                                  "zName",
                                  "0",
                                  "trainingLoolup",
                                  item.m0_target_group_idx_ref.ToString(),
                                  "TRN-TARGET"
                                  );
                    _txtcourse_score.Enabled = false;
                    _txtscore_through_per.Enabled = false;
                }

                selectcourse_train(_ddlu0_training_plan_idx_ref);
                _ddlu0_training_plan_idx_ref.SelectedValue = valuecourse;
                //select_ddl_training_location(_ddl_training_location);
                _ddl_training_location.SelectedValue = valueplace;
                _txtother.Text = valuefile_reason;

                data_elearning dataelearning_detail;
                training_course obj_detail;
                //el_u1_training_course_lecturer
                dataelearning_detail = new data_elearning();
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U1-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u1_training_course_lecturer();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                        dr["lecturer_type"] = v_item.lecturer_type.ToString();
                        dr["m0_institution_idx_ref"] = v_item.m0_institution_idx_ref.ToString();
                        dr["zName"] = v_item.institution_name;
                        ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
                    }
                    ViewState["vsel_u1_training_course_lecturer"] = ds;
                }
                _func_dmu.zSetGridData(_Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);

                //el_u2_training_course_objective
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U2-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u2_training_course_objective();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                        dr["m0_objective_idx_ref"] = v_item.m0_objective_idx_ref.ToString();
                        dr["zName"] = v_item.objective_name;
                        dr["pass_test_flag"] = v_item.pass_test_flag.ToString();
                        dr["pass_test_per"] = v_item.pass_test_per.ToString();
                        dr["hour_training_flag"] = v_item.hour_training_flag.ToString();
                        dr["hour_training_per"] = v_item.hour_training_per.ToString();
                        dr["write_report_training_flag"] = v_item.write_report_training_flag.ToString();
                        dr["publish_training_flag"] = v_item.publish_training_flag.ToString();
                        dr["publish_training_description"] = v_item.publish_training_description;
                        dr["course_lecturer_flag"] = v_item.course_lecturer_flag.ToString();
                        dr["other_flag"] = v_item.other_flag.ToString();
                        dr["other_description"] = v_item.other_description;
                        dr["hrd_nofollow_flag"] = v_item.hrd_nofollow_flag.ToString();
                        dr["hrd_follow_flag"] = v_item.hrd_follow_flag.ToString();
                        dr["hrd_follow_day"] = v_item.hrd_follow_day;
                        ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                    }
                    ViewState["vsel_u2_training_course_objective"] = ds;
                }
                _func_dmu.zSetGridData(_Gvobjective, ViewState["vsel_u2_training_course_objective"]);

                //el_u3_training_course_employee
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U3-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u3_training_course_employee();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u3_training_course_employee"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u3_training_course_employee"].NewRow();
                        dr["emp_idx_ref"] = v_item.EmpIDX.ToString();
                        dr["empcode"] = v_item.EmpCode;
                        dr["zName"] = v_item.FullNameTH;
                        dr["RDeptID"] = v_item.RDeptID.ToString();
                        dr["RDeptName"] = v_item.DeptNameTH;
                        dr["zPostName"] = v_item.PosNameTH;
                        dr["zCostId"] = v_item.CostIDX.ToString();
                        dr["zCostCenter"] = v_item.CostNo;
                        dr["zTel"] = v_item.MobileNo;
                        dr["register_status"] = v_item.register_status.ToString();
                        if (v_item.zregister_date == null)
                        {
                            dr["zregister_date"] = "";
                        }
                        else
                        {
                            dr["zregister_date"] = v_item.zregister_date;
                        }
                        dr["register_date"] = v_item.register_date;
                        dr["register_user"] = v_item.register_user.ToString();
                        dr["register_remark"] = v_item.register_remark;
                        dr["u3_training_course_idx"] = v_item.u3_training_course_idx.ToString();

                        dr["signup_status"] = v_item.signup_status.ToString();
                        dr["signup_date"] = v_item.signup_date;
                        dr["signup_user"] = v_item.signup_user.ToString();
                        dr["signup_remark"] = v_item.signup_remark;


                        dr["test_scores"] = v_item.grade_avg.ToString();
                        dr["test_scores_status"] = v_item.test_scores_status.ToString();
                        dr["test_scores_date"] = v_item.signup_date;
                        dr["test_scores_user"] = v_item.test_scores_user.ToString();
                        dr["test_scores_remark"] = v_item.test_scores_remark;

                        if (v_item.zsignup_date == null)
                        {
                            dr["zsignup_date"] = "";
                        }
                        else
                        {
                            dr["zsignup_date"] = v_item.zsignup_date;
                        }
                        if (v_item.ztest_scores_date == null)
                        {
                            dr["ztest_scores_date"] = "";
                        }
                        else
                        {
                            dr["ztest_scores_date"] = v_item.ztest_scores_date;
                        }
                        dr["zstatus_name"] = v_item.zstatus_name;
                        dr["grade_status"] = v_item.grade_status.ToString();

                        ds.Tables["dsel_u3_training_course_employee"].Rows.Add(dr);
                    }
                    ViewState["vsel_u3_training_course_employee"] = ds;
                }


                _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);
                ViewState["zcostcenter_idx_ref"] = item.costcenter_idx_ref.ToString();
                ViewState["zCostNo"] = item.CostNo;
                ViewState["zUpdate"] = "update";
                //   setCourseSel("update");
                //  //  zCalculateCosts();


                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U4-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u4_training_course_expenses();

                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u4_training_course_expenses"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u4_training_course_expenses"].NewRow();
                        dr["expenses_description"] = v_item.expenses_description.ToString();
                        dr["amount"] = v_item.amount.ToString();
                        dr["vat"] = v_item.vat.ToString();
                        dr["withholding_tax"] = v_item.withholding_tax.ToString();

                        ds.Tables["dsel_u4_training_course_expenses"].Rows.Add(dr);
                    }
                    ViewState["vsel_u4_training_course_expenses"] = ds;
                }
                _func_dmu.zSetGridData(_Gvtrn_expenses, ViewState["vsel_u4_training_course_expenses"]);


                //litDebug.Text = ViewState["ChkAns"].ToString();
                string[] ToId = valuefile.Split(',');

                foreach (ListItem li in _chkchoice_file.Items)
                {
                    foreach (string To_check in ToId)
                    {

                        if (li.Value == To_check)
                        {
                            li.Selected = true;

                            if (To_check == "5")
                            {
                                _div_otherreason.Visible = true;

                            }
                            else
                            {
                                _div_otherreason.Visible = false;
                            }

                            break;
                        }
                        else
                        {
                            li.Selected = false;
                        }
                    }
                }

                _ddlcostcenter_idx_ref.SelectedValue = item.costcenter_idx_ref.ToString();
                _ddlRDeptID_ref.SelectedValue = item.RDeptID_ref.ToString();

                //el_u8_training_course_date
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U8-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u8_training_course_date();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u8_training_course_date"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u8_training_course_date"].NewRow();
                        dr["zdate"] = v_item.zdate;
                        dr["ztime_start"] = v_item.ztime_start;
                        dr["ztime_end"] = v_item.ztime_end;
                        dr["training_course_date_qty"] = v_item.training_course_date_qty.ToString();
                        ds.Tables["dsel_u8_training_course_date"].Rows.Add(dr);
                    }
                    ViewState["vsel_u8_training_course_date"] = ds;
                }
                _func_dmu.zSetGridData(_Gvu8trncoursedate, ViewState["vsel_u8_training_course_date"]);

                if (_Mode == "P")
                {
                    _pnlhistory.Visible = true;
                    _GvHistory = (GridView)_FormView.FindControl("GvHistory");
                    //history
                    dataelearning_detail.el_training_course_action = new training_course[1];
                    obj_detail = new training_course();
                    obj_detail.u0_training_course_idx = id;
                    obj_detail.operation_status_id = "L0";
                    dataelearning_detail.el_training_course_action[0] = obj_detail;
                    dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                    _func_dmu.zSetGridData(_GvHistory, dataelearning_detail.el_training_course_action);

                }

            }
            // setCourseSel();
            // zCalculateCosts();
        }

    }

    private void CreateDs_el_u1_training_course_lecturer()
    {
        string sDs = "dsel_u1_training_course_lecturer";
        string sVs = "vsel_u1_training_course_lecturer";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_institution_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_u2_training_course_objective()
    {
        string sDs = "dsel_u2_training_course_objective";
        string sVs = "vsel_u2_training_course_objective";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_objective_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_per", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_per", typeof(String));
        ds.Tables[sDs].Columns.Add("write_report_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_description", typeof(String));
        ds.Tables[sDs].Columns.Add("course_lecturer_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_description", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_nofollow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_day", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_u3_training_course_employee()
    {
        string sDs = "dsel_u3_training_course_employee";
        string sVs = "vsel_u3_training_course_employee";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u3_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("empcode", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptName", typeof(String));
        ds.Tables[sDs].Columns.Add("zPostName", typeof(String));
        ds.Tables[sDs].Columns.Add("zCostId", typeof(String));
        ds.Tables[sDs].Columns.Add("zCostCenter", typeof(String));
        ds.Tables[sDs].Columns.Add("zTel", typeof(String));
        ds.Tables[sDs].Columns.Add("register_status", typeof(String));
        ds.Tables[sDs].Columns.Add("register_date", typeof(String));
        ds.Tables[sDs].Columns.Add("zregister_date", typeof(String));
        ds.Tables[sDs].Columns.Add("register_user", typeof(String));
        ds.Tables[sDs].Columns.Add("register_remark", typeof(String));

        ds.Tables[sDs].Columns.Add("signup_status", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_user", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_date", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("zsignup_date", typeof(String));

        ds.Tables[sDs].Columns.Add("test_scores", typeof(String));
        ds.Tables[sDs].Columns.Add("test_scores_status", typeof(String));
        ds.Tables[sDs].Columns.Add("test_scores_user", typeof(String));
        ds.Tables[sDs].Columns.Add("test_scores_date", typeof(String));
        ds.Tables[sDs].Columns.Add("test_scores_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("ztest_scores_date", typeof(String));

        ds.Tables[sDs].Columns.Add("zstatus_name", typeof(String));
        ds.Tables[sDs].Columns.Add("grade_status", typeof(String));

        ViewState[sVs] = ds;

    }

    private void CreateDs_el_u4_training_course_expenses()
    {
        string sDs = "dsel_u4_training_course_expenses";
        string sVs = "vsel_u4_training_course_expenses";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u4_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("expenses_description", typeof(String));
        // ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        //  ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("amount", typeof(String));
        ds.Tables[sDs].Columns.Add("vat", typeof(String));
        ds.Tables[sDs].Columns.Add("withholding_tax", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_u5_training_course_follow()
    {
        string sDs = "dsel_u5_training_course_follow";
        string sVs = "vsel_u5_training_course_follow";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u5_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_per", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_per", typeof(String));
        ds.Tables[sDs].Columns.Add("write_report_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_description", typeof(String));
        ds.Tables[sDs].Columns.Add("course_lecturer_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_description", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_nofollow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_day", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_CourseBudget()
    {
        string sDs = "dsel_coursebudget";
        string sVs = "vsel_coursebudget";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("costno", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_CourseBudgetDept()
    {
        string sDs = "dsel_coursebudgetdept";
        string sVs = "vsel_coursebudgetdept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_CourseBudget_dept()
    {
        string sDs = "dsel_coursebudget_dept";
        string sVs = "vsel_coursebudget_dept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("costno", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_CourseBudgetDept_dept()
    {
        string sDs = "dsel_coursebudgetdept_dept";
        string sVs = "vsel_coursebudgetdept_dept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_month()
    {
        string sDs = "dsel_month";
        string sVs = "vsel_month";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_year", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m1", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m2", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m3", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m4", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m5", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m6", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m7", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m8", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m9", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m10", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m11", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m12", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_monthList()
    {
        string sDs = "dsel_month_L";
        string sVs = "vsel_month_L";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_year", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m1", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m2", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m3", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m4", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m5", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m6", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m7", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m8", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m9", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m10", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m11", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_m12", typeof(String));
        ViewState[sVs] = ds;

    }

    private void zShowDefaultMonth()
    {
        CreateDs_el_month();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        GridView GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
        DataSet Ds = (DataSet)ViewState["vsel_month"];
        DataRow dr;
        dr = Ds.Tables["dsel_month"].NewRow();
        dr["id"] = "1";
        dr["training_course_year"] = DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture);
        dr["training_course_m1"] = "";
        dr["training_course_m2"] = "";
        dr["training_course_m3"] = "";
        dr["training_course_m4"] = "";
        dr["training_course_m5"] = "";
        dr["training_course_m6"] = "";
        dr["training_course_m7"] = "";
        dr["training_course_m8"] = "";
        dr["training_course_m9"] = "";
        dr["training_course_m10"] = "";
        dr["training_course_m11"] = "";
        dr["training_course_m12"] = "";
        Ds.Tables["dsel_month"].Rows.Add(dr);
        ViewState["vsel_month"] = Ds;
        _func_dmu.zSetGridData(GvMonthList, Ds.Tables["dsel_month"]);
    }

    private void zShowDefaultMonthList()
    {
        CreateDs_el_month();
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        GridView GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        DataSet Ds = (DataSet)ViewState["vsel_month_L"];
        DataRow dr;
        _func_dmu.zSetGridData(GvMonthList_L, Ds.Tables["dsel_month_L"]);
    }

    private void setlecturertype(int _type, int _id)
    {

        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        _ddlm0_institution_idx_ref = (DropDownList)_FormView.FindControl("ddlm0_institution_idx_ref");

        if (_type == 0) //ภายใน
        {
            _func_dmu.zDropDownList(_ddlm0_institution_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    _id.ToString(),
                                    "EMPLOYEE"
                                    );
        }
        else
        {
            _func_dmu.zDropDownList(_ddlm0_institution_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    _id.ToString(),
                                    "INSTITUTION"
                                    );
        }
    }

    private Boolean checkError()
    {
        Boolean _Boolean = false;
        string _sError = " ไม่ถูกต้อง", _sError_sel = "กรุณาเลือก";
        int _iMonth = 0, _iYear = 0;
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));

        _txttraining_course_qty = (TextBox)_FormView.FindControl("txttraining_course_qty");
        _txttraining_course_amount = (TextBox)_FormView.FindControl("txttraining_course_amount");
        _txttraining_course_model = (TextBox)_FormView.FindControl("txttraining_course_model");
        _txttraining_course_budget = (TextBox)_FormView.FindControl("txttraining_course_budget");
        _txttraining_course_costperhead = (TextBox)_FormView.FindControl("txttraining_course_costperhead");
        _Gvinstitution = (GridView)_FormView.FindControl("Gvinstitution");
        _txtdatestart_create = (TextBox)_FormView.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)_FormView.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)_FormView.FindControl("txt_timeend_create");
        _Gvobjective = (GridView)_FormView.FindControl("Gvobjective");
        _Gvemp = (GridView)_FormView.FindControl("Gvemp");
        _Gvu8trncoursedate = (GridView)_FormView.FindControl("Gvu8trncoursedate");
        _txtscore_through_per = (TextBox)_FormView.FindControl("txtscore_through_per");

        string sdate_start = _func_dmu.zDateToDB(_txtdatestart_create.Text.Trim()) + " " + _txt_timestart_create.Text.Trim();
        DateTime DTdate_start = DateTime.Parse(sdate_start);
        //string sdate_end = _func_dmu.zDateToDB(_txtdateend_create.Text.Trim()) + " " + _txt_timeend_create.Text.Trim();
        // DateTime DTdate_end = DateTime.Parse(sdate_end);

        if ((_func_dmu.zStringToDecimal(_txtscore_through_per.Text) < 0) ||
             (_func_dmu.zStringToDecimal(_txtscore_through_per.Text) > 100)
             )
        {

            showAlert("กรุณากรอกคะแนนผ่าน % ให้ถูกต้อง");
            return true;
        }
        else if (_func_dmu.zCheckNumber(_Gvinstitution.Rows.Count.ToString()) == false)
        {

            showAlert(_sError_sel + "วิทยากร");
            return true;
        }
        else if (_Gvu8trncoursedate.Rows.Count == 0)
        {

            showAlert(_sError_sel + "วันที่อบรม");
            return true;
        }
        else if (_func_dmu.zCheckNumber(_Gvobjective.Rows.Count.ToString()) == false)
        {

            showAlert(_sError_sel + "วัตถุประสงค์");
            return true;
        }
        else if (_func_dmu.zCheckNumber(_Gvemp.Rows.Count.ToString()) == false)
        {

            showAlert(_sError_sel + "รหัสพนักงานผู้เข้าร่วมอบรม");
            return true;
        }
        else
        {
            zCalculateCosts();
            return _Boolean;
        }

    }

    private FormView getFv(string _sMode)
    {
        return fvCRUD;
        //if (_sMode == "I")
        //{
        //    return fvCRUD;
        //}
        //else
        //{
        //    return fv_Update;
        //}
    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            if (textbox.ID == "txttraining_course_planbudget_total")
            {
                zCalculateCosts();
            }
        }
    }

    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        training_course obj_training_course = new training_course();
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.training_course_updated_by = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlDelel_u_plan_course, _data_elearning);
    }

    public string getTextEmp(string register_status, string register_date)
    {
        string text = string.Empty;
        int id = 0;

        if ((_func_dmu.zStringToInt(register_status) == 1))
        {
            text = "<span class='statusmaster-online' data-toggle='tooltip' title='ลงทะเบียนเสร็จแล้ว วันที " + register_date + "'><i class='glyphicon glyphicon-ok'></i></span>";
            //"<span style='color:#26A65B;'> ลงทะเบียนเสร็จแล้ว วันที " + register_date + " </span>";
        }
        else
        {
            if ((Hddfld_mode.Value == "P") || (Hddfld_permission.Value != "HR")) //(Hddfld_in_out_plan.Value == "OP")
            {
                text = "<span class='' data-toggle='tooltip' title='รอลงทะเบียน'><i class='fa fa-hourglass-start'></i></span>";
            }
            else
            {
                text = "";
            }

        }


        return text;
    }

    public string getTextDoc(int hr_approve,
                             int md_approve,
                             string hr_decision_name,
                             string hr_node_name,
                             string hr_actor_name,
                             string md_decision_name,
                             string md_node_name,
                             string md_actor_name
        )
    {
        string text = string.Empty;
        int id = 0;

        if ((hr_approve == 4) && (md_approve == 4))
        {
            text = "<span style='color:#26A65B;'>" + md_decision_name + " จบการดำเนินการ โดย " + md_actor_name + "</span>";
        }
        else if ((hr_approve == 4) && (md_approve != 4))
        {
            id = md_approve;
            if (id == 5) // 5   กลับไปแก้ไข
            {
                text = "<span style='color:#F89406;'>" + md_decision_name + " โดย " + md_actor_name + "</span>";
            }
            else if (id == 6)
            {
                text = "<span style='color:#F03434;'>" + md_decision_name + " โดย " + md_actor_name + "</span>";
            }
            else
            {
                text = "รอการอนุมัติจาก MD";
            }
        }
        else
        {
            id = hr_approve;
            if (id == 5) // 5   กลับไปแก้ไข
            {
                text = "<span style='color:#F89406;'>" + hr_decision_name + " โดย " + hr_actor_name + "</span>";
            }
            else if (id == 6)
            {
                text = "<span style='color:#F03434;'>" + hr_decision_name + " โดย " + hr_actor_name + "</span>";
            }
            else
            {
                text = hr_decision_name + " โดย " + hr_actor_name;
            }
        }

        return text;
    }

    private int zShowdataHR_WDetail(int id, string _zstatus, string _flag)
    {
        int _int = 1;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = id;
        obj.operation_status_id = "U0-LISTDATA-HR";
        obj.zstatus = _zstatus;// "HR-W";
        dataelearning.el_training_course_action[0] = obj;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fv_preview, dataelearning.el_training_course_action);

        //End SetMode
        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        // Hddfld_status.Value = "E";
        if (dataelearning.el_training_course_action != null)
        {
            foreach (var item in dataelearning.el_training_course_action)
            {
                Hddfld_training_course_no.Value = item.training_course_no;
                Hddfld_u0_training_course_idx.Value = item.u0_training_course_idx.ToString();
                Panel pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                if (_flag == "HR")
                {
                    if ((item.md_approve_status == 4) || (item.md_approve_status == 6))
                    {
                        _int = 0;
                    }
                }
                else if (_flag == "LEADER")
                {
                    if ((item.approve_status == 4) || (item.approve_status == 6))
                    {
                        _int = 0;
                    }
                }

                _Gvinstitution = (GridView)fv_preview.FindControl("Gvinstitution");
                _Gvobjective = (GridView)fv_preview.FindControl("Gvobjective");
                _Gvemp = (GridView)fv_preview.FindControl("Gvemp");
                _Gvtrn_expenses = (GridView)fv_preview.FindControl("Gvtrn_expenses");
                _Gvu8trncoursedate = (GridView)fv_preview.FindControl("Gvu8trncoursedate");
                _pnltarget_group = (Panel)fv_preview.FindControl("pnltarget_group");
                if (item.io_page_flag == "OP")
                {
                    _pnltarget_group.Visible = true;
                }
                else
                {
                    _pnltarget_group.Visible = false;
                }

                data_elearning dataelearning_detail;
                training_course obj_detail;
                //el_u1_training_course_lecturer
                dataelearning_detail = new data_elearning();
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U1-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u1_training_course_lecturer();

                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                        dr["lecturer_type"] = v_item.lecturer_type.ToString();
                        dr["m0_institution_idx_ref"] = v_item.m0_institution_idx_ref.ToString();
                        dr["zName"] = v_item.institution_name;
                        ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
                    }
                    ViewState["vsel_u1_training_course_lecturer"] = ds;
                }
                _func_dmu.zSetGridData(_Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);

                //el_u2_training_course_objective
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U2-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u2_training_course_objective();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u2_training_course_objective"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u2_training_course_objective"].NewRow();
                        dr["m0_objective_idx_ref"] = v_item.m0_objective_idx_ref.ToString();
                        dr["zName"] = v_item.objective_name;
                        dr["pass_test_flag"] = v_item.pass_test_flag.ToString();
                        dr["pass_test_per"] = v_item.pass_test_per.ToString();
                        dr["hour_training_flag"] = v_item.hour_training_flag.ToString();
                        dr["hour_training_per"] = v_item.hour_training_per.ToString();
                        dr["write_report_training_flag"] = v_item.write_report_training_flag.ToString();
                        dr["publish_training_flag"] = v_item.publish_training_flag.ToString();
                        dr["publish_training_description"] = v_item.publish_training_description;
                        dr["course_lecturer_flag"] = v_item.course_lecturer_flag.ToString();
                        dr["other_flag"] = v_item.other_flag.ToString();
                        dr["other_description"] = v_item.other_description;
                        dr["hrd_nofollow_flag"] = v_item.hrd_nofollow_flag.ToString();
                        dr["hrd_follow_flag"] = v_item.hrd_follow_flag.ToString();
                        dr["hrd_follow_day"] = v_item.hrd_follow_day;
                        ds.Tables["dsel_u2_training_course_objective"].Rows.Add(dr);
                    }
                    ViewState["vsel_u2_training_course_objective"] = ds;
                }
                _func_dmu.zSetGridData(_Gvobjective, ViewState["vsel_u2_training_course_objective"]);

                //el_u3_training_course_employee
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U3-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u3_training_course_employee();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u3_training_course_employee"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u3_training_course_employee"].NewRow();
                        dr["emp_idx_ref"] = v_item.EmpIDX.ToString();
                        dr["empcode"] = v_item.EmpCode;
                        dr["zName"] = v_item.FullNameTH;
                        dr["RDeptID"] = v_item.RDeptID.ToString();
                        dr["RDeptName"] = v_item.DeptNameTH;
                        dr["zPostName"] = v_item.PosNameTH;
                        dr["zCostId"] = v_item.CostIDX.ToString();
                        dr["zCostCenter"] = v_item.CostNo;
                        dr["zTel"] = v_item.MobileNo;
                        dr["register_status"] = v_item.register_status.ToString();
                        if (v_item.zregister_date == null)
                        {
                            dr["zregister_date"] = "";
                        }
                        else
                        {
                            dr["zregister_date"] = v_item.zregister_date;
                        }
                        dr["register_date"] = v_item.register_date;
                        dr["register_user"] = v_item.register_user.ToString();
                        dr["register_remark"] = v_item.register_remark;
                        dr["u3_training_course_idx"] = v_item.u3_training_course_idx.ToString();

                        dr["signup_status"] = v_item.signup_status.ToString();
                        dr["signup_date"] = v_item.signup_date;
                        dr["signup_user"] = v_item.signup_user.ToString();
                        dr["signup_remark"] = v_item.signup_remark;

                        dr["test_scores"] = v_item.grade_avg.ToString();
                        dr["test_scores_status"] = v_item.test_scores_status.ToString();
                        dr["test_scores_date"] = v_item.signup_date;
                        dr["test_scores_user"] = v_item.test_scores_user.ToString();
                        dr["test_scores_remark"] = v_item.test_scores_remark;

                        if (v_item.zsignup_date == null)
                        {
                            dr["zsignup_date"] = "";
                        }
                        else
                        {
                            dr["zsignup_date"] = v_item.zsignup_date;
                        }
                        if (v_item.ztest_scores_date == null)
                        {
                            dr["ztest_scores_date"] = "";
                        }
                        else
                        {
                            dr["ztest_scores_date"] = v_item.ztest_scores_date;
                        }
                        dr["zstatus_name"] = v_item.zstatus_name;
                        dr["grade_status"] = v_item.grade_status.ToString();

                        ds.Tables["dsel_u3_training_course_employee"].Rows.Add(dr);
                    }
                    ViewState["vsel_u3_training_course_employee"] = ds;
                }
                _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);

                //el_u4_training_course_expenses
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U4-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u4_training_course_expenses();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u4_training_course_expenses"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u4_training_course_expenses"].NewRow();
                        dr["expenses_description"] = v_item.expenses_description;
                        dr["amount"] = v_item.amount.ToString();
                        dr["vat"] = v_item.vat.ToString();
                        dr["withholding_tax"] = v_item.withholding_tax.ToString();
                        ds.Tables["dsel_u4_training_course_expenses"].Rows.Add(dr);
                    }
                    ViewState["vsel_u4_training_course_expenses"] = ds;
                }
                _func_dmu.zSetGridData(_Gvtrn_expenses, ViewState["vsel_u4_training_course_expenses"]);

                //el_u8_training_course_date
                dataelearning_detail.el_training_course_action = new training_course[1];
                obj_detail = new training_course();
                obj_detail.u0_training_course_idx = id;
                obj_detail.operation_status_id = "U8-FULL";
                dataelearning_detail.el_training_course_action[0] = obj_detail;
                dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
                CreateDs_el_u8_training_course_date();
                if (dataelearning_detail.el_training_course_action != null)
                {
                    DataSet ds = (DataSet)ViewState["vsel_u8_training_course_date"];
                    DataRow dr;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        dr = ds.Tables["dsel_u8_training_course_date"].NewRow();
                        dr["zdate"] = v_item.zdate;
                        dr["ztime_start"] = v_item.ztime_start;
                        dr["ztime_end"] = v_item.ztime_end;
                        dr["training_course_date_qty"] = v_item.training_course_date_qty.ToString();
                        ds.Tables["dsel_u8_training_course_date"].Rows.Add(dr);
                    }
                    ViewState["vsel_u8_training_course_date"] = ds;
                }
                _func_dmu.zSetGridData(_Gvu8trncoursedate, ViewState["vsel_u8_training_course_date"]);


            }
        }

        // zShowDataPlanYear(_func_dmu.zStringToInt(Hddfld_u0_training_course_idx.Value), "A");

        return _int;
    }

    public void zShowDataPlanYear(int id, string sMode)
    {



        data_elearning dataelearning_u1 = new data_elearning();
        dataelearning_u1.el_training_course_action = new training_course[1];
        training_course obj_u1 = new training_course();
        obj_u1.u0_training_course_idx = id;
        obj_u1.operation_status_id = "U1-FULL";
        dataelearning_u1.el_training_course_action[0] = obj_u1;
        dataelearning_u1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_u1);

        CreateDs_el_monthList();


    }

    public string getlecturer_type(int id, string _a, string _b)
    {
        string _string = "";
        if (id == 0)
        {
            _string = "ภายใน";
        }
        else
        {
            _string = "ภายนอก";
        }
        _string = _string + " " + _a + " " + _b;
        return _string;

    }

    private void zSaveApprove(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove");

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.operation_status_id = "APPROVE-HR";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_course.u0_idx = 19;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_course.u0_idx = 20;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_course.u0_idx = 21;
        }
        obj_training_course.approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_course.node_idx = 2;
        obj_training_course.actor_idx = 6;
        obj_training_course.app_flag = 1;
        obj_training_course.app_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = _func_dmu.zJson(_urlSetUpdel_u_plan_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);

        if (ddlStatusapprove.SelectedValue == "4")
        {
            sendEmailhrdtomd(id);
        }
        sendEmailhrdtohr_all(id);


    }

    private void zSaveApproveMD(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove_md");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove_md");

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.operation_status_id = "APPROVE-MD";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.md_approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.md_approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_course.md_u0_idx = 22;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_course.md_u0_idx = 23;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_course.md_u0_idx = 24;
        }
        obj_training_course.md_approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_course.md_node_idx = 2;
        obj_training_course.md_actor_idx = 4;
        obj_training_course.md_app_flag = 1;
        obj_training_course.md_app_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);

        sendEmailmdtohrd_all(id);

    }

    // start scheduler

    public string MonthTH(int AMonth)
    {

        return _func_dmu.zMonthTH(AMonth);
    }

    public string getHtmlSched()
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj_trn_plan_course = new training_course();
        obj_trn_plan_course.filter_keyword = txtFilterKeyword_SCHED.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj_trn_plan_course.zyear = _func_dmu.zStringToInt(ddlYearSearch_SCHED.SelectedValue);
        obj_trn_plan_course.u0_course_idx_ref = _func_dmu.zStringToInt(ddltrn_groupSearch_SCHED.SelectedValue);

        // obj.zstatus = "MD-A";
        obj_trn_plan_course.operation_status_id = "U0-LISTDATA-SCHED";
        dataelearning.el_training_course_action[0] = obj_trn_plan_course;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        //_func_dmu.zSetRepeaterData(repMonths, _data_elearning.el_training_course_action);
        string sName = "", sHtmlAll = "", sHtml = "", sHtml1 = "", sHtml_Body1 = "", sHtml_Body2 = "";

        sHtml = "";
        int icount = 0;
        if (dataelearning.el_training_course_action != null)
        {
            foreach (var item in dataelearning.el_training_course_action)
            {
                sHtml_Body1 = "<td style=\"width: 80px;padding: 5px 0px;\"> </td>";
                sHtml1 = "";
                //im_total = 0;
                //ic = 0;
                int im1 = 0, im2 = 0, im3 = 0, im4 = 0, im5 = 0
            , im6 = 0, im7 = 0, im8 = 0
            , im9 = 0, im10 = 0, im11 = 0, im12 = 0
            , im1_data = 0, im2_data = 0, im3_data = 0
            , im4_data = 0, im5_data = 0
            , im6_data = 0, im7_data = 0, im8_data = 0
            , im9_data = 0, im10_data = 0, im11_data = 0
            , im12_data = 0
            , im_total = 0, ic = 0
            ;
                sName = item.course_name;
                //if (item.training_course_m1 > 0)
                //{
                //    im1 = 1;
                //    im1_data = item.training_course_m1;
                //}
                //if (item.training_course_m2 > 0)
                //{
                //    im2 = 1;
                //    im2_data = item.training_course_m2;
                //}
                //if (item.training_course_m3 > 0)
                //{
                //    im3 = 1;
                //    im3_data = item.training_course_m3;
                //}
                //if (item.training_course_m4 > 0)
                //{
                //    im4 = 1;
                //    im4_data = item.training_course_m4;
                //}
                //if (item.training_course_m5 > 0)
                //{
                //    im5 = 1;
                //    im5_data = item.training_course_m5;
                //}
                //if (item.training_course_m6 > 0)
                //{
                //    im6 = 1;
                //    im6_data = item.training_course_m6;
                //}
                //if (item.training_course_m7 > 0)
                //{
                //    im7 = 1;
                //    im7_data = item.training_course_m7;
                //}
                //if (item.training_course_m8 > 0)
                //{
                //    im8 = 1;
                //    im8_data = item.training_course_m8;
                //}
                //if (item.training_course_m9 > 0)
                //{
                //    im9 = 1;
                //    im9_data = item.training_course_m9;
                //}
                //if (item.training_course_m10 > 0)
                //{
                //    im10 = 1;
                //    im10_data = item.training_course_m10;
                //}
                //if (item.training_course_m11 > 0)
                //{
                //    im11 = 1;
                //    im11_data = item.training_course_m11;
                //}
                //if (item.training_course_m12 > 0)
                //{
                //    im12 = 1;
                //    im12_data = item.training_course_m12;
                //}
                if (im1 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im2 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im3 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im4 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im5 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im6 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im7 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im8 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im9 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im10 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im11 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im12 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im_total > 0)
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_course_idx, item.training_group_color);
                    im_total = 0;
                }
                sHtml1 = "<tr>" + sHtml1 + "</tr>";
                sHtml = sHtml + sHtml1;
            }
        }

        return sHtml;
    }

    public string getHtml(int icolspan = 0, string sdata = "", int id = 0, string sColor = "")
    {
        string sName = sdata;
        if (sdata.Length > 8)
        {
            sdata = _func_dmu.zTruncate(sdata, icolspan * 8);
            // sdata = sdata + "...";
        }
        int iwidth = 90 * icolspan;
        //if (icolspan == 3)
        //{
        //    iwidth = 93 * icolspan;
        //}
        if (icolspan >= 4)
        {
            iwidth = 93 * icolspan;
            // iwidth = iwidth + (1 * (icolspan - 1));
        }

        //width: 80px;  width: " + iwidth.ToString() + "px;
        string sHtml_Body = " <td  style=\"padding: 5px 0px;\" colspan=" + icolspan.ToString() + "> " +
                          " <center>" +
            " <input id=\"btn_sched" + id.ToString() + "\" type=\"button\" value=\"" + sdata + "\" class=\"pull-left\" style=\"background-color: " + sColor + ";" +
        " border: none;" +
   "      color: white;" +
    "     text-align: center;" +
    "     text-decoration: none;" +
     "    display: inline-block;" +
     "      padding: 5px 5px;" +
      "   font-size: 14px;" +
    "  width: 100%;   " +
     "    cursor: pointer;\" " +
     " data-toggle=\"tooltip\" title=\"" + sName + "\" " +
     "   onclick=\"ShowCurrentTime1(" + id.ToString() + ")\"  " +
     " />" +

      "</center>" +
                             " </td>";



        //"      padding: 5px 5px;" +
        //"     width: auto;" +
        // " onclick = \"ShowCurrentTime()\" " +
        if (icolspan == 0)
        {
            sHtml_Body = "";
        }
        return sHtml_Body;
    }


    [WebMethod]
    public static string GetCurrentTime(string name)
    {
        // string name = "";
        string sStr = "";
        using (SqlConnection conn = new SqlConnection())
        {

            conn.ConnectionString = ConfigurationManager
                    .ConnectionStrings["conn_mas"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder query = new StringBuilder();
                query.Clear();
                query.AppendLine("SELECT u0_training_course_idx ");
                query.AppendLine(", training_course_no ");
                query.AppendLine(", CONVERT(VARCHAR(30), training_course_date, 103) as training_course_date ");
                query.AppendLine(", training_course_year ");
                query.AppendLine(", u0_course_idx_ref ");
                query.AppendLine(", lecturer_type ");
                query.AppendLine(", m0_institution_idx_ref ");
                query.AppendLine(", m0_target_group_idx_ref ");
                query.AppendLine(", training_course_qty ");
                query.AppendLine(", training_course_amount ");
                query.AppendLine(", training_course_model ");
                query.AppendLine(", training_course_m1 ");
                query.AppendLine(", training_course_m2 ");
                query.AppendLine(", training_course_m3 ");
                query.AppendLine(", training_course_m4 ");
                query.AppendLine(", training_course_m5 ");
                query.AppendLine(", training_course_m6 ");
                query.AppendLine(", training_course_m7 ");
                query.AppendLine(", training_course_m8 ");
                query.AppendLine(", training_course_m9 ");
                query.AppendLine(", training_course_m10 ");
                query.AppendLine(", training_course_m11 ");
                query.AppendLine(", training_course_m12 ");
                query.AppendLine(", training_course_budget ");
                query.AppendLine(", training_course_costperhead ");
                query.AppendLine(", training_course_status ");
                query.AppendLine(", training_course_created_by ");
                query.AppendLine(", training_course_created_at ");
                query.AppendLine(", training_course_updated_by ");
                query.AppendLine(", training_course_updated_at ");
                query.AppendLine(", isnull(a.priority_name, '') + '  ' + isnull(a.course_name, '') course_name ");
                query.AppendLine(", training_group_name ");
                query.AppendLine(", training_branch_name ");
                query.AppendLine(", priority_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(", approve_status ");
                query.AppendLine(", u0_idx ");
                query.AppendLine(", node_idx ");
                query.AppendLine(", actor_idx ");
                query.AppendLine(", approve_remark ");
                query.AppendLine(", decision_name ");
                query.AppendLine(", node_name ");
                query.AppendLine(", actor_name ");
                query.AppendLine(", md_approve_status ");
                query.AppendLine(", md_approve_remark ");
                query.AppendLine(", md_u0_idx ");
                query.AppendLine(", md_node_idx ");
                query.AppendLine(", md_actor_idx ");
                query.AppendLine(", md_decision_name ");
                query.AppendLine(", md_node_name ");
                query.AppendLine(", md_actor_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(",case when lecturer_type = 0 then ");
                query.AppendLine("(select  top 1 emp.FullNameTH from Centralized.dbo.ViewEmployee emp ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and emp.EmpStatus = 1 ");
                query.AppendLine("and emp.EmpIDX = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("else  ");
                query.AppendLine("(select  top 1 m0_t.institution_name from el_m0_institution m0_t ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and m0_t.institution_status = 1 ");
                query.AppendLine("and m0_t.m0_institution_idx = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("end institution_name ");
                query.AppendLine("FROM View_el_u0_training_course a");
                query.AppendLine(" where 1=1 ");
                query.AppendLine(" and u0_training_course_idx = " + name);

                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {

                        //customers.Add(string.Format("{0}-{1}", sdr["ContactName"], sdr["CustomerId"]));
                        sStr = sStr + "เลขที่เอกสาร : " + sdr["training_course_no"].ToString() +
                               Environment.NewLine +
                               " วันที่สร้างเอกสาร : " + sdr["training_course_date"].ToString() +
                               Environment.NewLine +
                               " ชื่อหลักสูตร : " + sdr["course_name"].ToString() +
                               Environment.NewLine +
                               " กลุ่มวิชา : " + sdr["training_group_name"].ToString() +
                               Environment.NewLine +
                               " สาขาวิชา : " + sdr["training_branch_name"].ToString() +
                               Environment.NewLine +
                               " วิทยากร : " + sdr["institution_name"].ToString();
                        //Environment.NewLine +
                        //" กลุ่มเป้าหมาย : " + sdr["target_group_name"].ToString();
                        //Environment.NewLine +
                        //" จำนวนคนต่อรุ่น : " + sdr["training_course_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_course_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_course_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_course_date"].ToString() +
                        //Environment.NewLine +
                        //" วันที่สร้างเอกสาร : " + sdr["training_course_date"].ToString() +
                    }
                }
                conn.Close();
            }
            // return customers.ToArray();
        }
        return sStr;


        //  return "Hello " + name + Environment.NewLine + "The Current Time is: <br/>"
        // + DateTime.Now.ToString();
        //zShowdataDetail_sched(_func_dmu.zStringToInt(name), "HR-A", "HR");
        //return "Hello " + name;

        //fv_detail_sched.DataSource = dataelearning;

        //dataelearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);


        //dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        //_func_dmu.zSetFormViewData(fv_detail_sched, dataelearning.el_training_course_action);


    }

    [WebMethod]
    public static string CodebehindMethodName()
    {
        // string name = "";
        return "Hello 9000 " + Environment.NewLine + "The Current Time is: "
             + DateTime.Now.ToString();
    }

    private void zShowdataDetail_sched(int id, string _zstatus, string _flag)
    {
        int _int = 1;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx = id;
        obj.operation_status_id = "U0-LISTDATA-HR";
        obj.zstatus = _zstatus;// "HR-W";
        dataelearning.el_training_course_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        //  _func_dmu.zSetFormViewData(fv_detail_sched, dataelearning.el_training_course_action);


    }

    // end scheduler

    protected void AddMonthList()
    {
        string _sError = " ไม่ถูกต้อง";
        int iError = 0;
        int _iMonth = 0, _iMonth_L = 0, _iYear = 0, _iYear_L = 0;
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));
        _txttraining_course_model = (TextBox)_FormView.FindControl("txttraining_course_model");
        //* start check error *//

        _GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
        _iYear = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtyear_item")).Text);
        _iMonth = 0;
        for (int i = 1; i <= 12; i++)
        {
            _iMonth = _iMonth + _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtm" + i.ToString() + "_item")).Text);
        }

        _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        _iMonth_L = 0;
        for (int j = 0; j < _GvMonthList_L.Rows.Count; j++)
        {
            for (int i = 1; i <= 12; i++)
            {
                _iMonth_L = _iMonth_L + _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtm" + i.ToString() + "_item_L")).Text);
            }
            if (_iYear == _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text))
            {
                _iYear_L = _func_dmu.zStringToInt(((Label)_GvMonthList_L.Rows[j].FindControl("txtyear_item_L")).Text);
            }
        }

        if (_iYear <= 0)
        {
            showAlert("ปีแผนการอบรม" + _sError);
            iError++;
        }
        else if (_iYear == _iYear_L)
        {
            showAlert("ปีแผนการอบรมนี้มีแล้ว");
            iError++;
        }
        else if (_iMonth <= 0)
        {

            showAlert("แผนการอบรม" + _sError);
            iError++;
        }
        else if (_func_dmu.zStringToInt(_txttraining_course_model.Text) < (_iMonth))
        {

            showAlert("แผนการอบรม" + _sError);
            iError++;
        }
        else if (_func_dmu.zStringToInt(_txttraining_course_model.Text) < (_iMonth + _iMonth_L))
        {

            showAlert("จำนวนรุ่น" + _sError);
            iError++;
        }

        //* start check error *//
        if (iError == 0)
        {

            _GvMonthList = (GridView)_FormView.FindControl("GvMonthList");
            _GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");

            DataSet Ds = (DataSet)ViewState["vsel_month_L"];
            DataRow dr;
            dr = Ds.Tables["dsel_month_L"].NewRow();
            dr["id"] = Ds.Tables["dsel_month_L"].Rows.Count + 1;
            _iMonth = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtyear_item")).Text);
            dr["training_course_year"] = _iMonth.ToString();
            for (int i = 1; i <= 12; i++)
            {
                _iMonth = _func_dmu.zStringToInt(((TextBox)_GvMonthList.Rows[0].FindControl("txtm" + i.ToString() + "_item")).Text);
                if (i == 1)
                {
                    dr["training_course_m1"] = _iMonth.ToString();
                }
                else if (i == 2)
                {
                    dr["training_course_m2"] = _iMonth.ToString();
                }
                else if (i == 3)
                {
                    dr["training_course_m3"] = _iMonth.ToString();
                }
                else if (i == 4)
                {
                    dr["training_course_m4"] = _iMonth.ToString();
                }
                else if (i == 5)
                {
                    dr["training_course_m5"] = _iMonth.ToString();
                }
                else if (i == 6)
                {
                    dr["training_course_m6"] = _iMonth.ToString();
                }
                else if (i == 7)
                {
                    dr["training_course_m7"] = _iMonth.ToString();
                }
                else if (i == 8)
                {
                    dr["training_course_m8"] = _iMonth.ToString();
                }
                else if (i == 9)
                {
                    dr["training_course_m9"] = _iMonth.ToString();
                }
                else if (i == 10)
                {
                    dr["training_course_m10"] = _iMonth.ToString();
                }
                else if (i == 11)
                {
                    dr["training_course_m11"] = _iMonth.ToString();
                }
                else if (i == 12)
                {
                    dr["training_course_m12"] = _iMonth.ToString();
                }
            }
            Ds.Tables["dsel_month_L"].Rows.Add(dr);
            ViewState["vsel_month_L"] = Ds;
            _func_dmu.zSetGridData(_GvMonthList_L, Ds.Tables["dsel_month_L"]);
            zShowDefaultMonth();
        }

    }

    public string getStrformate(string str, int iformat)
    {
        str = _func_dmu.zFormatfloat(str, iformat);
        return str;
    }

    private void sendEmailhrtohrd(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {
            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HR-TO-HRD";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            // litDebug.Text = _func_dmu.zJson(_urlsendEmail_plan_course_hrtohrd, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_plan_course_hrtohrd, _data_elearning);
        }
        catch { }
    }

    private void sendEmailhrdtomd(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {
            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HRD-TO-MD";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_plan_course_hrdtomd, _data_elearning);
        }
        catch { }
    }

    private void sendEmailhrdtohr_all(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {
            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-HRD-TO-HR-ALL";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_plan_course_hrdtohr_all, _data_elearning);
        }
        catch { }

    }

    private void sendEmailmdtohrd_all(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-MD-TO-HRD-ALL-COURSE";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_plan_course_mdtohrd_all, _data_elearning);
        }
        catch { }
    }

    private void sendEmailusertoleader(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {
            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-USER-TO-LEADER";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //  litkeg.Text = _func_dmu.zJson(_urlsendEmail_outplan_course_usertoleader, _data_elearning);
            // litkeg.Text = _funcTool.convertObjectToJson(_data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_outplan_course_usertoleader, _data_elearning);
        }
        catch { }
    }

    public string getbackgroundcolor(int id = 0)
    {
        string sStr = "";
        if (id > 0)
        {
            sStr = "background-color: #92d050;";
        }

        return sStr;
    }
    //end Report

    public void zCalculateCosts()
    {
        _FormView = getFv(_func_dmu.zGetMode(Hddfld_training_course_no.Value));

        _txttraining_course_total = (TextBox)fvCRUD.FindControl("txttraining_course_total");
        _txttraining_course_net_charge = (TextBox)fvCRUD.FindControl("txttraining_course_net_charge");
        _txttraining_course_budget_total = (TextBox)fvCRUD.FindControl("txttraining_course_budget_total");
        _txttraining_course_total_avg = (TextBox)fvCRUD.FindControl("txttraining_course_total_avg");
        _txttraining_course_net_charge_tax = (TextBox)fvCRUD.FindControl("txttraining_course_net_charge_tax");
        _txttraining_course_budget_balance = (TextBox)fvCRUD.FindControl("txttraining_course_budget_balance");
        _txttraining_course_planbudget_total = (TextBox)fvCRUD.FindControl("txttraining_course_planbudget_total");
        _txttraining_course_budget_total_per = (TextBox)fvCRUD.FindControl("txttraining_course_budget_total_per");

        //_GvMonthList_L = (GridView)_FormView.FindControl("GvMonthList_L");
        //_Gvinstitution = (GridView)_FormView.FindControl("Gvinstitution");
        //_Gvobjective = (GridView)_FormView.FindControl("Gvobjective");
        _Gvemp = (GridView)_FormView.FindControl("Gvemp");
        _Gvtrn_expenses = (GridView)_FormView.FindControl("Gvtrn_expenses");


        decimal amount = 0, vat = 0, withholding_tax = 0, a = 0, v = 0, w = 0;
        DataSet Ds = (DataSet)ViewState["vsel_u4_training_course_expenses"];
        foreach (DataRow dtrow in Ds.Tables["dsel_u4_training_course_expenses"].Rows)
        {

            a = _func_dmu.zStringToDecimal(dtrow["amount"].ToString());
            v = _func_dmu.zStringToDecimal(dtrow["vat"].ToString());
            w = _func_dmu.zStringToDecimal(dtrow["withholding_tax"].ToString());
            v = v - w;
            if (v < 0)
            {
                v = v * (-1);
            }
            a = a * (v / 100);
            a = Math.Round(a, 2);
            vat = vat + a;
            amount = amount + _func_dmu.zStringToDecimal(dtrow["amount"].ToString());
            withholding_tax = withholding_tax + _func_dmu.zStringToDecimal(dtrow["withholding_tax"].ToString());

        }
        decimal dcEmp = 0, dcAvg = 0;
        _txttraining_course_total.Text = _func_dmu.zFormatfloat(amount.ToString(), 2);
        dcEmp = _Gvemp.Rows.Count;
        if (dcEmp > 0)
        {
            dcAvg = amount / dcEmp;
        }
        dcAvg = Math.Round(dcAvg, 2);
        _txttraining_course_total_avg.Text = _func_dmu.zFormatfloat(dcAvg.ToString(), 2);

        _txttraining_course_budget_total.Text = _func_dmu.zFormatfloat(amount.ToString(), 2);

        amount = amount + vat;
        _txttraining_course_net_charge.Text = _func_dmu.zFormatfloat(amount.ToString(), 2);
        dcEmp = 0; dcAvg = 0;
        dcEmp = _Gvemp.Rows.Count;
        if (dcEmp > 0)
        {
            dcAvg = amount / dcEmp;
        }
        dcAvg = Math.Round(dcAvg, 2);
        _txttraining_course_net_charge_tax.Text = _func_dmu.zFormatfloat(dcAvg.ToString(), 2);
        amount = _func_dmu.zStringToDecimal(_txttraining_course_planbudget_total.Text);
        amount = amount - _func_dmu.zStringToDecimal(_txttraining_course_budget_total.Text);
        _txttraining_course_budget_balance.Text = _func_dmu.zFormatfloat(amount.ToString(), 2);
        decimal dcTotal = 0;
        decimal dcUse = 0;
        decimal dcPer = 0;
        dcTotal = _func_dmu.zStringToDecimal(_txttraining_course_planbudget_total.Text);
        dcUse = _func_dmu.zStringToDecimal(_txttraining_course_budget_total.Text);
        if (dcTotal > 0)
        {
            dcPer = ((dcUse - dcTotal) / dcTotal) * 100;
            dcPer = Math.Round(dcPer, 2);
            dcPer = dcPer * (-1);
        }


        _txttraining_course_budget_total_per.Text = _func_dmu.zFormatfloat(dcPer.ToString(), 2);
    }

    public void setCourseBudget(int id, string status)
    {
        _ddlcostcenter_idx_ref = (DropDownList)fvCRUD.FindControl("ddlcostcenter_idx_ref");
        _ddlRDeptID_ref = (DropDownList)fvCRUD.FindControl("ddlRDeptID_ref");
        _txttraining_course_planbudget_total = (TextBox)fvCRUD.FindControl("txttraining_course_planbudget_total");
        _txttraining_course_budget_total = (TextBox)fvCRUD.FindControl("txttraining_course_budget_total");
        _txttraining_course_budget_balance = (TextBox)fvCRUD.FindControl("txttraining_course_budget_balance");
        _txttraining_course_budget_total_per = (TextBox)fvCRUD.FindControl("txttraining_course_budget_total_per");
        CreateDs_el_CourseBudget();
        CreateDs_el_CourseBudgetDept();

        if (id == 1)
        {
            DataSet Ds = (DataSet)ViewState["vsel_coursebudget"];
            DataRow dr;
            dr = Ds.Tables["dsel_coursebudget"].NewRow();
            if (ViewState["zUpdate"].ToString() == "update")
            {
                dr["id"] = ViewState["zcostcenter_idx_ref"].ToString();
                dr["costno"] = ViewState["zCostNo"].ToString();
            }
            else
            {
                dr["id"] = ViewState["CostIDX"].ToString();
                dr["costno"] = ViewState["CostNo"].ToString();
                //litDebug.Text = "CostNo" + ViewState["CostNo"].ToString();
            }

            Ds.Tables["dsel_coursebudget"].Rows.Add(dr);
            ViewState["vsel_coursebudget"] = Ds;
            //  gggggg
            _func_dmu.zSetDdlData(_ddlcostcenter_idx_ref, ViewState["vsel_coursebudget"], "costno", "id", 1);
            _ddlcostcenter_idx_ref.SelectedIndex = 1;

            DataSet Ds1 = (DataSet)ViewState["vsel_coursebudgetdept"];
            DataRow dr1;
            dr1 = Ds1.Tables["dsel_coursebudgetdept"].NewRow();
            dr1["id"] = ViewState["rdept_idx"].ToString();
            dr1["zName"] = ViewState["RDeptName"].ToString();
            Ds1.Tables["dsel_coursebudgetdept"].Rows.Add(dr1);
            ViewState["vsel_coursebudgetdept"] = Ds1;
            //_func_dmu.zSetDdlData(_ddlRDeptID_ref, ViewState["vsel_coursebudgetdept"], "zName", "id", 1);
            //_ddlRDeptID_ref.SelectedIndex = 1;

        }
        else if ((id == 2) || (id == 3))
        {
            ViewState["zUpdate"] = "";
            //_func_dmu.zSetDdlData(_ddlcostcenter_idx_ref, ViewState["vsel_coursebudget_dept"], "costno", "id", 1);
            //_func_dmu.zSetDdlData(_ddlRDeptID_ref, ViewState["vsel_coursebudgetdept_dept"], "zName", "id", 1);
        }



    }

    public void add_course_as_dept()
    {
        CreateDs_el_CourseBudget_dept();
        CreateDs_el_CourseBudgetDept_dept();

        int itemObj = 0;
        DataSet dsU1 = (DataSet)ViewState["vsel_u3_training_course_employee"];
        course[] objcourse1 = new course[dsU1.Tables["dsel_u3_training_course_employee"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsel_u3_training_course_employee"].Rows)
        {
            int icount = 0;
            DataSet Ds = (DataSet)ViewState["vsel_coursebudget_dept"];
            foreach (DataRow dtrow in Ds.Tables["dsel_coursebudget_dept"].Rows)
            {
                if (item["zCostId"].ToString().Trim() == dtrow["id"].ToString().Trim())
                {
                    icount++;
                }
            }
            if (icount == 0)
            {
                DataRow dr;
                dr = Ds.Tables["dsel_coursebudget_dept"].NewRow();
                dr["id"] = item["zCostId"].ToString();
                dr["costno"] = item["zCostCenter"].ToString() + " - " + item["RDeptName"].ToString();
                dr["RDeptID"] = item["RDeptID"].ToString();
                Ds.Tables["dsel_coursebudget_dept"].Rows.Add(dr);
                ViewState["vsel_coursebudget_dept"] = Ds;
            }
            // Dept
            icount = 0;
            DataSet Ds_dept = (DataSet)ViewState["vsel_coursebudgetdept_dept"];
            foreach (DataRow dtrow in Ds_dept.Tables["dsel_coursebudgetdept_dept"].Rows)
            {
                if (item["RDeptID"].ToString().Trim() == dtrow["id"].ToString().Trim())
                {
                    icount++;
                }
            }
            if (icount == 0)
            {
                DataRow dr;
                dr = Ds_dept.Tables["dsel_coursebudgetdept_dept"].NewRow();
                dr["id"] = item["RDeptID"].ToString();
                dr["zName"] = item["RDeptName"].ToString();
                Ds_dept.Tables["dsel_coursebudgetdept_dept"].Rows.Add(dr);
                ViewState["vsel_coursebudgetdept_dept"] = Ds_dept;
            }


            itemObj++;

        }
    }

    private void zSaveDetail(int _id, int node_idx, int actor_idx, int app_flag, int type_request)
    {
        if (_id <= 0)
        {
            return;
        }

        int ic = 0, icount = 0, irow = 0, u0_training_course_idx = 0;
        int itemObj = 0;

        u0_training_course_idx = _id;
        data_elearning dataelearning = new data_elearning();
        //******************  start el_u1_training_course_lecturer *********************//
        itemObj = 0;
        DataSet dsU1 = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
        training_course[] objcourse1 = new training_course[dsU1.Tables["dsel_u1_training_course_lecturer"].Rows.Count];
        foreach (DataRow item in dsU1.Tables["dsel_u1_training_course_lecturer"].Rows)
        {
            objcourse1[itemObj] = new training_course();
            objcourse1[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse1[itemObj].lecturer_type = _func_dmu.zStringToInt(item["lecturer_type"].ToString());
            objcourse1[itemObj].m0_institution_idx_ref = _func_dmu.zStringToInt(item["m0_institution_idx_ref"].ToString());
            objcourse1[itemObj].training_course_status = 1;
            objcourse1[itemObj].training_course_updated_by = emp_idx;
            objcourse1[itemObj].operation_status_id = "U1";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse1 = new training_course[1];
            objcourse1[itemObj] = new training_course();
            objcourse1[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse1[itemObj].operation_status_id = "U1-DEL";
        }
        dataelearning.el_training_course_action = objcourse1;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        //******************  end el_u1_training_course_lecturer *********************//

        //******************  start el_u2_training_course_objective *********************//
        add_objective_dataToDataset(1);
        itemObj = 0;
        DataSet dsU2 = (DataSet)ViewState["vsel_u2_training_course_objective"];
        training_course[] objcourse2 = new training_course[dsU2.Tables["dsel_u2_training_course_objective"].Rows.Count];
        foreach (DataRow item in dsU2.Tables["dsel_u2_training_course_objective"].Rows)
        {
            objcourse2[itemObj] = new training_course();
            objcourse2[itemObj].u0_training_course_idx_ref = u0_training_course_idx;

            objcourse2[itemObj].m0_objective_idx_ref = _func_dmu.zStringToInt(item["m0_objective_idx_ref"].ToString());
            objcourse2[itemObj].pass_test_flag = _func_dmu.zStringToInt(item["pass_test_flag"].ToString());
            objcourse2[itemObj].pass_test_per = _func_dmu.zStringToDecimal(item["pass_test_per"].ToString());
            objcourse2[itemObj].hour_training_flag = _func_dmu.zStringToInt(item["hour_training_flag"].ToString());
            objcourse2[itemObj].hour_training_per = _func_dmu.zStringToDecimal(item["hour_training_per"].ToString());
            objcourse2[itemObj].write_report_training_flag = _func_dmu.zStringToInt(item["write_report_training_flag"].ToString());
            objcourse2[itemObj].publish_training_flag = _func_dmu.zStringToInt(item["publish_training_flag"].ToString());
            objcourse2[itemObj].publish_training_description = item["publish_training_description"].ToString();
            objcourse2[itemObj].course_lecturer_flag = _func_dmu.zStringToInt(item["course_lecturer_flag"].ToString());
            objcourse2[itemObj].other_flag = _func_dmu.zStringToInt(item["other_flag"].ToString());
            objcourse2[itemObj].other_description = item["other_description"].ToString();
            objcourse2[itemObj].hrd_nofollow_flag = _func_dmu.zStringToInt(item["hrd_nofollow_flag"].ToString());
            objcourse2[itemObj].hrd_follow_flag = _func_dmu.zStringToInt(item["hrd_follow_flag"].ToString());
            objcourse2[itemObj].hrd_follow_day = _func_dmu.zStringToInt(item["hrd_follow_day"].ToString());

            objcourse2[itemObj].training_course_status = 1;
            objcourse2[itemObj].training_course_updated_by = emp_idx;
            objcourse2[itemObj].operation_status_id = "U2";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse2 = new training_course[1];
            objcourse2[itemObj] = new training_course();
            objcourse2[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse2[itemObj].operation_status_id = "U2-DEL";
        }
        dataelearning.el_training_course_action = objcourse2;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        //******************  end el_u2_training_course_objective *********************//

        //******************  start el_u3_training_course_employee *********************//
        itemObj = 0;
        DataSet dsU3 = (DataSet)ViewState["vsel_u3_training_course_employee"];
        training_course[] objcourse3 = new training_course[dsU3.Tables["dsel_u3_training_course_employee"].Rows.Count];
        foreach (DataRow item in dsU3.Tables["dsel_u3_training_course_employee"].Rows)
        {
            objcourse3[itemObj] = new training_course();
            objcourse3[itemObj].u3_training_course_idx = _func_dmu.zStringToInt(item["u3_training_course_idx"].ToString());
            objcourse3[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse3[itemObj].emp_idx_ref = _func_dmu.zStringToInt(item["emp_idx_ref"].ToString());
            objcourse3[itemObj].register_status = _func_dmu.zStringToInt(item["register_status"].ToString());
            objcourse3[itemObj].register_user = _func_dmu.zStringToInt(item["register_user"].ToString());
            objcourse3[itemObj].register_remark = item["register_remark"].ToString();
            objcourse3[itemObj].register_date = item["register_date"].ToString();

            objcourse3[itemObj].training_course_status = 1;
            objcourse3[itemObj].training_course_updated_by = emp_idx;
            objcourse3[itemObj].operation_status_id = "U3";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse3 = new training_course[1];
            objcourse3[itemObj] = new training_course();
            objcourse3[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse3[itemObj].operation_status_id = "U3-DEL";
        }
        dataelearning.el_training_course_action = objcourse3;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        //******************  end el_u3_training_course_employee *********************//

        //******************  start el_u4_training_course_expenses *********************//
        itemObj = 0;
        DataSet dsU4 = (DataSet)ViewState["vsel_u4_training_course_expenses"];
        training_course[] objcourse4 = new training_course[dsU4.Tables["dsel_u4_training_course_expenses"].Rows.Count];
        foreach (DataRow item in dsU4.Tables["dsel_u4_training_course_expenses"].Rows)
        {
            objcourse4[itemObj] = new training_course();
            objcourse4[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse4[itemObj].expenses_description = item["expenses_description"].ToString();
            objcourse4[itemObj].amount = _func_dmu.zStringToDecimal(item["amount"].ToString());
            objcourse4[itemObj].vat = _func_dmu.zStringToDecimal(item["vat"].ToString());
            objcourse4[itemObj].withholding_tax = _func_dmu.zStringToDecimal(item["withholding_tax"].ToString());
            objcourse4[itemObj].training_course_status = 1;
            objcourse4[itemObj].training_course_updated_by = emp_idx;
            objcourse4[itemObj].operation_status_id = "U4";
            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse4 = new training_course[1];
            objcourse4[itemObj] = new training_course();
            objcourse4[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse4[itemObj].operation_status_id = "U4-DEL";
        }
        dataelearning.el_training_course_action = objcourse4;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        //******************  end el_u4_training_course_expenses *********************//

        //******************  start el_u8_training_course_date *********************//
        itemObj = 0;
        DataSet dsU8 = (DataSet)ViewState["vsel_u8_training_course_date"];
        training_course[] objcourse8 = new training_course[dsU8.Tables["dsel_u8_training_course_date"].Rows.Count];
        foreach (DataRow item in dsU8.Tables["dsel_u8_training_course_date"].Rows)
        {

            objcourse8[itemObj] = new training_course();
            objcourse8[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse8[itemObj].training_course_date_start = _func_dmu.zDateToDB(item["zdate"].ToString()) + " " + item["ztime_start"].ToString();
            objcourse8[itemObj].training_course_date_end = _func_dmu.zDateToDB(item["zdate"].ToString()) + " " + item["ztime_end"].ToString();
            objcourse8[itemObj].training_course_date_qty = _func_dmu.zStringToDecimal(item["training_course_date_qty"].ToString());
            objcourse8[itemObj].training_course_status = 1;
            objcourse8[itemObj].training_course_updated_by = emp_idx;
            objcourse8[itemObj].operation_status_id = "U8";

            objcourse8[itemObj].node_idx = node_idx;
            objcourse8[itemObj].actor_idx = actor_idx;
            objcourse8[itemObj].app_flag = app_flag;
            objcourse8[itemObj].type_request = type_request;

            itemObj++;

        }
        if (itemObj == 0)
        {
            objcourse8 = new training_course[1];
            objcourse8[itemObj] = new training_course();
            objcourse8[itemObj].u0_training_course_idx_ref = u0_training_course_idx;
            objcourse8[itemObj].operation_status_id = "U8-DEL";
        }
        dataelearning.el_training_course_action = objcourse8;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        //******************  end el_u8_training_course_date *********************//


    }

    public string course_plan_status_name(string staus)
    {
        string SetName = "";
        if (staus == "I")
        {
            SetName = "In Plan";
        }
        else if (staus == "O")
        {
            SetName = "Out Plan";
        }

        return SetName;
    }

    public string getlabelSave()
    {
        string SetName = "";
        if (Hddfld_training_course_no.Value != "")
        {
            SetName = "บันทึกการเปลี่ยนแปลง";
        }
        else
        {
            SetName = "บันทึก";
        }
        return SetName;
    }

    public string DownloadFile(string sDocNo, string sFolder_file = "")
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        if (sFolder_file == "")
        {
            sFolder_file = _Folder_plan_course;
        }
        _PathFile = _PathFile + sFolder_file + "/" + sDocNo + "/";
        string sFileName = "", filePath = "";
        if (sDocNo == "")
        {

        }
        else
        {



            try
            {

                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        try
                        {
                            sFileName = Path.GetFileName(file);
                            filePath = file;
                        }
                        catch { }
                    }
                }
            }
            catch { }
        }
        if (sFileName != "")
        {
            filePath = _PathFile + sFileName;
            filePath = ResolveUrl(filePath);
        }
        else
        {
            filePath = "";
        }
        return filePath;

    }

    public void ImportEmployee()
    {
        _txttraining_course_file_name = (TextBox)fvCRUD.FindControl("txttraining_course_file_name");
        _Gvemp = (GridView)fvCRUD.FindControl("Gvemp");
        string sFile = "", sFileName = "";
        string _itemExtension = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + "IMPORT_EMP" + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFile = Path.GetFileName(file);
                        _itemExtension = Path.GetExtension(file);
                    }
                }
            }
            catch { }

            if (sFile != "")
            {
                if (_itemExtension.ToLower() == ".xls" || _itemExtension.ToLower() == ".xlsx")
                {
                    string filePath = _PathFile + "/" + sFile;
                    filePath = Server.MapPath(filePath);

                    string conStr = String.Empty;
                    if (_itemExtension.ToLower() == ".xls")
                    {
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        //conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    else
                    {
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        // conStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }

                    conStr = String.Format(conStr, filePath, "Yes");
                    OleDbConnection connExcel = new OleDbConnection(conStr);
                    OleDbCommand cmdExcel = new OleDbCommand();
                    OleDbDataAdapter oda = new OleDbDataAdapter();
                    DataTable dt = new DataTable();

                    //OleDbConnection conn = new OleDbConnection(conStr);
                    //if (conn.State == ConnectionState.Closed)
                    //{
                    //    conn.Open();
                    //}

                    cmdExcel.Connection = connExcel;
                    connExcel.Open();
                    DataTable dtExcelSchema;
                    dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                    connExcel.Close();
                    connExcel.Open();
                    cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                    oda.SelectCommand = cmdExcel;
                    oda.Fill(dt);
                    connExcel.Close();

                    var count_alert_u1 = dt.Rows.Count;

                    var u0_waterimport = new training_course[dt.Rows.Count];

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        u0_waterimport[i] = new training_course();

                        if (dt.Rows[i][0].ToString().Trim() != String.Empty)
                        {
                            string sEmp_Code = "";
                            int iEmp_Idx = 0;
                            sEmp_Code = dt.Rows[i][0].ToString().Trim();
                            data_elearning dataelearning = new data_elearning();
                            dataelearning.employeeM_action = new employeeM[1];
                            employeeM obj = new employeeM();
                            obj.EmpCode = sEmp_Code;
                            dataelearning.employeeM_action[0] = obj;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
                            dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_employee, dataelearning);
                            if (dataelearning.employeeM_action != null)
                            {
                                foreach (var item in dataelearning.employeeM_action)
                                {
                                    iEmp_Idx = item.EmpIDX;
                                }
                            }
                            if (iEmp_Idx > 0)
                            {
                                add_employee_data(1, iEmp_Idx);

                            }

                        }
                    }

                    _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                    _PathFile = _PathFile + _Folder_plan_course_bin + "/" + "IMPORT_EMP" + "/" + Hddfld_folder.Value;
                    try
                    {
                        if (_PathFile != "")
                        {
                            string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string file in filesLoc)
                            {
                                File.Delete(file);
                            }
                        }
                    }
                    catch { }

                    _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);
                    setCourseSel();
                    zCalculateCosts();
                }
            }

        }

    }

    private DataTable OpenData(StreamReader sr, int ARow = 2)
    {
        string tmpstr, rline = null;
        int i = 0, j, ncol, nrow;

        rline = sr.ReadLine();
        ncol = 0;
        while (i < rline.Length)
        {
            tmpstr = null;
            while (i < rline.Length && rline[i] != ',')
            {
                i++;
            }
            i++;
            ncol++;
        }
        if (ncol == 0)
        {
            return null;
        }

        i = 0;
        j = 0;
        while (i < rline.Length)
        {
            tmpstr = null;
            while (i < rline.Length && rline[i] != ',')
            {
                tmpstr = tmpstr + rline[i].ToString();
                i++;
            }
            i++;
            j++;
        }
        // CREATE TABLE
        DataTable aTable = new DataTable("DATA");
        DataColumn dtCol;

        //dtCol = new DataColumn();
        //dtCol.DataType = System.Type.GetType("System.String");
        //dtCol.ColumnName = "RecNo";
        //dtCol.Caption = "RecN.";
        //dtCol.ReadOnly = true;
        //dtCol.Unique = false;
        //aTable.Columns.Add(dtCol);
        ncol = 1;
        for (i = 0; i < ncol; i++)
        {
            dtCol = new DataColumn();
            dtCol.DataType = System.Type.GetType("System.String");
            dtCol.ColumnName = "emp_idx";// colstr[i];
            dtCol.Caption = "emp_idx";// colstr[i];
            dtCol.ReadOnly = true;
            dtCol.Unique = false;
            aTable.Columns.Add(dtCol);
        }
        // CREATE TABLE

        string[] rowstr = new string[ncol];
        DataRow dtRow;
        nrow = 0;
        while ((rline = sr.ReadLine()) != null)
        {
            nrow++;
            i = 0;
            j = 0;
            while (i < rline.Length)
            {
                tmpstr = null;
                while (i < rline.Length && rline[i] != ',')
                {
                    tmpstr = tmpstr + rline[i].ToString();
                    i++;
                }
                rowstr[j] = tmpstr;
                i++;
                j++;
            }

            dtRow = aTable.NewRow();
            //dtRow["RecNo"] = nrow.ToString();
            for (int k = 0; k < ncol; k++)
            {
                // dtRow[colstr[k]] = rowstr[k];
            }
            aTable.Rows.Add(dtRow);
        }

        return aTable;
    }

    public void importEmployeeProcessRequest(HttpPostedFile _HttpPostedFile, int item)
    {

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + "IMPORT_EMP" + "/" + Hddfld_folder.Value;
            zDeleteFileBinEmployee();
            if (_HttpPostedFile != null && _HttpPostedFile.ContentLength > 0)
            {
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                string _itemExtension = Path.GetExtension(_HttpPostedFile.FileName);
                string _itemNameNew = item.ToString() + _itemExtension.ToLower();
                string _itemFilePath = "";
                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                _itemFilePath = Server.MapPath(newFilePath);
                _HttpPostedFile.SaveAs(_itemFilePath);
            }
        }

    }

    public void zDeleteFileBinEmployee()
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course_bin + "/" + "IMPORT_EMP" + "/" + Hddfld_folder.Value;
        try
        {
            if (_PathFile != "")
            {
                string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                List<ListItem> files = new List<ListItem>();
                foreach (string file in filesLoc)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch { }
                }
            }
        }
        catch { }
    }
    // start get value preview //
    public string getValue_plan_status(string Str)
    {
        string sName = "";
        if (Str == "I")
        {
            sName = "In Plan";
        }
        else if (Str == "O")
        {
            sName = "Out Plan";
        }
        return sName;
    }

    public string getValue_course_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "1")
        {
            sName = "คอร์สตามผู้ทำรายการ";
        }
        else if (Str.ToString() == "2")
        {
            sName = "คอร์สตามแผนก";
        }
        else if (Str.ToString() == "3")
        {
            sName = "คอร์สตามผู้เรียน";
        }
        return sName;
    }

    public string getValue_place(int Str)
    {
        string sName = "";
        if (Str.ToString() == "3")
        {
            sName = "นพวงศ์";
        }
        else if (Str.ToString() == "4")
        {
            sName = "โรจนะ";
        }
        else if (Str.ToString() == "5")
        {
            sName = "เมืองทองธานี";
        }
        return sName;
    }

    public string getValue_lecturer_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "0")
        {
            sName = "ภายใน";
        }
        else if (Str.ToString() == "1")
        {
            sName = "ภายนอก";
        }
        return sName;
    }
    public string getValue_planbudget_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "1")
        {
            sName = "มีค่าใช้จ่ายอยู่ในงบประมาณ";
        }
        else if (Str.ToString() == "2")
        {
            sName = "เกินงบประมาณ";
        }
        else if (Str.ToString() == "3")
        {
            sName = "ฟรี";
        }
        return sName;

    }
    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    // end get value preview //
    private void zShowdata_register(int id)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.idx = id;
        obj.operation_status_id = "TRN-PLAN-COURSE-EMP";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(FvEdit_register, dataelearning.el_training_course_action);

        if (dataelearning.el_training_course_action != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_register();", true);
        }

    }
    private void zSave_register(int id)
    {
        int idx;
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)FvEdit_register.FindControl("ddlStatusapprove");
        TextBox txtdetailApprove = (TextBox)FvEdit_register.FindControl("txtdetailApprove");
        TextBox txtu3_training_course_idx = (TextBox)FvEdit_register.FindControl("txtu3_training_course_idx");
        TextBox txtu0_training_course_idx_ref = (TextBox)FvEdit_register.FindControl("txtu0_training_course_idx_ref");

        training_course obj_training_course = new training_course();
        //traning_req U1
        idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
        obj_training_course.u3_training_course_idx = _func_dmu.zStringToInt(txtu3_training_course_idx.Text);
        obj_training_course.emp_idx_ref = emp_idx;
        obj_training_course.operation_status_id = "APPROVE-REGISTER-HR";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.register_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.register_remark = txtdetailApprove.Text.Trim();
        }


        obj_training_course.approve_status = 1;
        obj_training_course.u0_idx = 33;
        obj_training_course.node_idx = 10;
        obj_training_course.actor_idx = 1;
        obj_training_course.app_flag = 1;
        obj_training_course.app_user = emp_idx;
        obj_training_course.approve_remark = obj_training_course.register_remark;

        obj_training_course.register_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);
        obj_training_course.register_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);
        zShowData_Employee(idx);

    }
    private void zShowData_Employee(int id)
    {
        //el_u3_training_course_employee
        CreateDs_el_u3_training_course_employee();
        setEmployeeDefault();
        data_elearning dataelearning_detail = new data_elearning();
        training_course obj_detail;
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U3-FULL";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        // CreateDs_el_u3_training_course_employee();
        if (dataelearning_detail.el_training_course_action != null)
        {
            DataSet ds = (DataSet)ViewState["vsel_u3_training_course_employee"];
            DataRow dr;
            foreach (var v_item in dataelearning_detail.el_training_course_action)
            {
                dr = ds.Tables["dsel_u3_training_course_employee"].NewRow();
                dr["emp_idx_ref"] = v_item.EmpIDX.ToString();
                dr["empcode"] = v_item.EmpCode;
                dr["zName"] = v_item.FullNameTH;
                dr["RDeptID"] = v_item.RDeptID.ToString();
                dr["RDeptName"] = v_item.DeptNameTH;
                dr["zPostName"] = v_item.PosNameTH;
                dr["zCostId"] = v_item.CostIDX.ToString();
                dr["zCostCenter"] = v_item.CostNo;
                dr["zTel"] = v_item.MobileNo;
                dr["register_status"] = v_item.register_status.ToString();
                if (v_item.zregister_date == null)
                {
                    dr["zregister_date"] = "";
                }
                else
                {
                    dr["zregister_date"] = v_item.zregister_date;
                }
                dr["register_date"] = v_item.register_date;
                dr["register_user"] = v_item.register_user.ToString();
                dr["register_remark"] = v_item.register_remark;
                dr["u3_training_course_idx"] = v_item.u3_training_course_idx.ToString();

                dr["signup_status"] = v_item.signup_status.ToString();
                dr["signup_date"] = v_item.signup_date;
                dr["signup_user"] = v_item.signup_user.ToString();
                dr["signup_remark"] = v_item.signup_remark;

                dr["test_scores"] = v_item.grade_avg.ToString();
                dr["test_scores_status"] = v_item.test_scores_status.ToString();
                dr["test_scores_date"] = v_item.signup_date;
                dr["test_scores_user"] = v_item.test_scores_user.ToString();
                dr["test_scores_remark"] = v_item.test_scores_remark;

                if (v_item.zsignup_date == null)
                {
                    dr["zsignup_date"] = "";
                }
                else
                {
                    dr["zsignup_date"] = v_item.zsignup_date;
                }
                if (v_item.ztest_scores_date == null)
                {
                    dr["ztest_scores_date"] = "";
                }
                else
                {
                    dr["ztest_scores_date"] = v_item.ztest_scores_date;
                }
                dr["zstatus_name"] = v_item.zstatus_name;
                dr["grade_status"] = v_item.grade_status.ToString();

                ds.Tables["dsel_u3_training_course_employee"].Rows.Add(dr);
            }
            ViewState["vsel_u3_training_course_employee"] = ds;
        }
        _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u3_training_course_employee"]);
        //  setCourseSel();
        //  zCalculateCosts();
        _Gvemp.Focus();
    }

    private void zShowdata_signup(int id)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.idx = id;
        obj.operation_status_id = "TRN-PLAN-COURSE-EMP-SIGNUP";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(FvEdit_signup, dataelearning.el_training_course_action);

        if (dataelearning.el_training_course_action != null)
        {
            // _ddlcostcenter_idx_ref = (DropDownList)_FormView.FindControl("ddlcostcenter_idx_ref");
            //_ddlRDeptID_ref = (DropDownList)_FormView.FindControl("ddlRDeptID_ref");
            // Hddfld_costno.Value = _ddlcostcenter_idx_ref.SelectedValue;
            //  Hddfld_rdept.Value = _ddlRDeptID_ref.SelectedValue;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_signup();", true);

            //  _ddlcostcenter_idx_ref.SelectedValue = Hddfld_costno.Value;

        }

    }
    private void zSave_signup(int id)
    {
        int idx;
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)FvEdit_signup.FindControl("ddlStatusapprove");
        TextBox txtdetailApprove = (TextBox)FvEdit_signup.FindControl("txtdetailApprove");
        TextBox txtu3_training_course_idx = (TextBox)FvEdit_signup.FindControl("txtu3_training_course_idx");
        TextBox txtu0_training_course_idx_ref = (TextBox)FvEdit_signup.FindControl("txtu0_training_course_idx_ref");

        training_course obj_training_course = new training_course();
        //traning_req U1
        idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
        obj_training_course.u3_training_course_idx = _func_dmu.zStringToInt(txtu3_training_course_idx.Text);
        obj_training_course.emp_idx_ref = emp_idx;
        obj_training_course.operation_status_id = "APPROVE-SIGNUP-HR";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.signup_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.signup_remark = txtdetailApprove.Text.Trim();
        }


        obj_training_course.signup_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);


        obj_training_course.approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);
        obj_training_course.u0_idx = 35;
        obj_training_course.node_idx = 16;
        obj_training_course.actor_idx = 1;
        obj_training_course.app_flag = 1;
        obj_training_course.app_user = emp_idx;
        obj_training_course.approve_remark = obj_training_course.signup_remark;

        obj_training_course.signup_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);
        zShowData_Employee(idx);



    }

    public Boolean getsignup(string register, string signup, string status = "")
    {

        Boolean _Boolean = false;
        int iregister = _func_dmu.zStringToInt(register);
        int isignup = _func_dmu.zStringToInt(signup);
        if ((iregister == 0) || (Hddfld_mode.Value == "P") || (Hddfld_permission.Value != "HR")) //(Hddfld_in_out_plan.Value == "OP")
        {
            _Boolean = false;
        }
        else
        {
            if (isignup == 1)
            {
                _Boolean = false;
            }
            else
            {
                _Boolean = true;
            }

        }
        if ((status == "scores") && ((Hddfld_mode.Value == "P") || (Hddfld_permission.Value != "HR"))) //(Hddfld_in_out_plan.Value == "OP")
        {
            _Boolean = true;
        }

        return _Boolean;
    }
    public string getTextEmp_signup(string register, string signup, string signup_date)
    {
        string text = string.Empty;
        int id = 0;

        if ((getsignup(register, signup) == false) || (Hddfld_mode.Value == "P") || (Hddfld_permission.Value != "HR")) //(Hddfld_in_out_plan.Value == "OP")
        {
            if (_func_dmu.zStringToInt(signup) > 0)
            {
                text = "<span class='statusmaster-online' data-toggle='tooltip' title='ลงชื่อเข้าอบรม วันที " + signup_date + "'><i class='glyphicon glyphicon-ok'></i></span>";
            }
            else
            {
                text = "<span class='' data-toggle='tooltip' title='รอลงชื่อเข้าอบรม'><i class='fa fa-hourglass-start'></i></span>";
            }
        }


        return text;
    }

    public string getTextEmp_del(string register_status)
    {
        string text = string.Empty;
        int id = 0;

        if ((_func_dmu.zStringToInt(register_status) == 1))
        {
            text = "<span class=''><i class='glyphicon glyphicon-minus'></i></span>";
            //"<span style='color:#26A65B;'> ลงทะเบียนเสร็จแล้ว วันที " + register_date + " </span>";
        }
        else
        {
            // text = "<span class='' data-toggle='tooltip' title='รอลงทะเบียน'><i class='fa fa-hourglass-start'></i></span>";
            text = "";
        }


        return text;
    }
    private void zShowdata_test_scores(int id)
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.idx = id;
        obj.operation_status_id = "TRN-PLAN-COURSE-EMP-UPDATESCORES";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(FvEdit_test_scores, dataelearning.el_training_course_action);
        _txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        TextBox txttest_scores_total = (TextBox)FvEdit_test_scores.FindControl("txttest_scores_total");

        if (dataelearning.el_training_course_action != null)
        {
            txttest_scores_total.Text = _txtcourse_score.Text;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_scores();", true);
        }

    }
    private void zSave_test_scores(int id)
    {
        int idx;
        if (id == 0)
        {
            return;
        }

        TextBox txttest_scores = (TextBox)FvEdit_test_scores.FindControl("txttest_scores");
        TextBox txtdetailApprove = (TextBox)FvEdit_test_scores.FindControl("txtdetailApprove");
        TextBox txtu3_training_course_idx = (TextBox)FvEdit_test_scores.FindControl("txtu3_training_course_idx");
        TextBox txtu0_training_course_idx_ref = (TextBox)FvEdit_test_scores.FindControl("txtu0_training_course_idx_ref");
        _txtcourse_score = (TextBox)fvCRUD.FindControl("txtcourse_score");
        _txtscore_through_per = (TextBox)fvCRUD.FindControl("txtscore_through_per");
        _txtcourse_assessment = (TextBox)fvCRUD.FindControl("txtcourse_assessment");

        if (_func_dmu.zStringToInt(txttest_scores.Text) <= _func_dmu.zStringToInt(_txtcourse_score.Text))
        {



            training_course obj_training_course = new training_course();
            //traning_req U1
            idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
            _data_elearning.el_training_course_action = new training_course[1];
            obj_training_course.training_course_updated_by = emp_idx;
            obj_training_course.u0_training_course_idx = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
            obj_training_course.u3_training_course_idx = _func_dmu.zStringToInt(txtu3_training_course_idx.Text);
            obj_training_course.emp_idx_ref = emp_idx;
            obj_training_course.operation_status_id = "APPROVE-UPDATESCORES-HR";
            if (txtdetailApprove.Text.Trim().Length > 150)
            {
                obj_training_course.signup_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
            }
            else
            {
                obj_training_course.signup_remark = txtdetailApprove.Text.Trim();
            }

            obj_training_course.test_scores = _func_dmu.zStringToDecimal(txttest_scores.Text);
            obj_training_course.test_scores_status = 1;
            obj_training_course.test_scores_user = emp_idx;


            obj_training_course.approve_status = 2;
            obj_training_course.u0_idx = 36;
            obj_training_course.node_idx = 17;
            obj_training_course.actor_idx = 1;
            obj_training_course.app_flag = 1;
            obj_training_course.app_user = emp_idx;
            obj_training_course.approve_remark = obj_training_course.signup_remark;

            //คำนวน Gard 
            decimal _pass_per = 0, _total = 0, _scores = 0, _per = 0;
            int istatus = 0;
            _pass_per = _func_dmu.zStringToDecimal(_txtscore_through_per.Text);
            _total = _func_dmu.zStringToDecimal(_txtcourse_score.Text);
            _scores = _func_dmu.zStringToDecimal(txttest_scores.Text);
            _per = (_scores / _total) * 100;
            _per = Math.Round(_per, 2);
            if (_func_dmu.zStringToInt(_txtcourse_assessment.Text) > 0)
            {
                if (_per >= _pass_per)
                {
                    istatus = 1;
                }
                else
                {
                    istatus = 2;
                }
            }
            else
            {
                istatus = 6;
            }

            obj_training_course.grade_status = istatus;
            obj_training_course.grade_user = emp_idx;
            obj_training_course.grade_avg = _per;

            _data_elearning.el_training_course_action[0] = obj_training_course;
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);
            zShowData_Employee(idx);

        }

    }
    private void zShowdata_EmpResulte(int id)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx_ref = id;
        obj.operation_status_id = "U7-LIST-RESULTE";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(Gvemp_resulte, dataelearning.el_training_course_action);

        if (dataelearning.el_training_course_action != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_emp_resulte();", true);
        }

    }
    private void zSave_EmpResulte()
    {

        int iemp_idx_ref = 0, itemObj = 0, iAction = 0
            , iu0_training_course_idx_ref = 0
            , iu7_training_course_idx = 0;
        data_elearning dataelearning = new data_elearning();
        training_course[] obj_tr_u7 = new training_course[Gvemp_resulte.Rows.Count];

        for (int j = 0; j < Gvemp_resulte.Rows.Count; j++)
        {
            iemp_idx_ref = _func_dmu.zStringToInt(((Label)Gvemp_resulte.Rows[j].FindControl("txtemp_zemp_idx_ref_item_L")).Text);
            iu0_training_course_idx_ref = _func_dmu.zStringToInt(((Label)Gvemp_resulte.Rows[j].FindControl("lbemp_u0_training_course_idx_ref")).Text);
            iu7_training_course_idx = _func_dmu.zStringToInt(((Label)Gvemp_resulte.Rows[j].FindControl("lbemp_u7_training_course_idx")).Text);
            iAction = _func_dmu.zStringToInt(((DropDownList)Gvemp_resulte.Rows[j].FindControl("cbemp_sel_item_L")).SelectedValue);
            string sRemark = ((TextBox)Gvemp_resulte.Rows[j].FindControl("txtemp_zRemark_item_L")).Text;
            obj_tr_u7[itemObj] = new training_course();
            obj_tr_u7[itemObj].u0_training_course_idx_ref = iu0_training_course_idx_ref;
            obj_tr_u7[itemObj].u7_training_course_idx = iu7_training_course_idx;
            obj_tr_u7[itemObj].emp_idx_ref = iemp_idx_ref;
            obj_tr_u7[itemObj].resulte_app_status = iAction;
            obj_tr_u7[itemObj].resulte_app_user = emp_idx;
            obj_tr_u7[itemObj].operation_status_id = "U7";
            obj_tr_u7[itemObj].zstatus = "SUPER";
            obj_tr_u7[itemObj].remark = sRemark;
            obj_tr_u7[itemObj].training_course_updated_by = emp_idx;

            obj_tr_u7[itemObj].approve_status = iAction;
            if (iAction == 4)
            {
                obj_tr_u7[itemObj].u0_idx = 30;
            }
            else
            {
                obj_tr_u7[itemObj].u0_idx = 31;
            }
            obj_tr_u7[itemObj].node_idx = 2;
            obj_tr_u7[itemObj].actor_idx = 1;
            obj_tr_u7[itemObj].app_flag = 0;
            obj_tr_u7[itemObj].app_user = emp_idx;


            itemObj++;
        }
        dataelearning.el_training_course_action = obj_tr_u7;
        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        zShowData_Employee(iu0_training_course_idx_ref);

        dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.u0_training_course_idx_ref = iu0_training_course_idx_ref;
        obj.operation_status_id = "U7-LIST-RESULTE";
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(Gvemp_resulte, dataelearning.el_training_course_action);
        _pnladd_emp_resulte = (Panel)_FormView.FindControl("pnladd_emp_resulte");
        if (dataelearning.el_training_course_action != null)
        {
            _pnladd_emp_resulte.Visible = true;
        }
        else
        {
            _pnladd_emp_resulte.Visible = false;
        }
    }
    protected string getStatus_Resulte(int status)
    {

        if (status > 0)
        {

            return "<span data-toggle='tooltip' title='มีผู้ขอสิทธิ์เข้าคอร์สอบรมจำนวน " + status.ToString() + " คน '><i class='glyphicon glyphicon-user'></i></span>";
        }
        else
        {
            return "";
        }
    }
    public Boolean getvalue_compare(string value, int value_compare)
    {

        Boolean _Boolean = false;
        int iregister = _func_dmu.zStringToInt(value);
        if ((iregister == value_compare) || (Hddfld_mode.Value == "P") || (Hddfld_permission.Value != "HR")) //(Hddfld_in_out_plan.Value == "OP")
        {
            _Boolean = false;
        }
        else
        {
            _Boolean = true;

        }

        return _Boolean;
    }

    private void ShowListsummary() //รายการที่ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_summary.Text;
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_summary.SelectedValue);
        obj.idx = _func_dmu.zStringToInt(ddlSummary.SelectedValue);
        obj.zstatus = "HR";
        // obj.RDeptID_ref = _func_dmu.zStringToInt(ViewState["RDeptID"].ToString());
        // obj.RSecID = _func_dmu.zStringToInt(ViewState["RSecID"].ToString());
        obj.operation_status_id = "U6-LISTDATA-SUMMARY";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvsummaryList, dataelearning.el_training_course_action);
        ShowHRsummary();
    }
    private void ShowHRsummary()
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.emp_idx_ref = emp_idx;
        obj.zstatus = "HR-COUNT";
        obj.operation_status_id = "U6-SUMMARY-HR-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        btnsummary.Text = "ผลการฝึกอบรม <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

    }
    private void zShowdata_SummaryDetail(int id_u0, int id_u6, string _zstatus)
    {
        btnSave_summary.Visible = true;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.emp_idx_ref = emp_idx;
        obj.u0_training_course_idx = id_u0;
        obj.operation_status_id = "U0-LISTDATA-REGISTER";
        obj.zstatus = _zstatus;
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fv_summarycourse, dataelearning.el_training_course_action);

        //End SetMode
        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        if (dataelearning.el_training_course_action != null)
        {
            foreach (var item in dataelearning.el_training_course_action)
            {
                Hddfld_training_course_no.Value = item.training_course_no;
                Hddfld_u0_training_course_idx.Value = item.u0_training_course_idx.ToString();
                // Panel pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                data_elearning dataelearning_u6 = new data_elearning();
                dataelearning_u6.el_training_course_action = new training_course[1];
                training_course obj_u6 = new training_course();
                obj_u6.u6_training_course_idx = id_u6;
                obj_u6.operation_status_id = "U6-FULL";
                obj_u6.zstatus = _zstatus;
                dataelearning_u6.el_training_course_action[0] = obj_u6;
                dataelearning_u6 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_u6);
                fv_summaryLearner.ChangeMode(FormViewMode.Edit);
                _func_dmu.zSetFormViewData(fv_summaryLearner, dataelearning_u6.el_training_course_action);

            }
        }

    }
    private Boolean zSave_summary()
    {
        int idx;
        Boolean _Boolean = false;
        TextBox txtu6_training_course_idx = (TextBox)fv_summaryLearner.FindControl("txtu6_training_course_idx");
        TextBox txtu0_training_course_idx_ref = (TextBox)fv_summaryLearner.FindControl("txtu0_training_course_idx_ref");
        TextBox txtu3_training_course_idx = (TextBox)fv_summaryLearner.FindControl("txtu3_training_course_idx");
        TextBox txtsumm_hr_remark = (TextBox)fv_summaryLearner.FindControl("txtsumm_hr_remark");

        idx = _func_dmu.zStringToInt(txtu6_training_course_idx.Text);
        training_course obj_training_course = new training_course();
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.u6_training_course_idx = idx;
        obj_training_course.u0_training_course_idx_ref = _func_dmu.zStringToInt(txtu0_training_course_idx_ref.Text);
        obj_training_course.u3_training_course_idx_ref = _func_dmu.zStringToInt(txtu3_training_course_idx.Text);
        obj_training_course.training_course_status = 1;
        obj_training_course.summ_hr_remark = txtsumm_hr_remark.Text;
        obj_training_course.zstatus = "HR";
        obj_training_course.summ_hr_user = emp_idx;
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.operation_status_id = "U6";

        obj_training_course.approve_status = 4;
        obj_training_course.u0_idx = 38;
        obj_training_course.node_idx = 2;
        obj_training_course.actor_idx = 1;
        obj_training_course.app_flag = 1;
        obj_training_course.app_user = emp_idx;
        obj_training_course.approve_remark = "";

        _data_elearning.el_training_course_action[0] = obj_training_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, _data_elearning);

        // idx = _data_elearning.return_code;
        _Boolean = true;
        return _Boolean;
    }

    /** start qr code **/
    public void zQrCode(int id)
    {

    }
    /** end qr code **/

    /** start out plan **/
    public string getTilteCourse()
    {
        string sStr = "";
        if (Hddfld_in_out_plan.Value == "OP")
        {
            sStr = "ข้อมูลคอร์สอบรม (Out Plan)";
            setObjectMode(false);


        }
        else
        {
            sStr = "ข้อมูลคอร์สอบรม (In Plan)";
            setObjectMode(true);
        }
        return sStr;
    }

    public void setObject()
    {
        _pnlcourse_plan_status = (Panel)fvCRUD.FindControl("pnlcourse_plan_status");
        _pnlcourse_outplan_status = (Panel)fvCRUD.FindControl("pnlcourse_outplan_status");
        _pnlcourse_outplan_status2 = (Panel)fvCRUD.FindControl("pnlcourse_outplan_status2");
        _pnlcourse_plan_status1 = (Panel)fvCRUD.FindControl("pnlcourse_plan_status1");
        _rqf_ddlu0_training_plan_idx_ref = (RequiredFieldValidator)fvCRUD.FindControl("rqf_ddlu0_training_plan_idx_ref");
        _rqf_txtcourse_name = (RequiredFieldValidator)fvCRUD.FindControl("rqf_txtcourse_name");
        _rqf_ddm0_training_group_idx_ref = (RequiredFieldValidator)fvCRUD.FindControl("rqf_ddm0_training_group_idx_ref");
        _rqf_ddm0_training_branch_idx = (RequiredFieldValidator)fvCRUD.FindControl("rqf_ddm0_training_branch_idx");
        _ddlm0_training_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_training_group_idx_ref");
        _ddm0_training_branch_idx = (DropDownList)fvCRUD.FindControl("ddm0_training_branch_idx");
        _txtcourse_name = (TextBox)fvCRUD.FindControl("txtcourse_name");
        _rdolecturer_type_place = (RadioButtonList)fvCRUD.FindControl("rdolecturer_type_place");
        _ddlm0_target_group_idx_ref = (DropDownList)fvCRUD.FindControl("ddlm0_target_group_idx_ref");
        _rqf_ddlm0_target_group_idx_ref = (RequiredFieldValidator)fvCRUD.FindControl("rqf_ddlm0_target_group_idx_ref");

    }
    public void setRequiredFieldValidator(RequiredFieldValidator Requiredfv,
                                          string sValidationGroup,
                                          string sControlToValidate,
                                          Boolean mode)
    {
        if (mode == true)
        {
            Requiredfv.ValidationGroup = sValidationGroup;
            Requiredfv.ControlToValidate = sControlToValidate;

        }
        else
        {
            Requiredfv.ValidationGroup = "";
            Requiredfv.ControlToValidate = "";
        }
    }
    public void setObjectMode(Boolean mode)
    {
        setObject();
        Boolean notmode = false;
        if (mode == true)
        {
            notmode = false;
        }
        else
        {
            notmode = true;
        }
        _pnlcourse_plan_status1.Visible = false;
        //_pnlcourse_outplan_status2.Visible = false;
        if (Hddfld_training_course_no.Value != "")
        {
            notmode = false;
            mode = true;
            if (Hddfld_in_out_plan.Value == "OP")
            {
                _pnlcourse_plan_status1.Visible = false;
            }
            if ((Hddfld_in_out_plan.Value == "OP") &&
                 (Hddfld_permission.Value != "USER"))
            {
                _pnlcourse_plan_status1.Visible = false; //true
            }
        }
        else
        {
            _pnlcourse_plan_status1.Visible = false;
        }
        if (Hddfld_in_out_plan.Value == "OP")
        {
            _pnlcourse_outplan_status2.Visible = true;
            setRequiredFieldValidator(_rqf_ddlm0_target_group_idx_ref,
                                  "btnSaveInsert",
                                  "ddlm0_target_group_idx_ref",
                                   true);

        }
        else
        {
            _pnlcourse_outplan_status2.Visible = true;

            setRequiredFieldValidator(_rqf_ddlm0_target_group_idx_ref,
                                  "btnSaveInsert",
                                  "ddlm0_target_group_idx_ref",
                                   true);
        }
        _pnlcourse_plan_status.Visible = mode;
        _pnlcourse_outplan_status.Visible = notmode;

        setRequiredFieldValidator(_rqf_ddlu0_training_plan_idx_ref,
                                  "btnSaveInsert",
                                  "ddlu0_training_plan_idx_ref",
                                   mode);

        setRequiredFieldValidator(_rqf_txtcourse_name,
                                  "btnSaveInsert",
                                  "txtcourse_name",
                                   notmode);
        setRequiredFieldValidator(_rqf_ddm0_training_group_idx_ref,
                                  "btnSaveInsert",
                                  "ddlm0_training_group_idx_ref",
                                   notmode);
        setRequiredFieldValidator(_rqf_ddm0_training_branch_idx,
                                  "btnSaveInsert",
                                  "ddm0_training_branch_idx",
                                   notmode);


    }
    public void setplace()
    {
        _ddlplace_idx_ref = (DropDownList)fvCRUD.FindControl("ddlplace_idx_ref");
        _rdolecturer_type_place = (RadioButtonList)fvCRUD.FindControl("rdolecturer_type_place");
        _rqf_place_idx_ref = (RequiredFieldValidator)fvCRUD.FindControl("rqf_place_idx_ref");
        _pnlplace = (Panel)fvCRUD.FindControl("pnlplace");
        //setRequiredFieldValidator(_rqf_place_idx_ref,
        //"btnSaveInsert",
        //"ddlplace_idx_ref",
        // true);
        //if ((Hddfld_in_out_plan.Value == "OP"))
        //{
        //    if (_rdolecturer_type_place.SelectedValue == "0")
        //    {
        //        //_pnlplace.Visible = false;
        //    }
        //    else
        //    {
        //        _pnlplace.Visible = false;
        //        //setRequiredFieldValidator(_rqf_place_idx_ref,
        //        //                  "btnSaveInsert",
        //        //                  "ddlplace_idx_ref",
        //        //                   false);
        //    }
        //}
        //else
        //{
        //    _rdolecturer_type_place.Visible = false;
        //    _pnlplace.Visible = false;
        //}

    }
    public int genSyllabusAuto()
    {
        setObject();
        int _id = 0;
        if (_txtcourse_name.Text != "")
        {
            string m0_prefix = "", _docno = ""
                , FromcourseRunNo = "u_course"
                ;

            m0_prefix = getbranchcode(int.Parse(_ddlm0_training_group_idx_ref.SelectedValue));
            if (m0_prefix == "")
            {
                showAlert("รหัสกลุ่มวิชาไม่ถูกต้อง");
            }
            else
            {
                int u0_course_idx_ref = 0;
                course obj_course = new course();
                _data_elearning.el_course_action = new course[1];
                //obj_course.course_no = _func_dmu.zRun_Number(FromcourseRunNo, m0_prefix, "YYYY-COURSE", "N", "0000");
                //_docno = obj_course.course_no;

                obj_course.course_date = _func_dmu.zDateToDB(_txttraining_course_date.Text);
                obj_course.m0_training_group_idx_ref = int.Parse(_ddlm0_training_group_idx_ref.SelectedValue);
                obj_course.m0_training_branch_idx_ref = int.Parse(_ddm0_training_branch_idx.SelectedValue);
                obj_course.course_name = _txtcourse_name.Text;
                obj_course.course_remark = _txttraining_course_description.Text;
                obj_course.level_code = 1;
                obj_course.course_priority = 2; //Need
                obj_course.course_status = 1;
                obj_course.course_assessment = 1;
                obj_course.RDeptID_ref = _func_dmu.zStringToInt(ViewState["rdept_idx"].ToString());
                obj_course.io_page_flag = "OP";
                obj_course.course_score = _func_dmu.zStringToInt(_txtcourse_score.Text);
                obj_course.score_through_per = _func_dmu.zStringToDecimal(_txtscore_through_per.Text);


                obj_course.course_created_by = emp_idx;
                obj_course.course_updated_by = emp_idx;
                obj_course.course_type_etraining = 1;
                obj_course.course_type_elearning = 0;
                obj_course.course_plan_status = "O";

                //node
                obj_course.approve_status = 0;
                obj_course.u0_idx = 5;
                obj_course.node_idx = 9;
                obj_course.actor_idx = 1;
                obj_course.app_flag = 0;
                obj_course.app_user = 0;

                obj_course.operation_status_id = "U0";
                _data_elearning.el_course_action[0] = obj_course;
                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_course, _data_elearning);
                u0_course_idx_ref = _data_elearning.return_code;

                _id = u0_course_idx_ref;
            }

        }
        return _id;
    }
    private string getbranchcode(int id)
    {
        string sreturn = "";
        data_elearning dataelearning = new data_elearning();
        dataelearning.trainingLoolup_action = new trainingLoolup[1];
        trainingLoolup obj = new trainingLoolup();
        obj.idx = id;
        obj.operation_status_id = "TRN-GROUP";
        dataelearning.trainingLoolup_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning);
        if (dataelearning.trainingLoolup_action != null)
        {
            obj = dataelearning.trainingLoolup_action[0];
            sreturn = obj.doc_no;
        }

        return sreturn;
    }

    private void zSaveApproveLeader(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove_leader");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove_leader");

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.operation_status_id = "APPROVE-LEADER";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.super_app_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.super_app_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_course.u0_idx = 26;
            obj_training_course.super_u0_idx = 26;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_course.u0_idx = 27;
            obj_training_course.super_u0_idx = 27;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_course.u0_idx = 28;
            obj_training_course.super_u0_idx = 28;
        }
        obj_training_course.super_app_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_course.node_idx = 2;
        obj_training_course.actor_idx = 3;

        obj_training_course.super_node_idx = 2;
        obj_training_course.super_actor_idx = 3;

        obj_training_course.super_app_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = _func_dmu.zJson(_urlSetUpdel_u_plan_course, _data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);

        if (ddlStatusapprove.SelectedValue == "4")
        {
            sendEmailleadertohr(id);
        }
        sendEmailleadertouser_all(id);


    }

    private void ShowListData_WApp_Leader(string status)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_Leader.Text;
        obj.zyear = int.Parse(ddlYearSearch_Leader.SelectedValue);
        if (status != "LEADER_W")
        {
            obj.approve_status = int.Parse(ddlStatusapprove_Leader.SelectedValue);
        }
        obj.operation_status_id = "U0-LISTDATA";
        obj.zstatus = status;
        obj.emp_idx_ref = emp_idx;
        obj.JobLevel = _func_dmu.zStringToInt(ViewState["joblevel_idx"].ToString());
        obj.RSecID = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());

        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        _func_dmu.zSetGridData(GvListDataLeader, dataelearning.el_training_course_action);
        ShowLeaderW();
    }


    /** end out plan **/
    public void setModeApprove(string status, int _md_app, int ihr_status)
    {
        Panel pnlDetailApp;
        Panel pnlapprove_hr;
        Panel pnlApprove_HR_W;
        Panel pnlApprove_HR_A;
        Label lbtitle_preview;

        Panel pnlapprove_md;
        Panel pnlApprove_MD_W;
        Panel pnlApprove_MD_A;

        Panel pnlapprove_leader;
        Panel pnlApprove_leader_W;
        Panel pnlApprove_leader_A;

        pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
        pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
        pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
        lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

        pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
        pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

        pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
        pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");

        pnlapprove_leader = (Panel)fv_preview.FindControl("pnlapprove_leader");
        pnlApprove_leader_W = (Panel)fv_preview.FindControl("pnlApprove_leader_W");
        pnlApprove_leader_A = (Panel)fv_preview.FindControl("pnlApprove_leader_A");

        pnlDetailApp.Visible = _func_dmu.zIntToBoolean(_md_app);
        pnlApprove_HR_W.Visible = false;
        pnlApprove_HR_A.Visible = false;

        pnlApprove_MD_W.Visible = false;
        pnlApprove_MD_A.Visible = false;

        pnlapprove_hr.Visible = false;
        pnlapprove_md.Visible = false;

        pnlapprove_leader.Visible = false;
        pnlApprove_leader_W.Visible = false;
        pnlApprove_leader_A.Visible = false;

        if (status == "btnDetail_GvHR_WList")
        {
            pnlApprove_HR_W.Visible = true;
            pnlapprove_hr.Visible = true;
            lbtitle_preview.Text = lbHR_Wait.Text;
        }
        else if (status == "btnDetail_GvHR_AList")
        {
            pnlApprove_HR_A.Visible = true;
            pnlapprove_hr.Visible = true;
            lbtitle_preview.Text = lbHR_App.Text;
        }
        else if (status == "btnDetail_GvMD_WList")
        {
            pnlDetailApp.Visible = true;
            pnlApprove_MD_W.Visible = true;
            pnlapprove_md.Visible = true;
            lbtitle_preview.Text = lbMD_Wait.Text;
        }
        else if (status == "btnDetail_GvMD_AList")
        {
            pnlDetailApp.Visible = true;
            pnlApprove_MD_A.Visible = true;
            pnlapprove_md.Visible = true;
            lbtitle_preview.Text = lbMD_App.Text;
        }
        else if (status == "btnDetailWaitApprove")
        {
            pnlapprove_leader.Visible = true;
            pnlApprove_leader_W.Visible = true;
            lbtitle_preview.Text = lbLeaderList.Text;
            if (ihr_status == 1)
            {
                pnlDetailApp.Visible = false;
            }
        }
        else if (status == "liLeaderApproveList")
        {
            pnlapprove_leader.Visible = true;
            pnlApprove_leader_A.Visible = true;
            lbtitle_preview.Text = lbLeaderApproveList.Text;
            if (ihr_status == 1)
            {
                pnlDetailApp.Visible = false;
            }
        }

    }
    public string getTextAction(int iapprove_status, string actor_name)
    {
        string text = string.Empty;
        if (iapprove_status == 4)
        {
            text = "<span style='font-size:13px;'>" + actor_name + "</span>";//color:#26A65B;
        }
        else if (iapprove_status == 5)
        {
            text = "<span style='font-size:13px;'>" + actor_name + "</span>";//color:#F89406;
        }
        else if (iapprove_status == 6)
        {
            text = "<span style='font-size:13px;'>" + actor_name + "</span>"; //color:#F03434;
        }
        else
        {
            text = "<span style='font-size:13px;'>" + actor_name + "</span>";
        }

        return text;
    }
    private void sendEmailleadertohr(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {
            data_elearning dataelearning = new data_elearning();
            trainingLoolup obj_Loolup = new trainingLoolup();
            dataelearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-MD-TO-HRD-ALL";
            dataelearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _funcTool.convertObjectToJson(dataelearning);
            //_func_dmu.zJson(_urlsendemail_outplan_leadertohr, dataelearning);
            _func_dmu.zCallServicePostNetwork(_urlsendemail_outplan_leadertohr, dataelearning);
        }
        catch { }

    }
    private void sendEmailleadertouser_all(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {

            data_elearning dataelearning = new data_elearning();
            trainingLoolup obj_Loolup = new trainingLoolup();
            dataelearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "E-MAIL-LEADER-TO-USER-ALL-COURSE-OP";
            dataelearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendemail_outplan_leadertouser_all, dataelearning);
        }
        catch { }
    }
    private void CreateDs_el_u8_training_course_date()
    {
        string sDs = "dsel_u8_training_course_date";
        string sVs = "vsel_u8_training_course_date";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u6_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_date_start", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_date_end", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_date_qty", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zdate", typeof(String));
        ds.Tables[sDs].Columns.Add("ztime_start", typeof(String));
        ds.Tables[sDs].Columns.Add("ztime_end", typeof(String));
        ViewState[sVs] = ds;

    }
    private void setTrainingDateDefault()
    {
        _Gvu8trncoursedate = (GridView)fvCRUD.FindControl("Gvu8trncoursedate");
        CreateDs_el_u8_training_course_date();
        _func_dmu.zSetGridData(_Gvemp, ViewState["vsel_u8_training_course_date"]);

    }
    public void add_TrainingDate()
    {
        _txtdatestart_create = (TextBox)fvCRUD.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)fvCRUD.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)fvCRUD.FindControl("txt_timeend_create");
        _txttraining_course_date_qty = (TextBox)fvCRUD.FindControl("txttraining_course_date_qty");
        int item = 0;

        if (_txtdatestart_create.Text.Trim() == "")
        {
            item++;
            showAlert("กรุณากรอกวันที่อบรม");

        }
        else if (_txt_timestart_create.Text.Trim() == "")
        {
            item++;
            showAlert("กรุณากรอกเวลาที่เริ่มอบรม");
        }
        else if (_txt_timeend_create.Text.Trim() == "")
        {
            item++;
            showAlert("กรุณากรอกเวลาที่สิ้นสุดการอบรม");
        }
        else if (_func_dmu.zStringToDecimal(_txttraining_course_date_qty.Text) <= 0)
        {
            item++;
            showAlert("กรุณากรอกรวมชม.ที่เรียนให้ถูกต้อง");
        }
        if (item == 0)
        {
            string sdate_start = _func_dmu.zDateToDB(_txtdatestart_create.Text.Trim()) + " " + _txt_timestart_create.Text.Trim();
            DateTime DTdate_start = DateTime.Parse(sdate_start);
            string sdate_end = _func_dmu.zDateToDB(_txtdatestart_create.Text.Trim()) + " " + _txt_timeend_create.Text.Trim();
            DateTime DTdate_end = DateTime.Parse(sdate_end);
            if (DTdate_start >= DTdate_end)
            {
                item++;
                showAlert("เวลาที่เริ่มและเวลาที่สิ้นสุดการอบรมไม่ถูกต้อง");
            }
        }
        if (item == 0)
        {
            add_TrainingDate_data();
        }
    }
    private void add_TrainingDate_data()
    {
        int i = 0;
        _txtdatestart_create = (TextBox)fvCRUD.FindControl("txtdatestart_create");
        _txt_timestart_create = (TextBox)fvCRUD.FindControl("txt_timestart_create");
        _txt_timeend_create = (TextBox)fvCRUD.FindControl("txt_timeend_create");
        _txttraining_course_date_qty = (TextBox)fvCRUD.FindControl("txttraining_course_date_qty");
        _Gvu8trncoursedate = (GridView)fvCRUD.FindControl("Gvu8trncoursedate");

        DataSet ds = (DataSet)ViewState["vsel_u8_training_course_date"];
        foreach (DataRow item in ds.Tables["dsel_u8_training_course_date"].Rows)
        {
            if (
                (item["zdate"].ToString().Trim() == _txtdatestart_create.Text.Trim())
                )
            {
                i++;
            }
        }
        if (i > 0)
        {
            showAlert("วันที่อบรมนี้มีอยู่แล้วกรุณากรอกใหม่");
            _txtdatestart_create.Focus();
        }
        else
        {
            DataRow dr;

            dr = ds.Tables["dsel_u8_training_course_date"].NewRow();
            dr["zdate"] = _txtdatestart_create.Text;
            dr["ztime_start"] = _txt_timestart_create.Text;
            dr["ztime_end"] = _txt_timeend_create.Text;
            dr["training_course_date_qty"] = _func_dmu.zStringToInt(_txttraining_course_date_qty.Text).ToString();
            ds.Tables["dsel_u8_training_course_date"].Rows.Add(dr);
            ViewState["vsel_u8_training_course_date"] = ds;
            _func_dmu.zSetGridData(_Gvu8trncoursedate, ds.Tables["dsel_u8_training_course_date"]);

            _txtdatestart_create.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
            _txt_timestart_create.Text = "09.00";
            _txt_timeend_create.Text = "16.00";
            _txttraining_course_date_qty.Text = "6";
        }
    }
    private void ShowLeaderW()
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.emp_idx_ref = emp_idx;
        obj.RSecID = _func_dmu.zStringToInt(ViewState["rsec_idx"].ToString());
        obj.operation_status_id = "U0-LEADER-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        btnLeaderList.Text = lbLeaderList.Text + " <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

    }

    private void CreateDs_el_u9_trncourse_emp_signup()
    {
        string sDs = "dsel_u9_training_course_employee_signup";
        string sVs = "vsel_u9_training_course_employee_signup";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u9_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u3_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u8_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_date_start", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_date_end", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_status", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_user", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_date", typeof(String));
        ds.Tables[sDs].Columns.Add("signup_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("signout_status", typeof(String));
        ds.Tables[sDs].Columns.Add("signout_user", typeof(String));
        ds.Tables[sDs].Columns.Add("signout_date", typeof(String));
        ds.Tables[sDs].Columns.Add("signout_remark", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_at", typeof(String));
        ds.Tables[sDs].Columns.Add("zdate", typeof(String));
        ds.Tables[sDs].Columns.Add("ztime_start", typeof(String));
        ds.Tables[sDs].Columns.Add("ztime_end", typeof(String));
        ViewState[sVs] = ds;

    }
    private void setU8StartEndDatetime(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        //obj_detail.EmpIDX = emp_idx;
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U8-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.el_training_course_action);

    }

    private void genQrCode_Test(int _id, string _docno)
    {
        if (_id == 0)
        {
            return;
        }

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

        string newFilePath = "";
        newFilePath = _PathFile + _folder_plan_course_qrcode + "/" + _docno + "/";
        try
        {
            Directory.CreateDirectory(Server.MapPath(newFilePath));
        }
        catch { }
        data_elearning dataelearning_detail;
        training_course obj_detail;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.operation_status_id = "U0-FULL-QRCODE";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        string _qrcode_test = "", _qrcode_evaluationform = "";
        foreach (var item in dataelearning_detail.el_training_course_action)
        {
            _qrcode_test = item.qrcode_test;
            _qrcode_evaluationform = item.qrcode_evaluationform;
        }
        if (_qrcode_evaluationform == null)
        {
            _qrcode_evaluationform = "";
        }
        if (_qrcode_test == null)
        {
            _qrcode_test = "";
        }
        if (_qrcode_evaluationform == "")
        {
            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "evaluationform";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
            if (dataelearning_detail.el_training_course_action == null)
            {
                _qrcode_evaluationform = "ไม่พบข้อมูลแบบประเมิน";
            }
        }
        if (_qrcode_evaluationform == "")
        {
            string txtGenQrCode = _func_dmu._LikeGenQrCode_evaluation + _funcTool.getEncryptRC4(_id.ToString(), "id");
            string getPathimages = newFilePath;
            string fileNameimage = _docno + "_qrcode_evaluationform"; // ชื่อรูป QRCode
            string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

            QRCodeEncoder encoder = new QRCodeEncoder();
            Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

            bi.Save(Server.MapPath(getPathimages + fileNameimage + ".jpg"), ImageFormat.Jpeg);

            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.qrcode_evaluationform = fileNameimage + ".jpg";
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "U0-QRCODE-EVALUATION";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, dataelearning_detail);

            _qrcode_evaluationform = fileNameimage + ".jpg";
        }
        if (_qrcode_test == "")
        {
            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "course_test";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
            if (dataelearning_detail.el_training_course_action == null)
            {
                _qrcode_test = "ไม่พบข้อมูลแบบทดสอบ";
            }
        }
        if (_qrcode_test == "")
        {
            string txtGenQrCode = _func_dmu._LikeGenQrCode_test + _funcTool.getEncryptRC4(_id.ToString(), "id");
            string getPathimages = newFilePath;
            string fileNameimage = _docno + "_qrcode_test"; // ชื่อรูป QRCode
            string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

            QRCodeEncoder encoder = new QRCodeEncoder();
            Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

            bi.Save(Server.MapPath(getPathimages + fileNameimage + ".jpg"), ImageFormat.Jpeg);

            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.qrcode_test = fileNameimage + ".jpg";
            obj_detail.u0_training_course_idx = _id;
            obj_detail.operation_status_id = "U0-QRCODE-TEST";
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, dataelearning_detail);

            _qrcode_test = fileNameimage + ".jpg";
        }

        if ((_qrcode_evaluationform != "") || (_qrcode_test != ""))
        {
            string URL = ResolveClientUrl(ResolveUrl("~/el_TrnCourse_qrcode/"));
            URL = URL + _funcTool.getEncryptRC4(_id.ToString(), "id");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "show window",
            "shwwindow('" + URL + "');", true);

        }

    }

    protected void Button1_Click(object sender, EventArgs e)

    {

        Response.Redirect("page2.aspx");

    }

    public string getImageIO(string _img, int _i)
    {
        string SetName = "";
        if (_i == 1)
        {
            if (_img == "I")
            {
                SetName = ResolveUrl("~/images/elearning/inplan.png");
            }
            else if (_img == "O")
            {
                SetName = ResolveUrl("~/images/elearning/outplan.png");
            }
        }
        else
        {
            if (_img == "I")
            {
                SetName = "In Plan";
            }
            else if (_img == "O")
            {
                SetName = "Out Plan";
            }
        }

        return SetName;
    }

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)fvCRUD.FindControl("gvFile");
            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
            checkfile = "0";
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

}