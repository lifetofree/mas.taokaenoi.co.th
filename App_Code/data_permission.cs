﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_permission")]
public class data_permission
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }


    [XmlElement("permission_list")]
    public permission_detail[] permission_list { get; set; }
    [XmlElement("system_list")]
    public system_detail[] system_list { get; set; }
    [XmlElement("role_list")]
    public role_detail[] role_list { get; set; }
    [XmlElement("systemall_list")]
    public systemall_detail[] systemall_list { get; set; }
    [XmlElement("searchindex_list")]
    public searchindex_detail[] searchindex_list { get; set; }
    [XmlElement("systemrole_list")]
    public systemrole_detail[] systemrole_list { get; set; }
}

[Serializable]
public class permission_detail
{
    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]  
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]  
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]   
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]    
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]  
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]   
    public string SecNameEN { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }

    [XmlElement("PosNameTH")]   
    public string PosNameTH { get; set; }

    [XmlElement("PosNameEN")]
    public string PosNameEN { get; set; }

    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }

    //[XmlElement("EmpIDX")]
    //public int EmpIDX { get; set; }

    [XmlElement("EmpIDXUser")]
    public int EmpIDXUser { get; set; }

    [XmlElement("EStatus")]
    public int EStatus { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }

    [XmlElement("JobGradeIDX1")]
    public int JobGradeIDX1 { get; set; }

    [XmlElement("JobGradeIDX2")]
    public int JobGradeIDX2 { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

}

[Serializable]
public class system_detail
{

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_en")]
    public string system_name_en { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

   

}

[Serializable]
public class role_detail
{

    [XmlElement("ods_idx")]
    public int ods_idx { get; set; }

    [XmlElement("role_idx")]
    public int role_idx { get; set; }

    [XmlElement("role_name_th")]
    public string role_name_th { get; set; }

    [XmlElement("role_name_en")]
    public string role_name_en { get; set; }

    [XmlElement("role_status")]
    public int role_status { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_en")]
    public string system_name_en { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("rolepermission_idxstring")]
    public string rolepermission_idxstring { get; set; }


}

[Serializable]
public class systemrole_detail
{

    [XmlElement("ods_idx")]
    public int ods_idx { get; set; }

    [XmlElement("role_idx")]
    public int role_idx { get; set; }

    [XmlElement("role_name_th")]
    public string role_name_th { get; set; }

    [XmlElement("role_name_en")]
    public string role_name_en { get; set; }

    [XmlElement("role_status")]
    public int role_status { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_en")]
    public string system_name_en { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("rolepermission_idxstring")]
    public string rolepermission_idxstring { get; set; }


}

[Serializable]
public class systemall_detail
{

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_en")]
    public string system_name_en { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }
}

[Serializable]
public class searchindex_detail
{

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("PosNameEN")]
    public string PosNameEN { get; set; }

    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }

    //[XmlElement("EmpIDX")]
    //public int EmpIDX { get; set; }

    [XmlElement("EmpIDXUser")]
    public int EmpIDXUser { get; set; }

    [XmlElement("EStatus")]
    public int EStatus { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }

    [XmlElement("JobGradeIDX1")]
    public int JobGradeIDX1 { get; set; }

    [XmlElement("JobGradeIDX2")]
    public int JobGradeIDX2 { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }


}