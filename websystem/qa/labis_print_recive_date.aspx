﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labis_print_recive_date.aspx.cs" Inherits="websystem_qa_labis_print_recive_date" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print Sample Code</title>

    <style type="text/css" media="print,screen">
        @page {
            /*size: A4 landscape;*/
            size: A4 portrait;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>

</head>
<body onload="window.print()">
    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <div>
            <div class="headOrg" style="text-align: center;">
                <strong>Sample list</strong>
            </div>
            <%--    OnRowDataBound="gvRowDataBound"--%>
            <table class="table" style="width: 100%">
                <asp:Repeater ID="rptPrintReciveDate" OnItemDataBound="rptOnRowDataBound" runat="server">
                    <ItemTemplate>
                        <tr style="width: 100%">
                            <asp:Label CssClass="btn btn-default tar" ID="lbPrintSampleCodeU1Doc" data-toggle="tooltip" Visible="false" title="Print" runat="server"
                                Text='<%# Eval("u1_qalab_idx") %>'></asp:Label>

                            <asp:GridView ID="gvPrintReciveDate" runat="server" FooterStyle-Height="0" HeaderStyle-Wrap="true"
                                FooterStyle-Wrap="false" AllowPaging="True" AllowSorting="True" RowStyle-Font-Size="8" AutoGenerateColumns="false" HeaderStyle-Width="5%"
                                BackColor="White" BorderWidth="1px" Width="100%"
                                CellPadding="0" Font-Size="8" OnRowDataBound="gvRowDataBound"
                                PageSize="14">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Qr Code" HeaderStyle-Width="12%">
                                        <ItemTemplate>
                                            <div style="text-align: center">
                                                <asp:Image ID="ImagShowQRCode" Width="50" Height="50" runat="server"></asp:Image>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sample Code" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <div style="text-align: center">
                                                <asp:Label ID="lblsampleCode" Text='<%# Eval("sample_code") %>' runat="server"></asp:Label>
                                                <asp:Label ID="lbPrintU1Doc" Visible="false" Text='<%# Eval("u1_qalab_idx") %>' runat="server"></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Tested Items" HeaderStyle-Width="25%">
                                        <ItemTemplate>
                                            <div style="padding: 1px;">
                                                <table class="table table-striped" style="width: 100%">
                                                    <asp:Repeater ID="rp_test_sample" runat="server">

                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbltestdetailname" runat="server" Text='<%# " - " + Eval("test_detail_name") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="u2_idx_n3" runat="server" visible="false">
                                                                <td>
                                                                    <asp:Label ID="lblu2qalab_idx" runat="server" Text='<%# Eval("u2_qalab_idx") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr id="test_detail_idx_n11" runat="server" visible="false">
                                                                <td>
                                                                    <asp:Label ID="lbltest_detail_idx_n11" runat="server" Text='<%# Eval("test_detail_idx") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>

                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tested Set" HeaderStyle-Width="1%">
                                        <ItemTemplate>
                                            <div style="text-align: center">
                                                <asp:Label ID="lbtestedSet" runat="server" Text='<%# Eval("set_test_name") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <div style="text-align: center">
                                                <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </tr>
                        <tr>
                            <asp:Label CssClass="btn btn-default tar" ID="sample_code" data-toggle="tooltip" Visible="false" runat="server"
                                Text='<%# Eval("sample_code") %>'></asp:Label>
                        </tr>
                        <br />

                    </ItemTemplate>

                </asp:Repeater>
            </table>
            <div class="pull-right" style="text-align: right;">
                <asp:Label ID="lblPrintDate1" Font-Size="7" Text="PRINT DATE :" runat="server" />
                <span>
                    <asp:Label ID="lblPrintDate" Font-Size="7" runat="server" /></span>

            </div>
            <%-- </div>--%>
            <div class="page-break" id="pb2" runat="server" visible="false"></div>
        </div>
    </form>
</body>
</html>
