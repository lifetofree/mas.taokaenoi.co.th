using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_times")]
public class data_times
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    [XmlElement("finger_template_list")]
    public finger_template_detail[] finger_template_list { get; set; }
    [XmlElement("search_template_list")]
    public search_template_detail[] search_template_list { get; set; }

    [XmlElement("finger_machine_list")]
    public finger_machine_detail[] finger_machine_list { get; set; }
    [XmlElement("search_machine_list")]
    public search_machine_detail[] search_machine_list { get; set; }

    [XmlElement("tm_place_list")]
    public tm_place_detail[] tm_place_list { get; set; }
    [XmlElement("search_place_list")]
    public search_place_detail[] search_place_list { get; set; }

    [XmlElement("tm_location_list")]
    public tm_location_detail[] tm_location_list { get; set; }
    [XmlElement("search_location_list")]
    public search_location_detail[] search_location_list { get; set; }

    [XmlElement("tm_zone_list")]
    public tm_zone_detail[] tm_zone_list { get; set; }
    [XmlElement("search_zone_list")]
    public search_zone_detail[] search_zone_list { get; set; }

    [XmlElement("tm_group_list")]
    public tm_group_detail[] tm_group_list { get; set; }
    [XmlElement("search_group_list")]
    public search_group_detail[] search_group_list { get; set; }

    [XmlElement("tm_group_list_m2")]
    public tm_group_detail_m2[] tm_group_list_m2 { get; set; }
    [XmlElement("search_group_list_m2")]
    public search_group_detail_m2[] search_group_list_m2 { get; set; }

    [XmlElement("tm_machine_admin_list")]
    public tm_machine_admin_detail[] tm_machine_admin_list { get; set; }

    [XmlElement("tm_Finger_time_list")]
    public tm_finger_time_detail[] tm_finger_time_list { get; set; }
    [XmlElement("search_finger_time_list")]
    public search_finger_time_detail[] search_finger_time_list { get; set; } 

    [XmlElement("tm_plant_list")]
    public tm_plant_detail[] tm_plant_list { get; set; }
    [XmlElement("search_tm_plant_list")]
    public search_tm_plant_detail[] search_tm_plant_list { get; set; }
}

#region template
[Serializable]
public class finger_template_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("index0")]
    public string index0 { get; set; }
    [XmlElement("index1")]
    public string index1 { get; set; }
    [XmlElement("index2")]
    public string index2 { get; set; }
    [XmlElement("index3")]
    public string index3 { get; set; }
    [XmlElement("index4")]
    public string index4 { get; set; }
    [XmlElement("index5")]
    public string index5 { get; set; }
    [XmlElement("index6")]
    public string index6 { get; set; }
    [XmlElement("index7")]
    public string index7 { get; set; }
    [XmlElement("index8")]
    public string index8 { get; set; }
    [XmlElement("index9")]
    public string index9 { get; set; }
    [XmlElement("template_status")]
    public int template_status { get; set; }
    [XmlElement("createdate")]
    public string createdate { get; set; }
    [XmlElement("updatedate")]
    public string updatedate { get; set; }

    [XmlElement("machine_idx")]
    public int machine_idx { get; set; }
    [XmlElement("ip_host")]
    public string ip_host { get; set; }

    [XmlElement("emp_idx_action")]
    public int emp_idx_action { get; set; }
}

[Serializable]
public class search_template_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_rdept_idx")]
    public string s_rdept_idx { get; set; }
    [XmlElement("s_rsec_idx")]
    public string s_rsec_idx { get; set; }
    [XmlElement("s_plant_idx")]
    public string s_plant_idx { get; set; }
}
#endregion template

#region machine
[Serializable]
public class finger_machine_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("ip_host")]
    public string ip_host { get; set; }
    [XmlElement("machine_name")]
    public string machine_name { get; set; }
    [XmlElement("zone_idx")]
    public int zone_idx { get; set; }
    [XmlElement("place_idx")]
    public int place_idx { get; set; }
    [XmlElement("loc_idx")]
    public int loc_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("flag_attendance")]
    public int flag_attendance { get; set; }
    [XmlElement("machine_status")]
    public int machine_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("zone_name")]
    public string zone_name { get; set; }
    [XmlElement("place_name")]
    public string place_name { get; set; }
    [XmlElement("loc_name")]
    public string loc_name { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("template_idx")]
    public int template_idx { get; set; }
}

[Serializable]
public class search_machine_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_ip_host")]
    public string s_ip_host { get; set; }
    [XmlElement("s_machine_name")]
    public string s_machine_name { get; set; }
    [XmlElement("s_zone_idx")]
    public string s_zone_idx { get; set; }
    [XmlElement("s_place_idx")]
    public string s_place_idx { get; set; }
    [XmlElement("s_loc_idx")]
    public string s_loc_idx { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_rdept_idx")]
    public string s_rdept_idx { get; set; }
    [XmlElement("s_rsec_idx")]
    public string s_rsec_idx { get; set; }
    [XmlElement("s_machine_status")]
    public string s_machine_status { get; set; }
}
#endregion machine

#region place
[Serializable]
public class tm_place_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("place_name")]
    public string place_name { get; set; }
    [XmlElement("place_status")]
    public int place_status { get; set; }
}

[Serializable]
public class search_place_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_place_name")]
    public string s_place_name { get; set; }
    [XmlElement("s_place_status")]
    public string s_place_status { get; set; }
}
#endregion place

#region location
[Serializable]
public class tm_location_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("location_name")]
    public string location_name { get; set; }
    [XmlElement("location_status")]
    public int location_status { get; set; }
}

[Serializable]
public class search_location_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_location_name")]
    public string s_location_name { get; set; }
    [XmlElement("s_location_status")]
    public string s_location_status { get; set; }
}
#endregion location

#region zone
[Serializable]
public class tm_zone_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("zone_name")]
    public string zone_name { get; set; }
    [XmlElement("zone_status")]
    public int zone_status { get; set; }
}

[Serializable]
public class search_zone_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_zone_name")]
    public string s_zone_name { get; set; }
    [XmlElement("s_zone_status")]
    public string s_zone_status { get; set; }
}
#endregion zone

#region group
[Serializable]
public class tm_group_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("group_name")]
    public string group_name { get; set; }
    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }
    [XmlElement("group_status")]
    public int group_status { get; set; }

    [XmlElement("plant_name")]
    public string plant_name { get; set; }
}

[Serializable]
public class search_group_detail
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_group_name")]
    public string s_group_name { get; set; }
    [XmlElement("s_plant_idx")]
    public string s_plant_idx { get; set; }
    [XmlElement("s_plant_name")]
    public string s_plant_name { get; set; }
    [XmlElement("s_group_status")]
    public string s_group_status { get; set; }
}

[Serializable]
public class tm_group_detail_m2
{
    [XmlElement("m2_idx")]
    public int m2_idx { get; set; }
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("flag_priority")]
    public int flag_priority { get; set; }
    [XmlElement("approve_status")]
    public int approve_status { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
}

[Serializable]
public class search_group_detail_m2
{
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
    [XmlElement("s_flag_priority")]
    public int s_flag_priority { get; set; }
    [XmlElement("s_approve_status")]
    public string s_approve_status { get; set; }
}
#endregion group

#region machine administrator
[Serializable]
public class tm_machine_admin_detail
{
    [XmlElement("m1_idx")]
    public int m1_idx { get; set; }
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("ip_host")]
    public string ip_host { get; set; }
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("privilege_level")]
    public int privilege_level { get; set; }
    [XmlElement("admin_status")]
    public int admin_status { get; set; }
}
#endregion machine administrator

#region finger time
[Serializable]
public class tm_finger_time_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("finger_time")]
    public string finger_time { get; set; }
    [XmlElement("machine_name")]
    public string machine_name { get; set; }
}

[Serializable]
public class search_finger_time_detail
{
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_start_date")]
    public string s_start_date { get; set; }
    [XmlElement("s_end_date")]
    public string s_end_date { get; set; }
    [XmlElement("s_machine_idx")]
    public string s_machine_idx { get; set; }
    [XmlElement("s_machine_ip")]
    public string s_machine_ip { get; set; }
}
#endregion finger time

#region plant list
[Serializable]
public class tm_plant_detail
{
    [XmlElement("plant_idx")]
    public int plant_idx { get; set; }
    [XmlElement("plant_name")]
    public string plant_name { get; set; }
    [XmlElement("flag_visitor")]
    public int flag_visitor { get; set; }
    [XmlElement("plant_status")]
    public int plant_status { get; set; }
}

[Serializable]
public class search_tm_plant_detail
{
    [XmlElement("s_plant_idx")]
    public string s_plant_idx { get; set; }
    [XmlElement("s_plant_name")]
    public string s_plant_name { get; set; }
    [XmlElement("s_flag_visitor")]
    public string s_flag_visitor { get; set; }
    [XmlElement("s_plant_status")]
    public string s_plant_status { get; set; }
}
#endregion plant list