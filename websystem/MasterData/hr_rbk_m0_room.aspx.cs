﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_room : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Food"];
    static string _urlGetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Food"];
    static string _urlSetRbkm0FoodDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0FoodDel"];

    static string _urlGetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Place"];
    static string _urlGetRbkm0Equiment = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Equiment"];

    static string _urlSetRbkm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Room"];
    static string _urlGetRbkm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Room"];
    static string _urlGetRbkm0EquimentDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0EquimentDetail"];
    static string _urlSetRbkm0RoomDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0RoomDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            SelectRoomDetail();
            getEquimentList();
            getEquimentList();
            getEquimentListUpdate();
           
        }

        linkBtnTrigger(btnAddRoomBooking);
        linkBtnTrigger(btnEditRoom);

    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        SelectRoomDetail();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddRoomBooking":

                //GvFoodInRoom.EditIndex = -1;
                SelectRoomDetail();
                GvRoomBooking.Visible = false;
                btnAddRoomBooking.Visible = false;
                fvRoomBooking.Visible = true;
                setFormData(fvRoomBooking, FormViewMode.Insert, null);
                CleardataSetEquimentList();
                DropDownList ddlPlace = (DropDownList)fvRoomBooking.FindControl("ddlPlace");
              
                getPlace(ddlPlace, 0);
                getEquiment(ddlEquiment, 0);

                break;

            case "CmdSave":
                InsertRoomDetail();
                
                break;

            case "CmdCancel":
                btnAddRoomBooking.Visible = true;
                SelectRoomDetail();
                GvRoomBooking.Visible = true;
                fvRoomBooking.Visible = false;
                setOntop.Focus();
                break;

            case "CmdDeleteRoom":
                int room_idx_del = int.Parse(cmdArg);

                //litdebug.Text = "222";

                data_roombooking data_m0_room_del = new data_roombooking();
                rbk_m0_room_detail _m0_room_del = new rbk_m0_room_detail();
                data_m0_room_del.rbk_m0_room_list = new rbk_m0_room_detail[1];

                _m0_room_del.m0_room_idx = room_idx_del;
                _m0_room_del.cemp_idx = _emp_idx;

                data_m0_room_del.rbk_m0_room_list[0] = _m0_room_del;
                data_m0_room_del = callServicePostRoomBooking(_urlSetRbkm0RoomDel, data_m0_room_del);

                SelectRoomDetail();
                //Gv_select_unit.Visible = false;
                btnAddRoomBooking.Visible = true;
                fvRoomBooking.Visible = false;

                break;
            case "cmdAddEquiment":
                setEquimentList();

                break;
            case "CmdEdit":
 
                btnAddRoomBooking.Visible = false;
                GvRoomBooking.Visible = false;

                fvRoomBooking.Visible = true;
                fvRoomBooking.ChangeMode(FormViewMode.Edit);
 
                actionReadRoomDetail(int.Parse(cmdArg));
               

                break;
            case "cmdAddEquimentUpdate":
                setEquimentListUpdate();
                break;

            case "cmdSaveUpdate":

                //linkBtnTrigger(btnSaveUpdate);
                Label lblm0_room_idx_edit_old = (Label)fvRoomBooking.FindControl("lblm0_room_idx_edit");
                Label lbl_place_name_edit_old = (Label)fvRoomBooking.FindControl("lbl_place_name_edit");
                Label lbl_room_name_en_edit_old = (Label)fvRoomBooking.FindControl("lbl_room_name_en_edit");

                DropDownList ddlPlaceUpdate = (DropDownList)fvRoomBooking.FindControl("ddlPlaceUpdate");
                TextBox txt_roomname_th_Update = (TextBox)fvRoomBooking.FindControl("txt_roomname_th_Update");
                TextBox txt_roomname_en_Update = (TextBox)fvRoomBooking.FindControl("txt_roomname_en_Update");
                TextBox txt_capacity_people_Update = (TextBox)fvRoomBooking.FindControl("txt_capacity_people_Update");
                TextBox txt_room_detail_Update = (TextBox)fvRoomBooking.FindControl("txt_room_detail_Update");
                DropDownList ddlm0_room_statusUpdate = (DropDownList)fvRoomBooking.FindControl("ddlm0_room_statusUpdate");
                FileUpload UploadFileRoomEdit = (FileUpload)fvRoomBooking.FindControl("UploadFileRoomEdit");

                data_roombooking data_m0_room_edit = new data_roombooking();
                rbk_m0_room_detail _m0_room_edit = new rbk_m0_room_detail();
                data_m0_room_edit.rbk_m0_room_list = new rbk_m0_room_detail[1];

                _m0_room_edit.m0_room_idx = int.Parse(cmdArg);
                _m0_room_edit.place_idx = int.Parse(ddlPlaceUpdate.SelectedValue);
                _m0_room_edit.cemp_idx = _emp_idx;
                _m0_room_edit.room_name_th = txt_roomname_th_Update.Text;
                _m0_room_edit.room_name_en = txt_roomname_en_Update.Text;
                _m0_room_edit.capacity_people = int.Parse(txt_capacity_people_Update.Text);
                _m0_room_edit.room_detail = txt_room_detail_Update.Text;
                _m0_room_edit.m0_room_status = int.Parse(ddlm0_room_statusUpdate.SelectedValue);


                //equiment update list vs
                var _dataset_Equiment_update = (DataSet)ViewState["vsEquimentUpdate"];

                var _add_Equiment_update = new rbk_m1_room_detail[_dataset_Equiment_update.Tables[0].Rows.Count];
                int row_equimentlist_update = 0;

                foreach (DataRow dtrow_Equimentlist_update in _dataset_Equiment_update.Tables[0].Rows)
                {
                    _add_Equiment_update[row_equimentlist_update] = new rbk_m1_room_detail();

                    _add_Equiment_update[row_equimentlist_update].equiment_idx = int.Parse(dtrow_Equimentlist_update["drEquimentIdxUpdate"].ToString());
                    _add_Equiment_update[row_equimentlist_update].cemp_idx = _emp_idx;

                    row_equimentlist_update++;

                }

                data_m0_room_edit.rbk_m0_room_list[0] = _m0_room_edit;
                data_m0_room_edit.rbk_m1_room_list = _add_Equiment_update;
              
                //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room_edit));
                data_m0_room_edit = callServicePostRoomBooking(_urlSetRbkm0Room, data_m0_room_edit);
                if (data_m0_room_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                else
                {

                    if (UploadFileRoomEdit.HasFile)
                    {
                        string getPathfile_edit = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_edit_old = lblm0_room_idx_edit_old.Text;
                        //lbl_place_name_edit_old.Text.Trim() + lbl_room_name_en_edit_old.Text + lblm0_room_idx_edit_old.Text;

                        string directoryName_edit_new = lblm0_room_idx_edit_old.Text;// ddlPlaceUpdate.SelectedItem + txt_roomname_en_Update.Text + lblm0_room_idx_edit_old.Text;
                        // string document_code = ViewState["_no_invoice"].ToString();

                        string filePath_old = Server.MapPath(getPathfile_edit + directoryName_edit_old);

                        if (!Directory.Exists(filePath_old))
                        {
  
                            string filePath_new = Server.MapPath(getPathfile_edit + directoryName_edit_new);

                            if (!Directory.Exists(filePath_new))
                            {
                                Directory.CreateDirectory(filePath_new);
                            }
                            string extension = Path.GetExtension(UploadFileRoomEdit.FileName);

                            UploadFileRoomEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_new) + "\\" + directoryName_edit_new + extension);

                        }
                        else
                        {
                            string path_old = string.Empty;
                            List<string> files = new List<string>();
                            DirectoryInfo dirListRoom = new DirectoryInfo(filePath_old);
                            foreach (FileInfo file in dirListRoom.GetFiles())
                            {
                                path_old = ResolveUrl(filePath_old + "\\" + file.Name);
                            }
                            File.Delete(path_old);

                            string extension = Path.GetExtension(UploadFileRoomEdit.FileName);
                            UploadFileRoomEdit.SaveAs(Server.MapPath(getPathfile_edit + directoryName_edit_old) + "\\" + directoryName_edit_old + extension);

                        }

                    }
                    else
                    {
                      
                    }

                    SelectRoomDetail();

                    btnAddRoomBooking.Visible = true;
                    GvRoomBooking.Visible = true;
                    fvRoomBooking.Visible = false;
                    setOntop.Focus();

                }

                break;

        }
    }
    #endregion btnCommand

    #region data table
    protected void getEquimentList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsEquimentList = new DataSet();
        dsEquimentList.Tables.Add("dsEquimentTable");
        dsEquimentList.Tables["dsEquimentTable"].Columns.Add("drEquimentIdx", typeof(String));
        dsEquimentList.Tables["dsEquimentTable"].Columns.Add("drEquimentText", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialAmount", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialPerUnit", typeof(String));
        ViewState["vsEquimentList"] = dsEquimentList;
    }
    
    protected void setEquimentList()
    {
        if (ViewState["vsEquimentList"] != null)
        {
            DropDownList ddlEquiment = (DropDownList)fvRoomBooking.FindControl("ddlEquiment");
            GridView gvEquimentList = (GridView)fvRoomBooking.FindControl("gvEquimentList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsEquimentList"];

            foreach (DataRow dr in dsContacts.Tables["dsEquimentTable"].Rows)
            {
                if (dr["drEquimentIdx"].ToString() == ddlEquiment.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsEquimentTable"].NewRow();
            drContacts["drEquimentIdx"] = ddlEquiment.SelectedValue;
            drContacts["drEquimentText"] = ddlEquiment.SelectedItem.Text;

            dsContacts.Tables["dsEquimentTable"].Rows.Add(drContacts);
            ViewState["vsEquimentList"] = dsContacts;
            setGridData(gvEquimentList, dsContacts.Tables["dsEquimentTable"]);
            gvEquimentList.Visible = true;
        }
    }

    protected void getEquimentListUpdate()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPlaceListUpdate = new DataSet();
        dsPlaceListUpdate.Tables.Add("dsEquimentTableUpdate");
        dsPlaceListUpdate.Tables["dsEquimentTableUpdate"].Columns.Add("drEquimentIdxUpdate", typeof(String));
        dsPlaceListUpdate.Tables["dsEquimentTableUpdate"].Columns.Add("drEquimentTextUpdate", typeof(String));

        ViewState["vsEquimentUpdate"] = dsPlaceListUpdate;
    }

    protected void setEquimentListUpdate()
    {
        if (ViewState["vsEquimentUpdate"] != null)
        {
            DropDownList ddlEquimentUpdate = (DropDownList)fvRoomBooking.FindControl("ddlEquimentUpdate");
            GridView gvEquimentListUpdate = (GridView)fvRoomBooking.FindControl("gvEquimentListUpdate");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            DataSet dsContacts = (DataSet)ViewState["vsEquimentUpdate"];
            foreach (DataRow dr in dsContacts.Tables["dsEquimentTableUpdate"].Rows)
            {
                if (dr["drEquimentIdxUpdate"].ToString() == ddlEquimentUpdate.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsEquimentTableUpdate"].NewRow();
            drContacts["drEquimentIdxUpdate"] = ddlEquimentUpdate.SelectedValue;
            drContacts["drEquimentTextUpdate"] = ddlEquimentUpdate.SelectedItem.Text;

            dsContacts.Tables["dsEquimentTableUpdate"].Rows.Add(drContacts);
            ViewState["vsEquimentUpdate"] = dsContacts;
            setGridData(gvEquimentListUpdate, dsContacts.Tables["dsEquimentTableUpdate"]);
            gvEquimentListUpdate.Visible = true;

            //litdebug.Text = "4444";
        }
        else
        {
            //litdebug.Text = "4444";
        }
    }

    protected void CleardataSetEquimentList()
    {
        var gvEquimentList = (GridView)fvRoomBooking.FindControl("gvEquimentList");
        ViewState["vsEquimentList"] = null;
        gvEquimentList.DataSource = ViewState["vsEquimentList"];
        gvEquimentList.DataBind();
        getEquimentList();
    }

    protected void CleardataSetEquimentListUpdate()
    {
        var gvEquimentListUpdate = (GridView)fvRoomBooking.FindControl("gvEquimentListUpdate");
        ViewState["vsEquimentUpdate"] = null;
        gvEquimentListUpdate.DataSource = ViewState["vsEquimentUpdate"];
        gvEquimentListUpdate.DataBind();
        getEquimentListUpdate();
    }

    #endregion data table

    #region Custom Functions
    protected void InsertRoomDetail()
    {

        DropDownList ddlPlace_insert = (DropDownList)fvRoomBooking.FindControl("ddlPlace");
        TextBox txt_roomname_th = (TextBox)fvRoomBooking.FindControl("txt_roomname_th");
        TextBox txt_roomname_en = (TextBox)fvRoomBooking.FindControl("txt_roomname_en");
        TextBox txt_capacity_people = (TextBox)fvRoomBooking.FindControl("txt_capacity_people");
        TextBox txt_room_detail = (TextBox)fvRoomBooking.FindControl("txt_room_detail");

        FileUpload UploadFileRoom = (FileUpload)fvRoomBooking.FindControl("UploadFileRoom");

        data_roombooking data_m0_room = new data_roombooking();
        rbk_m0_room_detail m0_room_insert = new rbk_m0_room_detail();
        data_m0_room.rbk_m0_room_list = new rbk_m0_room_detail[1];

        m0_room_insert.m0_room_idx = 0;
        m0_room_insert.place_idx = int.Parse(ddlPlace_insert.SelectedValue);
        m0_room_insert.cemp_idx = _emp_idx;
        m0_room_insert.room_name_th = txt_roomname_th.Text;
        m0_room_insert.room_name_en = txt_roomname_en.Text;
        m0_room_insert.capacity_people = int.Parse(txt_capacity_people.Text);
        m0_room_insert.room_detail = txt_room_detail.Text;


        //m1 room
        var _dataset_EquimentList = (DataSet)ViewState["vsEquimentList"];

        var _add_EquimentList = new rbk_m1_room_detail[_dataset_EquimentList.Tables[0].Rows.Count];
        int row_equimentlist = 0;

        foreach (DataRow dtrow_Equimentlist in _dataset_EquimentList.Tables[0].Rows)
        {
            _add_EquimentList[row_equimentlist] = new rbk_m1_room_detail();

            _add_EquimentList[row_equimentlist].equiment_idx = int.Parse(dtrow_Equimentlist["drEquimentIdx"].ToString());
            _add_EquimentList[row_equimentlist].cemp_idx = _emp_idx;

            row_equimentlist++;

        }

        data_m0_room.rbk_m0_room_list[0] = m0_room_insert;
        data_m0_room.rbk_m1_room_list = _add_EquimentList;
        //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room));

        //litdebug.Text = _dataset_EquimentList.Tables[0].Rows.Count.ToString();

        if(_dataset_EquimentList.Tables[0].Rows.Count > 0)
        {
            data_m0_room = callServicePostRoomBooking(_urlSetRbkm0Room, data_m0_room);

            //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
            if (data_m0_room.return_code == 1)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

            }
            else
            {
                ViewState["vs_createRoom_DetailFile"] = data_m0_room.rbk_m0_room_list[0].m0_room_idx;
                if (UploadFileRoom.HasFile)
                {
                    string getPathfile = ConfigurationManager.AppSettings["path_flie_roombooking"];
                    string room_name = ViewState["vs_createRoom_DetailFile"].ToString();//ddlPlace_insert.SelectedItem + txt_roomname_en.Text + ViewState["vs_createRoom_DetailFile"].ToString();
                    string fileName_upload = room_name;
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                    if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                    string extension = Path.GetExtension(UploadFileRoom.FileName);

                    UploadFileRoom.SaveAs(Server.MapPath(getPathfile + room_name) + "\\" + fileName_upload + extension);
                    //litdebug.Text = room_name;
                }
                else
                {
                    //litdebug.Text = "3333";
                }

                SelectRoomDetail();
                GvRoomBooking.Visible = true;
                //SelectFoodDetail();
                btnAddRoomBooking.Visible = true;
                fvRoomBooking.Visible = false;
                setOntop.Focus();

                //txtfood_name.Text = String.Empty;
                //tex_place_code.Text = String.Empty;
            }

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรอกข้อมูลให้ครบถ้วนก่อนทำการบันทึก');", true);
           
        }


    }

    protected void SelectRoomDetail()
    {

        data_roombooking data_m0_room_detail = new data_roombooking();
        rbk_m0_room_detail m0_room_detail = new rbk_m0_room_detail();
        data_m0_room_detail.rbk_m0_room_list = new rbk_m0_room_detail[1];

        m0_room_detail.condition = 0;

        data_m0_room_detail.rbk_m0_room_list[0] = m0_room_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_room_detail = callServicePostRoomBooking(_urlGetRbkm0Room, data_m0_room_detail);

        setGridData(GvRoomBooking, data_m0_room_detail.rbk_m0_room_list);
       
    }

    protected void actionReadRoomDetail(int _m0_room_idx)
    {
       
        data_roombooking data_m0_room_detail_edit = new data_roombooking();
        rbk_m0_room_detail m0_room_detail_edit = new rbk_m0_room_detail();
        data_m0_room_detail_edit.rbk_m0_room_list = new rbk_m0_room_detail[1];

        m0_room_detail_edit.condition = 1;
        m0_room_detail_edit.m0_room_idx = _m0_room_idx;

        data_m0_room_detail_edit.rbk_m0_room_list[0] = m0_room_detail_edit;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_room_detail_edit));        
        data_m0_room_detail_edit = callServicePostRoomBooking(_urlGetRbkm0Room, data_m0_room_detail_edit);
        setFormData(fvRoomBooking, FormViewMode.Edit, data_m0_room_detail_edit.rbk_m0_room_list);
        //linkBtnTrigger(btnSaveUpdate);



    }

    protected void getPlace(DropDownList ddlName, int _place_idx)
    {

        data_roombooking data_m0_place_detail = new data_roombooking();
        rbk_m0_place_detail m0_place_detail = new rbk_m0_place_detail();
        data_m0_place_detail.rbk_m0_place_list = new rbk_m0_place_detail[1];

        m0_place_detail.condition = 1;

        data_m0_place_detail.rbk_m0_place_list[0] = m0_place_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_place_detail = callServicePostRoomBooking(_urlGetRbkm0Place, data_m0_place_detail);

        setDdlData(ddlName, data_m0_place_detail.rbk_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "0"));
        ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getEquiment(DropDownList ddlName, int _equiment_idx)
    {

        data_roombooking data_m0_equiment_detail = new data_roombooking();
        rbk_m0_equiment_detail m0_equiment_detail = new rbk_m0_equiment_detail();
        data_m0_equiment_detail.rbk_m0_equiment_list = new rbk_m0_equiment_detail[1];

        m0_equiment_detail.condition = 1;

        data_m0_equiment_detail.rbk_m0_equiment_list[0] = m0_equiment_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_equiment_detail = callServicePostRoomBooking(_urlGetRbkm0Equiment, data_m0_equiment_detail);

        setDdlData(ddlName, data_m0_equiment_detail.rbk_m0_equiment_list, "equiment_name", "equiment_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกอุปกรณ์ ---", "0"));
        ddlName.SelectedValue = _equiment_idx.ToString();

    }

    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }


    #endregion

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "fvRoomBooking":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        //linkBtnTrigger(btnSaveUpdate);
                        CleardataSetEquimentListUpdate();

                        Label lblm0_room_idx_edit = (Label)FvName.FindControl("lblm0_room_idx_edit");
                        Label lblplace_idx_edit = (Label)FvName.FindControl("lblplace_idx_edit");
                        Label lbl_place_name_edit = (Label)FvName.FindControl("lbl_place_name_edit");
                        Label lbl_room_name_en_edit = (Label)FvName.FindControl("lbl_room_name_en_edit");
                        DropDownList ddlPlaceUpdate = (DropDownList)FvName.FindControl("ddlPlaceUpdate");

                        getPlace(ddlPlaceUpdate, int.Parse(lblplace_idx_edit.Text));
                        getEquiment(ddlEquimentUpdate, 0);

                        data_roombooking data_m1_room_detail_edit = new data_roombooking();
                        rbk_m1_room_detail m1_room_detail_edit = new rbk_m1_room_detail();
                        data_m1_room_detail_edit.rbk_m1_room_list = new rbk_m1_room_detail[1];
  
                        m1_room_detail_edit.m0_room_idx = int.Parse(lblm0_room_idx_edit.Text);

                        data_m1_room_detail_edit.rbk_m1_room_list[0] = m1_room_detail_edit;

                        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m1_room_detail_edit));
                        data_m1_room_detail_edit = callServicePostRoomBooking(_urlGetRbkm0EquimentDetail, data_m1_room_detail_edit);

                        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m1_room_detail_edit));
                        if(data_m1_room_detail_edit.return_code == 0)
                        {
                            foreach (var equiment_edit in data_m1_room_detail_edit.rbk_m1_room_list)
                            {
                                if (ViewState["vsEquimentUpdate"] != null)
                                {
                                    CultureInfo culture = new CultureInfo("en-US");
                                    Thread.CurrentThread.CurrentCulture = culture;
                                    DataSet dsContacts = (DataSet)ViewState["vsEquimentUpdate"];
                                    DataRow drContacts = dsContacts.Tables["dsEquimentTableUpdate"].NewRow();

                                    drContacts["drEquimentIdxUpdate"] = equiment_edit.equiment_idx;
                                    drContacts["drEquimentTextUpdate"] = equiment_edit.equiment_name;

                                    dsContacts.Tables["dsEquimentTableUpdate"].Rows.Add(drContacts);
                                    ViewState["vsEquimentUpdate"] = dsContacts;
                                    setGridData(gvEquimentListUpdate, dsContacts.Tables["dsEquimentTableUpdate"]);

                                    gvEquimentListUpdate.Visible = true;
                                }
                            }
                        }


                        //File Datil Edit
                        //var lbl_m0_room_idx_file = (Label)FvName.FindControl("lbl_m0_room_idx");
                        //var lbl_place_name_file = (Label)FvName.FindControl("lbl_place_name");
                        //var lbl_room_name_en_view_file = (Label)FvName.FindControl("lbl_room_name_en_view");

                        var btnViewFileRoomEdit = (HyperLink)FvName.FindControl("btnViewFileRoomEdit");

                        string filePath_View_edit = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_View_edit = lblm0_room_idx_edit.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                        if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_View_edit)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_View_edit));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewFileRoomEdit.NavigateUrl = filePath_View_edit + directoryName_View_edit + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_edit + directoryName_View_edit + "/" + getfiles;
                                imgedit.Height = 100;
                                btnViewFileRoomEdit.Controls.Add(imgedit);


                            }
                            btnViewFileRoomEdit.Visible = true;
                        }
                        else
                        {
                            btnViewFileRoomEdit.Visible = false;
                        }

                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvRoomBooking":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_m0_room_idx = (Label)e.Row.FindControl("lbl_m0_room_idx");
                    Label lbl_m0_room_statuss = (Label)e.Row.FindControl("lbl_m0_room_statuss");
                    Label m0_room_statusOnline = (Label)e.Row.FindControl("m0_room_statusOnline");
                    Label m0_room_statusOffline = (Label)e.Row.FindControl("m0_room_statusOffline");
                    Repeater rptEquimentDetail = (Repeater)e.Row.FindControl("rptEquimentDetail");

                    ViewState["vs_m0_room_status"] = lbl_m0_room_statuss.Text;


                    if (ViewState["vs_m0_room_status"].ToString() == "1")
                    {
                        m0_room_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_m0_room_status"].ToString() == "0")
                    {
                        m0_room_statusOffline.Visible = true;
                    }

                    //Detail Equiment
                    data_roombooking data_m1_room_detail = new data_roombooking();
                    rbk_m1_room_detail m1_room_detail = new rbk_m1_room_detail();
                    data_m1_room_detail.rbk_m1_room_list = new rbk_m1_room_detail[1];

                    m1_room_detail.m0_room_idx = int.Parse(lbl_m0_room_idx.Text);

                    data_m1_room_detail.rbk_m1_room_list[0] = m1_room_detail;
                    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                    data_m1_room_detail = callServicePostRoomBooking(_urlGetRbkm0EquimentDetail, data_m1_room_detail);
                    setRepeaterData(rptEquimentDetail, data_m1_room_detail.rbk_m1_room_list);


                    //View File In Room
                    var lbl_m0_room_idx_file = (Label)e.Row.FindControl("lbl_m0_room_idx");
                    var lbl_place_name_file = (Label)e.Row.FindControl("lbl_place_name");
                    var lbl_room_name_en_view_file = (Label)e.Row.FindControl("lbl_room_name_en_view");

                    var btnViewFileRoom_ = (HyperLink)e.Row.FindControl("btnViewFileRoom");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View = ConfigurationManager.AppSettings["path_flie_roombooking"];
                    string directoryName = lbl_m0_room_idx_file.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                    if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileRoom_.NavigateUrl = filePath_View + directoryName + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View + directoryName + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileRoom_.Controls.Add(img);



                        }
                        btnViewFileRoom_.Visible = true;
                    }
                    else
                    {
                        btnViewFileRoom_.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }


                }
                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                ////GvFoodInRoom.EditIndex = e.NewEditIndex;
                ////SelectFoodDetail();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":

                ////var txt_food_idx_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_idx_edit");
                ////var txt_food_name_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_name_edit");
                ////var ddlfood_status_edit = (DropDownList)GvFoodInRoom.Rows[e.RowIndex].FindControl("ddlfood_status_edit");


                ////GvFoodInRoom.EditIndex = -1;


                ////data_roombooking data_m0_food_edit = new data_roombooking();
                ////rbk_m0_food_detail m0_food_edit = new rbk_m0_food_detail();
                ////data_m0_food_edit.rbk_m0_food_list = new rbk_m0_food_detail[1];

                ////m0_food_edit.food_idx = int.Parse(txt_food_idx_edit.Text);
                ////m0_food_edit.food_name = txt_food_name_edit.Text;
                ////m0_food_edit.cemp_idx = _emp_idx;
                ////m0_food_edit.food_status = int.Parse(ddlfood_status_edit.SelectedValue);

                ////data_m0_food_edit.rbk_m0_food_list[0] = m0_food_edit;

                //////litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_resultuse_edit));

                ////data_m0_food_edit = callServicePostRoomBooking(_urlSetRbkm0Food, data_m0_food_edit);

                ////if (data_m0_food_edit.return_code == 1)
                ////{
                ////    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                ////    SelectFoodDetail();
                ////}
                ////else
                ////{
                ////    SelectFoodDetail();
                ////}
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                ////GvFoodInRoom.EditIndex = -1;
                ////SelectFoodDetail();

                ////btnAddFood.Visible = true;
                ////FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvRoomBooking":
                GvRoomBooking.PageIndex = e.NewPageIndex;
                GvRoomBooking.DataBind();
                SelectRoomDetail();
                //SelectFoodDetail();
                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "btnRemoveEquiment":
                    GridView gvEquimentList = (GridView)fvRoomBooking.FindControl("gvEquimentList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsEquimentList"];
                    dsContacts.Tables["dsEquimentTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvEquimentList, dsContacts.Tables["dsEquimentTable"]);
                    if (dsContacts.Tables["dsEquimentTable"].Rows.Count < 1)
                    {
                        gvEquimentList.Visible = false;
                    }
                    break;
                case "btnRemoveEquimentUpdate":
                    GridView gvEquimentListUpdate = (GridView)fvRoomBooking.FindControl("gvEquimentListUpdate");
                    GridViewRow rowSelect_del = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_del = rowSelect_del.RowIndex;
                    DataSet dsContacts_del = (DataSet)ViewState["vsEquimentUpdate"];
                    dsContacts_del.Tables["dsEquimentTableUpdate"].Rows[rowIndex_del].Delete();
                    dsContacts_del.AcceptChanges();
                    setGridData(gvEquimentListUpdate, dsContacts_del.Tables["dsEquimentTableUpdate"]);
                    if (dsContacts_del.Tables["dsEquimentTableUpdate"].Rows.Count < 1)
                    {
                        gvEquimentListUpdate.Visible = false;
                    }
                    break;

            }
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions

    #region Trigger 
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    #endregion Trigger
}