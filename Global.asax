<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RegisterRoutes(RouteTable.Routes);
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends.
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer
        // or SQLServer, the event is not raised.

    }
    void RegisterRoutes(RouteCollection routes)
    {
        //routes.MapPageRoute("xx", "xx", "~/xx/xx.aspx");
        //routes.MapPageRoute("xx", "Notice/{idx}/{type}", "~/xx/xx/xx.aspx", true, new RouteValueDictionary { { "idx", "0" }, { "type", "xxx" } });
        routes.MapPageRoute("default", "", "~/websystem/dashboard.aspx");
        routes.MapPageRoute("login", "login", "~/websystem/login.aspx");
        routes.MapPageRoute("changepassword", "changepassword", "~/websystem/change_password.aspx");
        routes.MapPageRoute("warning", "warning", "~/websystem/warning.aspx");
        routes.MapPageRoute("error", "error", "~/websystem/error.aspx");
        routes.MapPageRoute("success", "success", "~/websystem/success.aspx");

        // control panel
        routes.MapPageRoute("manage-menu", "manage-menu", "~/websystem/controlpanel/managemenu.aspx");
        routes.MapPageRoute("manage-permission", "manage-permission", "~/websystem/controlpanel/basicpermission.aspx");
        routes.MapPageRoute("log-management", "log-management", "~/websystem/controlpanel/logManagement.aspx");
        // control panel

        // begin Employee shift
        routes.MapPageRoute("employee-shift", "employee-shift", "~/websystem/emps/emps.aspx");
        routes.MapPageRoute("part-time", "part-time", "~/websystem/emps/parttime.aspx");
        routes.MapPageRoute("reward", "reward", "~/websystem/emps/reward.aspx");
        routes.MapPageRoute("group", "group", "~/websystem/emps/group-diary.aspx");
        routes.MapPageRoute("hr-holidays", "hr-holidays", "~/websystem/hr/employee_holidays.aspx");
        // end Employee shift

        // Holder
        routes.MapPageRoute("holder-devices", "holder-devices", "~/websystem/ITServices/HoldersDevices.aspx");
        // IT REPAIR
        routes.MapPageRoute("sap-status", "sap-status", "~/websystem/MasterData/MasterStatus.aspx");
        routes.MapPageRoute("it-status", "it-status", "~/websystem/MasterData/MasterStatusIT.aspx");
        routes.MapPageRoute("gm-status", "gm-status", "~/websystem/MasterData/MasterStatusGM.aspx");
        routes.MapPageRoute("pos-status", "pos-status", "~/websystem/MasterData/MasterStatusPOS.aspx");

        routes.MapPageRoute("pos-lv1", "pos-lv1", "~/websystem/MasterData/MasterCasePOSLV1.aspx");
        routes.MapPageRoute("pos-lv2", "pos-lv2", "~/websystem/MasterData/MasterCasePOSLV2.aspx");
        routes.MapPageRoute("pos-lv3", "pos-lv3", "~/websystem/MasterData/MasterCasePOSLV3.aspx");
        routes.MapPageRoute("pos-lv4", "pos-lv4", "~/websystem/MasterData/MasterCasePOSLV4.aspx");

        routes.MapPageRoute("it-lv1", "it-lv1", "~/websystem/MasterData/itr_m0_CaseITLV1.aspx");
        routes.MapPageRoute("it-lv2", "it-lv2", "~/websystem/MasterData/itr_m0_CaseITLV2.aspx");
        routes.MapPageRoute("it-lv3", "it-lv3", "~/websystem/MasterData/itr_m0_CaseITLV3.aspx");
        routes.MapPageRoute("it-lv4", "it-lv4", "~/websystem/MasterData/itr_m0_CaseITLV4.aspx");


        routes.MapPageRoute("res-lv1", "res-lv1", "~/websystem/MasterData/itr_m0_CaseRESLV1.aspx");
        routes.MapPageRoute("res-lv2", "res-lv2", "~/websystem/MasterData/itr_m0_CaseRESLV2.aspx");
        routes.MapPageRoute("res-lv3", "res-lv3", "~/websystem/MasterData/itr_m0_CaseRESLV3.aspx");
        routes.MapPageRoute("res-lv4", "res-lv4", "~/websystem/MasterData/itr_m0_CaseRESLV4.aspx");


        routes.MapPageRoute("shortdept", "shortdept", "~/websystem/MasterData/MasterShortDept.aspx");


        routes.MapPageRoute("sap-priority", "sap-priority", "~/websystem/MasterData/MasterPriority.aspx");
        routes.MapPageRoute("itrepair", "itrepair/{link}", "~/websystem/ITServices/ITRepair.aspx", true, new RouteValueDictionary { { "link", "0" } });
        routes.MapPageRoute("reportitrepair", "reportitrepair", "~/websystem/ITServices/ReportITRepair.aspx");
        routes.MapPageRoute("stackedcolumn", "stackedcolumn", "~/websystem/ITServices/stackedcolumn.aspx");
        routes.MapPageRoute("reportchr", "reportchr", "~/websystem/ITServices/ReportCHR.aspx");

        routes.MapPageRoute("chr-system", "chr-system", "~/websystem/MasterData/chr_m0_system.aspx");
        routes.MapPageRoute("chr-typechr", "chr-typechr", "~/websystem/MasterData/chr_m0_typechr.aspx");
        routes.MapPageRoute("chr-typeclose1", "chr-typeclose1", "~/websystem/MasterData/chr_m0_typeclose1.aspx");
        routes.MapPageRoute("chr-typeclose2", "chr-typeclose2", "~/websystem/MasterData/chr_m0_typeclose2.aspx");
        routes.MapPageRoute("chr-user", "chr-user", "~/websystem/MasterData/chr_m0_user.aspx");
        routes.MapPageRoute("chr-iso", "chr-iso", "~/websystem/MasterData/chr_m0_iso.aspx");
        routes.MapPageRoute("chr-assign", "chr-assign", "~/websystem/MasterData/chr_m0_assign.aspx");


        routes.MapPageRoute("nmon_typetopic", "nmon_typetopic", "~/websystem/MasterData/nmon_typetopic.aspx");
        routes.MapPageRoute("nmon_topic", "nmon_topic", "~/websystem/MasterData/nmon_topic.aspx");



        routes.MapPageRoute("chr-approve", "chr-approve", "~/websystem/ITServices/CHR_Approve.aspx");

        // MA Online
        routes.MapPageRoute("ma-online", "ma-online", "~/websystem/ITServices/Ma_Online.aspx");

        // EN Repair
        routes.MapPageRoute("enrepair", "enrepair", "~/websystem/EN/ENRepair.aspx");
        routes.MapPageRoute("en-typeitem", "en-typeitem", "~/websystem/MasterData/en_m0_typeitem.aspx");
        routes.MapPageRoute("en-standard", "en-standard", "~/websystem/MasterData/en_m0_standard.aspx");
        routes.MapPageRoute("en-method", "en-method", "~/websystem/MasterData/en_m0_method.aspx");
        routes.MapPageRoute("en-tooling", "en-tooling", "~/websystem/MasterData/en_m0_tooling.aspx");
        routes.MapPageRoute("en-timing", "en-timing", "~/websystem/MasterData/en_m0_timming.aspx");
        routes.MapPageRoute("en-content", "en-content", "~/websystem/MasterData/en_m0_content.aspx");
        routes.MapPageRoute("en-form-result", "en-form-result", "~/websystem/MasterData/en_u0_form_result.aspx");
        //routes.MapPageRoute("en-planning", "en-planning", "~/websystem/EN/enp_planning.aspx");

        routes.MapPageRoute("en-planning", "en-planning/{link}", "~/websystem/EN/enp_planning.aspx", true, new RouteValueDictionary { { "link", "0" } });


        routes.MapPageRoute("rcm-setquestion", "rcm-setquestion", "~/websystem/MasterData/rcm_m0_setquestion.aspx");
        //   routes.MapPageRoute("rcm-question", "rcm-question", "~/websystem/hr/rcm_examination.aspx");
        routes.MapPageRoute("rcm-question", "rcm-question/{link}", "~/websystem/hr/rcm_examination.aspx", true, new RouteValueDictionary { { "link", "0" } });


        routes.MapPageRoute("rst-exitinterview", "rst-exitinterview", "~/websystem/MasterData/rst_m0_exitinterview.aspx");
        routes.MapPageRoute("hr-resignation", "hr-resignation", "~/websystem/hr/hr_resignation.aspx");
        routes.MapPageRoute("hr-manpower", "hr-manpower", "~/websystem/hr/hr_manpower.aspx");

        // start IT ASSET

        routes.MapPageRoute("it-asset", "it-asset", "~/websystem/ITServices/it-asset_new.aspx");
        routes.MapPageRoute("price-reference", "price-reference", "~/websystem/ITServices/Price-Reference.aspx");
        routes.MapPageRoute("its-typedevice", "its-typedevice", "~/websystem/MasterData/its_m0_typedevices.aspx");
        routes.MapPageRoute("its-device", "its-device", "~/websystem/MasterData/its_m1_typedevices.aspx");



        routes.MapPageRoute("it-asset-print", "it-asset-print", "~/websystem/ITServices/it_asset_print.aspx");
        routes.MapPageRoute("it-asset-print-org", "it-asset-print-org", "~/websystem/ITServices/it_asset_print_org.aspx");
        routes.MapPageRoute("it-asset-print-org-detail", "it-asset-print-org-detail", "~/websystem/ITServices/it_asset_print_org_detail.aspx");
        routes.MapPageRoute("it-asset-trnf", "it-asset-trnf", "~/websystem/ITServices/it-asset_new_transfer.aspx");

        // master
        routes.MapPageRoute("it-spec", "it-spec", "~/websystem/ITServices/its_m0_specit.aspx");
        routes.MapPageRoute("it-device-pr-status", "it-device-pr-status", "~/websystem/ITServices/its_m0_device_pr_status.aspx");

        // end IT ASSET

        routes.MapPageRoute("mis-profile", "mis-profile", "~/websystem/ITServices/mis-profile.aspx");
        routes.MapPageRoute("mis-tel", "mis-tel", "~/websystem/MasterData/mis_u0_tel.aspx");


        // Job
        routes.MapPageRoute("Manage-Job", "Manage-Job", "~/websystem/Job/ManageJobs.aspx");
        routes.MapPageRoute("Job-Application", "Job-Application", "~/websystem/Job/Job.aspx");
        routes.MapPageRoute("Job-INtroductory", "Job-INtroductory", "~/websystem/Job/JobINtroductory.aspx");


        //begin employee profile
        routes.MapPageRoute("Manage-Employee", "Manage-Employee", "~/websystem/ManageEmployee/ManageEmployee.aspx");
        routes.MapPageRoute("sarary", "sarary", "~/websystem/ManageEmployee/private_sarary.aspx");
        routes.MapPageRoute("profile", "profile", "~/websystem/ManageEmployee/Profile_Employee.aspx");
        routes.MapPageRoute("uploadfile", "uploadfile", "~/websystem/ManageEmployee/manage_employee.aspx");
        routes.MapPageRoute("warningLoginAgian", "warningLoginAgian", "~/websystem/warning_login_agian.aspx");
        routes.MapPageRoute("alertsuccess", "alertsuccess", "~/websystem/ManageEmployee/alert_success.aspx");
        //end employee profile

        // network deviecs
        routes.MapPageRoute("nwd-type", "nwd-type", "~/websystem/masterdata/nwd_m0_type.aspx");
        routes.MapPageRoute("nwd-category", "nwd-category", "~/websystem/masterdata/nwd_m0_category.aspx");
        routes.MapPageRoute("nwd-place", "nwd-place", "~/websystem/masterdata/nwd_m0_place.aspx");
        routes.MapPageRoute("nwd-room", "nwd-room", "~/websystem/masterdata/nwd_m0_room.aspx");
        routes.MapPageRoute("nwd-company", "nwd-company", "~/websystem/masterdata/nwd_m0_company.aspx");
        routes.MapPageRoute("nwd-floor", "nwd-floor", "~/websystem/masterdata/nwd_m0_floor.aspx");
        //routes.MapPageRoute("nwd-chamber", "nwd-chamber", "~/websystem/masterdata/nwd_m0_chamber.aspx");
        routes.MapPageRoute("nwd-position", "nwd-position", "~/websystem/masterdata/nwd_r0_position.aspx");
        routes.MapPageRoute("nwd-image", "nwd-image", "~/websystem/masterdata/nwd_m0_plan.aspx");
        routes.MapPageRoute("network-layout", "network-layout", "~/websystem/ITServices/Network-Layout.aspx");

        routes.MapPageRoute("network-devices", "network-devices", "~/websystem/ITServices/networkdevices.aspx");


        routes.MapPageRoute("network-devicesdetail", "network-devices-detail/{u0_devicenetwork_idx}", "~/websystem/ITServices/networkdevicesdetail.aspx", true, new RouteValueDictionary { { "u0_devicenetwork_idx", "0" } });
        // network deviecs

        // software license
        routes.MapPageRoute("detail-company", "detail-company", "~/websystem/masterdata/itswl_m0_company.aspx");
        routes.MapPageRoute("detail-software-department", "detail-software-department", "~/websystem/masterdata/itswl_m0_software.aspx");
        routes.MapPageRoute("detail-software", "detail-software", "~/websystem/masterdata/itswl_m0_softwarename.aspx");
        routes.MapPageRoute("software-license", "software-license", "~/websystem/ITServices/softwarelicense.aspx");
        // software license

        // google license
        routes.MapPageRoute("manage-google-license", "manage-google-license", "~/websystem/ITServices/google_license.aspx");
        routes.MapPageRoute("holder-google-license", "holder-google-license", "~/websystem/ITServices/holder_google_license.aspx");

        // google license

        // emps probation
        routes.MapPageRoute("form-evaluation", "form-evaluation", "~/websystem/MasterData/probation_m0_topics.aspx");

        //begin finger Management
        routes.MapPageRoute("finger-management", "finger-management", "~/websystem/fingerscan/fingerManagement.aspx");
        //end finger Management

        //begin Status alert 
        //begin network config ip address 
        routes.MapPageRoute("status-alert-config", "status-alert-config", "~/websystem/MasterData/statal-ip-address.aspx");
        routes.MapPageRoute("status-alert-report", "status-alert-report", "~/websystem/ITServices/status-alert.aspx");
        //end Status alert

        // it purchase
        routes.MapPageRoute("IT-Purchase", "IT-Purchase", "~/websystem/itpurchase/it_purchase.aspx");
        routes.MapPageRoute("print-purchase", "print-purchase", "~/websystem/itpurchase/purchase-print.aspx");
        routes.MapPageRoute("manage-equipment", "manage-equipment", "~/websystem/MasterData/master_purchase_equipment.aspx");


        //begin Active Directory Online 
        routes.MapPageRoute("active-directory-permission-category", "active-directory-permission-category", "~/websystem/MasterData/ad-permission-category.aspx");
        routes.MapPageRoute("active-directory-permission-type", "active-directory-permission-type", "~/websystem/MasterData/ad-permission-type.aspx");
        routes.MapPageRoute("active-directory-permission-policy", "active-directory-permission-policy", "~/websystem/MasterData/ad-permission-policy.aspx");
        routes.MapPageRoute("active-directory-employee-lifetime-type", "active-directory-employee-lifetime-type", "~/websystem/MasterData/ad-employee-type.aspx");
        routes.MapPageRoute("active-directory-online", "active-directory-online", "~/websystem/ITServices/ad-online.aspx");
        //end Active Directory Online

        // it device
        routes.MapPageRoute("devices-register", "devices-register", "~/websystem/ITServices/DeviceRegister.aspx");

        // dcc
        // car/par online
        routes.MapPageRoute("car-par-online", "car-par-online", "~/websystem/dcc/cp_online.aspx");
        // car/par online
        // dcc


        // mis : it news
        routes.MapPageRoute("it-news", "it-news", "~/websystem/ITServices/it-news.aspx");
        routes.MapPageRoute("manage-it-news", "manage-it-news", "~/websystem/MasterData/it-news-master.aspx");
        // Job Order
        routes.MapPageRoute("joborder", "joborder", "~/websystem/ITServices/joborder.aspx");
        routes.MapPageRoute("job-order", "job-order", "~/websystem/ITServices/job_order.aspx");
        //qa : qa lab
        routes.MapPageRoute("print-formcal", "print-formcal", "~/websystem/qa/labcal-formprint.aspx");
        routes.MapPageRoute("print-form", "print-form", "~/websystem/qa/lab-formprint.aspx");
        routes.MapPageRoute("print-samplecode-lab", "print-samplecode-lab", "~/websystem/qa/print_samplecode_qalab.aspx");
        routes.MapPageRoute("print-sample-cal", "print-sample-cal", "~/websystem/qa/labcal-sampleprint.aspx");
        routes.MapPageRoute("print-tool", "print-tool", "~/websystem/qa/labcal-tool-print.aspx");

        // MIS : IT Reference 
        routes.MapPageRoute("IT-Reference", "IT-Reference", "~/websystem/reference-price/reference-price.aspx");

        // MIS : employee data
        routes.MapPageRoute("mis-employee-data", "mis-employee-data", "~/websystem/mis/emp_data.aspx");

        // qa labis
        routes.MapPageRoute("labis-material", "labis-material", "~/websystem/MasterData/qa_m0_material.aspx");
        routes.MapPageRoute("labis-test-item", "labis-test-item", "~/websystem/MasterData/qa_lab_test_type.aspx");
        routes.MapPageRoute("labis-test-type", "labis-test-type", "~/websystem/MasterData/qa_lab_test_detail.aspx");
        routes.MapPageRoute("labis-place", "labis-place", "~/websystem/MasterData/qa_lab_place.aspx");
        routes.MapPageRoute("labis-set-testdetail", "labis-set-testdetail", "~/websystem/MasterData/qa_lab_set_test_detail.aspx");
        routes.MapPageRoute("labis-lab", "labis-lab", "~/websystem/MasterData/qa_lab_m0_lab.aspx");
        routes.MapPageRoute("labis-lab-print", "labis-lab-print", "~/websystem/qa/labis_print.aspx");
        routes.MapPageRoute("labis-lab-print-barcode", "labis-lab-print-barcode", "~/websystem/qa/labis_print_barcode.aspx");
        routes.MapPageRoute("labis-lab-details", "labis-lab-details/{u1_qalab_idx}", "~/websystem/qa/labis_details.aspx", true, new RouteValueDictionary { { "u1_qalab_idx", "0" } });
        routes.MapPageRoute("labis", "labis", "~/websystem/qa/labis.aspx");
        routes.MapPageRoute("labis-print-report-analysis", "labis-print-report-analysis", "~/websystem/qa/labis_print_report_analysis.aspx");
        routes.MapPageRoute("labis-print-recive-date", "labis-print-recive-date/{u1_qalab_idx}", "~/websystem/qa/labis_print_recive_date.aspx", true, new RouteValueDictionary { { "u1_qalab_idx", "0" } });


        // qa cims
        routes.MapPageRoute("cims-unit", "cims-unit", "~/websystem/MasterData/qa_cims_m0_unit.aspx");
        routes.MapPageRoute("cims-equipment-name", "cims-equipment-name", "~/websystem/MasterData/qa_cims_m0_equipment_name.aspx");
        routes.MapPageRoute("cims-equipment-type", "cims-equipment-type", "~/websystem/MasterData/qa_cims_m0_equipment_type.aspx");
        routes.MapPageRoute("cims-equipment-result", "cims-equipment-result", "~/websystem/MasterData/qa_cims_m0_equipment_result.aspx");
        routes.MapPageRoute("cims-place", "cims-place", "~/websystem/MasterData/qa_cims_m0_place.aspx");
        routes.MapPageRoute("cims-brand", "cims-brand", "~/websystem/MasterData/qa_cims_m0_brand.aspx");
        routes.MapPageRoute("cims-lab", "cims-lab", "~/websystem/MasterData/qa_cims_m0_lab.aspx");
        routes.MapPageRoute("cims-investigate", "cims-investigate", "~/websystem/MasterData/qa_cims_m0_investigate.aspx");
        routes.MapPageRoute("cims", "cims", "~/websystem/qa/cims.aspx");
        routes.MapPageRoute("cims-tool", "cims-tool", "~/websystem/qa/cims_register_tools.aspx");
        routes.MapPageRoute("cims-resolution", "cims-resolution", "~/websystem/MasterData/qa_cims_m0_equipment_resolution.aspx");
        routes.MapPageRoute("cims-frequency", "cims-frequency", "~/websystem/MasterData/qa_cims_m0_equipment_frequency.aspx");
        routes.MapPageRoute("cims-form-table", "cims-form-table", "~/websystem/MasterData/qa_cims_m0_form_calculate.aspx");
        routes.MapPageRoute("cims-signature", "cims-signature", "~/websystem/MasterData/qa_cims_m2_form_calculate.aspx");

        //master data sap
        routes.MapPageRoute("sap-module", "sap-module", "~/websystem/MasterData/m0_sap_module_lv1.aspx");
        routes.MapPageRoute("sap-title", "sap-title", "~/websystem/MasterData/m0_sap_module_lv2.aspx");
        routes.MapPageRoute("sap-type", "sap-type", "~/websystem/MasterData/m0_sap_module_lv3.aspx");
        routes.MapPageRoute("sap-reason", "sap-reason", "~/websystem/MasterData/m0_sap_module_lv4.aspx");
        routes.MapPageRoute("sap-how-to", "sap-how-to", "~/websystem/MasterData/m0_sap_module_lv5.aspx");

        // hr
        // master data
        routes.MapPageRoute("organization", "organization", "~/websystem/hr/hr_organization.aspx");
        routes.MapPageRoute("organizations", "organizations", "~/websystem/MasterData/cen_organization_m0.aspx");
        routes.MapPageRoute("workgroup", "workgroup", "~/websystem/MasterData/cen_workgroup_m0.aspx");
        routes.MapPageRoute("linework", "linework", "~/websystem/MasterData/cen_linework_m0.aspx");
        routes.MapPageRoute("department", "department", "~/websystem/MasterData/cen_department_m0.aspx");
        routes.MapPageRoute("section", "section", "~/websystem/MasterData/cen_section_m0.aspx");
        routes.MapPageRoute("position", "position", "~/websystem/MasterData/cen_position_m0.aspx");
        routes.MapPageRoute("empgroup", "empgroup", "~/websystem/MasterData/cen_employee_group_m0.aspx");
        routes.MapPageRoute("jobgrade", "jobgrade", "~/websystem/MasterData/cen_jobgrade_m0.aspx");
        routes.MapPageRoute("joblevel", "joblevel", "~/websystem/MasterData/cen_joblevel_m0.aspx");
        routes.MapPageRoute("costcenter", "costcenter", "~/websystem/MasterData/cen_costcenter_m0.aspx");

        routes.MapPageRoute("hr_news_Demo", "hr_news_Demo", "~/websystem/hr/hr_news_Demo.aspx");
        routes.MapPageRoute("hr-news", "hr-news", "~/websystem/hr/hr_news.aspx");

        routes.MapPageRoute("import-employee-nat", "import-employee-nat", "~/websystem/hr/import_employee_nat.aspx");

        // master data

        // webevent
        routes.MapPageRoute("new-year-party", "new-year-party", "~/webevent/tknHny2018.aspx");

        //health check
        routes.MapPageRoute("health-check-print", "health-check-print", "~/websystem/hr/hr_health_check_print.aspx");
        routes.MapPageRoute("health-check", "health-check", "~/websystem/hr/hr_health_check.aspx");
        routes.MapPageRoute("health-check-doctor", "health-check-doctor", "~/websystem/MasterData/hc-doctor-history.aspx");

        //master data daily check
        routes.MapPageRoute("drc-devices", "drc-devices", "~/websystem/MasterData/drc_m0_devices_name.aspx");
        routes.MapPageRoute("drc-placework", "drc-placework", "~/websystem/MasterData/drc_m0_typework.aspx");
        routes.MapPageRoute("drc-manage-set", "drc-manage-set", "~/websystem/MasterData/drc_m0_manage_setname.aspx");

        //room booking
        //routes.MapPageRoute("room-booking", "room-booking", "~/websystem/hr/hr_room_booking.aspx");
        routes.MapPageRoute("room-booking", "room-booking/{link_u0_room_idx}", "~/websystem/hr/hr_room_booking.aspx", true, new RouteValueDictionary { { "link_u0_room_idx", "0" } });
        routes.MapPageRoute("equiment-room", "equiment-room", "~/websystem/MasterData/hr_rbk_m0_equiment.aspx");
        routes.MapPageRoute("food-room", "food-room", "~/websystem/MasterData/hr_rbk_m0_food.aspx");
        routes.MapPageRoute("place-room", "place-room", "~/websystem/MasterData/hr_rbk_m0_place.aspx");
        routes.MapPageRoute("result-use-room", "result-use-room", "~/websystem/MasterData/hr_rbk_m0_result_use.aspx");
        routes.MapPageRoute("room-detail", "room-detail", "~/websystem/MasterData/hr_rbk_m0_room.aspx");
        routes.MapPageRoute("conference-device", "conference-device", "~/websystem/MasterData/hr_rbk_m0_conference.aspx");

        //routes.MapPageRoute("drc-manage-set", "drc-manage-set", "~/websystem/MasterData/drc_m0_manage_setname.aspx");
        //routes.MapPageRoute("JsonResponse", "JsonResponse", "~/websystem/hr/JsonResponse.ashx");
        //room booking

        //car booking
        routes.MapPageRoute("car-detail", "car-detail", "~/websystem/MasterData/cbk_m0_car_detail.aspx");
        routes.MapPageRoute("expenses-car", "expenses-car", "~/websystem/MasterData/cbk_m0_expenses.aspx");
        routes.MapPageRoute("place-car", "place-car", "~/websystem/MasterData/cbk_m0_place.aspx");
        routes.MapPageRoute("place-test", "place-test", "~/websystem/MasterData/cbk_m0_placetest.aspx");
        routes.MapPageRoute("car-booking", "car-booking", "~/websystem/hr/hr_car_booking.aspx");
        //car booking

        //Master Feedback IT
        routes.MapPageRoute("MFeedBack1", "MFeedBack1", "~/websystem/masterdata/M0_FeedBack_1.aspx");
        routes.MapPageRoute("MFeedBack2", "MFeedBack2", "~/websystem/masterdata/M0_FeedBack_2.aspx");
        routes.MapPageRoute("MFeedBack3", "MFeedBack3", "~/websystem/masterdata/M0_FeedBack_3.aspx");
        routes.MapPageRoute("UFeedBack", "UFeedBack", "~/websystem/masterdata/UFeedBack.aspx");
        //Master Feedback TMMS   
        routes.MapPageRoute("MFeedBack1TMMS", "MFeedBack1TMMS", "~/websystem/masterdata/M0_FeedBack_1TMMS.aspx");
        routes.MapPageRoute("TFeedBack1", "TFeedBack1", "~/websystem/masterdata/T0_FeedBack_1.aspx");
        routes.MapPageRoute("TFeedBack2", "TFeedBack2", "~/websystem/masterdata/T0_FeedBack_2.aspx");
        routes.MapPageRoute("TFeedBack3", "TFeedBack3", "~/websystem/masterdata/T0_FeedBack_3.aspx");

        //Master Product 
        routes.MapPageRoute("MD-ProductType", "MD-ProductType", "~/websystem/masterdata/MD-ProductType.aspx");
        routes.MapPageRoute("MD-ProductList", "MD-ProductList", "~/websystem/masterdata/MD-ProductList.aspx");

        //Start elearning
        //Master
        routes.MapPageRoute("Trainingm", "Trainingm", "~/websystem/elearning/master_el/el_m0_training.aspx");
        routes.MapPageRoute("Group-course", "Group-course", "~/websystem/elearning/master_el/el_m0_training_type.aspx");
        routes.MapPageRoute("Training-group", "Training-group", "~/websystem/elearning/master_el/el_m0_training_group.aspx");
        routes.MapPageRoute("Training-branch", "Training-branch", "~/websystem/elearning/master_el/el_m0_training_branch.aspx");
        routes.MapPageRoute("Training-organization", "Training-organization", "~/websystem/elearning/master_el/el_m0_training_organ.aspx");
        routes.MapPageRoute("schedule-training-day", "schedule-training-day", "~/websystem/elearning/master_el/el_m0_sche_trn_day.aspx");
        routes.MapPageRoute("E-mail-training", "E-mail-training", "~/websystem/elearning/master_el/el_m0_email.aspx");
        routes.MapPageRoute("training-level", "training-level", "~/websystem/elearning/master_el/el_m0_level.aspx");
        routes.MapPageRoute("evaluation-group", "evaluation-group", "~/websystem/elearning/master_el/el_m0_evaluation_group.aspx");
        routes.MapPageRoute("evaluation-form", "evaluation-form", "~/websystem/elearning/master_el/el_m0_evaluation_form.aspx");
        routes.MapPageRoute("target-group", "target-group", "~/websystem/elearning/master_el/el_m0_target_group.aspx");
        routes.MapPageRoute("institution", "institution", "~/websystem/elearning/master_el/el_m0_institution.aspx");
        routes.MapPageRoute("lecturer", "lecturer", "~/websystem/elearning/master_el/el_m0_lecturer.aspx");
        routes.MapPageRoute("objective", "objective", "~/websystem/elearning/master_el/el_m0_objective.aspx");
        routes.MapPageRoute("meetingroom_management", "meetingroom_management", "~/websystem/elearning/management_el/el_rbk_m0_meetingroom.aspx");
        routes.MapPageRoute("training-iso", "training-iso", "~/websystem/elearning/master_el/el_m0_iso.aspx");
        routes.MapPageRoute("meetingroom_management_file", "meetingroom_management_file", "~/websystem/elearning/management_el/el_rbk_m0_meetingroom_file.aspx");

        //transection
        routes.MapPageRoute("TrainingNeedsSurvey", "TrainingNeedsSurvey", "~/websystem/elearning/transection_el/el_TrnNeedsSurvey.aspx");
        routes.MapPageRoute("TrainingPlan", "TrainingPlan", "~/websystem/elearning/transection_el/el_TrnPlan.aspx");
        routes.MapPageRoute("syllabus", "syllabus", "~/websystem/elearning/transection_el/el_TrnCourse.aspx");
        routes.MapPageRoute("Video", "Video", "~/websystem/elearning/transection_el/el_TrnVideo.aspx");
        routes.MapPageRoute("el_TrnCourse_el", "el_TrnCourse_el", "~/websystem/elearning/transection_el/el_TrnCourse_el.aspx");
        routes.MapPageRoute("el_manage", "el_manage", "~/websystem/elearning/management_el/el_management.aspx");
        routes.MapPageRoute("trainingcourse", "trainingcourse", "~/websystem/elearning/transection_el/el_Trn_plan_course.aspx");
        routes.MapPageRoute("trainingcourse-register", "trainingcourse-register", "~/websystem/elearning/transection_el/el_Trn_plan_course_register.aspx");
        routes.MapPageRoute("el-std", "el-std", "~/websystem/elearning/transection_el/std.aspx");
        routes.MapPageRoute("el_TrnCourse_evaluation", "el_TrnCourse_evaluation/{id}", "~/websystem/elearning/transection_el/el_Trn_plan_course_evaluform.aspx", true, new RouteValueDictionary { { "id", "0" } });
        routes.MapPageRoute("el_TrnCourse_qrcode", "el_TrnCourse_qrcode/{id}", "~/websystem/elearning/transection_el/el_Trn_plan_course_qrcode.aspx", true, new RouteValueDictionary { { "id", "0" } });
        routes.MapPageRoute("el_TrnCourse_test", "el_TrnCourse_test/{id}/{type}", "~/websystem/elearning/transection_el/el_Trn_plan_course_test.aspx", true, new RouteValueDictionary { { "id", "0" }, { "type", "training" } });
        routes.MapPageRoute("el_TrnCourse_std", "el_TrnCourse_std", "~/websystem/elearning/transection_el/el_Trn_plan_course_std.aspx");
        routes.MapPageRoute("el-video-play", "el-video-play", "~/websystem/elearning/transection_el/el_video_play.aspx");

        //report
        routes.MapPageRoute("el_rpt_trainingplan", "el_rpt_trainingplan", "~/websystem/elearning/report_el/el_rpt_trainingplan.aspx");

        //en
        routes.MapPageRoute("en_report_plan", "en_report_plan", "~/websystem/en/en_rpt_plan.aspx");

        //End eleaning

        //Start hr_evaluation
        //transection hr_evaluation
        routes.MapPageRoute("hr-perf-evalu-employee", "hr-perf-evalu-employee", "~/websystem/hr/hr_perf_evalu_emp.aspx");
        routes.MapPageRoute("hr-perf-evalu-employee-monthly", "hr-perf-evalu-employee-monthly", "~/websystem/hr/hr_perf_evalu_emp_monthly.aspx");
        routes.MapPageRoute("hr-perf-evalu-employee-profile", "hr-perf-evalu-employee-profile", "~/websystem/hr/hr_perf_evalu_emp_profile.aspx");
        routes.MapPageRoute("hr-perf-evalu-emp-profile-print", "hr-perf-evalu-emp-profile-print", "~/websystem/hr/hr_perf_evalu_emp_profile_print.aspx");

        //End hr_evaluation

        routes.MapPageRoute("hr-employee", "hr-employee", "~/websystem/hr/employeeform.aspx");
        routes.MapPageRoute("hr-employee-register", "hr-employee-register", "~/websystem/hr/employee_register.aspx");
        routes.MapPageRoute("hr-employee-recurit", "hr-employee-recurit", "~/websystem/hr/employee_recurit.aspx");
        routes.MapPageRoute("hr-employee-profile", "hr-employee-profile", "~/websystem/hr/employee_profile.aspx");
        routes.MapPageRoute("hr-employee-mange-position", "hr-employee-mange-position", "~/websystem/hr/employee_mange_position.aspx");
        routes.MapPageRoute("hr-employee-master-visatype", "hr-employee-master-visatype", "~/websystem/hr/hr_visatype.aspx");
        routes.MapPageRoute("hr-employee-group-shift", "hr-employee-group-shift", "~/websystem/hr/employee_group_shift.aspx");
        routes.MapPageRoute("hr-employee-partime", "hr-employee-partime", "~/websystem/hr/employee_partime.aspx");
        routes.MapPageRoute("hr-employee-shift", "hr-employee-shift", "~/websystem/hr/employee_shift.aspx");
        routes.MapPageRoute("hr-employee-permission", "hr-employee-permission", "~/websystem/hr/hr_permission.aspx");
        routes.MapPageRoute("recruit-vdo", "recruit-vdo", "~/websystem/hr/employee_recruit_vdo.aspx");

  
        // HR : Recurit V2

        routes.MapPageRoute("hr-employee-register-v2", "hr-employee-register-v2", "~/websystem/hr/employee_register_v2.aspx");
        routes.MapPageRoute("hr-employee-recurit-v2", "hr-employee-recurit-v2", "~/websystem/hr/employee_recurit_v2.aspx");

        routes.MapPageRoute("recruit_exam", "recruit_exam", "~/websystem/hr/employee_recruit_exam.aspx");

        // TM : Finger Template, master data
        routes.MapPageRoute("tm-fingerprint-master", "tm-fingerprint-master", "~/websystem/fingerscan/master_machine.aspx");
        routes.MapPageRoute("tm-fingerprint-management", "tm-fingerprint-management", "~/websystem/fingerscan/fingerprint_manage.aspx");
        // TM : Finger Template, master data

        // LP : Lunch Project
        routes.MapPageRoute("lunch-project-report", "lunch-project-report", "~/websystem/hr/lunch_project_report.aspx");
        // LP : Lunch Project

        // hr overtime
        routes.MapPageRoute("hr-overtime", "hr-overtime", "~/websystem/hr/hr_overtime.aspx");
        routes.MapPageRoute("hr-overtime_rotate", "hr-overtime_rotate", "~/websystem/hr/hr_overtime_shiftrotate.aspx");
        routes.MapPageRoute("hr-overtime_report", "hr-overtime_report", "~/websystem/hr/hr_overtime_report.aspx");
        // hr overtime

        // laws
        routes.MapPageRoute("laws-trademarks-profile", "laws-trademarks-profile", "~/websystem/laws/law_trademarks_profile.aspx");
        routes.MapPageRoute("laws-trademarks-contract", "laws-trademarks-contract", "~/websystem/laws/law_trademarks_contract.aspx");
        routes.MapPageRoute("laws-trademarks-recordsheet", "laws-trademarks-recordsheet", "~/websystem/laws/law_trademark_recordsheet.aspx");
        routes.MapPageRoute("laws-doctype", "laws-doctype", "~/websystem/MasterData/tcm_m0_doctype.aspx");
        routes.MapPageRoute("laws-subdoctype", "laws-subdoctype", "~/websystem/MasterData/tcm_m0_subdoctype.aspx");
        routes.MapPageRoute("laws-trademarks-permise", "laws-trademarks-permise", "~/websystem/laws/law_trademark_permise.aspx");
        // laws

        // hr corporate kpi
        routes.MapPageRoute("hr-corporate-kpi", "hr-corporate-kpi", "~/websystem/hr/corporate_kpi.aspx");

        // VM : Visitor Management
        routes.MapPageRoute("vm-visitors", "vm-visitors", "~/websystem/visitors/visitors.aspx");

        routes.MapPageRoute("vm-security-login", "vm-security-login", "~/websystem/visitors/security_login.aspx");
        routes.MapPageRoute("vm-security", "vm-security", "~/websystem/visitors/security_main.aspx");
        routes.MapPageRoute("vm-security-board", "vm-security-board", "~/websystem/visitors/security_board.aspx");
        // VM : Visitor Management

        // HR : Report Time
        routes.MapPageRoute("hr-report-time", "hr-report-time", "~/websystem/hr/hr_report_time.aspx");
        routes.MapPageRoute("hr-report-time-payroll", "hr-report-time-payroll", "~/websystem/hr/hr_report_time_payroll.aspx");
        routes.MapPageRoute("hr-report-dashboard", "hr-report-dashboard", "~/websystem/hr/hr_report_dashboard.aspx");

        // HR : M0 Costcenter
        routes.MapPageRoute("hr-costcenter", "hr-costcenter", "~/websystem/hr/hr_costcenter.aspx");

        // HR : M0 Asset Holder
        routes.MapPageRoute("hr-asset-type", "hr-asset-type", "~/websystem/hr/hr_asset_type.aspx");
        routes.MapPageRoute("hr-unit-type", "hr-unit-type", "~/websystem/hr/hr_unit_type.aspx");

        // HR : TPM 
        routes.MapPageRoute("tpm-master", "tpm-master", "~/websystem/MasterData/tpm_type_m1.aspx");
        routes.MapPageRoute("tpm-formresult", "tpm-formresult", "~/websystem/hr/tpm_form_result.aspx");
        routes.MapPageRoute("tpm-reports", "tpm-reports", "~/websystem/hr/tpm_evalution_result.aspx");

        // QA : QMR Problem
        routes.MapPageRoute("sdp", "sdp", "~/websystem/qa/qmr_problem.aspx");
        routes.MapPageRoute("sdp-supplier", "sdp-supplier", "~/websystem/qa/qmr_problem_supplier.aspx");
        routes.MapPageRoute("sdp-login", "sdp-login", "~/websystem/qa/qmr_problem_supplier_login.aspx");

        // HR : Blacklist
        routes.MapPageRoute("hr-blacklist", "hr-blacklist", "~/websystem/hr/hr_blacklist.aspx");
        routes.MapPageRoute("hr-m0-blacklist", "hr-m0-blacklist", "~/websystem/MasterData/m0_blacklist.aspx");

        // PRD : FSC Random
        routes.MapPageRoute("fsc-random", "fsc-random", "~/websystem/prd/fsc_random.aspx");
        routes.MapPageRoute("fsc-survey-list", "fsc-survey-list", "~/websystem/prd/fsc_survey_list.aspx");

        // HR : Plan Sale
        routes.MapPageRoute("hr-plansale", "hr-plansale", "~/websystem/hr/hr_plansale.aspx");

        // MIS : load file for SAP
        routes.MapPageRoute("sap-load-file", "sap-load-file", "~/websystem/sap/sap_attach.aspx");

        // HR : slip
        routes.MapPageRoute("hr-slip", "hr-slip", "~/websystem/hr/hr_slip.aspx");
        routes.MapPageRoute("hr-tkn-slip", "hr-tkn-slip", "~/websystem/hr/hr_tkn_slip.aspx");

        // HR : e-traing request m0
        routes.MapPageRoute("el-m0-course", "el-m0-course", "~/websystem/elearning/master_el/el_m0_course_costcenter.aspx");
        routes.MapPageRoute("el-m0-expert-salary", "el-m0-expert-salary", "~/websystem/elearning/master_el/el_m0_expert_salary.aspx");

        // Ecommerce : load file slip online
        routes.MapPageRoute("ecommerce-loadfile-slip", "ecommerce-loadfile-slip", "~/websystem/ecommerce/ecommerce_attach.aspx");
        routes.MapPageRoute("ecommerce-sales-report", "ecommerce-sales-report", "~/websystem/ecommerce/ecom_report.aspx");
        routes.MapPageRoute("ecommerce-fileinterface", "ecommerce-fileinterface", "~/websystem/ecommerce/ecom_fileinterface.aspx");
        routes.MapPageRoute("ecommerce-sale-unit", "ecommerce-sale-unit", "~/websystem/MasterData/ecom_m0_saleunit.aspx");
        routes.MapPageRoute("ecommerce-set-promotion", "ecommerce-set-promotion", "~/websystem/MasterData/ecom_m0_promotion.aspx");
        routes.MapPageRoute("ecommerce-set-channel", "ecommerce-set-channel", "~/websystem/MasterData/ecom_m0_channel.aspx");
        routes.MapPageRoute("ecommerce-set-material", "ecommerce-set-material", "~/websystem/MasterData/ecom_m0_material.aspx");
        routes.MapPageRoute("ecommerce-bom", "ecommerce-bom", "~/websystem/MasterData/ecom_m0_bom.aspx");

        // Investor : Board Conference
        routes.MapPageRoute("conference", "conference", "~/websystem/conference/login_conference.aspx");
        routes.MapPageRoute("admin-conference", "admin-conference", "~/websystem/conference/admin_conference.aspx");

        // EPN  :  Master
        routes.MapPageRoute("epn_m0_supplier", "epn_m0_supplier", "~/websystem/EPN/epn_m0_supplier.aspx");
        routes.MapPageRoute("epn_m0_type", "epn_m0_type", "~/websystem/EPN/epn_m0_type.aspx");
        routes.MapPageRoute("epn_certificate", "epn_certificate", "~/websystem/EPN/epn_certificate.aspx");

        // HR : PMS //
        routes.MapPageRoute("pms-master", "pms-master", "~/websystem/MasterData/pms_setmaster_m0.aspx");
        routes.MapPageRoute("pms-performance", "pms-performance", "~/websystem/hr/pms_performance.aspx");
        routes.MapPageRoute("pms-management", "pms-management", "~/websystem/hr/pms_management.aspx");
        routes.MapPageRoute("pms-reports", "pms-reports", "~/websystem/hr/pms_reports.aspx");

        // HR : Checkin
        routes.MapPageRoute("hr-checkin-report", "hr-checkin-report", "~/websystem/hr/hr_checkin_report.aspx");
    }
</script>
