﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_mange_position.aspx.cs" Inherits="websystem_hr_employee_mange_position" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <%----------- Menu Tab Start---------------%>
            <div id="BoxTabMenuIndex" runat="server">
                <div class="form-group">
                    <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                        <div class="container-fluid bg-red">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="sub-navbar">
                                    <a class="navbar-brand" href="#">Menu</a>
                                </div>
                            </div>

                            <div class="collapse navbar-collapse" id="Menu1">
                                <asp:LinkButton ID="lbadd_position" CssClass="btn_menulist" runat="server" CommandName="btnadd_position" OnCommand="btnCommand" CommandArgument="1" Text="ตำแหน่งรับสมัครงาน"></asp:LinkButton>
                                <asp:LinkButton ID="lbadd_groupposition" CssClass="btn_menulist" runat="server" CommandName="btnadd_groupposition" OnCommand="btnCommand" CommandArgument="2" Text="กลุ่มงาน"></asp:LinkButton>
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
            <asp:HiddenField ID="Hddfld_folderImage" runat="server" />
            <%-------------- Menu Tab End--------------%>

            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewPosition" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ตำแหน่งรับสมัครงาน</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnAddMaPosition" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddStatus" runat="server" CommandName="btnAddMaPosition" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <asp:Panel ID="Pn_Position" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; เพิ่มตำแหน่งรับสมัครงาน</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อตำแหน่ง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Name_add" runat="server" CssClass="form-control" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtP_Name_add" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกตำแหน่งงาน"
                                                                ValidationExpression="กรุณากรอกตำแหน่งงาน"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                ValidationGroup="SaveDataSet" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtP_Name_add"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" runat="server" Text="กลุ่มงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlgroupposition_add" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กลุ่มงาน...." />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="รายละเอียดงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Description_add" runat="server" autocomplete="off" TextMode="MultiLine" Rows="7" CssClass="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="สถานที่ปฏิบัติงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Location_add" runat="server" CssClass="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label8" runat="server" Text="คุณสมบัติ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Property_add" runat="server" autocomplete="off" TextMode="MultiLine" Rows="7" CssClass="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="อัตรา" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Unit_add" runat="server" CssClass="form-control" />
															<asp:RequiredFieldValidator ID="RequiredFiator8" ValidationGroup="SaveDataSet" runat="server" Display="None" ControlToValidate="txtP_Unit_add" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกอัตรา"
                                                                ValidationExpression="กรุณากรอกอัตรา"
                                                                SetFocusOnError="true" />
															<asp:RegularExpressionValidator ID="R_unit_add" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtP_Unit_add" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveDataSet" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Unit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_unit_add" Width="160" />
											<ajaxToolkit:ValidatorCalloutExtender ID="Valida4ender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiator8" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label9" runat="server" Text="เงินเดือน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Salary_add" runat="server" CssClass="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label10" runat="server" Text="ติดต่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Contract_add" runat="server" CssClass="form-control" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label11" runat="server" Text="ความสำคัญ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlPiority_add" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="เลือกความสำคัญ...." />
                                                                <asp:ListItem Value="1" Text="ปกติ" />
                                                                <asp:ListItem Value="2" Text="ด่วน" />
                                                            </asp:DropDownList>
															<asp:RequiredFieldValidator ID="Requirator10" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                ControlToValidate="ddlPiority_add" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกความสำคัญ"
                                                                ValidationExpression="กรุณาเลือกความสำคัญ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirator10" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddStatus_add" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label15" CssClass="col-sm-3 control-label" runat="server" Text="กำหนดแบบทดสอบ" />
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlchoose" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="ไม่มีแบบทดสอบ" />
                                                                <asp:ListItem Value="1" Text="มีแบบทดสอบ" />

                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="div_typequest" runat="server" visible="false">
                                                        <asp:Label ID="Label20" runat="server" Text="ระบุประเภทคำถาม" CssClass="col-sm-3 control-label"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddltypequest" ValidationGroup="SaveDataSet" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำถาม"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                ControlToValidate="ddltypequest" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทคำถาม"
                                                                ValidationExpression="กรุณาเลือกประเภทคำถาม" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                        </div>
                                                    </div>

                                                    <div id="divchoosetype" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddlOrg" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddlDep" ValidationGroup="SaveTopic" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddlSec" ValidationGroup="SaveTopic" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label14" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                            <div class="col-sm-6">
                                                                <asp:DropDownList ID="ddlPos" ValidationGroup="SaveTopic" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                                    <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                    </div>


                                                    <div class="form-group" id="div_question" runat="server" visible="false">
                                                        <asp:Label ID="Label12" runat="server" Text="ชุดคำถาม" CssClass="col-sm-3 control-label"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddltopic" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกชุดคำถาม"></asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                                ControlToValidate="ddltopic" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกชุดคำถาม"
                                                                ValidationExpression="กรุณาเลือกชุดคำถาม" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-warning btn-sm" runat="server" Text="ADD +" ValidationGroup="SaveDataSet" OnCommand="btnCommand" CommandName="CmdAdddataset"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="GvTopicName"
                                                                        runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None"
                                                                        HeaderStyle-CssClass="info"
                                                                        OnRowDataBound="Master_RowDataBound"
                                                                        Visible="false"
                                                                        OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">


                                                                        <PagerStyle CssClass="pageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>


                                                                            <asp:TemplateField HeaderText="ตำแหน่งเปิดรับสมัคร" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <div class="word-wrap">
                                                                                        <asp:Label ID="lblP_Name" runat="server" Text='<%# Eval("P_Name") %>' />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ระบุประเภทคำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <div class="word-wrap">
                                                                                        <asp:Label ID="lbltypequest" runat="server" Text='<%# Eval("typequest") %>' />
                                                                                        <asp:Label ID="lblm0tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField HeaderText="ชุดคำถาม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>

                                                                                    <asp:Label ID="lbltopic_name" runat="server" Text='<%# Eval("Topic_name") %>' />
                                                                                    <asp:Label ID="lblm0toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btndeleteQuiz" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeleteQuiz"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                                </ItemTemplate>

                                                                                <EditItemTemplate />
                                                                                <FooterTemplate />
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                        <div class="col-sm-2">
                                                        </div>
                                                    </div>




                                                    <div class="form-group" id="btnsave_cancel" runat="server" visible="true">
                                                        <div class="col-sm-2 col-sm-offset-3">
                                                            <asp:LinkButton ID="lbladd" ValidationGroup="SaveDataSet" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd_ma_position" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lblcancel_pos" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_ma_position" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaPosition" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="PIDX"
                                PageSize="15"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblPIDX" runat="server" Visible="false" Text='<%# Eval("PIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtPIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("PIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อตำแหน่ง" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Name_update" runat="server" CssClass="form-control" Text='<%# Eval("P_Name")%>' />

															<asp:RequiredFieldValidator ID="Requ3ator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtP_Name_update" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกตำแหน่งงาน"
                                                                ValidationExpression="กรุณากรอกตำแหน่งงาน"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requ3ator8" Width="160" />
                                                            <asp:RegularExpressionValidator ID="Regulaeidator1" runat="server"
                                                                ValidationGroup="Save" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtP_Name_update"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Vali1er12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regulaeidator1" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" runat="server" Text="กลุ่มงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <asp:Label ID="lblPosGroupIDX_update" runat="server" Visible="false" Text='<%# Eval("PosGroupIDX")%>'></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlgroupposition_update" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กลุ่มงาน...." />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="รายละเอียดงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Description_update" runat="server" autocomplete="off" TextMode="MultiLine" Rows="7" CssClass="form-control" Text='<%# Eval("P_Description")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="สถานที่ปฏิบัติงาน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Location_update" runat="server" CssClass="form-control" Text='<%# Eval("P_Location")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label8" runat="server" Text="คุณสมบัติ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Property_update" runat="server" autocomplete="off" TextMode="MultiLine" Rows="7" CssClass="form-control" Text='<%# Eval("P_Property")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="อัตรา" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Unit_update" runat="server" CssClass="form-control" Text='<%# Eval("P_Unit")%>' />
															<asp:RegularExpressionValidator ID="R_unit_update" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtP_Unit_update" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="Save" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="VS_Unit_update" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_unit_update" Width="160" />

											<asp:RequiredFieldValidator ID="Requir8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtP_Unit_update" Font-Size="11"
                                                                ErrorMessage="กรุณากรอกอัตรา"
                                                                ValidationExpression="กรุณากรอกอัตรา"
                                                                SetFocusOnError="true" />
											<ajaxToolkit:ValidatorCalloutExtender ID="Valida4ende4r12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir8" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label9" runat="server" Text="เงินเดือน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Salary_update" runat="server" CssClass="form-control" Text='<%# Eval("P_Salary")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label10" runat="server" Text="ติดต่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtP_Contract_update" runat="server" CssClass="form-control" Text='<%# Eval("P_Contract")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label11" runat="server" Text="ความสำคัญ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddlPiority_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("P_Piority") %>'>
                                                                <asp:ListItem Value="0" Text="เลือกความสำคัญ...." />
                                                                <asp:ListItem Value="1" Text="ปกติ" />
                                                                <asp:ListItem Value="2" Text="ด่วน" />
                                                            </asp:DropDownList>
															<asp:RequiredFieldValidator ID="Re10" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlPiority_update" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกความสำคัญ"
                                                                ValidationExpression="กรุณาเลือกความสำคัญ" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valen3220" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re10" Width="160" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("P_Status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-7">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ชื่อตำแหน่ง" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_Name" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานที่ปฏิบัติงาน" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_Location" runat="server" Text='<%# Eval("P_Location") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คุณสมบัติ" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_Property" runat="server" Text='<%# Eval("P_Property") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_Unit" runat="server" Text='<%# Eval("P_Unit") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เงินเดือน" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblP_Salary" runat="server" Text='<%# Eval("P_Salary") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%">
                                        <ItemTemplate>
                                            <asp:GridView ID="GvTopic" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="PIDX"
                                                OnRowDataBound="Master_RowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbltopicname" runat="server" Text='<%# Eval("topic_name") %>'></asp:Label>
                                                                <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>'></asp:Label></small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ผู้รับผิดชอบ" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%">
                                                        <ItemTemplate>
                                                            <asp:GridView ID="GvPerson" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                HeaderStyle-CssClass="success small"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="P_EmpIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <small>
                                                                                <asp:Label ID="lblFullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label></small>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                </Columns>
                                                            </asp:GridView>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                </Columns>
                                            </asp:GridView>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnDelete_ma_position" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("PIDX") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>

                <asp:View ID="ViewGroupPosition" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; กลุ่มงาน</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">

                                <asp:LinkButton ID="btnAddGroupPosition" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddGroupPosition" runat="server" CommandName="btnAddGroupPosition" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <asp:Panel ID="Pn_GroupPosition" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; เพิ่มกลุ่มงาน</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="ชื่อกลุ่มงาน(TH)" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtPosGroupNameTH_add" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="ชื่อกลุ่มงาน(EN)" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txtPosGroupNameEN_add" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlstatus_position" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1">Online</asp:ListItem>
                                                        <asp:ListItem Value="0">Offline</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButton2" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd_group_position" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_group_position" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvGroupPosition" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="PosGroupIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblPosGroupIDX" runat="server" Visible="false" Text='<%# Eval("PosGroupIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtPosGroupIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("PosGroupIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อกลุ่มงาน(TH)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtPosGroupNameTH_update" runat="server" CssClass="form-control" Text='<%# Eval("PosGroupNameTH")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ชื่อกลุ่มงาน(EN)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtPosGroupNameEN_update" runat="server" CssClass="form-control" Text='<%# Eval("PosGroupNameEN")%>' />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                        <div class="col-sm-6">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("PosGroupStatus") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-7">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ชื่อกลุ่มงาน(TH)" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อกลุ่มงาน(EN)" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdep" runat="server" Text='<%# Eval("PosGroupNameEN") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnDelete_group_position" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("PosGroupIDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>

            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

