﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="m0_blacklist.aspx.cs"
    Inherits="websystem_MasterData_m0_blacklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewindex" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnAdd" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม" data-toggle="tooltip" title="เพิ่ม" runat="server"
                    CommandName="cmdAdd" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Blacklist</asp:LinkButton>
            </div>

            <asp:FormView ID="fvsearch" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ค้นหา Blacklist </h3>
                        </div>

                        <div class="panel-body">

                            <div class="col-md-10 col-md-offset-1">

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เลือกข้อมูล Blacklist</label>
                                            <asp:DropDownList ID="ddlModule" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">Blacklist....</asp:ListItem>
                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ข้อมูล Blacklist</label>
                                            <asp:TextBox ID="txt_blacklist" placeholder="กรอกข้อมูล Blacklist" runat="server" CssClass="form-control"> </asp:TextBox>


                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnsearch" ValidationGroup="search" CssClass="btn btn-success" Text="ค้นหา" data-toggle="tooltip"
                                                title="ค้นหา" runat="server" CommandName="Cmdsearch" OnCommand="btnCommand"></asp:LinkButton>
                                            <asp:LinkButton ID="bthclear" ValidationGroup="search" CssClass="btn btn-default" Text="ล้างค่า" data-toggle="tooltip"
                                                title="ล้างค่า" runat="server" CommandName="Cmdclear" OnCommand="btnCommand"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">

                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูล Blacklist</h3>
                        </div>

                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <asp:Label ID="Blacklist" runat="server" Text="ข้อมูล blacklist" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtBlacklist" runat="server" CssClass="form-control" placeholder="กรอกชื่อ Blacklist ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_txtBlacklist" runat="server"
                                                    ControlToValidate="txtBlacklist" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อ Blacklist" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Re_txtBlacklist_name" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtBlacklist" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="txtStatus" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="btnSave" ValidationGroup="Save" CssClass="btn btn-success" Text="Save"
                                                    data-toggle="tooltip" title="บันทึก" runat="server" CommandName="CmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="CmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <div id="Gvblacklist_scroll" style="overflow-x: auto; width: 100%" runat="server">
                <asp:GridView ID="Gvblacklist" runat="server" Visible="true"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                    HeaderStyle-CssClass="success"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowEditing="Master_RowEditing"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    AutoPostBack="false">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <asp:Label ID="idx" runat="server" Visible="false" Text='<%# Eval("Blacklist_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txt_Blacklist_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("Blacklist_idx") %>'></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_blacklist_name_edit" CssClass="col-sm-3 control-label" runat="server" Text="ข้อมูล blacklist" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_blacklist_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("Blacklist_detail") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_blacklist_name_edit" runat="server"
                                                    ControlToValidate="txt_blacklist_name_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อ blacklist" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_blacklist_name_edit" Width="220" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_blacklist_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlblacklist_status_edit" Text='<%# Eval("Blacklist_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ข้อมูล Blacklist" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_test" runat="server"
                                            Text='<%# Eval("Blacklist_detail") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_status" Visible="false" runat="server"
                                            CssClass="col-sm-12" Text='<%# Eval("Blacklist_status") %>'></asp:Label>
                                        <asp:Label ID="Blacklist_statusOnline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Online"
                                            CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                        </asp:Label>
                                        <asp:Label ID="Blacklist_statusOffline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                        </asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px">
                                    <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                        data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="CmdDelete"
                                        data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                        CommandArgument='<%#Eval("Blacklist_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

        </asp:View>
    </asp:MultiView>
</asp:Content>
