﻿using System;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for function_dm
/// </summary>
public class function_dmu
{
    public function_dmu()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    data_employee _dataEmployee = new data_employee();
    data_en_planning _data_en_planning = new data_en_planning();

    string _localJson = "";
    //public string _PathFileImage = "~/images/elearning/";
    public string _IconnoImage = "noimage.jpg";
    public string _IconnoFile = "nofile.png";
    public string _IconFile = "file.png";
    //public string _PathFileImageUrl = "images/elearning/";
    //public string _PathFileuploadfilesUrl = "uploadfiles/elearning/";
    public string _HostServerDev = "dev.mas.taokaenoi.co.th";
    public string _HostServer = "mas.taokaenoi.co.th";
    public string _LikeDownloadQrCode = "http://demo.taokaenoi.co.th/el_TrnCourse_qrcode";
    public string _LikeGenQrCode_evaluation = "http://demo.taokaenoi.co.th/el_TrnCourse_evaluation/";
    public string _LikeGenQrCode_test = "http://demo.taokaenoi.co.th/el_TrnCourse_test/";
    public string _LikeMAS = "http://demo.taokaenoi.co.th/";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //org
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];



    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];
    public string _urlGetel_lu_TraningAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_TraningAll"];

    // public string _urlGetel_lu_TraningAll = _serviceUrl + "api_elearning.asmx/Getel_lu_TraningAll?jsonIn=";

    // EN
    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string _urlGeten_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGeten_lookup"];


    public string zDayTH(int ADay)
    {
        string sDay = "";
        if (ADay == 1)
        {
            sDay = "จันทร์";
        }
        else if (ADay == 2)
        {
            sDay = "อังคาร";
        }
        else if (ADay == 3)
        {
            sDay = "พุธ";
        }
        else if (ADay == 4)
        {
            sDay = "พฤหัสบดี";
        }
        else if (ADay == 5)
        {
            sDay = "ศุกร์";
        }
        else if (ADay == 6)
        {
            sDay = "เสาร์";
        }
        else if (ADay == 7)
        {
            sDay = "อาทิตย์";
        }
        return sDay;
    }
    public string zMonthTH(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "มกราคม"; }
        else if (AMonth == 2) { sMonth = "กุมภาพันธ์ "; }
        else if (AMonth == 3) { sMonth = "มีนาคม"; }
        else if (AMonth == 4) { sMonth = "เมษายน"; }
        else if (AMonth == 5) { sMonth = "พฤษภาคม"; }
        else if (AMonth == 6) { sMonth = "มิถุนายน"; }
        else if (AMonth == 7) { sMonth = "กรกฎาคม"; }
        else if (AMonth == 8) { sMonth = "สิงหาคม"; }
        else if (AMonth == 9) { sMonth = "กันยายน"; }
        else if (AMonth == 10) { sMonth = "ตุลาคม"; }
        else if (AMonth == 11) { sMonth = "พฤศจิกายน"; }
        else if (AMonth == 12) { sMonth = "ธันวาคม"; }
        return sMonth;
    }
    public string zMonthEN(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "January"; }
        else if (AMonth == 2) { sMonth = "February "; }
        else if (AMonth == 3) { sMonth = "March"; }
        else if (AMonth == 4) { sMonth = "April"; }
        else if (AMonth == 5) { sMonth = "May"; }
        else if (AMonth == 6) { sMonth = "June"; }
        else if (AMonth == 7) { sMonth = "July"; }
        else if (AMonth == 8) { sMonth = "August"; }
        else if (AMonth == 9) { sMonth = "September"; }
        else if (AMonth == 10) { sMonth = "October"; }
        else if (AMonth == 11) { sMonth = "November"; }
        else if (AMonth == 12) { sMonth = "December"; }
        return sMonth;
    }
    public string zMonthEN_Short(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "Jan"; }
        else if (AMonth == 2) { sMonth = "Feb "; }
        else if (AMonth == 3) { sMonth = "Mar"; }
        else if (AMonth == 4) { sMonth = "Apr"; }
        else if (AMonth == 5) { sMonth = "May "; }
        else if (AMonth == 6) { sMonth = "June"; }
        else if (AMonth == 7) { sMonth = "July"; }
        else if (AMonth == 8) { sMonth = "Aug"; }
        else if (AMonth == 9) { sMonth = "Sept"; }
        else if (AMonth == 10) { sMonth = "Oct"; }
        else if (AMonth == 11) { sMonth = "Nov"; }
        else if (AMonth == 12) { sMonth = "Dec"; }
        return sMonth;
    }
    public string zYearTH(int AYear)
    {
        string sYear = "";
        AYear = AYear + 543;
        sYear = AYear.ToString();
        return sYear;
    }

    public string zRun_Number(string Form_Name, string no_prefix, string option_name, string Run_flag, string num_width)
    {
        string _urlGetel_m0_runno = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_runno"];
        string _local_xml = string.Empty;
        string _runnoService = "runnoService";

        string sRunDocNo = "";
        m0_runno objM0runno = new m0_runno();
        _data_elearning.el_m0_runno_action = new m0_runno[1];
        objM0runno.m0_formname = Form_Name;
        objM0runno.m0_prefix = no_prefix;
        objM0runno.option_name = option_name;
        objM0runno.num_width = num_width;
        _data_elearning.el_m0_runno_action[0] = objM0runno;
        //_kdm_local_xml = kdm_servExec.actionExec(kdm_iBomConn, "data_ibom", kdm_runnoService, kdm_dataIBom, 21);
        //kdm_dataIBom = (data_ibom)_kdm_funcTool.convertXmlToObject(typeof(data_ibom), _kdm_local_xml);
        _data_elearning = zCallServiceNetwork(_urlGetel_m0_runno, _data_elearning);
        if (_data_elearning.return_code == 0)
        {
            sRunDocNo = _data_elearning.el_m0_runno_action[0].m0_docno;

        }
        else
        {
            sRunDocNo = "";
        }
        return sRunDocNo;
    }

    public string zMonthTH_Short(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "ม.ค."; }
        else if (AMonth == 2) { sMonth = "ก.พ."; }
        else if (AMonth == 3) { sMonth = "มี.ค."; }
        else if (AMonth == 4) { sMonth = "เม.ย."; }
        else if (AMonth == 5) { sMonth = "พ.ค."; }
        else if (AMonth == 6) { sMonth = "มิ.ย."; }
        else if (AMonth == 7) { sMonth = "ก.ค."; }
        else if (AMonth == 8) { sMonth = "ส.ค."; }
        else if (AMonth == 9) { sMonth = "ก.ย."; }
        else if (AMonth == 10) { sMonth = "ต.ค."; }
        else if (AMonth == 11) { sMonth = "พ.ย."; }
        else if (AMonth == 12) { sMonth = "ธ.ค."; }
        return sMonth;
    }
    public string zThaiBaht(string txt)
    {
        string bahtTxt, n, bahtTH = "";
        double amount;
        try { amount = Convert.ToDouble(txt); }
        catch { amount = 0; }
        bahtTxt = amount.ToString("####.00");
        string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
        string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
        string[] temp = bahtTxt.Split('.');
        string intVal = temp[0];
        string decVal = temp[1];
        if (Convert.ToDouble(bahtTxt) == 0)
            bahtTH = "ศูนย์บาทถ้วน";
        else
        {
            for (int i = 0; i < intVal.Length; i++)
            {
                n = intVal.Substring(i, 1);
                if (n != "0")
                {
                    if ((i == (intVal.Length - 1)) && (n == "1"))
                        bahtTH += "เอ็ด";
                    else if ((i == (intVal.Length - 2)) && (n == "2"))
                        bahtTH += "ยี่";
                    else if ((i == (intVal.Length - 2)) && (n == "1"))
                        bahtTH += "";
                    else
                        bahtTH += num[Convert.ToInt32(n)];
                    bahtTH += rank[(intVal.Length - i) - 1];
                }
            }
            bahtTH += "บาท";
            if (decVal == "00")
                bahtTH += "ถ้วน";
            else
            {
                for (int i = 0; i < decVal.Length; i++)
                {
                    n = decVal.Substring(i, 1);
                    if (n != "0")
                    {
                        if ((i == decVal.Length - 1) && (n == "1"))
                            bahtTH += "เอ็ด";
                        else if ((i == (decVal.Length - 2)) && (n == "2"))
                            bahtTH += "ยี่";
                        else if ((i == (decVal.Length - 2)) && (n == "1"))
                            bahtTH += "";
                        else
                            bahtTH += num[Convert.ToInt32(n)];
                        bahtTH += rank[(decVal.Length - i) - 1];
                    }
                }
                bahtTH += "สตางค์";
            }
        }
        return bahtTH;
    }
    public string zDateToDB(string date)
    {
        if (date != string.Empty)
        {
            var dateSplit = date.Split('/');
            try
            {
                date = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
            }
            catch {

                dateSplit = date.Split('.');
                date = dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
            }
            
            return date;
        }
        return string.Empty;
    }

    public void zGridLookUp(GridView Gv,
                            string AService,
                            string Aoperation_status_id = "",
                            int id = 0

        )
    {
        if (Aoperation_status_id == "")
        {
            if (AService == "m0_training")
            {
                m0_training obj = new m0_training();
                _data_elearning.el_m0_training_action = new m0_training[1];
                _data_elearning.el_m0_training_action[0] = obj;
                _data_elearning = zCallServiceNetwork(_urlGetel_m0_training, _data_elearning);
                zSetGridData(Gv, _data_elearning.el_m0_training_action);
            }
        }
        else
        {
            if (AService == "trainingLoolup")
            {
                trainingLoolup obj = new trainingLoolup();
                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj.operation_status_id = Aoperation_status_id;
                if (id > 0)
                {
                    obj.idx = id;
                }
                _data_elearning.trainingLoolup_action[0] = obj;
                _data_elearning = zCallServicePostNetwork(_urlGetel_lu_TraningAll, _data_elearning);
                zSetGridData(Gv, _data_elearning.trainingLoolup_action);
            }
        }

    }

    public void zDropDownList(DropDownList _ddl,
                                             string Alabel,
                                             string AFiledId,
                                             string AFiledName,
                                             string AFiledDefault,
                                             string AService,
                                             string Avalue = "",
                                             string Aoperation_status_id = ""
                                             )
    {


        //  DropDownList _ddl = new DropDownList();

        if ((AFiledId == null) || (AFiledName == null) || (AFiledId == "") || (AFiledName == ""))
        {
            return;
        }
        if (Alabel == "")
        {
            Alabel = "เลือก";
        }
        if (Aoperation_status_id == "")
        {
            if (AService == "m0_training")
            {
                m0_training obj = new m0_training();
                _data_elearning.el_m0_training_action = new m0_training[1];
                _data_elearning.el_m0_training_action[0] = obj;
                _data_elearning = zCallServiceNetwork(_urlGetel_m0_training, _data_elearning);
                _ddl.Items.Clear();
                _ddl.AppendDataBoundItems = true;
                if (Alabel == "-")
                {
                    _ddl.Items.Add(new ListItem(Alabel, AFiledDefault));
                }
                else
                {
                    _ddl.Items.Add(new ListItem("-- " + Alabel + " --", AFiledDefault));
                }

                _ddl.DataSource = _data_elearning.el_m0_training_action;
            }
        }
        else
        {
            if (AService == "trainingLoolup")
            {
                trainingLoolup obj = new trainingLoolup();
                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj.operation_status_id = Aoperation_status_id;
                _data_elearning.trainingLoolup_action[0] = obj;
                _data_elearning = zCallServicePostNetwork(_urlGetel_lu_TraningAll, _data_elearning);
                _ddl.Items.Clear();
                _ddl.AppendDataBoundItems = true;
                if (Alabel == "-")
                {
                    _ddl.Items.Add(new ListItem(Alabel, AFiledDefault));
                }
                else
                {
                    _ddl.Items.Add(new ListItem("-- " + Alabel + " --", AFiledDefault));
                }
                _ddl.DataSource = _data_elearning.trainingLoolup_action;
            }
        }
        _ddl.DataTextField = AFiledName;
        _ddl.DataValueField = AFiledId;
        _ddl.DataBind();

        if (Avalue != "")
        {
            _ddl.SelectedValue = Avalue.ToString();
        }

    }

    public void zDropDownListWhereID(DropDownList _ddl,
                                             string Alabel,
                                             string AFiledId,
                                             string AFiledName,
                                             string AFiledDefault,
                                             string AService,
                                             string Avalue = "",
                                             string Aoperation_status_id = "",
                                             string AWhereField = "",
                                             int AWhereID = 0
                                             )
    {


        //  DropDownList _ddl = new DropDownList();

        if ((AFiledId == null) || (AFiledName == null) || (AFiledId == "") || (AFiledName == ""))
        {
            return;
        }
        if (Alabel == "")
        {
            Alabel = "เลือก";
        }
        if (Aoperation_status_id == "")
        {
            if (AService == "m0_training")
            {
                m0_training obj = new m0_training();
                _data_elearning.el_m0_training_action = new m0_training[1];
                _data_elearning.el_m0_training_action[0] = obj;
                _data_elearning = zCallServiceNetwork(_urlGetel_m0_training, _data_elearning);
                _ddl.Items.Clear();
                _ddl.AppendDataBoundItems = true;
                if (Alabel == "-")
                {
                    _ddl.Items.Add(new ListItem(Alabel, AFiledDefault));
                }
                else
                {
                    _ddl.Items.Add(new ListItem("-- " + Alabel + " --", AFiledDefault));
                }

                _ddl.DataSource = _data_elearning.el_m0_training_action;
            }
        }
        else
        {
            if (AService == "trainingLoolup")
            {
                trainingLoolup obj = new trainingLoolup();
                _data_elearning.trainingLoolup_action = new trainingLoolup[1];
                obj.operation_status_id = Aoperation_status_id;
                if ((AWhereField != "") && (AWhereID >= 0))
                {
                    if (AWhereField == "idx")
                    {
                        obj.idx = AWhereID;
                    }
                    else if (AWhereField == "m0_training_group_idx_ref")
                    {
                        obj.m0_training_group_idx_ref = AWhereID;
                    }
                    else if (AWhereField == "zyear")
                    {
                        obj.zyear = AWhereID;
                    }
                    else if (AWhereField == "zId_G")
                    {
                        obj.zId_G = AWhereID;
                    }
                    else if (AWhereField == "DeptIDX")
                    {
                        obj.DeptIDX = AWhereID;
                    }
                }
                _data_elearning.trainingLoolup_action[0] = obj;
                _data_elearning = zCallServicePostNetwork(_urlGetel_lu_TraningAll, _data_elearning);
                _ddl.Items.Clear();
                _ddl.AppendDataBoundItems = true;
                if (Alabel == "-")
                {
                    _ddl.Items.Add(new ListItem(Alabel, AFiledDefault));
                }
                else
                {
                    _ddl.Items.Add(new ListItem("-- " + Alabel + " --", AFiledDefault));
                }
                _ddl.DataSource = _data_elearning.trainingLoolup_action;
            }
        }
        _ddl.DataTextField = AFiledName;
        _ddl.DataValueField = AFiledId;
        _ddl.DataBind();

        if (Avalue != "")
        {
            _ddl.SelectedValue = Avalue.ToString();
        }

    }

    public string zAC(int iar = 0)
    {
        string sAr = "";
        string[] array = new string[131]
        {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
            "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
            "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ",
            "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ",
            ""

        };
        sAr = array[iar];
        return sAr;

    }
    public data_elearning zCallServiceNetwork(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);

        return _data_elearning;
    }

    public data_elearning zCallServicePostNetwork(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);

        return _data_elearning;
    }

    public data_itasset zCallServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }

    public string zJson(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _cmdUrl + _localJson;
        return _localJson;
    }

    public void zSetFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    public void zSetGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    public string zFormatfloat(string qty, int iformat)
    {
        if (qty == "")
        {
            qty = "0";
        }
        return string.Format("{0:n" + iformat.ToString() + "}", float.Parse(qty));
    }
    public Boolean zCheckErrorImage(string sImage)
    {
        Boolean bErrror = false;
        sImage = sImage.ToLower().Trim();
        if (sImage == ".jpg")
        {

        }
        else if (sImage == ".gif")
        {

        }
        else if (sImage == ".png")
        {

        }
        else if (sImage == ".jpeg")
        {

        }
        else if (sImage == "")
        {

        }
        else
        {
            bErrror = true;
        }
        return bErrror;
    }
    public int zBooleanToInt(Boolean _Boolean)
    {
        int iBoolean = 0;
        if (_Boolean == true)
        {
            iBoolean = 1;
        }
        return iBoolean;
    }

    public void zGetOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = zCallServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        zSetDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือก ---", "0"));
    }

    public void zGetDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = zCallServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        zSetDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือก ---", "0"));
    }

    public void zGetSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = zCallServicePostEmployee(_urlGetSectionList, _dataEmployee);
        zSetDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือก ---", "0"));
    }
    public data_employee zCallServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    public void zSetDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value,int zitem = 0)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
        if(zitem > 0)
        {
            ddlName.Items.Insert(0, new ListItem("--- เลือก ---", "0"));
        }
    }
    public string zGetDataDropDownList(DropDownList ddlName)
    {
        string sValue = "";

        if (ddlName.SelectedIndex > 0)
        {
            sValue = ddlName.Items[ddlName.SelectedIndex].ToString();
        }

        return sValue;
    }
    public void zClearDataDropDownList(DropDownList ddlName,string sDefault="")
    {
        ddlName.Items.Clear();
        if (sDefault == "")
        {
            ddlName.Items.Insert(0, new ListItem("--- เลือก ---", "0"));
        }
        else
        {
            ddlName.Items.Insert(0, new ListItem(sDefault, "0"));
        }
    }
    public int zStringToInt(string _string)
    {
        int iBoolean = 0;
        if ((_string == null) || _string == "")
        {

        }
        else
        {
            iBoolean = int.Parse(_string);
        }
        return iBoolean;
    }
    public Boolean zIntToBoolean(int _int)
    {
        Boolean iBoolean = false;
        if ((_int == null) || _int == 0)
        {

        }
        else
        {
            iBoolean = true;
        }
        return iBoolean;
    }
    //Start Mode Data
    public void zModePanel(Panel _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Visible = false;
        }
        else
        {
            _Object.Visible = true;
        }
    }
    public void zModeDropDownList(DropDownList _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeRadioButtonList(RadioButtonList _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }

    public void zModeCheckboxList(CheckBoxList _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeFileUpload(FileUpload _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeGridView(GridView _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeGridViewVisible(GridView _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Visible = false;
        }
        else
        {
            _Object.Visible = true;
        }
    }
    public void zModeTextBox(TextBox _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeLinkButton(LinkButton _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Visible = false;
        }
        else
        {
            _Object.Visible = true;
        }
    }
    public void zModeCheckBox(CheckBox _Object, string _Mode)
    {
        if (_Mode == "P")
        {
            _Object.Enabled = false;
        }
        else
        {
            _Object.Enabled = true;
        }
    }
    public void zModeGridViewCol(GridView _Object, string _Mode, int _Columns)
    {
        if (_Mode == "P")
        {
            _Object.Columns[_Columns].Visible = false;
        }
        else
        {
            _Object.Columns[_Columns].Visible = true;
        }
    }

    //End Mode Data

    public void zSetDdlMonth(DropDownList ddlName)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.Items.Add(new ListItem("-", "0"));
        for (int i = 1; i <= 12; i++)
        {
            ddlName.Items.Add(new ListItem(zMonthTH(i), i.ToString()));
        }
    }

    public data_elearning zShowDataDsLookup(
                                             string Aoperation_status_id = "",
                                             string AWhereField = "",
                                             int AWhereID = 0
                                             )
    {
        data_elearning _dataelearning = new data_elearning();
        if ((AWhereField != "") && (AWhereID >= 0))
        {

            trainingLoolup obj = new trainingLoolup();
            _dataelearning.trainingLoolup_action = new trainingLoolup[1];
            obj.operation_status_id = Aoperation_status_id;
            if (AWhereField == "idx")
            {
                obj.idx = AWhereID;
            }
            else if (AWhereField == "m0_training_group_idx_ref")
            {
                obj.m0_training_group_idx_ref = AWhereID;
            }
            _dataelearning.trainingLoolup_action[0] = obj;
            _dataelearning = zCallServicePostNetwork(_urlGetel_lu_TraningAll, _dataelearning);

        }
        return _dataelearning;

    }
    public Boolean zCheckNumber(string _string)
    {
        Boolean _Boolean = false;
        if ((_string == null) || _string == "")
        {

        }
        else if((float.Parse(_string) <= 0) )
        {

        }
        else
        {
            _Boolean = true;
        }
        return _Boolean;
    }
    public Boolean zCheckAmount(string _string)
    {
        Boolean _Boolean = false;
        if ((_string == null) || _string == "")
        {
            _Boolean = true;
        }
        else if ((float.Parse(_string) < 0))
        {

        }
        else
        {
            _Boolean = true;
        }
        return _Boolean;
    }
    public decimal zStringToDecimal(string _string)
    {
        decimal iBoolean = 0;
        if ((_string == null) || _string == "")
        {

        }
        else
        {
            iBoolean = decimal.Parse(_string);
        }
        return iBoolean;
    }
    public string zObjectToXml(Object objData)
    {
        return HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning)); 
    }
    public string zIntToEmpty(int _int)
    {
        string iBoolean = "";
        if ((_int == null) || _int == 0)
        {

        }
        else
        {
            iBoolean = _int.ToString();
        }
        return iBoolean;
    }
    public string zGetMode(string _string)
    {
        string iBoolean = "";
        if ((_string == null) || _string == "")
        {
            iBoolean = "I";
        }
        else
        {
            iBoolean = iBoolean = "E";
        }
        return iBoolean;
    }

    public void zSetRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }
    public string zHostname(string host)
    {
        string hostname = "";
        //if (host== "172.16.11.44")
        //{
        //    hostname = _HostServerDev;
        //}
        //else
        //{
        //    hostname = _HostServer;
        //}
        host = "172.16.11.44";
        hostname = host + "/" + _HostServerDev;
        return hostname;
    }
    public string zTruncate(string value, int maxLength)
    {
        if (string.IsNullOrEmpty(value)) return value;
        return value.Length <= maxLength ? value : value.Substring(0, maxLength) + "...";
    }
    public string zMonthColor(int AMonth)
    {
        string sMonth = "";
        if (AMonth == 1) { sMonth = "#FFA07A"; }
        else if (AMonth == 2) { sMonth = "#FF6347 "; }
        else if (AMonth == 3) { sMonth = "#FFDAB9"; }
        else if (AMonth == 4) { sMonth = "#00FA9A"; }
        else if (AMonth == 5) { sMonth = "#00BFFF"; }
        else if (AMonth == 6) { sMonth = "#E6E6FA"; }
        else if (AMonth == 7) { sMonth = "#D8BFD8"; }
        else if (AMonth == 8) { sMonth = "#FFB6C1"; }
        else if (AMonth == 9) { sMonth = "#FF8C00"; }
        else if (AMonth == 10) { sMonth = "#F08080"; }
        else if (AMonth == 11) { sMonth = "#DDA0DD"; }
        else if (AMonth == 12) { sMonth = "#008CBA"; }
        return sMonth;
    }
    public Color zGetColor(string sColor)
    {
        Color _color1 = Color.Empty;
        if (sColor != "")
        {
            try
            {
                _color1 = ColorTranslator.FromHtml(sColor);
            }
            catch
            {
                _color1 = Color.Empty;
            }
        }
        //txttraining_group_color.BackColor = System.Drawing.ColorTranslator.FromHtml("#008CBA")
        return _color1;
    }
    public string zGetTextDropDownList(DropDownList ddlName)
    {
        string _string = "";

        _string = ddlName.Items[ddlName.SelectedIndex].Text;

        return _string;
    }

    // Start EN
    /*
    public data_en_planning zCallServicePost_en_planning(string _cmdUrl, data_en_planning _data_en_planning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_en_planning);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_en_planning = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _data_en_planning;
    }
    */

    public void zENLocation(DropDownList ddlName)
    {
        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];

        _dtmachine = zCallServicePostENRepair(urlSelect_Location, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("เลือก...", "0"));
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    public DataMachine zCallServicePostENRepair(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);
        
        return _dtmachine;
    }
    public data_en_planning zCallServicePostENPlanning(string _cmdUrl, data_en_planning _data_en_planning)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_en_planning);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _data_en_planning = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _data_en_planning;
    }
    public void zENtypecodemachine(DropDownList ddlName, int tmcidx)
    {
        data_en_planning _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = zCallServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("เลือก...", "0"));

    }
    public void zENtypemachine(DropDownList ddlName)
    {
        data_en_planning _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan.BoxEN_TypeItemList[0] = typemachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = zCallServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("เลือก...", "0"));

    }
    public void zENBuilding(DropDownList ddlName, int LocIDX)
    {

        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail building = new RepairMachineDetail();

        building.LocIDX = LocIDX;
        _dtmachine.BoxRepairMachine[0] = building;

        _dtmachine = zCallServicePostENRepair(urlSelect_Building, _dtmachine);

        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("เลือก...", "0"));
    }
    public void zENDropDownList(DropDownList _ddl,
                                             string Alabel,
                                             string AFiledId,
                                             string AFiledName,
                                             string AFiledDefault,
                                             string Avalue = "",
                                             string Aoperation_status_id = ""
                                             )
    {


        //  DropDownList _ddl = new DropDownList();

        if ((AFiledId == null) || (AFiledName == null) || (AFiledId == "") || (AFiledName == ""))
        {
            return;
        }
        if (Alabel == "")
        {
            Alabel = "เลือก...";
        }
        en_lookup obj = new en_lookup();
        _data_en_planning.en_lookup_action = new en_lookup[1];
        obj.operation_status_id = Aoperation_status_id;
        _data_en_planning.en_lookup_action[0] = obj;
        _data_en_planning = zCallServicePostENPlanning(_urlGeten_lookup, _data_en_planning);
        _ddl.Items.Clear();
        _ddl.AppendDataBoundItems = true;
        if (Alabel == "-")
        {
            _ddl.Items.Add(new ListItem(Alabel, AFiledDefault));
        }
        else
        {
            _ddl.Items.Add(new ListItem( Alabel, AFiledDefault));
        }
        _ddl.DataSource = _data_en_planning.en_lookup_action;
        _ddl.DataTextField = AFiledName;
        _ddl.DataValueField = AFiledId;
        _ddl.DataBind();

        if (Avalue != "")
        {
            _ddl.SelectedValue = Avalue.ToString();
        }

    }
    public data_en_planning zENLookupDataList(data_en_planning _dtenplan,string _operation_status_id,int idx = 0)
    {
        //data_en_planning dataenplanning = new data_en_planning();
        _dtenplan.en_lookup_action = new en_lookup[1];
        en_lookup obj_lookup = new en_lookup();
        obj_lookup.operation_status_id = _operation_status_id;
        if(_operation_status_id == "iso_plan_year")
        {
            if(idx == 0)
            {
                //ใช้ใน แบบฟอร์มใบบันทึก แผนการทำ PM เครื่องจักรประจำปี
                obj_lookup.idx = 1; 
            }
            else
            {
                obj_lookup.idx = idx;
            }
        }
        _dtenplan.en_lookup_action[0] = obj_lookup;
        _dtenplan = zCallServicePostENPlanning(_urlGeten_lookup, _dtenplan);

        return _dtenplan;

    }
    
    // End EN

}