﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_register : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";
    private string ConCL_Re = "conn_recruiter";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetCountryList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountryList"];
    static string _urlGetProvincetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvincetList"];
    static string _urlGetAmphurList = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurList"];
    static string _urlGetDistList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistList"];
    static string _urlGetApproveLonList = _serviceUrl + ConfigurationManager.AppSettings["urlGetApproveLonList"];
    static string _UrlSetEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["UrlSetEmployeeList"];
    static string _urlSetODSPList = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList"];
    static string _urlSetODSPList_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Update"];
    static string _urlSetODSPList_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Delete"];
    static string _urlGetChildList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildList"];
    static string _urlGetPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorList"];
    static string _urlGetEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationList"];
    static string _urlGetTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainList"];
    static string _urlSetUpdateChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateChildList"];
    static string _urlSetUpdatePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePriorList"];
    static string _urlSetUpdateEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateEducationList"];
    static string _urlSetUpdateTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateTrainList"];
    static string _urlSetDeleteChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteChildList"];
    static string _urlSetDeletePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeletePriorList"];
    static string _urlSetDeleteEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEducationList"];
    static string _urlSetDeleteTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteTrainList"];
    static string _urlSetDeleteEmployeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmployeeList"];
    static string _urlSetResetpass_employeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetResetpass_employeeList"];
    static string _urlurlSetInsertEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertEmployeeList"];
    static string _urlSetUpdateDetail_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList"];
    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string _urlSetupdateapprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdateapprove_employeeList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];
    static string _urlGetReferList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferList"];
    static string _urlSetUpdateReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateReferList"];
    static string _urlSetDeleteReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteReferList"];
    static string _urlSetInsertRegisterShotList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertRegisterShotList"];
    static string _urlGetEmployee_Register = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee_Register"];
    static string _urlSetupdate_employee_registerList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_employee_registerList"];
    static string _urlGetChildRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildRegisList"];
    static string _urlGetPriorRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorRegisList"];
    static string _urlGetEducationRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationRegisList"];
    static string _urlGetTrainRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainRegisList"];
    static string _urlGetReferRegisList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferRegisList"];

    static string _urlSetUpdateRegisReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisReferList"];
    static string _urlSetUpdateRegisChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisChildList"];
    static string _urlSetUpdateRegisPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisPriorList"];
    static string _urlSetUpdateRegisEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisEducationList"];
    static string _urlSetUpdateRegisTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRegisTrainList"];
    static string _urlSetDeleteRegisReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisReferList"];
    static string _urlSetDeleteRegisChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisChildList"];
    static string _urlSetDeleteRegisPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisPriorList"];
    static string _urlSetDeleteRegisEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisEducationList"];
    static string _urlSetDeleteRegisTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteRegisTrainList"];
    static string _urlGetselect_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_ma_positionList"];
    static string _urlGetselect_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_group_positionList"];

    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];
    string _Folder_courseBin = "course_test_img_bin";
    HttpPostedFile file_delImage;

    int emp_idx = 0;
    int defaultInt = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";
    public string checkfile;


    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        //Session["title"] = "";
        Literal masterTitle = (Literal)Master.FindControl("litTitle");
        //Html masterTitle = (Literal)Master.FindControl("litTitle");

        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = 173;// Session["emp_idx"].ToString();
            select_empIdx_present();

            Menu_Color(0);
            //MvMaster.SetActiveView(View_Present);
            //MvMaster.SetActiveView(ViewPosition);
            Box_panel_present.Visible = true;
            Box_Menu.Visible = false;

            SelectMaPositionList();
            Select_static();
            ////FvInsert_Mini.ChangeMode(FormViewMode.Insert);
            ////FvInsert_Mini.DataBind();
            ////Menu_Color(1);
            ////txtfocus.Focus();  
            Insert_static(0);
            Page.Title = "TKN Register - Position";
        }

        //FormView FvInsert_Mini1 = (FormView)ViewAdd_mini.FindControl("FvInsert_Mini");
        //LinkButton btn_save_file1 = (LinkButton)FvInsert_Mini1.FindControl("btn_save_file1");
        //LinkButton btn_save_document = (LinkButton)FvInsert_Mini1.FindControl("btn_save_document");

        btnTrigger(lbadd_mini);
        btnTrigger(lbadd_full);
        btnTrigger(lbedit);
        btnTrigger(lbbenefit);
        btnTrigger(lbposition);
        btnTrigger(lbcontract);
    }

    #region INSERT&SELECT&UPDATE

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;
        ViewState["ip_address"] = "";
        ViewState["ID_identity"] = "";
        btnsave_mini.BackColor = System.Drawing.ColorTranslator.FromHtml("#BEBEBE");
        btnSave_full.BackColor = System.Drawing.ColorTranslator.FromHtml("#BEBEBE");
        //FormView Fv_Search_Emp_Index = (FormView)ViewEdit.FindControl("Fv_Search_Emp_Index");
        //Panel pn_bg_genarol = (Panel)ViewEdit.FindControl("pn_bg_genarol");
        //Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
        //Fv_Search_Emp_Index.DataBind();

        //Button btnsearch_index = (Button)Fv_Search_Emp_Index.FindControl("btnsearch_index");
        //Button btnclearsearch_index = (Button)Fv_Search_Emp_Index.FindControl("btnclearsearch_index");
        //btnsearch_index.BackColor = System.Drawing.ColorTranslator.FromHtml("#BEBEBE");
        //btnclearsearch_index.BackColor = System.Drawing.ColorTranslator.FromHtml("#BEBEBE");
        btnupdate.BackColor = System.Drawing.ColorTranslator.FromHtml("#BEBEBE");

        Panel1.Attributes.Add("style", "background-size: 100% 100%");
        Panel1.BackImageUrl = "~/masterpage/images/hr/background_register_1.png";

        Panel5.Attributes.Add("style", "background-size: 100% 100%");
        Panel5.BackImageUrl = "~/masterpage/images/hr/background_register_2.png";

        string ipaddress1;
        ipaddress1 = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (ipaddress1 == "" || ipaddress1 == null)
        {
            ipaddress1 = Request.ServerVariables["REMOTE_ADDR"];
        }
        lblgetip.Text = ipaddress1.ToString();
        ViewState["ip_address"] = ipaddress1.ToString();

    }

    protected void Select_Employee_index_search()
    {
        //TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempcode_s");
        //TextBox txtempname_s = (TextBox)Fv_Search_Emp_Index.FindControl("txtempname_s");

        //DropDownList ddlorg_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlorg_s");
        //DropDownList ddldep_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddldep_s");
        //DropDownList ddlsec_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlsec_s");
        //DropDownList ddlpos_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlpos_s");
        //DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlemptype_s");
        //DropDownList ddlempstatus_s = (DropDownList)Fv_Search_Emp_Index.FindControl("ddlempstatus_s");

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.type_select_emp = 2; //Index     

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["Box_dataEmployee_index"] = _dataEmployee.employee_list;
        //setGridData(GvEmployee, _dataEmployee.employee_list);
    }

    protected void Select_FvEdit_Detail(int Empidx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = Empidx;
        _dataEmployee_.type_select_emp = 4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setFormData(FvEdit_Detail, _dataEmployee.employee_list);
    }

    protected void Select_Fv_Detail(FormView FvName, String IDcard)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_code = "";
        _dataEmployee_.type_select_emp = 4;
        _dataEmployee_.identity_card = IDcard;


        _dataEmployee.employee_list[0] = _dataEmployee_;
        //Label3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetEmployee_Register, _dataEmployee);
        if (_dataEmployee.return_code == "0")
        {
            setFormData(FvName, _dataEmployee.employee_list);
            btnupdate.Visible = true;
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Data", "alert('" + "คุณกรอกข้อมูลบัตรประชาชนผิด" + "')", true);
            btnupdate.Visible = false;
        }
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("ประจำสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "r0idx";
        ddlName.DataBind();
    }

    protected void Select_hospital(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกโรงพยาบาล...", "0"));

        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        _dataEmployee.hospital_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.hospital_list;
        ddlName.DataTextField = "Name";
        ddlName.DataValueField = "HosIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_country(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetCountryList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "country_name", "country_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเทศ....", "0"));
    }

    protected void select_nationality(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "NatName", "NatIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
    }

    protected void select_province(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetProvincetList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "prov_name", "prov_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกจังหวัด....", "0"));
    }

    protected void select_amphur(DropDownList ddlName, int prov_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetAmphurList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "amp_name", "amp_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกอำเภอ....", "0"));
    }

    protected void select_dist(DropDownList ddlName, int prov_idx, int amphur_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _addresslist.amp_idx = amphur_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetDistList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "dist_name", "dist_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำบล....", "0"));
    }

    protected void select_approve_add(DropDownList ddlName, int org_idx, int dep_idx, int sec_idx, int pos_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = 0;
        _dataEmployee_.org_idx = org_idx;
        _dataEmployee_.rdept_idx = dep_idx;
        _dataEmployee_.rsec_idx = sec_idx;
        _dataEmployee_.rpos_idx = pos_idx;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetApproveLonList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_approve1", "emp_idx_approve1");
        ddlName.Items.Insert(0, new ListItem("เลือกผู้อนุมัติ ....", "0"));
    }

    protected void Select_Refer_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

        _dataEmployee_c.BoxReferent_list = new Referent_List[1];
        Referent_List _dataEmployee_1 = new Referent_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxReferent_list[0] = _dataEmployee_1;

        _dataEmployee_c = callServiceEmployee(_urlGetReferRegisList, _dataEmployee_c);
        //var sas = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee_c));
        //Label3.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee_c));
        setGridData(GvReference_View, _dataEmployee_c.BoxReferent_list);
        ViewState["BoxGvRefer_view"] = _dataEmployee_c.BoxReferent_list;
    }

    protected void Select_Child_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

        _dataEmployee_c.BoxChild_list = new Child_List[1];
        Child_List _dataEmployee_1 = new Child_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxChild_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetChildRegisList, _dataEmployee_c);
        setGridData(GvChildAdd_View, _dataEmployee_c.BoxChild_list);
        ViewState["BoxGvChild_view"] = _dataEmployee_c.BoxChild_list;
    }

    protected void Select_Prior_view(int emp_idx)
    {
        data_employee _dataEmployee_p = new data_employee();
        GridView GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

        _dataEmployee_p.BoxPrior_list = new Prior_List[1];
        Prior_List _dataEmployee_2 = new Prior_List();

        _dataEmployee_2.EmpIDX = emp_idx;

        _dataEmployee_p.BoxPrior_list[0] = _dataEmployee_2;
        _dataEmployee_p = callServiceEmployee(_urlGetPriorRegisList, _dataEmployee_p);
        setGridData(GvPri_View, _dataEmployee_p.BoxPrior_list);
        ViewState["BoxGvPrior_view"] = _dataEmployee_p.BoxPrior_list;
    }

    protected void Select_Education_view(int emp_idx)
    {
        data_employee _dataEmployee_e = new data_employee();
        GridView GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

        _dataEmployee_e.BoxEducation_list = new Education_List[1];
        Education_List _dataEmployee_ = new Education_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_e.BoxEducation_list[0] = _dataEmployee_;
        _dataEmployee_e = callServiceEmployee(_urlGetEducationRegisList, _dataEmployee_e);
        setGridData(GvEducation_View, _dataEmployee_e.BoxEducation_list);
        ViewState["BoxGvEducation_view"] = _dataEmployee_e.BoxEducation_list;
    }

    protected void Select_Train_view(int emp_idx)
    {
        data_employee _dataEmployee_t = new data_employee();
        GridView GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

        _dataEmployee_t.BoxTrain_list = new Train_List[1];
        Train_List _dataEmployee_ = new Train_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_t.BoxTrain_list[0] = _dataEmployee_;
        _dataEmployee_t = callServiceEmployee(_urlGetTrainRegisList, _dataEmployee_t);
        setGridData(GvTrain_View, _dataEmployee_t.BoxTrain_list);
        ViewState["BoxGvTrain_view"] = _dataEmployee_t.BoxTrain_list;
    }

    protected void SelectMaPositionList()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_ma_positionList, _dataEmployee);
        setGridData(GvMaPosition, _dataEmployee.BoxManage_Position_list);
    }

    protected void SelectMaPositionList_ViewDetail(int PIDX)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PIDX = PIDX;

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_ma_positionList, _dataEmployee);
        setFormData(FvDetail_Position, _dataEmployee.BoxManage_Position_list);
    }

    protected void SelectGroupPositionList(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_group_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "PosGroupNameTH", "PosGroupIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทกลุ่มงาน....", "0"));
    }

    protected void SelectPositionList(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetselect_ma_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "P_Name", "PIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่งงาน....", "0"));
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    protected void btnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    protected void btnTrigger_button(Button linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    public string imageupload_del(string name)
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        string _itemNameNew = "";
        if (name != "")
        {
            _itemNameNew = name + ".png";
        }
        else
        {
            name = "";
        }
        if (name != "")
        {
            //  Hddfld_folderImage.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");
            //}
            if (Directory.Exists(Server.MapPath(_PathFile)))
            {

                _PathFile = _PathFile + _Folder_courseBin + "/" + Hddfld_folderImage.Value;

                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                try
                {
                    File.Delete(Server.MapPath(newFilePath));
                }
                catch { }
            }
        }
        return name;
    }

    protected void Select_static()
    {
        _dataEmployee = new data_employee();
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.type_select_emp = 11; //Index     

        _dataEmployee.employee_list[0] = _dataEmployee_;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));


        _dataEmployee = callServiceEmployee(_urlGetEmployee_Register, _dataEmployee);
        lbl_static.Text = "ผู้เยี่ยมชมทั้งหมด ";
        lbl_static2.Text = _dataEmployee.employee_list[0].emp_status.ToString();
        lbl_static3.Text = " คน";

        lbl_static.ForeColor = System.Drawing.Color.Yellow;
        lbl_static2.ForeColor = System.Drawing.Color.White;
        lbl_static3.ForeColor = System.Drawing.Color.Yellow;
    }

    protected void Select_check_ipaddress()
    {
        string ip_address = "";

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.type_select_emp = 12; //Index
        _dataEmployee_.ip_address = ViewState["ip_address"].ToString(); // "192.168.120.107";

        _dataEmployee.employee_list[0] = _dataEmployee_;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee_));

        _dataEmployee = callServiceEmployee(_urlGetEmployee_Register, _dataEmployee);
        ip_address = _dataEmployee.return_code.ToString();
        ViewState["check_ip"] = ip_address.ToString(); //0 = มีค่า , 1 = ไม่มีค่า
    }

    protected void Select_tranfer(string identity)
    {
        //string url_test = "http://mas.taokaenoi.co.th/rcm-question";
        string url_test = "http://localhost/mas.taokaenoi.co.th/rcm-question";

        HttpResponse response = HttpContext.Current.Response;
        response.Clear();

        StringBuilder s = new StringBuilder();
        s.Append("<html>");
        s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
        s.AppendFormat("<form name='form' action='{0}' method='post'>", url_test);
        s.Append("<input type='hidden' name='identity_card' value=" + identity.ToString() + " />");

        s.Append("</form></body></html>");
        response.Write(s.ToString());
        response.End();
    }

    #endregion

    #region Directories_File URL

    protected void showimage_upload(string x, Image y)
    {

        string getPath = ConfigurationSettings.AppSettings["path_flie_emp_register"];
        string pat = getPath + x.ToString() + "/" + x.ToString() + ".jpg";
        y.Visible = true;
        y.ImageUrl = pat; //Page.ResolveUrl("~/ImageFiles/") + fi.Name;
        //Label3.Text = pat;
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView Gv)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                Gv.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                Gv.DataBind();
                ds1.Dispose();
                ViewState["RowCountGvFile"] = dt1.Rows.Count;
            }
            else
            {

                Gv.DataSource = null;
                Gv.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            checkfile = "11";
        }
    }

    #endregion

    #region INSERT / UPDATE

    protected void Insert_Employee()
    {
        //------------------------------- Page 1

        var ddlstartdate_in = (DropDownList)FvInsert_Mini.FindControl("ddlstartdate_in");
        var txtsalary = (TextBox)FvInsert_Mini.FindControl("txtsalary");
        var ddltypecategory = (DropDownList)FvInsert_Mini.FindControl("ddltypecategory");
        var ddlpositionfocus = (DropDownList)FvInsert_Mini.FindControl("ddlpositionfocus");
        var ddlposition1 = (DropDownList)FvInsert_Mini.FindControl("ddlposition1");
        var ddlposition2 = (DropDownList)FvInsert_Mini.FindControl("ddlposition2");
        var ddlposition3 = (DropDownList)FvInsert_Mini.FindControl("ddlposition3");
        var txtdescription = (TextBox)FvInsert_Mini.FindControl("txtdescription");

        var ddl_prefix_th = (DropDownList)FvInsert_Mini.FindControl("ddl_prefix_th");
        var txt_name_th = (TextBox)FvInsert_Mini.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvInsert_Mini.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvInsert_Mini.FindControl("ddl_prefix_en");
        var txt_name_en = (TextBox)FvInsert_Mini.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvInsert_Mini.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvInsert_Mini.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvInsert_Mini.FindControl("txt_nickname_en");
        var txtbirth = (TextBox)FvInsert_Mini.FindControl("txtbirth");
        var ddl_sex = (DropDownList)FvInsert_Mini.FindControl("ddl_sex");
        //var ddl_status = (DropDownList)FvInsert_Mini.FindControl("ddl_status");
        //var ddlnation = (DropDownList)FvInsert_Mini.FindControl("ddlnation");
        //var ddlrace = (DropDownList)FvInsert_Mini.FindControl("ddlrace");
        //var ddlreligion = (DropDownList)FvInsert_Mini.FindControl("ddlreligion");
        var ddlmilitary = (DropDownList)FvInsert_Mini.FindControl("ddlmilitary");
        var txtidcard = (TextBox)FvInsert_Mini.FindControl("txtidcard");

        var txtorgold = (TextBox)FvInsert_Mini.FindControl("txtorgold");
        var txtposold = (TextBox)FvInsert_Mini.FindControl("txtposold");
        var txtstartdate_ex = (TextBox)FvInsert_Mini.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvInsert_Mini.FindControl("txtresigndate_ex");
        var GvPri = (GridView)FvInsert_Mini.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvInsert_Mini.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvInsert_Mini.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvInsert_Mini.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvInsert_Mini.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvInsert_Mini.FindControl("txtstudy");
        var GvEducation = (GridView)FvInsert_Mini.FindControl("GvEducation");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvInsert_Mini.FindControl("txtaddress_present_mini");
        var ddlcountry_present = (DropDownList)FvInsert_Mini.FindControl("ddlcountry_present_mini");
        var ddlprovince_present = (DropDownList)FvInsert_Mini.FindControl("ddlprovince_present_mini");
        var ddlamphoe_present = (DropDownList)FvInsert_Mini.FindControl("ddlamphoe_present_mini");
        var ddldistrict_present = (DropDownList)FvInsert_Mini.FindControl("ddldistrict_present_mini");
        var txttelmobile_present = (TextBox)FvInsert_Mini.FindControl("txttelmobile_present");
        //var txttelhome_present = (TextBox)FvInsert_Mini.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvInsert_Mini.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvInsert_Mini.FindControl("txtemail_present");

        var ddldvcarstatus = (DropDownList)FvInsert_Mini.FindControl("ddldvcarstatus");
        var ddldvmtstatus = (DropDownList)FvInsert_Mini.FindControl("ddldvmtstatus");
        //------------------------------- Page 4

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].EmpINID = int.Parse(ddlstartdate_in.SelectedValue);
        _dataEmployee_[0].salary = int.Parse(txtsalary.Text);
        _dataEmployee_[0].JobTypeIDX = int.Parse(ddltypecategory.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlpositionfocus.SelectedValue);
        _dataEmployee_[0].PosGrop1 = int.Parse(ddlposition1.SelectedValue);
        _dataEmployee_[0].PosGrop2 = int.Parse(ddlposition2.SelectedValue);
        _dataEmployee_[0].PosGrop3 = int.Parse(ddlposition3.SelectedValue);
        _dataEmployee_[0].DetailProfile = txtdescription.Text;

        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        //_dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        //_dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }

        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = 0; //int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = 177; //int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = 0; //int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = 0; // int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        //_dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;
        //_dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue);
        //_dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue);


        _dtEmployee.employee_list = _dataEmployee_;

        if (ViewState["vsTemp_Prior"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();
                AddPrior[b].Pri_description = dr["Pri_description"].ToString();
                AddPrior[b].Pri_salary = dr["Pri_salary"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }

        if (ViewState["vsTemp_Education"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }

        _dtEmployee = callServiceEmployee_Check(_urlSetInsertRegisterShotList, _dtEmployee);
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "ข้อมูลของคุณถูกจัดส่งให้ HR เรียบร้อยแล้ว" + "')", true);

        if (_dtEmployee.return_code == "9")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "มีรหัสบัตรประขาขนนี้อยู่ในระบบแล้ว" + "')", true);
        }
        else
        {
            Insert_static(1);
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "ข้อมูลของคุณถูกจัดส่งให้ HR เรียบร้อยแล้ว" + "')", true);
            Menu_Color(5);
            MvMaster.SetActiveView(ViewPosition);
            SelectMaPositionList();
            Select_check_ipaddress();

            //if (ViewState["check_ip"].ToString() == "0") //เน็ตในบริษัท
            //{
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_mini();", true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "ข้อมูลของคุณถูกจัดส่งให้ HR เรียบร้อยแล้ว" + "')", true);

            //}  
        }
    }

    protected void Insert_Employee_Full()
    {
        //------------------------------- Page 1

        var ddlstartdate_in = (DropDownList)FvInsertEmp.FindControl("ddlstartdate_in");
        var txtsalary = (TextBox)FvInsertEmp.FindControl("txtsalary");
        var ddltypecategory = (DropDownList)FvInsertEmp.FindControl("ddltypecategory");
        var ddlpositionfocus = (DropDownList)FvInsertEmp.FindControl("ddlpositionfocus");
        var ddlposition1 = (DropDownList)FvInsertEmp.FindControl("ddlposition1");
        var ddlposition2 = (DropDownList)FvInsertEmp.FindControl("ddlposition2");
        var ddlposition3 = (DropDownList)FvInsertEmp.FindControl("ddlposition3");
        var txtdescription = (TextBox)FvInsertEmp.FindControl("txtdescription");

        var ddl_prefix_th_full = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_th_full");
        var txt_name_th = (TextBox)FvInsertEmp.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvInsertEmp.FindControl("txt_surname_th");
        var ddl_prefix_en_full = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_en_full");
        var txt_name_en = (TextBox)FvInsertEmp.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvInsertEmp.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvInsertEmp.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvInsertEmp.FindControl("txt_nickname_en");
        var txtbirth = (TextBox)FvInsertEmp.FindControl("txtbirth");
        var ddl_sex_full = (DropDownList)FvInsertEmp.FindControl("ddl_sex_full");
        var ddl_status = (DropDownList)FvInsertEmp.FindControl("ddl_status"); //
        var ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation"); //
        var ddlrace = (DropDownList)FvInsertEmp.FindControl("ddlrace"); //
        var ddlreligion = (DropDownList)FvInsertEmp.FindControl("ddlreligion"); //
        var ddlmilitary_full = (DropDownList)FvInsertEmp.FindControl("ddlmilitary_full");
        var txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvInsertEmp.FindControl("txtissued_at"); //
        var txtexpcard = (TextBox)FvInsertEmp.FindControl("txtexpcard"); //

        //var ddllocation = (DropDownList)FvInsertEmp.FindControl("ddllocation");

        var ddldvcar = (DropDownList)FvInsertEmp.FindControl("ddldvcar"); //
        var ddldvmt = (DropDownList)FvInsertEmp.FindControl("ddldvmt");
        var ddldvfork = (DropDownList)FvInsertEmp.FindControl("ddldvfork");
        var ddldvtruck = (DropDownList)FvInsertEmp.FindControl("ddldvtruck");
        var ddldvcarstatus = (DropDownList)FvInsertEmp.FindControl("ddldvcarstatus");
        var ddldvmtstatus = (DropDownList)FvInsertEmp.FindControl("ddldvmtstatus");

        var txtlicens_car = (TextBox)FvInsertEmp.FindControl("txtlicens_car");
        var txtlicens_moto = (TextBox)FvInsertEmp.FindControl("txtlicens_moto");
        var txtlicens_fork = (TextBox)FvInsertEmp.FindControl("txtlicens_fork");
        var txtlicens_truck = (TextBox)FvInsertEmp.FindControl("txtlicens_truck");

        var txtaccountname = (TextBox)FvInsertEmp.FindControl("txtaccountname"); //
        var txtaccountnumber = (TextBox)FvInsertEmp.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvInsertEmp.FindControl("txtemercon");
        var txtrelation = (TextBox)FvInsertEmp.FindControl("txtrelation");
        var txttelemer = (TextBox)FvInsertEmp.FindControl("txttelemer"); //

        var txtfathername = (TextBox)FvInsertEmp.FindControl("txtfathername");
        var txttelfather = (TextBox)FvInsertEmp.FindControl("txttelfather");
        var txtlicenfather = (TextBox)FvInsertEmp.FindControl("txtlicenfather");
        var txtmothername = (TextBox)FvInsertEmp.FindControl("txtmothername");
        var txttelmother = (TextBox)FvInsertEmp.FindControl("txttelmother");
        var txtlicenmother = (TextBox)FvInsertEmp.FindControl("txtlicenmother");
        var txtwifename = (TextBox)FvInsertEmp.FindControl("txtwifename");
        var txttelwife = (TextBox)FvInsertEmp.FindControl("txttelwife");
        var txtlicenwifename = (TextBox)FvInsertEmp.FindControl("txtlicenwifename");

        var txtchildname = (TextBox)FvInsertEmp.FindControl("txtchildname");
        var ddlchildnumber = (DropDownList)FvInsertEmp.FindControl("ddlchildnumber");
        var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd"); //

        var txtorgold = (TextBox)FvInsertEmp.FindControl("txtorgold"); //
        var txtposold = (TextBox)FvInsertEmp.FindControl("txtposold");
        var txtstartdate_ex = (TextBox)FvInsertEmp.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvInsertEmp.FindControl("txtresigndate_ex");
        var GvPri = (GridView)FvInsertEmp.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvInsertEmp.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvInsertEmp.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvInsertEmp.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvInsertEmp.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvInsertEmp.FindControl("txtstudy");
        var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");

        var txttraincourses = (TextBox)FvInsertEmp.FindControl("txttraincourses");
        var txttraindate = (TextBox)FvInsertEmp.FindControl("txttraindate");
        var txttrainassessment = (TextBox)FvInsertEmp.FindControl("txttrainassessment");
        var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain"); //

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvInsertEmp.FindControl("txtaddress_present"); //
        var ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
        var ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
        var ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
        var ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");
        var txttelmobile_present = (TextBox)FvInsertEmp.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvInsertEmp.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvInsertEmp.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvInsertEmp.FindControl("txtemail_present");

        var txtaddress_permanentaddress = (TextBox)FvInsertEmp.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlcountry_permanentaddress");
        var ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");
        var ddlamphoe_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_permanentaddress");
        var ddldistrict_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddldistrict_permanentaddress");
        var txttelmobile_permanentaddress = (TextBox)FvInsertEmp.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvInsertEmp.FindControl("txttelhome_permanentaddress"); //

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvInsertEmp.FindControl("ddlhospital"); //
        var txtexaminationdate = (TextBox)FvInsertEmp.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvInsertEmp.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvInsertEmp.FindControl("txtheight");
        var txtweight = (TextBox)FvInsertEmp.FindControl("txtweight");
        var ddlblood = (DropDownList)FvInsertEmp.FindControl("ddlblood");
        var txtscar = (TextBox)FvInsertEmp.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvInsertEmp.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvInsertEmp.FindControl("ddlresultlab"); //

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].EmpINID = int.Parse(ddlstartdate_in.SelectedValue);
        _dataEmployee_[0].salary = int.Parse(txtsalary.Text);
        _dataEmployee_[0].JobTypeIDX = int.Parse(ddltypecategory.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlpositionfocus.SelectedValue);
        _dataEmployee_[0].PosGrop1 = int.Parse(ddlposition1.SelectedValue);
        _dataEmployee_[0].PosGrop2 = int.Parse(ddlposition2.SelectedValue);
        _dataEmployee_[0].PosGrop3 = int.Parse(ddlposition3.SelectedValue);
        _dataEmployee_[0].DetailProfile = txtdescription.Text;

        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th_full.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text; //
        _dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text; //
        _dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }


        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex_full.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary_full.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน

        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร
        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ

        _dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        _dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        _dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  
        _dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  
        _dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  
        _dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; // 
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text;
        _dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue);
        _dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue);

        _dataEmployee_[0].bank_idx = 0; // int.Parse(dllbank.SelectedValue); // ธนาคาร - New Table
        _dataEmployee_[0].bank_name = ""; // txtaccountname.Text; // ชื่อบัญชี
        _dataEmployee_[0].bank_no = ""; // txtaccountnumber.Text; // เลขที่บัญชี

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text;

        //_dataEmployee_[0].org_idx = int.Parse(ddlorg.SelectedValue);
        //_dataEmployee_[0].rdept_idx = int.Parse(ddldep.SelectedValue);
        //_dataEmployee_[0].rsec_idx = int.Parse(ddlsec.SelectedValue);
        //_dataEmployee_[0].rpos_idx = int.Parse(ddlpos.SelectedValue);

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;

        _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _dataEmployee_[0].soc_no = txtsecurityid.Text;  // รหัสบัตรประกันสังคม       
        if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม


        if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        _dataEmployee_[0].Scar = txtscar.Text;  // แผลเป้น
        _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB


        _dtEmployee.employee_list = _dataEmployee_;

        if (ViewState["vsTemp_Refer"] != null)
        {
            var dsrefer = (DataSet)ViewState["vsTemp_Refer"];
            var Addrefer = new Referent_List[dsrefer.Tables[0].Rows.Count];
            foreach (DataRow dr in dsrefer.Tables[0].Rows)
            {
                Addrefer[f] = new Referent_List();
                Addrefer[f].ref_fullname = dr["ref_fullname"].ToString();
                Addrefer[f].ref_position = dr["ref_position"].ToString();
                Addrefer[f].ref_relation = dr["ref_relation"].ToString();
                Addrefer[f].ref_tel = dr["ref_tel"].ToString();

                f++;
            }

            if (dsrefer.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxReferent_list = Addrefer;
            }
        }


        if (ViewState["vsTemp_Child"] != null)
        {
            var dschild = (DataSet)ViewState["vsTemp_Child"];
            var AddChild = new Child_List[dschild.Tables[0].Rows.Count];
            foreach (DataRow dr in dschild.Tables[0].Rows)
            {
                AddChild[a] = new Child_List();
                AddChild[a].Child_name = dr["Child_name"].ToString(); //ชื่อบัตร
                AddChild[a].Child_num = dr["Child_num"].ToString(); //จำนวนบุตร

                a++;
            }

            if (dschild.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxChild_list = AddChild;
            }
        }


        if (ViewState["vsTemp_Prior_full"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior_full"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();
                AddPrior[b].Pri_description = dr["Pri_description"].ToString();
                AddPrior[b].Pri_salary = dr["Pri_salary"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }


        if (ViewState["vsTemp_Education_full"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education_full"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }


        if (ViewState["vsTemp_Train"] != null)
        {
            var dsTrain = (DataSet)ViewState["vsTemp_Train"];
            var AddTrain = new Train_List[dsTrain.Tables[0].Rows.Count];
            foreach (DataRow dr in dsTrain.Tables[0].Rows)
            {
                AddTrain[d] = new Train_List();
                AddTrain[d].Train_courses = dr["Train_courses"].ToString();
                AddTrain[d].Train_date = dr["Train_date"].ToString();
                AddTrain[d].Train_assessment = dr["Train_assessment"].ToString();

                d++;
            }

            if (dsTrain.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxTrain_list = AddTrain;
            }
        }

        //fsaw.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        //fsa.Text = _funcTool.convertObjectToJson(_dtEmployee);

        _dtEmployee = callServiceEmployee_Check(_urlSetInsertRegisterShotList, _dtEmployee);


        if (_dtEmployee.return_code == "9")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "มีรหัสบัตรประขาขนนี้อยู่ในระบบแล้ว" + "')", true);
        }
        else
        {
            Insert_static(1);
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "ข้อมูลของคุณถูกจัดส่งให้ HR เรียบร้อยแล้ว" + "')", true);
            Menu_Color(5);
           // MvMaster.SetActiveView(ViewPosition);
            SelectMaPositionList();
            Select_check_ipaddress();

            //if (ViewState["check_ip"].ToString() == "0") //เน็ตในบริษัท
            //{
            ViewState["ID_identity"] = txtidcard.Text;
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการบันทึก : " + "ข้อมูลของคุณถูกจัดส่งให้ HR เรียบร้อยแล้ว" + "')", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_full();", true);
            //}  
        }

        //Select_Employee();
        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "UploadError", "alert('" + "ผลการบันทึก : " + ViewState["return_sucess"].ToString() + " - รหัสพนักงาน : " + ViewState["return_code"].ToString() +  "')", true);
    }

    protected void Update_Employee()
    {
        var lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");
        var ddlstartdate_in = (DropDownList)FvEdit_Detail.FindControl("ddlstartdate_in");
        var txtsalary = (TextBox)FvEdit_Detail.FindControl("txtsalary");
        var ddltypecategory = (DropDownList)FvEdit_Detail.FindControl("ddltypecategory");
        var ddlpositionfocus = (DropDownList)FvEdit_Detail.FindControl("ddlpositionfocus");
        var ddlposition1 = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
        var ddlposition2 = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
        var ddlposition3 = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");
        var txtdescription = (TextBox)FvEdit_Detail.FindControl("txtdescription");

        var ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        var txt_name_th = (TextBox)FvEdit_Detail.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvEdit_Detail.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        var txt_name_en = (TextBox)FvEdit_Detail.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvEdit_Detail.FindControl("txt_surname_en");
        var txt_nickname_th = (TextBox)FvEdit_Detail.FindControl("txt_nickname_th");
        var txt_nickname_en = (TextBox)FvEdit_Detail.FindControl("txt_nickname_en");
        //var ddlcostcenter = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
        var txtbirth = (TextBox)FvEdit_Detail.FindControl("txtbirth");
        var ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
        var ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
        var ddlnation = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        var ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
        var ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
        var ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
        var txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvEdit_Detail.FindControl("txtissued_at");
        var txtexpcard = (TextBox)FvEdit_Detail.FindControl("txtexpcard");

        //var ddllocation = (DropDownList)FvEdit_Detail.FindControl("ddllocation");

        var ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
        var ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
        var ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
        var ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");

        var txtlicens_car = (TextBox)FvEdit_Detail.FindControl("txtlicen_car");
        var txtlicens_moto = (TextBox)FvEdit_Detail.FindControl("txtlicens_moto");
        var txtlicens_fork = (TextBox)FvEdit_Detail.FindControl("txtlicens_fork");
        var txtlicens_truck = (TextBox)FvEdit_Detail.FindControl("txtlicens_truck");
        var ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
        var ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

        //var dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
        var txtaccountname = (TextBox)FvEdit_Detail.FindControl("txtaccountname");
        var txtaccountnumber = (TextBox)FvEdit_Detail.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvEdit_Detail.FindControl("txtemercon");
        var txtrelation = (TextBox)FvEdit_Detail.FindControl("txtrelation");
        var txttelemer = (TextBox)FvEdit_Detail.FindControl("txttelemer");

        var txtfathername = (TextBox)FvEdit_Detail.FindControl("txtfathername_edit");
        var txttelfather = (TextBox)FvEdit_Detail.FindControl("txttelfather_edit");
        var txtlicenfather = (TextBox)FvEdit_Detail.FindControl("txtlicenfather_edit");
        var txtmothername = (TextBox)FvEdit_Detail.FindControl("txtmothername_edit");
        var txttelmother = (TextBox)FvEdit_Detail.FindControl("txttelmother_edit");
        var txtlicenmother = (TextBox)FvEdit_Detail.FindControl("txtlicenmother_edit");
        var txtwifename = (TextBox)FvEdit_Detail.FindControl("txtwifename_edit");
        var txttelwife = (TextBox)FvEdit_Detail.FindControl("txttelwife_edit");
        var txtlicenwifename = (TextBox)FvEdit_Detail.FindControl("txtlicenwife_edit");

        var txtchildname = (TextBox)FvEdit_Detail.FindControl("txtchildname");
        var ddlchildnumber = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber");
        var GvChildAdd = (GridView)FvEdit_Detail.FindControl("GvChildAdd");

        var txtorgold = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
        var txtposold = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
        var txtstartdate_ex = (TextBox)FvEdit_Detail.FindControl("txtstartdate_ex");
        var txtresigndate_ex = (TextBox)FvEdit_Detail.FindControl("txtresigndate_ex");
        var txtprisalary_edit = (TextBox)FvEdit_Detail.FindControl("txtprisalary_edit");
        var txtpridescription_edit = (TextBox)FvEdit_Detail.FindControl("txtpridescription_edit");
        var GvPri = (GridView)FvEdit_Detail.FindControl("GvPri");

        var ddleducationback = (DropDownList)FvEdit_Detail.FindControl("ddleducationback");
        var txtschoolname = (TextBox)FvEdit_Detail.FindControl("txtschoolname");
        var txtstarteducation = (TextBox)FvEdit_Detail.FindControl("txtstarteducation");
        var txtendeducation = (TextBox)FvEdit_Detail.FindControl("txtendeducation");
        var txtstudy = (TextBox)FvEdit_Detail.FindControl("txtstudy");
        var GvEducation = (GridView)FvEdit_Detail.FindControl("GvEducation");

        var txttraincourses = (TextBox)FvEdit_Detail.FindControl("txttraincourses");
        var txttraindate = (TextBox)FvEdit_Detail.FindControl("txttraindate");
        var txttrainassessment = (TextBox)FvEdit_Detail.FindControl("txttrainassessment");
        var GvTrain = (GridView)FvEdit_Detail.FindControl("GvTrain");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvEdit_Detail.FindControl("txtaddress_present");
        var ddlcountry_present = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
        var ddlprovince_present = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        var ddlamphoe_present = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        var ddldistrict_present = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        var txttelmobile_present = (TextBox)FvEdit_Detail.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvEdit_Detail.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvEdit_Detail.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvEdit_Detail.FindControl("txtemail_present");

        var txtaddress_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
        var ddlprovince_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        var ddlamphoe_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        var ddldistrict_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
        var txttelmobile_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelhome_permanentaddress");

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
        var txtexaminationdate = (TextBox)FvEdit_Detail.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvEdit_Detail.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvEdit_Detail.FindControl("txtheight");
        var txtweight = (TextBox)FvEdit_Detail.FindControl("txtweight");
        var ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
        var txtscar = (TextBox)FvEdit_Detail.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

        //------------------------------- Page 5

        var ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
        var ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(lbl_emp_idx.Text);

        _dataEmployee_[0].EmpINID = int.Parse(ddlstartdate_in.SelectedValue);
        _dataEmployee_[0].salary = int.Parse(txtsalary.Text);
        _dataEmployee_[0].JobTypeIDX = int.Parse(ddltypecategory.SelectedValue);
        _dataEmployee_[0].PIDX = int.Parse(ddlpositionfocus.SelectedValue);
        _dataEmployee_[0].PosGrop1 = int.Parse(ddlposition1.SelectedValue);
        _dataEmployee_[0].PosGrop2 = int.Parse(ddlposition2.SelectedValue);
        _dataEmployee_[0].PosGrop3 = int.Parse(ddlposition3.SelectedValue);
        _dataEmployee_[0].DetailProfile = txtdescription.Text;

        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        _dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        _dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        _dataEmployee_[0].costcenter_idx = 0;//int.Parse(ddlcostcenter.SelectedValue);
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; }

        //_dataEmployee_[0].LocIDX = int.Parse(ddllocation.SelectedValue); //No Base

        _dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue);
        _dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        _dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน
        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร
        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ

        _dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        _dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        _dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  No Base
        _dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  No Base
        _dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  No Base
        _dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  No Base
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; //  No Base
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  No Base
        _dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; //  No Base
        _dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  No Base
        _dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue);
        _dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue);

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //  No Base
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //  No Base
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text; //  No Base

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;  // ******

        _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _dataEmployee_[0].soc_no = txtsecurityid.Text;  // รหัสบัตรประกันสังคม       
        if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม

        if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        _dataEmployee_[0].Scar = txtscar.Text;  // แผลเป้น
        _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB

        _dtEmployee.employee_list = _dataEmployee_;

        if (ViewState["vsTemp_Refer_edit"] != null)
        {
            var dsrefer = (DataSet)ViewState["vsTemp_Refer_edit"];
            var Addrefer = new Referent_List[dsrefer.Tables[0].Rows.Count];
            foreach (DataRow dr in dsrefer.Tables[0].Rows)
            {
                Addrefer[f] = new Referent_List();
                Addrefer[f].ref_fullname = dr["ref_fullname"].ToString();
                Addrefer[f].ref_position = dr["ref_position"].ToString();
                Addrefer[f].ref_relation = dr["ref_relation"].ToString();
                Addrefer[f].ref_tel = dr["ref_tel"].ToString();

                f++;
            }

            if (dsrefer.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxReferent_list = Addrefer;
            }
        }


        if (ViewState["vsTemp_Child_edit"] != null)
        {
            var dschild = (DataSet)ViewState["vsTemp_Child_edit"];
            var AddChild = new Child_List[dschild.Tables[0].Rows.Count];
            foreach (DataRow dr in dschild.Tables[0].Rows)
            {
                AddChild[a] = new Child_List();
                AddChild[a].Child_name = dr["Child_name"].ToString(); //ชื่อบัตร
                AddChild[a].Child_num = dr["Child_num"].ToString(); //จำนวนบุตร

                a++;
            }

            if (dschild.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxChild_list = AddChild;
            }
        }


        if (ViewState["vsTemp_Prior_edit"] != null)
        {
            var dsPrior = (DataSet)ViewState["vsTemp_Prior_edit"];
            var AddPrior = new Prior_List[dsPrior.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPrior.Tables[0].Rows)
            {
                AddPrior[b] = new Prior_List();
                AddPrior[b].Pri_Nameorg = dr["Pri_Nameorg"].ToString();
                AddPrior[b].Pri_pos = dr["Pri_pos"].ToString();
                AddPrior[b].Pri_startdate = dr["Pri_startdate"].ToString();
                AddPrior[b].Pri_resigndate = dr["Pri_resigndate"].ToString();
                AddPrior[b].Pri_description = dr["Pri_description"].ToString();
                AddPrior[b].Pri_salary = dr["Pri_salary"].ToString();

                b++;
            }

            if (dsPrior.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxPrior_list = AddPrior;
            }
        }


        if (ViewState["vsTemp_Education_edit"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_Education_edit"];
            var AddEdu = new Education_List[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new Education_List();
                AddEdu[c].Edu_qualification_ID = dr["Edu_qualification_ID"].ToString();
                AddEdu[c].Edu_qualification = dr["Edu_qualification"].ToString();
                AddEdu[c].Edu_name = dr["Edu_name"].ToString();
                AddEdu[c].Edu_branch = dr["Edu_branch"].ToString();
                AddEdu[c].Edu_start = dr["Edu_start"].ToString();
                AddEdu[c].Edu_end = dr["Edu_end"].ToString();

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxEducation_list = AddEdu;
            }
        }


        if (ViewState["vsTemp_Train_edit"] != null)
        {
            var dsTrain = (DataSet)ViewState["vsTemp_Train_edit"];
            var AddTrain = new Train_List[dsTrain.Tables[0].Rows.Count];
            foreach (DataRow dr in dsTrain.Tables[0].Rows)
            {
                AddTrain[d] = new Train_List();
                AddTrain[d].Train_courses = dr["Train_courses"].ToString();
                AddTrain[d].Train_date = dr["Train_date"].ToString();
                AddTrain[d].Train_assessment = dr["Train_assessment"].ToString();

                d++;
            }

            if (dsTrain.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtEmployee.BoxTrain_list = AddTrain;
            }
        }


        try
        {
            //Label2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
            _dtEmployee = callServiceEmployee_Check(_urlSetupdate_employee_registerList, _dtEmployee);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        catch (FileNotFoundException ex)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    protected void Insert_static(int type_visit)
    {
        _dataEmployee = new data_employee();

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.type_select_emp = 10; //Index
        _dataEmployee_.ip_address = ViewState["ip_address"].ToString();
        _dataEmployee_.btype_idx = type_visit;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetEmployee_Register, _dataEmployee);
    }

    #endregion

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvInsertEmp":
                FormView FvInsertEmp = (FormView)ViewAdd_full.FindControl("FvInsertEmp");
                DropDownList ddlhospital = (DropDownList)FvInsertEmp.FindControl("ddlhospital");

                DropDownList ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
                DropDownList ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
                DropDownList ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
                DropDownList ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");

                DropDownList ddlcountry_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlcountry_permanentaddress");
                DropDownList ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");

                DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
                Image img_profile_full = (Image)FvInsertEmp.FindControl("img_profile_full");
                DropDownList ddlposition_1_full = (DropDownList)FvInsertEmp.FindControl("ddlposition1");
                DropDownList ddlposition_2_full = (DropDownList)FvInsertEmp.FindControl("ddlposition2");
                DropDownList ddlposition_3_full = (DropDownList)FvInsertEmp.FindControl("ddlposition3");
                DropDownList ddlpositionfocus_full = (DropDownList)FvInsertEmp.FindControl("ddlpositionfocus");

                if (FvInsertEmp.CurrentMode == FormViewMode.Insert)
                {
                    //Select_Coscenter(ddlcostcenter);
                    Select_hospital(ddlhospital);
                    //select_org(ddlorg);
                    //select_org(ddlorg_add);
                    select_country(ddlcountry_present);
                    ddlcountry_present.SelectedValue = "218";
                    select_province(ddlprovince_present);
                    select_country(ddlcountry_permanentaddress);
                    select_province(ddlprovince_permanentaddress);
                    //Select_Location(ddllocation);
                    select_nationality(ddlnation);
                    ddlnation.SelectedValue = "177";
                    img_profile_full.Visible = false;
                    SelectGroupPositionList(ddlposition_1_full);
                    SelectGroupPositionList(ddlposition_2_full);
                    SelectGroupPositionList(ddlposition_3_full);
                    SelectPositionList(ddlpositionfocus_full);
                }

                break;

            case "FvInsert_Mini":
                FormView FvInsert_Mini = (FormView)ViewAdd_mini.FindControl("FvInsert_Mini");
                DropDownList ddlcountry_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddlcountry_present_mini");
                DropDownList ddlprovince_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddlprovince_present_mini");
                DropDownList ddlamphoe_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddlamphoe_present_mini");
                DropDownList ddldistrict_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddldistrict_present_mini");
                Image img_profile_ = (Image)FvInsert_Mini.FindControl("img_profile");
                GridView gvFileEmp = (GridView)FvInsert_Mini.FindControl("gvFileEmp");
                DropDownList ddlposition_1 = (DropDownList)FvInsert_Mini.FindControl("ddlposition1");
                DropDownList ddlposition_2 = (DropDownList)FvInsert_Mini.FindControl("ddlposition2");
                DropDownList ddlposition_3 = (DropDownList)FvInsert_Mini.FindControl("ddlposition3");
                DropDownList ddlpositionfocus = (DropDownList)FvInsert_Mini.FindControl("ddlpositionfocus");

                if (FvInsert_Mini.CurrentMode == FormViewMode.Insert)
                {
                    select_country(ddlcountry_present_mini);
                    ddlcountry_present_mini.SelectedValue = "218";
                    select_province(ddlprovince_present_mini);
                    img_profile_.Visible = false;
                    setGridData(gvFileEmp, null);
                    SelectGroupPositionList(ddlposition_1);
                    SelectGroupPositionList(ddlposition_2);
                    SelectGroupPositionList(ddlposition_3);
                    SelectPositionList(ddlpositionfocus);
                }
                break;

            case "FvEdit_Detail":

                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");
                ViewState["fv_emp_idx"] = lbl_emp_idx.Text;
                Label lbl_cost_idx = (Label)FvEdit_Detail.FindControl("lbl_cost_idx");
                DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
                Label lbl_mil_idx = (Label)FvEdit_Detail.FindControl("lbl_mil_idx");

                Label lbl_probation_status = (Label)FvEdit_Detail.FindControl("lbl_probation_status");
                Label lbl_emp_status = (Label)FvEdit_Detail.FindControl("lbl_emp_status");
                Label lblemp_in_idx = (Label)FvEdit_Detail.FindControl("lblemp_in_idx");
                Label lbljob_type_idx = (Label)FvEdit_Detail.FindControl("lbljob_type_idx");
                Label lblPosGroupIDX_1 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_1");
                Label lblPosGroupIDX_2 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_2");
                Label lblPosGroupIDX_3 = (Label)FvEdit_Detail.FindControl("lblPosGroupIDX_3");

                DropDownList ddlstartdate_in = (DropDownList)FvEdit_Detail.FindControl("ddlstartdate_in");
                DropDownList ddltypecategory = (DropDownList)FvEdit_Detail.FindControl("ddltypecategory");
                DropDownList ddlposition1 = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
                DropDownList ddlposition2 = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
                DropDownList ddlposition3 = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");

                //ddl ข้อมูล Dropdown fix
                DropDownList ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
                DropDownList ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
                DropDownList ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
                DropDownList ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
                DropDownList ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
                DropDownList ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
                DropDownList ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary");
                DropDownList ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
                DropDownList ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
                DropDownList ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
                DropDownList ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");
                DropDownList ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
                DropDownList ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

                DropDownList ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
                DropDownList ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
                DropDownList ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

                Label lbl_prefix_idx = (Label)FvEdit_Detail.FindControl("lbl_prefix_idx");
                Label lbl_sex_idx = (Label)FvEdit_Detail.FindControl("lbl_sex_idx");
                Label lbl_married_status_idx = (Label)FvEdit_Detail.FindControl("lbl_married_status_idx");
                Label lbl_race_idx = (Label)FvEdit_Detail.FindControl("lbl_race_idx");
                Label lbl_rel_idx = (Label)FvEdit_Detail.FindControl("lbl_rel_idx");
                Label lbl_dvcar_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcar_idx");
                Label lbl_dvmt_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmt_idx");
                Label lbl_dvfork_idx = (Label)FvEdit_Detail.FindControl("lbl_dvfork_idx");
                Label lbl_dvtruck_idx = (Label)FvEdit_Detail.FindControl("lbl_dvtruck_idx");
                Label lbl_dvcar_status = (Label)FvEdit_Detail.FindControl("lbl_dvcar_status");
                Label lbl_dvmoto_status = (Label)FvEdit_Detail.FindControl("lbl_dvmoto_status");
                //Label lbl_bank_idx = (Label)FvEdit_Detail.FindControl("lbl_bank_idx");
                Label lbl_BRHIDX = (Label)FvEdit_Detail.FindControl("lbl_BRHIDX");
                Label lbl_medical_id = (Label)FvEdit_Detail.FindControl("lbl_medical_id");
                Label lbl_resultlab_id = (Label)FvEdit_Detail.FindControl("lbl_resultlab_id");

                //ที่อยู่ปัจจุบัน
                DropDownList ddlcountry_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
                DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
                DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
                DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
                Label lbl_country_edit = (Label)FvEdit_Detail.FindControl("lbl_country_edit");
                Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
                Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
                Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

                //ที่อยู่ตามทะเบียนบ้าน
                DropDownList ddlcountry_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
                DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
                DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
                DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
                Label lbl_ddlcountry_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlcountry_permanent_edit");
                Label lbl_ddlprovince_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlprovince_permanent_edit");
                Label lbl_ddlamphoe_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlamphoe_permanent_edit");
                Label lbl_ddldistrict_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddldistrict_permanent_edit");

                DropDownList ddlhospital_edit = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
                Label lbl_hos_edit = (Label)FvEdit_Detail.FindControl("lbl_hos_edit");
                Label lbl_location_idx = (Label)FvEdit_Detail.FindControl("lbl_location_idx");
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                Label lbl_nation_idx = (Label)FvEdit_Detail.FindControl("lbl_nation_idx");

                Image img_profile_edit = (Image)FvEdit_Detail.FindControl("img_profile_edit");
                GridView gvFileEmp_edit = (GridView)FvEdit_Detail.FindControl("gvFileEmp_edit");
                Label lblidentity_card_edit = (Label)FvEdit_Detail.FindControl("lblidentity_card_edit");
                Panel ImagProfile_default_edit = (Panel)FvEdit_Detail.FindControl("ImagProfile_default_edit");
                DropDownList ddlposition_1_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition1");
                DropDownList ddlposition_2_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition2");
                DropDownList ddlposition_3_edit = (DropDownList)FvEdit_Detail.FindControl("ddlposition3");
                DropDownList ddlpositionfocus_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpositionfocus");
                Label lblPIDX_edit = (Label)FvEdit_Detail.FindControl("lblPIDX_edit");

                if (FvEdit_Detail.CurrentMode == FormViewMode.Edit)
                {
                    ddlstartdate_in.SelectedValue = lblemp_in_idx.Text;
                    ddltypecategory.SelectedValue = lbljob_type_idx.Text;

                    ddlposition1.SelectedValue = lblPosGroupIDX_1.Text;

                    if (lblPosGroupIDX_2.Text != "0")
                    {
                        ddlposition2.SelectedValue = lblPosGroupIDX_2.Text;
                    }

                    if (lblPosGroupIDX_3.Text != "0")
                    {
                        ddlposition3.SelectedValue = lblPosGroupIDX_3.Text;
                    }

                    ddl_prefix_th.SelectedValue = lbl_prefix_idx.Text;
                    ddl_prefix_en.SelectedValue = lbl_prefix_idx.Text;
                    ddl_sex.SelectedValue = lbl_sex_idx.Text;
                    ddl_status.SelectedValue = lbl_married_status_idx.Text;
                    ddlrace.SelectedValue = lbl_race_idx.Text;
                    ddlreligion.SelectedValue = lbl_rel_idx.Text;
                    ddldvcar.SelectedValue = lbl_dvcar_idx.Text;
                    ddldvmt.SelectedValue = lbl_dvmt_idx.Text;
                    ddldvfork.SelectedValue = lbl_dvfork_idx.Text;
                    ddldvtruck.SelectedValue = lbl_dvtruck_idx.Text;
                    ddlblood.SelectedValue = lbl_BRHIDX.Text;
                    ddlmedicalcertificate.SelectedValue = lbl_medical_id.Text;
                    ddlresultlab.SelectedValue = lbl_resultlab_id.Text;
                    ddlmilitary_edit.SelectedValue = lbl_mil_idx.Text;

                    ddldvcarstatus.SelectedValue = lbl_dvcar_status.Text;
                    ddldvmtstatus.SelectedValue = lbl_dvmoto_status.Text;

                    Select_hospital(ddlhospital_edit);
                    ddlhospital_edit.SelectedValue = lbl_hos_edit.Text;

                    select_country(ddlcountry_present_edit);
                    ddlcountry_present_edit.SelectedValue = lbl_country_edit.Text;
                    select_province(ddlprovince_present_edit);
                    ddlprovince_present_edit.SelectedValue = lbl_prov_edit.Text;
                    select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                    ddlamphoe_present_edit.SelectedValue = lbl_amp_edit.Text;
                    select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                    ddldistrict_present_edit.SelectedValue = lbl_dist_edit.Text;

                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = lbl_ddlcountry_permanent_edit.Text;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = lbl_ddlprovince_permanent_edit.Text;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = lbl_ddlamphoe_permanent_edit.Text;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = lbl_ddldistrict_permanent_edit.Text;

                    Select_Refer_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Child_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Prior_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Education_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Train_view(int.Parse(ViewState["fv_emp_idx"].ToString()));

                    select_nationality(ddlnation_edit);
                    ddlnation_edit.SelectedValue = lbl_nation_idx.Text;
                    img_profile_edit.Visible = false;
                    SelectGroupPositionList(ddlposition_1_edit);
                    SelectGroupPositionList(ddlposition_2_edit);
                    SelectGroupPositionList(ddlposition_3_edit);
                    SelectPositionList(ddlpositionfocus_edit);
                    ddlpositionfocus_edit.SelectedValue = lblPIDX_edit.Text;

                    try
                    {
                        showimage_upload(lblidentity_card_edit.Text, img_profile_edit);
                        ImagProfile_default_edit.Visible = false;

                        string getPathLotus = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                        string filePathLotus = Server.MapPath(getPathLotus + lblidentity_card_edit.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                        SearchDirectories(myDirLotus, lblidentity_card_edit.Text, gvFileEmp_edit);
                    }
                    catch
                    {

                    }
                }
                break;

            case "FvDetail_Position":
                Label lblP_Piority = (Label)FvDetail_Position.FindControl("lblP_Piority");
                Label lblP_Name_fv = (Label)FvDetail_Position.FindControl("lblP_Name_fv");

                if (FvDetail_Position.CurrentMode == FormViewMode.ReadOnly)
                {
                    lblP_Piority_view.ForeColor = System.Drawing.Color.Yellow;

                    if (lblP_Piority.Text == "2")
                    {
                        lblP_Piority_view.Text = "รับสมัครด่วน !!!!!";
                    }
                    else
                    {
                        lblP_Piority_view.Text = "รับสมัคร !!!!!";
                    }

                    lblP_Name_View.Text = lblP_Name_fv.Text;
                    lblP_Namehead_View.ForeColor = System.Drawing.Color.WhiteSmoke;
                    lblP_Name_View.ForeColor = System.Drawing.Color.WhiteSmoke;
                }

                break;
        }
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        lbadd_mini.ForeColor = System.Drawing.Color.White;
        lbadd_full.ForeColor = System.Drawing.Color.White;
        lbedit.ForeColor = System.Drawing.Color.White;
        lbbenefit.ForeColor = System.Drawing.Color.White;
        lbposition.ForeColor = System.Drawing.Color.White;
        lbcontract.ForeColor = System.Drawing.Color.White;
        lbbacktkn.ForeColor = System.Drawing.Color.White;

        switch (choice)
        {
            case 0: //View Present             
                Box_Menu.Visible = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_index();", true);

                //Panel pn_bg_transfer = (Panel)Box_panel_present.FindControl("pn_bg_transfer");
                //pn_bg_transfer.Attributes.Add("style", "background-size: 100% 100%");
                //pn_bg_transfer.BackImageUrl = "~/masterpage/images/hr/pic_main_tranfer2.jpg";
                break;

            case 1: //Add_Mini
                Box_Menu.Visible = true;
                //Box_panel_present.Visible = false;
                lbadd_mini.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090");
                lbadd_full.BackColor = System.Drawing.Color.Transparent;
                lbedit.BackColor = System.Drawing.Color.Transparent;
                lbbenefit.BackColor = System.Drawing.Color.Transparent;
                lbposition.BackColor = System.Drawing.Color.Transparent;
                lbcontract.BackColor = System.Drawing.Color.Transparent;
                break;
            case 2: //Add_Full
                Box_Menu.Visible = true;
                Box_panel_present.Visible = false;
                lbadd_mini.BackColor = System.Drawing.Color.Transparent;
                lbadd_full.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090"); //System.Drawing.Color.Black;
                lbedit.BackColor = System.Drawing.Color.Transparent;
                lbbenefit.BackColor = System.Drawing.Color.Transparent;
                lbposition.BackColor = System.Drawing.Color.Transparent;
                lbcontract.BackColor = System.Drawing.Color.Transparent;
                break;
            case 3: //Edit   
                Box_Menu.Visible = true;
                //Box_panel_present.Visible = false;
                lbadd_mini.BackColor = System.Drawing.Color.Transparent;
                lbadd_full.BackColor = System.Drawing.Color.Transparent;
                lbedit.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090"); //System.Drawing.Color.Black;
                lbbenefit.BackColor = System.Drawing.Color.Transparent;
                lbposition.BackColor = System.Drawing.Color.Transparent;
                lbcontract.BackColor = System.Drawing.Color.Transparent;
                Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Index.DataBind();
                BoxEditEmployee.Visible = false;
                break;
            case 4: //Benefits             
                Box_Menu.Visible = true;
                //Box_panel_present.Visible = false;
                lbadd_mini.BackColor = System.Drawing.Color.Transparent;
                lbadd_full.BackColor = System.Drawing.Color.Transparent;
                lbedit.BackColor = System.Drawing.Color.Transparent;
                lbbenefit.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090"); //System.Drawing.Color.Black;
                lbposition.BackColor = System.Drawing.Color.Transparent;
                lbcontract.BackColor = System.Drawing.Color.Transparent;
                break;
            case 5: //Position            
                Box_Menu.Visible = true;
                Box_panel_present.Visible = false;
                lbadd_mini.BackColor = System.Drawing.Color.Transparent;
                lbadd_full.BackColor = System.Drawing.Color.Transparent;
                lbedit.BackColor = System.Drawing.Color.Transparent;
                lbbenefit.BackColor = System.Drawing.Color.Transparent;
                lbposition.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090");
                lbcontract.BackColor = System.Drawing.Color.Transparent;
                break;
            case 6: //Contract      
                Box_Menu.Visible = true;
                lbadd_mini.BackColor = System.Drawing.Color.Transparent;
                lbadd_full.BackColor = System.Drawing.Color.Transparent;
                lbedit.BackColor = System.Drawing.Color.Transparent;
                lbbenefit.BackColor = System.Drawing.Color.Transparent;
                lbposition.BackColor = System.Drawing.Color.Transparent;
                lbcontract.BackColor = System.Drawing.ColorTranslator.FromHtml("#708090");

                break;
        }
    }
    #endregion


    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvReference_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvChildAdd_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvChildAdd_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Child_num = (Label)e.Row.FindControl("lbl_Child_num");
                        var ddlchildnumber = (DropDownList)e.Row.FindControl("ddlchildnumber");
                        ddlchildnumber.SelectedValue = lbl_Child_num.Text;
                    }
                }
                break;
            case "GvPri_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "GvEducation_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEducation_View.EditIndex == e.Row.RowIndex)
                    {
                        var lbl_Edu_qualification_ID = (Label)e.Row.FindControl("lbl_Edu_qualification_ID");
                        var ddleducationback = (DropDownList)e.Row.FindControl("ddleducationback");
                        ddleducationback.SelectedValue = lbl_Edu_qualification_ID.Text;
                    }
                }
                break;
            case "GvTrain_View":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;
            case "GvMaPosition":
                var GvMaPosition = (GridView)ViewPosition.FindControl("GvMaPosition");

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lblPiority = (Label)e.Row.FindControl("lblPiority");
                    var img_status = (Image)e.Row.FindControl("img_status");

                    if (lblPiority.Text.ToString() != "0")
                    {
                        img_status.Visible = true;
                    }
                    else
                    {
                        img_status.Visible = false;
                    }
                }
                break;

        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.PageIndex = e.NewPageIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.PageIndex = e.NewPageIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.PageIndex = e.NewPageIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.PageIndex = e.NewPageIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.PageIndex = e.NewPageIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
            case "GvMaPosition":
                GvMaPosition.PageIndex = e.NewPageIndex;
                GvMaPosition.DataBind();
                SelectMaPositionList();
                break;
        }
    }

    #endregion

    #region GvRowEditing
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = e.NewEditIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = e.NewEditIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = e.NewEditIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = e.NewEditIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = e.NewEditIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
        }

    }
    #endregion

    #region GvRowUpdating
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "GvReference_View":

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                int ReIDX = Convert.ToInt32(GvReference_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtRef_Name_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_Name_edit");
                var txtRef_pos_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_pos_edit");
                var txtRef_relation_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_relation_edit");
                var txtRef_tel_edit = (TextBox)GvReference_View.Rows[e.RowIndex].FindControl("txtRef_tel_edit");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;
                _dataEmployee_ref.ref_fullname = txtRef_Name_edit.Text;
                _dataEmployee_ref.ref_position = txtRef_pos_edit.Text;
                _dataEmployee_ref.ref_relation = txtRef_relation_edit.Text;
                _dataEmployee_ref.ref_tel = txtRef_tel_edit.Text;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "GvChildAdd_View":

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                int CHIDX = Convert.ToInt32(GvChildAdd_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtChild_name_edit = (TextBox)GvChildAdd_View.Rows[e.RowIndex].FindControl("txtChild_name_edit");
                var ddlchildnumber = (DropDownList)GvChildAdd_View.Rows[e.RowIndex].FindControl("ddlchildnumber");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;
                _dataEmployee_1.Child_name = txtChild_name_edit.Text;
                _dataEmployee_1.Child_num = ddlchildnumber.SelectedValue.ToString();

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "GvPri_View":

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                int ExperIDX = Convert.ToInt32(GvPri_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtPri_Nameorg_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_Nameorg_edit");
                var txtPri_pos_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_pos_edit");
                //var txtPri_worktime_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txtPri_worktime_edit");
                var txt_Pri_start_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_start_edit");
                var txt_Pri_resign_edit = (TextBox)GvPri_View.Rows[e.RowIndex].FindControl("txt_Pri_resign_edit");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;
                _dataEmployee_2.Pri_Nameorg = txtPri_Nameorg_edit.Text;
                _dataEmployee_2.Pri_pos = txtPri_pos_edit.Text;
                //_dataEmployee_2.Pri_worktime = txtPri_worktime_edit.Text;
                _dataEmployee_2.Pri_startdate = txt_Pri_start_edit.Text;
                _dataEmployee_2.Pri_resigndate = txt_Pri_resign_edit.Text;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisPriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "GvEducation_View":

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                int EDUIDX = Convert.ToInt32(GvEducation_View.DataKeys[e.RowIndex].Values[0].ToString());
                var ddleducationback = (DropDownList)GvEducation_View.Rows[e.RowIndex].FindControl("ddleducationback");
                var txtEdu_name_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_name_edit");
                var txtEdu_branch_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_branch_edit");
                var txtEdu_start_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_start_edit");
                var txtEdu_end_edit = (TextBox)GvEducation_View.Rows[e.RowIndex].FindControl("txtEdu_end_edit");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;
                _dataEmployee_3.Edu_qualification_ID = ddleducationback.SelectedValue;
                _dataEmployee_3.Edu_name = txtEdu_name_edit.Text;
                _dataEmployee_3.Edu_branch = txtEdu_branch_edit.Text;
                _dataEmployee_3.Edu_start = txtEdu_start_edit.Text;
                _dataEmployee_3.Edu_end = txtEdu_end_edit.Text;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "GvTrain_View":

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                int TNIDX = Convert.ToInt32(GvTrain_View.DataKeys[e.RowIndex].Values[0].ToString());
                var txtTrain_courses_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_courses_edit");
                var txtTrain_date_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_date_edit");
                var txtTrain_assessment_edit = (TextBox)GvTrain_View.Rows[e.RowIndex].FindControl("txtTrain_assessment_edit");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;
                _dataEmployee_4.Train_courses = txtTrain_courses_edit.Text;
                _dataEmployee_4.Train_date = txtTrain_date_edit.Text;
                _dataEmployee_4.Train_assessment = txtTrain_assessment_edit.Text;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetUpdateRegisTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;

        }

    }

    #endregion

    #region GvRowCancelingEdit
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;


        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvReference_Edit":

                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var dsvsBu_ = (DataSet)ViewState["vsTemp_Refer_edit"];
                var drDr1_ = dsvsBu_.Tables[0].Rows;

                drDr1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer_edit"] = dsvsBu_;
                GvReference_Edit.EditIndex = -1;
                GvReference_Edit.DataSource = ViewState["vsTemp_Refer_edit"];
                GvReference_Edit.DataBind();

                break;

            case "GvReference":

                var GvReference = (GridView)FvInsertEmp.FindControl("GvReference");
                var dsvsR_ = (DataSet)ViewState["vsTemp_Refer"];
                var drDr1R_ = dsvsR_.Tables[0].Rows;

                drDr1R_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Refer"] = dsvsR_;
                GvReference.EditIndex = -1;
                GvReference.DataSource = ViewState["vsTemp_Refer"];
                GvReference.DataBind();

                break;

            case "GvChildAdd_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var dsvsBuyequipmentDelete_ = (DataSet)ViewState["vsTemp_Child_edit"];
                var drDriving_ = dsvsBuyequipmentDelete_.Tables[0].Rows;

                drDriving_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child_edit"] = dsvsBuyequipmentDelete_;
                GvChildAdd_Edit.EditIndex = -1;
                GvChildAdd_Edit.DataSource = ViewState["vsTemp_Child_edit"];
                GvChildAdd_Edit.DataBind();

                break;

            case "GvChildAdd":

                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var dsvsBuyequipmentDelete_1 = (DataSet)ViewState["vsTemp_Child"];
                var drDriving_1 = dsvsBuyequipmentDelete_1.Tables[0].Rows;

                drDriving_1.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Child"] = dsvsBuyequipmentDelete_1;
                GvChildAdd.EditIndex = -1;
                GvChildAdd.DataSource = ViewState["vsTemp_Child"];
                GvChildAdd.DataBind();

                break;

            case "GvPri":

                var GvPri = (GridView)FvInsert_Mini.FindControl("GvPri");
                var dsvsBuyequipmentDelete1 = (DataSet)ViewState["vsTemp_Prior"];
                var drDriving1 = dsvsBuyequipmentDelete1.Tables[0].Rows;

                drDriving1.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior"] = dsvsBuyequipmentDelete1;
                GvPri.EditIndex = -1;
                GvPri.DataSource = ViewState["vsTemp_Prior"];
                GvPri.DataBind();

                break;

            case "GvPri_full":

                var GvPri_full = (GridView)FvInsertEmp.FindControl("GvPri_full");
                var dsvsntDelete1_1 = (DataSet)ViewState["vsTemp_Prior_full"];
                var drDriving1_1 = dsvsntDelete1_1.Tables[0].Rows;

                drDriving1_1.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior_full"] = dsvsntDelete1_1;
                GvPri_full.EditIndex = -1;
                GvPri_full.DataSource = ViewState["vsTemp_Prior_full"];
                GvPri_full.DataBind();

                break;

            case "GvPri_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var dsvsBuyequipmentDelete1_ = (DataSet)ViewState["vsTemp_Prior_edit"];
                var drDriving1_ = dsvsBuyequipmentDelete1_.Tables[0].Rows;

                drDriving1_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Prior_edit"] = dsvsBuyequipmentDelete1_;
                GvPri_Edit.EditIndex = -1;
                GvPri_Edit.DataSource = ViewState["vsTemp_Prior_edit"];
                GvPri_Edit.DataBind();

                break;

            case "GvEducation":

                var GvEducation = (GridView)FvInsert_Mini.FindControl("GvEducation");
                var dsvsBuyequipmentDelete2 = (DataSet)ViewState["vsTemp_Education"];
                var drDriving2 = dsvsBuyequipmentDelete2.Tables[0].Rows;

                drDriving2.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education"] = dsvsBuyequipmentDelete2;
                GvEducation.EditIndex = -1;
                GvEducation.DataSource = ViewState["vsTemp_Education"];
                GvEducation.DataBind();

                break;

            case "GvEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var dsvsBuyequipmentDelete2_ = (DataSet)ViewState["vsTemp_Education_edit"];
                var drDriving2_ = dsvsBuyequipmentDelete2_.Tables[0].Rows;

                drDriving2_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education_edit"] = dsvsBuyequipmentDelete2_;
                GvEducation_Edit.EditIndex = -1;
                GvEducation_Edit.DataSource = ViewState["vsTemp_Education_edit"];
                GvEducation_Edit.DataBind();

                break;

            case "GvEducation_full":

                var GvEducation_full = (GridView)FvInsertEmp.FindControl("GvEducation_full");
                var dsvsBete2 = (DataSet)ViewState["vsTemp_Education_full"];
                var drDiving2 = dsvsBete2.Tables[0].Rows;

                drDiving2.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Education_full"] = dsvsBete2;
                GvEducation_full.EditIndex = -1;
                GvEducation_full.DataSource = ViewState["vsTemp_Education_full"];
                GvEducation_full.DataBind();

                break;

            case "GvTrain":

                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var dsvsBuyequipmentDelete3 = (DataSet)ViewState["vsTemp_Train"];
                var drDriving3 = dsvsBuyequipmentDelete3.Tables[0].Rows;

                drDriving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train"] = dsvsBuyequipmentDelete3;
                GvTrain.EditIndex = -1;
                GvTrain.DataSource = ViewState["vsTemp_Train"];
                GvTrain.DataBind();

                break;

            case "GvTrain_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var dsvsBuyequipmentDelete3_ = (DataSet)ViewState["vsTemp_Train_edit"];
                var drDriving3_ = dsvsBuyequipmentDelete3_.Tables[0].Rows;

                drDriving3_.RemoveAt(e.RowIndex);

                ViewState["vsTemp_Train_edit"] = dsvsBuyequipmentDelete3_;
                GvTrain_Edit.EditIndex = -1;
                GvTrain_Edit.DataSource = ViewState["vsTemp_Train_edit"];
                GvTrain_Edit.DataBind();

                break;

            case "GvPosAdd":

                var GvPosAdd = (GridView)FvInsertEmp.FindControl("GvPosAdd");
                var Delete3 = (DataSet)ViewState["vsTemp_PosAdd"];
                var driving3 = Delete3.Tables[0].Rows;

                driving3.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd"] = Delete3;
                GvPosAdd.EditIndex = -1;
                GvPosAdd.DataSource = ViewState["vsTemp_PosAdd"];
                GvPosAdd.DataBind();

                break;

            case "GvPosAdd_Edit":

                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var Delete4 = (DataSet)ViewState["vsTemp_PosAdd_edit"];
                var driving4 = Delete4.Tables[0].Rows;

                driving4.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd_edit"] = Delete4;
                GvPosAdd_Edit.EditIndex = -1;
                GvPosAdd_Edit.DataSource = ViewState["vsTemp_PosAdd_edit"];
                GvPosAdd_Edit.DataBind();

                break;
        }

    }
    #endregion

    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "ckreference":

                var ckreference = (CheckBox)FvInsertEmp.FindControl("ckreference");
                var GvReferenceAdd = (GridView)FvInsertEmp.FindControl("GvReference");
                var BoxReference = (Panel)FvInsertEmp.FindControl("BoxReference");

                if (ckreference.Checked)
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = false;
                    GvReferenceAdd.DataSource = null;
                    GvReferenceAdd.DataBind();
                }
                break;

            case "ckreference_edit":

                var ckreference_edit = (CheckBox)FvEdit_Detail.FindControl("ckreference_edit");
                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var BoxReference_edit = (Panel)FvEdit_Detail.FindControl("BoxReference");

                if (ckreference_edit.Checked)
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = false;
                    GvReference_Edit.DataSource = null;
                    GvReference_Edit.DataBind();
                }
                break;

            case "chkchild":

                var chkchild = (CheckBox)FvInsertEmp.FindControl("chkchild");
                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var BoxChild = (Panel)FvInsertEmp.FindControl("BoxChild");

                if (chkchild.Checked)
                {
                    ViewState["vsTemp_Child"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child"] = dsEquipment;
                    GvChildAdd.DataSource = dsEquipment.Tables[0];
                    GvChildAdd.DataBind();

                    BoxChild.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child"] = dsEquipment;
                    GvChildAdd.DataSource = dsEquipment.Tables[0];
                    GvChildAdd.DataBind();

                    BoxChild.Visible = false;
                    GvChildAdd.DataSource = null;
                    GvChildAdd.DataBind();
                }
                break;

            case "chkchild_edit":

                var chkchild_edit = (CheckBox)FvEdit_Detail.FindControl("chkchild_edit");
                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var BoxChild_edit = (Panel)FvEdit_Detail.FindControl("BoxChild");

                if (chkchild_edit.Checked)
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = false;
                    GvChildAdd_Edit.DataSource = null;
                    GvChildAdd_Edit.DataBind();
                }
                break;

            case "chkorg":

                var chkorg = (CheckBox)FvInsert_Mini.FindControl("chkorg");
                var GvPri = (GridView)FvInsert_Mini.FindControl("GvPri");
                var BoxOrgOld = (Panel)FvInsert_Mini.FindControl("BoxOrgOld_mini");

                if (chkorg.Checked)
                {
                    ViewState["vsTemp_Prior"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("OrgOld");

                    dsEquipment.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_salary", typeof(String));

                    ViewState["vsTemp_Prior"] = dsEquipment;
                    GvPri.DataSource = dsEquipment.Tables[0];
                    GvPri.DataBind();

                    BoxOrgOld.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Prior"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("OrgOld");

                    dsEquipment.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Pri_salary", typeof(String));

                    ViewState["vsTemp_Prior"] = dsEquipment;
                    GvPri.DataSource = dsEquipment.Tables[0];
                    GvPri.DataBind();

                    BoxOrgOld.Visible = false;
                    GvPri.DataSource = null;
                    GvPri.DataBind();
                }
                break;

            case "chkorg_full":

                var chkorg_full = (CheckBox)FvInsertEmp.FindControl("chkorg_full");
                var GvPri_full = (GridView)FvInsertEmp.FindControl("GvPri_full");
                var BoxOrgOld_full = (Panel)FvInsertEmp.FindControl("BoxOrgOld_full");


                if (chkorg_full.Checked)
                {
                    ViewState["vsTemp_Prior_full"] = null;

                    var dspri_full = new DataSet();
                    dspri_full.Tables.Add("OrgOld_full");

                    dspri_full.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_salary", typeof(String));

                    ViewState["vsTemp_Prior_full"] = dspri_full;
                    GvPri_full.DataSource = dspri_full.Tables[0];
                    GvPri_full.DataBind();

                    BoxOrgOld_full.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior"] = null;

                    var dspri_full = new DataSet();
                    dspri_full.Tables.Add("OrgOld_full");

                    dspri_full.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dspri_full.Tables[0].Columns.Add("Pri_salary", typeof(String));

                    ViewState["vsTemp_Prior"] = dspri_full;
                    GvPri_full.DataSource = dspri_full.Tables[0];
                    GvPri_full.DataBind();

                    BoxOrgOld_full.Visible = false;
                    GvPri_full.DataSource = null;
                    GvPri_full.DataBind();
                }
                break;

            case "chkorg_edit":

                var chkorg_edit = (CheckBox)FvEdit_Detail.FindControl("chkorg_edit");
                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var BoxOrgOld_edit = (Panel)FvEdit_Detail.FindControl("BoxOrgOld");


                if (chkorg_edit.Checked)
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_salary", typeof(String));


                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_description", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_salary", typeof(String));

                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = false;
                    GvPri_Edit.DataSource = null;
                    GvPri_Edit.DataBind();
                }


                break;

            case "ckeducation":

                var ckeducation = (CheckBox)FvInsert_Mini.FindControl("ckeducation");
                var GvEducation = (GridView)FvInsert_Mini.FindControl("GvEducation");
                var BoxEducation = (Panel)FvInsert_Mini.FindControl("BoxEducation");

                if (ckeducation.Checked)
                {
                    ViewState["vsTemp_Education"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Edu");

                    dsEquipment.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education"] = dsEquipment;
                    GvEducation.DataSource = dsEquipment.Tables[0];
                    GvEducation.DataBind();

                    BoxEducation.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Edu");

                    dsEquipment.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education"] = dsEquipment;
                    GvEducation.DataSource = dsEquipment.Tables[0];
                    GvEducation.DataBind();

                    BoxEducation.Visible = false;
                    GvEducation.DataSource = null;
                    GvEducation.DataBind();
                }
                break;

            case "ckeducation_full":

                var ckeducation_full = (CheckBox)FvInsertEmp.FindControl("ckeducation_full");
                var GvEducation_full = (GridView)FvInsertEmp.FindControl("GvEducation_full");
                var BoxEducation_full = (Panel)FvInsertEmp.FindControl("BoxEducation_full");

                if (ckeducation_full.Checked)
                {
                    ViewState["vsTemp_Education_full"] = null;

                    var dsfull = new DataSet();
                    dsfull.Tables.Add("Edu");

                    dsfull.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsfull.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_full"] = dsfull;
                    GvEducation_full.DataSource = dsfull.Tables[0];
                    GvEducation_full.DataBind();

                    BoxEducation_full.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education_full"] = null;

                    var dsfull = new DataSet();
                    dsfull.Tables.Add("Edu");

                    dsfull.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsfull.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsfull.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_full"] = dsfull;
                    GvEducation_full.DataSource = dsfull.Tables[0];
                    GvEducation_full.DataBind();

                    BoxEducation_full.Visible = false;
                    GvEducation_full.DataSource = null;
                    GvEducation_full.DataBind();
                }
                break;

            case "ckeducation_edit":

                var ckeducation_edit = (CheckBox)FvEdit_Detail.FindControl("ckeducation_edit");
                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var BoxEducation_edit = (Panel)FvEdit_Detail.FindControl("BoxEducation");

                if (ckeducation_edit.Checked)
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = false;
                    GvEducation_Edit.DataSource = null;
                    GvEducation_Edit.DataBind();
                }


                break;

            case "cktraining":

                var cktraining = (CheckBox)FvInsertEmp.FindControl("cktraining");
                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var BoxTrain = (Panel)FvInsertEmp.FindControl("BoxTrain");


                if (cktraining.Checked)
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = false;
                    GvTrain.DataSource = null;
                    GvTrain.DataBind();
                }
                break;

            case "cktraining_edit":

                var cktraining_edit = (CheckBox)FvEdit_Detail.FindControl("cktraining_edit");
                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var BoxTrain_edit = (Panel)FvEdit_Detail.FindControl("BoxTrain");


                if (cktraining_edit.Checked)
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = false;
                    GvTrain_Edit.DataSource = null;
                    GvTrain_Edit.DataBind();
                }


                break;

            case "chkposition_edit":

                var chkposition_edit = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var BoxPosition_edit = (Panel)FvEdit_Detail.FindControl("BoxPosition_edit");

                if (chkposition_edit.Checked)
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = false;
                    GvPosAdd_Edit.DataSource = null;
                    GvPosAdd_Edit.DataBind();
                }
                break;

            case "chkAddress":
                var chkAddress = (CheckBox)FvInsertEmp.FindControl("chkAddress");

                //ViewState["ddlamphoe_permanentaddress"] = 0;
                //ViewState["ddldistrict_present"] = 0;

                var BoxAddress_parent = (Panel)FvInsertEmp.FindControl("BoxAddress_parent");
                var txtaddress_present = (TextBox)BoxAddress_parent.FindControl("txtaddress_present");
                var ddlcountry_present = (DropDownList)BoxAddress_parent.FindControl("ddlcountry_present");
                var ddlprovince_present = (DropDownList)BoxAddress_parent.FindControl("ddlprovince_present");
                var ddlamphoe_present = (DropDownList)BoxAddress_parent.FindControl("ddlamphoe_present");
                var ddldistrict_present = (DropDownList)BoxAddress_parent.FindControl("ddldistrict_present");
                var txttelmobile_present = (TextBox)BoxAddress_parent.FindControl("txttelmobile_present");
                var txttelhome_present = (TextBox)BoxAddress_parent.FindControl("txttelhome_present");
                var txtzipcode_present = (TextBox)BoxAddress_parent.FindControl("txtzipcode_present");
                var txtemail_present = (TextBox)BoxAddress_parent.FindControl("txtemail_present");

                var txtaddress_permanentaddress = (TextBox)BoxAddress_parent.FindControl("txtaddress_permanentaddress");
                var ddlcountry_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlcountry_permanentaddress");
                var ddlprovince_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlprovince_permanentaddress");
                var ddlamphoe_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddlamphoe_permanentaddress");
                var ddldistrict_permanentaddress = (DropDownList)BoxAddress_parent.FindControl("ddldistrict_permanentaddress");
                var txttelmobile_permanentaddress = (TextBox)BoxAddress_parent.FindControl("txttelmobile_permanentaddress");


                //ViewState["ddldistrict_present"] = ddldistrict_present.SelectedValue;

                if (chkAddress.Checked)
                {
                    BoxAddress_parent.Visible = false;
                    txtaddress_permanentaddress.Text = txtaddress_present.Text;
                    select_country(ddlcountry_permanentaddress);
                    ddlcountry_permanentaddress.SelectedValue = ddlcountry_present.SelectedValue;
                    select_province(ddlprovince_permanentaddress);
                    ddlprovince_permanentaddress.SelectedValue = ddlprovince_present.SelectedValue;
                    select_amphur(ddlamphoe_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue));
                    ddlamphoe_permanentaddress.SelectedValue = ddlamphoe_present.SelectedValue;
                    select_dist(ddldistrict_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue), int.Parse(ddlamphoe_permanentaddress.SelectedValue));
                    ddldistrict_permanentaddress.SelectedValue = ddldistrict_present.SelectedValue;
                    txttelmobile_permanentaddress.Text = txttelmobile_present.Text;

                    txtaddress_permanentaddress.Enabled = false;
                    ddlcountry_permanentaddress.Enabled = false;
                    ddlprovince_permanentaddress.Enabled = false;
                    ddlamphoe_permanentaddress.Enabled = false;
                    ddldistrict_permanentaddress.Enabled = false;
                    txttelmobile_permanentaddress.Enabled = false;
                }
                else
                {
                    BoxAddress_parent.Visible = true;
                    select_country(ddlcountry_permanentaddress);
                    select_province(ddlprovince_permanentaddress);

                    txtaddress_permanentaddress.Text = null;
                    txtaddress_permanentaddress.Enabled = true;
                    ddlcountry_permanentaddress.Enabled = true;
                    ddlprovince_permanentaddress.Enabled = true;
                    ddlamphoe_permanentaddress.Enabled = true;
                    ddlamphoe_permanentaddress.SelectedValue = null;
                    ddldistrict_permanentaddress.Enabled = true;
                    ddldistrict_permanentaddress.SelectedValue = null;
                    txttelmobile_permanentaddress.Text = null;
                    txttelmobile_permanentaddress.Enabled = true;
                }

                break;

            case "chkAddress_edit":
                var chkAddress_edit = (CheckBox)FvEdit_Detail.FindControl("chkAddress_edit");
                var BoxAddress_edit = (Panel)FvEdit_Detail.FindControl("BoxAddress_edit");
                var BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");

                var txtaddress_present_edit = (TextBox)BoxAddress.FindControl("txtaddress_present");
                var ddlcountry_present_edit = (DropDownList)BoxAddress.FindControl("ddlcountry_present_edit");
                var ddlprovince_present_edit = (DropDownList)BoxAddress.FindControl("ddlprovince_present_edit");
                var ddlamphoe_present_edit = (DropDownList)BoxAddress.FindControl("ddlamphoe_present_edit");
                var ddldistrict_present_edit = (DropDownList)BoxAddress.FindControl("ddldistrict_present_edit");
                var txttelmobile_present_edit = (TextBox)BoxAddress.FindControl("txttelmobile_present");
                var txttelhome_present_edit = (TextBox)BoxAddress.FindControl("txttelhome_present");
                var txtzipcode_present_edit = (TextBox)BoxAddress.FindControl("txtzipcode_present");
                var txtemail_present_edit = (TextBox)BoxAddress.FindControl("txtemail_present");

                var txtaddress_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txtaddress_permanentaddress");
                var ddlcountry_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlcountry_permanentaddress_edit");
                var ddlprovince_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlprovince_permanentaddress_edit");
                var ddlamphoe_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddlamphoe_permanentaddress_edit");
                var ddldistrict_permanentaddress_edit = (DropDownList)BoxAddress_edit.FindControl("ddldistrict_permanentaddress_edit");
                var txttelmobile_permanentaddress_edit = (TextBox)BoxAddress_edit.FindControl("txttelmobile_permanentaddress");


                if (chkAddress_edit.Checked)
                {
                    BoxAddress_edit.Visible = false;
                    txtaddress_permanentaddress_edit.Text = txtaddress_present_edit.Text;
                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = ddlcountry_present_edit.SelectedValue;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = ddlprovince_present_edit.SelectedValue;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = ddlamphoe_present_edit.SelectedValue;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = ddldistrict_present_edit.SelectedValue;
                    txttelmobile_permanentaddress_edit.Text = txttelmobile_present_edit.Text;

                    txtaddress_permanentaddress_edit.Enabled = false;
                    ddlcountry_permanentaddress_edit.Enabled = false;
                    ddlprovince_permanentaddress_edit.Enabled = false;
                    ddlamphoe_permanentaddress_edit.Enabled = false;
                    ddldistrict_permanentaddress_edit.Enabled = false;
                    txttelmobile_permanentaddress_edit.Enabled = false;
                }
                else
                {
                    BoxAddress_edit.Visible = true;
                    select_country(ddlcountry_permanentaddress_edit);
                    select_province(ddlprovince_permanentaddress_edit);

                    txtaddress_permanentaddress_edit.Text = null;
                    txtaddress_permanentaddress_edit.Enabled = true;
                    ddlcountry_permanentaddress_edit.Enabled = true;
                    ddlprovince_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.Enabled = true;
                    ddlamphoe_permanentaddress_edit.SelectedValue = null;
                    ddldistrict_permanentaddress_edit.Enabled = true;
                    ddldistrict_permanentaddress_edit.SelectedValue = null;
                    txttelmobile_permanentaddress_edit.Text = null;
                    txttelmobile_permanentaddress_edit.Enabled = true;
                }
                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
        TextBox txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
        TextBox txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");

        DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        DropDownList ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        DropDownList ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        DropDownList ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

        DropDownList ddlcountry_present = (DropDownList)FvInsertEmp.FindControl("ddlcountry_present");
        DropDownList ddlprovince_present = (DropDownList)FvInsertEmp.FindControl("ddlprovince_present");
        DropDownList ddlamphoe_present = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_present");
        DropDownList ddldistrict_present = (DropDownList)FvInsertEmp.FindControl("ddldistrict_present");

        DropDownList ddlprovince_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlprovince_permanentaddress");
        DropDownList ddlamphoe_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddlamphoe_permanentaddress");
        DropDownList ddldistrict_permanentaddress = (DropDownList)FvInsertEmp.FindControl("ddldistrict_permanentaddress");

        DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");

        DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

        DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
        Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
        Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

        DropDownList ddl_prefix_th = (DropDownList)FvInsert_Mini.FindControl("ddl_prefix_th");
        DropDownList ddl_prefix_en = (DropDownList)FvInsert_Mini.FindControl("ddl_prefix_en");
        DropDownList ddlmilitary = (DropDownList)FvInsert_Mini.FindControl("ddlmilitary");
        DropDownList ddl_sex = (DropDownList)FvInsert_Mini.FindControl("ddl_sex");

        DropDownList ddl_prefix_th_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        DropDownList ddl_prefix_en_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");

        DropDownList ddlprovince_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddlprovince_present_mini");
        DropDownList ddlamphoe_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddlamphoe_present_mini");
        DropDownList ddldistrict_present_mini = (DropDownList)FvInsert_Mini.FindControl("ddldistrict_present_mini");

        DropDownList ddl_prefix_th_full = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_th_full");
        DropDownList ddl_prefix_en_full = (DropDownList)FvInsertEmp.FindControl("ddl_prefix_en_full");
        DropDownList ddlmilitary_full = (DropDownList)FvInsertEmp.FindControl("ddlmilitary_full");
        DropDownList ddl_sex_full = (DropDownList)FvInsertEmp.FindControl("ddl_sex_full");

        switch (ddName.ID)
        {

            case "ddlorg_add_edit":
                select_dep(ddldep_add_edit, int.Parse(ddlorg_add_edit.SelectedValue));
                break;
            case "ddldep_add_edit":
                select_sec(ddlsec_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue));
                break;
            case "ddlsec_add_edit":
                select_pos(ddlpos_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue), int.Parse(ddlsec_add_edit.SelectedValue));
                break;

            case "ddlprovince_present_mini":
                select_amphur(ddlamphoe_present_mini, int.Parse(ddlprovince_present_mini.SelectedValue));
                break;
            case "ddlamphoe_present_mini":
                select_dist(ddldistrict_present_mini, int.Parse(ddlprovince_present_mini.SelectedValue), int.Parse(ddlamphoe_present_mini.SelectedValue));
                break;

            case "ddlprovince_present":
                select_amphur(ddlamphoe_present, int.Parse(ddlprovince_present.SelectedValue));
                break;
            case "ddlamphoe_present":
                select_dist(ddldistrict_present, int.Parse(ddlprovince_present.SelectedValue), int.Parse(ddlamphoe_present.SelectedValue));
                break;

            case "ddlprovince_permanentaddress":
                select_amphur(ddlamphoe_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress":
                select_dist(ddldistrict_permanentaddress, int.Parse(ddlprovince_permanentaddress.SelectedValue), int.Parse(ddlamphoe_permanentaddress.SelectedValue));
                break;

            case "ddlprovince_present_edit":
                select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                break;
            case "ddlamphoe_present_edit":
                select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                break;

            case "ddlprovince_permanentaddress_edit":
                select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress_edit":
                select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                break;
            case "ddl_prefix_th":
                if (ddl_prefix_th.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en.SelectedValue = "1";
                    ddlmilitary.SelectedValue = "1";
                    ddl_sex.SelectedValue = "1";
                }
                else if (ddl_prefix_th.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en.SelectedValue = "2";
                    ddlmilitary.SelectedValue = "3";
                    ddl_sex.SelectedValue = "2";
                }
                else if (ddl_prefix_th.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en.SelectedValue = "3";
                    ddlmilitary.SelectedValue = "3";
                    ddl_sex.SelectedValue = "2";
                }
                break;
            case "ddl_prefix_en":
                if (ddl_prefix_en.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th.SelectedValue = "1";
                    ddlmilitary.SelectedValue = "1";
                    ddl_sex.SelectedValue = "1";
                }
                else if (ddl_prefix_en.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th.SelectedValue = "2";
                    ddlmilitary.SelectedValue = "3";
                    ddl_sex.SelectedValue = "2";
                }
                else if (ddl_prefix_en.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th.SelectedValue = "3";
                    ddlmilitary.SelectedValue = "3";
                    ddl_sex.SelectedValue = "2";
                }
                break;
            case "ddl_prefix_th_full":
                if (ddl_prefix_th_full.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en_full.SelectedValue = "1";
                    ddlmilitary_full.SelectedValue = "1";
                    ddl_sex_full.SelectedValue = "1";
                }
                else if (ddl_prefix_th_full.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en_full.SelectedValue = "2";
                    ddlmilitary_full.SelectedValue = "3";
                    ddl_sex_full.SelectedValue = "2";
                }
                else if (ddl_prefix_th_full.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en_full.SelectedValue = "3";
                    ddlmilitary_full.SelectedValue = "3";
                    ddl_sex_full.SelectedValue = "2";
                }
                break;
            case "ddl_prefix_en_full":
                if (ddl_prefix_en_full.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th_full.SelectedValue = "1";
                    ddlmilitary_full.SelectedValue = "1";
                    ddl_sex_full.SelectedValue = "1";
                }
                else if (ddl_prefix_en_full.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th_full.SelectedValue = "2";
                    ddlmilitary_full.SelectedValue = "3";
                    ddl_sex_full.SelectedValue = "2";
                }
                else if (ddl_prefix_en_full.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th_full.SelectedValue = "3";
                    ddlmilitary_full.SelectedValue = "3";
                    ddl_sex_full.SelectedValue = "2";
                }
                break;
            case "ddl_prefix_th_edit":
                if (ddl_prefix_th_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "1";
                }
                break;
            case "ddl_prefix_en_edit":
                if (ddl_prefix_en_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;

            case "ddlnation":
                if (ddlnation.SelectedValue == "177")
                {
                    txtsecurityid.Text = txtidcard.Text;
                }
                else
                {
                    ddlmilitary.SelectedValue = "3";
                }
                break;
            case "ddlnation_edit":
                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
        }
    }

    #endregion

    #region callService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceEmployee_Check(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;
        //Label2.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        //ViewState["return_code"] = _dataEmployee.return_code;
        //ViewState["return_sucess"] = _dataEmployee.return_msg;

        return _dataEmployee;
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnSave1":
                Select_check_ipaddress();

                //ViewState["check_ip"] = "0";
                if (ViewState["check_ip"].ToString() == "0") //เน็ตในบริษัท
                {
                    //Go To Test
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "คุณสามารถทำข้อสอบได้" + "')", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_edit();", true);
                    Menu_Color(2);
                    MvMaster.SetActiveView(ViewAdd_full);
                    FvInsertEmp.ChangeMode(FormViewMode.Insert);
                    FvInsertEmp.DataBind();
                    Page.Title = "TKN Register - Form Register Full";

                    DropDownList ddlnation_p = (DropDownList)FvInsertEmp.FindControl("ddlnation");
                    TextBox txtidcard_p = (TextBox)FvInsertEmp.FindControl("txtidcard");
                    TextBox txtsecurityid_p = (TextBox)FvInsertEmp.FindControl("txtsecurityid");

                    if (ddlnation_p.SelectedValue == "177")
                    {
                        txtsecurityid_p.Text = txtidcard_p.Text;
                    }
                }
                else
                {
                    Menu_Color(5);
                    MvMaster.SetActiveView(ViewPosition);
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "คุณไม่มีสิทธิ์ทำข้อสอบ กรุณาติดต่อ HR" + "')", true);
                }
                break;
            case "btnadd_mini":

                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewAdd_mini);

                FvInsert_Mini.ChangeMode(FormViewMode.Insert);
                FvInsert_Mini.DataBind();
                txtfocus.Focus();
                Page.Title = "TKN Register - Form Register Short";

                FormView FvInsert_Mini1 = (FormView)ViewAdd_mini.FindControl("FvInsert_Mini");
                LinkButton btn_save_file1 = (LinkButton)FvInsert_Mini1.FindControl("btn_save_file1");
                LinkButton btn_save_document = (LinkButton)FvInsert_Mini1.FindControl("btn_save_document");

                btnTrigger(btn_save_file1);
                btnTrigger(btn_save_document);
                btnTrigger(lbedit);
                break;
            case "btnadd_full":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewAdd_full);
                FvInsertEmp.ChangeMode(FormViewMode.Insert);
                FvInsertEmp.DataBind();
                Page.Title = "TKN Register - Form Register Full";

                DropDownList ddlnation = (DropDownList)FvInsertEmp.FindControl("ddlnation");
                TextBox txtidcard = (TextBox)FvInsertEmp.FindControl("txtidcard");
                TextBox txtsecurityid = (TextBox)FvInsertEmp.FindControl("txtsecurityid");

                if (ddlnation.SelectedValue == "177")
                {
                    txtsecurityid.Text = txtidcard.Text;
                }

                break;

            case "btnEdit":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewEdit);

                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");
                Page.Title = "TKN Register - Edit Profile";
                /*if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }*/
                break;
            case "btnBenefits":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewBenefits);
                Page.Title = "TKN Register - Benefits";
                break;
            case "btnPosition":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewPosition);
                SelectMaPositionList();
                Page.Title = "TKN Register - Position";
                break;
            case "btnContract":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewContract);
                Page.Title = "TKN Register - Contract";
                break;
            case "btnbacktaokaenoi":
                Menu_Color(int.Parse(cmdArg));
                Response.Redirect("http://www.taokaenoi.co.th");
                break;

            case "btn_search_index":
                //Select_Employee_index_search();
                TextBox txtidcard_search = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");

                Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Index.DataBind();

                BoxEditEmployee.Visible = true;

                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                Page.Title = "TKN Register - Edit Profile";
                Select_Fv_Detail(FvEdit_Detail, txtidcard_search.Text);
                ViewState["ID_identity"] = txtidcard_search.Text;
                break;

            case "btnSave": // Save Data ----------------------------
                Insert_Employee();
                break;
            case "btnUpdate":
                Update_Employee();
                break;

            case "btnSave_full":
                Insert_Employee_Full();

                break;

            case "AddChild":

                var GvChildAdd = (GridView)FvInsertEmp.FindControl("GvChildAdd");
                var txtchildname = (TextBox)FvInsertEmp.FindControl("txtchildname");
                var ddlchildnumber = (DropDownList)FvInsertEmp.FindControl("ddlchildnumber");

                var dsEquiment = (DataSet)ViewState["vsTemp_Child"];

                var drEquiment = dsEquiment.Tables[0].NewRow();
                drEquiment["Child_name"] = txtchildname.Text;
                drEquiment["Child_num"] = ddlchildnumber.SelectedValue;

                dsEquiment.Tables[0].Rows.Add(drEquiment);

                ViewState["vsTemp_Child"] = dsEquiment;

                GvChildAdd.DataSource = dsEquiment.Tables[0];
                GvChildAdd.DataBind();

                txtchildname.Text = null;
                ddlchildnumber.SelectedValue = null;

                break;

            case "AddPrior":

                var GvPri = (GridView)FvInsert_Mini.FindControl("GvPri");
                var txtorgold = (TextBox)FvInsert_Mini.FindControl("txtorgold");
                var txtposold = (TextBox)FvInsert_Mini.FindControl("txtposold");
                var txtstartdate_ex = (TextBox)FvInsert_Mini.FindControl("txtstartdate_ex");
                var txtresigndate_ex = (TextBox)FvInsert_Mini.FindControl("txtresigndate_ex");
                var txtpridescription = (TextBox)FvInsert_Mini.FindControl("txtpridescription");
                var txtprisalary = (TextBox)FvInsert_Mini.FindControl("txtprisalary");

                var dsOrg = (DataSet)ViewState["vsTemp_Prior"];

                var drOrg = dsOrg.Tables[0].NewRow();
                drOrg["Pri_Nameorg"] = txtorgold.Text;
                drOrg["Pri_pos"] = txtposold.Text;
                drOrg["Pri_startdate"] = txtstartdate_ex.Text;
                drOrg["Pri_resigndate"] = txtresigndate_ex.Text;
                drOrg["Pri_description"] = txtpridescription.Text;
                drOrg["Pri_salary"] = txtprisalary.Text;
                dsOrg.Tables[0].Rows.Add(drOrg);

                ViewState["vsTemp_Prior"] = dsOrg;

                GvPri.DataSource = dsOrg.Tables[0];
                GvPri.DataBind();

                txtorgold.Text = null;
                txtposold.Text = null;
                txtstartdate_ex.Text = null;
                txtresigndate_ex.Text = null;
                txtpridescription.Text = null;
                txtprisalary.Text = null;

                break;

            case "AddPrior_full":

                var GvPri_full = (GridView)FvInsertEmp.FindControl("GvPri_full");
                var txtorgold_full = (TextBox)FvInsertEmp.FindControl("txtorgold");
                var txtposold_full = (TextBox)FvInsertEmp.FindControl("txtposold");
                var txtstartdate_ex_full = (TextBox)FvInsertEmp.FindControl("txtstartdate_ex");
                var txtresigndate_ex_full = (TextBox)FvInsertEmp.FindControl("txtresigndate_ex");
                var txtpridescription_full = (TextBox)FvInsertEmp.FindControl("txtpridescription");
                var txtprisalary_full = (TextBox)FvInsertEmp.FindControl("txtprisalary");

                var dsOrg_full = (DataSet)ViewState["vsTemp_Prior_full"];

                var drOrg_full = dsOrg_full.Tables[0].NewRow();
                drOrg_full["Pri_Nameorg"] = txtorgold_full.Text;
                drOrg_full["Pri_pos"] = txtposold_full.Text;
                drOrg_full["Pri_startdate"] = txtstartdate_ex_full.Text;
                drOrg_full["Pri_resigndate"] = txtresigndate_ex_full.Text;
                drOrg_full["Pri_description"] = txtpridescription_full.Text;
                drOrg_full["Pri_salary"] = txtprisalary_full.Text;
                dsOrg_full.Tables[0].Rows.Add(drOrg_full);

                ViewState["vsTemp_Prior_full"] = dsOrg_full;

                GvPri_full.DataSource = dsOrg_full.Tables[0];
                GvPri_full.DataBind();

                txtorgold_full.Text = null;
                txtposold_full.Text = null;
                txtstartdate_ex_full.Text = null;
                txtresigndate_ex_full.Text = null;
                txtpridescription_full.Text = null;
                txtprisalary_full.Text = null;

                break;
            case "AddEducation":

                var GvEducation = (GridView)FvInsert_Mini.FindControl("GvEducation");
                var ddleducationback = (DropDownList)FvInsert_Mini.FindControl("ddleducationback");
                var txtschoolname = (TextBox)FvInsert_Mini.FindControl("txtschoolname");
                var txtstarteducation = (TextBox)FvInsert_Mini.FindControl("txtstarteducation");
                var txtendeducation = (TextBox)FvInsert_Mini.FindControl("txtendeducation");
                var txtstudy = (TextBox)FvInsert_Mini.FindControl("txtstudy");

                var dsEdu = (DataSet)ViewState["vsTemp_Education"];

                var drEdu = dsEdu.Tables[0].NewRow();
                drEdu["Edu_qualification_ID"] = ddleducationback.SelectedValue;
                drEdu["Edu_qualification"] = ddleducationback.SelectedItem.Text;
                drEdu["Edu_name"] = txtschoolname.Text;
                drEdu["Edu_branch"] = txtstudy.Text;
                drEdu["Edu_start"] = txtstarteducation.Text;
                drEdu["Edu_end"] = txtendeducation.Text;

                dsEdu.Tables[0].Rows.Add(drEdu);

                ViewState["vsTemp_Education"] = dsEdu;

                GvEducation.DataSource = dsEdu.Tables[0];
                GvEducation.DataBind();

                ddleducationback.SelectedValue = null;
                txtschoolname.Text = null;
                txtstarteducation.Text = null;
                txtendeducation.Text = null;
                txtstudy.Text = null;

                break;

            case "AddEducation_full":

                var GvEducation_full = (GridView)FvInsertEmp.FindControl("GvEducation_full");
                var ddleducationback_full = (DropDownList)FvInsertEmp.FindControl("ddleducationback");
                var txtschoolname_full = (TextBox)FvInsertEmp.FindControl("txtschoolname");
                var txtstarteducation_full = (TextBox)FvInsertEmp.FindControl("txtstarteducation");
                var txtendeducation_full = (TextBox)FvInsertEmp.FindControl("txtendeducation");
                var txtstudy_full = (TextBox)FvInsertEmp.FindControl("txtstudy");

                var dsEdu_full = (DataSet)ViewState["vsTemp_Education_full"];

                var drEdu_full = dsEdu_full.Tables[0].NewRow();
                drEdu_full["Edu_qualification_ID"] = ddleducationback_full.SelectedValue;
                drEdu_full["Edu_qualification"] = ddleducationback_full.SelectedItem.Text;
                drEdu_full["Edu_name"] = txtschoolname_full.Text;
                drEdu_full["Edu_branch"] = txtstudy_full.Text;
                drEdu_full["Edu_start"] = txtstarteducation_full.Text;
                drEdu_full["Edu_end"] = txtendeducation_full.Text;

                dsEdu_full.Tables[0].Rows.Add(drEdu_full);

                ViewState["vsTemp_Education_full"] = dsEdu_full;

                GvEducation_full.DataSource = dsEdu_full.Tables[0];
                GvEducation_full.DataBind();

                ddleducationback_full.SelectedValue = null;
                txtschoolname_full.Text = null;
                txtstarteducation_full.Text = null;
                txtendeducation_full.Text = null;
                txtstudy_full.Text = null;

                break;

            case "cktraining":

                var cktraining = (CheckBox)FvInsertEmp.FindControl("cktraining");
                var GvTrain = (GridView)FvInsertEmp.FindControl("GvTrain");
                var BoxTrain = (Panel)FvInsertEmp.FindControl("BoxTrain");


                if (cktraining.Checked)
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Train");

                    dsEquipment.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train"] = dsEquipment;
                    GvTrain.DataSource = dsEquipment.Tables[0];
                    GvTrain.DataBind();

                    BoxTrain.Visible = false;
                    GvTrain.DataSource = null;
                    GvTrain.DataBind();
                }
                break;

            case "AddChild_Edit":

                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var txtchildname_edit = (TextBox)FvEdit_Detail.FindControl("txtchildname_edit");
                var ddlchildnumber_edit = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber_edit");

                var dsEquiment_ = (DataSet)ViewState["vsTemp_Child_edit"];

                var drEquiment_ = dsEquiment_.Tables[0].NewRow();
                drEquiment_["Child_name"] = txtchildname_edit.Text;
                drEquiment_["Child_num"] = ddlchildnumber_edit.SelectedValue;

                dsEquiment_.Tables[0].Rows.Add(drEquiment_);

                ViewState["vsTemp_Child_edit"] = dsEquiment_;

                GvChildAdd_Edit.DataSource = dsEquiment_.Tables[0];
                GvChildAdd_Edit.DataBind();

                txtchildname_edit.Text = null;
                ddlchildnumber_edit.SelectedValue = null;

                break;
            case "AddReference":
                var GvReferenceAdd = (GridView)FvInsertEmp.FindControl("GvReference");
                var txtfullname_add = (TextBox)FvInsertEmp.FindControl("txtfullname_add");
                var txtposition_add = (TextBox)FvInsertEmp.FindControl("txtposition_add");
                var txtrelation_add = (TextBox)FvInsertEmp.FindControl("txtrelation_add");
                var txttel_add = (TextBox)FvInsertEmp.FindControl("txttel_add");

                var dsRefer = (DataSet)ViewState["vsTemp_Refer"];

                var drRefer = dsRefer.Tables[0].NewRow();
                drRefer["ref_fullname"] = txtfullname_add.Text;
                drRefer["ref_position"] = txtposition_add.Text;
                drRefer["ref_relation"] = txtrelation_add.Text;
                drRefer["ref_tel"] = txttel_add.Text;

                dsRefer.Tables[0].Rows.Add(drRefer);

                ViewState["vsTemp_Refer"] = dsRefer;
                GvReferenceAdd.DataSource = dsRefer.Tables[0];
                GvReferenceAdd.DataBind();

                txtfullname_add.Text = null;
                txtposition_add.Text = null;
                txtrelation_add.Text = null;
                txttel_add.Text = null;

                break;
            case "AddReference_Edit":
                var GvReferenceEdit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var txtfullname_edit = (TextBox)FvEdit_Detail.FindControl("txtfullname_add");
                var txtposition_edit = (TextBox)FvEdit_Detail.FindControl("txtposition_add");
                var txtrelation_edit = (TextBox)FvEdit_Detail.FindControl("txtrelation_add");
                var txttel_edit = (TextBox)FvEdit_Detail.FindControl("txttel_add");

                var dsRefer_edit = (DataSet)ViewState["vsTemp_Refer_edit"];

                var drRefer_edit = dsRefer_edit.Tables[0].NewRow();
                drRefer_edit["ref_fullname"] = txtfullname_edit.Text;
                drRefer_edit["ref_position"] = txtposition_edit.Text;
                drRefer_edit["ref_relation"] = txtrelation_edit.Text;
                drRefer_edit["ref_tel"] = txttel_edit.Text;

                dsRefer_edit.Tables[0].Rows.Add(drRefer_edit);

                ViewState["vsTemp_Refer_edit"] = dsRefer_edit;
                GvReferenceEdit.DataSource = dsRefer_edit.Tables[0];
                GvReferenceEdit.DataBind();

                txtfullname_edit.Text = null;
                txtposition_edit.Text = null;
                txtrelation_edit.Text = null;
                txttel_edit.Text = null;

                break;


            case "AddPrior_Edit":

                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var txtorgold_edit = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
                var txtposold_edit = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
                var txtstartdate_edit = (TextBox)FvEdit_Detail.FindControl("txtstartdate_edit");
                var txtresigndate_edit = (TextBox)FvEdit_Detail.FindControl("txtresigndate_edit");
                var txtpridescription_edit = (TextBox)FvEdit_Detail.FindControl("txtpridescription_edit");
                var txtprisalary_edit = (TextBox)FvEdit_Detail.FindControl("txtprisalary_edit");

                var dsOrg_edit = (DataSet)ViewState["vsTemp_Prior_edit"];

                var drOrg_edit = dsOrg_edit.Tables[0].NewRow();
                drOrg_edit["Pri_Nameorg"] = txtorgold_edit.Text;
                drOrg_edit["Pri_pos"] = txtposold_edit.Text;
                drOrg_edit["Pri_startdate"] = txtstartdate_edit.Text;
                drOrg_edit["Pri_resigndate"] = txtresigndate_edit.Text;
                drOrg_edit["Pri_description"] = txtpridescription_edit.Text;
                drOrg_edit["Pri_salary"] = txtprisalary_edit.Text;

                dsOrg_edit.Tables[0].Rows.Add(drOrg_edit);

                ViewState["vsTemp_Prior_edit"] = dsOrg_edit;

                GvPri_Edit.DataSource = dsOrg_edit.Tables[0];
                GvPri_Edit.DataBind();

                txtorgold_edit.Text = null;
                txtposold_edit.Text = null;
                txtstartdate_edit.Text = null;
                txtresigndate_edit.Text = null;
                txtpridescription_edit.Text = null;
                txtprisalary_edit.Text = null;

                break;

            case "AddEducation_Edit":

                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var ddleducationback_edit = (DropDownList)FvEdit_Detail.FindControl("ddleducationback_edit");
                var txtschoolname_edit = (TextBox)FvEdit_Detail.FindControl("txtschoolname_edit");
                var txtstarteducation_edit = (TextBox)FvEdit_Detail.FindControl("txtstarteducation_edit");
                var txtendeducation_edit = (TextBox)FvEdit_Detail.FindControl("txtendeducation_edit");
                var txtstudy_edit = (TextBox)FvEdit_Detail.FindControl("txtstudy_edit");

                var dsEdu_edit = (DataSet)ViewState["vsTemp_Education_edit"];

                var drEdu_edit = dsEdu_edit.Tables[0].NewRow();
                drEdu_edit["Edu_qualification_ID"] = ddleducationback_edit.SelectedValue;
                drEdu_edit["Edu_qualification"] = ddleducationback_edit.SelectedItem.Text;
                drEdu_edit["Edu_name"] = txtschoolname_edit.Text;
                drEdu_edit["Edu_branch"] = txtstudy_edit.Text;
                drEdu_edit["Edu_start"] = txtstarteducation_edit.Text;
                drEdu_edit["Edu_end"] = txtendeducation_edit.Text;

                dsEdu_edit.Tables[0].Rows.Add(drEdu_edit);

                ViewState["vsTemp_Education_edit"] = dsEdu_edit;

                GvEducation_Edit.DataSource = dsEdu_edit.Tables[0];
                GvEducation_Edit.DataBind();

                ddleducationback_edit.SelectedValue = null;
                txtschoolname_edit.Text = null;
                txtstarteducation_edit.Text = null;
                txtendeducation_edit.Text = null;
                txtstudy_edit.Text = null;

                break;

            case "Addtraining":

                var GvTrain_full = (GridView)FvInsertEmp.FindControl("GvTrain");
                var txttraincourses = (TextBox)FvInsertEmp.FindControl("txttraincourses");
                var txttraindate = (TextBox)FvInsertEmp.FindControl("txttraindate");
                var txttrainassessment = (TextBox)FvInsertEmp.FindControl("txttrainassessment");

                var dsTrain = (DataSet)ViewState["vsTemp_Train"];

                var drTrain = dsTrain.Tables[0].NewRow();
                drTrain["Train_courses"] = txttraincourses.Text;
                drTrain["Train_date"] = txttraindate.Text;
                drTrain["Train_assessment"] = txttrainassessment.Text;

                dsTrain.Tables[0].Rows.Add(drTrain);

                ViewState["vsTemp_Train"] = dsTrain;

                GvTrain_full.DataSource = dsTrain.Tables[0];
                GvTrain_full.DataBind();

                txttraincourses.Text = null;
                txttraindate.Text = null;
                txttrainassessment.Text = null;

                break;

            case "Addtraining_Edit":

                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var txttraincourses_edit = (TextBox)FvEdit_Detail.FindControl("txttraincourses_edit");
                var txttraindate_edit = (TextBox)FvEdit_Detail.FindControl("txttraindate_edit");
                var txttrainassessment_edit = (TextBox)FvEdit_Detail.FindControl("txttrainassessment_edit");

                var dsTrain_edit = (DataSet)ViewState["vsTemp_Train_edit"];

                var drTrain_edit = dsTrain_edit.Tables[0].NewRow();
                drTrain_edit["Train_courses"] = txttraincourses_edit.Text;
                drTrain_edit["Train_date"] = txttraindate_edit.Text;
                drTrain_edit["Train_assessment"] = txttrainassessment_edit.Text;

                dsTrain_edit.Tables[0].Rows.Add(drTrain_edit);

                ViewState["vsTemp_Train_edit"] = dsTrain_edit;

                GvTrain_Edit.DataSource = dsTrain_edit.Tables[0];
                GvTrain_Edit.DataBind();

                txttraincourses_edit.Text = null;
                txttraindate_edit.Text = null;
                txttrainassessment_edit.Text = null;

                break;

            case "DeleteGvRefer_View":
                string cmdArg_ref = e.CommandArgument.ToString();
                int ReIDX = int.Parse(cmdArg_ref);

                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

                _dataEmployee.BoxReferent_list = new Referent_List[1];
                Referent_List _dataEmployee_ref = new Referent_List();

                _dataEmployee_ref.ReIDX = ReIDX;

                _dataEmployee.BoxReferent_list[0] = _dataEmployee_ref;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisReferList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvReference_View.EditIndex = -1;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;

            case "DeleteGvChildAdd_View":
                string cmdArg2 = e.CommandArgument.ToString();
                int CHIDX = int.Parse(cmdArg2);

                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

                _dataEmployee.BoxChild_list = new Child_List[1];
                Child_List _dataEmployee_1 = new Child_List();

                _dataEmployee_1.CHIDX = CHIDX;

                _dataEmployee.BoxChild_list[0] = _dataEmployee_1;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisChildList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));

                GvChildAdd_View.EditIndex = -1;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;

            case "DeleteGvPri_View":
                string cmdArg3 = e.CommandArgument.ToString();
                int ExperIDX = int.Parse(cmdArg3);

                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

                _dataEmployee.BoxPrior_list = new Prior_List[1];
                Prior_List _dataEmployee_2 = new Prior_List();

                _dataEmployee_2.ExperIDX = ExperIDX;

                _dataEmployee.BoxPrior_list[0] = _dataEmployee_2;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisPriorList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvPri_View.EditIndex = -1;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;

            case "DeleteGvEducation_View":
                string cmdArg4 = e.CommandArgument.ToString();
                int EDUIDX = int.Parse(cmdArg4);

                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

                _dataEmployee.BoxEducation_list = new Education_List[1];
                Education_List _dataEmployee_3 = new Education_List();

                _dataEmployee_3.EDUIDX = EDUIDX;

                _dataEmployee.BoxEducation_list[0] = _dataEmployee_3;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisEducationList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvEducation_View.EditIndex = -1;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;

            case "DeleteGvTrain_View":
                string cmdArg5 = e.CommandArgument.ToString();
                int TNIDX = int.Parse(cmdArg5);

                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

                _dataEmployee.BoxTrain_list = new Train_List[1];
                Train_List _dataEmployee_4 = new Train_List();

                _dataEmployee_4.TNIDX = TNIDX;

                _dataEmployee.BoxTrain_list[0] = _dataEmployee_4;
                _dataEmployee = callServiceEmployee(_urlSetDeleteRegisTrainList, _dataEmployee);

                Select_FvEdit_Detail(int.Parse(ViewState["fv_emp_idx"].ToString()));
                //Menu_Color_Edit(1);
                //txtfocus.Focus();
                GvTrain_View.EditIndex = -1;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;

            case "savefile_picture":
                var txtidcard_upload = (TextBox)FvInsert_Mini.FindControl("txtidcard");
                var ImagProfile_default = (Panel)FvInsert_Mini.FindControl("ImagProfile_default");
                var txtfocus_picture = (HyperLink)FvInsert_Mini.FindControl("txtfocus_picture");
                var img_profile = (Image)FvInsert_Mini.FindControl("img_profile");

                HttpFileCollection upload_picture_profile1 = Request.Files;
                for (int j = 0; j < upload_picture_profile1.Count; j++)
                {
                    HttpPostedFile hpfLoadfile = upload_picture_profile1[j];
                    if (hpfLoadfile.ContentLength > 1)
                    {
                        string getPath_upload = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                        string RECode_uploadfile = txtidcard_upload.Text;
                        string fileName_upload = RECode_uploadfile;
                        string filePath_upload = Server.MapPath(getPath_upload + RECode_uploadfile);
                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(hpfLoadfile.FileName);

                        hpfLoadfile.SaveAs(Server.MapPath(getPath_upload + RECode_uploadfile) + "\\" + fileName_upload + extension);
                        ImagProfile_default.Visible = false;
                        txtfocus_picture.Focus();
                        showimage_upload(txtidcard_upload.Text, img_profile);
                    }
                }

                break;

            case "savefile_document":
                var txtidcard_upload_1 = (TextBox)FvInsert_Mini.FindControl("txtidcard");
                var txtfocus_1 = (HyperLink)FvInsert_Mini.FindControl("txtfocus_");
                GridView gvFileEmp = (GridView)FvInsert_Mini.FindControl("gvFileEmp");

                HttpFileCollection hfcit2 = Request.Files;
                //UpdatePanel1.Update();

                hfcit2 = Request.Files;
                if (hfcit2.Count > 0)
                {
                    for (int ii = 0; ii < hfcit2.Count; ii++)
                    {
                        HttpPostedFile hpfLo = hfcit2[ii];
                        if (hpfLo.ContentLength > 1)
                        {
                            string getPath = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                            string RECode1 = txtidcard_upload_1.Text;
                            string fileName1 = RECode1 + ii;
                            string filePath1 = Server.MapPath(getPath + RECode1);
                            if (!Directory.Exists(filePath1))
                            {
                                Directory.CreateDirectory(filePath1);
                            }
                            string extension = Path.GetExtension(hpfLo.FileName);

                            hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                        }
                    }

                }
                else
                {

                }

                try
                {
                    string getPathLotus = ConfigurationSettings.AppSettings["path_flie_emp_register"];

                    string filePathLotus = Server.MapPath(getPathLotus + txtidcard_upload_1.Text);
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, txtidcard_upload_1.Text, gvFileEmp);
                    txtfocus_1.Focus();
                }
                catch
                {

                }

                break;

            case "savefile_picture_full":
                var txtidcard_upload_full = (TextBox)FvInsertEmp.FindControl("txtidcard");
                var ImagProfile_default_full = (Panel)FvInsertEmp.FindControl("ImagProfile_default_full");
                var txtfocus_picture_full = (HyperLink)FvInsertEmp.FindControl("txtfocus_picture_full");
                var img_profile_full = (Image)FvInsertEmp.FindControl("img_profile_full");

                HttpFileCollection upload_picture_profile1_full = Request.Files;
                for (int j = 0; j < upload_picture_profile1_full.Count; j++)
                {
                    HttpPostedFile hpfLoadfile_full = upload_picture_profile1_full[j];
                    if (hpfLoadfile_full.ContentLength > 1)
                    {
                        string getPath_upload = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                        string RECode_uploadfile = txtidcard_upload_full.Text;
                        string fileName_upload = RECode_uploadfile;
                        string filePath_upload = Server.MapPath(getPath_upload + RECode_uploadfile);
                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(hpfLoadfile_full.FileName);

                        hpfLoadfile_full.SaveAs(Server.MapPath(getPath_upload + RECode_uploadfile) + "\\" + fileName_upload + extension);
                        ImagProfile_default_full.Visible = false;
                        txtfocus_picture_full.Focus();
                        showimage_upload(txtidcard_upload_full.Text, img_profile_full);
                    }
                }

                break;

            case "savefile_document_full":
                var txtidcard_upload_detail_full = (TextBox)FvInsertEmp.FindControl("txtidcard");
                var txtfocus_detail_full = (HyperLink)FvInsertEmp.FindControl("txtfocus_detail_full");
                GridView gvFileEmp_full = (GridView)FvInsertEmp.FindControl("gvFileEmp_full");

                HttpFileCollection hfcit3 = Request.Files;
                //UpdatePanel1.Update();

                hfcit3 = Request.Files;
                if (hfcit3.Count > 0)
                {
                    for (int ii = 0; ii < hfcit3.Count; ii++)
                    {
                        HttpPostedFile hpfLo2 = hfcit3[ii];
                        if (hpfLo2.ContentLength > 1)
                        {
                            string getPath = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                            string RECode1 = txtidcard_upload_detail_full.Text;
                            string fileName1 = RECode1 + ii;
                            string filePath1 = Server.MapPath(getPath + RECode1);
                            if (!Directory.Exists(filePath1))
                            {
                                Directory.CreateDirectory(filePath1);
                            }
                            string extension = Path.GetExtension(hpfLo2.FileName);

                            hpfLo2.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                        }
                    }

                }
                else
                {

                }

                try
                {
                    string getPathLotus = ConfigurationSettings.AppSettings["path_flie_emp_register"];

                    string filePathLotus = Server.MapPath(getPathLotus + txtidcard_upload_detail_full.Text);
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, txtidcard_upload_detail_full.Text, gvFileEmp_full);
                    txtfocus_detail_full.Focus();
                }
                catch
                {

                }

                break;

            case "savefile_picture_edit":
                var txtidcard_upload_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                var ImagProfile_default_edit = (Panel)FvEdit_Detail.FindControl("ImagProfile_default_edit");
                var txtfocus_picture_edit = (HyperLink)FvEdit_Detail.FindControl("txtfocus_picture_edit");
                var img_profile_edit = (Image)FvEdit_Detail.FindControl("img_profile_edit");

                HttpFileCollection upload_picture_profile1_edit = Request.Files;
                for (int j = 0; j < upload_picture_profile1_edit.Count; j++)
                {
                    HttpPostedFile hpfLoadfile_edit = upload_picture_profile1_edit[j];
                    if (hpfLoadfile_edit.ContentLength > 1)
                    {
                        string getPath_upload = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                        string RECode_uploadfile = txtidcard_upload_edit.Text;
                        string fileName_upload = RECode_uploadfile;
                        string filePath_upload = Server.MapPath(getPath_upload + RECode_uploadfile);
                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(hpfLoadfile_edit.FileName);

                        hpfLoadfile_edit.SaveAs(Server.MapPath(getPath_upload + RECode_uploadfile) + "\\" + fileName_upload + extension);
                        ImagProfile_default_edit.Visible = false;
                        txtfocus_picture_edit.Focus();
                        showimage_upload(txtidcard_upload_edit.Text, img_profile_edit);
                    }
                }

                break;

            case "savefile_document_edit":
                var txtidcard_detail_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                var txtfocus_detail_edit = (HyperLink)FvEdit_Detail.FindControl("txtfocus_detail_edit");
                GridView gvFileEmp_edit = (GridView)FvEdit_Detail.FindControl("gvFileEmp_edit");

                HttpFileCollection hfcit4 = Request.Files;

                gsd.Text = ViewState["RowCountGvFile"].ToString();
                hfcit4 = Request.Files;
                if (hfcit4.Count > 0)
                {
                    for (int ii = 0; ii < hfcit4.Count; ii++)
                    {
                        HttpPostedFile hpfLo3 = hfcit4[ii];
                        if (hpfLo3.ContentLength > 1)
                        {
                            string getPath = ConfigurationSettings.AppSettings["path_flie_emp_register"];
                            string RECode1 = txtidcard_detail_edit.Text;
                            string fileName1 = RECode1 + int.Parse(ViewState["RowCountGvFile"].ToString());
                            string filePath1 = Server.MapPath(getPath + RECode1);
                            if (!Directory.Exists(filePath1))
                            {
                                Directory.CreateDirectory(filePath1);
                            }
                            string extension = Path.GetExtension(hpfLo3.FileName);

                            hpfLo3.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                        }
                    }
                }
                else
                {

                }

                try
                {
                    string getPathLotus = ConfigurationSettings.AppSettings["path_flie_emp_register"];

                    string filePathLotus = Server.MapPath(getPathLotus + txtidcard_detail_edit.Text);
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, txtidcard_detail_edit.Text, gvFileEmp_edit);
                    txtfocus_detail_edit.Focus();
                }
                catch
                {

                }

                break;
            case "btnView_ma_position":
                int PIDX_Search = int.Parse(e.CommandArgument.ToString());
                SelectMaPositionList_ViewDetail(PIDX_Search);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;
            case "btnView_Mtt":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_mtt();", true);
                break;
            case "btnView_Npw":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_npw();", true);
                break;
            case "btnView_Rjn":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_rjn();", true);
                break;
            case "btnView_plant":
                string cmd = e.CommandArgument.ToString();
                int Plant = int.Parse(cmd);

                switch (Plant)
                {
                    case 1:
                        ibmenu_mtt_white.Visible = true;
                        ibmenu_mtt_black.Visible = false;
                        ibmenu_npw_white.Visible = false;
                        ibmenu_npw_black.Visible = true;
                        ibmenu_rjn_white.Visible = false;
                        ibmenu_rjn_black.Visible = true;

                        panel_mtt.Visible = true;
                        panel_npw.Visible = false;
                        panel_rjn.Visible = false;
                        break;
                    case 2:
                        ibmenu_mtt_white.Visible = false;
                        ibmenu_mtt_black.Visible = true;
                        ibmenu_npw_white.Visible = true;
                        ibmenu_npw_black.Visible = false;
                        ibmenu_rjn_white.Visible = false;
                        ibmenu_rjn_black.Visible = true;

                        panel_mtt.Visible = false;
                        panel_npw.Visible = true;
                        panel_rjn.Visible = false;
                        break;
                    case 3:
                        ibmenu_mtt_white.Visible = false;
                        ibmenu_mtt_black.Visible = true;
                        ibmenu_npw_white.Visible = false;
                        ibmenu_npw_black.Visible = true;
                        ibmenu_rjn_white.Visible = true;
                        ibmenu_rjn_black.Visible = false;

                        panel_mtt.Visible = false;
                        panel_npw.Visible = false;
                        panel_rjn.Visible = true;
                        break;
                }
                break;
            case "btntest":
                Select_check_ipaddress();

                //ViewState["check_ip"] = "0";
                if (ViewState["check_ip"].ToString() == "0") //เน็ตในบริษัท
                {
                    //Go To Test
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "คุณสามารถทำข้อสอบได้" + "')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_test_edit();", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "คุณไม่มีสิทธิ์ทำข้อสอบ กรุณาติดต่อ HR" + "')", true);
                }
                break;
            case "btntranfer_test_mini":
                //TextBox txtidcard_mini = (TextBox)FvInsert_Mini.FindControl("txtidcard");
                //Select_tranfer(txtidcard_mini.Text);
                break;
            case "btntranfer_test_full":
                //Label fsaw = (Label)ViewAdd_full.FindControl("fsaw");
                //fsaw.Text = ViewState["ID_identity"].ToString();
                Select_tranfer(ViewState["ID_identity"].ToString());
                break;
            case "btntranfer_test":
                //TextBox txtidcard_search_ = (TextBox)Fv_Search_Emp_Index.FindControl("txtidcard_s");
                //Select_tranfer(txtidcard_search_.Text);
                Select_tranfer(ViewState["ID_identity"].ToString());
                break;

        }
    }
    #endregion
}