﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_mange_position : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();
    data_rcm_question _dtrcm = new data_rcm_question();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetinsert_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetinsert_group_positionList"];
    static string _urlSetinsert_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetinsert_ma_positionList"];
    static string _urlGetselect_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_group_positionList"];
    static string _urlGetselect_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetselect_ma_positionList"];
    static string _urlSetupdate_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_group_positionList"];
    static string _urlSetupdate_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdate_ma_positionList"];
    static string _urlSetdelete_group_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetdelete_group_positionList"];
    static string _urlSetdelete_ma_positionList = _serviceUrl + ConfigurationManager.AppSettings["urlSetdelete_ma_positionList"];
    static string _urlSelectm0typequest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0typequest"];
    static string _urlSelectm0topicquest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0topicquest"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlget_select_ma_position_topic = _serviceUrl + ConfigurationManager.AppSettings["urlget_select_ma_position_topic"];
    static string _urlget_select_ma_position_person = _serviceUrl + ConfigurationManager.AppSettings["urlget_select_ma_position_person"];
    static string _urlSelect_TopicManagePosition = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TopicManagePosition"];

    
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            MvMaster.SetActiveView(ViewPosition);
            SelectMaPositionList();
            Menu_Color(1);
        }
    }

    #region INSERT&SELECT&UPDATE

    protected void Insert_Ma_Position()
    {
        _dataEmployee = new data_employee();
        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new Manage_Position_List_Topic[ds_udoc1_insert.Tables[0].Rows.Count];

        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.P_Name = txtP_Name_add.Text;
        dtemployee.PosGroupIDX = int.Parse(ddlgroupposition_add.SelectedValue);
        dtemployee.P_Description = txtP_Description_add.Text;
        dtemployee.P_Location = txtP_Location_add.Text;
        dtemployee.P_Property = txtP_Property_add.Text;
        dtemployee.P_Unit = int.Parse(txtP_Unit_add.Text);
        dtemployee.P_Salary = txtP_Salary_add.Text;
        dtemployee.P_Contract = txtP_Contract_add.Text;
        dtemployee.P_EmpCreate = int.Parse(ViewState["EmpIDX"].ToString());
        dtemployee.P_Piority = int.Parse(ddlPiority_add.SelectedValue);
        dtemployee.P_Status = int.Parse(ddStatus_add.SelectedValue);
        dtemployee.P_Choose = int.Parse(ddlchoose.SelectedValue);

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new Manage_Position_List_Topic();

             _document1[i].m0_toidx = int.Parse(dtrow["m0_toidx"].ToString());
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

            i++;

            _dataEmployee.Boxm1_personlist_topic = _document1;

        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));


        _dataEmployee = callService(_urlSetinsert_ma_positionList, _dataEmployee);
    }

    protected void Insert_Group_Position()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PosGroupNameTH = txtPosGroupNameTH_add.Text;
        dtemployee.PosGroupNameEN = txtPosGroupNameEN_add.Text;
        dtemployee.PosGroupStatus = int.Parse(ddlstatus_position.SelectedValue);

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlSetinsert_group_positionList, _dataEmployee);
    }

    protected void SelectMaPositionList()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

        _dataEmployee = callService(_urlGetselect_ma_positionList, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaPosition, _dataEmployee.BoxManage_Position_list);
    }
    /*
        protected void SelectMaPositionList_Topic(int PIDX,GridView Gvname)
        {
           _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
            Manage_Position_List select_topic = new Manage_Position_List();

            select_topic.PIDX = PIDX;
            _dataEmployee.BoxManage_Position_list[0] = select_topic;

            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));


            _dataEmployee = callService(_urlget_select_ma_position_topic, _dataEmployee);
            //text.Text = _dataEmployee.ToString();

            setGridData(Gvname, _dataEmployee.Boxm1_personlist_topic);
        }
        */

    protected void SelectMaPositionList_Topic(int PIDX, GridView Gvname)
    {
        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm1_personlist = new m1_persondetail[1];
        m1_persondetail select = new m1_persondetail();

        select.PIDX = PIDX;
        _dtrcm.Boxm1_personlist[0] = select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelect_TopicManagePosition, _dtrcm);

        setGridData(Gvname, _dtrcm.Boxm1_personlist);


    }

    protected void SelectMaPositionList_Person(int m0_toidx, GridView Gvname)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List select_topic = new Manage_Position_List();

        select_topic.m0_toidx = m0_toidx;
        _dataEmployee.BoxManage_Position_list[0] = select_topic;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

        _dataEmployee = callService(_urlget_select_ma_position_person, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(Gvname, _dataEmployee.Boxm1_personlist_topic);
    }

    protected void SelectGroupPositionList()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;

        _dataEmployee = callService(_urlGetselect_group_positionList, _dataEmployee);
        setGridData(GvGroupPosition, _dataEmployee.BoxManage_Position_list);
    }

    protected void Update_Ma_Position_List()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PIDX = int.Parse(ViewState["txtPIDX_update"].ToString());
        dtemployee.P_Name = ViewState["txtP_Name_update"].ToString();
        dtemployee.PosGroupIDX = int.Parse(ViewState["ddlgroupposition_update"].ToString());
        dtemployee.P_Description = ViewState["txtP_Description_update"].ToString();
        dtemployee.P_Location = ViewState["txtP_Location_update"].ToString();
        dtemployee.P_Property = ViewState["txtP_Property_update"].ToString();
        dtemployee.P_Unit = int.Parse(ViewState["txtP_Unit_update"].ToString());
        dtemployee.P_Salary = ViewState["txtP_Salary_update"].ToString();
        dtemployee.P_Contract = ViewState["txtP_Contract_update"].ToString();
        dtemployee.P_EmpCreate = int.Parse(ViewState["EmpIDX"].ToString());
        dtemployee.P_Piority = int.Parse(ViewState["ddlPiority_update"].ToString());
        dtemployee.P_Status = int.Parse(ViewState["ddStatus_ma_update"].ToString());

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;
        _dataEmployee = callService(_urlSetupdate_ma_positionList, _dataEmployee);
    }

    protected void Update_Group_Position_List()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PosGroupIDX = int.Parse(ViewState["PosGroupIDX_update"].ToString());
        dtemployee.PosGroupNameTH = ViewState["txtPosGroupNameTH_update"].ToString();
        dtemployee.PosGroupNameEN = ViewState["txtPosGroupNameEN_update"].ToString();
        dtemployee.PosGroupStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;
        _dataEmployee = callService(_urlSetupdate_group_positionList, _dataEmployee);
    }

    protected void Delete_Ma_Position_List()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PIDX = int.Parse(ViewState["PIDX_delete"].ToString());

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;
        _dataEmployee = callService(_urlSetdelete_ma_positionList, _dataEmployee);
    }

    protected void Delete_Group_Position_List()
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List dtemployee = new Manage_Position_List();

        dtemployee.PosGroupIDX = int.Parse(ViewState["PosGroupIDX_delete"].ToString());

        _dataEmployee.BoxManage_Position_list[0] = dtemployee;
        _dataEmployee = callService(_urlSetdelete_group_positionList, _dataEmployee);
    }

    protected void select_Group_Position(DropDownList ddlName)
    {
        _dataEmployee.BoxManage_Position_list = new Manage_Position_List[1];
        Manage_Position_List _PosList = new Manage_Position_List();

        _dataEmployee.BoxManage_Position_list[0] = _PosList;

        _dataEmployee = callService(_urlGetselect_group_positionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.BoxManage_Position_list, "PosGroupNameTH", "PosGroupIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกกลุ่มตำแหน่ง....", "0"));
    }

    protected void gettypequestion(DropDownList ddlName)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typequest = new m0_typequest[1];
        m0_typequest _typequest = new m0_typequest();

        _dtrcm.Boxm0_typequest[0] = _typequest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0typequest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_typequest, "type_quest", "m0_tqidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำถาม...", "0"));
    }

    protected void gettopicquestion(DropDownList ddlName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.rposidx = rposidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0topicquest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_topicquest, "topic_name", "m0_toidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชุดคำถาม...", "0"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getPositionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _positionList = new position_details();
        _positionList.org_idx = _org_idx;
        _positionList.rdept_idx = _rdept_idx;
        _positionList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _positionList;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง....", "0"));
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected data_rcm_question callServicePostRCM(string _cmdUrl, data_rcm_question _dtrcm)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtrcm);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtrcm = (data_rcm_question)_funcTool.convertJsonToObject(typeof(data_rcm_question), _localJson);


        return _dtrcm;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaPosition":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaPosition.EditIndex != e.Row.RowIndex)
                    {
                        Label lblPIDX = (Label)e.Row.FindControl("lblPIDX");
                        GridView GvTopic = (GridView)e.Row.Cells[6].FindControl("GvTopic");

                        SelectMaPositionList_Topic(int.Parse(lblPIDX.Text), GvTopic);
                    }
                    if (GvMaPosition.EditIndex == e.Row.RowIndex)
                    {
                        
                        var lblPosGroupIDX_update = (Label)e.Row.FindControl("lblPosGroupIDX_update");
                        var ddlgroupposition_update = (DropDownList)e.Row.FindControl("ddlgroupposition_update");
                        select_Group_Position(ddlgroupposition_update);
                        ddlgroupposition_update.SelectedValue = lblPosGroupIDX_update.Text;

                      
                        /*var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;*/

                    }
                  

                }

                break;

            case "GvTopic":
               
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0_toidx = (Label)e.Row.FindControl("lblm0_toidx");
                    GridView GvPerson = (GridView)e.Row.Cells[1].FindControl("GvPerson");

                    SelectMaPositionList_Person(int.Parse(lblm0_toidx.Text), GvPerson);
                }
                    break;

            case "GvGroupPosition":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvGroupPosition.EditIndex == e.Row.RowIndex)
                    {

                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvGroupPosition":
                GvGroupPosition.PageIndex = e.NewPageIndex;
                GvGroupPosition.DataBind();
                SelectGroupPositionList();
                break;

            case "GvMaPosition":
                GvMaPosition.PageIndex = e.NewPageIndex;
                GvMaPosition.DataBind();
                SelectMaPositionList();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvGroupPosition":
                GvGroupPosition.EditIndex = e.NewEditIndex;
                SelectGroupPositionList();
                break;
            case "GvMaPosition":
                GvMaPosition.EditIndex = e.NewEditIndex;
                SelectMaPositionList();
                break;
        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvGroupPosition":
                GvGroupPosition.EditIndex = -1;
                SelectGroupPositionList();
                break;
            case "GvMaPosition":
                GvMaPosition.EditIndex = -1;
                SelectMaPositionList();
                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvGroupPosition":

                int PosGroupIDX = Convert.ToInt32(GvGroupPosition.DataKeys[e.RowIndex].Values[0].ToString());
                var txtPosGroupNameTH_update = (TextBox)GvGroupPosition.Rows[e.RowIndex].FindControl("txtPosGroupNameTH_update");
                var txtPosGroupNameEN_update = (TextBox)GvGroupPosition.Rows[e.RowIndex].FindControl("txtPosGroupNameEN_update");
                var ddStatus_update = (DropDownList)GvGroupPosition.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvGroupPosition.EditIndex = -1;

                ViewState["PosGroupIDX_update"] = PosGroupIDX;
                ViewState["txtPosGroupNameTH_update"] = txtPosGroupNameTH_update.Text;
                ViewState["txtPosGroupNameEN_update"] = txtPosGroupNameEN_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Group_Position_List();
                SelectGroupPositionList();

                break;

            case "GvMaPosition":

                int txtPIDX_update = Convert.ToInt32(GvMaPosition.DataKeys[e.RowIndex].Values[0].ToString());
                var txtP_Name_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Name_update");
                var ddlgroupposition_update = (DropDownList)GvMaPosition.Rows[e.RowIndex].FindControl("ddlgroupposition_update");
                var txtP_Description_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Description_update");
                var txtP_Location_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Location_update");
                var txtP_Property_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Property_update");
                var txtP_Unit_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Unit_update");
                var txtP_Salary_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Salary_update");
                var txtP_Contract_update = (TextBox)GvMaPosition.Rows[e.RowIndex].FindControl("txtP_Contract_update");
                var ddlPiority_update = (DropDownList)GvMaPosition.Rows[e.RowIndex].FindControl("ddlPiority_update");
                var ddStatus_ma_update = (DropDownList)GvMaPosition.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaPosition.EditIndex = -1;

                ViewState["txtPIDX_update"] = txtPIDX_update;
                ViewState["txtP_Name_update"] = txtP_Name_update.Text;
                ViewState["ddlgroupposition_update"] = ddlgroupposition_update.SelectedValue;
                ViewState["txtP_Description_update"] = txtP_Description_update.Text;
                ViewState["txtP_Location_update"] = txtP_Location_update.Text;
                ViewState["txtP_Property_update"] = txtP_Property_update.Text;
                ViewState["txtP_Unit_update"] = txtP_Unit_update.Text;
                ViewState["txtP_Salary_update"] = txtP_Salary_update.Text;
                ViewState["txtP_Contract_update"] = txtP_Contract_update.Text;
                ViewState["ddlPiority_update"] = ddlPiority_update.SelectedValue;
                ViewState["ddStatus_ma_update"] = ddStatus_ma_update.SelectedValue;

                Update_Ma_Position_List();
                SelectMaPositionList();

                break;
        }
    }

    #endregion

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Add_Mini             
                lbadd_position.BackColor = System.Drawing.Color.LightGray;
                lbadd_groupposition.BackColor = System.Drawing.Color.Transparent;
                break;
            case 2: //Add_Full
                lbadd_position.BackColor = System.Drawing.Color.Transparent;
                lbadd_groupposition.BackColor = System.Drawing.Color.LightGray;
                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddName = (DropDownList)sender;
            DropDownList ddlchooseqs = (DropDownList)Pn_Position.FindControl("ddlchooseqs");

            switch (ddName.ID)
            {
                case "ddlchoose":
                    if(ddlchoose.SelectedValue == "1")
                    {
                        div_typequest.Visible = true;
                        div_question.Visible = true;
                        btnsave_cancel.Visible = false;
                    }
                    else
                    {
                        div_typequest.Visible = false;
                        div_question.Visible = false;
                        btnsave_cancel.Visible = true;
                    }
                    break;

                case "ddltypequest":


                    if (ddltypequest.SelectedValue == "1")
                    {
                        divchoosetype.Visible = false;

                    }
                    else if (ddltypequest.SelectedValue == "2")
                    {
                        divchoosetype.Visible = true;
                        getOrganizationList(ddlOrg);

                    }

                    gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), 0, 0, 0, 0);

                    break;

                case "ddlOrg":
                    getDepartmentList(ddlDep, int.Parse(ddlOrg.SelectedValue));
                    ddltopic.SelectedValue = "0";

                    break;

                case "ddlDep":
                    getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue));
                    if (ddlDep.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), 0, 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), 0, 0, 0);

                    }

                    break;

                case "ddlSec":
                    getPositionList(ddlPos, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue));
                    if (ddlSec.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue), 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                               , 0);
                    }
                    break;
                case "ddlPos":
                    if (ddlPos.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , int.Parse(ddlPos.SelectedValue));

                        }
                    }
                    else
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , 0);

                        }
                    }

                    break;
            }
        }

    }


    #endregion


    #region SetDefault Viewstate
    protected void SetViewState_Topic()
    {

        DataSet dsPersonList = new DataSet();
        dsPersonList.Tables.Add("dsAddListTable");

        dsPersonList.Tables["dsAddListTable"].Columns.Add("P_Name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("typequest", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("m0_tqidx", typeof(int));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("Org_Name", typeof(String));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("OrgIDX", typeof(int));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("Dep_Name", typeof(String));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("DepIDX", typeof(int));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("Sec_Name", typeof(String));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("SecIDX", typeof(int));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("Position_Name", typeof(String));
        //dsPersonList.Tables["dsAddListTable"].Columns.Add("PosIDX", typeof(int));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("Topic_name", typeof(String));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("m0_toidx", typeof(int));

        ViewState["vsBuyequipment"] = dsPersonList;

    }

    protected void setAddList_Topic(TextBox position_addon, DropDownList typequest, DropDownList orgidx, DropDownList deptidx, DropDownList secidx, DropDownList posidx, DropDownList m0toidx, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {

                if (dr["P_Name"].ToString() == position_addon.Text &&
                   int.Parse(dr["m0_tqidx"].ToString()) == int.Parse(typequest.SelectedValue) &&
                   int.Parse(dr["m0_toidx"].ToString()) == int.Parse(m0toidx.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                setGridData(gvName, (DataSet)ViewState["vsBuyequipment"]);

            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();


            drContacts["P_Name"] = position_addon.Text;
            drContacts["typequest"] = typequest.SelectedItem.Text;
            drContacts["m0_tqidx"] = typequest.SelectedValue;
            drContacts["Topic_name"] = m0toidx.SelectedItem.Text;
            drContacts["m0_toidx"] = m0toidx.SelectedValue;

            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetTopicFormList(GridView GvName)
    {
        ViewState["vsBuyequipment"] = null;
        GvName.DataSource = ViewState["vsBuyequipment"];
        GvName.DataBind();
        SetViewState_Topic();
    }

    #endregion

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {

                case "btndeleteQuiz":
                    GridView GvTopicName = (GridView)Pn_Position.FindControl("GvTopicName");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    Control btnsave_cancel = (Control)Pn_Position.FindControl("btnsave_cancel");

                    int rowselect = rowSelect.RowIndex;


                    DataSet dsContacts_person = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts_person.Tables["dsAddListTable"].Rows[rowselect].Delete();
                    dsContacts_person.AcceptChanges();
                    setGridData(GvTopicName, dsContacts_person.Tables["dsAddListTable"]);

                    if (dsContacts_person.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvTopicName.Visible = false;
                        btnsave_cancel.Visible = false;
                    }
                    break;

            }
        }
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnAddGroupPosition":
                btnAddGroupPosition.Visible = false;
                Pn_GroupPosition.Visible = true;
                break;

            case "btnCancel_group_position":
                btnAddGroupPosition.Visible = true;
                Pn_GroupPosition.Visible = false;
                break;

            case "btnAdd_group_position":
                Insert_Group_Position();
                SelectGroupPositionList();
                Pn_GroupPosition.Visible = false;
                txtPosGroupNameTH_add.Text = null;
                txtPosGroupNameEN_add.Text = null;
                ddlstatus_position.SelectedValue = "0";
                break;

            case "btnDelete_group_position":
                int PosGroupIDX = int.Parse(cmdArg);
                ViewState["PosGroupIDX_delete"] = PosGroupIDX;
                Delete_Group_Position_List();
                SelectGroupPositionList();
                break;

            case "btnadd_position":
                Menu_Color(1);
                MvMaster.SetActiveView(ViewPosition);
                SelectMaPositionList();
                break;

            case "btnadd_groupposition":
                Menu_Color(2);
                MvMaster.SetActiveView(ViewGroupPosition);
                SelectGroupPositionList();
                break;

            /*-------------------------------------------------------------*/

            case "btnAddMaPosition":
                btnAddMaPosition.Visible = false;
                Pn_Position.Visible = true;
                select_Group_Position(ddlgroupposition_add);
                gettypequestion(ddltypequest);
                txtP_Name_add.Text = String.Empty;
                ddlgroupposition_add.SelectedValue = "0";
                txtP_Description_add.Text = String.Empty;
                txtP_Location_add.Text = String.Empty;
                txtP_Property_add.Text = String.Empty;
                txtP_Unit_add.Text = String.Empty;
                txtP_Salary_add.Text = String.Empty;
                txtP_Contract_add.Text = String.Empty;
                ddlPiority_add.SelectedValue = "0";
                ddStatus_add.SelectedValue = "1";
                ddltypequest.SelectedValue = "0";
                ddltopic.SelectedValue = "0";
                CleardataSetTopicFormList(GvTopicName);
                GvTopicName.Visible = false;
                divchoosetype.Visible = false;

                ddlOrg.SelectedValue = "0";
                ddlDep.SelectedValue = "0";
                ddlSec.SelectedValue = "0";
                ddlPos.SelectedValue = "0";
                div_question.Visible = false;
                div_typequest.Visible = false;
                ddlchoose.SelectedValue = "0";
                break;

            case "btnCancel_ma_position":
                btnAddMaPosition.Visible = true;
                Pn_Position.Visible = false;
                break;

            case "btnAdd_ma_position":
                Insert_Ma_Position();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnDelete_ma_position":
                int PIDX = int.Parse(cmdArg);
                ViewState["PIDX_delete"] = PIDX;
                Delete_Ma_Position_List();
                SelectMaPositionList();
                break;

            case "CmdAdddataset":
                btnsave_cancel.Visible = true;
                setAddList_Topic(txtP_Name_add, ddltypequest, ddlOrg, ddlDep, ddlSec, ddlPos, ddltopic, GvTopicName);
                break;
        }

    }
    #endregion

}