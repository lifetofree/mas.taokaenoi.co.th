<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="tpm_evalution_result.aspx.cs" Inherits="websystem_hr_tpm_evalution_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                ส่วนของพนักงาน</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbHead" runat="server" CommandName="navHead" OnCommand="navCommand">
                                ส่วนของหัวหน้างาน</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbHr" runat="server" CommandName="navHr" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info" id="div_list_heading" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="far fa-user"></i>&nbsp;<asp:Literal ID="litListHeadingTitle" runat="server"
                                Text="ข้อมูลพนักงาน"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:FormView ID="fvEmpProfile" runat="server" Width="100%" DefaultMode="ReadOnly"
                                OnDataBound="fvDataBound">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hfRdeptIdx" runat="server" Value='<%# Eval("rdept_idx")%>'>
                                    </asp:HiddenField>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ชื่อ-นามสกุล :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">บริษัท :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbOrgNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("org_name_th") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ฝ่าย :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbDeptNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("dept_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">แผนก :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbSecNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("sec_name_th") %>'></asp:TextBox>
                                        </div>
                                        <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="tbPosNameTh" runat="server" CssClass="form-control"
                                                Enabled="False" Text='<%# Eval("pos_name_th") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary" id="div_list_heading2" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="far fa-file-alt"></i>&nbsp;<asp:Literal ID="litListHeadingTitle2" runat="server"
                                Text="ผลการประเมิน"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all"
                            id="ContentMain_gvDocList" style="border-collapse:collapse;">
                            <tbody>
                                <tr class="info" style="font-size:Small;">
                                    <th scope="col" width="25%"></th>
                                    <th scope="col" width="25%">Weight</th>
                                    <th scope="col" width="25%">Score</th>
                                    <th scope="col" width="25%">Total</th>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Corp KPIs :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentCal1" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litSumCal1" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreCal1" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Individual KPIs :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentCal2" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litSumCal2" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreCal2" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Core Value :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentCal3" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litSumCal3" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreCal3" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Competency :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentCal4" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litSumCal4" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreCal4" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Time Attendance :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentCal5" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litSumCal5" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreCal5" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td class="text-right"><strong>Summary :</strong></td>
                                    <td class="text-right">
                                        <asp:Literal ID="litPercentSum" runat="server" Text="0.0000%"></asp:Literal>
                                    </td>
                                    <td class="text-right">-</td>
                                    <td class="text-right">
                                        <asp:Literal ID="litScoreSum" runat="server" Text="0.0000"></asp:Literal>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-default" id="div_list_heading3" runat="server">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="far fa-chart-bar"></i>&nbsp;<asp:Literal ID="litListHeadingTitle3" runat="server"
                                Text="สถิติการลา"></asp:Literal>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-md-3 control-label">ขาด :</label>
                                <div class="col-md-3 form-control-static">
                                    -
                                </div>
                                <label class="col-md-6 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ลาป่วย :</label>
                                <div class="col-md-3 form-control-static">
                                    -
                                </div>
                                <label class="col-md-6 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">ลากิจ :</label>
                                <div class="col-md-3 form-control-static">
                                    -
                                </div>
                                <label class="col-md-6 control-label text-left"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">... :</label>
                                <div class="col-md-3 form-control-static">
                                    -
                                </div>
                                <label class="col-md-6 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">มาสาย :</label>
                                <div class="col-md-3 form-control-static">
                                    -
                                </div>
                                <label class="col-md-6 control-label"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewHead" runat="server">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="panel panel-default" ID="divSearchHead" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fas fa-search"></i>&nbsp;<asp:Literal ID="litListHeadingTitleHead"
                                    runat="server" Text="Search"></asp:Literal>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="fvSearchHead" runat="server" Width="100%" DefaultMode="Insert">
                                <InsertItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHeadOrg" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHeadDept" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHeadSec" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHeadPos" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbSearchEmpCode" runat="server" CssClass="form-control"
                                                    placeholder="ค้นหาโดยรหัสพนักงาน" MaxLength="8"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทรายงาน :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHeadType" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                    <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearch" runat="server"
                                                    CssClass="btn btn-sm btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchHead"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbClear" runat="server"
                                                    CssClass="btn btn-sm btn-info" OnCommand="btnCommand"
                                                    CommandName="cmdClearHead" CommandArgument="0"><i
                                                        class="fas fa-redo"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                <asp:LinkButton ID="lbExport" runat="server"
                                                    CssClass="btn btn-sm btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdExportHead" CommandArgument="0"><i
                                                        class="far fa-file-excel"></i>&nbsp;Export</asp:LinkButton>
                                            </div>
                                            <label class="col-md-6"></label>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                    <asp:GridView ID="gvHeadList" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-responsive" OnRowDataBound="gvRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging" AllowPaging="true" PageSize="10">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก"
                            LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="องค์กร/ฝ่าย/แผนก/ตำแหน่ง">
                                <ItemTemplate>
                                    องค์กร : <asp:Label ID="lblOrgNameTh" runat="server"
                                        Text='<%# Eval("org_name_th") %>'>
                                    </asp:Label><br />
                                    ฝ่าย : <asp:Label ID="lblDeptNameTh" runat="server"
                                        Text='<%# Eval("dept_name_th") %>'>
                                    </asp:Label><br />
                                    แผนก : <asp:Label ID="lblSecNameTh" runat="server"
                                        Text='<%# Eval("sec_name_th") %>'>
                                    </asp:Label><br />
                                    ตำแหน่ง : <asp:Label ID="lblPosNameTh" runat="server"
                                        Text='<%# Eval("pos_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Corp KPIs">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal1" runat="server" Text='<%# Eval("score_cal_1") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal1" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_1"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Individual KPIs">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal2" runat="server" Text='<%# Eval("score_cal_2") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal2" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_2"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Core Value">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal3" runat="server" Text='<%# Eval("score_cal_3") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal3" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_3"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Competencies">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal4" runat="server" Text='<%# Eval("score_cal_4") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal4" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_4"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time Attendance">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal5" runat="server" Text='<%# Eval("score_cal_5") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal5" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_5"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Summary">
                                <ItemStyle CssClass="text-right text-success" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCalSum" runat="server">
                                        </asp:Label>
                                    </strong>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="row" ID="divHeadGraph" runat="server">
                        <div class="col-md-12" ID="divHeadChart1" runat="server" Visible="False">
                            <div class="panel panel-info">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart1" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" ID="divHeadChart2" runat="server" Visible="False">
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart2" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" ID="divHeadChart3" runat="server" Visible="False">
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart3" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" ID="divHeadChart4" runat="server" Visible="False">
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart4" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" ID="divHeadChart5" runat="server" Visible="False">
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart5" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" ID="divHeadChart6" runat="server" Visible="False">
                            <div class="panel panel-warning">
                                <div class="panel-body">
                                    <asp:Literal ID="litHeadChart6" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewHr" runat="server">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="panel panel-default" ID="divSearchHr" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fas fa-search"></i>&nbsp;<asp:Literal ID="litListHeadingTitleHr"
                                    runat="server" Text="Search"></asp:Literal>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="fvSearchHr" runat="server" Width="100%" DefaultMode="Insert">
                                <InsertItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค์กร :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHrOrg" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHrDept" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHrSec" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHrPos" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbSearchEmpCode" runat="server" CssClass="form-control"
                                                    placeholder="ค้นหาโดยรหัสพนักงาน" MaxLength="8"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทรายงาน :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlHrType" runat="server" CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                    <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSearch" runat="server"
                                                    CssClass="btn btn-sm btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearchHr"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbClear" runat="server"
                                                    CssClass="btn btn-sm btn-info" OnCommand="btnCommand"
                                                    CommandName="cmdClearHr" CommandArgument="0"><i
                                                        class="fas fa-redo"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                                <asp:LinkButton ID="lbExport" runat="server"
                                                    CssClass="btn btn-sm btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdExportHr" CommandArgument="0"><i
                                                        class="far fa-file-excel"></i>&nbsp;Export</asp:LinkButton>
                                            </div>
                                            <label class="col-md-6"></label>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:GridView ID="gvHrList" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-responsive" OnRowDataBound="gvRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging" AllowPaging="true" PageSize="10">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก"
                            LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="องค์กร/ฝ่าย/แผนก/ตำแหน่ง">
                                <ItemTemplate>
                                    องค์กร : <asp:Label ID="lblOrgNameTh" runat="server"
                                        Text='<%# Eval("org_name_th") %>'>
                                    </asp:Label><br />
                                    ฝ่าย : <asp:Label ID="lblDeptNameTh" runat="server"
                                        Text='<%# Eval("dept_name_th") %>'>
                                    </asp:Label><br />
                                    แผนก : <asp:Label ID="lblSecNameTh" runat="server"
                                        Text='<%# Eval("sec_name_th") %>'>
                                    </asp:Label><br />
                                    ตำแหน่ง : <asp:Label ID="lblPosNameTh" runat="server"
                                        Text='<%# Eval("pos_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Corp KPIs">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal1" runat="server" Text='<%# Eval("score_cal_1") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal1" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_1"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Individual KPIs">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal2" runat="server" Text='<%# Eval("score_cal_2") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal2" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_2"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Core Value">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal3" runat="server" Text='<%# Eval("score_cal_3") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal3" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_3"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Competencies">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal4" runat="server" Text='<%# Eval("score_cal_4") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal4" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_4"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time Attendance">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCal5" runat="server" Text='<%# Eval("score_cal_5") %>'>
                                        </asp:Label>
                                    </strong><br />
                                    <asp:Label ID="lblCal5" runat="server"
                                        Text='<%# "(" + setDecimal((decimal)Eval("cal_5"), 0).ToString() + "%)" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Summary">
                                <ItemStyle CssClass="text-right text-success" />
                                <ItemTemplate>
                                    <strong>
                                        <asp:Label ID="lblScoreCalSum" runat="server">
                                        </asp:Label>
                                    </strong>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="row" ID="divHrGraph" runat="server">
                    <div class="col-md-12" ID="divHrChart1" runat="server" Visible="False">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart1" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" ID="divHrChart2" runat="server" Visible="False">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart2" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" ID="divHrChart3" runat="server" Visible="False">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart3" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" ID="divHrChart4" runat="server" Visible="False">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart4" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" ID="divHrChart5" runat="server" Visible="False">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart5" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" ID="divHrChart6" runat="server" Visible="False">
                        <div class="panel panel-warning">
                            <div class="panel-body">
                                <asp:Literal ID="litHrChart6" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>