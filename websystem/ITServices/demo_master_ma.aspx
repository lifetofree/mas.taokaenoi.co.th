﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="demo_master_ma.aspx.cs" Inherits="websystem_ITServices_demo_master_ma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            position: relative;
            min-height: 1px;
            float: left;
            width: 33.33333333%;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
        .auto-style2 {
            position: relative;
            min-height: 1px;
            float: left;
            width: 100%;
            left: 0px;
            top: 0px;
            padding-left: 15px;
            padding-right: 15px;
        }
        </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <script type="text/javascript" src='<%=ResolveUrl("~/Scripts/jquery.MultiFile.js")%>'></script>



    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>
     <script>
         $(document).ready(function () {
             $('.from-date-datepicker').datetimepicker({
                 format: 'DD/MM/YYYY'
             });
         })
    </script>


    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <%-- Tab Menubar --%>
     <div id="BoxTabMenuIndex" runat="server">
         <nav class="navbar navbar-default">
             <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="linkIndex" runat="server">
                        <asp:LinkButton ID="btn_home" runat="server" 
                            CommandName="btn_home" 
                            OnCommand="btnCommand" Text="หน้าหลัก" />
                    </li>
                    <li id="Link1" runat="server">
                        <asp:LinkButton ID="btn_add" runat="server"
                            CommandName="btn_add" OnCommand="btnCommand" Text="เพิ่มข้อมูล" />
                    </li>
                    <li id="Link3" runat="server">
                        <asp:LinkButton ID="btn_report"  runat="server"
                        CommandName="btn_report" OnCommand="btnCommand" Text="รายงาน" />
                    </li>
                </ul>
            </div>
          </nav>
     </div>
     
    <%-- Multi view master --%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <%-- view index --%>
        <asp:View ID="ViewIndex" runat="server">
                <div class="form-group">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ข้อมูลรวม Master MA</strong></h3>
                    </div>
                    </div>
            <%-- action search --%>
                <div class="panel panel-info" >
                <div class="panel-body">
                   <div class="form-group">
                     <div class="form-horizontal" role="form">
                         <div class="form-group">
                             <div class="col-md-12">
                             <div class="col-xs-3">
                                 <asp:Label ID="Label16" CssClass="control-label" runat="server" Text="หัวข้อค้นหา" />
                                 <asp:DropDownList ID="ddlSearch" runat="server" CssClass="form-control">
                                     <asp:ListItem Value="NULL">-- Select --</asp:ListItem>
                                     <asp:ListItem Value="com_id">Com ID</asp:ListItem>
                                     <asp:ListItem Value="emp_id">รหัสผู้ครอบครอง</asp:ListItem>
                                     <asp:ListItem Value="asset_code">Asset Code</asp:ListItem>
                                     <asp:ListItem Value="name">ชื่อผู้ครอบครอง</asp:ListItem>
                                </asp:DropDownList>
                                 <asp:RequiredFieldValidator ID="ReqddlSearch" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="ddlSearch" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlSearch" Width="160" />
                             </div>
                             <div class="col-xs-3">
                                 <asp:Label ID="Label22" CssClass="control-label" runat="server" Text="คำค้น" />
                                 <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" />
                                 <asp:RequiredFieldValidator ID="ReqTxtSearch" ValidationGroup="Save_Accept" runat="server" Display="None"
                                  ControlToValidate="txtSearch" Font-Size="11"
                                  ErrorMessage="Please check values not null."
                                  ValidationExpression="Please check values not null." />
                                 <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxtSearch" Width="160" />
                             </div>
                     <div class="col-xs-3">
                          <br />
                           <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save"  
                                     ValidationGroup="Save_Accept" CommandName="btn_search"  OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการค้นหาใช่หรือไม่?')">
                                     <i class="fa fa-search"></i>ยืนยันข้อมูล</asp:LinkButton>
                            </div>
                    
                     </div>
                    </div>
                     </div>
                     </div>
                    <%-- //End action search --%>
                    <br />
                    <div class="col-md-12">
                        <div class="col-xs-3">
                        <asp:Label ID="lbErrorSearch" runat="server" CssClass="control-label" ForeColor="Red" />
                        </div>
                    </div>
                     <br />
                      <div class="auto-style2">
                       <div id="gridviewindex" class="row" runat="server">
                             <asp:GridView ID="GviewIndex" runat="server"
                                  AutoGenerateColumns="false"
                                  DataKeyNames="log_id"
                                  CssClass="table table-striped table-bordered table-responsive"
                                  HeaderStyle-CssClass="info"
                                  AllowPaging="true"
                                  PageSize="10">
                                 <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                            <Columns>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                            <ItemTemplate>
                                <asp:LinkButton ID="edit" CssClass="text-edit" runat="server" CommandName="btn_edit" OnCommand="btnCommand"
                                 CommandArgument='<%# Eval("log_id") %>' data-toggle="tooltip" title="แก้ไข" ><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btndelete" OnCommand="btnCommand" CommandArgument='<%# Eval("log_id") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblog_id" runat="server" Text='<%# Eval("log_id") %>'/>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ครอบครอง" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <p><b>รหัสพนักงาน :</b><asp:Label ID="lbEmpid" runat="server" Text='<%# Eval("emp_id") %>'/></p>
                                       <p><b>ชื่อ - สกุล :</b><asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("emp_name") %>' /></p>
                                       <p><b>แผนก :</b><asp:Label ID="lbDepartment" runat="server" Text='<%# Eval("department_name") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ทะเบียนคอม" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <p><b>รหัสอุปกรณ์ :</b><asp:Label ID="lbComid" runat="server" Text='<%# Eval("com_id") %>'/></p>
                                       <p><b>Asset Code :</b><asp:Label ID="lbAssetCode" runat="server" Text='<%# Eval("asset_code") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <asp:Label ID="lbDetail" runat="server"  Text='<%# Eval("detail") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่บันทึก" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbDate" runat="server" Text='<%# Eval("date_log") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbStatus" runat="server" Text='<%# Eval("status_ma") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Support ID" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbSupport" runat="server" Text='<%# Eval("support_id") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                            </asp:GridView>
                            </div>
                       </div>
                    </div>
                    </div>
                    </div>
        </asp:View>
        <%-- //End View Index --%>

        <%-- //Start View Edit --%>
        <asp:View ID="ViewEdit" runat="server">
           <div class="panel panel-danger">
              <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-edit "></i><strong>&nbsp; แก้ไขข้อมูล</strong></h3>
               </div>
               <div class="panel-body">
                   <div class="form-group">
                        <div class="form-horizontal" role="form">
                          <div class="form-group">
                                <div class="col-xs-4">
                                     <asp:Label ID="Label1" CssClass="control-label" runat="server" Text="รหัสเอกสาร"/>
                                     <asp:TextBox ID="lbMaster_id" CssClass="form-control" runat="server" Text='<%# Eval("log_id") %>' />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Label2" CssClass="control-label" runat="server" Text="รหัสผู้ครอบครอง" />
                                    <asp:TextBox ID="txtEmp_id" CssClass="form-control" runat="server" Text='<%# Eval("emp_id") %>'/>
                                <asp:RegularExpressionValidator ID="RegEmp_id" ValidationGroup="Save_Accept"  runat="server" Display="None"
                                 ControlToValidate="txtEmp_id" Font-Size="11"
                                 ErrorMessage="Please input type values interger."
                                 ValidationExpression="^\d+" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegEmp_id" Width="160" />
                                <asp:RequiredFieldValidator ID="ReqEmpID" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="txtEmp_id" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqEmpID" Width="160" />
                               </div>
                              </div>
                              <div class="form-group">
                              <div class="col-xs-4">
                                    <asp:Label ID="Label3" CssClass="control-label" runat="server" Text="ชื่อ - สกุล" />
                                    <asp:TextBox ID="txtEmp_name" CssClass="form-control" runat="server" Text='<%# Eval("emp_name") %>' />
                                
                                <asp:RequiredFieldValidator ID="ReqEmpName" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="txtEmp_name" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqEmpName" Width="160" />
                                </div> 
                                  <div class="col-xs-4">
                                    <asp:Label ID="Label4" CssClass="control-label" runat="server" Text="แผนก" />
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control"   >
                                        <asp:ListItem Value="0">--select--</asp:ListItem>
                                        <asp:ListItem Value="1001">MIS</asp:ListItem>
                                        <asp:ListItem Value="1002">Marketing</asp:ListItem>
                                    </asp:DropDownList>
                                
                                <asp:RequiredFieldValidator ID="ReqDepartment" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="ddlDepartment" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqDepartment" Width="160" />
                                </div>
                                </div>
                            <div class="form-group">
                                  <div class="col-xs-4">
                                    <asp:Label ID="Label5" CssClass="control-label" runat="server" Text="COM ID" />
                                    <asp:TextBox ID="txtComID" runat="server" CssClass="form-control" Text='<%# Eval("com_id") %>'  />
                                
                               
                                </div>
                                      <div class="col-xs-4">
                                    <asp:Label ID="Label6" CssClass="control-label" runat="server" Text="Asset Code" />
                                    <asp:TextBox ID="txtAssetCode" runat="server" CssClass="form-control" Text='<%# Eval("asset_code") %>'  />
                                 </div>
                                <div class="col-xs-4">
                                <asp:RequiredFieldValidator ID="ReqComid" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="txtComID" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqComid" Width="160" />
                                <asp:RegularExpressionValidator ID="RegAssetCode" ValidationGroup="Save_Accept"  runat="server" Display="None"
                                 ControlToValidate="txtAssetCode" Font-Size="11"
                                 ErrorMessage="Please input type values interger."
                                 ValidationExpression="^\d+" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegAssetCode" Width="160" />
                                <asp:RequiredFieldValidator ID="ReqAssetCode" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="txtAssetCode" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                               <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAssetCode" Width="160" />
                                </div>
                        </div> 
                        
                       
                        <div class="form-group">
                                <div class="col-xs-4">
                                    <asp:Label ID="Label7" CssClass="control-label" runat="server" Text="รายละเอียด" />
                                    <asp:TextBox ID="txtDetail" runat="server" CssClass="form-control"  Text='<%# Eval("detail") %>' />
                                <asp:RequiredFieldValidator ID="ReqDetail" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="txtDetail" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqDetail" Width="160" />
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="Label8" CssClass="control-label" runat="server" Text="สถานะ" />
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" DataTextField='<%# Eval("status_ma") %>'>
                                         <asp:ListItem Value="NULL">-- Select --</asp:ListItem>
                                         <asp:ListItem>Waitting</asp:ListItem>
                                         <asp:ListItem>Complete</asp:ListItem>
                                         <asp:ListItem>Cancel</asp:ListItem>
                                     </asp:DropDownList>
                                
                                <asp:RequiredFieldValidator ID="ReqStatus" ValidationGroup="Save_Accept" runat="server" Display="None"
                                ControlToValidate="ddlStatus" Font-Size="11"
                                ErrorMessage="Please check values not null."
                                ValidationExpression="Please check values not null." />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqStatus" Width="160" />
                            </div>
                            </div> 
                            <br />

                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-7">
                                    
                                    <asp:LinkButton ID="LinkButton1" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btn_save" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i>บันทึก</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btn_cancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i>ยกเลิก</asp:LinkButton>
                                </div>
                               <asp:Label ID="lbtest" runat="server" CssClass="control-label" />
                            </div>
               </div>
               </div>
               </div>
               </div>
        </asp:View>
        <%-- //End View Edit --%>

        <%-- //View Insert --%>
        <asp:View ID="ViewInsert" runat="server">
       
            <div class="panel panel-info" >
                <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-save"></i><strong>&nbsp; เพิ่มข้อมูล Master MA.</strong></h3>
               </div>
             <div class="panel-body">
                 <div class="form-group">
                        <div class="form-horizontal" role="form">
                             <div class="form-group">
                                
                                    <div class="col-xs-4">
                                    <asp:Label ID="Label9" CssClass="control-label" runat="server" Text="รหัสผู้ครอบครอง" />
                                    <asp:TextBox ID="txtAddEmp_id" runat="server" CssClass="form-control" />
                                    <asp:RegularExpressionValidator ID="RegAddEmp_id" ValidationGroup="Save_Accept"  runat="server" Display="None"
                                        ControlToValidate="txtAddEmp_id" Font-Size="11"
                                        ErrorMessage="Please input type values interger."
                                        ValidationExpression="^\d+" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegAddEmp_id" Width="160" />
                                     <asp:RequiredFieldValidator ID="ReqAddEmp_id" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtAddEmp_id" Font-Size="11"
                                        ErrorMessage="Please check values not null."
                                        ValidationExpression="Please check values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddEmp_id" Width="160" />
                                    </div>
                                    <div class="auto-style1">
                                    <asp:Label ID="Label11" CssClass="control-label" runat="server" Text="ชื่อ - สกุล" />
                                    <asp:TextBox ID ="txtAddEmp_name" runat="server" CssClass="form-control" />
                                    <asp:RegularExpressionValidator ID="RegTxtName" ValidationGroup="Save_Accept"  runat="server" Display="None"
                                        ControlToValidate="txtAddEmp_name" Font-Size="11"
                                        ErrorMessage="Please input type values characters."
                                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegTxtName" Width="160" />
                                    <asp:RequiredFieldValidator ID="ReqAddEmp_name" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtAddEmp_name" Font-Size="11"
                                        ErrorMessage="Please check values not null."
                                        ValidationExpression="Please check values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddEmp_name" Width="160" />
                                    </div>
                                    </div>
                   
                                 <div class="form-group">
                                    <div class="col-xs-4">
                                    
                                    <asp:Label ID="Label10" CssClass="control-label" runat="server" Text="ฝ่าย" />
                                    <asp:DropDownList ID="ddlAddDepartment" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                        <asp:ListItem Value="1001">MIS</asp:ListItem>
                                        <asp:ListItem Value="1002">Marketting</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ReqAddDepartment" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="ddlAddDepartment" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddDepartment" Width="160" />
                                    </div>
                                    
                                    <div class="col-xs-4">
                                      <asp:Label ID="Label12" CssClass="control-label" runat="server" Text="รหัสอุปกรณ์" />
                                      <asp:TextBox ID="txtAddComid" runat="server" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="ReqAddCom_id" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtAddComid" Font-Size="11"
                                        ErrorMessage="Please check values not null."
                                        ValidationExpression="Please check values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddCom_id" Width="160" />
                                    </div>
                                 </div>
                            <div class="form-group">
                                    <div class="col-xs-4">
                                      <asp:Label ID="Label13" CssClass="control-label" runat="server" Text="Asset Code" />
                                      <asp:TextBox ID="txtAddAsset_code" runat="server" CssClass="form-control" />
                                        <asp:RegularExpressionValidator ID="RegAddAsset_Code" ValidationGroup="Save_Accept"  runat="server" Display="None"
                                        ControlToValidate="txtAddAsset_code" Font-Size="11"
                                        ErrorMessage="Please input type values interger."
                                        ValidationExpression="^\d+" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegAddAsset_Code" Width="160" />
                                        <asp:RequiredFieldValidator ID="ReqAddAsset_code" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtAddAsset_code" Font-Size="11"
                                        ErrorMessage="Please check values not null."
                                        ValidationExpression="Please check values not null." />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddAsset_code" Width="160" />
                                    </div>
                                    <div class="col-xs-4">
                                      <asp:Label ID="Label14" CssClass="control-label" runat="server" Text="รายละเอียดการ MA" />
                                      <asp:TextBox ID="txtAddDetail" runat="server" TextMode="MultiLine" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="ReqAddDetail" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtAddDetail" Font-Size="11"
                                        ErrorMessage="Please check values not null."
                                        ValidationExpression="Please check values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddDetail" Width="160" />
                                    </div>
                                </div>
                             <div class="form-group">
                      
                                    <div class="col-xs-4">
                                        <asp:Label ID="Label15" CssClass="control-label" runat="server" Text="สถานะเอกสาร" />
                                      <asp:DropDownList ID="ddlAddStatus" runat="server" CssClass="form-control">
                                             <asp:ListItem Value="NULL">-- Select --</asp:ListItem>
                                             <asp:ListItem>Waitting</asp:ListItem>
                                             <asp:ListItem>Complete</asp:ListItem>
                                             <asp:ListItem>Cancel</asp:ListItem>
                                      </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="ReqAddStatus" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="ddlAddStatus" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqAddStatus" Width="160" />
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:Label ID="Label21" CssClass="control-label" runat="server" Text="Support ID" />
                                        <asp:TextBox ID="txtSupport_id" runat="server" CssClass="form-control" Text="1000234" ReadOnly="true" />
                                    </div>
                                    </div>
                   
                            
                        </div>
                     <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-7">
                                    <asp:LinkButton ID="btn_insert" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btn_insert"  OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i>บันทึก</asp:LinkButton>
                                    <asp:LinkButton ID="btn_delete" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btn_delete" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i>ยกเลิก</asp:LinkButton>
                                </div>
                      </div>
                      </div>
                    <br />
                    <br />
                     
                         
                         
                </div>
                 
                </div>
                            
                            <div id="GvInsertDiv" class="row" runat="server">
                            <asp:Panel ID ="PanelInsert" runat="server">
                            <asp:GridView ID="GvInsert" runat="server"
                              AutoGenerateColumns="false"
                              DataKeyNames="log_id"
                              CssClass="table table-striped table-bordered table-responsive"
                              HeaderStyle-CssClass="info"
                              AllowPaging="true"
                              PageSize="10">
                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                            <ItemTemplate>
                                <asp:LinkButton ID="edit" CssClass="text-edit" runat="server" CommandName="btn_edit" OnCommand="btnCommand"
                                 CommandArgument='<%# Eval("log_id") %>' data-toggle="tooltip" title="แก้ไข" ><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btndelete" OnCommand="btnCommand" CommandArgument='<%# Eval("log_id") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblog_id" runat="server" Text='<%# Eval("log_id") %>'/>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ครอบครอง" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <p><b>รหัสพนักงาน :</b><asp:Label ID="lbEmpid" runat="server" Text='<%# Eval("emp_id") %>'/></p>
                                       <p><b>ชื่อ - สกุล :</b><asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("emp_name") %>' /></p>
                                       <p><b>แผนก :</b><asp:Label ID="lbDepartment" runat="server" Text='<%# Eval("department_name") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ทะเบียนคอม" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <p><b>รหัสอุปกรณ์ :</b><asp:Label ID="lbComid" runat="server" Text='<%# Eval("com_id") %>'/></p>
                                       <p><b>Asset Code :</b><asp:Label ID="lbAssetCode" runat="server" Text='<%# Eval("asset_code") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbDetail" runat="server" Text='<%# Eval("detail") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่บันทึก" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbDate" runat="server" Text='<%# Eval("date_log") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbStatus" runat="server" Text='<%# Eval("status_ma") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Support ID" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbSupport" runat="server" Text='<%# Eval("support_id") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                                        
                            </asp:GridView>
                                   
                             </asp:Panel>
                                </div>
                          
            
                    
                        
        <%-- End View Insert --%>

        <%-- Start View Search --%>
     
        </asp:View>
        <%-- //End View Insert --%>

       

        <%-- //Start View Report --%>
         <asp:View ID="ViewReport" runat="server">
             <div class="panel panel-info" >
                 <div class="panel-heading">
                   <h3 class="panel-title"><i class="glyphicon glyphicon-export"></i><strong>&nbsp; Data Report.</strong></h3>
                 </div>
                 <div class="panel-body">
                     <div class="form-horizontal" role="form">
                         <div class="form-group">
                            <div class="col-md-12">
                            <div class="col-xs-3">
                                <asp:Label ID="Label17" CssClass="control-label" runat="server" Text="ช่วงเวลาที่ค้นหา" />
                                <asp:DropDownList ID="ddldateTime" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddldateTime_SelectedIndexChanged">
                                     <asp:ListItem Value="NULL">-- Select --</asp:ListItem>
                                     <asp:ListItem Value="more than">มากกว่า</asp:ListItem>
                                     <asp:ListItem Value="less than">น้อยกว่า</asp:ListItem>
                                     <asp:ListItem Value="between">ระหว่างวันที่</asp:ListItem>
                                </asp:DropDownList>
                                 <asp:RequiredFieldValidator ID="ReqdateTime" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="ddldateTime" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqdateTime" Width="160" />
                            </div>
                            <div class="col-xs-3">
                                <asp:Label ID="Label19" CssClass="control-label" runat="server" Text="วันที่ค้นหา" />
                                <div class='input-group date'>
                                    <asp:TextBox ID="txtDate_Start" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"/>
                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    <asp:RequiredFieldValidator ID="ReqStart_Date" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtDate_Start" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqStart_Date" Width="160" />
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <asp:Label ID="Label20" CssClass="control-label" runat="server" Text="ถึงวันที่" />
                                <div class='input-group date'>
                                 <asp:TextBox ID="txtDate_End" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%" style="left: 0px; top: 0px" />
                                 <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                <asp:RequiredFieldValidator ID="ReqEnd_date" ValidationGroup="Save_Accept" runat="server" Display="None"
                                        ControlToValidate="txtDate_End" Font-Size="11"
                                        ErrorMessage="Please select values not null."
                                        ValidationExpression="Please select values not null." />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqEnd_date" Width="160" />
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <br />
                                <asp:LinkButton ID="btn_searchDate" runat="server" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save"  
                                     ValidationGroup="Save_Accept" CommandName="btn_searchDate"  OnCommand="btnCommand">
                                     <i class="fa fa-search"></i>ค้นหาข้อมูล</asp:LinkButton>
                            </div>
                            <div class="col-xs-3">
                                <asp:Label ID="lbError" CssClass="control-label" runat="server" Font-Bold="true" ForeColor="Red" />
                            </div>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>
           
                     
                        <div id="Div1" class="row" runat="server">
                          <h4><asp:Label ID="lbhearder" runat="server" CssClass="control-label" /></h4>
                          
                           <asp:GridView ID="GviewReport" runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="log_id"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-CssClass="info"
                                AllowPaging="true"
                                PageSize="10">
                               <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                          
                            <asp:TemplateField HeaderText="เลขที่ MA" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblog_id" runat="server" Text='<%# Eval("log_id") %>'/>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ครอบครอง" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <p><b>รหัสพนักงาน :</b><asp:Label ID="lbEmpid" runat="server"  Text='<%# Eval("emp_id") %>'/></p>
                                       <p><b>ชื่อ - สกุล :</b><asp:Label ID="lbEmpName" runat="server"  Text='<%# Eval("emp_name") %>' /></p>
                                       <p><b>แผนก :</b><asp:Label ID="lbDepartment" runat="server"   Text='<%# Eval("department_name") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ทะเบียนคอม" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <p><b>รหัสอุปกรณ์ :</b><asp:Label ID="lbComid" runat="server" Text='<%# Eval("com_id") %>'/></p>
                                       <p><b>Asset Code :</b><asp:Label ID="lbAssetCode" runat="server" Text='<%# Eval("asset_code") %>' /></p>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small class="text-left">
                                       <asp:Label ID="lbDetail" runat="server" CssClass="text-left" Text='<%# Eval("detail") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่บันทึก" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbDate" runat="server" Text='<%# Eval("date_log") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbStatus" runat="server" Text='<%# Eval("status_ma") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Support ID" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                       <asp:Label ID="lbSupport" runat="server" Text='<%# Eval("support_id") %>' />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>
                            </Columns>
                           </asp:GridView>
                        </div>
                       <%-- <div id="Div2" class="row" runat="server">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-xs-3">
                                    <asp:LinkButton ID="btn_export" runat="server" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save"  
                                     ValidationGroup="Save_Accept" CommandName="btn_export"  OnCommand="btnCommand" >
                                     <i class="fa fa-export"></i>Export to Excel</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
        </asp:View>
        <%-- //End View Report --%>

       
    </asp:MultiView>

</asp:Content>

