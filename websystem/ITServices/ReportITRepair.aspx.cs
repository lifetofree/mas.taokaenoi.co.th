﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using DotNet.Highcharts.Enums;
using System.Web.UI.DataVisualization.Charting;



public partial class websystem_ITServices_ReportITRepair : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    // dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();


    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    string sum;
    string url = "http://mas.taokaenoi.co.th/itrepair";
    // string urlsap = "http://172.16.11.5/taokaenoi.co.th/MAS/ReportSap";
    // string url_it = "http://172.16.11.5/taokaenoi.co.th/MAS/ReportIT";
    string url_fb = "http://www.taokaenoi.co.th/MAS/ReportFB";
    decimal tot_base = 0;
    decimal tot_actual = 0;
    decimal tot_over = 0;
    int[] deptit = { 20, 21 };
    //int[] depttobi = { 97, 21 };


    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetOrganization = _serviceUrl + ConfigurationManager.AppSettings["urlOrganization"];
    static string urlGetAdminIT = _serviceUrl + ConfigurationManager.AppSettings["urlGetAdminIT"];

    static string urlSelect_ddlLV1IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1IT"];
    static string urlSelect_ddlLV2IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV2IT"];
    static string urlSelect_ddlLV3IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV3IT"];
    static string urlSelect_ddlLV4IT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV4IT"];
    static string urlSelect_StatusSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusSAP"];
    static string urlSelect_SYSTEMList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SYSTEMList"];
    static string urlSelect_POS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_POS"];
    static string urlSelect_PlacePOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_PlacePOS"];
    static string urlSelectCaseLV1POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV1POSU0"];
    static string urlSelectCaseLV2POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV2POSU0"];
    static string urlSelectCaseLV3POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV3POSU0"];
    static string urlSelectCaseLV4POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV4POSU0"];
    static string urlSelect_ReportPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ReportPOS"];
    static string urlSelect_ExportPOS = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ExportPOS"];
    static string urlSelect_ddlLV1SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV1"];
    static string urlSelect_ddlLV2SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV2"];
    static string urlSelect_ddlLV3SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV3"];
    static string urlSelect_ddlLV4SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV4"];
    static string urlSelect_ddlLV5SAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ddlLV5"];

    static string urlSelect_ReportSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ReportSAP"];
    static string urlSelect_ExportSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ExportSAP"];
    static string urlSelect_ReportIT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ReportIT"];
    static string urlSelect_ExportIT = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ExportIT"];
    static string urlSelect_Priority = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Priority"];
    static string urlSelect_SAPTBSLA = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPTBSLA"];
    static string urlSelect_SAPTBSLA_WherePIDX = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SAPTBSLA_WherePIDX"];

    static string urlSelectMasterDevicesRes = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterDevicesRes"];
    static string urlSelectMasterEN_RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterEN_RES"];
    static string urlSelectCaseLV1RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV1_status1"];
    static string urlSelectCaseLV2RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV2_status1"];
    static string urlSelectCaseLV3RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV3_status1"];
    static string urlSelectCaseLV4RES = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterResCaseLV4_status1"];
    static string urlReportRESTable = _serviceUrl + ConfigurationManager.AppSettings["urlReportRESTable"];
    static string urlExportExcelRESReport = _serviceUrl + ConfigurationManager.AppSettings["urlExportExcelRESReport"];


    // PHON
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    // PHON

    #endregion

    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();

            if (deptit.Contains(int.Parse(ViewState["rdept_idx"].ToString())))
            {
                ddSystemSearch.Items.Clear();
                ddSystemSearch.Items.Insert(0, new ListItem("POS", "20"));
                ddSystemSearch.Items.Insert(0, new ListItem("IT", "3"));
                ddSystemSearch.Items.Insert(0, new ListItem("SAP", "2"));
                ddSystemSearch.Items.Insert(0, new ListItem("กรุณาเลือกประเภทระบบงาน....", "0"));
                ddSystemSearch.SelectedValue = "0";
            }
            else //if(ViewState["Pos_idx"].ToString() == "13")
            {
                ddSystemSearch.Items.Clear();
                ddSystemSearch.Items.Insert(0, new ListItem("RES", "28"));
                ddSystemSearch.Items.Insert(0, new ListItem("กรุณาเลือกประเภทระบบงาน....", "0"));
                ddSystemSearch.SelectedValue = "0";

            }
            getOrganizationList(ddlorgidx);
            Select_AdminAccept();
            Select_ddlLV1IT();
            //Select_ddlLV2IT();
            //Select_ddlLV3IT();
            //Select_ddlLV4IT();
            Select_ddlPOSLV1();


            lbReportPOS.BackColor = System.Drawing.Color.LightGray;


            Select_Status();
            GenerateddlYear(ddlyear);
        }
        linkBtnTrigger(lbindex);
        linkBtnTrigger(lbalterrepair);
        linkBtnTrigger(lbReportQuestIT);



    }

    #endregion

    #region CallService

    protected DataSupportIT callServiceITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        //text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);




        return _dtsupport;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    #endregion

    #region Select SQL
    protected void Select_Priority(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("กรุณาเลือกลำดับความสำคัญ....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxStatusPriority = new PrioritySAP[1];
        PrioritySAP search = new PrioritySAP();


        _dtsupport.BoxStatusPriority[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_Priority, _dtsupport);

        ddlName.DataSource = _dtsupport.BoxStatusPriority;
        ddlName.DataTextField = "Priority_name";
        ddlName.DataValueField = "PIDX";
        ddlName.DataBind();
    }

    protected void SetDevicesRes(DropDownList ddlName)
    {
        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList dtsupport = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = dtsupport;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        _dtsupport = callServicePostITRepair(urlSelectMasterDevicesRes, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxUserRequest, "devices_name", "res_devicesidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์", "0"));

    }

    protected void SetEnRES(DropDownList ddlName)
    {
        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList dtsupport = new UserRequestList();

        _dtsupport.BoxUserRequest[0] = dtsupport;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        _dtsupport = callServicePostITRepair(urlSelectMasterEN_RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxUserRequest, "en_name", "enidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกช่าง", "0"));

    }

    protected void SetDDLCaseLV1(DropDownList ddlName)
    {

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxRESList, "RES1_Name", "RES1IDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อแจ้ง", "0"));
    }

    protected void SetDDLCaseLV2(DropDownList ddlName, int res1idx)
    {

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES1IDX = res1idx;

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV2RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxRESList, "RES2_Name", "RES2IDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรายการซ่อม", "0"));

    }

    protected void SetDDLCaseLV3(DropDownList ddlName, int res2idx)
    {

        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES2IDX = res2idx;

        _dtsupport.BoxRESList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV3RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxRESList, "RES3_Name", "RES3IDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกวิธีแก้ไข", "0"));

    }

    protected void SetDDLCaseLV4(DropDownList ddlName, int res3idx)
    {
        _dtsupport.BoxRESList = new RESList[1];
        RESList dtsupport = new RESList();

        dtsupport.RES3IDX = res3idx;

        _dtsupport.BoxRESList[0] = dtsupport;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelectCaseLV4RES, _dtsupport);
        setDdlData(ddlName, _dtsupport.BoxRESList, "RES4_Name", "RES4IDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกคำแนะนำ", "0"));

    }

    #region Case POS

    protected void Select_ddlPOSLV1()
    {

        ddlPOSLV1.Items.Clear();
        ddlPOSLV1.AppendDataBoundItems = true;
        ddlPOSLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxPOSList = new POSList[1];
        POSList casepos1 = new POSList();

        _dtsupport.BoxPOSList[0] = casepos1;


        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

        ddlPOSLV1.DataSource = _dtsupport.BoxPOSList;
        ddlPOSLV1.DataTextField = "Name_Code1";
        ddlPOSLV1.DataValueField = "POS1IDX";
        ddlPOSLV1.DataBind();

    }

    #endregion

    #region Casse IT

    protected void Select_ddlLV1IT()
    {

        ddlITLV1.Items.Clear();
        ddlITLV1.AppendDataBoundItems = true;
        ddlITLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();


        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1IT, _dtsupport);

        ddlITLV1.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV1.DataTextField = "Name_Code1";
        ddlITLV1.DataValueField = "CIT1IDX";
        ddlITLV1.DataBind();



    }

    protected void Select_ddlLV2IT()
    {

        ddlITLV2.Items.Clear();
        ddlITLV2.AppendDataBoundItems = true;
        ddlITLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);
        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV2IT, _dtsupport);

        ddlITLV2.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV2.DataTextField = "Name_Code2";
        ddlITLV2.DataValueField = "CIT2IDX";
        ddlITLV2.DataBind();



    }

    protected void Select_ddlLV3IT()
    {

        ddlITLV3.Items.Clear();
        ddlITLV3.AppendDataBoundItems = true;
        ddlITLV3.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV3IT, _dtsupport);

        ddlITLV3.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV3.DataTextField = "Name_Code3";
        ddlITLV3.DataValueField = "CIT3IDX";
        ddlITLV3.DataBind();



    }

    protected void Select_ddlLV4IT()
    {

        ddlITLV4.Items.Clear();
        ddlITLV4.AppendDataBoundItems = true;
        ddlITLV4.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));


        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);

        _dtsupport.BoxUserRequest[0] = closejobit;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV4IT, _dtsupport);

        ddlITLV4.DataSource = _dtsupport.BoxUserRequest;
        ddlITLV4.DataTextField = "Name_Code4";
        ddlITLV4.DataValueField = "CIT4IDX";
        ddlITLV4.DataBind();



    }

    #endregion

    #region Case SAP

    protected void Select_ddlLV1()
    {
        var ddlSAPLV1 = (DropDownList)ViewReport.FindControl("ddlSAPLV1");

        ddlSAPLV1.Items.Clear();
        ddlSAPLV1.AppendDataBoundItems = true;
        ddlSAPLV1.Items.Add(new ListItem("Select Module ....", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ddSystemSearch.SelectedValue);

        _dtsupport.BoxUserRequest[0] = ddllv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV1SAP, _dtsupport);


        ddlSAPLV1.DataSource = _dtsupport.BoxUserRequest;
        ddlSAPLV1.DataTextField = "Name_Code1";
        ddlSAPLV1.DataValueField = "MS1IDX";
        ddlSAPLV1.DataBind();


    }

    protected void Select_ddlLV3()
    {

        ddlSAPLV3.Items.Clear();
        ddlSAPLV3.AppendDataBoundItems = true;
        ddlSAPLV3.Items.Add(new ListItem("Select Cause ........", "0"));


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ddSystemSearch.SelectedValue);

        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV3SAP, _dtsupport);


        ddlSAPLV3.DataSource = _dtsupport.BoxUserRequest;
        ddlSAPLV3.DataTextField = "Name_Code3";
        ddlSAPLV3.DataValueField = "MS3IDX";
        ddlSAPLV3.DataBind();


    }

    protected void Select_ddlLV4()
    {

        ddlSAPLV4.Items.Clear();
        ddlSAPLV4.AppendDataBoundItems = true;
        ddlSAPLV4.Items.Add(new ListItem("Select Consist ........", "0"));


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ddSystemSearch.SelectedValue);

        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV4SAP, _dtsupport);

        ddlSAPLV4.DataSource = _dtsupport.BoxUserRequest;
        ddlSAPLV4.DataTextField = "Name_Code4";
        ddlSAPLV4.DataValueField = "MS4IDX";
        ddlSAPLV4.DataBind();


    }

    protected void Select_ddlLV5()
    {

        ddlSAPLV5.Items.Clear();
        ddlSAPLV5.AppendDataBoundItems = true;
        ddlSAPLV5.Items.Add(new ListItem("Select Adjust ........", "0"));

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList ddllv1 = new UserRequestList();

        ddllv1.SysIDX_add = int.Parse(ddSystemSearch.SelectedValue);

        _dtsupport.BoxUserRequest[0] = ddllv1;

        _dtsupport = callServicePostITRepair(urlSelect_ddlLV5SAP, _dtsupport);

        ddlSAPLV5.DataSource = _dtsupport.BoxUserRequest;
        ddlSAPLV5.DataTextField = "Name_Code5";
        ddlSAPLV5.DataValueField = "MS5IDX";
        ddlSAPLV5.DataBind();


    }

    #endregion

    #region AdminName

    protected void Select_AdminAccept()
    {

        ddladminaccept.Items.Clear();
        ddladminaccept.AppendDataBoundItems = true;
        ddladminaccept.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddladmindoing.Items.Clear();
        ddladmindoing.AppendDataBoundItems = true;
        ddladmindoing.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddlacceptsap.Items.Clear();
        ddlacceptsap.AppendDataBoundItems = true;
        ddlacceptsap.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddldoingsap.Items.Clear();
        ddldoingsap.AppendDataBoundItems = true;
        ddldoingsap.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddladminaccept_pos.Items.Clear();
        ddladminaccept_pos.AppendDataBoundItems = true;
        ddladminaccept_pos.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddladmindoing_pos.Items.Clear();
        ddladmindoing_pos.AppendDataBoundItems = true;
        ddladmindoing_pos.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddlsla_admingetjob.Items.Clear();
        ddlsla_admingetjob.AppendDataBoundItems = true;
        ddlsla_admingetjob.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddlsla_adminclosejob.Items.Clear();
        ddlsla_adminclosejob.AppendDataBoundItems = true;
        ddlsla_adminclosejob.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail organite = new employee_detail();

        _dtEmployee.employee_list[0] = organite;

        _dtEmployee = callServicePostEmp(urlGetAdminIT, _dtEmployee);

        ddladminaccept.DataSource = _dtEmployee.employee_list;
        ddladminaccept.DataTextField = "emp_fullname_th";
        ddladminaccept.DataValueField = "emp_idx";
        ddladminaccept.DataBind();

        ddladmindoing.DataSource = _dtEmployee.employee_list;
        ddladmindoing.DataTextField = "emp_fullname_th";
        ddladmindoing.DataValueField = "emp_idx";
        ddladmindoing.DataBind();


        ddlacceptsap.DataSource = _dtEmployee.employee_list;
        ddlacceptsap.DataTextField = "emp_fullname_th";
        ddlacceptsap.DataValueField = "emp_idx";
        ddlacceptsap.DataBind();

        ddldoingsap.DataSource = _dtEmployee.employee_list;
        ddldoingsap.DataTextField = "emp_fullname_th";
        ddldoingsap.DataValueField = "emp_idx";
        ddldoingsap.DataBind();

        ddladminaccept_pos.DataSource = _dtEmployee.employee_list;
        ddladminaccept_pos.DataTextField = "emp_fullname_th";
        ddladminaccept_pos.DataValueField = "emp_idx";
        ddladminaccept_pos.DataBind();

        ddladmindoing_pos.DataSource = _dtEmployee.employee_list;
        ddladmindoing_pos.DataTextField = "emp_fullname_th";
        ddladmindoing_pos.DataValueField = "emp_idx";
        ddladmindoing_pos.DataBind();

        ddlsla_admingetjob.DataSource = _dtEmployee.employee_list;
        ddlsla_admingetjob.DataTextField = "emp_fullname_th";
        ddlsla_admingetjob.DataValueField = "emp_idx";
        ddlsla_admingetjob.DataBind();

        ddlsla_adminclosejob.DataSource = _dtEmployee.employee_list;
        ddlsla_adminclosejob.DataTextField = "emp_fullname_th";
        ddlsla_adminclosejob.DataValueField = "emp_idx";
        ddlsla_adminclosejob.DataBind();
    }

    protected void Select_Checkbox_AdminList()
    {
        ddladminaccept_pos.Items.Clear();
        ddladminaccept_pos.AppendDataBoundItems = true;
        ddladminaccept_pos.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddladmindoing_pos.Items.Clear();
        ddladmindoing_pos.AppendDataBoundItems = true;
        ddladmindoing_pos.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));


        chkadminaccept.Items.Clear();
        chkadminaccept.AppendDataBoundItems = true;

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail organite = new employee_detail();
        _dtEmployee.employee_list[0] = organite;

        _dtEmployee = callServicePostEmp(urlSelect_POS, _dtEmployee);
        chkadminaccept.DataSource = _dtEmployee.employee_list;
        chkadminaccept.DataTextField = "emp_fullname_th";
        chkadminaccept.DataValueField = "emp_idx";
        chkadminaccept.DataBind();

        chkadmindoing.DataSource = _dtEmployee.employee_list;
        chkadmindoing.DataTextField = "emp_fullname_th";
        chkadmindoing.DataValueField = "emp_idx";
        chkadmindoing.DataBind();


        ddladminaccept_pos.DataSource = _dtEmployee.employee_list;
        ddladminaccept_pos.DataTextField = "emp_fullname_th";
        ddladminaccept_pos.DataValueField = "emp_idx";
        ddladminaccept_pos.DataBind();

        ddladmindoing_pos.DataSource = _dtEmployee.employee_list;
        ddladmindoing_pos.DataTextField = "emp_fullname_th";
        ddladmindoing_pos.DataValueField = "emp_idx";
        ddladmindoing_pos.DataBind();


    }

    #endregion

    #region Select

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void Select_Checkbox_PlaceList()
    {
        chkplace.Items.Clear();
        chkplace.AppendDataBoundItems = true;

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList organite = new UserRequestList();
        _dtsupport.BoxUserRequest[0] = organite;

        _dtsupport = callServicePostITRepair(urlSelect_PlacePOS, _dtsupport);
        chkplace.DataSource = _dtsupport.BoxUserRequest;
        chkplace.DataTextField = "LocName";
        chkplace.DataValueField = "r0idx";
        chkplace.DataBind();

    }

    protected void Select_Status()
    {

        ddlSearchStatus.Items.Clear();
        ddlSearchStatus.AppendDataBoundItems = true;
        ddlSearchStatus.Items.Add(new ListItem("กรุณาเลือกสถานะดำเนินการ....", "88"));


        _dtsupport = new DataSupportIT();
        _dtsupport.BoxSystem = new SystemtList[1];
        SystemtList search = new SystemtList();

        search.SysIDX = int.Parse(ddSystemSearch.SelectedValue);

        _dtsupport.BoxSystem[0] = search;

        _dtsupport = callServicePostITRepair(urlSelect_StatusSAP, _dtsupport);


        ddlSearchStatus.DataSource = _dtsupport.BoxSystem;
        ddlSearchStatus.DataTextField = "status_name";
        ddlSearchStatus.DataValueField = "stidx";
        ddlSearchStatus.DataBind();
    }

    protected void Select_Master_List()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        report.DategetJob = AddStartdate.Text;
        report.DatecloseJob = AddEndDate.Text;
        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
        report.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
        report.POS2IDX = int.Parse(ddlPOSLV2.SelectedValue);
        report.POS3IDX = int.Parse(ddlPOSLV3.SelectedValue);
        report.POS4IDX = int.Parse(ddlPOSLV4.SelectedValue);


        ViewState["ChkPlace"] = null;
        List<String> PlaceList = new List<string>();
        foreach (ListItem item in chkplace.Items)
        {
            if (item.Selected)
            {
                PlaceList.Add(item.Value);

            }
        }

        String place = String.Join(",", PlaceList.ToArray());
        ViewState["ChkPlace"] = place;
        report.ChkPlace = ViewState["ChkPlace"].ToString();


        ViewState["ChkAdaccept"] = null;
        List<String> AcceptList = new List<string>();
        foreach (ListItem item in chkadminaccept.Items)
        {
            if (item.Selected)
            {
                AcceptList.Add(item.Value);

            }
        }

        String Adaccept = String.Join(",", AcceptList.ToArray());
        ViewState["ChkAdaccept"] = Adaccept;
        report.ChkAdaccept = ViewState["ChkAdaccept"].ToString();


        ViewState["ChkAddoing"] = null;
        List<String> AddoingList = new List<string>();
        foreach (ListItem item in chkadmindoing.Items)
        {
            if (item.Selected)
            {
                AddoingList.Add(item.Value);

            }
        }

        String Addoing = String.Join(",", AddoingList.ToArray());
        ViewState["ChkAddoing"] = Addoing;
        report.ChkAddoing = ViewState["ChkAddoing"].ToString();

        _dtsupport.BoxUserRequest[0] = report;
        //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertJsonToXml(_localJson1));

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


        _dtsupport = callServicePostITRepair(urlSelect_ReportPOS, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxUserRequest);
    }

    protected void SelectIT_Master_List()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        report.DategetJob = AddStartdate.Text;
        report.DatecloseJob = AddEndDate.Text;
        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
        report.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);
        report.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);
        report.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);
        report.CIT4IDX = int.Parse(ddlITLV4.SelectedValue);
        report.AdminIDX = int.Parse(ddladminaccept.SelectedValue);
        report.AdminDoingIDX = int.Parse(ddladmindoing.SelectedValue);

        _dtsupport.BoxUserRequest[0] = report;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlSelect_ReportIT, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxUserRequest);
    }

    protected void SelectSAP_Master_List()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        report.DategetJob = AddStartdate.Text;
        report.DatecloseJob = AddEndDate.Text;
        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
        report.MS1IDX = int.Parse(ddlSAPLV1.SelectedValue);
        report.MS2IDX = int.Parse(ddlSAPLV2.SelectedValue);
        report.MS3IDX = int.Parse(ddlSAPLV3.SelectedValue);
        report.MS4IDX = int.Parse(ddlSAPLV4.SelectedValue);
        report.MS5IDX = int.Parse(ddlSAPLV5.SelectedValue);
        report.AdminIDX = int.Parse(ddlacceptsap.SelectedValue);
        report.AdminDoingIDX = int.Parse(ddldoingsap.SelectedValue);



        _dtsupport.BoxUserRequest[0] = report;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text = _localJson1;
        _dtsupport = callServicePostITRepair(urlSelect_ReportSAP, _dtsupport);
        setGridData(GvSAP, _dtsupport.BoxReportSAPList);
    }

    protected void SelectRES_Master_List()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        report.DategetJob = AddStartdate.Text;
        report.DatecloseJob = AddEndDate.Text;
        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
        report.RES1IDX = int.Parse(ddlRESLV1.SelectedValue);
        report.RES2IDX = int.Parse(ddlRESLV2.SelectedValue);
        report.RES3IDX = int.Parse(ddlRESLV3.SelectedValue);
        report.RES4IDX = int.Parse(ddlRESLV4.SelectedValue);
        report.enidx = int.Parse(ddlen.SelectedValue);
        report.DeviceIDX = int.Parse(ddldevicesres.SelectedValue);
        _dtsupport.BoxUserRequest[0] = report;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(urlReportRESTable, _dtsupport);
        setGridData(GvRES, _dtsupport.BoxUserRequest);
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void SelectTBSLA_SAP()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.DC_YEAR = ddlsla_year.SelectedValue.ToString();
        report.DC_Mount = ddlsla_month.SelectedValue.ToString();
        report.AdminIDX = int.Parse(ddlsla_admingetjob.SelectedValue);
        report.AdminDoingIDX = int.Parse(ddlsla_adminclosejob.SelectedValue);





        _dtsupport.BoxUserRequest[0] = report;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text = _localJson1;
        _dtsupport = callServicePostITRepair(urlSelect_SAPTBSLA, _dtsupport);
        setGridData(GvSLA, _dtsupport.BoxUserRequest);



    }

    protected void SelectTBSLA_WherePIDX_SAP()
    {

        _dtsupport = new DataSupportIT();
        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList report = new UserRequestList();

        report.DC_YEAR = ddlsla_year.SelectedValue.ToString();
        report.DC_Mount = ddlsla_month.SelectedValue.ToString();
        report.AdminIDX = int.Parse(ddlsla_admingetjob.SelectedValue);
        report.AdminDoingIDX = int.Parse(ddlsla_adminclosejob.SelectedValue);
        report.PIDX_Add = int.Parse(ddlsla_priority.SelectedValue);




        _dtsupport.BoxUserRequest[0] = report;
        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        //string _localJson1 = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text = _localJson1;
        _dtsupport = callServicePostITRepair(urlSelect_SAPTBSLA_WherePIDX, _dtsupport);
        setGridData(GvSLA, _dtsupport.BoxUserRequest);
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Chart POS

    public void Select_ChartLV1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 241);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.POS1_Name.ToString();
                lv1count[i] = data.countit1id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartLV2()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 242);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv2code = new string[count];
            object[] lv2count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv2code[i] = data.POS2_Name.ToString();
                lv2count[i] = data.countit2id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv2code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv2count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartLV3()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 243);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv3code = new string[count];
            object[] lv3count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv3code[i] = data.POS3_Name.ToString();
                lv3count[i] = data.countit3id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv3code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv3count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartLV4()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 244);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv4code = new string[count];
            object[] lv4count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv4code[i] = data.POS4_Name.ToString();
                lv4count[i] = data.countit4id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv4code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv4count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartLV5()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 245);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv5code = new string[count];
            object[] lv5count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv5code[i] = data.Name.ToString();
                lv5count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv5code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                    Name = "Name Repair",
                    Data = new Data(lv5count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }
    #endregion

    #region Chart IT

    public void Select_ChartITLV1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 254);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.CIT1_Code.ToString();
                lv1count[i] = data.countit1id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartITLV2()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 255);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv2code = new string[count];
            object[] lv2count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv2code[i] = data.CIT2_Code.ToString();
                lv2count[i] = data.countit2id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv2code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv2count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartITLV3()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 256);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv3code = new string[count];
            object[] lv3count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv3code[i] = data.CIT3_Name.ToString();
                lv3count[i] = data.countit3id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv3code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv3count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartITLV4()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 257);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv4code = new string[count];
            object[] lv4count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv4code[i] = data.CIT4_Code.ToString();
                lv4count[i] = data.countit4id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv4code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv4count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartITLV5()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 258);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv5code = new string[count];
            object[] lv5count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv5code[i] = data.Name.ToString();
                lv5count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv5code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                    Name = "Name Repair",
                    Data = new Data(lv5count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    #endregion

    #region Chart SAP

    public void Select_ChartSAPLV1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 250);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];

            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.MS1_Code.ToString();
                lv1count[i] = data.countit1id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(

                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }


            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartSAPLV1_1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 250);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in datasupport.BoxUserRequest)
            {
                caseclose.Add(new object[] { data.MS1_Code.ToString(), data.countit1id.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Times" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
        }
        else
        {
            litReportChart1.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartSAPLV2()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 251);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv2code = new string[count];
            object[] lv2count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv2code[i] = data.MS1_Code.ToString();
                lv2count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv2code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {

                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv2count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartSAPLV3()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 252);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv3code = new string[count];
            object[] lv3count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv3code[i] = data.MS3_Code.ToString();
                lv3count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv3code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv3count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartSAPLV4()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 253);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv4code = new string[count];
            object[] lv4count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv4code[i] = data.Name.ToString();
                lv4count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });

            chart.SetXAxis(new XAxis { Categories = lv4code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Name = "Department Name",
                    Data = new Data(lv4count),
                    Type = ChartTypes.Column
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartSAPLV4_1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 253);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in datasupport.BoxUserRequest)
            {
                caseclose.Add(new object[] { data.Name.ToString(), data.cc.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Times" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
        }
        else
        {
            litReportChart1.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 288);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);



        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] baseline = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.countit1id.ToString();
                lv1count[i] = data.sumtime.ToString();
                baseline[i] = data.baseline.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Downtimes",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "Base Line",
                 Data = new Data(baseline)
             }
            }

            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority_TB()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));
        if (ddlmonth.SelectedValue.ToString() == "00")
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 295);

        }
        else
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 288);

        }

        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        setGridData(GvChartSAP, datasupport.BoxUserRequest);
    }

    public void Select_ChartPriority_urgent()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 289);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] baseline = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.countit1id.ToString();
                lv1count[i] = data.sumtime.ToString();
                baseline[i] = data.baseline.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Downtimes",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "Base Line",
                 Data = new Data(baseline)
             }
            }

            );
            litReportChart_urgent.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart_urgent.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority_high()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 290);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] baseline = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.countit1id.ToString();
                lv1count[i] = data.sumtime.ToString();
                baseline[i] = data.baseline.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Downtimes",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "Base Line",
                 Data = new Data(baseline)
             }
            }

            );
            litReportChart_high.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart_high.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority_medium()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 291);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] baseline = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.countit1id.ToString();
                lv1count[i] = data.sumtime.ToString();
                baseline[i] = data.baseline.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart2 = new Highcharts("chart2");
            chart2.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart2.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart2.SetXAxis(new XAxis { Categories = lv1code });

            chart2.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Downtimes",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "Base Line",
                 Data = new Data(baseline)
             }
            }

            );
            litReportChart_medium.Text = chart2.ToHtmlString();
        }
        else
        {
            litReportChart_medium.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority_low()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        searchlv1.PIDX_Add = int.Parse(ddlpriority.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 292);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] baseline = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.countit1id.ToString();
                lv1count[i] = data.sumtime.ToString();
                baseline[i] = data.baseline.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart3 = new Highcharts("chart3");
            chart3.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart3.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart3.SetXAxis(new XAxis { Categories = lv1code });

            chart3.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Downtimes",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "Base Line",
                 Data = new Data(baseline)
             }
            }

            );
            litReportChart_low.Text = chart3.ToHtmlString();
        }
        else
        {
            litReportChart_low.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartPriority_TB_NotWherePIDX()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.AdminIDX = int.Parse(ddladminaccept_pos.SelectedValue);
        searchlv1.AdminDoingIDX = int.Parse(ddladmindoing_pos.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;

        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        if (ddlmonth.SelectedValue.ToString() == "00")
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 297);
            //  text.Text = "297";
        }
        else
        {
            _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 296);
            //  text.Text = "296";
        }

        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);
        setGridData(GvChartSAP, datasupport.BoxUserRequest);
    }


    #endregion

    #region Chart RES

    public void Select_ChartRESLV1()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.enidx = int.Parse(ddlenres.SelectedValue);
        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 619);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES1_Name.ToString();
                lv1count[i] = data.countit1id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRESLV2()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.enidx = int.Parse(ddlenres.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 620);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES2_Name.ToString();
                lv1count[i] = data.countit2id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRESLV3()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.enidx = int.Parse(ddlenres.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 621);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES3_Name.ToString();
                lv1count[i] = data.countit3id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRESLV4()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.enidx = int.Parse(ddlenres.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 622);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES4_Name.ToString();
                lv1count[i] = data.countit4id.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Name Repair",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRESLV5()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();

        datasupport.BoxUserRequest[0] = searchlv1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(supportlv1));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 623);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv5code = new string[count];
            object[] lv5count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv5code[i] = data.Name.ToString();
                lv5count[i] = data.cc.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv5code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                    Name = "Name Department",
                    Data = new Data(lv5count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartRESLV6()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        searchlv1.enidx = int.Parse(ddlenres.SelectedValue);

        datasupport.BoxUserRequest[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 624);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES1_Name.ToString();
                lv1count[i] = data.price.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "ค่าใช้จ่าย",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    /*
    public void Select_ChartRESLV6()
    {
        DataSupportIT datasupport = new DataSupportIT();
        UserRequestList searchlv1 = new UserRequestList();
        datasupport.BoxUserRequest = new UserRequestList[1];


        searchlv1.DC_YEAR = ddlyear.SelectedValue.ToString();
        searchlv1.DC_Mount = ddlmonth.SelectedValue.ToString();
        datasupport.BoxUserRequest[0] = searchlv1;

        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "DataSupportIT", "ITSupport", datasupport, 624);
        datasupport = (DataSupportIT)_funcTool.convertXmlToObject(typeof(DataSupportIT), _local_xml);

        if (datasupport.ReturnCode == "0")
        {
            int count = datasupport.BoxUserRequest.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] pricecase = new object[count];
            int i = 0;
            foreach (var data in datasupport.BoxUserRequest)
            {
                lv1code[i] = data.RES1_Name.ToString();
                lv1count[i] = data.CreateDateUser.ToString();
                pricecase[i] = data.price.ToString();

                  for (int ii = 0; ii < lv1code[i].Length; ii++)
                  {
                      switch (lv1code[i])
                      {
                          case "1":
                              lv1code[i] = "มกราคม";

                              break;
                          case "2":
                              lv1code[i] = "กุมภาพันธ์";

                              break;
                          case "3":
                              lv1code[i] = "มีนาคม";

                              break;
                          case "4":
                              lv1code[i] = "เมษายน";

                              break;
                          case "5":
                              lv1code[i] = "พฤษภาคม";

                              break;
                          case "6":
                              lv1code[i] = "มิถุนายน";

                              break;
                          case "7":
                              lv1code[i] = "กรกฎาคม";

                              break;
                          case "8":
                              lv1code[i] = "สิงหาคม";

                              break;
                          case "9":
                              lv1code[i] = "กันยายน";

                              break;
                          case "10":
                              lv1code[i] = "ตุลาคม";

                              break;
                          case "11":
                              lv1code[i] = "พฤศจิกายน";

                              break;
                          case "12":
                              lv1code[i] = "ธันวาคม";

                              break;
                      }
                  }
                  

                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "หัวข้อแจ้ง",
                    Data = new Data(lv1count)
                },
                 new DotNet.Highcharts.Options.Series
             {
                 Type = DotNet.Highcharts.Enums.ChartTypes.Line,
                  Name = "ค่าใช้จ่าย",
                 Data = new Data(pricecase)
             }
            }

            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }
    */

    #endregion

    #endregion

    #region Gridview
    #region Databound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvSLA":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvSLA.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var lit_sum = ((Literal)e.Row.FindControl("lit_sum"));
                        var lit_count = ((Literal)e.Row.FindControl("lit_count"));
                        var lit_average_sumtime = ((Literal)e.Row.FindControl("lit_average_sumtime"));
                        var lit_over = ((Literal)e.Row.FindControl("lit_over"));
                        var lit_over_sumtime = ((Literal)e.Row.FindControl("lit_over_sumtime"));
                        var lit_sumtime = ((Literal)e.Row.FindControl("lit_sumtime"));
                        var lit_base = ((Literal)e.Row.FindControl("lit_base"));
                        var lit_closejob = ((Literal)e.Row.FindControl("lit_closejob"));
                        var lit_pidx = ((Literal)e.Row.FindControl("lit_pidx"));
                        var lit_over_average = ((Literal)e.Row.FindControl("lit_over_average"));


                        if (lit_sumtime.Text != "")
                        {
                            if (Convert.ToDecimal(lit_sumtime.Text) < 0)
                            {
                                lit_sumtime.Text = "0";
                                lit_average_sumtime.Text = "0";
                            }

                            else
                            {
                                lit_sumtime.Text = lit_sumtime.Text;
                            }
                        }
                        else if (lit_sumtime.Text == "")
                        {
                            lit_sumtime.Text = "";
                        }

                        tot_actual = Convert.ToDecimal(lit_average_sumtime.Text);
                        lit_average_sumtime.Text = Convert.ToString(tot_actual.ToString("#.##"));



                        if (_dtsupport.ReturnCode == "0")
                        {
                            var _linqTestDetail = (from data in _dtsupport.BoxUserRequest //_itemTestDetail
                                                   where data.DatecloseJob == lit_closejob.Text//int.Parse(chk_select.Text)/*_itemTestDetail[i]*/
                                                   && data.PIDX == int.Parse(lit_pidx.Text)

                                                   select new
                                                   {
                                                       data.overtime,
                                                       data.countlist_downtime

                                                   }).ToList();
                            //  string defaultLocation = "";
                            decimal overtime = 0;
                            decimal avg_overtime = 0;
                            decimal totavg_overtime = 0;
                            int i = 0;

                            for (int ex = 0; ex < _linqTestDetail.Count(); ex++)
                            {
                                overtime += decimal.Parse(_linqTestDetail[ex].overtime.ToString());
                                avg_overtime = decimal.Parse(_linqTestDetail[ex].countlist_downtime.ToString());
                                i++;
                            }
                            lit_over_sumtime.Text = overtime.ToString();

                            if (overtime > 0)
                            {
                                totavg_overtime = overtime / i;
                                lit_over_average.Text = Convert.ToString(totavg_overtime.ToString("#.##"));
                            }
                            else
                            {
                                lit_over_average.Text = "0.00";
                            }


                        }
                        else
                        {
                            lit_over_sumtime.Text = "0.00";
                        }

                    }
                }

                break;

            case "GvChartSAP":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvChartSAP.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lit_base = ((Literal)e.Row.FindControl("lit_base"));
                        var lit_actual = ((Literal)e.Row.FindControl("lit_actual"));
                        var lit_over = ((Literal)e.Row.FindControl("lit_over"));


                        if (lit_over.Text != "")
                        {
                            if (Convert.ToDecimal(lit_over.Text) < 0)
                            {
                                lit_over.Text = "0";
                            }

                            else
                            {
                                lit_over.Text = lit_over.Text;
                            }

                            tot_over += Convert.ToDecimal(lit_over.Text);
                        }
                        else if (lit_over.Text == "")
                        {
                            lit_over.Text = "";
                        }

                        if (lit_base.Text != "")
                        {
                            tot_base += Convert.ToDecimal(lit_base.Text);
                        }
                        if (lit_actual.Text != "")
                        {
                            tot_actual += Convert.ToDecimal(lit_actual.Text);
                        }

                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    var lit_totbase = ((Literal)e.Row.FindControl("lit_totbase"));
                    var lit_totactual = ((Literal)e.Row.FindControl("lit_totactual"));
                    var lit_totover = ((Literal)e.Row.FindControl("lit_totover"));

                    lit_totbase.Text = Convert.ToString(tot_base.ToString("#.##"));
                    lit_totactual.Text = Convert.ToString(tot_actual.ToString("#.##"));

                    if (tot_over == 0)
                    {
                        lit_totover.Text = "0";
                    }
                    else
                    {
                        lit_totover.Text = Convert.ToString(tot_over.ToString("#.##"));

                    }


                }
                break;

            case "GVExportRES":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvChartSAP.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var littotalprice = ((Literal)e.Row.FindControl("littotalprice"));
                        if (_dtsupport.ReturnCode == "0")
                        {
                            var _linqTestDetail = (from data in _dtsupport.BoxExportRESList

                                                   select new
                                                   {
                                                       data.price,

                                                   }).ToList();
                            decimal total = 0;
                            int i = 0;
                           
                            for (int ex = 0; ex < _linqTestDetail.Count(); ex++)
                            {
                                if (_linqTestDetail[ex].price.ToString() != string.Empty && _linqTestDetail[ex].price.ToString() != "" && _linqTestDetail[ex].price.ToString() != null)
                                {
                                    total += decimal.Parse(_linqTestDetail[ex].price.ToString());
                                }
                                i++;
                            }
                            littotalprice.Text = String.Format("{0:N2}", total); //total.ToString();

                            
                        }
                        else
                        {
                            littotalprice.Text = "0.00";
                        }
                    }
                }
                break;

            case "GvRES":
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    var lit_total = ((Literal)e.Row.FindControl("lit_total"));

                    if (_dtsupport.ReturnCode == "0")
                    {
                        var _linqTestDetail = (from data in _dtsupport.BoxUserRequest

                                               select new
                                               {
                                                   data.price,

                                               }).ToList();
                        decimal total = 0;
                        int i = 0;

                        for (int ex = 0; ex < _linqTestDetail.Count(); ex++)
                        {
                            if (_linqTestDetail[ex].price.ToString() != string.Empty && _linqTestDetail[ex].price.ToString() != "" && _linqTestDetail[ex].price.ToString() != null)
                            {
                                total += decimal.Parse(_linqTestDetail[ex].price.ToString());
                            }
                            i++;
                        }
                        lit_total.Text = String.Format("{0:N2}", total); //total.ToString();
                    }
                    else
                    {
                        lit_total.Text = "0.00";
                    }
                }
                break;
        }
    }

    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                if (ddSystemSearch.SelectedValue == "20")
                {
                    Select_Master_List();
                }
                else if (ddSystemSearch.SelectedValue == "3")
                {
                    SelectIT_Master_List();
                }

                break;
            case "GvSAP":

                GvSAP.PageIndex = e.NewPageIndex;
                GvSAP.DataBind();

                SelectSAP_Master_List();

                break;

            case "GvSLA":

                GvSLA.PageIndex = e.NewPageIndex;
                GvSLA.DataBind();

                if (ViewState["ddlsla_priority"].ToString() != "0")
                {
                    SelectTBSLA_WherePIDX_SAP();
                }
                else
                {
                    SelectTBSLA_SAP();

                }

                mergeCell(2);
                break;

            case "GvRES":
                GvRES.PageIndex = e.NewPageIndex;
                GvRES.DataBind();
                SelectRES_Master_List();
                break;
        }
    }

    #endregion

    #region MergeCell
    protected void mergeCell(int sysidx)
    {

        switch (sysidx)
        {
            case 2:
                for (int rowIndex = GvSLA.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvSLA.Rows[rowIndex];
                    GridViewRow previousRow = GvSLA.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[1].FindControl("lit_prior")).Text == ((Literal)previousRow.Cells[1].FindControl("lit_prior")).Text &&
                        ((Literal)currentRow.Cells[8].FindControl("lit_sumtime")).Text == ((Literal)previousRow.Cells[8].FindControl("lit_sumtime")).Text &&
                        ((Literal)currentRow.Cells[9].FindControl("lit_over_sumtime")).Text == ((Literal)previousRow.Cells[9].FindControl("lit_over_sumtime")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[1].RowSpan = 2;
                            currentRow.Cells[8].RowSpan = 2;
                            currentRow.Cells[9].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                            currentRow.Cells[8].RowSpan = previousRow.Cells[8].RowSpan + 1;
                            currentRow.Cells[9].RowSpan = previousRow.Cells[9].RowSpan + 1;
                        }
                        previousRow.Cells[0].Visible = false;
                        previousRow.Cells[1].Visible = false;
                        previousRow.Cells[8].Visible = false;
                        previousRow.Cells[9].Visible = false;
                    }
                    if (((Literal)currentRow.Cells[1].FindControl("lit_prior")).Text == ((Literal)previousRow.Cells[1].FindControl("lit_prior")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[1].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                        }
                        previousRow.Cells[1].Visible = false;
                    }
                    if (((Literal)currentRow.Cells[8].FindControl("lit_sumtime")).Text == ((Literal)previousRow.Cells[8].FindControl("lit_sumtime")).Text)
                    {
                        if (previousRow.Cells[8].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[8].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[8].RowSpan = previousRow.Cells[8].RowSpan + 1;
                        }
                        previousRow.Cells[8].Visible = false;
                    }
                    if (((Literal)currentRow.Cells[9].FindControl("lit_over_sumtime")).Text == ((Literal)previousRow.Cells[9].FindControl("lit_over_sumtime")).Text &&
                         ((Literal)currentRow.Cells[7].FindControl("lit_closejob")).Text == ((Literal)previousRow.Cells[7].FindControl("lit_closejob")).Text)
                    {
                        if (previousRow.Cells[9].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[9].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[9].RowSpan = previousRow.Cells[9].RowSpan + 1;
                        }
                        previousRow.Cells[9].Visible = false;
                    }
                }
                break;
            case 28:
                for (int rowIndex = GVExportRES.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GVExportRES.Rows[rowIndex];
                    GridViewRow previousRow = GVExportRES.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[2].FindControl("littotalprice")).Text == ((Literal)previousRow.Cells[2].FindControl("littotalprice")).Text)
                    {
                        if (previousRow.Cells[2].RowSpan < 2)
                        {
                            currentRow.Cells[2].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                        }
                        previousRow.Cells[2].Visible = false;
                    }
                }
                break;
        }
    }


    #endregion

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddSystemSearch":

                if (ddSystemSearch.SelectedValue == "0")
                {

                    ddlSearchStatus.Items.Clear();
                    ddlSearchStatus.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ....", "88"));
                    ddlSearchStatus.SelectedValue = "88";
                }
                else
                {
                    ddlSearchStatus.Items.Clear();
                    ddlSearchStatus.AppendDataBoundItems = true;
                    ddlSearchStatus.Items.Add(new ListItem("กรุณาเลือกสถานะดำเนินการ....", "88"));

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxSystem = new SystemtList[1];
                    SystemtList search = new SystemtList();

                    search.SysIDX = int.Parse(ddSystemSearch.SelectedValue);

                    _dtsupport.BoxSystem[0] = search;

                    _dtsupport = callServicePostITRepair(urlSelect_StatusSAP, _dtsupport);


                    ddlSearchStatus.DataSource = _dtsupport.BoxSystem;
                    ddlSearchStatus.DataTextField = "status_name";
                    ddlSearchStatus.DataValueField = "stidx";
                    ddlSearchStatus.DataBind();


                    if (ddSystemSearch.SelectedValue == "3")
                    {
                        ddltypereport.Items.Clear();
                        ddltypereport.AppendDataBoundItems = true;
                        ddltypereport.Items.Add(new ListItem("กรุณาเลือกประเภทรายงาน", "0"));
                        ddltypereport.Items.Add(new ListItem("ตาราง", "1"));
                        ddltypereport.Items.Add(new ListItem("กราฟ", "2"));
                        panel_tbsla.Visible = false;
                        if (ddltypereport.SelectedValue == "1")
                        {
                            panel_table.Visible = true;
                            div_it.Visible = true;
                            div_pos.Visible = false;
                            div_Sap.Visible = false;
                            panel_chart.Visible = false;
                            div_button.Visible = true;

                        }
                        else
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = true;
                            Chartsap.Visible = false;
                            Chartres.Visible = false;
                        }
                        Select_AdminAccept();

                    }
                    else if (ddSystemSearch.SelectedValue == "20")
                    {
                        ddltypereport.Items.Clear();
                        ddltypereport.AppendDataBoundItems = true;
                        ddltypereport.Items.Add(new ListItem("กรุณาเลือกประเภทรายงาน", "0"));
                        ddltypereport.Items.Add(new ListItem("ตาราง", "1"));
                        ddltypereport.Items.Add(new ListItem("กราฟ", "2"));
                        panel_tbsla.Visible = false;
                        if (ddltypereport.SelectedValue == "1")
                        {
                            panel_table.Visible = true;
                            div_it.Visible = false;
                            div_pos.Visible = true;
                            div_Sap.Visible = false;
                            Select_Checkbox_AdminList();

                            Select_Checkbox_PlaceList();
                            panel_chart.Visible = false;
                            div_button.Visible = true;
                        }
                        else
                        {
                            Select_Checkbox_AdminList();

                            panel_table.Visible = false;
                            Chartitpos.Visible = true;
                            Chartsap.Visible = false;
                            Chartres.Visible = false;

                        }

                    }
                    else if (ddSystemSearch.SelectedValue == "2")
                    {
                        ddltypereport.Items.Clear();
                        ddltypereport.AppendDataBoundItems = true;
                        ddltypereport.Items.Add(new ListItem("กรุณาเลือกประเภทรายงาน", "0"));
                        ddltypereport.Items.Add(new ListItem("ตาราง", "1"));
                        ddltypereport.Items.Add(new ListItem("กราฟ", "2"));
                        ddltypereport.Items.Add(new ListItem("ตาราง SLA", "3"));

                        if (ddltypereport.SelectedValue == "1")
                        {
                            panel_table.Visible = true;
                            div_it.Visible = false;
                            div_pos.Visible = false;
                            div_Sap.Visible = true;
                            panel_chart.Visible = false;
                            panel_tbsla.Visible = false;
                            div_button.Visible = true;


                        }
                        else if (ddltypereport.SelectedValue == "2")
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = false;
                            Chartres.Visible = false;
                            Chartsap.Visible = true;
                            panel_tbsla.Visible = false;
                        }
                        else if (ddltypereport.SelectedValue == "3")
                        {
                            panel_table.Visible = false;
                            panel_chart.Visible = false;
                        }

                    }
                    if (ddSystemSearch.SelectedValue == "28")
                    {
                        ddltypereport.Items.Clear();
                        ddltypereport.AppendDataBoundItems = true;
                        ddltypereport.Items.Add(new ListItem("กรุณาเลือกประเภทรายงาน", "0"));
                        ddltypereport.Items.Add(new ListItem("ตาราง", "1"));
                        ddltypereport.Items.Add(new ListItem("กราฟ", "2"));
                        panel_tbsla.Visible = false;
                        if (ddltypereport.SelectedValue == "1")
                        {
                            panel_table.Visible = true;
                            div_it.Visible = false;
                            div_pos.Visible = false;
                            div_Sap.Visible = false;
                            panel_chart.Visible = false;
                            div_button.Visible = true;

                        }
                        else
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = false;
                            Chartsap.Visible = false;
                            Chartres.Visible = true;

                        }

                    }

                }


                break;

            case "ddltypereport":


                switch (ddltypereport.SelectedValue)
                {
                    case "1":

                        if (ddSystemSearch.SelectedValue == "3")
                        {

                            panel_table.Visible = true;
                            div_it.Visible = true;
                            div_Sap.Visible = false;
                            div_res.Visible = false;

                            div_pos.Visible = false;
                            panel_chart.Visible = false;
                            Select_AdminAccept();
                            div_res.Visible = false;

                        }
                        else if (ddSystemSearch.SelectedValue == "20")
                        {

                            panel_table.Visible = true;
                            div_pos.Visible = true;
                            div_it.Visible = false;
                            div_res.Visible = false;

                            div_Sap.Visible = false;
                            Select_Checkbox_AdminList();
                            Select_Checkbox_PlaceList();
                            panel_chart.Visible = false;
                            div_res.Visible = false;

                        }
                        else if (ddSystemSearch.SelectedValue == "2")
                        {
                            panel_table.Visible = true;
                            div_res.Visible = false;
                            div_pos.Visible = false;
                            div_it.Visible = false;
                            div_Sap.Visible = true;
                            panel_chart.Visible = false;
                            Select_AdminAccept();
                            Select_ddlLV1();
                            Select_ddlLV3();
                            Select_ddlLV4();
                            Select_ddlLV5();
                            Select_Priority(ddlpriority_tb);

                        }
                        if (ddSystemSearch.SelectedValue == "28")
                        {
                            panel_table.Visible = true;
                            div_res.Visible = true;
                            div_it.Visible = false;
                            div_pos.Visible = false;
                            div_Sap.Visible = false;
                            panel_chart.Visible = false;
                            SetDDLCaseLV1(ddlRESLV1);
                            SetDevicesRes(ddldevicesres);
                            SetEnRES(ddlen);
                        }
                        div_button.Visible = true;
                        btnexport.Visible = true;
                        panel_tbsla.Visible = false;

                        break;


                    case "2":

                        panel_table.Visible = false;
                        panel_chart.Visible = true;
                        grid1.Visible = false;

                        if (ddSystemSearch.SelectedValue == "3")
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = true;
                            Chartsap.Visible = false;
                            Chartres.Visible = false;
                            divadmin.Visible = true;
                            Select_AdminAccept();
                        }
                        else if (ddSystemSearch.SelectedValue == "20")
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = true;
                            Chartsap.Visible = false;
                            Chartres.Visible = false;
                            divadmin.Visible = true;
                            Select_Checkbox_AdminList();

                        }
                        else if (ddSystemSearch.SelectedValue == "2")
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = false;
                            Chartsap.Visible = true;
                            Chartres.Visible = false;
                            divadmin.Visible = true;
                            Select_AdminAccept();
                        }
                        if (ddSystemSearch.SelectedValue == "28")
                        {
                            panel_table.Visible = false;
                            Chartitpos.Visible = false;
                            Chartsap.Visible = false;
                            Chartres.Visible = true;
                            divadmin.Visible = false;
                            SetEnRES(ddlenres);
                        }
                        div_button.Visible = true;
                        btnexport.Visible = false;
                        panel_tbsla.Visible = false;
                        break;

                    case "3":
                        if (ddSystemSearch.SelectedValue == "2")
                        {
                            panel_table.Visible = false;
                            panel_chart.Visible = false;
                            panel_tbsla.Visible = true;
                            GenerateddlYear(ddlsla_year);
                            Select_Priority(ddlsla_priority);
                            Select_AdminAccept();
                            div_button.Visible = true;
                            btnexport.Visible = false;
                        }
                        break;


                }

                break;

            case "ddlTypeSearchDate":

                AddStartdate.Text = string.Empty;
                AddEndDate.Text = string.Empty;

                ddlSearchDate.AppendDataBoundItems = true;
                ddlSearchDate.Items.Clear();
                ddlSearchDate.Items.Add(new ListItem("เลือกเงื่อนไข ....", "00"));
                ddlSearchDate.Items.Add(new ListItem("มากกว่า >", "1"));
                ddlSearchDate.Items.Add(new ListItem("น้อยกว่า <", "2"));
                ddlSearchDate.Items.Add(new ListItem("ระหว่าง <>", "3"));


                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));

                break;

            case "ddlPOSLV1":

                if (ddlPOSLV1.SelectedValue == "0")
                {

                    ddlPOSLV1.Items.Clear();
                    ddlPOSLV1.AppendDataBoundItems = true;
                    ddlPOSLV1.Items.Add(new ListItem("เลือกเคสแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos1 = new POSList();

                    _dtsupport.BoxPOSList[0] = casepos1;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

                    ddlPOSLV1.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV1.DataTextField = "Name_Code1";
                    ddlPOSLV1.DataValueField = "POS1IDX";
                    ddlPOSLV1.DataBind();


                    ddlPOSLV2.Items.Clear();
                    ddlPOSLV2.AppendDataBoundItems = true;
                    ddlPOSLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));

                    ddlPOSLV3.Items.Clear();
                    ddlPOSLV3.AppendDataBoundItems = true;
                    ddlPOSLV3.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));

                    ddlPOSLV4.Items.Clear();
                    ddlPOSLV4.AppendDataBoundItems = true;
                    ddlPOSLV4.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));

                }
                else
                {
                    ddlPOSLV2.Items.Clear();
                    ddlPOSLV2.AppendDataBoundItems = true;
                    ddlPOSLV2.Items.Add(new ListItem("เลือกอาการแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos1 = new POSList();

                    casepos1.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos1;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV2POSU0, _dtsupport);

                    ddlPOSLV2.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV2.DataTextField = "Name_Code2";
                    ddlPOSLV2.DataValueField = "POS2IDX";
                    ddlPOSLV2.DataBind();



                    ddlPOSLV3.Items.Clear();
                    ddlPOSLV3.AppendDataBoundItems = true;
                    ddlPOSLV3.Items.Add(new ListItem("เลือกแก้ไขแจ้งซ่อม....", "0"));

                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos3 = new POSList();

                    casepos3.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos3;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV3POSU0, _dtsupport);

                    ddlPOSLV3.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV3.DataTextField = "Name_Code3";
                    ddlPOSLV3.DataValueField = "POS3IDX";
                    ddlPOSLV3.DataBind();


                    ddlPOSLV4.Items.Clear();
                    ddlPOSLV4.AppendDataBoundItems = true;
                    ddlPOSLV4.Items.Add(new ListItem("เลือกสาเหตุแจ้งซ่อม....", "0"));


                    _dtsupport = new DataSupportIT();

                    _dtsupport.BoxPOSList = new POSList[1];
                    POSList casepos4 = new POSList();

                    casepos4.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                    _dtsupport.BoxPOSList[0] = casepos4;


                    _dtsupport = callServicePostITRepair(urlSelectCaseLV4POSU0, _dtsupport);

                    ddlPOSLV4.DataSource = _dtsupport.BoxPOSList;
                    ddlPOSLV4.DataTextField = "Name_Code4";
                    ddlPOSLV4.DataValueField = "POS4IDX";
                    ddlPOSLV4.DataBind();



                }

                break;

            case "ddlSAPLV1":

                var ddlSAPLV1 = (DropDownList)ViewReport.FindControl("ddlSAPLV1");

                if (ddlSAPLV1.SelectedValue == "0")
                {
                    ddlSAPLV1.Items.Clear();
                    ddlSAPLV1.AppendDataBoundItems = true;
                    ddlSAPLV1.Items.Add(new ListItem("Select Module ....", "0"));

                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxUserRequest = new UserRequestList[1];
                    UserRequestList ddllv1 = new UserRequestList();

                    _dtsupport.BoxUserRequest[0] = ddllv1;

                    _dtsupport = callServicePostITRepair(urlSelect_ddlLV1SAP, _dtsupport);


                    ddlSAPLV1.DataSource = _dtsupport.BoxUserRequest;
                    ddlSAPLV1.DataTextField = "Name_Code1";
                    ddlSAPLV1.DataValueField = "MS1IDX";
                    ddlSAPLV1.DataBind();

                    ddlSAPLV2.Items.Clear();
                    ddlSAPLV2.Items.Insert(0, new ListItem("Select Topic....", "0"));
                    ddlSAPLV2.SelectedValue = "0";


                }
                else
                {

                    ddlSAPLV2.Items.Clear();
                    ddlSAPLV2.AppendDataBoundItems = true;
                    ddlSAPLV2.Items.Add(new ListItem("Select Topic ....", "0"));
                    _dtsupport = new DataSupportIT();
                    _dtsupport.BoxUserRequest = new UserRequestList[1];
                    UserRequestList ddllv2 = new UserRequestList();

                    ddllv2.MS1IDX = int.Parse(ddlSAPLV1.SelectedValue);
                    _dtsupport.BoxUserRequest[0] = ddllv2;

                    _dtsupport = callServicePostITRepair(urlSelect_ddlLV2SAP, _dtsupport);


                    ddlSAPLV2.DataSource = _dtsupport.BoxUserRequest;
                    ddlSAPLV2.DataTextField = "Name_Code2";
                    ddlSAPLV2.DataValueField = "MS2IDX";
                    ddlSAPLV2.DataBind();
                }


                break;

            case "ddlchartsap":
                if (ddlchartsap.SelectedValue == "7")
                {
                    div_priority.Visible = true;
                    Select_Priority(ddlpriority);
                }
                else
                {
                    div_priority.Visible = false;
                }
                break;

            case "ddlRESLV1":
                SetDDLCaseLV2(ddlRESLV2, int.Parse(ddlRESLV1.SelectedValue));
                break;

            case "ddlRESLV2":
                SetDDLCaseLV3(ddlRESLV3, int.Parse(ddlRESLV2.SelectedValue));
                break;

            case "ddlRESLV3":
                SetDDLCaseLV4(ddlRESLV4, int.Parse(ddlRESLV3.SelectedValue));
                break;

            case "ddlchartres":
                if(ddlchartres.SelectedValue != "5")
                {
                    divenreschart.Visible = true;
                }
                else
                {
                    divenreschart.Visible = false;
                }
                break;

            case "ddlITLV1":
                Select_ddlLV2IT();

                break;
            case "ddlITLV2":
                Select_ddlLV3IT();
                break;
            case "ddlITLV3":
                Select_ddlLV4IT();
                break;
        }
    }
    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":
            case "btnalterrepair":

                string emp = ViewState["EmpIDX"].ToString();
                string rdept = ViewState["rdept_idx"].ToString();


                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder s = new StringBuilder();
                s.Append("<html>");
                s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s.AppendFormat("<form name='form' action='{0}' method='post'>", url);
                s.Append("<input type='hidden' name='emp_idx' value=" + emp + " />");
                s.Append("<input type='hidden' name='rdept_idx' value=" + rdept + " />");

                s.Append("</form></body></html>");
                response.Write(s.ToString());
                response.End();


                break;

            case "btnReportFB":

                string emp_fb = ViewState["EmpIDX"].ToString();
                string rdept_fb = ViewState["rdept_idx"].ToString();


                HttpResponse response_fb = HttpContext.Current.Response;
                response_fb.Clear();

                StringBuilder s_fb = new StringBuilder();
                s_fb.Append("<html>");
                s_fb.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s_fb.AppendFormat("<form name='form' action='{0}' method='post'>", url_fb);
                s_fb.Append("<input type='hidden' name='emp_idx' value=" + emp_fb + " />");
                s_fb.Append("<input type='hidden' name='rdept_idx' value=" + rdept_fb + " />");

                s_fb.Append("</form></body></html>");
                response_fb.Write(s_fb.ToString());
                response_fb.End();
                break;

            case "btnReportPOS":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbalterrepair.BackColor = System.Drawing.Color.Transparent;
                //   lbReportSap.BackColor = System.Drawing.Color.Transparent;
                lbReportQuestIT.BackColor = System.Drawing.Color.Transparent;
                //  lbReportIT.BackColor = System.Drawing.Color.Transparent;
                lbReportPOS.BackColor = System.Drawing.Color.LightGray;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsearch":


                switch (ddSystemSearch.SelectedValue)
                {
                    case "20":
                        if (ddltypereport.SelectedValue == "1")
                        {

                            grant.Visible = false;
                            grid1.Visible = true;
                            Select_Master_List();
                            GvMaster.Visible = true;
                            GvSAP.Visible = false;
                            GvSLA.Visible = false;
                            GvRES.Visible = false;

                        }
                        else
                        {
                            grant.Visible = true;
                            grid1.Visible = false;

                            switch (ddlchart.SelectedValue)
                            {
                                case "1":
                                    Select_ChartLV1();
                                    break;

                                case "2":
                                    Select_ChartLV2();
                                    break;
                                case "3":
                                    Select_ChartLV3();
                                    break;
                                case "4":
                                    Select_ChartLV4();
                                    break;
                                case "5":
                                    Select_ChartLV5();
                                    break;

                            }
                        }
                        GvSAP.Visible = false;
                        gvchart_priority.Visible = false;
                        break;

                    case "2":
                        if (ddltypereport.SelectedValue == "1")
                        {
                            grant.Visible = false;
                            grid1.Visible = true;
                            GvSLA.Visible = false;
                            GvRES.Visible = false;

                            grant_priority.Visible = false;

                            SelectSAP_Master_List();
                            GvSAP.Visible = true;
                            GvMaster.Visible = false;
                            gvchart_priority.Visible = false;
                        }
                        else if (ddltypereport.SelectedValue == "3")
                        {
                            grant.Visible = false;
                            grant_priority.Visible = false;
                            grid1.Visible = true;
                            gvchart_priority.Visible = false;

                            if (ddlsla_priority.SelectedValue != "0")
                            {
                                SelectTBSLA_WherePIDX_SAP();
                            }
                            else
                            {
                                SelectTBSLA_SAP();

                            }
                            ViewState["ddlsla_priority"] = ddlsla_priority.SelectedValue.ToString();
                            mergeCell(2);
                            GvSLA.Visible = true;
                            GvSAP.Visible = false;
                            GvMaster.Visible = false;
                        }
                        else
                        {
                            grant.Visible = true;
                            grid1.Visible = false;
                            grant_priority.Visible = false;
                            gvchart_priority.Visible = false;

                            switch (ddlchartsap.SelectedValue)
                            {
                                case "1":

                                    litReportChart1.Visible = true;
                                    Select_ChartSAPLV1();
                                    Select_ChartSAPLV1_1();
                                    gvchart_priority.Visible = false;
                                    break;

                                case "2":

                                    litReportChart1.Visible = false;
                                    Select_ChartSAPLV2();
                                    gvchart_priority.Visible = false;
                                    break;
                                case "3":

                                    litReportChart1.Visible = false;
                                    Select_ChartSAPLV3();
                                    gvchart_priority.Visible = false;
                                    break;
                                case "4":

                                    litReportChart1.Visible = true;
                                    Select_ChartSAPLV4();
                                    Select_ChartSAPLV4_1();
                                    gvchart_priority.Visible = false;
                                    break;

                                case "5":
                                    litReportChart1.Visible = false;
                                    grant.Visible = false;
                                    gvchart_priority.Visible = false;
                                    if (ddlyear.SelectedValue == "0")
                                    {
                                        Session["DC_YEAR"] = "0";
                                    }
                                    else
                                    {
                                        Session["DC_YEAR"] = ddlyear.SelectedValue.ToString();
                                    }

                                    if (ddlmonth.SelectedValue == "0")
                                    {
                                        Session["DC_Mount"] = "00";
                                    }
                                    else
                                    {
                                        Session["DC_Mount"] = ddlmonth.SelectedValue.ToString();
                                    }

                                    if (ddladminaccept_pos.SelectedValue == "0")
                                    {
                                        Session["AdminIDX"] = "0";
                                    }
                                    else
                                    {
                                        Session["AdminIDX"] = ddladminaccept_pos.SelectedValue.ToString();
                                    }

                                    if (ddladmindoing_pos.SelectedValue == "0")
                                    {
                                        Session["AdminDoingIDX"] = "0";
                                    }
                                    else
                                    {
                                        Session["AdminDoingIDX"] = ddladmindoing_pos.SelectedValue.ToString();
                                    }
                                    Session["System"] = "ITREPAIR";
                                    Session["Choose"] = "Sum";
                                    Response.Write("<script>window.open('http://demo.taokaenoi.co.th/stackedcolumn','_blank');</script>");
                                    //  Response.Write("<script>window.open('http://localhost/mas.taokaenoi.co.th/websystem/ITServices/stackedcolumn.aspx','_blank');</script>");


                                    break;

                                case "6":
                                    litReportChart1.Visible = false;
                                    grant.Visible = false;
                                    //chart_div.Visible = true;
                                    //   Select_ChartSAPLV5();

                                    if (ddlyear.SelectedValue == "0")
                                    {
                                        Session["DC_YEAR"] = "0";
                                    }
                                    else
                                    {
                                        Session["DC_YEAR"] = ddlyear.SelectedValue.ToString();
                                    }

                                    if (ddlmonth.SelectedValue == "0")
                                    {
                                        Session["DC_Mount"] = "00";
                                    }
                                    else
                                    {
                                        Session["DC_Mount"] = ddlmonth.SelectedValue.ToString();
                                    }

                                    if (ddladminaccept_pos.SelectedValue == "0")
                                    {
                                        Session["AdminIDX"] = "0";
                                    }
                                    else
                                    {
                                        Session["AdminIDX"] = ddladminaccept_pos.SelectedValue.ToString();
                                    }

                                    if (ddladmindoing_pos.SelectedValue == "0")
                                    {
                                        Session["AdminDoingIDX"] = "0";
                                    }
                                    else
                                    {
                                        Session["AdminDoingIDX"] = ddladmindoing_pos.SelectedValue.ToString();
                                    }
                                    Session["System"] = "ITREPAIR";
                                    Session["Choose"] = "Top5";

                                    Response.Write("<script>window.open('http://mas.taokaenoi.co.th/stackedcolumn','_blank');</script>");

                                    gvchart_priority.Visible = false;

                                    break;
                                case "7":
                                    litReportChart1.Visible = false;
                                    if (ddlpriority.SelectedValue != "0")
                                    {
                                        Select_ChartPriority();
                                        Select_ChartPriority_TB();
                                        grant.Visible = true;
                                        grant_priority.Visible = false;
                                        gvchart_priority.Visible = true;
                                    }
                                    else
                                    {
                                        Select_ChartPriority_urgent();
                                        Select_ChartPriority_high();
                                        Select_ChartPriority_medium();
                                        Select_ChartPriority_low();
                                        grant.Visible = false;
                                        grant_priority.Visible = true;
                                        gvchart_priority.Visible = true;
                                        Select_ChartPriority_TB_NotWherePIDX();
                                    }

                                    break;
                            }
                        }

                        break;

                    case "3":
                        if (ddltypereport.SelectedValue == "1")
                        {
                            grant.Visible = false;
                            grid1.Visible = true;
                            GvSLA.Visible = false;

                            SelectIT_Master_List();
                            GvMaster.Visible = true;
                            GvSAP.Visible = false;
                            GvRES.Visible = false;

                        }
                        else
                        {
                            grant.Visible = true;
                            grid1.Visible = false;

                            switch (ddlchart.SelectedValue)
                            {
                                case "1":
                                    Select_ChartITLV1();
                                    break;

                                case "2":
                                    Select_ChartITLV2();
                                    break;
                                case "3":
                                    Select_ChartITLV3();
                                    break;
                                case "4":
                                    Select_ChartITLV4();
                                    break;
                                case "5":
                                    Select_ChartITLV5();
                                    break;

                            }
                        }
                        gvchart_priority.Visible = false;
                        break;

                    case "28":
                        if (ddltypereport.SelectedValue == "1")
                        {
                            grant.Visible = false;
                            grid1.Visible = true;
                            GvSLA.Visible = false;

                            SelectRES_Master_List();
                            GvMaster.Visible = false;
                            GvSAP.Visible = false;
                            GvRES.Visible = true;
                        }
                        else
                        {
                            grant.Visible = true;
                            grid1.Visible = false;

                            switch (ddlchartres.SelectedValue)
                            {
                                case "1":
                                    Select_ChartRESLV1();
                                    break;

                                case "2":
                                    Select_ChartRESLV2();
                                    break;
                                case "3":
                                    Select_ChartRESLV3();
                                    break;
                                case "4":
                                    Select_ChartRESLV4();
                                    break;
                                case "5":
                                    Select_ChartRESLV5();
                                    break;
                                case "6":
                                    Select_ChartRESLV6();
                                    break;

                            }

                        }
                        break;
                }


                break;

            case "btnexport":

                _dtsupport = new DataSupportIT();
                _dtsupport.BoxUserRequest = new UserRequestList[1];
                UserRequestList report = new UserRequestList();

                switch (ddSystemSearch.SelectedValue)
                {
                    case "20":
                        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                        report.DategetJob = AddStartdate.Text;
                        report.DatecloseJob = AddEndDate.Text;
                        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
                        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
                        report.POS1IDX = int.Parse(ddlPOSLV1.SelectedValue);
                        report.POS2IDX = int.Parse(ddlPOSLV2.SelectedValue);
                        report.POS3IDX = int.Parse(ddlPOSLV3.SelectedValue);
                        report.POS4IDX = int.Parse(ddlPOSLV4.SelectedValue);


                        ViewState["ChkPlace"] = null;
                        List<String> PlaceList = new List<string>();
                        foreach (ListItem item in chkplace.Items)
                        {
                            if (item.Selected)
                            {
                                PlaceList.Add(item.Value);

                            }
                        }

                        String place = String.Join(",", PlaceList.ToArray());
                        ViewState["ChkPlace"] = place;
                        report.ChkPlace = ViewState["ChkPlace"].ToString();


                        ViewState["ChkAdaccept"] = null;
                        List<String> AcceptList = new List<string>();
                        foreach (ListItem item in chkadminaccept.Items)
                        {
                            if (item.Selected)
                            {
                                AcceptList.Add(item.Value);

                            }
                        }

                        String Adaccept = String.Join(",", AcceptList.ToArray());
                        ViewState["ChkAdaccept"] = Adaccept;
                        report.ChkAdaccept = ViewState["ChkAdaccept"].ToString();


                        ViewState["ChkAddoing"] = null;
                        List<String> AddoingList = new List<string>();
                        foreach (ListItem item in chkadmindoing.Items)
                        {
                            if (item.Selected)
                            {
                                AddoingList.Add(item.Value);

                            }
                        }

                        String Addoing = String.Join(",", AddoingList.ToArray());
                        ViewState["ChkAddoing"] = Addoing;
                        report.ChkAddoing = ViewState["ChkAddoing"].ToString();

                        _dtsupport.BoxUserRequest[0] = report;
                        _dtsupport = callServicePostITRepair(urlSelect_ExportPOS, _dtsupport);


                        GridView2.DataSource = _dtsupport.BoxExportPOSList;
                        GridView2.DataBind();
                        break;

                    case "2":
                        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                        report.DategetJob = AddStartdate.Text;
                        report.DatecloseJob = AddEndDate.Text;
                        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
                        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
                        report.MS1IDX = int.Parse(ddlSAPLV1.SelectedValue);
                        report.MS2IDX = int.Parse(ddlSAPLV2.SelectedValue);
                        report.MS3IDX = int.Parse(ddlSAPLV3.SelectedValue);
                        report.MS4IDX = int.Parse(ddlSAPLV4.SelectedValue);
                        report.MS5IDX = int.Parse(ddlSAPLV5.SelectedValue);
                        report.AdminIDX = int.Parse(ddlacceptsap.SelectedValue);
                        report.AdminDoingIDX = int.Parse(ddldoingsap.SelectedValue);



                        _dtsupport.BoxUserRequest[0] = report;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


                        _dtsupport = callServicePostITRepair(urlSelect_ExportSAP, _dtsupport);

                        GridView2.DataSource = _dtsupport.BoxExportSAPList;
                        GridView2.DataBind();

                        break;

                    case "3":
                        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                        report.DategetJob = AddStartdate.Text;
                        report.DatecloseJob = AddEndDate.Text;
                        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
                        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
                        report.CIT1IDX = int.Parse(ddlITLV1.SelectedValue);
                        report.CIT2IDX = int.Parse(ddlITLV2.SelectedValue);
                        report.CIT3IDX = int.Parse(ddlITLV3.SelectedValue);
                        report.CIT4IDX = int.Parse(ddlITLV4.SelectedValue);
                        report.AdminIDX = int.Parse(ddladminaccept.SelectedValue);
                        report.AdminDoingIDX = int.Parse(ddladmindoing.SelectedValue);


                        _dtsupport.BoxUserRequest[0] = report;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


                        _dtsupport = callServicePostITRepair(urlSelect_ExportIT, _dtsupport);

                        GridView2.DataSource = _dtsupport.BoxExportITList;
                        GridView2.DataBind();


                        break;

                    case "28":
                        report.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                        report.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                        report.DategetJob = AddStartdate.Text;
                        report.DatecloseJob = AddEndDate.Text;
                        report.StaIDX = int.Parse(ddlSearchStatus.SelectedValue);
                        report.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                        report.RDeptID = int.Parse(ddlrdeptidx.SelectedValue);
                        report.RES1IDX = int.Parse(ddlRESLV1.SelectedValue);
                        report.RES2IDX = int.Parse(ddlRESLV2.SelectedValue);
                        report.RES3IDX = int.Parse(ddlRESLV3.SelectedValue);
                        report.RES4IDX = int.Parse(ddlRESLV4.SelectedValue);
                        report.enidx = int.Parse(ddlen.SelectedValue);
                        report.DeviceIDX = int.Parse(ddldevicesres.SelectedValue);


                        _dtsupport.BoxUserRequest[0] = report;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));


                        _dtsupport = callServicePostITRepair(urlExportExcelRESReport, _dtsupport);

                        GVExportRES.DataSource = _dtsupport.BoxExportRESList;
                        GVExportRES.DataBind();
                        mergeCell(28);
                        break;
                }


                if (ddSystemSearch.SelectedValue != "28")
                {

                    GridView2.AllowSorting = false;
                    GridView2.AllowPaging = false;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                    // Response.Charset = ""; set character 
                    Response.Charset = "utf-8";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    Response.ContentType = "application/vnd.ms-excel";

                    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);


                    GridView2.Columns[0].Visible = true;
                    GridView2.HeaderRow.BackColor = Color.White;

                    foreach (TableCell cell in GridView2.HeaderRow.Cells)
                    {
                        cell.BackColor = GridView2.HeaderStyle.BackColor;
                    }

                    foreach (GridViewRow row in GridView2.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = GridView2.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = GridView2.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    GridView2.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                else if(ddSystemSearch.SelectedValue == "28")
                {
                    GVExportRES.AllowSorting = false;
                    GVExportRES.AllowPaging = false;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                    // Response.Charset = ""; set character 
                    Response.Charset = "utf-8";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    Response.ContentType = "application/vnd.ms-excel";

                    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);


                    GVExportRES.Columns[0].Visible = true;
                    GVExportRES.HeaderRow.BackColor = Color.White;

                    foreach (TableCell cell in GVExportRES.HeaderRow.Cells)
                    {
                        cell.BackColor = GVExportRES.HeaderStyle.BackColor;
                    }

                    foreach (GridViewRow row in GVExportRES.Rows)
                    {
                        row.BackColor = Color.White;
                        foreach (TableCell cell in row.Cells)
                        {
                            if (row.RowIndex % 2 == 0)
                            {
                                cell.BackColor = GVExportRES.AlternatingRowStyle.BackColor;
                            }
                            else
                            {
                                cell.BackColor = GVExportRES.RowStyle.BackColor;
                            }
                            cell.CssClass = "textmode";
                        }
                    }

                    GVExportRES.RenderControl(hw);

                    //style to format numbers to string
                    string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
                break;

            case "btnrefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

        }
    }
    #endregion
}