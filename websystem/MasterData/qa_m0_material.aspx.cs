﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_m0_material : System.Web.UI.Page
{
    #region Init

    function_tool _funcTool = new function_tool();

    data_imxls dataImxls = new data_imxls();
    data_imxls dataImxlsStack = new data_imxls();
    service_execute servExec = new service_execute();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    //-- m0_material --//
    static string _urlQaSetMaterialImport = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetMaterialImport"];
    static string _urlQaGetMaterialImport = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetMaterialImport"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;


    #endregion Init

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

            btnTrigger(btnImport);
            // show_iconload.Visible = true;
        }
        //linkBtnTrigger(lbCreate);
        ////actionIndexWithRules();
        btnTrigger(btnImport);
        //show_iconload.Visible = true;
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();


        //Panelloading.Visible = true;
        switch (cmdName)
        {
            case "btnImport":
                //Panelloading.Visible = true;
                //FileUpload upload = (FileUpload)fvActor1Node1.FindControl("upload");
                // show_iconload.Visible = true;
                if (upload.HasFile)
                {
                    //show_iconload.Visible = true;

                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(upload.PostedFile.FileName);
                    string extension = Path.GetExtension(upload.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_material_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {


                        upload.SaveAs(filePath);
                        actionImport(filePath, extension, "Yes", FileName);                        ////File.Delete(filePath);


                        GvImportMaterial.PageIndex = 0;
                        actionIndex();

                        //setGridData(GvImportMaterial, ViewState["vs_gvimport"]);
                        //Panelloading.Visible = false;
                        //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                    }
                    else
                    {

                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                }
                break;

            case "btnDescription":
                //gvXls.Visible = false;
                //divGvXlsWhere.Visible = true;
                //btnBack.Visible = true;
                //actionRead(int.Parse(cmdArg));
                break;

            case "btnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }
    #endregion btnCommand

    #region gvmaster Import material
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        //Panelloading.Visible = true;
        //show_iconload.Visible = false;

        switch (GvName.ID)
        {
            case "GvImportMaterial":

                //show_iconload.Visible = false;

                GvImportMaterial.PageIndex = e.NewPageIndex;
                GvImportMaterial.DataBind();
                actionIndex();
                //Panelloading.Visible = false;
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvImportMaterial":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;
        }
    }

    #endregion gvmaster Import material

    #region Action
    protected void actionIndex()
    {
        data_qa _data_qaindex = new data_qa();

        _data_qaindex.qa_m0_material_list = new qa_m0_material_detail[1];
        qa_m0_material_detail _m0_materialindex = new qa_m0_material_detail();

        //_m0_materialindex. = 0;

        _data_qaindex.qa_m0_material_list[0] = _m0_materialindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevicesindex));
        _data_qaindex = callServicePostMasterQA(_urlQaGetMaterialImport, _data_qaindex);

        ViewState["vs_gvimport"] = _data_qaindex.qa_m0_material_list;

        setGridData(GvImportMaterial, ViewState["vs_gvimport"]);

    }


    protected void actionImport(string FilePath, string Extension, string isHDR, string fileName)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
        conStr = String.Format(conStr, FilePath, isHDR);
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();


        //litDebug.Text = dt.Rows.Count.ToString();
        _data_qa.qa_m0_material_list = new qa_m0_material_detail[1];
        _data_qa.qa_log_material_list = new qa_log_material_detail[1];


        qa_log_material_detail _logmaterialList = new qa_log_material_detail();
        qa_m0_material_detail _materialList = new qa_m0_material_detail();
        //_materialList.imxls_filename = fileName.ToString();
        _logmaterialList.material_count = dt.Rows.Count;
        _logmaterialList.cemp_idx = _emp_idx;//int.Parse(ViewState["EmpIDX"].ToString());


        var count_alert = dt.Rows.Count;

        var m0_matimport = new qa_m0_material_detail[dt.Rows.Count];

        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][0].ToString().Trim() != String.Empty || dt.Rows[i][1].ToString().Trim() != String.Empty)
            {

                // _materialList.material_count = int.Parse(dt.Rows.Count.ToString()) - 1;
                m0_matimport[i] = new qa_m0_material_detail();
                m0_matimport[i].material_code = dt.Rows[i][0].ToString().Trim();
                m0_matimport[i].material_name = dt.Rows[i][1].ToString().Trim();

                // j++;

                i++;
            }
        }

        _data_qa.qa_m0_material_list = m0_matimport;
        _data_qa.qa_log_material_list[0] = _logmaterialList;


        _data_qa = callServicePostMasterQA(_urlQaSetMaterialImport, _data_qa);

        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('จำนวน Material ทั้งหมดที่เพิ่ม = {0} ข้อมูล');", dt.Rows.Count.ToString()), true);



    }

    protected void actionInformationDistribute(string idxCreated)
    {
        //imxlsdistribute objImxlsDistribute = new imxlsdistribute();
        //dataImxls.imxls_distribute_action = new imxlsdistribute[1];
        //objImxlsDistribute.idx_added = idxCreated;
        //dataImxls.imxls_distribute_action[0] = objImxlsDistribute;
        //servExec.actionExec(masConn, "data_imxls", imxlsDistributeService, dataImxls, Constants.SELECT_WHERE);
    }
    #endregion Action

    #region Custom functions

    protected void btnTrigger(LinkButton btnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = btnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }


    #endregion Custom functions

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();


    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }
    #endregion reuse

}