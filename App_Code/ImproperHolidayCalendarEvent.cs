﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImproperHolidayCalendarEvent
/// </summary>
public class ImproperHolidayCalendarEvent
{
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public int holiday_idx { get; set; }
    public int org_idx { get; set; }
    public string holiday_name { get; set; }
    public bool allDay { get; set; }

}