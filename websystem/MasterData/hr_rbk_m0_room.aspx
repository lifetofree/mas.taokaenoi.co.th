﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_rbk_m0_room.aspx.cs" Inherits="websystem_MasterData_hr_rbk_m0_room" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnAddRoomBooking" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มห้องประชุม" data-toggle="tooltip" title="เพิ่มห้องประชุม" runat="server"
                    CommandName="cmdAddRoomBooking" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มห้องประชุม</asp:LinkButton>
            </div>

            <asp:FormView ID="fvRoomBooking" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">เพิ่มห้องประชุม</div>
                                <div class="panel-body">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>สถานที่</label>
                                                <asp:DropDownList ID="ddlPlace" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlPlace"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlPlace" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกสถานที่"
                                                    ValidationGroup="btnSave" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlace" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ชื่อห้องประชุม (TH)</label>
                                                <asp:TextBox ID="txt_roomname_th" runat="server" CssClass="form-control" placeholder="กรอกชื่อห้องประชุม (TH) ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_roomname_th"
                                                    runat="server"
                                                    ControlToValidate="txt_roomname_th" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องประชุม (TH)"
                                                    ValidationGroup="btnSave" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_roomname_th" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ชื่อห้องประชุม (EN)</label>
                                                <asp:TextBox ID="txt_roomname_en" runat="server" CssClass="form-control" placeholder="กรอกชื่อห้องประชุม (EN) ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_roomname_en"
                                                    runat="server"
                                                    ControlToValidate="txt_roomname_en" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องประชุม (EN)"
                                                    ValidationGroup="btnSave" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_roomname_en" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ปริมาณผู้เข้าอบรม</label>
                                                <asp:TextBox ID="txt_capacity_people" runat="server" CssClass="form-control" MaxLength="3" placeholder="กรอกปริมาณผู้เข้าอบรม ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_capacity_people"
                                                    runat="server"
                                                    ControlToValidate="txt_capacity_people" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกปริมาณผู้เข้าอบรม"
                                                    ValidationGroup="btnSave" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_capacity_people" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>รายละเอียด</label>
                                                <asp:TextBox ID="txt_room_detail" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="กรอกรายละเอียด ..." />

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="updateEquimentInsert" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <div id="div_DetailEquiment" class="panel panel-default" runat="server">
                                                        <div class="panel-body">
                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label>อุปกรณ์ภายในห้อง</label>
                                                                    <asp:DropDownList ID="ddlEquiment" runat="server" CssClass="form-control" />
                                                                    <asp:RequiredFieldValidator ID="Re_ddlEquiment"
                                                                        runat="server"
                                                                        InitialValue="0"
                                                                        ControlToValidate="ddlEquiment" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกอุปกรณ์ภายในห้อง"
                                                                        ValidationGroup="addEquimentList" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlEquiment" Width="250" />

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label>&nbsp;</label>
                                                                    <div class="clearfix"></div>
                                                                    <asp:LinkButton ID="btnAddEquiment" runat="server"
                                                                        CssClass="btn btn-primary col-md-12"
                                                                        Text="เพิ่มอุปกรณ์" OnCommand="btnCommand" CommandName="cmdAddEquiment"
                                                                        ValidationGroup="addEquimentList" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <asp:GridView ID="gvEquimentList"
                                                                    runat="server"
                                                                    CssClass="table table-striped table-responsive"
                                                                    GridLines="None"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                            <ItemTemplate>
                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="drEquimentText" HeaderText="อุปกรณ์ภายในห้อง" ItemStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                            HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnRemoveEquiment" runat="server"
                                                                                    CssClass="btn btn-danger btn-xs"
                                                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                    CommandName="btnRemoveEquiment"><i class="fa fa-times"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnUploadfileRoom" runat="server">
                                                        <div class="form-group">
                                                            <label>แนบไฟล์</label>
                                                            <%--<div class="col-sm-10">--%>
                                                            <asp:FileUpload ID="UploadFileRoom" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>
                                                            <%--<asp:RequiredFieldValidator ID="requiredUploadFileResult"
                                                        runat="server" ValidationGroup="saveDocResult"
                                                        Display="None"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="UploadFileRoom"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="*กรุณาเลือกไฟล์" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredUploadFileResult" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="requiredUploadFileResult" Width="220" />

                                                    <asp:RegularExpressionValidator ID="RegularExpreUploadFileResult"
                                                        runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg, png หรือ pdf" SetFocusOnError="true"
                                                        Display="None" ControlToValidate="UploadFileResult" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF|.png|.PNG)$"
                                                        ValidationGroup="saveDocResult" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorUploadFileResult" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpreUploadFileResult" Width="220" />--%>
                                                            <%--</div>--%>
                                                        </div>
                                                    </asp:Panel>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnInsertEquiment" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                            CommandName="CmdSave" Text="บันทึก" ValidationGroup="btnSave"
                                                            OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                        <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                            CommandName="CmdCancel" Text="ยกเลิก" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnInsertEquiment" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>

                <EditItemTemplate>
                    <asp:Label ID="lblm0_room_idx_edit" runat="server" Text='<%# Eval("m0_room_idx") %>' Visible="false" />
                    <asp:Label ID="lblplace_idx_edit" runat="server" Text='<%# Eval("place_idx") %>' Visible="false" />
                    <asp:Label ID="lbl_place_name_edit" runat="server" Text='<%# Eval("place_name") %>' Visible="false" />
                    <asp:Label ID="lbl_room_name_en_edit" runat="server" Text='<%# Eval("room_name_en") %>' Visible="false" />

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">แก้ไขรายการห้องประชุม</div>
                                <div class="panel-body">
                                    <div class="col-md-10 col-md-offset-1">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>สถานที่</label>
                                                <asp:DropDownList ID="ddlPlaceUpdate" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlPlaceUpdatee"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlPlaceUpdate" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกสถานที่"
                                                    ValidationGroup="SaveUpdate" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceUpdatee" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ชื่อห้องประชุม (TH)</label>
                                                <asp:TextBox ID="txt_roomname_th_Update" runat="server" CssClass="form-control" Text='<%# Eval("room_name_th") %>' placeholder="กรอกชื่อห้องประชุม (TH) ..." />

                                                <asp:RequiredFieldValidator ID="Re_txt_roomname_th_Update"
                                                    runat="server"
                                                    ControlToValidate="txt_roomname_th_Update" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องประชุม (TH)"
                                                    ValidationGroup="SaveUpdate" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_roomname_th_Update" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ชื่อห้องประชุม (EN)</label>
                                                <asp:TextBox ID="txt_roomname_en_Update" runat="server" CssClass="form-control" Text='<%# Eval("room_name_en") %>' placeholder="กรอกชื่อห้องประชุม (EN) ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_roomname_en_Update"
                                                    runat="server"
                                                    ControlToValidate="txt_roomname_en_Update" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อห้องประชุม (EN)"
                                                    ValidationGroup="SaveUpdate" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_roomname_en_Update" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ปริมาณผู้เข้าอบรม</label>
                                                <asp:TextBox ID="txt_capacity_people_Update" runat="server" MaxLength="2" CssClass="form-control" Text='<%# Eval("capacity_people") %>' placeholder="ปริมาณผู้เข้าอบรม ..." />
                                                <asp:RequiredFieldValidator ID="Re_txt_capacity_people_Update"
                                                    runat="server"
                                                    ControlToValidate="txt_capacity_people_Update" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกปริมาณผู้เข้าอบรม"
                                                    ValidationGroup="SaveUpdate" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_capacity_people_Update" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>รายละเอียด</label>
                                                <asp:TextBox ID="txt_room_detail_Update" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" Text='<%# Eval("room_detail") %>' placeholder="กรอกรายละเอียด ..." />

                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                        <asp:UpdatePanel ID="updatePanelGetPlaceUpdate" runat="server">
                                            <ContentTemplate>
                                                <div class="clearfix"></div>
                                                <div class="col-md-12">
                                                    <div id="panelPlaceUpdate" class="panel panel-default" runat="server">
                                                        <div class="panel-body">

                                                            <div class="col-md-10">
                                                                <div class="form-group">
                                                                    <label>อุปกรณ์ภายในห้อง</label>
                                                                    <asp:DropDownList ID="ddlEquimentUpdate" runat="server" CssClass="form-control" />
                                                                    <asp:RequiredFieldValidator ID="Re_ddlEquimentUpdate"
                                                                        runat="server"
                                                                        InitialValue="0"
                                                                        ControlToValidate="ddlEquimentUpdate" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกอุปกรณ์ภายในห้อง"
                                                                        ValidationGroup="addEquimentUpdate" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19444" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlEquimentUpdate" Width="250" />

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label>&nbsp;</label>
                                                                    <div class="clearfix"></div>
                                                                    <asp:LinkButton ID="btnAddEquimentUpdate" runat="server" CssClass="btn btn-primary col-md-12"
                                                                        Text="เพิ่มอุปกรณ์" OnCommand="btnCommand" CommandName="cmdAddEquimentUpdate"
                                                                        ValidationGroup="addEquimentUpdate" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <asp:GridView ID="gvEquimentListUpdate" runat="server"
                                                                    CssClass="table table-striped table-responsive"
                                                                    GridLines="None"
                                                                    OnRowCommand="onRowCommand"
                                                                    AutoGenerateColumns="false">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                            <ItemTemplate>
                                                                                <%# (Container.DataItemIndex + 1) %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="drEquimentTextUpdate" HeaderText="อุปกรณ์ภายในห้อง" HeaderStyle-Font-Size="Smaller"
                                                                            ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" />

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnRemoveEquimentUpdate" runat="server" CssClass="btn btn-danger btn-xs"
                                                                                    OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')" CommandName="btnRemoveEquimentUpdate">
                                                                                    <i class="fa fa-times"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>รายละเอียดไฟล์แนบ</label>
                                                <asp:HyperLink runat="server" ID="btnViewFileRoomEdit" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>สถานะ</label>
                                                <asp:DropDownList ID="ddlm0_room_statusUpdate" runat="server" CssClass="form-control"
                                                    SelectedValue='<%# Eval("m0_room_status") %>'>
                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                    <asp:ListItem Value="0" Text="ยกเลิกใช้งาน" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnUploadfileRoomEdit" runat="server">
                                                        <div class="form-group">
                                                            <label>แนบไฟล์</label>
                                                            <%--<div class="col-sm-10">--%>
                                                            <asp:FileUpload ID="UploadFileRoomEdit" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>
                                                            <%--<asp:RequiredFieldValidator ID="requiredUploadFileResult"
                                                        runat="server" ValidationGroup="saveDocResult"
                                                        Display="None"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="UploadFileRoom"
                                                        Font-Size="13px" ForeColor="Red"
                                                        ErrorMessage="*กรุณาเลือกไฟล์" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredUploadFileResult" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="requiredUploadFileResult" Width="220" />

                                                    <asp:RegularExpressionValidator ID="RegularExpreUploadFileResult"
                                                        runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg, png หรือ pdf" SetFocusOnError="true"
                                                        Display="None" ControlToValidate="UploadFileResult" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF|.png|.PNG)$"
                                                        ValidationGroup="saveDocResult" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorUploadFileResult" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpreUploadFileResult" Width="220" />--%>
                                                            <%--</div>--%>
                                                        </div>
                                                    </asp:Panel>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server" OnCommand="btnCommand" CommandName="cmdSaveUpdate" 
                                                            CommandArgument='<%# Eval("m0_room_idx") %>' Text="บันทึกการแก้ไข" ValidationGroup="SaveUpdate" />
                                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="CmdCancel" Text="ยกเลิก" />
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </EditItemTemplate>
            </asp:FormView>

            <asp:GridView ID="GvRoomBooking" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_m0_room_idx" runat="server" Visible="false" Text='<%# Eval("m0_room_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รูปห้องประชุม" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <%--<asp:ImageButton ID="btnViewFileRoom" runat="server" CssClass="pull-letf" data-toggle="tooltip" title="view" Target="_blank" data-original-title="" Width="100px" Height="100px" OnClick="header1_Click"/>--%>
                                <div class="col-sm-4">
                                    <asp:HyperLink runat="server" ID="btnViewFileRoom" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank">
                                    <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_place_idx" runat="server" Text='<%# Eval("place_idx") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lbl_place_name" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อห้องประชุม" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <p><b>ชื่อห้องประชุม (TH) :</b> <%# Eval("room_name_th") %></p>
                                    <p><b>ชื่อห้องประชุม (EN) :</b> <%# Eval("room_name_en") %></p>
                                    <asp:Label ID="lbl_room_name_en_view" runat="server" Visible="false" Text='<%# Eval("room_name_en") %>'></asp:Label>
                                    <%--<asp:Label ID="lbl_room_name_th" runat="server" Text='<%# Eval("room_name_th") %>'></asp:Label>
                                    <asp:Label ID="lbl_room_name_en" runat="server" Text='<%# Eval("room_name_en") %>'></asp:Label>--%>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">

                                    <asp:Label ID="lbl_room_detail" runat="server" Text='<%# Eval("room_detail") %>'></asp:Label>

                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ปริมาณผู้เข้าอบรม" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">

                                    <asp:Label ID="lbl_capacity_people" runat="server" Text='<%# Eval("capacity_people") %>'></asp:Label>

                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="อุปกรณ์ภายในห้อง" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                    <asp:Repeater ID="rptEquimentDetail" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="อุปกรณ์ภายในห้อง"><%# Eval("equiment_name") %></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_m0_room_statuss" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("m0_room_status") %>'></asp:Label>
                                    <asp:Label ID="m0_room_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="m0_room_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                <ContentTemplate>
                                    <div style="padding-top: 5px">
                                        <asp:LinkButton ID="btnEditRoom" CssClass="text-edit" runat="server" CommandName="CmdEdit" CommandArgument='<%# Eval("m0_room_idx") %>' data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                        <asp:LinkButton ID="btnTodeleteRoom" CssClass="text-edit" runat="server" CommandName="CmdDeleteRoom" CommandArgument='<%# Eval("m0_room_idx") %>' data-toggle="tooltip" OnCommand="btnCommand" title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                        <%--<asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="CmdDeleteRoom" CommandArgument='<%# Eval("m0_room_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>--%>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnEditRoom" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>

</asp:Content>
