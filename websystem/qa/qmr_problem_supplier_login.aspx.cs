﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_qmr_problem_supplier_login : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_qmr_problem _dtproblem = new data_qmr_problem();

    string _localJson = String.Empty;
    public string checkfile;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_Problem"];
    static string _urlInsertMaster = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMaster_Problem"];

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["EmpIDX"] = "9999999";
        LinkButton btnLogin = (LinkButton)ViewLogin.FindControl("btnLogin");

        linkBtnTrigger(btnLogin);

        if (!IsPostBack)
        {
            MvMaster.SetActiveView(ViewLogin);
        }
    }

    #region reuse
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected data_qmr_problem callServicePostQMR_Problem(string _cmdUrl, data_qmr_problem _dtproblem)
    {
        _localJson = _funcTool.convertObjectToJson(_dtproblem);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtproblem = (data_qmr_problem)_funcTool.convertJsonToObject(typeof(data_qmr_problem), _localJson);


        return _dtproblem;
    }
    protected void selectLogin(string user, string pass)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.userlogin = user;
        _select.passlogin = pass;
        _select.condition = 11;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        ViewState["rtcode"] = _dtproblem.ReturnCode.ToString();
        if (ViewState["rtcode"].ToString() == "0")
        {
            ViewState["m0_supidx"] = _dtproblem.Boxu0_ProblemDocument[0].m0_supidx.ToString();
        }
    }

    protected void UpdateResetPass(string user, string password,int cempidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _update = new M0_ProblemDocument();

        _update.user_login = user;
        _update.pass_login = password;
        _dtproblem.Boxm0_ProblemDocument[0] = _update;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 6;
        _select.CEmpIDX = cempidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {
            case "cmdLogin":

                ViewState["User"] = txtUser.Text;
                ViewState["Pass"] = txtPass.Text;
                selectLogin(ViewState["User"].ToString(), ViewState["Pass"].ToString());
                if (ViewState["rtcode"].ToString() == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เข้าสู่ระบบสำเร็จ!');", true);
                    if (ViewState["Pass"].ToString() != "12345678")
                    {
                        Session["User"] = ViewState["User"].ToString();
                        Session["Pass"] = ViewState["Pass"].ToString();
                        // Post ข้าม Form
                        Response.Redirect(ResolveUrl("~/sdp-supplier"));

                    }
                    else

                    {
                        MvMaster.SetActiveView(ViewChangePassword);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด กรุณาลองใหม่อีกครั้ง!!!');", true);
                }

                break;

            case "cmdReset":

                if (txtnewpass.Text != txtnewpass_con.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด ข้อมูลรหัสผ่านใหม่และข้อมูลยืนยันรหัสผ่านไม่ตรงกัน!!!');", true);
                }
                else
                {
                    UpdateResetPass(ViewState["User"].ToString(), txtnewpass.Text,int.Parse(ViewState["EmpIDX"].ToString()));
                    // Post ข้าม Form
                    Session["User"] = ViewState["User"].ToString();
                    Session["Pass"] = txtnewpass.Text;
                    Response.Redirect(ResolveUrl("~/sdp-supplier"));
                }
                break;
        }
    }
    #endregion
}