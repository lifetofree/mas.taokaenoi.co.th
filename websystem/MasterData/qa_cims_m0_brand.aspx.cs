﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_brand : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetBrand = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetBrand"];
    static string _urlCimsGetBrand = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetBrand"];
    static string _urlCimsDeleteBrand = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteBrand"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Brand();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Brand();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddBrand":

                Gv_select_brand.EditIndex = -1;
                Select_Brand();
                btn_addbrand.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_brand":
                Insert_Brand();
                Select_Brand();
                btn_addbrand.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_brand":
                btn_addbrand.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_brand":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Brand_de = new data_qa_cims();
                qa_cims_m0_brand_detail Brand_sde = new qa_cims_m0_brand_detail();

                Brand_de.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];

                Brand_sde.brand_idx = int.Parse(a.ToString());

                Brand_de.qa_cims_m0_brand_list[0] = Brand_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Brand_de = callServicePostMasterQACIMS(_urlCimsDeleteBrand, Brand_de);

                Select_Brand();
                //Gv_select_unit.Visible = false;
                btn_addbrand.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Brand()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_brand_name = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtbrand_name");
        DropDownList dropD_status_brand = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddBrand");

        data_qa_cims Brand_b = new data_qa_cims();
        qa_cims_m0_brand_detail Brand_s = new qa_cims_m0_brand_detail();
        Brand_b.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
        Brand_s.brand_name = tex_brand_name.Text;
        Brand_s.cemp_idx = _emp_idx;
        Brand_s.brand_status = int.Parse(dropD_status_brand.SelectedValue);
        Brand_b.qa_cims_m0_brand_list[0] = Brand_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Brand_b = callServicePostMasterQACIMS(_urlCimsSetBrand, Brand_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Brand_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_brand_name.Text = String.Empty;
        }
    }

    protected void Select_Brand()
    {
        data_qa_cims Brand_b = new data_qa_cims();
        qa_cims_m0_brand_detail Brand_s = new qa_cims_m0_brand_detail();

        Brand_b.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
        Brand_b.qa_cims_m0_brand_list[0] = Brand_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Brand_b = callServicePostMasterQACIMS(_urlCimsGetBrand, Brand_b);
        setGridData(Gv_select_brand, Brand_b.qa_cims_m0_brand_list);

    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_brand":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpbrand_status = (Label)e.Row.Cells[3].FindControl("lbpbrand_status");
                    Label brand_statusOnline = (Label)e.Row.Cells[3].FindControl("brand_statusOnline");
                    Label brand_statusOffline = (Label)e.Row.Cells[3].FindControl("brand_statusOffline");

                    ViewState["_brand_status"] = lbpbrand_status.Text;


                    if (ViewState["_brand_status"].ToString() == "1")
                    {
                        brand_statusOnline.Visible = true;
                    }
                    else if (ViewState["_brand_status"].ToString() == "0")
                    {
                        brand_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addbrand.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_brand":
                Gv_select_brand.EditIndex = e.NewEditIndex;
                Select_Brand();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_brand":

                var brand_idx = (TextBox)Gv_select_brand.Rows[e.RowIndex].FindControl("ID_brand");
                var brand_name = (TextBox)Gv_select_brand.Rows[e.RowIndex].FindControl("Name_brand");
                var brand_status = (DropDownList)Gv_select_brand.Rows[e.RowIndex].FindControl("ddEdit_brand");


                Gv_select_brand.EditIndex = -1;

                data_qa_cims brand_Update = new data_qa_cims();
                brand_Update.qa_cims_m0_brand_list = new qa_cims_m0_brand_detail[1];
                qa_cims_m0_brand_detail brand_s = new qa_cims_m0_brand_detail();
                brand_s.brand_idx = int.Parse(brand_idx.Text);
                brand_s.brand_name = brand_name.Text;
                brand_s.brand_status = int.Parse(brand_status.SelectedValue);

                brand_Update.qa_cims_m0_brand_list[0] = brand_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                brand_Update = callServicePostMasterQACIMS(_urlCimsSetBrand, brand_Update);

                Select_Brand();

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_brand":
                Gv_select_brand.EditIndex = -1;
                Select_Brand();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addbrand.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_brand":
                Gv_select_brand.PageIndex = e.NewPageIndex;
                Gv_select_brand.DataBind();
                Select_Brand();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}