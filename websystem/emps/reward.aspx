﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="reward.aspx.cs" Inherits="websystem_emps_reward" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

   <!-- Start MultiView -->
   <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
      <!-- Start Gridview & Update Form -->
      <asp:View ID="ViewIndex" runat="server">
         <div class="col-md-12">
            <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="สร้างผลตอบแทน" />
            <asp:GridView ID="GvMaster"
               runat="server"
               AutoGenerateColumns="false"
               DataKeyNames="m0_reward_idx"
               CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
               HeaderStyle-CssClass="info"
               AllowPaging="true"
               PageSize="10"
               OnRowEditing="Master_RowEditing"
               OnRowUpdating="Master_RowUpdating"
               OnRowCancelingEdit="Master_RowCancelingEdit"
               OnPageIndexChanging="Master_PageIndexChanging"
               OnRowDataBound="Master_RowDataBound">
               <PagerStyle CssClass="PageCustom" />
               <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
               <EmptyDataTemplate>
                  <div style="text-align: center">No result</div>
               </EmptyDataTemplate>
               <Columns>
                  <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                     HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                     <ItemTemplate>
                        <%# (Container.DataItemIndex +1) %>
                     </ItemTemplate>
                     <EditItemTemplate>
                        <asp:TextBox ID="m0_reward_idx" runat="server" CssClass="form-control"
                           Visible="False" Text='<%# Eval("m0_reward_idx")%>' />
                        <div class="col-md-6">
                           <div class="form-group">
                              <label class="pull-left">ชื่อผลตอบแทน</label>
                              <asp:UpdatePanel ID="panelRewardNameUpdate" runat="server">
                                 <ContentTemplate>
                                    <asp:TextBox ID="txtRewardNameUpdate" runat="server" CssClass="form-control"
                                       Text='<%# Eval("reward_name")%>' />
                                    <asp:RequiredFieldValidator ID="requiredRewardNameUpdate"
                                       ValidationGroup="saveRewardUpdate" runat="server"
                                       Display="Dynamic"
                                       SetFocusOnError="true"
                                       ControlToValidate="txtRewardNameUpdate"
                                       Font-Size="1em" ForeColor="Red"
                                       ErrorMessage="กรุณากรอกชื่อผลตอบแทน" />
                                 </ContentTemplate>
                              </asp:UpdatePanel>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label class="pull-left">มูลค่าผลตอบแทน</label>
                              <asp:UpdatePanel ID="panelRewardValueUpdate" runat="server">
                                 <ContentTemplate>
                                    <asp:TextBox ID="txtRewardValueUpdate" runat="server" CssClass="form-control"
                                       Text='<%# Eval("reward_value")%>' />
                                    <asp:RequiredFieldValidator ID="requiredRewardValueUpdate"
                                       ValidationGroup="saveRewardUpdate" runat="server"
                                       Display="Dynamic"
                                       SetFocusOnError="true"
                                       ControlToValidate="txtRewardValueUpdate"
                                       Font-Size="1em" ForeColor="Red"
                                       ErrorMessage="กรุณากรอกมูลค่าผลตอบแทน" />
                                 </ContentTemplate>
                              </asp:UpdatePanel>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label class="pull-left">สถานะ</label>
                              <asp:DropDownList ID="ddlRewardStatusUpdate" AutoPostBack="false" runat="server"
                                 CssClass="form-control" SelectedValue='<%# Eval("reward_status") %>'>
                                 <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                 <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                              </asp:DropDownList>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="pull-left">
                              <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                 ValidationGroup="saveRewardUpdate" CommandName="Update"
                                 OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                 บันทึกการเปลี่ยนแปลง
                              </asp:LinkButton>
                              <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                 CommandName="Cancel">ยกเลิก</asp:LinkButton>
                           </div>
                        </div>
                     </EditItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="ชื่อผลตอบแทน" ItemStyle-HorizontalAlign="Left"
                     HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                     <ItemTemplate>
                        <small>
                           <asp:Label ID="rewardName" runat="server" Text='<%# Eval("reward_name") %>' />
                        </small>
                     </ItemTemplate>
                     <EditItemTemplate />
                     <FooterTemplate />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="มูลค่าผลตอบแทน" ItemStyle-HorizontalAlign="Left"
                     HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                     <ItemTemplate>
                        <small>
                           <asp:Label ID="rewardValue" runat="server" Text='<%# Eval("reward_value") %>' />
                        </small>
                     </ItemTemplate>
                     <EditItemTemplate />
                     <FooterTemplate />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                     HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                     <ItemTemplate>
                        <small>
                           <asp:Label runat="server" Text='<%# getStatus((int)Eval("reward_status")) %>' />
                        </small>
                     </ItemTemplate>
                     <EditItemTemplate />
                     <FooterTemplate />
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="center"
                     HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                     <ItemTemplate>
                        <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                           data-toggle="tooltip" title="แก้ไข"><i class="fa fa-pencil fa-lg"></i></asp:LinkButton>
                        <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                           title="ลบ" CommandName="btnBan" OnCommand="btnCommand"
                           CommandArgument='<%# Eval("m0_reward_idx") %>'
                           OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                           <i class="fa fa-trash fa-lg"></i>
                        </asp:LinkButton>
                     </ItemTemplate>
                     <EditItemTemplate />
                     <FooterTemplate />
                  </asp:TemplateField>
               </Columns>
            </asp:GridView>
         </div>
      </asp:View>
      <!-- End Gridview & Update Form -->

      <!-- Start Insert Form -->
      <asp:View ID="ViewInsert" runat="server">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                     CommandName="btnCancel" OnCommand="btnCommand"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
               </div>
            </div>
            <div class="col-md-12">
               <div class="panel panel-info">
                  <div class="panel-heading">สร้างผลตอบแทน</div>
                  <div class="panel-body">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>ชื่อผลตอบแทน</label>
                           <asp:UpdatePanel ID="panelRewardName" runat="server">
                              <ContentTemplate>
                                 <asp:TextBox ID="txtRewardName" runat="server" CssClass="form-control"
                                    placeholder="ชื่อผลตอบแทน..." />
                                 <asp:RequiredFieldValidator ID="requiredRewardName"
                                    ValidationGroup="saveReward" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="txtRewardName"
                                    Font-Size="12px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกชื่อผลตอบแทน" />
                              </ContentTemplate>
                           </asp:UpdatePanel>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>มูลค่าผลตอบแทน</label>
                           <asp:UpdatePanel ID="panelRewardValue" runat="server">
                              <ContentTemplate>
                                 <asp:TextBox ID="txtRewardValue" runat="server" CssClass="form-control"
                                    placeholder="มูลค่าผลตอบแทน..." />
                                 <asp:RequiredFieldValidator ID="requiredRewardValue"
                                    ValidationGroup="saveReward" runat="server"
                                    Display="Dynamic"
                                    SetFocusOnError="true"
                                    ControlToValidate="txtRewardValue"
                                    Font-Size="12px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกมูลค่าผลตอบแทน" />
                              </ContentTemplate>
                           </asp:UpdatePanel>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>สถานะ</label>
                           <asp:DropDownList ID="ddlRewardStatus" runat="server" CssClass="form-control">
                              <asp:ListItem Value="1" Text="Online" />
                              <asp:ListItem Value="0" Text="Offline" />
                           </asp:DropDownList>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                           <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="saveReward" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </asp:View>
      <!-- End Insert Form -->

   </asp:MultiView>
   <!-- End MultiView -->

</asp:Content>
