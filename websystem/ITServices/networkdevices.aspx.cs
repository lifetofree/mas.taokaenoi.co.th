﻿using System;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;

using System.Collections.Generic;
using System.Linq;

using System.Text.RegularExpressions;
//using InfoSoftGlobal;
using System.Net;
using System.Web.Security;
using System.Text;

using AjaxControlToolkit;

using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;

using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;


public partial class websystem_ITServices_networkdevices : System.Web.UI.Page
{
    #region initial function/data

    service_mail servicemail = new service_mail();


    function_tool _funcTool = new function_tool();

    data_network _data_network = new data_network();
    data_networkdevices _data_networkdevices = new data_networkdevices();

    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    static string _keynetworkdevices = ConfigurationManager.AppSettings["keynetworkdevices"];


    static string _urlGetemp = _serviceUrl + ConfigurationManager.AppSettings["urlGetemp"];
    static string _urlGetempsearch = _serviceUrl + ConfigurationManager.AppSettings["urlGetempsearch"];
    static string _urlSetNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetNetwork"];
    static string _urlGetNetworkIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetNetworkIndex"];
    static string _urlGetNetworkView = _serviceUrl + ConfigurationManager.AppSettings["urlGetNetworkView"];
    static string _urlGetLogNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogNetwork"];
    static string _urlSetApproveNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveNetwork"];
    static string _urlSetRepairNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetRepairNetwork"];
    static string _urlGetLogRepair = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogRepair"];
    static string _urlSetMANetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetMANetwork"];
    static string _urlGetLogMA = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogMA"];
    static string _urlGetSearch = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearch"];
    static string _urlGetSearchCut = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchCut"];
    static string _urlGetShowDetailCut = _serviceUrl + ConfigurationManager.AppSettings["urlGetShowDetailCut"];
    static string _urlGetLogRepairCut = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogRepairCut"];
    static string _urlGetLogMACut = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogMACut"];
    static string _urlSetCutNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetCutNetwork"];
    static string _urlGetSearchMove = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchMove"];
    static string _urlGetShowDetailMove = _serviceUrl + ConfigurationManager.AppSettings["urlGetShowDetailMove"];
    static string _urlSetMoveNetwork = _serviceUrl + ConfigurationManager.AppSettings["urlSetMoveNetwork"];
    static string _urlGetExportExcel = _serviceUrl + ConfigurationManager.AppSettings["urlGetExportExcel"];
    static string _urlGetSearchReport = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchReport"];
    static string _urlGetEdit = _serviceUrl + ConfigurationManager.AppSettings["urlGetEdit"];

    static string _urlGetddltype = _serviceUrl + ConfigurationManager.AppSettings["urlGetddltype"];
    static string _urlGetddlcatenetwork = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlcatenetwork"];
    static string _urlGetm0Company = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Company"];
    static string _urlGetm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Place"];
    static string _urlGetddlroomnetwork = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlroomnetwork"];
    static string _urlGetm0Category = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Category"];
    static string _urlGetm0Room = _serviceUrl + ConfigurationManager.AppSettings["urlGetm0Room"];
    static string _urlGetddlCompany = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlCompany"];

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    string _localJson = "";
    int _tempInt = 0;

    string _mail_subject = "";
    string _mail_body = "";

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;


    int m0_node = 0;
    int m0_actor = 0;
    int m0_status = 0;

    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
         
        if (!IsPostBack)
        {

            actionIndexEmp(); // เช็คคนเข้าระบบ

            //คนที่สามารถเข้าระบบได้
            if (ViewState["emp_idx_emp"].ToString() == "1413" || ViewState["emp_idx_emp"].ToString() == "178" || ViewState["emp_idx_emp"].ToString() == "174" || ViewState["emp_idx_emp"].ToString() == "1394" || ViewState["emp_idx_emp"].ToString() == "1347" || ViewState["emp_idx_emp"].ToString() == "172" || ViewState["emp_idx_emp"].ToString() == "173" || ViewState["emp_idx_emp"].ToString() == "3760" || ViewState["emp_idx_emp"].ToString() == "1374" || ViewState["emp_idx_emp"].ToString() == "20140" || ViewState["emp_idx_emp"].ToString() == "3593")
            {
                emp_idx = int.Parse(Session["emp_idx"].ToString());


                initPage();
                actionIndex();
                Set_Defult_Index(); //set tab menu 


                /// *** ddl show search  *** ///
                actionddltypesearch();
                actionddlplacesearch();
                actionddlcompanysearch();
                actionempsearch();

                /// *** ddl show search  *** ///

                ////// *** เอารหัสพนักงานไปหารายละเอียด *** ///////
                _data_network.empinsert_list = new empinsert_detail[1];
                empinsert_detail _empDetailinsert = new empinsert_detail();

                _empDetailinsert.emp_idx = emp_idx;

                _data_network.empinsert_list[0] = _empDetailinsert;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_network));
                _data_network = callServiceDataNetwork(_urlGetemp, _data_network);

                ViewState["emp_name_th"] = _data_network.empinsert_list[0].emp_name_th;
                ViewState["dept_name_th"] = _data_network.empinsert_list[0].dept_name_th;
                ViewState["sec_name_th"] = _data_network.empinsert_list[0].sec_name_th;
                ViewState["pos_name_th"] = _data_network.empinsert_list[0].pos_name_th;
                ViewState["emp_mobile_no"] = _data_network.empinsert_list[0].emp_mobile_no;
                ViewState["emp_email"] = _data_network.empinsert_list[0].emp_email;


                //start teppanop //
                if (Session["_sesion_devices"] != null)
                {
                    if (Session["_sesion_devices"].ToString() == "create")
                    {
                        create_asset();
                    }
                }
                //end teppanop //


            }
            else
            {
                
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์เข้าระบบ');", true);

                divMenu.Visible = false;
                btnSearch.Visible = false;

                litShowDetail.Text = "คุณไม่มีสิทธิ์เข้าระบบ Network Devices";

                //Response.Redirect("http://mas.taokaenoi.co.th");
            }
        }
        else
        {
          
        }

        linkBtnTrigger(btnToDeviceNetwork);
        
       
        //used_java_Scripts(); // ***  วันที่  ***///
    }

    #region selected Emp System   
    protected void actionIndexEmp() //เอาไว้ใช้กัน Link ตรง
    {

        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

        ViewState["emp_idx_emp"] = _data_employee.employee_list[0].emp_idx;
        ViewState["rdept_idx_emp"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx_emp"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected Emp System  

    #region selected   
    protected void actionIndex()
    {

        data_network _data_networkindex = new data_network();
        _data_networkindex.network_list = new network_detail[1];

        network_detail _network_detailindex = new network_detail();

        //_network_detailindex.u0_devicenetwork_idx = 0;

        _data_networkindex.network_list[0] = _network_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_networkindex));
        _data_networkindex = callServiceDataNetwork(_urlGetNetworkIndex, _data_networkindex);


        ViewState["bind_data_index"] = _data_networkindex.network_list;
        setGridData(GvMaster, ViewState["bind_data_index"]);

    }

    protected void SelectViewIndex()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        data_network _data_networkview = new data_network();
        _data_networkview.bindnetwork_list = new bindnetwork_detail[1];

        bindnetwork_detail _bindnetwork_detailview = new bindnetwork_detail();

        _bindnetwork_detailview.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_index"].ToString());

        _data_networkview.bindnetwork_list[0] = _bindnetwork_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networkview = callServiceDataNetwork(_urlGetNetworkView, _data_networkview);
     
        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networkview.bindnetwork_list);

    }

    protected void SelectViewEditIndex()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        data_network _data_networkedit = new data_network();
        _data_networkedit.editnetwork_list = new editnetwork_detail[1];

        editnetwork_detail _editnetwork_detail = new editnetwork_detail();

        _editnetwork_detail.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_index"].ToString());

        _data_networkedit.editnetwork_list[0] = _editnetwork_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networkedit = callServiceDataNetwork(_urlGetEdit, _data_networkedit);


        /////////////////////////// Show ////////////////////////////////

        //place_idxedit
        ViewState["place_idxedit"] = _data_networkedit.editnetwork_list[0].place_idx;


        //place_nameedit
        if (_data_networkedit.editnetwork_list[0].place_nameedit != null)
        {
            ViewState["place_nameedit"] = _data_networkedit.editnetwork_list[0].place_nameedit;
        }
        else
        {
            ViewState["place_nameedit"] = "-";
        }

        //room_idxedit
        ViewState["room_idxedit"] = _data_networkedit.editnetwork_list[0].room_idx;


        //room_nameedit
        if (_data_networkedit.editnetwork_list[0].room_name != null)
        {
            ViewState["room_nameedit"] = _data_networkedit.editnetwork_list[0].room_name;
        }
        else
        {
            ViewState["room_nameedit"] = "-";
        }

        //brand_nameedit
        if(_data_networkedit.editnetwork_list[0].brand_name != null)
        {
            ViewState["brand_nameedit"] = _data_networkedit.editnetwork_list[0].brand_name;
        }
        else
        {
            ViewState["brand_nameedit"] = "-";
        }
        

        //generation_nameedit
        if(_data_networkedit.editnetwork_list[0].generation_name != null)
        {
            ViewState["generation_nameedit"] = _data_networkedit.editnetwork_list[0].generation_name;
        }
        else
        {
            ViewState["generation_nameedit"] = "-";
        }

        //type_idxedit
        ViewState["type_idxedit"] = _data_networkedit.editnetwork_list[0].type_idx;

        //type_nameedit
        if (_data_networkedit.editnetwork_list[0].type_name != null)
        {
            ViewState["type_nameedit"] = _data_networkedit.editnetwork_list[0].type_name;
        }
        else
        {
            ViewState["type_nameedit"] = "-";
        }

        //category_idxedit
        ViewState["category_idxedit"] = _data_networkedit.editnetwork_list[0].category_idx;

        //category_namedit
        if (_data_networkedit.editnetwork_list[0].category_name != null)
        {
            ViewState["category_namedit"] = _data_networkedit.editnetwork_list[0].category_name;
        }
        else
        {
            ViewState["category_namedit"] = "-";
        }

        //ip_address
        if(_data_networkedit.editnetwork_list[0].ip_address != null)
        {
            ViewState["ip_addressdit"] = _data_networkedit.editnetwork_list[0].ip_address;
        }
        else
        {
            ViewState["ip_addressdit"] = "-";
        }

        //serial_number
        if(_data_networkedit.editnetwork_list[0].serial_number != null)
        {
            ViewState["serial_numberedit"] = _data_networkedit.editnetwork_list[0].serial_number;
        }
        else
        {
            ViewState["serial_numberedit"] = "-";
        }
        
        //////date_purchase
        ////ViewState["date_purchaseedit"] = _data_networkedit.editnetwork_list[0].date_purchase;

        //////date_expire
        ////ViewState["date_expireedit"] = _data_networkedit.editnetwork_list[0].date_expire;
       
        //user_name
        if(_data_networkedit.editnetwork_list[0].user_name != null)
        {
            ViewState["user_nameedit"] = _data_networkedit.editnetwork_list[0].user_name;
        }
        else
        {
            ViewState["user_nameedit"] = "-";
        }
        

        //pass_word
        if(_data_networkedit.editnetwork_list[0].pass_word != null)
        {
            ViewState["pass_wordeditget"] = _data_networkedit.editnetwork_list[0].pass_word;

            string passeditshow = _funcTool.getDecryptRC4(ViewState["pass_wordeditget"].ToString(), _keynetworkdevices);

            ViewState["pass_wordedit"] = passeditshow;
        }
        else
        {
            ViewState["pass_wordedit"] = "-";
        }
        

        //asset_code
        ViewState["asset_codeedit"] = _data_networkedit.editnetwork_list[0].asset_code;

        //status_deviecs
        if(_data_networkedit.editnetwork_list[0].status_deviecs == 1)
        {
            ViewState["status_deviecsedit"] = "Online" ;
        }
        {
            ViewState["status_deviecsedit"] = "Offline";
        }
        

        /////////////////////////// Show ////////////////////////////////

    }

    protected void SelectEditViewIndex()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        data_network _data_networkview = new data_network();
        _data_networkview.bindnetwork_list = new bindnetwork_detail[1];

        bindnetwork_detail _bindnetwork_detailview = new bindnetwork_detail();

        _bindnetwork_detailview.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_edit"].ToString());

        _data_networkview.bindnetwork_list[0] = _bindnetwork_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networkview = callServiceDataNetwork(_urlGetNetworkView, _data_networkview);


        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networkview.bindnetwork_list);



    }

    protected void SelectEdit()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        data_network _data_networkview = new data_network();
        _data_networkview.bindnetwork_list = new bindnetwork_detail[1];

        bindnetwork_detail _bindnetwork_detailview = new bindnetwork_detail();

        _bindnetwork_detailview.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_edit"].ToString());

        _data_networkview.bindnetwork_list[0] = _bindnetwork_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networkview = callServiceDataNetwork(_urlGetNetworkView, _data_networkview);


        setFormData(FvViewDetail, FormViewMode.Edit, _data_networkview.bindnetwork_list);



    }

    protected void SelectLog()
    {
       
        data_network _data_networklog = new data_network();
        _data_networklog.lognetwork_list = new lognetwork_detail[1];

        lognetwork_detail _lognetwork_detaillog = new lognetwork_detail();

        _lognetwork_detaillog.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_index"].ToString());

        _data_networklog.lognetwork_list[0] = _lognetwork_detaillog;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networklog = callServiceDataNetwork(_urlGetLogNetwork, _data_networklog);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLog.DataSource = _data_networklog.lognetwork_list;
        rptLog.DataBind();



    }

    protected void SelectLogEdit()
    {

        data_network _data_networklog = new data_network();
        _data_networklog.lognetwork_list = new lognetwork_detail[1];

        lognetwork_detail _lognetwork_detaillog = new lognetwork_detail();

        _lognetwork_detaillog.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_edit"].ToString());

        _data_networklog.lognetwork_list[0] = _lognetwork_detaillog;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_networklog = callServiceDataNetwork(_urlGetLogNetwork, _data_networklog);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLog.DataSource = _data_networklog.lognetwork_list;
        rptLog.DataBind();



    }

    protected void SelectLogRepair()
    {

        data_network _data_networklog = new data_network();
        _data_networklog.logrepairnetwork_list = new logrepairnetwork_detail[1];

        logrepairnetwork_detail _lognetwork_detaillogrepair = new logrepairnetwork_detail();

        _lognetwork_detaillogrepair.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_repair"].ToString());

        _data_networklog.logrepairnetwork_list[0] = _lognetwork_detaillogrepair;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networklog));

        _data_networklog = callServiceDataNetwork(_urlGetLogRepair, _data_networklog);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogRepair.DataSource = _data_networklog.logrepairnetwork_list;
        rptLogRepair.DataBind();



    }

    protected void SelectLogMA()
    {

        data_network _data_networklogma = new data_network();
        _data_networklogma.logmanetwork_list = new logmanetwork_detail[1];

        logmanetwork_detail _lognetwork_detaillogma = new logmanetwork_detail();

        _lognetwork_detaillogma.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_ma"].ToString());

        _data_networklogma.logmanetwork_list[0] = _lognetwork_detaillogma;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networklog));

        _data_networklogma = callServiceDataNetwork(_urlGetLogMA, _data_networklogma);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogMA.DataSource = _data_networklogma.logmanetwork_list;
        rptLogMA.DataBind();



    }

    protected void SelectLogRepairCut()
    {

        data_network _data_networklogcut = new data_network();
        _data_networklogcut.logrepaircutnetwork_list = new logrepaircutnetwork_detail[1];

        logrepaircutnetwork_detail _detaillogrepaircut = new logrepaircutnetwork_detail();

        _detaillogrepaircut.u0_devicenetwork_idx = int.Parse(ViewState["_ddlregister_numbercut"].ToString());//int.Parse(ViewState["ddlregister_numbercut"]);

        _data_networklogcut.logrepaircutnetwork_list[0] = _detaillogrepaircut;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networklogcut));

        _data_networklogcut = callServiceDataNetwork(_urlGetLogRepairCut, _data_networklogcut);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogRepairCut.DataSource = _data_networklogcut.logrepaircutnetwork_list;
        rptLogRepairCut.DataBind();



    }

    protected void SelectLogMACut()
    {

        data_network _data_networklogmacut = new data_network();
        _data_networklogmacut.logmacutnetwork_list = new logmacutnetwork_detail[1];

        logmacutnetwork_detail _lognetwork_detaillogmacut = new logmacutnetwork_detail();

        _lognetwork_detaillogmacut.u0_devicenetwork_idx = int.Parse(ViewState["_ddlregister_numbercut"].ToString());

        _data_networklogmacut.logmacutnetwork_list[0] = _lognetwork_detaillogmacut;

        

        _data_networklogmacut = callServiceDataNetwork(_urlGetLogMACut, _data_networklogmacut);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networklogmacut));


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogMACut.DataSource = _data_networklogmacut.logmacutnetwork_list;
        rptLogMACut.DataBind();



    }

    protected void actionddltype()
    {
        
        FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");
        DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");

        ddltypeidxadd.Items.Clear();
        ddltypeidxadd.AppendDataBoundItems = true;
        ddltypeidxadd.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        data_networkdevices datanetworkdevices = new data_networkdevices();
        datanetworkdevices.m0category_list = new m0category_detail[1];
        m0category_detail _m0categoryDetail = new m0category_detail();

        datanetworkdevices.m0category_list[0] = _m0categoryDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        datanetworkdevices = callServiceNetwork(_urlGetddltype, datanetworkdevices);

        ddltypeidxadd.DataSource = datanetworkdevices.m0category_list;
        ddltypeidxadd.DataTextField = "type_name";
        ddltypeidxadd.DataValueField = "type_idx";
        ddltypeidxadd.DataBind();

       

    }

    protected void actionddlcompany()
    {
        //View ViewNetworkDevices = (View)FindControl("ViewNetworkDevices");
        FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");
        DropDownList ddlcompanyidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcompanyidxadd");

        ddlcompanyidxadd.Items.Clear();
        ddlcompanyidxadd.AppendDataBoundItems = true;
        ddlcompanyidxadd.Items.Add(new ListItem("กรุณาเลือกบริษัทที่ต่อประกัน...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0company_list = new m0company_detail[1];

        m0company_detail _m0company_detailindex = new m0company_detail();

        _m0company_detailindex.company_idx = 0;

        _data_networkdevicesindex.m0company_list[0] = _m0company_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevicesindex));

        //_data_networkdevicesindex = callServiceNetwork(_urlGetm0Company, _data_networkdevicesindex);

        _data_networkdevicesindex = callServiceNetwork(_urlGetddlCompany, _data_networkdevicesindex);

        ddlcompanyidxadd.DataSource = _data_networkdevicesindex.m0company_list;
        ddlcompanyidxadd.DataTextField = "company_name";
        ddlcompanyidxadd.DataValueField = "company_idx";
        ddlcompanyidxadd.DataBind();


    }

    protected void actionddlcompanyma()
    {
        //View ViewNetworkDevices = (View)FindControl("ViewNetworkDevices");
        FormView FvViewMA = (FormView)ViewIndex.FindControl("FvViewMA");
        DropDownList ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");

        ddlcompanyidxma.Items.Clear();
        ddlcompanyidxma.AppendDataBoundItems = true;
        ddlcompanyidxma.Items.Add(new ListItem("กรุณาเลือกบริษัทที่ต่อประกันใหม่ ...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0company_list = new m0company_detail[1];

        m0company_detail _m0company_detailindex = new m0company_detail();

        _m0company_detailindex.company_idx = 0;

        _data_networkdevicesindex.m0company_list[0] = _m0company_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        //_data_networkdevicesindex = callServiceNetwork(_urlGetm0Company, _data_networkdevicesindex);

        _data_networkdevicesindex = callServiceNetwork(_urlGetddlCompany, _data_networkdevicesindex);


        ddlcompanyidxma.DataSource = _data_networkdevicesindex.m0company_list;
        ddlcompanyidxma.DataTextField = "company_name";
        ddlcompanyidxma.DataValueField = "company_idx";
        ddlcompanyidxma.DataBind();


    }

    protected void actionddlplace()
    {
       
        FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");
        DropDownList ddlplaceadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlplaceadd");

        ddlplaceadd.Items.Clear();
        ddlplaceadd.AppendDataBoundItems = true;
        ddlplaceadd.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0place_list = new m0place_detail[1];

        m0place_detail _m0place_detailindex = new m0place_detail();

        _m0place_detailindex.place_idx = 0;

        _data_networkdevicesindex.m0place_list[0] = _m0place_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevicesindex);

        ddlplaceadd.DataSource = _data_networkdevices.m0place_list;
        ddlplaceadd.DataTextField = "place_name";
        ddlplaceadd.DataValueField = "place_idx";
        ddlplaceadd.DataBind();

        

    }

    protected void actionddlplacemove()
    {

        //FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");
        DropDownList ddlplacemove = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlplacemove");

        ddlplacemove.Items.Clear();
        ddlplacemove.AppendDataBoundItems = true;
        ddlplacemove.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง...", "0"));

        data_networkdevices _data_networkdevicesmove = new data_networkdevices();
        _data_networkdevicesmove.m0place_list = new m0place_detail[1];

        m0place_detail _m0place_detailmove = new m0place_detail();

        _m0place_detailmove.place_idx = 0;

        _data_networkdevicesmove.m0place_list[0] = _m0place_detailmove;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevicesmove);

        ddlplacemove.DataSource = _data_networkdevices.m0place_list;
        ddlplacemove.DataTextField = "place_name";
        ddlplacemove.DataValueField = "place_idx";
        ddlplacemove.DataBind();



    }

    #endregion selected 

    #region search   
    protected void actionddltypesearch()
    {

        DropDownList ddltypeidxsearch = (DropDownList)ViewIndex.FindControl("ddltypeidxsearch");
        // DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");

        ddltypeidxsearch.Items.Clear();
        ddltypeidxsearch.AppendDataBoundItems = true;
        ddltypeidxsearch.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        _data_networkdevices.m0category_list = new m0category_detail[1];
        m0category_detail _m0categoryDetail = new m0category_detail();

        _data_networkdevices.m0category_list[0] = _m0categoryDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

        ddltypeidxsearch.DataSource = _data_networkdevices.m0category_list;
        ddltypeidxsearch.DataTextField = "type_name";
        ddltypeidxsearch.DataValueField = "type_idx";
        ddltypeidxsearch.DataBind();


    }

    protected void actionddlplacesearch()
    {
        
        DropDownList ddlplacesearch = (DropDownList)ViewIndex.FindControl("ddlplacesearch");

        ddlplacesearch.Items.Clear();
        ddlplacesearch.AppendDataBoundItems = true;
        ddlplacesearch.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0place_list = new m0place_detail[1];

        m0place_detail _m0place_detailindex = new m0place_detail();

        _m0place_detailindex.place_idx = 0;

        _data_networkdevicesindex.m0place_list[0] = _m0place_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevicesindex);

        ddlplacesearch.DataSource = _data_networkdevices.m0place_list;
        ddlplacesearch.DataTextField = "place_name";
        ddlplacesearch.DataValueField = "place_idx";
        ddlplacesearch.DataBind();


    }

    protected void actionddlcompanysearch()
    {
       
        DropDownList ddlcompanyidxsearch = (DropDownList)ViewIndex.FindControl("ddlcompanyidxsearch");

        ddlcompanyidxsearch.Items.Clear();
        ddlcompanyidxsearch.AppendDataBoundItems = true;
        ddlcompanyidxsearch.Items.Add(new ListItem("กรุณาเลือกบริษัทที่ต่อประกัน...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0company_list = new m0company_detail[1];

        m0company_detail _m0company_detailindex = new m0company_detail();

        _m0company_detailindex.company_idx = 0;

        _data_networkdevicesindex.m0company_list[0] = _m0company_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices)); 

        //_data_networkdevicesindex = callServiceNetwork(_urlGetm0Company, _data_networkdevicesindex);
        _data_networkdevicesindex = callServiceNetwork(_urlGetddlCompany, _data_networkdevicesindex);

        ddlcompanyidxsearch.DataSource = _data_networkdevicesindex.m0company_list;
        ddlcompanyidxsearch.DataTextField = "company_name";
        ddlcompanyidxsearch.DataValueField = "company_idx";
        ddlcompanyidxsearch.DataBind();


    }

    protected void actionempsearch()
    {

        DropDownList ddlnamesearch = (DropDownList)ViewIndex.FindControl("ddlnamesearch");

        ddlnamesearch.Items.Clear();
        ddlnamesearch.AppendDataBoundItems = true;
        ddlnamesearch.Items.Add(new ListItem("กรุณาเลือกชื่อผู้รับผิดชอบ ...", "0"));

        _data_network.empinsert_list = new empinsert_detail[1];
        empinsert_detail _empDetailinsert = new empinsert_detail();

       // _empDetailinsert.emp_idx = emp_idx;

        _data_network.empinsert_list[0] = _empDetailinsert;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_network));
        _data_network = callServiceDataNetwork(_urlGetempsearch, _data_network);

        ddlnamesearch.DataSource = _data_network.empinsert_list;
        ddlnamesearch.DataTextField = "emp_name_th";
        ddlnamesearch.DataValueField = "emp_idx";
        ddlnamesearch.DataBind();


    }

    #endregion search 

    #region search report  
    protected void actionddltypesearchreport()
    {

        DropDownList ddltypeidxsearchreport = (DropDownList)ViewReport.FindControl("ddltypeidxsearchreport");
        // DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");

        ddltypeidxsearchreport.Items.Clear();
        ddltypeidxsearchreport.AppendDataBoundItems = true;
        ddltypeidxsearchreport.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

        _data_networkdevices.m0category_list = new m0category_detail[1];
        m0category_detail _m0categoryDetail = new m0category_detail();

        _data_networkdevices.m0category_list[0] = _m0categoryDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

        ddltypeidxsearchreport.DataSource = _data_networkdevices.m0category_list;
        ddltypeidxsearchreport.DataTextField = "type_name";
        ddltypeidxsearchreport.DataValueField = "type_idx";
        ddltypeidxsearchreport.DataBind();


    }

    protected void actionddlplacesearchreport()
    {

        DropDownList ddlplacesearchreport = (DropDownList)ViewReport.FindControl("ddlplacesearchreport");

        ddlplacesearchreport.Items.Clear();
        ddlplacesearchreport.AppendDataBoundItems = true;
        ddlplacesearchreport.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0place_list = new m0place_detail[1];

        m0place_detail _m0place_detailindex = new m0place_detail();

        _m0place_detailindex.place_idx = 0;

        _data_networkdevicesindex.m0place_list[0] = _m0place_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

        _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevicesindex);

        ddlplacesearchreport.DataSource = _data_networkdevices.m0place_list;
        ddlplacesearchreport.DataTextField = "place_name";
        ddlplacesearchreport.DataValueField = "place_idx";
        ddlplacesearchreport.DataBind();


    }

    protected void actionddlcompanysearchreport()
    {

        DropDownList ddlcompanyidxsearchreport = (DropDownList)ViewReport.FindControl("ddlcompanyidxsearchreport");

        ddlcompanyidxsearchreport.Items.Clear();
        ddlcompanyidxsearchreport.AppendDataBoundItems = true;
        ddlcompanyidxsearchreport.Items.Add(new ListItem("กรุณาเลือกบริษัทที่ต่อประกัน...", "0"));

        data_networkdevices _data_networkdevicesindex = new data_networkdevices();
        _data_networkdevicesindex.m0company_list = new m0company_detail[1];

        m0company_detail _m0company_detailindex = new m0company_detail();

        _m0company_detailindex.company_idx = 0;

        _data_networkdevicesindex.m0company_list[0] = _m0company_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices)); 

        //_data_networkdevicesindex = callServiceNetwork(_urlGetm0Company, _data_networkdevicesindex);
        _data_networkdevicesindex = callServiceNetwork(_urlGetddlCompany, _data_networkdevicesindex);

        ddlcompanyidxsearchreport.DataSource = _data_networkdevicesindex.m0company_list;
        ddlcompanyidxsearchreport.DataTextField = "company_name";
        ddlcompanyidxsearchreport.DataValueField = "company_idx";
        ddlcompanyidxsearchreport.DataBind();


    }

    protected void actionempsearchreport()
    {

        DropDownList ddlnamesearchreport = (DropDownList)ViewReport.FindControl("ddlnamesearchreport");

        ddlnamesearchreport.Items.Clear();
        ddlnamesearchreport.AppendDataBoundItems = true;
        ddlnamesearchreport.Items.Add(new ListItem("กรุณาเลือกชื่อผู้รับผิดชอบ ...", "0"));

        _data_network.empinsert_list = new empinsert_detail[1];
        empinsert_detail _empDetailinsert = new empinsert_detail();

        // _empDetailinsert.emp_idx = emp_idx;

        _data_network.empinsert_list[0] = _empDetailinsert;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_network));
        _data_network = callServiceDataNetwork(_urlGetempsearch, _data_network);

        ddlnamesearchreport.DataSource = _data_network.empinsert_list;
        ddlnamesearchreport.DataTextField = "emp_name_th";
        ddlnamesearchreport.DataValueField = "emp_idx";
        ddlnamesearchreport.DataBind();


    }

    #endregion search report

    #region search Cut Network
    protected void ddlsearchcut()
    {

        FormView FvCutNetworkDevices = (FormView)ViewNetworkDevicesCut.FindControl("FvCutNetworkDevices");
        DropDownList ddlregister_numbercut = (DropDownList)FvCutNetworkDevices.FindControl("ddlregister_numbercut");

        ddlregister_numbercut.Items.Clear();
        ddlregister_numbercut.AppendDataBoundItems = true;
        ddlregister_numbercut.Items.Add(new ListItem("กรุณาเลือกเลขทะเบียนอุปกรณ์ ...", "0"));

        _data_network.bindcutnetwork_list = new bindcutnetwork_detail[1];
        bindcutnetwork_detail _bindcutnetwork_detail = new bindcutnetwork_detail();

        _bindcutnetwork_detail.u0_devicenetwork_idx = 0;

        _data_network.bindcutnetwork_list[0] = _bindcutnetwork_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));
        _data_network = callServiceDataNetwork(_urlGetSearchCut, _data_network);

        ddlregister_numbercut.DataSource = _data_network.bindcutnetwork_list;
        ddlregister_numbercut.DataTextField = "register_number";
        ddlregister_numbercut.DataValueField = "u0_devicenetwork_idx";
        ddlregister_numbercut.DataBind();


    }
    protected void SearchDetailCut()
    {
        //GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        //Label u0_idx_view = (Label)GvMaster.FindControl("lbu0_idx");

        //FormView FvDetailShowCut = (FormView)ViewNetworkDevicesCut.FindControl("FvDetailShowCut");
        //Label litDebug1 = (Label)FvDetailShowCut.FindControl("litDebug1");
       // litDebug1.Text = "111111";

        data_network _data_networkdetailcut = new data_network();
        _data_networkdetailcut.bindcutnetwork_list = new bindcutnetwork_detail[1];

        bindcutnetwork_detail _bindcutnetwork_detailcut = new bindcutnetwork_detail();

        _bindcutnetwork_detailcut.u0_devicenetwork_idx = int.Parse(ViewState["_ddlregister_numbercut"].ToString());

        _data_networkdetailcut.bindcutnetwork_list[0] = _bindcutnetwork_detailcut;

       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdetailcut));
        _data_networkdetailcut = callServiceDataNetwork(_urlGetShowDetailCut, _data_networkdetailcut);


        if (_data_networkdetailcut.return_code == 0)
        {
            setFormData(FvDetailShowCut, FormViewMode.ReadOnly, _data_networkdetailcut.bindcutnetwork_list);
            FvDetailShowCut.Visible = true;
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเลขทะเบียนอุปกรณ์');", true);
            FvDetailShowCut.Visible = false;

            //setError(_data_networkdetailcut.return_code.ToString() + " - " + _data_networkdetailcut.return_msg);

        }

        

    }

    #endregion search Cut Network 

    #region search Move Network
    protected void ddlsearchmove()
    {

        FormView FvMoveNetworkDevices = (FormView)ViewNetworkDevicesMove.FindControl("FvMoveNetworkDevices");
        DropDownList ddlregister_numbermove = (DropDownList)FvMoveNetworkDevices.FindControl("ddlregister_numbermove");

        ddlregister_numbermove.Items.Clear();
        ddlregister_numbermove.AppendDataBoundItems = true;
        ddlregister_numbermove.Items.Add(new ListItem("กรุณาเลือกเลขทะเบียนอุปกรณ์ ...", "0"));

        _data_network.bindmovenetwork_list = new bindmovenetwork_detail[1];
        bindmovenetwork_detail _bindmovenetwork_detail = new bindmovenetwork_detail();

        _bindmovenetwork_detail.u0_devicenetwork_idx = 0;

        _data_network.bindmovenetwork_list[0] = _bindmovenetwork_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));
        _data_network = callServiceDataNetwork(_urlGetSearchMove, _data_network);

        ddlregister_numbermove.DataSource = _data_network.bindmovenetwork_list;
        ddlregister_numbermove.DataTextField = "register_number";
        ddlregister_numbermove.DataValueField = "u0_devicenetwork_idx";
        ddlregister_numbermove.DataBind();


    }

    protected void SearchDetailMove()
    {
       
        data_network _data_networkdetailmove = new data_network();
        _data_networkdetailmove.bindmovenetwork_list = new bindmovenetwork_detail[1];

        bindmovenetwork_detail _bindmovenetwork_detail = new bindmovenetwork_detail();

        _bindmovenetwork_detail.u0_devicenetwork_idx = int.Parse(ViewState["_ddlregister_numbermove"].ToString());

        _data_networkdetailmove.bindmovenetwork_list[0] = _bindmovenetwork_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdetailmove));
        _data_networkdetailmove = callServiceDataNetwork(_urlGetShowDetailMove, _data_networkdetailmove);


        if (_data_networkdetailmove.return_code == 0)
        {
            setFormData(FvDetailShowMove, FormViewMode.ReadOnly, _data_networkdetailmove.bindmovenetwork_list);
            FvDetailShowMove.Visible = true;
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเลขทะเบียนอุปกรณ์');", true);
            FvDetailShowMove.Visible = false;

            //setError(_data_networkdetailcut.return_code.ToString() + " - " + _data_networkdetailcut.return_msg);

        }



    }

    #endregion search Move Network 

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
     
        var ddName = (DropDownList)sender;
            
        switch (ddName.ID)
        {
   
            case "ddltypeidxadd":


                DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");
                DropDownList ddlcategoryidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcategoryidxadd");
              
                if (ddltypeidxadd.SelectedValue == "0")
                {
                    
                    ddltypeidxadd.Items.Clear();
                   
                    ddltypeidxadd.AppendDataBoundItems = true;
                    ddltypeidxadd.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์....", "0"));

                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

                

                    ddltypeidxadd.DataSource = _data_networkdevices.m0category_list;
                    ddltypeidxadd.DataTextField = "type_name";
                    ddltypeidxadd.DataValueField = "type_idx";
                    ddltypeidxadd.DataBind();
                    ddltypeidxadd.SelectedValue = "0";

                    ddlcategoryidxadd.Items.Clear();
                    ddlcategoryidxadd.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์...", "0"));
                    ddlcategoryidxadd.SelectedValue = "0";

                    
                }
                else
                {
                 
                    ddlcategoryidxadd.AppendDataBoundItems = true;
                    ddlcategoryidxadd.Items.Clear();

                    ddlcategoryidxadd.Items.Add(new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));

                   
                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _m0categoryDetail.type_idx = int.Parse(ddltypeidxadd.SelectedValue);

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlcatenetwork, _data_networkdevices);


                    ddlcategoryidxadd.DataSource = _data_networkdevices.m0category_list;
                    ddlcategoryidxadd.DataTextField = "category_name";
                    ddlcategoryidxadd.DataValueField = "category_idx";
                    ddlcategoryidxadd.DataBind();

                  
                }

                break;

            case "ddlplaceadd":
              
                DropDownList ddlplaceadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlplaceadd");
                DropDownList ddlroomadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlroomadd");

                if (ddlplaceadd.SelectedValue == "0")
                {

                    //MvMaster.SetActiveView(ViewNetworkDevices);

                    ddlplaceadd.Items.Clear();
                    
                    ddlplaceadd.AppendDataBoundItems = true;
                    ddlplaceadd.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));

                   // data_networkdevices _data_networkdevicesindex = new data_networkdevices();
                    _data_networkdevices.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detail = new m0place_detail();

                    _m0place_detail.place_idx = 0;

                    _data_networkdevices.m0place_list[0] = _m0place_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevices);

               
                    ddlplaceadd.DataSource = _data_networkdevices.m0place_list;
                    ddlplaceadd.DataTextField = "place_name";
                    ddlplaceadd.DataValueField = "place_idx";
                    ddlplaceadd.DataBind();
                    ddlplaceadd.SelectedValue = "0";

                    ddlroomadd.Items.Clear();
                    ddlroomadd.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));
                    ddlroomadd.SelectedValue = "0";

                   
                }
                else
                {
                 
                    ddlroomadd.AppendDataBoundItems = true;
                    ddlroomadd.Items.Clear();

                  
                    ddlroomadd.Items.Add(new ListItem("กรุณาเลือกอาคาร....", "0"));

                    _data_networkdevices.m0room_list = new m0room_detail[1];
                    m0room_detail _m0roomDetail = new m0room_detail();

                    _m0roomDetail.place_idx = int.Parse(ddlplaceadd.SelectedValue);

                    _data_networkdevices.m0room_list[0] = _m0roomDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlroomnetwork, _data_networkdevices);


                    ddlroomadd.DataSource = _data_networkdevices.m0room_list;
                    ddlroomadd.DataTextField = "room_name";
                    ddlroomadd.DataValueField = "room_idx";
                    ddlroomadd.DataBind();

                   
                }

                break;

            case "ddltypeidxsearch":

                DropDownList ddltypeidxsearch = (DropDownList)ViewIndex.FindControl("ddltypeidxsearch");
                DropDownList ddlcategoryidxsearch = (DropDownList)ViewIndex.FindControl("ddlcategoryidxsearch");

                if (ddltypeidxsearch.SelectedValue == "0")
                {
                    ddltypeidxsearch.Items.Clear();

                    ddltypeidxsearch.AppendDataBoundItems = true;
                    ddltypeidxsearch.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์....", "0"));

                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

                    ddltypeidxsearch.DataSource = _data_networkdevices.m0category_list;
                    ddltypeidxsearch.DataTextField = "type_name";
                    ddltypeidxsearch.DataValueField = "type_idx";
                    ddltypeidxsearch.DataBind();
                    ddltypeidxsearch.SelectedValue = "0";

                    ddlcategoryidxsearch.Items.Clear();
                    ddlcategoryidxsearch.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์...", "0"));
                    ddlcategoryidxsearch.SelectedValue = "0";

                }
                else
                {
                    ddlcategoryidxsearch.AppendDataBoundItems = true;
                    ddlcategoryidxsearch.Items.Clear();

                    ddlcategoryidxsearch.Items.Add(new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));


                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _m0categoryDetail.type_idx = int.Parse(ddltypeidxsearch.SelectedValue);

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlcatenetwork, _data_networkdevices);

                    ddlcategoryidxsearch.DataSource = _data_networkdevices.m0category_list;
                    ddlcategoryidxsearch.DataTextField = "category_name";
                    ddlcategoryidxsearch.DataValueField = "category_idx";
                    ddlcategoryidxsearch.DataBind();


                }

                break;

            case "ddlplacesearch":


                DropDownList ddlplacesearch = (DropDownList)ViewIndex.FindControl("ddlplacesearch");
                DropDownList ddlroomsearch = (DropDownList)ViewIndex.FindControl("ddlroomsearch");

                if (ddlplacesearch.SelectedValue == "0")
                {
                    ddlplacesearch.Items.Clear();

                    ddlplacesearch.AppendDataBoundItems = true;
                    ddlplacesearch.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));

                    // data_networkdevices _data_networkdevicesindex = new data_networkdevices();
                    _data_networkdevices.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detail = new m0place_detail();

                    _m0place_detail.place_idx = 0;

                    _data_networkdevices.m0place_list[0] = _m0place_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevices);

                    ddlplacesearch.DataSource = _data_networkdevices.m0place_list;
                    ddlplacesearch.DataTextField = "place_name";
                    ddlplacesearch.DataValueField = "place_idx";
                    ddlplacesearch.DataBind();
                    ddlplacesearch.SelectedValue = "0";

                    ddlroomsearch.Items.Clear();
                    ddlroomsearch.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));
                    ddlroomsearch.SelectedValue = "0";

                }
                else
                {
                    ddlroomsearch.AppendDataBoundItems = true;
                    ddlroomsearch.Items.Clear();

                    ddlroomsearch.Items.Add(new ListItem("กรุณาเลือกอาคาร....", "0"));

                    _data_networkdevices.m0room_list = new m0room_detail[1];
                    m0room_detail _m0roomDetail = new m0room_detail();

                    _m0roomDetail.place_idx = int.Parse(ddlplacesearch.SelectedValue);

                    _data_networkdevices.m0room_list[0] = _m0roomDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlroomnetwork, _data_networkdevices);

                    ddlroomsearch.DataSource = _data_networkdevices.m0room_list;
                    ddlroomsearch.DataTextField = "room_name";
                    ddlroomsearch.DataValueField = "room_idx";
                    ddlroomsearch.DataBind();


                }

                break;

            case "ddltype_nameedit":


                DropDownList ddltype_nameedit = (DropDownList)FvViewDetail.FindControl("ddltype_nameedit");
                DropDownList ddlcategory_nameedit = (DropDownList)FvViewDetail.FindControl("ddlcategory_nameedit");

                if (ddltype_nameedit.SelectedValue == "0")
                {

                    ddltype_nameedit.Items.Clear();

                    ddltype_nameedit.AppendDataBoundItems = true;
                    ddltype_nameedit.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์....", "0"));

                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);



                    ddltype_nameedit.DataSource = _data_networkdevices.m0category_list;
                    ddltype_nameedit.DataTextField = "type_name";
                    ddltype_nameedit.DataValueField = "type_idx";
                    ddltype_nameedit.DataBind();
                    ddltype_nameedit.SelectedValue = "0";

                    ddlcategory_nameedit.Items.Clear();
                    ddlcategory_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์...", "0"));
                    ddlcategory_nameedit.SelectedValue = "0";


                }
                else
                {

                    ddlcategory_nameedit.Enabled = true;

                    ddlcategory_nameedit.AppendDataBoundItems = true;
                    ddlcategory_nameedit.Items.Clear();

                    ddlcategory_nameedit.Items.Add(new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));


                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _m0categoryDetail.type_idx = int.Parse(ddltype_nameedit.SelectedValue);

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlcatenetwork, _data_networkdevices);


                    ddlcategory_nameedit.DataSource = _data_networkdevices.m0category_list;
                    ddlcategory_nameedit.DataTextField = "category_name";
                    ddlcategory_nameedit.DataValueField = "category_idx";
                    ddlcategory_nameedit.DataBind();


                }

                break;

            case "ddltxtplace_nameedit":


                DropDownList ddltxtplace_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtplace_nameedit");
                DropDownList ddltxtroom_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtroom_nameedit");

                if (ddltxtplace_nameedit.SelectedValue == "0")
                {

                    ddltxtplace_nameedit.Items.Clear();

                    ddltxtplace_nameedit.AppendDataBoundItems = true;
                    ddltxtplace_nameedit.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));

                    // data_networkdevices _data_networkdevicesindex = new data_networkdevices();
                    _data_networkdevices.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detail = new m0place_detail();

                    _m0place_detail.place_idx = 0;

                    _data_networkdevices.m0place_list[0] = _m0place_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevices);


                    ddltxtplace_nameedit.DataSource = _data_networkdevices.m0place_list;
                    ddltxtplace_nameedit.DataTextField = "place_name";
                    ddltxtplace_nameedit.DataValueField = "place_idx";
                    ddltxtplace_nameedit.DataBind();
                    ddltxtplace_nameedit.SelectedValue = "0";

                    ddltxtroom_nameedit.Items.Clear();
                    ddltxtroom_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));
                    ddltxtroom_nameedit.SelectedValue = "0";


                }
                else
                {

                    ddltxtroom_nameedit.Enabled = true;

                    ddltxtroom_nameedit.AppendDataBoundItems = true;
                    ddltxtroom_nameedit.Items.Clear();


                    ddltxtroom_nameedit.Items.Add(new ListItem("กรุณาเลือกอาคาร....", "0"));

                    _data_networkdevices.m0room_list = new m0room_detail[1];
                    m0room_detail _m0roomDetail = new m0room_detail();

                    _m0roomDetail.place_idx = int.Parse(ddltxtplace_nameedit.SelectedValue);

                    _data_networkdevices.m0room_list[0] = _m0roomDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlroomnetwork, _data_networkdevices);


                    ddltxtroom_nameedit.DataSource = _data_networkdevices.m0room_list;
                    ddltxtroom_nameedit.DataTextField = "room_name";
                    ddltxtroom_nameedit.DataValueField = "room_idx";
                    ddltxtroom_nameedit.DataBind();


                }

                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    txtdateexpirsearch.Enabled = true;

                }
                else
                {
                    txtdateexpirsearch.Enabled = false;
                    txtdateexpirsearch.Text = string.Empty;
                }


                break;

            case "ddlplacemove":

                DropDownList ddlplacemove = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlplacemove");
                DropDownList ddlroommove = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlroommove");

                if (ddlplacemove.SelectedValue == "0")
                {

                    //MvMaster.SetActiveView(ViewNetworkDevices);

                    ddlplacemove.Items.Clear();

                    ddlplacemove.AppendDataBoundItems = true;
                    ddlplacemove.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));

                    // data_networkdevices _data_networkdevicesindex = new data_networkdevices();
                    _data_networkdevices.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detail = new m0place_detail();

                    _m0place_detail.place_idx = 0;

                    _data_networkdevices.m0place_list[0] = _m0place_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevices);


                    ddlplacemove.DataSource = _data_networkdevices.m0place_list;
                    ddlplacemove.DataTextField = "place_name";
                    ddlplacemove.DataValueField = "place_idx";
                    ddlplacemove.DataBind();
                    ddlplacemove.SelectedValue = "0";

                    ddlroommove.Items.Clear();
                    ddlroommove.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));
                    ddlroommove.SelectedValue = "0";


                }
                else
                {

                    ddlroommove.AppendDataBoundItems = true;
                    ddlroommove.Items.Clear();


                    ddlroommove.Items.Add(new ListItem("กรุณาเลือกอาคาร....", "0"));

                    _data_networkdevices.m0room_list = new m0room_detail[1];
                    m0room_detail _m0roomDetail = new m0room_detail();

                    _m0roomDetail.place_idx = int.Parse(ddlplacemove.SelectedValue);

                    _data_networkdevices.m0room_list[0] = _m0roomDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlroomnetwork, _data_networkdevices);


                    ddlroommove.DataSource = _data_networkdevices.m0room_list;
                    ddlroommove.DataTextField = "room_name";
                    ddlroommove.DataValueField = "room_idx";
                    ddlroommove.DataBind();


                }

                break;

            case "ddltypeidxsearchreport":

                DropDownList ddltypeidxsearchreport = (DropDownList)ViewReport.FindControl("ddltypeidxsearchreport");
                DropDownList ddlcategoryidxsearchreport = (DropDownList)ViewReport.FindControl("ddlcategoryidxsearchreport");

                if (ddltypeidxsearchreport.SelectedValue == "0")
                {
                    ddltypeidxsearchreport.Items.Clear();

                    ddltypeidxsearchreport.AppendDataBoundItems = true;
                    ddltypeidxsearchreport.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์....", "0"));

                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

                    ddltypeidxsearchreport.DataSource = _data_networkdevices.m0category_list;
                    ddltypeidxsearchreport.DataTextField = "type_name";
                    ddltypeidxsearchreport.DataValueField = "type_idx";
                    ddltypeidxsearchreport.DataBind();
                    ddltypeidxsearchreport.SelectedValue = "0";

                    ddlcategoryidxsearchreport.Items.Clear();
                    ddlcategoryidxsearchreport.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์...", "0"));
                    ddlcategoryidxsearchreport.SelectedValue = "0";

                }
                else
                {
                    ddlcategoryidxsearchreport.AppendDataBoundItems = true;
                    ddlcategoryidxsearchreport.Items.Clear();

                    ddlcategoryidxsearchreport.Items.Add(new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));


                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _m0categoryDetail.type_idx = int.Parse(ddltypeidxsearchreport.SelectedValue);

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlcatenetwork, _data_networkdevices);

                    ddlcategoryidxsearchreport.DataSource = _data_networkdevices.m0category_list;
                    ddlcategoryidxsearchreport.DataTextField = "category_name";
                    ddlcategoryidxsearchreport.DataValueField = "category_idx";
                    ddlcategoryidxsearchreport.DataBind();


                }

                break;

            case "ddlplacesearchreport":


                DropDownList ddlplacesearchreport = (DropDownList)ViewReport.FindControl("ddlplacesearchreport");
                DropDownList ddlroomsearchreport = (DropDownList)ViewReport.FindControl("ddlroomsearchreport");

                if (ddlplacesearchreport.SelectedValue == "0")
                {
                    ddlplacesearchreport.Items.Clear();

                    ddlplacesearchreport.AppendDataBoundItems = true;
                    ddlplacesearchreport.Items.Add(new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));

                    // data_networkdevices _data_networkdevicesindex = new data_networkdevices();
                    _data_networkdevices.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detail = new m0place_detail();

                    _m0place_detail.place_idx = 0;

                    _data_networkdevices.m0place_list[0] = _m0place_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevices);

                    ddlplacesearchreport.DataSource = _data_networkdevices.m0place_list;
                    ddlplacesearchreport.DataTextField = "place_name";
                    ddlplacesearchreport.DataValueField = "place_idx";
                    ddlplacesearchreport.DataBind();
                    ddlplacesearchreport.SelectedValue = "0";

                    ddlroomsearchreport.Items.Clear();
                    ddlroomsearchreport.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร...", "0"));
                    ddlroomsearchreport.SelectedValue = "0";

                }
                else
                {
                    ddlroomsearchreport.AppendDataBoundItems = true;
                    ddlroomsearchreport.Items.Clear();

                    ddlroomsearchreport.Items.Add(new ListItem("กรุณาเลือกอาคาร....", "0"));

                    _data_networkdevices.m0room_list = new m0room_detail[1];
                    m0room_detail _m0roomDetail = new m0room_detail();

                    _m0roomDetail.place_idx = int.Parse(ddlplacesearchreport.SelectedValue);

                    _data_networkdevices.m0room_list[0] = _m0roomDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddlroomnetwork, _data_networkdevices);

                    ddlroomsearchreport.DataSource = _data_networkdevices.m0room_list;
                    ddlroomsearchreport.DataTextField = "room_name";
                    ddlroomsearchreport.DataValueField = "room_idx";
                    ddlroomsearchreport.DataBind();


                }

                break;

            case "ddlSearchDatereport":

                if (int.Parse(ddlSearchDatereport.SelectedValue) == 3)
                {
                    txtdateexpirsearchreport.Enabled = true;

                }
                else
                {
                    txtdateexpirsearchreport.Enabled = false;
                    txtdateexpirsearchreport.Text = string.Empty;
                }


                break;

        }
    }

    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //int _category_idx;
        //string _category_name;
        int _cemp_idx;

        // *** value insert ***///
        // *** value insert ***///

        empinsert_detail _empinsert_detail = new empinsert_detail();
        network_detail _network_detail = new network_detail();
        repairnetwork_detail _repairnetwork_detail = new repairnetwork_detail();
        manetwork_detail _manetwork_detail = new manetwork_detail();
        bindnetwork_detail _bindnetwork_detail = new bindnetwork_detail();
        bindcutnetwork_detail _bindcutnetwork_detail = new bindcutnetwork_detail();
        cutnetwork_detail _cutnetwork_detail = new cutnetwork_detail();
        movenetwork_detail _movenetwork_detail = new movenetwork_detail();
        exportnetwork_detail _exportnetwork_detail = new exportnetwork_detail();
        inexportnetwork_detail _inexportnetwork_detail = new inexportnetwork_detail();

        switch (cmdName)
        {

            case "btnToIndexList":

                btnToIndexList.BackColor = System.Drawing.Color.LightGray;
                btnToDeviceNetwork.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkMove.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkCut.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewIndex);

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnToDeviceNetwork":

              
                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetwork.BackColor = System.Drawing.Color.LightGray;
                btnToDeviceNetworkMove.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkCut.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewNetworkDevices);
              
                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();

                FvAddNetworkDevices.ChangeMode(FormViewMode.Insert);
                FvAddNetworkDevices.DataBind();

                //linkBtnTrigger(btnInsert);

                break;

            case "btnToDeviceNetworkMove":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetwork.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkMove.BackColor = System.Drawing.Color.LightGray;
                btnToDeviceNetworkCut.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewNetworkDevicesMove);

                FvMoveNetworkDevices.ChangeMode(FormViewMode.Insert);
                FvMoveNetworkDevices.DataBind();


                ////////////////////////////////////////

                FvMoveNetworkDevices.Visible = true;

                fvBacktoSearchMove.Visible = false;
                FvDetailShowMove.Visible = false;
                //SearchDetailMove();

                //div_savecut.Visible = true;
                div_statemove.Visible = false;

                ////////////////////////////////////////////

                break;

            case "btnToDeviceNetworkCut":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetwork.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkMove.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkCut.BackColor = System.Drawing.Color.LightGray;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewNetworkDevicesCut);

                FvCutNetworkDevices.ChangeMode(FormViewMode.Insert);
                FvCutNetworkDevices.DataBind();


                ///////////////////////////////////

                FvCutNetworkDevices.Visible = true;

                fvBacktoSearchCut.Visible = false;
                FvDetailShowCut.Visible = false;
                //SearchDetailCut();

                div_savecut.Visible = false;
                div_statecut.Visible = false;

                panel_history_repaircut.Visible = false;
                //SelectLogRepairCut();

                panel_history_macut.Visible = false;
                //SelectLogMACut();

                ///////////////////////////////////

                break;

            case "btnToReport":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetwork.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkMove.BackColor = System.Drawing.Color.Transparent;
                btnToDeviceNetworkCut.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.LightGray;

                MvMaster.SetActiveView(ViewReport);

                actionddltypesearchreport();
                actionddlplacesearchreport();
                actionddlcompanysearchreport();
                actionempsearchreport();

                gridviewreport.Visible = false;
                btnExport.Visible = false;

                // FvCutNetworkDevices.ChangeMode(FormViewMode.Insert);
                // FvCutNetworkDevices.DataBind();

                break;


            case "btnToInsert":
                //MvMaster.SetActiveView(ViewInsert);
                //actionddltype();
                break;

            case "btnCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsert":

                FileUpload upload_file = (FileUpload)FvAddNetworkDevices.FindControl("upload_file");

                TextBox _txtnamecreate = (TextBox)FvInsert.FindControl("txtnamecreate");
                TextBox _txtposcreate = (TextBox)FvInsert.FindControl("txtposcreate");
                TextBox _txtemailcreate = (TextBox)FvInsert.FindControl("txtemailcreate");

                TextBox _txtbrandadd =(TextBox)FvAddNetworkDevices.FindControl("txtbrandadd");
                TextBox _txtgenerationadd = (TextBox)FvAddNetworkDevices.FindControl("txtgenerationadd");
                DropDownList _ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");

               //// DropDownList _ddltypeidxnameadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");////

                DropDownList _ddlcategoryidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcategoryidxadd");

                ////DropDownList _ddlcategoryidxnameadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcategoryidxadd");////

                DropDownList _ddlcompanyidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcompanyidxadd");
                TextBox _txtipaddressadd = (TextBox)FvAddNetworkDevices.FindControl("txtipaddressadd");
                TextBox _txtserialadd = (TextBox)FvAddNetworkDevices.FindControl("txtserialadd");
                TextBox _txtdatepurchaseadd = (TextBox)FvAddNetworkDevices.FindControl("txtdatepurchaseadd");
                TextBox _txtdateexpireadd = (TextBox)FvAddNetworkDevices.FindControl("txtdateexpireadd");
                DropDownList _ddlplaceadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlplaceadd");

                DropDownList _ddlroomadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlroomadd");
                TextBox _txtusernameadd = (TextBox)FvAddNetworkDevices.FindControl("txtusernameadd");
                TextBox _txtpasswordadd = (TextBox)FvAddNetworkDevices.FindControl("txtpasswordadd");
                TextBox _txtassetcodeadd = (TextBox)FvAddNetworkDevices.FindControl("txtassetcodeadd");
                TextBox _txtregisteradd = (TextBox)FvAddNetworkDevices.FindControl("txtregisteradd");
                DropDownList _ddlstatusdevicesadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlstatusdevicesadd");
                TextBox _txtdetailadd = (TextBox)FvAddNetworkDevices.FindControl("txtdetailadd");

                _cemp_idx = emp_idx;
             
                //HttpFileCollection hfnetwork = Request.Files;

                _data_network.network_list = new network_detail[1];

                //_network_detail.u0_devicenetwork_idx = 0;//_type_idx; 

                _network_detail.cemp_idx = _cemp_idx;

                _network_detail.brand_name = _txtbrandadd.Text;
                _network_detail.generation_name = _txtgenerationadd.Text;
                _network_detail.type_idx = int.Parse(_ddltypeidxadd.SelectedValue);
                _network_detail.category_idx = int.Parse(_ddlcategoryidxadd.SelectedValue);
                _network_detail.company_idx = int.Parse(_ddlcompanyidxadd.SelectedValue);
                _network_detail.ip_address = _txtipaddressadd.Text;
                _network_detail.serial_number = _txtserialadd.Text;
                _network_detail.date_purchase = _txtdatepurchaseadd.Text;
                _network_detail.date_expire = _txtdateexpireadd.Text;
                _network_detail.place_idx = int.Parse(_ddlplaceadd.SelectedValue);
                _network_detail.room_idx = int.Parse(_ddlroomadd.SelectedValue);

                //////ข้อมูลส่งเมล
                ////_network_detail.name_create = _txtnamecreate.Text;
                ////_network_detail.poscreate = _txtposcreate.Text;
                ////_network_detail.date_create = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                ////_network_detail.type_name = _ddlcategoryidxadd.SelectedItem.ToString();
                ////_network_detail.category_name = _ddlcategoryidxadd.SelectedItem.ToString();
                ////_network_detail.place_name = _ddlplaceadd.SelectedItem.ToString();

                ////if(_ddlroomadd.SelectedItem.ToString() != null)
                ////{
                ////    _network_detail.room_name = _ddlroomadd.SelectedItem.ToString();
                ////}
                ////else
                ////{
                ////    _network_detail.room_name = "-";
                ////}

                //////ข้อมูลส่งเมล


                if (_txtusernameadd.Text != "" && _txtpasswordadd.Text != "")
                {
                    string _temp1 = _funcTool.getEncryptRC4(_txtpasswordadd.Text, _keynetworkdevices);

                    _network_detail.user_name = _txtusernameadd.Text;
                    _network_detail.pass_word = _temp1;

                    //litDebug.Text = _temp1;
                    
                }
                else
                {
                    _network_detail.user_name = "-";
                    _network_detail.pass_word = "-";
                }
                       
                _network_detail.asset_code = _txtassetcodeadd.Text;
                _network_detail.register_number = _txtregisteradd.Text;


                _network_detail.status_deviecs = int.Parse(_ddlstatusdevicesadd.SelectedValue);


                if(_txtdetailadd.Text != "")
                {
                    _network_detail.detail_devices = _txtdetailadd.Text;
                }
                else
                {
                    _network_detail.detail_devices = "-";
                }
               
                _network_detail.m0_actor_idx = 1;
                _network_detail.m0_node_idx = 1;
                _network_detail.doc_decision = 0;
                _data_network.network_list[0] = _network_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlSetNetwork, _data_network);



                ViewState["registerfilecode"] = _txtregisteradd.Text;

                //Loadfile

                if (upload_file.HasFile)
                {

                    string getPath = ConfigurationManager.AppSettings["upload_networkdevices_file"];
                    string RECode1 = ViewState["registerfilecode"].ToString();
                    string fileName1 = RECode1;
                    string filePath1 = Server.MapPath(getPath + RECode1);
                    if (!Directory.Exists(filePath1))
                    {
                        Directory.CreateDirectory(filePath1);
                    }
                    string extension = Path.GetExtension(upload_file.FileName);

                    upload_file.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                   
                }

               
                if (_data_network.return_code == 0)
                {
                    //service_mail servicemail = new service_mail();

                    //string email = "nipon.r@taokaenoi.co.th";

                    string email = "waraporn.teoy@gmail.com";
                    string replyempcreate = _txtemailcreate.Text;
                    
                    _mail_subject = "[MIS/Network : Network Devices] - " + ViewState["registerfilecode"].ToString();
                    _mail_body = servicemail.networkCreate(_data_network.network_list[0]);
                    //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                    servicemail.SendHtmlFormattedEmailFull(email, "", replyempcreate, _mail_subject, _mail_body);


                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);    
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีเลขทะเบียนนี้ในระบบแล้ว');", true);
                    //setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }


                break;
           
            case "btnSearch":
             
                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearch.Visible = false;

                break;

            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;
                btnSearch.Visible = true;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnViewindex":

                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(';');
                int u0idx_index = int.Parse(arg1[0]);
                string doc_code = arg1[1];
                int noidx_index = int.Parse(arg1[2]);

                

                ViewState["u0idx_index"] = u0idx_index;
                ViewState["registernamecode"] = doc_code;
                ViewState["noidx_index"] = noidx_index;

                gridviewindex.Visible = false;
                btnSearch.Visible = false;
                btnSearchBack.Visible = false;
                showsearch.Visible = false;


                div_showdata.Visible = true;

                SelectViewIndex();
               



                panel_history.Visible = true;

                SelectLog();
                div_btn.Visible = true;
                panelview.Visible = true;
                divshownamedetail.Visible = true;
                lbCmdCancelView.Visible = true;

                setFormData();

                setOntop.Focus();
                //SelectViewEditIndex();
                try
                {

                    string getPathLotus = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["registernamecode"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["registernamecode"].ToString());

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                }
                catch
                {

                }

                break;

            case "btnEditindex":

                string[] arg2 = new string[8];
                arg2 = e.CommandArgument.ToString().Split(';');
                int u0idx_edit = int.Parse(arg2[0]);
                string doc_code_edit = arg2[1];
                int noidx_edit = int.Parse(arg2[2]);
                int actoridx_edit = int.Parse(arg2[3]);
                int docisionidx_edit = int.Parse(arg2[4]);
                int cemp_idx_create_edit = int.Parse(arg2[5]);
                int status_deviecs_edit = int.Parse(arg2[6]);
                int status_move_edit = int.Parse(arg2[7]);
                int status_ma_edit = int.Parse(arg2[8]);

                ViewState["u0idx_edit"] = u0idx_edit;
                ViewState["registernamecode_edit"] = doc_code_edit;
                ViewState["noidx_edit"] = noidx_edit;
                ViewState["actoridx_edit"] = actoridx_edit;
                ViewState["docisionidx_edit"] = docisionidx_edit;
                ViewState["cemp_idx_create_edit"] = cemp_idx_create_edit;
                ViewState["status_deviecs_edit"] = status_deviecs_edit;
                ViewState["status_move_edit"] = status_move_edit;
                ViewState["status_ma_edit"] = status_ma_edit;
                //litDebug.Text = "11111";


                int emp_in;
                emp_in = emp_idx;
                //  เงื่อนไขในการแก้ไข ต้องให้ผู้อนุมัติทำการอนุมัติการสร้างก่อน ถึงจะทำการแก้ไขได้ //
                //if((int.Parse(ViewState["noidx_edit"].ToString()) == 9 && int.Parse(ViewState["actoridx_edit"].ToString()) == 2) && int.Parse(ViewState["docisionidx_edit"].ToString()) == 1)

                if ((emp_in == 1413 || emp_in == 3593) && (int.Parse(ViewState["docisionidx_edit"].ToString()) == 1) && (int.Parse(ViewState["status_deviecs_edit"].ToString()) != 0))
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;                 
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;

                    div_showdata.Visible = true;
                    panelmanage.Visible = true;
                    divshownamedetailmanage.Visible = true;

                    SelectEdit();

                    panel_history.Visible = true;

                    SelectLogEdit();

                    div_saveedit.Visible = true;

                    //div_btn.Visible = true;
                    //lbCmdCancelView.Visible = true;

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["registernamecode_edit"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories(myDirLotus, ViewState["registernamecode_edit"].ToString());

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                    }
                    catch
                    {

                    }
                }

                //else if ((emp_in == int.Parse(ViewState["cemp_idx_create_edit"].ToString())) && (int.Parse(ViewState["docisionidx_edit"].ToString()) == 2) && (int.Parse(ViewState["status_deviecs_edit"].ToString()) != 0) && (int.Parse(ViewState["status_move_edit"].ToString()) == 0))
                else if ((emp_in == 1413 || emp_in == 3593) && (int.Parse(ViewState["docisionidx_edit"].ToString()) == 2) && (int.Parse(ViewState["status_deviecs_edit"].ToString()) != 0) && (int.Parse(ViewState["status_move_edit"].ToString()) == 0))
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;

                    div_showdata.Visible = true;
                    panelmanage.Visible = true;
                    divshownamedetailmanage.Visible = true;

                    SelectEdit();

                    panel_history.Visible = true;

                    SelectLogEdit();

                    div_saveedit.Visible = true;

                    //div_btn.Visible = true;
                    //lbCmdCancelView.Visible = true;

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["registernamecode_edit"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories(myDirLotus, ViewState["registernamecode_edit"].ToString());

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                    }
                    catch
                    {

                    }
                }
                else
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;

                    div_showdata.Visible = true;
                    panelmanage.Visible = true;
                    divshownamedetailmanage.Visible = true;

                    SelectEditViewIndex(); //ไม่สามารถ แก้ไขได้เนื่องจากการสร้างทะเบียนยังไม่ถูกอนุมัติ

                    panel_history.Visible = true;


                    SelectLogEdit();

                    div_btn.Visible = true;
                    lbCmdCancelView.Visible = true;

                    //div_btn.Visible = true;
                    //panelview.Visible = true;
                    //divshownamedetail.Visible = true;
                    //lbCmdCancelView.Visible = true;

                    //setFormData();

                    try
                    {

                        string getPathLotus = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                        string filePathLotus = Server.MapPath(getPathLotus + ViewState["registernamecode_edit"].ToString());
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                        SearchDirectories(myDirLotus, ViewState["registernamecode_edit"].ToString());

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                    }
                    catch
                    {

                    }
                }
                setOntop.Focus();
                //else if ((emp_in == int.Parse(ViewState["cemp_idx_create_edit"].ToString())) && (int.Parse(ViewState["docisionidx_edit"].ToString()) == 2) && (int.Parse(ViewState["status_deviecs_edit"].ToString()) != 0) && (int.Parse(ViewState["status_ma_edit"].ToString()) == 0))
                //{
                //    gridviewindex.Visible = false;
                //    btnSearch.Visible = false;
                //    btnSearchBack.Visible = false;
                //    showsearch.Visible = false;

                //    div_showdata.Visible = true;
                //    panelmanage.Visible = true;
                //    divshownamedetailmanage.Visible = true;

                //    SelectEdit();

                //    panel_history.Visible = true;

                //    SelectLogEdit();

                //    div_saveedit.Visible = true;

                //    //div_btn.Visible = true;
                //    //lbCmdCancelView.Visible = true;

                //    try
                //    {

                //        string getPathLotus = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                //        string filePathLotus = Server.MapPath(getPathLotus + ViewState["registernamecode_edit"].ToString());
                //        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                //        SearchDirectories(myDirLotus, ViewState["registernamecode_edit"].ToString());

                //        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                //    }
                //    catch
                //    {

                //    }
                //}



                break;

            case "btnCancelView":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelApprove":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelEdit":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsaveapprove":

                HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
                HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");

                DropDownList ddl_approvestatus = (DropDownList)ViewIndex.FindControl("ddl_approvestatus");

                Label _txtregister_numbermailapprove = (Label)FvViewDetail.FindControl("txtregister_numberview");
                Label _txttype_nameviewmail = (Label)FvViewDetail.FindControl("txttype_nameview");
                Label _txtcategory_nameviewmail = (Label)FvViewDetail.FindControl("txtcategory_nameview");
                Label _txtplace_nameviewmail = (Label)FvViewDetail.FindControl("txtplace_nameview");
                Label _txtroom_nameviewmail = (Label)FvViewDetail.FindControl("txtroom_nameview");

                Label _txtasset_codeviewmail = (Label)FvViewDetail.FindControl("txtasset_codeview");
                Label _txtbrand_nameviewmail = (Label)FvViewDetail.FindControl("txtbrand_nameview");
                Label _txtgeneration_nameviewmail = (Label)FvViewDetail.FindControl("txtgeneration_nameview");

                Panel div_showplacemove = (Panel)FvViewDetail.FindControl("div_showplacemove");
                Label lbshow_place_idx = (Label)div_showplacemove.FindControl("lbshow_place_idx");
                Label lbshow_room_idx = (Label)div_showplacemove.FindControl("lbshow_room_idx");
                TextBox txtplace_name_newmove = (TextBox)div_showplacemove.FindControl("txtplace_name_newmove");
                TextBox txtroom_name_newmove = (TextBox)div_showplacemove.FindControl("txtroom_name_newmove");


                Panel div_showcompanyma = (Panel)FvViewDetail.FindControl("div_showcompanyma");
                Label lbcompany_idxma = (Label)div_showcompanyma.FindControl("lbcompany_idxma");
                TextBox txtdate_startma = (TextBox)div_showcompanyma.FindControl("txtdate_startma");
                TextBox txtdate_endma = (TextBox)div_showcompanyma.FindControl("txtdate_endma");

                TextBox txtcompany_idxmamail = (TextBox)div_showcompanyma.FindControl("txtcompany_idxma");

                ////// ส่วน edit
                Panel div_showedit = (Panel)ViewIndex.FindControl("div_showedit");
                TextBox txtbrand_nameedit = (TextBox)div_showedit.FindControl("txtbrand_nameedit");
                TextBox txtgeneration_nameedit = (TextBox)div_showedit.FindControl("txtgeneration_nameedit");
                Label lbtype_idxedit = (Label)div_showedit.FindControl("lbtype_idxedit");
                Label lbcategory_idxedit = (Label)div_showedit.FindControl("lbcategory_idxedit");
                TextBox txtip_addressedit = (TextBox)div_showedit.FindControl("txtip_addressedit");
                TextBox txtserial_numberedit = (TextBox)div_showedit.FindControl("txtserial_numberedit");
                Label lbplace_idxedit = (Label)div_showedit.FindControl("lbplace_idxedit");
                Label lbroom_idxedit = (Label)div_showedit.FindControl("lbroom_idxedit");

                TextBox txtasset_codeedit = (TextBox)div_showedit.FindControl("txtasset_codeedit");

                TextBox txtuser_nameedit = (TextBox)div_showedit.FindControl("txtuser_nameedit");
                
                TextBox txtpass_wordedit = (TextBox)div_showedit.FindControl("txtpass_wordedit");
           
                ////// ส่วน edit


                int node = int.Parse(hf_m0_node_idx.Value);
                int actor = int.Parse(hf_m0_actor_idx.Value);
                _cemp_idx = emp_idx;


                if (actor == 2)
                {
                    //UpdateApproveList();
                                 
                    _data_network.network_list = new network_detail[1];
                   
                   // _network_detail.cemp_idx = _cemp_idx;
                    _network_detail.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_index"].ToString());
                    _network_detail.m0_node_idx = node;
                    _network_detail.m0_actor_idx = actor;
                    _network_detail.cemp_idx_approve = _cemp_idx;
                    _network_detail.doc_decision = int.Parse(ddl_approvestatus.SelectedValue);

                    
                    _network_detail.place_idx = int.Parse(lbshow_place_idx.Text);
                    _network_detail.room_idx = int.Parse(lbshow_room_idx.Text);


                    _network_detail.company_new_idx = int.Parse(lbcompany_idxma.Text);
                    _network_detail.date_purchase_new = txtdate_startma.Text;
                    _network_detail.date_expire_new = txtdate_endma.Text;


                    _network_detail.name_create = ViewState["emp_name_th"].ToString();
                    _network_detail.pos_name_th = ViewState["pos_name_th"].ToString();
                    _network_detail.register_number = _txtregister_numbermailapprove.Text;
                    _network_detail.date_create = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");
                    _network_detail.type_name = _txttype_nameviewmail.Text;
                    _network_detail.category_name = _txtcategory_nameviewmail.Text;
                    _network_detail.place_name = _txtplace_nameviewmail.Text;
                 
                    _network_detail.room_name = _txtroom_nameviewmail.Text;

                    _network_detail.asset_code = _txtasset_codeviewmail.Text;
                    _network_detail.brand_name = _txtbrand_nameviewmail.Text;
                    _network_detail.generation_name = _txtgeneration_nameviewmail.Text;

                    
                    _network_detail.place_new_name = txtplace_name_newmove.Text;


                    _network_detail.company_name = txtcompany_idxmamail.Text;



                    if (txtroom_name_newmove.Text != "")
                    {
                        _network_detail.room_new_name = txtroom_name_newmove.Text;
                    }
                    else
                    {
                        _network_detail.room_new_name = "-";
                    }
                    


                    _network_detail.status_approve = ddl_approvestatus.SelectedItem.ToString();

                    //ทำการแก้ไข
                    if (node == 11 && int.Parse(ddl_approvestatus.SelectedValue) != 2)
                    {
                        ////// edit ///////
                        _network_detail.brand_name_edit = txtbrand_nameedit.Text;
                        _network_detail.generation_name_edit = txtgeneration_nameedit.Text;
                        _network_detail.type_idx_edit = int.Parse(lbtype_idxedit.Text);

                        if (lbcategory_idxedit.Text != "-")
                        {
                            _network_detail.category_idx_edit = int.Parse(lbcategory_idxedit.Text);
                        }
                        else
                        {
                            _network_detail.category_idx_edit = 0;
                        }
                        _network_detail.ip_address_edit = txtip_addressedit.Text;
                        _network_detail.serial_number_edit = txtserial_numberedit.Text;
                        _network_detail.place_idx_edit = int.Parse(lbplace_idxedit.Text);

                        if (lbroom_idxedit.Text != "-")
                        {
                            _network_detail.room_idx_edit = int.Parse(lbroom_idxedit.Text);
                        }
                        else
                        {
                            _network_detail.room_idx_edit = 0;
                        }

                        if (txtasset_codeedit.Text != "")
                        {
                            _network_detail.asset_code_edit = txtasset_codeedit.Text;
                        }
                        else
                        {
                            _network_detail.asset_code_edit = "-";
                        }

                       //// _network_detail.asset_code_edit = txtasset_codeedit.Text;
                       ////// convert pass
                       ////if (txtuser_nameedit.Text != "" && txtpass_wordedit.Text != "")
                       ////{
                       ////    string _temp9 = _funcTool.getEncryptRC4(txtpass_wordedit.Text, _keynetworkdevices);

                            ////    _network_detail.user_name_edit = txtuser_nameedit.Text;
                            ////    _network_detail.pass_word_edit = _temp9.ToString();
                            ////}
                            ////else
                            ////{
                            ////    _network_detail.user_name_edit = "-";
                            ////    _network_detail.pass_word_edit = "-";
                            ////}


                            ////// edit ///////

                    }

                    _data_network.network_list[0] = _network_detail;

                    if (node == 2)  //สร้าง
                    {
                        // *** ส่งเมล *** //
                        //string tomail = "basis@taokaenoi.co.th";
                        string tomail = "waraporn.teoy@gmail.com";
                        string replyempcreate = ViewState["emp_email"].ToString();

                        _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numbermailapprove.Text;
                        _mail_body = servicemail.networkApproveCreate(_data_network.network_list[0]);
                        //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        servicemail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);

                        // *** ส่งเมล *** //
                    }

                    else if (node == 4)  //ตัดชำรุด
                    {
                        // *** ส่งเมล *** //
                        //string tomail = "basis@taokaenoi.co.th";
                        string tomail = "waraporn.teoy@gmail.com";
                        string replyempcreate = ViewState["emp_email"].ToString();

                        _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numbermailapprove.Text;
                        _mail_body = servicemail.networkApproveCut(_data_network.network_list[0]);
                        //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        servicemail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);

                        // *** ส่งเมล *** //
                    }

                    else if (node == 5)  //ย้าย
                    {
                        // *** ส่งเมล *** //
                        //string tomail = "basis@taokaenoi.co.th";
                        string tomail = "waraporn.teoy@gmail.com";
                        string replyempcreate = ViewState["emp_email"].ToString();

                        _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numbermailapprove.Text;
                        _mail_body = servicemail.networkApproveMove(_data_network.network_list[0]);
                        //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        servicemail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);

                        // *** ส่งเมล *** //
                    }

                    else if (node == 3)  //ma
                    {
                        // *** ส่งเมล *** //
                        //string tomail = "basis@taokaenoi.co.th";
                        string tomail = "waraporn.teoy@gmail.com";
                        string replyempcreate = ViewState["emp_email"].ToString();

                        _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numbermailapprove.Text;
                        _mail_body = servicemail.networkApproveMA(_data_network.network_list[0]);
                        //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        servicemail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);

                        // *** ส่งเมล *** //
                    }

                    else if (node == 11)  //edit
                    {
                        // *** ส่งเมล *** //
                        //string tomail = "basis@taokaenoi.co.th";
                        string tomail = "waraporn.teoy@gmail.com";
                        string replyempcreate = ViewState["emp_email"].ToString();

                        _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numbermailapprove.Text;
                        _mail_body = servicemail.networkApproveEdit(_data_network.network_list[0]);
                        //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                        servicemail.SendHtmlFormattedEmailFull(tomail, "", replyempcreate, _mail_subject, _mail_body);

                        // *** ส่งเมล *** //
                    }

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                    _data_network = callServiceDataNetwork(_urlSetApproveNetwork, _data_network);

                    if (_data_network.return_code == 0)
                    {
                        //actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);    

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                    }



                }


                break;

            case "btnsaveedit":

                int u0_idxeditnetwork;

                u0_idxeditnetwork = int.Parse(ViewState["u0idx_edit"].ToString());
                _cemp_idx = emp_idx;

                TextBox _txtregister_numberview = (TextBox)FvViewDetail.FindControl("txtregister_numberview");
                TextBox _txtbrand_nameview = (TextBox)FvViewDetail.FindControl("txtbrand_nameview");
                TextBox _txtgeneration_nameview = (TextBox)FvViewDetail.FindControl("txtgeneration_nameview");
                DropDownList _ddltype_nameedit = (DropDownList)FvViewDetail.FindControl("ddltype_nameedit");
                DropDownList _ddlcategory_nameedit = (DropDownList)FvViewDetail.FindControl("ddlcategory_nameedit");
                TextBox _txtip_addressview = (TextBox)FvViewDetail.FindControl("txtip_addressview");
                TextBox _txtserial_numberview = (TextBox)FvViewDetail.FindControl("txtserial_numberview");
                TextBox _txtdate_purchaseview = (TextBox)FvViewDetail.FindControl("txtdate_purchaseview");
                TextBox _txtdate_expireview = (TextBox)FvViewDetail.FindControl("txtdate_expireview");
                DropDownList _ddltxtplace_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtplace_nameedit");
                DropDownList _ddltxtroom_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtroom_nameedit");
                TextBox _txtuser_nameview = (TextBox)FvViewDetail.FindControl("txtuser_nameview");

                TextBox _txtpass_wordview = (TextBox)FvViewDetail.FindControl("txtpass_wordview");

                TextBox _txtasset_codeview = (TextBox)FvViewDetail.FindControl("txtasset_codeview");
                DropDownList _ddlstatus_deviecsview = (DropDownList)FvViewDetail.FindControl("ddlstatus_deviecsview");

                _data_network.network_list = new network_detail[1];

                _network_detail.u0_devicenetwork_idx = u0_idxeditnetwork;

                _network_detail.cemp_idx = _cemp_idx;

                _network_detail.brand_name = _txtbrand_nameview.Text;
                _network_detail.generation_name = _txtgeneration_nameview.Text;
                _network_detail.type_idx = int.Parse(_ddltype_nameedit.SelectedValue);
                _network_detail.category_idx = int.Parse(_ddlcategory_nameedit.SelectedValue);
                //_network_detail.company_idx = int.Parse(_ddlcompanyidxadd.SelectedValue);
                _network_detail.ip_address = _txtip_addressview.Text;
                _network_detail.serial_number = _txtserial_numberview.Text;
                _network_detail.date_purchase = _txtdate_purchaseview.Text;
                _network_detail.date_expire = _txtdate_expireview.Text;
                _network_detail.place_idx = int.Parse(_ddltxtplace_nameedit.SelectedValue);
                _network_detail.room_idx = int.Parse(_ddltxtroom_nameedit.SelectedValue);

                if (_txtuser_nameview.Text != "" && _txtpass_wordview.Text != "")
                {
                    string _temp1 = _funcTool.getEncryptRC4(_txtpass_wordview.Text, _keynetworkdevices);

                    _network_detail.user_name = _txtuser_nameview.Text;
                    _network_detail.pass_word = _temp1;

                    //litDebug.Text = _temp1;

                }
                else
                {
                    _network_detail.user_name = "-";
                    _network_detail.pass_word = "-";
                }

                _network_detail.asset_code = _txtasset_codeview.Text;

                _network_detail.register_number = _txtregister_numberview.Text;


                _network_detail.status_deviecs = int.Parse(_ddlstatus_deviecsview.SelectedValue);

        
                _network_detail.m0_actor_idx = 1;
                ////_network_detail.m0_node_idx = 1;
                _network_detail.m0_node_idx = 10;
                _network_detail.doc_decision = 6;
                ////_network_detail.doc_decision = 0;



                _network_detail.name_create = ViewState["emp_name_th"].ToString();
                _network_detail.pos_name_th = ViewState["pos_name_th"].ToString();              
                _network_detail.date_create = DateTime.Now.ToString("dd/MM/yyyy hh:mm tt");


                _data_network.network_list[0] = _network_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));


                string emailedit = "waraporn.teoy@gmail.com";
                //string emailedit = "nipon.r@taokaenoi.co.th";

                //string replyempedit = "basis@taokaenoi.co.th";
                string replyempedit = "waraporn.teoy@gmail.com";

                _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numberview.Text;
                _mail_body = servicemail.networkEdit(_data_network.network_list[0]);
                //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                servicemail.SendHtmlFormattedEmailFull(emailedit, "", replyempedit, _mail_subject, _mail_body);


                _data_network = callServiceDataNetwork(_urlSetNetwork, _data_network);

                if (_data_network.return_code == 0)
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);    

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnRepairindex":


                string[] arg3 = new string[5];
                arg3 = e.CommandArgument.ToString().Split(';');
                int u0idx_repair = int.Parse(arg3[0]);
                string doc_code_repair = arg3[1];
                int noidx_repair = int.Parse(arg3[2]);
                int actoridx_repair = int.Parse(arg3[3]);
                int docisionidx_repair = int.Parse(arg3[4]);
                int status_deviecs_repair = int.Parse(arg3[5]);

                ViewState["u0idx_repair"] = u0idx_repair;
                ViewState["registernamecode_repair"] = doc_code_repair;
                ViewState["noidx_repair"] = noidx_repair;
                ViewState["actoridx_repair"] = actoridx_repair;
                ViewState["docisionidx_repair"] = docisionidx_repair;
                ViewState["status_deviecs_repair"] = status_deviecs_repair;
                //litDebug.Text = "11111";

                if(ViewState["status_deviecs_repair"].ToString() == "1")
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;


                    div_showdata.Visible = true;
                    panelrepair.Visible = true;
                    divshownamedetailrepair.Visible = true;

                    FvViewRepair.ChangeMode(FormViewMode.Insert);
                    FvViewRepair.DataBind();

                    Label lb_u0_idxrepair = (Label)FvViewRepair.FindControl("lb_u0_idxrepair");
                    TextBox txtregisterrepair = (TextBox)FvViewRepair.FindControl("txtregisterrepair");


                    panel_history_repair.Visible = true;
                    div_saverepair.Visible = true;
                    btnsaverepair.Visible = true;

                    lb_u0_idxrepair.Text = ViewState["u0idx_repair"].ToString();
                    txtregisterrepair.Text = ViewState["registernamecode_repair"].ToString();

                    SelectLogRepair();
                }
                else
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;


                    div_showdata.Visible = true;
                    panelrepair.Visible = true;
                    divshownamedetailrepair.Visible = true;

                    FvViewDetailRepair.ChangeMode(FormViewMode.Insert);
                    FvViewDetailRepair.DataBind();


                    Label lb_u0_idxrepairview = (Label)FvViewDetailRepair.FindControl("lb_u0_idxrepairview");
                    TextBox txtregisterrepairview = (TextBox)FvViewDetailRepair.FindControl("txtregisterrepairview");


                    lb_u0_idxrepairview.Text = ViewState["u0idx_repair"].ToString();
                    txtregisterrepairview.Text = ViewState["registernamecode_repair"].ToString();


                    panel_history_repair.Visible = true;
                    div_saverepair.Visible = true;

                    SelectLogRepair();

                }

                setOntop.Focus();

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelRepair":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsaverepair":

                TextBox _txtdaterepair = (TextBox)FvViewRepair.FindControl("txtdaterepair");
                TextBox _txtdetailrepair = (TextBox)FvViewRepair.FindControl("txtdetailrepair");
                _cemp_idx = emp_idx;


                _data_network.repairnetwork_list = new repairnetwork_detail[1];

                _repairnetwork_detail.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_repair"].ToString());
                _repairnetwork_detail.cemp_idx = _cemp_idx;
                _repairnetwork_detail.register_number = ViewState["registernamecode_repair"].ToString();
                //_repairnetwork_detail.register_number = ViewState["registernamecode_repair"].ToString();

                _repairnetwork_detail.date_repair = _txtdaterepair.Text;

                if(_txtdetailrepair.Text != "")
                {
                    _repairnetwork_detail.comment = _txtdetailrepair.Text;
                }
                else
                {
                    _repairnetwork_detail.comment = "-";
                }
                         
                _data_network.repairnetwork_list[0] = _repairnetwork_detail;

                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_network));

                _data_network = callServiceDataNetwork(_urlSetRepairNetwork, _data_network);

                if (_data_network.return_code == 0)
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);    

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }


                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnMAindex":

                string[] arg4 = new string[7];
                arg4 = e.CommandArgument.ToString().Split(';');
                int u0idx_ma = int.Parse(arg4[0]);
                string doc_code_ma = arg4[1];
                int noidx_ma = int.Parse(arg4[2]);
                int actoridx_ma = int.Parse(arg4[3]);
                int docisionidx_ma = int.Parse(arg4[4]);
                string company_name_ma = arg4[5];
                string date_expire_ma = arg4[6];
                int status_deviecs_ma = int.Parse(arg4[7]);

                ViewState["u0idx_ma"] = u0idx_ma;
                ViewState["doc_code_ma"] = doc_code_ma;
                ViewState["noidx_ma"] = noidx_ma;
                ViewState["actoridx_ma"] = actoridx_ma;
                ViewState["docisionidx_ma"] = docisionidx_ma;
                ViewState["company_name_ma"] = company_name_ma;
                ViewState["date_expire_ma"] = date_expire_ma;
                ViewState["status_deviecs_ma"] = status_deviecs_ma;
                //litDebug.Text = "11111";

                //กรณีผู้อนุมัติทำการอนุมัติ ทะเบียน
                if (ViewState["status_deviecs_ma"].ToString() == "1" && ViewState["docisionidx_ma"].ToString() == "1")

                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;

                    div_showdata.Visible = true;
                    panelma.Visible = true;
                    divshownamedetailma.Visible = true;

                    FvViewMA.ChangeMode(FormViewMode.Insert);
                    FvViewMA.DataBind();

                    actionddlcompanyma();

                    //Label lb_u0_idxma = (Label)FvViewMA.FindControl("lb_u0_idxma");
                    Label lb_u0_idxma = (Label)FvViewMA.FindControl("lb_u0_idxma");
                    TextBox txtregisterma = (TextBox)FvViewMA.FindControl("txtregisterma");
                    TextBox txtcompany_idxma = (TextBox)FvViewMA.FindControl("txtcompany_idxma");
                    TextBox txtdate_expirema = (TextBox)FvViewMA.FindControl("txtdate_expirema");
                    
                   // lb_u0_idxma.Visible = true;

                    lb_u0_idxma.Text = ViewState["u0idx_ma"].ToString();
                    txtregisterma.Text = ViewState["doc_code_ma"].ToString();
                    txtcompany_idxma.Text = ViewState["company_name_ma"].ToString();
                    txtdate_expirema.Text = ViewState["date_expire_ma"].ToString();

                  
                    panel_history_ma.Visible = true;
                    div_savema.Visible = true;
                    btnsaveMA.Visible = true;
               
                    SelectLogMA();
                }
                else if (ViewState["status_deviecs_ma"].ToString() == "1" && ViewState["docisionidx_ma"].ToString() == "2")

                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;

                    div_showdata.Visible = true;
                    panelma.Visible = true;
                    divshownamedetailma.Visible = true;

                    FvViewMA.ChangeMode(FormViewMode.Insert);
                    FvViewMA.DataBind();

                    actionddlcompanyma();

                    //Label lb_u0_idxma = (Label)FvViewMA.FindControl("lb_u0_idxma");
                    Label lb_u0_idxma = (Label)FvViewMA.FindControl("lb_u0_idxma");
                    TextBox txtregisterma = (TextBox)FvViewMA.FindControl("txtregisterma");
                    TextBox txtcompany_idxma = (TextBox)FvViewMA.FindControl("txtcompany_idxma");
                    TextBox txtdate_expirema = (TextBox)FvViewMA.FindControl("txtdate_expirema");

                    // lb_u0_idxma.Visible = true;

                    lb_u0_idxma.Text = ViewState["u0idx_ma"].ToString();
                    txtregisterma.Text = ViewState["doc_code_ma"].ToString();
                    txtcompany_idxma.Text = ViewState["company_name_ma"].ToString();
                    txtdate_expirema.Text = ViewState["date_expire_ma"].ToString();


                    panel_history_ma.Visible = true;
                    div_savema.Visible = true;
                    btnsaveMA.Visible = true;

                    SelectLogMA();
                }
                else
                {
                    gridviewindex.Visible = false;
                    btnSearch.Visible = false;
                    btnSearchBack.Visible = false;
                    showsearch.Visible = false;


                    div_showdata.Visible = true;
                    panelma.Visible = true;
                    divshownamedetailma.Visible = true;

                    FvViewDetailMA.ChangeMode(FormViewMode.Insert);
                    FvViewDetailMA.DataBind();

                    Label lb_u0_idxmaview = (Label)FvViewDetailMA.FindControl("lb_u0_idxmaview");

                    TextBox txtregistermaview = (TextBox)FvViewDetailMA.FindControl("txtregistermaview");
                    TextBox txtcompany_idxmaview = (TextBox)FvViewDetailMA.FindControl("txtcompany_idxmaview");
                    TextBox txtdate_expiremaview = (TextBox)FvViewDetailMA.FindControl("txtdate_expiremaview");

                    lb_u0_idxmaview.Text = ViewState["u0idx_ma"].ToString();
                    txtregistermaview.Text = ViewState["doc_code_ma"].ToString();
                    txtcompany_idxmaview.Text = ViewState["company_name_ma"].ToString();
                    txtdate_expiremaview.Text = ViewState["date_expire_ma"].ToString();

                  
                    panel_history_ma.Visible = true;
                    div_savema.Visible = true;

                    SelectLogMA();
                    
                }

                setOntop.Focus();

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;


            case "btnCancelMA":

                
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsaveMA":

                DropDownList _ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");
                TextBox _txtdatestartma = (TextBox)FvViewMA.FindControl("txtdatestartma");
                TextBox _txtdateendma = (TextBox)FvViewMA.FindControl("txtdateendma");
                TextBox _txtdetailma = (TextBox)FvViewMA.FindControl("txtdetailma");

                TextBox _txtregistermawmail = (TextBox)FvViewMA.FindControl("txtregisterma");
                TextBox _txtcompany_idxmamail = (TextBox)FvViewMA.FindControl("txtcompany_idxma");
                TextBox _txtdate_expiremamail = (TextBox)FvViewMA.FindControl("txtdate_expirema");
               


                _cemp_idx = emp_idx;


                _data_network.manetwork_list = new manetwork_detail[1];

                _manetwork_detail.u0_devicenetwork_idx = int.Parse(ViewState["u0idx_ma"].ToString());
               
                _manetwork_detail.register_number = ViewState["doc_code_ma"].ToString();
                _manetwork_detail.company_idx = int.Parse(_ddlcompanyidxma.SelectedValue);
                _manetwork_detail.date_start = _txtdatestartma.Text;
                _manetwork_detail.date_end = _txtdateendma.Text;

                _manetwork_detail.m0_node_idx = 6;//node;
                _manetwork_detail.m0_actor_idx = 1;//actor;
                _manetwork_detail.doc_decision = 3;//ดำเนินย้าย
                _manetwork_detail.cemp_idx = _cemp_idx;

                if (_txtdetailma.Text != "")
                {
                    _manetwork_detail.comment = _txtdetailma.Text;
                }
                else
                {
                    _manetwork_detail.comment = "-";
                }


                //ข้อมูลส่งเมล
                //_manetwork_detail.register_number = ViewState["doc_code_ma"].ToString();
                _manetwork_detail.name_create = ViewState["emp_name_th"].ToString();
                _manetwork_detail.pos_name_th = ViewState["pos_name_th"].ToString();
                ////_manetwork_detail.date_create = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt");
                _manetwork_detail.company_name_old = _txtcompany_idxmamail.Text;
                _manetwork_detail.date_expire_old = _txtdate_expiremamail.Text;
                _manetwork_detail.company_name = _ddlcompanyidxma.SelectedItem.ToString();
                //_manetwork_detail.company_name = _ddlcompanyidxma.SelectedItem.ToString();

                //ข้อมูลส่งเมล
         
                _data_network.manetwork_list[0] = _manetwork_detail;

                string tomailma = "waraporn.teoy@gmail.com";
                //string tomailma = "nipon.r@taokaenoi.co.th";
                //string replyempcreatema = "basis@taokaenoi.co.th";
                string replyempcreatema = "waraporn.teoy@gmail.com";

                ////////string replyempcreate = "nipon@taokeanoi.co.th";//ViewState["emp_email"].ToString();  

                _mail_subject = "[MIS/Network : Network Devices] - " + ViewState["doc_code_ma"].ToString();
                _mail_body = servicemail.networkMa(_data_network.manetwork_list[0]);
                //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                servicemail.SendHtmlFormattedEmailFull(tomailma, "", replyempcreatema, _mail_subject, _mail_body);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlSetMANetwork, _data_network);

                if (_data_network.return_code == 0)
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);    

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }


                break;

            case "btnQrCodeindex":

                string[] arg5 = new string[7];

                arg5 = e.CommandArgument.ToString().Split(';');

                int u0idx_qr = int.Parse(arg5[0]);
                string doc_code_qr = arg5[1];
                int noidx_qr = int.Parse(arg5[2]);
                int actoridx_qr = int.Parse(arg5[3]);
                int docisionidx_qr = int.Parse(arg5[4]);
                string company_name_qr = arg5[5];
                string date_expire_qr = arg5[6];
                int status_deviecs_qr = int.Parse(arg5[7]);

                ViewState["u0idx_qr"] = u0idx_qr;
                ViewState["doc_code_qr"] = doc_code_qr;
                ViewState["noidx_qr"] = noidx_qr;
                ViewState["actoridx_qr"] = actoridx_qr;
                ViewState["docisionidx_qr"] = docisionidx_qr;
                ViewState["company_name_qr"] = company_name_qr;
                ViewState["date_expire_qr"] = date_expire_qr;
                ViewState["status_deviecs_qr"] = status_deviecs_qr;
                             
                gridviewindex.Visible = false;
                btnSearch.Visible = false;
                btnSearchBack.Visible = false;
                showsearch.Visible = false;

                
                div_showdata.Visible = true;
                BackToIndexforQrCode.Visible = true;
                panelqrcode.Visible = true;
                divshownamedetailqrcode.Visible = true;

                div_qrcode.Visible = true;
                imageqrshow.Visible = true;

              
                TextBox txtu0_idx_qr = (TextBox)ViewIndex.FindControl("txtu0_idx_qr");
                TextBox txtregisterqr = (TextBox)ViewIndex.FindControl("txtregisterqr");
                TextBox txtCodeqr = (TextBox)ViewIndex.FindControl("txtCode");

                txtregisterqr.Text = ViewState["doc_code_qr"].ToString();
                txtu0_idx_qr.Text = ViewState["u0idx_qr"].ToString();
                ////txtCodeqr.Text = "http://mas.taokaenoi.co.th/network-detail?aaa=sdasdadasdasdasdasd";

                //string txtGenQrCode = "http://mas.taokaenoi.co.th/network-devices-detail/" + _funcTool.getEncryptRC4(ViewState["u0idx_qr"].ToString(), _keynetworkdevices) ;

                string txtGenQrCode = "http://demo.taokaenoi.co.th/network-devices-detail/" + _funcTool.getEncryptRC4(ViewState["u0idx_qr"].ToString(), _keynetworkdevices) ;

                ////string txtGenQrCode = "http://localhost/mas.taokaenoi.co.th/network-devices-detail/" + _funcTool.getEncryptRC4(ViewState["u0idx_qr"].ToString(), _keynetworkdevices);

                ViewState["txtCodeqr"] = txtCodeqr.Text;

          
                //GenQRCode
                string getPathimages = ConfigurationManager.AppSettings["upload_Qr_Code"];
                string fileNameimage = ViewState["doc_code_qr"].ToString(); // ชื่อรูป QRCode

                string fileimagePath = Server.MapPath(getPathimages + fileNameimage);

                QRCodeEncoder encoder = new QRCodeEncoder();
                Bitmap bi = encoder.Encode(txtGenQrCode.ToString());

                bi.Save(Server.MapPath(getPathimages + fileNameimage + ".jpg"), ImageFormat.Jpeg);


                //Show ViewNetworkDevicesMove
                string getPathShowimages = ConfigurationManager.AppSettings["upload_Qr_Code"];            
                string fileimageShowPath = Server.MapPath(getPathShowimages + ViewState["doc_code_qr"].ToString() + ".jpg");
              
                //// Show รูป
                if (!File.Exists(Server.MapPath(getPathShowimages + ViewState["doc_code_qr"].ToString() + ".jpg")))
                {
                    ImagShowQRCode.Visible = false;
                }
                else
                {
                    ImagShowQRCode.ImageUrl = getPathShowimages + ViewState["doc_code_qr"].ToString() + ".jpg";
                }


                if(txtGenQrCode != null)
                {

                }
                else
                {

                }

                setOntop.Focus();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelIndex":


                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnBackToIndexforQrCode":


                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSearchIndex":

                DropDownList _ddlplacesearch = (DropDownList)ViewIndex.FindControl("ddlplacesearch");
                DropDownList _ddlroomsearch = (DropDownList)ViewIndex.FindControl("ddlroomsearch");
                DropDownList _ddltypeidxsearch = (DropDownList)ViewIndex.FindControl("ddltypeidxsearch");
                DropDownList _ddlcategoryidxsearch = (DropDownList)ViewIndex.FindControl("ddlcategoryidxsearch");
                TextBox _txtbrandsearch = (TextBox)ViewIndex.FindControl("txtbrandsearch");
                TextBox _txtgenerationsearch = (TextBox)ViewIndex.FindControl("txtgenerationsearch");
                DropDownList _ddlnamesearch = (DropDownList)ViewIndex.FindControl("ddlnamesearch");
                DropDownList _ddlcompanyidxsearch = (DropDownList)ViewIndex.FindControl("ddlcompanyidxsearch");

                TextBox _txtipaddresssearch = (TextBox)ViewIndex.FindControl("txtipaddresssearch");
                TextBox _txtserialsearch = (TextBox)ViewIndex.FindControl("txtserialsearch");
                TextBox _txtregistersearch = (TextBox)ViewIndex.FindControl("txtregistersearch");

                DropDownList _ddlSearchDate = (DropDownList)ViewIndex.FindControl("ddlSearchDate");
                TextBox _txtdatepurchasesearch = (TextBox)ViewIndex.FindControl("txtdatepurchasesearch");
                TextBox _txtdateexpirsearch = (TextBox)ViewIndex.FindControl("txtdateexpirsearch");

                _cemp_idx = emp_idx;

                _data_network.bindnetwork_list = new bindnetwork_detail[1];

                _bindnetwork_detail.place_idx = int.Parse(_ddlplacesearch.SelectedValue);
                _bindnetwork_detail.room_idx = int.Parse(_ddlroomsearch.SelectedValue);
                _bindnetwork_detail.type_idx = int.Parse(_ddltypeidxsearch.SelectedValue);
                _bindnetwork_detail.category_idx = int.Parse(_ddlcategoryidxsearch.SelectedValue);
                _bindnetwork_detail.brand_name = _txtbrandsearch.Text;
                _bindnetwork_detail.generation_name = _txtgenerationsearch.Text;
                _bindnetwork_detail.cemp_idx_create = int.Parse(_ddlnamesearch.SelectedValue);
                _bindnetwork_detail.company_idx = int.Parse(_ddlcompanyidxsearch.SelectedValue);
                _bindnetwork_detail.ip_address = _txtipaddresssearch.Text;
                _bindnetwork_detail.serial_number = _txtserialsearch.Text;
                _bindnetwork_detail.register_number = _txtregistersearch.Text;

                _bindnetwork_detail.if_date = int.Parse(_ddlSearchDate.SelectedValue);
                _bindnetwork_detail.date_purchase = _txtdatepurchasesearch.Text;
                _bindnetwork_detail.date_expire = _txtdateexpirsearch.Text;

                _data_network.bindnetwork_list[0] = _bindnetwork_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlGetSearch, _data_network);

                ViewState["bind_data_index"] = _data_network.bindnetwork_list;
                setGridData(GvMaster, ViewState["bind_data_index"]);


                break;

            case "btnSearchCut":

                //FormView FvCutNetworkDevices = (FormView)ViewNetworkDevicesCut.FindControl("FvCutNetworkDevices");
                DropDownList _ddlregister_numbercut = (DropDownList)FvCutNetworkDevices.FindControl("ddlregister_numbercut");

                ViewState["_ddlregister_numbercut"] = _ddlregister_numbercut.SelectedValue;
                ViewState["register_numbercut"] = _ddlregister_numbercut.SelectedItem.ToString();

                //FvDetailShowCut.ChangeMode(FormViewMode.Insert);
                if(int.Parse(ViewState["_ddlregister_numbercut"].ToString()) != 0)

                {
                    FvCutNetworkDevices.Visible = false;

                    fvBacktoSearchCut.Visible = true;
                    SearchDetailCut();


                    div_savecut.Visible = true;
                    div_statecut.Visible = true;


                    try
                    {

                        string getPathFileCut = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                        string filePathCut = Server.MapPath(getPathFileCut + ViewState["register_numbercut"].ToString());
                        DirectoryInfo myDirCut = new DirectoryInfo(filePathCut);

                        SearchDirectoriesCut(myDirCut, ViewState["register_numbercut"].ToString());

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                    }
                    catch
                    {

                    }


                    panel_history_repaircut.Visible = true;
                    SelectLogRepairCut();


                    panel_history_macut.Visible = true;
                    SelectLogMACut();


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเลขทะเบียนอุปกรณ์');", true);
                }
              

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelCut":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSearchCutBack":

                //MvMaster.SetActiveView(ViewNetworkDevicesCut);

                FvCutNetworkDevices.ChangeMode(FormViewMode.Insert);
                FvCutNetworkDevices.DataBind();

                
                FvCutNetworkDevices.Visible = true;

                fvBacktoSearchCut.Visible = false;
                //SearchDetailCut();
                FvDetailShowCut.Visible = false;
                //gvFileCut.Visible = false;

                div_savecut.Visible = false;
                div_statecut.Visible = false;

                panel_history_repaircut.Visible = false;
                panel_history_macut.Visible = false;

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSaveCut":

               
                DropDownList _ddl_statuscut = (DropDownList)ViewNetworkDevicesCut.FindControl("ddl_statuscut");
                TextBox _lbu0idx_networkcut = (TextBox)FvDetailShowCut.FindControl("lbu0idx_networkcut");

                TextBox _txtregister_numberdetailcut = (TextBox)FvDetailShowCut.FindControl("txtregister_numberdetailcut");
                TextBox _txtasset_codecut = (TextBox)FvDetailShowCut.FindControl("txtasset_codecut");
                TextBox _txtbrand_namecut = (TextBox)FvDetailShowCut.FindControl("txtbrand_namecut");
                TextBox _txtgeneration_namecut = (TextBox)FvDetailShowCut.FindControl("txtgeneration_namecut");
                TextBox _txtdiff_datecut = (TextBox)FvDetailShowCut.FindControl("txtdiff_datecut");

                _cemp_idx = emp_idx;

             
                _data_network.cutnetwork_list = new cutnetwork_detail[1];
                _cutnetwork_detail.u0_devicenetwork_idx = int.Parse(_lbu0idx_networkcut.Text);
                _cutnetwork_detail.m0_node_idx = 7;//node;
                _cutnetwork_detail.m0_actor_idx = 1;//actor;
                _cutnetwork_detail.cemp_idx_create = _cemp_idx;
                _cutnetwork_detail.doc_decision = int.Parse(_ddl_statuscut.SelectedValue);


                //ข้อมูลส่งเมล
                _cutnetwork_detail.name_create = ViewState["emp_name_th"].ToString();
                _cutnetwork_detail.pos_name_th = ViewState["pos_name_th"].ToString();
                _cutnetwork_detail.register_number = _txtregister_numberdetailcut.Text;
                _cutnetwork_detail.asset_code = _txtasset_codecut.Text;
                _cutnetwork_detail.date_create = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt");
                _cutnetwork_detail.brand_name = _txtbrand_namecut.Text;
                _cutnetwork_detail.generation_name = _txtgeneration_namecut.Text;
                _cutnetwork_detail.diff_date = _txtdiff_datecut.Text;

                //ข้อมูลส่งเมล
                _data_network.cutnetwork_list[0] = _cutnetwork_detail;

                string tomailcut = "waraporn.teoy@gmail.com";
                //string tomailcut = "nipon.r@taokaenoi.co.th";
                string replyempcreatecut = "basis@taokaenoi.co.th";
                ////string replyempcreate = "nipon@taokeanoi.co.th";//ViewState["emp_email"].ToString();  

                _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numberdetailcut.Text;
                _mail_body = servicemail.networkCut(_data_network.cutnetwork_list[0]);
                //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                servicemail.SendHtmlFormattedEmailFull(tomailcut, "", replyempcreatecut, _mail_subject, _mail_body);


                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlSetCutNetwork, _data_network);

                if (_data_network.return_code == 0)
                {          
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);    

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }

                break;

            case "btnSearchMove":

                //FormView FvCutNetworkDevices = (FormView)ViewNetworkDevicesCut.FindControl("FvCutNetworkDevices");
                DropDownList _ddlregister_numbermove = (DropDownList)FvMoveNetworkDevices.FindControl("ddlregister_numbermove");

                ViewState["_ddlregister_numbermove"] = _ddlregister_numbermove.SelectedValue;
                ViewState["register_numbermove"] = _ddlregister_numbermove.SelectedItem.ToString();

                //FvDetailShowCut.ChangeMode(FormViewMode.Insert);
                if (int.Parse(ViewState["_ddlregister_numbermove"].ToString()) != 0)

                {

                    FvMoveNetworkDevices.Visible = false;

                    fvBacktoSearchMove.Visible = true;
                    SearchDetailMove();

                    div_statemove.Visible = true;
                    ddlstatus_move.SelectedValue = "0";


                    try
                    {

                        string getPathFileMove = ConfigurationManager.AppSettings["upload_networkdevices_file"];

                        string filePathMove = Server.MapPath(getPathFileMove + ViewState["register_numbermove"].ToString());
                        DirectoryInfo myDirMove = new DirectoryInfo(filePathMove);

                        SearchDirectoriesMove(myDirMove, ViewState["register_numbermove"].ToString());

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);

                    }
                    catch
                    {

                    }

                    actionddlplacemove();


                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเลขทะเบียนอุปกรณ์');", true);
                }
             

                break;
            case "btnSearchMoveBack":

                //MvMaster.SetActiveView(ViewNetworkDevicesCut);

                FvMoveNetworkDevices.ChangeMode(FormViewMode.Insert);
                FvMoveNetworkDevices.DataBind();

                FvMoveNetworkDevices.Visible = true;

                fvBacktoSearchMove.Visible = false;
                FvDetailShowMove.Visible = false;
                             
                div_statemove.Visible = false;

               
                break;

            case "btnCancelMove":
             
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsertMove":

                DropDownList _ddlstatus_move = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlstatus_move");
                DropDownList _ddlplacemove = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlplacemove");
                DropDownList _ddlroommove = (DropDownList)ViewNetworkDevicesMove.FindControl("ddlroommove");
                TextBox _txtdetailmove = (TextBox)ViewNetworkDevicesMove.FindControl("txtdetailmove");
                TextBox _lbu0idx_networkmove = (TextBox)FvDetailShowMove.FindControl("lbu0idx_networkmove");


                TextBox _txtregister_numberdetailmovemail = (TextBox)FvDetailShowMove.FindControl("txtregister_numberdetailmove");
                TextBox _txtasset_codemovemail = (TextBox)FvDetailShowMove.FindControl("txtasset_codemove");
                TextBox _txtbrand_namemovemail = (TextBox)FvDetailShowMove.FindControl("txtbrand_namemove");
                TextBox _txtgeneration_namemovemail = (TextBox)FvDetailShowMove.FindControl("txtgeneration_namemove");
                TextBox _txtplace_namemoveoldmail = (TextBox)FvDetailShowMove.FindControl("txtplace_namemove");
                TextBox _txtroom_namemoveoldmail = (TextBox)FvDetailShowMove.FindControl("txtroom_namemove");

                _cemp_idx = emp_idx;

                _data_network.movenetwork_list = new movenetwork_detail[1];
                _movenetwork_detail.u0_devicenetwork_idx = int.Parse(_lbu0idx_networkmove.Text);
                _movenetwork_detail.place_new_idx = int.Parse(_ddlplacemove.SelectedValue);
                _movenetwork_detail.room_new_idx = int.Parse(_ddlroommove.SelectedValue);
                _movenetwork_detail.m0_node_idx = 8;//node;
                _movenetwork_detail.m0_actor_idx = 1;//actor;
                _movenetwork_detail.cemp_idx_create = _cemp_idx;
                _movenetwork_detail.doc_decision = int.Parse(_ddlstatus_move.SelectedValue);
                _movenetwork_detail.detail_devices = _txtdetailmove.Text;


                //ข้อมูลส่งเมล
                
                _movenetwork_detail.name_create = ViewState["emp_name_th"].ToString();
                _movenetwork_detail.pos_name_th = ViewState["pos_name_th"].ToString();
                _movenetwork_detail.date_create = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt"); 
                _movenetwork_detail.register_number = _txtregister_numberdetailmovemail.Text;
                _movenetwork_detail.asset_code = _txtasset_codemovemail.Text;
                _movenetwork_detail.brand_name = _txtbrand_namemovemail.Text;
                _movenetwork_detail.generation_name = _txtgeneration_namemovemail.Text;
                _movenetwork_detail.place_name = _txtplace_namemoveoldmail.Text;
                _movenetwork_detail.room_name = _txtroom_namemoveoldmail.Text;
                _movenetwork_detail.place_new_name = _ddlplacemove.SelectedItem.ToString();

                if(int.Parse(_ddlroommove.SelectedValue) != 0)
                {
                    _movenetwork_detail.room_new_name = _ddlroommove.SelectedItem.ToString();
                }
                else
                {
                    _movenetwork_detail.room_new_name = "-";
                }


                _data_network.movenetwork_list[0] = _movenetwork_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));


                string emailmove = "waraporn.teoy@gmail.com";
                //string emailmove = "nipon.r@taokaenoi.co.th";
                string replyempmove = "waraporn.teoy@gmail.com";
                ////string replyempmove = "nipon@taokeanoi.co.th";

                _mail_subject = "[MIS/Network : Network Devices] - " + _txtregister_numberdetailmovemail.Text;
                _mail_body = servicemail.networkMove(_data_network.movenetwork_list[0]);
                //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);


                _data_network = callServiceDataNetwork(_urlSetMoveNetwork, _data_network);

                if (_data_network.return_code == 0)
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);   
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    //setError(_data_network.return_code.ToString() + " - " + _data_network.return_msg);
                }

                break;
         

            case "btnSearchReport":

                btnExport.Visible = true;
                gridviewreport.Visible = true;

                DropDownList _ddlplacesearchreport = (DropDownList)ViewReport.FindControl("ddlplacesearchreport");
                DropDownList _ddlroomsearchreport = (DropDownList)ViewReport.FindControl("ddlroomsearchreport");
                DropDownList _ddltypeidxsearchreport = (DropDownList)ViewReport.FindControl("ddltypeidxsearchreport");
                DropDownList _ddlcategoryidxsearchreport = (DropDownList)ViewReport.FindControl("ddlcategoryidxsearchreport");
                TextBox _txtbrandsearchreport = (TextBox)ViewReport.FindControl("txtbrandsearchreport");
                TextBox _txtgenerationsearchreport = (TextBox)ViewReport.FindControl("txtgenerationsearchreport");
                DropDownList _ddlnamesearchreport = (DropDownList)ViewReport.FindControl("ddlnamesearchreport");
                DropDownList _ddlcompanyidxsearchreport = (DropDownList)ViewReport.FindControl("ddlcompanyidxsearchreport");

                TextBox _txtipaddresssearchreport = (TextBox)ViewReport.FindControl("txtipaddresssearchreport");
                TextBox _txtserialsearchreport = (TextBox)ViewReport.FindControl("txtserialsearchreport");
                TextBox _txtregistersearchreport = (TextBox)ViewReport.FindControl("txtregistersearchreport");

                DropDownList _ddlSearchDatereport = (DropDownList)ViewReport.FindControl("ddlSearchDatereport");
                TextBox _txtdatepurchasesearchreport = (TextBox)ViewReport.FindControl("txtdatepurchasesearchreport");
                TextBox _txtdateexpirsearchreport = (TextBox)ViewReport.FindControl("txtdateexpirsearchreport");

                _cemp_idx = emp_idx;

                ViewState["_ddlplacesearchreport"] = int.Parse(ddlplacesearchreport.SelectedValue);
                ViewState["_ddlroomsearchreport"] = int.Parse(_ddlroomsearchreport.SelectedValue);
                ViewState["_ddltypeidxsearchreport"] = int.Parse(_ddltypeidxsearchreport.SelectedValue);
                ViewState["_ddlcategoryidxsearchreport"] = int.Parse(_ddlcategoryidxsearchreport.SelectedValue);
                ViewState["_txtbrandsearchreport"] = _txtbrandsearchreport.Text;
                ViewState["_txtgenerationsearchreport"] = _txtgenerationsearchreport.Text;
                ViewState["_ddlnamesearchreport"] = int.Parse(_ddlnamesearchreport.SelectedValue);
                ViewState["_ddlcompanyidxsearchreport"] = int.Parse(_ddlcompanyidxsearchreport.SelectedValue);
                ViewState["_txtipaddresssearchreport"] = _txtipaddresssearchreport.Text;
                ViewState["_txtregistersearchreport"] = _txtregistersearchreport.Text;
                ViewState["_ddlSearchDatereport"] = int.Parse(_ddlSearchDatereport.SelectedValue);
                ViewState["_txtdatepurchasesearchreport"] = _txtdatepurchasesearchreport.Text;
                ViewState["_txtdateexpirsearchreport"] = _txtdateexpirsearchreport.Text;

                _data_network.inexportnetwork_list = new inexportnetwork_detail[1];

                _inexportnetwork_detail.place_idx = int.Parse(_ddlplacesearchreport.SelectedValue);
                _inexportnetwork_detail.room_idx = int.Parse(_ddlroomsearchreport.SelectedValue);
                _inexportnetwork_detail.type_idx = int.Parse(_ddltypeidxsearchreport.SelectedValue);
                _inexportnetwork_detail.category_idx = int.Parse(_ddlcategoryidxsearchreport.SelectedValue);
                _inexportnetwork_detail.brand_name = _txtbrandsearchreport.Text;
                _inexportnetwork_detail.generation_name = _txtgenerationsearchreport.Text;
                _inexportnetwork_detail.cemp_idx_create = int.Parse(_ddlnamesearchreport.SelectedValue);
                _inexportnetwork_detail.company_idx = int.Parse(_ddlcompanyidxsearchreport.SelectedValue);
                _inexportnetwork_detail.ip_address = _txtipaddresssearchreport.Text;
                _inexportnetwork_detail.serial_number = _txtserialsearchreport.Text;
                _inexportnetwork_detail.register_number = _txtregistersearchreport.Text;

                _inexportnetwork_detail.if_date = int.Parse(_ddlSearchDatereport.SelectedValue);
                _inexportnetwork_detail.date_purchase = _txtdatepurchasesearchreport.Text;
                _inexportnetwork_detail.date_expire = _txtdateexpirsearchreport.Text;

                _data_network.inexportnetwork_list[0] = _inexportnetwork_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlGetSearchReport, _data_network);

                ViewState["bind_data_report"] = _data_network.inexportnetwork_list;

                GvReport.PageIndex = 0;
                setGridData(GvReport, ViewState["bind_data_report"]);
                
                ////var _linqFilterSelf = from data in _data_network.bindnetwork_list
                //where data.menu_idx != int.Parse(hfMenuIdx.Value)
                // select data;

                break;

            case "btnExport":

                btnExport.Visible = true;

                DropDownList _ddlplacesearchexport = (DropDownList)ViewReport.FindControl("ddlplacesearchreport");
                DropDownList _ddlroomsearchexport = (DropDownList)ViewReport.FindControl("ddlroomsearchreport");
                DropDownList _ddltypeidxsearchexport = (DropDownList)ViewReport.FindControl("ddltypeidxsearchreport");
                DropDownList _ddlcategoryidxsearchexport = (DropDownList)ViewReport.FindControl("ddlcategoryidxsearchreport");
                TextBox _txtbrandsearchexport = (TextBox)ViewReport.FindControl("txtbrandsearchreport");
                TextBox _txtgenerationsearchexport = (TextBox)ViewReport.FindControl("txtgenerationsearchreport");
                DropDownList _ddlnamesearchexport = (DropDownList)ViewReport.FindControl("ddlnamesearchreport");
                DropDownList _ddlcompanyidxsearchexport = (DropDownList)ViewReport.FindControl("ddlcompanyidxsearchreport");

                TextBox _txtipaddresssearchexport = (TextBox)ViewReport.FindControl("txtipaddresssearchreport");
                TextBox _txtserialsearchexport = (TextBox)ViewReport.FindControl("txtserialsearchreport");
                TextBox _txtregistersearchexport = (TextBox)ViewReport.FindControl("txtregistersearchreport");

                DropDownList _ddlSearchDateexport = (DropDownList)ViewReport.FindControl("ddlSearchDatereport");
                TextBox _txtdatepurchasesearchexport = (TextBox)ViewReport.FindControl("txtdatepurchasesearchreport");
                TextBox _txtdateexpirsearchexport = (TextBox)ViewReport.FindControl("txtdateexpirsearchreport");

                _cemp_idx = emp_idx;

                //data_network _data_networkexport = new data_network();
                _data_network.inexportnetwork_list = new inexportnetwork_detail[1];
                // _data_network.exportnetwork_list = new exportnetwork_detail[1];

                _inexportnetwork_detail.place_idx = int.Parse(_ddlplacesearchexport.SelectedValue);
                _inexportnetwork_detail.room_idx = int.Parse(_ddlroomsearchexport.SelectedValue);
                _inexportnetwork_detail.type_idx = int.Parse(_ddltypeidxsearchexport.SelectedValue);
                _inexportnetwork_detail.category_idx = int.Parse(_ddlcategoryidxsearchexport.SelectedValue);
                _inexportnetwork_detail.brand_name = _txtbrandsearchexport.Text;
                _inexportnetwork_detail.generation_name = _txtgenerationsearchexport.Text;
                _inexportnetwork_detail.cemp_idx_create = int.Parse(_ddlnamesearchexport.SelectedValue);
                _inexportnetwork_detail.company_idx = int.Parse(_ddlcompanyidxsearchexport.SelectedValue);
                _inexportnetwork_detail.ip_address = _txtipaddresssearchexport.Text;
                _inexportnetwork_detail.serial_number = _txtserialsearchexport.Text;
                _inexportnetwork_detail.register_number = _txtregistersearchexport.Text;

                _inexportnetwork_detail.if_date = int.Parse(_ddlSearchDateexport.SelectedValue);
                _inexportnetwork_detail.date_purchase = _txtdatepurchasesearchexport.Text;
                _inexportnetwork_detail.date_expire = _txtdateexpirsearchexport.Text;

                _data_network.inexportnetwork_list[0] = _inexportnetwork_detail;

                ////_data_network.exportnetwork_list[0] = _exportnetwork_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network));

                _data_network = callServiceDataNetwork(_urlGetExportExcel, _data_network);

                ///litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_network.exportnetwork_list));


                GvShowExportExcel.DataSource = _data_network.exportnetwork_list;
                GvShowExportExcel.DataBind();

                GvShowExportExcel.AllowSorting = false;
                GvShowExportExcel.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");

                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GvShowExportExcel.Columns[0].Visible = true;
                GvShowExportExcel.HeaderRow.BackColor = Color.White;

                foreach (GridViewRow row in GvShowExportExcel.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GvShowExportExcel.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GvShowExportExcel.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }


                GvShowExportExcel.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                //////Set_Null();


                break;


        }
    }
    #endregion btnCommand

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        btnToIndexList.BackColor = System.Drawing.Color.LightGray;
        btnToDeviceNetwork.BackColor = System.Drawing.Color.Transparent;
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {
        
        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
       
        m0_node = int.Parse(hf_m0_node_idx.Value);// int.Parse(ViewState["M0_Node"].ToString());
      
        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;

            case 2: // ผู้อนุมติ สร้าง

                setFormDataActor();

                break;
            case 3: // ผู้อนุมติ ma

                setFormDataActor();
                break;

            case 4: // ผู้อนุมติ ตัดชำรุด

                setFormDataActor();

                break;

            case 5: // ผู้อนุมติ ย้าย

                setFormDataActor();

                break;

            case 11: // ผู้อนุมติ การแก้ไข

                setFormDataActor();

                break;

        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
        HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");
        HiddenField hf_doc_decision = (HiddenField)FvViewDetail.FindControl("hf_doc_decision");

        Panel div_showplacemove = (Panel)FvViewDetail.FindControl("div_showplacemove");
        Panel div_showcompanyma = (Panel)FvViewDetail.FindControl("div_showcompanyma");
        //Panel div_showedit = (Panel)FvViewDetail.FindControl("div_showedit");

        //FormView FvDetailShowCut = (FormView)ViewNetworkDevicesCut.FindControl("FvDetailShowCut");

        m0_node = int.Parse(hf_m0_node_idx.Value);
        m0_actor = int.Parse(hf_m0_actor_idx.Value);
        m0_status = int.Parse(hf_doc_decision.Value);

        ViewState["m0_node_idx"] = m0_node;
      
        var btnsaveapprove = (LinkButton)ViewIndex.FindControl("btnsaveapprove");
       

        switch (m0_actor)
        {
            case 1:

                if((emp_idx != 0 && emp_idx != 174) || (emp_idx != 0 && emp_idx != 1394))
                {

                    
                        div_btn.Visible = true;
                        FvViewDetail.ChangeMode(FormViewMode.ReadOnly);
                        FvViewDetail.DataBind();

                }

                break;

            case 2:
                if (emp_idx == 174 || emp_idx == 1394)
                {
                    //ผู้อนุมัติ พิจารณาผลการสร้าง
                    if ((m0_actor == 2 && m0_node == 2))
                    {
                        div_btn.Visible = false;
                       
                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;


                    }

                    //ผู้อนุมัติ พิจารณาผลการแก้ไข
                    else if ((m0_actor == 2 && m0_node == 11))
                    {
                        SelectViewEditIndex();

                        div_btn.Visible = false;


                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;

                        div_showedit.Visible = true;

                        ///////  แสดงรายละเอียดการแก้ไข  ///////////

                        //place_idxedit
                        lbplace_idxedit.Text = ViewState["place_idxedit"].ToString();

                        //place_nameedit
                        txtplace_nameedit.Text = ViewState["place_nameedit"].ToString();

                        //room_idxedit                     
                        lbroom_idxedit.Text = ViewState["room_idxedit"].ToString();

                        //room_nameedit                     
                        txtroom_nameedit.Text = ViewState["room_nameedit"].ToString();
                        

                        //brand_nameedit
                        txtbrand_nameedit.Text = ViewState["brand_nameedit"].ToString();


                        //generation_nameedit
                        txtgeneration_nameedit.Text = ViewState["generation_nameedit"].ToString();

                        //type_idxedit
                        lbtype_idxedit.Text = ViewState["type_idxedit"].ToString();

                        //type_nameedit
                        txttype_nameedit.Text = ViewState["type_nameedit"].ToString();

                        //category_idxedit
                        lbcategory_idxedit.Text = ViewState["category_idxedit"].ToString();

                        //category_namedit                      
                        txtcategory_nameedit.Text = ViewState["category_namedit"].ToString();
                                              
                        //ip_addressdit
                        txtip_addressedit.Text = ViewState["ip_addressdit"].ToString();

                        //serial_numberedit
                        txtserial_numberedit.Text = ViewState["serial_numberedit"].ToString();
                    
                        //////user_nameedit
                        ////txtuser_nameedit.Text = ViewState["user_nameedit"].ToString();

                        //////pass_wordedit
                        ////txtpass_wordedit.Text = ViewState["pass_wordedit"].ToString();

                        //asset_codeedit
                        txtasset_codeedit.Text = ViewState["asset_codeedit"].ToString();

                        //status_deviecsedit
                        txtstatus_deviecsedit.Text = ViewState["status_deviecsedit"].ToString();



                        ///////  แสดงรายละเอียดการแก้ไข  ///////////     



                    }

                    //ผู้อนุมัติ พิจารณาผลการย้าย
                    else if ((m0_actor == 2 && m0_node == 5))                  
                    {
                        div_btn.Visible = false;
                        

                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;

                        div_showplacemove.Visible = true;
                        ////div_showedit.Visible = true;



                    }
                    //ผู้อนุมัติ พิจารณาผล ma
                    else if ((m0_actor == 2 && m0_node == 3))
                    {
                        div_btn.Visible = false;


                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;

                        div_showcompanyma.Visible = true;
                        //div_showplacemove.Visible = true;


                    }
                    //ผู้อนุมัติ พิจารณาผลการตัดชำรุด
                    else if ((m0_actor == 2 && m0_node == 4))
                    {
                        div_btn.Visible = false;


                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;

                        //div_showplacemove.Visible = true;


                    }
                 

                    else
                    {
                        //div_showplacemove.Visible = true;

                        div_btn.Visible = true;
                        btnsaveapprove.Visible = false;                                            

                    }
                }
                else if(emp_idx != 0)
                {
                    //คนอื่นดู พิจารณาผลการสร้าง
                    if (m0_actor == 2 && m0_node == 2)
                    {
                        div_btn.Visible = true;

                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;
                       

                    }
                    //คนอื่น ดูผลการตัดชำรุด
                    else if (m0_actor == 2 && m0_node == 4)
                    {
                        div_btn.Visible = true;

                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;


                    }
                    //คนอื่น ดูผลการย้าย
                    else if (m0_actor == 2 && m0_node == 5)
                    {
                        div_btn.Visible = true;


                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;
                        btnsaveapprove.Visible = false;

                        div_showplacemove.Visible = true;
                      

                    }
                    //คนอื่น ดูผล ma
                    else if (m0_actor == 2 && m0_node == 3)
                    {
                        div_btn.Visible = true;


                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;
                        btnsaveapprove.Visible = false;

                        div_showcompanyma.Visible = true;


                    }

                    //คนอื่น ดูผล แก้ไข
                    else if (m0_actor == 2 && m0_node == 11)
                    {

                        SelectViewEditIndex();

                        div_btn.Visible = true;


                        div_approvstatecreate.Visible = false;
                        div_approve.Visible = false;
                        btnsaveapprove.Visible = false;

                        div_showedit.Visible = true;

                        div_showedit.Visible = true;

                        ///////  แสดงรายละเอียดการแก้ไข  ///////////

                        //place_idxedit
                        lbplace_idxedit.Text = ViewState["place_idxedit"].ToString();

                        //place_nameedit
                        txtplace_nameedit.Text = ViewState["place_nameedit"].ToString();

                        //room_idxedit                     
                        lbroom_idxedit.Text = ViewState["room_idxedit"].ToString();

                        //room_nameedit                     
                        txtroom_nameedit.Text = ViewState["room_nameedit"].ToString();


                        //brand_nameedit
                        txtbrand_nameedit.Text = ViewState["brand_nameedit"].ToString();


                        //generation_nameedit
                        txtgeneration_nameedit.Text = ViewState["generation_nameedit"].ToString();

                        //type_idxedit
                        lbtype_idxedit.Text = ViewState["type_idxedit"].ToString();

                        //type_nameedit
                        txttype_nameedit.Text = ViewState["type_nameedit"].ToString();

                        //category_idxedit
                        lbcategory_idxedit.Text = ViewState["category_idxedit"].ToString();

                        //category_namedit                      
                        txtcategory_nameedit.Text = ViewState["category_namedit"].ToString();

                        //ip_addressdit
                        txtip_addressedit.Text = ViewState["ip_addressdit"].ToString();

                        //serial_numberedit
                        txtserial_numberedit.Text = ViewState["serial_numberedit"].ToString();

                        //////user_nameedit
                        ////txtuser_nameedit.Text = ViewState["user_nameedit"].ToString();

                        //////pass_wordedit
                        ////txtpass_wordedit.Text = ViewState["pass_wordedit"].ToString();

                        //asset_codeedit
                        txtasset_codeedit.Text = ViewState["asset_codeedit"].ToString();

                        //status_deviecsedit
                        txtstatus_deviecsedit.Text = ViewState["status_deviecsedit"].ToString();



                        ///////  แสดงรายละเอียดการแก้ไข  ///////////     


                    }

                    else
                    {
                        //div_showplacemove.Visible = true;

                        div_btn.Visible = true;
                        btnsaveapprove.Visible = false;


                    }
                }
                else
                {

                   
                    FvViewDetail.ChangeMode(FormViewMode.ReadOnly);
                    FvViewDetail.DataBind();

                    //FvDetailShowCut.ChangeMode(FormViewMode.ReadOnly);
                    //FvDetailShowCut.DataBind();
                }

                break;

            case 4:
                if (emp_idx == 174 || emp_idx == 1394)
                {
                    if (m0_actor == 2)
                    {
                        div_btn.Visible = false;


                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;

                    }
                    else
                    {
                        div_btn.Visible = true;
                        btnsaveapprove.Visible = false;
                    }
                }
                else
                {
                    FvViewDetail.ChangeMode(FormViewMode.ReadOnly);
                    FvViewDetail.DataBind();

                   
                }

                break;

            
        }

    }

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
          
            case "FvInsert":

                FormView FvInsert = (FormView)ViewNetworkDevices.FindControl("FvInsert");

                if (FvInsert.CurrentMode == FormViewMode.Insert)
                {
                
                    var txtnamecreate = ((TextBox)FvInsert.FindControl("txtnamecreate"));
                    var txtdeptcreate = ((TextBox)FvInsert.FindControl("txtdeptcreate"));
                    var txtseccreate = ((TextBox)FvInsert.FindControl("txtseccreate"));
                    var txtposcreate = ((TextBox)FvInsert.FindControl("txtposcreate"));
                    var txtemailcreate = ((TextBox)FvInsert.FindControl("txtemailcreate"));
                    var txttelcreate = ((TextBox)FvInsert.FindControl("txttelcreate"));

                    //var txtrequesname1 = ((Label)ViewNetworkDevices.FindControl("txtrequesname1"));

                    txtnamecreate.Text = ViewState["emp_name_th"].ToString();
                    txtdeptcreate.Text = ViewState["dept_name_th"].ToString();
                    txtseccreate.Text = ViewState["sec_name_th"].ToString();
                    txtposcreate.Text = ViewState["pos_name_th"].ToString();
                    txtemailcreate.Text = ViewState["emp_email"].ToString();
                    txttelcreate.Text = ViewState["emp_mobile_no"].ToString();

                }
                break;

            case "FvAddNetworkDevices":

                
                 UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");       
                LinkButton btnInsert = (LinkButton)FvAddNetworkDevices.FindControl("btnInsert");
                DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");


                if (FvAddNetworkDevices.CurrentMode == FormViewMode.Insert)
                {
                    actionddltype();
                    actionddlcompany();
                    actionddlplace();                   

                    var txtbrandadd = ((TextBox)FvAddNetworkDevices.FindControl("txtbrandadd"));
                    var txtgenerationadd = ((TextBox)FvAddNetworkDevices.FindControl("txtgenerationadd"));

                    linkBtnTrigger(btnInsert);
                    //ddlTrigger(ddltypeidxadd);
                }

                break;

            case "FvViewDetail":

                FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");

                if (FvViewDetail.CurrentMode == FormViewMode.ReadOnly)
                {
                    



                }

                if (FvViewDetail.CurrentMode == FormViewMode.Edit)
                {
                    Label lbtype_idxedit = (Label)FvViewDetail.FindControl("lbtype_idxedit");
                    DropDownList ddltype_nameedit = (DropDownList)FvViewDetail.FindControl("ddltype_nameedit");
                 
                    //แก้ไขประเภทอุปกรณ์
                    ddltype_nameedit.AppendDataBoundItems = true;

                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetail = new m0category_detail();

                    _data_networkdevices.m0category_list[0] = _m0categoryDetail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);

                    ddltype_nameedit.DataSource = _data_networkdevices.m0category_list;
                    ddltype_nameedit.DataTextField = "type_name";
                    ddltype_nameedit.DataValueField = "type_idx";
                    ddltype_nameedit.DataBind();
                    ddltype_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกชื่อประเภทอุปกรณ์....", "0"));
                    ddltype_nameedit.SelectedValue = lbtype_idxedit.Text;


                    //edit ชนิดอุปกรณ์

                    Label lbcategory_idxedit = (Label)FvViewDetail.FindControl("lbcategory_idxedit");
                    DropDownList ddlcategory_nameedit = (DropDownList)FvViewDetail.FindControl("ddlcategory_nameedit");

                    ddlcategory_nameedit.AppendDataBoundItems = true;
                   
                    _data_networkdevices.m0category_list = new m0category_detail[1];
                    m0category_detail _m0categoryDetailedit = new m0category_detail();

                    //_m0categoryDetailedit.type_idx = int.Parse(ddltype_nameedit.SelectedValue);

                    _data_networkdevices.m0category_list[0] = _m0categoryDetailedit;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Category, _data_networkdevices);


                    ddlcategory_nameedit.DataSource = _data_networkdevices.m0category_list;
                    ddlcategory_nameedit.DataTextField = "category_name";
                    ddlcategory_nameedit.DataValueField = "category_idx";
                    ddlcategory_nameedit.DataBind();
                    ddlcategory_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));
                    ddlcategory_nameedit.SelectedValue = lbcategory_idxedit.Text;


                    //แก้ไขสถานที่ติดตั้ง
                    Label lbplace_idxedit = (Label)FvViewDetail.FindControl("lbplace_idxedit");
                    DropDownList ddltxtplace_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtplace_nameedit");


                    ddltxtplace_nameedit.AppendDataBoundItems = true;

                    data_networkdevices _data_networkdevicesedit = new data_networkdevices();
                    _data_networkdevicesedit.m0place_list = new m0place_detail[1];

                    m0place_detail _m0place_detailedit = new m0place_detail();

                    _m0place_detailedit.place_idx = 0;

                    _data_networkdevicesedit.m0place_list[0] = _m0place_detailedit;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_networkdevices = callServiceNetwork(_urlGetm0Place, _data_networkdevicesedit);

                    ddltxtplace_nameedit.DataSource = _data_networkdevices.m0place_list;
                    ddltxtplace_nameedit.DataTextField = "place_name";
                    ddltxtplace_nameedit.DataValueField = "place_idx";
                    ddltxtplace_nameedit.DataBind();
                    ddltxtplace_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่ติดตั้ง....", "0"));
                    ddltxtplace_nameedit.SelectedValue = lbplace_idxedit.Text;

                    //แก้ไข อาคาร
                    Label lbroom_idxedit = (Label)FvViewDetail.FindControl("lbroom_idxedit");
                    DropDownList ddltxtroom_nameedit = (DropDownList)FvViewDetail.FindControl("ddltxtroom_nameedit");

                    ddltxtroom_nameedit.AppendDataBoundItems = true;

                    //data_networkdevices _data_networkdevicesedit = new data_networkdevices();
                    _data_networkdevicesedit.m0room_list = new m0room_detail[1];

                    m0room_detail _m0room_detailedit = new m0room_detail();

                    _m0room_detailedit.room_idx = 0;

                    _data_networkdevicesedit.m0room_list[0] = _m0room_detailedit;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevicesedit));
                    _data_networkdevicesedit = callServiceNetwork(_urlGetm0Room, _data_networkdevicesedit);

                    ddltxtroom_nameedit.DataSource = _data_networkdevicesedit.m0room_list;
                    ddltxtroom_nameedit.DataTextField = "room_name";
                    ddltxtroom_nameedit.DataValueField = "room_idx";
                    ddltxtroom_nameedit.DataBind();
                    ddltxtroom_nameedit.Items.Insert(0, new ListItem("กรุณาเลือกอาคาร ....", "0"));
                    ddltxtroom_nameedit.SelectedValue = lbroom_idxedit.Text;



                }
                break;

            case "FvViewRepair":

                FormView FvViewRepair = (FormView)ViewIndex.FindControl("FvViewDetail");

                if (FvViewRepair.CurrentMode == FormViewMode.Insert)
                {

                    
                }

                break;

            case "FvViewMA":

                FormView FvViewMA = (FormView)ViewIndex.FindControl("FvViewMA");

                if (FvViewMA.CurrentMode == FormViewMode.Insert)
                {

                }

                break;

            case "FvViewDetailMA":

                FormView FvViewDetailMA = (FormView)ViewIndex.FindControl("FvViewMA");

                if (FvViewDetailMA.CurrentMode == FormViewMode.Insert)
                {

                  
                }

                if (FvViewDetailMA.CurrentMode == FormViewMode.ReadOnly)
                {

                   
                }

                break;

            case "FvViewDetailRepair":

                FormView FvViewDetailRepair = (FormView)ViewIndex.FindControl("FvViewDetailRepair");

                if (FvViewDetailRepair.CurrentMode == FormViewMode.Insert)
                {

                  
                }

                if (FvViewDetailRepair.CurrentMode == FormViewMode.ReadOnly)
                {

                   
                }

                break;

            case "FvCutNetworkDevices":

                //UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                FormView FvCutNetworkDevices = (FormView)ViewNetworkDevicesCut.FindControl("FvCutNetworkDevices");
               // LinkButton btnInsert = (LinkButton)FvAddNetworkDevices.FindControl("btnInsert");
                DropDownList ddlregister_numbercut = (DropDownList)FvCutNetworkDevices.FindControl("ddlregister_numbercut");


                if (FvCutNetworkDevices.CurrentMode == FormViewMode.Insert)
                {

                    ddlsearchcut();
                  
                }

                break;

            case "FvDetailShowCut":
               
                FormView FvDetailShowCut = (FormView)ViewNetworkDevicesCut.FindControl("FvDetailShowCut");

                if (FvDetailShowCut.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                    
                break;

            case "FvMoveNetworkDevices":
               
                FormView FvMoveNetworkDevices = (FormView)ViewNetworkDevicesMove.FindControl("FvMoveNetworkDevices");             
                DropDownList ddlregister_numbermove = (DropDownList)FvMoveNetworkDevices.FindControl("ddlregister_numbermove");

                if (FvMoveNetworkDevices.CurrentMode == FormViewMode.Insert)
                {

                    ddlsearchmove();

                }

                break;

            case "FvDetailShowMove":

                FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");

                if (FvDetailShowMove.CurrentMode == FormViewMode.ReadOnly)
                {

                }

                break;

            case "FvViewQRCode":

                FormView FvViewQRCode = (FormView)ViewIndex.FindControl("FvViewQRCode");

                if (FvViewQRCode.CurrentMode == FormViewMode.Insert)
                {

                }

                break;

            case "FvDetailShowQRCode":

                FormView FvDetailShowQRCode = (FormView)ViewDetailQrCode.FindControl("FvDetailShowQRCode");

                if (FvDetailShowQRCode.CurrentMode == FormViewMode.ReadOnly)
                {

                }

                break;

        }
    }

    #endregion

    #region Directories_File URL 


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        GridView gvFile = (GridView)FvViewDetail.FindControl("gvFile");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFile.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFile.DataSource = null;
            gvFile.DataBind();

        }



    }

    public void SearchDirectoriesCut(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        FormView FvDetailShowCut = (FormView)ViewNetworkDevicesCut.FindControl("FvDetailShowCut");

        GridView gvFileCut = (GridView)FvDetailShowCut.FindControl("gvFileCut");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFileCut.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFileCut.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFileCut.DataSource = null;
            gvFileCut.DataBind();

        }



    }

    public void SearchDirectoriesMove(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");
        GridView gvFileMove = (GridView)FvDetailShowMove.FindControl("gvFileMove");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFileMove.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFileMove.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFileMove.DataSource = null;
            gvFileMove.DataBind();

        }



    }

    public void SearchDirectoriesQRCode(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        if (files.Length > 0)
        {

            foreach (FileInfo file in files)
            {
                //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                //{
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt1.Rows.Add(file.Name);
                dt1.Rows[i][1] = f[0];
                i++;
                //}
            }

        }

        GridView gvFileQrCode = (GridView)FvViewDetail.FindControl("gvFileQrCode");
        // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            // gvFileLo1.Visible = true;
            gvFileQrCode.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvFileQrCode.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvFileQrCode.DataSource = null;
            gvFileQrCode.DataBind();

        }



    }

    #endregion

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtstatus_deviecsview = (Label)FvViewDetail.FindControl("txtstatus_deviecsview");

            return txtstatus_deviecsview.Text = "Online";
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtstatus_deviecsview = (Label)FvViewDetail.FindControl("txtstatus_deviecsview");

            return txtstatus_deviecsview.Text = "Offline";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    //protected string retrunpat_edit(string z)
    //{
    //    string getPath = ConfigurationSettings.AppSettings["upload_profile_picture"];
    //    string path = getPath + z.ToString() + "/" + z.ToString() + ".jpg";
    //    return path;

    //    //if(pat == )
    //}

    protected string getRoomView(string roomname_view)
    {
        if (roomname_view != null)
        {

            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtroom_nameview = (Label)FvViewDetail.FindControl("txtroom_nameview");

            return txtroom_nameview.Text = roomname_view;
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtroom_nameview = (Label)FvViewDetail.FindControl("txtroom_nameview");

            return txtroom_nameview.Text = "-";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getRoomname(string roomname_move)
    {
        if (roomname_move != null)
        {
           
            FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");
            TextBox txtroom_namemove = (TextBox)FvDetailShowMove.FindControl("txtroom_namemove");

            return txtroom_namemove.Text = roomname_move;

           
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvDetailShowMove = (FormView)ViewNetworkDevicesMove.FindControl("FvDetailShowMove");
            TextBox txtroom_namemove = (TextBox)FvDetailShowMove.FindControl("txtroom_namemove");

            return txtroom_namemove.Text = "-";//ViewState[roomname_move].ToString();
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string returnfile(string filenameregister)
    {
        string getPath = ConfigurationManager.AppSettings["upload_networkdevices_file"];
        string path = getPath + filenameregister.ToString() + "/" + filenameregister.ToString() + ".jpg";
        return path;

        //if(pat == )
    }

    protected string getPass(String passwordview)
    {
        if (passwordview != null)
        {

            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtpass_wordview = (Label)FvViewDetail.FindControl("txtpass_wordview");

            string pass = _funcTool.getDecryptRC4(passwordview.ToString(), _keynetworkdevices);

            return txtpass_wordview.Text = pass;
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            Label txtpass_wordview = (Label)FvViewDetail.FindControl("txtpass_wordview");

            //string pass = _funcTool.getDecryptRC4(txtpass_wordview.ToString(), "password");

            return txtpass_wordview.Text = "-";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getPassEdit(String passwordview)
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        TextBox txtpass_wordview = (TextBox)FvViewDetail.FindControl("txtpass_wordview");

        if (passwordview != null)
        {
        
            string pass = _funcTool.getDecryptRC4(passwordview.ToString(), _keynetworkdevices);

            return txtpass_wordview.Text = pass;
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            //FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
            //Label txtpass_wordview = (Label)FvViewDetail.FindControl("txtpass_wordview");

            //string pass = _funcTool.getDecryptRC4(txtpass_wordview.ToString(), "password");

            return txtpass_wordview.Text = "-";
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                setGridData(GvMaster, ViewState["bind_data_index"]);

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                GvMaster.Focus();
                //actionIndex();
                break;
            case "GvReport":

                setGridData(GvReport, ViewState["bind_data_report"]);

                GvReport.PageIndex = e.NewPageIndex;
                GvReport.DataBind();
                GvReport.Focus();
                ////actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvFile":
              
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "gvFileCut":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "gvFileMove":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "gvFileQrCode":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "GvMaster":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    /////////// เปิด-ปิด ปุ่ม   ///////////
                    var lbStatus_deviecs = (Label)e.Row.FindControl("lbStatus_deviecs");

                    var btnEditindex = (LinkButton)e.Row.FindControl("btnEditindex"); //ปุ่ม แก้ไข
                    var btnRepairindex = (LinkButton)e.Row.FindControl("btnRepairindex"); //ปุ่ม ซ่อม
                    var btnMAindex = (LinkButton)e.Row.FindControl("btnMAindex"); //ปุ่ม ma
                    var btnQrCodeindex = (LinkButton)e.Row.FindControl("btnQrCodeindex"); //ปุ่ม Gen qrcode


                    if (lbStatus_deviecs.Text == "1")
                    {                      
                        btnEditindex.Visible = true;
                        btnRepairindex.Visible = true;
                        btnMAindex.Visible = true;
                        btnQrCodeindex.Visible = true;
                    }
                    else
                    {
                        btnEditindex.Visible = false;
                        btnRepairindex.Visible = false;
                        btnMAindex.Visible = false;
                        btnQrCodeindex.Visible = false;
                    }

                    /////////// เปิด-ปิด ปุ่ม  ///////////

                }

                break;


        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;

            case "GvReport":
                GvReport.EditIndex = e.NewEditIndex;
                ////actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "GvReport":
                GvReport.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            //case "GvMaster":


            //    int category_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
            //    var ddlTypeNameUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlTypeNameUpdate");
            //    var txtCategoryNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtCategoryNameUpdate");
            //    var ddlCategoryStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlCategoryStatusUpdate");

            //    GvMaster.EditIndex = -1;

            //    _data_networkdevices.m0category_list = new m0category_detail[1];
            //    m0category_detail _m0category_detail = new m0category_detail();

            //    _m0category_detail.category_idx = category_idx_update;
            //    _m0category_detail.type_idx = int.Parse(ddlTypeNameUpdate.SelectedValue);
            //    _m0category_detail.category_name = txtCategoryNameUpdate.Text;
            //    _m0category_detail.category_status = int.Parse(ddlCategoryStatusUpdate.SelectedValue);
            //    _m0category_detail.cemp_idx = emp_idx;

            //    _data_networkdevices.m0category_list[0] = _m0category_detail;

            //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
            //    _data_networkdevices = callServiceNetwork(_urlSetm0Category, _data_networkdevices);

            //    if (_data_networkdevices.return_code == 0)
            //    {
            //        //initPage();
            //        //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
            //        actionIndex();


            //    }
            //    else
            //    {
            //        setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
            //    }


            //    break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {

            case "FvDetailShowCut":

              
                //setFormData(FvDetailShowCut, FormViewMode.Insert, _data_networkdetailcut.bindcutnetwork_list, 0);

                break;

        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_network callServiceDataNetwork(string _cmdUrl, data_network _data_network)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_network);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_network = (data_network)_funcTool.convertJsonToObject(typeof(data_network), _localJson);

        return _data_network;
    }

    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _data_networkdevices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_networkdevices);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_networkdevices = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _data_networkdevices;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }



    #endregion reuse

    #region reuse Triggers
    protected void btnTrigger(Button btnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = btnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void ddlTrigger(DropDownList ddlID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = ddlID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion Triggers

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    //#region  JS Scripts
    //protected void used_java_Scripts()
    //{
    //    string jsemp_start = String.Empty;
    //    string js_date_picker = String.Empty;
    //    string jsemp_end = String.Empty;
    //    string js_conn = "";

    //    jsemp_start +=

    // "var prm = Sys.WebForms.PageRequestManager.getInstance();" +
    // "prm.add_endRequest(function() {";
    //    js_date_picker +=
    //    "$(function () {" +
    //        "$('.from-date-datepicker').datetimepicker({" +
    //          "format: 'DD/MM/YYYY'" +
    //        "});" +

    //     "});";


    //    jsemp_end +=
    //    "});";

    //    js_conn = jsemp_start + js_date_picker + jsemp_end;
    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "", js_conn, true);
    //}
    //#endregion

    ////protected void upload_file_UploadComplete(object sender, AjaxFileUploadEventArgs e)
    ////{
    ////    //FormView FvAddNetworkDevices = (FormView)ViewNetworkDevices.FindControl("FvAddNetworkDevices");
    ////    //AjaxFileUpload upload_file = (AjaxFileUpload)FvAddNetworkDevices.FindControl("upload_file");

    ////    string getPath = ConfigurationManager.AppSettings["upload_networkdevices_file"];

    ////    string RECode1 = "1234";//ViewState["registerfilecode"].ToString();
    ////    string fileName1 = RECode1;
    ////    string filePath1 = Server.MapPath(getPath + RECode1);
    ////    if (!Directory.Exists(filePath1))
    ////    {
    ////        Directory.CreateDirectory(filePath1);
    ////    }

    ////    //string extension = ".jpg";// Path.GetExtension(upload_file.FileName);

    ////    //upload_file.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);

    ////    upload_file.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + e.FileName);

    ////    //string filePath = (Server.MapPath(getPath) + e.FileName);
    ////    //upload_file.SaveAs(filePath);




    ////}


    //start teppanop //
    private void create_asset()
    {
        ViewState["asset_u0idx"] = Session["_sesion_u0idx"];
        ViewState["asset_u2idx"] = Session["_sesion_u2idx"];
        ViewState["asset_u0_docket_idx"] = Session["_sesion_u0_docket_idx"];
        ViewState["asset_asset_no"] = Session["_sesion_asset_no"];
        ViewState["asset_pono"] = Session["_sesion_pono"];
        ViewState["asset_tdidx"] = Session["_sesion_tdidx"];
        ViewState["asset_devices"] = Session["_sesion_devices"];
       // ViewState["asset_devicestype"] = Session["_sesion_devicestype"];
        

        ViewState["asset_org_idx_its"] = Session["_sesion_org_idx_its"];
        ViewState["asset_rdept_idx_its"] = Session["_sesion_rdept_idx_its"];
        ViewState["asset_rsec_idx_its"] = Session["_sesion_rsec_idx_its"];

        Session.Remove("_sesion_u0idx");
        Session.Remove("_sesion_u2idx");
        Session.Remove("_sesion_u0_docket_idx");
        Session.Remove("_sesion_pono");
        Session.Remove("_sesion_tdidx");
        Session.Remove("_sesion_devices");
        Session.Remove("_sesion_org_idx_its");
        Session.Remove("_sesion_rdept_idx_its");
        Session.Remove("_sesion_rsec_idx_its");
       // Session.Remove("_sesion_devicestype");

        btnToIndexList.BackColor = System.Drawing.Color.Transparent;
        btnToDeviceNetwork.BackColor = System.Drawing.Color.LightGray;
        btnToDeviceNetworkMove.BackColor = System.Drawing.Color.Transparent;
        btnToDeviceNetworkCut.BackColor = System.Drawing.Color.Transparent;
        btnToReport.BackColor = System.Drawing.Color.Transparent;

        MvMaster.SetActiveView(ViewNetworkDevices);

        FvInsert.ChangeMode(FormViewMode.Insert);
        FvInsert.DataBind();

        FvAddNetworkDevices.ChangeMode(FormViewMode.Insert);
        FvAddNetworkDevices.DataBind();

        TextBox txt_add_acc = (TextBox)FvAddNetworkDevices.FindControl("txtassetcodeadd");
        DropDownList ddltypeidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddltypeidxadd");
        DropDownList ddlcategoryidxadd = (DropDownList)FvAddNetworkDevices.FindControl("ddlcategoryidxadd");

        txt_add_acc.Text = ViewState["asset_asset_no"].ToString();
        ddltypeidxadd.SelectedValue = "1";

        if (ddltypeidxadd.SelectedValue == "0")
        {

            ddltypeidxadd.Items.Clear();

            ddltypeidxadd.AppendDataBoundItems = true;
            ddltypeidxadd.Items.Add(new ListItem("กรุณาเลือกประเภทอุปกรณ์....", "0"));

            data_networkdevices _data_networkdevices = new data_networkdevices();
            _data_networkdevices.m0category_list = new m0category_detail[1];
            m0category_detail _m0categoryDetail = new m0category_detail();

            _data_networkdevices.m0category_list[0] = _m0categoryDetail;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

            _data_networkdevices = callServiceNetwork(_urlGetddltype, _data_networkdevices);



            ddltypeidxadd.DataSource = _data_networkdevices.m0category_list;
            ddltypeidxadd.DataTextField = "type_name";
            ddltypeidxadd.DataValueField = "type_idx";
            ddltypeidxadd.DataBind();
            ddltypeidxadd.SelectedValue = "0";

            ddlcategoryidxadd.Items.Clear();
            ddlcategoryidxadd.Items.Insert(0, new ListItem("กรุณาเลือกชนิดอุปกรณ์...", "0"));
            ddlcategoryidxadd.SelectedValue = "0";


        }
        else
        {

            ddlcategoryidxadd.AppendDataBoundItems = true;
            ddlcategoryidxadd.Items.Clear();

            ddlcategoryidxadd.Items.Add(new ListItem("กรุณาเลือกชนิดอุปกรณ์....", "0"));

            data_networkdevices _data_networkdevices = new data_networkdevices();
            _data_networkdevices.m0category_list = new m0category_detail[1];
            m0category_detail _m0categoryDetail = new m0category_detail();

            _m0categoryDetail.type_idx = int.Parse(ddltypeidxadd.SelectedValue);

            _data_networkdevices.m0category_list[0] = _m0categoryDetail;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

            _data_networkdevices = callServiceNetwork(_urlGetddlcatenetwork, _data_networkdevices);


            ddlcategoryidxadd.DataSource = _data_networkdevices.m0category_list;
            ddlcategoryidxadd.DataTextField = "category_name";
            ddlcategoryidxadd.DataValueField = "category_idx";
            ddlcategoryidxadd.DataBind();


        }
        ddlcategoryidxadd.SelectedValue = ViewState["asset_tdidx"].ToString();
      //  litDebug.Text = ViewState["asset_tdidx"].ToString();

    }
    //end teppanop //



}