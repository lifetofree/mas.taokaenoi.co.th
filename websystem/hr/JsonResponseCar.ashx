﻿<%@ WebHandler Language="C#" Class="JsonResponseCar" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;

public class JsonResponseCar : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";

        DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
        //try { start = Convert.ToDateTime(context.Request.QueryString["start"]); } catch (Exception e) { start = DateTime.Now; }
        DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);
        //try { end = Convert.ToDateTime(context.Request.QueryString["end"]); } catch (Exception e) { end = DateTime.Now; }
        //String place_idx_value = context.Request.QueryString["place_idx"];

        int car_use_idx = int.Parse(context.Session["DetailCarUseIDXSearch"].ToString());//3;//int.Parse(place_idx_value);//3;//Convert.ToDateTime(context.Request.QueryString["end"]);
        int detailtype_car_idx = int.Parse(context.Session["DetailtypecarSearch"].ToString());//3;//int.Parse(place_idx_value);//3;//Convert.ToDateTime(context.Request.QueryString["end"]);
        int m0_car_idx = int.Parse(context.Session["ddlm0car_idxSearch"].ToString());//0;

        List<int> idList = new List<int>();
        List<ImproperCarCalendarEvent> tasksList = new List<ImproperCarCalendarEvent>();

        //Generate JSON serializable events
        foreach (CalendarCarEvent cevent in EventCarDAO.getEvents(start, end, detailtype_car_idx, m0_car_idx, car_use_idx))
        {
            tasksList.Add(new ImproperCarCalendarEvent
            {
                id = cevent.id,
                title = cevent.title,
                start = String.Format("{0:s}", cevent.start),
                end = String.Format("{0:s}", cevent.end),

                description = cevent.description,
                place_name = cevent.place_name,
                car_register = cevent.car_register,
                topic_booking = cevent.topic_booking,
                allDay = cevent.allDay,
                emp_name_th = cevent.emp_name_th,
                detailtype_car_name = cevent.detailtype_car_name,
                car_use_name = cevent.car_use_name,
                type_booking_name = cevent.type_booking_name,
            });
            idList.Add(cevent.id);
        }

        context.Session["idList"] = idList;

        //Serialize events to string
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string sJSON = oSerializer.Serialize(tasksList);

        //Write JSON to response object
        context.Response.Write(sJSON);
    }

    public bool IsReusable
    {
        get { return false; }
    }
}