﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_food : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Food"];
    static string _urlGetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Food"];
    static string _urlSetRbkm0FoodDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0FoodDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            SelectFoodDetail();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        SelectFoodDetail();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddFood":

                GvFoodInRoom.EditIndex = -1;
                SelectFoodDetail();
                btnAddFood.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;

                break;

            case "CmdSave":
                InsertFoodDetail();
                ////SelectFoodDetail();
                ////btnAddFood.Visible = true;
                ////FvInsert.Visible = false;
                break;

            case "CmdCancel":
                btnAddFood.Visible = true;
                FvInsert.Visible = false;

                break;

            case "CmdDelete":

                int food_del = int.Parse(cmdArg);

                data_roombooking data_m0_food_del = new data_roombooking();
                rbk_m0_food_detail m0_food_del = new rbk_m0_food_detail();
                data_m0_food_del.rbk_m0_food_list = new rbk_m0_food_detail[1];

                m0_food_del.food_idx = food_del;
                m0_food_del.cemp_idx = _emp_idx;

                data_m0_food_del.rbk_m0_food_list[0] = m0_food_del;
                data_m0_food_del = callServicePostRoomBooking(_urlSetRbkm0FoodDel, data_m0_food_del);

                SelectFoodDetail();
                //Gv_select_unit.Visible = false;
                btnAddFood.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void InsertFoodDetail()
    {

        TextBox txtfood_name = (TextBox)FvInsert.FindControl("txtfood_name");
        DropDownList ddlFoodStatus = (DropDownList)FvInsert.FindControl("ddlFoodStatus");

        data_roombooking data_m0_food = new data_roombooking();
        rbk_m0_food_detail m0_food_insert = new rbk_m0_food_detail();
        data_m0_food.rbk_m0_food_list = new rbk_m0_food_detail[1];

        m0_food_insert.food_idx = 0;
        m0_food_insert.food_name = txtfood_name.Text;
        m0_food_insert.cemp_idx = _emp_idx;
        m0_food_insert.food_status = int.Parse(ddlFoodStatus.SelectedValue);

        data_m0_food.rbk_m0_food_list[0] = m0_food_insert;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_food));

        data_m0_food = callServicePostRoomBooking(_urlSetRbkm0Food, data_m0_food);


        if (data_m0_food.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txtfood_name.Text = String.Empty;
            //tex_place_code.Text = String.Empty;
        }
    }

    protected void SelectFoodDetail()
    {

        data_roombooking data_m0_food_detail = new data_roombooking();
        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

        m0_food_detail.condition = 0;

        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_food_detail));
        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);
        
        setGridData(GvFoodInRoom, data_m0_food_detail.rbk_m0_food_list);

    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvFoodInRoom":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_food_status = (Label)e.Row.Cells[3].FindControl("lbl_food_status");
                    Label food_statusOnline = (Label)e.Row.Cells[3].FindControl("food_statusOnline");
                    Label food_statusOffline = (Label)e.Row.Cells[3].FindControl("food_statusOffline");

                    ViewState["vs_food_status"] = lbl_food_status.Text;


                    if (ViewState["vs_food_status"].ToString() == "1")
                    {
                        food_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_food_status"].ToString() == "0")
                    {
                        food_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAddFood.Visible = true;
                    FvInsert.Visible = false;

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                GvFoodInRoom.EditIndex = e.NewEditIndex;
                SelectFoodDetail();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":

                var txt_food_idx_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_idx_edit");
                var txt_food_name_edit = (TextBox)GvFoodInRoom.Rows[e.RowIndex].FindControl("txt_food_name_edit");
                var ddlfood_status_edit = (DropDownList)GvFoodInRoom.Rows[e.RowIndex].FindControl("ddlfood_status_edit");


                GvFoodInRoom.EditIndex = -1;


                data_roombooking data_m0_food_edit = new data_roombooking();
                rbk_m0_food_detail m0_food_edit = new rbk_m0_food_detail();
                data_m0_food_edit.rbk_m0_food_list = new rbk_m0_food_detail[1];

                m0_food_edit.food_idx = int.Parse(txt_food_idx_edit.Text);
                m0_food_edit.food_name = txt_food_name_edit.Text;
                m0_food_edit.cemp_idx = _emp_idx;
                m0_food_edit.food_status = int.Parse(ddlfood_status_edit.SelectedValue);

                data_m0_food_edit.rbk_m0_food_list[0] = m0_food_edit;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_resultuse_edit));

                data_m0_food_edit = callServicePostRoomBooking(_urlSetRbkm0Food, data_m0_food_edit);

                if (data_m0_food_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    SelectFoodDetail();
                }
                else
                {
                    SelectFoodDetail();
                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                GvFoodInRoom.EditIndex = -1;
                SelectFoodDetail();

                btnAddFood.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFoodInRoom":
                GvFoodInRoom.PageIndex = e.NewPageIndex;
                GvFoodInRoom.DataBind();
                SelectFoodDetail();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions
}