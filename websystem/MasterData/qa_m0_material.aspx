﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_m0_material.aspx.cs" Inherits="websystem_MasterData_qa_m0_material" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="test" runat="server"></asp:Literal>


    <div class="col-sm-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <%-- <asp:Label ID="notRules" runat="server" CssClass="text-danger" />--%>
    <%--<div id="fileArea" runat="server" class="imxls-file-area">--%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">
            <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                <ContentTemplate>
                    <asp:FileUpload ID="upload" runat="server" />

                    <asp:RequiredFieldValidator ID="requiredFileUpload"
                        runat="server" ValidationGroup="saveInsertFile"
                        Display="None"
                        SetFocusOnError="true"
                        ControlToValidate="upload"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="กรุณาเลือกไฟล์" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                        TargetControlID="requiredFileUpload" Width="160" PopupPosition="BottomRight" />

                    <br />
                    <%-- <asp:LinkButton ID="btnImport" runat="server" Text="Import" ValidationGroup="saveInsertFile" OnCommand="btnCommand" CommandName="btnImport"             CssClass="btn btn-success imxls-files-loading"
                        />--%>

                    <asp:LinkButton ID="btnImport" runat="server" Text="Import" ValidationGroup="saveInsertFile" OnCommand="btnCommand" CommandName="btnImport"
                        OnClientClick="return ValidatePage('.btnImport', '.loading-icon-to-import-detail','saveInsertFile');"
                        CssClass="btn btn-success btnImport" />
                    <div class="loading-icon-to-import-detail" id="show_iconload" runat="server" >
                        <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>' width="35" height="35" />

                    </div>

                    <br />
                    <br />
                </ContentTemplate>

                <Triggers>
                    <asp:PostBackTrigger ControlID="btnImport" />
                </Triggers>
            </asp:UpdatePanel>



            <%--<img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>' width="40" height="40" id="loading" class="imxls-img-loading" />--%>

            <asp:GridView ID="GvImportMaterial"
                runat="server"
                AutoGenerateColumns="false"
                HeaderStyle-CssClass="success"
                CssClass="table table-striped table-bordered table-responsive col-md-12"
                DataKeyNames="material_code"
                PageSize="10"
                AllowPaging="true"
                OnPageIndexChanging="Master_PageIndexChanging"
                OnRowDataBound="Master_RowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center">No result</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <small>
                                <%# (Container.DataItemIndex +1) %></small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Material Code" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="left">

                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbl_material_code" runat="server" Text='<%# Eval("material_code") %>' />
                            </small>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Material Name" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="left">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbl_material_name" runat="server" Text='<%# Eval("material_name") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            
        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">
        $('.loading-icon-to-import-detail').hide();
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

        $('.loading-icon-to-import-detail').hide();
        });

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function ValidatePage(toHide, toShow) {
            if (typeof (Page_ClientValidate) == 'function') {
                Page_ClientValidate();
            }
            if (Page_IsValid) {
                $(toHide).hide();
                $(toShow).show();
            }

        }

        //function ValidatePage(toHide, toShow) {

        //    $(toHide).hide();
        //    $(toShow).show();

        //}

    </script>


</asp:Content>

