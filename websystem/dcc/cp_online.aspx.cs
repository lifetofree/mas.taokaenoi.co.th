using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_dcc_cp_online : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_dcc _dataDcc = new data_dcc();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    static string _urlGetCPM0DocTypeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCPM0DocTypeList"];
    static string _urlGetCPM1DocTypeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCPM1DocTypeList"];
    static string _urlGetCPM0NodeDecisionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCPM0NodeDecisionList"];
    static string _urlSetCPU0DocumentList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCPU0DocumentList"];
    static string _urlGetCPU0DocumentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCPU0DocumentList"];
    static string _urlGetCPU0DocumentDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetCPU0DocumentDetail"];

    static string imgPath = ConfigurationSettings.AppSettings["PathFile_CarPar"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    string coshare = "";
    string Temp_coshare = "";

    // rdept permission
    int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        // check permission
        foreach (int item in rdept_qmr)
        {
            if(_dataEmployee.employee_list[0].rdept_idx == item)
            {
                _flag_qmr = true;
                break;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            initPage();
        }
        linkBtnTrigger(lbCreate);
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0);
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdDocSave":
                // fvEmpDetail
                HiddenField hfEmpOrgIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpOrgIDX");
                HiddenField hfEmpRdeptIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpRdeptIDX");
                HiddenField hfEmpRsecIDX = (HiddenField)fvEmpDetail.FindControl("hfEmpRsecIDX");
                // fvEmpDetail
                
                // fvActor1Node1
                HiddenField hfActorIDX = (HiddenField)fvActor1Node1.FindControl("hfActorIDX");
                HiddenField hfNodeIDX = (HiddenField)fvActor1Node1.FindControl("hfNodeIDX");
                HiddenField hfU0IDX = (HiddenField)fvActor1Node1.FindControl("hfU0IDX");
                // fvActor1Node1
                
                FileUpload fuAttachFile = (FileUpload)fvActor1Node1.FindControl("fuAttachFile");

                int actor_idx = int.TryParse(hfActorIDX.Value, out _default_int) ? int.Parse(hfActorIDX.Value) : _default_int;
                int node_idx = int.TryParse(hfNodeIDX.Value, out _default_int) ? int.Parse(hfNodeIDX.Value) : _default_int;

                _dataDcc.cp_u0_document_list = new cp_u0_document[1];
                _dataDcc.cp_u1_document_list = new cp_u1_document[1];
                cp_u0_document _u0_doc = new cp_u0_document();
                cp_u1_document _u1_doc = new cp_u1_document();

                // cp_u0_document
                _u0_doc.uidx = int.TryParse(hfU0IDX.Value, out _default_int) ? int.Parse(hfU0IDX.Value) : _default_int;

                // cp_u1_document
                _u1_doc.u0_idx = int.TryParse(hfU0IDX.Value, out _default_int) ? int.Parse(hfU0IDX.Value) : _default_int;
                _u1_doc.emp_idx = _emp_idx;
                _u1_doc.org_idx = int.TryParse(hfEmpOrgIDX.Value, out _default_int) ? int.Parse(hfEmpOrgIDX.Value) : _default_int;
                _u1_doc.rdept_idx = int.TryParse(hfEmpRdeptIDX.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX.Value) : _default_int;
                _u1_doc.actor_idx = actor_idx;
                _u1_doc.from_node = node_idx;

                //litDebug.Text = hfcit2.Count.ToString();

                // switch action by node
                switch (node_idx)
                {
                    case 1:
                        DropDownList ddlDocType = (DropDownList)fvActor1Node1.FindControl("ddlDocType");
                        DropDownList ddlDocSubType = (DropDownList)fvActor1Node1.FindControl("ddlDocSubType");
                        DropDownList ddlOrg = (DropDownList)fvActor1Node1.FindControl("ddlOrg");
                        DropDownList ddlDept = (DropDownList)fvActor1Node1.FindControl("ddlDept");
                        DropDownList ddlSec = (DropDownList)fvActor1Node1.FindControl("ddlSec");
                        TextBox tbDocTitle = (TextBox)fvActor1Node1.FindControl("tbDocTitle");
                        TextBox tbDocDetail = (TextBox)fvActor1Node1.FindControl("tbDocDetail");
                        
                        // cp_u0_document
                        _u0_doc.emp_idx = _emp_idx;
                        _u0_doc.req_rdept_idx = int.TryParse(hfEmpRdeptIDX.Value, out _default_int) ? int.Parse(hfEmpRdeptIDX.Value) : _default_int;
                        _u0_doc.req_rsec_idx = int.TryParse(hfEmpRsecIDX.Value, out _default_int) ? int.Parse(hfEmpRsecIDX.Value) : _default_int;

                        _u0_doc.doc_type = int.TryParse(ddlDocType.SelectedItem.Value, out _default_int) ? int.Parse(ddlDocType.SelectedItem.Value) : _default_int;
                        _u0_doc.doc_sub_type = int.TryParse(ddlDocSubType.SelectedItem.Value, out _default_int) ? int.Parse(ddlDocSubType.SelectedItem.Value) : _default_int;
                        _u0_doc.doc_title = tbDocTitle.Text.Trim();
                        _u0_doc.doc_detail = tbDocDetail.Text.Trim();
                        _u0_doc.org_idx = int.TryParse(ddlOrg.SelectedItem.Value, out _default_int) ? int.Parse(ddlOrg.SelectedItem.Value) : _default_int;
                        _u0_doc.res_rdept_idx = int.TryParse(ddlDept.SelectedItem.Value, out _default_int) ? int.Parse(ddlDept.SelectedItem.Value) : _default_int;
                        _u0_doc.res_rsec_idx = int.TryParse(ddlSec.SelectedItem.Value, out _default_int) ? int.Parse(ddlSec.SelectedItem.Value) : _default_int;
                        
                        // cp_u1_document
                        _u1_doc.comment = "-";
                        _u1_doc.node_decision = 0;
                        
                        break;
                    case 6:
                    case 10:
                        DropDownList ddlActor1Approve = (DropDownList)fvActor1.FindControl("ddlActor1Approve");
                        TextBox tbActor1Comment = (TextBox)fvActor1.FindControl("tbActor1Comment");
                        _u1_doc.comment = tbActor1Comment.Text.Trim();
                        _u1_doc.node_decision = int.TryParse(ddlActor1Approve.SelectedItem.Value, out _default_int) ? int.Parse(ddlActor1Approve.SelectedItem.Value) : _default_int;
                        break;
                    case 2:
                    case 7:
                    case 11:
                        if (node_idx == 2)
                        {
                            if (ViewState["Coshare_RSecIDX"].ToString() != "" || ViewState["Coshare_RSecIDX"].ToString() != ",")
                            {
                                _u0_doc.coshare_list = ViewState["Coshare_RSecIDX"].ToString();
                            }
                        }
                        DropDownList ddlActor2Approve = (DropDownList)fvActor2.FindControl("ddlActor2Approve");
                        TextBox tbActor2Comment = (TextBox)fvActor2.FindControl("tbActor2Comment");
                        _u1_doc.comment = tbActor2Comment.Text.Trim();
                        _u1_doc.node_decision = int.TryParse(ddlActor2Approve.SelectedItem.Value, out _default_int) ? int.Parse(ddlActor2Approve.SelectedItem.Value) : _default_int;
                        break;
                    case 3:
                    case 5:
                    case 13:
                        DropDownList ddlActor3Approve = (DropDownList)fvActor3.FindControl("ddlActor3Approve");
                        TextBox tbActor3Comment = (TextBox)fvActor3.FindControl("tbActor3Comment");
                        _u1_doc.comment = tbActor3Comment.Text.Trim();
                        _u1_doc.node_decision = int.TryParse(ddlActor3Approve.SelectedItem.Value, out _default_int) ? int.Parse(ddlActor3Approve.SelectedItem.Value) : _default_int;
                        break;
                    case 4:
                        TextBox tbComment1 = (TextBox)fvActor4.FindControl("tbComment1");// man
                        TextBox tbComment2 = (TextBox)fvActor4.FindControl("tbComment2");// machine
                        TextBox tbComment3 = (TextBox)fvActor4.FindControl("tbComment3");// material
                        TextBox tbComment4 = (TextBox)fvActor4.FindControl("tbComment4");// method
                        TextBox tbComment5 = (TextBox)fvActor4.FindControl("tbComment5");// money
                        TextBox tbComment6 = (TextBox)fvActor4.FindControl("tbComment6");// environment
                        TextBox tbComment7 = (TextBox)fvActor4.FindControl("tbComment7");// solution
                        FileUpload fuAttachFileActor4 = (FileUpload)fvActor4.FindControl("fuAttachFileActor4");

                        string _comment = tbComment1.Text.Trim() + "|";
                        _comment += tbComment2.Text.Trim() + "|";
                        _comment += tbComment3.Text.Trim() + "|";
                        _comment += tbComment4.Text.Trim() + "|";
                        _comment += tbComment5.Text.Trim() + "|";
                        _comment += tbComment6.Text.Trim() + "|";
                        _comment += tbComment7.Text.Trim() + "|";

                        // cp_u1_document
                        _u1_doc.comment = _comment;
                        _u1_doc.node_decision = 0;
                        break;
                }

                _dataDcc.cp_u0_document_list[0] = _u0_doc;
                _dataDcc.cp_u1_document_list[0] = _u1_doc;

                // call services
                _dataDcc = setCPU0DocumentList(_dataDcc);
                var dirName_u0 = _dataDcc.cp_u0_document_list[0].doc_code;
                var dirName_u1 = _dataDcc.cp_u1_document_list[0].uidx;
                //litDebug.Text = dirName.ToString();

                if (node_idx == 1 || node_idx == 4)
                {
                    setDir(imgPath + dirName_u0);
                    HttpFileCollection hfc = Request.Files;
                    if (hfc.Count > 0)
                    {
                        System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Server.MapPath(imgPath + dirName_u0));
                        int ocount = dir.GetFiles().Length;

                        for (int i = ocount; i < (hfc.Count + ocount); i++)
                        {
                            HttpPostedFile hpf = hfc[i - ocount];
                            if (hpf.ContentLength > 0)
                            {
                                string fileName = dirName_u0 + "_" + dirName_u1 + "_" + i;
                                string filePath = Server.MapPath(imgPath + dirName_u0);
                                string extension = Path.GetExtension(hpf.FileName);
                                hpf.SaveAs(Path.Combine(filePath, fileName + extension));
                            }
                        }
                    }
                    else
                    {
                        //text.Text = "don'thasfile";
                    }
                }
                setActiveTab("docListDoing", 0);
                break;
            case "cmdDocCancel":
                setActiveTab("docListDoing", 0);
                break;
            case "cmdDocDetail":
                setActiveTab("docCreate", int.Parse(cmdArg));

                BoxgvFileList.Visible = true;
                gvFileList.Visible = true;
                //string filePathL = ConfigurationSettings.AppSettings["PathFile_CarPar"];
                //string filePathL = Server.MapPath(imgPath + String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0")));
                //DirectoryInfo myDirL = new DirectoryInfo(filePathL);
                //SearchDirectories(myDirL, String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0")));

                string getPathLotus = ConfigurationSettings.AppSettings["PathFile_CarPar"];
                string filePathLotus = Server.MapPath(getPathLotus + "CP600002");
                DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                SearchDirectories(myDirLotus, "CP600002_125_0");
                //string LinkHost11 = string.Format("http://{0}", Request.Url.Host);
                //sf.Text = myDirLotus + String.Format("/{0}", Path.GetFileName("CP600002_125_0"));
                sf.Text = filePathLotus;

                break;
            case "AddCoshare":
                DropDownList ddlOrg_coshare = (DropDownList)fvActor2.FindControl("ddlOrg_coshare");
                DropDownList ddlDept_coshare = (DropDownList)fvActor2.FindControl("ddlDept_coshare");
                DropDownList ddlSec_coshare = (DropDownList)fvActor2.FindControl("ddlSec_coshare");
                GridView GvCoshare = (GridView)fvActor2.FindControl("GvCoshare");
                
                #region Add-Coshare
                var ds_Add_coshare = (DataSet)ViewState["vsCoshare"];
                var dr_Add_coshare = ds_Add_coshare.Tables[0].NewRow();
                dr_Add_coshare["OrgIDX"] = int.Parse(ddlOrg_coshare.SelectedValue);
                dr_Add_coshare["OrgNameEN"] = ddlOrg_coshare.SelectedItem.Text;
                dr_Add_coshare["RDeptIDX"] = int.Parse(ddlDept_coshare.SelectedValue);
                dr_Add_coshare["DeptNameEN"] = ddlDept_coshare.SelectedItem.Text;
                dr_Add_coshare["RSecIDX"] = int.Parse(ddlSec_coshare.SelectedValue);
                dr_Add_coshare["SecNameEN"] = ddlSec_coshare.SelectedItem.Text;
                ds_Add_coshare.Tables[0].Rows.Add(dr_Add_coshare);
                ViewState["vsCoshare"] = ds_Add_coshare;
                #endregion

                setGridData(GvCoshare, (DataSet)ViewState["vsCoshare"]);

                foreach (GridViewRow row in GvCoshare.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        coshare = (row.Cells[0].FindControl("lbl_RSecIDX") as Label).Text;
                        Temp_coshare += coshare + ",";
                    }
                }

                ViewState["Coshare_RSecIDX"] = Temp_coshare;
                getOrganizationList(ddlOrg_coshare);
                ddlDept_coshare.Items.Clear();
                ddlSec_coshare.Items.Clear();
                break;
            case "cmd_delete_coshare":
                string RSecIDX = cmdArg;
                GridView GvCoshare_delete = (GridView)fvActor2.FindControl("GvCoshare");
                var ds_coshare_delete = (DataSet)ViewState["vsCoshare"];
                ViewState["RSecIDX_delete"] = RSecIDX;

                for (int counter = 0; counter < ds_coshare_delete.Tables[0].Rows.Count; counter++)
                {
                    if (ds_coshare_delete.Tables[0].Rows[counter]["RSecIDX"].ToString() == ViewState["RSecIDX_delete"].ToString())
                    {
                        ds_coshare_delete.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }
                setGridData(GvCoshare_delete, (DataSet)ViewState["vsCoshare"]);
                break;
        }
    }

    protected data_dcc setCPU0DocumentList(data_dcc _data_dcc)
    {
        _dataDcc = callServicePostDcc(_urlSetCPU0DocumentList, _data_dcc);
        return _dataDcc;
    }

    protected data_dcc getCPU0DocumentList(data_dcc _data_dcc)
    {
        _dataDcc = callServicePostDcc(_urlGetCPU0DocumentList, _data_dcc);
        return _dataDcc;
    }

    protected data_dcc getCPU0DocumentDetail(data_dcc _data_dcc)
    {
        _dataDcc = callServicePostDcc(_urlGetCPU0DocumentDetail, _data_dcc);
        return _dataDcc;
    }
    #endregion event command

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch(gvName.ID)
        {
            case "gvDoingList":
                setActiveTab("docListDoing", 0);
                break;
            case "gvDoneList":
                setActiveTab("docListDone", 0);
                break;
        }
    }
    #endregion paging

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFileList":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }
                break;
        }
    }
    #endregion RowDatabound

    #region dropdown list
    protected void getCPM0DocTypeList(DropDownList ddlName)
    {
        _dataDcc.cp_m0_document_type_list = new cp_m0_document_type[1];
        cp_m0_document_type _m0DocType = new cp_m0_document_type();
        _m0DocType.midx = 0;
        _dataDcc.cp_m0_document_type_list[0] = _m0DocType;

        _dataDcc = callServicePostDcc(_urlGetCPM0DocTypeList, _dataDcc);
        setDdlData(ddlName, _dataDcc.cp_m0_document_type_list, "type_name", "midx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทเอกสาร ---", "-1"));
    }

    protected void getCPM1DocTypeList(DropDownList ddlName, int _m0_idx)
    {
        _dataDcc.cp_m1_document_type_list = new cp_m1_document_type[1];
        cp_m1_document_type _m1DocType = new cp_m1_document_type();
        _m1DocType.m0_idx = _m0_idx;
        _dataDcc.cp_m1_document_type_list[0] = _m1DocType;

        _dataDcc = callServicePostDcc(_urlGetCPM1DocTypeList, _dataDcc);
        setDdlData(ddlName, _dataDcc.cp_m1_document_type_list, "type_name", "midx");
    }

    protected void getCPM0NodeDecisionList(DropDownList ddlName, int _node_idx)
    {
        _dataDcc.cp_m0_node_decision_list = new cp_m0_node_decision[1];
        cp_m0_node_decision _m0Decision = new cp_m0_node_decision();
        _m0Decision.m0_node_idx = _node_idx;
        _dataDcc.cp_m0_node_decision_list[0] = _m0Decision;

        _dataDcc = callServicePostDcc(_urlGetCPM0NodeDecisionList, _dataDcc);
        setDdlData(ddlName, _dataDcc.cp_m0_node_decision_list, "decision_name", "midx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผลการพิจารณา ---", "-1"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_en", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_en", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_en", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        // fvActor1Node1
        DropDownList ddlDocType = (DropDownList)fvActor1Node1.FindControl("ddlDoctype");
        DropDownList ddlDocSubType = (DropDownList)fvActor1Node1.FindControl("ddlDocSubType");
        DropDownList ddlOrg = (DropDownList)fvActor1Node1.FindControl("ddlOrg");
        DropDownList ddlDept = (DropDownList)fvActor1Node1.FindControl("ddlDept");
        DropDownList ddlSec = (DropDownList)fvActor1Node1.FindControl("ddlSec");
        DropDownList ddlOrg_coshare = (DropDownList)fvActor2.FindControl("ddlOrg_coshare");
        DropDownList ddlDept_coshare = (DropDownList)fvActor2.FindControl("ddlDept_coshare");
        DropDownList ddlSec_coshare = (DropDownList)fvActor2.FindControl("ddlSec_coshare");
        // fvActor1Node1

        switch (ddlName.ID)
        {
            case "ddlDocType":
                getCPM1DocTypeList(ddlDocSubType, int.Parse(ddlDocType.SelectedItem.Value));
                ddlDocType.Focus();
                break;
            case "ddlOrg":
                ddlDocType.Focus();
                getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedItem.Value));
                ddlSec.Items.Clear();
                break;
            case "ddlDept":
                ddlDocType.Focus();
                getSectionList(ddlSec, int.Parse(ddlOrg.SelectedItem.Value), int.Parse(ddlDept.SelectedItem.Value));
                break;
            case "ddlOrg_coshare":
                getDepartmentList(ddlDept_coshare, int.Parse(ddlOrg_coshare.SelectedItem.Value));
                ddlSec_coshare.Items.Clear();
                break;
            case "ddlDept_coshare":
                getSectionList(ddlSec_coshare, int.Parse(ddlOrg_coshare.SelectedItem.Value), int.Parse(ddlDept_coshare.SelectedItem.Value));
                break;
        }
    }
    #endregion dropdown list

    #region formview
    protected void fvDataBound(object sender, EventArgs e)
    {
        FormView fvName = (FormView)sender;
        switch(fvName.ID)
        {
            // case "fv5M1E":
            //     break;
        }
    }
    #endregion formview

    protected string MapURL(string path)
    {
        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {
        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            //GridView gvFileList = (GridView)rptProcessDetail.FindControl("gvFileList");
            if (dt1.Rows.Count > 0)
            {
                ds1.Tables.Add(dt1);
                gvFileList.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFileList.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFileList.DataSource = null;
                gvFileList.DataBind();

            }
        }
        catch
        {
        }
    }

    #region repeater
    protected void rptItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptName = (Repeater)sender;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            switch(rptName.ID)
            {
                case "rptProcessDetail":
                    HiddenField hfProcessFromNode = (HiddenField)e.Item.FindControl("hfProcessFromNode");
                    Label lblComment = (Label)e.Item.FindControl("lblComment");
                    FormView fv5M1E = (FormView)e.Item.FindControl("fv5M1E");

                    int _from_node = int.Parse(hfProcessFromNode.Value);
                    switch(_from_node)
                    {
                        case 1:
                            //fsaa.te
                            //try
                            //{
                            //Label fsaa = (Label)rptProcessDetail.FindControl("fsaa");
                            //fsaa.Text = "ddd";
                            //}
                            //catch
                            //{

                            //}

                            //try
                            //{
                            //GridView gvFileList = (GridView)rptProcessDetail.FindControl("gvFileList");
                            //    string dirName = imgPath + String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0")); //dataMaster.MaterialRegisterList[0].MCode + dataMaster.MaterialRegisterList[0].RCode + dataMaster.MaterialRegisterList[0].R2Code;
                            //    string[] filesindirectory = Directory.GetFiles(Server.MapPath(dirName));
                            //    List<String> images = new List<string>(filesindirectory.Count());

                            //foreach (string item in filesindirectory)
                            //{
                            //    images.Add(String.Format(dirName + "/{0}", Path.GetFileName(item)));
                            //}

                            //setGridData(gvFileList, filesindirectory);
                            //gvFileList.DataSource = filesindirectory;
                            //gvFileList.DataBind();
                            //}
                            //catch
                            //{

                            //}
                            break;
                        case 4:
                            //Label fsaa = (Label)rptProcessDetail.FindControl("fsaa");
                            //Label fsaa = (Label)e.Item.FindControl("fsaa");

                            lblComment.Visible = false;
                            setFormData(fv5M1E, FormViewMode.ReadOnly, split5M1E(lblComment.Text));
                            //string filePathL_1 = ConfigurationSettings.AppSettings["PathFile_CarPar"];
                            //string filePathLotus = Server.MapPath(filePathL_1 + "CP600002_125_0");
                            //DirectoryInfo myDirLotus_1 = new DirectoryInfo(filePathLotus);
                            //SearchDirectories(myDirLotus_1, "CP600002" + "/" + "CP600002_125_0");
                            //fsaa.Text = "44444";//String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0"));

                            //string filePathL = Server.MapPath(imgPath + String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0")));
                            //DirectoryInfo myDirL = new DirectoryInfo(filePathL_1);
                            //SearchDirectories(myDirL, String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0")));

                            //string getPathLotus = ConfigurationSettings.AppSettings["PathFile_SAP"];
                            //string filePathLotus = Server.MapPath(getPathLotus + ViewState["DocCode"].ToString());
                            //DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                            //SearchDirectories(myDirLotus, ViewState["DocCode"].ToString());

                            break;
                        default:
                            Label fsaa1 = (Label)rptProcessDetail.FindControl("fsaa");
                            fsaa1.Text = "44444";//String.Format("CP600002" + "/{0}", Path.GetFileName("CP600002_125_0"));

                            break;
                    }
                    break;
            }
        }
    }
    #endregion repeater

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("docListDoing", 0);

        #region View-Coshare
        var ds_coshare = new DataSet();
        ds_coshare.Tables.Add("Coshare");
        ds_coshare.Tables[0].Columns.Add("OrgIDX", typeof(int));
        ds_coshare.Tables[0].Columns.Add("OrgNameEN", typeof(String));
        ds_coshare.Tables[0].Columns.Add("RDeptIDX", typeof(int));
        ds_coshare.Tables[0].Columns.Add("DeptNameEN", typeof(String));
        ds_coshare.Tables[0].Columns.Add("RSecIDX", typeof(int));
        ds_coshare.Tables[0].Columns.Add("SecNameEN", typeof(String));
        ViewState["vsCoshare"] = ds_coshare;
        #endregion

        ViewState["Coshare_RSecIDX"] = "";

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_dcc callServicePostDcc(string _cmdUrl, data_dcc _dataDcc)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataDcc);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataDcc = (data_dcc)_funcTool.convertJsonToObject(typeof(data_dcc), _localJson);

        return _dataDcc;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        _dataDcc.cp_search_key_list = new cp_search_key[1];
        cp_search_key _search = new cp_search_key();
        cp_u0_document _u0_doc = new cp_u0_document();

        // clear other formview, repeater except fvEmpDetail, fvActor1Node1
        setRepeaterData(rptProcessDetail, null);
        setFormData(fvActor1, FormViewMode.ReadOnly, null);
        setFormData(fvActor2, FormViewMode.ReadOnly, null);
        setFormData(fvActor3, FormViewMode.ReadOnly, null);
        setFormData(fvActor4, FormViewMode.ReadOnly, null);
        // clear other formview, repeater except fvEmpDetail, fvActor1Node1

        switch(activeTab)
        {
            case "docCreate":
                // fvEmpDetail
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                if(uidx == 0)
                {
                    // fvActor1Node1
                    setFormData(fvActor1Node1, FormViewMode.Insert, null);
                    getCPM0DocTypeList((DropDownList)fvActor1Node1.FindControl("ddlDocType"));
                    getOrganizationList((DropDownList)fvActor1Node1.FindControl("ddlOrg"));
                }
                else if(uidx > 0)
                {
                    _search.s_uidx = uidx.ToString();
                    _dataDcc.cp_search_key_list[0] = _search;
                    _dataDcc = getCPU0DocumentDetail(_dataDcc);

                    // stored base document data
                    _u0_doc = _dataDcc.cp_u0_document_list[0];
                    int _c = (_dataDcc.cp_u1_document_list).Length-1;
                    cp_u1_document[] _u1_doc = new cp_u1_document[_c];
                    _u1_doc = _dataDcc.cp_u1_document_list;
                    
                    // fvActor1Node1
                    setFormData(fvActor1Node1, FormViewMode.ReadOnly, _dataDcc.cp_u0_document_list);
                    // rptProcessDetail
                    setRepeaterData(rptProcessDetail, _u1_doc);

                    int node_idx = _u0_doc.current_node;
                    int actor_idx = _u0_doc.current_actor;

                    // fvActor1Node1 ddl
                    DropDownList ddlDocType = (DropDownList)fvActor1Node1.FindControl("ddlDocType");
                    DropDownList ddlDocSubType = (DropDownList)fvActor1Node1.FindControl("ddlDocSubType");
                    DropDownList ddlOrg = (DropDownList)fvActor1Node1.FindControl("ddlOrg");
                    DropDownList ddlDept = (DropDownList)fvActor1Node1.FindControl("ddlDept");
                    DropDownList ddlSec = (DropDownList)fvActor1Node1.FindControl("ddlSec");
                    getCPM0DocTypeList(ddlDocType);
                    getCPM1DocTypeList(ddlDocSubType, _u0_doc.doc_type);
                    getOrganizationList(ddlOrg);
                    getDepartmentList(ddlDept, _u0_doc.org_idx);
                    getSectionList(ddlSec, _u0_doc.org_idx, _u0_doc.res_rdept_idx);

                    // set selected ddl
                    ddlDocType.SelectedValue = _u0_doc.doc_type.ToString();
                    ddlDocSubType.SelectedValue = _u0_doc.doc_sub_type.ToString();
                    ddlOrg.SelectedValue = _u0_doc.org_idx.ToString();
                    ddlDept.SelectedValue = _u0_doc.res_rdept_idx.ToString();
                    ddlSec.SelectedValue = _u0_doc.res_rsec_idx.ToString();

                    // switch action by node
                    switch(node_idx)
                    {
                        case 1:
                            break;
                        case 6:
                        case 10:
                            setFormData(fvActor1, FormViewMode.Insert, null);
                            DropDownList ddlActor1Approve = (DropDownList)fvActor1.FindControl("ddlActor1Approve");
                            getCPM0NodeDecisionList(ddlActor1Approve, node_idx);
                            break;
                        case 2:
                        case 7:
                        case 11:
                            setFormData(fvActor2, FormViewMode.Insert, null);
                            DropDownList ddlActor2Approve = (DropDownList)fvActor2.FindControl("ddlActor2Approve");
                            getCPM0NodeDecisionList(ddlActor2Approve, node_idx);
                            if(node_idx == 2)
                            {
                                DropDownList ddlOrg_coshare = (DropDownList)fvActor2.FindControl("ddlOrg_coshare");
                                GridView GvCoshare = (GridView)fvActor2.FindControl("GvCoshare");
                                getOrganizationList(ddlOrg_coshare);
                                setGridData(GvCoshare, (DataSet)ViewState["vsCoshare"]);
                            }
                            else
                            {
                                HtmlGenericControl div_coshare = (HtmlGenericControl)fvActor2.FindControl("div_coshare");
                                div_coshare.Style.Add("display", "none");
                            }
                            break;
                        case 3:
                        case 5:
                        case 13:
                            setFormData(fvActor3, FormViewMode.Insert, null);
                            DropDownList ddlActor3Approve = (DropDownList)fvActor3.FindControl("ddlActor3Approve");
                            getCPM0NodeDecisionList(ddlActor3Approve, node_idx);
                            //----- check doc_loop condition -----//
                            break;
                        case 4:
                            setFormData(fvActor4, FormViewMode.Insert, null);
                            break;
                    }
                }
            break;
            case "docListNew":
            break;
            case "docListDoing":
                _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
                if(_flag_qmr)
                {
                    _search.s_req_rdept_idx = "0";
                }
                else
                {
                    _search.s_req_rdept_idx = _dataEmployee.employee_list[0].rdept_idx.ToString();
                }
                
                _search.s_doc_status = "1,2,3";
                _dataDcc.cp_search_key_list[0] = _search;

                _dataDcc = getCPU0DocumentList(_dataDcc);
                setGridData(gvDoingList, _dataDcc.cp_u0_document_list);
            break;
            case "docListDone":
                _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
                if(_flag_qmr)
                {
                    _search.s_req_rdept_idx = "0";
                }
                else
                {
                    _search.s_req_rdept_idx = _dataEmployee.employee_list[0].rdept_idx.ToString();
                }
                
                _search.s_doc_status = "4";
                _dataDcc.cp_search_key_list[0] = _search;

                _dataDcc = getCPU0DocumentList(_dataDcc);
                setGridData(gvDoneList, _dataDcc.cp_u0_document_list);
            break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx)
    {
        setActiveView(activeTab, uidx);
        switch(activeTab)
        {
            case "docCreate":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
            break;
            case "docListNew":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
            break;
            case "docListDoing":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
            break;
            case "docListDone":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
            break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected DataTable split5M1E(string _dataIn)
    {
        string[] _data5M1E = _dataIn.Split('|');
        DataTable _dt5M1E = new DataTable();
        _dt5M1E.Columns.Add("M1", typeof(string));
        _dt5M1E.Columns.Add("M2", typeof(string));
        _dt5M1E.Columns.Add("M3", typeof(string));
        _dt5M1E.Columns.Add("M4", typeof(string));
        _dt5M1E.Columns.Add("M5", typeof(string));
        _dt5M1E.Columns.Add("E1", typeof(string));
        _dt5M1E.Columns.Add("S1", typeof(string));
        DataRow row = _dt5M1E.NewRow();
        row["M1"] = _data5M1E[0];
        row["M2"] = _data5M1E[1];
        row["M3"] = _data5M1E[2];
        row["M4"] = _data5M1E[3];
        row["M5"] = _data5M1E[4];
        row["E1"] = _data5M1E[5];
        row["S1"] = _data5M1E[6];
        _dt5M1E.Rows.Add(row);

        return _dt5M1E;
    }
    #endregion reuse
}