using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_cen_master")]
public class data_cen_master {
    [XmlElement ("return_code")]
    public int return_code { get; set; }
    [XmlElement ("return_msg")]
    public string return_msg { get; set; }
    [XmlElement ("return_idx")]
    public int return_idx { get; set; }

    [XmlElement ("master_mode")]
    public string master_mode { get; set; }

    [XmlElement ("cen_aff_list_m0")]
    public cen_aff_detail_m0[] cen_aff_list_m0 { get; set; } //affilate
    [XmlElement ("cen_empgroup_list_m0")]
    public cen_empgroup_detail_m0[] cen_empgroup_list_m0 { get; set; } //employee group
    [XmlElement ("cen_jobgrade_list_m0")]
    public cen_jobgrade_detail_m0[] cen_jobgrade_list_m0 { get; set; } //jobgrade
    [XmlElement ("cen_joblevel_list_m0")]
    public cen_joblevel_detail_m0[] cen_joblevel_list_m0 { get; set; } //joblevel
    [XmlElement ("cen_costcenter_list_m0")]
    public cen_costcenter_detail_m0[] cen_costcenter_list_m0 { get; set; } //cost center

    [XmlElement ("cen_org_list_m0")]
    public cen_org_detail_m0[] cen_org_list_m0 { get; set; } //orgenization
    [XmlElement ("cen_wg_list_m0")]
    public cen_wg_detail_m0[] cen_wg_list_m0 { get; set; } //work group
    [XmlElement ("cen_lw_list_m0")]
    public cen_lw_detail_m0[] cen_lw_list_m0 { get; set; } //line work
    [XmlElement ("cen_dept_list_m0")]
    public cen_dept_detail_m0[] cen_dept_list_m0 { get; set; } //department
    [XmlElement ("cen_sec_list_m0")]
    public cen_sec_detail_m0[] cen_sec_list_m0 { get; set; } //section
    [XmlElement ("cen_pos_list_m0")]
    public cen_pos_detail_m0[] cen_pos_list_m0 { get; set; } //position

    [XmlElement ("search_cen_master_list")]
    public search_cen_master_detail[] search_cen_master_list { get; set; }
}

[Serializable]
public class cen_aff_detail_m0 {
    [XmlElement ("aff_idx")]
    public int aff_idx { get; set; }
    [XmlElement ("aff_name_th")]
    public string aff_name_th { get; set; }
    [XmlElement ("aff_name_en")]
    public string aff_name_en { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("aff_status")]
    public int aff_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_empgroup_detail_m0 {
    [XmlElement ("empgroup_idx")]
    public int empgroup_idx { get; set; }
    [XmlElement ("empgroup_name_th")]
    public string empgroup_name_th { get; set; }
    [XmlElement ("empgroup_name_en")]
    public string empgroup_name_en { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("empgroup_status")]
    public int empgroup_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_jobgrade_detail_m0 {
    [XmlElement ("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement ("jobgrade_name")]
    public string jobgrade_name { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("jobgrade_status")]
    public int jobgrade_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_joblevel_detail_m0 {
    [XmlElement ("joblevel_idx")]
    public int joblevel_idx { get; set; }
    [XmlElement ("joblevel_name")]
    public string joblevel_name { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("joblevel_status")]
    public int joblevel_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_costcenter_detail_m0 {
    [XmlElement ("cost_idx")]
    public int cost_idx { get; set; }
    [XmlElement ("cost_no")]
    public string cost_no { get; set; }
    [XmlElement ("cost_name")]
    public string cost_name { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("cost_status")]
    public int cost_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_org_detail_m0 {
    [XmlElement ("org_idx")]
    public int org_idx { get; set; }
    [XmlElement ("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement ("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("org_status")]
    public int org_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class cen_wg_detail_m0 : cen_org_detail_m0 {
    [XmlElement ("wg_idx")]
    public int wg_idx { get; set; }
    [XmlElement ("wg_name_th")]
    public string wg_name_th { get; set; }
    [XmlElement ("wg_name_en")]
    public string wg_name_en { get; set; }
    [XmlElement ("wg_status")]
    public int wg_status { get; set; }

}

[Serializable]
public class cen_lw_detail_m0 : cen_wg_detail_m0 {
    [XmlElement ("lw_idx")]
    public int lw_idx { get; set; }
    [XmlElement ("lw_name_th")]
    public string lw_name_th { get; set; }
    [XmlElement ("lw_name_en")]
    public string lw_name_en { get; set; }
    [XmlElement ("lw_status")]
    public int lw_status { get; set; }
}

[Serializable]
public class cen_dept_detail_m0 : cen_lw_detail_m0 {
    [XmlElement ("dept_idx")]
    public int dept_idx { get; set; }
    [XmlElement ("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement ("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement ("dept_status")]
    public int dept_status { get; set; }
}

[Serializable]
public class cen_sec_detail_m0 : cen_dept_detail_m0 {
    [XmlElement ("sec_idx")]
    public int sec_idx { get; set; }
    [XmlElement ("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement ("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement ("sec_status")]
    public int sec_status { get; set; }
}

[Serializable]
public class cen_pos_detail_m0 : cen_sec_detail_m0 {
    [XmlElement ("pos_idx")]
    public int pos_idx { get; set; }
    [XmlElement ("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement ("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement ("pos_name_en")]
    public string pos_name_en { get; set; }
    [XmlElement ("man_power")]
    public int man_power { get; set; }
    [XmlElement ("pos_status")]
    public int pos_status { get; set; }
}

[Serializable]
public class cen_pos_emp_detail_r0 {
    [XmlElement ("pos_emp_idx")]
    public int pos_emp_idx { get; set; }
    [XmlElement ("emp_dx")]
    public int emp_dx { get; set; }
    [XmlElement ("pos_idx")]
    public int pos_idx { get; set; }
    [XmlElement ("flag_main")]
    public int flag_main { get; set; } //main = 1
    [XmlElement ("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement ("pos_emp_status")]
    public int pos_emp_status { get; set; }
    [XmlElement ("create_date")]
    public string create_date { get; set; }
    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_cen_master_detail {
    [XmlElement ("s_aff_idx")]
    public string s_aff_idx { get; set; }
    [XmlElement ("s_aff_name")]
    public string s_aff_name { get; set; }

    [XmlElement ("s_empgroup_idx")]
    public string s_empgroup_idx { get; set; }
    [XmlElement ("s_empgroup_name")]
    public string s_empgroup_name { get; set; }

    [XmlElement ("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement ("s_org_name")]
    public string s_org_name { get; set; }

    [XmlElement ("s_wg_idx")]
    public string s_wg_idx { get; set; }
    [XmlElement ("S_wg_name")]
    public string S_wg_name { get; set; }

    [XmlElement ("s_lw_idx")]
    public string s_lw_idx { get; set; }
    [XmlElement ("s_lw_name")]
    public string s_lw_name { get; set; }

    [XmlElement ("s_dept_idx")]
    public string s_dept_idx { get; set; }
    [XmlElement ("s_dept_name")]
    public string s_dept_name { get; set; }

    [XmlElement ("s_sec_idx")]
    public string s_sec_idx { get; set; }
    [XmlElement ("s_sec_name")]
    public string s_sec_name { get; set; }

    [XmlElement ("s_pos_idx")]
    public string s_pos_idx { get; set; }
    [XmlElement ("s_pos_name")]
    public string s_pos_name { get; set; }

    [XmlElement ("s_jobgrade_idx")]
    public string s_jobgrade_idx { get; set; }
    [XmlElement ("s_jobgrade_name")]
    public string s_jobgrade_name { get; set; }

    [XmlElement ("s_joblevel_idx")]
    public string s_joblevel_idx { get; set; }
    [XmlElement ("s_joblevel_name")]
    public string s_joblevel_name { get; set; }
    
    [XmlElement ("s_cost_idx")]
    public string s_cost_idx { get; set; }
    [XmlElement ("s_cost_name")]
    public string s_cost_name { get; set; }

    [XmlElement ("s_pos_emp_idx")]
    public string s_pos_emp_idx { get; set; }
    [XmlElement ("s_flag_main")]
    public string s_flag_main { get; set; }
    [XmlElement("s_status")]
    public string s_status { get; set; }
}