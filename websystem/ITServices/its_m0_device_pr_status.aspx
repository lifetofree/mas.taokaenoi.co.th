﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="its_m0_device_pr_status.aspx.cs" Inherits="websystem_its_m0_device_pr_status" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="row">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary pull-right" runat="server"
                    data-toggle="tooltip" title="สร้างสถานะการซื้ออุปกรณ์"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้างสถานะการซื้ออุปกรณ์</asp:LinkButton>
            </div>

            <asp:Panel ID="pnlsearch" runat="server">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>สถานะการซื้ออุปกรณ์</label>
                                <asp:TextBox ID="txtFilterKeyword" runat="server"
                                    CssClass="form-control"
                                    placeholder="สถานะการซื้ออุปกรณ์..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0_pr_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="m0_pr_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("m0_pr_idx")%>' />

                                <div class="col-md-8 col-md-offset-2">
                                    

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะการซื้ออุปกรณ์</label>
                                                <asp:UpdatePanel ID="panelpr_nameUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txtpr_nameUpdate" runat="server" CssClass="form-control"
                                                            Text='<%# Eval("pr_name")%>' />
                                                        <asp:RequiredFieldValidator ID="requiredpr_nameUpdate"
                                                            ValidationGroup="savepr_nameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtpr_nameUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกสถานะการซื้ออุปกรณ์" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">หมายเหตุ</label>
                                                <asp:UpdatePanel ID="panelpr_remarkUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtpr_remarkUpdate" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" Rows="4"
                                                            Text='<%# Eval("pr_remark")%>'
                                                            placeholder="หมายเหตุ..." />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <asp:DropDownList ID="ddlpr_statusUpdate" AutoPostBack="false" runat="server"
                                                    CssClass="form-control" SelectedValue='<%# Eval("pr_status") %>'>
                                                    <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="savepr_nameUpdate" CommandName="Update"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"
                                                data-toggle="tooltip" title="บันทึก"
                                                Text="<i class='fa fa-save fa-lg'></i> บันทึก">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                                data-toggle="tooltip" title="ยกเลิก"
                                                CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะการซื้ออุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="pr_name" runat="server" Text='<%# Eval("pr_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="pr_remark" runat="server" Text='<%# Eval("pr_remark") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="pr_status" runat="server" Text='<%# getStatus((int)Eval("pr_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    Visible="true"
                                    CommandArgument='<%# Eval("m0_pr_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <%--<i class="glyphicon glyphicon-blackboard"></i>--%>
                        <strong>&nbsp; สถานะการซื้ออุปกรณ์
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="row">


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานะการซื้ออุปกรณ์</label>
                                    <asp:UpdatePanel ID="pnlpr_name" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtpr_name" runat="server" CssClass="form-control"
                                                placeholder="สถานะการซื้ออุปกรณ์..." />
                                            <asp:RequiredFieldValidator ID="requiredpr_name"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtpr_name"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                ErrorMessage="กรุณากรอกสถานะการซื้ออุปกรณ์" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>หมายเหตุ</label>
                                    <asp:UpdatePanel ID="pnlpr_remark" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtpr_remark" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" Rows="4"
                                                placeholder="หมายเหตุ..." />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานะ</label>
                                    <asp:DropDownList ID="ddlpr_status" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <asp:LinkButton CssClass="btn btn-success" runat="server"
                                        CommandName="btnInsert" OnCommand="btnCommand"
                                        Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                        data-toggle="tooltip" title="บันทึก"
                                        ValidationGroup="save" />
                                    <asp:LinkButton CssClass="btn btn-danger"
                                        data-toggle="tooltip" title="ยกเลิก" runat="server"
                                        Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                        CommandName="btnCancel" OnCommand="btnCommand" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

</asp:Content>
