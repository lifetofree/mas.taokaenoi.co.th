﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee-probation.aspx.cs" Inherits="websystem_probation_employee_probation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
<asp:Literal ID="Literal1" runat="server"></asp:Literal>
<asp:Literal ID="check_XML" runat="server"></asp:Literal>
<asp:Literal ID="check_XML1" runat="server"></asp:Literal>
<asp:Literal ID="check_XML2" runat="server"></asp:Literal>
<asp:Literal ID="check_XML3" runat="server"></asp:Literal>
<asp:Literal ID="check_XML4" runat="server"></asp:Literal>
<asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
<!-- view Home page -->
<asp:View ID="viewHomepage" runat="server">
    <div class="col-sm-12" runat="server" id="content_Homepage">

        <asp:Repeater ID="hiddenValue" runat="server">
        <ItemTemplate>
            <asp:HiddenField ID="hidden_node_idx" runat="server"  Value='<%# Eval("node_idx_probation") %>' />
            <asp:HiddenField ID="hidden_actor_idx" runat="server" Value='<%# Eval("actor_idx_probation") %>' />
            <asp:HiddenField ID="hidden_emp_idx_assessor1" runat="server" Value='<%# Eval("emp_assessor1_idx") %>' />
            <asp:HiddenField ID="hidden_emp_idx_assessor2" runat="server" Value='<%# Eval("emp_assessor2_idx") %>' />
            <asp:HiddenField ID="hidden_hr_director_idx" runat="server" Value='<%# Eval("emp_idx_hr_director") %>' />
        </ItemTemplate>
        </asp:Repeater>

        <label>รายชื่อประเมินทดลองงานของพนักงาน</label>
        <asp:GridView ID="gvListNameProbation" Visible="true" runat="server" AutoGenerateColumns="false" DataKeyNames="emp_probation_idx"
            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
            HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="5"
            OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
                <div style="text-align: center">ไม่พบข้อมูลแบบฟอร์มการประเมิน</div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                    <ItemTemplate>
                        <div style="text-align: center; padding-top: 70px;">
                            <asp:Label ID="emp_probation_idx" runat="server" Visible="false" Text='<%# Eval("emp_probation_idx") %>' />
                            <%# (Container.DataItemIndex +1) %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                    <ItemTemplate>
                        <asp:Label ID="u0_assessment_idx" runat="server" Visible="false" Text='<%# Eval("u0_probation_idx") %>' />
                        <asp:Label ID="lb_empCode_employee" runat="server" CssClass="col-sm-12">
                            <small><p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emps_code_probation") %></p></small>
                        </asp:Label>
                        <asp:Label ID="lb_fullName_employee" runat="server" CssClass="col-sm-12">
                            <small><p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emps_fullname_probation") %></p></small>
                        </asp:Label>
                        <asp:Label ID="lb_orgName_probation" runat="server" CssClass="col-sm-12">
                            <small><p><b>องค์กร:</b> &nbsp;<%# Eval("name_org_probation") %></p></small>
                        </asp:Label>
                        <asp:Label ID="lb_dpart_probation" runat="server" CssClass="col-sm-12">
                            <small><p><b>ฝ่าย:</b> &nbsp;<%# Eval("name_dept_probation") %></p></small>
                        </asp:Label>
                        <asp:Label ID="lb_section_probation" runat="server" CssClass="col-sm-12">
                            <small><p><b>แผนก:</b> &nbsp;<%# Eval("name_sec_probation") %></p></small>
                        </asp:Label>
                        <asp:Label ID="lb_position_probation" runat="server" CssClass="col-sm-12">
                            <small><p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("name_pos_probation") %></p></small>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="วันเข้างาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                    <ItemTemplate>
                        <div style="text-align: center; padding-top: 70px;">
                            <asp:Label ID="lb_position_probation1" runat="server" CssClass="col-sm-12">
                            <small><p><b>วันเข้างาน:</b> &nbsp;<%# Eval("date_start_probation") %></p></small>
                            </asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                    <ItemTemplate>
                            <div style="text-align: center; padding-top: 50px;">
                            <asp:Label ID="lb_status_node" runat="server" ForeColor="Green" CssClass="col-sm-12">
                            <small><span><%# Eval("node_probation_status") %>&nbsp;<%# Eval("node_probation_name") %>&nbsp;<b>โดย</b>&nbsp;<%# Eval("actor_probation_name") %></span></small>
                            </asp:Label>
                            <asp:Label ID="lb_status_node_defult" runat="server" CssClass="col-sm-12" Visible="false">
                            <small><span>รอดำเนินการ&nbsp;สร้าง/แก้ไข&nbsp;<b>โดย</b>&nbsp;แผนกสรรหาว่าจ้าง</span></small>
                            </asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                    <ItemTemplate>
                        <div style="text-align: center; padding-top: 70px;">
                            <asp:LinkButton ID="btnmanage_assessment" CssClass="btn btn-success btn-sm" runat="server"
                                CommandArgument='<%#Eval("emp_probation_idx") + ";" + Eval("u0_probation_idx")  %>'
                                data-toggle="tooltip" title="ทำการประเมิน" OnCommand="btn_command" CommandName="manage_assessment">
                                <i class="glyphicon glyphicon-list-alt"></i> ประเมิน</asp:LinkButton>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:View>
<asp:View ID="viewPageEvaluation" runat="server">
    <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
    <div class="col-sm-12" runat="server" id="content_button_backhome">
        <asp:LinkButton ID="btnBackHomePage" CssClass="btn btn-info btn-sm"
            runat="server" data-toggle="tooltip" data-placement="top"
            title="กลับหน้าแรก" CommandName="comebackhome" OnCommand="btn_command">
        <i class="glyphicon glyphicon-home"></i> &nbsp; กลับหน้าแรก</asp:LinkButton>
    </div>
    <h5>&nbsp;</h5>
    <asp:Panel class="col-sm-8" runat="server" id="content_information_employee">
        <%--  <div class="col-sm-9">--%>
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <i class="glyphicon glyphicon-user"></i><strong>&nbsp; Information Employee</strong>&nbsp;[ข้อมูลพนักงาน]</h4>
            </div>
            <div class="panel-body">
                <div class="post-content">
                    <asp:Repeater ID="Information_employee" runat="server">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">รหัสพนักงาน :</font>&nbsp;</b>
                                            <asp:Label ID="lb_emps_code_probation" Text='<%# Eval("emps_code_probation") %>' runat="server">
                                        <%-- <span><%# Eval("emps_code_probation") %></span>--%></asp:Label></small>

                                </div>
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">ชื่อ - นามสกุล :</font>&nbsp;</b>
                                            <asp:Label ID="lb_emps_fullname_probation" Text='<%# Eval("emps_fullname_probation") %>' runat="server">
                                        <%-- <span><%# Eval("emps_fullname_probation") %></span>--%></asp:Label></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">องค์กร :</font>&nbsp;</b>
                                            <asp:Label ID="lb_name_org_probation" Text='<%# Eval("name_org_probation") %>' runat="server">
                                        <%-- <span><%# Eval("name_org_probation") %></span>--%></asp:Label></small>
                                </div>
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">ฝ่าย :</font>&nbsp;</b>
                                        <span><%# Eval("name_dept_probation") %></span></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">แผนก :</font>&nbsp;</b>
                                            <asp:Label ID="lb_name_sec_probation" runat="server">
                                        <span><%# Eval("name_sec_probation") %></span></asp:Label></small>
                                </div>
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">ตำแหน่ง :</font>&nbsp;</b>
                                            <asp:Label ID="lb_name_pos_probation" Text='<%# Eval("name_pos_probation") %>' runat="server">
                                        <%--  <span><%# Eval("name_pos_probation") %>--%><%--</span>--%></asp:Label></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">jobgrade :</font>&nbsp;</b>
                                        <span><%# Eval("jobgrade_level") %></span></small>
                                    <asp:Label ID="lb_jobgrade_level" runat="server" CssClass="col-sm-12"></asp:Label>
                                </div>
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">ค่าจ้างปัจจุบัน :</font>&nbsp;</b>
                                        <span>xx,xxx บาท/วัน/เดือน<%--<%# Eval("date_start_probation") %>--%></span></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">วันเริ่มงาน :</font>&nbsp;</b>
                                        <asp:Label ID="lb_date_start_probation" Text='<%# Eval("date_start_probation") %>' runat="server">
                                        <%-- <span><%# Eval("date_start_probation") %></span>--%></asp:Label></small>
                                </div>
                                <div class="col-sm-6">
                                    <small class="list-group-item-heading text_left">
                                        <b><font color="#1A5276">วันครบรอบทดลองงาน :</font>&nbsp;</b>
                                        <span><%# Eval("date_start_probation") %></span></small>
                                </div>
                            </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="col-sm-4" runat="server" id="content_statistic">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <i class="glyphicon glyphicon-user"></i><strong>&nbsp;</strong>&nbsp;ผลการประเมินรอบที่ผ่านมา</h4>
            </div>
            <div class="panel-body">
                <div class="post-content">
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12" runat="server" id="content_form_evaluation">
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="col-sm-2"></div>
                <div class="pull-left">
                    <a href="#">
                        <img class="media-object img-thumbnail"
                            src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/logoprobation.png")%>'
                            width="120px" height="120px" style="margin-top: 0px;">
                    </a>
                </div>
                <div class="post-content">
                    <div class="col-sm-6">
                        <div style="text-align: center; margin-top: 10px;">
                            <label>บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)</label><br />
                            <label><small>แบบฟอร์มบันทึกประเมินผลการทดลองงาน  119 วัน ระดับหัวหน้างาน/พนักงานรายเดือน</small></label>
                        </div>
                    </div>
                </div>
                <h5>&nbsp;</h5>
                <h5>&nbsp;</h5>
                <h5>&nbsp;</h5>
                    <asp:GridView ID="gvform_employee_type_monthly" runat="server" AutoGenerateColumns="false" DataKeyNames="m0_topics_idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="30" CellSpacing="10"
                    OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <Columns>
                        <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center" />
                            <HeaderTemplate><small><center>ลำดับ</center></small></HeaderTemplate>
                            <ItemTemplate>
                                    <div style="text-align: center;" >
                                <asp:Label ID="m0_topics_idx" runat="server" Visible="false" Text='<%# Eval("m0_topics_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                                        </div>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center" />
                            <HeaderTemplate><small>หัวข้อที่ประเมิน</small</HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lb_topics_name" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("topics_name") %>'></asp:Label>
                                        <%--  <asp:Label ID="lb_type_topics" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("emps_type_probation") %>'></asp:Label>--%>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                <small><center>ดีเยี่ยม</center>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                <div style="text-align: center;">
                                    <asp:RadioButton ID="radio_excellent" GroupName="result_point" runat="server" />
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                <small><center>ดีมาก</center>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                <div style="text-align: center;">
                                    <asp:RadioButton ID="radio_veryGood" GroupName="result_point" runat="server" />
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                <small><center>ดี</center>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                <div style="text-align: center;">
                                    <asp:RadioButton ID="radio_good" GroupName="result_point" runat="server" />
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                <small><center>พอใช้</center>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                <div style="text-align: center;">
                                    <asp:RadioButton ID="radio_fair" GroupName="result_point" runat="server" />
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                <small><center>ยังไม่น่าพอใจ</center>
                                </small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                <div style="text-align: center;">
                                    <asp:RadioButton ID="radio_unsatisfactory" GroupName="result_point" runat="server" />
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                            <HeaderStyle Width="40px" BorderWidth="1px" BorderColor="#DDDDDD" BorderStyle="Solid" />
                            <ItemStyle CssClass="img_center"/>
                            <HeaderTemplate>
                                    <small><center>หมายเหตุ</center></small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center;" >
                                    <asp:TextBox ID="txttopicsName" runat="server" CssClass="form-control"
                            placeholder="หากมีหมายเหตุ..." />
                                                </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    <asp:LinkButton CssClass="btn btn-success pull-right" data-toggle="tooltip" title="บันทึกผลการประเมิน" runat="server"
                CommandName="save_result_point" OnCommand="btn_command"> บันทึกผลการประเมิน</asp:LinkButton>

            </div>
        </div>
    </div>
        <div class="col-sm-12" runat="server" id="content_conclude" visible="false">
            <div class="panel panel-success">
                    <div class="panel-heading">
                <h4 class="panel-title">
                    <i class="glyphicon glyphicon-user"></i><strong>&nbsp;Summary Result</strong>&nbsp;สรุปผลการประเมิน</h4>
            </div>
            <div class="panel-body">
                    <div class="pull-left">
                    <%--  <div class="col-sm-12" runat="server" id="Div1">--%>
                        <div class="col-sm-6">
                        <h4><span class="label label-default">&nbsp;&nbsp;คะแนนรวม</span></h4>
                        <div class="panel panel-success">
                            <asp:Literal ID="summary_result" runat="server"></asp:Literal>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <h4><span class="label label-default">คะแนนเฉลี่ย</span></h4>
                        <div class="panel panel-success">
                            <asp:Literal ID="average_score_result" runat="server"></asp:Literal>
                        </div>
                        </div>
                    </div>
                    <div class="post-content">
                        <div class="col-sm-9">
                        <h4><span class="label label-default">สรุปผลการปฏิบัติงานที่ได้</span></h4>
                        <div class="panel panel-success">
                                   

                        </div>
                        </div>
                    </div>
            <div class="form-group">
            <div class="col-sm-12">
                <label>ความคิดเห็นเพิ่มเติมผู้ประเมิน</label>
                <asp:UpdatePanel ID="panelcomment" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtcomment" runat="server"  TextMode="MultiLine" Rows="2" CssClass="form-control"
                            placeholder="ความคิดเห็นเพิ่มเติมผู้ประเมิน..." />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                    <asp:LinkButton CssClass="btn btn-success pull-right" data-toggle="tooltip" title="ส่งความคิดเห็น" runat="server"
                CommandName="save_result_point" OnCommand="btn_command"><i class="glyphicon glyphicon-send"></i> ส่งความคิดเห็น</asp:LinkButton>
            </div>
                        
        </div>

                         

            </div>
        </div>
            </div>


</asp:View>
</asp:MultiView>

</asp:Content>

