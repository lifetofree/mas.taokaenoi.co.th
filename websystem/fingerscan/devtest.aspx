<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="devtest.aspx.cs" Inherits="websystem_fingerscan_devtest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Connection &amp; Method</div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">IP</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbIp" runat="server" CssClass="form-control" placeholder="IP" MaxLength="50" Text="192.168.120.123" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Port</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbPort" runat="server" CssClass="form-control" placeholder="Port" MaxLength="5" Text="4370" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbConnect" CssClass="btn btn-success" runat="server" data-original-title="Connect" data-toggle="tooltip" Text="Connect" OnCommand="btnCommand" CommandName="cmdConnect"></asp:LinkButton>
                                <asp:LinkButton ID="lbGetAttLog" CssClass="btn btn-info" runat="server" data-original-title="Get Log" data-toggle="tooltip" Text="Get Log" OnCommand="btnCommand" CommandName="cmdGetAttLog"></asp:LinkButton>
                                <asp:LinkButton ID="lbDelAttLog" CssClass="btn btn-warning" runat="server" data-original-title="Clear Log" data-toggle="tooltip" Text="Clear Log" OnCommand="btnCommand" CommandName="cmdDelAttLog"></asp:LinkButton>
                                <asp:LinkButton ID="lbRestartDevice" CssClass="btn btn-warning" runat="server" data-original-title="Restart Device" data-toggle="tooltip" Text="Restart Device" OnCommand="btnCommand" CommandName="cmdRestartDevice"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGetTime" CssClass="btn btn-info" runat="server" data-original-title="Get Time" data-toggle="tooltip" Text="Get Time" OnCommand="btnCommand" CommandName="cmdGetTime"></asp:LinkButton>
                                <asp:LinkButton ID="lbSetTime" CssClass="btn btn-success" runat="server" data-original-title="Set Time" data-toggle="tooltip" Text="Set Time" OnCommand="btnCommand" CommandName="cmdSetTime"></asp:LinkButton>
                                <asp:LinkButton ID="lbBeep" CssClass="btn btn-success" runat="server" data-original-title="Beep 150 ms" data-toggle="tooltip" Text="Beep 150 ms" OnCommand="btnCommand" CommandName="cmdBeep"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Device Status</label>
                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlDeviceStatus" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbCheckStatus" CssClass="btn btn-success" runat="server" data-original-title="Check" data-toggle="tooltip" Text="Check" OnCommand="btnCommand" CommandName="cmdCheckStatus"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Employee Code</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" placeholder="Employee Code" MaxLength="1000" Text="56000088" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">From IP</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbFromIp" runat="server" CssClass="form-control" placeholder="IP" MaxLength="50" Text="192.168.120.123" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">From Port</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbFromPort" runat="server" CssClass="form-control" placeholder="Port" MaxLength="4" Text="4370" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">From Finger Index</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbFromFingerIndex" runat="server" CssClass="form-control" placeholder="From Finger Index" MaxLength="2" Text="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">To IP</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbToIp" runat="server" CssClass="form-control" placeholder="IP" MaxLength="50" Text="192.168.120.123" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">To Port</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbToPort" runat="server" CssClass="form-control" placeholder="Port" MaxLength="4" Text="4370" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">To Finger Index</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbToFingerIndex" runat="server" CssClass="form-control" placeholder="From Finger Index" MaxLength="2" Text="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGetTemplate" CssClass="btn btn-info" runat="server" data-original-title="Get Template" data-toggle="tooltip" Text="Get Template" OnCommand="btnCommand" CommandName="cmdGetTemplate"></asp:LinkButton>
                                <asp:LinkButton ID="lbCopyTemplate" CssClass="btn btn-success" runat="server" data-original-title="Copy Template" data-toggle="tooltip" Text="Copy Template" OnCommand="btnCommand" CommandName="cmdCopyTemplate"></asp:LinkButton>
                                <asp:LinkButton ID="lbDeleteTemplate" CssClass="btn btn-danger" runat="server" data-original-title="Delete Template" data-toggle="tooltip" Text="Delete Template" OnCommand="btnCommand" CommandName="cmdDeleteTemplate"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGetAllTemplate" CssClass="btn btn-success" runat="server" data-original-title="Get All Template From Machine to DB, text" data-toggle="tooltip" Text="Get All Template From Machine to DB, text" OnCommand="btnCommand" CommandName="cmdGetAllTemplate"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbFromMachineToDbPerson" CssClass="btn btn-success" runat="server" data-original-title="Get from Machine to DB by Person" data-toggle="tooltip" Text="Get from Machine to DB by Person" OnCommand="btnCommand" CommandName="cmdFromMachineToDbPerson"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbFromDbToMachine" CssClass="btn btn-success" runat="server" data-original-title="Get from DB to Machine" data-toggle="tooltip" Text="Get from DB to Machine" OnCommand="btnCommand" CommandName="cmdFromDbToMachine"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGetUserInfo" CssClass="btn btn-info" runat="server" data-original-title="Get User Info" data-toggle="tooltip" Text="Get User Info" OnCommand="btnCommand" CommandName="cmdGetUserInfo"></asp:LinkButton>
                                <asp:LinkButton ID="lbGetAllUserInfo" CssClass="btn btn-info" runat="server" data-original-title="Get All User Info" data-toggle="tooltip" Text="Get All User Info" OnCommand="btnCommand" CommandName="cmdGetAllUserInfo"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbClearAdmin" CssClass="btn btn-warning" runat="server" data-original-title="Clear Admin" data-toggle="tooltip" Text="Clear Admin" OnCommand="btnCommand" CommandName="cmdClearAdmin"></asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbDelEnroll" CssClass="btn btn-danger" runat="server" data-original-title="Delete Enroll" data-toggle="tooltip" Text="Delete Enroll" OnCommand="btnCommand" CommandName="cmdDelEnroll"></asp:LinkButton>
                                <asp:LinkButton ID="lbDelAllEnroll" CssClass="btn btn-danger" runat="server" data-original-title="Delete All Enroll" data-toggle="tooltip" Text="Delete All Enroll" OnCommand="btnCommand" CommandName="cmdDelAllEnroll" OnClientClick="return confirm('Are you sure delete all enroll on this machine ?');"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Name</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbName" runat="server" CssClass="form-control" placeholder="Name" MaxLength="20" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Password</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbPassword" runat="server" CssClass="form-control" placeholder="Password" MaxLength="20" TextMode="Password" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Privilege</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbPrivilege" runat="server" CssClass="form-control" placeholder="Privilege" MaxLength="1" Text="0" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Enabled</label>
                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlEnabled" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="true" Value="true" />
                                    <asp:ListItem Text="false" Value="false" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbSetInfo" CssClass="btn btn-success" runat="server" data-original-title="Set Info" data-toggle="tooltip" Text="Set Info" OnCommand="btnCommand" CommandName="cmdSetInfo"></asp:LinkButton>
                                <asp:LinkButton ID="lbGetUserCard" CssClass="btn btn-info" runat="server" data-original-title="Get Card" data-toggle="tooltip" Text="Get Card" OnCommand="btnCommand" CommandName="cmdGetUserCard"></asp:LinkButton>
                                <asp:LinkButton ID="lbSetCard" CssClass="btn btn-success" runat="server" data-original-title="Set Card" data-toggle="tooltip" Text="Set Card" OnCommand="btnCommand" CommandName="cmdSetUserCard"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="form-horizontal" role="form">
                        <label class="col-sm-4 control-label">Scanner Function</label>
                        <div class="col-md-8">
                            <asp:LinkButton ID="lbScannerInit" CssClass="btn btn-info" runat="server" data-original-title="Init" data-toggle="tooltip" Text="Scanner Init" OnCommand="btnCommand" CommandName="cmdScannerInit"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Result</div>
                <div class="panel-body">
                    <asp:Literal ID="litResult" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
