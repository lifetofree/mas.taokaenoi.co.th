﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImproperCalendarEvent
/// </summary>
public class ImproperCalendarEvent
{
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public string place_name { get; set; }
    public string room_name_th { get; set; }
    public bool allDay { get; set; }
    public string topic_booking { get; set; }
    public string emp_name_th { get; set; }
}