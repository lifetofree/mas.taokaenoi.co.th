﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_news.aspx.cs" Inherits="websystem_hr_hr_news" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <style type="text/css">
        .embed-responsive {
            position: relative;
            display: block;
            height: 0;
            padding: 0;
            overflow: hidden;
        }


        /*      body {
  overflow-y: scroll;
}*/

        .desc {
            /*font-family: 'Kanit', 'Tahoma', 'Geneva', 'sans-serif' !important;*/
            font-family: Kanit, Tahoma, Geneva, sans-serif !important;
            font-weight: bold;
            display: table-cell;
            color: black;
            font-size: 14px;
        }

        .center {
            /*  font-family: Georgia, serif;
                color: #369;
                font-size: 36px; */
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }

        .img-container {
            text-align: center;
        }

        .danger {
            background-color: red; /* Green */
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
        }

        .checked {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: normal;
        }

        p.thick {
            font-weight: bold;
        }

        .onclickcolor:active {
        }
        /* .btn:focus{
    background: red;
}*/

        a.onclickcolors:hover {
            color: red;
        }

        a:active {
        }
        /*        .list-group-item {
            margin-bottom:0px;


        }*/
        .borderRadius {
            border-left: 1px solid;
            border-right: 1px solid;
            border-top: 0px solid;
            border-bottom: 1px solid;
            border-radius: 0px 0px 0px 0px;
        }

        .preimg {
            border: 0px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 150px;
        }

            .preimg:hover {
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }

        .prepdf {
            border: 0px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 75px;
        }

            .prepdf:hover {
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }

        #carousel-example-generic.carousel .item {
            height: auto;
        }

        .respon {
            width: 100%;
            height: auto;
        }

        /*.item img {
            position: absolute;
            object-fit: cover;
            top: 0;
            left: 0;
            min-height: 300px;
        }*/

        visiblebutton {
            visibility: visible;
        }

        /*        @media (max-width: 736px) {
            #carousel-example-generic.carousel .item {
                height: 110px;
            }
        }*/

        #mask {
            position: absolute;
            left: 0;
            top: 0;
            z-index: 9000;
            background-color: #26262c;
            display: none;
        }

        #boxes .window {
            position: absolute;
            left: 0;
            top: 0;
            width: 440px;
            height: 850px;
            display: none;
            z-index: 9999;
            padding: 20px;
            border-radius: 5px;
            text-align: center;
        }

        #boxes #dialog {
            width: auto;
            height: auto;
            padding: 10px 10px 10px 10px;
            background-color: #ffffff;
            font-size: 15pt;
        }

        .agree:hover {
            background-color: #D1D1D1;
        }

        .popupoption:hover {
            background-color: #D1D1D1;
            color: green;
        }

        .popupoption2:hover {
            color: red;
        }
    </style>

    <div class="col-sm-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbtab0" runat="server" CommandName="cmdtab0" OnCommand="navCommand" CommandArgument="tab0"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab1" OnCommand="navCommand" CommandArgument="tab1"> สร้างประเภท</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> สร้างหัวข้อย่อย</asp:LinkButton>
                        </li>
                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbtab3" runat="server" CommandName="cmdtab3" OnCommand="navCommand" CommandArgument="tab3"> สร้างรายการ</asp:LinkButton>
                        </li>
                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbtab4" runat="server" CommandName="cmdView" OnCommand="navCommand" CommandArgument="tab4"> อัพโหลดรูป</asp:LinkButton>
                        </li>
                        <%--<li id="li4" runat="server">
                            <asp:LinkButton ID="lbtab4" runat="server" CommandName="cmdtab3" OnCommand="navCommand" class="dropdown-toggle" data-toggle="dropdown"
                                role="button" aria-haspopup="true" aria-expanded="false" CommandArgument="tab4"> อัพโหลดรูป <span class="caret"></span></asp:LinkButton>
                            <ul class="dropdown-menu">
                                <li>
                                    <asp:LinkButton ID="lbtab4_1" runat="server" data-original-title="" data-toggle="tooltip" OnCommand="navCommand"
                                        CommandArgument="tab4_1" CommandName="cmdView"> อัพโหลดรูปสไลด์</asp:LinkButton>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <asp:LinkButton ID="lbtab4_2" runat="server" data-original-title="" data-toggle="tooltip" OnCommand="navCommand"
                                        CommandArgument="tab4_2" CommandName="cmdView"> อัพโหลดรูปหน้าแรก</asp:LinkButton>
                                </li>

                            </ul>
                        </li>--%>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1JVvWVA3x7v8fPNrXopS_SIccQZBW-EOvFzw830vyBao/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="mvNews" runat="server">

        <asp:View ID="vTypeNews" runat="server">
            <div class="col-sm-12">
                <asp:FormView ID="searchTypeNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">



                                            <div class="form-group">
                                                <asp:Label ID="lbs_listnews" runat="server" CssClass="col-sm-2 control-label">ค้นหา</asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tb_serchnews_s" runat="server" CssClass="form-control" placeholder="กรอกชื่อประเภทข่าวสาร">

                                                    </asp:TextBox>


                                                </div>


                                                <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>

                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-10 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument=",TypeNewsmange">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument=",TypeNewsmange" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;รีเซ็ต
                                                
                                                    </asp:LinkButton>
                                                </div>
                                                <%-- <label class="col-sm-3 control-label"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <div class="form-group">
                    <asp:LinkButton ID="lbCreateTypenews" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument=",TypeNews"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                </div>

                <asp:FormView ID="FvTypeNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่ม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="labeltypenews" runat="server" class="col-sm-3 control-label">ชื่อประเภทข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_typenews" runat="server" CssClass="form-control" placeholder="กรอกชื่อประเภทข่าวสาร" MaxLength="250">
                                                    </asp:TextBox>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldtb_typenews" runat="server" ControlToValidate="tb_typenews" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_typenews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_typenews" Width="220" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# "0" + "," + "TypeNews" + ","%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",TypeNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไข</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="labeltypenews" runat="server" class="col-sm-3 control-label">ชื่อประเภทข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_typenews" runat="server" CssClass="form-control" Text='<%# Eval("type_name") %>' placeholder="กรอกชื่อประเภทข่าวสาร" MaxLength="250">
                                                    </asp:TextBox>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldtb_typenews" runat="server" ControlToValidate="tb_typenews" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" SetFocusOnError="true" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_typenews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_typenews" Width="220" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("type_status") %>'>
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("type_idx") + "," + "TypeNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",TypeNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <asp:GridView ID="gvTypeNews" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-sm-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="Master_PageIndexChanging">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">

                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvtype" runat="server" Visible="false" Text='<%# Eval("type_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="ชื่อประเภทข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvname" runat="server" Text='<%# Eval("type_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvstatus" runat="server" Text='<%# convertStatus((int)Eval("type_status")) %>'
                                        CssClass='<%# convertCss((int)Eval("type_status")) %>'>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไข" CommandArgument='<%# Eval("type_idx") + "," + "TypeNews"%>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                    <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("type_idx") + "," + "TypeNews"%>' title="ลบ"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="vTitleNews" runat="server">
            <div class="col-sm-12">
                <asp:FormView ID="searchTitleNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="lbs_titlenews" runat="server" CssClass="col-sm-2 control-label">ค้นหา</asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tb_serchnews_s" runat="server" CssClass="form-control" placeholder="กรอกชื่อประเภทหรือหัวข้อข่าวสาร..">

                                                    </asp:TextBox>


                                                </div>


                                                <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>

                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-10 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument=",TitleNewsmange">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument=",TitleNewsmange" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;รีเซ็ต
                                                
                                                    </asp:LinkButton>
                                                </div>
                                                <%-- <label class="col-sm-3 control-label"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <div class="form-group">
                    <asp:LinkButton ID="lbCreateTitleNews" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument=",TitleNews"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                </div>

                <asp:FormView ID="FvTitleNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่ม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Labeltypenews" runat="server" Text="ประเภทข่าวสาร" Class="col-sm-3 control-label">
                                               ประเภทข่าวสาร<span class="text-danger"> *</span>
                                                </asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDowntypeNews" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDowntypeNews" runat="server" ControlToValidate="DropDowntypeNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDowntypeNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDowntypeNews" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="labeltitlenews" runat="server" class="col-sm-3 control-label">หัวข้อย่อยข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_titlenews" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อย่อยข่าวสาร" MaxLength="250">
                                                    </asp:TextBox>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldtb_titlenews" runat="server" ControlToValidate="tb_titlenews" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_titlenews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_titlenews" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Lbtypefile" runat="server" Text="กำหนดรูปแบบไฟล์" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltypefile" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---กรุณาเลือก---" />
                                                        <asp:ListItem Value="1" Text="ไฟล์รูปภาพ" />
                                                        <asp:ListItem Value="0" Text="ไฟล์เอกสาร" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldddltypefile" runat="server" ControlToValidate="ddltypefile" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutddltypefile" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddltypefile" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# "0" + "," + "TitleNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",TitleNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไข</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="labeltypenews" runat="server" class="col-sm-3 control-label">ชื่อประเภทข่าวสาร<span class="text-danger">*</span></asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDowntypeNews" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDowntypeNews" runat="server" ControlToValidate="DropDowntypeNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDowntypeNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDowntypeNews" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="labeltitlenews" runat="server" class="col-sm-3 control-label">หัวข้อย่อยข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_titlenews" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อย่อยข่าวสาร" MaxLength="250" Text='<%# Eval("title_name") %>'>
                                                    </asp:TextBox>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldtb_titlenews" runat="server" ControlToValidate="tb_titlenews" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_titlenews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_titlenews" Width="220" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Lbtypefile" runat="server" Text="กำหนดรูปแบบไฟล์" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddltypefile" CssClass="form-control" runat="server" Text='<%# Eval("title_type_file") %>'>

                                                        <asp:ListItem Value="1" Text="ไฟล์รูปภาพ" />
                                                        <asp:ListItem Value="0" Text="ไฟล์เอกสาร" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldddltypefile" runat="server" ControlToValidate="ddltypefile" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutddltypefile" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddltypefile" Width="220" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("title_status") %>'>
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("title_idx") + "," + "TitleNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",TitleNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>

                <asp:GridView ID="gvTitleNews" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-sm-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="Master_PageIndexChanging">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">

                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvtitle_idx" runat="server" Visible="false" Text='<%# Eval("title_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="ชื่อประเภทข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvtype_idx" runat="server" Text='<%# Eval("type_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หัวข้อข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvname" runat="server" Text='<%# Eval("title_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รูปแบบไฟล์แนบ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvtypefile" runat="server" Text='<%# convertFileType((int)Eval("title_type_file")) %>'>
                                
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvstatus" runat="server" Text='<%# convertStatus((int)Eval("title_status")) %>'
                                        CssClass='<%# convertCss((int)Eval("title_status")) %>'>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไข" CommandArgument='<%# Eval("title_idx") + "," + "TitleNews"%>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                    <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("title_idx") + "," + "TitleNews"%>' title="ลบ"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>


        <asp:View ID="vListNews" runat="server">
            <div class="col-sm-12">
                <asp:FormView ID="searchListNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">


                                            <div class="form-group">
                                                <asp:Label ID="Labeltypenews" runat="server" Text="ประเภทข่าวสาร" Class="col-sm-2 control-label">
                                               
                                                </asp:Label>

                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDowntypeNews_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Label ID="LabelTitleNews" runat="server" Text="หัวข้อข่าวสาร" Class="col-sm-2 control-label">
                                            
                                                </asp:Label>

                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownTitleNews_s" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="lbs_listnews" runat="server" CssClass="col-sm-2 control-label">ค้นหา</asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="tb_serchnews_s" runat="server" CssClass="form-control" placeholder="กรอกชื่อข่าวสารหรือรายละเอียด..">

                                                    </asp:TextBox>


                                                </div>




                                                <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>

                                            </div>



                                            <div class="form-group">



                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-2 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-4">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />



                                                </div>

                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-2 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-4">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" value="" />

                                                </div>

                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-10 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument=",ListNewsmange">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument=",ListNewsmange" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;รีเซ็ต
                                                
                                                    </asp:LinkButton>


                                                </div>
                                                <%--  <label class="col-sm-3 control-label"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>



                <div class="form-group">
                    <asp:LinkButton ID="lbCreateListNews" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument=",ListNews"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                </div>

                <asp:FormView ID="FvListNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่ม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Labeltypenews" runat="server" Text="ประเภทข่าวสาร" Class="col-sm-3 control-label">
                                               ประเภทข่าวสาร<span class="text-danger"> *</span>
                                                </asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDowntypeNews" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDowntypeNews" runat="server" ControlToValidate="DropDowntypeNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDowntypeNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDowntypeNews" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LabelTitleNews" runat="server" Text="หัวข้อข่าวสาร" Class="col-sm-3 control-label">
                                               หัวข้อข่าวสาร<span class="text-danger"> *</span>
                                                </asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDownTitleNews" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownTitleNews" runat="server" ControlToValidate="DropDownTitleNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownTitleNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownTitleNews" Width="220" />
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="labellistnewsname" runat="server" class="col-sm-3 control-label">ชื่อข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_listnewsname" runat="server" CssClass="form-control" placeholder="กรอกชื่อข่าวสาร" MaxLength="250">
                                                    </asp:TextBox>
                                                    <%--<asp:TextBox ID="txtTinyMCE" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldtb_listnewsname" runat="server" ControlToValidate="tb_listnewsname" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_listnewsname" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_listnewsname" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="labellistnewsDesc" runat="server" class="col-sm-3 control-label">รายละเอียดข่าวสาร</asp:Label>
                                                <div class="col-sm-6">
                                                    <%--<asp:TextBox ID="tb_listnewsDesc" runat="server" CssClass="form-control" placeholder="กรอกรายละเอียดข่าวสาร" MaxLength="2000">
                                                </asp:TextBox>--%>
                                                    <%--<asp:TextBox ID="txtTinyMCE" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
                                                    <%--  <asp:UpdatePanel ID="updateRoomInsert" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                                    <asp:TextBox ID="tb_listnewsDesc" runat="server" TextMode="MultiLine" CssClass="tinymce"></asp:TextBox>
                                                    <%--    </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>
                                            <%--           <asp:UpdatePanel ID="updateRoomInsert" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                            <div class="form-group">
                                                <asp:Label ID="labelFile_Upload" runat="server" class="col-sm-3 control-label">ไฟล์แนบ</asp:Label>
                                                <div class="col-sm-6">
                                                    <%-- <asp:FileUpload ID="File_Upload" runat="server"/>--%>
                                                    <%-- <asp:UpdatePanel ID="File_UploadUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                                    <asp:FileUpload ID="File_Upload" Font-Size="small" ViewStateMode="Enabled" runat="server" ClientIDMode="Static" CssClass="control-label UploadFileImage multi max-1 with-preview" />
                                                    <%--onchange="preview_image(event)"--%>
                                                    <%-- <div id="checkfileimg" runat="server" visible="false">
                                                        <img id="output_image" style="max-width: 300px;" />
                                                    </div>

                                                    <div id="checkfilepdf" runat="server" visible="false">
                                                        <i id="output_pdf" class=""></i>
                                                    </div>--%>

                                                    <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </div>
                                                <div class="col-sm-3">


                                                    <asp:CheckBox ID="checkBoxDelete" runat="server" CssClass="checked" Visible="false" Text=" &nbsp; Delete File"></asp:CheckBox>
                                                </div>
                                            </div>

                                            <%--                                          </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            <%--<asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-3 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-3 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" value="" />

                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>
                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>

                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" OnClientClick="return confirm('คุณต้องการสร้างรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# "0" + "," + "ListNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",ListNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไข</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Labeltypenews" runat="server" Text="ประเภทข่าวสาร" Class="col-sm-3 control-label">
                                               ประเภทข่าวสาร<span class="text-danger"> *</span>
                                                </asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDowntypeNews" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDowntypeNews" runat="server" ControlToValidate="DropDowntypeNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDowntypeNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDowntypeNews" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="LabelTitleNews" runat="server" Text="หัวข้อข่าวสาร" Class="col-sm-3 control-label">
                                               หัวข้อข่าวสาร<span class="text-danger"> *</span>
                                                </asp:Label>

                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDownTitleNews" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownTitleNews" runat="server" ControlToValidate="DropDownTitleNews" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownTitleNews" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownTitleNews" Width="220" />
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="labellistnewsname" runat="server" class="col-sm-3 control-label">ชื่อข่าวสาร<span class="text-danger">*</span></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="tb_listnewsname" runat="server" CssClass="form-control" placeholder="กรอกชื่อข่าวสาร" MaxLength="250" Text='<%# Eval("list_name") %>'>
                                                    </asp:TextBox>

                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldtb_listnewsname" runat="server" ControlToValidate="tb_listnewsname" ErrorMessage="*กรุณาใส่ข้อมูลให้ถูกต้อง" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttb_listnewsname" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtb_listnewsname" Width="220" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="labellistnewsDesc" runat="server" class="col-sm-3 control-label">รายละเอียดข่าวสาร</asp:Label>
                                                <div class="col-sm-6">
                                                    <%-- <asp:TextBox ID="tb_listnewsDesc" runat="server" CssClass="form-control" placeholder="กรอกรายละเอียดข่าวสาร" MaxLength="2000" Text='<%# Eval("list_desc") %>'>
                                                </asp:TextBox>--%>
                                                    <%--<asp:TextBox ID="txtTinyMCE" runat="server" TextMode="MultiLine"></asp:TextBox>--%>
                                                    <asp:TextBox ID="tb_listnewsDesc" runat="server" TextMode="MultiLine" CssClass="tinymce" Text='<%#  HttpUtility.HtmlDecode(HttpUtility.HtmlDecode((string)Eval("list_desc"))) %>'></asp:TextBox>


                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="label2" runat="server" class="col-sm-3 control-label">ไฟล์แนบ</asp:Label>
                                                <div class="col-sm-6">
                                                    <%-- <asp:FileUpload ID="File_Upload" runat="server"/>--%>
                                                    <asp:FileUpload ID="File_Upload" Font-Size="small" ViewStateMode="Enabled" runat="server" ClientIDMode="Static" CssClass="control-label UploadFileImage multi max-1 with-preview" />
                                                    <%-- <asp:LinkButton ID="LinkButtonedititem" runat="server"><%# HaveFile((string)Eval("list_file") )%></asp:LinkButton>--%>

                                                    <%-- <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand"><i class = "fa fa-trash"></i>&nbsp;Delete File</asp:LinkButton>--%>

                                                    <div id="preimgList" runat="server"></div>

                                                    <asp:CheckBox ID="checkBoxDelete" runat="server" CssClass="checked" Text=" &nbsp; Delete File"></asp:CheckBox>
                                                    <%--OnClientClick="window.open('/mas.taokaenoi.co.th/uploadfiles/hr_news/65/65.png'); return false;"--%>
                                                    <asp:TextBox ID="tbFile_Upload" runat="server" Visible="false" CssClass="form-control" MaxLength="250" Text='<%# Eval("list_file") %>'>
                                                    </asp:TextBox>
                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>

                                            <%--<asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>--%>
                                            <%--<div class="form-group">
                                                <asp:label ID="labelstartdate" runat="server" class="col-sm-3 control-label">ตั้งแต่วันที่</asp:label>
                                                <div class="col-sm-6">
                                                    
                                                        <asp:HiddenField ID="txtSearchFromHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="วันที่เริ่มต้นจอง..." Text='<%# Eval("list_timeout_start") %>'
                                                            CssClass="form-control datetimepicker-from cursor-pointer" />
                                                        
                                                        <asp:RequiredFieldValidator ID="Re_txtDateStart_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateStart_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่เริ่มต้นจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateStart_create" Width="200" PopupPosition="BottomLeft" />

                                                    
                                                </div>
                                                 <div class="col-sm-3">
                                                </div>
                                                </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-3 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" Text='<%# Eval("list_timeout_start") %>' />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>

                                            <%--<div class="form-group">
                                                <asp:label ID="labelenddate" runat="server" class="col-sm-3 control-label">ถึงวันที่</asp:label>
                                                <div class="col-sm-6">
                                                    
                                                        <asp:HiddenField ID="txtSearchToHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateEnd_create" runat="server" placeholder="วันที่สิ้นสุดการจอง..."
                                                            CssClass="form-control datetimepicker-from cursor-pointer" />
                                                       
                                                        <asp:RequiredFieldValidator ID="Re_txtDateEnd_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateEnd_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่สิ้นสุดการจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateEnd_create" Width="200" PopupPosition="BottomLeft" />
                                                   
                                                </div>
                                                 <div class="col-sm-3">
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-3 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด ..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" Text='<%# Eval("list_timeout_end") %>' />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>
                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>

                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("list_status") %>'>
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("list_idx") + "," + "ListNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",ListNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>

                <asp:GridView ID="gvListNews" runat="server" AutoGenerateColumns="false" Width="100%"
                    CssClass="table table-striped table-bordered table-responsive footable col-sm-12"
                    HeaderStyle-CssClass="info"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="Master_PageIndexChanging">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>





                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">


                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvlist_idx" runat="server" Visible="false" Text='<%# Eval("list_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex +1) %>
                                </div>


                            </ItemTemplate>


                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="ชื่อประเภทข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvtype_idx" runat="server" Text='<%# Eval("type_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvtitle_name" runat="server" Text='<%# Eval("title_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รูปแบบไฟล์" HeaderStyle-CssClass="text-center" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvtitle_type_file" runat="server" Text='<%# convertFileType((int)Eval("title_type_file")) %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อข่าวสาร" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvname" runat="server" Text='<%# Eval("list_name") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <%--<asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>
                            <asp:Label ID="gvlist_desc" runat="server" Text='<%#  HttpUtility.HtmlDecode((string)Eval("list_desc")) %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                        <%-- <asp:TemplateField HeaderText="ไฟล์แนบ" HeaderStyle-CssClass="text-center">
                        <ItemTemplate>


                            <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("list_file")) %>' class="img-responsive" />
                            <asp:LinkButton ID="LinkButtonImage" runat="server" Text="View" Font-Bold="True" OnCommand="btnCommand" CommandName="cmdView" CommandArgument='<%# Eval("list_file") + "," + "ListNews"%>'>
                            </asp:LinkButton>
                            <%--   <asp:Label ID="lb111" runat="server" text='<%# (string) FormatImageUrl((string) Eval("list_file")) %>'></asp:Label>--%>

                        <%-- <div id="file_pdf" runat="server"><i class="far fa-file-pdf"></i></div>
                            <asp:LinkButton ID="LinkButtonDoc" runat="server" Text="View" Font-Bold="True" OnCommand="btnCommand" CommandName="cmdView" CommandArgument='<%# Eval("list_file") + "," + "ListNews"%>'>
                            </asp:LinkButton>--%>



                        <%-- </ItemTemplate>
                    </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="วันที่เริ่มต้น" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvlist_timeout_start" runat="server" Text='<%# Eval("list_timeout_start") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สิ้นสุด" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvlist_timeout_end" runat="server" Text='<%# Eval("list_timeout_end") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvstatus" runat="server" Text='<%# convertStatus((int)Eval("list_status")) %>'
                                        CssClass='<%# convertCss((int)Eval("list_status")) %>'>
                                    </asp:Label>
                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>


                                <div style="text-align: center; padding-top: 5px;">


                                    <%--<asp:LinkButton ID="viewdesc" runat="server" CssClass="viewdesc" data-toggle="collapse" OnClientClick="" ><i class="fas fa-eye"></i></asp:LinkButton>--%>

                                    <div id="viewdesc" class="btn btn-warning" style="cursor: pointer"><i class="fas fa-eye"></i></div>

                                    <%-- <asp:LinkButton id="viewdesc" runat="server" OnClientClick="return false;" class="btn btn-warning" style="cursor: pointer" ><i class="fas fa-eye"></i></asp:LinkButton>--%>

                                    <%-- <asp:LinkButton ID="LinkButtonTest" CssClass="text-edit" runat="server" CommandName="cmdTest" data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไข" CommandArgument='<%# Eval("list_idx") + "," + "ListNews"%>'>test</asp:LinkButton>
                                    --%>
                                    <%-- <div id="collapsegvListNewsDesc" class="panel-collapse collapse collapse555" runat="server">--%>

                                    <div id="tabledesc" runat="server" class="panel-collapse collapse">
                                        <%--<asp:GridView ID="gvListNewsDesc" runat="server" AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-responsive footable col-sm-12"
                                        HeaderStyle-CssClass="info">
                                        <Columns>
                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:Label ID="gvlblist_desc" runat="server" Text='<%#Error((string)Eval("list_desc")) %>'>--%>
                                        <%--<div id="test" runat="server"></div>
                                                        
                                                      <center>
                                               
                                                          <div id="test2" runat="server"></div>
                                                             
                                                         </center>  
                                       <%-- <asp:Literal ID="test1" runat="server"></asp:Literal>
                                      </asp:Label>



                                    </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>--%>


                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">
                                                    <center>
                                               <div class="desc" >รายละเอียดข่าวสาร</div>
                                                </center>
                                                </h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="panel-heading">
                                                    <div class="form-horizontal" role="form">
                                                        <div class="panel-heading">

                                                            <div id="collapseDesc" runat="server"></div>

                                                            <center>
                                               
                                                          <div id="collapsefile" runat="server"></div>
                                                             
                                                         </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>





                                    <%-- <asp:LinkButton ID="lbView">tt</i></asp:LinkButton>--%>

                                    <%--<asp:LinkButton ID="lbViewcdsd" runat="server" data-placement="top" title="ดู" CommandArgument='<%# Eval("list_idx") %>'><i class="fas fa-eye"></i></asp:LinkButton>--%>

                                    <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไข" CommandArgument='<%# Eval("list_idx") + "," + "ListNews"%>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                    <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("list_idx") + "," + "ListNews"%>' title="ลบ"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>




        </asp:View>

        <%-- <asp:DataList ID="datalistimg" runat="server">
                                            <ItemTemplate>

                                            
                                         <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") %>' class="img-responsive" Width="75%" />

                                                </ItemTemplate>
                                            </asp:DataList>--%>
        <asp:View ID="vNews" runat="server">

            <%--style="overflow-y: hidden; padding-right: 15px; position: fixed;"--%>
            <%--style="overflow-y: hidden; padding-right: 15px; position: fixed; min-height: 100px;"--%>
            <div id="divmyModal" runat="server">
                <%--<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
               
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">
                                
                                <asp:Repeater ID="Repeatermodal" runat="server">
                                    <ItemTemplate>

                                        <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") %>' class="img-fluid" Width="100%" />
                                        
                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                                   
                            </div>

                        </div>
                    </div>
                    </div>--%>

                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; padding-right: 15px; position: fixed;">

                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-body">

                                <asp:Repeater ID="Repeatermodal" runat="server">
                                    <ItemTemplate>

                                        <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") + "?r=" + Guidimagecache() %>' class="img-fluid" Width="100%" />


                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <%--<div id="divmyModal" runat="server">
           
            <div id="boxes">
<div style="top: 50%; left: 50%; display: none;" id="dialog" class="window"> 
<div id="san">
    <asp:Repeater ID="Repeatermodal" runat="server">
                                    <ItemTemplate>

                                        <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") %>' class="img-fluid" Width="100%" />
                                        
                                        
                                    </ItemTemplate>
                                </asp:Repeater>
</div>
</div>
<div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>
</div>--%>

            <script type="text/javascript">


                $(document).ready(function () {
                    $("#myModal").modal('show');
                    //alert("sadds");
                });



                //const showDialog = () => {
                //    document.getElementById('dialog').classList.add('show')
                //    const scrollY = document.documentElement.style.getPropertyValue('--scroll-y');
                //    const body = document.body;
                //};
                //$(window).on('load', function () {
                //    $('#myModal').modal('show');
                //    //alert("sadds");
                //});

                //setTimeout(function () {
                //    $('#myModal').modal();
                //}, 2000);



            </script>

            <div class="col-sm-12">




                <div id="divcarousel" runat="server">
                    <div class="form-group">

                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <asp:Repeater ID="imagesNumber" runat="server">
                                    <ItemTemplate>
                                        <li data-target="#carousel-example-generic" data-slide-to='<%# Eval("slide_idx") %>' class="<%# (Container.ItemIndex == 0 ? "active" : "") %>"></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ol>
                            <!-- Wrapper for slides -->

                            <div class="carousel-inner" role="listbox">

                                <asp:Repeater ID="images" runat="server">
                                    <ItemTemplate>
                                        <div class="item <%# (Container.ItemIndex == 0 ? "active" : "") %>">


                                            <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") + "?r=" + Guidimagecache() %>' CssClass="img-fluid" Width="100%" />


                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>


                            <!-- Controls -->

                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>

                        </div>


                    </div>
                </div>






                <div class="panel panel-default" style="background-color: dodgerblue;">
                    <div class="panel-body">


                        <div class="input-group">
                            <div class="input-group-btn">

                                <asp:LinkButton ID="btnHome" runat="server" Font-Bold="true" CommandName="cmdHome" OnCommand="btnCommand" CommandArgument=",ListNews" CssClass="form-control btn btn-default" data-toggle="tooltip"><i class="fa fa-home" aria-hidden="true"></i>  หน้าแรก</asp:LinkButton>

                            </div>
                            <div class="col-sm-5 input-group pull-right">
                                <asp:TextBox ID="txtsearch" runat="server" placeholder="ค้นหาชื่อข่าวสารหรือรายละเอียดข่าวสาร . . ."
                                    CssClass="form-control"></asp:TextBox>
                                <span class="input-group-btn">
                                    <asp:LinkButton ID="btnsearch" runat="server" CommandName="cmdSearch" data-toggle="tooltip"
                                        OnCommand="btnCommand" CommandArgument=",ListNews" BackColor="limegreen" CssClass="form-control btn btn-success" aria-hidden="true">ค้นหา</asp:LinkButton>
                                </span>

                            </div>
                        </div>


                    </div>
                </div>

            </div>



            <div class="col-sm-3">
                <div class="list-group">

                    <asp:LinkButton ID="LinkButtonTypeNews" runat="server" BackColor="#d9edf7" Font-Bold="true" ForeColor="black" CssClass="list-group-item"><h5>ประเภทข่าวสาร</h5></asp:LinkButton>
                    <asp:Repeater ID="rplistnews" runat="server" OnItemDataBound="rpt_item">
                        <ItemTemplate>


                            <asp:LinkButton ID="btnmenuTypeNews" runat="server" data-toggle="collapse" href=".collapse4" data-placement="top" title=""
                                CommandArgument='<%#Eval("type_idx") + "," + "TypeNews" %>' BorderColor="SkyBlue" CssClass="list-group-item btn-info">
                               <p class="card-title"><i class="fa fa-tags" aria-hidden="true"></i>  <%# Eval("type_name")%></p>
                                 <%-- <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; <%# Eval("type_name") %></p>--%>
                            </asp:LinkButton>

                            <div id="collapse4" class="panel-collapse collapse collapse4" runat="server">

                                <asp:Repeater ID="RepeaterView" runat="server">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnmenuTypeNews" runat="server" data-toggle="tooltip" data-placement="top" title=""
                                            CommandName="cmdReadMore" OnCommand="btnCommand"
                                            CommandArgument='<%#Eval("title_idx") + "," + "TitleNews" %>' BorderColor="SkyBlue" CssClass="list-group-item borderRadius" Style="border-radius: 0px;">
                               <p class="card-title"><i class="fas fa-angle-right" aria-hidden="true"></i>  <%# Eval("title_name")%></p>
                                 <%-- <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; <%# Eval("type_name") %></p>--%>
                                        </asp:LinkButton>
                                        <%--border-bottom: 0px solid;--%>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                        </ItemTemplate>



                    </asp:Repeater>



                </div>
            </div>








            <div id="menulistNews" runat="server" class="col-sm-9">
                <div class="list-group">
                    <asp:LinkButton ID="LinkButtonlistNews" runat="server" BackColor="#d9edf7" Font-Bold="true" ForeColor="black" CssClass="list-group-item"><h5>รายการข่าวสาร</h5></asp:LinkButton>
                    <asp:Repeater ID="RepeaterListNews" runat="server" OnItemDataBound="check_item">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnmenulistNews" runat="server" data-toggle="collapse" href=".collapse4" data-placement="top" title=""
                                CommandArgument='<%#Eval("list_idx") + "," + "ListNews" %>' BorderColor="SkyBlue"
                                CssClass='<%#convertCssOnclick((string)Eval("list_idx").ToString()) %>'
                                Enabled=' <%#convertbuttonOnclick((int)Eval("list_idx")) %>'>
                        

                               <p class="card-title">  
                                   <b><%# Eval("list_name") + " " %> </b> 
                                   ประเภท : <%# Eval("type_name") + " "  %>
                                    หัวข้อย่อย : <%#  Eval("title_name") %>
                               </p>
                                    
                            </asp:LinkButton>


                            <asp:Label ID="lbtest" runat="server" Text="Empty" Visible="false"></asp:Label>


                            <div id="collapse4" class="panel-collapse collapse collapse4" runat="server">

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <center>
                                               <div class="desc" >รายละเอียดข่าวสาร</div>
                                                </center>
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="panel-heading">
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">

                                                    <div id="collapseDesc" runat="server"></div>

                                                    <center>
                                               
                                                          <div id="collapsefile" runat="server"></div>
                                                             
                                                         </center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </ItemTemplate>
                        <%-- <FooterTemplate>
        <%-- Label used for showing Error Message 

                           <div id="ErrorMessage2" runat="server">sdasdsd</div>
        <asp:Label ID="ErrorMessage" runat="server" Text="Sorry!!" Visible="false">
        </asp:Label>
    </FooterTemplate>--%>
                    </asp:Repeater>
                    <asp:LinkButton ID="btnmenuall_news" runat="server" Text="ดูรายการทั้งหมด...." data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="ดูรายการทั้งหมด" CommandName="btnmenuall_news" OnCommand="btnCommand" CommandArgument=",btnmenuall_news"
                        BackColor="dodgerblue" ForeColor="White" CssClass="list-group-item btn-success" Style="border: unset"></asp:LinkButton>

                    <asp:LinkButton ID="btnmenuall_Hidden" runat="server" Text="ซ่อนรายการทั้งหมด...." data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="ซ่อนรายการทั้งหมด" Visible="false" CommandName="btnmenuall_Hidden" CommandArgument=",btnmenuall_Hidden" OnCommand="btnCommand" BackColor="dodgerblue" ForeColor="White" CssClass="list-group-item btn-success" Style="border: unset"></asp:LinkButton>
                    <asp:HyperLink ID="HyperLink1" runat="server"></asp:HyperLink>
                </div>
            </div>





            <%--            <div id="fvlistdetail" runat="server" class="col-sm-9" visible="false">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <asp:Literal ID="LiteralName" runat="server"></asp:Literal>

                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel-heading">

                            <div class="form-horizontal" role="form">

                                <div class="panel-heading">
                                    <asp:Label ID="lbtext" runat="server">
                                        <asp:Literal ID="LiteralDesc" runat="server"></asp:Literal>



                                        <asp:Literal ID="ltEmbed" runat="server"></asp:Literal>


                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
        </asp:View>


        <asp:View ID="vSlideNews" runat="server">
            <div class="col-sm-12">


                <asp:FormView ID="searchSlideNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหา</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">



                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="การแสดงผล" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownListshow" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---เลือกการแสดงผล---" />
                                                        <asp:ListItem Value="1" Text="Slide" />
                                                        <asp:ListItem Value="2" Text="Main" />
                                                    </asp:DropDownList>
                                                </div>




                                                <asp:Label ID="LabelDropDownstatus" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="DropDownstatus_s" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="---เลือกสถานะ---" />
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>

                                            </div>



                                            <div class="form-group">



                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-2 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-4">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />



                                                </div>

                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-2 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-4">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" value="" />

                                                </div>

                                                <%--<asp:RequiredFieldValidator InitialValue="" ID="Req_ID252" runat="server" ControlToValidate="DropDownOrg_s" ErrorMessage="กรุณาเลือกองค์กร" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-10 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbsearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument=",SlideNewsmange">
                                                <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="lbsearch_cancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument=",SlideNewsmange" CausesValidation="false">
                                                    <i class="fas fa-redo" aria-hidden="true"></i>&nbsp;รีเซ็ต
                                                
                                                    </asp:LinkButton>


                                                </div>
                                                <%--  <label class="col-sm-3 control-label"></label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>


                <div class="form-group">
                    <asp:LinkButton ID="lbCreateSlideNews" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument=",SlideNews"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                </div>

                <asp:FormView ID="FvSlideNews" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่ม</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">


                                            <div class="form-group">
                                                <asp:Label ID="lbshow" runat="server" Text="การแสดงผล" CssClass="col-sm-3 control-label">
                                                    การแสดงผล<span class="text-danger"> *</span>
                                                </asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDownListshow" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="" Text="---กรุณาเลือก---" />
                                                        <asp:ListItem Value="1" Text="Slide" />
                                                        <asp:ListItem Value="2" Text="Main" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownListshow" runat="server" ControlToValidate="DropDownListshow" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownListshow" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownListshow" Width="220" />

                                                </div>
                                            </div>

                                            <%--           <asp:UpdatePanel ID="updateRoomInsert" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                            <div class="form-group">
                                                <asp:Label ID="labelFile_Upload" runat="server" class="col-sm-3 control-label">ไฟล์แนบ
                                                    <span class="text-danger"> *</span>
                                                </asp:Label>
                                                <div class="col-sm-6">
                                                    <%-- <asp:FileUpload ID="File_Upload" runat="server"/>--%>
                                                    <%-- <asp:UpdatePanel ID="File_UploadUpdatePanel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                                    <asp:FileUpload ID="File_Upload" Font-Size="small" ViewStateMode="Enabled" runat="server" ClientIDMode="Static" CssClass="control-label UploadFileImage multi max-1 with-preview" accept="jpg|png|jpeg" />
                                                    <%--<p ><font color="red">**ขนาดไฟล์แนะนำ 1000 x 400</font></p>--%>
                                                    <div id="wordslide" runat="server" visible="false">
                                                        <span class="text-danger">**ขนาดไฟล์แนะนำ 1000 x 400 (ยาว:กว้าง)</span>
                                                    </div>

                                                    <div id="wordMain" runat="server" visible="false">
                                                        <span class="text-danger">**ขนาดไฟล์แนะนำมากกว่า 1000 x 1000 (ยาว:กว้าง) ขึ้นไป</span>
                                                    </div>
                                                    <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldFile_Upload" runat="server" ControlToValidate="File_Upload" ErrorMessage="*กรุณาอัพโหลดไฟล์แนบ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutFile_Upload" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldFile_Upload" Width="220" />
                                                    <asp:CheckBox ID="checkBoxDelete" runat="server" CssClass="checked" Visible="false" Text=" &nbsp; Delete File"></asp:CheckBox>
                                                </div>
                                            </div>

                                            <%--                                          </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                            <%--<asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-3 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" value="" />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-3 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" value="" />

                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>
                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>



                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" OnClientClick="return confirm('คุณต้องการสร้างรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# "0" + "," + "SlideNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                    <%--OnClientClick="return confirm('คุณต้องการสร้างรายการนี้ใช่หรือไม่ ?')"--%>
                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",SlideNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไข</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">


                                            <div class="form-group">
                                                <asp:Label ID="lbshow" runat="server" Text="การแสดงผล" CssClass="col-sm-3 control-label">
                                                     การแสดงผล<span class="text-danger"> *</span>
                                                </asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="DropDownListshow" CssClass="form-control" runat="server" Text='<%# Eval("slide_show") %>' OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="" Text="---กรุณาเลือก---" />
                                                        <asp:ListItem Value="1" Text="Slide" />
                                                        <asp:ListItem Value="2" Text="Main" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldDropDownListshow" runat="server" ControlToValidate="DropDownListshow" ErrorMessage="*กรุณาเลือกรายการ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutDropDownListshow" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDropDownListshow" Width="220" />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="label2" runat="server" class="col-sm-3 control-label">ไฟล์แนบ
                                                   
                                                </asp:Label>
                                                <div class="col-sm-6">
                                                    <%-- <asp:FileUpload ID="File_Upload" runat="server"/>--%>
                                                    <asp:FileUpload ID="File_Upload" Font-Size="small" ViewStateMode="Enabled" runat="server" ClientIDMode="Static" CssClass="control-label UploadFileImage multi max-1 with-preview" accept="jpg|png|jpeg" required="" />
                                                    <div id="wordslide" runat="server" visible="false">
                                                        <span class="text-danger">**ขนาดไฟล์แนะนำ 1000 x 400 (ยาว:กว้าง)</span>
                                                    </div>

                                                    <div id="wordMain" runat="server" visible="false">
                                                        <span class="text-danger">**ขนาดไฟล์แนะนำมากกว่า 1000 x 1000 (ยาว:กว้าง) ขึ้นไป</span>
                                                    </div>
                                                    <%-- <asp:LinkButton ID="LinkButtonedititem" runat="server"><%# HaveFile((string)Eval("slide_file") )%></asp:LinkButton>--%>

                                                    <%-- <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand"><i class = "fa fa-trash"></i>&nbsp;Delete File</asp:LinkButton>--%>

                                                    <div id="preimg" runat="server"></div>

                                                    <%--<asp:CheckBox ID="checkBoxDelete" runat="server" CssClass="checked" Text=" &nbsp; Delete File"></asp:CheckBox>--%>
                                                    <%--OnClientClick="window.open('/mas.taokaenoi.co.th/uploadfiles/hr_news/65/65.png'); return false;"--%>
                                                    <asp:TextBox ID="tbFile_Upload" runat="server" Visible="false" CssClass="form-control" MaxLength="250" Text='<%# Eval("slide_file") %>'>
                                                    </asp:TextBox>



                                                    <%--<a target="_blank" href="img_forest.jpg">
                                                    <img src="img_forest.jpg">
                                                    </a>--%>
                                                </div>
                                                <div class="col-sm-3">
                                                    <%--<asp:RequiredFieldValidator InitialValue="" ID="RequiredFieldFile_Upload" runat="server" ControlToValidate="File_Upload" ErrorMessage="*กรุณาอัพโหลดไฟล์แนบ" ForeColor="Red" Display="None"></asp:RequiredFieldValidator>
                                                 <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutFile_Upload" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldFile_Upload" Width="220" />--%>
                                                </div>
                                            </div>

                                            <%--<asp:UpdatePanel ID="updateRoomInsert" runat="server">
                                        <ContentTemplate>--%>
                                            <%--<div class="form-group">
                                                <asp:label ID="labelstartdate" runat="server" class="col-sm-3 control-label">ตั้งแต่วันที่</asp:label>
                                                <div class="col-sm-6">
                                                    
                                                        <asp:HiddenField ID="txtSearchFromHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateStart_create" runat="server" placeholder="วันที่เริ่มต้นจอง..." Text='<%# Eval("list_timeout_start") %>'
                                                            CssClass="form-control datetimepicker-from cursor-pointer" />
                                                        
                                                        <asp:RequiredFieldValidator ID="Re_txtDateStart_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateStart_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่เริ่มต้นจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateStart_create" Width="200" PopupPosition="BottomLeft" />

                                                    
                                                </div>
                                                 <div class="col-sm-3">
                                                </div>
                                                </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelstarttime" runat="server" class="col-sm-3 control-label">เริ่ม</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeStartCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timestart_create" placeholder="วันที่เริ่มต้น..." runat="server" CssClass="form-control clockpicker cursor-pointer" Text='<%# Eval("slide_timeout_start") %>' />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>

                                            <%--<div class="form-group">
                                                <asp:label ID="labelenddate" runat="server" class="col-sm-3 control-label">ถึงวันที่</asp:label>
                                                <div class="col-sm-6">
                                                    
                                                        <asp:HiddenField ID="txtSearchToHidden" runat="server" />
                                                        <asp:TextBox ID="txtDateEnd_create" runat="server" placeholder="วันที่สิ้นสุดการจอง..."
                                                            CssClass="form-control datetimepicker-from cursor-pointer" />
                                                       
                                                        <asp:RequiredFieldValidator ID="Re_txtDateEnd_create"
                                                            runat="server"
                                                            ControlToValidate="txtDateEnd_create" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกวันที่สิ้นสุดการจอง"
                                                            ValidationGroup="SaveIndex" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtDateEnd_create" Width="200" PopupPosition="BottomLeft" />
                                                   
                                                </div>
                                                 <div class="col-sm-3">
                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="labelendtime" runat="server" class="col-sm-3 control-label">สิ้นสุด</asp:Label>
                                                <div class="col-sm-6">

                                                    <asp:HiddenField ID="HiddenTimeEndCreate" runat="server" />
                                                    <asp:TextBox ID="txt_timeend_create" placeholder="วันที่สิ้นสุด ..." runat="server" CssClass="form-control clockpicker-to cursor-pointer" Text='<%# Eval("slide_timeout_end") %>' />



                                                </div>
                                                <div class="col-sm-3">
                                                </div>
                                            </div>
                                            <%--</ContentTemplate>
                                            </asp:UpdatePanel>--%>





                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server" Text='<%# Eval("slide_status") %>'>
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:LinkButton ID="LinkButtonSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument='<%# Eval("slide_idx") + "," + "SlideNews"%>'><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButtonCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument=",SlideNews" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                                <label class="col-sm-3 control-label"></label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>



                <asp:GridView ID="gvSlideNews" runat="server" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-sm-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center">

                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvslide_idx" runat="server" Visible="false" Text='<%# Eval("slide_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="ไฟล์รูปภาพ" HeaderStyle-CssClass="text-center" ItemStyle-Width="200px">
                            <ItemTemplate>
                                <%--<asp:Label ID="gvslide_file" runat="server" Text='<%# Eval("slide_file") %>'>

                                </asp:Label>--%>
                                <asp:Image ID="image" runat="server" ImageUrl='<%# (string) FormatImageUrl((string) Eval("slide_file"),"slide") + "?r=" + Guidimagecache() %>' class="img-responsive" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่เริ่มต้น" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvslide_timeout_start" runat="server" Text='<%# Eval("slide_timeout_start") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สิ้นสุด" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="gvslide_timeout_end" runat="server" Text='<%# Eval("slide_timeout_end") %>'>

                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การแสดงผล" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvshow" runat="server" Text='<%# convertShow((int)Eval("slide_show")) %>'
                                        CssClass='<%# convertCss((int)Eval("slide_show")) %>'>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="gvstatus" runat="server" Text='<%# convertStatus((int)Eval("slide_status")) %>'
                                        CssClass='<%# convertCss((int)Eval("slide_status")) %>'>
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:LinkButton ID="lbView" CssClass="btn btn-warning" runat="server" title="ดู"><i class="fas fa-eye"></i></asp:LinkButton>

                                    <asp:LinkButton ID="lbEdit" CssClass="btn btn-info" runat="server" CommandName="cmdEdit" data-toggle="tooltip" OnCommand="btnCommand" title="แก้ไข" CommandArgument='<%# Eval("slide_idx") + "," + "SlideNews"%>'><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                    <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server" CommandName="cmdDelete" data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("slide_idx") + "," + "SlideNews"%>' title="ลบ"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

    </asp:MultiView>
    <script type="text/javascript" src="./Scripts/jquery.MultiFileNew.js"></script>
    <script type='text/javascript'>

        $(function () {
            $('.UploadFileImage').MultiFile({

            });

        });



        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileImage').MultiFile({

                });

            });

        });

    </script>

    <%--    <script type='text/javascript'>
        function preview_image(event) {
            var reader = new FileReader();
            reader.onload = function () {
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }

            $("#output_image").attr("style", "max-width: 300px;");
            $("#output_pdf").attr("class", "far fa-file-pdf fa-2x");
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>

    <script type='text/javascript'>

        function myFunction() {
            $("#output_image").attr("style", "max-width: 0px;");
            $("#output_pdf").attr("class", "");
        }

    </script>--%>

    <script type="text/javascript">
        //no post back form
        $(document).ready(TimePickerCtrl);
        function TimePickerCtrl($) {

            var startTime = $('.clockpicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',


            });

            var endTime = $('.clockpicker-to').datetimepicker({
                format: 'DD/MM/YYYY HH:mm',
                // minDate: startTime.data("DateTimePicker").date(),

            });


            $('.clockpicker').on('dp.change', function (e) {
                var dateTo = $('.clockpicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.clockpicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY HH:mm"));
                }

            });

            $('.clockpicker-to').on('dp.change', function (e) {
                var dateFrom = $('.clockpicker').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.clockpicker').data("DateTimePicker").date(e.date.format("DD/MM/YYYY HH:mm"));
                }
            });


            function setMinDate() {
                return endTime
                    .data("DateTimePicker").minDate(
                        startTime.data("DateTimePicker").date()
                    )
                    ;
            }
            var bound = false;
            function bindMinEndTimeToStartTime() {

                return bound || startTime.on('dp.change', setMinDate);
            }

            endTime.on('dp.change', () => {
                bindMinEndTimeToStartTime();
                bound = true;
                setMinDate();
            });

        }
        //is form
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $(document).ready(TimePickerCtrl);
            function TimePickerCtrl($) {

                var startTime = $('.clockpicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm',


                });



                var endTime = $('.clockpicker-to').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm',
                    //minDate: startTime.data("DateTimePicker").date(),

                });

                $('.clockpicker').click(function () {
                    $('.clockpicker').data("DateTimePicker").show();
                });
                $('.clockpicker').on('dp.change', function (e) {
                    var dateTo = $('.clockpicker-to').data("DateTimePicker").date();
                    if (e.date > dateTo) {
                        $('.clockpicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY HH:mm"));
                    }

                });
                $('.clockpicker-to').on('dp.change', function (e) {
                    var dateFrom = $('.clockpicker').data("DateTimePicker").date();
                    if (e.date < dateFrom) {
                        $('.clockpicker').data("DateTimePicker").date(e.date.format("DD/MM/YYYY HH:mm"));
                    }
                });

                //$('.show-from-onclick').click(function () {
                //    $('.datetimepicker-from').data("DateTimePicker").show();
                //});


                function setMinDate() {
                    return endTime
                        .data("DateTimePicker").minDate(
                            startTime.data("DateTimePicker").date()
                        )
                        ;
                }
                var bound = false;
                function bindMinEndTimeToStartTime() {

                    return bound || startTime.on('dp.change', setMinDate);
                }

                endTime.on('dp.change', () => {
                    bindMinEndTimeToStartTime();
                    bound = true;
                    setMinDate();
                });

            }

        });



    </script>




    <script type="text/javascript">

        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            entity_encoding: 'raw',
            encoding: "xml",

            //plugins: "paste textcolor",
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent ',
            plugins: 'print preview image link media template table hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            //toolbar: "forecolor backcolor",
            paste_as_text: true,
            setup: function (ed) {
                ed.on('keydown', function (event) {
                    if (event.keyCode == 9) { // tab pressed
                        if (event.shiftKey) {
                            ed.execCommand('Outdent');
                        }
                        else {
                            ed.execCommand('Indent');
                        }

                        event.preventDefault();
                        return false;
                    }
                });
            }

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                entity_encoding: 'raw',
                encoding: "xml",
                plugins: 'print preview image link media template table hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount contextmenu colorpicker textpattern help',
                toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
                //plugins: "paste",
                paste_as_text: true,
                setup: function (ed) {
                    ed.on('keydown', function (event) {
                        if (event.keyCode == 9) { // tab pressed
                            if (event.shiftKey) {
                                ed.execCommand('Outdent');
                            }
                            else {
                                ed.execCommand('Indent');
                            }

                            event.preventDefault();
                            return false;
                        }
                    });
                }

            });

        });


    </script>



    <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script type="text/javascript">

        //$("#viewdesc").on("click", function () {
        //    //$("#lbView").live("click", function () {
        //    alert("The paragraph was clicked.");
        //    $(this).closest("tr").after("<tr><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
        //    $(this).attr("id", "viewdescminus");
        //    $(this).attr("class", "text-trash");
        //}); 
        //$("#viewdescminus").on("click", function () {
        //        alert("The paragraph was clicked222222.");  
        //    $(this).attr("id", "viewdesc");
        //    $(this).attr("class", "text-info");

        //    $(this).closest("tr").next().remove();
        //});

        $(document).on("click", '#viewdesc', function () {
            //$("#lbView").live("click", function () {

            $(this).closest("tr").after("<tr><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("id", "viewdescminus");
            $(this).attr("class", "btn");
        });
        $(document).on("click", '#viewdescminus', function () {

            $(this).attr("id", "viewdesc");
            $(this).attr("class", "btn btn-warning");

            $(this).closest("tr").next().remove();
        });

        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(function () {
        //    //$("#lbView2").click(function () {
        //    $("#viewdesc").on("click", function () {
        //        //$("#lbView").live("click", function () {
        //        //alert("The paragraph was clicked.");
        //        $(this).closest("tr").after("<tr><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
        //        $(this).attr("id", "viewdescminus");
        //        $(this).attr("class", "text-trash");
        //    });
        //    $("#viewdescminus").on("click", function () {
        //        //    alert("The paragraph was clicked.");  
        //        $(this).attr("id", "viewdesc");
        //        $(this).attr("class", "text-info");
        //        $(this).closest("tr").next().remove();
        //    });

        //});

    </script>


    <script type="text/javascript">
        var _URL = window.URL;

        $(".testfile").change(function (e) {
            //function myFunctionn() {
            alert('eieei');
            var file, img;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);

                img.onload = function () {

                    if (this.height > 200 || this.width > 200) {

                        //show width and height to user

                        alert("Height and Width must not exceed 200px.");
                        document.getElementById('File_Upload').value = "";


                        $(function () {
                            $('.MultiFile-remove').trigger('click');
                        });
                        _URL.revokeObjectURL(objectUrl);
                    }
                };
                //console.log(objectUrl);
                img.src = objectUrl;
            }
        })
        //}


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            var _URL = window.URL;

            $(".testfile").change(function (e) {
                alert('eieei222222');
                var file, img;
                if ((file = this.files[0])) {
                    img = new Image();
                    var objectUrl = _URL.createObjectURL(file);
                    img.onload = function () {

                        if (this.height > 200 || this.width > 200) {

                            //show width and height to user

                            alert("Height and Width must not exceed 200px.");
                            //document.getElementById('File_Upload').value = "";


                            $(function () {
                                $('.MultiFile-remove').trigger('click');
                            });
                        }
                    };
                    //console.log(objectUrl);
                    img.src = objectUrl;
                }
            }).change();

        });

    </script>

    <script type="text/javascript">
        function CheckDimension() {
            //Get reference of File.
            var fileUpload = document.getElementById("File_Upload");

            //Check whether the file is valid Image.
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

                //Check whether HTML5 is supported.
                if (typeof (fileUpload.files) != "undefined") {
                    //Initiate the FileReader object.
                    var reader = new FileReader();
                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {
                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;
                            if (height > 200 || width > 200) {

                                //show width and height to user
                                document.getElementById("width").innerHTML = width;
                                document.getElementById("height").innerHTML = height;
                                alert("Height and Width must not exceed 200px.");
                                return false;
                            }
                            alert("Uploaded image has valid Height and Width.");
                            return true;
                        };

                    }
                } else {
                    alert("This browser does not support HTML5.");
                    return false;
                }
            } else {
                alert("Please select a valid Image file.");
                return false;
            }
        }

        function readURL(input) {
            //console.log(input.files);
            //console.log(input.files[0]);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = new Image;
                    img.onload = function () {
                        if (this.height > 400 || this.width < 800) {

                            alert("ความกว้างของภาพอย่างน้อย 800px และความสูงของภาพไม่เกิน 450px.");
                            //document.getElementById('File_Upload').value = "";

                            $(function () {
                                $('.MultiFile-remove').trigger('click');
                            });
                        }
                    };
                    img.src = reader.result;
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


</asp:Content>




