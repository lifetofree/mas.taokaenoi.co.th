﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_rbk_m0_meetingroom.aspx.cs" Inherits="websystem_el_rbk_m0_meetingroom" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:HiddenField ID="hddf_file_flag" runat="server" />
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="row">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary pull-right" runat="server"
                    data-toggle="tooltip" title="สร้างการจัดการ Meeting Room"
                    CommandName="btnToInsert" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> สร้างการจัดการ Meeting Room</asp:LinkButton>
            </div>

            <asp:Panel ID="pnlsearch" runat="server">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>รายละเอียด</label>
                                <asp:TextBox ID="txtFilterKeyword" runat="server"
                                    CssClass="form-control"
                                    placeholder="..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnFilter" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">
                <%--OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"--%>
                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="m0_meeting_idx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                    <asp:Label ID="lbfile_flag" runat="server"
                                        Visible="false"
                                        Text='<%# Eval("file_flag") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>

                                <asp:TextBox ID="m0_meeting_idx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("m0_meeting_idx")%>' />

                                <div class="col-md-8 col-md-offset-2">

                                    <div class="col-md-12">
                                        <div class="pull-left">
                                        </div>
                                    </div>
                                </div>

                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                            HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="zplace_name" runat="server" Text='<%# Eval("zplace_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ตำแหน่งที่ต้องการแสดง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="zpositon_status_name" runat="server" Text='<%# Eval("zpositon_status_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="meeting_remark" runat="server" Text='<%# Eval("meeting_remark") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รูปภาพ/วิดีโอ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                            Visible="false"
                            >
                            <ItemTemplate>
                                <small>
                                    <%--<asp:TextBox ID="txtmeeting_file_name" runat="server"
                                        Visible="false"
                                        Text='<%# Eval("meeting_file_name") %>' />
                                    <asp:Panel ID="pnl_img" runat="server" Visible="false">
                                        <img
                                            src='<%# ResolveUrl(getImgUrl((string)Eval("meeting_file_name"))) %>'
                                            style="width: 200px; height: 150px" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_video" runat="server" Visible="false">
                                        
                                        <video style="" width="200" height="150" controls>
                                            <source src='<%# getImgUrl((string)Eval("meeting_file_name")) %>' type="video/mp3">
                                            <source src='<%# getImgUrl((string)Eval("meeting_file_name")) %>' type="video/mp4">
                                            <source src='<%# getImgUrl((string)Eval("meeting_file_name")) %>' type="video/ogg">
                                            <source src='<%# getImgUrl((string)Eval("meeting_file_name")) %>' type="video/wmv">
                                        </video>
                                        <asp:Label ID="txtfile_duration" runat="server"
                                            Text='<%# "ระยะเวลา : "+Eval("file_duration")+" นาที" %>' />
                                    </asp:Panel>--%>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่เริ่ม-วันที่สิ้นสุด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="meeting_date" runat="server" Text='<%# Eval("meeting_date_from")+" - "+Eval("meeting_date_to") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="meeting_status" runat="server" Text='<%# getStatus((int)Eval("meeting_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="btnEdit"
                                    OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_meeting_idx") %>'
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_meeting_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <asp:View ID="ViewEdit" runat="server">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <%--<i class="glyphicon glyphicon-blackboard"></i>--%>
                        <strong>&nbsp; การจัดการ Meeting Room
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="row">
                            <asp:FormView ID="fv_Edit" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <EditItemTemplate>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานที่</label>
                                                <asp:UpdatePanel ID="panelplace_idx_refUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:DropDownList ID="ddlplace_idx_refUpdate" runat="server"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                        <%--
                                                            SelectedValue='<%# Eval("place_idx_ref") == null ? "0" : Eval("place_idx_ref") %>'
                                                            <asp:ListItem Value="0">-- เลือก --</asp:ListItem>
                                                            <asp:ListItem Value="3">นพวงศ์</asp:ListItem>
                                                            <asp:ListItem Value="4">โรจนะ</asp:ListItem>
                                                            <asp:ListItem Value="5">เมืองทองธานี</asp:ListItem>--%>
                                                        <asp:RequiredFieldValidator ID="Requiredddlplace_idx_refUpdate"
                                                            ValidationGroup="saveCmdUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddlplace_idx_refUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกสถานที่" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ตำแหน่งที่ต้องการแสดง</label>
                                                <asp:UpdatePanel ID="panelpositon_statusUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <asp:DropDownList ID="ddlpositon_statusUpdate" runat="server"
                                                            CssClass="form-control"
                                                            SelectedValue='<%# Eval("positon_status") == null ? "0" : Eval("positon_status") %>'>
                                                            <asp:ListItem Value="0">-- เลือก --</asp:ListItem>
                                                            <asp:ListItem Value="1">Center</asp:ListItem>
                                                            <asp:ListItem Value="2">Footer</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="requiredpositon_statusUpdate"
                                                            ValidationGroup="saveCmdUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddlpositon_statusUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            ErrorMessage="กรุณากรอกตำแหน่งที่ต้องการแสดง" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">รายละเอียด</label>
                                                <asp:UpdatePanel ID="panelmeeting_remarkUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtmeeting_remarkUpdate" runat="server"
                                                            CssClass="form-control"
                                                            TextMode="MultiLine" Rows="4"
                                                            Text='<%# Eval("meeting_remark")%>'
                                                            placeholder="หมายเหตุ..." />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                    </Triggers>
                                                </asp:UpdatePanel>

                                            </small>
                                        </div>
                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <small>
                                                    <label class="pull-left">รูปภาพ/วิดีโอ</label>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <br />
                                                            <asp:CheckBox ID="cb_delimage"
                                                                runat="server" Text="ลบรูป/วิดีโอ"
                                                                CssClass="pull-left" />
                                                            <br />
                                                            <asp:FileUpload ID="fldmeeting_file_nameUpdate"
                                                                ViewStateMode="Enabled" AutoPostBack="true"
                                                                Font-Size="small" ClientIDMode="Static" runat="server"
                                                                CssClass="form-control multi max-1 accept-png|jpg|gif|jpeg|mp4|mp3 with-preview"></asp:FileUpload>

                                                            <asp:TextBox ID="txtmeeting_file_nameUpdate" runat="server"
                                                                Visible="false" CssClass="form-control"
                                                                Text='<%# Eval("meeting_file_name") %>' />
                                                            <asp:TextBox ID="txtfile_flagUpdate" runat="server"
                                                                Visible="false" CssClass="form-control"
                                                                Text='<%# Eval("file_flag") %>' />
                                                            <br />
                                                            <asp:Image ID="imgStoreListUpdate" runat="server" Visible="false"
                                                                ImageUrl='<%# getImgUrl((string)Eval("meeting_file_name")) %>'
                                                                CommandName="btnToAddSO" OnCommand="btnCommand"
                                                                CssClass="pull-left"
                                                                CommandArgument='<%# Eval("m0_meeting_idx") %>'
                                                                Height="100" Width="100" />
                                                            <asp:TextBox ID="txtm0_meeting_idxUpdate" runat="server" CssClass="form-control"
                                                                Visible="false" Text='<%# Eval("m0_meeting_idx")%>' />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </small>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row" runat="server" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ระยะเวลา / นาที</label>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>

                                                        <asp:TextBox ID="txtfile_durationUpdate" runat="server"
                                                            CssClass="form-control"
                                                            Text='<%# Eval("file_duration")%>'
                                                            TextMode="Number">
                                                        </asp:TextBox>
                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtfile_duration"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                InitialValue="0"
                                                ErrorMessage="กรุณากรอกระยะเวลา / นาที" />--%>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันที่เริ่ม</label>
                                                <asp:UpdatePanel ID="pnlmeeting_date_fromUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtmeeting_date_fromUpdate" runat="server"
                                                                CssClass="form-control filter-order-from"
                                                                Text='<%# Eval("meeting_date_from")%>' />
                                                            <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันที่สิ้นสุด</label>
                                                <asp:UpdatePanel ID="UpdatePanelmeeting_date_toUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtmeeting_date_toUpdate" runat="server"
                                                                CssClass="form-control filter-order-from"
                                                                Text='<%# Eval("meeting_date_to")%>' />
                                                            <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>

                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <asp:UpdatePanel ID="Panel_meeting_statusUpdate" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlmeeting_statusUpdate" AutoPostBack="false" runat="server"
                                                            CssClass="form-control" SelectedValue='<%# Eval("meeting_status") %>'>
                                                            <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbCmdUpdate" />

                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="saveCmdUpdate" CommandName="btnupdate"
                                                OnCommand="btnCommand"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"
                                                data-toggle="tooltip" title="บันทึก"
                                                Text="<i class='fa fa-save fa-lg'></i> บันทึก">
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                                Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                                data-toggle="tooltip" title="ยกเลิก"
                                                OnCommand="btnCommand"
                                                CommandName="btnCancel"></asp:LinkButton>
                                        </div>
                                    </div>

                                </EditItemTemplate>
                            </asp:FormView>

                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <%--<i class="glyphicon glyphicon-blackboard"></i>--%>
                        <strong>&nbsp; การจัดการ Meeting Room
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="row">

                            <%--  --%>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>สถานที่</label>
                                    <asp:UpdatePanel ID="UpdatePanel_place_idx_ref" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlplace_idx_ref" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Selected="True">-- เลือก --</asp:ListItem>
                                                <asp:ListItem Value="3">นพวงศ์</asp:ListItem>
                                                <asp:ListItem Value="4">โรจนะ</asp:ListItem>
                                                <asp:ListItem Value="5">เมืองทองธานี</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="requiredplace_idx_ref"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddlplace_idx_ref"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                InitialValue="0"
                                                ErrorMessage="กรุณากรอกสถานที่" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>ตำแหน่งที่ต้องการแสดง</label>
                                    <asp:UpdatePanel ID="UpdatePanel_positon_status" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlpositon_status" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Selected="True">-- เลือก --</asp:ListItem>
                                                <asp:ListItem Value="1">Center</asp:ListItem>
                                                <asp:ListItem Value="2">Footer</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiredpositon_status"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="ddlpositon_status"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                InitialValue="0"
                                                ErrorMessage="กรุณากรอกตำแหน่งที่ต้องการแสดง" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <%--  --%>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>รายละเอียด</label>
                                    <asp:UpdatePanel ID="pnlmeeting_remark" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtmeeting_remark" runat="server" CssClass="form-control"
                                                TextMode="MultiLine" Rows="4"
                                                placeholder="รายละเอียด..." />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="row"  runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>รูปภาพ/วิดีโอ</label>
                                        <asp:UpdatePanel ID="UpdatePanel_meeting_file_name" runat="server">
                                            <ContentTemplate>
                                                <asp:FileUpload ID="fldmeeting_file_name"
                                                    ViewStateMode="Enabled" AutoPostBack="true"
                                                    Font-Size="small" ClientIDMode="Static" runat="server"
                                                    CssClass="form-control multi max-1 accept-png|jpg|gif|jpeg|mp4|mp3 with-preview"></asp:FileUpload>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>

                            </div>

                            <div class="row"  runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>ระยะเวลา / นาที</label>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtfile_duration" runat="server"
                                                    CssClass="form-control"
                                                    TextMode="Number">
                                                </asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                ValidationGroup="save" runat="server"
                                                Display="Dynamic"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtfile_duration"
                                                Font-Size="1em" ForeColor="Red"
                                                CssClass="pull-left"
                                                InitialValue="0"
                                                ErrorMessage="กรุณากรอกระยะเวลา / นาที" />--%>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>วันที่เริ่ม</label>
                                        <asp:UpdatePanel ID="pnlmeeting_date_from_ins" runat="server">
                                            <ContentTemplate>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtmeeting_date_from" runat="server" CssClass="form-control filter-order-from" />
                                                    <span class="input-group-addon show-order-sale-log-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>วันที่สิ้นสุด</label>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtmeeting_date_to" runat="server" CssClass="form-control filter-order-from" />
                                                    <span class="input-group-addon show-order-sale-log-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>สถานะ</label>
                                        <asp:UpdatePanel ID="UpdatePanel_meeting_status" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlmeeting_status" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveInsert" />

                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">

                                    <asp:LinkButton
                                        ID="btnSaveInsert"
                                        CssClass="btn btn-success" runat="server"
                                        CommandName="btnInsert" OnCommand="btnCommand"
                                        Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                        data-toggle="tooltip" title="บันทึก"
                                        ValidationGroup="save" />
                                    <asp:LinkButton CssClass="btn btn-danger"
                                        ID="btnCancel"
                                        data-toggle="tooltip" title="ยกเลิก" runat="server"
                                        Text="<i class='fa fa-close fa-lg'></i> ยกเลิก"
                                        CommandName="btnCancel" OnCommand="btnCommand" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->


    </asp:MultiView>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

</asp:Content>
