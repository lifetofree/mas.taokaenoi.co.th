﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_form_calculate.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_form_calculate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
    <asp:TextBox ID="name" runat="server" CssClass="form-control" placeholder="ชื่อ ..." Visible="false" Enabled="true" />
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <asp:View ID="docManageFormCalM0" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddFormCal" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มชื่อฟอร์ม" data-toggle="tooltip" title="เพิ่มชื่อฟอร์ม" runat="server"
                    CommandName="cmdAddFormName" OnClientClick="dotim()" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มชื่อฟอร์ม</asp:LinkButton>
            </div>

            <asp:FormView ID="fvFormName" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">เพิ่มชื่อฟอร์ม</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name" runat="server" Text="ชื่อฟอร์มกรอกผล" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_form_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มกรอกผล ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Retxtformname" runat="server"
                                            ControlToValidate="txt_form_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อฟอร์มกรอกผล" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtformname" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtformname" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbnodtDetails" runat="server" Text="ระบุหมายเหตุ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtNodtDetails" runat="server" TextMode="MultiLine" CssClass="form-control tinymce" Rows="7" placeholder="กรุณากรอกหมายเหตุ"></asp:TextBox>
                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                            Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                        <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                            ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name_status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_status_form_name" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveFormName" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvFormList" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound"
                AutoPostBack="false">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center;"><b>ไม่มีข้อมูลชื่อฟอร์ม</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="formcal_idx" runat="server" Visible="false" Text='<%# Eval("m0_formcal_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m0_formcal_idx" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_formcal_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_m0_formcal_idx" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์มกรอกผล" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_formcal_name" runat="server" CssClass="form-control " Text='<%# Eval("m0_formcal_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_update_formcal_name" runat="server"
                                                ControlToValidate="txt_update_formcal_name" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์มกรอกผล" ValidationGroup="update_formcal_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valitxt_update_formcal_name" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_update_formcal_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                       <div class="form-group">
                                        <asp:Label ID="lbnotedtails_update" CssClass="col-sm-3 control-label" runat="server" Text="หมายเหตุ" />
                                        <div class="col-sm-6">
                                            <asp:Label ID="notedetails" CssClass="col-sm-3 control-label" Visible="false" Text='<%# Eval("note_detail") %>' runat="server" />
                                            <asp:TextBox ID="txt_update_note_detail" runat="server" AutoPostBack="true" TextMode="MultiLine" CssClass="form-control tinymce" Rows="7" placeholder="กรุณากรอกหมายเหตุ" Text='<%# Eval("note_detail") %>'></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_form_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlUpdateStatus" Text='<%# Eval("m0_form_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" OnClientClick="dotim()" ValidationGroup="Editinvname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" OnClientClick="dotim()" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="35%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="formcal_name" runat="server" Visible="true" Text='<%# Eval("m0_formcal_name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="40%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbformcal_name_note_detail" runat="server" Visible="false" Text='<%# Eval("note_detail") %>' />
                            <asp:Label ID="lbformcal_name_note_detail_show" runat="server" Visible="true" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbformcal_status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("m0_form_status") %>'></asp:Label>
                                <asp:Label ID="lbformcal_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                </asp:Label>
                                <asp:Label ID="lbformcal_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("m0_formcal_idx") + "," + Eval("m0_formcal_name") %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelete" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                CommandArgument='<%#Eval("m0_formcal_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </asp:View>

        <asp:View ID="docManageFormCalM1" runat="server">
            <div class="form-group">
                
                <asp:LinkButton ID="btnBack" CssClass="btn btn-danger" Visible="true" data-original-title="ย้อนกลับ" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                    CommandName="cmdBack" OnCommand="btnCommand">&nbsp;ย้อนกลับ</asp:LinkButton>
            </div>

            <div class="form-group">
                <asp:LinkButton ID="btnAddTableResult" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มตารางกรอกผลสอบเทียบ" data-toggle="tooltip" title="เพิ่มตารางกรอกผลสอบเทียบ" runat="server"
                    CommandName="cmdAddTableResult" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มตารางกรอกผลสอบเทียบ</asp:LinkButton>
            </div>

            <div class="alert alert-info" id="divShowDetails" runat="server" visible="true">
                <asp:Label ID="showNameForm" runat="server"></asp:Label>
            </div>

            <asp:FormView ID="fvFormTableResult" runat="server" DefaultMode="insert" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="showNameForm" Text="เพิ่มตารางสำหรับกรอกผลสอบเทียบ" runat="server"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbnameFrom" runat="server" Text="ชื่อฟอร์ม" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtFormName" runat="server" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_table_name" runat="server" Text="ชื่อตารางกรอกผลสอบเทียบ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_form_table_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อตารางกรอกผลสอบเทียบ ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Retxt_form_table_name" runat="server"
                                            ControlToValidate="txt_form_table_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อตารางกรอกผลสอบเทียบ" ValidationGroup="addtable" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_form_table_name" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxt_form_table_name" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="รายละเอียด" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_table_decription" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" placeholder="กรอกรายละเอียดตารางกรอกผลสอบเทียบ ..." Enabled="true" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name_status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_status_form_table_name" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveTableResult" ValidationGroup="Savetable" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSaveTableResult" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancelTable" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelTable" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvTableResult" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound"
                AutoPostBack="false">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center;"><b>ไม่มีข้อมูลชื่อตารางกรอกผลสอบเทียบ</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="formcal_table_idx" runat="server" Visible="false" Text='<%# Eval("m1_formcal_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m1_formcal_table_idx" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m1_formcal_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbformname" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์ม" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_formcal_for_update_name" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_m0_formcal_table_idx" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อตารางกรอกผลสอบเทียบ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_formcal_table_name" runat="server" CssClass="form-control " Text='<%# Eval("table_result_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_update_formcal_table_name" runat="server"
                                                ControlToValidate="txt_update_formcal_table_name" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อตารางกรอกผลสอบเทียบ" ValidationGroup="update_formcal_table_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_update_formcal_table_name" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_update_formcal_table_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_table_decriptionedit" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียด" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_table_decriptionedit" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" Text='<%# Eval("table_decription") %>'></asp:TextBox>
                                           <%-- <asp:TextBox ID="txt_table_decription" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" placeholder="กรอกรายละเอียดตารางกรอกผลสอบเทียบ ..." Enabled="true" />--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldtxt_table_decriptionedit" runat="server"
                                                ControlToValidate="txt_table_decriptionedit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียด" ValidationGroup="update_formcal_table_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxt_table_decriptionedit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_form_table_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlUpdateTableStatus" Text='<%# Eval("status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editinvname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อตารางกรอกผลสอบเทียบ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="formcal_name" runat="server" Visible="true" Text='<%# Eval("table_result_name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="40%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="formcal_table_result_details" runat="server" Visible="true" Text='<%# Eval("table_decription") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbformcal_table_status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("status") %>'></asp:Label>
                                <asp:Label ID="lbformcal_table_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                </asp:Label>
                                <asp:Label ID="lbformcal_table_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btn_edit_table" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndeletetable" CssClass="text-trash" runat="server" CommandName="cmdDeleteTable"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                CommandArgument='<%#Eval("m1_formcal_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </asp:View>
    </asp:MultiView>

      <script type="text/javascript">
        function dotim() {
            tinyMCE.triggerSave();
        }

    </script>
    <script type="text/javascript">


        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            // inline: true,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: "",
            menubar: false,
            resize: false,
            statusbar: false,
            entity_encoding: 'raw',
            entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
            //entities: '160,nbsp,60,lt,62,gt',
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo ",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });

            }

        });

        //var plainText = tinymce.activeEditor.getContent().replace(/<[^>] *>/ g, "");
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                //inline: true,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: "",
                menubar: false,
                resize: false,
                statusbar: false,
                entity_encoding: 'raw',
                entities: "160,nbsp,38,amp,34,quot,162,cent,8364,euro,163,pound,165,yen,169,copy,174,reg,8482,trade,8240,permil,60,lt,62,gt,8804,le,8805,ge,176,deg,8722,minus",
                //entities: '160,nbsp,60,lt,62,gt',
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }


            });

            $(".multi").MultiFile();


        })

    </script>

</asp:Content>

