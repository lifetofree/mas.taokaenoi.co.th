﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Drawing;

using System.IO;
using System.Configuration;
using System.Data.SqlClient;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Net.Mail;
using System.Threading;

public partial class websystem_Sir_TEST_ManageJobs : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute _service_execute = new service_execute();
    data_job _data_job = new data_job();
    data_employee _data_employee = new data_employee();
    data_presonal _data_presonal = new data_presonal();

    //DBConn DBConn = new DBConn();
    //private FunctionWeb _functionWeb = new FunctionWeb();
    private string JOBActionAc = "JOB";
    private string MASActionAc = "InMas";
    private string _jobs = "jobs";
    private string _Mas = "conn_centralized";
    private string BoxXML;
    string localXml = String.Empty;
    string _local_xml = "";

    //string _iBookingConn = "iBookingConn";
    //int _actionType = 0;
    //string _local_xml = "";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        ShowEmp();
        ShowEmpMain();
        if (!this.IsPostBack)
        {
            Show_Location();
            Select_PositionWent();
            Select_PositionWentF2();
            Show_Departmenthome();
            Show_Sectionhome();
            Show_Positionhome();
            lbMenu0.BackColor = System.Drawing.Color.LightGray;
            lbMenu1.BackColor = System.Drawing.Color.Transparent;
            txtfocus_vLONList.Focus();
        }
    }

    #region Dataset
    protected void Select_tabel_parent()
    {
        var dsHistoryEmploy = new DataSet();
        dsHistoryEmploy.Tables.Add("HistoryEmploy");

        dsHistoryEmploy.Tables[0].Columns.Add("id_empmain2", typeof(String));
        dsHistoryEmploy.Tables[0].Columns.Add("name_company", typeof(String));
        dsHistoryEmploy.Tables[0].Columns.Add("position_old", typeof(String));
        dsHistoryEmploy.Tables[0].Columns.Add("motive", typeof(String));
        dsHistoryEmploy.Tables[0].Columns.Add("salary_old", typeof(String));
        dsHistoryEmploy.Tables[0].Columns.Add("job_brief_details", typeof(String));

        if (ViewState["vsBuyHistoryEmploy"] == null)
        {
            ViewState["vsBuyHistoryEmploy"] = dsHistoryEmploy;

        }
    }
    #endregion

    #region ค้นหาข้อมูล
    protected void ShowEmp_SearchMain()
    {
        GridView gvEmpMainS = (GridView)FormView2.FindControl("gvEmpMainS");
        DropDownList ddworkMain = (DropDownList)FormView2.FindControl("ddworkMain");
        TextBox tbageMain = (TextBox)FormView2.FindControl("tbageMain");
        TextBox date_SMain = (TextBox)FormView2.FindControl("date_SMain");
        DropDownList ddlSearchDateMain = (DropDownList)FormView2.FindControl("ddlSearchDateMain");
        TextBox dateStatMain = (TextBox)FormView2.FindControl("dateStatMain");

        Out_ReportEmp_list SelectOutEmp_SearchM = new Out_ReportEmp_list();
        _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
        if (ddworkMain.SelectedValue != "00")
        {
            SelectOutEmp_SearchM.Position_SearchM = int.Parse(ddworkMain.SelectedValue);
        }
        if (tbageMain.Text != "")
        {
            SelectOutEmp_SearchM.Age_SearchM = int.Parse(tbageMain.Text);
        }
        if (date_SMain.Text != "")
        {
            SelectOutEmp_SearchM.Date_SearchM = date_SMain.Text;
        }
        if (ddlSearchDateMain.SelectedValue != "00")
        {
            SelectOutEmp_SearchM.IFSearchbetweenM = int.Parse(ddlSearchDateMain.SelectedValue);
        }
        if (dateStatMain.Text != "")
        {
            SelectOutEmp_SearchM.DatecloseJobM = dateStatMain.Text;
        }

        if (ddworkMain.SelectedValue == "00")
        {
            SelectOutEmp_SearchM.Position_SearchM = 999;
        }
        if (tbageMain.Text == "")
        {
            SelectOutEmp_SearchM.Age_SearchM = 999;
        }
        if (date_SMain.Text == "")
        {
            SelectOutEmp_SearchM.Date_SearchM = "999";
        }
        if (ddlSearchDateMain.SelectedValue == "00")
        {
            SelectOutEmp_SearchM.IFSearchbetweenM = 999;
        }
        if (dateStatMain.Text == "")
        {
            SelectOutEmp_SearchM.DatecloseJobM = "999";
        }

        _data_job.Out_ReportEmp[0] = SelectOutEmp_SearchM;

        data_job EmpSMain = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 64));

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        gvEmpMainS.DataSource = EmpSMain.Out_ReportEmp;
        gvEmpMainS.DataBind();
    }

    //#region initial function/data
    //function_tool _funcTool = new function_tool();

    //static string _hrsBenefits = ConfigurationManager.AppSettings["hrsBenefits"];
    //#endregion initial function/data

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!IsPostBack)
    //    {
    //        string _temp = _funcTool.getEncryptRC4("stringtest", _hrsBenefits);
    //        Response.Write(_temp);
    //        Response.Write("<br />");
    //        Response.Write(_funcTool.getDecryptRC4(_temp, _hrsBenefits));
    //    }
    //}

    protected void ShowEmp_Search()
    {
        GridView gvEmpPrimaryF2 = (GridView)FormView1.FindControl("gvEmpPrimaryF2");
        DropDownList ddwork = (DropDownList)FormView1.FindControl("ddwork");
        TextBox tbage = (TextBox)FormView1.FindControl("tbage");
        TextBox date_S = (TextBox)FormView1.FindControl("date_S");
        DropDownList ddlSearchDate = (DropDownList)FormView1.FindControl("ddlSearchDate");
        TextBox dateStat = (TextBox)FormView1.FindControl("dateStat");

        Out_Empprimary_list SelectOutEmp_Search = new Out_Empprimary_list();
        _data_job.Out_Empprimary = new Out_Empprimary_list[1];
        if (ddwork.SelectedValue != "00")
        {
            SelectOutEmp_Search.Position_Search = int.Parse(ddwork.SelectedValue);
        }
        if (tbage.Text != "")
        {
            SelectOutEmp_Search.Age_Search = int.Parse(tbage.Text);
        }
        if (date_S.Text != "")
        {
            SelectOutEmp_Search.Date_Search = date_S.Text;
        }
        if (ddlSearchDate.SelectedValue != "00")
        {
            SelectOutEmp_Search.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        }
        if (dateStat.Text != "")
        {
            SelectOutEmp_Search.DatecloseJob = dateStat.Text;
        }

        if (ddwork.SelectedValue == "00")
        {
            SelectOutEmp_Search.Position_Search = 999;
        }
        if (tbage.Text == "")
        {
            SelectOutEmp_Search.Age_Search = 999;
        }
        if (date_S.Text == "")
        {
            SelectOutEmp_Search.Date_Search = "999";
        }
        if (ddlSearchDate.SelectedValue == "00")
        {
            SelectOutEmp_Search.IFSearchbetween = 999;
        }
        if (dateStat.Text == "")
        {
            SelectOutEmp_Search.DatecloseJob = "999";
        }

        _data_job.Out_Empprimary[0] = SelectOutEmp_Search;

        data_job EmpPrimary = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 79));

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        gvEmpPrimaryF2.DataSource = EmpPrimary.Out_Empprimary;
        gvEmpPrimaryF2.DataBind();
    }
    #endregion

    #region bind data
    protected void ShowEmp()
    {
        GridView gvEmpPrimary = (GridView)FormView1.FindControl("gvEmpPrimary");

        Out_Empprimary_list SelectOutEmp = new Out_Empprimary_list();
        _data_job.Out_Empprimary = new Out_Empprimary_list[1];
        _data_job.Out_Empprimary[0] = SelectOutEmp;

        data_job EmpPrimary = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 47));

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(EmpPrimary));
        gvEmpPrimary.DataSource = EmpPrimary.Out_Empprimary;
        gvEmpPrimary.DataBind();
    }

    protected void ShowEmpMain()
    {
        GridView gvEmpMain = (GridView)FormView2.FindControl("gvEmpMain");

        Out_ReportEmp_list SelectOutEmpMain = new Out_ReportEmp_list();
        _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
        _data_job.Out_ReportEmp[0] = SelectOutEmpMain;

        data_job EmpMain = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 54));

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(EmpMain));
        gvEmpMain.DataSource = EmpMain.Out_ReportEmp;
        gvEmpMain.DataBind();
    }

    protected void Showfile()
    {
        GridView gvDocumentFile = (GridView)FormView3.FindControl("gvDocumentFile");

        Insert_FileDoc_list SelectIfileDoc = new Insert_FileDoc_list();
        _data_job.Insert_FileDoc = new Insert_FileDoc_list[1];
        SelectIfileDoc.EmpIDXFile = int.Parse(ViewState["idEmpMain"].ToString());
        _data_job.Insert_FileDoc[0] = SelectIfileDoc;

        data_job EmpIdFileDoc = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 42));

        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(EmpIdFileDoc));
        gvDocumentFile.DataSource = EmpIdFileDoc.Insert_FileDoc;
        gvDocumentFile.DataBind();
    }
    #endregion

    #region เรียก Dropdown
    protected void Select_PositionWentF2()
    {
        DropDownList tbwork = (DropDownList)FormView2.FindControl("ddworkMain");
        tbwork.AppendDataBoundItems = true;
        tbwork.Items.Add(new ListItem("กรุณาเลือกตำแหน่งงาน", "00"));

        BoxSelectwantpositionList1 selectpositionF2 = new BoxSelectwantpositionList1();
        _data_job.BoxSelectwantposition1 = new BoxSelectwantpositionList1[1];
        _data_job.BoxSelectwantposition1[0] = selectpositionF2;

        data_job ddselectpositionF2 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 11));

        tbwork.DataSource = ddselectpositionF2.BoxSelectwantposition1;
        tbwork.DataTextField = "PosGroupNameTH";
        tbwork.DataValueField = "PosGroupIDX";
        tbwork.DataBind();
    }

    protected void Select_PositionWent()
    {
        DropDownList ddwork = (DropDownList)FormView1.FindControl("ddwork");
        ddwork.AppendDataBoundItems = true;
        ddwork.Items.Add(new ListItem("ตำแหน่งงาน", "00"));

        BoxSelectwantpositionList1 selectposition = new BoxSelectwantpositionList1();
        _data_job.BoxSelectwantposition1 = new BoxSelectwantpositionList1[1];
        _data_job.BoxSelectwantposition1[0] = selectposition;

        data_job ddselectposition = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 11));

        ddwork.DataSource = ddselectposition.BoxSelectwantposition1;
        ddwork.DataTextField = "PosGroupNameTH";
        ddwork.DataValueField = "PosGroupIDX";
        ddwork.DataBind();
    }

    protected void Show_Positionhome()
    {
        DropDownList ddPosition = (DropDownList)FormView3.FindControl("ddPosition");
        ddPosition.AppendDataBoundItems = true;
        ddPosition.Items.Add(new ListItem("โปรดระบุแผนก", "00"));
    }
    protected void Show_Position()
    {
        DropDownList ddPosition = (DropDownList)FormView3.FindControl("ddPosition");
        ddPosition.Items.Clear();
        ddPosition.AppendDataBoundItems = true;
        ddPosition.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง", "00"));

        Select_Position_list SelectOutPo = new Select_Position_list();
        _data_job.Select_Position = new Select_Position_list[1];
        SelectOutPo.RPosIDX_ = int.Parse(ViewState["PositionIDX"].ToString());
        SelectOutPo.RDepart2_ = int.Parse(ViewState["DepartIDX"].ToString());
        _data_job.Select_Position[0] = SelectOutPo;

        data_job OutPo = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 75));

        ddPosition.DataSource = OutPo.Select_Position;
        ddPosition.DataTextField = "PosNameTH";
        ddPosition.DataValueField = "RPosIDX";
        ddPosition.DataBind();
    }

    protected void Show_Sectionhome()
    {
        DropDownList dddepartment = (DropDownList)FormView3.FindControl("dddepartment");
        dddepartment.AppendDataBoundItems = true;
        dddepartment.Items.Add(new ListItem("โปรดระบุฝ่าย", "00"));
    }
    protected void Show_Section()
    {
        DropDownList dddepartment = (DropDownList)FormView3.FindControl("dddepartment");
        dddepartment.Items.Clear();
        dddepartment.AppendDataBoundItems = true;
        dddepartment.Items.Add(new ListItem("กรุณาเลือกแผนก", "00"));

        Select_Section_list SelectOutsection = new Select_Section_list();
        _data_job.Select_Section = new Select_Section_list[1];
        SelectOutsection.SecIDX_ = int.Parse(ViewState["DepartIDX"].ToString());
        _data_job.Select_Section[0] = SelectOutsection;

        data_job Outsection = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 74));

        dddepartment.DataSource = Outsection.Select_Section;
        dddepartment.DataTextField = "SecNameTH";
        dddepartment.DataValueField = "RSecIDX";
        dddepartment.DataBind();
    }

    protected void Show_Departmenthome()
    {
        DropDownList ddparty = (DropDownList)FormView3.FindControl("ddparty");
        ddparty.AppendDataBoundItems = true;
        ddparty.Items.Add(new ListItem("โปรดระบุบริษัท", "00"));
    }
    protected void Show_Department()
    {
        DropDownList ddparty = (DropDownList)FormView3.FindControl("ddparty");
        ddparty.Items.Clear();
        ddparty.AppendDataBoundItems = true;
        ddparty.Items.Add(new ListItem("กรุณาเลือกฝ่าย", "00"));

        Select_Department_list SelectOutDepart = new Select_Department_list();
        _data_job.Select_Department = new Select_Department_list[1];
        SelectOutDepart.DeptIDX_ = int.Parse(ViewState["CompanyIDX"].ToString());
        _data_job.Select_Department[0] = SelectOutDepart;

        data_job OutDepart = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 73));

        ddparty.DataSource = OutDepart.Select_Department;
        ddparty.DataTextField = "DeptNameTH";
        ddparty.DataValueField = "RDeptIDX";
        ddparty.DataBind();
    }

    protected void Show_Location()
    {
        DropDownList ddcompany = (DropDownList)FormView3.FindControl("ddcompany");
        ddcompany.AppendDataBoundItems = true;
        ddcompany.Items.Add(new ListItem("กรุณาเลือกบริษัท", "00"));

        Select_Location_list SelectOutLocation = new Select_Location_list();
        _data_job.Select_Location = new Select_Location_list[1];
        _data_job.Select_Location[0] = SelectOutLocation;

        data_job Location = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 72));

        ddcompany.DataSource = Location.Select_Location;
        ddcompany.DataTextField = "OrgNameTH";
        ddcompany.DataValueField = "OrgIDX";
        ddcompany.DataBind();
    }
    #endregion

    #region Gvbuyequipment_RowDataBound

    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        Panel hiImYes = (Panel)e.Item.FindControl("hiImYes");
        Panel hiImNo = (Panel)e.Item.FindControl("hiImNo");
        string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
        //string pat = getPath + x.ToString() + "/" + x.ToString() + "0.jpg";
        if (!Directory.Exists(Server.MapPath(getPath + ViewState["idEmpMain"].ToString())))
        {
            hiImYes.Visible = false;
            hiImNo.Visible = true;
            //test.Text = "test1";
        }
        else
        {
            hiImYes.Visible = true;
            hiImNo.Visible = false;
            //test.Text = "test2";
        }
    }



    protected void Gvbuyequipment_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvEmpMain":
                GridView gvEmpMain = (GridView)FormView2.FindControl("gvEmpMain");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvEmpMain.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbstate_ok = (Label)e.Row.FindControl("lbstate_ok");
                        var lbstate_no = (Label)e.Row.FindControl("lbstate_no");
                        var lblhoderstate = ((Label)e.Row.Cells[0].FindControl("lblhoderstate"));

                        if (int.Parse(lblhoderstate.Text) == 1)  // Active
                        {

                            lbstate_no.Visible = false;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00 ");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = true;


                        }
                        else
                        {
                            lbstate_ok.Visible = false;
                            lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A9A9A9 ");
                            lbstate_no.Style["font-weight"] = "bold";
                            lbstate_no.Visible = true;
                        }
                    }
                }
                break;

            case "gvEmpMainS":
                GridView gvEmpMainS = (GridView)FormView2.FindControl("gvEmpMainS");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvEmpMainS.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbstate_ok = (Label)e.Row.FindControl("lbstate_ok");
                        var lbstate_no = (Label)e.Row.FindControl("lbstate_no");
                        var lblhoderstate = ((Label)e.Row.Cells[0].FindControl("lblhoderstate"));

                        if (int.Parse(lblhoderstate.Text) == 1)  // Active
                        {

                            lbstate_no.Visible = false;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00 ");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = true;


                        }
                        else
                        {
                            lbstate_ok.Visible = false;
                            lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A9A9A9 ");
                            lbstate_no.Style["font-weight"] = "bold";
                            lbstate_no.Visible = true;
                        }
                    }
                }
                break;

            case "gvDocumentFile":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

            case "gvEmpPrimary":
                GridView gvEmpPrimary = (GridView)FormView1.FindControl("gvEmpPrimary");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvEmpPrimary.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        //var lblhoderstate = (Label)e.Row.FindControl("lblhoderstate");
                        var lbstate_ok = (Label)e.Row.FindControl("lbstate_ok");
                        var lbstate_no = (Label)e.Row.FindControl("lbstate_no");
                        var lblhoderstate = ((Label)e.Row.Cells[0].FindControl("lblhoderstate"));

                        if (int.Parse(lblhoderstate.Text) == 1)  // Active
                        {
                            lbstate_no.Visible = false;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00 ");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = true;


                        }
                        else
                        {
                            lbstate_ok.Visible = false;
                            lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A9A9A9 ");
                            lbstate_no.Style["font-weight"] = "bold";
                            lbstate_no.Visible = true;
                        }
                    }
                }
                break;

            case "gvEmpPrimaryF2":
                GridView gvEmpPrimaryF2 = (GridView)FormView1.FindControl("gvEmpPrimaryF2");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvEmpPrimaryF2.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        //var lblhoderstate = (Label)e.Row.FindControl("lblhoderstate");
                        var lbstate_ok = (Label)e.Row.FindControl("lbstate_ok");
                        var lbstate_no = (Label)e.Row.FindControl("lbstate_no");
                        var lblhoderstate = ((Label)e.Row.Cells[0].FindControl("lblhoderstate"));

                        if (int.Parse(lblhoderstate.Text) == 1)  // Active
                        {
                            lbstate_no.Visible = false;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00 ");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = true;


                        }
                        else
                        {
                            lbstate_ok.Visible = false;
                            lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A9A9A9 ");
                            lbstate_no.Style["font-weight"] = "bold";
                            lbstate_no.Visible = true;
                        }
                    }
                }
                break;
        }
    }
    #endregion

    #region Master_PageIndexChanging
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvEmpPrimary = (GridView)FormView1.FindControl("gvEmpPrimary");
        GridView gvEmpPrimaryF2 = (GridView)FormView1.FindControl("gvEmpPrimaryF2");
        GridView gvEmpMain = (GridView)FormView2.FindControl("gvEmpMain");
        GridView gvEmpMainS = (GridView)FormView2.FindControl("gvEmpMainS");
        GridView gvDocumentFile = (GridView)FormView2.FindControl("gvDocumentFile");
        var GvNameTEST = (GridView)sender;
        switch (GvNameTEST.ID)
        {
            case "gvEmpPrimary":
                gvEmpPrimary.PageIndex = e.NewPageIndex;
                gvEmpPrimary.DataBind();
                ShowEmp();
                break;

            case "gvEmpPrimaryF2":
                gvEmpPrimaryF2.PageIndex = e.NewPageIndex;
                gvEmpPrimaryF2.DataBind();
                ShowEmp_Search();
                break;

            case "gvEmpMain":
                gvEmpMain.PageIndex = e.NewPageIndex;
                gvEmpMain.DataBind();
                ShowEmpMain();
                break;

            case "gvEmpMainS":
                gvEmpMainS.PageIndex = e.NewPageIndex;
                gvEmpMainS.DataBind();
                ShowEmp_SearchMain();
                break;

            case "gvDocumentFile":
                gvDocumentFile.PageIndex = e.NewPageIndex;
                gvDocumentFile.DataBind();
                ShowEmp();
                break;
        }
    }
    #endregion

    #region บาย repeter
    protected void Repeater1_PreRender(object sender, RepeaterItemEventArgs e)
    {
        Label Label67 = (Label)e.Item.FindControl("Label67");
        Label Label68 = (Label)e.Item.FindControl("Label68");
        Panel Yes_Edu = (Panel)e.Item.FindControl("Yes_Edu");
        Panel No_Edu = (Panel)e.Item.FindControl("No_Edu");
        var Repet = (Repeater)sender;
        switch (Repet.ID)
        {
            case "RepeaterEmpMain1":
                if(ViewState["ExpiredDateSecurrity"].ToString() == "01/01/1900")
                {
                    Label67.Visible = false;
                    Label68.Visible = false;
                }
                else
                {
                    Label67.Visible = true;
                    Label68.Visible = true;
                }

                Panel hiImYes = (Panel)e.Item.FindControl("hiImYes");
                Panel hiImNo = (Panel)e.Item.FindControl("hiImNo");
                string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
                //string pat = getPath + x.ToString() + "/" + x.ToString() + "0.jpg";
                if (!Directory.Exists(Server.MapPath(getPath + ViewState["idEmpMain"].ToString())))
                {
                    hiImYes.Visible = false;
                    hiImNo.Visible = true;
                    //test.Text = "test1";
                }
                else
                {
                    hiImYes.Visible = true;
                    hiImNo.Visible = false;
                    //test.Text = "test2";
                }

                Panel HidneScore = (Panel)e.Item.FindControl("HidneScore");
                Panel HidneScoreNo = (Panel)e.Item.FindControl("HidneScoreNo");
                Panel Panel5 = (Panel)e.Item.FindControl("Panel50");
                Panel Panel6 = (Panel)e.Item.FindControl("Panel60");
                Panel Panel7 = (Panel)e.Item.FindControl("Panel70");
                if (ViewState["Scorelogic"].ToString() == "999" || int.Parse(ViewState["Scorelogic"].ToString()) <= 9)
                {
                    HidneScoreNo.Visible = true;
                }
                if (int.Parse(ViewState["Scorelogic"].ToString()) >= 10 && int.Parse(ViewState["Scorelogic"].ToString()) <= 21)
                {
                    Panel5.Visible = true;
                }
                if (int.Parse(ViewState["Scorelogic"].ToString()) >= 22 && int.Parse(ViewState["Scorelogic"].ToString()) <= 30)
                {
                    Panel6.Visible = true;
                }
                if (int.Parse(ViewState["Scorelogic"].ToString()) >= 31 && int.Parse(ViewState["Scorelogic"].ToString()) <= 35)
                {
                    Panel7.Visible = true;
                }
                break;

            case "RepeaterEmp":
                if (ViewState["NameEdu"].ToString() != "ไม่มีข้อมูล")
                {
                    Yes_Edu.Visible = true;
                }
                else
                {
                    Yes_Edu.Visible = false;
                    No_Edu.Visible = true;
                }
                break;
        }
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        Repeater RepeaterEmp = (Repeater)FormView1.FindControl("RepeaterEmp");
        DropDownList ddworkMain = (DropDownList)FormView2.FindControl("ddworkMain");
        TextBox tbageMain = (TextBox)FormView2.FindControl("tbageMain");
        TextBox date_SMain = (TextBox)FormView2.FindControl("date_SMain");
        Panel Div1F2 = (Panel)FormView2.FindControl("Div1F2");
        Panel Div2F2 = (Panel)FormView2.FindControl("Div2F2");
        DropDownList ddlSearchDateMain = (DropDownList)FormView2.FindControl("ddlSearchDateMain");
        TextBox dateStatMain = (TextBox)FormView2.FindControl("dateStatMain");
        Panel Hinddate2 = (Panel)FormView2.FindControl("Hinddate2");

        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "BackMain2":
                MvtestMaster.SetActiveView(View1);
                ShowEmpMain();
                break;

            case "backMain":
                lbMenu0.BackColor = System.Drawing.Color.Transparent;
                lbMenu1.BackColor = System.Drawing.Color.LightGray;
                MvtestMaster.SetActiveView(View1);
                Div1F2.Visible = true;
                Div2F2.Visible = false;
                Hinddate2.Visible = false;

                ddworkMain.SelectedValue = "00";
                tbageMain.Text = String.Empty;
                date_SMain.Text = String.Empty;
                dateStatMain.Text = String.Empty;
                dateStatMain.Enabled = false;
                ddlSearchDateMain.SelectedValue = "00";
                break;

            case "btnSearchMain":
                if (ddworkMain.SelectedValue != "00" || tbageMain.Text != "" || date_SMain.Text != "")
                {
                    Div1F2.Visible = false;
                    Div2F2.Visible = true;
                    ShowEmp_SearchMain();
                    //test.Text = "test1";
                }
                else
                {
                    Div1F2.Visible = true;
                    Div2F2.Visible = false;
                    //test.Text = "test2";
                }
                break;

            case "back":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnSearch":
                DropDownList ddwork = (DropDownList)FormView1.FindControl("ddwork");
                TextBox tbage = (TextBox)FormView1.FindControl("tbage");
                TextBox date_S = (TextBox)FormView1.FindControl("date_S");
                Panel Div1F1 = (Panel)FormView1.FindControl("Div1F1");
                Panel Div2F1 = (Panel)FormView1.FindControl("Div2F1");

                if (ddwork.SelectedValue != "00" || tbage.Text != "" || date_S.Text != "")
                {
                    Div1F1.Visible = false;
                    Div2F1.Visible = true;
                    ShowEmp_Search();
                    //test.Text = "test1";
                }
                else
                {
                    Div1F1.Visible = true;
                    Div2F1.Visible = false;
                    //test.Text = "test2";
                }
                break;

            case "Emp":
                int idEmp = int.Parse(cmdArg);
                ViewState["idEmpprimary"] = idEmp;
                Out_Empprimary_list SelectOutEmppass = new Out_Empprimary_list();
                _data_job.Out_Empprimary = new Out_Empprimary_list[1];
                SelectOutEmppass.EmpPrimary_IDX_send = idEmp;
                _data_job.Out_Empprimary[0] = SelectOutEmppass;

                data_job EmpPrimaryselect = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 48));
                ViewState["prefix"] = EmpPrimaryselect.Out_Empprimary[0].Prefix;
                ViewState["Name"] = EmpPrimaryselect.Out_Empprimary[0].Name_Emp;
                ViewState["Last"] = EmpPrimaryselect.Out_Empprimary[0].Lastname_Emp;
                ViewState["Age"] = EmpPrimaryselect.Out_Empprimary[0].Age_Emp;
                ViewState["Num"] = EmpPrimaryselect.Out_Empprimary[0].Phone_number;
                ViewState["Email"] = EmpPrimaryselect.Out_Empprimary[0].Email;
                ViewState["NameEdu"] = EmpPrimaryselect.Out_Empprimary[0].NameINstitute;
                ViewState["Educa"] = EmpPrimaryselect.Out_Empprimary[0].Education;
                ViewState["corse"] = EmpPrimaryselect.Out_Empprimary[0].Course;
                ViewState["po1"] = EmpPrimaryselect.Out_Empprimary[0].position1;
                ViewState["po2"] = EmpPrimaryselect.Out_Empprimary[0].position2;
                ViewState["mon"] = EmpPrimaryselect.Out_Empprimary[0].WantMoney;


                RepeaterEmp.DataSource = EmpPrimaryselect.Out_Empprimary;
                RepeaterEmp.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;

            case "pass":
                Thread.Sleep(5000);
                Out_Empprimary_list changstatusEmp = new Out_Empprimary_list();
                _data_job.Out_Empprimary = new Out_Empprimary_list[1];
                changstatusEmp.EmpPrimary_IDX_send = int.Parse(ViewState["idEmpprimary"].ToString());
                _data_job.Out_Empprimary[0] = changstatusEmp;

                data_job EmpPrimarychang = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 49));

                //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
                RepeaterEmp.DataSource = EmpPrimarychang.Out_Empprimary;
                RepeaterEmp.DataBind();

                ///////////////////////////////////////////////////////////////
                Insert_Genlink_list InGenLink = new Insert_Genlink_list();
                _data_job.Insert_Genlink = new Insert_Genlink_list[1];
                InGenLink.EmpPimary_idx = int.Parse(ViewState["idEmpprimary"].ToString());
                _data_job.Insert_Genlink[0] = InGenLink;

                data_job Insert_GenlinkOut = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 83));
                ViewState["JenCode"] = Insert_GenlinkOut.return_JenCode;
                ViewState["JenCodeAge"] = Insert_GenlinkOut.return_AgeJenCode;
                ///////////////////////////////////////////////////////////////

                string _temp1 = _funcTool.getEncryptRC4(ViewState["po1"].ToString(), "url");
                string _temp2 = _funcTool.getEncryptRC4(ViewState["po2"].ToString(), "url");
                string _temp3 = _funcTool.getEncryptRC4(ViewState["mon"].ToString(), "url");
                string _temp4 = _funcTool.getEncryptRC4(ViewState["prefix"].ToString(), "url");
                string _temp5 = _funcTool.getEncryptRC4(ViewState["Name"].ToString(), "url");
                string _temp6 = _funcTool.getEncryptRC4(ViewState["Last"].ToString(), "url");
                string _temp7 = _funcTool.getEncryptRC4(ViewState["Email"].ToString(), "url");
                string _temp8 = _funcTool.getEncryptRC4(ViewState["Num"].ToString(), "url");
                string _temp9 = _funcTool.getEncryptRC4(ViewState["JenCode"].ToString(), "url");
                string _temp10 = _funcTool.getEncryptRC4(ViewState["JenCodeAge"].ToString(), "url");

                ViewState["url"] = "http://demo.taokaenoi.co.th/Job-Application?position1=" + _temp1 + "&position2=" + _temp2 + "&Monney=" + _temp3 + "&Prefix=" + _temp4 + "&name=" + _temp5 + "&last=" + _temp6 + "&Email=" + _temp7 + "&number=" + _temp8 + "&Jen=" + _temp9 + "&JenAge=" + _temp10;
                
                SentEmailComment();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnViewSelected":
                lbMenu0.BackColor = System.Drawing.Color.LightGray;
                lbMenu1.BackColor = System.Drawing.Color.Transparent;
                MvtestMaster.SetActiveView(ViewAdd1);
                ShowEmp();
                break;

            case "btnViewSelected1":
                lbMenu0.BackColor = System.Drawing.Color.Transparent;
                lbMenu1.BackColor = System.Drawing.Color.LightGray;
                MvtestMaster.SetActiveView(View1);
                ShowEmpMain();
                break;

            case "EmpMain":
                Select_tabel_parent();
                int idEmpMain = int.Parse(cmdArg);
                ViewState["idEmpMain"] = idEmpMain;

                Out_ReportEmp_list SelectOutEmppassMain = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutEmppassMain.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutEmppassMain;
                Repeater RepeaterEmpMain1 = (Repeater)FormView3.FindControl("RepeaterEmpMain1");
                Repeater RepeaterEmpMain2 = (Repeater)FormView3.FindControl("RepeaterEmpMain2");
                Repeater RepeaterEmpMain3 = (Repeater)FormView3.FindControl("RepeaterEmpMain3");
                Repeater RepeaterEmpMain4 = (Repeater)FormView3.FindControl("RepeaterEmpMain4");
                Repeater RepeaterEmpMain5 = (Repeater)FormView3.FindControl("RepeaterEmpMain5");
                Repeater RepeaterEmpMain6 = (Repeater)FormView3.FindControl("RepeaterEmpMain6");
                Repeater RepeaterEmpMain7 = (Repeater)FormView3.FindControl("RepeaterEmpMain7");
                Repeater RepeaterEmpMain8 = (Repeater)FormView3.FindControl("RepeaterEmpMain8");

                data_job EmpMainselect = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 55));
                ViewState["Scorelogic"] = EmpMainselect.Out_ReportEmp[0].Score_logic;
                ViewState["SexIDX"] = EmpMainselect.Out_ReportEmp[0].SexIDX;
                ViewState["PrefixIDX"] = EmpMainselect.Out_ReportEmp[0].PrefixIDX;
                ViewState["FirstNameTH"] = EmpMainselect.Out_ReportEmp[0].FirstNameTH;
                ViewState["LastNameTH"] = EmpMainselect.Out_ReportEmp[0].LastNameTH;
                ViewState["NickNameTH"] = EmpMainselect.Out_ReportEmp[0].NickNameTH;
                ViewState["NickNameEN"] = EmpMainselect.Out_ReportEmp[0].NickNameEN;
                ViewState["FirstNameEN"] = EmpMainselect.Out_ReportEmp[0].FirstNameEN;
                ViewState["LastNameEN"] = EmpMainselect.Out_ReportEmp[0].LastNameEN;
                ViewState["PhoneNo"] = EmpMainselect.Out_ReportEmp[0].PhoneNo;
                ViewState["MobileNo"] = EmpMainselect.Out_ReportEmp[0].MobileNo;
                ViewState["Email"] = EmpMainselect.Out_ReportEmp[0].Email;
                ViewState["Birthday"] = EmpMainselect.Out_ReportEmp[0].Birthday;
                ViewState["NatIDX"] = EmpMainselect.Out_ReportEmp[0].NatIDX;
                ViewState["RaceIDX"] = EmpMainselect.Out_ReportEmp[0].RaceIDX;
                ViewState["RelIDX"] = EmpMainselect.Out_ReportEmp[0].RelIDX;
                ViewState["EmpAddress"] = EmpMainselect.Out_ReportEmp[0].PresentAddress;
                ViewState["DistIDX"] = EmpMainselect.Out_ReportEmp[0].DistIDX;
                ViewState["AmpIDX"] = EmpMainselect.Out_ReportEmp[0].AmpIDX;
                ViewState["ProvIDX"] = EmpMainselect.Out_ReportEmp[0].ProvIDX;
                ViewState["PostCode"] = EmpMainselect.Out_ReportEmp[0].PostCode;
                ViewState["CountryIDX"] = EmpMainselect.Out_ReportEmp[0].CountryIDX;
                ViewState["MarriedStatus"] = EmpMainselect.Out_ReportEmp[0].MarriedStatus;
                ViewState["InhabitStatus"] = EmpMainselect.Out_ReportEmp[0].InhabitStatus;

                ViewState["IdentityCard"] = EmpMainselect.Out_ReportEmp[0].CardID;
                ViewState["IssuedAt"] = EmpMainselect.Out_ReportEmp[0].IssuedAtCardID;
                ViewState["IDateExpired"] = EmpMainselect.Out_ReportEmp[0].ExpiredDateCardID;
                ViewState["EmpAddressR"] = EmpMainselect.Out_ReportEmp[0].EmpAddress;
                ViewState["MilIDX"] = EmpMainselect.Out_ReportEmp[0].MilitaryService;
                ViewState["ExpiredDateSecurrity"] = EmpMainselect.Out_ReportEmp[0].ExpiredDateSecurrity;

                ViewState["Height"] = EmpMainselect.Out_ReportEmp[0].Height;
                ViewState["Weight"] = EmpMainselect.Out_ReportEmp[0].Weight;
                ViewState["Scar"] = EmpMainselect.Out_ReportEmp[0].Scar;
                ViewState["BTypeIDX"] = EmpMainselect.Out_ReportEmp[0].BloodIDX;

                RepeaterEmpMain1.DataSource = EmpMainselect.Out_ReportEmp;
                RepeaterEmpMain1.DataBind();

                Out_ReportEmp_list SelectOutEmpparent = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutEmpparent.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutEmpparent;

                //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
                data_job EmpMainselectParent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 56));
                RepeaterEmpMain2.DataSource = EmpMainselectParent.Out_ReportEmp;
                RepeaterEmpMain2.DataBind();


                Out_ReportEmp_list SelectOutEmpEdu = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutEmpEdu.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutEmpEdu;

                data_job EmpMainselectEdu = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 57));
                //ViewState["CheckEdu"] = EmpMainselectEdu.Out_ReportEmp[0].Edu_name;
                RepeaterEmpMain3.DataSource = EmpMainselectEdu.Out_ReportEmp;
                RepeaterEmpMain3.DataBind();

                if (RepeaterEmpMain3.Items.Count <= 0)
                {
                    Panel1.Visible = true;
                    Panel2.Visible = false;
                }
                if (RepeaterEmpMain3.Items.Count > 0)
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                }


                Out_ReportEmp_list SelectOutEmpEduTop1 = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutEmpEduTop1.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutEmpEduTop1;

                data_job EmpMainselectEduTop1 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 63));
                test.Text = EmpMainselectEduTop1.return_Check;
                if (EmpMainselectEduTop1.return_Check != "0")
                {
                    ViewState["Major"] = EmpMainselectEduTop1.Out_ReportEmp[0].Course;
                    ViewState["Faculty"] = EmpMainselectEduTop1.Out_ReportEmp[0].Faculty;
                    ViewState["University"] = EmpMainselectEduTop1.Out_ReportEmp[0].Institute;
                    ViewState["EduName"] = EmpMainselectEduTop1.Out_ReportEmp[0].Education;
                    ViewState["GPA"] = EmpMainselectEduTop1.Out_ReportEmp[0].Grade;
                    ViewState["academic_year"] = EmpMainselectEduTop1.Out_ReportEmp[0].ToAttended;
                }
                else
                {
                    ViewState["Major"] = "-";
                    ViewState["Faculty"] = 0;
                    ViewState["University"] = "-";
                    ViewState["EduName"] = 0;
                    ViewState["GPA"] = "-";
                    ViewState["academic_year"] = "-";
                }

                Out_ReportEmp_list SelectOutHisEmp = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutHisEmp.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutHisEmp;

                data_job EmpMainselectHisEmp = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 58));

                RepeaterEmpMain4.DataSource = EmpMainselectHisEmp.Out_ReportEmp;
                RepeaterEmpMain4.DataBind();

                if (RepeaterEmpMain4.Items.Count <= 0)
                {
                    Panel3.Visible = true;
                    Panel4.Visible = false;
                }
                if (RepeaterEmpMain4.Items.Count > 0)
                {
                    Panel3.Visible = false;
                    Panel4.Visible = true;
                }


                Select_Row_list SelectRow = new Select_Row_list();
                _data_job.Select_Row = new Select_Row_list[1];
                SelectRow.EmpIDXSendMain2 = idEmpMain;
                _data_job.Select_Row[0] = SelectRow;

                data_job EmpMainselectRow = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 78));
                ViewState["Row"] = EmpMainselectRow.Select_Row[0].His_Row;

                if (ViewState["Row"].ToString() != "0")
                {
                    for (int o = 0; o < int.Parse(ViewState["Row"].ToString()); o++)
                    {
                        var dsHistoryEmploy = (DataSet)ViewState["vsBuyHistoryEmploy"];
                        var drHistoryEmploy = dsHistoryEmploy.Tables[0].NewRow();

                        drHistoryEmploy["id_empmain2"] = EmpMainselectHisEmp.return_Check;
                        drHistoryEmploy["name_company"] = EmpMainselectHisEmp.Out_ReportEmp[o].NameEmployed;
                        drHistoryEmploy["position_old"] = EmpMainselectHisEmp.Out_ReportEmp[o].PositionBusiness;
                        drHistoryEmploy["motive"] = EmpMainselectHisEmp.Out_ReportEmp[o].Resignation;
                        drHistoryEmploy["salary_old"] = EmpMainselectHisEmp.Out_ReportEmp[o].WorkEmp;
                        drHistoryEmploy["job_brief_details"] = EmpMainselectHisEmp.Out_ReportEmp[o].SalaryEmp;

                        dsHistoryEmploy.Tables[0].Rows.Add(drHistoryEmploy);
                        ViewState["vsBuyHistoryEmploy"] = dsHistoryEmploy;
                    }
                }
                if (ViewState["Row"].ToString() == "0")
                {
                    var dsHistoryEmploy = (DataSet)ViewState["vsBuyHistoryEmploy"];
                    var drHistoryEmploy = dsHistoryEmploy.Tables[0].NewRow();

                    drHistoryEmploy["id_empmain2"] = EmpMainselectHisEmp.return_Check;
                    drHistoryEmploy["name_company"] = "-";
                    drHistoryEmploy["position_old"] = "-";
                    drHistoryEmploy["motive"] = "-";
                    drHistoryEmploy["salary_old"] = "-";
                    drHistoryEmploy["job_brief_details"] = "-";

                    dsHistoryEmploy.Tables[0].Rows.Add(drHistoryEmploy);
                    ViewState["vsBuyHistoryEmploy"] = dsHistoryEmploy;
                }

                Out_ReportEmp_list SelectOutTalent = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutTalent.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutTalent;

                data_job EmpMainselectTalent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 62));
                //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(EmpMainselectTalent));
                RepeaterEmpMain5.DataSource = EmpMainselectTalent.Out_ReportEmp;
                RepeaterEmpMain5.DataBind();

                Out_ReportEmp_list SelectOutLangu = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutLangu.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutLangu;

                data_job EmpMainselectLangu = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 59));
                RepeaterEmpMain6.DataSource = EmpMainselectLangu.Out_ReportEmp;
                RepeaterEmpMain6.DataBind();

                Out_ReportEmp_list SelectOutDrive = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutDrive.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutDrive;

                data_job EmpMainselectDrive = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 61));
                RepeaterEmpMain7.DataSource = EmpMainselectDrive.Out_ReportEmp;
                RepeaterEmpMain7.DataBind();

                Out_ReportEmp_list SelectOutNoparent = new Out_ReportEmp_list();
                _data_job.Out_ReportEmp = new Out_ReportEmp_list[1];
                SelectOutNoparent.EmpIDXSendMain = idEmpMain;
                _data_job.Out_ReportEmp[0] = SelectOutNoparent;

                data_job EmpMainselectNoparent = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 62));
                ViewState["mobile_number"] = EmpMainselectNoparent.Out_ReportEmp[0].TelephoneEmergency;
                ViewState["fullname_reference"] = EmpMainselectNoparent.Out_ReportEmp[0].EmergencyFL;
                ViewState["relationship"] = EmpMainselectNoparent.Out_ReportEmp[0].RelationEmergency;
                ViewState["address_reference"] = EmpMainselectNoparent.Out_ReportEmp[0].AddressEmergency;

                RepeaterEmpMain8.DataSource = EmpMainselectNoparent.Out_ReportEmp;
                RepeaterEmpMain8.DataBind();

                Showfile();
                MvtestMaster.SetActiveView(View2);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal2();", true);
                break;

            case "passMain":
                insert_MAS();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('โอนข้อมูลสำเร็จ');", true);
                MvtestMaster.SetActiveView(View1);
                ShowEmpMain();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

        }
    }
    #endregion

    #region insert MAS
    protected void insert_MAS()
    {
        #region U0_Emp/U0_Employee_Sarary/U1_Employee_SararyLog/U1_Employee/U0_EditLog
        DropDownList ddcompany = (DropDownList)FormView3.FindControl("ddcompany");
        DropDownList ddparty = (DropDownList)FormView3.FindControl("ddparty");
        DropDownList dddepartment = (DropDownList)FormView3.FindControl("dddepartment");
        DropDownList ddPosition = (DropDownList)FormView3.FindControl("ddPosition"); //ใช้แค่ตัวนี้
        TextBox date_Start = (TextBox)FormView3.FindControl("date_Start");
        //#region แปลง Smalldate
        //string outputtextdatestart = date_Start.Text.Substring(6) + "/" + date_Start.Text.Substring(3, 2) + "/" + date_Start.Text.Substring(0, 2);
        //#endregion
        ViewState["DateStart"] = date_Start.Text;

        employee_detail AddMainEmp = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];
        AddMainEmp.emp_type_idx = 2;
        AddMainEmp.rpos_idx = int.Parse(ddPosition.SelectedValue);
        AddMainEmp.emp_status = 2;
        AddMainEmp.emp_start_date = ViewState["DateStart"].ToString();
        AddMainEmp.sex_idx = int.Parse(ViewState["SexIDX"].ToString());
        AddMainEmp.prefix_idx = int.Parse(ViewState["PrefixIDX"].ToString());
        AddMainEmp.emp_firstname_th = ViewState["FirstNameTH"].ToString();
        AddMainEmp.emp_lastname_th = ViewState["LastNameTH"].ToString();
        AddMainEmp.emp_nickname_th = ViewState["NickNameTH"].ToString();
        AddMainEmp.emp_nickname_en = ViewState["NickNameEN"].ToString();
        AddMainEmp.emp_firstname_en = ViewState["FirstNameEN"].ToString();
        AddMainEmp.emp_lastname_en = ViewState["LastNameEN"].ToString();
        AddMainEmp.emp_phone_no = ViewState["PhoneNo"].ToString();
        AddMainEmp.emp_mobile_no = ViewState["MobileNo"].ToString();
        AddMainEmp.emp_email = ViewState["Email"].ToString();
        AddMainEmp.emp_birthday = ViewState["Birthday"].ToString();
        AddMainEmp.nat_idx = int.Parse(ViewState["NatIDX"].ToString());
        AddMainEmp.race_idx = int.Parse(ViewState["RaceIDX"].ToString());
        AddMainEmp.rel_idx = int.Parse(ViewState["RelIDX"].ToString());
        AddMainEmp.emp_address = ViewState["EmpAddress"].ToString();
        AddMainEmp.dist_idx = int.Parse(ViewState["DistIDX"].ToString());
        AddMainEmp.amp_idx = int.Parse(ViewState["AmpIDX"].ToString());
        AddMainEmp.prov_idx = int.Parse(ViewState["ProvIDX"].ToString());
        AddMainEmp.post_code = ViewState["PostCode"].ToString();
        AddMainEmp.country_idx = int.Parse(ViewState["CountryIDX"].ToString());
        AddMainEmp.sarary = "0";

        #region U0_Employee_Reference_Persons
        Reference_persons_list AddU0_Employee_Reference_Persons = new Reference_persons_list();
        _data_employee.Reference_persons = new Reference_persons_list[1];
        AddU0_Employee_Reference_Persons.mobile_number = ViewState["mobile_number"].ToString();
        AddU0_Employee_Reference_Persons.fullname_reference = ViewState["fullname_reference"].ToString();
        AddU0_Employee_Reference_Persons.relationship = ViewState["relationship"].ToString();
        AddU0_Employee_Reference_Persons.address_reference = ViewState["address_reference"].ToString();
        #endregion

        #region U0_EmpTH 
        EmployeeTHlist AddEmpTH = new EmployeeTHlist();
        _data_employee.BoxEmployeeTHlist = new EmployeeTHlist[1];
        AddEmpTH.BProvIDX = 0;
        AddEmpTH.SocNo = "-";
        AddEmpTH.SocHosIDX = 0;
        AddEmpTH.SocDateExpired = "01/01/1999";
        AddEmpTH.IdentityCard = ViewState["IdentityCard"].ToString();
        AddEmpTH.IssuedAt = ViewState["IssuedAt"].ToString();
        AddEmpTH.IDateExpired = ViewState["IDateExpired"].ToString();
        AddEmpTH.EmpAddress = ViewState["EmpAddressR"].ToString();
        AddEmpTH.DistIDX = int.Parse(ViewState["DistIDX"].ToString());
        AddEmpTH.AmpIDX = int.Parse(ViewState["AmpIDX"].ToString());
        AddEmpTH.ProvIDX = int.Parse(ViewState["ProvIDX"].ToString());
        AddEmpTH.PostCode = ViewState["PostCode"].ToString();
        AddEmpTH.CountryIDX = int.Parse(ViewState["CountryIDX"].ToString());
        AddEmpTH.PhoneNo = ViewState["MobileNo"].ToString();
        #endregion

        #region EmpFR
        EmployeeFRlist AddEmpFR = new EmployeeFRlist();
        _data_employee.BoxEmployeeFRlist = new EmployeeFRlist[1];
        AddEmpFR.EmpAddress = ViewState["EmpAddressR"].ToString();
        AddEmpFR.CountryIDX = int.Parse(ViewState["CountryIDX"].ToString());
        AddEmpFR.PhoneNo = ViewState["MobileNo"].ToString();
        AddEmpFR.VisaIDX = "-";
        AddEmpFR.TrDateIssued = "01/01/1999";
        AddEmpFR.TrExpired = "01/01/1999";
        AddEmpFR.WPermitNo = "-";
        AddEmpFR.WDateIssued = "01/01/1999";
        AddEmpFR.WDateExpired = "01/01/1999";
        AddEmpFR.Passport_Issued = "-";
        AddEmpFR.WPermit_Issued = "-";
        #endregion

        #region U0EmpHealth
        EmployeeHealthlist AddEmpHealth = new EmployeeHealthlist();
        _data_employee.BoxEmployeeHealthlist = new EmployeeHealthlist[1];
        AddEmpHealth.Height = float.Parse(ViewState["Height"].ToString());
        AddEmpHealth.Weight = float.Parse(ViewState["Weight"].ToString());
        AddEmpHealth.Scar = ViewState["Scar"].ToString();
        AddEmpHealth.BTypeIDX = int.Parse(ViewState["BTypeIDX"].ToString());
        #endregion

        #region U0Education 
        DetailEducation AddEducation = new DetailEducation();
        _data_employee.EducationList = new DetailEducation[1];
        AddEducation.Major = ViewState["Major"].ToString();
        AddEducation.Faculty = int.Parse(ViewState["Faculty"].ToString());
        AddEducation.University = ViewState["University"].ToString();
        AddEducation.Eduidx = int.Parse(ViewState["EduName"].ToString());
        AddEducation.GPA = ViewState["GPA"].ToString();
        AddEducation.academic_year = ViewState["academic_year"].ToString();
        //AddEducation.exper_idx = int.Parse(ViewState["exper_idx"].ToString());
        #endregion

        _data_employee.employee_list[0] = AddMainEmp;
        _data_employee.Reference_persons[0] = AddU0_Employee_Reference_Persons;
        _data_employee.BoxEmployeeTHlist[0] = AddEmpTH;
        _data_employee.BoxEmployeeFRlist[0] = AddEmpFR;
        _data_employee.BoxEmployeeHealthlist[0] = AddEmpHealth;
        _data_employee.EducationList[0] = AddEducation;

        _local_xml = _service_execute.actionExec(_Mas, "data_employee", MASActionAc, _data_employee, 100);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["id_empmain"] = _data_employee.return_code;
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_employee));
        #endregion

        #region insert 101
        employee_detail AddEmpODSP = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];
        AddEmpODSP.emp_idx = int.Parse(ViewState["id_empmain"].ToString());
        AddEmpODSP.rpos_idx = int.Parse(ddPosition.SelectedValue);
        _data_employee.employee_list[0] = AddEmpODSP;

        //test2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_employee));
        _service_execute.actionExec(_Mas, "data_employee", MASActionAc, _data_employee, 106);
        #endregion

        #region 102
        var dsBuyHistoryEmploy = (DataSet)ViewState["vsBuyHistoryEmploy"];

        var Add_HisEmploy = new boxExperienceSend_BList[dsBuyHistoryEmploy.Tables[0].Rows.Count];
        int i = 0;

        foreach (DataRow dr in dsBuyHistoryEmploy.Tables[0].Rows)
        {

            Add_HisEmploy[i] = new boxExperienceSend_BList();
            Add_HisEmploy[i].emp_idx_experS = Int32.Parse(ViewState["id_empmain"].ToString());
            if (dr["name_company"] != null && dr["name_company"].ToString() != "")
            {
                Add_HisEmploy[i].name_companyS = dr["name_company"].ToString();
            }
            else
            {
                Add_HisEmploy[i].name_companyS = "ไม่มีข้อมูล";
            }
            if (dr["position_old"] != null && dr["position_old"].ToString() != "")
            {
                Add_HisEmploy[i].position_oldS = dr["position_old"].ToString();
            }
            else
            {
                Add_HisEmploy[i].position_oldS = "ไม่มีข้อมูล";
            }
            if (dr["motive"] != null && dr["motive"].ToString() != "")
            {
                Add_HisEmploy[i].motiveS = dr["motive"].ToString();
            }
            else
            {
                Add_HisEmploy[i].motiveS = "ไม่มีข้อมูล";
            }
            if (dr["salary_old"] != null && dr["salary_old"].ToString() != "")
            {
                Add_HisEmploy[i].salary_oldS = dr["salary_old"].ToString();
            }
            else
            {
                Add_HisEmploy[i].salary_oldS = "ไม่มีข้อมูล";
            }
            if (dr["job_brief_details"] != null && dr["job_brief_details"].ToString() != "")
            {
                Add_HisEmploy[i].job_brief_detailsS = dr["job_brief_details"].ToString();
            }
            else
            {
                Add_HisEmploy[i].job_brief_detailsS = "ไม่มีข้อมูล";
            }
            i++;
        }

        data_presonal Box_Add_Add_HisEmploy = new data_presonal();
        Box_Add_Add_HisEmploy.boxExperienceSend_B = Add_HisEmploy;

        //test2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Box_Add_Add_HisEmploy));
        _service_execute.actionExec(_Mas, "data_presonal", MASActionAc, Box_Add_Add_HisEmploy, 102);
        #endregion

        #region เปลี่ยนสเตตัส Job
        Send_StatuJob_list SendStatusJob = new Send_StatuJob_list();
        _data_job.Send_StatuJob = new Send_StatuJob_list[1];
        SendStatusJob.idEmpMain = int.Parse(ViewState["idEmpMain"].ToString());
        _data_job.Send_StatuJob[0] = SendStatusJob;

        _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 77);
        #endregion

        ddcompany.SelectedValue = "00";
        ddparty.SelectedValue = "00";
        dddepartment.SelectedValue = "00";
        ddPosition.SelectedValue = "00";
    }
    #endregion

    #region returnpart 
    protected string retrunpat(int x)
    {
        string getPath = ConfigurationSettings.AppSettings["PathFileJobONLine"];
        string pat = getPath + x.ToString() + "/" + x.ToString() + "0.jpg";
        return pat;
    }
    protected string retrunpatFile(string x)
    {
        string getPath = ConfigurationSettings.AppSettings["PathFileJobDocONLine"];
        string pat = getPath + ViewState["idEmpMain"].ToString() + x.ToString() + "/" + ViewState["idEmpMain"].ToString() + x.ToString() + "0.jpg";
        return pat;

    }
    #endregion

    #region เลือกแล้วเก็บค่า value
    protected void selectchang(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        DropDownList ddparty = (DropDownList)FormView3.FindControl("ddparty");
        DropDownList dddepartment = (DropDownList)FormView3.FindControl("dddepartment");
        DropDownList ddPosition = (DropDownList)FormView3.FindControl("ddPosition");
        TextBox date_S = (TextBox)FormView1.FindControl("date_S");
        TextBox dateStat = (TextBox)FormView1.FindControl("dateStat");
        DropDownList ddlSearchDate = (DropDownList)FormView1.FindControl("ddlSearchDate");
        TextBox date_SMain = (TextBox)FormView2.FindControl("date_SMain");
        TextBox dateStatMain = (TextBox)FormView2.FindControl("dateStatMain");
        DropDownList ddlSearchDateMain = (DropDownList)FormView2.FindControl("ddlSearchDateMain");
        Panel Hinddate = (Panel)FormView1.FindControl("Hinddate");
        Panel Hinddate2 = (Panel)FormView2.FindControl("Hinddate2");
        switch (ddName.ID)
        {
            case "ddlSearchDateMain":
                date_SMain.Text = string.Empty;
                if (ddlSearchDateMain.SelectedValue != "00")
                {
                    Hinddate2.Visible = true;
                }
                else
                {
                    Hinddate2.Visible = false;
                }
                if (ddlSearchDateMain.SelectedValue == "3")
                {
                    dateStatMain.Enabled = true;
                }
                else
                {
                    dateStatMain.Enabled = false;
                    dateStatMain.Text = string.Empty;
                }
                break;

            case "ddlSearchDate":
                date_S.Text = string.Empty;
                if (ddlSearchDate.SelectedValue != "00")
                {
                    Hinddate.Visible = true;
                }
                else
                {
                    Hinddate.Visible = false;
                }
                if (ddlSearchDate.SelectedValue == "3")
                {
                    dateStat.Enabled = true;
                }
                else
                {
                    dateStat.Enabled = false;
                    dateStat.Text = string.Empty;
                }
                break;
            case "ddwork":
                ViewState["Position_S"] = int.Parse(((DropDownList)FormView1.FindControl("ddwork")).SelectedValue);
                //test.Text = ViewState["Position_S"].ToString();
                break;
            case "ddcompany":
                ViewState["CompanyIDX"] = int.Parse(((DropDownList)FormView3.FindControl("ddcompany")).SelectedValue);
                ddparty.Items.Clear();
                if (ViewState["CompanyIDX"].ToString() != null)
                {
                    ddparty.Items.Clear();
                    Show_Department();
                    //ddparty.Focus();
                }

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal2();", true);
                break;

            case "ddparty":
                ViewState["DepartIDX"] = int.Parse(((DropDownList)FormView3.FindControl("ddparty")).SelectedValue);
                dddepartment.Items.Clear();
                if (ViewState["DepartIDX"].ToString() != null)
                {
                    dddepartment.Items.Clear();
                    Show_Section();
                    //dddepartment.Focus();
                }
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal2();", true);
                break;

            case "dddepartment":
                ViewState["PositionIDX"] = int.Parse(((DropDownList)FormView3.FindControl("dddepartment")).SelectedValue);
                ddPosition.Items.Clear();
                if (ViewState["PositionIDX"].ToString() != null)
                {
                    ddPosition.Items.Clear();
                    Show_Position();
                    //ddPosition.Focus();
                }
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal2();", true);
                break;
        }
    }
    #endregion

    #region SentMail
    protected void SentEmailComment()
    {
        string Link = ViewState["url"].ToString();
        string mail = ViewState["Email"].ToString(); //"kantida3620@gmail.com";//
        //string mail = "puritat.bom@gmail.com";
        string To = mail;
        string Bcc = "puritat.bom@gmail.com";

        MailMessage m = new MailMessage();
        SmtpClient sc = new SmtpClient();

        m.From = new MailAddress("puritat.bom@gmail.com");
        string[] ToId = To.Split(',');
        foreach (string ToEmail in ToId)
        {
            if (ToEmail != String.Empty)
            {
                m.To.Add(new MailAddress(ToEmail)); //Adding Multiple To email Id
            }
        }

        string[] BCCId = Bcc.Split(',');
        foreach (string BccEmail in BCCId)

        {
            if (BccEmail != String.Empty)
            {
                m.Bcc.Add(new MailAddress(BccEmail)); //Adding Multiple BCC email Id
            }
        }

        m.Subject = "Link สำหรับสมัครงานเถ้าแก่น้อย";
        m.IsBodyHtml = true;

        m.Body = "บริษัทได้สนใจข้อมูลเบื้องต้นของท่าน กรุณากรอกข้อมูลเพิ่มเติม" + "<br>" + "<br>"
            + "กรุณาเข้า Link นี้ : " + Link.ToString();

        sc.Host = "smtp.gmail.com";
        sc.EnableSsl = true;
        sc.UseDefaultCredentials = true;
        sc.Port = 587;
        sc.Credentials = new
        System.Net.NetworkCredential("noreply@taokaenoi.co.th", "knlolksosgsuakcd");
        //system@tkn
        sc.Send(m);
    }
    #endregion

    }