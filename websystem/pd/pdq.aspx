﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="pdq.aspx.cs" Inherits="websystem_pd_pdq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-lg-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab1"> สร้างรายการ</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> รายการทั่วไป</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbtab3" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab3"> รายการรออนุมัติ</asp:LinkButton>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-lg-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>
    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <asp:View ID="tab1" runat="server">
            <div class="col-lg-12">
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">องค์กร</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ฝ่าย</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">แผนก</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ตำแหน่ง</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvCreate" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างรายการ</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Order No</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlOrderNo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="-- Order No --" Value="0"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="ONoxxxx1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="ONoxxxx2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-lg-2 control-label">Line</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlLineNo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="-- Line --" Value="0"></asp:ListItem>
                                                <%-- <asp:ListItem Value="1" Text="Line ย่าง"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Line อบ"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Line ทอด"></asp:ListItem>--%>
                                            </asp:DropDownList>


                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Room</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlRoomNo" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="-- Room --" Value="0"></asp:ListItem>
                                                <%-- <asp:ListItem Value="1" Text="ห้องวัตถุดิบ"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="ห้องอบสาหร่าย"></asp:ListItem>--%>
                                            </asp:DropDownList>

                                        </div>
                                        <label class="col-lg-2 control-label">Form</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlFormNo" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-- Form --" Value="0"></asp:ListItem>
                                                <%-- <asp:ListItem Value="1" Text="FM-PD-PD-001/119"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="FM-PD-PD-001/123"></asp:ListItem>--%>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">

                                            <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-primary" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchCreate" CommandName="cmdSearchCreate" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                        </div>
                                        <label class="col-lg-2 control-label"></label>
                                        <label class="col-lg-4"></label>

                                    </div>

                                    <%--FM-PD-PD-001/119--%>
                                    <asp:Panel ID="Panel_ShowFormFM001_119" runat="server">

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_Materialname" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">หน่วย</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="ddlUnit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="-- หน่วย --" Value="0"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="PAC"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="L"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="KG"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดยกมา</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountQuoted" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchQuoted" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดรับเข้า</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountReceive" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchReceive" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดคงเหลือ</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountBalance" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchBalance" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="txt_Comment" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>

                                    </asp:Panel>
                                    <%--FM-PD-PD-001/119--%>


                                    <%-- FM_PD_PD_001_183 --%>
                                    <asp:Panel ID="Panel_ShowFormFM_183" runat="server">
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Material</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox1" CssClass="form-control" placeHolder="Material" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox2" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หน่วย</label>
                                            <div class="col-lg-4">
                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="-- หน่วย --" Value="0"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="PAC"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="L"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="KG"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-lg-2 control-label">Batch No.</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox4" CssClass="form-control" placeHolder="Batch No." runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 1</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox3" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox5" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 2</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox6" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox7" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 3</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox8" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox9" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 4</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox10" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox11" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 5</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox12" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox13" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 6</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox14" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox15" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 7</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox16" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox17" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 8</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox18" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox19" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 9</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox20" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox21" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 10</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox22" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox23" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="TextBox24" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>
                                    </asp:Panel>
                                    <%-- FM_PD_PD_001_183 --%>


                                    <%--FM-PD-PD-001/123--%>
                                    <asp:Panel ID="Panel_ShowFormFM001_123" runat="server">

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="txt_Time" CssClass="form-control" placeHolder="เวลา" runat="server">
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">ชื่อผลิตภัณฑ์</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_ProductName" CssClass="form-control" placeHolder="ชื่อผลิตภัณฑ์" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Oven 1</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">อุณหภูมิตู้อบ 1(300-320)</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="txt_Temperatures1" CssClass="form-control" placeHolder="อุณหภูมิตู้อบ1(300-320)" runat="server">
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">ความเร็วรอบ(450-550 rpm)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_Speed1" CssClass="form-control" placeHolder="ความเร็วรอบ(450-550 rpm)" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <div class="col-lg-4">
                                                <asp:CheckBox ID="cbCheckAlarm" runat="server" CssClass="form-check-input" Text="ตรวจสอบไฟ Alarm"></asp:CheckBox>
                                            </div>

                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Oven 2</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">อุณหภูมิตู้อบ 2(300-320)</label>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T1" CssClass="form-control" placeHolder="T1" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T2" CssClass="form-control" placeHolder="T2" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T3" CssClass="form-control" placeHolder="T3" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-1 control-label"></label>

                                            <label class="col-lg-2 control-label">ความเร็วรอบ(850-1050 rpm)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_Speed2" CssClass="form-control" placeHolder="ความเร็วรอบ(850-1050 rpm)" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="txt_commentoven" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>


                                    </asp:Panel>
                                    <%--FM-PD-PD-001/123--%>


                                    <%-- FM-PD-PD-001/143 --%>
                                    <asp:Panel ID="Panel_ShowFormFM_143" runat="server">
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Material</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox25" CssClass="form-control" placeHolder="Material" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox26" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">ผลิตภัณฑ์</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox27" CssClass="form-control" placeHolder="ผลิตภัณฑ์" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">รายการถุงโหล</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox49" CssClass="form-control" placeHolder="รายการถุงโหล" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">mat</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox28" CssClass="form-control" placeHolder="mat" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox29" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวนซอง</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox30" CssClass="form-control" placeHolder="จำนวนซอง" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ราคา (บาท)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox31" CssClass="form-control" placeHolder="ราคา (บาท)" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Supplier</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox32" CssClass="form-control" placeHolder="Supplier" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ประเทศผู้ผลิต</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox33" CssClass="form-control" placeHolder="ประเทศผู้ผลิต" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Barcode</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox34" CssClass="form-control" placeHolder="Barcode" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">รูปแบบการพิมพ์วันที่</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox35" CssClass="form-control" placeHolder="รูปแบบการพิมพ์วันที่" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">รอยซีลถุงโหล</label>
                                            <div class="col-lg-4">

                                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="มาตรฐาน"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="ไม่ได้มาตรฐาน"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-lg-2 control-label">การเคลียร์ไลน์</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="เคลียร์"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="ไม่เคลียร์"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="TextBox48" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>
                                    </asp:Panel>
                                    <%-- FM_PD_PD_001_183 --%>


                                    <%-- Panel Save --%>
                                    <asp:Panel ID="Panel_Save" runat="server">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <div class="col-lg-4">
                                                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdSave" CommandArgument="0" ValidationGroup="SaveCreate"><i class="fas fa-save"></i>&nbsp;บันทึกข้อมูล</asp:LinkButton>

                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times"></i>&nbsp;ยกเลิก</asp:LinkButton>

                                            </div>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>
                                    </asp:Panel>
                                    <%-- Panel Save --%>
                                </div>

                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>

            </div>
        </asp:View>

        <asp:View ID="tab2" runat="server">
            <div class="col-lg-12">
                รายการทั่วไป
            </div>
        </asp:View>

        <asp:View ID="tab3" runat="server">
            <div class="col-lg-12">

                <asp:FormView ID="fvEmpLogin" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">องค์กร</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ฝ่าย</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">แผนก</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-lg-2 control-label">ตำแหน่ง</label>
                                        <div class="col-lg-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvApprove" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">อนุมัติรายการ</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Order No</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlOrderNo" runat="server" CssClass="form-control"   >
                                                <asp:ListItem Text="-- Order No --" Value="0"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="ONoxxxx1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="ONoxxxx2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-lg-2 control-label">Line</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlLineNo" runat="server" CssClass="form-control"  >
                                                <asp:ListItem Text="-- Line --" Value="0"></asp:ListItem>
                                            </asp:DropDownList>


                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Room</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlRoomNo" runat="server" CssClass="form-control"  >
                                                <asp:ListItem Text="-- Room --" Value="0"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <label class="col-lg-2 control-label">Form</label>
                                        <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                            <asp:DropDownList ID="ddlFormNo" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="-- Form --" Value="0"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label"></label>
                                        <div class="col-lg-4">

                                            <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn btn-primary" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchCreate" CommandName="cmdSearchCreate" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                        </div>
                                        <label class="col-lg-2 control-label"></label>
                                        <label class="col-lg-4"></label>

                                    </div>

                                    <%--FM-PD-PD-001/119--%>
                                    <asp:Panel ID="Panel_ShowFormFM001_119" runat="server">

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_Materialname" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">หน่วย</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="ddlUnit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="-- หน่วย --" Value="0"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="PAC"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="L"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="KG"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดยกมา</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountQuoted" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchQuoted" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดรับเข้า</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountReceive" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchReceive" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">ยอดคงเหลือ</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_CountBalance" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="txt_BatchBalance" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="txt_Comment" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>

                                    </asp:Panel>
                                    <%--FM-PD-PD-001/119--%>


                                    <%-- FM_PD_PD_001_183 --%>
                                    <asp:Panel ID="Panel_ShowFormFM_183" runat="server">
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Material</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox1" CssClass="form-control" placeHolder="Material" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox2" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หน่วย</label>
                                            <div class="col-lg-4">
                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="-- หน่วย --" Value="0"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="PAC"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="L"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="KG"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-lg-2 control-label">Batch No.</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox4" CssClass="form-control" placeHolder="Batch No." runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 1</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox3" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox5" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 2</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox6" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox7" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 3</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox8" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox9" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 4</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox10" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox11" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 5</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox12" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox13" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 6</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox14" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox15" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 7</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox16" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox17" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 8</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox18" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox19" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 9</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox20" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox21" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Batch 10</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox22" CssClass="form-control" placeHolder="เวลา" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">จำนวน</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox23" CssClass="form-control" placeHolder="จำนวน" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="TextBox24" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>
                                    </asp:Panel>
                                    <%-- FM_PD_PD_001_183 --%>


                                    <%--FM-PD-PD-001/123--%>
                                    <asp:Panel ID="Panel_ShowFormFM001_123" runat="server">

                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">เวลา</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="txt_Time" CssClass="form-control" placeHolder="เวลา" runat="server">
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">ชื่อผลิตภัณฑ์</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_ProductName" CssClass="form-control" placeHolder="ชื่อผลิตภัณฑ์" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Oven 1</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">อุณหภูมิตู้อบ 1(300-320)</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="txt_Temperatures1" CssClass="form-control" placeHolder="อุณหภูมิตู้อบ1(300-320)" runat="server">
                                                </asp:TextBox>
                                            </div>

                                            <label class="col-lg-2 control-label">ความเร็วรอบ(450-550 rpm)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_Speed1" CssClass="form-control" placeHolder="ความเร็วรอบ(450-550 rpm)" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <div class="col-lg-4">
                                                <asp:CheckBox ID="cbCheckAlarm" runat="server" CssClass="form-check-input" Text="ตรวจสอบไฟ Alarm"></asp:CheckBox>
                                            </div>

                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">Oven 2</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">อุณหภูมิตู้อบ 2(300-320)</label>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T1" CssClass="form-control" placeHolder="T1" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T2" CssClass="form-control" placeHolder="T2" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <div class="col-lg-1">
                                                <asp:TextBox ID="txt_T3" CssClass="form-control" placeHolder="T3" runat="server">
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-1 control-label"></label>

                                            <label class="col-lg-2 control-label">ความเร็วรอบ(850-1050 rpm)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="txt_Speed2" CssClass="form-control" placeHolder="ความเร็วรอบ(850-1050 rpm)" runat="server">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="txt_commentoven" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>


                                    </asp:Panel>
                                    <%--FM-PD-PD-001/123--%>


                                    <%-- FM-PD-PD-001/143 --%>
                                    <asp:Panel ID="Panel_ShowFormFM_143" runat="server">
                                        <hr />
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4 control-label-static">รายละเอียดการกรอกข้อมูล</label>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Material</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox25" CssClass="form-control" placeHolder="Material" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ชื่อวัตถุดิบ</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox26" CssClass="form-control" placeHolder="ชื่อวัตถุดิบ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">ผลิตภัณฑ์</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox27" CssClass="form-control" placeHolder="ผลิตภัณฑ์" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">รายการถุงโหล</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox49" CssClass="form-control" placeHolder="รายการถุงโหล" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">mat</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox28" CssClass="form-control" placeHolder="mat" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">Batch</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox29" CssClass="form-control" placeHolder="Batch" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">จำนวนซอง</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox30" CssClass="form-control" placeHolder="จำนวนซอง" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ราคา (บาท)</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox31" CssClass="form-control" placeHolder="ราคา (บาท)" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Supplier</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox32" CssClass="form-control" placeHolder="Supplier" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">ประเทศผู้ผลิต</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox33" CssClass="form-control" placeHolder="ประเทศผู้ผลิต" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Barcode</label>
                                            <div class="col-lg-4">

                                                <asp:TextBox ID="TextBox34" CssClass="form-control" placeHolder="Barcode" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">รูปแบบการพิมพ์วันที่</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:TextBox ID="TextBox35" CssClass="form-control" placeHolder="รูปแบบการพิมพ์วันที่" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">รอยซีลถุงโหล</label>
                                            <div class="col-lg-4">

                                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="มาตรฐาน"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="ไม่ได้มาตรฐาน"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-lg-2 control-label">การเคลียร์ไลน์</label>
                                            <div class="col-lg-4 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="เคลียร์"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="ไม่เคลียร์"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">หมายเหตุ</label>
                                            <div class="col-lg-10">

                                                <asp:TextBox ID="TextBox48" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="หมายเหตุ" runat="server">
                                           
                                                </asp:TextBox>
                                            </div>

                                            <%--  <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>--%>
                                        </div>
                                    </asp:Panel>
                                    <%-- FM_PD_PD_001_183 --%>


                                    <%-- Panel Save --%>
                                    <asp:Panel ID="Panel_Save" runat="server">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <div class="col-lg-4">
                                                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdSave" CommandArgument="0" ValidationGroup="SaveCreate"><i class="fas fa-save"></i>&nbsp;บันทึกข้อมูล</asp:LinkButton>

                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times"></i>&nbsp;ยกเลิก</asp:LinkButton>

                                            </div>
                                            <label class="col-lg-2 control-label"></label>
                                            <label class="col-lg-4"></label>
                                        </div>
                                    </asp:Panel>
                                    <%-- Panel Save --%>
                                </div>

                            </div>
                        </div>
                    </InsertItemTemplate>

                </asp:FormView>


            </div>
        </asp:View>
    </asp:MultiView>
    <!--multiview-->

</asp:Content>
