﻿using System;
using System.Xml.Serialization;



[Serializable]
[XmlRoot("data_hr_news")]

public class data_hr_news
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }
    [XmlElement("news_mode")]
    public int news_mode { get; set; }
    


    [XmlElement("hr_news_type_m0")]
    public hr_news_type_detail_m0[] hr_news_type_m0 { get; set; } //type news m0

    [XmlElement("hr_news_type_m1")]
    public hr_news_type_detail_m1[] hr_news_type_m1 { get; set; } //title news m1

    [XmlElement("hr_news_list_u0")]
    public hr_news_list_detail_u0[] hr_news_list_u0 { get; set; } //list news u0

    [XmlElement("hr_news_slide_u0")]
    public hr_news_slide_detail_u0[] hr_news_slide_u0 { get; set; } //slide news u0

    [XmlElement("hr_news_img_u0")]
    public hr_news_img_detail_u0[] hr_news_img_u0 { get; set; } //img news u0

    [XmlElement("hr_news_search_list")]
    public hr_news_search_detail[] hr_news_search_list { get; set; } //news search

    [XmlElement("hr_news_per_list")]
    public hr_news_per_detail[] hr_news_per_list { get; set; } //news permission



}
[Serializable]

public class hr_news_type_detail_m0
{
    [XmlElement("type_idx")]
    public int type_idx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("type_status")]
    public int type_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
[Serializable]

public class hr_news_type_detail_m1
{
    [XmlElement("title_idx")]
    public int title_idx { get; set; }
    [XmlElement("type_idx")]
    public int type_idx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("title_name")]
    public string title_name { get; set; }
    [XmlElement("title_type_file")]
    public int title_type_file { get; set; }
    [XmlElement("title_status")]
    public int title_status { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
[Serializable]

public class hr_news_list_detail_u0
{
    [XmlElement("title_idx")]
    public int title_idx { get; set; }
    [XmlElement("type_idx")]
    public int type_idx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("title_name")]
    public string title_name { get; set; }
    [XmlElement("title_type_file")]
    public int title_type_file { get; set; }



    [XmlElement("list_idx")]
    public int list_idx { get; set; }
    [XmlElement("list_name")]
    public string list_name { get; set; }
    [XmlElement("list_desc")]
    public string list_desc { get; set; }
    [XmlElement("list_file")]
    public string list_file { get; set; }
    [XmlElement("list_timeout_start")]
    public string list_timeout_start { get; set; }
    [XmlElement("list_timeout_end")]
    public string list_timeout_end { get; set; }
    [XmlElement("list_status")]
    public int list_status { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
[Serializable]
public class hr_news_slide_detail_u0
{
    [XmlElement("slide_idx")]
    public int slide_idx { get; set; }
    [XmlElement("slide_file")]
    public string slide_file { get; set; }
    [XmlElement("slide_timeout_start")]
    public string slide_timeout_start { get; set; }
    [XmlElement("slide_timeout_end")]
    public string slide_timeout_end { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("slide_status")]
    public int slide_status { get; set; }
    [XmlElement("slide_show")]
    public int slide_show { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
[Serializable]
public class hr_news_img_detail_u0
{
    [XmlElement("img_idx")]
    public int img_idx { get; set; }
    [XmlElement("img_file")]
    public string img_file { get; set; }
    [XmlElement("img_timeout_start")]
    public string img_timeout_start { get; set; }
    [XmlElement("img_timeout_end")]
    public string img_timeout_end { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("img_status")]
    public int img_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class hr_news_search_detail
{

    [XmlElement("s_type")]
    public string s_type { get; set; }

    [XmlElement("s_title")]
    public string s_title { get; set; }

    [XmlElement("s_name")]
    public string s_name { get; set; }

    [XmlElement("s_status")]
    public string s_status { get; set; }

    [XmlElement("s_show")]
    public string s_show { get; set; }

    [XmlElement("s_start_time")]
    public string s_start_time { get; set; }

    [XmlElement("s_end_time")]
    public string s_end_time { get; set; }
}

[Serializable]
public class hr_news_per_detail
{
    [XmlElement("per_idx")]
    public int per_idx { get; set; }
    [XmlElement("per_rpos_id")]
    public string per_rpos_id { get; set; }
    [XmlElement("per_name")]
    public string per_name { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("per_status")]
    public int per_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }

}
