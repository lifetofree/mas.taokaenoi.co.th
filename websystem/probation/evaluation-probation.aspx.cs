﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_probation_evaluation_probatioon : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_probation data_probation_ = new data_probation();
    service_mail servicemail = new service_mail();



    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetTypevaluation = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypevaluation"];
    static string _urlSetNamevaluation = _serviceUrl + ConfigurationManager.AppSettings["urlSetNamevaluation"];
    static string _urlGetTopicsList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTopicsList"];
    static string _urlGetsubTopicsList = _serviceUrl + ConfigurationManager.AppSettings["urlGetsubTopicsList"];
    static string _urlGetListNameProbation = _serviceUrl + ConfigurationManager.AppSettings["urlGetListNameProbation"];
    static string _urlGetInformationNameEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetInformationNameEmployee"];
    static string _urlGetlistTopics = _serviceUrl + ConfigurationManager.AppSettings["urlGetlistTopics"];
    static string _urlSetResultAssessment = _serviceUrl + ConfigurationManager.AppSettings["urlSetResultAssessment"];
    static string _urlGetlistNameForPermission = _serviceUrl + ConfigurationManager.AppSettings["urlGetlistNameForPermission"];




    string _localJson = "";
    int _tempInt = 0;
    string _mail_subject = "";
    string _mail_body = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            mvMultiview.SetActiveView(viewHomepage);
            Setpermissions();
            action_selectNAme();
            //action_actor();
           // action_node();
        }
    }

    #region  Setpermissions
    protected void Setpermissions()
    {
        ViewState["emp_idx"] = 2;//int.Parse(Session["emp_idx"].ToString());

    }

#endregion

    #region setGridViewDataBind
    protected void setGridViewDataBind(GridView gridViewName, Object obj)
    {
        gridViewName.DataSource = obj;
        gridViewName.DataBind();
    }
    #endregion

    #region action node
    protected void action_node()
    {
        int node_idx = 0;
        int actor_idx = 0;
        int idx_assessor1 = 0;
        int idx_assessor2 = 0;
        int idx_hr_director = 0;

        HiddenField hidden_node = (HiddenField)hiddenValue.Items[0].FindControl("hidden_node_idx");
        HiddenField hidden_actor = (HiddenField)hiddenValue.Items[0].FindControl("hidden_actor_idx");
        HiddenField hidden_idx_assessor1 = (HiddenField)hiddenValue.Items[0].FindControl("hidden_emp_idx_assessor1");
        HiddenField hidden_idx_assessor2 = (HiddenField)hiddenValue.Items[0].FindControl("hidden_emp_idx_assessor2");
        HiddenField hidden_hr_director = (HiddenField)hiddenValue.Items[0].FindControl("hidden_hr_director_idx");

        node_idx = int.Parse(hidden_node.Value);
        actor_idx = int.Parse(hidden_actor.Value);
        idx_assessor1 = int.Parse(hidden_idx_assessor1.Value);
        idx_assessor2 = int.Parse(hidden_idx_assessor2.Value);
        idx_hr_director = int.Parse(hidden_hr_director.Value);

        switch (node_idx)
        {
            case 1:
                //if (actor_idx == 1 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                //   || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1)
                //{
                //    //if (actor_idx == 1)
                //    //{
                //    //    ViewState["setNodeValue"] = 2;
                //    //    ViewState["setActorValue"] = 2;
                //    //}

                //}
                break;

            case 2:

                if (actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                    || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                    || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                    || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                    || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director
                    || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director)
                {

                    //if (actor_idx == 2)
                    //{
                    //    ViewState["setNodeValue"] = 2;
                    //    ViewState["setActorValue"] = 2;
                    //}
                    gvListNameProbation.Visible = true;
                    check_XML.Text = "ผู้ประเมินคนที่ 1 ดูได้";
                }

                break;

            case 3:
                if (actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director)
                {
                    gvListNameProbation.Visible = true;
                    check_XML.Text = "ผู้ประเมินคนที่ 1,2 ดูได้";
                }
                break;

            case 4:
                if (actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor1
                   || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_assessor2
                   || actor_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director
                   || actor_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director
                   || actor_idx == 4 && int.Parse(ViewState["emp_idx"].ToString()) == idx_hr_director)
                {
                    gvListNameProbation.Visible = true;
                    check_XML.Text = "ผู้ประเมิน ดูได้ หมด";
                }
                break;

            case 5:

                break;
        }

    }


#endregion

    #region action actor
    protected void action_actor()
    {
     //   int u0_assessment_idx = 0;
        int m0_node_idx = 0;
        int m0_actor_idx = 0;
        int emp_idx_assessor1 = 0;
        int emp_idx_assessor2 = 0;
        int emp_idx_hr_director = 0; 

       // Label hidden_u0_assessment_idx = (Label).FindControl("hidden_u0_assessment_idx");
        HiddenField hidden_node_idx = (HiddenField)hiddenValue.Items[0].FindControl("hidden_node_idx");
        HiddenField hidden_actor_idx = (HiddenField)hiddenValue.Items[0].FindControl("hidden_actor_idx");
        HiddenField hidden_emp_idx_assessor1 = (HiddenField)hiddenValue.Items[0].FindControl("hidden_emp_idx_assessor1");
        HiddenField hidden_emp_idx_assessor2 = (HiddenField)hiddenValue.Items[0].FindControl("hidden_emp_idx_assessor2");
        HiddenField hidden_hr_director_idx = (HiddenField)hiddenValue.Items[0].FindControl("hidden_hr_director_idx");

       // u0_assessment_idx = int.Parse(hidden_u0_assessment_idx.Value);
        m0_node_idx = int.Parse(hidden_node_idx.Value);
        m0_actor_idx = int.Parse(hidden_actor_idx.Value);
        emp_idx_assessor1 = int.Parse(hidden_emp_idx_assessor1.Value);
        emp_idx_assessor2 = int.Parse(hidden_emp_idx_assessor2.Value);
        emp_idx_hr_director = int.Parse(hidden_hr_director_idx.Value);

       // action_node();


        switch (m0_actor_idx)
        {
            case 1:
                //if (m0_node_idx == 1 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_assessor1 && emp_idx_assessor1 != 0
                //    || m0_node_idx == 2 || m0_node_idx == 3)
                //{

                //    gvListNameProbation.Visible = true;
                //    ViewState["setNodeValue"] = 2;
                //    ViewState["setActorValue"] = 2;
                //    check_XML.Text = "ผู้ประเมินคนที่ 1";
                //}
                break;

           case 2: 

                //if (m0_node_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_assessor2 && emp_idx_assessor2 != 0
                //    )
                //{
                //    gvListNameProbation.Visible = true;
                //    ViewState["setNodeValue"] = 3;
                //    ViewState["setActorValue"] = 3;
                //    check_XML.Text = "ผู้ประเมินคนที่ 2";
                //}

                //else if (m0_node_idx == 2 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_assessor1)
                //{
                //    gvListNameProbation.Visible = true;

                //}
                break;

            case 3:
                //if (m0_node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_hr_director && emp_idx_hr_director != 0
                // )
                //{
                //    gvListNameProbation.Visible = true;
                //    ViewState["setNodeValue"] = 3;
                //    ViewState["setActorValue"] = 4;
                //    check_XML.Text = "HR Director";
                //}

                //else if (m0_node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_assessor2)
                //{
                //    gvListNameProbation.Visible = true;

                //}
                break;

            case 4: //Hr director
                //if (m0_node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_hr_director)
                //{
                //    check_XML.Text = "HR Director";
                //}
                //else if (m0_node_idx == 3 && int.Parse(ViewState["emp_idx"].ToString()) == emp_idx_hr_director)
                //{
                //    gvListNameProbation.Visible = true;

                //}

                break;

        }
    }
#endregion

    #region select data
    protected void action_selectNAme()
    {
        data_probation data_probation_ = new data_probation();
        data_probation_.u0_name_probation_list = new u0_name_probation_detail[1];
        u0_name_probation_detail _name_probation = new u0_name_probation_detail();

        _name_probation.emp_probation_idx = int.Parse(ViewState["emp_idx"].ToString());

        data_probation_.u0_name_probation_list[0] = _name_probation;
        //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_probation_));
        data_probation_ = callServiceProbation(_urlGetlistNameForPermission, data_probation_);
        setGridViewDataBind(gvListNameProbation, data_probation_.u0_name_probation_list);

       // setRepeaterDataBind(hiddenValue, data_probation_.u0_name_probation_list);


    }

    protected void actionSelect_list_topics()
    {

        data_probation _data_probation = new data_probation();
        _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];
        m0_name_evaluation_detail select_information = new m0_name_evaluation_detail();

        select_information.emps_idx_topics = int.Parse(ViewState["emp_probation_idx"].ToString());

        _data_probation.m0_name_evaluation_list[0] = select_information;
        //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_probation));
        _data_probation = callServiceProbation(_urlGetlistTopics, _data_probation);

        setGridViewDataBind(gvform_employee_type_monthly, _data_probation.m0_name_evaluation_list);
        
    }

   protected void action_select_information_employee()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.u0_name_probation_list = new u0_name_probation_detail[1];
        u0_name_probation_detail select_information = new u0_name_probation_detail();
        select_information.emp_probation_idx = int.Parse(ViewState["emp_probation_idx"].ToString());
        _data_probation.u0_name_probation_list[0] = select_information;
        _data_probation = callServiceProbation(_urlGetInformationNameEmployee, _data_probation);

        setRepeaterDataBind(Information_employee, _data_probation.u0_name_probation_list);

     
    }


    #endregion

    #region insert data 


#endregion

    #region setRepeaterDataBind
    protected void setRepeaterDataBind(Repeater RepeaterName, Object obj)
    {
        RepeaterName.DataSource = obj;
        RepeaterName.DataBind();
    }
    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvListNameProbation":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lb_type_topics = (Label)e.Row.Cells[1].FindControl("lb_type_topics");

                    //if (int.Parse(lb_type_topics.Text) == 3)
                    //{
                    //   // BoundField test = new BoundField();
                    //   // test.DataField = "New DATAfield Name";
                    //   //// test.Headertext = "New Header";
                    //   // gvListNameProbation.Columns.Add(test);
                    //}
                    ////if()

                    //Repeater Repeater1 = (Repeater)e.Row.Cells[2].FindControl("Repeater1");
                    ////Label label_status_evaluation = (Label)e.Row.Cells[2].FindControl("label_status_evaluation");
                    ////Label status_offline = (Label)e.Row.Cells[2].FindControl("status_offline");
                    ////Label status_online = (Label)e.Row.Cells[2].FindControl("status_online");
                    //Repeater1.DataSource = null;
                    //Repeater1.DataBind();
                    //ViewState["status_evaluation"] = label_status_evaluation.Text;

                    //if (ViewState["status_evaluation"].ToString() == "9")
                    //{
                    //    status_offline.Visible = true;
                    //}
                    //else if (ViewState["status_evaluation"].ToString() == "1")
                    //{
                    //    status_online.Visible = true;
                    //}
                }
                break;

            case "gvform_evaluation_probation":
               // GridViewRow gvr = e.Row;
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ////int index = gvform_evaluation_probation.Rows.Count;
                    ////GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);

                    ////TableHeaderCell header = new TableHeaderCell();
                    ////TableHeaderRow headerRow = new TableHeaderRow();

                    ////headerRow.Controls.Add(header);
                  
                    //////  TableCell cell = new TableCell();

                    ////gvform_evaluation_probation.Controls[0].Controls.Add(row);

                    //////   //
                    //   TableHeaderRow  header = new TableHeaderRow();
                    //   label_txtHeader.Width = 350;
                    //  // label_txtHeader.Text = "ggggggggggggggg";

                    ////   header.Width = 350;
                    //   //header.Text = "ggggggggggggggg";
                    //   cell.Controls.Add(header);

                    ////   cell.Controls.Add(label_txtHeader);
                    //   TableCell cell2 = new TableCell();
                    //   TableCell cell3 = new TableCell();
                    //   TableCell cell4 = new TableCell();
                    //   TableCell cell5 = new TableCell();
                    //   TableCell cell6 = new TableCell();
                    //   TableCell cell7 = new TableCell();
                    //   row.Cells.Add(cell);
                    //   row.Cells.Add(cell2);
                    //   row.Cells.Add(cell3);
                    //   row.Cells.Add(cell4);
                    //   row.Cells.Add(cell5);
                    //   row.Cells.Add(cell6);
                    //   row.Cells.Add(cell7);
                    //   row.Cells[0].Visible = false;
                    //   row.Cells[1].Visible = false;
                    //   row.Cells[2].ColumnSpan = 6;
                    //   row.Cells[3].Visible = false;
                    //   row.Cells[4].Visible = false;
                    //   row.Cells[5].Visible = false;
                    //   row.Cells[6].Visible = false;
                    //   //row.Cells[2].Visible = false;
                 //   gvform_evaluation_probation.Controls[0].Controls.Add(row);


                }

                break;

        }
    }

    #endregion

    #region gvPageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvEvaluationType":

                break;
        }

    }

    #endregion

    #region btn_Command

    protected void btn_command(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmd_arg = e.CommandArgument.ToString();

        switch (cmd_name)
        {
            case "manage_assessment":
                mvMultiview.SetActiveView(viewPageEvaluation);
                string[] argument = new string[1];
                argument = e.CommandArgument.ToString().Split(';');
                int emp_probation_idx = int.Parse(argument[0]);
                int u0_assessment_idx = int.Parse(argument[1]);

                ViewState["emp_probation_idx"] = emp_probation_idx;
                ViewState["u0_assessment_idx"] = u0_assessment_idx;

               // check_XML3.Text = ViewState["u0_assessment_idx"].ToString();

                //  ViewState["emp_probation_idx"] = int.Parse(cmd_arg);
                action_select_information_employee();
                actionSelect_list_topics();
                setFocus.Focus();
                break;

            case "comebackhome":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "save_result_point":
             
                int excellent = 0;
                int numRow = 0;
                int veryGood = 0;
                int Good = 0;
                int fair = 0;
                int unsatisfactory = 0;


                data_probation _data_probation = new data_probation();
                _data_probation.u0_result_assessment_list = new u0_result_assessment_detail[1];
                u0_result_assessment_detail result_assessment = new u0_result_assessment_detail();

                _data_probation.u0_assessment_list = new u0_assessment_detail[1];
                u0_assessment_detail u0_aseessment = new u0_assessment_detail();

                ArrayList idTopics = new ArrayList();

                ViewState["count_select_excellent"] = 0;
                ViewState["count_select_veryGood"] = 0;
                ViewState["count_select_good"] = 0;
                ViewState["count_select_fair"] = 0;
                ViewState["count_select_unsatisfactory"] = 0;

                for (int i = 0; i < gvform_employee_type_monthly.Rows.Count; i++)
                {
                    numRow++;
                    ViewState["CountNumRow"] = numRow;
                    RadioButton radio_excellent = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_excellent");
                    RadioButton radio_veryGood = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_veryGood");
                    RadioButton radio_good = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_good");
                    RadioButton radio_fair = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_fair");
                    RadioButton radio_unsatisfactory = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_unsatisfactory");

                    Label m0_topics_idx = (Label)gvform_employee_type_monthly.Rows[i].FindControl("m0_topics_idx");
                    TextBox txttopicsName = (TextBox)gvform_employee_type_monthly.Rows[i].FindControl("txttopicsName");

                    if (radio_excellent.Checked)
                    {
                        excellent++;
                        ViewState["count_select_excellent"] = excellent;
                    }

                    if (radio_veryGood.Checked)
                    {
                        veryGood++;
                        ViewState["count_select_veryGood"] = veryGood;
                    }
                  
                    if (radio_good.Checked)
                    {
                        Good++;
                        ViewState["count_select_good"] = Good;
                    }
                   
                    if (radio_fair.Checked)
                    {
                        fair++;
                        ViewState["count_select_fair"] = fair;
                    }
                  
                    if (radio_unsatisfactory.Checked)
                    {
                        unsatisfactory++;
                        ViewState["count_select_unsatisfactory"] = unsatisfactory;
                    }

                    ViewState["summaryResult"] = idTopics;
                    check_XML4.Text = ViewState["summaryResult"].ToString();

                }

                int resultExcellent = 0;
                int resultVeryGood = 0;
                int resultGood = 0;
                int resultFair = 0;
                int resultUnsatisfactory = 0;
                double summary = 0;

                resultExcellent = int.Parse(ViewState["count_select_excellent"].ToString()) * 1;
                resultVeryGood = int.Parse(ViewState["count_select_veryGood"].ToString()) * 2;
                resultGood = int.Parse(ViewState["count_select_good"].ToString()) * 3;
                resultFair = int.Parse(ViewState["count_select_fair"].ToString()) * 4;
                resultUnsatisfactory = int.Parse(ViewState["count_select_unsatisfactory"].ToString()) * 5;


               // var Information_employee = (Repeater)viewPageEvaluation.FindControl("Information_employee");

                summary = resultExcellent + resultVeryGood + resultGood + resultFair + resultUnsatisfactory;
                ViewState["summary"] = summary;

                string averageValue = "";
               // double resultAverage = 0.0;
                averageValue = String.Format("{0:F2}", summary / int.Parse(ViewState["CountNumRow"].ToString())); /// 14;

                  ViewState["resultSummary"] =  averageValue + "<br />"; 
                //  check_XML1.Text = "ผลลัพธ์ : " + ViewState["resultSummary"].ToString() + "<br />";
                //  check_XML3.Text = "จำนวนแถว : " + ViewState["CountNumRow"].ToString() + "<br />";

                for (int i = 0; i <= gvform_employee_type_monthly.Rows.Count - 1; i++)
                {
                    RadioButton radio_excellent = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_excellent");
                    RadioButton radio_veryGood = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_veryGood");
                    RadioButton radio_good = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_good");
                    RadioButton radio_fair = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_fair");
                    RadioButton radio_unsatisfactory = (RadioButton)gvform_employee_type_monthly.Rows[i].FindControl("radio_unsatisfactory");

                    Label m0_topics_idx = (Label)gvform_employee_type_monthly.Rows[i].FindControl("m0_topics_idx");
                    TextBox txttopicsNote = (TextBox)gvform_employee_type_monthly.Rows[i].FindControl("txttopicsName");

                 

                    if (radio_excellent.Checked)
                    { ViewState["check_value"] = 1; }

                    if (radio_veryGood.Checked)
                    { ViewState["check_value"] = 2; }

                    if (radio_good.Checked)
                    { ViewState["check_value"] = 3; }

                    if (radio_fair.Checked)
                    { ViewState["check_value"] = 4; }

                    if (radio_unsatisfactory.Checked)
                    { ViewState["check_value"] = 5; }

                    result_assessment.idTopics = int.Parse(m0_topics_idx.Text);
                    result_assessment.full_score_result = int.Parse(ViewState["summary"].ToString());
                    result_assessment.status_result = 1;
                    result_assessment.score_result = int.Parse(ViewState["check_value"].ToString());
                    //  result_assessment.average_score_result = Convert.ToInt32(averageValue);
                    result_assessment.number_of_assessment = 1; //ต้องเป็นเลขโหนด
                    result_assessment.note_assessment = txttopicsNote.Text;
                    result_assessment.emp_idx_result = int.Parse(ViewState["emp_probation_idx"].ToString());
                    result_assessment.emp_idx_assessor = int.Parse(ViewState["emp_idx"].ToString());

                    //Node && Actor
                    u0_aseessment.u0_assessment_idx = 3;//int.Parse(ViewState["u0_assessment_idx"].ToString());
                    u0_aseessment.m0_actor_idx = 2;//int.Parse(ViewState["setActorValue"].ToString());
                    u0_aseessment.m0_node_idx = 2;//int.Parse(ViewState["setNodeValue"].ToString());

                    _data_probation.u0_assessment_list[0] = u0_aseessment;
                    _data_probation.u0_result_assessment_list[0] = result_assessment;

                    //  _data_probation = callServiceProbation(_urlSetResultAssessment, _data_probation);
                    Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_probation.u0_assessment_list));

                    content_conclude.Visible = true;

                    summary_result.Text = "<font size='5'>" +"<center>" + ViewState["summary"].ToString() + "</center>" + "</font>";
                    average_score_result.Text = "<font size='5'>" + "<center>" + ViewState["resultSummary"].ToString() + "</center>" + "</font>";
                    // int result_summary = int.Parse(ViewState["resultSummary"].ToString());
                    // ViewState["resultSummary"] = result_summary;

                    //if (int.Parse(averageValue.ToString()) <= 1.4)
                    //{
                    //    check_XML1.Text = "ดีเยี่ยม";
                    //}
                    //else if (int.Parse(averageValue.ToString()) >= 1.5 && int.Parse(averageValue.ToString()) <= 2.4)
                    //{
                    //    check_XML1.Text = "ดีมาก";
                    //}
                    //else if (int.Parse(averageValue.ToString()) >= 2.5 && int.Parse(averageValue.ToString()) <= 3.4)
                    //{
                    //    check_XML1.Text = "ดี";
                    //}
                    //else if (int.Parse(averageValue.ToString()) >= 3.5 && int.Parse(averageValue.ToString()) <= 4.4)
                    //{
                    //    check_XML1.Text = "พอใช้";
                    //}
                    //else if (int.Parse(averageValue.ToString()) >= 4.5 && int.Parse(averageValue.ToString()) <= 5.0)
                    //{
                    //    check_XML1.Text = "ไม่น่าพอใจ";
                    //}

                }

                //ข้อมูลส่งเมล์
                var content_information_employee = (Panel)viewPageEvaluation.FindControl("content_information_employee");
                Repeater Information_employee = (Repeater)content_information_employee.FindControl("Information_employee");
                Label lb_emps_code_probation = (Label)Information_employee.Items[0].FindControl("lb_emps_code_probation");
                Label lb_emps_fullname_probation = (Label)Information_employee.Items[0].FindControl("lb_emps_fullname_probation");
                Label lb_name_pos_probation = (Label)Information_employee.Items[0].FindControl("lb_name_pos_probation");
                Label lb_date_start_probation = (Label)Information_employee.Items[0].FindControl("lb_date_start_probation");

                u0_aseessment.emps_code_probation = lb_emps_code_probation.Text;//int.Parse(ViewState["u0_assessment_idx"].ToString());
                u0_aseessment.emps_fullname_probation = lb_emps_fullname_probation.Text;//int.Parse(ViewState["setActorValue"].ToString());
                u0_aseessment.name_pos_probation = lb_name_pos_probation.Text;//int.Parse(ViewState["setNodeValue"].ToString());
                u0_aseessment.date_start_probation = lb_date_start_probation.Text;//int.Parse(ViewState["setNodeValue"].ToString());


                _data_probation.u0_assessment_list[0] = u0_aseessment;

                //  _data_probation = callServiceProbation(_urlSetResultAssessment, _data_probation);
                Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_probation.u0_assessment_list));

                string emailmove = "msirirak4441@gmail.com";
                string replyempmove = "teoy_42@hotmail.com";
                ////string replyempmove = "nipon@taokeanoi.co.th";


                _mail_subject = "[HR/TEst :Probation] - " + "ประเมินทดลองงาน";
                _mail_body = servicemail.assessment_probation(_data_probation.u0_assessment_list[0]);

                //servicemail.SendHtmlFormattedEmailFull(email, "", "nipon@taokeanoi.co.th", _mail_subject, _mail_body);
                servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body);

                break;
        }

    }

#endregion

    #region call method
    protected data_probation callServiceProbation(string _cmdUrl, data_probation data_probation_)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(data_probation_);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        data_probation_ = (data_probation)_funcTool.convertJsonToObject(typeof(data_probation), _localJson);

        return data_probation_;
    }
    #endregion


}