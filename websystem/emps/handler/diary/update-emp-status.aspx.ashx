﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      int empIdxDiary = 0;
      int updator = 0;
      string callback = context.Request.QueryString["callback"];
      int.TryParse(context.Request.QueryString["empIdxDiary"], out empIdxDiary);
      int.TryParse(context.Request.QueryString["updator"], out updator);
      string json = this.updateAnnounceDiaryStatus(empIdxDiary, updator);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string updateAnnounceDiaryStatus(int empIdxDiary, int updator)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = "UPDATE emps_u2_announce_diary SET announce_diary_status = 2, announce_diary_updated_at = '" + now + "', announce_diary_updated_by = @updator WHERE announce_diary_emp_idx_ref = @empIdxDiary";
            cmd.Parameters.AddWithValue("@empIdxDiary", empIdxDiary);
            cmd.Parameters.AddWithValue("@updator", updator);
            cmd.ExecuteNonQuery();
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
