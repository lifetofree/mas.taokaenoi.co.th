﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_equipment_name : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetEquipment_name"];
    static string _urlCimsGetEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetEquipment_name"];
    static string _urlCimsDeleteEquipment_name = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteEquipment_name"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Equipment_name();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Equipment_name();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddEquipment_name":

                Gv_select_equipment_name.EditIndex = -1;
                Select_Equipment_name();
                btn_addequipment_name.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_equipment_name":
                Insert_Equipment_name();
                Select_Equipment_name();
                btn_addequipment_name.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_equipment_name":
                btn_addequipment_name.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_equipment_name":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Equipment_name_de = new data_qa_cims();
                qa_cims_m0_equipment_name_detail Equipment_name_sde = new qa_cims_m0_equipment_name_detail();

                Equipment_name_de.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];

                Equipment_name_sde.equipment_idx = int.Parse(a.ToString());

                Equipment_name_de.qa_cims_m0_equipment_name_list[0] = Equipment_name_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Equipment_name_de = callServicePostMasterQACIMS(_urlCimsDeleteEquipment_name, Equipment_name_de);

                Select_Equipment_name();
                //Gv_select_unit.Visible = false;
                btn_addequipment_name.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Equipment_name()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_equipment_name = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtequipment_name");
        DropDownList dropD_status_equipment_name = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddEquipment_name");

        data_qa_cims Equipment_name_b = new data_qa_cims();
        qa_cims_m0_equipment_name_detail Equipment_name_s = new qa_cims_m0_equipment_name_detail();
        Equipment_name_b.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        Equipment_name_s.equipment_name = tex_equipment_name.Text;
        Equipment_name_s.cemp_idx = _emp_idx;
        Equipment_name_s.equipment_status = int.Parse(dropD_status_equipment_name.SelectedValue);
        Equipment_name_b.qa_cims_m0_equipment_name_list[0] = Equipment_name_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Equipment_name_b = callServicePostMasterQACIMS(_urlCimsSetEquipment_name, Equipment_name_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Equipment_name_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_equipment_name.Text = String.Empty;
        }
    }

    protected void Select_Equipment_name()
    {
        data_qa_cims Equipment_name_b = new data_qa_cims();
        qa_cims_m0_equipment_name_detail Equipment_name_s = new qa_cims_m0_equipment_name_detail();

        Equipment_name_b.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
        Equipment_name_b.qa_cims_m0_equipment_name_list[0] = Equipment_name_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Equipment_name_b = callServicePostMasterQACIMS(_urlCimsGetEquipment_name, Equipment_name_b);
        setGridData(Gv_select_equipment_name, Equipment_name_b.qa_cims_m0_equipment_name_list);
        Gv_select_equipment_name.DataSource = Equipment_name_b.qa_cims_m0_equipment_name_list;
        Gv_select_equipment_name.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_equipment_name":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbequipment_status = (Label)e.Row.Cells[3].FindControl("lbequipment_status");
                    Label equipment_statusOnline = (Label)e.Row.Cells[3].FindControl("equipment_statusOnline");
                    Label equipment_statusOffline = (Label)e.Row.Cells[3].FindControl("equipment_statusOffline");

                    ViewState["_equipment_status"] = lbequipment_status.Text;


                    if (ViewState["_equipment_status"].ToString() == "1")
                    {
                        equipment_statusOnline.Visible = true;
                    }
                    else if (ViewState["_equipment_status"].ToString() == "0")
                    {
                        equipment_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addequipment_name.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_equipment_name":
                Gv_select_equipment_name.EditIndex = e.NewEditIndex;
                Select_Equipment_name();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_equipment_name":

                var equipment_idx = (TextBox)Gv_select_equipment_name.Rows[e.RowIndex].FindControl("ID_equipment_name");
                var equipment_name = (TextBox)Gv_select_equipment_name.Rows[e.RowIndex].FindControl("Name_equipment_name");
                var equipment_status = (DropDownList)Gv_select_equipment_name.Rows[e.RowIndex].FindControl("ddEdit_equipment_name");


                Gv_select_equipment_name.EditIndex = -1;

                data_qa_cims equipment_name_Update = new data_qa_cims();
                equipment_name_Update.qa_cims_m0_equipment_name_list = new qa_cims_m0_equipment_name_detail[1];
                qa_cims_m0_equipment_name_detail equipment_name_s = new qa_cims_m0_equipment_name_detail();
                equipment_name_s.equipment_idx = int.Parse(equipment_idx.Text);
                equipment_name_s.equipment_name = equipment_name.Text;
                equipment_name_s.equipment_status = int.Parse(equipment_status.SelectedValue);

                equipment_name_Update.qa_cims_m0_equipment_name_list[0] = equipment_name_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                equipment_name_Update = callServicePostMasterQACIMS(_urlCimsSetEquipment_name, equipment_name_Update);

                Select_Equipment_name();

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_equipment_name":
                Gv_select_equipment_name.EditIndex = -1;
                Select_Equipment_name();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addequipment_name.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_equipment_name":
                Gv_select_equipment_name.PageIndex = e.NewPageIndex;
                Gv_select_equipment_name.DataBind();
                Select_Equipment_name();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}