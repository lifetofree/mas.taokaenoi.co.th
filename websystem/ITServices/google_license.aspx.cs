﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_google_license : System.Web.UI.Page
{

    #region initial function/data

    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();

    data_googlelicense _data_googlelicense = new data_googlelicense();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();
    data_softwarelicense_devices _data_softwarelicense_devices = new data_softwarelicense_devices();
    data_employee _data_employee = new data_employee();

    data_employee dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //static string _keynetworkdevices = ConfigurationManager.AppSettings["keynetworkdevices"];  

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    //Google License
    static string _urlGetTypeBuyggl = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeBuyggl"];
    static string _urlGetSoftwarenameggl = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwarenameggl"];
    static string _urlGetCompanyggl = _serviceUrl + ConfigurationManager.AppSettings["urlGetCompanyggl"];
    static string _urlSetGooglelicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetGooglelicense"];
    static string _urlGetGooglelicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetGooglelicense"];
    static string _urlGetGooglelicenseDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetGooglelicenseDetail"];
    static string _urlGetSetGooglelicenseHD = _serviceUrl + ConfigurationManager.AppSettings["urlGetSetGooglelicenseHD"];
    static string _urlSetGooglelicenseHDEmail = _serviceUrl + ConfigurationManager.AppSettings["urlSetGooglelicenseHDEmail"];
    static string _urlGetHolderGmailLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetHolderGmailLicense"];
    static string _urlGetddlLotcode = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlLotcode"];
    static string _urlGetDetailInLot = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailInLot"];
    static string _urlSetTranferGoogleLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetTranferGoogleLicense"];
    static string _urlSetDeleteEmpggllicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmpggllicense"];
    static string _urlGetSearchIndexGooglelicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchIndexGooglelicense"];
    static string _urlSetBindReportGooglelicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetBindReportGooglelicense"];
    static string _urlGetEmpInReportGGl = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmpInReportGGl"];
    static string _urlGetDataSearchReportGGL = _serviceUrl + ConfigurationManager.AppSettings["urlGetDataSearchReportGGL"];
    static string _urlGetLogDeleteHolderGGL = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDeleteHolderGGL"];
    static string _urlSetGooglEditEmail = _serviceUrl + ConfigurationManager.AppSettings["urlSetGooglEditEmail"];
    static string _urlGetSelectTypeEmail = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectTypeEmail"];


    //Org
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlSectionSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSectionSW"];
    static string _urlGetddlEmpSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlEmpSW"];


    string _localJson = "";
    int _tempInt = 0;

    string _mail_subject = "";
    string _mail_body = "";

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    int m0_node = 0;
    int m0_actor = 0;
    int m0_status = 0;
    int _set_statusFilter = 0;
    int _setHeader_Filter = 0;

    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        //set Show Filter Search DDL TypeEmail In Gv Header
        ViewState["Vs_SetHeader_Filter"] = 0;

        if (!IsPostBack)
        {
            initPage();
            btnSearch.Visible = true;
            btnResetSearchPage.Visible = true;
            actionIndex();
            initPageLoad();

            Set_Defult_Index(); //set tab menu 
            btnToGoogleLicense.Visible = true;
            btnDetailGoogleLicense.Visible = false;
            div_saveInsertHolder.Visible = false;
            btnToReport.Visible = true;

            ddlOrganizationSearch();

            /////////////////////////////////////////////////////
            var dsEquipment = new DataSet();
            dsEquipment.Tables.Add("Equipment");


            //dsEquipment.Tables[0].Columns.Add("Software_License_dataset", typeof(String));
            //dsEquipment.Tables[0].Columns.Add("Softwareidx_License_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("OrgNameTH_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("OrgIDX_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("DeptNameTH_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("RDeptIDX_dataset", typeof(int));
            dsEquipment.Tables[0].Columns.Add("Costcenter_dataset", typeof(String));
            dsEquipment.Tables[0].Columns.Add("numlicense_dataset", typeof(int));
            ////dsEquipment.Tables[0].Columns.Add("status_id", typeof(int));
            //dsEquipment.Tables[0].Columns.Add("ra_name_sub", typeof(String));
            //dsEquipment.Tables[0].Columns.Add("name_sub", typeof(String));


            ViewState["vsBuyequipment"] = dsEquipment;

            ////////////////////////////////////////////////////


        }



    }

    #region Selected 

    protected void getTypeEmail(DropDownList ddlName, int _type_email_idx)
    {

        data_googlelicense _data_ddltype_email = new data_googlelicense();
        m0_type_email_ggl_detail _m0_ggltype_email = new m0_type_email_ggl_detail();
        _data_ddltype_email.m0_type_email_ggl_list = new m0_type_email_ggl_detail[1];

        _data_ddltype_email.m0_type_email_ggl_list[0] = _m0_ggltype_email;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        _data_ddltype_email = callServiceGoogleLicense(_urlGetSelectTypeEmail, _data_ddltype_email);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_ddltype_email));

        setDdlData(ddlName, _data_ddltype_email.m0_type_email_ggl_list, "m0type_email_name", "m0type_email_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทอีเมล ---", "0"));
        ddlName.SelectedValue = _type_email_idx.ToString();

    }

    protected void ddlTypeBuyGoogle()
    {

        FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        DropDownList ddltype_idxinsert = (DropDownList)FvInsert.FindControl("ddltype_idxinsert");

        ddltype_idxinsert.Items.Clear();
        ddltype_idxinsert.AppendDataBoundItems = true;
        ddltype_idxinsert.Items.Add(new ListItem("กรุณาเลือกประเภทการซื้อ ...", "00"));

        data_googlelicense _data_ddltype_insert = new data_googlelicense();
        m0_ggltype_detail _m0_ggltype_detail = new m0_ggltype_detail();
        _data_ddltype_insert.m0_ggltype_list = new m0_ggltype_detail[1];

        _data_ddltype_insert.m0_ggltype_list[0] = _m0_ggltype_detail;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_ddltype_insert = callServiceGoogleLicense(_urlGetTypeBuyggl, _data_ddltype_insert);

        //ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;

        ddltype_idxinsert.DataSource = _data_ddltype_insert.m0_ggltype_list;
        ddltype_idxinsert.DataTextField = "type_name";
        ddltype_idxinsert.DataValueField = "type_idx";
        ddltype_idxinsert.DataBind();


    }

    protected void ddlSoftwarenameGoogle()
    {

        FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        DropDownList ddlsoftware_name_idxinsert = (DropDownList)FvInsert.FindControl("ddlsoftware_name_idxinsert");

        ddlsoftware_name_idxinsert.Items.Clear();
        ddlsoftware_name_idxinsert.AppendDataBoundItems = true;
        ddlsoftware_name_idxinsert.Items.Add(new ListItem("กรุณาเลือกชื่อ Software ...", "00"));

        data_googlelicense _data_ddlsoftware_name_insert = new data_googlelicense();
        _data_ddlsoftware_name_insert.m0_gglsoftwarename_list = new m0_gglsoftwarename_detail[1];
        m0_gglsoftwarename_detail _m0_gglsoftwarename_detail = new m0_gglsoftwarename_detail();

        _data_ddlsoftware_name_insert.m0_gglsoftwarename_list[0] = _m0_gglsoftwarename_detail;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_ddlsoftware_name_insert = callServiceGoogleLicense(_urlGetSoftwarenameggl, _data_ddlsoftware_name_insert);

        //ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;

        ddlsoftware_name_idxinsert.DataSource = _data_ddlsoftware_name_insert.m0_gglsoftwarename_list;
        ddlsoftware_name_idxinsert.DataTextField = "name";
        ddlsoftware_name_idxinsert.DataValueField = "software_name_idx";
        ddlsoftware_name_idxinsert.DataBind();


    }

    protected void ddlCompanyGoogle()
    {

        FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        DropDownList ddlcompany_idxinsert = (DropDownList)FvInsert.FindControl("ddlcompany_idxinsert");

        ddlcompany_idxinsert.Items.Clear();
        ddlcompany_idxinsert.AppendDataBoundItems = true;
        ddlcompany_idxinsert.Items.Add(new ListItem("กรุณาเลือกบริษัทที่ซื้อ ...", "00"));

        data_googlelicense _data_ddlcompany_insert = new data_googlelicense();
        _data_ddlcompany_insert.m0_gglcompany_list = new m0_gglcompany_detail[1];
        m0_gglcompany_detail _m0_gglcompany_detail = new m0_gglcompany_detail();

        _data_ddlcompany_insert.m0_gglcompany_list[0] = _m0_gglcompany_detail;

        ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

        _data_ddlcompany_insert = callServiceGoogleLicense(_urlGetCompanyggl, _data_ddlcompany_insert);

        //ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;

        ddlcompany_idxinsert.DataSource = _data_ddlcompany_insert.m0_gglcompany_list;
        ddlcompany_idxinsert.DataTextField = "company_name";
        ddlcompany_idxinsert.DataValueField = "company_idx";
        ddlcompany_idxinsert.DataBind();


    }

    protected void ddlOrganizationGoogle()
    {
        FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");
        DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");

        ddlorg_idxinsert.Items.Clear();
        ddlorg_idxinsert.AppendDataBoundItems = true;
        ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

        _data_softwarelicense.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

        ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
        ddlorg_idxinsert.DataTextField = "OrgNameTH";
        ddlorg_idxinsert.DataValueField = "OrgIDX";
        ddlorg_idxinsert.DataBind();


    }

    protected void actionIndex()
    {

        data_googlelicense _data_googlelicenseindex = new data_googlelicense();
        _data_googlelicenseindex.bind_gglbuy_list = new bind_gglbuy_detail[1];

        bind_gglbuy_detail bind_gglbuy_detailindex = new bind_gglbuy_detail();

        bind_gglbuy_detailindex.cemp_idx = emp_idx;//int.Parse(ViewState["emp_permission_in"].ToString());
        //bind_gglbuy_detailindex.rsec_idx = int.Parse(ViewState["rsec_idx_empwhere"].ToString());

        _data_googlelicenseindex.bind_gglbuy_list[0] = bind_gglbuy_detailindex;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_softwarelicense_devicesindex));
        _data_googlelicenseindex = callServiceGoogleLicense(_urlGetGooglelicense, _data_googlelicenseindex);


        ViewState["bind_data_index"] = _data_googlelicenseindex.bind_gglbuy_list;

        setGridData(GvMaster, ViewState["bind_data_index"]);


    }

    protected void SelectDetailInsert()
    {
        data_googlelicense _data_googlelicenseview = new data_googlelicense();
        _data_googlelicenseview.bind_gglbuy_list = new bind_gglbuy_detail[1];

        bind_gglbuy_detail _bind_gglbuy_detailview = new bind_gglbuy_detail();

        _bind_gglbuy_detailview.m0_idx = int.Parse(ViewState["m0_idx_buy"].ToString());
        _bind_gglbuy_detailview.org_idx = int.Parse(ViewState["org_idx_buy"].ToString());
        _bind_gglbuy_detailview.rdept_idx = int.Parse(ViewState["rdept_idx_buy"].ToString());
        _bind_gglbuy_detailview.u0_buy_idx = int.Parse(ViewState["u0_idx_buy"].ToString());

        _data_googlelicenseview.bind_gglbuy_list[0] = _bind_gglbuy_detailview;

        _data_googlelicenseview = callServiceGoogleLicense(_urlGetGooglelicenseDetail, _data_googlelicenseview);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseview));

        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_googlelicenseview.bind_gglbuy_list);
    }

    protected void ddlSecInsertHolder()
    {

        //FormView FvInsert = (FormView)ViewSoftwareDevices.FindControl("FvInsert");
        DropDownList _ddlrsec_idxinsert = (DropDownList)FvInsert_HolderLicense.FindControl("ddlrsec_idxinsert");

        _ddlrsec_idxinsert.Items.Clear();
        _ddlrsec_idxinsert.AppendDataBoundItems = true;
        _ddlrsec_idxinsert.Items.Add(new ListItem("กรุณาเลือกแผนก ...", "00"));

        data_softwarelicense _data_softwarelicense_orgreport = new data_softwarelicense();
        _data_softwarelicense_orgreport.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;
        _organization_detail.OrgIDX = int.Parse(ViewState["org_idx_buy"].ToString());
        _organization_detail.RDeptIDX = int.Parse(ViewState["rdept_idx_buy"].ToString());

        _data_softwarelicense_orgreport.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgreport = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicense_orgreport);

        _ddlrsec_idxinsert.DataSource = _data_softwarelicense_orgreport.organization_list;
        _ddlrsec_idxinsert.DataTextField = "SecNameTH";
        _ddlrsec_idxinsert.DataValueField = "RSecIDX";
        _ddlrsec_idxinsert.DataBind();




    }

    protected void SelectDetailHolderGmail()
    {

        data_googlelicense _data_googlelicenseshowholder = new data_googlelicense();
        _data_googlelicenseshowholder.bind_holdergmail_list = new bind_holdergmail_detail[1];

        bind_holdergmail_detail _bind_holdergmail = new bind_holdergmail_detail();

        _bind_holdergmail.m0_idx = int.Parse(ViewState["m0_idx_buy"].ToString());

        _data_googlelicenseshowholder.bind_holdergmail_list[0] = _bind_holdergmail;

        _data_googlelicenseshowholder = callServiceGoogleLicense(_urlGetHolderGmailLicense, _data_googlelicenseshowholder);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseview));

        if (_data_googlelicenseshowholder.return_code == 0)
        {
            ViewState["vs_Count_Holder_Check"] = _data_googlelicenseshowholder.bind_holdergmail_list[0].count_empholder;
            ViewState["vs_Type_Mail_Holder"] = _data_googlelicenseshowholder.bind_holdergmail_list[0].m0type_email_idx;
            setGridData(GvShowDetailHolder, _data_googlelicenseshowholder.bind_holdergmail_list);

            //litDebug.Text = ViewState["vs_Count_Holder_Check"].ToString();
        }
        else
        {
            ViewState["vs_Count_Holder_Check"] = "0";
            ViewState["vs_Type_Mail_Holder"] = "0";
            setGridData(GvShowDetailHolder, null);
            //litDebug.Text = ViewState["vs_Count_Holder_Check"].ToString();
        }

    }

    protected void ddlLotGoogle()
    {

        FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        Panel lot_dateexpire = (Panel)FvInsert.FindControl("lot_dateexpire");
        DropDownList ddllblot_code = (DropDownList)lot_dateexpire.FindControl("ddllblot_code");

        ddllblot_code.Items.Clear();
        ddllblot_code.AppendDataBoundItems = true;
        ddllblot_code.Items.Add(new ListItem("กรุณาเลือก lot ที่ต่ออายุ ...", "00"));

        data_googlelicense _data_ddlsoftware_lot = new data_googlelicense();
        _data_ddlsoftware_lot.u0_gglbuy_list = new u0_gglbuy_detail[1];
        u0_gglbuy_detail _u0_gglbuy_detail_lot = new u0_gglbuy_detail();

        _data_ddlsoftware_lot.u0_gglbuy_list[0] = _u0_gglbuy_detail_lot;

        // _data_googlelicenseshowholder = callServiceGoogleLicense(_urlGetHolderGmailLicense, _data_googlelicenseshowholder);
        _data_ddlsoftware_lot = callServiceGoogleLicense(_urlGetddlLotcode, _data_ddlsoftware_lot);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_ddlsoftware_lot));


        ////ViewState["type_idx"] = _data_ddl_sw.softwarename_list[0].software_type;
        ddllblot_code.DataSource = _data_ddlsoftware_lot.u0_gglbuy_list;
        ddllblot_code.DataTextField = "lot_codebuy";
        ddllblot_code.DataValueField = "u0_buy_idx";
        ddllblot_code.DataBind();



    }

    protected void ddlOrganizationTranfer()
    {
        //FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        //Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");
        DropDownList ddlorg_idxtranfer = (DropDownList)ViewIndex.FindControl("ddlorg_idxtranfer");

        ddlorg_idxtranfer.Items.Clear();
        ddlorg_idxtranfer.AppendDataBoundItems = true;
        ddlorg_idxtranfer.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgtranfer = new data_softwarelicense();
        _data_softwarelicense_orgtranfer.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgtranfer.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgtranfer = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgtranfer);

        ddlorg_idxtranfer.DataSource = _data_softwarelicense_orgtranfer.organization_list;
        ddlorg_idxtranfer.DataTextField = "OrgNameTH";
        ddlorg_idxtranfer.DataValueField = "OrgIDX";
        ddlorg_idxtranfer.DataBind();


    }

    protected void SelectDetailEmpDelete()
    {

        data_googlelicense _data_googlelicenseshowdelete = new data_googlelicense();
        _data_googlelicenseshowdelete.bind_holdergmail_list = new bind_holdergmail_detail[1];

        bind_holdergmail_detail _bind_holdergmail = new bind_holdergmail_detail();

        _bind_holdergmail.m0_idx = int.Parse(ViewState["m0_idx_delete"].ToString());

        _data_googlelicenseshowdelete.bind_holdergmail_list[0] = _bind_holdergmail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseshowdelete));

        _data_googlelicenseshowdelete = callServiceGoogleLicense(_urlGetHolderGmailLicense, _data_googlelicenseshowdelete);

        ViewState["data_delete_empholder"] = _data_googlelicenseshowdelete.bind_holdergmail_list;

        // check return_code
        if (_data_googlelicenseshowdelete.return_code == 0)
        {
            //if (ViewState["data_delete_empholder"] == null)
            //{


            //}
            setGridData(GvDeleteEmpHolder, ViewState["data_delete_empholder"]);

            //GvMaster.Columns[8].Visible = true;

        }
        else
        {

            setGridData(GvDeleteEmpHolder, ViewState["data_delete_empholder"]);
            //setError(_data_googlelicenseshowdelete.return_code.ToString() + " - " + _data_googlelicenseshowdelete.return_msg);
        }

    }

    protected void ddlOrganizationSearch()
    {
        //FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        //Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");
        DropDownList ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");

        ddlorg_idxsearch.Items.Clear();
        ddlorg_idxsearch.AppendDataBoundItems = true;
        ddlorg_idxsearch.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgsearch = new data_softwarelicense();
        _data_softwarelicense_orgsearch.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgsearch.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgsearch = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgsearch);

        ddlorg_idxsearch.DataSource = _data_softwarelicense_orgsearch.organization_list;
        ddlorg_idxsearch.DataTextField = "OrgNameTH";
        ddlorg_idxsearch.DataValueField = "OrgIDX";
        ddlorg_idxsearch.DataBind();


    }

    protected void ddlOrganizationReport()
    {

        DropDownList ddlorg_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlorg_idx_report");

        ddlorg_idx_report.Items.Clear();
        ddlorg_idx_report.AppendDataBoundItems = true;
        ddlorg_idx_report.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgreport = new data_softwarelicense();
        _data_softwarelicense_orgreport.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgreport.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgreport = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgreport);

        ddlorg_idx_report.DataSource = _data_softwarelicense_orgreport.organization_list;
        ddlorg_idx_report.DataTextField = "OrgNameTH";
        ddlorg_idx_report.DataValueField = "OrgIDX";
        ddlorg_idx_report.DataBind();


    }

    protected void SelectLogDeleteGoogle()
    {

        ////ViewState["m0_idx_delete"] = m0_idx_delete;
        ////ViewState["org_idx_delete"] = org_idx_delete;
        ////ViewState["rdept_idx_delete"] = rdept_idx_delete;
        ////ViewState["u0_idx_delete"] = u0_idx_delete;

        data_googlelicense _data_googlelicenselog_delete = new data_googlelicense();
        _data_googlelicenselog_delete.log_delete_holder_ggl_list = new log_delete_holder_ggl_detail[1];

        log_delete_holder_ggl_detail _log_delete_holder_ggl_detail = new log_delete_holder_ggl_detail();

        _log_delete_holder_ggl_detail.m0_idx = int.Parse(ViewState["m0_idx_delete"].ToString());

        _data_googlelicenselog_delete.log_delete_holder_ggl_list[0] = _log_delete_holder_ggl_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_googlelicenselog_delete = callServiceGoogleLicense(_urlGetLogDeleteHolderGGL, _data_googlelicenselog_delete);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogDeleteHolderGooglelicense.DataSource = _data_googlelicenselog_delete.log_delete_holder_ggl_list;
        rptLogDeleteHolderGooglelicense.DataBind();



    }

    #endregion Selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //int _category_idx;
        //string _category_name;
        int _cemp_idx;

        // *** value insert ***///
        // *** value insert ***///

        m0_ggldataset_detail _m0_ggldataset_detail = new m0_ggldataset_detail();
        u0_gglbuy_detail _u0_gglbuy_detail = new u0_gglbuy_detail();
        m1_gglholderemail_detail _m1_gglholderemail_detail = new m1_gglholderemail_detail();
        m0_gglholderemail_detail _m0_gglholderemail_detail = new m0_gglholderemail_detail();
        ggltranfer_detail _ggltranfer_detail = new ggltranfer_detail();
        del_holdergmail_detail _del_holdergmail_detail = new del_holdergmail_detail();
        bind_gglsearch_detail _bind_gglsearch_detail = new bind_gglsearch_detail();
        bindreport_ggl_detail _bindreport_ggl_detail = new bindreport_ggl_detail();



        switch (cmdName)
        {
            case "btnToIndexList":

                btnToIndexList.BackColor = System.Drawing.Color.LightGray;
                btnToGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnDetailGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewIndex);

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                setOntop.Focus();

                break;

            case "btnToGoogleLicense":


                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToGoogleLicense.BackColor = System.Drawing.Color.LightGray;
                btnDetailGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewGoogleInsert);

                FvInsert.ChangeMode(FormViewMode.Insert);
                FvInsert.DataBind();

                ddlTypeBuyGoogle();
                //ddlSoftwarenameGoogle();
                ddlCompanyGoogle();
                ddlOrganizationGoogle();
                ddlLotGoogle();
                //ddlTypeHolder();


                break;

            case "btnDetailGoogleLicense":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnDetailGoogleLicense.BackColor = System.Drawing.Color.LightGray;
                btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewDetailHolderGmail);


                break;

            case "btnToReport":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnDetailGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.LightGray;

                MvMaster.SetActiveView(ViewGoogleReport);

                ddlOrganizationReport();

                data_googlelicense _data_googlelicense_report = new data_googlelicense();
                _data_googlelicense_report.bindreport_ggl_list = new bindreport_ggl_detail[1];

                _data_googlelicense_report.bindreport_ggl_list[0] = _bindreport_ggl_detail;

                // _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_search));dddd

                _data_googlelicense_report = callServiceGoogleLicense(_urlSetBindReportGooglelicense, _data_googlelicense_report);

                ViewState["bind_data_report"] = _data_googlelicense_report.bindreport_ggl_list;

                gridviewreport.Visible = true;
                setGridData(GvReport, ViewState["bind_data_report"]);


                break;

            case "btnSearch":

                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;

                _setHeader_Filter = 1;
                ViewState["Vs_SetHeader_Filter"] = 1;
                actionIndex();

                break;

            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;

                show_insert_empholderggl.Visible = false;
                gridview_showdetail_holder.Visible = false;
                Show_InsertHolder.Visible = false;

                div_saveInsertHolder.Visible = false;
                div_showdelete_holder.Visible = false;
                panel_delete_googlelicense.Visible = false;
                div_show_managedel_emp.Visible = false;
                show_data_beforetranfer.Visible = false;
                div_tranfer_googlelicense.Visible = false;
                div_save_tranfer.Visible = false;


                btnSearch.Visible = true;
                btnResetSearchPage.Visible = true;
                gridviewindex.Visible = true;
                GvMaster.Visible = true;

                //if (ViewState["bind_data_index"] != null)
                //{
                //    GvMaster.PageIndex = 0;
                //    setGridData(GvMaster, ViewState["bind_data_index"]);
                //}
                //else
                //{
                //    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //}

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnCancelIndex_First":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSearchIndex_First":

                DropDownList _ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");
                DropDownList _ddlrdept_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrdept_idxsearch");
                DropDownList _ddlrsec_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrsec_idxsearch");
                DropDownList _ddlemp_idxsearch = (DropDownList)ViewIndex.FindControl("ddlemp_idxsearch");
                TextBox _txtcostcenter_no_search = (TextBox)ViewIndex.FindControl("txtcostcenter_no");
                TextBox _txtname_email_search = (TextBox)ViewIndex.FindControl("txtname_email");

                _cemp_idx = emp_idx;

                data_googlelicense _data_googlelicense_search = new data_googlelicense();
                _data_googlelicense_search.bind_gglsearch_list = new bind_gglsearch_detail[1];

                _bind_gglsearch_detail.org_idx = int.Parse(_ddlorg_idxsearch.SelectedValue);
                _bind_gglsearch_detail.rdept_idx = int.Parse(_ddlrdept_idxsearch.SelectedValue);
                _bind_gglsearch_detail.rsec_idx = int.Parse(_ddlrsec_idxsearch.SelectedValue);
                _bind_gglsearch_detail.emp_idx = int.Parse(_ddlemp_idxsearch.SelectedValue);
                _bind_gglsearch_detail.costcenter_no = _txtcostcenter_no_search.Text;
                _bind_gglsearch_detail.email = _txtname_email_search.Text;
                _bind_gglsearch_detail.cemp_idx = _cemp_idx;

                _data_googlelicense_search.bind_gglsearch_list[0] = _bind_gglsearch_detail;

                // _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_search));dddd

                _data_googlelicense_search = callServiceGoogleLicense(_urlGetSearchIndexGooglelicense, _data_googlelicense_search);

                ViewState["bind_data_index"] = _data_googlelicense_search.bind_gglsearch_list;

                GvMaster.PageIndex = 0;
                ViewState["Vs_SetHeader_Filter"] = 1;
                setGridData(GvMaster, ViewState["bind_data_index"]);


                break;

            case "btnInsertDetail":

                FormView FvInsert_insert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
                DropDownList _ddlsoftware_name_idxinsert = (DropDownList)FvInsert_insert.FindControl("ddlsoftware_name_idxinsert");


                Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");

                DropDownList _ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                DropDownList _ddlrdept_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlrdept_idxinsert");

                TextBox _txtcostcenter_cost = (TextBox)PaneldetailLicense.FindControl("txtcostcenter_cost"); // เลขที่ costcenter
                TextBox _txtnumlicense_cost = (TextBox)PaneldetailLicense.FindControl("txtnumlicense_cost");

                Panel gidviewdetaillicense = (Panel)PaneldetailLicense.FindControl("gidviewdetaillicense");
                GridView GVShowdetailLicense = (GridView)gidviewdetaillicense.FindControl("GVShowdetailLicense");


                //
                // _txtnumlicense_dept.Text = "1";

                //DropDownList _ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");

                //////  create data set show detail license  ////////

                var dsEquiment = (DataSet)ViewState["vsBuyequipment"];
                var drEquiment = dsEquiment.Tables[0].NewRow();

                int numrow = dsEquiment.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlorg_idxinsert_check"] = _ddlorg_idxinsert.SelectedValue;
                ViewState["_ddlrdept_idxinsert_check"] = _ddlrdept_idxinsert.SelectedValue;
                ViewState["_costcenter_check"] = _txtcostcenter_cost.Text;

                if (numrow > 0)
                {
                    foreach (DataRow check in dsEquiment.Tables[0].Rows)
                    {
                        ViewState["check_org"] = check["OrgIDX_dataset"];
                        ViewState["check_dept"] = check["RDeptIDX_dataset"];

                        ViewState["check_costcenter"] = check["Costcenter_dataset"];

                        if ((int.Parse(ViewState["_ddlrdept_idxinsert_check"].ToString()) == int.Parse(ViewState["check_dept"].ToString())) || (ViewState["_costcenter_check"].ToString() == ViewState["check_costcenter"].ToString()))
                        {
                            ViewState["CheckDataset"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset"] = "1";

                        }
                    }

                    if (ViewState["CheckDataset"].ToString() == "1")
                    {
                        //drEquiment["Software_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedItem;
                        //drEquiment["Softwareidx_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedValue;

                        drEquiment["OrgNameTH_dataset"] = _ddlorg_idxinsert.SelectedItem;
                        drEquiment["OrgIDX_dataset"] = _ddlorg_idxinsert.SelectedValue;

                        drEquiment["DeptNameTH_dataset"] = _ddlrdept_idxinsert.SelectedItem;
                        drEquiment["RDeptIDX_dataset"] = _ddlrdept_idxinsert.SelectedValue;

                        drEquiment["Costcenter_dataset"] = _txtcostcenter_cost.Text;

                        drEquiment["numlicense_dataset"] = int.Parse(_txtnumlicense_cost.Text);

                        dsEquiment.Tables[0].Rows.Add(drEquiment);
                        ViewState["vsBuyequipment"] = dsEquiment;

                        //////  create data set show detail license  ////////


                        gidviewdetaillicense.Visible = true;

                        GVShowdetailLicense.DataSource = dsEquiment.Tables[0];
                        GVShowdetailLicense.DataBind();

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                        //break;
                    }

                }
                else
                {
                    //drEquiment["Software_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedItem;
                    //drEquiment["Softwareidx_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedValue;

                    drEquiment["OrgNameTH_dataset"] = _ddlorg_idxinsert.SelectedItem;
                    drEquiment["OrgIDX_dataset"] = _ddlorg_idxinsert.SelectedValue;

                    drEquiment["DeptNameTH_dataset"] = _ddlrdept_idxinsert.SelectedItem;
                    drEquiment["RDeptIDX_dataset"] = _ddlrdept_idxinsert.SelectedValue;

                    drEquiment["Costcenter_dataset"] = _txtcostcenter_cost.Text;

                    drEquiment["numlicense_dataset"] = int.Parse(_txtnumlicense_cost.Text);

                    dsEquiment.Tables[0].Rows.Add(drEquiment);
                    ViewState["vsBuyequipment"] = dsEquiment;

                    //////  create data set show detail license  ////////


                    gidviewdetaillicense.Visible = true;

                    GVShowdetailLicense.DataSource = dsEquiment.Tables[0];
                    GVShowdetailLicense.DataBind();

                }

                break;

            case "btnInsert":

                FormView FvInsert_insert_google = (FormView)ViewGoogleInsert.FindControl("FvInsert");

                //insert ด้านบน
                DropDownList _ddltype_idxinsertadd = (DropDownList)FvInsert_insert_google.FindControl("ddltype_idxinsert");
                TextBox _txtcount_licenseinsertadd = (TextBox)FvInsert_insert_google.FindControl("txtcount_licenseinsert"); //จำนวน License ทั้งหมด
                //DropDownList _ddlsoftware_name_idxinsertadd = (DropDownList)FvInsert_insert_google.FindControl("ddlsoftware_name_idxinsert");
                DropDownList _ddlcompany_idxinsertadd = (DropDownList)FvInsert_insert_google.FindControl("ddlcompany_idxinsert");
                TextBox _txtdatepurchaseinsertadd = (TextBox)FvInsert_insert_google.FindControl("txtdatepurchaseinsert");
                TextBox _txtdateexpireinsertadd = (TextBox)FvInsert_insert_google.FindControl("txtdateexpireinsert");
                TextBox _txtprice_insertadd = (TextBox)FvInsert_insert_google.FindControl("txtprice_insert");
                TextBox _txtcount_licenseinsert = (TextBox)FvInsert_insert_google.FindControl("txtcount_licenseinsert");
                TextBox _txtpo_code = (TextBox)FvInsert_insert_google.FindControl("txtpo_code");

                Panel lot_codebuy = (Panel)FvInsert_insert_google.FindControl("lot_codebuy");
                TextBox _txtlblot_codebuy = (TextBox)lot_codebuy.FindControl("txtlblot_codebuy");

                Panel lot_dateexpire = (Panel)FvInsert_insert_google.FindControl("lot_dateexpire");
                DropDownList _ddllblot_code = (DropDownList)lot_dateexpire.FindControl("ddllblot_code");

                Panel showdetailpermissionsystem = (Panel)FvInsert_insert_google.FindControl("showdetailpermissionsystem");

                CheckBox chk_licenseadd = (CheckBox)FvInsert_insert_google.FindControl("chk_license");
                //insert ด้านบน

                //in ด้านล่าง
                Panel PaneldetailLicense_dataset = (Panel)FvInsert_insert_google.FindControl("PaneldetailLicense");
                TextBox Count_numin_colum = (TextBox)PaneldetailLicense_dataset.FindControl("Count_numin_colum");


                //in ด้านล่าง

                //CheckText.Visible = false;
                showdetailpermissionsystem.Visible = false;

                if (_ddltype_idxinsertadd.SelectedValue == "1") // กรณีซื้อใหม่
                {
                    var dsBuyequipment = (DataSet)ViewState["vsBuyequipment"];

                    var m0_ggldataset = new m0_ggldataset_detail[dsBuyequipment.Tables[0].Rows.Count];

                    var SumTotalLinse = dsBuyequipment.Tables[0].Rows.Count;

                    // Check จำนวน License ตอน insert
                    int i = 0;
                    foreach (DataRow dr in dsBuyequipment.Tables[0].Rows)
                    {

                        //AddPur1[i] = new SetADDFormList();
                        //AddPur1[i].R0_Name = dr["topic_name_id"].ToString();
                        i += int.Parse(dr["numlicense_dataset"].ToString());
                        //i += int.Parse(dr["numlicense_dataset"].ToString());
                        //AddPur1[i].R0_Status = int.Parse(dr["name_id"].ToString());

                    }

                    Count_numin_colum.Text = i.ToString(); // จำนวนรวมของ License ที่เพิ่มตรง dataset

                    //Check องค์กร ฝ่าย ตอนแบ่ง License
                    int j = 0;
                    foreach (DataRow drw in dsBuyequipment.Tables[0].Rows)
                    {


                        m0_ggldataset[j] = new m0_ggldataset_detail();
                        m0_ggldataset[j].org_idx = int.Parse(drw["OrgIDX_dataset"].ToString());
                        m0_ggldataset[j].rdept_idx = int.Parse(drw["RDeptIDX_dataset"].ToString());
                        m0_ggldataset[j].costcenter_no = drw["Costcenter_dataset"].ToString();
                        m0_ggldataset[j].num_license = int.Parse(drw["numlicense_dataset"].ToString());



                        j++;

                    }

                    if (chk_licenseadd.Checked)
                    {


                        ////// Check License ว่า ใส่ครบไหม  ///////
                        if (_txtcount_licenseinsertadd.Text == Count_numin_colum.Text)
                        {
                            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ok!!!');", true);


                            //_company_name = ((TextBox)ViewInsert.FindControl("txtCompanyName")).Text.Trim();
                            // DropDownList ddlCompanyStatus = (DropDownList)ViewInsert.FindControl("ddlCompanyStatus");
                            _cemp_idx = emp_idx;

                            data_googlelicense _data_googlelicense_buy = new data_googlelicense();
                            _data_googlelicense_buy.u0_gglbuy_list = new u0_gglbuy_detail[1];


                            _u0_gglbuy_detail.type_idx = int.Parse(_ddltype_idxinsertadd.SelectedValue);

                            _u0_gglbuy_detail.po_code = _txtpo_code.Text;

                            if (_ddltype_idxinsertadd.SelectedValue == "2")
                            {
                                _u0_gglbuy_detail.lot_codebuy = _ddllblot_code.SelectedItem.ToString();
                            }
                            else
                            {
                                _u0_gglbuy_detail.lot_codebuy = "-";
                            }


                            //_u0_gglbuy_detail.software_name_idx = int.Parse(_ddlsoftware_name_idxinsertadd.SelectedValue);
                            _u0_gglbuy_detail.company_idx = int.Parse(_ddlcompany_idxinsertadd.SelectedValue);
                            _u0_gglbuy_detail.date_purchase = _txtdatepurchaseinsertadd.Text;
                            _u0_gglbuy_detail.date_expire = _txtdateexpireinsertadd.Text;
                            _u0_gglbuy_detail.price = int.Parse(_txtprice_insertadd.Text);
                            _u0_gglbuy_detail.count_license = int.Parse(_txtcount_licenseinsertadd.Text);
                            _u0_gglbuy_detail.cemp_idx = _cemp_idx;

                            _data_googlelicense_buy.u0_gglbuy_list[0] = _u0_gglbuy_detail;

                            _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                            //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_buy));

                            _data_googlelicense_buy = callServiceGoogleLicense(_urlSetGooglelicense, _data_googlelicense_buy);


                            if (_data_googlelicense_buy.return_code == 0)
                            {

                                ////actionIndex();
                                //MvMaster.SetActiveView(ViewIndex);
                                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                            }
                            else
                            {
                                setError(_data_googlelicense_buy.return_code.ToString() + " - " + _data_googlelicense_buy.return_msg);
                            }

                        }
                        else if (int.Parse(Count_numin_colum.Text) > int.Parse(_txtcount_licenseinsertadd.Text))
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งเกิน จำนวน License ทั้งหมดที่มี!!!');", true);
                        }
                        else if (int.Parse(Count_numin_colum.Text) < int.Parse(_txtcount_licenseinsertadd.Text))
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('จำนวน License ที่แบ่งน้อยกว่า จำนวน License ทั้งหมดที่มี!!!');", true);
                        }
                        ////// Check License ว่า ใส่ครบไหม  ///////
                        //}
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาแบ่ง Lisense ตามเลขที่ Costcenter!!!');", true);

                        showdetailpermissionsystem.Visible = true;
                        //CheckText.Visible = true;

                    }
                }
                else if (_ddltype_idxinsertadd.SelectedValue == "2") //กรณีต่ออายุ
                {

                    _cemp_idx = emp_idx;

                    data_googlelicense _data_googlelicense_insertexpire = new data_googlelicense();
                    _data_googlelicense_insertexpire.u0_gglbuy_list = new u0_gglbuy_detail[1];


                    _u0_gglbuy_detail.type_idx = int.Parse(_ddltype_idxinsertadd.SelectedValue);
                    _u0_gglbuy_detail.po_code = _txtpo_code.Text;
                    _u0_gglbuy_detail.u0_buy_idx = int.Parse(_ddllblot_code.SelectedValue);
                    _u0_gglbuy_detail.lot_code = _ddllblot_code.SelectedItem.ToString();
                    _u0_gglbuy_detail.company_idx = int.Parse(_ddlcompany_idxinsertadd.SelectedValue);
                    _u0_gglbuy_detail.date_purchase = _txtdatepurchaseinsertadd.Text;
                    _u0_gglbuy_detail.date_expire = _txtdateexpireinsertadd.Text;
                    _u0_gglbuy_detail.price = int.Parse(_txtprice_insertadd.Text);
                    _u0_gglbuy_detail.count_license = int.Parse(_txtcount_licenseinsertadd.Text);
                    _u0_gglbuy_detail.cemp_idx = _cemp_idx;

                    _data_googlelicense_insertexpire.u0_gglbuy_list[0] = _u0_gglbuy_detail;

                    // _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_insertexpire));

                    _data_googlelicense_insertexpire = callServiceGoogleLicense(_urlSetGooglelicense, _data_googlelicense_insertexpire);

                    if (_data_googlelicense_insertexpire.return_code == 0)
                    {

                        ////actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);


                    }
                    else
                    {
                        setError(_data_googlelicense_insertexpire.return_code.ToString() + " - " + _data_googlelicense_insertexpire.return_msg);
                    }

                }


                break;

            case "btnCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsertemp":

                showsearch.Visible = false;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;
                gridviewindex.Visible = false;

                string[] arg1 = new string[3];
                arg1 = e.CommandArgument.ToString().Split(';');
                int m0_idx_buy = int.Parse(arg1[0]);
                int org_idx_buy = int.Parse(arg1[1]);
                int rdept_idx_buy = int.Parse(arg1[2]);
                int u0_idx_buy = int.Parse(arg1[3]);

                ViewState["m0_idx_buy"] = m0_idx_buy;
                ViewState["org_idx_buy"] = org_idx_buy;
                ViewState["rdept_idx_buy"] = rdept_idx_buy;
                ViewState["u0_idx_buy"] = u0_idx_buy;

                fvBacktoIndex.Visible = true;
                show_insert_empholderggl.Visible = true;

                SelectDetailInsert();

                Show_InsertHolder.Visible = true;

                //FvInsert_HolderLicense.ChangeMode(FormViewMode.Insert);
                //FvInsert_HolderLicense.DataBind();

                data_googlelicense _data_googlelicenseinserthd = new data_googlelicense();
                _data_googlelicenseinserthd.bindInsert_gglbuy_list = new bindInsert_gglbuy_detail[1];

                bindInsert_gglbuy_detail _bindInsert_gglbuy_detailhd = new bindInsert_gglbuy_detail();

                _bindInsert_gglbuy_detailhd.m0_idx = int.Parse(ViewState["m0_idx_buy"].ToString());
                _bindInsert_gglbuy_detailhd.org_idx = int.Parse(ViewState["org_idx_buy"].ToString());
                _bindInsert_gglbuy_detailhd.rdept_idx = int.Parse(ViewState["rdept_idx_buy"].ToString());
                _bindInsert_gglbuy_detailhd.u0_buy_idx = int.Parse(ViewState["u0_idx_buy"].ToString());

                _data_googlelicenseinserthd.bindInsert_gglbuy_list[0] = _bindInsert_gglbuy_detailhd;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseview));

                _data_googlelicenseinserthd = callServiceGoogleLicense(_urlGetSetGooglelicenseHD, _data_googlelicenseinserthd);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseinserthd));

                setFormData(FvInsert_HolderLicense, FormViewMode.ReadOnly, _data_googlelicenseinserthd.bindInsert_gglbuy_list);

                FormView FvInsert_HolderLicense1 = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                TextBox txtname_email = (TextBox)FvInsert_HolderLicense1.FindControl("txtname_email");
                TextBox txtstorage_insert = (TextBox)FvInsert_HolderLicense1.FindControl("txtstorage_insert");
                DropDownList ddl_type_email_checkholder = (DropDownList)FvInsert_HolderLicense1.FindControl("ddl_type_email");

                FormView FvViewDetail_ = (FormView)ViewIndex.FindControl("FvViewDetail");
                Label lbl_m0type_email_idxview = (Label)FvViewDetail_.FindControl("lbl_m0type_email_idxview");


                ViewState["name_email"] = _data_googlelicenseinserthd.bindInsert_gglbuy_list[0].name_license_gmail;

                if (ViewState["name_email"].ToString() == "")
                {
                    txtname_email.Enabled = true;
                    //txtname_email.Text = "222";
                }
                else
                {
                    txtname_email.Enabled = false;
                    txtname_email.Text = _data_googlelicenseinserthd.bindInsert_gglbuy_list[0].name_license_gmail;
                }


                txtstorage_insert.Text = "30"; //storage defult


                ddlSecInsertHolder();

                //div_saveInsertHolder.Visible = true;

                gridview_showdetail_holder.Visible = true;
                div_showholdergmail.Visible = true;
                div_ggl_licenseholder.Visible = true;

                SelectDetailHolderGmail();


                //litDebug.Text = ViewState["vs_Count_Holder_Check"].ToString();
                //getTypeEmail(ddl_type_email_checkholder, 1);
                if (int.Parse(ViewState["vs_Count_Holder_Check"].ToString()) > 1) //mail all
                {
                    getTypeEmail(ddl_type_email_checkholder, 0);
                    ddl_type_email_checkholder.Items[2].Enabled = false;
                    //ddl_type_email_checkholder.SelectedValue.Enabled = "1";
                }
                else if ((int.Parse(ViewState["vs_Count_Holder_Check"].ToString()) == 1 || int.Parse(ViewState["vs_Count_Holder_Check"].ToString()) == 0) && int.Parse(lbl_m0type_email_idxview.Text) == 2)
                {
                    getTypeEmail(ddl_type_email_checkholder, 0);
                    ddl_type_email_checkholder.Items[1].Enabled = false;

                    //ddl_type_email_checkholder.Items[int.Parse(lbl_m0type_email_idxview.Text)].Enabled = false;
                }
                else if ((int.Parse(ViewState["vs_Count_Holder_Check"].ToString()) == 1 || int.Parse(ViewState["vs_Count_Holder_Check"].ToString()) == 0) && int.Parse(lbl_m0type_email_idxview.Text) == 1)
                {
                    getTypeEmail(ddl_type_email_checkholder, 0);
                    ddl_type_email_checkholder.Items[2].Enabled = false;
                    //ddl_type_email_checkholder.SelectedValue = "2";
                }
                else
                {
                    getTypeEmail(ddl_type_email_checkholder, 0);
                }
                


                setOntop.Focus();

                break;

            case "btnEditGoogle":

                FormView FvViewDetail_edit = (FormView)ViewIndex.FindControl("FvViewDetail");

                showsearch.Visible = false;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;
                gridviewindex.Visible = false;
                div_saveInsertHolder.Visible = false;

                string[] arg4 = new string[5];
                arg4 = e.CommandArgument.ToString().Split(';');
                int m0_idx_edit = int.Parse(arg4[0]);
                int org_idx_edit = int.Parse(arg4[1]);
                int rdept_idx_edit = int.Parse(arg4[2]);
                int u0_idx_edit = int.Parse(arg4[3]);
                int Count_EmpGoogleLicnese_edit = int.Parse(arg4[4]);
                string name_license_gmail_edit = arg4[5];

                ViewState["m0_idx_edit"] = m0_idx_edit;
                ViewState["org_idx_edit"] = org_idx_edit;
                ViewState["rdept_idx_edit"] = rdept_idx_edit;
                ViewState["u0_idx_edit"] = u0_idx_edit;
                ViewState["Count_EmpGoogleLicnese_edit"] = Count_EmpGoogleLicnese_edit;
                ViewState["name_license_gmail_edit"] = name_license_gmail_edit;

                fvBacktoIndex.Visible = true;
                show_insert_empholderggl.Visible = true;

                data_googlelicense _data_googlelicenseview_esit = new data_googlelicense();
                _data_googlelicenseview_esit.bind_gglbuy_list = new bind_gglbuy_detail[1];

                bind_gglbuy_detail _bind_gglbuy_detailview_edit = new bind_gglbuy_detail();

                _bind_gglbuy_detailview_edit.m0_idx = int.Parse(ViewState["m0_idx_edit"].ToString());
                _bind_gglbuy_detailview_edit.org_idx = int.Parse(ViewState["org_idx_edit"].ToString());
                _bind_gglbuy_detailview_edit.rdept_idx = int.Parse(ViewState["rdept_idx_edit"].ToString());
                _bind_gglbuy_detailview_edit.u0_buy_idx = int.Parse(ViewState["u0_idx_edit"].ToString());

                _data_googlelicenseview_esit.bind_gglbuy_list[0] = _bind_gglbuy_detailview_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseview));

                _data_googlelicenseview_esit = callServiceGoogleLicense(_urlGetGooglelicenseDetail, _data_googlelicenseview_esit);

                setFormData(FvViewDetail_edit, FormViewMode.Edit, _data_googlelicenseview_esit.bind_gglbuy_list);

                TextBox _txtname_license_gmail = (TextBox)FvViewDetail_edit.FindControl("txtname_license_gmail");
                TextBox _txtstorage_gmail = (TextBox)FvViewDetail_edit.FindControl("txtstorage_gmail");


                if ((int.Parse(ViewState["Count_EmpGoogleLicnese_edit"].ToString())) == 0 && (ViewState["name_license_gmail_edit"].ToString() != ""))
                {
                    _txtname_license_gmail.Enabled = true;
                    _txtstorage_gmail.Enabled = false;

                }
                else if ((int.Parse(ViewState["Count_EmpGoogleLicnese_edit"].ToString())) == 0 && (ViewState["name_license_gmail_edit"].ToString() == ""))
                {
                    _txtname_license_gmail.Enabled = false;
                    _txtstorage_gmail.Enabled = false;
                }
                else
                {
                    _txtname_license_gmail.Enabled = false;
                    _txtstorage_gmail.Enabled = true;
                    //_txtstorage_gmail_view.Enabled = true;
                }

                setOntop.Focus();
                break;

            case "btnCancelEditEmail":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSaveEditEmail":

                FormView FvViewDetail_save_edit = (FormView)ViewIndex.FindControl("FvViewDetail");
                TextBox txtname_license_gmail_old = (TextBox)FvViewDetail_save_edit.FindControl("txtname_license_gmail_old");

                TextBox txtname_license_gmail = (TextBox)FvViewDetail_save_edit.FindControl("txtname_license_gmail");


                TextBox txtStoragegmail_old = (TextBox)FvViewDetail_save_edit.FindControl("txtStoragegmail_old");
                TextBox txtstorage_gmail = (TextBox)FvViewDetail_save_edit.FindControl("txtstorage_gmail");



                data_googlelicense _data_googlelicense_savemailedit = new data_googlelicense();
                _data_googlelicense_savemailedit.bind_gglbuy_list = new bind_gglbuy_detail[1];

                bind_gglbuy_detail _bind_gglbuy_detail_saveedit = new bind_gglbuy_detail();

                _bind_gglbuy_detail_saveedit.m0_idx = int.Parse(ViewState["m0_idx_edit"].ToString());
                _bind_gglbuy_detail_saveedit.name_license_gmail = txtname_license_gmail.Text;
                _bind_gglbuy_detail_saveedit.storage = int.Parse(txtstorage_gmail.Text);
                //_bind_gglbuy_detail_saveedit.u0_buy_idx = int.Parse(ViewState["u0_idx_edit"].ToString());

                _data_googlelicense_savemailedit.bind_gglbuy_list[0] = _bind_gglbuy_detail_saveedit;

                if ((txtname_license_gmail_old.Text == txtname_license_gmail.Text) && (txtStoragegmail_old.Text == txtstorage_gmail.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลเหมือนเดิม!!! กรุณาเปลี่ยนแปลงข้อมูลก่อนทำการบันทึก');", true);
                }
                //else if((txtname_license_gmail_old.Text != txtname_license_gmail.Text) && (txtStoragegmail_old.Text == txtstorage_gmail.Text))
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลเหมือนเดิม!!! กรุณาเปลี่ยนแปลงข้อมูลก่อนทำการบันทึก');", true);
                //}
                else
                {
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_savemailedit));
                    _data_googlelicense_savemailedit = callServiceGoogleLicense(_urlSetGooglEditEmail, _data_googlelicense_savemailedit);

                    if (_data_googlelicense_savemailedit.return_code == 0)
                    {

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);

                    }
                    else
                    {

                        setError(_data_googlelicense_savemailedit.return_code.ToString() + " - " + _data_googlelicense_savemailedit.return_msg);

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีผู้ถือครองนี้อยู่ในระบบแล้ว!! กรุณาเลือกผู้ถือครองใหม่');", true);
                        //break;

                    }


                }





                break;

            case "btnCancelHolder":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsertHolder":

                FormView FvInsert_HolderLicenseemail = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                TextBox _txtorg_idxemail = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtorg_idxemail");
                TextBox _txtOrgNameTH_inlicenseinsert = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtOrgNameTH_inlicenseinsert");
                TextBox _txtrdept_idxemail = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtrdept_idxemail");
                TextBox _txtDeptNameTH_inlicense = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtDeptNameTH_inlicense");
                DropDownList _ddlrsec_idxinsert = (DropDownList)FvInsert_HolderLicenseemail.FindControl("ddlrsec_idxinsert");
                DropDownList _ddlemp_idxinsert = (DropDownList)FvInsert_HolderLicenseemail.FindControl("ddlemp_idxinsert");
                TextBox _txtname_email = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtname_email");

                TextBox _txtstorage_insert = (TextBox)FvInsert_HolderLicenseemail.FindControl("txtstorage_insert");

                Panel gvmail_holder = (Panel)FvInsert_HolderLicenseemail.FindControl("gvmail_holder");
                GridView GVShowdetailmailholder = (GridView)gvmail_holder.FindControl("GVShowdetailmailholder");

                // _txtnumlicense_dept.Text = "1";

                //DropDownList _ddlcompanyidxma = (DropDownList)FvViewMA.FindControl("ddlcompanyidxma");

                //////  create data set show detail license  ////////

                var dsEquiment_holdermail = (DataSet)ViewState["vsBuyequipmentholdermail"];
                var drEquiment_holdermail = dsEquiment_holdermail.Tables[0].NewRow();

                int numrow_holdermail = dsEquiment_holdermail.Tables[0].Rows.Count; //จำนวนแถว

                ViewState["_ddlemp_idxinsert_check"] = _ddlemp_idxinsert.SelectedValue;

                //ViewState["_ddlorg_idxinsert_check"] = _ddlorg_idxinsert.SelectedValue;
                //ViewState["_ddlrdept_idxinsert_check"] = _ddlrdept_idxinsert.SelectedValue;
                //ViewState["_costcenter_check"] = _txtcostcenter_cost.Text;

                if (numrow_holdermail > 0)
                {
                    foreach (DataRow check_holdermail in dsEquiment_holdermail.Tables[0].Rows)
                    {
                        ViewState["check_ddlemp_idxinsert"] = check_holdermail["emp_idx_emailholder"];
                        //ViewState["check_dept"] = check_holdermail["RDeptIDX_dataset"];

                        //ViewState["check_costcenter"] = check_holdermail["Costcenter_dataset"];


                        if ((int.Parse(ViewState["_ddlemp_idxinsert_check"].ToString()) == int.Parse(ViewState["check_ddlemp_idxinsert"].ToString())))
                        {
                            ViewState["CheckDataset_holder"] = "0";
                            break;

                        }
                        else
                        {
                            ViewState["CheckDataset_holder"] = "1";

                        }
                    }

                    if (ViewState["CheckDataset_holder"].ToString() == "1")
                    {
                        //drEquiment["Software_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedItem;
                        //drEquiment["Softwareidx_License_dataset"] = _ddlsoftware_name_idxinsert.SelectedValue;

                        drEquiment_holdermail["org_idx_emailholger"] = int.Parse(_txtorg_idxemail.Text);
                        drEquiment_holdermail["orgname_th_emailholder"] = _txtOrgNameTH_inlicenseinsert.Text;

                        drEquiment_holdermail["rdept_idx_emailholder"] = int.Parse(_txtrdept_idxemail.Text);
                        drEquiment_holdermail["rdeptname_th_emailholder"] = _txtDeptNameTH_inlicense.Text;

                        drEquiment_holdermail["rsec_idx_emailholder"] = _ddlrsec_idxinsert.SelectedValue;
                        drEquiment_holdermail["rsecname_th_emailholder"] = _ddlrsec_idxinsert.SelectedItem;

                        drEquiment_holdermail["emp_idx_emailholder"] = _ddlemp_idxinsert.SelectedValue;
                        drEquiment_holdermail["empnameth_emailholder"] = _ddlemp_idxinsert.SelectedItem;

                        drEquiment_holdermail["emailname_license"] = _txtname_email.Text;


                        dsEquiment_holdermail.Tables[0].Rows.Add(drEquiment_holdermail);
                        ViewState["vsBuyequipmentholdermail"] = dsEquiment_holdermail;

                        //////  create data set show detail license  ////////


                        gvmail_holder.Visible = true;

                        GVShowdetailmailholder.DataSource = dsEquiment_holdermail.Tables[0];
                        GVShowdetailmailholder.DataBind();

                        if (dsEquiment_holdermail.Tables[0].Rows.Count != 0)
                        {
                            _txtname_email.Enabled = false;
                        }
                        else if (dsEquiment_holdermail.Tables[0].Rows.Count == 0)
                        {
                            _txtname_email.Enabled = true;
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เพิ่มชื่อผู้ถือครองนี้ไปแล้ว!!!');", true);
                        //break;
                    }

                }
                else
                {

                    drEquiment_holdermail["org_idx_emailholger"] = int.Parse(_txtorg_idxemail.Text);
                    drEquiment_holdermail["orgname_th_emailholder"] = _txtOrgNameTH_inlicenseinsert.Text;

                    drEquiment_holdermail["rdept_idx_emailholder"] = int.Parse(_txtrdept_idxemail.Text);
                    drEquiment_holdermail["rdeptname_th_emailholder"] = _txtDeptNameTH_inlicense.Text;

                    drEquiment_holdermail["rsec_idx_emailholder"] = _ddlrsec_idxinsert.SelectedValue;
                    drEquiment_holdermail["rsecname_th_emailholder"] = _ddlrsec_idxinsert.SelectedItem;

                    drEquiment_holdermail["emp_idx_emailholder"] = _ddlemp_idxinsert.SelectedValue;
                    drEquiment_holdermail["empnameth_emailholder"] = _ddlemp_idxinsert.SelectedItem;

                    drEquiment_holdermail["emailname_license"] = _txtname_email.Text;


                    dsEquiment_holdermail.Tables[0].Rows.Add(drEquiment_holdermail);
                    ViewState["vsBuyequipmentholdermail"] = dsEquiment_holdermail;

                    //////  create data set show detail license  ////////


                    gvmail_holder.Visible = true;

                    GVShowdetailmailholder.DataSource = dsEquiment_holdermail.Tables[0];
                    GVShowdetailmailholder.DataBind();

                    //check Row in dataset 
                    if (dsEquiment_holdermail.Tables[0].Rows.Count != 0)
                    {
                        _txtname_email.Enabled = false;
                    }
                    else if (dsEquiment_holdermail.Tables[0].Rows.Count == 0)
                    {
                        _txtname_email.Enabled = true;
                    }

                }



                break;

            case "btnSaveHolderMail":

                //////////////
                FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
                TextBox txt_m0_idx_email = (TextBox)FvViewDetail.FindControl("txt_m0_idx_email");
                TextBox txt_u0_buy_idx_email = (TextBox)FvViewDetail.FindControl("txt_u0_buy_idx_email");

                ///////////////

                FormView FvInsert_HolderLicenseinsert = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                TextBox _txtname_email_insertemail = (TextBox)FvInsert_HolderLicenseinsert.FindControl("txtname_email");

                TextBox _txtstorage_insert_mail = (TextBox)FvInsert_HolderLicenseinsert.FindControl("txtstorage_insert");

                //type email
                DropDownList ddl_type_email_insert = (DropDownList)FvInsert_HolderLicenseinsert.FindControl("ddl_type_email");
                DropDownList ddlrsec_idxinsert_insert = (DropDownList)FvInsert_HolderLicenseinsert.FindControl("ddlrsec_idxinsert");
                DropDownList ddlemp_idxinsert_insert = (DropDownList)FvInsert_HolderLicenseinsert.FindControl("ddlemp_idxinsert");

                ///////////
                _cemp_idx = emp_idx;

                //string count_softwareidx = "";

                //ข้อมูลใน dataset
                var dsBuyequipment_insertmail = (DataSet)ViewState["vsBuyequipmentholdermail"];

                var m1_gglholderemail_detail_mailinsert = new m1_gglholderemail_detail[dsBuyequipment_insertmail.Tables[0].Rows.Count];

                //Check องค์กร ฝ่าย ตอนแบ่ง License
                int i_holder = 0;
                ////int i_software = 1;
                string software_email = string.Empty;
                foreach (DataRow drw in dsBuyequipment_insertmail.Tables[0].Rows)
                {

                    m1_gglholderemail_detail_mailinsert[i_holder] = new m1_gglholderemail_detail();

                    ////u1_softwarelicense_detail_dataset[dataset].software_idx = int.Parse(drw["software_idx_dataset"].ToString());
                    m1_gglholderemail_detail_mailinsert[i_holder].org_idx = int.Parse(drw["org_idx_emailholger"].ToString());
                    m1_gglholderemail_detail_mailinsert[i_holder].rdept_idx = int.Parse(drw["rdept_idx_emailholder"].ToString());
                    m1_gglholderemail_detail_mailinsert[i_holder].rsec_idx = int.Parse(drw["rsec_idx_emailholder"].ToString());
                    m1_gglholderemail_detail_mailinsert[i_holder].emp_idx = int.Parse(drw["emp_idx_emailholder"].ToString());
                    m1_gglholderemail_detail_mailinsert[i_holder].name_license_gmail = drw["emailname_license"].ToString();


                    ////software_email += string.Format("<tr><td>{0}. {1}</td></tr>", i_software++, drw["swlicense_name_dataset"].ToString());


                    i_holder++;

                }

                // check emp ว่าเพิ่มไปแล้วหรือยัง
                //int i = 0;
                string count_emp_check = "";

                string emp_idx_check = string.Empty;
                foreach (DataRow dr in dsBuyequipment_insertmail.Tables[0].Rows)
                {

                    count_emp_check += int.Parse(dr["emp_idx_emailholder"].ToString()) + ",";

                }

                emp_idx_check = count_emp_check;

                data_googlelicense _data_googlelicenseinsert_holder = new data_googlelicense();
                _data_googlelicenseinsert_holder.m0_gglholderemail_list = new m0_gglholderemail_detail[1];

                _m0_gglholderemail_detail.m0_idx = int.Parse(txt_m0_idx_email.Text);
                _m0_gglholderemail_detail.u0_buy_idx = int.Parse(txt_u0_buy_idx_email.Text);
                _m0_gglholderemail_detail.name_license_gmail = _txtname_email_insertemail.Text;
                if (emp_idx_check.ToString() != "")
                {
                    _m0_gglholderemail_detail.emp_idx_check = emp_idx_check;
                }
                else
                {
                    _m0_gglholderemail_detail.emp_idx_check = ddlemp_idxinsert_insert.SelectedValue;
                }

                _m0_gglholderemail_detail.storage = int.Parse(_txtstorage_insert_mail.Text);
                _m0_gglholderemail_detail.m0type_email_idx = int.Parse(ddl_type_email_insert.SelectedValue);
                _m0_gglholderemail_detail.rsec_idx = int.Parse(ddlrsec_idxinsert_insert.SelectedValue);
                _m0_gglholderemail_detail.emp_idx_holder = int.Parse(ddlemp_idxinsert_insert.SelectedValue);

                _m0_gglholderemail_detail.cemp_idx = _cemp_idx;
                _m0_gglholderemail_detail.m0_node_idx = 1;
                _m0_gglholderemail_detail.m0_actor_idx = 1;
                _m0_gglholderemail_detail.doc_decision = 0;


                _data_googlelicenseinsert_holder.m0_gglholderemail_list[0] = _m0_gglholderemail_detail;
                _data_googlelicenseinsert_holder.m1_gglholderemail_list = m1_gglholderemail_detail_mailinsert;

                // litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseinsert_holder));

                if (dsBuyequipment_insertmail.Tables[0].Rows.Count != 0)
                {
                    _data_googlelicenseinserthd = callServiceGoogleLicense(_urlSetGooglelicenseHDEmail, _data_googlelicenseinsert_holder);


                    if (_data_googlelicenseinserthd.return_code == 0)
                    {

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);

                    }
                    else
                    {

                        //setError(_data_googlelicenseinserthd.return_code.ToString() + " - " + _data_googlelicenseinserthd.return_msg);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลในระบบแล้ว');", true);
                        break;

                    }


                }
                else if (dsBuyequipment_insertmail.Tables[0].Rows.Count == 0 && ddl_type_email_insert.SelectedValue == "2" && _txtname_email_insertemail.Text != "" && ddlrsec_idxinsert_insert.SelectedValue != "00" && ddlemp_idxinsert_insert.SelectedValue != "00") //email ส่วนตัว
                {

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseinsert_holder));
                    _data_googlelicenseinserthd = callServiceGoogleLicense(_urlSetGooglelicenseHDEmail, _data_googlelicenseinsert_holder);
                    if (_data_googlelicenseinserthd.return_code == 0)
                    {

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);

                    }
                    else
                    {

                        //setError(_data_googlelicenseinserthd.return_code.ToString() + " - " + _data_googlelicenseinserthd.return_msg);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลในระบบแล้ว');", true);
                        break;

                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลให้ครบถ้วน');", true);
                }


                break;

            case "btnEditCost":

                showsearch.Visible = false;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;
                gridviewindex.Visible = false;


                string[] arg2 = new string[3];
                arg2 = e.CommandArgument.ToString().Split(';');
                int m0_idx_tranfer = int.Parse(arg2[0]);
                int org_idx_tranfer = int.Parse(arg2[1]);
                int rdept_idx_tranfer = int.Parse(arg2[2]);
                int u0_idx_tranfer = int.Parse(arg2[3]);

                ViewState["m0_idx_tranfer"] = m0_idx_tranfer;
                ViewState["org_idx_tranfer"] = org_idx_tranfer;
                ViewState["rdept_idx_tranfer"] = rdept_idx_tranfer;
                ViewState["u0_idx_tranfer"] = u0_idx_tranfer;


                show_data_beforetranfer.Visible = true;

                fvBacktoIndex.Visible = true;
                btnSearchBack.Visible = true;

                div_tranfer_googlelicense.Visible = true;
                div_save_tranfer.Visible = true;

                data_googlelicense _data_googledetailtranfer = new data_googlelicense();
                _data_googledetailtranfer.bindInsert_gglbuy_list = new bindInsert_gglbuy_detail[1];

                bindInsert_gglbuy_detail _bindInsert_gglbuy_detailtranfer = new bindInsert_gglbuy_detail();

                _bindInsert_gglbuy_detailtranfer.m0_idx = int.Parse(ViewState["m0_idx_tranfer"].ToString());
                _bindInsert_gglbuy_detailtranfer.org_idx = int.Parse(ViewState["org_idx_tranfer"].ToString());
                _bindInsert_gglbuy_detailtranfer.rdept_idx = int.Parse(ViewState["rdept_idx_tranfer"].ToString());
                _bindInsert_gglbuy_detailtranfer.u0_buy_idx = int.Parse(ViewState["u0_idx_tranfer"].ToString());

                _data_googledetailtranfer.bindInsert_gglbuy_list[0] = _bindInsert_gglbuy_detailtranfer;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseview));

                _data_googledetailtranfer = callServiceGoogleLicense(_urlGetSetGooglelicenseHD, _data_googledetailtranfer);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicenseinserthd));


                //FvInsert_HolderLicense.ChangeMode(FormViewMode.Insert);
                //FvInsert_HolderLicense.DataBind();

                setFormData(FvBeforeTranfer, FormViewMode.ReadOnly, _data_googledetailtranfer.bindInsert_gglbuy_list);

                ddlOrganizationTranfer();

                setOntop.Focus();

                break;

            case "btnCancelTranfer":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "btnSaveTranfer":

                FormView FvBeforeTranfer_insert = (FormView)ViewIndex.FindControl("FvBeforeTranfer");
                TextBox txtm0_idx_tranfer = (TextBox)FvBeforeTranfer_insert.FindControl("txtm0_idx_tranfer");

                DropDownList ddlorg_idxtranfer = (DropDownList)ViewIndex.FindControl("ddlorg_idxtranfer");
                DropDownList ddlrdept_idxtranfer = (DropDownList)ViewIndex.FindControl("ddlrdept_idxtranfer");
                TextBox txtcostcenter_costtranfer = (TextBox)ViewIndex.FindControl("txtcostcenter_costtranfer");

                _cemp_idx = emp_idx;


                data_googlelicense _data_googledetailtranfer_insert = new data_googlelicense();
                _data_googledetailtranfer_insert.ggltranfer_list = new ggltranfer_detail[1];


                _ggltranfer_detail.m0_idx = int.Parse(ViewState["m0_idx_tranfer"].ToString());
                _ggltranfer_detail.org_idx = int.Parse(ddlorg_idxtranfer.SelectedValue);
                _ggltranfer_detail.rdept_idx = int.Parse(ddlrdept_idxtranfer.SelectedValue);
                _ggltranfer_detail.costcenter_no = txtcostcenter_costtranfer.Text;
                _ggltranfer_detail.cemp_idx = emp_idx;

                _data_googledetailtranfer_insert.ggltranfer_list[0] = _ggltranfer_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googledetailtranfer_insert));

                _data_googledetailtranfer = callServiceGoogleLicense(_urlSetTranferGoogleLicense, _data_googledetailtranfer_insert);

                if (_data_googledetailtranfer.return_code == 0)
                {

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                else
                {

                    setError(_data_googledetailtranfer.return_code.ToString() + " - " + _data_googledetailtranfer.return_msg);

                }


                break;

            case "btnDeleteemp":

                showsearch.Visible = false;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;
                gridviewindex.Visible = false;

                string[] arg3 = new string[3];
                arg3 = e.CommandArgument.ToString().Split(';');
                int m0_idx_delete = int.Parse(arg3[0]);
                int org_idx_delete = int.Parse(arg3[1]);
                int rdept_idx_delete = int.Parse(arg3[2]);
                int u0_idx_delete = int.Parse(arg3[3]);

                ViewState["m0_idx_delete"] = m0_idx_delete;
                ViewState["org_idx_delete"] = org_idx_delete;
                ViewState["rdept_idx_delete"] = rdept_idx_delete;
                ViewState["u0_idx_delete"] = u0_idx_delete;


                fvBacktoIndex.Visible = true;
                btnSearchBack.Visible = true;
                div_showdelete_holder.Visible = true;
                show_delete_emp.Visible = true;
                show_detail_empdelete.Visible = true;

                SelectDetailEmpDelete();
                div_show_managedel_emp.Visible = true;

                panel_delete_googlelicense.Visible = true;
                SelectLogDeleteGoogle();

                setOntop.Focus();

                break;

            case "btnCancelEmpLicense":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "btnDeleteEmpLicense":

                bind_holdergmail_detail[] _tempdelete_emp_holder = (bind_holdergmail_detail[])ViewState["data_delete_empholder"];

                var _del_holder = new bind_holdergmail_detail[_tempdelete_emp_holder.Count()];

                GridView GvDeleteEmpHolder = (GridView)ViewIndex.FindControl("GvDeleteEmpHolder");



                int count_delholder = 0;
                ViewState["count_row_check"] = 0;
                foreach (GridViewRow gr in GvDeleteEmpHolder.Rows)
                {

                    Label lbu0_idx = (Label)gr.FindControl("lbu0_idx");
                    Label lbemp_idxdetail = (Label)gr.FindControl("lbemp_idxdetail");
                    Label lbrsec_idxdetail = (Label)gr.FindControl("lbrsec_idxdetail");
                    Label lbname_license_gmaildetail = (Label)gr.FindControl("lbname_license_gmaildetail");
                    CheckBox check_delete_emp = (CheckBox)gr.FindControl("check_delete_emp");

                    if (check_delete_emp.Checked)
                    {
                        _del_holder[count_delholder] = new bind_holdergmail_detail();

                        _del_holder[count_delholder].m0_idx = int.Parse(lbu0_idx.Text);
                        _del_holder[count_delholder].emp_idx = int.Parse(lbemp_idxdetail.Text);
                        _del_holder[count_delholder].rsec_idx = int.Parse(lbrsec_idxdetail.Text);
                        _del_holder[count_delholder].name_license_gmail = lbname_license_gmaildetail.Text;
                        _del_holder[count_delholder].cemp_idx = emp_idx;
                        _del_holder[count_delholder].m0_actor_idx = 4;
                        _del_holder[count_delholder].m0_node_idx = 4;
                        _del_holder[count_delholder].doc_decision = 4;


                        //count_delete = count_delholder + 1;
                        count_delholder++;

                        ViewState["count_row_check"] = count_delholder;
                    }

                }


                if (int.Parse(ViewState["count_row_check"].ToString()) < 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูลก่อนทำการบันทึก!!!');", true);

                    break;
                }
                else
                {

                    data_googlelicense _data_google_insertdel = new data_googlelicense();
                    _data_google_insertdel.bind_holdergmail_list = new bind_holdergmail_detail[1];

                    _data_google_insertdel.bind_holdergmail_list = _del_holder;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_google_insertdel));
                    _data_google_insertdel = callServiceGoogleLicense(_urlSetDeleteEmpggllicense, _data_google_insertdel);

                    if (_data_google_insertdel.return_code == 0)
                    {
                        //litDebug.Text = ViewState["count_row_check"].ToString() ;
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }


                }


                break;

            case "btnSearchReport":

                DropDownList ddlorg_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlorg_idx_report");
                DropDownList ddlrdept_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlrdept_idx_report");
                DropDownList ddlrsec_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlrsec_idx_report");
                DropDownList ddlemp_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlemp_idx_report");
                TextBox txtcostcenter_no_report = (TextBox)ViewGoogleReport.FindControl("txtcostcenter_no_report");
                TextBox txtemail_name_report = (TextBox)ViewGoogleReport.FindControl("txtemail_name_report");


                _cemp_idx = emp_idx;

                data_googlelicense _data_googledetailsearch_report = new data_googlelicense();
                _data_googledetailsearch_report.bindreport_ggl_list = new bindreport_ggl_detail[1];


                _bindreport_ggl_detail.org_idx = int.Parse(ddlorg_idx_report.SelectedValue);
                _bindreport_ggl_detail.rdept_idx = int.Parse(ddlrdept_idx_report.SelectedValue);
                _bindreport_ggl_detail.rsec_idx = int.Parse(ddlrsec_idx_report.SelectedValue);
                _bindreport_ggl_detail.emp_idx = int.Parse(ddlemp_idx_report.SelectedValue);
                _bindreport_ggl_detail.costcenter_no = txtcostcenter_no_report.Text;
                _bindreport_ggl_detail.email = txtemail_name_report.Text;

                _data_googledetailsearch_report.bindreport_ggl_list[0] = _bindreport_ggl_detail;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googledetailsearch_report));

                _data_googledetailsearch_report = callServiceGoogleLicense(_urlGetDataSearchReportGGL, _data_googledetailsearch_report);

                ViewState["bind_data_report"] = _data_googledetailsearch_report.bindsearch_report_ggl_list;

                gridviewreport.Visible = true;
                GvReport.PageIndex = 0;
                setGridData(GvReport, ViewState["bind_data_report"]);


                break;

            case "btnCancelReport":

                btnToIndexList.BackColor = System.Drawing.Color.Transparent;
                btnToGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnDetailGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                btnToReport.BackColor = System.Drawing.Color.LightGray;

                MvMaster.SetActiveView(ViewGoogleReport);

                DropDownList _ddlrdept_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlrdept_idx_report");
                DropDownList _ddlrsec_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlrsec_idx_report");
                DropDownList _ddlemp_idx_report = (DropDownList)ViewGoogleReport.FindControl("ddlemp_idx_report");
                TextBox _txtcostcenter_no_report = (TextBox)ViewGoogleReport.FindControl("txtcostcenter_no_report");
                TextBox _txtemail_name_report = (TextBox)ViewGoogleReport.FindControl("txtemail_name_report");

                // clear data
                ddlOrganizationReport();

                _ddlrdept_idx_report.Items.Clear();
                _ddlrdept_idx_report.Items.Insert(0, new ListItem("เลือกฝ่าย ...", "00"));

                _ddlrsec_idx_report.Items.Clear();
                _ddlrsec_idx_report.Items.Insert(0, new ListItem("เลือกแผนก ...", "00"));

                _ddlemp_idx_report.Items.Clear();
                _ddlemp_idx_report.Items.Insert(0, new ListItem("เลือกพนักงาน ...", "00"));

                _txtcostcenter_no_report.Text = String.Empty;
                _txtemail_name_report.Text = String.Empty;

                // clear data


                data_googlelicense _data_googlelicense_report_reset = new data_googlelicense();
                _data_googlelicense_report_reset.bindreport_ggl_list = new bindreport_ggl_detail[1];

                _data_googlelicense_report_reset.bindreport_ggl_list[0] = _bindreport_ggl_detail;

                // _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_search));dddd

                _data_googlelicense_report_reset = callServiceGoogleLicense(_urlSetBindReportGooglelicense, _data_googlelicense_report_reset);

                ViewState["bind_data_report"] = _data_googlelicense_report_reset.bindreport_ggl_list;

                gridviewreport.Visible = true;
                GvReport.PageIndex = 0;
                setGridData(GvReport, ViewState["bind_data_report"]);



                setOntop.Focus();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

        }

    }

    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status != 0)
        {

            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
            //return "ถูกใช้งานแล้ว";

            return "<span class='status-online f-bold'>ถูกใช้งาน</span>";
        }
        else
        {
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
            return "<span class='status-offline f-bold'>ยังไม่ถูกใช้งาน</span>";
            //return "ยังไม่ถูกใช้งาน";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GVShowdetailLicense":

                FormView FvInsert_insert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
                Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");
                Panel gidviewdetaillicense = (Panel)PaneldetailLicense.FindControl("gidviewdetaillicense");
                GridView GVShowdetailLicense = (GridView)gidviewdetaillicense.FindControl("GVShowdetailLicense");

                GVShowdetailLicense.PageIndex = e.NewPageIndex;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                GVShowdetailLicense.Focus();

                break;
            case "GvMaster":

                setGridData(GvMaster, ViewState["bind_data_index"]);

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                GvMaster.Focus();

                // actionIndex();
                break;

            case "GVShowdetailmailholder":

                FormView FvInsert_HolderLicense = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                //Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");

                Panel gvmail_holder = (Panel)FvInsert_HolderLicense.FindControl("gvmail_holder");
                GridView GVShowdetailmailholder = (GridView)gvmail_holder.FindControl("GVShowdetailmailholder");

                GVShowdetailmailholder.PageIndex = e.NewPageIndex;
                GVShowdetailmailholder.DataSource = ViewState["vsBuyequipmentholdermail"];
                GVShowdetailmailholder.DataBind();

                GVShowdetailmailholder.Focus();

                break;

            case "GvReport":

                setGridData(GvReport, ViewState["bind_data_report"]);

                GvReport.PageIndex = e.NewPageIndex;
                GvReport.DataBind();

                GvReport.Focus();
                // actionIndex();
                break;

            case "GVShowEditdetailLicense":


                break;

            case "GvShowDetailInsertExpire":

                setGridData(GvShowDetailInsertExpire, ViewState["ShowDetail_lot"]);

                GvShowDetailInsertExpire.PageIndex = e.NewPageIndex;
                GvShowDetailInsertExpire.DataBind();



                break;


        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    UpdatePanel UpdatePanel_Header = (UpdatePanel)e.Row.FindControl("UpdatePanel_Header");
                    DropDownList ddlTypeEmailFilter = (DropDownList)e.Row.FindControl("ddlTypeEmailFilter");
                    getTypeEmail(ddlTypeEmailFilter, _set_statusFilter);

                    if (ViewState["Vs_SetHeader_Filter"].ToString() == "1")
                    {
                        UpdatePanel_Header.Visible = false;
                    }
                    else
                    {
                        UpdatePanel_Header.Visible = true;
                    }

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label _Name_License = (Label)e.Row.Cells[2].FindControl("Name_License");
                    Label _Count_EmpGoogleLicnese = (Label)e.Row.Cells[2].FindControl("Count_EmpGoogleLicnese");
                    LinkButton _btnEditCost = (LinkButton)e.Row.Cells[7].FindControl("btnEditCost");
                    LinkButton _btnDeleteemp = (LinkButton)e.Row.Cells[7].FindControl("btnDeleteemp");
                    LinkButton _btnEditGoogle = (LinkButton)e.Row.Cells[7].FindControl("btnEditGoogle");

                    // check ถ้าเมลว่าง สามารถโอนย้ายได้
                    if (_Name_License.Text == "" && _Count_EmpGoogleLicnese.Text == "0")
                    {
                        _btnEditCost.Visible = true;
                    }
                    else if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text == "0")
                    {
                        _btnEditCost.Visible = true;
                    }
                    else
                    {
                        _btnEditCost.Visible = false;
                    }

                    //Check ว่าสามารถลบ ผู้ถือครอง Google License ได้
                    if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text != "0")
                    {
                        _btnDeleteemp.Visible = true;
                    }
                    else if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text == "0")
                    {
                        _btnDeleteemp.Visible = false;
                    }
                    else
                    {
                        _btnDeleteemp.Visible = false;
                    }

                    //check ปุ่ม edit email
                    //if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text != "0")
                    //{
                    //    _btnEditGoogle.Visible = true;
                    //}
                    //else if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text == "0")
                    //{
                    //    _btnEditGoogle.Visible = false;
                    //}
                    //else
                    //{
                    //    _btnEditGoogle.Visible = false;
                    //}

                }
                break;

            case "GvReport":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbm0_idx_report = (Label)e.Row.FindControl("lbm0_idx_report");
                    Repeater rp_detail_holder_gmail = (Repeater)e.Row.FindControl("rp_detail_holder_gmail");


                    data_googlelicense _data_googledetailholder_report = new data_googlelicense();
                    _data_googledetailholder_report.bind_holdergmail_list = new bind_holdergmail_detail[1];

                    bind_holdergmail_detail _bind_holdergmail_detail_report = new bind_holdergmail_detail();


                    _bind_holdergmail_detail_report.m0_idx = int.Parse(lbm0_idx_report.Text);

                    _data_googledetailholder_report.bind_holdergmail_list[0] = _bind_holdergmail_detail_report;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googledetailholder_report));

                    _data_googledetailholder_report = callServiceGoogleLicense(_urlGetEmpInReportGGl, _data_googledetailholder_report);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googledetailholder_report));

                    rp_detail_holder_gmail.DataSource = _data_googledetailholder_report.bind_holdergmail_list;
                    rp_detail_holder_gmail.DataBind();

                }

                break;



        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVShowdetailLicense":

                FormView FvInsert_insert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
                Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");
                Panel gidviewdetaillicense = (Panel)PaneldetailLicense.FindControl("gidviewdetaillicense");
                GridView GVShowdetailLicense = (GridView)gidviewdetaillicense.FindControl("GVShowdetailLicense");

                GVShowdetailLicense.EditIndex = e.NewEditIndex;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                break;
            case "GvMaster":

                //setGridData(GvMaster, ViewState["bind_data_index"]);

                GvMaster.EditIndex = e.NewEditIndex;

                //GvMaster.PageIndex = e.NewPageIndex;
                // GvMaster.DataBind();


                actionIndex();
                break;

            case "GVShowdetailmailholder":

                FormView FvInsert_HolderLicense = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                //Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");

                Panel gvmail_holder = (Panel)FvInsert_HolderLicense.FindControl("gvmail_holder");
                GridView GVShowdetailmailholder = (GridView)gvmail_holder.FindControl("GVShowdetailmailholder");

                GVShowdetailmailholder.EditIndex = e.NewEditIndex;
                GVShowdetailmailholder.DataSource = ViewState["vsBuyequipmentholdermail"];
                GVShowdetailmailholder.DataBind();

                break;

            case "GVShowEditdetailLicense":


                break;


        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GVLicenseDept":
                ////int company_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                ////var txtCompanyNameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtCompanyNameUpdate");
                ////var ddlCompanyStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlCompanyStatusUpdate");


                ////GvMaster.EditIndex = -1;

                ////_data_networkdevices.m0company_list = new m0company_detail[1];
                ////m0company_detail _m0company_detail = new m0company_detail();
                ////_m0company_detail.company_idx = company_idx_update;
                ////_m0company_detail.company_name = txtCompanyNameUpdate.Text;
                ////_m0company_detail.company_status = int.Parse(ddlCompanyStatusUpdate.SelectedValue);
                ////_m0company_detail.cemp_idx = emp_idx;

                ////_data_networkdevices.m0company_list[0] = _m0company_detail;

                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                ////_data_networkdevices = callServiceNetwork(_urlSetm0Company, _data_networkdevices);

                ////if (_data_networkdevices.return_code == 0)
                ////{
                ////    initPage();
                ////    setDataList(dtlMenu, _dataMenu.m0_menu_list);
                ////    actionIndex();


                ////}
                ////else
                ////{
                ////    setError(_data_networkdevices.return_code.ToString() + " - " + _data_networkdevices.return_msg);
                ////}


                break;
        }
    }

    protected void Master_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GVShowdetailLicense":

                FormView FvInsert_insert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
                Panel PaneldetailLicense = (Panel)FvInsert_insert.FindControl("PaneldetailLicense");
                Panel gidviewdetaillicense = (Panel)PaneldetailLicense.FindControl("gidviewdetaillicense");
                GridView GVShowdetailLicense = (GridView)gidviewdetaillicense.FindControl("GVShowdetailLicense");

                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                GVShowdetailLicense.EditIndex = -1;
                GVShowdetailLicense.DataSource = ViewState["vsBuyequipment"];
                GVShowdetailLicense.DataBind();

                break;

            case "GVShowdetailmailholder":

                FormView FvInsert_HolderLicense = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");

                TextBox txtname_email = (TextBox)FvInsert_HolderLicense.FindControl("txtname_email");


                Panel gvmail_holder = (Panel)FvInsert_HolderLicense.FindControl("gvmail_holder");
                GridView GVShowdetailmailholder = (GridView)gvmail_holder.FindControl("GVShowdetailmailholder");

                var dsvsBuyequipmentDelete_holdermail = (DataSet)ViewState["vsBuyequipmentholdermail"];
                var drDriving_holderemail = dsvsBuyequipmentDelete_holdermail.Tables[0].Rows;

                drDriving_holderemail.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipmentholdermail"] = dsvsBuyequipmentDelete_holdermail;
                GVShowdetailmailholder.EditIndex = -1;
                GVShowdetailmailholder.DataSource = ViewState["vsBuyequipmentholdermail"];
                GVShowdetailmailholder.DataBind();

                if (dsvsBuyequipmentDelete_holdermail.Tables[0].Rows.Count != 0)
                {
                    txtname_email.Enabled = false;
                }
                else if (dsvsBuyequipmentDelete_holdermail.Tables[0].Rows.Count == 0)
                {
                    txtname_email.Enabled = true;
                }


                break;

            case "GvShowDetailDeptEdit":

                ////FormView FvViewDetail = (FormView)ViewDetail.FindControl("FvViewDetail");
                ////FvViewDetail.ChangeMode(FormViewMode.Edit);

                ////GridView GvShowDetailDeptEdit = (GridView)FvViewDetail.FindControl("GvShowDetailDeptEdit");

                //DataTable dtx = new DataTable();
                //dtx = (DataTable)ViewState["data_bind_deptdetail"];
                //DataRow drx = dtx.NewRow();
                //drx["OrgNameTH"] = _ddlorg_idxeditlicense.SelectedItem;
                //drx["DeptNameTH"] = _ddlrdept_idxeditlicense.SelectedItem;
                //drx["num_license_rdept_idx"] = _txtnumlicense_depteditlicense.Text;
                //dtx.Rows.Add(drx);
                //ViewState["data_bind_deptdetail"] = dtx;
                ////Jon.DataSource = dtx;
                ////Jon.DataBind();

                //GvShowDetailDeptEdit.DataSource = dtx;
                //GvShowDetailDeptEdit.DataBind();


                //DataTable dtx1 = new DataTable();
                //dtx1 = (DataTable)ViewState["data_bind_deptdetail"];

                //DataRow drx1 = dtx1.NewRow();

                //DataTable sourceData = (DataTable)GvShowDetailDeptEdit.DataSource;

                //sourceData.Rows[e.RowIndex].Delete();

                //GridVie1.DataSource = sourceData;
                //GridView1.DataBind();

                //test_del.Text = "22222";

                ////var data_bind_deptdetail = (DataSet)ViewState["data_bind_deptdetail"];
                ////var drdelete = data_bind_deptdetail.Tables[0].Rows;

                ////drdelete.RemoveAt(e.RowIndex);

                ////ViewState["data_bind_deptdetail"] = data_bind_deptdetail;
                ////GvShowDetailDeptEdit.EditIndex = -1;
                ////GvShowDetailDeptEdit.DataSource = ViewState["data_bind_deptdetail"];
                ////GvShowDetailDeptEdit.DataBind();

                break;

            case "GVShowEditdetailLicense":


                ////FormView FvViewDetail3 = (FormView)ViewDetail.FindControl("FvViewDetail");
                ////FvViewDetail3.ChangeMode(FormViewMode.Edit);

                ////Panel PaneldetailLicenseEdit = (Panel)FvViewDetail3.FindControl("PaneldetailLicenseEdit");
                ////Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                ////GridView GVShowEditdetailLicense = (GridView)gidviewdetaileditlicense.FindControl("GVShowEditdetailLicense");


                ////var dsvsBuyequipmentDeleteEdit = (DataSet)ViewState["vsBuyequipmentEdit"];
                ////var drDrivingEdit = dsvsBuyequipmentDeleteEdit.Tables[0].Rows;

                ////drDrivingEdit.RemoveAt(e.RowIndex);

                ////ViewState["vsBuyequipmentEdit"] = dsvsBuyequipmentDeleteEdit;
                ////GVShowEditdetailLicense.EditIndex = -1;
                ////GVShowEditdetailLicense.DataSource = ViewState["vsBuyequipmentEdit"];
                ////GVShowEditdetailLicense.DataBind();

                break;

                //case "GvShowDetailDeptEdit":

                //    //FvViewDetail.DataBind();
                //    //FvViewDetail.ChangeMode(FormViewMode.Edit);

                //    ////Panel PaneldetailLicenseEdit = (Panel)FvViewDetail.FindControl("PaneldetailLicenseEdit");
                //    ////Panel gidviewdetaileditlicense = (Panel)PaneldetailLicenseEdit.FindControl("gidviewdetaileditlicense");
                //    //GridView GvShowDetailDeptEdit = (GridView)FvViewDetail.FindControl("GvShowDetailDeptEdit");

                //    ////ViewState["data_bind_deptdetail"]
                //    //var databindDeleteEdit = (DataSet)ViewState["data_bind_deptdetail"];
                //    //var databindDrivingEdit = databindDeleteEdit.Tables[0].Rows;

                //    //databindDrivingEdit.RemoveAt(e.RowIndex);

                //    //ViewState["data_bind_deptdetail"] = databindDeleteEdit;
                //    //GvShowDetailDeptEdit.EditIndex = -1;
                //    //GvShowDetailDeptEdit.DataSource = ViewState["data_bind_deptdetail"];
                //    //GvShowDetailDeptEdit.DataBind();

                //    break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region Checkbox เชคแสดง
    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chk_license":

                FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
                CheckBox chk_license = (CheckBox)FvInsert.FindControl("chk_license");
                Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");

                DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                DropDownList ddlrdept_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlrdept_idxinsert");
                TextBox txtcostcenter_cost = (TextBox)PaneldetailLicense.FindControl("txtcostcenter_cost");
                TextBox txtnumlicense_cost = (TextBox)PaneldetailLicense.FindControl("txtnumlicense_cost");

                Panel showdetailpermissionsystem = (Panel)FvInsert.FindControl("showdetailpermissionsystem");
                //Panel showdetailpermissionsystem = (Panel)FvInsert.FindControl("showdetailpermissionsystem");

                // actionSystemall();
                if (chk_license.Checked)
                {
                    PaneldetailLicense.Visible = true;
                    showdetailpermissionsystem.Visible = false;


                    ddlorg_idxinsert.Items.Clear();
                    ddlorg_idxinsert.AppendDataBoundItems = true;
                    ddlorg_idxinsert.Items.Add(new ListItem("กรุณาเลือกองค์กร ...", "00"));

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    //_permissionDetail.OrgIDX = 0;

                    _data_softwarelicense.organization_list[0] = _organization_detail;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense);

                    ddlorg_idxinsert.DataSource = _data_softwarelicense.organization_list;
                    ddlorg_idxinsert.DataTextField = "OrgNameTH";
                    ddlorg_idxinsert.DataValueField = "OrgIDX";
                    ddlorg_idxinsert.DataBind();

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxinsert.SelectedValue = "00";

                    //txtnumlicense_dept.Text = "1";

                }
                else
                {

                    PaneldetailLicense.Visible = false;
                    showdetailpermissionsystem.Visible = true;
                    ddlorg_idxinsert.Items.Clear();

                    ddlrdept_idxinsert.Items.Clear();
                    txtnumlicense_cost.Text = "";
                    txtcostcenter_cost.Text = "";


                }
                break;

            case "check_delete_emp":

                //ViewState["data_delete_empholder"] = _data_googlelicenseshowdelete.bind_holdergmail_list;

                if (ViewState["data_delete_empholder"] != null)
                {

                    bind_holdergmail_detail[] _tempList = (bind_holdergmail_detail[])ViewState["data_delete_empholder"];
                    int _checked = int.Parse(cb.Text);
                    _tempList[_checked].selected = cb.Checked;

                    //chk.Checked = true;
                    //cb.Enabled = true;

                }

                break;


        }
    }
    #endregion

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        btnToIndexList.BackColor = System.Drawing.Color.LightGray;
        //btnToDeviceSoftware.BackColor = System.Drawing.Color.Transparent;
    }

    #endregion

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsert":

                break;

            case "FvViewDetail":

                if (FvViewDetail.CurrentMode == FormViewMode.ReadOnly)
                {



                }

                if (FvViewDetail.CurrentMode == FormViewMode.Edit)
                {

                }

                break;

            case "FvInsert_HolderLicense":

                if (FvInsert_HolderLicense.CurrentMode == FormViewMode.Insert)
                {

                }

                break;

            case "FvBeforeTranfer":

                if (FvBeforeTranfer.CurrentMode == FormViewMode.Insert)
                {

                }

                break;

        }

    }

    #endregion

    #region ddlSelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;

        // RepeaterItem rpItem = ddName.NamingContainer as RepeaterItem;

        switch (ddName.ID)
        {

            #region ddl org
            case "ddlorg_idxinsert":

                Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");
                DropDownList ddlorg_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlorg_idxinsert");
                DropDownList ddlrdept_idxinsert = (DropDownList)PaneldetailLicense.FindControl("ddlrdept_idxinsert");

                if (ddlorg_idxinsert.SelectedValue == "00")
                {

                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.AppendDataBoundItems = true;
                    ddlrdept_idxinsert.Items.Add(new ListItem("กรุณาเลือกฝ่าย ...", "00"));

                }
                else
                {

                    _data_softwarelicense.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxinsert.SelectedValue);

                    _data_softwarelicense.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense);


                    ddlrdept_idxinsert.Items.Clear();
                    ddlrdept_idxinsert.AppendDataBoundItems = true;
                    ddlrdept_idxinsert.Items.Add(new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxinsert.DataSource = _data_softwarelicense.organization_list;
                    ddlrdept_idxinsert.DataTextField = "DeptNameTH";
                    ddlrdept_idxinsert.DataValueField = "RDeptIDX";
                    ddlrdept_idxinsert.DataBind();



                }

                break;
            #endregion

            #region ddl orginsert
            case "ddlrsec_idxinsert":

                FormView FvInsert_HolderLicense = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");

                DropDownList ddlrsec_idxinsert = (DropDownList)FvInsert_HolderLicense.FindControl("ddlrsec_idxinsert");
                DropDownList ddlemp_idxinsert = (DropDownList)FvInsert_HolderLicense.FindControl("ddlemp_idxinsert");


                if (ddlrsec_idxinsert.SelectedValue == "00")
                {

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง ...", "00"));

                }
                else
                {
                    data_softwarelicense _data_softwarelicensevieworg2 = new data_softwarelicense();
                    _data_softwarelicensevieworg2.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ViewState["org_idx_buy"].ToString());
                    _organization_detail.RDeptIDX = int.Parse(ViewState["rdept_idx_buy"].ToString());
                    _organization_detail.RSecIDX = int.Parse(ddlrsec_idxinsert.SelectedValue);

                    _data_softwarelicensevieworg2.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensevieworg2));

                    _data_softwarelicensevieworg2 = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicensevieworg2);

                    ddlemp_idxinsert.Items.Clear();
                    ddlemp_idxinsert.AppendDataBoundItems = true;
                    ddlemp_idxinsert.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง ...", "00"));
                    ddlemp_idxinsert.DataSource = _data_softwarelicensevieworg2.organization_list;
                    ddlemp_idxinsert.DataTextField = "emp_name_th";
                    ddlemp_idxinsert.DataValueField = "emp_idx";
                    ddlemp_idxinsert.DataBind();


                }

                break;


            case "ddltype_idxinsert":

                Panel PaneldetailLicense_type = (Panel)FvInsert.FindControl("PaneldetailLicense");
                DropDownList ddltype_idxinsert = (DropDownList)PaneldetailLicense_type.FindControl("ddltype_idxinsert");
                Panel lot_dateexpire = (Panel)FvInsert.FindControl("lot_dateexpire");
                Panel lot_codebuy = (Panel)FvInsert.FindControl("lot_codebuy");

                Panel showdetailpermissionsystem = (Panel)FvInsert.FindControl("showdetailpermissionsystem");
                CheckBox chk_license = (CheckBox)FvInsert.FindControl("chk_license");

                Panel PaneldetailLicense_expire = (Panel)FvInsert.FindControl("PaneldetailLicense");

                TextBox txtdatepurchaseinsert = (TextBox)FvInsert.FindControl("txtdatepurchaseinsert");
                TextBox txtdateexpireinsert = (TextBox)FvInsert.FindControl("txtdateexpireinsert");
                TextBox txtprice_insert = (TextBox)FvInsert.FindControl("txtprice_insert");
                TextBox txtcount_licenseinsert = (TextBox)FvInsert.FindControl("txtcount_licenseinsert");

                if (ddltype_idxinsert.SelectedValue == "2") // ต่ออายุ
                {

                    ddlCompanyGoogle();
                    ddlLotGoogle();

                    txtdatepurchaseinsert.Text = string.Empty;
                    txtdateexpireinsert.Text = string.Empty;
                    txtprice_insert.Text = string.Empty;
                    txtcount_licenseinsert.Text = string.Empty;
                    txtcount_licenseinsert.Enabled = false;

                    lot_codebuy.Visible = false;
                    lot_dateexpire.Visible = true;

                    chk_license.Visible = false;
                    PaneldetailLicense_expire.Visible = false;

                    showdetailpermissionsystem.Visible = false;


                }
                else if (ddltype_idxinsert.SelectedValue == "1") // 
                {
                    ddlCompanyGoogle();

                    txtdatepurchaseinsert.Text = string.Empty;
                    txtdateexpireinsert.Text = string.Empty;
                    txtprice_insert.Text = string.Empty;
                    txtcount_licenseinsert.Text = string.Empty;
                    txtcount_licenseinsert.Enabled = true;

                    lot_codebuy.Visible = false;
                    lot_dateexpire.Visible = false;

                    chk_license.Visible = true;
                    chk_license.Checked = false;

                    show_grid_renew.Visible = false;
                    div_showdetail_expire.Visible = false;
                    div_show_detailinsert_expire.Visible = false;
                }
                else
                {
                    ddlCompanyGoogle();
                    ddlLotGoogle();

                    txtdatepurchaseinsert.Text = string.Empty;
                    txtdateexpireinsert.Text = string.Empty;
                    txtprice_insert.Text = string.Empty;
                    txtcount_licenseinsert.Text = string.Empty;
                    txtcount_licenseinsert.Enabled = true;

                    lot_codebuy.Visible = false;
                    lot_dateexpire.Visible = false;

                    chk_license.Visible = true;
                    chk_license.Checked = false;

                    show_grid_renew.Visible = false;
                    div_showdetail_expire.Visible = false;
                    div_show_detailinsert_expire.Visible = false;
                    PaneldetailLicense_expire.Visible = false;
                }


                break;

            case "ddllblot_code":

                Panel lot_dateexpire_renew = (Panel)FvInsert.FindControl("lot_dateexpire");
                DropDownList ddllblot_code = (DropDownList)lot_dateexpire_renew.FindControl("ddllblot_code");

                TextBox _txtcount_licenseinsert = (TextBox)FvInsert.FindControl("txtcount_licenseinsert");

                show_grid_renew.Visible = true;
                div_showdetail_expire.Visible = true;
                div_show_detailinsert_expire.Visible = true;

                //bind lot
                data_googlelicense _data_u0_gglrenew_detail = new data_googlelicense();
                _data_u0_gglrenew_detail.u0_gglrenew_list = new u0_gglrenew_detail[1];
                u0_gglrenew_detail _u0_gglrenew_detail = new u0_gglrenew_detail();

                _u0_gglrenew_detail.u0_buy_idx = int.Parse(ddllblot_code.SelectedValue);

                _data_u0_gglrenew_detail.u0_gglrenew_list[0] = _u0_gglrenew_detail;



                // _data_googlelicenseshowholder = callServiceGoogleLicense(_urlGetHolderGmailLicense, _data_googlelicenseshowholder);
                _data_u0_gglrenew_detail = callServiceGoogleLicense(_urlGetDetailInLot, _data_u0_gglrenew_detail);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_u0_gglrenew_detail));
                ViewState["ShowDetail_lot"] = _data_u0_gglrenew_detail.u0_gglrenew_list;
                setGridData(GvShowDetailInsertExpire, ViewState["ShowDetail_lot"]);

                if (_data_u0_gglrenew_detail.u0_gglrenew_list[0].count_all != 0)
                {
                    ViewState["count_all_expire"] = _data_u0_gglrenew_detail.u0_gglrenew_list[0].count_all;
                    _txtcount_licenseinsert.Text = ViewState["count_all_expire"].ToString();

                    _txtcount_licenseinsert.Enabled = false;

                }


                break;
            #endregion

            #region ddl org tranfer
            case "ddlorg_idxtranfer":


                DropDownList ddlorg_idxtranfer = (DropDownList)ViewIndex.FindControl("ddlorg_idxtranfer");
                DropDownList ddlrdept_idxtranfer = (DropDownList)ViewIndex.FindControl("ddlrdept_idxtranfer");

                if (ddlorg_idxtranfer.SelectedValue == "00")
                {

                    ddlorg_idxtranfer.Items.Clear();

                    ddlorg_idxtranfer.AppendDataBoundItems = true;
                    ddlorg_idxtranfer.Items.Add(new ListItem("กรุณาเลือกองค์กร ....", "00"));

                    data_softwarelicense _data_softwarelicense_orgtranfer1 = new data_softwarelicense();
                    _data_softwarelicense_orgtranfer1.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _data_softwarelicense_orgtranfer1.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense_orgtranfer1 = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgtranfer1);



                    ddlorg_idxtranfer.DataSource = _data_softwarelicense_orgtranfer1.organization_list;
                    ddlorg_idxtranfer.DataTextField = "OrgNameTH";
                    ddlorg_idxtranfer.DataValueField = "OrgIDX";
                    ddlorg_idxtranfer.DataBind();
                    ddlorg_idxtranfer.SelectedValue = "00";

                    ddlrdept_idxtranfer.Items.Clear();
                    ddlrdept_idxtranfer.Items.Insert(00, new ListItem("กรุณาเลือกฝ่าย ...", "00"));
                    ddlrdept_idxtranfer.SelectedValue = "00";


                }
                else
                {

                    ddlrdept_idxtranfer.AppendDataBoundItems = true;
                    ddlrdept_idxtranfer.Items.Clear();

                    ddlrdept_idxtranfer.Items.Add(new ListItem("กรุณาเลือกฝ่าย ....", "00"));


                    data_softwarelicense _data_softwarelicense_orgtranfer2 = new data_softwarelicense();
                    _data_softwarelicense_orgtranfer2.organization_list = new organization_detail[1];
                    organization_detail _organization_detail = new organization_detail();

                    _organization_detail.OrgIDX = int.Parse(ddlorg_idxtranfer.SelectedValue);

                    _data_softwarelicense_orgtranfer2.organization_list[0] = _organization_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                    _data_softwarelicense_orgtranfer2 = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicense_orgtranfer2);


                    ddlrdept_idxtranfer.DataSource = _data_softwarelicense_orgtranfer2.organization_list;
                    ddlrdept_idxtranfer.DataTextField = "DeptNameTH";
                    ddlrdept_idxtranfer.DataValueField = "RDeptIDX";
                    ddlrdept_idxtranfer.DataBind();


                }

                break;
            #endregion

            #region ddl_search

            case "ddlorg_idxsearch":
                if (ddlorg_idxsearch.SelectedValue == "00")
                {
                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarelicensevieworg12 = new data_softwarelicense();
                    _data_softwarelicensevieworg12.organization_list = new organization_detail[1];
                    organization_detail _organization_detail12 = new organization_detail();

                    _organization_detail12.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);

                    _data_softwarelicensevieworg12.organization_list[0] = _organization_detail12;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensevieworg1));

                    _data_softwarelicensevieworg12 = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicensevieworg12); ;


                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idxsearch.DataSource = _data_softwarelicensevieworg12.organization_list;
                    ddlrdept_idxsearch.DataTextField = "DeptNameTH";
                    ddlrdept_idxsearch.DataValueField = "RDeptIDX";
                    ddlrdept_idxsearch.DataBind();

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                break;

            case "ddlrdept_idxsearch":
                if (ddlrdept_idxsearch.SelectedValue == "00")
                {
                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept22 = new data_softwarelicense();
                    _data_softwarelicenseviewdept22.organization_list = new organization_detail[1];
                    organization_detail _organization_detail22 = new organization_detail();

                    _organization_detail22.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail22.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);

                    _data_softwarelicenseviewdept22.organization_list[0] = _organization_detail22;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept22 = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicenseviewdept22);


                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));
                    ddlrsec_idxsearch.DataSource = _data_softwarelicenseviewdept22.organization_list;
                    ddlrsec_idxsearch.DataTextField = "SecNameTH";
                    ddlrsec_idxsearch.DataValueField = "RSecIDX";
                    ddlrsec_idxsearch.DataBind();

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                break;

            case "ddlrsec_idxsearch":
                if (ddlrsec_idxsearch.SelectedValue == "00")
                {

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept33 = new data_softwarelicense();
                    _data_softwarelicenseviewdept33.organization_list = new organization_detail[1];
                    organization_detail _organization_detail33 = new organization_detail();

                    _organization_detail33.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail33.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);
                    _organization_detail33.RSecIDX = int.Parse(ddlrsec_idxsearch.SelectedValue);

                    _data_softwarelicenseviewdept33.organization_list[0] = _organization_detail33;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept33 = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicenseviewdept33);


                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                    ddlemp_idxsearch.DataSource = _data_softwarelicenseviewdept33.organization_list;
                    ddlemp_idxsearch.DataTextField = "emp_name_th";
                    ddlemp_idxsearch.DataValueField = "emp_idx";
                    ddlemp_idxsearch.DataBind();


                }
                break;






            #endregion

            #region ddl_Report

            case "ddlorg_idx_report":

                if (ddlorg_idx_report.SelectedValue == "00")
                {
                    ddlrdept_idx_report.Items.Clear();
                    ddlrdept_idx_report.AppendDataBoundItems = true;
                    ddlrdept_idx_report.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));

                    ddlrsec_idx_report.Items.Clear();
                    ddlrsec_idx_report.AppendDataBoundItems = true;
                    ddlrsec_idx_report.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarerdeptreport = new data_softwarelicense();
                    _data_softwarerdeptreport.organization_list = new organization_detail[1];
                    organization_detail _organization_detail12 = new organization_detail();

                    _organization_detail12.OrgIDX = int.Parse(ddlorg_idx_report.SelectedValue);

                    _data_softwarerdeptreport.organization_list[0] = _organization_detail12;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensevieworg1));

                    _data_softwarerdeptreport = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarerdeptreport); ;


                    ddlrdept_idx_report.Items.Clear();
                    ddlrdept_idx_report.AppendDataBoundItems = true;
                    ddlrdept_idx_report.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idx_report.DataSource = _data_softwarerdeptreport.organization_list;
                    ddlrdept_idx_report.DataTextField = "DeptNameTH";
                    ddlrdept_idx_report.DataValueField = "RDeptIDX";
                    ddlrdept_idx_report.DataBind();

                    ddlrsec_idx_report.Items.Clear();
                    ddlrsec_idx_report.AppendDataBoundItems = true;
                    ddlrsec_idx_report.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                break;

            case "ddlrdept_idx_report":
                if (ddlrdept_idx_report.SelectedValue == "00")
                {
                    ddlrsec_idx_report.Items.Clear();
                    ddlrsec_idx_report.AppendDataBoundItems = true;
                    ddlrsec_idx_report.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarersec_idxreport = new data_softwarelicense();
                    _data_softwarersec_idxreport.organization_list = new organization_detail[1];
                    organization_detail _organization_detailrsec_report = new organization_detail();

                    _organization_detailrsec_report.OrgIDX = int.Parse(ddlorg_idx_report.SelectedValue);
                    _organization_detailrsec_report.RDeptIDX = int.Parse(ddlrdept_idx_report.SelectedValue);

                    _data_softwarersec_idxreport.organization_list[0] = _organization_detailrsec_report;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarersec_idxreport = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarersec_idxreport);


                    ddlrsec_idx_report.Items.Clear();
                    ddlrsec_idx_report.AppendDataBoundItems = true;
                    ddlrsec_idx_report.Items.Add(new ListItem("เลือกแผนก ...", "00"));
                    ddlrsec_idx_report.DataSource = _data_softwarersec_idxreport.organization_list;
                    ddlrsec_idx_report.DataTextField = "SecNameTH";
                    ddlrsec_idx_report.DataValueField = "RSecIDX";
                    ddlrsec_idx_report.DataBind();

                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                break;

            case "ddlrsec_idx_report":
                if (ddlrsec_idx_report.SelectedValue == "00")
                {

                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                else
                {

                    data_softwarelicense _data_softwarelicenseemp_report = new data_softwarelicense();
                    _data_softwarelicenseemp_report.organization_list = new organization_detail[1];
                    organization_detail _organization_detail33 = new organization_detail();

                    _organization_detail33.OrgIDX = int.Parse(ddlorg_idx_report.SelectedValue);
                    _organization_detail33.RDeptIDX = int.Parse(ddlrdept_idx_report.SelectedValue);
                    _organization_detail33.RSecIDX = int.Parse(ddlrsec_idx_report.SelectedValue);

                    _data_softwarelicenseemp_report.organization_list[0] = _organization_detail33;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseemp_report = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicenseemp_report);


                    ddlemp_idx_report.Items.Clear();
                    ddlemp_idx_report.AppendDataBoundItems = true;
                    ddlemp_idx_report.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                    ddlemp_idx_report.DataSource = _data_softwarelicenseemp_report.organization_list;
                    ddlemp_idx_report.DataTextField = "emp_name_th";
                    ddlemp_idx_report.DataValueField = "emp_idx";
                    ddlemp_idx_report.DataBind();


                }
                break;
            #endregion

            case "ddl_type_email":

                FormView _FvInsert_HolderLicense = (FormView)ViewIndex.FindControl("FvInsert_HolderLicense");
                DropDownList ddl_type_email = (DropDownList)_FvInsert_HolderLicense.FindControl("ddl_type_email");
                Panel Panel_SelectDetailInsert = (Panel)_FvInsert_HolderLicense.FindControl("Panel_SelectDetailInsert");
                Panel Panel_AddbtnInsert = (Panel)_FvInsert_HolderLicense.FindControl("Panel_AddbtnInsert");
                Panel gvmail_holder = (Panel)_FvInsert_HolderLicense.FindControl("gvmail_holder");
                DropDownList _ddlemp_idxinsert = (DropDownList)Panel_SelectDetailInsert.FindControl("ddlemp_idxinsert");

                ddlSecInsertHolder();
                _ddlemp_idxinsert.Items.Clear();
                _ddlemp_idxinsert.AppendDataBoundItems = true;
                _ddlemp_idxinsert.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง ...", "00"));

                //ViewState["vsBuyequipmentholdermail"] = null;//aaa

                clearDataSetInsertHolder();


                if (ddl_type_email.SelectedValue == "1") // Mail กลาง
                {
                    //litDebug.Text = "1111";
                    Panel_SelectDetailInsert.Visible = true;
                    Panel_AddbtnInsert.Visible = true;
                    div_saveInsertHolder.Visible = true;
                    gvmail_holder.Visible = false;

                    //clearDataSetOnClickCheckBox();
                    //ViewState["vsBuyequipmentholdermail"] = null;//aaa

                }
                else if (ddl_type_email.SelectedValue == "2") //Mail ส่วนตัว
                {
                    //litDebug.Text = "2222";
                    Panel_SelectDetailInsert.Visible = true;
                    div_saveInsertHolder.Visible = true;
                    Panel_AddbtnInsert.Visible = false;
                    gvmail_holder.Visible = false;
                    //clearDataSetOnClickCheckBox();
                    //ViewState["vsBuyequipmentholdermail"] = null;//aaa
                }
                else
                {
                    Panel_SelectDetailInsert.Visible = false;
                    Panel_AddbtnInsert.Visible = false;
                    gvmail_holder.Visible = false;
                    div_saveInsertHolder.Visible = false;
                    //clearDataSetOnClickCheckBox();
                    // ViewState["vsBuyequipmentholdermail"] = null;//aaa
                }

                break;
            case "ddlTypeEmailFilter":

                if (ddlTypeEmailFilter.SelectedValue != "0")
                {
                    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                    //litDebug.Text = "1";

                    data_googlelicense _data_googlelicense_filter = new data_googlelicense();
                    _data_googlelicense_filter.bind_gglbuy_list = new bind_gglbuy_detail[1];

                    bind_gglbuy_detail bind_gglbuy_detail_filter = new bind_gglbuy_detail();

                    bind_gglbuy_detail_filter.cemp_idx = emp_idx;

                    _data_googlelicense_filter.bind_gglbuy_list[0] = bind_gglbuy_detail_filter;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_softwarelicense_devicesindex));
                    _data_googlelicense_filter = callServiceGoogleLicense(_urlGetGooglelicense, _data_googlelicense_filter);


                    bind_gglbuy_detail[] _item_FilterTypeEmail = (bind_gglbuy_detail[])_data_googlelicense_filter.bind_gglbuy_list;

                    var _linq_FilterTypeEmail = (from data in _item_FilterTypeEmail
                                                 where
                                                 data.m0type_email_idx == int.Parse(ddlTypeEmailFilter.SelectedValue)

                                                 select data
                                            ).ToList();

                    _set_statusFilter = int.Parse(ddlTypeEmailFilter.SelectedValue);

                    //

                    ViewState["bind_data_index"] = _linq_FilterTypeEmail;
                    setGridData(GvMaster, ViewState["bind_data_index"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else
                {

                    actionIndex();

                }

                break;


        }

    }

    #endregion ddlSelectedIndexChanged 

    protected void initPageLoad()
    {

        ////////////////  เพิ่มผู้ถือครอง Google - License    ///////////////////
        var dsEquipment_holdermail = new DataSet();
        dsEquipment_holdermail.Tables.Add("Equipment_holdermail");


        //dsEquipment.Tables[0].Columns.Add("Software_License_dataset", typeof(String));
        //dsEquipment.Tables[0].Columns.Add("Softwareidx_License_dataset", typeof(int));
        dsEquipment_holdermail.Tables[0].Columns.Add("orgname_th_emailholder", typeof(String));
        dsEquipment_holdermail.Tables[0].Columns.Add("org_idx_emailholger", typeof(int));

        dsEquipment_holdermail.Tables[0].Columns.Add("rdeptname_th_emailholder", typeof(String));
        dsEquipment_holdermail.Tables[0].Columns.Add("rdept_idx_emailholder", typeof(int));

        dsEquipment_holdermail.Tables[0].Columns.Add("rsecname_th_emailholder", typeof(String));
        dsEquipment_holdermail.Tables[0].Columns.Add("rsec_idx_emailholder", typeof(int));

        dsEquipment_holdermail.Tables[0].Columns.Add("empnameth_emailholder", typeof(String));
        dsEquipment_holdermail.Tables[0].Columns.Add("emp_idx_emailholder", typeof(int));

        dsEquipment_holdermail.Tables[0].Columns.Add("emailname_license", typeof(String));

        ViewState["vsBuyequipmentholdermail"] = dsEquipment_holdermail;


        ////////////////  เพิ่มผู้ถือครอง Google - License    ///////////////////

    }

    protected void clearDataSetInsertHolder()
    {
        Panel gvmail_holder = (Panel)FvInsert_HolderLicense.FindControl("gvmail_holder");
        GridView GVShowdetailmailholder = (GridView)FvInsert_HolderLicense.FindControl("GVShowdetailmailholder");

        ViewState["vsBuyequipmentholdermail"] = null;
        ViewState["va_Clear_insertHolder"] = ViewState["vsBuyequipmentholdermail"];

        GVShowdetailmailholder.DataSource = ViewState["va_Clear_insertHolder"];
        GVShowdetailmailholder.DataBind();

        initPageLoad();
    }

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

    }

    protected void setVisible()
    {

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected string getStatusemp(int status_emp_computer)
    {

        if (status_emp_computer != 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='มีผู้ถือครอง'><i class='glyphicon glyphicon-user'></i></span>";
        }
        else
        {
            return string.Empty;
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_googlelicense callServiceGoogleLicense(string _cmdUrl, data_googlelicense _data_googlelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_googlelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_googlelicense = (data_googlelicense)_funcTool.convertJsonToObject(typeof(data_googlelicense), _localJson);

        return _data_googlelicense;
    }

    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _data_employee)
    {
        _localJson = _funcTool.convertObjectToJson(_data_employee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _data_employee;
    }

    #endregion reuse

    #region repeater
    protected void rptRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        }


    }
    #endregion

}