﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_MasterCasePOSLV2 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsertCaseLV2POS = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCaseLV2POS"];
    static string urlSelectCaseLV2POS = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV2POS"];
    static string urlUpdaeteCaseLV2POS = _serviceUrl + ConfigurationManager.AppSettings["urlUpdaeteCaseLV2POS"];
    static string urlDeleteCaseLV2POS = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteCaseLV2POS"];
    static string urlSelectCaseLV1POSU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV1POSU0"];



    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            // SelectddlLV1();
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_CaseLV2()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList insert = new POSList();

        insert.POS1IDX = int.Parse(ddlLV1.SelectedValue);
        insert.POS2_Name = txtlv.Text;
        insert.POS2_Code = txtcode.Text;
        insert.POS2Status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = insert;

        _dtsupport = callServicePostITRepair(urlInsertCaseLV2POS, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;


        _dtsupport = callServicePostITRepair(urlSelectCaseLV2POS, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxPOSList);
    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        dtsupport.POS1IDX = int.Parse(ViewState["POS1IDX"].ToString());
        dtsupport.POS2IDX = int.Parse(ViewState["POS2IDX"].ToString());
        dtsupport.POS2_Name = ViewState["txtname_edit"].ToString();
        dtsupport.POS2_Code = ViewState["txtcode_edit"].ToString();
        dtsupport.POS2Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlUpdaeteCaseLV2POS, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        dtsupport.POS2IDX = int.Parse(ViewState["POS2IDX"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlDeleteCaseLV2POS, _dtsupport);
    }

    protected void SelectddlLV1()
    {
        ddlLV1.Items.Clear();
        ddlLV1.AppendDataBoundItems = true;
        ddlLV1.Items.Add(new ListItem("กรุณาเลือกเคสปิดงานLV1....", "0"));


        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);


        ddlLV1.DataSource = _dtsupport.BoxPOSList;
        ddlLV1.DataTextField = "Name_Code1";
        ddlLV1.DataValueField = "POS1IDX";
        ddlLV1.DataBind();
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }


    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlPOS1IDX_edit = ((DropDownList)e.Row.FindControl("ddlPOS1IDX_edit"));
                        var lbLV1 = ((Label)e.Row.FindControl("lbLV1"));


                        _dtsupport.BoxPOSList = new POSList[1];
                        POSList dtsupport = new POSList();

                        _dtsupport.BoxPOSList[0] = dtsupport;

                        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POSU0, _dtsupport);

                        ddlPOS1IDX_edit.DataSource = _dtsupport.BoxPOSList;
                        ddlPOS1IDX_edit.DataTextField = "Name_Code1";
                        ddlPOS1IDX_edit.DataValueField = "POS1IDX";
                        ddlPOS1IDX_edit.DataBind();
                        ddlPOS1IDX_edit.SelectedValue = lbLV1.Text;
                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int POS2IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var ddlPOS1IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlPOS1IDX_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["POS1IDX"] = ddlPOS1IDX_edit.SelectedValue;
                ViewState["POS2IDX"] = POS2IDX;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":

                btnadd.Visible = false;
                Panel_Add.Visible = true;
                SelectddlLV1();
                break;

            case "btnCancel":
                btnadd.Visible = true;
                Panel_Add.Visible = false;
                txtcode.Text = String.Empty;
                txtlv.Text = String.Empty;

                break;

            case "btnAdd":
                Insert_CaseLV2();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int POS2IDX = int.Parse(cmdArg);
                ViewState["POS2IDX"] = POS2IDX;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion
}