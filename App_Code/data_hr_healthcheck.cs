﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_hr_healthcheck
/// </summary>
/// 
[Serializable]
[XmlRoot("data_hr_healthcheck")]
public class data_hr_healthcheck
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // master data //
    [XmlElement("healthcheck_historyemployee_list")]
    public hr_healthcheck_historyemployee_detail[] healthcheck_historyemployee_list { get; set; }

    // history type //
    [XmlElement("healthcheck_historytype_list")]
    public hr_healthcheck_historytype_detail[] healthcheck_historytype_list { get; set; }
    [XmlElement("healthcheck_history_topic_list")]
    public hr_healthcheck_history_topic_detail[] healthcheck_history_topic_list { get; set; }

    // selected Form type check //
    [XmlElement("healthcheck_historyform_list")]
    public hr_healthcheck_historyform_detail[] healthcheck_historyform_list { get; set; }

    // selected detail type health check//
    [XmlElement("healthcheck_typecheck_list")]
    public hr_healthcheck_typecheck_list_detail[] healthcheck_typecheck_list { get; set; }

    // master data doctor
    [XmlElement("healthcheck_m0_province_list")]
    public hr_healthcheck_m0_province_detail[] healthcheck_m0_province_list { get; set; }
    [XmlElement("healthcheck_m0_amphur_list")]
    public hr_healthcheck_m0_amphur_detail[] healthcheck_m0_amphur_list { get; set; }
    [XmlElement("healthcheck_m0_district_list")]
    public hr_healthcheck_m0_district_detail[] healthcheck_m0_district_list { get; set; }
    [XmlElement("healthcheck_m0_doctor_list")]
    public hr_healthcheck_m0_doctor_detail[] healthcheck_m0_doctor_list { get; set; }

    //master data doctor

    //history employee
    [XmlElement("healthcheck_employee_profile_list")]
    public hr_healthcheck_employee_profile_detail[] healthcheck_employee_profile_list { get; set; }
    [XmlElement("healthcheck_m0_detailtype_list")]
    public hr_healthcheck_m0_detailtype_detail[] healthcheck_m0_detailtype_list { get; set; }

    //history employee check
    [XmlElement("healthcheck_u0_healthcheck_list")]
    public hr_healthcheck_u0_healthcheck_detail[] healthcheck_u0_healthcheck_list { get; set; }

    //history work check
    [XmlElement("healthcheck_u0_history_work_list")]
    public hr_healthcheck_u0_history_work_detail[] healthcheck_u0_history_work_list { get; set; }
    [XmlElement("healthcheck_u1_history_work_list")]
    public hr_healthcheck_u1_history_work_detail[] healthcheck_u1_history_work_list { get; set; }
    [XmlElement("healthcheck_u2_history_work_list")]
    public hr_healthcheck_u2_history_work_detail[] healthcheck_u2_history_work_list { get; set; }

    //history injury check
    [XmlElement("healthcheck_u0_history_injury_list")]
    public hr_healthcheck_u0_history_injury_detail[] healthcheck_u0_history_injury_list { get; set; }
    [XmlElement("healthcheck_u1_history_injury_list")]
    public hr_healthcheck_u1_history_injury_detail[] healthcheck_u1_history_injury_list { get; set; }
    [XmlElement("healthcheck_u2_history_injury_list")]
    public hr_healthcheck_u2_history_injury_detail[] healthcheck_u2_history_injury_list { get; set; }

    //history health check
    [XmlElement("healthcheck_u0_history_health_list")]
    public hr_healthcheck_u0_history_health_detail[] healthcheck_u0_history_health_list { get; set; }
    [XmlElement("healthcheck_u1_history_health_list")]
    public hr_healthcheck_u1_history_health_detail[] healthcheck_u1_history_health_list { get; set; }
    [XmlElement("healthcheck_u2_history_health_list")]
    public hr_healthcheck_u2_history_health_detail[] healthcheck_u2_history_health_list { get; set; }
    [XmlElement("healthcheck_u3_history_health_list")]
    public hr_healthcheck_u3_history_health_detail[] healthcheck_u3_history_health_list { get; set; }

    //history health sick
    [XmlElement("healthcheck_u0_history_sick_list")]
    public hr_healthcheck_u0_history_sick_detail[] healthcheck_u0_history_sick_list { get; set; }
    [XmlElement("healthcheck_u1_history_sick_list")]
    public hr_healthcheck_u1_history_sick_detail[] healthcheck_u1_history_sick_list { get; set; }
    [XmlElement("healthcheck_u2_history_sick_list")]
    public hr_healthcheck_u2_history_sick_detail[] healthcheck_u2_history_sick_list { get; set; }
    [XmlElement("healthcheck_u3_history_sick_list")]
    public hr_healthcheck_u3_history_sick_detail[] healthcheck_u3_history_sick_list { get; set; }
    [XmlElement("healthcheck_u4_history_sick_list")]
    public hr_healthcheck_u4_history_sick_detail[] healthcheck_u4_history_sick_list { get; set; }

}

[Serializable]
public class hr_healthcheck_historyemployee_detail
{
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("TypeReset")]
    public int TypeReset { get; set; }
    [XmlElement("EmpReset")]
    public int EmpReset { get; set; }
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }
    [XmlElement("EmpCodeX")]
    public string EmpCodeX { get; set; }
    [XmlElement("EmpPassword")]
    public string EmpPassword { get; set; }
    [XmlElement("EmpType")]
    public int EmpType { get; set; }
    [XmlElement("EmpTypeName")]
    public string EmpTypeName { get; set; }
    [XmlElement("EmpProbation")]
    public int EmpProbation { get; set; }
    [XmlElement("EmpProbationDate")]
    public string EmpProbationDate { get; set; }
    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }
    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }
    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }
    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }
    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }
    [XmlElement("SecNameTH")]
    private string SecNameTH { get; set; }
    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }
    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }
    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }
    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }
    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }
    [XmlElement("CostName")]
    public string CostName { get; set; }
    [XmlElement("CostNo")]
    public string CostNo { get; set; }
    [XmlElement("SexIDX")]
    public int SexIDX { get; set; }
    [XmlElement("RSecID")]
    public int RSecID { get; set; }
    [XmlElement("RSec_Position")]
    public int RSec_Position { get; set; }
    [XmlElement("SexNameTH")]
    public string SexNameTH { get; set; }
    [XmlElement("PrefixIDX")]
    public int PrefixIDX { get; set; }
    [XmlElement("FullNameEmpCode")]
    public string FullNameEmpCode { get; set; }
    [XmlElement("FirstNameTH")]
    public string FirstNameTH { get; set; }
    [XmlElement("LastNameTH")]
    public string LastNameTH { get; set; }
    [XmlElement("NickNameTH")]
    public string NickNameTH { get; set; }
    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }
    [XmlElement("FirstNameEN")]
    public string FirstNameEN { get; set; }
    [XmlElement("LastNameEN")]
    public string LastNameEN { get; set; }
    [XmlElement("NickNameEN")]
    public string NickNameEN { get; set; }
    [XmlElement("FullNameEN")]
    public string FullNameEN { get; set; }
    [XmlElement("PhoneNo")]
    public string PhoneNo { get; set; }
    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }
    [XmlElement("Email")]
    public string Email { get; set; }
    [XmlElement("Birthday")]
    public string Birthday { get; set; }
    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }
    [XmlElement("NatName")]
    public string NatName { get; set; }
    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }
    [XmlElement("RaceName")]
    public string RaceName { get; set; }
    [XmlElement("RelIDX")]
    public int RelIDX { get; set; }
    [XmlElement("RelNameTH")]
    public string RelNameTH { get; set; }
    [XmlElement("EmpAddr")]
    public string EmpAddr { get; set; }
    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }
    [XmlElement("DistName")]
    public string DistName { get; set; }
    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }
    [XmlElement("AmpName")]
    public string AmpName { get; set; }
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }
    [XmlElement("ProvName")]
    public string ProvName { get; set; }
    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }
    [XmlElement("CountryName")]
    public string CountryName { get; set; }
    [XmlElement("MarriedStatus")]
    public int MarriedStatus { get; set; }
    [XmlElement("PerNameTH")]
    public string PerNameTH { get; set; }
    [XmlElement("InhabitStatus")]
    public int InhabitStatus { get; set; }
    [XmlElement("LiveNameTH")]
    public string LiveNameTH { get; set; }
    [XmlElement("EmpIN")]
    public string EmpIN { get; set; }
    [XmlElement("EmpOUT")]
    public string EmpOUT { get; set; }
    [XmlElement("EmpStatus")]
    public int EmpStatus { get; set; }
    [XmlElement("EmpCreate")]
    public string EmpCreate { get; set; }
    [XmlElement("EmpUpdate")]
    public string EmpUpdate { get; set; }
    [XmlElement("JobGradeIDX")]
    public int JobGradeIDX { get; set; }
    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
    [XmlElement("JobGradeName")]
    public string JobGradeName { get; set; }
    [XmlElement("MenuIDX")]
    public int MenuIDX { get; set; }
    [XmlElement("IPAddress")]
    public string IPAddress { get; set; }
    [XmlElement("Search")]
    public string Search { get; set; }
    [XmlElement("NameApprover1")]
    public string NameApprover1 { get; set; }
    [XmlElement("NameApprover2")]
    public string NameApprover2 { get; set; }
    [XmlElement("Experience_Month")]
    public int Experience_Month { get; set; }
    [XmlElement("Experience_cut_day")]
    public int Experience_cut_day { get; set; }
    [XmlElement("Experience_cut_Month")]
    public int Experience_cut_Month { get; set; }
    [XmlElement("Experience_cut_Year_In")]
    public int Experience_cut_Year_In { get; set; }
    [XmlElement("EmpStatusName")]
    public string EmpStatusName { get; set; }
    [XmlElement("IdentityCard")]
    public string IdentityCard { get; set; }
    [XmlElement("MilName")]
    public string MilName { get; set; }
    [XmlElement("EmpProbation_Ex")]
    public string EmpProbation_Ex { get; set; }
    [XmlElement("EmpProbationDate_Ex")]
    public string EmpProbationDate_Ex { get; set; }
    [XmlElement("EmpIN_Ex")]
    public string EmpIN_Ex { get; set; }
    [XmlElement("Birthday_Ex")]
    public string Birthday_Ex { get; set; }
    [XmlElement("EmpStatus_Ex")]
    public string EmpStatus_Ex { get; set; }
    [XmlElement("EmpOUT_Ex")]
    public string EmpOUT_Ex { get; set; }
    [XmlElement("AddStartdate")]
    public string AddStartdate { get; set; }
    [XmlElement("AddEndDate")]
    public string AddEndDate { get; set; }
    [XmlElement("EmpStatusOut_Ex")]
    public string EmpStatusOut_Ex { get; set; }
    [XmlElement("PostCode")]
    public string PostCode { get; set; }

    [XmlElement("OrgAddress")]
    public string OrgAddress { get; set; }

    [XmlElement("PostCode_Org")]
    public string PostCode_Org { get; set; }


}

[Serializable]
public class hr_healthcheck_historytype_detail
{
    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("type_status")]
    public int type_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}

[Serializable]
public class hr_healthcheck_historyform_detail
{
    [XmlElement("r0_form_idx")]
    public int r0_form_idx { get; set; }

    [XmlElement("r1_form_idx")]
    public int r1_form_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("form_name")]
    public string form_name { get; set; }

    [XmlElement("form_status")]
    public int form_status { get; set; }

    [XmlElement("m0_topic_idx")]
    public int m0_topic_idx { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("option_idx")]
    public int option_idx { get; set; }

    [XmlElement("root_idx")]
    public int root_idx { get; set; }

    [XmlElement("set_idx")]
    public int set_idx { get; set; }

    [XmlElement("setroot_idx")]
    public int setroot_idx { get; set; }

    [XmlElement("set_name")]
    public string set_name { get; set; }

}

[Serializable]
public class hr_healthcheck_typecheck_list_detail
{
    [XmlElement("m0_detail_typecheck_idx")]
    public int m0_detail_typecheck_idx { get; set; }

    [XmlElement("detail_typecheck")]
    public string form_ndetail_typecheckame { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class hr_healthcheck_history_topic_detail
{
    [XmlElement("m0_history_topic_idx")]
    public int m0_history_topic_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("history_topic_name")]
    public string history_topic_name { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

}

[Serializable]
public class hr_healthcheck_m0_province_detail
{
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvCode")]
    public int ProvCode { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    
}

[Serializable] 
public class hr_healthcheck_m0_amphur_detail
{
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }


}

[Serializable]
public class hr_healthcheck_m0_district_detail
{
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }


}

[Serializable]
public class hr_healthcheck_m0_doctor_detail
{
    [XmlElement("m0_doctor_idx")]
    public int m0_doctor_idx { get; set; }

    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }

    [XmlElement("doctor_name")]
    public string doctor_name { get; set; }

    [XmlElement("card_number")]
    public string card_number { get; set; }

    [XmlElement("health_authority_name")]
    public string health_authority_name { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("village_no")]
    public string village_no { get; set; }

    [XmlElement("road_name")]
    public string road_name { get; set; }

    [XmlElement("phone_number")]
    public string phone_number { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("doctor_status")]
    public int doctor_status { get; set; }

}

[Serializable]
public class hr_healthcheck_employee_profile_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }
    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }
    [XmlElement("costcenter_no")]
    public int costcenter_no { get; set; }

    [XmlElement("job_level")]
    public int job_level { get; set; }
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }
    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }

}

[Serializable]
public class hr_healthcheck_u0_healthcheck_detail
{
    [XmlElement("u0_history_sick_idx")]
    public int u0_history_sick_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("detail")]
    public string detail { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("ProvName")]
    public string ProvName { get; set; }

    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }

    [XmlElement("AmpName")]
    public string AmpName { get; set; }

    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }

    [XmlElement("DistName")]
    public string DistName { get; set; }

    [XmlElement("doctor_name")]
    public string doctor_name { get; set; }

    [XmlElement("card_number")]
    public string card_number { get; set; }

    [XmlElement("health_authority_name")]
    public string health_authority_name { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("village_no")]
    public string village_no { get; set; }

    [XmlElement("road_name")]
    public string road_name { get; set; }

    [XmlElement("phone_number")]
    public string phone_number { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("doctor_status")]
    public int doctor_status { get; set; }

}

[Serializable]
public class hr_healthcheck_m0_detailtype_detail
{
    [XmlElement("m0_detail_typecheck_idx")]
    public int m0_detail_typecheck_idx { get; set; }

    [XmlElement("detail_typecheck")]
    public string detail_typecheck { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

}

[Serializable]
public class hr_healthcheck_u0_history_work_detail
{
    [XmlElement("u0_historywork_idx")]
    public int u0_historywork_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historywork_status")]
    public int u0_historywork_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u1_history_work_detail
{
    [XmlElement("u1_historywork_idx")]
    public int u1_historywork_idx { get; set; }

    [XmlElement("u0_historywork_idx")]
    public int u0_historywork_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historywork_status")]
    public int u0_historywork_status { get; set; }

    [XmlElement("u1_historywork_status")]
    public int u1_historywork_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u2_history_work_detail
{
    [XmlElement("u2_historywork_idx")]
    public int u2_historywork_idx { get; set; }

    [XmlElement("u1_historywork_idx")]
    public int u1_historywork_idx { get; set; }

    [XmlElement("u0_historywork_idx")]
    public int u0_historywork_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historywork_status")]
    public int u0_historywork_status { get; set; }

    [XmlElement("u2_historywork_status")]
    public int u2_historywork_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("department_name")]
    public string department_name { get; set; }

    [XmlElement("business_type")]
    public string business_type { get; set; }

    [XmlElement("job_description")]
    public string job_description { get; set; }

    [XmlElement("job_startdate")]
    public string job_startdate { get; set; }

    [XmlElement("job_enddate")]
    public string job_enddate { get; set; }

    [XmlElement("risk_health")]
    public string risk_health { get; set; }

    [XmlElement("protection_equipment")]
    public string protection_equipment { get; set; }


}

[Serializable]
public class hr_healthcheck_u0_history_injury_detail
{
    [XmlElement("u0_historyinjury_idx")]
    public int u0_historyinjury_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historyinjury_status")]
    public int u0_historyinjury_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u1_history_injury_detail
{
    [XmlElement("u1_historyinjury_idx")]
    public int u1_historyinjury_idx { get; set; }

    [XmlElement("u0_historyinjury_idx")]
    public int u0_historyinjury_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historyinjury_status")]
    public int u0_historyinjury_status { get; set; }

    [XmlElement("u1_historyinjury_status")]
    public int u1_historyinjury_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u2_history_injury_detail
{
    [XmlElement("u2_historyinjury_idx")]
    public int u2_historyinjury_idx { get; set; }

    [XmlElement("u0_historyinjury_idx")]
    public int u0_historyinjury_idx { get; set; }
 
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("date_injuly")]
    public string date_injuly { get; set; }

    [XmlElement("injury_detail")]
    public string injury_detail { get; set; }

    [XmlElement("cause_Injury")]
    public string cause_Injury { get; set; }

    [XmlElement("disability")]
    public string disability { get; set; }

    [XmlElement("lost_organ")]
    public string lost_organ { get; set; }

    [XmlElement("not_working")]
    public int not_working { get; set; }

    [XmlElement("u2_historyinjury_status")]
    public int u2_historyinjury_status { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("detail_not_working")]
    public string detail_not_working { get; set; }


}

[Serializable]
public class hr_healthcheck_u0_history_health_detail
{
    [XmlElement("u0_historyhealth_idx")]
    public int u0_historyhealth_idx { get; set; }

    [XmlElement("u2_historyhealth_idx")]
    public int u2_historyhealth_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historyhealth_status")]
    public int u0_historyhealth_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u1_history_health_detail
{
    [XmlElement("u1_historyhealth_idx")]
    public int u1_historyhealth_idx { get; set; }

    [XmlElement("u0_historyhealth_idx")]
    public int u0_historyhealth_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u1_historyhealth_status")]
    public int u1_historyhealth_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u2_history_health_detail
{
    [XmlElement("u2_historyhealth_idx")]
    public int u2_historyhealth_idx { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("u0_historyhealth_idx")]
    public int u0_historyhealth_idx { get; set; }

    [XmlElement("m0_detail_typecheck_idx")]
    public int m0_detail_typecheck_idx { get; set; }

    [XmlElement("m0_doctor_idx")]
    public int m0_doctor_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("date_check")]
    public string date_check { get; set; }

    [XmlElement("num_check")]
    public string num_check { get; set; }

    [XmlElement("weight")]
    public string weight { get; set; }

    [XmlElement("height")]
    public string height { get; set; }

    [XmlElement("body_mass")]
    public string body_mass { get; set; }

    [XmlElement("bloodpressure")]
    public string bloodpressure { get; set; }

    [XmlElement("specify_resultbody")]
    public string specify_resultbody { get; set; }

    [XmlElement("resultlab")]
    public string resultlab { get; set; }

    [XmlElement("u2_historyhealth_status")]
    public int u2_historyhealth_status { get; set; }

    [XmlElement("pulse")]
    public string pulse { get; set; }

    [XmlElement("detail_typecheck")]
    public string detail_typecheck { get; set; }

    [XmlElement("doctor_name")]
    public string doctor_name { get; set; }

    [XmlElement("card_number")]
    public string card_number { get; set; }

    [XmlElement("health_authority_name")]
    public string health_authority_name { get; set; }

    [XmlElement("location_name")]
    public string location_name { get; set; }

    [XmlElement("village_no")]
    public string village_no { get; set; }

    [XmlElement("road_name")]
    public string road_name { get; set; }

 

}

[Serializable]
public class hr_healthcheck_u3_history_health_detail
{
    [XmlElement("u3_historyhealth_idx")]
    public int u3_historyhealth_idx { get; set; }

    [XmlElement("u0_historyhealth_idx")]
    public int u0_historyhealth_idx { get; set; }

    [XmlElement("u2_historyhealth_idx")]
    public int u2_historyhealth_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("rike_health")]
    public string rike_health { get; set; }

    [XmlElement("result_health")]
    public string result_health { get; set; }

    [XmlElement("u3_historyhealth_status")]
    public int u3_historyhealth_status { get; set; }


}

[Serializable]
public class hr_healthcheck_u0_history_sick_detail
{
    [XmlElement("u0_historysick_idx")]
    public int u0_historysick_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("m0_type_idx")]
    public int m0_type_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u0_historysick_status")]
    public int u0_historysick_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u1_history_sick_detail
{
    [XmlElement("u1_historysick_idx")]
    public int u1_historysick_idx { get; set; }

    [XmlElement("u0_historysick_idx")]
    public int u0_historysick_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u1_historysick_status")]
    public int u1_historysick_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u2_history_sick_detail
{
    [XmlElement("u2_historysick_idx")]
    public int u2_historysick_idx { get; set; }

    [XmlElement("u0_historysick_idx")]
    public int u0_historysick_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("havedisease")]
    public string havedisease { get; set; }

    [XmlElement("surgery")]
    public string surgery { get; set; }

    [XmlElement("immune")]
    public string immune { get; set; }

    [XmlElement("drugs_eat")]
    public string drugs_eat { get; set; }

    [XmlElement("allergy_history")]
    public string allergy_history { get; set; }

    [XmlElement("smoking")]
    public int smoking { get; set; }

    [XmlElement("smoking_value1")]
    public string smoking_value1 { get; set; }

    [XmlElement("smoking_value2")]
    public string smoking_value2 { get; set; }

    [XmlElement("smoking_value3")]
    public string smoking_value3 { get; set; }

    [XmlElement("smoking_value4")]
    public string smoking_value4 { get; set; }

    [XmlElement("alcohol")]
    public int alcohol { get; set; }

    [XmlElement("alcohol_value1")]
    public string alcohol_value1 { get; set; }

    [XmlElement("alcohol_value2")]
    public string alcohol_value2 { get; set; }

    [XmlElement("addict_drug")]
    public string addict_drug { get; set; }

    [XmlElement("datahealth")]
    public string datahealth { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

}

[Serializable]
public class hr_healthcheck_u3_history_sick_detail
{
    [XmlElement("u3_historysick_idx")]
    public int u3_historysick_idx { get; set; }

    [XmlElement("u0_historysick_idx")]
    public int u0_historysick_idx { get; set; }

    [XmlElement("eversick")]
    public string eversick { get; set; }

    [XmlElement("sick_year")]
    public string sick_year { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u3_historysick_status")]
    public int u3_historysick_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}

[Serializable]
public class hr_healthcheck_u4_history_sick_detail
{
    [XmlElement("u4_historysick_idx")]
    public int u4_historysick_idx { get; set; }

    [XmlElement("u0_historysick_idx")]
    public int u0_historysick_idx { get; set; }

    [XmlElement("relation_family")]
    public string relation_family { get; set; }

    [XmlElement("disease_family")]
    public string disease_family { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("u4_historysick_status")]
    public int u4_historysick_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

}
