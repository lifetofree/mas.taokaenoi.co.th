﻿
using System;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_ecom_fileinterface")]
public class data_ecom_fileinterface
{

    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master data ---//
    [XmlElement("ecom_fileinterface_list")]
    public ecom_fileinterface_data[] ecom_fileinterface_list { get; set; }

    [XmlElement("ecom_temptosap_list")]
    public ecom_temptosap_data[] ecom_temptosap_list { get; set; }

    [XmlElement("ecom_m0_data_list")]
    public ecom_m0_data[] ecom_m0_data_list { get; set; }


    [XmlElement("ecom_u0_data_list")]
    public ecom_u0_data[] ecom_u0_data_list { get; set; }

    [XmlElement("ecom_u0_data_detail")]
    public ecom_u0_data_gv[] ecom_u0_data_detail { get; set; }

    [XmlElement("ecom_u1_data_list")]
    public ecom_u1_data[] ecom_u1_data_list { get; set; }

    [XmlElement("ecom_m0_saleunit_list")]
    public ecom_m0_fif_saleunit[] ecom_m0_saleunit_list { get; set; }

    [XmlElement("ecom_m0_promotion_list")]
    public ecom_m0_fif_promotion[] ecom_m0_promotion_list { get; set; }

    [XmlElement("ecom_m0_data_bom_list")]
    public ecom_m0_data_bom[] ecom_m0_data_bom_list { get; set; }
    [XmlElement("ecom_m1_data_bom_list")]
    public ecom_m1_data_bom[] ecom_m1_data_bom_list { get; set; }

    [XmlElement("ecom_m0_material_list")]
    public ecom_m0_material[] ecom_m0_material_list { get; set; }

    [XmlElement("ecom_m0_m1_bom_list")]
    public ecom_m0_m1_bom[] ecom_m0_m1_bom_list { get; set; }

}
[Serializable]
public class ecom_m0_data_bom
{

    [XmlElement("bom_m0_idx")]
    public int bom_m0_idx { get; set; }

    [XmlElement("bom_name")]
    public string bom_name { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("emp_idx_update")]
    public int emp_idx_update { get; set; }
    [XmlElement("bom_status")]
    public int bom_status { get; set; }

    [XmlElement("start_date")]
    public string start_date { get; set; }
    [XmlElement("end_date")]
    public string end_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }

}

[Serializable]
public class ecom_m0_material
{
    [XmlElement("material_id")]
    public int material_id { get; set; }
    [XmlElement("material_name")]
    public string material_name { get; set; }
    [XmlElement("material")]
    public string material { get; set; }
    [XmlElement("name_and_material")]
    public string name_and_material { get; set; }
    [XmlElement("mat_status")]
    public int mat_status { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    

}


[Serializable]
public class ecom_m1_data_bom
{

    [XmlElement("bom_m1_idx")]
    public int bom_m1_idx { get; set; }
    [XmlElement("bom_m0_idx")]
    public int bom_m0_idx { get; set; }
    [XmlElement("product_name")]
    public string product_name { get; set; }
    [XmlElement("material")]
    public string material { get; set; }
    [XmlElement("unit_name")]
    public string unit_name { get; set; }
    [XmlElement("price")]
    public decimal price { get; set; }
    [XmlElement("discount")]
    public decimal discount { get; set; }
    [XmlElement("VAT")]
    public decimal VAT { get; set; }
    [XmlElement("PromotionID")]
    public string PromotionID { get; set; }
    [XmlElement("PromotionDesc")]
    public string PromotionDesc { get; set; }
    [XmlElement("SAP_code")]
    public string SAP_code { get; set; }

    [XmlElement("amount")]
    public int amount { get; set; }
    [XmlElement("type_promotion")]
    public int type_promotion { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("emp_idx_update")]
    public int emp_idx_update { get; set; }
    [XmlElement("bom_m1_status")]
    public int bom_m1_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }


}

[Serializable]
public class ecom_m0_m1_bom
{
    [XmlElement("bom_name")]
    public string bom_name { get; set; }
    [XmlElement("start_date")]
    public string start_date { get; set; }
    [XmlElement("end_date")]
    public string end_date { get; set; }
    [XmlElement("product_name")]
    public string product_name { get; set; }
    [XmlElement("material")]
    public string material { get; set; }
    [XmlElement("unit_name")]
    public string unit_name { get; set; }
    [XmlElement("amount")]
    public int amount { get; set; }
    [XmlElement("VAT")]
    public float VAT { get; set; }
    [XmlElement("price")]
    public float price { get; set; }
    [XmlElement("discount")]
    public float discount { get; set; }
    [XmlElement("PromotionID")]
    public string PromotionID { get; set; }
    [XmlElement("PromotionDesc")]
    public string PromotionDesc { get; set; }
    [XmlElement("SAP_code")]
    public string SAP_code { get; set; }

    [XmlElement("type_promotion")]
    public int type_promotion { get; set; }
}
[Serializable]
public class ecom_m0_fif_promotion
{
    [XmlElement("PromotionDesc")]
    public string PromotionDesc { get; set; }
    [XmlElement("PromotionID")]
    public string PromotionID { get; set; }

    [XmlElement("ZAP_code")]
    public string ZAP_code { get; set; }

    [XmlElement("type_promotion")]
    public string type_promotion { get; set; }

    [XmlElement("amount")]
    public float amount { get; set; }
}

[Serializable]
public class ecom_m0_fif_saleunit
{
    [XmlElement("unit_name")]
    public string unit_name { get; set; }
    [XmlElement("material")]
    public string material { get; set; }
}


[Serializable]
public class ecom_fileinterface_data
{
    [XmlElement("date_start")]
    public string date_start { get; set; }
    [XmlElement("date_end")]
    public string date_end { get; set; }
    [XmlElement("qty")]
    public int qty { get; set; }
    [XmlElement("city")]
    public string city { get; set; }
    [XmlElement("alias")]
    public string alias { get; set; }
    [XmlElement("cname")]
    public string cname { get; set; }
    [XmlElement("phone")]
    public string phone { get; set; }
    [XmlElement("address")]
    public string address { get; set; }
    [XmlElement("currency")]
    public string currency { get; set; }
    [XmlElement("postcode")]
    public string postcode { get; set; }
    [XmlElement("net_price")]
    public float net_price { get; set; }
    [XmlElement("reference")]
    public string reference { get; set; }
    [XmlElement("tmaterial")]
    public string tmaterial { get; set; }
    [XmlElement("price_unit")]
    public float price_unit { get; set; }
    [XmlElement("vat_amount")]
    public float vat_amount { get; set; }
    [XmlElement("county_name")]
    public string county_name { get; set; }
    [XmlElement("delivery_date")]
    public string delivery_date { get; set; }
    [XmlElement("id_order_state")]
    public string id_order_state { get; set; }
    [XmlElement("date_add")]
    public string date_add { get; set; }
    [XmlElement("product_name")]
    public string product_name { get; set; }
    [XmlElement("freeitem")]
    public string freeitem { get; set; }

}

[Serializable]
public class ecom_temptosap_data
{
    [XmlElement("Order_type")]
    public string Order_type { get; set; }
    [XmlElement("Sale_Org")]
    public string Sale_Org { get; set; }
    [XmlElement("Channel")]
    public string Channel { get; set; }
    [XmlElement("Division")]
    public string Division { get; set; }
    [XmlElement("Sales_office")]
    public string Sales_office { get; set; }
    [XmlElement("Sales_group")]
    public string Sales_group { get; set; }
    [XmlElement("Customer_No")]
    public string Customer_No { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("Address")]
    public string Address { get; set; }
    [XmlElement("Honse_number")]
    public string Honse_number { get; set; }
    [XmlElement("Postal_Code")]
    public string Postal_Code { get; set; }
    [XmlElement("District")]
    public string District { get; set; }
    [XmlElement("City")]
    public string City { get; set; }
    [XmlElement("Country")]
    public string Country { get; set; }
    [XmlElement("Tel")]
    public string Tel { get; set; }
    [XmlElement("Tax_number")]
    public string Tax_number { get; set; }
    [XmlElement("PO_number")]
    public string PO_number { get; set; }
    [XmlElement("Delivery_Date")]
    public string Delivery_Date { get; set; }
    [XmlElement("Discount_1")]
    public Decimal Discount_1 { get; set; }
    [XmlElement("Item")]
    public string Item { get; set; }
    [XmlElement("Material")]
    public string Material { get; set; }
    [XmlElement("Quantity")]
    public string Quantity { get; set; }
    [XmlElement("usu")]
    public string usu { get; set; }
    [XmlElement("Plant")]
    public string Plant { get; set; }
    [XmlElement("Price_Unit")]
    public float Price_Unit { get; set; }
    [XmlElement("Currency")]
    public string Currency { get; set; }
    [XmlElement("DiscountType")]
    public string DiscountType { get; set; }
    [XmlElement("DiscountAmount")]
    public int DiscountAmount { get; set; }
    [XmlElement("PromotionID")]
    public string PromotionID { get; set; }
    [XmlElement("PromotionDesc")]
    public string PromotionDesc { get; set; }
    [XmlElement("NetPriceAmount")]
    public float NetPriceAmount { get; set; }
    [XmlElement("VATAmount")]
    public float VATAmount { get; set; }
    [XmlElement("Item_cate")]
    public string Item_cate { get; set; }


}

[Serializable]
public class ecom_m0_data
{

    [XmlElement("channel_idx")]
    public string channel_idx { get; set; }
    [XmlElement("channel_name")]
    public string channel_name { get; set; }
    [XmlElement("cemp_idx")]
    public string cemp_idx { get; set; }
    [XmlElement("m0_order_type")]
    public string m0_order_type { get; set; }
    [XmlElement("m0_sale_org")]
    public string m0_sale_org { get; set; }
    [XmlElement("m0_channel")]
    public string m0_channel { get; set; }
    [XmlElement("m0_division")]
    public string m0_division { get; set; }
    [XmlElement("m0_sales_office")]
    public string m0_sales_office { get; set; }
    [XmlElement("m0_sales_group")]
    public string m0_sales_group { get; set; }
    [XmlElement("m0_customer_no")]
    public string m0_customer_no { get; set; }
    [XmlElement("channel_status")]
    public string channel_status { get; set; }
}

[Serializable]
public class ecom_u0_data
{

    [XmlElement("u0_fileinterface_idx")]
    public int u0_fileinterface_idx { get; set; }
    [XmlElement("file_name_import")]
    public string file_name_import { get; set; }
    [XmlElement("file_name_real")]
    public string file_name_real { get; set; }
    [XmlElement("user_emp_idx")]
    public int user_emp_idx { get; set; }
    [XmlElement("export_sap_status")]
    public int export_sap_status { get; set; }
    [XmlElement("export_sap_emp_idx")]
    public int export_sap_emp_idx { get; set; }
    [XmlElement("channel_idx")]
    public int channel_idx { get; set; }
    [XmlElement("status")]
    public string status { get; set; }

}

[Serializable]
public class ecom_u0_data_gv
{

    [XmlElement("u0_fileinterface_idx")]
    public int u0_fileinterface_idx { get; set; }
    [XmlElement("file_name_import")]
    public string file_name_import { get; set; }
    [XmlElement("file_name_real")]
    public string file_name_real { get; set; }
    [XmlElement("user_emp_idx")]
    public int user_emp_idx { get; set; }
    [XmlElement("export_sap_status")]
    public int export_sap_status { get; set; }
    [XmlElement("export_sap_emp_idx")]
    public int export_sap_emp_idx { get; set; }
    [XmlElement("channel_idx")]
    public int channel_idx { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }


    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
    [XmlElement("channel_name")]
    public string channel_name { get; set; }
    [XmlElement("emp_name_import")]
    public string emp_name_import { get; set; }
    [XmlElement("emp_name_exsap")]
    public string emp_name_exsap { get; set; }


}




[Serializable]
public class ecom_u1_data
{

    [XmlElement("u1_fileinterface_idx")]
    public string u1_fileinterface_idx { get; set; }
    [XmlElement("u0_fileinterface_idx")]
    public string u0_fileinterface_idx { get; set; }
    [XmlElement("u1_order_type")]
    public string u1_order_type { get; set; }
    [XmlElement("u1_sale_org")]
    public string u1_sale_org { get; set; }
    [XmlElement("u1_channel")]
    public string u1_channel { get; set; }
    [XmlElement("u1_division")]
    public string u1_division { get; set; }
    [XmlElement("u1_sales_office")]
    public string u1_sales_office { get; set; }
    [XmlElement("u1_sales_group")]
    public string u1_sales_group { get; set; }
    [XmlElement("u1_customer_no")]
    public string u1_customer_no { get; set; }
    [XmlElement("u1_name")]
    public string u1_name { get; set; }
    [XmlElement("u1_address")]
    public string u1_address { get; set; }
    [XmlElement("u1_honse_number")]
    public string u1_honse_number { get; set; }

    [XmlElement("u1_postal_code")]
    public string u1_postal_code { get; set; }
    [XmlElement("u1_district")]
    public string u1_district { get; set; }
    [XmlElement("u1_city")]
    public string u1_city { get; set; }
    [XmlElement("u1_country")]
    public string u1_country { get; set; }
    [XmlElement("u1_tel")]
    public string u1_tel { get; set; }
    [XmlElement("u1_tax_number")]
    public string u1_tax_number { get; set; }
    [XmlElement("u1_po_number")]
    public string u1_po_number { get; set; }
    [XmlElement("u1_delivery_date")]
    public string u1_delivery_date { get; set; }

    [XmlElement("u1_discount_1")]
    public decimal u1_discount_1 { get; set; }
    
    [XmlElement("u1_item")]
    public string u1_item { get; set; }
    [XmlElement("u1_material")]
    public string u1_material { get; set; }
    [XmlElement("u1_quantity")]
    public string u1_quantity { get; set; }

    [XmlElement("u1_un_sale_unit")]
    public string u1_un_sale_unit { get; set; }
    [XmlElement("u1_plant")]
    public string u1_plant { get; set; }
    [XmlElement("u1_price_unit")]
    public float u1_price_unit { get; set; }
    [XmlElement("u1_currency")]
    public string u1_currency { get; set; }
    [XmlElement("u1_discounttype")]
    public string u1_discounttype { get; set; }
    [XmlElement("u1_discountAmount")]
    public string u1_discountAmount { get; set; }
    [XmlElement("u1_promotionID")]
    public string u1_promotionID { get; set; }
    [XmlElement("u1_promotionDesc")]
    public string u1_promotionDesc { get; set; }
    [XmlElement("u1_netPriceAmount")]
    public float u1_netPriceAmount { get; set; }

    [XmlElement("u1_VATamount")]
    public float u1_VATamount { get; set; }
    [XmlElement("u1_item_cate")]
    public string u1_item_cate { get; set; }

    [XmlElement("export_sap_status")]
    public int export_sap_status { get; set; }
    [XmlElement("export_sap_emp_idx")]
    public int export_sap_emp_idx { get; set; }



}

