﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_equipment_resolution.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_equipment_resolution" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewResolution" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_InsertResolution" CssClass="btn btn-primary" data-original-title="เพิ่มข้อมูลค่าอ่านละเอียด" data-toggle="tooltip" title="เพิ่มข้อมูลค่าอ่านละเอียด" runat="server"
                    CommandName="cmdInsertResolution" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลค่าอ่านละเอียด</asp:LinkButton>
            </div>

            <asp:GridView ID="GvResolution" runat="server" Visible="true"
                AutoGenerateColumns="false"
                DataKeyNames="equipment_resolution_idx"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูล</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_equipment_resolution_idx" runat="server" Visible="false" Text='<%# Eval("equipment_resolution_idx") %>' />

                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_equipment_resolution_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("equipment_resolution_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_equipment_result" CssClass="col-sm-3 control-label" runat="server" Text="ค่าอ่านละเอียด" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_resolution_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("resolution_name") %>'></asp:TextBox>
                                            
                                            <asp:RequiredFieldValidator ID="Re_txt_resolution_name_edit" runat="server"
                                                ControlToValidate="txt_resolution_name_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกกรอกค่าอ่านละเอียด" ValidationGroup="SaveEdit" />

                                            <asp:RegularExpressionValidator ID="Rq_txt_resolution_name_edit" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลขเท่านั้น" Font-Size="11"
                                                ControlToValidate="txt_resolution_name_edit"
                                                ValidationExpression="((\d+)((\.\d{1,5})?))$" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_resolution_name_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_resolution_name_edit" Width="220" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorRe_txt_resolution_name_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rq_txt_resolution_name_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="หน่วย" />
                                        <div class="col-sm-6">
                                            <asp:Label ID="lbl_unit_idx_edit" runat="server" Text='<%# Bind("unit_idx") %>' Visible="false" />
                                            <asp:DropDownList ID="ddlunit_idx_edit" AutoPostBack="true" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Re_ddlunit_idx_edit" runat="server" InitialValue="0" ControlToValidate="ddlunit_idx_edit" Display="None" 
                                            SetFocusOnError="true" ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="SaveEdit" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="Val_Re_ddlunit_idx_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlunit_idx_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_cal_type" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">

                                            <asp:DropDownList ID="ddl_resolution_status_edit" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("resolution_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="SaveEdit" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbequipment_result" runat="server" Text='<%# Eval("resolution_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbequipment_unit_idx" Visible="false" runat="server" Text='<%# Eval("unit_idx") %>'></asp:Label>

                                    <asp:Label ID="lbl_unit_symbol_en" runat="server" Text='<%# Eval("unit_symbol_en") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_resolution_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("resolution_status") %>'></asp:Label>

                                    <asp:Label ID="lbl_resolution_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lbl_resolution_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteResolution" CssClass="text-trash" runat="server" CommandName="cmdDelResolution"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบค่าอ่านละเอียดนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("equipment_resolution_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>

        <!-- Start Insert Form -->
        <asp:View ID="ViewInsert" runat="server">

            <%-- ค่าอ่านละเอียด--%>


            <%--<div class="col-md-12">
                <div class="form-group">
                    <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                        CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                </div>
            </div>--%>

            <div class="form-group">
                <asp:LinkButton ID="btnCancelInsert" CssClass="btn btn-danger" data-toggle="tooltip" title="" runat="server"
                    CommandName="cmdCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่มค่าอ่านละเอียด (Resolution)</h3>

                </div>

                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                <div class="form-group">
                                    <asp:Label ID="lbl_resolution_name" runat="server" Text="ค่าอ่านละเอียด" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_resolution_name" runat="server" CssClass="form-control" placeholder="กรอกค่าอ่านละเอียด ..." Enabled="true" />

                                        <asp:RequiredFieldValidator ID="Re_txt_resolution_name" runat="server"
                                            ControlToValidate="txt_resolution_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกกรอกค่าอ่านละเอียด" ValidationGroup="Save" />

                                        <asp:RegularExpressionValidator ID="Rq_txt_resolution_name" runat="server" ValidationGroup="Save" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลขเท่านั้น" Font-Size="11"
                                            ControlToValidate="txt_resolution_name"
                                            ValidationExpression="((\d+)((\.\d{1,5})?))$" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txt_resolution_name" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_resolution_name" Width="220" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_resolution_name" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rq_txt_resolution_name" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <%-- <asp:RequiredFieldValidator ID="Rqtxtcalpoint" ValidationGroup="saveInsertPoint" runat="server" Display="None"
                                                ControlToValidate="txtcalpoint" Font-Size="11"
                                                ErrorMessage="กรุณากรอกข้อมูล" />

                                            <asp:RegularExpressionValidator ID="Rqtxtcalpoint1" runat="server" ValidationGroup="saveInsertPoint" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะตัวเลขจำนวนเต็มเท่านั้น" Font-Size="11"
                                                ControlToValidate="txtcalpoint"
                                                ValidationExpression="\d+" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender266" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtcalpoint" Width="160" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender778" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rqtxtcalpoint1" Width="160" />--%>

                                <div class="form-group">
                                    <asp:Label ID="lbl_unit_idx" CssClass="col-sm-3 control-label" runat="server" Text="หน่วย" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_unit_idx" CssClass="form-control" runat="server">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="Re_ddl_unit_idx" runat="server" InitialValue="0" ControlToValidate="ddl_unit_idx" Display="None" 
                                            SetFocusOnError="true" ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="Save" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="Val_Re_ddl_unit_idx" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_unit_idx" Width="220" />

                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <%--<div class="form-group">
                                    <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_frequency_unit" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="เดือน/ครั้ง" />
                                            <asp:ListItem Value="2" Text="ปี/ครั้ง" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>--%>

                                <div class="form-group">
                                    <asp:Label ID="lbl_resolution_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_resolution_status" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btn_SaveResolution" ValidationGroup="Save" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSaveResolution" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btn_CancelResulution" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <!-- End Insert Form -->

    </asp:MultiView>


</asp:Content>

