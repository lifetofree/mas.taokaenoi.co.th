<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="lunch_project_report.aspx.cs" Inherits="websystem_hr_lunch_project_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="row">
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-default" ID="div_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litHeadingTitle" runat="server" Text="ค้นหารายงาน"></asp:Literal>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:UpdatePanel ID="upSearchReport" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สถานที่ <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPlantIdx" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ร้านที่ <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlResIdx" runat="server" CssClass="form-control">
                                                    <asp:ListItem value="0" Text="ทั้งหมด" />
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทรายงาน <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlReportType" runat="server"
                                                    CssClass="form-control">
                                                    <asp:ListItem value="1" Text="รายวัน" />
                                                    <asp:ListItem value="2" Text="รายเดือน" />
                                                    <asp:ListItem value="3" Text="รายปี" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ช่วงวันที่ <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbStart" runat="server"
                                                    CssClass="form-control date-datepicker" placeholder="ช่วงวันที่" />
                                            </div>
                                            <label class="col-md-2 control-label">ถึงวันที่ <span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEnd" runat="server"
                                                    CssClass="form-control date-datepicker" placeholder="ถึงวันที่" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4 control-label-static">
                                                <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary"
                                                    OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="0">
                                                    <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info"
                                                    OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbExport" runat="server" CssClass="btn btn-success"
                                                    OnCommand="btnCommand" CommandName="cmdExport" CommandArgument="0"
                                                    Visible="false">
                                                    <i class="far fa-file-excel" aria-hidden="true"></i>&nbsp;Export to
                                                    Excel</asp:LinkButton>
                                            </label>
                                            <label class="col-md-6 control-label"></label>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lbExport" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="gvReportList" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnRowDataBound="gvRowDataBound" ShowFooter="True" DataKeyNames="u0_idx">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ - นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ฝ่าย">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeptNameTh" runat="server" Text='<%# Eval("dept_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แผนก">
                                <ItemTemplate>
                                    <asp:Label ID="lblSecNameTh" runat="server" Text='<%# Eval("sec_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ร้าน">
                                <ItemTemplate>
                                    <asp:Label ID="lblResIdx" runat="server"
                                        Text='<%# "ร้านที่ " + Eval("res_idx").ToString() %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">รวม(บาท)</p>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เวลาที่บันทึก">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSum" runat="server" Text='-'></asp:Label>
                                    </p>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvReportList2" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnRowDataBound="gvRowDataBound" ShowFooter="True">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ปี-เดือน">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("create_date") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ร้าน">
                                <ItemTemplate>
                                    <asp:Label ID="lblResIdx" runat="server"
                                        Text='<%# "ร้านที่ " + Eval("res_idx").ToString() %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">รวม</p>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวน">
                                <ItemTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblCountAmount" runat="server"
                                            Text='<%# Eval("count_amount") %>'>
                                        </asp:Label>
                                    </p>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSumAmount" runat="server" Text='-'></asp:Label>
                                    </p>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รวมเป็นเงิน(บาท)">
                                <ItemTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSum" runat="server" Text='-'></asp:Label>
                                    </p>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSumTotal" runat="server" Text='-'></asp:Label>
                                    </p>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvReportList3" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnRowDataBound="gvRowDataBound" ShowFooter="True">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ปี">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("create_date") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ร้าน">
                                <ItemTemplate>
                                    <asp:Label ID="lblResIdx" runat="server"
                                        Text='<%# "ร้านที่ " + Eval("res_idx").ToString() %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">รวม</p>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จำนวน">
                                <ItemTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblCountAmount" runat="server"
                                            Text='<%# Eval("count_amount") %>'>
                                        </asp:Label>
                                    </p>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSumAmount" runat="server" Text='-'></asp:Label>
                                    </p>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รวมเป็นเงิน(บาท)">
                                <ItemTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSum" runat="server" Text='-'></asp:Label>
                                    </p>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <p class="text-right">
                                        <asp:Label ID="lblSumTotal" runat="server" Text='-'></asp:Label>
                                    </p>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Literal ID="litReport" runat="server"></asp:Literal>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>

    <script type="text/javascript">
        $(function () {
            $('.date-datepicker').datetimepicker({
                //format: 'DD/MM/YYYY HH:mm'
                format: 'YYYY-MM-DD HH:mm'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.date-datepicker').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm'
                });
            });
        });

    </script>
</asp:Content>