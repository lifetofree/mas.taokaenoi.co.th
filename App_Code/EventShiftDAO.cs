﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for EventDAO
/// </summary>
public class EventShiftDAO
{
    //change the connection string as per your database connection.
    private static string connectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;

    //this method retrieves all events within range start-end
    //public static List<CalendarEvent> getEvents(DateTime start, DateTime end, int place_idx)
    public static List<CalendarShiftEvent> getEvents(DateTime start, DateTime end, int u0_empshift_idx)
    {
        //string sta = "10/09/2018";
        //string end_ = "20/09/2018";

        List<CalendarShiftEvent> events = new List<CalendarShiftEvent>();
        SqlConnection con = new SqlConnection(connectionString);
        //SqlCommand cmd = new SqlCommand("SELECT event_id, description, title, event_start, event_end, all_day FROM rbk_test_event", con);
        //empshift_status = 1 and(@u0_empshift_idx = 0 OR u0_empshift_idx = @u0_empshift_idx)
        SqlCommand cmd = new SqlCommand("SELECT u0_empshift_idx, announce_diary_date_start, announce_diary_date_end, parttime_name_th FROM view_emps_u0_empshift where empshift_status = 1 and (@u0_empshift_idx = 0 OR u0_empshift_idx = @u0_empshift_idx)", con);// AND date_start_checkdate>=@start AND date_end_checkdate<=@end
        cmd.Parameters.Add("@start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@u0_empshift_idx", SqlDbType.Int).Value = u0_empshift_idx;
        //cmd.Parameters.Add("@m0_room_idx", SqlDbType.Int).Value = 1; // m0_room_idx;

        using (con)
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                events.Add(new CalendarShiftEvent()
                {
                    id = Convert.ToInt32(reader["u0_empshift_idx"]),
                    title = Convert.ToString( "กะเข้างาน : " + reader["parttime_name_th"]),
                    description = Convert.ToString(reader["parttime_name_th"]),
                    start = Convert.ToDateTime(reader["announce_diary_date_start"]),
                    end = Convert.ToDateTime(reader["announce_diary_date_end"]),

                    //u0_empshift_idx = Convert.ToInt32(reader["u0_empshift_idx"]),
                    //title = Convert.ToString("ชื่อกะ :" + " " + reader["parttime_name_th"]),
                    //parttime_name_th = Convert.ToString(reader["parttime_name_th"]),
                    //start = Convert.ToDateTime(reader["announce_diary_date_start"]),
                    //end = Convert.ToDateTime(reader["announce_diary_date_end"]),

                });
            }
        }
        return events;

       
        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains an extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }

    //this method updates the event title and description
    public static void updateEvent(int id, String title, String description)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET title=@title, description=@description WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = description;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method updates the event start and end time ... allDay parameter added for FullCalendar 2.x
    public static void updateEventTime(int id, DateTime start, DateTime end, bool allDay)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET event_start=@event_start, event_end=@event_end, all_day=@all_day WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = allDay;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this mehtod deletes event with the id passed in.
    public static void deleteEvent(int id)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("DELETE FROM Event WHERE (event_id = @event_id)", con);
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method adds events to the database
    public static int addEvent(CalendarEvent cevent)
    {
        //add event to the database and return the primary key of the added event row

        //insert
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("INSERT INTO Event(title, description, event_start, event_end, all_day) VALUES(@title, @description, @event_start, @event_end, @all_day)", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

        int key = 0;
        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();

            //get primary key of inserted row
            cmd = new SqlCommand("SELECT max(event_id) FROM Event where title=@title AND description=@description AND event_start=@event_start AND event_end=@event_end AND all_day=@all_day", con);
            cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
            cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
            cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
            cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

            key = (int)cmd.ExecuteScalar();
        }

        return key;
    }
}
