﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_report_dashboard : System.Web.UI.Page
{
    #region Connect
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_hr_dashboard _dthr_dashboard = new data_hr_dashboard();

    string _localJson = String.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetReport_DashBoard = _serviceUrl + ConfigurationManager.AppSettings["urlget_report_dashboard"];
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            ViewState["rdept_idx_count"] = "0";
            ViewState["planint_idx_count"] = "0";
            select_empIdx_present();
            GenerateddlYear(ddlyear);
            getOrganizationList(ddlOrg_report);
        }
    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["EmpType"] = _dtEmployee.employee_list[0].emp_type_idx;

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    public void Select_ChartGroup(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 1;
        _select.orgidx = orgidx;
        _select.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);


        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {
            int count = _dthr_dashboard.BoxReport_Dashboard.Length;
            string[] lv1group = new string[count];
            object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dthr_dashboard.BoxReport_Dashboard)
            {
                lv1group[i] = data.pos_name.ToString();
                lv1count[i] = data.countTIDX.ToString();
                i++;
            }
            Highcharts chart3 = new Highcharts("chart3");
            chart3.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart3.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " " } } });
            chart3.SetXAxis(new XAxis { Categories = lv1group });

            chart3.SetPlotOptions(new PlotOptions
            {
                Bar = new PlotOptionsBar
                {
                    Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                    BorderWidth = 2,
                    BorderColor = Color.Gray,
                    Shadow = false,
                    DataLabels = new PlotOptionsBarDataLabels
                    {
                        Enabled = true,
                        Color = Color.White,
                        Shadow = true

                    }

                }

            });

            object[,] v_zcount = new object[lv1group.Length, lv1count.Length];
            Series[] s_list = new Series[lv1group.Length];
            for (int m = 0; m < lv1group.Length; m++)
            {
                object[] v_display = new object[lv1count.Length];
                for (int n = 0; n < lv1count.Length; n++)
                {
                    v_display[n] = v_zcount[m, n];
                }

                s_list[m] = new Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                    Name = lv1group[m],
                    Data = new Data(v_display)
                };
            }


            chart3.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
                    Name = "Group Name",
                    Data = new Data(lv1count)
                }
            );
            litReportChart_groupemp.Text = chart3.ToHtmlString();
        }
        else
        {
            litReportChart_groupemp.Text = "<p class='bg-danger'>No result.</p>";
        }




    }

    public void Select_ChartJobGrade(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 2;
        _select.orgidx = orgidx;
        _select.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dthr_dashboard.BoxReport_Dashboard)
            {
                caseclose.Add(new object[] { data.JobLevel.ToString(), data.count_jobgrade_idx.ToString() });
            }

            Highcharts chart_job = new Highcharts("chart_job");
            chart_job.SetTitle(new Title { Text = " " });
            chart_job.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart_job.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }",
                    }
                }
            });


            chart_job.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart_job.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });

            litReportChart_jobgrade.Text = chart_job.ToHtmlString();
        }
        else
        {
            litReportChart_jobgrade.Text = "<p class='bg-danger'>No result.</p>";
        }




    }

    public void Select_ChartPlant(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 3;
        _select.orgidx = orgidx;
        _select.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        //                txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {
            var caseplant = new List<object>();

            foreach (var data in _dthr_dashboard.BoxReport_Dashboard)
            {
                caseplant.Add(new object[] { data.LocName.ToString(), data.count_plant_idx.ToString() });
            }

            Highcharts chart_plant = new Highcharts("chart_plant");
            chart_plant.SetTitle(new Title { Text = " " });
            chart_plant.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart_plant.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart_plant.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart_plant.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseplant.ToArray())
                        }
                 });

            litReportChart_qtyplant.Text = chart_plant.ToHtmlString();
        }
        else
        {
            litReportChart_qtyplant.Text = "<p class='bg-danger'>No result.</p>";
        }




    }

    public void Select_ChartSex(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 4;
        _select.orgidx = orgidx;
        _select.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);


        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {
            int count = _dthr_dashboard.BoxReport_Dashboard.Length;
            string[] lv1group = new string[count];
            // object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dthr_dashboard.BoxReport_Dashboard)
            {
                lv1group[i] = data.pos_name.ToString();
                // lv1count[i] = data.countTIDX.ToString();
                i++;
            }
            Highcharts chart_sex = new Highcharts("chart_sex");
            chart_sex.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart_sex.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = " Number of Group Employee By Sex " } } });
            chart_sex.SetXAxis(new XAxis { Categories = lv1group });

            chart_sex.SetPlotOptions(new PlotOptions
            {
                Column = new PlotOptionsColumn
                {
                    Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                    BorderWidth = 2,
                    BorderColor = Color.Gray,
                    Shadow = false,
                    DataLabels = new PlotOptionsColumnDataLabels
                    {
                        Enabled = true,
                        Color = Color.Black,
                        Shadow = true

                    }

                }

            });

            System.Text.StringBuilder builder_sex_1 = new System.Text.StringBuilder();
            System.Text.StringBuilder builder_sex_2 = new System.Text.StringBuilder();

            foreach (var safePrime in _dthr_dashboard.BoxReport_Dashboard)
            {
                // Append each int to the StringBuilder overload.
                builder_sex_1.Append(safePrime.male.ToString()).Append(",");
                builder_sex_2.Append(safePrime.female.ToString()).Append(",");
            }

            string employee_male_1 = builder_sex_1.ToString();
            string employee_female_2 = builder_sex_2.ToString();


            chart_sex.SetSeries(
              new Series[]
              {


                new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "ชาย",
                        Data = new Data(new object[] { employee_male_1 }),
                        Color = ColorTranslator.FromHtml("#99ebff")
                },
                 new Series{
                            Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                            Name = "หญิง",
                            Data = new Data(new object[] { employee_female_2}),
                            Color = ColorTranslator.FromHtml("#ff80d5")
                    },
              }
              );

            litReportChart_sexemp.Text = chart_sex.ToHtmlString();


            //object[,] v_zcount = new object[lv1group.Length, lv1count.Length];
            //Series[] s_list = new Series[lv1group.Length];
            //for (int m = 0; m < lv1group.Length; m++)
            //{
            //    object[] v_display = new object[lv1count.Length];
            //    for (int n = 0; n < lv1count.Length; n++)
            //    {
            //        v_display[n] = v_zcount[m, n];
            //    }

            //    s_list[m] = new Series
            //    {
            //        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
            //        Name = lv1group[m],
            //        Data = new Data(v_display)
            //    };
            //}


            //chart3.SetSeries(
            //    new DotNet.Highcharts.Options.Series
            //    {
            //        Type = DotNet.Highcharts.Enums.ChartTypes.Bar,
            //        Name = "Group Name",
            //        Data = new Data(lv1count)
            //    }
            //);
            //litReportChart_groupemp.Text = chart3.ToHtmlString();
        }
        else
        {
            litReportChart_sexemp.Text = "<p class='bg-danger'>No result.</p>";
        }




    }

    public void Select_TabelOnlyHeadder(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 5;
        _select.orgidx = orgidx;
        _select.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);
        ViewState["vs_listHeaderPlant"] = _dthr_dashboard.BoxReport_Dashboard;

        string _header_text = String.Empty;
        int _header_idx;
        int _countrow = 0;
        DataTable table = new DataTable();
        // table.Columns.Add("Approve", typeof(String));
        table.Columns.Add("ฝ่าย", typeof(String));

        if (_dthr_dashboard.BoxReport_Dashboard == null)
        {
            GvPlantByDept.Visible = false;

        }
        else
        {
            foreach (var _loop_header in _dthr_dashboard.BoxReport_Dashboard)
            {

                _header_idx = int.Parse(_loop_header.plant_idx.ToString());
                _header_text = _loop_header.LocName.ToString();
                table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

                _countrow++;

            }
        }

        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_plant = new Report_DashBoardDetail();

        _select_plant.condition = 6;
        _select_plant.orgidx = orgidx;
        _select_plant.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select_plant;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listDepet"] = _dthr_dashboard.BoxReport_Dashboard;

        table.Columns.Add("ผลรวม", typeof(String));
        int _countrow1 = 0;

        if (_dthr_dashboard.ReturnCode.ToString() == "1")
        {
            GvPlantByDept.Visible = false;

        }
        else
        {
            foreach (var _loop_rowText in _dthr_dashboard.BoxReport_Dashboard)
            {

                Label dynamicLabel = new Label();
                dynamicLabel.Text = _loop_rowText.dept_name_th.ToString();
                table.Rows.Add(dynamicLabel.Text);


                _countrow1++;

            }
            GvPlantByDept.Visible = true;

        }

        ViewState["vsTable"] = table;


        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_countplant = new Report_DashBoardDetail();

        _select_countplant.condition = 7;
        _select_countplant.orgidx = orgidx;
        _select_countplant.date_now = datenow;
        _select_countplant.rdept_idx = int.Parse(ViewState["rdept_idx_count"].ToString());
        _select_countplant.plant_idx = int.Parse(ViewState["planint_idx_count"].ToString());

        _dthr_dashboard.BoxReport_Dashboard[0] = _select_countplant;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listCount_Plantidx"] = _dthr_dashboard.BoxReport_Dashboard;


        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {

            setGridData(GvPlantByDept, ViewState["vsTable"]);
        }
        else
        {
            setGridData(GvPlantByDept, null);
        }

    }

    public void Select_Appsent(int orgidx, string datenow)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 8;
        _select.orgidx = orgidx;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);
        ViewState["vs_listHeaderPlant_Appsent"] = _dthr_dashboard.BoxReport_Dashboard;

        string _header_text = String.Empty;
        int _header_idx;
        int _countrow = 0;
        DataTable table = new DataTable();
        // table.Columns.Add("Approve", typeof(String));
        table.Columns.Add("ฝ่าย", typeof(String));

        if (_dthr_dashboard.BoxReport_Dashboard == null)
        {
            GvAppsent.Visible = false;

        }
        else
        {
            foreach (var _loop_header in _dthr_dashboard.BoxReport_Dashboard)
            {

                _header_idx = int.Parse(_loop_header.plant_idx.ToString());
                _header_text = _loop_header.LocName.ToString();
                table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

                _countrow++;

            }
        }


        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_plant = new Report_DashBoardDetail();

        _select_plant.condition = 9;
        _select_plant.orgidx = orgidx;
        _select_plant.date_now = datenow;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select_plant;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listDepet_Appsent"] = _dthr_dashboard.BoxReport_Dashboard;

        table.Columns.Add("ผลรวม", typeof(String));
        int _countrow1 = 0;

        if (_dthr_dashboard.ReturnCode.ToString() == "1")
        {
            GvAppsent.Visible = false;

        }
        else
        {
            foreach (var _loop_rowText in _dthr_dashboard.BoxReport_Dashboard)
            {

                Label dynamicLabel = new Label();
                dynamicLabel.Text = _loop_rowText.dept_name_th.ToString();
                table.Rows.Add(dynamicLabel.Text);


                _countrow1++;

            }
            GvAppsent.Visible = true;

        }

        ViewState["vsTable_Appsent"] = table;


        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_countplant = new Report_DashBoardDetail();

        _select_countplant.condition = 14;
        _select_countplant.orgidx = orgidx;
        _select_countplant.date_now = datenow;


        _dthr_dashboard.BoxReport_Dashboard[0] = _select_countplant;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listCount_Plantidx_Appsent"] = _dthr_dashboard.BoxReport_Dashboard;

        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {

            setGridData(GvAppsent, ViewState["vsTable_Appsent"]);
        }
        else
        {
            setGridData(GvAppsent, null);
        }

    }

    public void Select_Resign(int orgidx, string year, string month)
    {
        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select = new Report_DashBoardDetail();

        _select.condition = 11;
        _select.orgidx = orgidx;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);
        ViewState["vs_listHeaderPlant_Resign"] = _dthr_dashboard.BoxReport_Dashboard;

        string _header_text = String.Empty;
        int _header_idx;
        int _countrow = 0;
        DataTable table = new DataTable();
        // table.Columns.Add("Approve", typeof(String));
        table.Columns.Add("ฝ่าย", typeof(String));

        if (_dthr_dashboard.BoxReport_Dashboard == null)
        {
            GvResign.Visible = false;

        }
        else
        {
            foreach (var _loop_header in _dthr_dashboard.BoxReport_Dashboard)
            {

                _header_idx = int.Parse(_loop_header.plant_idx.ToString());
                _header_text = _loop_header.LocName.ToString();
                table.Columns.Add(_header_text, typeof(String), _header_idx.ToString());

                _countrow++;

            }
        }


        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_plant = new Report_DashBoardDetail();

        _select_plant.condition = 12;
        _select_plant.orgidx = orgidx;

        _dthr_dashboard.BoxReport_Dashboard[0] = _select_plant;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listDepet_Resign"] = _dthr_dashboard.BoxReport_Dashboard;

        table.Columns.Add("ผลรวม", typeof(String));
        int _countrow1 = 0;

        if (_dthr_dashboard.ReturnCode.ToString() == "1")
        {
            GvResign.Visible = false;

        }
        else
        {
            foreach (var _loop_rowText in _dthr_dashboard.BoxReport_Dashboard)
            {

                Label dynamicLabel = new Label();
                dynamicLabel.Text = _loop_rowText.dept_name_th.ToString();
                table.Rows.Add(dynamicLabel.Text);


                _countrow1++;

            }
            GvResign.Visible = true;

        }

        ViewState["vsTable_Resign"] = table;



        _dthr_dashboard = new data_hr_dashboard();

        _dthr_dashboard.BoxReport_Dashboard = new Report_DashBoardDetail[1];
        Report_DashBoardDetail _select_countplant = new Report_DashBoardDetail();

        _select_countplant.condition = 13;
        _select_countplant.orgidx = orgidx;
        _select_countplant.DC_Month = month;
        _select_countplant.DC_YEAR = year;


        _dthr_dashboard.BoxReport_Dashboard[0] = _select_countplant;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dthr_dashboard));

        _dthr_dashboard = callServicePostReportDashBoard(_urlGetReport_DashBoard, _dthr_dashboard);

        ViewState["vs_listCount_Plantidx_Resign"] = _dthr_dashboard.BoxReport_Dashboard;


        if (_dthr_dashboard.ReturnCode.ToString() == "0")
        {

            setGridData(GvResign, ViewState["vsTable_Resign"]);
        }
        else
        {
            setGridData(GvResign, null);
        }

    }

    #endregion

    #region Gridview
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvPlantByDept":
                int _resultDB2 = 0;
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList"] = count_test;
                    }


                    int _resultDB = int.Parse(ViewState["rowCoutList"].ToString());

                    //litDebug.Text = _resultDB.ToString();

                    int _numrowDB = _resultDB;
                    int _numrowDB_div = _resultDB - 1;
                    for (int _loop = 0; _loop < e.Row.Cells.Count; _loop++)
                    {
                        if (_loop == 0)
                        {
                            e.Row.Cells[_loop].Text = "ฝ่าย";
                        }
                        else if (_loop <= _resultDB) // หัว
                        {



                            string _text_header = String.Empty;
                            int _countrow1 = 1;
                            foreach (var _loop_rowText in _templist)
                            {

                                _text_header = _loop_rowText.LocName.ToString();
                                e.Row.Cells[_countrow1].Text = _text_header.ToString();
                                _countrow1++;
                            }
                        }
                        else
                        {
                        }

                    }


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList2"] = count_test;
                    }


                    _resultDB2 = int.Parse(ViewState["rowCoutList2"].ToString());

                    for (int _loop2 = 0; _loop2 < e.Row.Cells.Count; _loop2++)
                    {
                        if (_loop2 == 0)
                        {
                            // e.Row.Cells[_loop2].Text = "sample code";
                        }
                        else if (_loop2 <= _resultDB2)
                        {


                            string sample_codeselect = string.Empty;

                            sample_codeselect = e.Row.Cells[0].Text;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx"];

                            var _linqTest = (from data in _item_Result
                                             where data.plant_idx == int.Parse(e.Row.Cells[_loop2].Text)

                                             select new
                                             {
                                                 data.count_plant_idx,
                                                 data.dept_name_th

                                             }).ToList();


                            int test_show = 0;

                            e.Row.Cells[_loop2].Text = "0";
                            for (int z = 0; z < _linqTest.Count(); z++)
                            {

                                test_show += int.Parse(_linqTest[z].count_plant_idx.ToString());

                                if (e.Row.Cells[0].Text == _linqTest[z].dept_name_th.ToString())
                                {

                                    e.Row.Cells[_loop2].Text = _linqTest[z].count_plant_idx.ToString();

                                }
                            }

                        }
                        else // แสดงผลรวมช่องสุดท้าย
                        {
                            int qty = 0;
                            e.Row.Cells[_loop2].BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");

                            _resultDB2 = int.Parse(ViewState["rowCoutList2"].ToString());

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.dept_name_th == e.Row.Cells[0].Text

                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th

                                                   }).ToList();


                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {
                                qty += int.Parse(count__linqTest[z].count_plant_idx.ToString());
                            }

                            e.Row.Cells[_loop2].Text = qty.ToString();

                        }

                        e.Row.Cells[_loop2].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");


                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant"];
                    int count_test = 0;
                    string _header_idx = String.Empty;
                    string countrow = "";
                    int count_last = e.Row.Cells.Count;

                    foreach (var test in _templist)
                    {

                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList"] = count_test;
                        _header_idx += test.plant_idx + ",";



                        e.Row.Cells[0].Text = "ผลรวม";
                        e.Row.Cells[0].Style["font-weight"] = "bold";



                        //if (e.Row.Cells[count_test].Text == e.Row.Cells[e.Row.Cells.Count - 1].Text)
                        //{
                        int qty_total = 0;

                        Report_DashBoardDetail[] _item_Result_last = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx"];

                        var count__linqTest_last = (from data in _item_Result_last

                                                    select new
                                                    {
                                                        data.count_plant_idx,
                                                        data.dept_name_th

                                                    }).ToList();


                        for (int z = 0; z < count__linqTest_last.Count(); z++)
                        {
                            qty_total += int.Parse(count__linqTest_last[z].count_plant_idx.ToString());
                        }

                        e.Row.Cells[count_last - 1].Text = qty_total.ToString();


                        if (e.Row.Cells[count_test].Text != e.Row.Cells[0].Text)
                        {
                            int qty_count = 0;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.plant_idx == test.plant_idx
                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th,
                                                       data.plant_idx,
                                                       data.LocName
                                                   }).ToList();

                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {

                                if (test.plant_idx.ToString() == count__linqTest[z].plant_idx.ToString())
                                {
                                    qty_count += int.Parse(count__linqTest[z].count_plant_idx.ToString());

                                }
                            }

                            e.Row.Cells[count_test].Text = qty_count.ToString();
                        }


                        e.Row.Cells[count_last - 1].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[count_test].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;


                    }





                }
                break;

            case "GvAppsent":
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Appsent"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList_Appsent"] = count_test;
                    }


                    int _resultDB = int.Parse(ViewState["rowCoutList_Appsent"].ToString());

                    //litDebug.Text = _resultDB.ToString();

                    int _numrowDB = _resultDB;
                    int _numrowDB_div = _resultDB - 1;
                    for (int _loop = 0; _loop < e.Row.Cells.Count; _loop++)
                    {
                        if (_loop == 0)
                        {
                            e.Row.Cells[_loop].Text = "ฝ่าย";
                        }
                        else if (_loop <= _resultDB) // หัว
                        {



                            string _text_header = String.Empty;
                            int _countrow1 = 1;
                            foreach (var _loop_rowText in _templist)
                            {

                                _text_header = _loop_rowText.LocName.ToString();
                                e.Row.Cells[_countrow1].Text = _text_header.ToString();
                                _countrow1++;
                            }
                        }
                        else
                        {

                        }

                    }


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Appsent"];
                    int count_test = 0;
                    string countrow = "";
                    int count_last = e.Row.Cells.Count;

                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList2_Appsent"] = count_test;

                    }

                    _resultDB2 = int.Parse(ViewState["rowCoutList2_Appsent"].ToString());

                    for (int _loop2 = 0; _loop2 < e.Row.Cells.Count; _loop2++)
                    {
                        if (_loop2 == 0)
                        {
                            // e.Row.Cells[_loop2].Text = "sample code";
                        }
                        else if (_loop2 <= _resultDB2)
                        {
                            BoundField field = (BoundField)((DataControlFieldCell)e.Row.Cells[_loop2]).ContainingField;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Appsent"];

                            var _linqTest = (from data in _item_Result
                                             where data.LocName == field.HeaderText
                                             select new
                                             {
                                                 data.count_plant_idx,
                                                 data.dept_name_th,
                                                 data.emp_type_idx,
                                                 data.LocName
                                             }).ToList();


                            int test_show = 0;

                            e.Row.Cells[_loop2].Text = "0";

                            for (int z = 0; z < _linqTest.Count(); z++)
                            {

                                test_show += int.Parse(_linqTest[z].count_plant_idx.ToString());

                                if (e.Row.Cells[0].Text == _linqTest[z].dept_name_th.ToString())
                                {

                                    e.Row.Cells[_loop2].Text = _linqTest[z].count_plant_idx.ToString();

                                }
                            }


                        }
                        else // แสดงผลรวมช่องสุดท้าย
                        {
                            //e.Row.Cells[_loop2].Text = "-";

                            int qty = 0;
                            e.Row.Cells[_loop2].BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");

                            _resultDB2 = int.Parse(ViewState["rowCoutList2_Appsent"].ToString());

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Appsent"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.dept_name_th == e.Row.Cells[0].Text

                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th

                                                   }).ToList();


                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {
                                qty += int.Parse(count__linqTest[z].count_plant_idx.ToString());
                            }

                            e.Row.Cells[_loop2].Text = qty.ToString();

                        }
                       
                        e.Row.Cells[_loop2].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                    }

                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");


                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Appsent"];
                    int count_test = 0;
                    int count_last = e.Row.Cells.Count;

                    foreach (var test in _templist)
                    {

                        count_test++;
                        ViewState["rowCoutList"] = count_test;

                        e.Row.Cells[0].Text = "ผลรวม";
                        e.Row.Cells[0].Style["font-weight"] = "bold";

                        int qty_total = 0;

                        Report_DashBoardDetail[] _item_Result_last = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Appsent"];

                        var count__linqTest_last = (from data in _item_Result_last

                                                    select new
                                                    {
                                                        data.count_plant_idx,
                                                        data.dept_name_th

                                                    }).ToList();


                        for (int z = 0; z < count__linqTest_last.Count(); z++)
                        {
                            qty_total += int.Parse(count__linqTest_last[z].count_plant_idx.ToString());
                        }

                        e.Row.Cells[count_last - 1].Text = qty_total.ToString();

                       
                        if (e.Row.Cells[count_test].Text != e.Row.Cells[0].Text)
                        {
                            int qty_count = 0;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Appsent"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.plant_idx == test.plant_idx &&
                                                   data.emp_type_idx == test.emp_type_idx
                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th,
                                                       data.plant_idx,
                                                       data.LocName,
                                                       data.emp_type_idx
                                                   }).ToList();


                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {

                                if (test.plant_idx.ToString() == count__linqTest[z].plant_idx.ToString() && test.emp_type_idx.ToString() == count__linqTest[z].emp_type_idx.ToString())
                                {
                                    qty_count += int.Parse(count__linqTest[z].count_plant_idx.ToString());

                                }
                            }

                            e.Row.Cells[count_test].Text = qty_count.ToString();
                        }

                        e.Row.Cells[count_test].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                        e.Row.Cells[count_last - 1].HorizontalAlign = HorizontalAlign.Center;
                    }


                    int RowIndex = e.Row.RowIndex;
                    int DataItemIndex = e.Row.DataItemIndex;
                    int Columnscount = e.Row.Cells.Count;
                    GridViewRow row = new GridViewRow(RowIndex, DataItemIndex, DataControlRowType.Footer, DataControlRowState.Normal);


                    for (int i = 0; i < Columnscount; i++)
                    {

                        TableCell tablecell = new TableCell();

                        if (i == 0)
                        {
                            tablecell.Text = "ผลรวมแต่ละสถานที่";
                            tablecell.Style["font-weight"] = "bold";
                            row.Cells.Add(tablecell);

                        }
                        else if (i != Columnscount - 1)
                        {
                            int qty_count_add = 0;


                            BoundField field = (BoundField)((DataControlFieldCell)e.Row.Cells[i]).ContainingField;
                            BoundField field_before = (BoundField)((DataControlFieldCell)e.Row.Cells[i - 1]).ContainingField;

                            string header_cut = field.HeaderText.Replace("รายวัน ", "").Replace("รายเดือน ", "");
                            string header_before = field_before.HeaderText.Replace("รายวัน ", "").Replace("รายเดือน ", "");

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Appsent"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.Plant_name == header_cut
                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th,
                                                       data.plant_idx,
                                                       data.LocName,
                                                       data.emp_type_idx,
                                                       data.Plant_name
                                                   }).ToList();


                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {

                                if (header_cut == count__linqTest[z].Plant_name.ToString())
                                {
                                    qty_count_add += int.Parse(count__linqTest[z].count_plant_idx.ToString());

                                }

                            }

                            tablecell.Text = qty_count_add.ToString();

                            //txt.Text += header_cut + " ,,,,,, " + header_before + "////////";

                            if (header_cut == header_before)
                            {
                                row.Cells[i - 1].Visible = false;
                                tablecell.ColumnSpan = 2;
                            }
                            else
                            {
                                tablecell.ColumnSpan = tablecell.ColumnSpan + 1;
                            }

                            row.Cells.Add(tablecell);

                        }
                        else
                        {
                            tablecell.Text = e.Row.Cells[i].Text;
                            row.Cells.Add(tablecell);
                        }

                        tablecell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffcc");

                        row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        row.Cells[0].HorizontalAlign = HorizontalAlign.Left;


                    }
                    this.GvAppsent.Controls[0].Controls.Add(row);

                }
                break;

            case "GvResign":
                int _resultDB2_resign = 0;
                if (e.Row.RowType == DataControlRowType.Header)
                {

                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Resign"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList_Resign"] = count_test;
                    }


                    int _resultDB = int.Parse(ViewState["rowCoutList_Resign"].ToString());

                    //litDebug.Text = _resultDB.ToString();

                    int _numrowDB = _resultDB;
                    int _numrowDB_div = _resultDB - 1;
                    for (int _loop = 0; _loop < e.Row.Cells.Count; _loop++)
                    {
                        if (_loop == 0)
                        {
                            e.Row.Cells[_loop].Text = "ฝ่าย";
                        }
                        else if (_loop <= _resultDB) // หัว
                        {



                            string _text_header = String.Empty;
                            int _countrow1 = 1;
                            foreach (var _loop_rowText in _templist)
                            {

                                _text_header = _loop_rowText.LocName.ToString();
                                e.Row.Cells[_countrow1].Text = _text_header.ToString();
                                _countrow1++;
                            }
                        }
                        else
                        {
                        }

                    }


                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Resign"];
                    int count_test = 0;
                    string countrow = "";
                    foreach (var test in _templist)
                    {
                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList_Resign"] = count_test;
                    }


                    _resultDB2_resign = int.Parse(ViewState["rowCoutList_Resign"].ToString());

                    for (int _loop2 = 0; _loop2 < e.Row.Cells.Count; _loop2++)
                    {
                        if (_loop2 == 0)
                        {
                            // e.Row.Cells[_loop2].Text = "sample code";
                        }
                        else if (_loop2 <= _resultDB2_resign)
                        {


                            string sample_codeselect = string.Empty;

                            sample_codeselect = e.Row.Cells[0].Text;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Resign"];

                            var _linqTest = (from data in _item_Result
                                             where data.plant_idx == int.Parse(e.Row.Cells[_loop2].Text)

                                             select new
                                             {
                                                 data.count_plant_idx,
                                                 data.dept_name_th

                                             }).ToList();


                            int test_show = 0;

                            e.Row.Cells[_loop2].Text = "0";
                            for (int z = 0; z < _linqTest.Count(); z++)
                            {

                                test_show += int.Parse(_linqTest[z].count_plant_idx.ToString());

                                if (e.Row.Cells[0].Text == _linqTest[z].dept_name_th.ToString())
                                {

                                    e.Row.Cells[_loop2].Text = _linqTest[z].count_plant_idx.ToString();

                                }
                            }

                        }
                        else // แสดงผลรวมช่องสุดท้าย
                        {
                            int qty = 0;
                            e.Row.Cells[_loop2].BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");

                            _resultDB2_resign = int.Parse(ViewState["rowCoutList_Resign"].ToString());

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Resign"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.dept_name_th == e.Row.Cells[0].Text

                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th

                                                   }).ToList();


                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {
                                qty += int.Parse(count__linqTest[z].count_plant_idx.ToString());
                            }

                            e.Row.Cells[_loop2].Text = qty.ToString();

                        }
                        e.Row.Cells[_loop2].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#d9edf7");


                    Report_DashBoardDetail[] _templist = (Report_DashBoardDetail[])ViewState["vs_listHeaderPlant_Resign"];
                    int count_test = 0;
                    string _header_idx = String.Empty;
                    string countrow = "";
                    int count_last = e.Row.Cells.Count;

                    foreach (var test in _templist)
                    {

                        countrow += test.LocName.ToString();
                        count_test++;
                        ViewState["rowCoutList"] = count_test;
                        _header_idx += test.plant_idx + ",";



                        e.Row.Cells[0].Text = "ผลรวม";
                        e.Row.Cells[0].Style["font-weight"] = "bold";



                        //if (e.Row.Cells[count_test].Text == e.Row.Cells[e.Row.Cells.Count - 1].Text)
                        //{
                        int qty_total = 0;

                        Report_DashBoardDetail[] _item_Result_last = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Resign"];

                        var count__linqTest_last = (from data in _item_Result_last

                                                    select new
                                                    {
                                                        data.count_plant_idx,
                                                        data.dept_name_th

                                                    }).ToList();


                        for (int z = 0; z < count__linqTest_last.Count(); z++)
                        {
                            qty_total += int.Parse(count__linqTest_last[z].count_plant_idx.ToString());
                        }

                        e.Row.Cells[count_last - 1].Text = qty_total.ToString();


                        if (e.Row.Cells[count_test].Text != e.Row.Cells[0].Text)
                        {
                            int qty_count = 0;

                            Report_DashBoardDetail[] _item_Result = (Report_DashBoardDetail[])ViewState["vs_listCount_Plantidx_Resign"];

                            var count__linqTest = (from data in _item_Result
                                                   where data.plant_idx == test.plant_idx
                                                   select new
                                                   {
                                                       data.count_plant_idx,
                                                       data.dept_name_th,
                                                       data.plant_idx,
                                                       data.LocName
                                                   }).ToList();

                            for (int z = 0; z < count__linqTest.Count(); z++)
                            {

                                if (test.plant_idx.ToString() == count__linqTest[z].plant_idx.ToString())
                                {
                                    qty_count += int.Parse(count__linqTest[z].count_plant_idx.ToString());

                                }
                            }

                            e.Row.Cells[count_test].Text = qty_count.ToString();
                        }

                        e.Row.Cells[count_last - 1].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[count_test].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                    }





                }
                break;
        }
    }
    #endregion
    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_hr_dashboard callServicePostReportDashBoard(string _cmdUrl, data_hr_dashboard _dthr_dashboard)
    {
        _localJson = _funcTool.convertObjectToJson(_dthr_dashboard);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dthr_dashboard = (data_hr_dashboard)_funcTool.convertJsonToObject(typeof(data_hr_dashboard), _localJson);


        return _dthr_dashboard;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }
    #endregion


    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            switch (ddlName.ID)
            {

                case "ddltype_report":
                    if (ddltype_report.SelectedValue == "1")
                    {
                        div_searchdate.Visible = true;
                        div_searchmonth.Visible = false;
                    }
                    else if (ddltype_report.SelectedValue == "2")
                    {
                        div_searchdate.Visible = true;
                        div_searchmonth.Visible = false;
                    }
                    else
                    {
                        div_searchdate.Visible = false;
                        div_searchmonth.Visible = true;
                    }

                    div_chart_dashboard.Visible = false;
                    div_Table_Appsent.Visible = false;
                    div_Table_Resign.Visible = false;
                    break;
            }
        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "CmdSearch":

                if (ddltype_report.SelectedValue == "1")
                {
                    Select_ChartGroup(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);
                    Select_ChartJobGrade(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);
                    Select_ChartPlant(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);
                    Select_ChartSex(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);
                    Select_TabelOnlyHeadder(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);
                    div_chart_dashboard.Visible = true;
                    div_Table_Appsent.Visible = false;
                    div_Table_Resign.Visible = false;
                }
                else if (ddltype_report.SelectedValue == "2")
                {
                    div_chart_dashboard.Visible = false;
                    div_Table_Appsent.Visible = true;
                    div_Table_Resign.Visible = false;
                    Select_Appsent(int.Parse(ddlOrg_report.SelectedValue), AddStartdate_report.Text);

                }
                else if (ddltype_report.SelectedValue == "3")
                {
                    div_chart_dashboard.Visible = false;
                    div_Table_Appsent.Visible = false;
                    div_Table_Resign.Visible = true;
                    Select_Resign(int.Parse(ddlOrg_report.SelectedValue), ddlyear.SelectedValue, ddlmonth.SelectedValue);
                }
                break;
            case "btnrefresh":
                div_chart_dashboard.Visible = false;
                div_Table_Appsent.Visible = false;
                div_Table_Resign.Visible = false;
                AddStartdate_report.Text = String.Empty;
                ddlOrg_report.SelectedValue = "0";
                ddlmonth.SelectedValue = "0";
                ddlyear.SelectedValue = "0";
                break;
        }



    }
    #endregion
}