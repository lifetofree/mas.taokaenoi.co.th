﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ManageJobs.aspx.cs" Inherits="websystem_Sir_TEST_ManageJobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script>
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(function() {
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.datepickerBlock').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY',
                minDate: moment(),
                maxDate: moment().add(720, 'days'),
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.datepickerBlock').datetimepicker({
                    useCurrent: false,
                    format: 'DD/MM/YYYY',
                    minDate: moment(),
                    maxDate: moment().add(720, 'days'),
                });
            });
        });

    </script>

    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');
        }
        function openModal2() {
            $('#ordine2').modal('show');
        }
        function openModal3() {
            $('#ordine').modal('hide');
        }
    </script>

    <div id="divMenu_bar" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ulActiveMenu" runat="server">
                    <li id="IndexList" runat="server">
                        <asp:LinkButton ID="lbMenu0" CssClass="btn_menulist" runat="server" CommandName="btnViewSelected" OnCommand="btnCommand">ข้อมูลเบื้องต้น</asp:LinkButton>

                    </li>
                    <li id="Li1" runat="server">
                        <asp:LinkButton ID="lbMenu1" CssClass="btn_menulist" runat="server" CommandName="btnViewSelected1" OnCommand="btnCommand">โอนข้อมูลเป็นพนักงาน</asp:LinkButton>
                    </li>
                </ul>
            </div>
        </nav>
    </div>




    <asp:Literal ID="test" runat="server"></asp:Literal>
    <asp:HyperLink runat="server" ID="txtfocus_vLONList" />
    <div class="container col-md-12">
        <asp:MultiView ID="MvtestMaster" runat="server" ActiveViewIndex="0">

            <asp:View ID="ViewAdd1" runat="server">
                <div class="col-md-12" role="main">
                    <asp:FormView ID="FormView1" runat="server" Width="100%" DefaultMode="Insert">
                        <InsertItemTemplate>
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading" style="margin-top: 2%; margin-left: -1%; margin-bottom: -1%;">
                                    <h4><i class="glyphicon glyphicon-search"></i>&nbsp;<b>ค้นหาข้อมูล</b> <small>(Search)</small></h4>
                                </div>
                                <div class="panel panel-default">
                                    <br />
                                    <div class="form-group">
                                        <div class="col-md-9">
                                            <div class="col-md-3 control-label" style="margin-left: 3%;">
                                                <span>ค้นหาจากตำแหน่ง/อายุ :</span>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddwork" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="tbage" runat="server" CssClass="form-control" placeholder="อายุ" MaxLength="2"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="PostcodeALNum" runat="server"
                                                    ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                    SetFocusOnError="true"
                                                    Display="Dynamic"
                                                    Font-Size="11" ForeColor="Red"
                                                    ControlToValidate="tbage"
                                                    ValidationExpression="^[0-9]{1,2}$"
                                                    ValidationGroup="formInsert" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-10">
                                            <div class="col-md-3 control-label" style="margin-left: 0%;">
                                                <span>ระบุวันที่ค้นหา :</span>
                                            </div>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="selectchang">
                                                    <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                    <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                    <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                    <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Panel ID="Hinddate" runat="server" Visible="false">
                                                <div class="col-md-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="date_S" runat="server" CssClass="form-control" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="dateStat" runat="server" CssClass="form-control" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8">
                                            <div class="col-md-12" style="margin-left: 3%;">
                                                <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="btnSearch"
                                                    OnCommand="btnCommand"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                                <span>
                                                    <asp:LinkButton ID="reset" CssClass="btn btn-info" runat="server" Text="Reset" CommandName="back" OnCommand="btnCommand"><i class="fa fa-refresh"></i>&nbsp;Reset</asp:LinkButton>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr class="so_sweethr006" />

                            <div style="margin-top: 4%;">
                                <h4>ข้อมูลผู้สมัคร<small> (List Applicant)</small></h4>
                            </div>
                            <asp:Panel ID="Div1F1" runat="server" Visible="true">
                                <asp:GridView ID="gvEmpPrimary"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    DataKeyNames="EmpPrimary_IDX"
                                    HeaderStyle-Height="40px"
                                    ShowFooter="False"
                                    ShowHeaderWhenEmpty="True"
                                    AllowPaging="True"
                                    PageSize="5"
                                    BorderStyle="None"
                                    CellSpacing="2"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Gvbuyequipment_RowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <%# (Container.DataItemIndex +1) %>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Name_Lastname" runat="server" CssClass="col-sm-12" Text='<%# Eval("PrefixNameTH") + " " + Eval("Name_Emp")+ " " + Eval("Lastname_Emp") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อายุ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%-- <div class="panel-heading">--%>
                                                <asp:Label ID="Age" runat="server" CssClass="col-sm-12" Text='<%# Eval("Age_Emp") %>'></asp:Label>
                                                <%-- </div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 1" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Po1" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameEN") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 2" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Po2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สมัคร" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Date" runat="server" CssClass="col-sm-12" Text='<%# Eval("Date_Emp") %>'></asp:Label>
                                                <%-- </div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstate_ok" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                <asp:Label ID="lbstate_wait" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                <asp:Label ID="lbstate_no" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                <asp:Label ID="lblhoderstate" Visible="false" runat="server" Text='<%# Bind("Status_EmpPrimary") %>'></asp:Label>
                                                <%--<asp:Label ID="lblrt_Holder" runat="server" Text=""></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:LinkButton ID="DeleteFile" runat="server" CommandArgument='<%# Eval("EmpPrimary_IDX") %>' data-toggle="tooltip" OnCommand="btnCommand" CssClass="btn btn-danger" CommandName="Emp"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>

                            <asp:Panel ID="Div2F1" runat="server" Visible="false">
                                <asp:GridView ID="gvEmpPrimaryF2"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    DataKeyNames="EmpPrimary_IDX"
                                    HeaderStyle-Height="40px"
                                    ShowFooter="False"
                                    ShowHeaderWhenEmpty="True"
                                    AllowPaging="True"
                                    PageSize="5"
                                    BorderStyle="None"
                                    CellSpacing="2"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Gvbuyequipment_RowDataBound">
                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="#" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%--<%--<div class="panel-heading">--%>
                                                <%# (Container.DataItemIndex +1) %>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium" HeaderStyle-Height="70%">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Name_LastnameF2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PrefixNameTH") + " " + Eval("Name_Emp")+ " " + Eval("Lastname_Emp") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อายุ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="AgeF2" runat="server" CssClass="col-sm-12" Text='<%# Eval("Age_Emp") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 1" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Po1F2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameEN") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 2" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="Po2F2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันที่สมัคร" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:Label ID="DateF2" runat="server" CssClass="col-sm-12" Text='<%# Eval("Date_Emp") %>'></asp:Label>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstate_ok" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                <asp:Label ID="lbstate_wait" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                <asp:Label ID="lbstate_no" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                <asp:Label ID="lblhoderstate" Visible="false" runat="server" Text='<%# Bind("Status_EmpPrimary") %>'></asp:Label>
                                                <%--<asp:Label ID="lblrt_Holder" runat="server" Text=""></asp:Label>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <%--<div class="panel-heading">--%>
                                                <asp:LinkButton ID="DeleteFileF2" runat="server" CommandArgument='<%# Eval("EmpPrimary_IDX") %>' data-toggle="tooltip" OnCommand="btnCommand" CssClass="btn btn-danger" CommandName="Emp"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>


                            <!-- Modal -->
                            <asp:UpdatePanel ID="UpdatePanelModul" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-lg-12" runat="server" id="Div2">
                                        <div id="ordine" class="modal open" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <%--<button type="button" class="close" data-dismiss="modal">×</button>--%>
                                                        <h4 class="modal-title">ข้อมูลผู้สมัครงาน</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:Repeater ID="RepeaterEmp" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                            <ItemTemplate>
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <h4><span class="label label-default">ประวัติส่วนตัว</span></h4>
                                                                        <div class="col-md-offset-1">
                                                                            <b>

                                                                                <asp:Label ID="H1" runat="server" Text="ชื่อ :"></asp:Label></b>&nbsp; &nbsp; &nbsp;&nbsp; 
                                                                            <asp:Label ID="Name_Last" runat="server" Text='<%# Eval("PrefixNameTH") + " " + Eval("Name_Emp")+ " " + Eval("Lastname_Emp") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H2" runat="server" Text="อายุ :"></asp:Label></b>&nbsp; &nbsp; &nbsp;
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Age_Emp") %>' />
                                                                            <asp:Label ID="Label18" runat="server" Text="ปี"></asp:Label>
                                                                            <br />
                                                                            <b>
                                                                                <asp:Label ID="H4" runat="server" Text="Email :"></asp:Label></b>&nbsp;&nbsp; 
                                                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Email") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H3" runat="server" Text="เบอร์โทรติดต่อ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Phone_number") %>' /><br />
                                                                        </div>
                                                                    </div>

                                                                    <h4><span class="label label-default">การศึกษา</span></h4>
                                                                    <asp:Panel ID="No_Edu" runat="server" Visible="false">
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-1">
                                                                                <b><asp:Label ID="Label192" runat="server" Text="ไม่มีข้อมูล"></asp:Label></b>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="Yes_Edu" runat="server" Visible="true">
                                                                    <div class="form-group">
                                                                        <div class="col-md-offset-1">
                                                                            <b>
                                                                                <asp:Label ID="H5" runat="server" Text="ชื่อสถาบัน :"></asp:Label></b>&nbsp;&nbsp; 
                                                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("NameINstitute") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H6" runat="server" Text="ระดับการศึกษา :"></asp:Label></b>&nbsp; &nbsp;&nbsp;
                                                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("Edu_name") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H7" runat="server" Text="สาขา/วิชาที่ศึกษา :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("Course") %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                    </asp:Panel>

                                                                    <div class="form-group">
                                                                        <h4><span class="label label-default">ตำแหน่งงานที่สนใจ</span></h4>
                                                                        <div class="col-md-offset-1">
                                                                            <b>
                                                                                <asp:Label ID="H8" runat="server" Text="ตำแหน่งที่ 1 :"></asp:Label></b>&nbsp;&nbsp; 
                                                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("PosGroupNameEN") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H9" runat="server" Text="ตำแหน่งที่ 2 :"></asp:Label></b>&nbsp;&nbsp;
                                                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("PosGroupNameTH") %>' /><br />
                                                                            <b>
                                                                                <asp:Label ID="H10" runat="server" Text="เงินเดือนที่ต้องการ :"></asp:Label></b>&nbsp;
                                                                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("WantMoney") %>' />
                                                                            <asp:Label ID="Label191" runat="server" Text="บาท"></asp:Label><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-12">
                                                            <asp:UpdateProgress ID="Progress" runat="server" AssociatedUpdatePanelID="Botton">
                                                                <ProgressTemplate>
                                                                    <div class="Div1">
                                                                        <h3 class="text-center">กรุณารอสักครู่</h3>
                                                                        <img class="img-responsive center-block" width="210" height="210" src='<%= ResolveUrl("~/masterpage/images/Waiting.gif") %>' />
                                                                    </div>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                            <asp:UpdatePanel ID="Botton" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="pass" class="btn btn-success" title="ผ่าน" runat="server" CommandName="pass" OnClientClick="return confirm('คุณต้องการยืนยันใช่หรือไม่ ?')" OnCommand="btnCommand"><i class="fa fa-plus-square"></i>&nbsp;ผ่าน</asp:LinkButton>
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>

            <asp:View ID="View1" runat="server">
                <div class="col-md-12" role="main">
                    <asp:FormView ID="FormView2" runat="server" Width="100%" DefaultMode="Insert">
                        <InsertItemTemplate>
                            <div class="form-group">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading" style="margin-top: 2%; margin-left: -1%; margin-bottom: -1%;">
                                        <h4><i class="glyphicon glyphicon-search"></i>&nbsp;<b>ค้นหาข้อมูล</b> <small>(Search)</small></h4>
                                    </div>
                                    <div class="panel panel-default">
                                        <br />
                                        <div class="form-group">
                                            <div class="col-md-9">
                                                <div class="col-md-3 control-label" style="margin-left: 3%;">
                                                    <span>ค้นหาจากตำแหน่ง/อายุ :</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddworkMain" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-md-3">
                                                    <asp:TextBox ID="tbageMain" runat="server" CssClass="form-control" placeholder="อายุ" MaxLength="2"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="PostcodeALNum" runat="server"
                                                        ErrorMessage="&nbsp;*เฉพาะตัวเลขเท่านั้น"
                                                        SetFocusOnError="true"
                                                        Display="Dynamic"
                                                        Font-Size="11" ForeColor="Red"
                                                        ControlToValidate="tbageMain"
                                                        ValidationExpression="^[0-9]{1,2}$"
                                                        ValidationGroup="formInsert" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-10">
                                                <div class="col-md-3 control-label" style="margin-left: 0%;">
                                                    <span>ระบุวันที่ค้นหา :</span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSearchDateMain" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="selectchang">
                                                        <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:Panel ID="Hinddate2" runat="server" Visible="false">
                                                    <div class="col-md-3">
                                                        <div class='input-group date from-date-datepicker'>
                                                            <asp:TextBox ID="date_SMain" runat="server" CssClass="form-control" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class='input-group date from-date-datepicker'>
                                                            <asp:TextBox ID="dateStatMain" runat="server" CssClass="form-control" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <div class="col-md-12" style="margin-left: 3%;">
                                                    <asp:LinkButton ID="btnSearchMain" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="btnSearchMain"
                                                        OnCommand="btnCommand"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                                    <span>
                                                        <asp:LinkButton ID="reset" CssClass="btn btn-info" runat="server" Text="Reset" CommandName="backMain" OnCommand="btnCommand"><i class="fa fa-refresh"></i>&nbsp;Reset</asp:LinkButton>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="so_sweethr006" />

                                    <div style="margin-top: 4%;">
                                        <h4>ข้อมูลผู้สมัคร<small> (List Applicant)</small></h4>
                                    </div>
                                    <asp:Panel ID="Div1F2" runat="server" Visible="true">
                                        <asp:GridView ID="gvEmpMain"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-CssClass="table_headCenter"
                                            DataKeyNames="EmpIDX"
                                            HeaderStyle-Height="40px"
                                            ShowFooter="False"
                                            ShowHeaderWhenEmpty="True"
                                            AllowPaging="True"
                                            PageSize="5"
                                            BorderStyle="None"
                                            CellSpacing="2"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowDataBound="Gvbuyequipment_RowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <%# (Container.DataItemIndex +1) %>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อ-นามสกุล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Name_Lastname" runat="server" CssClass="col-sm-12" Text='<%# Eval("PrefixNameTH") + " " + Eval("FirstNameTH")+ " " + Eval("LastNameTH") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="อายุ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Age" runat="server" CssClass="col-sm-12" Text='<%# Eval("Age") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 1" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Po1" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameEN") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 2" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Po2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่ส่งเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Date" runat="server" CssClass="col-sm-12" Text='<%# Eval("EmpCreate") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstate_ok" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                        <asp:Label ID="lbstate_wait" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                        <asp:Label ID="lbstate_no" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                        <asp:Label ID="lblhoderstate" Visible="false" runat="server" Text='<%# Bind("StatusEmpMain") %>'></asp:Label>
                                                        <%--<asp:Label ID="lblrt_Holder" runat="server" Text=""></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="6%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:LinkButton ID="DeleteFile2" runat="server" CommandArgument='<%# Eval("EmpIDX") %>' data-toggle="tooltip" OnCommand="btnCommand" CssClass="btn btn-danger" CommandName="EmpMain"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>

                                    <asp:Panel ID="Div2F2" runat="server" Visible="false">
                                        <asp:GridView ID="gvEmpMainS"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-CssClass="table_headCenter"
                                            DataKeyNames="EmpIDX"
                                            HeaderStyle-Height="40px"
                                            ShowFooter="False"
                                            ShowHeaderWhenEmpty="True"
                                            AllowPaging="True"
                                            PageSize="5"
                                            BorderStyle="None"
                                            CellSpacing="2"
                                            OnPageIndexChanging="Master_PageIndexChanging"
                                            OnRowDataBound="Gvbuyequipment_RowDataBound">
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <%# (Container.DataItemIndex +1) %>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ชื่อ-นามสกุล" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Name_Lastname2" runat="server" CssClass="col-sm-12" Text='<%# Eval("PrefixNameTH") + " " + Eval("FirstNameTH")+ " " + Eval("LastNameTH") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="อายุ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Age2" runat="server" CssClass="col-sm-12" Text='<%# Eval("Age") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 1" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Po12" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameEN") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ตำแหน่งงานที่สนใจ 2" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Po22" runat="server" CssClass="col-sm-12" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่ส่งเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:Label ID="Date2" runat="server" CssClass="col-sm-12" Text='<%# Eval("EmpCreate") %>'></asp:Label>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbstate_ok" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                        <asp:Label ID="lbstate_wait" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                        <asp:Label ID="lbstate_no" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-hourglass"></i></asp:Label>
                                                        <asp:Label ID="lblhoderstate" Visible="false" runat="server" Text='<%# Bind("StatusEmpMain") %>'></asp:Label>
                                                        <%--<asp:Label ID="lblrt_Holder" runat="server" Text=""></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="6%" HeaderStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%--<div class="panel-heading">--%>
                                                        <asp:LinkButton ID="DeleteFile22" runat="server" CommandArgument='<%# Eval("EmpIDX") %>' data-toggle="tooltip" OnCommand="btnCommand" CssClass="btn btn-danger" CommandName="EmpMain"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                        <%--</div>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>

            <asp:View ID="View2" runat="server">
                <div class="col-md-12" role="main">
                    <asp:FormView ID="FormView3" runat="server" Width="100%" DefaultMode="Insert">
                        <InsertItemTemplate>
                            <div class="form-group">
                                <asp:UpdatePanel ID="Profile" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h4><i class="glyphicon glyphicon-file"></i>&nbsp;<b>Profile</b> (ข้อมูลผู้สมัครงาน)</h4>
                                            </div>
                                            <h5>&nbsp;</h5>
                                            <div class="form-horizontal" role="form">

                                                <asp:Repeater ID="Logic" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" margin-top: 4%;">
                                                                    <div class="col-md-offset-2">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label1" runat="server" Text="คะแนนแบบทดสอบ Logic :"></asp:Label></b>&nbsp;&nbsp; 
                                                    <asp:Label ID="Name_Last" runat="server" Text='<%# Eval("Score_logic") %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="RepeaterEmpMain1" runat="server" OnItemDataBound="Repeater1_PreRender">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="form-group" style="margin-top: 2%;">
                                                                <div class="col-md-5">
                                                                    <div class="form-group" style="margin-left: 25%;">
                                                                        <div class="col-md-12">
                                                                            <asp:Panel ID="hiImYes" runat="server" Visible="false">
                                                                                <asp:Image ID="showIM" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# retrunpat((int)Eval("EmpIDX"))%>' Style="width: 200px; height: 200px" />
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="hiImNo" runat="server" Visible="true">
                                                                                <center><asp:Image ID="Image1" runat="server" class="img-responsive img-thumbnail" ImageUrl='<%# ResolveUrl("~/masterpage/images/Profile.png") %>' style="width: 200px; height: 200px" /></center>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                    <br />
                                                                    <div class="col-md-12">
                                                                        <div class="form-group" margin-top: 4%;">
                                                                            <div class="col-md-offset-2">
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="Label1" runat="server" Text="ชื่อ-นามสกุล :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Name_Last" runat="server" Text='<%# Eval("PrefixNameTH") + " " + Eval("FirstNameTH")+ " " + Eval("LastNameTH") %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="Label189" runat="server" Text="ชื่อเล่น :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label190" runat="server" Text='<%# Eval("NickNameTH") %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="Label19" runat="server" Text="วันเกิด :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label20" runat="server" Text='<%# Eval("Birthday") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label11" runat="server" Text="อายุ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Age") + " ปี" %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="H1" runat="server" Text="ตำแหน่งที่ที่สนใจอันดับ 1 :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("PosGroupNameEN") %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="H2" runat="server" Text="ตำแหน่งที่ที่สนใจอันดับ 2 :"></asp:Label></b>&nbsp;&nbsp;
                                                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("PosGroupNameTH") %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="H3" runat="server" Text="เงินเดือนที่ต้องการ :"></asp:Label></b>&nbsp;
                                                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("MoneyWant") + " บาท" %>' /><br />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <b>
                                                                                        <asp:Label ID="H4" runat="server" Text="วันที่พร้อมเริ่มงาน :"></asp:Label></b>&nbsp;
                                                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("AddStartdate") %>' /><br />
                                                                                </div>
                                                                                <asp:Panel ID="HidneScore" runat="server" Visible="false">
                                                                                <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label193" runat="server" Text="คะแนนแบบทดสอบ Logic :"></asp:Label></b>&nbsp;&nbsp; 
                                                    <asp:Label ID="Label194" runat="server" Text='<%# Eval("Score_logic") %>' /><br />
                                                                                </div>
                                                                                </asp:Panel>
                                                                                <asp:Panel ID="HidneScoreNo" runat="server" Visible="false">
                                                                                    <div class="form-group">
                                                                            <b>
                                                                                    <asp:Label ID="Label195" runat="server" Text="คะแนนแบบทดสอบ Logic : "></asp:Label></b><asp:Label ID="Label199" runat="server" Text='<%# Eval("Score_logic") %>' />&nbsp;คะแนน (ไม่สามารถพัฒนาได้)&nbsp;&nbsp; 
                                                                                    </div>
                                                                                </asp:Panel>
                                                                                <asp:Panel ID="Panel50" runat="server" Visible="false">
                                                                                    <div class="form-group">
                                                                            <b>
                                                                                    <asp:Label ID="Label196" runat="server" Text="คะแนนแบบทดสอบ Logic : "></asp:Label></b><asp:Label ID="Label200" runat="server" Text='<%# Eval("Score_logic") %>' />&nbsp;คะแนน (สามารถพัฒนาได้)&nbsp;&nbsp; 
                                                                                    </div>
                                                                                </asp:Panel>
                                                                                <asp:Panel ID="Panel60" runat="server" Visible="false">
                                                                                    <div class="form-group">
                                                                            <b>
                                                                                    <asp:Label ID="Label197" runat="server" Text="คะแนนแบบทดสอบ Logic : "></asp:Label></b><asp:Label ID="Label201" runat="server" Text='<%# Eval("Score_logic") %>' />&nbsp;คะแนน (มีความเหมาะสมในการรับเข้าทำงาน)&nbsp;&nbsp; 
                                                                                    </div>
                                                                                </asp:Panel>
                                                                                <asp:Panel ID="Panel70" runat="server" Visible="false">
                                                                                    <div class="form-group">
                                                                            <b>
                                                                                    <asp:Label ID="Label198" runat="server" Text="คะแนนแบบทดสอบ Logic : "></asp:Label></b><asp:Label ID="Label202" runat="server" Text='<%# Eval("Score_logic") %>' />&nbsp;คะแนน (มีความคิดริเริ่มสร้างสรรค์ มีศักยภาพดีมาก)&nbsp;&nbsp; 
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6" style="border-left:0.5px solid #e6dede;">
                                                                    <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                        <div class="col-md-offset-1 col-md-12">
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label15" runat="server" Text="ชื่อ-นามสกุล(English) :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label16" runat="server" Text='<%# Eval("PrefixNameEN") + " " + Eval("FirstNameEN")+ " " + Eval("LastNameEN") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label187" runat="server" Text="ชื่อเล่น(English) :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label188" runat="server" Text='<%# Eval("NickNameEN") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label21" runat="server" Text="จังหวัดที่เกิด :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label22" runat="server" Text='<%# Eval("ProvBirthTxt") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label23" runat="server" Text="ประเทศ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label24" runat="server" Text='<%# Eval("CountryName") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label25" runat="server" Text="จังหวัดที่อยู่ปัจจุบัน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label26" runat="server" Text='<%# Eval("ProvName") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label27" runat="server" Text="อำเภอ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label28" runat="server" Text='<%# Eval("AmpName") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label29" runat="server" Text="ตำบล :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label30" runat="server" Text='<%# Eval("DistName") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label31" runat="server" Text="รหัสไปรษณีย์ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label32" runat="server" Text='<%# Eval("PostCode") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label33" runat="server" Text="สัญชาติ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label34" runat="server" Text='<%# Eval("NatName") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label35" runat="server" Text="เชื้อชาติ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label38" runat="server" Text='<%# Eval("RaceTxt") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label36" runat="server" Text="เพศ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label37" runat="server" Text='<%# Eval("SexNameTH") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label39" runat="server" Text="น้ำหนัก :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label40" runat="server" Text='<%# Eval("Weight") + " กก." %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
                                                            <b>
                                                                <asp:Label ID="Label41" runat="server" Text="ส่วนสูง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label42" runat="server" Text='<%# Eval("Height") + " ซม." %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label43" runat="server" Text="ตำหนิ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label44" runat="server" Text='<%# Eval("Scar") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; 
                                                            <b>
                                                                <asp:Label ID="Label45" runat="server" Text="กรุ๊ปเลือด :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label46" runat="server" Text='<%# Eval("BGName") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label47" runat="server" Text="ศาสนา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label48" runat="server" Text='<%# Eval("RelNameTH") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label14" runat="server" Text="เบอร์โทรศัพท์มือถือ :"></asp:Label></b>&nbsp;
                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("MobileNo") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label49" runat="server" Text="เบอร์โทรศัพท์บ้าน :"></asp:Label></b>&nbsp;
                                                            <asp:Label ID="Label50" runat="server" Text='<%# Eval("PhoneNo") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label13" runat="server" Text="Email :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Email") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label51" runat="server" Text="ที่อยู่ปัจจุบันที่ติดต่อได้สะดวก :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label52" runat="server" Text='<%# Eval("PresentAddress") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label53" runat="server" Text="ที่อยู่ตามทะเบียนบ้าน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label54" runat="server" Text='<%# Eval("EmpAddress") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label55" runat="server" Text="เลขบัตรประชาชน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("CardID") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label57" runat="server" Text="ออกให้ ณ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label58" runat="server" Text='<%# Eval("IssuedAtCardID") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label59" runat="server" Text="วันที่ออกบัตร :"></asp:Label></b>&nbsp;&nbsp;
                                                            <asp:Label ID="Label60" runat="server" Text='<%# Eval("IssuedDateCardID") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                            <b>
                                                                <asp:Label ID="Label61" runat="server" Text="บัตรหมดอายุ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label62" runat="server" Text='<%# Eval("ExpiredDateCardID") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label63" runat="server" Text="บัตรรับรองสิทธิประกันสังคมเลขที่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label64" runat="server" Text='<%# Eval("SecurrityCard") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label65" runat="server" Text="ออกให้ ณ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label66" runat="server" Text='<%# Eval("IssuedAtSecurrity") %>' />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <b>
                                                                <asp:Label ID="Label67" runat="server" Text="บัตรหมดอายุ :" Visible="true"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label68" runat="server" Visible="true" Text='<%# Eval("ExpiredDateSecurrity") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label69" runat="server" Text="บัตรประจำตัวผู้เสียภาษีเลขที่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label70" runat="server" Text='<%# Eval("TaxCardID") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label71" runat="server" Text="ออกให้ ณ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label72" runat="server" Text='<%# Eval("IssuedAtTax") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label73" runat="server" Text="สถานะความเป็นอยู่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label74" runat="server" Text='<%# Eval("LivingStaTxt") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label75" runat="server" Text="สถานะครอบครัว :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label76" runat="server" Text='<%# Eval("NameTH") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label77" runat="server" Text="จำนวนบุตร :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label78" runat="server" Text='<%# Eval("Children") + " คน" %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label79" runat="server" Text="จำนวนบุตรที่กำลังศึกษา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label80" runat="server" Text='<%# Eval("ChildrenSchool") + " คน"  %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label81" runat="server" Text="จำนวนบุตรอายุเกิน 21 ปี :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label82" runat="server" Text='<%# Eval("Children21") + " คน"  %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label83" runat="server" Text="สถานะทางทหาร :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label84" runat="server" Text='<%# Eval("MilName") %>' />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <hr class="so_sweethr006" style="width: 95%; margin-top: -4%;" />
                                                        <div class="form-group" style="margin-left: 3.5%;">
                                                            <div class="col-md-6">
                                                                <h4><span class="label label-default">ประวัติครอบครัว</span></h4>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                        <div class="col-md-5">
                                                            <div class="form-group" style="margin-top: 1%; margin-left: -4.5%;">
                                                                <div class="col-md-offset-3 col-md-12">
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label85" runat="server" Text="ชื่อ-นามสกุลบิดา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label86" runat="server" Text='<%# Eval("FirstNameFather")+ " " + Eval("LastNameFather") %>' /><br />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label87" runat="server" Text="อายุ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label88" runat="server" Text='<%# Eval("AgeFather") + " ปี"  %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label89" runat="server" Text="อาชีพ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label90" runat="server" Text='<%# Eval("OccupationFather") %>' /><br />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label91" runat="server" Text="สถานะ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label92" runat="server" Text='<%# Eval("ALIVEName") %>' /><br />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label93" runat="server" Text="ชื่อ-นามสกุลมารดา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label94" runat="server" Text='<%# Eval("FirstNameMother")+ " " + Eval("LastNameMother") %>' /><br />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label95" runat="server" Text="อายุ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label96" runat="server" Text='<%# Eval("AgeMother") + " ปี"  %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label97" runat="server" Text="อาชีพ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label98" runat="server" Text='<%# Eval("OccupationMother") %>' /><br />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <b>
                                                                            <asp:Label ID="Label99" runat="server" Text="สถานะ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label100" runat="server" Text='<%# Eval("ALIVEName_Mother") %>' /><br />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                
                                                <div class="form-group">
                                                <div class="col-md-6">
                                                <div class="form-group" style="margin-top: -1%; margin-left: -9.5%;">
                                                <div class="col-md-offset-3 col-md-12">
                                                <asp:Repeater ID="RepeaterEmpMain2" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label101" runat="server" Text="ชื่อ-นามสกุลพี่น้อง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label102" runat="server" Text='<%# Eval("FirstNameParent")+ " " + Eval("LastNameParent") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label103" runat="server" Text="อายุ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label104" runat="server" Text='<%# Eval("AgeParent") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label105" runat="server" Text="อาชีพ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label106" runat="server" Text='<%# Eval("OccupationParent") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label107" runat="server" Text="สถานที่ทำงาน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label108" runat="server" Text='<%# Eval("FirmAddressParent") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label109" runat="server" Text="เบอร์โทรติดต่อ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label110" runat="server" Text='<%# Eval("NumberParent") %>' />
                                                                        </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                </div>
                                                </div>
                                                </div>
                                                </div>

                                                <hr class="so_sweethr006" style="width: 95%; margin-top: -4%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-6">
                                                        <h4><span class="label label-default">ประวัติการศึกษา</span></h4>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="Panel1" runat="server" Visible="false">
                                                    <div class="col-md-6">
                                                        <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                            <div class="col-md-offset-2 col-md-12">
                                                                <div class="form-group">
                                                                    <b>
                                                                        <asp:Label ID="lblMessage" runat="server" Text="ไม่มีประวัติการศึกษา"></asp:Label></b>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel2" runat="server" Visible="true">
                                                    <asp:Repeater ID="RepeaterEmpMain3" runat="server">
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                        <div class="col-md-offset-2 col-md-12">
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label101" runat="server" Text="ชื่อสถาบัน  :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label102" runat="server" Text='<%# Eval("Institute") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label103" runat="server" Text="ระดับการศึกษา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label104" runat="server" Text='<%# Eval("Edu_name") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label105" runat="server" Text="จังหวัด :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label106" runat="server" Text='<%# Eval("ProvEducation") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label107" runat="server" Text="ปีการศึกษา จาก :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label108" runat="server" Text='<%# Eval("YearAttendedStart") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label109" runat="server" Text="ถึง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label110" runat="server" Text='<%# Eval("ToAttended") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label111" runat="server" Text="วิชาที่ศึกษา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label112" runat="server" Text='<%# Eval("Course") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                        <b>
                                                            <asp:Label ID="Label183" runat="server" Text="คณะ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label184" runat="server" Text='<%# Eval("Faculty_Name") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label113" runat="server" Text="วุฒิที่ได้รับ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label114" runat="server" Text='<%# Eval("CompletedEdu") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                        <b>
                                                            <asp:Label ID="Label181" runat="server" Text="GPA :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label182" runat="server" Text='<%# Eval("Grade") %>' /><h6></h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:Panel>

                                                <%--<hr class="so_sweethr006" style="width: 95%;"/>--%>
                                                <hr class="so_sweethr006" style="width: 95%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-6">
                                                        <h4><span class="label label-default">ประวัติการทำงาน</span></h4>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="Panel3" runat="server" Visible="false">

                                                    <div class="col-md-6">
                                                        <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                            <div class="col-md-offset-2 col-md-12">
                                                                <div class="form-group">
                                                                    <b>
                                                                        <asp:Label ID="Label178" runat="server" Text="ไม่มีประวัติการทำงาน"></asp:Label></b>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel4" runat="server" Visible="true">
                                                    <asp:Repeater ID="RepeaterEmpMain4" runat="server">
                                                        <ItemTemplate>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                        <div class="col-md-offset-2 col-md-12">
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label113" runat="server" Text="ชื่อสถานประกอบการ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label114" runat="server" Text='<%# Eval("NameEmployed") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label115" runat="server" Text="เบอร์โทร :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label116" runat="server" Text='<%# Eval("TelBusiness") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label117" runat="server" Text="ประเภทธุรกิจ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label118" runat="server" Text='<%# Eval("TypeBusiness") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label121" runat="server" Text="ตำแหน่ง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label122" runat="server" Text='<%# Eval("PositionBusiness") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label123" runat="server" Text="ที่อยู่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label124" runat="server" Text='<%# Eval("AddressBusiness") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label125" runat="server" Text="ปีการทำงานจาก :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label126" runat="server" Text='<%# Eval("YearAttendedStartEmp") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label131" runat="server" Text="ถึง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label132" runat="server" Text='<%# Eval("ToAttendedEmp") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label111" runat="server" Text="ผู้บังคับบัญชา :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label112" runat="server" Text='<%# Eval("Supervisor") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label119" runat="server" Text="เงินเดือน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label120" runat="server" Text='<%# Eval("SalaryEmp") + " บาท" %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label127" runat="server" Text="เงินเดือนอื่นๆ  :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label128" runat="server" Text='<%# Eval("Revenue") + " บาท"  %>' /><br />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <b>
                                                                                    <asp:Label ID="Label129" runat="server" Text="ลักษณะงานโดยย่อ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label130" runat="server" Text='<%# Eval("WorkEmp") %>' /><br />
                                                                            </div>
                                                                            <div class="form-group" style="margin-bottom: -4%;">
                                                                                <b>
                                                                                    <asp:Label ID="Label185" runat="server" Text="สาเหตุการลาออก :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label186" runat="server" Text='<%# Eval("Resignation") %>' /><h6>&nbsp;</h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:Panel>

                                                <hr class="so_sweethr006" style="width: 95%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-6">
                                                        <h4><span class="label label-default">ความสามารถพิเศษ</span></h4>
                                                    </div>
                                                </div>
                                                <asp:Repeater ID="RepeaterEmpMain5" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                    <div class="col-md-offset-2 col-md-12">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label133" runat="server" Text="คอมพิวเตอร์/โปรแกรม :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label134" runat="server" Text='<%# Eval("Computer") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label135" runat="server" Text="พิมพ์ดีดไทย :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label136" runat="server" Text='<%# Eval("TypingTH") + " คำ/นาที" %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label137" runat="server" Text="พิมพ์ดีดีอังกฤษ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label138" runat="server" Text='<%# Eval("TypingENG") + " คำ/นาที"  %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="RepeaterEmpMain6" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                    <div class="col-md-offset-2 col-md-12">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label139" runat="server" Text="ภาษา  :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label140" runat="server" Text='<%# Eval("NameEN") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label141" runat="server" Text="การพูด :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label142" runat="server" Text='<%# Eval("PROFICName") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label143" runat="server" Text="ความเข้าใจ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label144" runat="server" Text='<%# Eval("ProficiencyUnder_txt") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label145" runat="server" Text="การอ่าน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label146" runat="server" Text='<%# Eval("ProficiencyRead_txt") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                            <b>
                                                                <asp:Label ID="Label147" runat="server" Text="การเขียน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label148" runat="server" Text='<%# Eval("ProficiencyWrint_txt") %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                <asp:Repeater ID="RepeaterEmpMain7" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group" style="margin-left: -1%; margin-top: 1%;">
                                                                    <div class="col-md-offset-2 col-md-12">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label139" runat="server" Text="ประเภทรถที่ขับขี่ได้  :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label140" runat="server" Text='<%# Eval("Name") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label141" runat="server" Text="ประเภทใบขับขี่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label142" runat="server" Text='<%# Eval("DriveLicense") %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                <hr class="so_sweethr006" style="width: 95%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-12">
                                                        <h4><span class="label label-default">ข้อมูลเพิ่มเติม</span></h4>
                                                    </div>
                                                </div>
                                                <asp:Repeater ID="RepeaterEmpMain8" runat="server">
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group" style="margin-left: 0.5%; margin-top: 1%;">
                                                                    <div class="col-md-offset-1">
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label101" runat="server" Text="ชื่อ-นามสกุลบุคคลที่ไม่ใช่ญาติ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label102" runat="server" Text='<%# Eval("FirstNameNP")+ " " + Eval("LastNameNP") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label139" runat="server" Text="ความสัมพันธ์  :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label140" runat="server" Text='<%# Eval("RelationshipNP") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label141" runat="server" Text="สถานที่ทำงาน :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label142" runat="server" Text='<%# Eval("FirmAddressNP") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label149" runat="server" Text="ตำแหน่ง :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label150" runat="server" Text='<%# Eval("PositionNP") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label151" runat="server" Text="โทรศัพท์ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label152" runat="server" Text='<%# Eval("TelNP") %>' /><br />
                                                                            <br />
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label153" runat="server" Text="ใครแนะนำให้ท่านมาสมัครงานที่นี้/หรือทราบได้อย่างไร :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label154" runat="server" Text='<%# Eval("ADVISEName") %>' /><br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label155" runat="server" Text="ท่านเคยยื่นใบสมัครหรือเคยทำงานกับบริษัทนี้มาก่อนหรือไม่? :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label156" runat="server" Text='<%# Eval("DECIName") %>' /><br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label157" runat="server" Text="ท่านเคยถูกจับหรือต้องคดีอาญาบ้างหรือไม่? :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label158" runat="server" Text='<%# Eval("HadCaptured_txt") %>' /><br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label159" runat="server" Text="ท่านเคยมีประวัติเป็นโรคภูมิแพ้/แพ้สารหรือยาอะไรบ้าง? :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label160" runat="server" Text='<%# Eval("DrugAllergy_txt") %>' /><br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label161" runat="server" Text="ท่านมีโรคประจำตัวหรือไม่? :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label162" runat="server" Text='<%# Eval("Disease_txt") %>' /><br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" style="margin-left: -3.5%;">
                                                                            <div class="col-md-6">
                                                                                <b>
                                                                                    <asp:Label ID="Label163" runat="server" Text="ท่านเคยป่วยหนัก,รับบาดแผลจากอุบัติเหตุ,รับการผ่าตัดหรือไม่? :"></asp:Label></b>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <asp:Label ID="Label164" runat="server" Text='<%# Eval("HadSurgery_txt") %>' /><br />
                                                                                <br />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label165" runat="server" Text="ในกรณีเกิดเหตุฉุกเฉินกรุณาติดต่อใคร? :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label166" runat="server" Text='<%# Eval("EmergencyFL") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label167" runat="server" Text="ความสัมพันธ์ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label168" runat="server" Text='<%# Eval("RelationEmergency") %>' />&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                                        <b>
                                                            <asp:Label ID="Label169" runat="server" Text="โทรศัพท์ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label170" runat="server" Text='<%# Eval("TelephoneEmergency") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label179" runat="server" Text="ที่อยู่ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label180" runat="server" Text='<%# Eval("AddressEmergency") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label171" runat="server" Text="ให้เขียนชื่อคนรู้จักที่ทำงานในบริษัทนี้ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label172" runat="server" Text='<%# Eval("KnowSomeone") %>' /><br />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <b>
                                                                                <asp:Label ID="Label173" runat="server" Text="ความสัมพันธ์ :"></asp:Label></b>&nbsp;&nbsp; 
                                                            <asp:Label ID="Label174" runat="server" Text='<%# Eval("RelationKS") %>' /><br />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <hr class="so_sweethr006" style="width: 95%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-12">
                                                        <h4><span class="label label-default">เอกสาร ( List Document )</span></h4>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-top: 3%;">
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-10" style="margin-left: auto; margin-right: auto;">
                                                        <asp:GridView ID="gvDocumentFile"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            DataKeyNames="File_IDX"
                                                            HeaderStyle-Height="40px"
                                                            ShowFooter="False"
                                                            ShowHeaderWhenEmpty="True"
                                                            AllowPaging="True"
                                                            PageSize="5"
                                                            BorderStyle="None"
                                                            CellSpacing="2"
                                                            OnPageIndexChanging="Master_PageIndexChanging"
                                                            OnRowDataBound="Gvbuyequipment_RowDataBound">
                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#" HeaderStyle-Width="" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <%--<div class="panel-heading">--%>
                                                                        <%# (Container.DataItemIndex +1) %>
                                                            </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Type ISO" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <%--<div class="panel-heading">--%>
                                                                        <asp:Label ID="TypeISO" runat="server" CssClass="col-sm-12" Text='<%# Eval("Name_Doc") %>'></asp:Label>
                                                                        <asp:TextBox ID="EmpIDFile" runat="server" Visible="false" Text='<%# Eval("EmpIDX") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="File" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <%--<div class="panel-heading">--%>
                                                                        <asp:HyperLink runat="server" ID="btnDLX" CssClass="btn btn-default btn-sm" Target="_blank" data-original-title="เอกสาร" NavigateUrl='<%#  retrunpatFile((string)Eval("NameDoc")) %>'><i class="fa fa-eye"></i>&nbsp;ดูไฟล์</asp:HyperLink>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <hr class="so_sweethr006" style="width: 95%;" />
                                                <div class="form-group" style="margin-left: 3.5%;">
                                                    <div class="col-md-12">
                                                        <h4><span class="label label-default">โอนข้อมูลผู้สมัครเป็นหนักงาน</span></h4>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-top: 3%;">
                                                    <div class="col-md-offset-1">
                                                        <label class="col-md-2 list-group-item-heading">
                                                            <asp:Label ID="Label30" runat="server" ForeColor="DarkBlue" Text="บริษัท"></asp:Label>
                                                            <small>
                                                                <p class="list-group-item-text">Company</p>
                                                            </small>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddcompany" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="ddPrefixEngAL0"
                                                                ValidationGroup="formInsert1"
                                                                runat="server" Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="ddcompany"
                                                                Font-Size="11" ForeColor="Red"
                                                                ErrorMessage="&nbsp;* กรุณาเลือกบริษัท"
                                                                ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-1">
                                                        <label class="col-md-2 list-group-item-heading">
                                                            <asp:Label ID="Label175" runat="server" ForeColor="DarkBlue" Text="ฝ่าย"></asp:Label>
                                                            <small>
                                                                <p class="list-group-item-text">Department</p>
                                                            </small>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddparty" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="ddPrefixEngAL1"
                                                                ValidationGroup="formInsert1"
                                                                runat="server" Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="ddparty"
                                                                Font-Size="11" ForeColor="Red"
                                                                ErrorMessage="&nbsp;* กรุณาเลือกฝ่าย"
                                                                ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-1">
                                                        <label class="col-md-2 list-group-item-heading">
                                                            <asp:Label ID="Label176" runat="server" ForeColor="DarkBlue" Text="แผนก"></asp:Label>
                                                            <small>
                                                                <p class="list-group-item-text">Section</p>
                                                            </small>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="dddepartment" runat="server" CssClass="form-control fa-align-left" AutoPostBack="true" OnSelectedIndexChanged="selectchang"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="ddPrefixEngAL2"
                                                                ValidationGroup="formInsert1"
                                                                runat="server" Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="dddepartment"
                                                                Font-Size="11" ForeColor="Red"
                                                                ErrorMessage="&nbsp;* กรุณาเลือกแผนก"
                                                                ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-1">
                                                        <label class="col-md-2 list-group-item-heading">
                                                            <asp:Label ID="Label177" runat="server" ForeColor="DarkBlue" Text="ตำแหน่ง"></asp:Label>
                                                            <small>
                                                                <p class="list-group-item-text">Position</p>
                                                            </small>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddPosition" runat="server" CssClass="form-control fa-align-left"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="ddPrefixEngAL4"
                                                                ValidationGroup="formInsert1"
                                                                runat="server" Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="ddPosition"
                                                                Font-Size="11" ForeColor="Red"
                                                                ErrorMessage="&nbsp;* กรุณาเลือกตำแหน่ง"
                                                                ValidationExpression="กรุณาเลือกคำนำหน้าชื่อ" InitialValue="00" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-1">
                                                        <label class="col-md-2 list-group-item-heading">
                                                            <asp:Label ID="Label17" runat="server" ForeColor="DarkBlue" Text="วันที่เริ่มงาน"></asp:Label>
                                                            <small>
                                                                <p class="list-group-item-text">Start date</p>
                                                            </small>
                                                        </label>
                                                        <div class="col-md-4">
                                                            <div class='input-group date datepickerBlock'>
                                                            <asp:TextBox ID="date_Start" runat="server" CssClass="form-control" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="form-group">
                                                    <div class="col-md-offset-1">
                                                        <asp:LinkButton ID="passMain" class="btn btn-success" data-toggle="tooltip" title="Send" runat="server" ValidationGroup="formInsert1" CommandName="passMain" OnClientClick="return confirm('คุณต้องการยืนยันใช่หรือไม่ ?')" OnCommand="btnCommand"><i class="fa fa-paper-plane-o"></i>&nbsp;ส่งข้อมูล</asp:LinkButton>
                                                        <asp:LinkButton ID="BackMain" class="btn btn-danger" data-toggle="tooltip" title="Cancel" runat="server" CommandName="BackMain2" OnCommand="btnCommand">ยกเลิก</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="passMain" />
                                        <asp:PostBackTrigger ControlID="BackMain" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </asp:View>

        </asp:MultiView>
    </div>
</asp:Content>

