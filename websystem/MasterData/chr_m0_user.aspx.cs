﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_chr_m0_user : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_chr _dtchr = new data_chr();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsert_Master_UserSAP = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_UserSAP"];
    static string urlSelect_Master_UserSAP = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_UserSAP"];
    static string urlUpdate_Master_UserSAP = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_UserSAP"];
    static string urlDelete_Master_UserSAP = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_UserSAP"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];

    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string urlSelect_System = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_SystemList"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
            getOrganizationList(ddlorg);
            getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));


        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
    }
    #endregion

    #region Select

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getCostCenterList(DropDownList ddlName)
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServicePostEmployee(_urlGetCostcenterOld, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.CostCenterDetail, "CostNo", "CostIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือก CostCenter...", "0"));
    }

    protected void getSystemList(DropDownList ddlName)
    {
        _dtchr = new data_chr();
        _dtchr.BoxMaster_SystemList = new MasterSystemList[1];
        MasterSystemList dataselect = new MasterSystemList();

        _dtchr.BoxMaster_SystemList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_System, _dtchr);
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        setDdlData(ddlName, _dtchr.BoxMaster_SystemList, "System_name", "Sysidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกระบบ...", "0"));

    }

    protected void SelectMasterList()
    {

        _dtchr.BoxMaster_UserSAPList = new MasterUserSAPList[1];
        MasterUserSAPList dataselect = new MasterUserSAPList();

        _dtchr.BoxMaster_UserSAPList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_Master_UserSAP, _dtchr);

        ViewState["Vs_DetailUserSapSearch"] = _dtchr.BoxMaster_UserSAPList;
        ViewState["Vs_DetailUserSap"] = _dtchr.BoxMaster_UserSAPList;
        setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);

    }

    protected void Insert_Master()
    {
        _dtchr.BoxMaster_UserSAPList = new MasterUserSAPList[1];
        MasterUserSAPList datainsert = new MasterUserSAPList();

        datainsert.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        datainsert.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        datainsert.Sysidx = int.Parse(ddlsystem.SelectedValue);
        datainsert.CostIDX = int.Parse(ddlcostidx.SelectedValue);
        datainsert.Usersap = txtname.Text;
        datainsert.U_Status = int.Parse(ddStatusadd.SelectedValue);
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BoxMaster_UserSAPList[0] = datainsert;

        _dtchr = callServicePostCHR(urlInsert_Master_UserSAP, _dtchr);
        if(_dtchr.ReturnCode == "0")
        {
            btnshow.Visible = true;
            update_Searchorg.Visible = true;
            SelectMasterList();
            getOrganizationList(ddlorg);
            getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลในระบบแล้ว ---');", true);
            btnshow.Visible = false;
            Panel_Add.Visible = true;
            
            update_Searchorg.Visible = false;


            return;
        }

    }

    protected void Delete_MasterSystem()
    {

        _dtchr.BoxMaster_UserSAPList = new MasterUserSAPList[1];
        MasterUserSAPList dataupdate = new MasterUserSAPList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.UserIDX = int.Parse(ViewState["UserIDX"].ToString());
        _dtchr.BoxMaster_UserSAPList[0] = dataupdate;

        _dtchr = callServicePostCHR(urlDelete_Master_UserSAP, _dtchr);

    }

    protected void Update_Master_List()
    {
        _dtchr.BoxMaster_UserSAPList = new MasterUserSAPList[1];
        MasterUserSAPList dataupdate = new MasterUserSAPList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.UserIDX = int.Parse(ViewState["UserIDX"].ToString());
        dataupdate.OrgIDX = int.Parse(ViewState["OrgIDX"].ToString());
        dataupdate.RDeptIDX = int.Parse(ViewState["RDeptIDX"].ToString());
        dataupdate.CostIDX = int.Parse(ViewState["CostIDX"].ToString());
        dataupdate.Usersap = ViewState["Usersap"].ToString();
        dataupdate.Sysidx = int.Parse(ViewState["Sysidx"].ToString());
        dataupdate.U_Status = int.Parse(ViewState["ddStatusUpdate"].ToString());


        _dtchr.BoxMaster_UserSAPList[0] = dataupdate;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        _dtchr = callServicePostCHR(urlUpdate_Master_UserSAP, _dtchr);
        if(_dtchr.ReturnCode == "0")
        {
            btnshow.Visible = true;
            SelectMasterList();

            //getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));

            if (int.Parse(ddlorg.SelectedValue) != 0 && int.Parse(ddldept.SelectedValue) != 0)
            {


                //litDebug.Text = "4";

                //data_chr _dtchr = new data_chr();
                MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                var _linq_FilterSearchIndex = (from data in _item_search
                                               where
                                               data.OrgIDX == int.Parse(ddlorg.SelectedValue)
                                               && (data.RDeptIDX == int.Parse(ddldept.SelectedValue))
                                               select data
                                        ).ToList();

                ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                //

                ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                //ViewState["Va_DetailCarbooking"] = null;


            }
            else if (int.Parse(ddlorg.SelectedValue) != 0)
            {
                //litDebug.Text = "5";

                //data_chr _dtchr = new data_chr();
                MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                var _linq_FilterSearchIndex = (from data in _item_search
                                               where
                                               data.OrgIDX == int.Parse(ddlorg.SelectedValue)


                                               select data
                                        ).ToList();


                ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                //ViewState["Va_DetailCarbooking"] = null;

            }
            else
            {
                //litDebug.Text = "6";
                SelectMasterList();

            }
        }
        else
        {
            btnshow.Visible = true;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลในระบบแล้ว ---');", true);

            if (int.Parse(ddlorg.SelectedValue) != 0 && int.Parse(ddldept.SelectedValue) != 0)
            {


                //litDebug.Text = "4";

                //data_chr _dtchr = new data_chr();
                MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                var _linq_FilterSearchIndex = (from data in _item_search
                                               where
                                               data.OrgIDX == int.Parse(ddlorg.SelectedValue)
                                               && (data.RDeptIDX == int.Parse(ddldept.SelectedValue))
                                               select data
                                        ).ToList();

                ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                //

                ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                //ViewState["Va_DetailCarbooking"] = null;


            }
            else if (int.Parse(ddlorg.SelectedValue) != 0)
            {
                //litDebug.Text = "5";

                //data_chr _dtchr = new data_chr();
                MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                var _linq_FilterSearchIndex = (from data in _item_search
                                               where
                                               data.OrgIDX == int.Parse(ddlorg.SelectedValue)


                                               select data
                                        ).ToList();


                ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                //ViewState["Va_DetailCarbooking"] = null;

            }
            else
            {
                //litDebug.Text = "6";
                SelectMasterList();

            }

            return;
        }



    }
    #endregion

    #region reuse
    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);

        return _dtchr;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void SetDefaultAdd()
    {
        getOrganizationList(ddlorgidx);
        getCostCenterList(ddlcostidx);
        getSystemList(ddlsystem);
        ddlrdeptidx.SelectedValue = "0";
    }

    #endregion reuse

    #region GridView
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlsysidx_edit = (DropDownList)e.Row.FindControl("ddlsysidx_edit");
                        var lbl_SysIDX_edit = (Label)e.Row.FindControl("lbl_SysIDX_edit");

                        var ddlorgidx_edit = (DropDownList)e.Row.FindControl("ddlorgidx_edit");
                        var lbl_OrgIDX_edit = (Label)e.Row.FindControl("lbl_OrgIDX_edit");

                        var ddlrdeptidx_edit = (DropDownList)e.Row.FindControl("ddlrdeptidx_edit");
                        var lbl_RDeptIDX_edit = (Label)e.Row.FindControl("lbl_RDeptIDX_edit");

                        var ddlcostidx_edit = (DropDownList)e.Row.FindControl("ddlcostidx_edit");
                        var lbl_CostIDX_edit = (Label)e.Row.FindControl("lbl_CostIDX_edit");



                        getSystemList(ddlsysidx_edit);
                        ddlsysidx_edit.SelectedValue = lbl_SysIDX_edit.Text;

                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = lbl_OrgIDX_edit.Text;

                        getDepartmentList(ddlrdeptidx_edit, int.Parse(ddlorgidx_edit.SelectedValue));
                        ddlrdeptidx_edit.SelectedValue = lbl_RDeptIDX_edit.Text;

                        getCostCenterList(ddlcostidx_edit);
                        ddlcostidx_edit.SelectedValue = lbl_CostIDX_edit.Text;

                    }

                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);

                

                //SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                ////SelectMasterList();
                setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int UserIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlsysidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlsysidx_edit");
                var ddlorgidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlrdeptidx_edit");
                var ddlcostidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlcostidx_edit");
                var txtUsersap_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtUsersap_edit");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["UserIDX"] = UserIDX;
                ViewState["Sysidx"] = ddlsysidx_edit.SelectedValue;
                ViewState["OrgIDX"] = ddlorgidx_edit.SelectedValue;
                ViewState["RDeptIDX"] = ddlrdeptidx_edit.SelectedValue;
                ViewState["CostIDX"] = ddlcostidx_edit.SelectedValue;
                ViewState["Usersap"] = txtUsersap_edit.Text;
                ViewState["ddStatusUpdate"] = ddStatusUpdate.SelectedValue;

                Update_Master_List();





                SelectMasterList();

                //getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));
                
                if (int.Parse(ddlorg.SelectedValue) != 0 && int.Parse(ddldept.SelectedValue) != 0)
                {


                    //litDebug.Text = "4";

                    data_chr _dtchr = new data_chr();
                    MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                    var _linq_FilterSearchIndex = (from data in _item_search
                                                   where
                                                   data.OrgIDX == int.Parse(ddlorg.SelectedValue)
                                                   && (data.RDeptIDX == int.Parse(ddldept.SelectedValue))
                                                   select data
                                            ).ToList();

                    ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                    //

                    ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                    setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                    //ViewState["Va_DetailCarbooking"] = null;


                }
                else if (int.Parse(ddlorg.SelectedValue) != 0)
                {
                    //litDebug.Text = "5";

                    data_chr _dtchr = new data_chr();
                    MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                    var _linq_FilterSearchIndex = (from data in _item_search
                                                   where
                                                   data.OrgIDX == int.Parse(ddlorg.SelectedValue)
                                                   

                                                   select data
                                            ).ToList();

               
                    ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                    setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else
                {
                    //litDebug.Text = "6";
                    SelectMasterList();

                }


                break;
        }
    }

    #endregion

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;

            case "ddlorgidx_edit":
                var ddNameDep = (DropDownList)sender;
                var row = (GridViewRow)ddNameDep.NamingContainer;


                var ddlorgidx_edit = (DropDownList)row.FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit = (DropDownList)row.FindControl("ddlrdeptidx_edit");

                getDepartmentList(ddlrdeptidx_edit, int.Parse(ddlorgidx_edit.SelectedValue));
                break;
            case "ddlorg":

                getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));
                if (int.Parse(ddlorg.SelectedValue) != 0)
                {
                    //litDebug.Text = "111";

                    data_chr _dtchr = new data_chr();
                    MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                    var _linq_FilterSearchIndex = (from data in _item_search
                                                   where
                                                   data.OrgIDX == int.Parse(ddlorg.SelectedValue)

                                                   select data
                                            ).ToList();

                    ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                    //

                    ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                    setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                    //ViewState["Va_DetailCarbooking"] = null;
                    
                }
                else
                {
                    //litDebug.Text = "22";
                    SelectMasterList();

                }

                break;
            case "ddldept":

                //getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));
                if (int.Parse(ddldept.SelectedValue) != 0)
                {
                    //litDebug.Text = "333";

                    data_chr _dtchr = new data_chr();
                    MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                    var _linq_FilterSearchIndex = (from data in _item_search
                                                   where
                                                   data.OrgIDX == int.Parse(ddlorg.SelectedValue)
                                                   && (data.RDeptIDX == int.Parse(ddldept.SelectedValue))

                                                   select data
                                            ).ToList();

                    ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                    //

                    ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                    setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else if (int.Parse(ddlorg.SelectedValue) != 0)
                {
                    
                   
                    //litDebug.Text = "111";

                    data_chr _dtchr = new data_chr();
                    MasterUserSAPList[] _item_search = (MasterUserSAPList[])ViewState["Vs_DetailUserSapSearch"];

                    var _linq_FilterSearchIndex = (from data in _item_search
                                                    where
                                                    data.OrgIDX == int.Parse(ddlorg.SelectedValue)

                                                    select data
                                            ).ToList();

                    ////_set_statusFilter = int.Parse(ddlPlacesFilter.SelectedValue);

                    //

                    ViewState["Vs_DetailUserSap"] = _linq_FilterSearchIndex;
                    setGridData(GvMaster, ViewState["Vs_DetailUserSap"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                    
                }
                else
                {
                    //litDebug.Text = "444";
                    SelectMasterList();

                }

                break;



        }

    }

    #endregion
    
    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                txtname.Text = String.Empty;
                SetDefaultAdd();
                update_Searchorg.Visible = false;
                break;

            case "btnCancel":
                btnshow.Visible = true;
                update_Searchorg.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                
                Panel_Add.Visible = false;
                Insert_Master();

                ////SelectMasterList();
                break;

            case "CmdDel":
                int UserIDX = int.Parse(cmdArg);
                ViewState["UserIDX"] = UserIDX;
                Delete_MasterSystem();
                SelectMasterList();

                break;
            case "cmdRefresh":
               
                SelectMasterList();
                getOrganizationList(ddlorg);
                getDepartmentList(ddldept, int.Parse(ddlorg.SelectedValue));
                break;
        }



    }


    #endregion
}