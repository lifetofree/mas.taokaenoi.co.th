﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Diagnostics.Contracts;
using System.Data;
using System.Data.SqlClient;
using ASP;

public partial class websystem_MasterData_cen_department_m0 : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    data_cen_master _data_cen_master = new data_cen_master();
    data_employee _dataEmployee = new data_employee();
    string _localJson = string.Empty;
    string textstatus = string.Empty;
    int _emp_idx = 0;
    //string textstatus = string.Empty;
    //string msg = string.Empty;
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCenMasterList"];
    static string _urlSetCenMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSetCenMasterList"];

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["forSearch"] = null;
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
            DropDownList ddlSearchorg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
            selectDdl(ddlSearchorg, "3", "", "", "", "", "");//select
            ddlSearchorg.SelectedValue = ""; //choose

            DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
            ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

            DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
            ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
            showDepartment();
            FormViewS.Visible = true;

        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {

        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        hlSetTotop.Focus();
        FvInsertEdit.Visible = false;
        gvDepartment.Visible = false;
        lbCreate.Visible = false;
        FormViewS.Visible = false;
        switch (cmdName)
        {
            case "cmdCreate":
                FvInsertEdit.Visible = true;
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Insert, null);
                DropDownList ddlInsertorg = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                selectDdl(ddlInsertorg, "3", "", "", "", "", "");//select
                ddlInsertorg.SelectedValue = ""; //choose

                DropDownList ddlInsertWg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
                ddlInsertWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                DropDownList ddlInsertLw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");
                ddlInsertLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                break;

            case "cmdSave":

                InsertDepartment(_functionTool.convertToInt(cmdArg));
                break;

            case "cmdEdit":

                FvInsertEdit.Visible = true;
                _search_cen_master_detail.s_org_idx = "";
                _search_cen_master_detail.s_wg_idx = "";
                _search_cen_master_detail.s_lw_idx = "";
                _search_cen_master_detail.s_dept_idx = cmdArg;
                _data_cen_master.master_mode = "6";
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setFvData(FvInsertEdit, FormViewMode.Edit, _data_cen_master.cen_dept_list_m0);



                DropDownList ddlEditorg = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
                selectDdl(ddlEditorg, "3", "", "", "", "", "");//select
                ddlEditorg.SelectedValue = _data_cen_master.cen_dept_list_m0[0].org_idx.ToString();//choose

                DropDownList ddlEditWg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
                selectDdl(ddlEditWg, "4", _data_cen_master.cen_dept_list_m0[0].org_idx.ToString(), "", "", "", "");
                ddlEditWg.SelectedValue = _data_cen_master.cen_dept_list_m0[0].wg_idx.ToString();

                DropDownList ddlEditLw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");
                selectDdl(ddlEditLw, "5", "", _data_cen_master.cen_dept_list_m0[0].wg_idx.ToString(), "", "", "");
                ddlEditLw.SelectedValue = _data_cen_master.cen_dept_list_m0[0].lw_idx.ToString();



                //_functionTool.setDdlData(ddlEditLw, _data_cen_master.cen_dept_list_m0, "lw_name_th", "lw_idx");

                FvInsertEdit.Visible = true;


                break;

            case "editSave":

                InsertDepartment(_functionTool.convertToInt(cmdArg));

                break;

            case "cmdDelete":
                cmdDelete(int.Parse(cmdArg));

                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddlSearchorg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                selectDdl(ddlSearchorg, "3", "", "", "", "", "");//select
                ddlSearchorg.SelectedValue = ""; //choose

                DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
                ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
                ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));

                gvDepartment.Visible = true;
                FormViewS.Visible = true;
                lbCreate.Visible = true;
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showDepartment();


                break;

            case "cmdCancel":
                gvDepartment.Visible = true;
                lbCreate.Visible = true;
                FormViewS.Visible = true;
                if (ViewState["forSearch"] == null)
                {
                    showDepartment();
                }
                else
                {
                    _functionTool.setGvData(gvDepartment, ((data_cen_master)ViewState["forSearch"]).cen_dept_list_m0);

                }
                break;

            case "cmdSearch":

                checkBoxSearch();
                lbCreate.Visible = true;
                gvDepartment.Visible = true;
                FormViewS.Visible = true;
                break;

            case "cmdReset":
                lbCreate.Visible = true;
                gvDepartment.Visible = true;
                FormViewS.Visible = true;
                _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);
                DropDownList ddlResetOrg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
                //selectOrg(ddlResetOrg, 0);

                DropDownList ddlResetWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
                ddlResetWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));

                DropDownList ddlResetLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
                ddlResetLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                ViewState["forSearch"] = null;
                ViewState["forWordSearch"] = null;
                showDepartment();
                break;
        }
    }

    protected void InsertDepartment(int id)
    {
        TextBox tex_TH_name = (TextBox)FvInsertEdit.FindControl("tb_deptnameth");
        TextBox tex_EN_name = (TextBox)FvInsertEdit.FindControl("tb_deptnameen");
        DropDownList dropD_status = (DropDownList)FvInsertEdit.FindControl("ddStatus");
        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
        DropDownList dropD_Lw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");



        //TextBox searchbox = (TextBox)FormViewS.FindControl("s_org");
        //litdebug.Text = tex_TH_name.Text;
        //litdebug.Text += tex_EN_name.Text;
        //litdebug.Text += dropD_status.SelectedValue;
        data_cen_master _data_cen_master = new data_cen_master();
        cen_dept_detail_m0 _cen_dept_detail_m0 = new cen_dept_detail_m0();

        _data_cen_master.master_mode = "6";
        _cen_dept_detail_m0.dept_idx = id;
        _cen_dept_detail_m0.dept_name_th = tex_TH_name.Text.Trim();
        _cen_dept_detail_m0.dept_name_en = tex_EN_name.Text.Trim();
        _cen_dept_detail_m0.dept_status = _functionTool.convertToInt(dropD_status.SelectedValue);
        _cen_dept_detail_m0.org_idx = _functionTool.convertToInt(dropD_Org.SelectedValue);
        _cen_dept_detail_m0.wg_idx = _functionTool.convertToInt(dropD_Wg.SelectedValue);
        _cen_dept_detail_m0.lw_idx = _functionTool.convertToInt(dropD_Lw.SelectedValue);
        _cen_dept_detail_m0.cemp_idx = _emp_idx;


        _data_cen_master.cen_dept_list_m0 = new cen_dept_detail_m0[1];
        _data_cen_master.cen_dept_list_m0[0] = _cen_dept_detail_m0;

        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToXml(_data_cen_master));
        //string msg = _data_cen_master.return_msg;
        //litdebug.Text = msg;

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
            FvInsertEdit.Visible = true;
            lbCreate.Visible = false;
        }
        else
        {
            lbCreate.Visible = true;
            FvInsertEdit.Visible = false;
            gvDepartment.Visible = true;
            _functionTool.setFvData(FormViewS, FormViewMode.Insert, null);

            DropDownList ddlSearchOrg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
            //selectOrg(ddlSearchOrg, 0);
            selectDdl(ddlSearchOrg, "3", "", "", "", "", "");
            DropDownList ddlSearchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
            ddlSearchWg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
            DropDownList ddlSearchLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
            ddlSearchLw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
            
            ViewState["forSearch"] = null;
            ViewState["forWordSearch"] = null;
            showDepartment();
            FormViewS.Visible = true;
        }
    }

        protected void showDepartment()
    {


        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        if (ViewState["forSearch"] == null)
        {

            _search_cen_master_detail.s_org_idx = "";
            _search_cen_master_detail.s_wg_idx = "";
            _search_cen_master_detail.s_lw_idx = "";
            _search_cen_master_detail.s_dept_idx = "";
            _data_cen_master.master_mode = "6";

            _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
            _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;

            _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

            _functionTool.setGvData(gvDepartment, _data_cen_master.cen_dept_list_m0);

        }
        else
        {
            _functionTool.setGvData(gvDepartment, ((data_cen_master)ViewState["forSearch"]).cen_dept_list_m0);
        }


    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        cen_wg_detail_m0 _cen_wg_detail_m0 = new cen_wg_detail_m0();
        cen_lw_detail_m0 _cen_lw_detail_m0 = new cen_lw_detail_m0();

        DropDownList ddlName = (DropDownList)sender;

        DropDownList dropD_Org = (DropDownList)FvInsertEdit.FindControl("DropDownOrg");
        DropDownList dropD_Wg = (DropDownList)FvInsertEdit.FindControl("DropDownWg");
        DropDownList dropD_Lw = (DropDownList)FvInsertEdit.FindControl("DropDownLw");


        DropDownList dropD_Org_s = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList dropD_Wg_s = (DropDownList)FormViewS.FindControl("DropDownWg_s");
        DropDownList dropD_Lw_s = (DropDownList)FormViewS.FindControl("DropDownLw_s");


        switch (ddlName.ID) {
            case "DropDownOrg":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text += HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Wg, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
            break;

            case "DropDownOrg_s":

                _data_cen_master.master_mode = "4";
                _search_cen_master_detail.s_org_idx = dropD_Org_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Wg_s, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                dropD_Wg_s.Items.Insert(0, new ListItem("---เลือกกลุ่มงาน---", ""));
                break;

            case "DropDownWg":
                
                
                _data_cen_master.master_mode = "5";
                _search_cen_master_detail.s_wg_idx = dropD_Wg.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Lw, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                dropD_Lw.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                break;

            case "DropDownWg_s":

               
                _data_cen_master.master_mode = "5";
                _search_cen_master_detail.s_wg_idx = dropD_Wg_s.SelectedValue;
                _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
                _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
                _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
                //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
                _functionTool.setDdlData(dropD_Lw_s, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                dropD_Lw_s.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));
                break;
        }
    }


    protected void checkBoxSearch()
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();
        DropDownList ddl_searchOrg = (DropDownList)FormViewS.FindControl("DropDownOrg_s");
        DropDownList ddl_searchWg = (DropDownList)FormViewS.FindControl("DropDownWg_s");
        DropDownList ddl_searchLw = (DropDownList)FormViewS.FindControl("DropDownLw_s");
        DropDownList ddl_status_for_s = (DropDownList)FormViewS.FindControl("DropDownstatus_s");
        TextBox searchbox = (TextBox)FormViewS.FindControl("tb_dept_s");

        //_search_cen_master_detail.s_org_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_dept_name = searchbox.Text.Trim();
        _search_cen_master_detail.s_org_idx = ddl_searchOrg.SelectedValue;
        _search_cen_master_detail.s_wg_idx = ddl_searchWg.SelectedValue;
        _search_cen_master_detail.s_lw_idx = ddl_searchLw.SelectedValue;
        _search_cen_master_detail.s_status = ddl_status_for_s.SelectedValue;
        _data_cen_master.master_mode = "6";

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);

        ViewState["forSearch"] = _data_cen_master;
        //ViewState["forSearch2"] = _data_cen_master;

        showDepartment();


    }

    protected void cmdDelete(int cmdArg)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        cen_dept_detail_m0 _cen_dept_detail_m0 = new cen_dept_detail_m0();

        _cen_dept_detail_m0.dept_idx = cmdArg;
        _cen_dept_detail_m0.dept_status = 9;
        _cen_dept_detail_m0.cemp_idx = _emp_idx;
        _data_cen_master.master_mode = "6";


        _data_cen_master.cen_dept_list_m0 = new cen_dept_detail_m0[1];
        _data_cen_master.cen_dept_list_m0[0] = _cen_dept_detail_m0;

        //--debug
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        _data_cen_master = callServiceMaster(_urlSetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _functionTool.setGvData(gvDepartment, _data_cen_master.cen_dept_list_m0);

        if (_data_cen_master.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถลบข้อมูลนี้ได้!!!');", true);

        }
        else
        {

        }


    }

    protected void selectDdl(DropDownList ddlName, string masterMode, string _org_idx, string _wg_idx, string _lw_idx, string _dept_idx, string _sec_idx)
    {
        data_cen_master _data_cen_master = new data_cen_master();
        search_cen_master_detail _search_cen_master_detail = new search_cen_master_detail();


        _data_cen_master.master_mode = masterMode;
        _search_cen_master_detail.s_org_idx = _org_idx;
        _search_cen_master_detail.s_wg_idx = _wg_idx;
        _search_cen_master_detail.s_lw_idx = _lw_idx;
        _search_cen_master_detail.s_dept_idx = _dept_idx;
        _search_cen_master_detail.s_sec_idx = _sec_idx;

        _data_cen_master.search_cen_master_list = new search_cen_master_detail[1];
        _data_cen_master.search_cen_master_list[0] = _search_cen_master_detail;
        // litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));
        _data_cen_master = callServiceMaster(_urlGetCenMasterList, _data_cen_master);
        //litdebug.Text = HttpUtility.HtmlEncode(_functionTool.convertObjectToJson(_data_cen_master));

        switch (masterMode)
        {
            case "3":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_org_list_m0, "org_name_th", "org_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกองค์กร---", ""));


                break;

            case "4":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_wg_list_m0, "wg_name_th", "wg_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกกลุ่มงาม---", ""));

                break;

            case "5":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_lw_list_m0, "lw_name_th", "lw_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกสายงาน---", ""));


                break;

            case "6":

                _functionTool.setDdlData(ddlName, _data_cen_master.cen_dept_list_m0, "dept_name_th", "dept_idx");
                ddlName.Items.Insert(0, new ListItem("---เลือกฝ่าย---", ""));


                break;
        }

    }


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        data_cen_master _data_cen_master = new data_cen_master();
        switch (GvName.ID)
        {
            case "gvDepartment":
                gvDepartment.PageIndex = e.NewPageIndex;
                showDepartment();
                break;

        }
        hlSetTotop.Focus();
    }

    protected data_cen_master callServiceMaster(string _cmdUrl, data_cen_master _data_cen_master)
    {
        _localJson = _functionTool.convertObjectToJson(_data_cen_master);

        _localJson = _functionTool.callServicePost(_cmdUrl, _localJson);

        _data_cen_master = (data_cen_master)_functionTool.convertJsonToObject(typeof(data_cen_master), _localJson);


        return _data_cen_master;
    }

    protected string convertStatus(int status)
    {
        if (status == 1)
            textstatus = "online";
        else
            textstatus = "offline";
        return textstatus;
    }
    protected string convertCss(int status)
    {
        if (status == 1)
            textstatus = "text-success";
        else
            textstatus = "text-danger";
        return textstatus;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _functionTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_functionTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

}