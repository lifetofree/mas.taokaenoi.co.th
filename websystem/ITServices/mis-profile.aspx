﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="mis-profile.aspx.cs" Inherits="websystem_ITServices_mis_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .bg-ad-profiletemplate-center {
            background: url('../../masterpage/images/mis-profile/PersonalInfo_Template_OK-04.png') no-repeat;
            background-size: 100% 100%;
        }

        .bg-ad-profile-templateheader-center {
            background: url('../../masterpage/images/mis-profile/PersonalInfo_Template_OK-01.png') no-repeat;
            background-size: 100% 100%;
        }
    </style>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Literal ID="text" runat="server"></asp:Literal>

            <div id="BoxTabMenuIndex" runat="server">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand">Menu</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav ulActiveMenu" runat="server">
                            <li id="IndexList" runat="server">
                                <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnrefresh" OnCommand="btnCommand">ข้อมูลทั่วไป</asp:LinkButton>
                            </li>
                            <li id="ProfileList" runat="server">
                                <asp:LinkButton ID="lbprofile" CssClass="btn_menulist" runat="server" CommandArgument="0" CommandName="ViewProfile" OnCommand="btnCommand">ข้อมูลส่วนตัว</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


                <%-------- Index Start----------%>
                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ค้นหาประวัติส่วนตัว</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากองค์กร : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกองค์กร....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlrdeptidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากแผนก : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlrsecidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกแผนก....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากตำแหน่ง : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlposition" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจาก Cost Center : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlcostcenter" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือก Cost Center....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากชื่อพนักงาน(TH) / รหัสพนักงาน : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtempidx" runat="server" CssClass="form-control"></asp:TextBox>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากสิทธิ์ AD : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlad" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกสิทธิ์ AD....</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                    <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากสถานะพนักงาน: " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1">กรุณาเลือกสถานะพนักงาน....</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                            <asp:ListItem Value="0">Non Active</asp:ListItem>
                                            <asp:ListItem Value="3">All</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <div class="form-group">

                                    <asp:Label ID="Label26" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจาก Email พนักงาน: " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group" id="div_button" runat="server" visible="true">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" ValidationGroup="SearchReport" runat="server" CommandName="btnsearch" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                        <asp:LinkButton ID="btnrefresh" CssClass="btn btn-info  btn-sm" runat="server" CommandName="btnrefresh" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>
                                        <asp:LinkButton ID="btnexport" CssClass="btn btn-primary btn-sm" runat="server" CommandName="btnexport" OnCommand="btnCommand" title="Export"><i class="glyphicon glyphicon-export"></i> Export Excel</asp:LinkButton>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div id="BoxGridView" runat="server">
                        <div class="panel-heading">
                            <div id="divGvMaster" style="overflow-x: scroll; width: 100%" runat="server">

                                <asp:GridView ID="GvMaster" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnPageIndexChanging="Master_PageIndexChanging">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>


                                        <asp:TemplateField HeaderText="บริษัท" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lborg" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbdept" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbcode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="เบอร์ติดต่อ" Visible="false" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbmobile" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อีเมล์" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbemail" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระดับสิทธิ์ AD" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lbad" runat="server" Text='<%# Eval("perm_pol_name") %>'></asp:Label>
                                                <asp:Label ID="lbempidx" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbVeiw1" runat="server" CssClass="btn btn-info btn-sm" OnCommand="btnCommand" CommandName="ViewProfile" title="View" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-file" ></i> </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>


                            </div>
                        </div>

                    </div>
                    <%--    <asp:GridView ID="GvMaster1" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="info"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        PageSize="10"
                        EnableEventValidation="false">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>


                            <asp:TemplateField HeaderText="บริษัท" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lborg" runat="server" Text='<%# Eval("org_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lbdept" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="lbcode" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbEmpName" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="เบอร์ติดต่อ" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbmobile" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อีเมล์" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbemail" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ระดับสิทธิ์ AD" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:Label ID="lbad" runat="server" Text='<%# Eval("perm_pol_name") %>'></asp:Label>
                                    <asp:Label ID="lbempidx" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbVeiw1" runat="server" CssClass="btn btn-info btn-sm" OnCommand="btnCommand" CommandName="ViewProfile" title="View" CommandArgument='<%# Eval("emp_idx") %>'><i class="fa fa-file-text-o" ></i> </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                    --%>


                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true" EnableEventValidation="false">
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </asp:View>

                <asp:View ID="ViewDetail" runat="server">

                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading bg-ad-profile-templateheader-center" style="height: 65pt;">
                            </div>
                            <div class="panel-body bg-ad-profiletemplate-center">

                                <asp:FormView ID="FvProfile" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <div class="col-lg-12">
                                                    <div class="form-group" style="text-align: center">
                                                        <asp:Image ID="images" runat="server" Width="25%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-02.png" />

                                                    </div>
                                                </div>


                                                <br />
                                                <br />
                                                <div class="col-lg-12">
                                                    <%--  <div class="col-lg-1">
                                                    </div>--%>
                                                    <div class="col-lg-5" style="background-color: white; border-radius: 2%">
                                                        <br />

                                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label7y" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="รหัสพนักงาน : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="LbEmpCode" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_code") %>' />
                                                                    <asp:Label ID="LbEmpidx" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_idx") %>' />

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label11" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ชื่อพนักงาน(EN)  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label12" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_name_en") %>' />
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label33" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="บริษัท  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="LbOrgNameTH" CssClass="control-labelnotop" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                                    <asp:Label ID="LbOrgidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_idx") %>' />
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label35" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="แผนก  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label36" CssClass="control-labelnotop" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                                    <asp:Label ID="Lbrsecidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rsec_idx") %>' />
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label37" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="Cost Center  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label38" CssClass="control-labelnotop" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                                                </div>

                                                            </div>
                                                        </div>




                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label41" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ประเภทพนักงาน  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label42" CssClass="control-labelnotop" runat="server" Text='<%# Eval("emp_type_name") %>' />
                                                                </div>

                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label39" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="สถานที่  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label40" CssClass="control-labelnotop" runat="server" Text='<%# Eval("LocName") %>' />
                                                                </div>

                                                            </div>
                                                        </div>



                                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                        <%--  <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th") %>' />
                                                                    <asp:Label ID="LbOrgidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_idx") %>' />
                                                                </div>

                                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                                <div class="col-xs-4">
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>--%>


                                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                                        <%--     <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                                    <asp:Label ID="Lbrsecidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rsec_idx") %>' />

                                                                </div>

                                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th") %>' />
                                                                </div>
                                                            </div>
                                                        </div>--%>
                                                    </div>
                                                    <div class="col-lg-2">
                                                    </div>


                                                    <div class="col-lg-5" style="background-color: white; border-radius: 2%">
                                                        <br />

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label18" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ชื่อพนักงาน(TH) : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label19" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label13" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ชื่อเล่นพนักงาน  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label14" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_nickname_th") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label20" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ฝ่าย  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th") %>' />
                                                                    <asp:Label ID="LbDeptidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rdept_idx") %>' />

                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label31" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="ตำแหน่ง  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label43" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ(มือถือ)  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label44" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("emp_mobile_no") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label45" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="สถานะการทำงาน  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label46" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# getStatus((int)Eval("emp_status")) %>' />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label47" CssClass="col-xs-4 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ  : " />
                                                                <div class="col-xs-8">
                                                                    <asp:Label ID="Label48" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%#Eval("PhoneNumber") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                                        <%--    <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                                                </div>

                                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ(มือถือ) : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_mobile_no") %>' />

                                                                </div>
                                                            </div>
                                                        </div>--%>

                                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                                        <%--   <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label15" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทพนักงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label16" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_type_name") %>' />

                                                                </div>
                                                                <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการทำงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label22" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# getStatus((int)Eval("emp_status")) %>' />

                                                                </div>

                                                            </div>
                                                        </div>--%>

                                                        <%--  <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label18" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่ : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label19" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("LocName") %>' />

                                                                </div>
                                                                <asp:Label ID="Label20" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label31" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%#Eval("PhoneNumber") %>' />

                                                                </div>

                                                            </div>
                                                        </div>--%>
                                                    </div>




                                                </div>
                                                <br />
                                                <div class="col-lg-12">

                                                    <div class="form-group">
                                                        <div class="col-sm-11">
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <asp:ImageButton ID="btnedit" Width="35%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-07.png" runat="server" OnCommand="btnCommand" CommandName="btnedit" data-toggle="tooltip" title="Edit" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <div class="col-lg-2">
                                                    <div class="form-group">

                                                        <asp:Image ID="images" ImageAlign="Middle" runat="server" Width="80%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-02.png" />

                                                    </div>
                                                </div>
                                                <div class="col-lg-10">
                                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label7y" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสพนักงาน : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="LbEmpCode" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                                                <asp:Label ID="LbEmpidx" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_idx") %>' />
                                                            </div>

                                                            <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อพนักงาน(TH) : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label11" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อพนักงาน(EN)  : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label12" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_name_en") %>' />
                                                            </div>

                                                            <asp:Label ID="Label13" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อเล่นพนักงาน  : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label14" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_nickname_th") %>' />
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_name_th") %>' />
                                                                <asp:Label ID="LbOrgidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("org_idx") %>' />
                                                            </div>

                                                            <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th") %>' />
                                                                <asp:Label ID="LbDeptidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rdept_idx") %>' />

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                                <asp:Label ID="Lbrsecidx" Visible="false" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("rsec_idx") %>' />

                                                            </div>

                                                            <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pos_name_th") %>' />
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                                    <div class="form-group">
                                                        <div class="col-md-12">

                                                            <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="Cost Center : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                                            </div>

                                                            <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ(มือถือ) : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_mobile_no") %>' />

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label15" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทพนักงาน : " />
                                                            <div class="col-xs-3">
                                                                <asp:Label ID="Label16" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_type_name") %>' />

                                                            </div>
                                                            <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานะการทำงาน : " />
                                                            <div class="col-xs-4">
                                                                <asp:Label ID="Label22" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# getStatus((int)Eval("emp_status")) %>' />

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label26" runat="server" Text="สถานที่" CssClass="col-xs-2  control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="lblLocIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("LocIDX") %>' Enabled="false" />
                                                                <asp:DropDownList ID="ddllocname" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                                    ControlToValidate="ddllocname" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                                <ajaxtoolkit:validatorcalloutextender id="ValidatorCalloutExtender1" runat="Server" highlightcssclass="validatorCalloutHighlight" targetcontrolid="RequiredFieldValidddator8" width="160" />

                                                            </div>
                                                            <asp:Label ID="Label17" runat="server" Text="เบอร์ติดต่อองค์กร" CssClass="col-xs-2  control-label text_right"></asp:Label>

                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="lblm0telidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("m0telidx") %>' Enabled="false" />

                                                                <asp:DropDownList ID="ddlmaintel" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Text="กรุณาเลือกเบอร์ติดต่อองค์กร..." Value="0"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                                    ControlToValidate="ddlmaintel" Font-Size="11"
                                                                    ErrorMessage="กรุณาเลือกเบอร์ติดต่อองค์กร"
                                                                    ValidationExpression="กรุณาเลือกเบอร์ติดต่อองค์กร" InitialValue="0" />
                                                                <ajaxtoolkit:validatorcalloutextender id="ValidatorCalloutExtender2" runat="Server" highlightcssclass="validatorCalloutHighlight" targetcontrolid="RequiredFieldValidator2" width="160" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label29" runat="server" Text="เบอร์ติดต่อภายใน" CssClass="col-xs-2  control-label text_right"></asp:Label>
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="lblu0tel" CssClass="form-control" runat="server" Text='<%# Bind("u0_tel") %>' />
                                                                <asp:TextBox ID="lblu0telidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("u0telidx") %>' />


                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="btnsaveedit" CssClass="btn btn-success btn-sm" runat="server" Text="Save" OnCommand="btnCommand" CommandName="btnsaveedit" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')" data-toggle="tooltip" title="Save"><i class="glyphicon glyphicon-floppy-saved"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnfvread" CssClass="btn btn-danger btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnfvread" data-toggle="tooltip" OnClientClick="return confirm('คุณต้องการยกเลิกการทำรายการนี้ใช่หรือไม่ ?')" title="Back"><i class="fa fa-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>

                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8" id="div_adonline" runat="server">
                        <div class="panel panel-warning">
                            <div class="panel-heading" style="background-color: #F6B333; text-align: center">
                                <h3 class="panel-title "><i class="glyphicon glyphicon-globe"></i><strong>&nbsp; AD Oline Info</strong></h3>
                            </div>

                            <div class="panel-body panel-body bg-ad-profiletemplate-center">
                                <div class="form-horizontal" role="form">

                                    <div class="col-lg-12">
                                        <div class="form-group" style="text-align: center">
                                            <asp:Image ID="image1" ImageAlign="Middle" runat="server" Width="30%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-03.png" />
                                        </div>
                                        <br />
                                        <div class="col-lg-12">
                                            <div class="col-lg-6" style="background-color: white; border-radius: 2%">
                                                <br />

                                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label7y" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ระดับสิทธิ์ AD : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbpermpol" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label49" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="ชื่อสิทธิ์ใช้งาน : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbuserad" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label50" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="User SAP : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbusersap" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label51" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="VPN SAP : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbvpnsap" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label52" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="VPN BI : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbvpnbi" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label53" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="VPN Express : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbvpnexpress" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label54" CssClass="col-xs-6 control-labelnotop text_right" runat="server" Text="Email : " />
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Lbemail" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-lg-1">
                                            </div>
                                            <div class="col-lg-5" style="background-color: white; border-radius: 2%">
                                                <br />
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label55" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้สิทธิ์ AD : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdatead" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="col-xs-6">
                                                            <asp:Label ID="Label57" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                           

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label30" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้ SAP : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdateusersap" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label27" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้ VPN SAP : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdatevpnsap" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label32" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้ VPN BI : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdatevpnbi" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label59" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้ VPN Express : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdatevpnexpress" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label60" CssClass="col-xs-7 control-labelnotop text_right" runat="server" Text="วันที่เริ่มใช้ Email : " />
                                                        <div class="col-xs-5">
                                                            <asp:Label ID="Lbdateemail" CssClass="control-labelnotop" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>




                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading" style="background-color: #A3D55D; text-align: center">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Holding Devices Info</strong></h3>
                            </div>
                            <div class="panel-body bg-ad-profiletemplate-center">
                                <div class="col-lg-12">
                                    <div class="form-group" style="text-align: center">
                                        <asp:Image ID="images" ImageAlign="Middle" runat="server" Width="50%" ImageUrl="~/masterpage/images/mis-profile/PersonalInfo_Template_OK-09.png" />
                                    </div>
                                </div>
                                <div class="col-lg-12" style="background-color: white">
                                    <br />
                                    <asp:GridView ID="GvHolder" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                        HeaderStyle-CssClass="primary"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        DataKeyNames="u0_didx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ">

                                                <ItemTemplate>
                                                    <%# (Container.DataItemIndex +1) %>
                                                    <asp:Label ID="lbu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbname" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="อุปกรณ์ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbu0code" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                                    <%--  <asp:UpdatePanel ID="Updatepn1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>

                                                    <asp:Label ID="btnsoftware" runat="server" CssClass="btn btn-xs"><i class="glyphicon glyphicon-folder-open"></i></asp:Label>
                                                    <%--  </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbcode" runat="server" Text='<%# Eval("HolderDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-1 col-sm-offset-11">
                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnrefresh" data-toggle="tooltip" title="Back"><i class="fa fa-times"></i></asp:LinkButton>
                        </div>
                    </div>


                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnsearch" />
            <asp:PostBackTrigger ControlID="btnexport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

