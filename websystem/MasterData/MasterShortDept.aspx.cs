﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_MasterShortDept : System.Web.UI.Page
{
    #region  Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    DataSupportIT _dtsupport = new DataSupportIT();

    static string _serviceUrl = _serviceUrl + ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlSelect_ShortDept = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ShortDept"];
    static string _urlUpdate_ShortDept = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ShortDept"];




    string _localJson = String.Empty;

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            getOrganizationList(ddlSearchOrg);

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
    }
    #endregion

    #region Reuse

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร.....", "0"));
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected DataSupportIT callServiceITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void SelectMasterList()
    {
        _dtsupport = new DataSupportIT();
        _dtsupport.BoxDepartmentList = new ShortDepartment[1];
        ShortDepartment dtsupport = new ShortDepartment();

        dtsupport.OrgIDX = int.Parse(ddlSearchOrg.SelectedValue);

        _dtsupport.BoxDepartmentList[0] = dtsupport;

        _dtsupport = callServiceITRepair(_urlSelect_ShortDept, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxDepartmentList);
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int DeptIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var shortdept = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtshortdept");

                GvMaster.EditIndex = -1;



                _dtsupport.BoxDepartmentList = new ShortDepartment[1];
                ShortDepartment dtsupport = new ShortDepartment();


                dtsupport.DeptIDX = DeptIDX;
                dtsupport.ShortDept = shortdept.Text;
                dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());


                _dtsupport.BoxDepartmentList[0] = dtsupport;

                _dtsupport = callServiceITRepair(_urlUpdate_ShortDept, _dtsupport);

                SelectMasterList();

                break;
        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdSearch":
                SelectMasterList();
                break;
        }
    }
    #endregion
}