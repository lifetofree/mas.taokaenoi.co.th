﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_employee_recruit_exam : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_rcm_question _dtrcm = new data_rcm_question();
    data_employee_recruit employee_recuit_exam = new data_employee_recruit();

    string _localJson = String.Empty;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string url = String.Empty;
    string idcard = String.Empty;
    int _tempInt = 0;
    int[] secidx = { 436, 180, 3549, 191, 459 };


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectm0topicquest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0topicquest"];
    static string _urlSelectQuestion = _serviceUrl + ConfigurationManager.AppSettings["urlSelectQuestion"];
    static string _urlSelectTypeansQuest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeansQuest"];
    static string _urlInsertAnswer = _serviceUrl + ConfigurationManager.AppSettings["urlInsertAnswer"];
    static string _urlGetEmployee_Register = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee_Register"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectResult"];
    static string _urlUpdateScore = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateScore"];
    static string _urlSelectIdentity = _serviceUrl + ConfigurationManager.AppSettings["urlSelectIdentity"];
    static string _urlSelectm0typequest = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0typequest"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlSelect_SearchResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SearchResult"];
    static string _urlSelect_Logquestion = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Logquestion"];

    static string _urlGetConfigExam = _serviceUrl + ConfigurationManager.AppSettings["urlGetConfigExam"];
    static string _urlGetDataExamBasic = _serviceUrl + ConfigurationManager.AppSettings["urlGetDataExamBasic"];
    static string _urlSetDataExamBasic = _serviceUrl + ConfigurationManager.AppSettings["urlSetDataExamBasic"];
    static string _urlGetPresonal_data_exam_sp = _serviceUrl + ConfigurationManager.AppSettings["urlGetPresonal_data_exam_sp"];


    static string keyrecruit = ConfigurationManager.AppSettings["keyrecruit"];

    #endregion

    #region Page_Load

    protected void Page_InIt(object sender, EventArgs e)
    {
        ViewState["identity_card"] = "4444444422222";
        if (Request.Form["identity_card"] != null)
        {

            ViewState["identity_card"] = Request.Form["identity_card"];
            //txt.Text = ViewState["identity_card"].ToString();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string key_code = "TKNExamRecruit"; // key ในการเข้ารหัส
        if (!Page.IsPostBack)
        {
            string view_page = string.Empty;
            ViewState["EmpIDX"] = string.Empty;
            ViewState["data_emp"] = null;
            ViewState["data_exam"] = null;
            if (Request.QueryString["u"] != null || Request.QueryString["k"] != null || Request.QueryString["d"] != null)
            {
                int data_now_totime = _funcTool.convertToInt(DateTime.Now.ToString("yyyyMMddHHmmssffff"));

                string date_end = Request.QueryString["d"].ToString().Trim(); // วันที่ ที่สามารถให้เข้ามาได้           
                string user_id = Request.QueryString["u"].ToString().Trim(); // EmpIDX
                string sum_check = Request.QueryString["k"].ToString().Trim(); // sum check
                string sum_md = _funcTool.getMd5Sum(user_id + date_end + key_code).Trim();
                if (sum_md == sum_check && data_now_totime >= _funcTool.convertToInt(date_end) && user_id != string.Empty)
                {
                    string page_name = string.Empty;

                    if (Request.QueryString["page"] != null)
                    {
                        page_name = Request.QueryString["page"].ToString().Trim();
                    }
                  
                    if (page_name == "exam_sp")
                    {
                        //

                        exam_sp_detail requset_more_detail = new exam_sp_detail();
                        employee_recuit_exam.exam_sp_list = new exam_sp_detail[1];
                        requset_more_detail.EmpIDX = _funcTool.convertToInt(user_id.ToString());

                        employee_recuit_exam.exam_sp_list[0] = requset_more_detail;
                        employee_recuit_exam = callServicePostAPI(_urlGetPresonal_data_exam_sp, employee_recuit_exam);
                      //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(employee_recuit_exam));
                        if (employee_recuit_exam.return_code == "0")
                        {
                            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(employee_recuit_exam));
                            if (employee_recuit_exam.exam_sp_list[0].unidx.ToString() == "11" && employee_recuit_exam.exam_sp_list[0].acidx.ToString() == "1")
                            {
                                requset_more_data temp_data = new requset_more_data();
                                employee_recuit_exam.requset_more_list = new requset_more_data[1];
                                temp_data.FirstNameTH = employee_recuit_exam.exam_sp_list[0].FirstNameTH.ToString();
                                temp_data.LastNameTH = employee_recuit_exam.exam_sp_list[0].LastNameTH.ToString();
                                employee_recuit_exam.requset_more_list[0] = temp_data;

                                ViewState["data_emp"] = employee_recuit_exam.requset_more_list;
                                ViewState["EmpIDX"] = employee_recuit_exam.exam_sp_list[0].EmpIDX.ToString();
                                ViewState["m0_tqidx"] = "2";
                                view_page = "ViewExam";

                            }
                            if (employee_recuit_exam.exam_sp_list[0].u0_anidx.ToString() != "0")
                            {
                                view_page = "viewreadonly";
                            }

                        }
                    }
                    else
                    {
                        requset_more_data requset_more_detail = new requset_more_data();
                        employee_recuit_exam.requset_more_list = new requset_more_data[1];
                        requset_more_detail.EmpIDX = _funcTool.convertToInt(user_id.ToString());

                        employee_recuit_exam.requset_more_list[0] = requset_more_detail;
                        employee_recuit_exam = callServicePostAPI(_urlGetConfigExam, employee_recuit_exam);
                        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(employee_recuit_exam));
                        if (employee_recuit_exam.return_code == "0")
                        {
                            ViewState["data_emp"] = employee_recuit_exam.requset_more_list;
                            ViewState["EmpIDX"] = employee_recuit_exam.requset_more_list[0].EmpIDX.ToString();

                            view_page = "ViewExam";
                            if (employee_recuit_exam.requset_more_list[0].rq_exam.ToString() == "0")
                            {
                                view_page = "viewerror";
                            }
                            else if (employee_recuit_exam.requset_more_list[0].exam_status.ToString() == "1")
                            {
                                view_page = "viewreadonly";
                            }

                        }

                    }

                }


            }

            if (view_page == string.Empty)
            {

                //string date_end = DateTime.Now.AddDays(10).ToString("yyyyMMddHHmmssffff");

                //string user_id = "293";
                //string sum_md = _funcTool.getMd5Sum(user_id + date_end + key_code);
                //litDebug.Text += "<br>";
                //litDebug.Text += date_end;
                //litDebug.Text += "<br>";
                //litDebug.Text += user_id;
                //litDebug.Text += "<br>";
                //litDebug.Text += sum_md;
                setActiveView("viewerror");
            }
            else
            {
                setActiveView(view_page);
            }

        }

    }


    #endregion

    #region Select
    protected void select_gettopicquestion(GridView gvName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.rposidx = rposidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0topicquest, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_topicquest);


    }

    protected void select_getquestion(GridView gvName, int m0_toidx, int m0_taidx, int condition, int cempidx, int anidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_question = new m0_question[1];
        m0_question _quest = new m0_question();

        _quest.m0_toidx = m0_toidx;
        _quest.m0_taidx = m0_taidx;
        _quest.condition = condition;
        _quest.CEmpIDX = cempidx;

        if (condition == 1)
        {
            _quest.u0_anidx = anidx;
        }

        _dtrcm.Boxm0_question[0] = _quest;



        _dtrcm = callServicePostRCM(_urlSelectQuestion, _dtrcm);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));
        setGridData(gvName, _dtrcm.Boxm0_question);
    }

    protected void select_gettypeanswer(GridView gvName, int m0_toidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typeanswer = new m0_typeanswer[1];
        m0_typeanswer _typeans = new m0_typeanswer();

        _typeans.m0_toidx = m0_toidx;

        _dtrcm.Boxm0_typeanswer[0] = _typeans;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectTypeansQuest, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_typeanswer);

        //lbltopic_name.Text = _dtrcm.Boxm0_typeanswer[0].topic_name.ToString();
        //txt.Text = _dtrcm.Boxm0_typeanswer[0].topic_name.ToString();
    }

    protected void Select_ProfileRecruit(string idcard)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest topic = new m0_topicquest();

        topic.identity_card = idcard;


        _dtrcm.Boxm0_topicquest[0] = topic;
        txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));
        _dtrcm = callServicePostRCM(_urlSelectIdentity, _dtrcm);
        txt.Text += "<br>" + HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        ViewState["EmpIDX"] = _dtrcm.Boxm0_topicquest[0].emp_idx;
        //ViewState["P_m0_toidx"] = _dtrcm.Boxm0_topicquest[0].P_m0_toidx.ToString();
        ViewState["P_m0_toidx"] = "1, 2, 3";
        ViewState["PIDX"] = _dtrcm.Boxm0_topicquest[0].PIDX;

        //ViewState["P_m0_toidx"] = "0";
    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_getresult(GridView gvName, int orgidx, int rdeptidx, int rsecidx, int cempidx, int m0_toidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.CEmpIDX = cempidx;
        _topicquest.m0_toidx = m0_toidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectResult, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_topicquest);


    }

    protected void gettypequestion(DropDownList ddlName)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_typequest = new m0_typequest[1];
        m0_typequest _typequest = new m0_typequest();

        _dtrcm.Boxm0_typequest[0] = _typequest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0typequest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_typequest, "type_quest", "m0_tqidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทคำถาม...", "0"));
    }

    protected void gettopicquestion(DropDownList ddlName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int rposidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.rposidx = rposidx;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //        txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelectm0topicquest, _dtrcm);
        setDdlData(ddlName, _dtrcm.Boxm0_topicquest, "topic_name", "m0_toidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชุดคำถาม...", "0"));
    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getPositionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _positionList = new position_details();
        _positionList.org_idx = _org_idx;
        _positionList.rdept_idx = _rdept_idx;
        _positionList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _positionList;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง....", "0"));
    }

    protected void selectSearch_getresult(GridView gvName, int m0_tqidx, int orgidx, int rdeptidx, int rsecidx, int m0_toidx, int searchdate, string datestart, string dateend)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topicquest = new m0_topicquest();

        _topicquest.m0_tqidx = m0_tqidx;
        _topicquest.orgidx = orgidx;
        _topicquest.rdeptidx = rdeptidx;
        _topicquest.rsecidx = rsecidx;
        _topicquest.m0_toidx = m0_toidx;
        _topicquest.IFSearchbetween = searchdate;
        _topicquest.DateStart = datestart;
        _topicquest.DateEnd = dateend;

        _dtrcm.Boxm0_topicquest[0] = _topicquest;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelect_SearchResult, _dtrcm);
        setGridData(gvName, _dtrcm.Boxm0_topicquest);


    }

    protected void select_gettypeanswer(Repeater rptName, int u0_anidx)
    {

        _dtrcm = new data_rcm_question();
        _dtrcm.Boxm0_topicquest = new m0_topicquest[1];
        m0_topicquest _topic = new m0_topicquest();

        _topic.u0_anidx = u0_anidx;

        _dtrcm.Boxm0_topicquest[0] = _topic;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtrcm = callServicePostRCM(_urlSelect_Logquestion, _dtrcm);
        setRepeaterData(rptName, _dtrcm.Boxl0_logquestion);

    }

    #endregion

    #region Insert AND Update
    protected void Insert_answer()
    {
        GridView GvTypeAns = (GridView)ViewDetailAdd.FindControl("GvTypeAns");
        int m0_tqidx = 0;
        data_rcm_question _dtrcm = new data_rcm_question();
        _dtrcm = new data_rcm_question();

        foreach (GridViewRow row in GvTypeAns.Rows)
        {
            Label lblm0_taidx_type = (Label)row.Cells[0].FindControl("lblm0_taidx_type");
            GridView GvQuestion = (GridView)row.Cells[0].FindControl("GvQuestion");


            int i = 0;
            var u1_ans = new u1_answer[GvQuestion.Rows.Count];

            foreach (GridViewRow row_quest in GvQuestion.Rows)
            {

                Label lblm0_quidx = (Label)row_quest.Cells[0].FindControl("lblm0_quidx");
                Label lblm0_toidx = (Label)row_quest.Cells[0].FindControl("lblm0_toidx");
                Label lblm0_tqidx = (Label)row_quest.Cells[0].FindControl("lblm0_tqidx");

                Label lblm0_taidx = (Label)row_quest.Cells[0].FindControl("lblm0_taidx");
                RadioButtonList rdochoice = (RadioButtonList)row_quest.Cells[0].FindControl("rdochoice");
                TextBox txtquest = (TextBox)row_quest.Cells[0].FindControl("txtquest");


                m0_tqidx = int.Parse(lblm0_tqidx.Text);

                _dtrcm.Boxu1_answer = new u1_answer[1];
                u1_ans[i] = new u1_answer();

                u1_ans[i].m0_toidx = int.Parse(lblm0_toidx.Text);
                u1_ans[i].m0_quidx = int.Parse(lblm0_quidx.Text);

                if (lblm0_taidx.Text == "1")
                {

                    if (rdochoice.SelectedValue != "0" && rdochoice.SelectedValue != null && rdochoice.SelectedValue != "")
                    {
                        u1_ans[i].answer = rdochoice.SelectedValue.ToString();
                    }
                }
                else if (lblm0_taidx.Text == "2")
                {
                    if (txtquest.Text != "" && txtquest.Text != null && txtquest.Text != "")
                    {
                        u1_ans[i].answer = txtquest.Text;
                    }
                }

                u1_ans[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                _dtrcm.Boxu1_answer = u1_ans;

                i++;

            }


            u0_answer u0_ans = new u0_answer();
            _dtrcm.Boxu0_answer = new u0_answer[1];

            u0_ans.m0_toidx = int.Parse(ViewState["m0_toidx"].ToString());
            u0_ans.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            u0_ans.PIDX = int.Parse(ViewState["PIDX"].ToString());
            u0_ans.unidx = 1;
            u0_ans.acidx = 1;
            u0_ans.staidx = 1;
            u0_ans.m0_taidx = int.Parse(lblm0_taidx_type.Text);
            u0_ans.m0_tqidx = m0_tqidx;

            _dtrcm.Boxu0_answer[0] = u0_ans;
            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

            _dtrcm = callServicePostRCM(_urlInsertAnswer, _dtrcm);

        }

        ViewState["Returncode"] = _dtrcm.ReturnCode.ToString();

    }
    protected void getExamdata()
    {
        employee_recuit_exam = new data_employee_recruit();
        topic_data _topic = new topic_data();
        employee_recuit_exam.topic_list = new topic_data[1];
        int m0_tqidx_sd = 1;
        if (ViewState["m0_tqidx"] != null) {
            m0_tqidx_sd = _funcTool.convertToInt(ViewState["m0_tqidx"].ToString());
        }
        _topic.m0_tqidx = m0_tqidx_sd;
        _topic.CEmpIDX = _funcTool.convertToInt(ViewState["EmpIDX"].ToString());
        employee_recuit_exam.topic_list[0] = _topic;
       employee_recuit_exam = callServicePostAPI(_urlGetDataExamBasic, employee_recuit_exam);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(employee_recuit_exam));
        if (employee_recuit_exam.return_code == "0")
        {
            ViewState["topic_list"] = employee_recuit_exam.topic_list;
            ViewState["type_answer_list"] = employee_recuit_exam.type_answer_list;
            ViewState["question_list"] = employee_recuit_exam.question_list;
        }

    }
    protected void Update_answer()
    {
        GridView GvTypeAns = (GridView)ViewDetailAdd.FindControl("GvTypeAns");
        int m0_tqidx = 0;
        data_rcm_question _dtrcm = new data_rcm_question();
        _dtrcm = new data_rcm_question();

        foreach (GridViewRow row in GvTypeAns.Rows)
        {
            int rowindex = GvTypeAns.Rows.Count;

            GridView GvQuestion = (GridView)row.Cells[0].FindControl("GvQuestion");
            Label lblm0_taidx_type = (Label)row.Cells[0].FindControl("lblm0_taidx_type");
            int i = 0;
            var u1_ans = new u1_answer[GvQuestion.Rows.Count];

            if (lblm0_taidx_type.Text == "2" && rowindex == 2 || lblm0_taidx_type.Text == "2" && rowindex == 1)
            {

                foreach (GridViewRow row_quest in GvQuestion.Rows)
                {

                    Label lblm0_quidx = (Label)row_quest.Cells[0].FindControl("lblm0_quidx");
                    Label lblm0_toidx = (Label)row_quest.Cells[0].FindControl("lblm0_toidx");
                    Label lblm0_tqidx = (Label)row_quest.Cells[0].FindControl("lblm0_tqidx");
                    Label lblu1_anidx = (Label)row_quest.Cells[0].FindControl("lblu1_anidx");
                    Label lblu0_anidx = (Label)row_quest.Cells[0].FindControl("lblu0_anidx");
                    Label lbldetermine = (Label)row_quest.Cells[0].FindControl("lbldetermine");

                    TextBox txtscore = (TextBox)row_quest.Cells[0].FindControl("txtscore");
                    HiddenField hfM0NodeIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0NodeIDX");
                    HiddenField hfM0ActoreIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0ActoreIDX");
                    HiddenField hfM0StatusIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0StatusIDX");
                    HiddenField hfM0Node_decision = (HiddenField)row_quest.Cells[0].FindControl("hfM0Node_decision");

                    ViewState["m0_node"] = hfM0NodeIDX.Value;
                    ViewState["m0_actor"] = hfM0ActoreIDX.Value;
                    ViewState["m0_status"] = hfM0StatusIDX.Value;
                    ViewState["m0_node_decision"] = hfM0Node_decision.Value;
                    ViewState["u0_anidx"] = lblu0_anidx.Text;
                    ViewState["lbldetermine"] = lbldetermine.Text;


                    m0_tqidx = int.Parse(lblm0_tqidx.Text);

                    _dtrcm.Boxu1_answer = new u1_answer[1];
                    u1_ans[i] = new u1_answer();


                    u1_ans[i].m0_toidx = int.Parse(lblm0_toidx.Text);
                    u1_ans[i].m0_quidx = int.Parse(lblm0_quidx.Text);
                    u1_ans[i].score = txtscore.Text;
                    u1_ans[i].u1_anidx = int.Parse(lblu1_anidx.Text);
                    u1_ans[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                    _dtrcm.Boxu1_answer = u1_ans;

                    i++;


                }


                u0_answer u0_ans = new u0_answer();
                _dtrcm.Boxu0_answer = new u0_answer[1];

                u0_ans.m0_toidx = int.Parse(ViewState["m0_toidx"].ToString());
                u0_ans.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                u0_ans.unidx = int.Parse(ViewState["m0_node"].ToString());
                u0_ans.acidx = int.Parse(ViewState["m0_actor"].ToString());


                if (ViewState["m0_node"].ToString() == "2" && m0_tqidx == 1) //  เจ้าหน้าที่ hr อนุมัติรายการพื้นฐาน
                {
                    u0_ans.staidx = int.Parse(ddlpass.SelectedValue);
                    u0_ans.determine = ddlpass.SelectedItem.Text;
                }
                else if (ViewState["m0_node"].ToString() == "3" && m0_tqidx == 2) //เจ้าหน้าที่รับผิดชอบอนุมัติรายการ เฉพาะทาง 
                {
                    u0_ans.staidx = int.Parse(ddlconfirm.SelectedValue);
                    u0_ans.determine = ddlpass.SelectedItem.Text;

                }
                else
                {


                    u0_ans.staidx = int.Parse(ViewState["m0_node_decision"].ToString());
                    u0_ans.determine = ViewState["lbldetermine"].ToString();
                }

                u0_ans.u0_anidx = int.Parse(ViewState["u0_anidx"].ToString());
                u0_ans.m0_tqidx = m0_tqidx;

                _dtrcm.Boxu0_answer[0] = u0_ans;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));
                _dtrcm = callServicePostRCM(_urlUpdateScore, _dtrcm);
                ViewState["ReturnCode"] = _dtrcm.ReturnCode.ToString();
                ViewState["ReturnMsg"] = _dtrcm.ReturnMsg.ToString();
                ViewState["ReturnEmpIDX"] = _dtrcm.ReturnEmpIDX.ToString();
            }
            else if (lblm0_taidx_type.Text == "1" && rowindex == 1)
            {
                foreach (GridViewRow row_quest in GvQuestion.Rows)
                {
                    Label lblm0_quidx = (Label)row_quest.Cells[0].FindControl("lblm0_quidx");
                    Label lblm0_toidx = (Label)row_quest.Cells[0].FindControl("lblm0_toidx");
                    Label lblm0_tqidx = (Label)row_quest.Cells[0].FindControl("lblm0_tqidx");
                    Label lblu1_anidx = (Label)row_quest.Cells[0].FindControl("lblu1_anidx");
                    Label lblu0_anidx = (Label)row_quest.Cells[0].FindControl("lblu0_anidx");
                    Label lbldetermine = (Label)row_quest.Cells[0].FindControl("lbldetermine");

                    TextBox txtscore = (TextBox)row_quest.Cells[0].FindControl("txtscore");
                    HiddenField hfM0NodeIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0NodeIDX");
                    HiddenField hfM0ActoreIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0ActoreIDX");
                    HiddenField hfM0StatusIDX = (HiddenField)row_quest.Cells[0].FindControl("hfM0StatusIDX");
                    HiddenField hfM0Node_decision = (HiddenField)row_quest.Cells[0].FindControl("hfM0Node_decision");

                    ViewState["m0_node"] = hfM0NodeIDX.Value;
                    ViewState["m0_actor"] = hfM0ActoreIDX.Value;
                    ViewState["m0_status"] = hfM0StatusIDX.Value;
                    ViewState["m0_node_decision"] = hfM0Node_decision.Value;
                    ViewState["u0_anidx"] = lblu0_anidx.Text;
                    ViewState["lbldetermine"] = lbldetermine.Text;
                    m0_tqidx = int.Parse(lblm0_tqidx.Text);
                    u0_answer u0_ans = new u0_answer();
                    _dtrcm.Boxu0_answer = new u0_answer[1];

                    u0_ans.m0_toidx = int.Parse(ViewState["m0_toidx"].ToString());
                    u0_ans.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                    u0_ans.unidx = int.Parse(ViewState["m0_node"].ToString());
                    u0_ans.acidx = int.Parse(ViewState["m0_actor"].ToString());


                    if (ViewState["m0_node"].ToString() == "2" && m0_tqidx == 1) //  เจ้าหน้าที่ hr อนุมัติรายการพื้นฐาน
                    {
                        u0_ans.staidx = int.Parse(ddlpass.SelectedValue);
                        u0_ans.determine = ddlpass.SelectedItem.Text;
                    }
                    else if (ViewState["m0_node"].ToString() == "3" && m0_tqidx == 2) //เจ้าหน้าที่รับผิดชอบอนุมัติรายการ เฉพาะทาง 
                    {
                        u0_ans.staidx = int.Parse(ddlconfirm.SelectedValue);
                        u0_ans.determine = ddlpass.SelectedItem.Text;
                    }
                    else
                    {
                        u0_ans.staidx = int.Parse(ViewState["m0_node_decision"].ToString());
                        u0_ans.determine = ViewState["lbldetermine"].ToString();
                    }

                    u0_ans.u0_anidx = int.Parse(ViewState["u0_anidx"].ToString());
                    u0_ans.m0_tqidx = m0_tqidx;

                    _dtrcm.Boxu0_answer[0] = u0_ans;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

                }

                _dtrcm = callServicePostRCM(_urlUpdateScore, _dtrcm);
                ViewState["ReturnCode"] = _dtrcm.ReturnCode.ToString();
                ViewState["ReturnMsg"] = _dtrcm.ReturnMsg.ToString();
                ViewState["ReturnEmpIDX"] = _dtrcm.ReturnEmpIDX.ToString();
            }

        }
    }
    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_rcm_question callServicePostRCM(string _cmdUrl, data_rcm_question _dtrcm)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtrcm);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtrcm = (data_rcm_question)_funcTool.convertJsonToObject(typeof(data_rcm_question), _localJson);


        return _dtrcm;
    }
    #endregion

    #region RowDatabound & RowCommand
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTypeAns":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeAns.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                        GridView GvQuestion = (GridView)e.Row.Cells[0].FindControl("GvQuestion");
                        Label lblm0_toidx_type = (Label)e.Row.Cells[0].FindControl("lblm0_toidx_type");

                        select_getquestion(GvQuestion, int.Parse(lblm0_toidx_type.Text), int.Parse(lblm0_taidx.Text), int.Parse(ViewState["m0_condition"].ToString()), int.Parse(ViewState["m0_cempidx"].ToString()), int.Parse(ViewState["u0_anidx"].ToString()));
                        //select_getquestion(GvQuestion, int.Parse("1"), int.Parse("1"), int.Parse("2"), int.Parse("173"), int.Parse(ViewState["u0_anidx"].ToString()));

                    }

                }
                break;

            case "GvQuestion":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm0_toidx = (Label)e.Row.Cells[0].FindControl("lblm0_toidx");
                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    Label lblno_choice = (Label)e.Row.Cells[0].FindControl("lblno_choice");
                    Panel panel_choice = (Panel)e.Row.Cells[0].FindControl("panel_choice");
                    Panel panel_comment = (Panel)e.Row.Cells[0].FindControl("panel_comment");
                    RadioButtonList rdochoice = (RadioButtonList)e.Row.Cells[0].FindControl("rdochoice");
                    TextBox txtquest = (TextBox)e.Row.Cells[0].FindControl("txtquest");
                    TextBox txtscore = (TextBox)e.Row.Cells[0].FindControl("txtscore");
                    Label lblchoice1 = (Label)e.Row.Cells[0].FindControl("lblchoice1");
                    Label lblchoice2 = (Label)e.Row.Cells[0].FindControl("lblchoice2");
                    Label lblchoice3 = (Label)e.Row.Cells[0].FindControl("lblchoice3");
                    Label lblchoice4 = (Label)e.Row.Cells[0].FindControl("lblchoice4");
                    Label lblanswer = (Label)e.Row.Cells[0].FindControl("lblanswer");
                    Control div_score = (Control)e.Row.Cells[0].FindControl("div_score");
                    HiddenField hfM0NodeIDX = (HiddenField)e.Row.Cells[0].FindControl("hfM0NodeIDX");
                    HiddenField hfM0ActoreIDX = (HiddenField)e.Row.Cells[0].FindControl("hfM0ActoreIDX");
                    HiddenField hfM0StatusIDX = (HiddenField)e.Row.Cells[0].FindControl("hfM0StatusIDX");
                    Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");

                    var images_q = (HyperLink)e.Row.FindControl("images_q");

                    if (lblm0_taidx.Text == "1")
                    {
                        panel_choice.Visible = true;
                        panel_comment.Visible = false;
                    }
                    else if (lblm0_taidx.Text == "2")
                    {
                        panel_choice.Visible = false;
                        panel_comment.Visible = true;

                    }


                    string getPathfile = ConfigurationManager.AppSettings["pathfile_recruit"];
                    string fileName_upload = lblm0_toidx.Text + "/" + lblm0_taidx.Text + "/" + lblno_choice.Text;// + "/" + "q.jpg"; 
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);
                    string pic = getPathfile + fileName_upload + "/q.jpg";

                    DirectoryInfo myDirLotus = new DirectoryInfo(filePath_upload);
                    SearchDirectories(myDirLotus, "q");

                    if (ViewState["path_q"].ToString() != "0")
                    {
                        images_q.Visible = true;
                        images_q.NavigateUrl = pic;

                        HtmlImage img = new HtmlImage();
                        img.Src = pic;
                        img.Height = 100;
                        images_q.Controls.Add(img);
                    }
                    else
                    {
                        images_q.Visible = false;
                    }
                    ViewState["path_q"] = "0";


                    for (int i = 1; i <= 4; i++)
                    {
                        string pic1 = getPathfile + fileName_upload + "/" + i + ".jpg";
                        DirectoryInfo myDirLotus1 = new DirectoryInfo(filePath_upload);
                        SearchDirectories(myDirLotus1, i.ToString());

                        if (i == 1)
                        {
                            if (ViewState["path_1"].ToString() == "0")
                            {
                                rdochoice.Items.Add(new ListItem(lblchoice1.Text, "1"));
                            }
                            else
                            {
                                // rdochoice.Items.Add(new ListItem(String.Format("<img src='{0}'>", pic1) + lblchoice1.Text, "1"));
                                //  rdochoice.Items.Add(new ListItem(String.Format("<img src='" + "'{0}'" + "'/>", pic1) + lblchoice1.Text, "1"));

                                //txt.Text = getPathfile + "," + filePath_upload + "," + pic1;
                                rdochoice.Items.Add(new ListItem(String.Format("<img Height=100px src='" + "uploadfiles/recruit/" + fileName_upload + "/" + i + ".jpg" + "'/><br />") + lblchoice1.Text, "1"));

                            }
                            ViewState["path_1"] = "0";

                        }
                        else if (i == 2)
                        {
                            if (ViewState["path_2"].ToString() == "0")
                            {
                                rdochoice.Items.Add(new ListItem(lblchoice2.Text, "2"));
                            }
                            else
                            {
                                rdochoice.Items.Add(new ListItem(String.Format("<img Height=100px src='" + "uploadfiles/recruit/" + fileName_upload + "/" + i + ".jpg" + "'/><br />") + lblchoice2.Text, "2"));
                            }
                            ViewState["path_2"] = "0";
                        }
                        else if (i == 3)
                        {
                            if (ViewState["path_3"].ToString() == "0")
                            {
                                rdochoice.Items.Add(new ListItem(lblchoice3.Text, "3"));
                            }
                            else
                            {
                                rdochoice.Items.Add(new ListItem(String.Format("<img Height=100px src='" + "uploadfiles/recruit/" + fileName_upload + "/" + i + ".jpg" + "'/><br />") + lblchoice3.Text, "3"));

                            }
                            ViewState["path_3"] = "0";
                        }
                        else if (i == 4)
                        {
                            if (ViewState["path_4"].ToString() == "0")
                            {
                                rdochoice.Items.Add(new ListItem(lblchoice4.Text, "4"));
                            }
                            else
                            {
                                rdochoice.Items.Add(new ListItem(String.Format("<img Height=100px src='" + "uploadfiles/recruit/" + fileName_upload + "/" + i + ".jpg" + "'/><br />") + lblchoice4.Text, "4"));

                            }
                            ViewState["path_4"] = "0";
                        }

                        if (ViewState["m0_condition"].ToString() == "1")
                        {
                            rdochoice.Enabled = false;
                            panel_comment.Enabled = false;
                            if (lblm0_taidx.Text == "1")
                            {
                                rdochoice.SelectedValue = lblanswer.Text;

                            }
                            else
                            {
                                txtquest.Text = lblanswer.Text;
                                div_score.Visible = true;
                                panel_comment.Enabled = true;
                                txtquest.Enabled = false;

                                if ((hfM0NodeIDX.Value == "3" && hfM0ActoreIDX.Value == "3" && hfM0StatusIDX.Value == "1" && lblm0_tqidx.Text == "2") ||
                                    (hfM0NodeIDX.Value == "2" && hfM0ActoreIDX.Value == "2" && hfM0StatusIDX.Value == "1" && lblm0_tqidx.Text == "1"))
                                {
                                    txtscore.Enabled = true;
                                }
                                else
                                {
                                    txtscore.Enabled = false;
                                }
                            }

                        }

                    }
                }

                break;

            case "GvSumExam":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeAns.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        Label lblunidx = (Label)e.Row.Cells[0].FindControl("lblunidx");
                        Label lbStatusDoc = (Label)e.Row.Cells[4].FindControl("lbStatusDoc");
                        Label lblnode_decision = (Label)e.Row.Cells[1].FindControl("lblnode_decision");
                        Label lbldetermine = (Label)e.Row.Cells[3].FindControl("lbldetermine");
                        Label lblstatusinterview = (Label)e.Row.Cells[3].FindControl("lblstatusinterview");
                        Label lblm0_tqidx = (Label)e.Row.Cells[2].FindControl("lblm0_tqidx");
                        Control div_showstatusinterview = (Control)e.Row.Cells[2].FindControl("div_showstatusinterview");



                        if (lblunidx.Text == "4" && lblnode_decision.Text == "4")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#002db3");
                            lbStatusDoc.Style["font-weight"] = "bold";
                        }
                        else if (lblunidx.Text == "4" && lblnode_decision.Text == "3")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            lbStatusDoc.Style["font-weight"] = "bold";
                        }
                        else
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            lbStatusDoc.Style["font-weight"] = "bold";
                        }

                        if (lbldetermine.Text == "อยู่ในระหว่างตรวจสอบ")
                        {
                            lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6600ff");
                            lbldetermine.Style["font-weight"] = "bold";
                        }
                        else if (lbldetermine.Text == "ผ่านแบบทดสอบ")
                        {
                            lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00cc99");
                            lbldetermine.Style["font-weight"] = "bold";
                        }
                        else if (lbldetermine.Text == "ไม่ผ่านแบบทดสอบ")
                        {
                            lbldetermine.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                            lbldetermine.Style["font-weight"] = "bold";
                        }

                        if (lblnode_decision.Text == "1")
                        {
                            lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            lblstatusinterview.Style["font-weight"] = "bold";
                        }
                        else if (lblnode_decision.Text == "3")
                        {
                            lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0099ff");
                            lblstatusinterview.Style["font-weight"] = "bold";
                        }
                        else if (lblnode_decision.Text == "4")
                        {
                            lblstatusinterview.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0066");
                            lblstatusinterview.Style["font-weight"] = "bold";
                        }


                        if (lblm0_tqidx.Text == "1")
                        {
                            div_showstatusinterview.Visible = false;
                        }
                        else
                        {
                            div_showstatusinterview.Visible = true;
                        }
                    }

                }
                break;
        }
    }
    #endregion

    #region reuse
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int countfile = dir.GetFiles().Length;

            ViewState["countfile"] = countfile;

            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name == "q.jpg" && target == "q")
                {

                    string[] f = Directory.GetFiles(dirfiles, "q.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_q"] = "1";
                }
                else if (file.Name == "1.jpg" && target == "1")
                {

                    string[] f = Directory.GetFiles(dirfiles, "1.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_1"] = "1";
                }
                else if (file.Name == "2.jpg" && target == "2")
                {

                    string[] f = Directory.GetFiles(dirfiles, "2.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_2"] = "1";
                }
                else if (file.Name == "3.jpg" && target == "3")
                {

                    string[] f = Directory.GetFiles(dirfiles, "3.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_3"] = "1";
                }
                else if (file.Name == "4.jpg" && target == "4")
                {

                    string[] f = Directory.GetFiles(dirfiles, "4.jpg", SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                    ViewState["path_4"] = "1";
                }


            }


        }
        catch (Exception ex)
        {
            // txt.Text = ex.ToString();
            ViewState["path"] = "0";
        }
    }

    protected void linkBtnTrigger(ImageButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void linkBtnTrigger1(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void SetDefault()
    {

        if (ViewState["identity_card"] != null)
        {
            if (ViewState["identity_card"].ToString() != "0")
            {
                if (ViewState["m0_condition"].ToString() == "2")
                {
                    Select_ProfileRecruit(ViewState["identity_card"].ToString());

                    if (ViewState["P_m0_toidx"].ToString() != "0")
                    {
                        string[] idxu0 = ViewState["P_m0_toidx"].ToString().Split(',');
                        foreach (string rtu0idx in idxu0)
                        {
                            if (rtu0idx != String.Empty && rtu0idx != "0")
                            {
                                ViewState["m0_toidx"] = rtu0idx;
                                MvMaster.SetActiveView(ViewDetailAdd);
                                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));
                            }
                        }
                    }
                    else
                    {
                        MvMaster.SetActiveView(ViewShowCompleteAgain);
                    }
                }
                else
                {
                    MvMaster.SetActiveView(ViewCheckResultHR);
                    select_empIdx_present();
                    gettypequestion(ddltypequest);
                    getOrganizationList(ddlOrg);


                    select_getresult(GvSumExam, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString())
                    , int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddltopic.SelectedValue));
                    mergeCell(GvSumExam);
                }
            }
        }
        else
        {
            MvMaster.SetActiveView(ViewCheckResultHR);
            select_empIdx_present();
            gettypequestion(ddltypequest);
            getOrganizationList(ddlOrg);
            select_getresult(GvSumExam, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddltopic.SelectedValue));
            mergeCell(GvSumExam);
            if (secidx.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
            {
                boxsearch.Visible = true;
            }
            else
            {
                boxsearch.Visible = false;
            }
        }

        linkBtnTrigger(imgbtnhome);
        linkBtnTrigger(imghomeagain);
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvSumExam":
                for (int rowIndex = GvSumExam.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvSumExam.Rows[rowIndex];
                    GridViewRow previousRow = GvSumExam.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[0].FindControl("lblPIDX")).Text == ((Literal)previousRow.Cells[0].FindControl("lblPIDX")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Literal)currentRow.Cells[1].FindControl("lbemp_name_th")).Text == ((Literal)previousRow.Cells[1].FindControl("lbemp_name_th")).Text &&
                       ((Label)currentRow.Cells[1].FindControl("lbldatedoing")).Text == ((Label)previousRow.Cells[1].FindControl("lbldatedoing")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }
                }
                break;
        }
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }


    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddName = (DropDownList)sender;

            switch (ddName.ID)
            {
                case "ddltypequest":
                    if (ddltypequest.SelectedValue == "1")
                    {
                        divchoosetype.Visible = false;
                    }
                    else if (ddltypequest.SelectedValue == "2")
                    {
                        divchoosetype.Visible = true;

                    }
                    gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), 0, 0, 0, 0);

                    break;

                case "ddlOrg":
                    getDepartmentList(ddlDep, int.Parse(ddlOrg.SelectedValue));
                    ddltopic.SelectedValue = "0";
                    break;
                case "ddlDep":
                    getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue));
                    if (ddlDep.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), 0, 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), 0, 0, 0);

                    }

                    break;

                case "ddlSec":
                    getPositionList(ddlPos, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue));
                    if (ddlSec.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue), 0);

                        }
                    }
                    else
                    {
                        gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                               , 0);
                    }
                    break;
                case "ddlPos":
                    if (ddlPos.SelectedValue != "0")
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , int.Parse(ddlPos.SelectedValue));

                        }
                    }
                    else
                    {
                        if (ddltypequest.SelectedValue == "2")
                        {
                            gettopicquestion(ddltopic, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue)
                                , 0);

                        }
                    }

                    break;
                case "ddlSearchDate":

                    if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                    {
                        AddEndDate.Enabled = true;
                    }
                    else
                    {
                        AddEndDate.Enabled = false;
                        AddEndDate.Text = string.Empty;
                    }

                    break;
            }
        }
    }
    #endregion


    protected void setActiveView(string activeTab, int doc_idx = 0)
    {
        MvMaster.SetActiveView((View)MvMaster.FindControl(activeTab));
        requset_more_data[] data_emp = (requset_more_data[])ViewState["data_emp"];
        switch (activeTab)
        {
            case "viewIndex":

                break;
            case "ViewExam":
                t_name_user.InnerText = data_emp[0].FirstNameTH.ToString() + " " + data_emp[0].LastNameTH.ToString();

                if (ViewState["data_exam"] == null)
                {
                    getExamdata();


                    if (ViewState["topic_list"] != null)
                    {
                        Reptertopic.DataSource = ViewState["topic_list"];
                        Reptertopic.DataBind();
                        ReptertopicTap.DataSource = ViewState["topic_list"];
                        ReptertopicTap.DataBind();
                    }
                }
                //t_name_user
                break;
            case "viewreadonly":

                break;
        }
    }
    protected void Boundtopicdata(object sender, RepeaterItemEventArgs args)
    {

        if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox m0_toidx = (TextBox)args.Item.FindControl("m0_toidx");

            Repeater childRepeater = (Repeater)args.Item.FindControl("ChildRepeater");
            type_answer_data[] type_answer_list = (type_answer_data[])ViewState["type_answer_list"];



            var ds_sample_data = new DataSet();
            ds_sample_data.Tables.Add("SampleData");
            ds_sample_data.Tables[0].Columns.Add("type_asn", typeof(String));
            ds_sample_data.Tables[0].Columns.Add("m0_taidx", typeof(int));
            ds_sample_data.Tables[0].Columns.Add("m0_toidx", typeof(int));
            foreach (type_answer_data val in type_answer_list)
            {
                var dr_Add_Sample = ds_sample_data.Tables[0].NewRow();
                dr_Add_Sample["type_asn"] = val.type_asn;
                dr_Add_Sample["m0_taidx"] = val.m0_taidx;
                dr_Add_Sample["m0_toidx"] = m0_toidx.Text;

                ds_sample_data.Tables[0].Rows.Add(dr_Add_Sample);
            }

            childRepeater.DataSource = ds_sample_data;
            childRepeater.DataBind();
        }
    }
    protected void Boundtopictype(object sender, RepeaterItemEventArgs args)
    {

        if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater Repquestion = (Repeater)args.Item.FindControl("Repquestion");
            TextBox m0_toidx = (TextBox)args.Item.FindControl("m0_toidx");
            TextBox m0_taidx = (TextBox)args.Item.FindControl("m0_taidx");

            question_data[] question_list = (question_data[])ViewState["question_list"];
            if (question_list != null) {
                /////////////////  ข้อมูล ของ m0  /////////////////////////
                var _ViewState_m0 = (from m0_viewstate in question_list
                                     where m0_viewstate.m0_toidx == _funcTool.convertToInt(m0_toidx.Text) && m0_viewstate.m0_taidx == _funcTool.convertToInt(m0_taidx.Text)
                                     select m0_viewstate).ToList();

                ////////////////////////////////////////////////////////
                /////

                Repquestion.DataSource = _ViewState_m0;
                Repquestion.DataBind();

            }

          
        }
    }
    protected void add_answer()
    {
        Repeater ReptertopicTap = (Repeater)ViewExam.FindControl("ReptertopicTap");
        int empidx = _funcTool.convertToInt(ViewState["EmpIDX"].ToString());
        if (ReptertopicTap.Items.Count > 0)
        {
            requset_more_data[] data_emp = (requset_more_data[])ViewState["data_emp"];
            employee_recuit_exam = new data_employee_recruit();
            employee_recuit_exam.recruit_u0_answer_list = new recruit_u0_answer_data[ReptertopicTap.Items.Count];
            employee_recuit_exam.requset_more_list = new requset_more_data[1];
            requset_more_data requset_more_data = new requset_more_data();

           
            employee_recuit_exam.requset_more_list[0] = requset_more_data;

            topic_data _topic = new topic_data();
            _topic.m0_tqidx = 1;
            if (ViewState["m0_tqidx"] != null)
            {
                _topic.m0_tqidx = _funcTool.convertToInt(ViewState["m0_tqidx"].ToString());

                requset_more_data.doc_decision = 17;
                requset_more_data.acidx = 1;
                requset_more_data.unidx = 11;
            }
            else {
                requset_more_data.doc_decision = data_emp[0].doc_decision;
                requset_more_data.acidx = data_emp[0].acidx;
                requset_more_data.unidx = data_emp[0].unidx;
            }
            employee_recuit_exam.topic_list = new topic_data[1];
            employee_recuit_exam.topic_list[0] = _topic;

            int i = 0;
            int ii = 0;
            question_data[] tank_u1 = (question_data[])ViewState["question_list"];
            recruit_u1_answer_data[] data_u1 = new recruit_u1_answer_data[tank_u1.Count()];
            foreach (RepeaterItem temp_ReptertopicTap in ReptertopicTap.Items)
            {
                TextBox m0_toidx_tap = (TextBox)temp_ReptertopicTap.FindControl("m0_toidx");
                recruit_u0_answer_data tank_u0_answer = new recruit_u0_answer_data();
                tank_u0_answer.m0_toidx = _funcTool.convertToInt(m0_toidx_tap.Text);
                tank_u0_answer.status = 1;
                tank_u0_answer.CEmpIDX = empidx;
                tank_u0_answer.UEmpIDX = empidx;

                Repeater ChildRepeater = (Repeater)temp_ReptertopicTap.FindControl("ChildRepeater");
                if (ChildRepeater.Items.Count > 0)
                {

                    foreach (RepeaterItem temp_ChildRepeater in ChildRepeater.Items)
                    {
                        TextBox m0_taidx_child = (TextBox)temp_ChildRepeater.FindControl("m0_taidx");
                        Repeater Repquestion = (Repeater)temp_ChildRepeater.FindControl("Repquestion");
                        if (Repquestion.Items.Count > 0)
                        {
                            foreach (RepeaterItem temp_Repquestion in Repquestion.Items)
                            {
                                data_u1[ii] = new recruit_u1_answer_data();
                                TextBox m0_taidx_re = (TextBox)temp_Repquestion.FindControl("m0_taidx");
                                TextBox m0_toidx_re = (TextBox)temp_Repquestion.FindControl("m0_toidx");
                                TextBox m0_quidx_child = (TextBox)temp_Repquestion.FindControl("m0_quidx");
                                string answer_emp = "0";
                                if (m0_taidx_re.Text == "1")
                                {
                                    RadioButton q_1 = (RadioButton)temp_Repquestion.FindControl("ra_choice_a");
                                    RadioButton q_2 = (RadioButton)temp_Repquestion.FindControl("ra_choice_b");
                                    RadioButton q_3 = (RadioButton)temp_Repquestion.FindControl("ra_choice_c");
                                    RadioButton q_4 = (RadioButton)temp_Repquestion.FindControl("ra_choice_d");

                                    if (q_1.Checked == true)
                                    {
                                        answer_emp = "1";
                                    }
                                    else if (q_2.Checked == true)
                                    {
                                        answer_emp = "2";
                                    }
                                    else if (q_3.Checked == true)
                                    {
                                        answer_emp = "3";
                                    }
                                    else if (q_4.Checked == true)
                                    {
                                        answer_emp = "4";
                                    }
                                }
                                else if (m0_taidx_re.Text == "2")
                                {
                                    TextBox answer_txt = (TextBox)temp_Repquestion.FindControl("answer_txt");
                                    answer_emp = answer_txt.Text;
                                }

                                data_u1[ii].m0_toidx = _funcTool.convertToInt(m0_toidx_re.Text);
                                data_u1[ii].m0_quidx = _funcTool.convertToInt(m0_quidx_child.Text);
                                data_u1[ii].answer = answer_emp;
                                data_u1[ii].status = 1;
                                data_u1[ii].CEmpIDX = empidx;
                                data_u1[ii].UEmpIDX = empidx;
                                ii++;
                            }
                        }
                    }
                }
                employee_recuit_exam.recruit_u0_answer_list[i] = tank_u0_answer;
                i++;
            }

            employee_recuit_exam.recruit_u1_answer_list = data_u1;

        }

        litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(employee_recuit_exam));
        employee_recuit_exam = callServicePostAPI(_urlSetDataExamBasic, employee_recuit_exam);
        if (employee_recuit_exam.return_code == "0")
        {
            setActiveView("viewreadonly");
        }
    }
    protected data_employee_recruit callServicePostAPI(string _cmdUrl, data_employee_recruit _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data = (data_employee_recruit)_funcTool.convertJsonToObject(typeof(data_employee_recruit), _localJson);


        return _data;
    }

    #region btn
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {
            #region CmdDetail
            case "CmdDetail":
                string[] arg1_add = new string[2];
                arg1_add = e.CommandArgument.ToString().Split(';');
                ViewState["m0_toidx"] = arg1_add[0];
                ViewState["m0_cempidx"] = arg1_add[1];
                MvMaster.SetActiveView(ViewDetailAdd);
                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));
                setOntop.Focus();

                break;
            #endregion CmdDetail
            #region CmdDetail_ViewScore
            case "CmdDetail_ViewScore":
                string[] arg1_view = new string[6];
                arg1_view = e.CommandArgument.ToString().Split(';');
                ViewState["m0_toidx"] = arg1_view[0];
                ViewState["m0_cempidx"] = arg1_view[1];
                ViewState["u0_anidx"] = arg1_view[2];
                string node = arg1_view[3];
                string m0_tqidx = arg1_view[4];
                string actor = arg1_view[5];

                MvMaster.SetActiveView(ViewDetailAdd);

                linkBtnTrigger1(lbladdscore);

                ViewState["m0_condition"] = "1";
                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));
                lbladd.Visible = false;
                panel_log.Visible = true;
                select_gettypeanswer(rptLog, int.Parse(ViewState["u0_anidx"].ToString()));

                //txt.Text = "node = " + node + ", actor = " + actor + ", m0_tqidx = " + m0_tqidx;

                if (node == "4")
                {
                    lbladdscore.Visible = false;
                    div_selecrpass.Visible = false;
                }
                else
                {
                    if (node == "3" && actor == "3" && m0_tqidx == "2" || node == "2" && actor == "2" && m0_tqidx == "1")
                    {
                        div_selecrpass.Visible = true;
                        if (node == "3" && actor == "3" && m0_tqidx == "2")
                        {
                            div_call.Visible = true;
                        }
                        else
                        {
                            div_call.Visible = false;
                        }
                    }
                    else
                    {
                        div_selecrpass.Visible = false;
                    }
                    lbladdscore.Visible = true;

                }
                setOntop.Focus();

                break;
            #endregion CmdDetail_ViewScore
            #region btnsaveanswer
            case "btnsaveanswer":
                Insert_answer();

                if (ViewState["Returncode"].ToString() != "0")
                {

                    ViewState["m0_toidx"] = ViewState["Returncode"].ToString();
                    select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));
                }
                else
                {
                    MvMaster.SetActiveView(ViewComplete);
                    linkBtnTrigger(imgbtnhome);
                }
                setOntop.Focus();

                break;
            #endregion btnsaveanswer
            #region cmdback
            case "cmdback":
                ddltypequest.SelectedValue = "0";
                ddlOrg.SelectedValue = "0";
                ddlDep.SelectedValue = "0";
                ddlSec.SelectedValue = "0";
                ddlPos.SelectedValue = "0";
                ddltopic.SelectedValue = "0";
                ddlSearchDate.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                AddEndDate.Text = String.Empty;
                divchoosetype.Visible = false;
                select_getresult(GvSumExam, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddltopic.SelectedValue));
                mergeCell(GvSumExam);
                setOntop.Focus();
                break;
            #endregion cmdback
            #region btnsavescore
            case "btnsavescore":
                Update_answer();

                if (ViewState["ReturnCode"].ToString() != "0")
                {
                    MvMaster.SetActiveView(ViewCheckResultHR);
                    select_getresult(GvSumExam, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddltopic.SelectedValue));
                    mergeCell(GvSumExam);
                    setOntop.Focus();
                }
                else
                {

                    //url = "http://mas.taokaenoi.co.th/hr-employee-recurit";
                    url = "http://localhost/mas.taokaenoi.co.th/hr-employee-recurit";
                    HttpResponse response1 = HttpContext.Current.Response;
                    response1.Clear();

                    StringBuilder s1 = new StringBuilder();
                    s1.Append("<html>");
                    s1.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                    s1.AppendFormat("<form name='form' action='{0}' method='post'>", url);
                    s1.Append("<input type='hidden' name='identity_card' value=" + ViewState["ReturnMsg"].ToString() + " />");
                    s1.Append("<input type='hidden' name='CempIDX' value=" + ViewState["ReturnEmpIDX"].ToString() + " />");
                    s1.Append("</form></body></html>");
                    response1.Write(s1.ToString());
                    response1.End();
                }
                break;
            #endregion btnsavescore
            #region btnCancel
            case "btnCancel":

                if (ViewState["m0_condition"].ToString() == "1")
                {
                    MvMaster.SetActiveView(ViewCheckResultHR);
                    select_empIdx_present();
                    select_getresult(GvSumExam, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ddltopic.SelectedValue));
                    mergeCell(GvSumExam);
                    linkBtnTrigger(imgbtnhome);
                }
                else
                {
                    MvMaster.SetActiveView(ViewIndex);
                    select_gettopicquestion(GvTopic, 0, 0, 0, 0, 0);
                    ViewState["m0_toidx"] = "0";
                }
                setOntop.Focus();

                break;
            #endregion btnCancel
            #region btnsearch
            case "btnsearch":
                selectSearch_getresult(GvSumExam, int.Parse(ddltypequest.SelectedValue), int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue), int.Parse(ddlSec.SelectedValue), int.Parse(ddltopic.SelectedValue), int.Parse(ddlSearchDate.SelectedValue), AddStartdate.Text, AddEndDate.Text);
                mergeCell(GvSumExam);

                break;
            #endregion btnsearch
            #region btnhome
            case "btnhome":
                //url = "http://mas.taokaenoi.co.th/hr-employee-register";
                url = "http://localhost/mas.taokaenoi.co.th/hr-employee-register";
                //Response.Write("<script>window.open('http://demo.taokaenoi.co.th/hr-employee-register','_blank');</script>");
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder s = new StringBuilder();
                s.Append("<html>");
                s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s.AppendFormat("<form name='form' action='{0}' method='post'>", url);

                s.Append("</form></body></html>");
                response.Write(s.ToString());
                response.End();

                break;
            #endregion btnhome
            #region btntest
            case "add_answer":
                add_answer();
                break;
            #endregion btntest
            case "backTomain":
                int empidx = _funcTool.convertToInt(ViewState["EmpIDX"].ToString());
                string key_code = "tkn_toedit_recuit";
                string url_ = "~/hr-employee-register-v2?pageview=editview&idx=" + empidx + "&sum=" + _funcTool.getMd5Sum(key_code + empidx);
                Response.Redirect(url_);
                break;
        }


    }
    #endregion



}