﻿<%@ Page Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_Trn_plan_course_test.aspx.cs" Inherits="websystem_el_Trn_plan_course_test" %>


<%--
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <link href="../../../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-datetimepicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/custom.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/fa/css/font-awesome.min.css" runat="server" rel="stylesheet" />
    <link href="./../Content/emps-contents/empshift-style-general.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-clockpicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/colorpicker/css/bootstrap-colorpicker.min.css" runat="server" rel="stylesheet" />

    <link href="../../../Content/jquery-ui-1.12.1/jquery-ui.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/fullcalendar.css" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />

    

</head>
<body>
    <form id="form1" runat="server">--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">



    <asp:Literal ID="Litdebug" runat="server" Text=""></asp:Literal>
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">


            <div style="height: auto; width: 100%; margin: 0 0; display: block; border: 3px solid #D9B4B4;font-family:'MS Reference Sans Serif';">
                <div runat="server" id="dv_header">

                    <%-- start header --%>


                    <div class="row">


                        <asp:Panel ID="Panel_course" runat="server" Height="300px">
                            <div class="row" runat="server" id="dv_course">
                                <table style="width: 100%; margin-top: 180px;">
                                    <tr>
                                        <td style="width: 40%;">&nbsp;</td>
                                        <td style="width: 60%; margin-left: 0;">
                                            <div class="row h4">
                                                <div class="form-group">
                                                    <asp:Label ID="lbcourse" runat="server" class="col-md-3 control-labelnotop text_right" Text="คอร์สอบรม :" />
                                                    <div class="col-md-9">

                                                        <asp:Literal ID="litcourse" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>

                                            <div class="row h4">

                                                <div class="form-group">

                                                    <asp:Label ID="lblecturer_type" class="col-md-3 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                                    <div class="col-md-9">
                                                        <asp:GridView ID="Gvinstitution"
                                                            CssClass="col-md-12 word-wrap"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            HeaderStyle-CssClass="default"
                                                            ShowFooter="false"
                                                            AllowPaging="false"
                                                            ShowHeader="false"
                                                            GridLines="None">

                                                            <Columns>

                                                                <asp:TemplateField
                                                                    HeaderText="ชื่อ - สกุล"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>


                                            </div>


                                        </td>

                                    </tr>

                                </table>

                            </div>

                        </asp:Panel>

                    </div>




                    <%-- end header --%>
                </div>
                <%-- detail --%>

                <div class="row" runat="server" visible="true">
                    <div class="col-md-12">
                        <div class="alert-message alert-message-success" runat="server" visible="false">
                            <blockquote class="danger" style="font-size: small; background-color: lightseagreen; color: aliceblue">
                                <h4><b>
                                    <asp:Label ID="lbltopic" runat="server"></asp:Label></b></h4>
                            </blockquote>
                        </div>
                        <asp:Panel ID="pnl_alert" runat="server" Visible="false">
                            <div class="alert alert-danger col-md-12 h3" style="text-align: center" id="dv_alert" runat="server">

                                <asp:Literal ID="lit_alert" runat="server" Text="ไม่พบข้อมูล"></asp:Literal>
                            </div>
                        </asp:Panel>

                        <div class="panel" runat="server" id="pnldata">

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:Panel ID="pnl_permission" runat="server" Visible="false">
                                        <div class="alert alert-danger col-md-12 h4" style="text-align: center" id="Div2" runat="server">

                                            <asp:Literal ID="lit_permission" runat="server" Text="คุณไม่มีสิทธิ์เรียนวิชานี้"></asp:Literal>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="pnl_gvdata" runat="server" Visible="true">
                                        <asp:GridView ID="GvListData"
                                            CssClass="table col-md-12 word-wrap"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            HeaderStyle-CssClass="default"
                                            ShowFooter="false"
                                            AllowPaging="false"
                                            ShowHeader="false"
                                            GridLines="None"
                                            OnRowDataBound="onRowDataBound">

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText=""
                                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbu0_training_course_idx" Visible="false" runat="server" Text='<%# Eval("u0_training_course_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbu2_course_idx" Visible="false" runat="server" Text='<%# Eval("u2_course_idx") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_item" Visible="false" runat="server" Text='<%# Eval("course_item") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_proposition" Visible="false" runat="server" Text='<%# Eval("course_proposition") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_a" Visible="false" runat="server" Text='<%# Eval("course_propos_a") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_b" Visible="false" runat="server" Text='<%# Eval("course_propos_b") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_c" Visible="false" runat="server" Text='<%# Eval("course_propos_c") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_d" Visible="false" runat="server" Text='<%# Eval("course_propos_d") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_proposition_img" Visible="false" runat="server" Text='<%# Eval("course_proposition_img") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_a_img" Visible="false" runat="server" Text='<%# Eval("course_propos_a_img") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_b_img" Visible="false" runat="server" Text='<%# Eval("course_propos_b_img") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_c_img" Visible="false" runat="server" Text='<%# Eval("course_propos_c_img") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_d_img" Visible="false" runat="server" Text='<%# Eval("course_propos_d_img") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_propos_answer" Visible="false" runat="server" Text='<%# Eval("course_propos_answer") %>'></asp:Label>
                                                        <asp:Label ID="lbtraining_course_item" Visible="false" runat="server" Text='<%# Eval("training_course_item") %>'></asp:Label>
                                                        <asp:Label ID="lbtraining_course_propos_answer" Visible="false" runat="server" Text='<%# Eval("training_course_propos_answer") %>'></asp:Label>
                                                        <asp:Label ID="lbcourse_no" Visible="false" runat="server" Text='<%# Eval("course_no") %>'></asp:Label>

                                                        <div class="word-wrap">

                                                            <asp:Label ID="lbid" runat="server" Visible="false" Text='<%# Eval("id") %>'></asp:Label>

                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-11">
                                                                    <asp:Image ID="img_proposition" runat="server" Width="150" Height="130" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-11">
                                                                    <asp:Label ID="lbzName" runat="server" Text='<%# Eval("training_course_item")+". "+Eval("course_proposition") %>'></asp:Label>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-11">

                                                                    <asp:RadioButtonList ID="rdoItem" runat="server" CssClass="control-labelnotop checkbox text_left"
                                                                        RepeatLayout="Flow" RepeatDirection="Vertical">
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                        <br />
                                        <div class="col-md-12">
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-11">
                                            <asp:LinkButton CssClass="btn btn-success" runat="server"
                                                Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                                data-toggle="tooltip" title="บันทึก"
                                                ID="btnSaveInsert"
                                                OnCommand="btnCommand" CommandName="btnSaveInsert" />

                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <footer>
                    <div runat="server" id="dv_footer">
                        <div class="row">
                            <asp:Panel ID="Panel2" runat="server" Height="250px"></asp:Panel>
                        </div>
                    </div>
                </footer>

            </div>


        </asp:View>

        <asp:View ID="ViewResult" runat="server">

            <asp:Panel ID="pnl_complete" runat="server" Visible="true">
                <div class="alert col-md-12 h4" id="Div1" runat="server">

                    <div class="row">
                       <%-- <div class="col-md-12" style="text-align: center">--%>
                            <%--<div class="col-md-12">--%>
                                <blockquote class="danger" style="font-size: medium; background-color: lavender;">
                                    <p><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                                        <asp:Literal ID="lit_result" runat="server" Text="ทดสอบหลังการฝึกอบรมเสร็จแล้ว"></asp:Literal></p>

                                </blockquote>
                            <%--</div>--%>
                       <%-- </div>--%>
                    </div>

                    <div class="row">
                        <asp:GridView ID="GvListResult"
                            CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                            runat="server"
                            AutoGenerateColumns="false"
                            HeaderStyle-CssClass="default"
                            ShowFooter="false"
                            AllowPaging="false"
                            ShowHeader="false"
                            GridLines="None"
                            OnRowDataBound="onRowDataBound">

                            <EmptyDataTemplate>
                                <div style="text-align: center">-</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText=""
                                    ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                    HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>

                                        <div class="word-wrap">

                                            <div class="row">

                                                <div class="form-group">

                                                    <asp:Label ID="Label20" class="col-md-3 control-labelnotop text_right" runat="server" Text="รหัส :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="lbemp_code" runat="server"
                                                            Text='<%# Eval("emp_code") %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">

                                                    <asp:Label ID="Label1" class="col-md-3 control-labelnotop text_right" runat="server" Text="ชื่อ :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="lbemp_name_th" runat="server"
                                                            Text='<%# Eval("emp_name_th") %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label10" class="col-md-3 control-labelnotop text_right" runat="server" Text="จำนวนครั้งที่สอบ :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="Label11" runat="server"
                                                            Text='<%# Eval("item_count") %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label2" class="col-md-3 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="lbcourse_score" runat="server"
                                                            Text='<%# Eval("course_score") %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label3" class="col-md-3 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="lbscore_through_per" runat="server"
                                                            Text='<%# Eval("score_through_per") %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label4" class="col-md-3 control-labelnotop text_right" runat="server" Text="คะแนนสอบ :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="Label5" runat="server"
                                                            Text='<%# getResultScoreTotal((decimal)Eval("course_score_avg"),(int)Eval("score_qty")) %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label8" class="col-md-3 control-labelnotop text_right" runat="server" Text="การประเมิน :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="Label9" runat="server"
                                                            Text='<%# getCourseAssessmentName((int)Eval("course_assessment")) %>'
                                                            CssClass="control-label" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">

                                                    <asp:Label ID="Label6" class="col-md-3 control-labelnotop text_right" runat="server" Text="ผลการสอบ :" />

                                                    <div class="col-md-9">
                                                        <asp:Label ID="lbresult_flag" runat="server" Visible="false"
                                                            Text='<%# Eval("result_flag") %>'
                                                            CssClass="control-label" />

                                                        <asp:Label ID="Label7" runat="server" Visible="true"
                                                            Text='<%# getStatusLevel((int)Eval("result_flag")) %>'
                                                            CssClass="control-label" />
                                                        <asp:Panel runat="server" ID="pnlicon_yes">

                                                            <img src='<%# ResolveUrl("~/images/elearning/icon_yes.png") %>' width="30" height="30" title="ผ่าน" id="imgicon_yes" runat="server" visible="true" />

                                                        </asp:Panel>
                                                        <asp:Panel runat="server" ID="pnlicon_no">

                                                            <img src='<%# ResolveUrl("~/images/elearning/icon_no.png") %>' width="30" height="30" title="ไม่ผ่าน" id="imgicon_no" runat="server" visible="true" />

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                    <div class="row">
                        <div class="form-group">

                            <div class="col-md-offset-2 col-md-10 ">

                                <asp:LinkButton ID="btnhome" runat="server"
                                    CommandName="btnhome" CssClass="btn btn-default"
                                    OnCommand="btnCommand"><i class="fa fa-home" >

                                                       </i>&nbsp;หน้าแรก</asp:LinkButton>

                                <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                    Text="<i class='fa fa-file'></i> ทำข้อสอบ"
                                    ID="btnrefresh"
                                    OnCommand="btnCommand" CommandName="btnrefresh" />

                            </div>
                        </div>
                    </div>
                    <hr />
                </div>

            </asp:Panel>

        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>

    <script type="text/javascript">

        function CallJS() {
            PageMethods.CodebehindMethodName(onSucceed, onError);
            return false;
        }
        function onSucceed(value) {
            alert(value)
        }
        function onError(value) {
            alert(value)
        }
    </script>

    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <style type="text/css">
        .wrapword_p1 {
            word-wrap: break-word;
        }
    </style>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;

        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from-createstart').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


        });
    </script>


    <script type="text/javascript">


        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            //  stepping: 30,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            //   stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>
    <script type="text/javascript">

        function SetTarget() {

            document.forms[0].target = "_blank";

        }
        function shwwindow(myurl) {
            window.open(myurl, '_blank');
        }

    </script>

    <div>
    </div>

</asp:Content>

<%--</form>
</body>
</html>--%>
