using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_controlpanel_managemenu : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_menu _dataMenu = new data_menu();
    data_system _data_system = new data_system();
    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetMenu = _serviceUrl + ConfigurationManager.AppSettings["urlSetMenu"];
    static string _urlGetMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetMenu"];
    static string _urlDeleteMenu = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMenu"];
    static string _urlSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSystem"];
    static string _urlGetSystem = _serviceUrl + ConfigurationManager.AppSettings["urlGetSystem"];
    static string _urlDeleteSystem = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteSystem"];
    static string _urlGetPermissionType = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermissionType"];
    static string _urlGetPermission = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermission"];
    static string _urlSetPermission = _serviceUrl + ConfigurationManager.AppSettings["urlSetPermission"];

    static string _urlGetPermissionMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermissionMenu"];
    static string _urlDeletePermission = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePermission"];

    static string _urlSetRole = _serviceUrl + ConfigurationManager.AppSettings["urlSetRole"];
    static string _urlGetRole = _serviceUrl + ConfigurationManager.AppSettings["urlGetRole"];
    static string _urlDeleteRole = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteRole"];
    static string _urlGetEditRole = _serviceUrl + ConfigurationManager.AppSettings["urlGetEditRole"];
    static string _urlGetEditRoleCheckBox = _serviceUrl + ConfigurationManager.AppSettings["urlGetEditRoleCheckBox"];

    static string _urlGetEmpMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmpMenu"];

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    int emp_idx = 0;
    int rdept_idx = 0;
    string _localJson = "";
	int _tempInt = 0;
    int _menu_idx = 0;
    string _defaultDdlText;
    string _defaultDdlValue;

    int _system_idx = 0;
    int _permissiontype_idx = 0;
    int _permission_idx = 0;
    int _permission_idxmenu = 0;

    int _role_idx = 0;


    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        actionIndex();
        rdept_idx = int.Parse(ViewState["rdept_idx"].ToString());

        ViewState["system_idxmenu"] = 0; // set ��� sysidx ����͡��������٢ͧ�к�
        ViewState["system_idxmenuper"] = 0; // set ��� sysidx ����͡��������٢ͧ�к�
        ViewState["menu_idxmenuper"] = 0; // set ��� sysidx ����͡��������٢ͧ�к�

        if (!IsPostBack)
    	{
            initPage();
            initPageSystem();           
            getSystemData(0);


        }
      
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //*** var in menu ***//
        string _menu_name_th;
        string _menu_name_en;
        string _menu_url;
        int _menu_relation;
        int _menu_order;
        string _menu_icon;
        int _cemp_idx;
        int _rdept_idx;
        int _menu_status;
        //*** var in menu ***//

        //*** var in system ***//
        ////int _systemidxmenu;
        string _system_name_th;
        string _system_name_en;
        int _system_order;
        int _system_status;
        int _systidxmenu;

        int _menuidxpermission;
        int _systidxpermission;

        string _permission_name;
        int _permission_type;


        string _role_name_th;
        string _role_name_en;

        int _systidxrole;
        int _menuidxrole;

        //*** var in system ***//

        m0_system_detail _systemDetail = new m0_system_detail();
        m0_menu_detail _menuDetail = new m0_menu_detail();
        m0_permission_detail _permissionDetail = new m0_permission_detail();
        m0_permissiontype_detail _permissiontypeDetail = new m0_permissiontype_detail();
        m0_role_detail _roleDetail = new m0_role_detail();


        DropDownList ddlMenuRelation = (DropDownList)fvMenu.FindControl("ddlMenuRelation");
        HiddenField hfMenuRelation = (HiddenField)fvMenu.FindControl("hfMenuRelation");

        switch (cmdName)
        {
            case "cmdSaveMenu":        
                _menu_idx = int.Parse(cmdArg);
                _systidxmenu = int.Parse(((Label)fvMenu.FindControl("fvinsertsysidx")).Text.ToString());
                _menu_name_th = ((TextBox)fvMenu.FindControl("tbNameTH")).Text.Trim();
                _menu_name_en = ((TextBox)fvMenu.FindControl("tbNameEN")).Text.Trim();
                _menu_url = ((TextBox)fvMenu.FindControl("tbURL")).Text.Trim();
                _menu_relation = int.Parse(ddlMenuRelation.SelectedValue);
                _menu_order = 0;
                _menu_icon = "";
                _cemp_idx = emp_idx;
                _menu_status = 1;
                //_systemidxmenu = ViewState["system_idxmenu"];

                _dataMenu.m0_menu_list = new m0_menu_detail[1];
                _menuDetail.system_idx = _systidxmenu; // int.Parse(ViewState["system_idxmenu"].ToString());
                _menuDetail.menu_idx = _menu_idx;
                _menuDetail.menu_name_th = _menu_name_th;
                _menuDetail.menu_name_en = _menu_name_en;
                _menuDetail.menu_url = _menu_url;
                _menuDetail.menu_relation = _menu_relation;
                _menuDetail.menu_order = _menu_order;
                _menuDetail.menu_icon = _menu_icon;
                _menuDetail.cemp_idx = _cemp_idx;
                _menuDetail.menu_status = _menu_status;
                _dataMenu.m0_menu_list[0] = _menuDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlSetMenu, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    getMenuData(0, _systidxmenu.ToString());
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);
                    setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, _systidxmenu.ToString(), "0", "0");
              

                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }

                break;
            case "cmdEditMenu":
             
                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(';');
                int _menu_idx1 = int.Parse(arg1[0]);
                int _systidxmenu1 = int.Parse(arg1[1]);
               
                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_menu_list = new m0_menu_detail[1];
                _menuDetail.menu_idx = _menu_idx1;
                _menuDetail.system_idx = _systidxmenu1;
                _dataMenu.m0_menu_list[0] = _menuDetail;

                _dataMenu = callServiceMenu(_urlGetMenu, _dataMenu);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    setFormData(fvMenu, FormViewMode.Edit, _dataMenu.m0_menu_list, _systidxmenu1.ToString(), "0","0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;

            case "cmdInsertSubMenu":

                string[] arg2 = new string[2];
                arg2 = e.CommandArgument.ToString().Split(';');
                int _menu_idx2 = int.Parse(arg2[0]);
                int _systidxmenu2 = int.Parse(arg2[1]);

                _dataMenu.m0_menu_list = new m0_menu_detail[1];
                _menuDetail.menu_idx = 0;
                _menuDetail.system_idx = _systidxmenu2;
                _menuDetail.menu_relation = _menu_idx2;//int.Parse(cmdArg);
                _dataMenu.m0_menu_list[0] = _menuDetail;
                setFormData(fvMenu, FormViewMode.Edit, _dataMenu.m0_menu_list, _systidxmenu2.ToString(), "0","0");

                break;
            case "cmdClearMenu":

                ////fvViewSystemList.Visible = true;
                ////fvViewSystem.Visible = true;

                ////fvViewMenu.Visible = false;
                ////fvViewMenudetail.Visible = false;
                ////fvBacktoSystem.Visible = false;

                ////setFormData(fvSystem, FormViewMode.Insert, null, "0", "0","0");


                //*** Clear 
                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                TextBox tbNameTH = (TextBox)fvMenu.FindControl("tbNameTH");
                TextBox tbNameEN = (TextBox)fvMenu.FindControl("tbNameEN");
                DropDownList ddlMenuRelation1 = (DropDownList)fvMenu.FindControl("ddlMenuRelation");
                TextBox tbURL = (TextBox)fvMenu.FindControl("tbURL");

                fvBacktoSystem.Visible = true;
                fvViewMenu.Visible = true;
                fvViewMenudetail.Visible = true;


                tbNameTH.Text = "";
                tbNameEN.Text = "";
                ddlMenuRelation1.SelectedValue = "0";
                tbURL.Text = "";

                break;

            case "cmdCancelMenu":
             
                //*** Clear 
                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;
             
                Label fvinsertsysidx = (Label)fvMenu.FindControl("fvinsertsysidx");

                ViewState["insertsysidx"] = fvinsertsysidx.Text;

                fvBacktoSystem.Visible = true;
                fvViewMenu.Visible = true;
                fvViewMenudetail.Visible = true;

                setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, ViewState["insertsysidx"].ToString(), "0", "0");
             
                break;

            case "cmdDeleteSubMenu":

                string[] arg3 = new string[2];
                arg3 = e.CommandArgument.ToString().Split(';');
                int _menu_idx3 = int.Parse(arg3[0]);
                int _systidxmenu3 = int.Parse(arg3[1]);

                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_menu_list = new m0_menu_detail[1];
                _menuDetail.menu_idx = _menu_idx3;//_menu_idx;               
                _dataMenu.m0_menu_list[0] = _menuDetail;

                _dataMenu = callServiceMenu(_urlDeleteMenu, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    ////setDataList(dtlMenu, _dataMenu.m0_menu_list);
                    getMenuData(0, _systidxmenu3.ToString());
                    setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, _systidxmenu3.ToString(), "0","0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;

            case "cmdAddRoleSystem":

                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;


                _system_idx = int.Parse(cmdArg);
                ViewState["_system_idx_role"] = _system_idx;


                fvViewAddRole.Visible = true;
                fvBacktoSysteminAddRole.Visible = true;
                fvViewRole.Visible = true;      
                                     
               
                setFormData(fvAddRole, FormViewMode.Insert, _dataMenu.m0_menu_list, _system_idx.ToString(), "0","0");
                getRole(0, "0", ViewState["_system_idx_role"].ToString());

                break;
            case "cmdBacktoSysteminAddRole": // ��Ѻ��к� �ҡ˹�� ADD Role

              
                fvViewAddRole.Visible = false;
                fvBacktoSysteminAddRole.Visible = false;
                fvViewRole.Visible = false;


                fvViewSystemList.Visible = true;
                fvViewSystem.Visible = true;

                setFormData(fvSystem, FormViewMode.Insert, null, "0", "0","0");


                break;

         
            case "cmdClearRole":

                ////fvBacktoSysteminAddRole.Visible = false;
                ////fvViewAddRole.Visible = false;
                ////fvViewRole.Visible = false;

                ////fvViewSystemList.Visible = true;
                ////fvViewSystem.Visible = true;

                ////setFormData(fvSystem, FormViewMode.Insert, null, "0", "0", "0");
                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;


              
                Label rolesysidx = (Label)fvAddRole.FindControl("rolesysidx");
                TextBox tbRoleNameTH = (TextBox)fvAddRole.FindControl("tbRoleNameTH");
                TextBox tbRoleNameEN = (TextBox)fvAddRole.FindControl("tbRoleNameEN");
                DropDownList ddlMenuNameTH = (DropDownList)fvAddRole.FindControl("ddlMenuNameTH");
                CheckBoxList rdopermissiontype11 = (CheckBoxList)fvAddRole.FindControl("rdopermissiontype");


                fvViewAddRole.Visible = true;
                fvBacktoSysteminAddRole.Visible = true;
                fvViewRole.Visible = true;

                ViewState["rolesysidx"] = rolesysidx.Text;
              

                tbRoleNameTH.Text = "";
                tbRoleNameEN.Text = "";
                ddlMenuNameTH.SelectedValue = "00";

                setFormData(fvAddRole, FormViewMode.Insert, _dataMenu.m0_menu_list, ViewState["rolesysidx"].ToString(), "0", "0");
                getRole(0, "0", ViewState["rolesysidx"].ToString());

  
                break;

          
            case "cmdCancelRole":

                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                Label rolesysidxcancel = (Label)fvAddRole.FindControl("rolesysidx");
                //Label rolemenuidx11 = (Label)fvAddRole.FindControl("rolemenuidx");

                //_system_idx = int.Parse(cmdArg);
                ViewState["rolesysidxcancel"] = rolesysidxcancel.Text;


                fvViewAddRole.Visible = true;
                fvBacktoSysteminAddRole.Visible = true;
                fvViewRole.Visible = true;


                setFormData(fvAddRole, FormViewMode.Insert, _dataMenu.m0_menu_list, ViewState["rolesysidxcancel"].ToString(), "0", "0");
                getRole(0, "0", ViewState["rolesysidxcancel"].ToString());

                break;

            case "cmdClearPermission":

                ////fvBacktoMenu.Visible = false;
                ////fvDataPermissionlist.Visible = false;
                ////fvViewPermissionMenu.Visible = false;

                ////fvViewSystemList.Visible = true;
                ////fvViewSystem.Visible = true;

                ////setFormData(fvSystem, FormViewMode.Insert, null, "0", "0", "0");

                //�Դ˹�� ADD Permission in menu
                fvBacktoSystem.Visible = false;
                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;
              
                TextBox tbPermissionName = (TextBox)fvPermission.FindControl("tbPermissionName");
                DropDownList ddlPermissionTypeper = (DropDownList)fvPermission.FindControl("ddlPermissionType");



                fvBacktoMenu.Visible = true;
                //fvViewPermission.Visible = true;
                fvViewPermissionMenu.Visible = true;
                fvDataPermissionlist.Visible = true;

                tbPermissionName.Text = "";
                ddlPermissionTypeper.SelectedValue = "00";

  
                break;

            case "cmdCancelPermission":

                ////fvBacktoMenu.Visible = false;
                ////fvDataPermissionlist.Visible = false;
                ////fvViewPermissionMenu.Visible = false;

                ////fvViewSystemList.Visible = true;
                ////fvViewSystem.Visible = true;

                ////setFormData(fvSystem, FormViewMode.Insert, null, "0", "0", "0");

                //�Դ˹�� ADD Permission in menu
                fvBacktoSystem.Visible = false;
                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                Label permissionsysidx = (Label)fvPermission.FindControl("permissionsysidx");
                Label permissionmenuidx = (Label)fvPermission.FindControl("permissionmenuidx");

                ViewState["permissionsysidx"] = permissionsysidx.Text;
                ViewState["permissionmenuidx"] = permissionmenuidx.Text;


                fvBacktoMenu.Visible = true;
                //fvViewPermission.Visible = true;
                fvViewPermissionMenu.Visible = true;
                fvDataPermissionlist.Visible = true;

                setFormData(fvPermission, FormViewMode.Insert, _dataMenu.m0_permission_list, ViewState["permissionsysidx"].ToString(), ViewState["permissionmenuidx"].ToString(), "0");
                getPermission(0, ViewState["permissionmenuidx"].ToString(), ViewState["permissionsysidx"].ToString());


                break;

            case "cmdSaveSystem":

                //HiddenField hfSystemIdx = (HiddenField)fvSystem.FindControl("hfSystemIdx");

                _system_idx = int.Parse(cmdArg);
                _system_name_th = ((TextBox)fvSystem.FindControl("tbSysNameTH")).Text.Trim();
                _system_name_en = ((TextBox)fvSystem.FindControl("tbSysNameEN")).Text.Trim();                
                _system_order = 0;                
                _cemp_idx = emp_idx;
                _system_status = 1;

                _rdept_idx = rdept_idx;

                _dataMenu.m0_system_list = new m0_system_detail[1];            
                _systemDetail.system_idx = _system_idx;
                _systemDetail.system_name_th = _system_name_th;
                _systemDetail.system_name_en = _system_name_en;               
                _systemDetail.system_order = _system_order;              
                _systemDetail.cemp_idx = _cemp_idx;
               // _systemDetail.rdept_idx = _rdept_idx;
                _systemDetail.system_status = _system_status;
                _dataMenu.m0_system_list[0] = _systemDetail;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));


                _dataMenu = callServiceMenu(_urlSystem, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                    //clearSession();
                    //clearViewState();        
                    initPageSystem();
                    setDataList(dtlSystem, _dataMenu.m0_system_list);

                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }

                break;
            case "cmdEditSystem":

               _system_idx = int.Parse(cmdArg);

                _dataMenu.m0_system_list = new m0_system_detail[1];
                _systemDetail.system_idx = _system_idx;

                _dataMenu.m0_system_list[0] = _systemDetail;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlGetSystem, _dataMenu);
              
                // check return_code
                if (_dataMenu.return_code == 0)

                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
                    setFormData(fvSystem, FormViewMode.Edit, _dataMenu.m0_system_list, "0", "0","0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }

                break;
            case "cmdDeleteSystem":
                _system_idx = int.Parse(cmdArg);
                _dataMenu.m0_system_list = new m0_system_detail[1];
                _systemDetail.system_idx = _system_idx;
                _dataMenu.m0_system_list[0] = _systemDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlDeleteSystem, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    setDataList(dtlSystem, _dataMenu.m0_system_list);
                    setFormData(fvSystem, FormViewMode.Insert, _dataMenu.m0_system_list, "0", "0","0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;
            case "cmdCancelSystem":
                setFormData(fvSystem, FormViewMode.Insert, null, "0", "0","0");

                break;
            case "cmdInsertMenu":

                //Label lblMenuLevel = (Label)dtlMenu.FindControl("lblMenuLevel");
                //Label lblMenuNameTH = (Label)dtlMenu.FindControl("lblMenuNameTH");

                //Set Form �Դ �Դ ����������к�
                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                _system_idx = int.Parse(cmdArg);
                ViewState["system_idxmenu"] = _system_idx;
          
                fvBacktoSystem.Visible = true;
                fvViewMenu.Visible = true;
                fvViewMenudetail.Visible = true;
                           
                setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, _system_idx.ToString(), "0","0");
                getMenuData(0, _system_idx.ToString());
             
                break;
            case "cmdBacktoSystem":

                //Set Form �Դ �Դ ��Ѻ�˹���к�
                fvBacktoSystem.Visible = false;
                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvBacktoMenu.Visible = false;
                fvDataPermissionlist.Visible = false;
                //fvViewPermission.Visible = false;
                fvViewPermissionMenu.Visible = false;


                fvViewSystemList.Visible = true;
                fvViewSystem.Visible = true;

                setFormData(fvSystem, FormViewMode.Insert, null, "0", "0","0");

                //(fvSystem, FormViewMode.Insert, null);
                break;

            case "cmdBacktoMenu":

                //�к�
                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                fvBacktoMenu.Visible = false;
                //fvViewPermission.Visible = false;
                fvDataPermissionlist.Visible = false;
                fvViewPermissionMenu.Visible = false;

                //Set Form �Դ �Դ ��Ѻ�˹���к�
                fvBacktoSystem.Visible = true;
                fvViewMenu.Visible = true;
                fvViewMenudetail.Visible = true;
                //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0");
                break;

            case "cmdAddPermission":

                
                //�Դ˹�� ADD Permission in menu
                fvBacktoSystem.Visible = false;
                fvViewMenu.Visible = false;
                fvViewMenudetail.Visible = false;

                fvViewSystemList.Visible = false;
                fvViewSystem.Visible = false;

                string[] arg4 = new string[2];
                arg4 = e.CommandArgument.ToString().Split(';');
                int _menu_idx4 = int.Parse(arg4[0]);
                int _systidxmenu4 = int.Parse(arg4[1]);

                ViewState["system_idxmenuper"] = _systidxmenu4; // set ��� sysidx ����͡��������٢ͧ�к�
                ViewState["menu_idxmenuper"] = _menu_idx4; // set ��� sysidx ����͡��������٢ͧ�к�


                fvBacktoMenu.Visible = true;
                //fvViewPermission.Visible = true;
                fvViewPermissionMenu.Visible = true;
                fvDataPermissionlist.Visible = true;

                setFormData(fvPermission, FormViewMode.Insert, _dataMenu.m0_permission_list, _systidxmenu4.ToString(), _menu_idx4.ToString(),"0");               
                getPermission(0, ViewState["menu_idxmenuper"].ToString(), ViewState["system_idxmenuper"].ToString());

               
                break;
               
          
            case "cmdSavePermission":
              
                DropDownList ddlPermissionType = (DropDownList)fvPermission.FindControl("ddlPermissionType");
                _cemp_idx = emp_idx;

                _permission_idx = int.Parse(cmdArg);
                _systidxpermission = int.Parse(((Label)fvPermission.FindControl("permissionsysidx")).Text.ToString());
                _menuidxpermission = int.Parse(((Label)fvPermission.FindControl("permissionmenuidx")).Text.ToString());

                _permission_name = ((TextBox)fvPermission.FindControl("tbPermissionName")).Text.Trim();
                _permission_type = int.Parse(ddlPermissionType.SelectedValue);

                _dataMenu.m0_permission_list = new m0_permission_detail[1];
                _permissionDetail.permission_idx = _permission_idx;
                _permissionDetail.system_idx = _systidxpermission;
                _permissionDetail.menu_idx = _menuidxpermission;
                _permissionDetail.permission_name = _permission_name;
                _permissionDetail.permission_type = _permission_type;
                _permissionDetail.cemp_idx = _cemp_idx;

                _dataMenu.m0_permission_list[0] = _permissionDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlSetPermission, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  

                    getPermission(0, _menuidxpermission.ToString(), _systidxpermission.ToString());

                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);
                    setFormData(fvPermission, FormViewMode.Insert, _dataMenu.m0_permission_list, _systidxpermission.ToString(), _menuidxpermission.ToString(),"0");

                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }

                break;

            case "cmdEditPermission":

                string[] arg5 = new string[3];
                arg5 = e.CommandArgument.ToString().Split(';');
                int _permission_idx5 = int.Parse(arg5[0]);
                int _systidxmenu5 = int.Parse(arg5[1]);
                int _menu_idx5 = int.Parse(arg5[2]);

                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_permission_list = new m0_permission_detail[1];
                _permissionDetail.menu_idx = _menu_idx5;
                _permissionDetail.system_idx = _systidxmenu5;
                _permissionDetail.permission_idx = _permission_idx5;
                _dataMenu.m0_permission_list[0] = _permissionDetail;

                _dataMenu = callServiceMenu(_urlGetPermissionMenu, _dataMenu);
                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    setFormData(fvPermission, FormViewMode.Edit, _dataMenu.m0_permission_list, _systidxmenu5.ToString(), _menu_idx5.ToString(),"0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;
            case "cmdDeletePermission":

                string[] arg6 = new string[3];
                arg6 = e.CommandArgument.ToString().Split(';');
                int _permission_idx6 = int.Parse(arg6[0]);
                int _systidxmenu6 = int.Parse(arg6[1]);
                int _menu_idx6 = int.Parse(arg6[2]);

                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_permission_list = new m0_permission_detail[1];                
                _permissionDetail.menu_idx = _menu_idx6;
                _permissionDetail.system_idx = _systidxmenu6;
                _permissionDetail.permission_idx = _permission_idx6;
                _dataMenu.m0_permission_list[0] = _permissionDetail;


                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlDeletePermission, _dataMenu);

                //check return_code
                if (_dataMenu.return_code == 0)
                {
                    ////setDataList(dtlMenu, _dataMenu.m0_menu_list);
                    //getMenuData(0, _systidxmenu3.ToString());

                    getPermission(0, _menu_idx6.ToString(), _systidxmenu6.ToString());
                    //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, _systidxmenu3.ToString(), "0");
                    setFormData(fvPermission, FormViewMode.Insert, _dataMenu.m0_permission_list, _systidxmenu6.ToString(), _menu_idx6.ToString(),"0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;

            case "cmdSaveRole":

                DropDownList ddlMenuNameTH1 = (DropDownList)fvAddRole.FindControl("ddlMenuNameTH");
                CheckBoxList rdopermissiontype = (CheckBoxList)fvAddRole.FindControl("rdopermissiontype");
                _cemp_idx = emp_idx;

                _role_idx = int.Parse(cmdArg);

                _systidxrole = int.Parse(((Label)fvAddRole.FindControl("rolesysidx")).Text.ToString());
                // _menuidxpermission = int.Parse(((Label)fvPermission.FindControl("permissionmenuidx")).Text.ToString());

                _role_name_th = ((TextBox)fvAddRole.FindControl("tbRoleNameTH")).Text.Trim();
                _role_name_en = ((TextBox)fvAddRole.FindControl("tbRoleNameEN")).Text.Trim();
                _menuidxrole = int.Parse(ddlMenuNameTH1.SelectedValue);
          
                _dataMenu.m0_role_list = new m0_role_detail[1];
                _roleDetail.role_idx = _role_idx;
                _roleDetail.role_name_th = _role_name_th;
                _roleDetail.role_name_en = _role_name_en;            
                _roleDetail.system_idx = _systidxrole;
                _roleDetail.menu_idx = _menuidxrole;
                _roleDetail.cemp_idx = _cemp_idx;


                // **** ǹ�ٻ CheckBox in Permission ****//
                string sumcheck_per = "";

                foreach (ListItem rdopermissiontypes in rdopermissiontype.Items)
                {
                    if (rdopermissiontypes.Selected)
                    {
                        //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

                        sumcheck_per += rdopermissiontypes.Value + ",";
                    }
                }
                _roleDetail.permission_idxstring = sumcheck_per;
               
                _dataMenu.m0_role_list[0] = _roleDetail;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
               
                // *** Check Alert Role Permission ***//
                if (sumcheck_per == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Please Select Role Permission');", true);
                }
                else
                {
                    _dataMenu = callServiceMenu(_urlSetRole, _dataMenu);


                    //check return_code
                    if (_dataMenu.return_code == 0)
                    {
                        //initPage();
                        //setDataList(dtlMenu, _dataMenu.m0_menu_list);  

                        getRole(0, _menuidxrole.ToString(), _systidxrole.ToString());

                        //setDataList(dtlMenu, _dataMenu.m0_menu_list);
                        setFormData(fvAddRole, FormViewMode.Insert, _dataMenu.m0_menu_list, _systidxrole.ToString(), _menuidxrole.ToString(), "0");

                    }
                    else
                    {
                        setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                    }
                }
                
                

                break;
            case "cmdDeleteRole":

                string[] arg7 = new string[3];
                arg7 = e.CommandArgument.ToString().Split(';');
                int _role_idx7 = int.Parse(arg7[0]);
                int _rolesystidxmenu7 = int.Parse(arg7[1]);
                int _rolemenu_idx7 = int.Parse(arg7[2]);

                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_role_list = new m0_role_detail[1];
                _roleDetail.menu_idx = _rolemenu_idx7;
                _roleDetail.system_idx = _rolesystidxmenu7;
                _roleDetail.role_idx = _role_idx7;
                _dataMenu.m0_role_list[0] = _roleDetail;


                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlDeleteRole, _dataMenu);

                //check return_code
                if (_dataMenu.return_code == 0)
                {
                    ////setDataList(dtlMenu, _dataMenu.m0_menu_list);
                    //getMenuData(0, _systidxmenu3.ToString());

                    getRole(0, _rolemenu_idx7.ToString(), _rolesystidxmenu7.ToString());
                    //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list, _systidxmenu3.ToString(), "0");
                    setFormData(fvAddRole, FormViewMode.Insert, _dataMenu.m0_menu_list, _rolesystidxmenu7.ToString(), _rolemenu_idx7.ToString(),"0");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;
            case "cmdEditRole":

                string[] arg8 = new string[3];
                arg8 = e.CommandArgument.ToString().Split(';');
                int _role_idx8 = int.Parse(arg8[0]);
                int _rolesyst_idx8 = int.Parse(arg8[1]);
                int _rolemenu_idx8 = int.Parse(arg8[2]);

                CheckBoxList rdopermissiontype1 = (CheckBoxList)fvAddRole.FindControl("rdopermissiontype");

                ////_menu_idx = int.Parse(cmdArg);
                _dataMenu.m0_role_list = new m0_role_detail[1];
                _roleDetail.menu_idx = _rolemenu_idx8;
                _roleDetail.system_idx = _rolesyst_idx8;
                _roleDetail.role_idx = _role_idx8;
                _dataMenu.m0_role_list[0] = _roleDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                _dataMenu = callServiceMenu(_urlGetEditRole, _dataMenu);

         
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
                //check return_code
                if (_dataMenu.return_code == 0)
                {
                    setFormData(fvAddRole, FormViewMode.Edit, _dataMenu.m0_role_list, _rolesyst_idx8.ToString(), _rolemenu_idx8.ToString(),_role_idx8.ToString());


                   
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }
                break;

        }
    }
    #endregion btnCommand

    #region bind data

    ////#region selected    
    ////protected void actionIndex()
    ////{
    ////    data_system _data_system1 = new data_system();

    ////    _data_system1.empmenusystem_list = new empmenusystem_detail[1];
    ////    empmenusystem_detail _empDetail = new empmenusystem_detail();

    ////    _empDetail.emp_idx = int.Parse(Session["emp_idx"].ToString());

    ////    _data_system1.empmenusystem_list[0] = _empDetail;

    ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
    ////    _data_system1 = callServiceMenuSystem(_urlGetEmpMenu, _data_system1);

    ////    ViewState["rdept_idx"] = _data_system1.empmenusystem_list[0].rdept_idx;
    ////    ViewState["rsec_idx"] = _data_system1.empmenusystem_list[0].rsec_idx;


    ////}

    ////#endregion selected

    #region selected    
    protected void actionIndex()
    {
        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + Session["emp_idx"].ToString());


        //ViewState["rdept_idx"] = _data_employee.empmenusystem_list[0].rdept_idx;
        //ViewState["rsec_idx"] = _data_employee.empmenusystem_list[0].rsec_idx;

        ViewState["rdept_idx"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected

    protected void getMenuData(int _menu_idx,string sysidxmenu)
    {

       
        _dataMenu.m0_menu_list = new m0_menu_detail[1];
        m0_menu_detail _menuDetail = new m0_menu_detail();
        _menuDetail.system_idx = int.Parse(sysidxmenu);
        _menuDetail.menu_idx = _menu_idx;
        _dataMenu.m0_menu_list[0] = _menuDetail;

        _dataMenu = callServiceMenu(_urlGetMenu, _dataMenu);

        // check return_code
        if(_dataMenu.return_code == 0)
        {
            setDataList(dtlMenu, _dataMenu.m0_menu_list);
          
            //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_menu_list);
        }
        else
        {
            setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
        }
    }
  
    protected void getSystemData(int _system_idx)
    {

        data_menu _dataMenu = new data_menu();
        _dataMenu.m0_system_list = new m0_system_detail[1];
        m0_system_detail _systemDetail = new m0_system_detail();
        _systemDetail.system_idx = _system_idx;
        _dataMenu.m0_system_list[0] = _systemDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataMenu));
        _dataMenu = callServiceMenu(_urlGetSystem, _dataMenu);

        // check return_code
        if (_dataMenu.return_code == 0)
        {
            setDataList(dtlSystem, _dataMenu.m0_system_list);
        }
        else
        {
            setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
        }
    }

    protected void getPermissionType(int _permissiontype_idx)
    {

        DropDownList ddlPermissionType = (DropDownList)fvPermission.FindControl("ddlPermissionType");

        ddlPermissionType.Items.Clear();
        ddlPermissionType.AppendDataBoundItems = true;
        ddlPermissionType.Items.Add(new ListItem("Select PermissionType...", "00"));

      
        //data_menu _dataMenu1 = new data_menu();

        _dataMenu.m0_permissiontype_list = new m0_permissiontype_detail[1];
        m0_permissiontype_detail _permissiontypeDetail = new m0_permissiontype_detail();
        _permissiontypeDetail.permissiontype_idx = 0;

        _dataMenu.m0_permissiontype_list[0] = _permissiontypeDetail;

        _dataMenu = callServiceMenu(_urlGetPermissionType, _dataMenu);
        //_dataMenu1 = (data_menu)_funcTool.convertXmlToObject(typeof(data_menu), _localJson);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu1));
        //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_permissiontype_list, "0");

        ddlPermissionType.DataSource = _dataMenu.m0_permissiontype_list;
        ddlPermissionType.DataTextField = "permissiontype_name";
        ddlPermissionType.DataValueField = "permissiontype_idx";
        ddlPermissionType.DataBind();
       
    }

  
    protected void getPermission(int _permission_idxmenu, string menuidxinpermission, string sysidxinpermission)
    {
      
        _dataMenu.m0_permission_list = new m0_permission_detail[1];
        m0_permission_detail _permissionDetail = new m0_permission_detail();
        _permissionDetail.system_idx = int.Parse(sysidxinpermission);
        _permissionDetail.menu_idx = int.Parse(menuidxinpermission);
        _permissionDetail.permission_idx = _permission_idxmenu;
        _dataMenu.m0_permission_list[0] = _permissionDetail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
        _dataMenu = callServiceMenu(_urlGetPermissionMenu, _dataMenu);

        

        //check return_code
        if (_dataMenu.return_code == 0)
        {
            setDataList(dtlpermissionmenu, _dataMenu.m0_permission_list);
            //setFormData(fvPermission, FormViewMode.Insert, _dataMenu1.m0_menu_list, "0", "0");
        }
        else
        {
            setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
        }
    }

    protected void getRole(int _role_idxsys, string menuidxrolesys, string sysidxrolesys)
    {

        _dataMenu.m0_role_list = new m0_role_detail[1];
        m0_role_detail _roleDetail = new m0_role_detail();
        _roleDetail.system_idx = int.Parse(sysidxrolesys);
        _roleDetail.menu_idx = int.Parse(menuidxrolesys);
        _roleDetail.role_idx = _role_idxsys;
        _dataMenu.m0_role_list[0] = _roleDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
        _dataMenu = callServiceMenu(_urlGetRole, _dataMenu);


        //check return_code
        if (_dataMenu.return_code == 0)
        {
            setDataList(dtlAddRole, _dataMenu.m0_role_list);
            //setFormData(fvPermission, FormViewMode.Insert, _dataMenu1.m0_menu_list, "0", "0");
        }
        else
        {
            setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
        }
    }

    protected void getRoleEdit(int _role_idxsys, string menuidxrolesys, string sysidxrolesys)
    {

        _dataMenu.m0_role_list = new m0_role_detail[1];
        m0_role_detail _roleDetail = new m0_role_detail();
        _roleDetail.system_idx = int.Parse(sysidxrolesys);
        _roleDetail.menu_idx = int.Parse(menuidxrolesys);
        _roleDetail.role_idx = _role_idxsys;
        _dataMenu.m0_role_list[0] = _roleDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu));
        _dataMenu = callServiceMenu(_urlGetEditRole, _dataMenu);


        //check return_code
        if (_dataMenu.return_code == 0)
        {
            setDataList(dtlAddRole, _dataMenu.m0_role_list);
            //setFormData(fvPermission, FormViewMode.Insert, _dataMenu1.m0_menu_list, "0", "0");
        }
        else
        {
            setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
        }
    }

    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {
            case "fvMenu":
                HiddenField hfMenuIdx = (HiddenField)fvMenu.FindControl("hfMenuIdx");
                HiddenField hfMenuRelation = (HiddenField)fvMenu.FindControl("hfMenuRelation");
                DropDownList ddlMenuRelation = (DropDownList)fvMenu.FindControl("ddlMenuRelation");

                Label fvinsertsysidx = (Label)fvMenu.FindControl("fvinsertsysidx");
                TextBox tbsysidx = (TextBox)fvMenu.FindControl("tbsysidx");
                fvinsertsysidx.Text = sysidx_menu;
                //fvinsertsysidx.Text = _system_idx.ToString();
                //tbsysidx.Text = _system_idx.ToString();

                //Label fvinsertsysidx = (Label)fvMenu.FindControl("fvinsertsysidx");

                //litDebug.Text = hfSysMenuIdx.Value;

                string _temp = "0";
                if (hfMenuIdx.Value == "0")
                {
                    _temp = hfMenuRelation.Value;
                    // litDebug.Text = _temp;
                }

                //if(fvinsertsysidx.Text != "0")
                //{
                //    _funcTool.showAlert(this, fvinsertsysidx.Text);
                //}
                //else
                //{
                //    _funcTool.showAlert(this, "N0");
                //}               

                _dataMenu.m0_menu_list = new m0_menu_detail[1];
                m0_menu_detail _menuDetail = new m0_menu_detail();
                _menuDetail.menu_idx = 0;
                _menuDetail.system_idx = int.Parse(sysidx_menu.ToString());
                _dataMenu.m0_menu_list[0] = _menuDetail;

                _dataMenu = callServiceMenu(_urlGetMenu, _dataMenu);

                // check return_code
                if (_dataMenu.return_code == 0)
                {
                    if (hfMenuIdx.Value != "0")
                    {
                        var _linqFilterSelf = from data in _dataMenu.m0_menu_list
                                              where data.menu_idx != int.Parse(hfMenuIdx.Value)
                                              select data;
                        setDropDownList(ddlMenuRelation, _linqFilterSelf);
                    }
                    else
                    {
                        setDropDownList(ddlMenuRelation, _dataMenu.m0_menu_list);
                        ddlMenuRelation.SelectedValue = _temp;
                    }
                    setError("");
                }
                else
                {
                    setError(_dataMenu.return_code.ToString() + " - " + _dataMenu.return_msg);
                }

                break;

            case "fvSystem":

                break;

            case "fvPermission":

                //HiddenField hfPermissionIdx = (HiddenField)fvPermission.FindControl("hfPermissionIdx");

                HiddenField hfPermissionIdx = (HiddenField)fvPermission.FindControl("hfPermissionIdx");
                HiddenField hfddlPermissionIdx = (HiddenField)fvPermission.FindControl("hfddlPermissionIdx");

                DropDownList ddlPermissionType = (DropDownList)fvPermission.FindControl("ddlPermissionType");
                Label permissionsysidx = (Label)fvPermission.FindControl("permissionsysidx");
                Label permissionmenuidx = (Label)fvPermission.FindControl("permissionmenuidx");


                permissionsysidx.Text = sysidx_menu;
                permissionmenuidx.Text = menuidx_per;



                if (hfPermissionIdx.Value == "0")
                {
                    // Start Show DropdownList PermissionType //
                    ddlPermissionType.Items.Clear();
                    ddlPermissionType.AppendDataBoundItems = true;
                    ddlPermissionType.Items.Add(new ListItem("Select PermissionType...", "00"));


                    data_menu _dataMenu2 = new data_menu();

                    _dataMenu2.m0_permissiontype_list = new m0_permissiontype_detail[1];
                    m0_permissiontype_detail _permissiontypeDetail = new m0_permissiontype_detail();
                    _permissiontypeDetail.permissiontype_idx = 0;

                    _dataMenu2.m0_permissiontype_list[0] = _permissiontypeDetail;

                    _dataMenu2 = callServiceMenu(_urlGetPermissionType, _dataMenu2);
                    //_dataMenu1 = (data_menu)_funcTool.convertXmlToObject(typeof(data_menu), _localJson);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu1));
                    //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_permissiontype_list, "0");

                    ddlPermissionType.DataSource = _dataMenu2.m0_permissiontype_list;
                    ddlPermissionType.DataTextField = "permissiontype_name";
                    ddlPermissionType.DataValueField = "permissiontype_idx";
                    ddlPermissionType.DataBind();

                    // END Show DropdownList PermissionType //

                }
                else
                {


                    ddlPermissionType.SelectedValue = hfddlPermissionIdx.Value;

                    data_menu _dataMenu2 = new data_menu();

                    _dataMenu2.m0_permissiontype_list = new m0_permissiontype_detail[1];
                    m0_permissiontype_detail _permissiontypeDetail = new m0_permissiontype_detail();
                    _permissiontypeDetail.permissiontype_idx = 0;

                    _dataMenu2.m0_permissiontype_list[0] = _permissiontypeDetail;

                    _dataMenu2 = callServiceMenu(_urlGetPermissionType, _dataMenu2);
                    //_dataMenu1 = (data_menu)_funcTool.convertXmlToObject(typeof(data_menu), _localJson);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataMenu1));
                    //setFormData(fvMenu, FormViewMode.Insert, _dataMenu.m0_permissiontype_list, "0");

                    ddlPermissionType.DataSource = _dataMenu2.m0_permissiontype_list;
                    ddlPermissionType.DataTextField = "permissiontype_name";
                    ddlPermissionType.DataValueField = "permissiontype_idx";
                    ddlPermissionType.DataBind();
                }


                break;

            case "fvAddRole":

                HiddenField hfAddRoleIdx = (HiddenField)fvAddRole.FindControl("hfAddRoleIdx");
                //HiddenField hfAddRoleRelationMenu = (HiddenField)fvAddRole.FindControl("hfAddRoleRelationMenu");
                HiddenField hfAddPermission = (HiddenField)fvAddRole.FindControl("hfAddPermission");
                DropDownList ddlMenuNameTH1 = (DropDownList)fvAddRole.FindControl("ddlMenuNameTH");
                CheckBoxList rdopermissiontype1 = (CheckBoxList)fvAddRole.FindControl("rdopermissiontype");
                Label rolesysidx = (Label)fvAddRole.FindControl("rolesysidx");
                Label rolemenuidx = (Label)fvAddRole.FindControl("rolemenuidx");

                CheckBox CheckBoxList1 = (CheckBox)fvAddRole.FindControl("CheckBoxList1");
                Label namepermission = (Label)fvAddRole.FindControl("namepermission");
                Label idxpermission = (Label)fvAddRole.FindControl("idxpermission");


                rolesysidx.Text = sysidx_menu;
                rolemenuidx.Text = menuidx_per;


                if (hfAddRoleIdx.Value == "0")
                {
                    ddlMenuNameTH1.Items.Clear();
                    ddlMenuNameTH1.AppendDataBoundItems = true;
                    ddlMenuNameTH1.Items.Add(new ListItem("Select Menu...", "00"));


                    data_menu _dataMenu5 = new data_menu();
                    _dataMenu5.m0_menu_list = new m0_menu_detail[1];
                    m0_menu_detail _menuDetailsys = new m0_menu_detail();
                    _menuDetailsys.system_idx = int.Parse(sysidx_menu);
                    _menuDetailsys.menu_idx = _menu_idx;
                    _dataMenu5.m0_menu_list[0] = _menuDetailsys;

                    _dataMenu5 = callServiceMenu(_urlGetMenu, _dataMenu5);

                    ddlMenuNameTH1.DataSource = _dataMenu5.m0_menu_list;
                    ddlMenuNameTH1.DataTextField = "menu_name_thlist";
                    ddlMenuNameTH1.DataValueField = "menu_idx";
                    ddlMenuNameTH1.DataBind();
                }
                else
                {
                    ddlMenuNameTH1.SelectedValue = menuidx_per;

                    ddlMenuNameTH1.Items.Clear();
                    ddlMenuNameTH1.AppendDataBoundItems = true;
                    ddlMenuNameTH1.Items.Add(new ListItem("Select Menu...", "00"));

                    data_menu _dataMenu5 = new data_menu();
                    _dataMenu5.m0_menu_list = new m0_menu_detail[1];
                    m0_menu_detail _menuDetailsys = new m0_menu_detail();
                    _menuDetailsys.system_idx = int.Parse(sysidx_menu);
                    _menuDetailsys.menu_idx = _menu_idx;
                    _dataMenu5.m0_menu_list[0] = _menuDetailsys;

                    _dataMenu5 = callServiceMenu(_urlGetMenu, _dataMenu5);

                    ddlMenuNameTH1.DataSource = _dataMenu5.m0_menu_list;
                    ddlMenuNameTH1.DataTextField = "menu_name_thlist";
                    ddlMenuNameTH1.DataValueField = "menu_idx";
                    ddlMenuNameTH1.DataBind();


                    data_menu _dataMenu9 = new data_menu();

                    _dataMenu9.m0_role_list = new m0_role_detail[1];
                    m0_role_detail _roleDetailsys = new m0_role_detail();

                    _roleDetailsys.role_idx = int.Parse(role_per);
                    _roleDetailsys.system_idx = int.Parse(rolesysidx.Text);
                    _roleDetailsys.menu_idx = int.Parse(menuidx_per);

                    _dataMenu9.m0_role_list[0] = _roleDetailsys;

                    _dataMenu9 = callServiceMenu(_urlGetEditRole, _dataMenu9);
                   // var rtu0idx = _dataMenu9.return_code;

                    rdopermissiontype1.AppendDataBoundItems = true;
                    rdopermissiontype1.DataSource = _dataMenu9.m0_role_list;
                    rdopermissiontype1.DataTextField = "permission_name";
                    rdopermissiontype1.DataValueField = "permission_idx";
                    rdopermissiontype1.DataBind();



                    data_menu _dataMenu10 = new data_menu();

                    _dataMenu10.m0_role_list = new m0_role_detail[1];
                    m0_role_detail _roleDetailsyscheck = new m0_role_detail();

                    _roleDetailsyscheck.role_idx = int.Parse(role_per);
                    _roleDetailsyscheck.system_idx = int.Parse(rolesysidx.Text);
                    _roleDetailsyscheck.menu_idx =int.Parse(menuidx_per);

                    _dataMenu10.m0_role_list[0] = _roleDetailsyscheck;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataMenu10));

                    _dataMenu10 = callServiceMenu(_urlGetEditRoleCheckBox, _dataMenu10);
                    ViewState["returncodecheck"] = _dataMenu10.return_msg;

                    var i = 0;

                    //string[] arrays = { "1", "2", "3" };
                    string check = ViewState["returncodecheck"].ToString();
                    string[] arrays1 = check.Split(new string[] { "," }, StringSplitOptions.None);

                    foreach (ListItem item in rdopermissiontype1.Items)
                    {
                        if (Array.IndexOf(arrays1, rdopermissiontype1.Items[i].Value) > -1)
                        {
                            rdopermissiontype1.Items[i].Selected = true;
                        }
                        i++;
                    }

                    
                    //if(rtu0idx == 0)
                    //{
                    //    foreach (ListItem rdopermissiontypes in rdopermissiontype1.Items)
                    //    {

                    //        rdopermissiontypes.Selected = true;

                    //    }


                    //}
                    //else
                    //{
                    //    foreach (ListItem rdopermissiontypes in rdopermissiontype1.Items)
                    //    {

                    //        rdopermissiontypes.Selected = false;

                    //    }

                    //}


                    ////data_menu _dataMenu9 = new data_menu();

                    ////_dataMenu9.m0_permission_list = new m0_permission_detail[1];
                    ////m0_permission_detail _menuDetailsys1 = new m0_permission_detail();

                    ////_menuDetailsys.system_idx = int.Parse(rolesysidx.Text);
                    ////_menuDetailsys.menu_idx = int.Parse(menuidx_per);

                    ////_dataMenu9.m0_permission_list[0] = _menuDetailsys1;
                    ////_dataMenu9 = callServiceMenu(_urlGetEditRole, _dataMenu9);
                    ////rdopermissiontype1.AppendDataBoundItems = true;
                    ////rdopermissiontype1.DataSource = _dataMenu9.m0_permission_list;
                    ////rdopermissiontype1.DataTextField = "permission_name";
                    ////rdopermissiontype1.DataValueField = "permission_idx";
                    ////rdopermissiontype1.DataBind();

                }

                break;
        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    protected void setDropDownList(DropDownList ddlName, Object obj)
    {
        ddlName.Items.Clear();

        switch(ddlName.ID)
        {
            case "ddlMenuRelation":
                ddlName.DataTextField = "menu_name_thlist";
                ddlName.DataValueField = "menu_idx";

                _defaultDdlText = "--- Root menu ---";
                _defaultDdlValue = "0";
                break;
           
           
        }
        ddlName.DataSource = obj;
        ddlName.DataBind();

        // Then add first item
        ddlName.Items.Insert(0, new ListItem(_defaultDdlText, _defaultDdlValue));
        // ddlName.SelectedValue = _defaultDdlValue;
        //********//
        HiddenField hfMenuRelation = (HiddenField)fvMenu.FindControl("hfMenuRelation");
        ddlName.SelectedValue = hfMenuRelation.Value;
        //********//
    }
    #endregion bind data

    #region DropdownList Change
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        switch (ddName.ID)
        {
            case "ddlMenuNameTH":

                DropDownList ddlMenuNameTH = (DropDownList)fvAddRole.FindControl("ddlMenuNameTH");
                DropDownList ddlPermission = (DropDownList)fvAddRole.FindControl("ddlPermission");
                CheckBoxList rdopermissiontype = (CheckBoxList)fvAddRole.FindControl("rdopermissiontype");

                Label rolesysidx = (Label)fvAddRole.FindControl("rolesysidx");
                //Label rolemenuidx = (Label)fvPermission.FindControl("rolemenuidx");
                //rolesysidx.Text = sysidx_menu;

                if (ddlMenuNameTH.SelectedValue == "00")
                {
                    ddlMenuNameTH.Items.Clear();
                    ddlMenuNameTH.AppendDataBoundItems = true;
                    ddlMenuNameTH.Items.Add(new ListItem("Select Menu....", "00"));

                    _dataMenu.m0_menu_list = new m0_menu_detail[1];
                    m0_menu_detail _menuDetailsys = new m0_menu_detail();
                    _menuDetailsys.system_idx = int.Parse(rolesysidx.Text);
                    _menuDetailsys.menu_idx = _menu_idx;
                    _dataMenu.m0_menu_list[0] = _menuDetailsys;

                    _dataMenu = callServiceMenu(_urlGetMenu, _dataMenu);

                    ddlMenuNameTH.DataSource = _dataMenu.m0_menu_list;
                    ddlMenuNameTH.DataTextField = "menu_name_thlist";
                    ddlMenuNameTH.DataValueField = "menu_idx";
                    ddlMenuNameTH.DataBind();
                    ddlMenuNameTH.SelectedValue = "00";

                    //ddlPermission.Items.Clear();
                    //ddlPermission.Items.Insert(0, new ListItem("Select Permission....", "00"));
                    //ddlPermission.SelectedValue = "00";

                    rdopermissiontype.Items.Clear();

                }
                else
                {
                  
                    //ddlPermission.AppendDataBoundItems = true;
                    //ddlPermission.Items.Clear();
                    //ddlPermission.Items.Add(new ListItem("Select Permission....", "00"));

                    rdopermissiontype.Items.Clear();

                    _dataMenu.m0_permission_list = new m0_permission_detail[1];
                    m0_permission_detail _menuDetailsys = new m0_permission_detail();

                    _menuDetailsys.system_idx = int.Parse(rolesysidx.Text);
                    _menuDetailsys.menu_idx = int.Parse(ddlMenuNameTH.SelectedValue);

                    _dataMenu.m0_permission_list[0] = _menuDetailsys;
                    _dataMenu = callServiceMenu(_urlGetPermissionMenu, _dataMenu);

                    //ddlPermission.DataSource = _dataMenu.m0_permission_list;
                    //ddlPermission.DataTextField = "permission_name";
                    //ddlPermission.DataValueField = "permission_idx";
                    //ddlPermission.DataBind();

                    rdopermissiontype.DataSource = _dataMenu.m0_permission_list;
                    rdopermissiontype.DataTextField = "permission_name";
                    rdopermissiontype.DataValueField = "permission_idx";
                    rdopermissiontype.DataBind();

                }

                break;
        }



    }

    #endregion DropdownList Change

    #region Gvbuyequipment_Editing
    protected void Gvbuyequipment_Editing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvpermissionsystem":

                GridView gvpermissionsystem = (GridView)fvAddRole.FindControl("gvpermissionsystem");

                gvpermissionsystem.EditIndex = e.NewEditIndex;
                gvpermissionsystem.DataSource = ViewState["vsBuyequipment"];
                gvpermissionsystem.DataBind();

                break;


        }
    }


    #endregion Gvbuyequipment_Editing

    #region Gvbuyequipment_RowDeleting

    protected void Gvbuyequipment_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvpermissionsystem":

                GridView gvpermissionsystem = (GridView)fvAddRole.FindControl("gvpermissionsystem");
                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                gvpermissionsystem.EditIndex = -1;
                gvpermissionsystem.DataSource = ViewState["vsBuyequipment"];
                gvpermissionsystem.DataBind();

                break;
        }
    }

    #endregion Gvbuyequipment_RowDeleting

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");
        

    }

    protected void initPageSystem()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        setFormData(fvSystem, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if(_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_menu callServiceMenu(string _cmdUrl, data_menu _dataMenu)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataMenu);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _dataMenu = (data_menu)_funcTool.convertJsonToObject(typeof(data_menu), _localJson);

        return _dataMenu;
    }

    protected data_system callServiceMenuSystem(string _cmdUrl, data_system _data_system)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_system);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_system = (data_system)_funcTool.convertJsonToObject(typeof(data_system), _localJson);

        return _data_system;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }


    #endregion reuse
}