﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_m0_evaluation_form : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_evaluation_form = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_evaluation_form"];
    static string _urlSetInsel_m0_evaluation_form = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_evaluation_form"];
    static string _urlSetUpdel_m0_evaluation_form = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_evaluation_form"];
    static string _urlDelel_m0_evaluation_form = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_evaluation_form"];
    static string _urlGetErrorel_m0_evaluation_form = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_evaluation_form"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_evaluation_form_action = new m0_evaluation_form[1];

        m0_evaluation_form obj = new m0_evaluation_form();

        obj.m0_evaluation_f_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;
        if(ddlm0_evaluation_group_idx_refsel.SelectedValue != "0")
        {
            obj.m0_evaluation_group_idx_ref = int.Parse(ddlm0_evaluation_group_idx_refsel.SelectedValue);
        }

        data_elearning.el_m0_evaluation_form_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //litDebug.Text = _func_dmu.zJson(_urlGetel_m0_evaluation_form, data_elearning);

        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_evaluation_form, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_evaluation_form_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_evaluation_f_idx;
        string _txtevaluation_f_name_en, _txtm0_evaluation_group_idx_ref , 
            _txtevaluation_f_item, _txtevaluation_f_remark, 
            _txtevaluation_f_name_th;
        int _cemp_idx;

        m0_evaluation_form objM0_ProductType = new m0_evaluation_form();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                DropDownList _ddm0_evaluation_group_ins = (DropDownList)ViewInsert.FindControl("ddlm0_evaluation_group_idx_ref");
                _func_dmu.zDropDownList(_ddm0_evaluation_group_ins,
                                 "",
                                 "zId",
                                 "zName",
                                 "0",
                                 "trainingLoolup",
                                 "",
                                 "EVALUA-GRP"

                                 );

                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtevaluation_f_item = ((TextBox)ViewInsert.FindControl("txtevaluation_f_item")).Text.Trim();
                _txtm0_evaluation_group_idx_ref = ((DropDownList)ViewInsert.FindControl("ddlm0_evaluation_group_idx_ref")).SelectedValue;
                _txtevaluation_f_name_en = ((TextBox)ViewInsert.FindControl("txtevaluation_f_name_en")).Text.Trim();
                _txtevaluation_f_name_th = ((TextBox)ViewInsert.FindControl("txtevaluation_f_name_th")).Text.Trim();
                _txtevaluation_f_remark = ((TextBox)ViewInsert.FindControl("txtevaluation_f_remark")).Text.Trim();
                DropDownList _ddlevaluation_f_status = (DropDownList)ViewInsert.FindControl("ddlevaluation_f_status");
                DropDownList _ddlevaluation_f_score = (DropDownList)ViewInsert.FindControl("ddlevaluation_f_score");
                _cemp_idx = emp_idx;
                int icode = 0;
                if (_txtevaluation_f_item != "")
                {   
                    icode = int.Parse(_txtevaluation_f_item);
                }
                //if (icode == 0)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ระดับต้องมากกว่าศูนย์');", true);
                //    return;
                //}
                //if(checkCodeError(0, _txtm0_evaluation_group_idx_ref, _txtevaluation_f_item) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ลำดับในกลุ่มหัวข้อการประเมินนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}
                m0_evaluation_form obj = new m0_evaluation_form();
                _data_elearning.el_m0_evaluation_form_action = new m0_evaluation_form[1];
                obj.m0_evaluation_f_idx = 0;//_type_idx; 
                obj.m0_evaluation_group_idx_ref = int.Parse(_txtm0_evaluation_group_idx_ref);
                _txtevaluation_f_item = "0";
                obj.evaluation_f_item = int.Parse(_txtevaluation_f_item);
                obj.evaluation_f_name_en = _txtevaluation_f_name_en;
                obj.evaluation_f_name_th = _txtevaluation_f_name_th;
                obj.evaluation_f_remark = _txtevaluation_f_remark;
                obj.evaluation_f_score = int.Parse(_ddlevaluation_f_score.SelectedValue);
                obj.evaluation_f_status = int.Parse(_ddlevaluation_f_status.SelectedValue);
                obj.evaluation_f_created_by = _cemp_idx;

                _data_elearning.el_m0_evaluation_form_action[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_evaluation_form, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_evaluation_f_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_m0_evaluation_form_action = new m0_evaluation_form[1];
                objM0_ProductType.m0_evaluation_f_idx = _m0_evaluation_f_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_m0_evaluation_form_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_m0_evaluation_form, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        DropDownList ddlm0_evaluation_group_idx_ref;
        TextBox txtm0_evaluation_group_idx_ref;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                   
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        txtm0_evaluation_group_idx_ref = (TextBox)e.Row.FindControl("hdtxtm0_evaluation_group_idx_ref");
                        ddlm0_evaluation_group_idx_ref = (DropDownList)e.Row.FindControl("ddlm0_evaluation_group_idx_refUpdate");
                        if(txtm0_evaluation_group_idx_ref.Text== null)
                        {
                            txtm0_evaluation_group_idx_ref.Text = "0";
                        }

                        _func_dmu.zDropDownList(ddlm0_evaluation_group_idx_ref,
                                    "",
                                    "zId",
                                    "zName",
                                    "0",
                                    "trainingLoolup",
                                    txtm0_evaluation_group_idx_ref.Text,
                                    "EVALUA-GRP"

                                    );
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    public string getValueTitle(int id = 0)
    {
    
        if (id == 1)
        {
            return "<span style='color:#26A65B;font-weight:700;'>คิดคะแนน</span>";
        }
        else
        {
            return "<span style='color:#F22613;font-weight:700;'>ไม่คิดคะแนน</span>";
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_evaluation_f_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtevaluation_f_item = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtevaluation_f_itemUpdate");
                var txtm0_evaluation_group_idx_ref = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlm0_evaluation_group_idx_refUpdate");
                var txtevaluation_f_name_en = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtevaluation_f_name_enUpdate");
                var txtevaluation_f_name_th = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtevaluation_f_name_thUpdate");
                var txtevaluation_f_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtevaluation_f_remarkUpdate");
                var ddlevaluation_f_score = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlevaluation_f_scoreUpdate");
                var ddlevaluation_f_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlevaluation_f_statusUpdate");
                int icode = 0;
                if (txtevaluation_f_item.Text != "")
                {
                    icode = int.Parse(txtevaluation_f_item.Text);
                }
                //if (icode == 0)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ระดับต้องมากกว่าศูนย์');", true);
                //    return;
                //}
                //if (checkCodeError(m0_evaluation_f_idx_update, txtm0_evaluation_group_idx_ref.SelectedValue, txtevaluation_f_item.Text.Trim()) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ลำดับในกลุ่มหัวข้อการประเมินนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}

                GvMaster.EditIndex = -1;

                _data_elearning.el_m0_evaluation_form_action = new m0_evaluation_form[1];
                m0_evaluation_form obj = new m0_evaluation_form();

                obj.m0_evaluation_f_idx = m0_evaluation_f_idx_update;
                obj.m0_evaluation_group_idx_ref = int.Parse(txtm0_evaluation_group_idx_ref.SelectedValue);
                txtevaluation_f_item.Text = "0";
                obj.evaluation_f_item = int.Parse(txtevaluation_f_item.Text);
                obj.evaluation_f_name_en = txtevaluation_f_name_en.Text;
                obj.evaluation_f_name_th = txtevaluation_f_name_th.Text;
                obj.evaluation_f_remark = txtevaluation_f_remark.Text;
                obj.evaluation_f_score = int.Parse(ddlevaluation_f_score.SelectedValue);
                obj.evaluation_f_status = int.Parse(ddlevaluation_f_status.SelectedValue);
                obj.evaluation_f_updated_by = emp_idx;

                _data_elearning.el_m0_evaluation_form_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_evaluation_form, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        _func_dmu.zDropDownList(ddlm0_evaluation_group_idx_refsel,
                                 "",
                                 "zId",
                                 "zName",
                                 "0",
                                 "trainingLoolup",
                                 "",
                                 "EVALUA-GRP"

                                 );

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_evaluation_f_idx, string _m0_evaluation_group_idx_ref, string _evaluation_f_item)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_evaluation_form_action = new m0_evaluation_form[1];

        m0_evaluation_form obj = new m0_evaluation_form();

        obj.m0_evaluation_f_idx = _m0_evaluation_f_idx;
        obj.m0_evaluation_group_idx_ref = int.Parse(_m0_evaluation_group_idx_ref);
        obj.evaluation_f_item = int.Parse(_evaluation_f_item);
        data_elearning.el_m0_evaluation_form_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_m0_evaluation_form, data_elearning);
        if(data_elearning.el_m0_evaluation_form_action != null)
        {
            //obj = data_elearning.el_m0_evaluation_form_action[0];
            //if (obj.m0_evaluation_group_idx_ref.ToString().Trim()== _m0_evaluation_group_idx_ref.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

}