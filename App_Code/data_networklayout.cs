﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_networklayout")]
public class data_networklayout
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("BoxnwlList")]
    public NWLList[] BoxnwlList { get; set; }

    [XmlElement("Export_BoxNWL")]
    public ExportNWLList[] Export_BoxNWL { get; set; }


}
[Serializable]
public class NWLList
{
    [XmlElement("REIDX")]
    public int REIDX { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("Search")]
    public string Search { get; set; }


    [XmlElement("u0idx")]
    public int u0idx { get; set; }

    [XmlElement("countdown_npw")]
    public int countdown_npw { get; set; }

    [XmlElement("countdown_idc")]
    public int countdown_idc { get; set; }

    [XmlElement("countdown_rjn")]
    public int countdown_rjn { get; set; }

    [XmlElement("countdown_mtt")]
    public int countdown_mtt { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("no_regis")]
    public string no_regis { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("DateDown")]
    public string DateDown { get; set; }

    [XmlElement("down_desciption")]
    public string down_desciption { get; set; }

    [XmlElement("TimeDown")]
    public string TimeDown { get; set; }

    [XmlElement("row_")]
    public int row_ { get; set; }

    [XmlElement("cell_")]
    public int cell_ { get; set; }

    [XmlElement("status_active")]
    public int status_active { get; set; }


    [XmlElement("img_name")]
    public string img_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("name_npw")]
    public string name_npw { get; set; }

    [XmlElement("name_idc")]
    public string name_idc { get; set; }

    [XmlElement("name_rjn")]
    public string name_rjn { get; set; }

    [XmlElement("name_mtt")]
    public string name_mtt { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("FLIDX")]
    public int FLIDX { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("BUIDX")]
    public int BUIDX { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }


    [XmlElement("countu0idx")]
    public int countu0idx { get; set; }   

    [XmlElement("Floor_name")]
    public string Floor_name { get; set; }

    [XmlElement("Locname")]
    public string Locname { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("create_log")]
    public string create_log { get; set; }

    [XmlElement("end_log")]
    public string end_log { get; set; }
}

[Serializable]
public class ExportNWLList
{
    [XmlElement("รหัสทะเบียนอุปกรณ์")]
    public string รหัสทะเบียนอุปกรณ์ { get; set; }

    [XmlElement("สถานที่")]
    public string สถานที่ { get; set; }

    [XmlElement("ชนิดอุปกรณ์")]
    public string ชนิดอุปกรณ์ { get; set; }

    [XmlElement("วันที่Down")]
    public string วันที่Down { get; set; }

    [XmlElement("เวลาที่Down")]
    public string เวลาที่Down { get; set; }

    [XmlElement("IPAddress")]
    public string IPAddress { get; set; }

    [XmlElement("ชื่ออุปกรณ์")]
    public string ชื่ออุปกรณ์ { get; set; }

    [XmlElement("SerialNumber")]
    public string SerialNumber { get; set; }

}
