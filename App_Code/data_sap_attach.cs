using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_sap_attach")]
public class data_sap_attach
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("sap_attach_list_u0")]
    public sap_attach_detail_u0[] sap_attach_list_u0 { get; set; }
    [XmlElement("search_sap_attach_list")]
    public search_sap_attach_detail[] search_sap_attach_list { get; set; }
}

[Serializable]
public class sap_attach_detail_u0
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("pr_no")]
    public string pr_no { get; set; }
    [XmlElement("file_name")]
    public string file_name { get; set; }
    [XmlElement("file_comment")]
    public string file_comment { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_sap_attach_detail
{
    [XmlElement("s_u0_idx")]
    public string s_u0_idx { get; set; }

    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_pr_no")]
    public string s_pr_nopr_no { get; set; }
    [XmlElement("s_file_name")]
    public string s_file_name { get; set; }
}