﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_cal_type : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetCal_type"];
    static string _urlCimsGetCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetCal_type"];
    static string _urlCimsDeleteCal_type = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteCal_type"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Cal_type();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Cal_type();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddCal_type":

                Gv_select_cal_type.EditIndex = -1;
                Select_Cal_type();
                btn_addcal_type.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_cal_type":
                Insert_Cal_type();
                Select_Cal_type();
                btn_addcal_type.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_cal_type":
                btn_addcal_type.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_cal_type":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Cal_type_de = new data_qa_cims();
                qa_cims_m0_cal_type_detail Cal_type_sde = new qa_cims_m0_cal_type_detail();

                Cal_type_de.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];

                Cal_type_sde.cal_type_idx = int.Parse(a.ToString());

                Cal_type_de.qa_cims_m0_cal_type_list[0] = Cal_type_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Cal_type_de = callServicePostMasterQACIMS(_urlCimsDeleteCal_type, Cal_type_de);

                Select_Cal_type();
                //Gv_select_unit.Visible = false;
                btn_addcal_type.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Cal_type()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_cal_type_name = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtcal_type_name");
        DropDownList dropD_status_cal_type = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddCal_type");

        data_qa_cims Cal_type_b = new data_qa_cims();
        qa_cims_m0_cal_type_detail Cal_type_s = new qa_cims_m0_cal_type_detail();
        Cal_type_b.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
        Cal_type_s.cal_type_name = tex_cal_type_name.Text;
        Cal_type_s.cemp_idx = _emp_idx;
        Cal_type_s.cal_type_status = int.Parse(dropD_status_cal_type.SelectedValue);
        Cal_type_b.qa_cims_m0_cal_type_list[0] = Cal_type_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Cal_type_b = callServicePostMasterQACIMS(_urlCimsSetCal_type, Cal_type_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Cal_type_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_cal_type_name.Text = String.Empty;
        }
    }

    protected void Select_Cal_type()
    {
        data_qa_cims Cal_type_b = new data_qa_cims();
        qa_cims_m0_cal_type_detail Cal_type_s = new qa_cims_m0_cal_type_detail();

        Cal_type_b.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
        Cal_type_b.qa_cims_m0_cal_type_list[0] = Cal_type_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Cal_type_b = callServicePostMasterQACIMS(_urlCimsGetCal_type, Cal_type_b);
        setGridData(Gv_select_cal_type, Cal_type_b.qa_cims_m0_cal_type_list);
        Gv_select_cal_type.DataSource = Cal_type_b.qa_cims_m0_cal_type_list;
        Gv_select_cal_type.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_cal_type":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpcal_type_status = (Label)e.Row.Cells[3].FindControl("lbpcal_type_status");
                    Label cal_type_statusOnline = (Label)e.Row.Cells[3].FindControl("cal_type_statusOnline");
                    Label cal_type_statusOffline = (Label)e.Row.Cells[3].FindControl("cal_type_statusOffline");

                    ViewState["_cal_type_status"] = lbpcal_type_status.Text;


                    if (ViewState["_cal_type_status"].ToString() == "1")
                    {
                        cal_type_statusOnline.Visible = true;
                    }
                    else if (ViewState["_cal_type_status"].ToString() == "0")
                    {
                        cal_type_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addcal_type.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_cal_type":
                Gv_select_cal_type.EditIndex = e.NewEditIndex;
                Select_Cal_type();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_cal_type":

                var cal_type_idx = (TextBox)Gv_select_cal_type.Rows[e.RowIndex].FindControl("ID_cal_type");
                var cal_type_name = (TextBox)Gv_select_cal_type.Rows[e.RowIndex].FindControl("Name_cal_type");
                var cal_type_status = (DropDownList)Gv_select_cal_type.Rows[e.RowIndex].FindControl("ddEdit_cal_type");


                Gv_select_cal_type.EditIndex = -1;

                data_qa_cims cal_type_Update = new data_qa_cims();
                cal_type_Update.qa_cims_m0_cal_type_list = new qa_cims_m0_cal_type_detail[1];
                qa_cims_m0_cal_type_detail cal_type_s = new qa_cims_m0_cal_type_detail();
                cal_type_s.cal_type_idx = int.Parse(cal_type_idx.Text);
                cal_type_s.cal_type_name = cal_type_name.Text;
                cal_type_s.cal_type_status = int.Parse(cal_type_status.SelectedValue);

                cal_type_Update.qa_cims_m0_cal_type_list[0] = cal_type_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                cal_type_Update = callServicePostMasterQACIMS(_urlCimsSetCal_type, cal_type_Update);

                Select_Cal_type();

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_cal_type":
                Gv_select_cal_type.EditIndex = -1;
                Select_Cal_type();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addcal_type.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_cal_type":
                Gv_select_cal_type.PageIndex = e.NewPageIndex;
                Gv_select_cal_type.DataBind();
                Select_Cal_type();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}