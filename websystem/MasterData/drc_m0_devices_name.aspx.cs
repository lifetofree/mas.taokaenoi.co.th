﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_drc_m0_devices_name : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_drc _data_drc = new data_drc();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    //master
    static string _urlSetM0DevicesDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0DevicesDRC"];
    static string _urlGetM0DevicesDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0DevicesDRC"];
    static string _urlSetUpdateM0DevicesDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateM0DevicesDRC"];
    static string _urlSetDelM0DevicesDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelM0DevicesDRC"];

    //master



    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        ////getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectMaster();
            ViewState["empIDX"] = Session["emp_idx"];
            ////Select_Equipment_result();


        }
    }
    #endregion Page Load

    protected void SelectMaster()
    {
        data_drc _data_drc = new data_drc();
        TypeTopicDetail _m0_devices_name = new TypeTopicDetail();

        _data_drc.BoxTypeTopicList = new TypeTopicDetail[1];

        _data_drc.BoxTypeTopicList[0] = _m0_devices_name;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        _data_drc = callServicePostDRC(_urlGetM0DevicesDRC, _data_drc);

        setGridData(GvMaster, _data_drc.BoxTypeTopicList);

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdInsert":

                MvMaster.SetActiveView(ViewInsert);

                break;

            case "cmdSaveTypetopic":
                //litdebug.Text = _emp_idx.ToString();
                //TextBox txtTypetopic = (TextBox)ViewInsert.FindControl("txtTypetopic");
                //DropDownList ddl_Typetopic_status = (DropDownList)ViewInsert.FindControl("ddl_Typetopic_status");

                data_drc _data_drc = new data_drc();
                TypeTopicDetail _m0_devices_name = new TypeTopicDetail();

                _data_drc.BoxTypeTopicList = new TypeTopicDetail[1];

                _m0_devices_name.Typename = txtTypetopic.Text;
                _m0_devices_name.Type_status = int.Parse(ddl_Typetopic_status.SelectedValue);
                _m0_devices_name.CEmpIDX = _emp_idx;

                _data_drc.BoxTypeTopicList[0] = _m0_devices_name;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetM0DevicesDRC, _data_drc);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resulution));
                if (_data_drc.return_code == 0)
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    

                }
                
                break;

            case "cmdDelete":

                int TYIDX_del = int.Parse(cmdArg);

                data_drc _data_drc_del = new data_drc();
                TypeTopicDetail _m0_devices_name_del = new TypeTopicDetail();

                _data_drc_del.BoxTypeTopicList = new TypeTopicDetail[1];

                _m0_devices_name_del.TYIDX = TYIDX_del;
                _m0_devices_name_del.CEmpIDX = _emp_idx;

                _data_drc_del.BoxTypeTopicList[0] = _m0_devices_name_del;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetDelM0DevicesDRC, _data_drc_del);


                SelectMaster();
               

                break;

            case "cmdCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion bind data

    #region Gridview
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lbl_Type_status = (Label)e.Row.FindControl("lbl_Type_status");
                    Label lbl_Type_statussOnline = (Label)e.Row.FindControl("lbl_Type_statussOnline");
                    Label lbl_Type_statusOffline = (Label)e.Row.FindControl("lbl_Type_statusOffline");

                    ViewState["vs_Type_status"] = lbl_Type_status.Text;


                    if (ViewState["vs_Type_status"].ToString() == "1")
                    {
                        lbl_Type_statussOnline.Visible = true;
                    }
                    else if (ViewState["vs_Type_status"].ToString() == "0")
                    {
                        lbl_Type_statusOffline.Visible = true;
                    }
                    else
                    {

                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                SelectMaster();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                var txt_TYIDX_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_TYIDX_edit");
                var txt_Typename_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_Typename_edit");
                var ddl_Type_status_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_Type_status_edit");


                GvMaster.EditIndex = -1;

                data_drc _data_drc = new data_drc();
                TypeTopicDetail _m0_devices_name = new TypeTopicDetail();

                _data_drc.BoxTypeTopicList = new TypeTopicDetail[1];

                _m0_devices_name.Typename = txt_Typename_edit.Text;
                _m0_devices_name.Type_status = int.Parse(ddl_Type_status_edit.SelectedValue);
                _m0_devices_name.CEmpIDX = _emp_idx;
                _m0_devices_name.TYIDX = int.Parse(txt_TYIDX_edit.Text);

                _data_drc.BoxTypeTopicList[0] = _m0_devices_name;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                _data_drc = callServicePostDRC(_urlSetUpdateM0DevicesDRC, _data_drc);


                if (_data_drc.return_code == 0)
                {
                    SelectMaster();
                }
             
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectMaster();
                break;
        }
    }

    #endregion Gridview

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion dropdownlist

    #region callService 

    protected data_drc callServicePostDRC(string _cmdUrl, data_drc _data_drc)
    {
        _localJson = _funcTool.convertObjectToJson(_data_drc);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_drc = (data_drc)_funcTool.convertJsonToObject(typeof(data_drc), _localJson);


        return _data_drc;
    }

    #endregion callService
}