﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_recurit.master" AutoEventWireup="true" CodeFile="qmr_problem_supplier.aspx.cs" Inherits="websystem_qa_qmr_problem_supplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
    <title>TKN : ระบบแจ้งปัญหาวัตถุดิบ</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <style type="text/css">
        .bg-login {
            background-image: url('../../images/qmr-problem/bg_login.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 440pt;
            text-align: center;
        }
    </style>
    <style type="text/css">
        .bg-changelogin {
            background-image: url('../../images/qmr-problem/bg-changelogin.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 440pt;
            text-align: center;
        }
    </style>

    <style type="text/css">
        .bg-system {
            background-image: url('../../images/qmr-problem/bg1.png');
            background-size: 100% 100%;
            /*width: 50%;*/
            height: 100%;
            text-align: left;
        }
    </style>
    <style type="text/css">
        .bg-detail1 {
            background-image: url('../../images/qmr-problem/bg_detail1.png');
            background-size: 50% 40%;
            /* width: 50%;
            height: 100%;*/
        }
    </style>


    <style type="text/css">
        .bg-login-color {
            background: #ff9999;
            border-color: #ff9999;
        }
    </style>

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewList" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 ">
                        <div class="col-lg-1 ">
                        </div>
                        <div class="col-lg-10">
                            <div>
                                <asp:Image ID="Image4" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr.png" Style="height: 100%; width: 100%;" />
                            </div>
                        </div>
                        <div class="col-lg-1">
                        </div>
                    </div>
                    <br />
                    <br />


                    <div class="col-lg-12 bg-system">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-10">
                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-10">

                                    <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server" CssClass="btn btn-sm btn-primary"
                                        NavigateUrl="https://docs.google.com/document/d/12f_g4iwoxwkfXtsA65MCb1phe_uSMtD9kuyNoGKXHdw/edit?usp=sharing" Target="_blank"
                                        CommandName="_divMenuBtnToDivManual" OnCommand="btnCommand"><i class="fa fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>

                                </div>
                            </div>

                            <div class="panel panel-danger" style="border-width: 2pt">
                                <div class="panel-heading" style="background-color: #ff9999; color: black;">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาข้อมูล</strong>  </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">


                                        <div class="form-group">
                                            <asp:Label ID="Label55" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlSearchDate_List" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="SelectedIndexChanged">
                                                    <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                                    <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                    <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                    <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="col-sm-3 ">

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartdate_List" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" ValidationGroup="SearchList" runat="server" Display="None"
                                                        ControlToValidate="AddStartdate_List" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender55" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator37" Width="160" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddEndDate_List" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                            <p class="help-block"><font color="red">**</font></p>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label143" runat="server" Text="กลุ่มวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlgroupproduct_List" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกกลุ่มวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทวัตถุดิบ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypeproduct_List" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label144" runat="server" Text="โครงสร้างวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypeproduct_list_m1" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาโครงสร้างวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>


                                            </div>
                                            <asp:Label ID="Label145" runat="server" Text="โครงสร้างย่อยวัตถุดิบ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypeproduct_list_m2" ValidationGroup="Save" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <%-- <asp:Label ID="Label123" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทวัตถุดิบ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypeproduct_List" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทวัตถุดิบ....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>--%>

                                            <asp:Label ID="Label124" CssClass="col-sm-2 control-label" runat="server" Text="ปัญหาที่พบ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlproblem_List" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกปัญหาที่พบ....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" ValidationGroup="SearchList" runat="server" CommandName="btnsearch_list" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton17" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnrefresh_list" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div style="overflow-x: auto; width: 100%">
                                <asp:GridView ID="GvIndex" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-Height="40px"
                                    HeaderStyle-BackColor="#ff9999"
                                    BorderStyle="Solid"
                                    HeaderStyle-BorderWidth="2pt"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    DataKeyNames="u0_docidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-Width="5%" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lblu1_docidx" runat="server" Visible="false" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doc_code") %>'></asp:Label>
                                                    <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                                    <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ข้อมูลสถานที่" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>

                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labesl15" runat="server">สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="litLocIDX" Visible="false" runat="server" Text='<%# Eval("LocIDX") %>' />
                                                    <asp:Literal ID="LitLocName" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">อาคาร: </asp:Label></strong>
                                                    <asp:Literal ID="litBuildingIDX" Visible="false" runat="server" Text='<%# Eval("BuildingIDX") %>' />
                                                    <asp:Literal ID="LitBuildingName" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">ไลน์ผลิต: </asp:Label></strong>
                                                    <asp:Literal ID="litm0_plidx" Visible="false" runat="server" Text='<%# Eval("m0_plidx") %>' />
                                                    <asp:Literal ID="Litproduction_name" runat="server" Text='<%# Eval("production_name") %>' />
                                                    </p>
                                 
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeslr15" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labelsd24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labsel25" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                    </p>
                                                 
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                    <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("CEmpIDX") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลแจ้งปัญหา" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label5" runat="server">กลุ่มวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("group_name") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Labsel15" runat="server">ประเภทวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("typeproduct_name") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Label10" runat="server">โครงสร้างวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("typeproduct_name_m1") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="Label11" runat="server">โครงสร้างย่อยวัตถุดิบ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("typeproduct_name_m2") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labqel24" runat="server">ประเภทปัญหา : </asp:Label></strong>
                                                    <asp:Literal ID="litproblem_name" runat="server" Text='<%# Eval("problem_name") %>' />
                                                    </p>
                                  <strong>
                                      <asp:Label ID="Label29" runat="server">วันที่แจ้ง : </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("create_date") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ข้อมูลประเมินปัญหา" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="qLabel5" runat="server">PO Number: </asp:Label></strong>
                                                    <asp:Literal ID="litpo_no" runat="server" Text='<%# Eval("po_no") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="qLabsel15" runat="server">Invoice Number: </asp:Label></strong>
                                                    <asp:Literal ID="littv_no" runat="server" Text='<%# Eval("tv_no") %>' />
                                                    </p>
                                                <strong>
                                                    <asp:Label ID="qLabel10" runat="server">ระดับปัญหา: </asp:Label></strong>
                                                    <asp:Literal ID="littype_problem_name" runat="server" Text='<%# Eval("type_problem_name") %>' />
                                                    </p>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#ffe3dd" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                                    </b>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-BorderWidth="2pt" ItemStyle-BackColor="#fff0ec" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("doc_code")+ ";" + Eval("CEmpIDX") %>'><i class="	fa fa-book"></i></asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="col-lg-1">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-md-12" id="div2" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload2" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|pdf" />
            </div>

            <div class="col-lg-12">
                <div class="col-lg-12 ">
                    <div class="col-lg-1 ">
                    </div>
                    <div class="col-lg-10">
                        <div class="bg-system">
                            <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr.png" Style="height: 100%; width: 100%;" />
                        </div>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>
            </div>
            <br />
            <div class="col-lg-12 bg-detail1">

                <div class="col-lg-12 ">
                    <div class="col-lg-1 ">
                    </div>
                    <div class="col-lg-10">
                        <div class="panel panel-danger" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #ff9999; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>
                            <asp:FormView ID="Fvdetailusercreate" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body ">
                                        <div class="form-horizontal" role="form">

                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- แผนก,ตำแหน่ง --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtsecidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                            <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                            <div class="form-group">

                                                <%--  <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>--%>

                                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="E-mail : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="col-lg-12  ">
                    <div class="col-lg-1 ">
                    </div>
                    <div class="col-lg-10">


                        <div class="panel panel-danger" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #ff9999; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; รายละเอียดการแจ้งปัญหา</strong></h3>
                            </div>
                            <asp:FormView ID="FvdetailProblem" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <ItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                            <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                            <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />

                                            <div class="form-group">
                                                <asp:Label ID="Label31" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="รหัสรายการ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="Label33" class="control-labelnotop" Text='<%# Eval("doc_code") %>' runat="server"> </asp:Label>
                                                    <asp:Label ID="lblu1_docidx" Visible="false" class="control-labelnotop" Text='<%# Eval("u1_docidx") %>' runat="server"> </asp:Label>
                                                </div>
                                                <div class="col-md-2 col-sm-offset-5" id="div_btnedit" runat="server" visible="false">
                                                    <asp:LinkButton ID="btnedit_form" CssClass="btn btn-sm btn-warning" OnCommand="btnCommand" CommandName="CmdEdit_Problem" CommandArgument='<%# Eval("u1_docidx") %>' runat="server"><i class="glyphicon glyphicon-pencil"></i> แก้ไขรายการ</asp:LinkButton>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label30" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="สถานที่ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lbllocidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("LocIDX") %>' runat="server"> </asp:Label>
                                                    <asp:Label ID="Label32" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>
                                                </div>
                                                <asp:Label ID="Label22" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="อาคาร " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="txtdoc_code" class="control-labelnotop" Text='<%# Eval("BuildingName") %>' runat="server"> </asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label35" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="ไลน์ผลิต " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm0_plidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_plidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label36" class="control-labelnotop" Text='<%# Eval("production_name") %>' runat="server"> </asp:Label>
                                                </div>
                                                <%--           <asp:Label ID="Label37" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ประเภทวัตถุดิบ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm0_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_tpidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label38" class="control-labelnotop" Text='<%# Eval("typeproduct_name") %>' runat="server"> </asp:Label>
                                                </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label146" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="กลุ่มวัตถุดิบ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm0_m0_group_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_group_tpidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label148" class="control-labelnotop" Text='<%# Eval("group_name") %>' runat="server"> </asp:Label>
                                                </div>
                                                <asp:Label ID="Label1" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="ประเภทวัตถุดิบ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm0_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_tpidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label3" class="control-labelnotop" Text='<%# Eval("typeproduct_name") %>' runat="server"> </asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label149" CssClass="col-sm-2 control-labelnotop text_right" runat="server" Text="โครงสร้างวัตถุดิบ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm1_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m1_tpidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label151" class="control-labelnotop" Text='<%# Eval("typeproduct_name_m1") %>' runat="server"> </asp:Label>
                                                </div>
                                                <asp:Label ID="Label152" CssClass="col-sm-3 control-labelnotop text_right" runat="server" Text="โครงสร้างย่อยวัตถุดิบ " />
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblm2_tpidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m2_tpidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label154" class="control-labelnotop" Text='<%# Eval("typeproduct_name_m2") %>' runat="server"> </asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label16" runat="server" Text="Mat No" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="lblmatidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("matidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="lblmat_no" class="control-labelnotop" Text='<%# Eval("mat_no") %>' runat="server"> </asp:Label>
                                                </div>
                                                <asp:Label ID="Label17" runat="server" Text="Mat Name" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblmast_name_show" class="control-labelnotop" Text='<%# Eval("matname") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group" runat="server" visible="false">
                                                <asp:Label ID="Label15" runat="server" Text="Mat" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:CheckBox ID="chkmat_show" runat="server" />
                                                    <label for="chkmat" class="control-labelnotop" style="font-weight: normal">เลข Mat สินค้า</label>
                                                    <%-- <asp:Label ID="Label16" runat="server" Text="ไม่มีเลข Mat สินค้า" CssClass="col-sm-3 control-label text_right"></asp:Label>--%>
                                                </div>

                                                <div id="divmatshow_no" runat="server" visible="false">
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label18" runat="server" Text="Batch" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblbatchnumber" class="control-labelnotop" Text='<%# Eval("batchnumber") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <%-- <div class="form-group">
                                                <asp:Label ID="Label15" runat="server" Text="Mat" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:CheckBox ID="chkmat_show" runat="server" />
                                                    <label for="chkmat" class="control-labelnotop" style="font-weight: normal">เลข Mat สินค้า</label>
                                                    <%-- <asp:Label ID="Label16" runat="server" Text="ไม่มีเลข Mat สินค้า" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                </div>

                                                <div id="divmatshow_no" runat="server" visible="false">
                                                    <asp:Label ID="Label16" runat="server" Text="Mat No" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                    <div class="col-sm-3">

                                                        <asp:Label ID="lblmatidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("matidx") %>' runat="server"> </asp:Label>

                                                        <asp:Label ID="lblmat_no" class="control-labelnotop" Text='<%# Eval("mat_no") %>' runat="server"> </asp:Label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label17" runat="server" Text="Mat Name" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblmast_name_show" class="control-labelnotop" Text='<%# Eval("matname") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label18" runat="server" Text="Batch" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblbatchnumber" class="control-labelnotop" Text='<%# Eval("batchnumber") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="Supplier Name" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblm0_supidx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_supidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label44" class="control-labelnotop" Text='<%# Eval("sup_name") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label135" runat="server" Text="จำนวนครั้งแจ้งปัญหา" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblcount_sup" class="control-labelnotop" Text='<%# Eval("count_sup") %>' runat="server"> </asp:Label>
                                                    <asp:Label ID="Label137" class="control-labelnotop" Text="ครั้ง" runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <hr />
                                            <div class="form-group">
                                                <asp:Label ID="Label85" runat="server" Text="วันที่ผลิต" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label86" class="control-labelnotop" Text='<%# Eval("datemodify") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label87" runat="server" Text="วันที่หมดอายุ" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label88" class="control-labelnotop" Text='<%# Eval("dateexp") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label89" runat="server" Text="วันที่พบปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label90" class="control-labelnotop" Text='<%# Eval("dateproblem") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label20" runat="server" Text="วันที่รับเข้า" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbldategetproduct" class="control-labelnotop" Text='<%# Eval("dategetproduct") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label21" runat="server" Text="จำนวนที่รับเข้า" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="lblqty_get" class="control-labelnotop" Text='<%# Eval("qty_get") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label39" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblunit_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_get") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label47" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label23" runat="server" Text="จำนวนที่พบปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="lblqty_problem" class="control-labelnotop" Text='<%# Eval("qty_problem") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label24" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblunit_problem_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_problem") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label49" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label25" runat="server" Text="จำนวนสุ่ม" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblqty_random" class="control-labelnotop" Text='<%# Eval("qty_random") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label27" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="lblunit_random_show" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_random") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label51" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label28" runat="server" Text="ประเภทปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-9">
                                                    <asp:Label ID="lblm0_pridx_show" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_pridx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label52" class="control-labelnotop" Text='<%# Eval("problem_name") %>' runat="server"> </asp:Label>
                                                </div>

                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Labedle1" class="col-sm-2 control-labelnotop text_right" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                                <div class="col-sm-9">
                                                    <asp:Label ID="lbldetail_remark" class="control-labelnotop" Text='<%# Eval("detail_remark") %>' runat="server"> </asp:Label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label110" runat="server" Text=" " CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-lg-8">
                                                    <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="default"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        Font-Size="Small">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-10">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                    <h4></h4>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>

                        </div>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="col-lg-12  ">
                    <div class="col-lg-1 ">
                    </div>
                    <div class="col-lg-10">
                        <div class="panel panel-danger" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #ff9999; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-question-sign"></i><strong>&nbsp; ประเมินระดับปัญหา</strong></h3>
                            </div>
                            <asp:FormView ID="FvQA_Comment" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <ItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <div class="form-group">
                                                <asp:Label ID="Label96" runat="server" Text="PO Number" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lblu2_docidx" Visible="false" class="control-labelnotop" Text='<%# Eval("u2_docidx") %>' runat="server"> </asp:Label>
                                                    <asp:Label ID="Label97" class="control-labelnotop" Text='<%# Eval("po_no") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label98" runat="server" Text="Invoice Number" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label100" class="control-labelnotop" Text='<%# Eval("tv_no") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label46" runat="server" Text="ระดับปัญหา" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label48" class="control-labelnotop" Text='<%# Eval("type_problem_name") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label50" runat="server" Text="ประเภทการส่งคืน" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="Label53" Visible="false" class="control-labelnotop" Text='<%# Eval("m0_rbidx") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label58" class="control-labelnotop" Text='<%# Eval("rollback_name") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="จำนวนส่งคืน" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label60" class="control-labelnotop" Text='<%# Eval("qty_rollback") %>' runat="server"> </asp:Label>

                                                </div>
                                                <asp:Label ID="Label61" runat="server" Text="หน่วย" CssClass="col-sm-3 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:Label ID="Label62" Visible="false" class="control-labelnotop" Text='<%# Eval("unit_rollback") %>' runat="server"> </asp:Label>

                                                    <asp:Label ID="Label63" class="control-labelnotop" Text='<%# Eval("unit_name") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label64" runat="server" Text="สาเหตุส่งคืน" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label65" class="control-labelnotop word-wrap" Text='<%# Eval("detail_rollback") %>' runat="server"> </asp:Label>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label57" runat="server" Text="สถานที่ดำเนินการ" CssClass="col-sm-2 control-labelnotop text_right"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:Label ID="Label105" class="control-labelnotop" Text='<%# Eval("LocName") %>' runat="server"> </asp:Label>

                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <asp:Label ID="Label37" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-8">

                                                        <div class="form-group" id="divshow_gvmaterial_risk" runat="server" visible="false">

                                                            <div class="col-sm-8" style="overflow-x: auto; width: 100%">

                                                                <asp:GridView ID="Gvshow_Material_risk" runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                    HeaderStyle-CssClass="info small"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="false"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    DataKeyNames="u3_docidx">

                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="Material Risk" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbm0_mridx" runat="server" Visible="false" Text='<%# Eval("m0_mridx") %>'></asp:Label>
                                                                                <asp:Label ID="material_risk" runat="server" Text='<%# Eval("material_risk") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ความคิดเห็นเพิ่มเติม" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("comment_risk") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>

                                                                <%-- <asp:CheckBoxList ID="chkmat_risk_show" Enabled="false" runat="server" CssClass="radio-list-inline-emps" RepeatColumns="2" RepeatDirection="Vertical">
                                                </asp:CheckBoxList>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:Label ID="Label38" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>

                <div class="col-lg-12  ">
                    <div class="col-lg-1 ">
                    </div>
                    <div class="col-lg-10">
                        <div class="panel panel-danger" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #ff9999; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i><strong>&nbsp; รายละเอียดการแก้ไข (ดำเนินการโดย Supplier)</strong></h3>
                            </div>
                            <div class="panel-body">

                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div style="overflow-x: auto; width: 100%">
                                        <asp:GridView ID="GvComment"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            DataKeyNames="l0_docidx"
                                            HeaderStyle-BackColor="#F8AE74"
                                            BorderStyle="Solid"
                                            HeaderStyle-BorderWidth="2pt"
                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="false"
                                            PageSize="100">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />


                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>
                                                <asp:TemplateField HeaderText="#">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("l0_docidx") %>' />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="ผู้ดำเนินการ" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("actor_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Comment" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTypeCode" runat="server" CssClass="col-sm-10" Text='<%# Eval("comment") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Date/Time" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelddw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("createdate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label13" CssClass="col-sm-2 control-label text_right" runat="server" Text="วันที่ต้องดำเนินการให้เสร็จ" />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcount_typeproblem" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:Label ID="Label14" CssClass="col-sm-2 control-label text_right" runat="server" Text="วัน" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label12" CssClass="col-sm-2 control-label text_right" runat="server" Text="วันที่ได้รับแจ้งปัญหา" />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdateupdate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label33" CssClass="col-sm-2 control-label text_right" runat="server" Text="รับทราบปัญหา" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะรับทราบปัญหา...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="ddl_approve" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะรับทราบปัญหา"
                                                ValidationExpression="กรุณาเลือกสถานะรับทราบปัญหา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label54" runat="server" Text="รายละเอียดการแก้ไข" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark_approve" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียดการแก้ไข"
                                                ValidationExpression="กรุณากรอกรายละเอียดการแก้ไข"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorwCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                ValidationGroup="SaveApprove" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_approve"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloqutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator8" Width="160" />

                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Label1d0s" class="col-sm-2 control-label text_right" runat="server" Text="Upload File " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadFileSupplier" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" runat="server" CssClass=" multi" accept="jpg|pdf" />

                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldVadlidator18"
                                                        runat="server" ControlToValidate="UploadFileSupplier" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="กรุณาอัพโหลดไฟล์"
                                                        ValidationGroup="SaveApprove" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="VdalidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVadlidator18" Width="200" PopupPosition="BottomLeft" />--%>

                                                    <small>
                                                        <p class="help-block"><font color="red">**Attach File name jpg,  pdf</font></p>
                                                    </small>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnApprove" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="btnApprove" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1">
                    </div>
                </div>


            </div>
        </asp:View>
    </asp:MultiView>
    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });


    </script>

    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>

    <script>
        $(function () {
            $('.UploadFileSupplier').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileSupplier').MultiFile({

                });

            });
        });
    </script>
    <%--    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileSupplier").MultiFile();
            }
        })
    </script>--%>
</asp:Content>

