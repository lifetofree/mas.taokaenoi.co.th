﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;

public partial class websystem_qa_qa_labcal : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    // bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        // _emp_idx = int.Parse(Session["emp_idx"].ToString());
        // getEmployeeProfile(_emp_idx);

        // _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        // // check permission
        // foreach (int item in rdept_qmr)
        // {
        //     if(_dataEmployee.employee_list[0].rdept_idx == item)
        //     {
        //         _flag_qmr = true;
        //         break;
        //     }
        // }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            initPage();
        }

    }



    #region event command


    protected void txtchange(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;
        var searchingToolID = (TextBox)fvInsertLabCal.FindControl("searchingToolID");
        var paneldetailTool = (Panel)fvInsertLabCal.FindControl("paneldetailTool");
        var div_showdetail_machine_old = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_old");
        var divShowWarning = (Panel)fvInsertLabCal.FindControl("divShowWarning");
        var btnInsertDetail = (LinkButton)fvInsertLabCal.FindControl("btnInsertDetail");
        switch (txtName.ID)
        {
            case "searchingToolID":
                if (searchingToolID.Text == "has")
                {

                    paneldetailTool.Visible = true;
                    btnInsertDetail.Focus();
                    divShowWarning.Visible = false;
                    div_showdetail_machine_old.Visible = false;
                }
                else
                {
                    paneldetailTool.Visible = false;
                    btnInsertDetail.Focus();
                    divShowWarning.Visible = true;
                    div_showdetail_machine_old.Visible = false;
                }
                searchingToolID.Text = String.Empty;
                break;

        }

    }

    protected void CheckboxChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        var txtinput_machine_name = (TextBox)fvInsertLabCal.FindControl("txtinput_machine_name");
        var txtinput_machine_brand = (TextBox)fvInsertLabCal.FindControl("txtinput_machine_brand");
        var chk_other_name = (CheckBox)fvInsertLabCal.FindControl("chk_other_name");
        var chk_other_brand = (CheckBox)fvInsertLabCal.FindControl("chk_other_brand");
        var ddl_machine_name = (DropDownList)fvInsertLabCal.FindControl("ddl_machine_name");
        var ddl_machine_brand = (DropDownList)fvInsertLabCal.FindControl("ddl_machine_brand");
        var chktoolID = (CheckBox)fvInsertLabCal.FindControl("chktoolID");
        var paneldetailTool = (Panel)fvInsertLabCal.FindControl("paneldetailTool");

        switch (cb.ID)
        {
            case "chk_other_name":
                if (chk_other_name.Checked)
                {
                    txtinput_machine_name.Visible = true;
                    ddl_machine_name.Visible = false;
                    txtinput_machine_brand.Visible = true;
                    ddl_machine_brand.Visible = false;
                }
                else
                {
                    txtinput_machine_name.Visible = false;
                    ddl_machine_name.Visible = true;
                    txtinput_machine_brand.Visible = false;
                    ddl_machine_brand.Visible = true;
                }

                break;

            case "chk_other_brand":
                if (chk_other_brand.Checked)
                {
                    txtinput_machine_brand.Visible = true;
                    ddl_machine_brand.Visible = false;
                }
                else
                {
                    txtinput_machine_brand.Visible = false;
                    ddl_machine_brand.Visible = true;
                }
                break;

            case "chktoolID":
                if (chktoolID.Checked)
                {
                    paneldetailTool.Visible = true;

                }
                else
                {
                    paneldetailTool.Visible = false;
                }
                break;

        }
    }


    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0);
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        Panel div_showdetail_machine_new = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_new");
        Panel div_showdetail_machine_old = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_old");
        LinkButton btnsavemachinenew = (LinkButton)fvInsertLabCal.FindControl("btnsavemachinenew");




        switch (cmdName)
        {
            case "showsearch":
                TabSearch.Visible = true;
                hiddensearch.Visible = true;
                btnsearchshow.Visible = false;
                break;

            case "cmdView":
                int _viewAction = int.Parse(cmdArg);
                switch (_viewAction)
                {
                    case 0:
                        genaral_machinelist.Visible = false;
                        genaral_machine_userview.Visible = true;
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;

                    case 1:
                        genaral_machinelist.Visible = true;
                        genaral_machine_userview.Visible = false;
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;

                    case 2:
                        setActiveTab("tab4", 0, 1);
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;

                    case 3:
                        setActiveTab("tab5", 0, 3);
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;
                    case 4:
                        setActiveTab("tab5", 0, 4);
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;
                }
                break;

            case "btnInsert":

                string[] cmdArgSave = cmdArg.Split(',');

                int from_actor_idx = int.TryParse(cmdArgSave[0].ToString(), out _default_int) ? int.Parse(cmdArgSave[0].ToString()) : _default_int;
                int from_node_idx = int.TryParse(cmdArgSave[1].ToString(), out _default_int) ? int.Parse(cmdArgSave[1].ToString()) : _default_int;
                int decision = int.TryParse(cmdArgSave[2].ToString(), out _default_int) ? int.Parse(cmdArgSave[2].ToString()) : _default_int;
                int to_actor_idx = int.TryParse(cmdArgSave[3].ToString(), out _default_int) ? int.Parse(cmdArgSave[3].ToString()) : _default_int;
                int to_node_idx = int.TryParse(cmdArgSave[4].ToString(), out _default_int) ? int.Parse(cmdArgSave[4].ToString()) : _default_int;

                //switch action by node
                switch (from_node_idx)
                {
                    case 1:
                        setActiveTab("tab2", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;
                    case 2:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 3:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 4:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 5:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 6:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 7:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        switch (to_node_idx)
                        {
                            case 8:
                                setActiveTab("tab2", to_node_idx, decision);
                                SETFOCUS.Focus();
                                break;
                        }

                        break;

                    case 9:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                    case 10:
                        setActiveTab("tab1", to_node_idx, decision);
                        SETFOCUS.Focus();
                        break;

                }
                break;


            case "cmdDocCancel":
                int _cancelAction = int.Parse(cmdArg);
                switch (_cancelAction)
                {
                    case 0:
                        setActiveTab("tab2", 0, 0);
                        SETFOCUS.Focus();
                        break;
                    case 1:
                        setActiveTab("tab3", 0, 0);
                        SETFOCUS.Focus();
                        break;
                }

                break;
            case "btnInsertDetail":
                div_showdetail_machine_old.Visible = true;
                break;
            case "btnCancel":
                setActiveTab("tab1", 0, 0);
                break;
            case "btninsertmachine":
                div_showdetail_machine_new.Visible = true;
                break;


            case "cmdAction":

                string[] cmdArgAction = cmdArg.Split(',');

                int from_actor = int.TryParse(cmdArgAction[0].ToString(), out _default_int) ? int.Parse(cmdArgAction[0].ToString()) : _default_int;
                int from_node = int.TryParse(cmdArgAction[1].ToString(), out _default_int) ? int.Parse(cmdArgAction[1].ToString()) : _default_int;
                int _decision = int.TryParse(cmdArgAction[2].ToString(), out _default_int) ? int.Parse(cmdArgAction[2].ToString()) : _default_int;
                int to_actor = int.TryParse(cmdArgAction[3].ToString(), out _default_int) ? int.Parse(cmdArgAction[3].ToString()) : _default_int;
                int to_node = int.TryParse(cmdArgAction[4].ToString(), out _default_int) ? int.Parse(cmdArgAction[4].ToString()) : _default_int;

                switch (from_node)
                {
                    case 1:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 2:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 3:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 4:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 5:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 7:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 8:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 9:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                    case 10:
                        setActiveTab("tab3", to_node, _decision);
                        SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdSearch":

                int _searchAction = int.Parse(cmdArg);
                switch (_searchAction)
                {
                    case 0:
                        if (txt_machine_search.Text == "")
                        {

                            showSearch.Visible = false;
                            genaral_machinelist.Visible = true;
                        }
                        else
                        {
                            showSearch.Visible = true;
                            txt_machine_search.Text = String.Empty;
                            genaral_machinelist.Visible = false;
                        }

                        break;

                    case 1:
                        if (txtsearch.Text != "")
                        {
                            details_search_doc.Visible = true;
                            genaralLabCal.Visible = false;
                            details_search_sam.Visible = false;
                            txtsearch.Text = String.Empty;
                        }
                        else
                        {
                            details_search_doc.Visible = false;
                        }
                        //else if (txtsearch.Text == "sam")
                        //{
                        //    details_search_sam.Visible = true;
                        //    details_search_doc.Visible = false;
                        //    genaralLabCal.Visible = false;
                        //    txtsearch.Text = String.Empty;
                        //}
                        break;
                }



                break;
            case "cmdReset":
                int _resetAction = int.Parse(cmdArg);
                switch (_resetAction)
                {
                    case 1:
                        details_search_sam.Visible = false;
                        details_search_doc.Visible = false;
                        genaralLabCal.Visible = true;
                        txtsearch.Text = String.Empty;
                        break;

                    case 0:

                        showSearch.Visible = false;
                        genaral_machinelist.Visible = true;
                        genaral_machine_userview.Visible = false;
                        break;
                    case 3:
                        TabSearch.Visible = false;
                        hiddensearch.Visible = false;
                        btnsearchshow.Visible = true;
                        break;
                }

                break;







        }
    }
    #endregion event command

    #region RadioButton
    protected void RadioButton_CheckedChanged(object sender, System.EventArgs e)
    {


        LinkButton btninsertmachine = (LinkButton)fvInsertLabCal.FindControl("btninsertmachine");
        var rd = (RadioButton)sender;

        Panel paneldetailTool = (Panel)fvInsertLabCal.FindControl("paneldetailTool");
        switch (rd.ID)
        {
            case "machine_1":

                RadioButton machine_1 = (RadioButton)fvInsertLabCal.FindControl("machine_1");
                Panel machine_old = (Panel)fvInsertLabCal.FindControl("machine_old");
                Panel machine_new = (Panel)fvInsertLabCal.FindControl("machine_new");


                Panel div_showdetail_machine_new = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_new");
                Panel div_showdetail_machine_old = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_old");
                if (machine_1.Checked)
                {
                    machine_old.Visible = true;
                    machine_new.Visible = false;
                    div_showdetail_machine_new.Visible = false;
                    paneldetailTool.Visible = false;

                }
                else
                {
                    machine_old.Visible = false;
                    machine_new.Visible = false;
                    paneldetailTool.Visible = false;
                }
                machine_old.Focus();

                break;

            case "machine_2":

                RadioButton machine_2 = (RadioButton)fvInsertLabCal.FindControl("machine_2");
                Panel machine_old_2 = (Panel)fvInsertLabCal.FindControl("machine_old");
                Panel machine_new_2 = (Panel)fvInsertLabCal.FindControl("machine_new");
                Panel div_showdetail_machine_new1 = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_new");
                Panel div_showdetail_machine_old1 = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_old");


                if (machine_2.Checked)
                {
                    machine_new_2.Visible = true;
                    machine_old_2.Visible = false;
                    div_showdetail_machine_old1.Visible = false;
                    paneldetailTool.Visible = false;

                }
                else
                {
                    machine_old_2.Visible = false;
                    machine_new_2.Visible = false;
                    paneldetailTool.Visible = false;
                }
                machine_2.Focus();

                break;

            case "locationtest_machine1":
                Panel location_select = (Panel)fvDocSelectLocationTest.FindControl("location_select");
                RadioButton locationtest_machine1 = (RadioButton)fvDocSelectLocationTest.FindControl("locationtest_machine1");

                if (locationtest_machine1.Checked)
                {
                    location_select.Visible = true;

                }
                else
                {
                    location_select.Visible = false;
                }

                locationtest_machine1.Focus();

                break;

            case "locationtest_machine2":
                Panel location_select_1 = (Panel)fvDocSelectLocationTest.FindControl("location_select");
                RadioButton locationtest_machine2 = (RadioButton)fvDocSelectLocationTest.FindControl("locationtest_machine2");

                if (locationtest_machine2.Checked)
                {
                    location_select_1.Visible = false;

                }
                else
                {
                    location_select_1.Visible = false;
                }

                break;

            case "certificate_1":

                Panel uploadfile = (Panel)fvInsertLabCal.FindControl("uploadfile");
                RadioButton certificate_1 = (RadioButton)fvInsertLabCal.FindControl("certificate_1");

                if (certificate_1.Checked)
                {
                    uploadfile.Visible = true;

                }
                else
                {
                    uploadfile.Visible = false;
                }

                btninsertmachine.Focus();
                break;

            case "certificate_2":

                Panel uploadfile_2 = (Panel)fvInsertLabCal.FindControl("uploadfile");
                RadioButton certificate_2 = (RadioButton)fvInsertLabCal.FindControl("certificate_2");


                if (certificate_2.Checked)
                {
                    uploadfile_2.Visible = false;

                }
                else
                {
                    uploadfile_2.Visible = false;
                }

                btninsertmachine.Focus();

                break;

        }
    }

    #endregion RadioButton

    #region  
    protected void selectedindexchange(object sender, EventArgs e)
    {
        var ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlapprove_cutoff":

                var ddlapprove_cutoff = (DropDownList)fvcutoff_HeaduserAction.FindControl("ddlapprove_cutoff");
                var btnsave_user_cutoff = (LinkButton)fvcutoff_HeaduserAction.FindControl("btnsave_user_cutoff");
                var btnsave_user_notcutoff = (LinkButton)fvcutoff_HeaduserAction.FindControl("btnsave_user_notcutoff");

                if (ddlapprove_cutoff.SelectedValue == "2")
                {
                    btnsave_user_notcutoff.Visible = true;
                    btnsave_user_cutoff.Visible = false;
                    btnsave_user_notcutoff.Focus();

                }
                else
                {
                    btnsave_user_notcutoff.Visible = false;
                    btnsave_user_cutoff.Visible = true;
                    btnsave_user_cutoff.Focus();
                }
                break;

            case "ddlapprove_HeadQacutoff":

                var ddlapprove_HeadQacutoff = (DropDownList)fvcutoff_HeadQaAction.FindControl("ddlapprove_HeadQacutoff");
                var btnsave_HeadQa_cutoff = (LinkButton)fvcutoff_HeadQaAction.FindControl("btnsave_HeadQa_cutoff");
                var btnsave_HeadQa_notcutoff = (LinkButton)fvcutoff_HeadQaAction.FindControl("btnsave_HeadQa_notcutoff");

                if (ddlapprove_HeadQacutoff.SelectedValue == "2")
                {
                    btnsave_HeadQa_notcutoff.Visible = true;
                    btnsave_HeadQa_cutoff.Visible = false;
                    btnsave_HeadQa_notcutoff.Focus();
                }
                else
                {
                    btnsave_HeadQa_notcutoff.Visible = false;
                    btnsave_HeadQa_cutoff.Visible = true;
                    btnsave_HeadQa_cutoff.Focus();
                }
                break;


            case "ddlapprove_Qacutoff":

                var ddlapprove_Qacutoff = (DropDownList)fvcutoff_QaAction.FindControl("ddlapprove_Qacutoff");
                var btnsave_Qa_cutoff = (LinkButton)fvcutoff_QaAction.FindControl("btnsave_Qa_cutoff");
                var btnsave_Qa_notcutoff = (LinkButton)fvcutoff_QaAction.FindControl("btnsave_Qa_notcutoff");

                if (ddlapprove_Qacutoff.SelectedValue == "2")
                {
                    btnsave_Qa_notcutoff.Visible = true;
                    btnsave_Qa_cutoff.Visible = false;
                    btnsave_Qa_notcutoff.Focus();
                }
                else
                {
                    btnsave_Qa_notcutoff.Visible = false;
                    btnsave_Qa_cutoff.Visible = true;
                    btnsave_Qa_cutoff.Focus();
                }
                break;

            case "ddlapprove_headuser":
                var ddlapprove_headuser = (DropDownList)fvActionRemove.FindControl("ddlapprove_headuser");
                var btnsave_headuser_not = (LinkButton)fvActionRemove.FindControl("btnsave_headuser_not");
                var btnsave_user = (LinkButton)fvActionRemove.FindControl("btnsave_user");
                var btnsave_headuser = (LinkButton)fvActionRemove.FindControl("btnsave_headuser");
                if (ddlapprove_headuser.SelectedValue == "2")
                {
                    btnsave_headuser_not.Visible = true;
                    btnsave_headuser.Visible = false;
                    btnsave_user.Visible = false;
                    btnsave_headuser_not.Focus();
                }
                else
                {
                    btnsave_headuser_not.Visible = false;
                    btnsave_headuser.Visible = true;
                    btnsave_user.Visible = false;
                    btnsave_headuser.Focus();
                }
                break;

            case "ddlapprove_Qa":

                var ddlapprove_Qa = (DropDownList)fvremove_qaAction.FindControl("ddlapprove_Qa");
                var btnsave_Qa = (LinkButton)fvremove_qaAction.FindControl("btnsave_Qa");
                var btnsave_Qa_not = (LinkButton)fvremove_qaAction.FindControl("btnsave_Qa_not");

                if (ddlapprove_Qa.SelectedValue == "2")
                {
                    btnsave_Qa_not.Visible = true;
                    btnsave_Qa.Visible = false;
                    btnsave_Qa_not.Focus();
                }
                else
                {
                    btnsave_Qa_not.Visible = false;
                    btnsave_Qa.Visible = true;
                    btnsave_Qa.Focus();
                }
                break;

            case "ddlapprove_head_Qa":

                var ddlapprove_head_Qa = (DropDownList)fvremove_Head_qaAction.FindControl("ddlapprove_head_Qa");
                var btnsave_HeadQa = (LinkButton)fvremove_Head_qaAction.FindControl("btnsave_HeadQa");
                var btnsave_HeadQa_not = (LinkButton)fvremove_Head_qaAction.FindControl("btnsave_HeadQa_not");

                if (ddlapprove_head_Qa.SelectedValue == "2")
                {
                    btnsave_HeadQa_not.Visible = true;
                    btnsave_HeadQa.Visible = false;
                    btnsave_HeadQa_not.Focus();
                }
                else
                {
                    btnsave_HeadQa_not.Visible = false;
                    btnsave_HeadQa.Visible = true;
                    btnsave_HeadQa.Focus();
                }
                break;

            case "ddlapprove_resive":

                var ddlapprove_resive = (DropDownList)fvremove_Recevive.FindControl("ddlapprove_resive");
                var btnsave_reciceve = (LinkButton)fvremove_Recevive.FindControl("btnsave_reciceve");
                var btnsave_reciceve_not = (LinkButton)fvremove_Recevive.FindControl("btnsave_reciceve_not");

                if (ddlapprove_resive.SelectedValue == "2")
                {
                    btnsave_reciceve_not.Visible = true;
                    btnsave_reciceve.Visible = false;
                    btnsave_reciceve_not.Focus();
                }
                else
                {
                    btnsave_reciceve_not.Visible = false;
                    btnsave_reciceve.Visible = true;
                    btnsave_reciceve.Focus();
                }
                break;

            case "ddlresult":

                break;
            case "ddlrecieve_1":
                var ddlrecieve1 = (DropDownList)ExternalLabRecive.FindControl("ddlrecieve_1");
                var inputComment_1 = (Panel)ExternalLabRecive.FindControl("inputComment_1");
                var btnSave2 = (LinkButton)ExternalLabRecive.FindControl("btnSave2");
                var notsave2 = (LinkButton)ExternalLabRecive.FindControl("notsave2");
                if (ddlrecieve1.SelectedValue == "2")
                {
                    inputComment_1.Visible = true;
                    notsave2.Visible = true;
                    btnSave2.Visible = false;
                }
                else
                {
                    inputComment_1.Visible = false;
                    notsave2.Visible = false;
                    btnSave2.Visible = true;
                }


                break;

            case "ddlrecieve":
                var ddlrecieve = (DropDownList)fvReciveLab.FindControl("ddlrecieve");
                var inputComment = (Panel)fvReciveLab.FindControl("inputComment");
                var btnDocSavenot = (LinkButton)fvReciveLab.FindControl("lbDocSavenot");
                var btnDocSave = (LinkButton)fvReciveLab.FindControl("lbDocSave33");
                if (int.Parse(ddlrecieve.SelectedValue) == 2)
                {
                    inputComment.Visible = true;
                    btnDocSavenot.Visible = true;
                    btnDocSave.Visible = false;
                }
                else
                {
                    inputComment.Visible = false;
                    btnDocSavenot.Visible = false;
                    btnDocSave.Visible = true;
                }
                btnDocSave.Focus();
                btnDocSavenot.Focus();

                break;

            case "ddlselecttype":
                var ddllocation = (DropDownList)fvDocSelectLocationTest.FindControl("ddllocation");
                var ddlselecttype = (DropDownList)fvDocSelectLocationTest.FindControl("ddlselecttype");
                var lbDocSave11 = (LinkButton)fvDocSelectLocationTest.FindControl("lbDocSave11");
                if (ddlselecttype.SelectedValue == "2")
                {
                    ddllocation.Enabled = false;
                }
                else
                {
                    ddllocation.Enabled = true;
                }
                lbDocSave11.Focus();
                break;

            case "ddlselecttype1":
                var ddllocation1 = (DropDownList)fvDocSelectLocationTest2.FindControl("ddllocation1");
                var ddlselecttype1 = (DropDownList)fvDocSelectLocationTest2.FindControl("ddlselecttype1");
                var save1 = (LinkButton)fvDocSelectLocationTest2.FindControl("save1");
                if (ddlselecttype1.SelectedValue == "2")
                {
                    ddllocation1.Enabled = false;
                    save1.Focus();
                }
                else
                {
                    ddllocation1.Enabled = true;
                    save1.Focus();
                }

                break;

            case "ddlapprove":
                var ddlapprove = (DropDownList)fvDetailsResultLabCal.FindControl("ddlapprove");
                var btnnotsave = (LinkButton)fvDetailsResultLabCal.FindControl("btnnotsave");
                var btnsave_1 = (LinkButton)fvDetailsResultLabCal.FindControl("btnsave_1");
                if (ddlapprove.SelectedValue == "2")
                {
                    btnnotsave.Visible = true;
                    btnsave_1.Visible = false;
                }
                else
                {
                    btnnotsave.Visible = false;
                    btnsave_1.Visible = true;
                }
                btnnotsave.Focus();
                btnsave_1.Focus();
                break;
        }

    }
    #endregion


    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("tab2", 0, 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int decision)
    {
        mvLabCal.SetActiveView((View)mvLabCal.FindControl(activeTab));
        setFormData(fvInsertLabCal, FormViewMode.ReadOnly, null);
        setFormData(fvDocDetail2, FormViewMode.ReadOnly, null);
        setFormData(fvDocSelectLocationTest, FormViewMode.ReadOnly, null);
        setFormData(fvDocinsertResultLabCal, FormViewMode.ReadOnly, null);
        setFormData(fvDetailsResultLabCal, FormViewMode.ReadOnly, null);
        setFormData(fvDetails3, FormViewMode.ReadOnly, null);
        setFormData(fvDetails4, FormViewMode.ReadOnly, null);
        setFormData(fvDetails5, FormViewMode.ReadOnly, null);
        setFormData(fvDocSelectLocationTest2, FormViewMode.ReadOnly, null);
        setFormData(fvDocinsertResultLabCalMachineOld, FormViewMode.ReadOnly, null);
        setFormData(fvDetailsCalComplete, FormViewMode.ReadOnly, null);
        setFormData(ExternalLabRecive, FormViewMode.ReadOnly, null);
        setFormData(fvReciveLab, FormViewMode.ReadOnly, null);
        setFormData(fvDetails_MachineBeforeremove, FormViewMode.ReadOnly, null);
        setFormData(fvDetailsMachine, FormViewMode.ReadOnly, null);
        setFormData(fvActionRemove, FormViewMode.ReadOnly, null);
        setFormData(fvremove_qaAction, FormViewMode.ReadOnly, null);
        setFormData(fvremove_Head_qaAction, FormViewMode.ReadOnly, null);
        setFormData(fvremove_Recevive, FormViewMode.ReadOnly, null);
        setFormData(fvMachine_beforeCutoff, FormViewMode.ReadOnly, null);
        setFormData(fvcutoff_userAction, FormViewMode.ReadOnly, null);
        setFormData(fvcutoff_HeaduserAction, FormViewMode.ReadOnly, null);
        setFormData(fvcutoff_QaAction, FormViewMode.ReadOnly, null);
        setFormData(fvcutoff_HeadQaAction, FormViewMode.ReadOnly, null);

        Panel div_showdetail_machine_new2 = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_new");
        Panel div_showdetail_machine_old2 = (Panel)fvInsertLabCal.FindControl("div_showdetail_machine_old");


        sampleList.Visible = false;
        sampleRecieve.Visible = false;
        sampleWaitingRecieve.Visible = false;
        sampleWaitingSupervisor.Visible = false;
        sampleResultAccept.Visible = false;
        sampleResultcerfiticate.Visible = false;
        resultLabCalMachinenew.Visible = false;
        logList.Visible = false;
        logListPreview.Visible = false;
        samplelistold.Visible = false;
        samplelistoldExternalLab.Visible = false;
        genaralLabCal_machine_oleCompelte.Visible = false;
        samplelistoldExternalLabComplete.Visible = false;
        genaralLabCal_machine_newCompelte.Visible = false;
        genaralLabCal_machine_oleCompelte.Visible = false;
        logCalExtanalMachineOld.Visible = false;
        sampleLABRecieve.Visible = false;
        samplelistoldExternalLabRecive.Visible = false;
        details_search_doc.Visible = false;
        details_search_sam.Visible = false;
        genaral_machinelist.Visible = false;
        genaral_machine_userview.Visible = false;
        showSearch.Visible = false;
        Waiting_Head_User.Visible = false;
        Waiting_Qa_tranfer.Visible = false;
        Waiting_Head_Qa_tranfer.Visible = false;
        Waiting_Receive_Machine.Visible = false;
        Receive_Machine_Tranfer_Complete.Visible = false;
        Machine_Tranfer_NotComplete.Visible = false;
        Waiting_Head_User_CutOff.Visible = false;
        Waiting_QA_cutoff.Visible = false;
        Waiting_HeadQA_cutoff.Visible = false;
        cutoff_complete.Visible = false;
        cutoff_Notcomplete.Visible = false;
        TabSearch.Visible = false;
        hiddensearch.Visible = false;
        btnsearchshow.Visible = true;

        switch (activeTab)
        {
            case "tab1":
                switch (uidx)
                {

                    case 0:
                        setFormData(fvInsertLabCal, FormViewMode.Insert, null);
                        fvInsertLabCal.Visible = true;
                        break;
                    case 2:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        sampleList.Visible = true;
                        logList.Visible = true;
                        break;
                    case 3:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                sampleList.Visible = true;
                                logList.Visible = true;
                                break;
                            case 1:
                                setFormData(fvDocSelectLocationTest, FormViewMode.Insert, null);
                                break;
                            case 8:
                                samplelistold.Visible = true;
                                logList.Visible = true;
                                break;
                            case 9:
                                sampleList.Visible = true;
                                logList.Visible = true;
                                break;

                        }
                        break;
                    case 4:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                sampleRecieve.Visible = true;
                                sampleWaitingRecieve.Visible = false;
                                logList.Visible = true;
                                break;
                        }
                        break;

                    case 5:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                sampleLABRecieve.Visible = true;
                                fvReciveLab.Visible = false;
                                logList.Visible = true;
                                break;
                            case 1:
                                setFormData(fvReciveLab, FormViewMode.Insert, null);
                                fvReciveLab.Visible = true;
                                break;
                            case 9:
                                logList.Visible = true;
                                sampleLABRecieve.Visible = true;
                                fvReciveLab.Visible = false;
                                break;
                        }

                        break;

                    case 6:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvDocinsertResultLabCal, FormViewMode.Insert, null);
                                break;
                            case 1:
                                sampleWaitingSupervisor.Visible = true;
                                logList.Visible = true;
                                break;
                        }

                        break;

                    case 7:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvDetailsResultLabCal, FormViewMode.Insert, null);

                                break;

                            case 1:
                                sampleResultAccept.Visible = true;
                                logList.Visible = true;
                                break;

                            case 2:
                                samplelistoldExternalLabComplete.Visible = true;
                                btnsaveold.Visible = true;
                                btncancelold.Visible = true;
                                logList.Visible = true;
                                break;

                        }

                        break;

                    case 9:
                        setFormData(fvDetails5, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 0:
                                setFormData(ExternalLabRecive, FormViewMode.Insert, null);
                                break;
                            case 1:
                                setFormData(fvDocSelectLocationTest2, FormViewMode.Insert, null);
                                break;
                            case 2:
                                samplelistoldExternalLab.Visible = true;
                                logList.Visible = true;
                                break;
                            case 3:
                                samplelistoldExternalLabRecive.Visible = true;
                                logList.Visible = true;
                                break;
                            case 4:
                                samplelistold.Visible = true;
                                logList.Visible = true;
                                break;


                        }
                        break;

                    case 10:
                        setFormData(fvDetails5, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 1:
                                setFormData(fvDocinsertResultLabCalMachineOld, FormViewMode.Insert, null);
                                break;

                        }

                        break;

                    case 11:
                        setFormData(fvDocDetail2, FormViewMode.Insert, null);
                        switch (decision)
                        {
                            case 2:
                                setFormData(fvDetails3, FormViewMode.Insert, null);
                                break;

                            case 3:
                                sampleResultcerfiticate.Visible = true;
                                lbDocSave1.Visible = true;
                                btncancelnew.Visible = true;
                                logList.Visible = true;
                                break;
                        }

                        break;

                }

                break;
            case "tab2":
                genaralLabCal.Visible = true;
                switch (uidx)
                {
                    case 2:
                        switch (decision)
                        {
                            case 1:
                                genaralLabCal.Visible = true;

                                break;
                        }
                        break;
                    case 3:
                        switch (decision)
                        {
                            case 1:
                                genaralLabCal.Visible = true;
                                break;
                        }
                        break;
                    case 8:
                        switch (decision)
                        {
                            case 1:
                                genaralLabCal_machine_newCompelte.Visible = true;
                                genaralLabCal.Visible = false;
                                break;
                            case 4:
                                genaralLabCal.Visible = false;
                                genaralLabCal_machine_newCompelte.Visible = false;
                                genaralLabCal_machine_oleCompelte.Visible = true;
                                break;
                        }

                        break;

                }
                break;
            case "tab3":
                switch (uidx)
                {
                    case 0:
                        genaral_machinelist.Visible = true;
                        detailsUser.Visible = false;
                        tool.Visible = true;
                        switch (decision)
                        {
                            case 1:
                                setFormData(fvDetailsMachine, FormViewMode.Insert, null);

                                break;
                        }

                        break;

                    case 1:
                        switch (decision)
                        {
                            case 1:

                                break;
                        }
                        break;

                    case 2:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvDetails_MachineBeforeremove, FormViewMode.Insert, null);
                                setFormData(fvActionRemove, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                fvActionRemove.Visible = true;
                                TabSearch.Visible = false;
                                break;

                            case 1:
                                Waiting_Head_User.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;

                            case 9:
                                Machine_Tranfer_NotComplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;

                                break;
                        }

                        break;

                    case 3:

                        setFormData(fvActionRemove, FormViewMode.Insert, null);
                        var txtcoment_tranfer = (TextBox)fvActionRemove.FindControl("txtcoment_tranfer");
                        var ddltranfer = (DropDownList)fvActionRemove.FindControl("ddltranfer");
                        var ddltranfer1 = (DropDownList)fvActionRemove.FindControl("ddltranfer1");
                        var ddltranfermachine = (DropDownList)fvActionRemove.FindControl("ddltranfermachine");
                        var btnsave_headuser = (LinkButton)fvActionRemove.FindControl("btnsave_headuser");
                        var btnsave_user = (LinkButton)fvActionRemove.FindControl("btnsave_user");
                        var formapprove = (Panel)fvActionRemove.FindControl("formapprove");

                        switch (decision)
                        {

                            case 0:

                                setFormData(fvDetails_MachineBeforeremove, FormViewMode.Insert, null);

                                detailsUser.Visible = true;

                                txtcoment_tranfer.Text = "test tranfer machine";
                                txtcoment_tranfer.Enabled = false;
                                ddltranfer.Enabled = false;
                                ddltranfermachine.Enabled = false;
                                tool.Visible = false;
                                btnsave_headuser.Visible = true;
                                btnsave_user.Visible = false;
                                formapprove.Visible = true;
                                ddltranfer1.Enabled = false;
                                break;

                            case 1:
                                tool.Visible = true;
                                Waiting_Qa_tranfer.Visible = true;
                                detailsUser.Visible = false;
                                fvActionRemove.Visible = false;
                                break;

                            case 9:
                                Machine_Tranfer_NotComplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;
                        }
                        break;

                    case 4:

                        switch (decision)
                        {
                            case 0:
                                setFormData(fvremove_qaAction, FormViewMode.Insert, null);
                                setFormData(fvDetails_MachineBeforeremove, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                break;

                            case 1:
                                tool.Visible = true;
                                Waiting_Head_Qa_tranfer.Visible = true;
                                detailsUser.Visible = false;

                                break;

                            case 9:
                                Machine_Tranfer_NotComplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;
                        }

                        break;

                    case 5:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvDetails_MachineBeforeremove, FormViewMode.Insert, null);
                                setFormData(fvremove_Head_qaAction, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;


                                break;
                            case 1:
                                Waiting_Receive_Machine.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;

                            case 9:
                                Machine_Tranfer_NotComplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;

                        }
                        break;

                    case 6:
                        switch (decision)
                        {

                            case 0:
                                setFormData(fvDetails_MachineBeforeremove, FormViewMode.Insert, null);
                                setFormData(fvremove_Recevive, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                break;

                            case 1:
                                Receive_Machine_Tranfer_Complete.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;
                        }
                        break;
                    //Cut Off Machine
                    case 8:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvMachine_beforeCutoff, FormViewMode.Insert, null);
                                setFormData(fvcutoff_userAction, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                break;

                            case 1:
                                Waiting_Head_User_CutOff.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;

                            case 9:
                                cutoff_Notcomplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;
                        }

                        break;

                    case 9:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvcutoff_HeaduserAction, FormViewMode.Insert, null);
                                setFormData(fvMachine_beforeCutoff, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                break;

                            case 1:
                                Waiting_QA_cutoff.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;

                            case 9:
                                cutoff_Notcomplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;

                        }
                        break;

                    case 10:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvcutoff_QaAction, FormViewMode.Insert, null);
                                setFormData(fvMachine_beforeCutoff, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;
                                break;

                            case 1:
                                Waiting_HeadQA_cutoff.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;

                            case 9:
                                cutoff_Notcomplete.Visible = true;
                                genaral_machinelist.Visible = false;
                                detailsUser.Visible = false;
                                tool.Visible = true;
                                break;
                        }


                        break;

                    case 11:
                        switch (decision)
                        {
                            case 0:
                                setFormData(fvcutoff_HeadQaAction, FormViewMode.Insert, null);
                                setFormData(fvMachine_beforeCutoff, FormViewMode.Insert, null);
                                detailsUser.Visible = true;
                                tool.Visible = false;

                                break;

                            case 1:
                                cutoff_complete.Visible = true;
                                tool.Visible = true;
                                detailsUser.Visible = false;
                                break;
                        }

                        break;


                }

                break;

            case "tab5":
                switch (uidx)
                {
                    case 0:
                        switch (decision)
                        {
                            case 3:
                                master_result_test.Visible = true;
                                master_unit.Visible = false;

                                break;

                            case 4:
                                master_unit.Visible = true;
                                master_result_test.Visible = false;
                                break;
                        }
                        break;
                }
                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int decision)
    {
        setActiveView(activeTab, uidx, decision);
        switch (activeTab)
        {
            case "tab1":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                //  li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "tab2":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                //   li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            case "tab3":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                // li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");
                break;
            //case "tab4":
            //    li0.Attributes.Add("class", "");
            //    li1.Attributes.Add("class", "");
            //    li2.Attributes.Add("class", "");
            //    li3.Attributes.Add("class", "active");
            //    li4.Attributes.Add("class", "");
            //    break;
            case "tab5":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                // li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");
                break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }
    #endregion reuse



}
