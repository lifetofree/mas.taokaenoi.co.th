﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_m0_saleunit.aspx.cs" Inherits="websystem_MasterData_ecom_m0_saleunit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addplace" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มหน่วยของสินค้า" data-toggle="tooltip" title="เพิ่มหน่วยของสินค้า" runat="server" CommandName="cmdAddUnit" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มหน่วยของสินค้า</asp:LinkButton>

                <div class="clearfix"></div>
            </div>
            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class=" panel panel-info">
                        <div class=" panel-heading">
                            <h3 class="panel-title">เพิ่มหน่วยของสินค้า
                            </h3>

                        </div>
                        <div class=" panel-body">

                            <div class=" form-horizontal" style="padding: 20px 30px;">

                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="รหัส Material"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtmaterial" runat="server" CssClass="form-control" placeholder="กรอกรหัส Material ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Re_place_name" runat="server"
                                            ControlToValidate="txtmaterial" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกรหัส Material" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="ชื่อหน่วยสินค้า"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtunit_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อหน่วยสินค้า..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtunit_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อหน่วยสินค้า" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <asp:DropDownList ID="ddPlace" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-offset-3 col-sm-9">
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdSave" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึก" Text="Save" ValidationGroup="Save"></asp:LinkButton>
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdCancel" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิก" Text="Cancel"></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <div id="div_searchmaster_page" runat="server">

                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">ค้นหา
                        </h3>
                    </div>
                    <div class=" panel-body">

                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label130" runat="server" Text="รหัส Material" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_search" runat="server" CssClass="form-control" PlaceHolder="........" />

                                </div>
                                <asp:Label ID="Label131" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="Src_status" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="ALL" />
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_src" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="Gv_select_place" runat="server" Visible="true" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                OnRowEditing="Master_RowEditing"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                PageSize="20"
                AutoPostBack="FALSE">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>

                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("un_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_un_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("un_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lbupdates_place_name" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหน่วยสินค้า" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_place" runat="server" CssClass="form-control " Text='<%# Eval("unit_name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorplace_name" runat="server"
                                                    ControlToValidate="Name_place" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorplace_name" Width="220" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="รหัส Material" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_material" runat="server" CssClass="form-control " Text='<%# Eval("material") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                    ControlToValidate="Name_material" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกรหัส Material" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_place" Text='<%# Eval("un_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รหัส Material" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("material") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อหน่วยสินค้า" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("unit_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding: 5px 10px 0px; text-align: left;">
                                    <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("un_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding: 5px 10px 0px; text-align: center;">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("un_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

    </asp:MultiView>


</asp:Content>
