﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_report_time_payroll.aspx.cs" Inherits="websystem_hr_hr_report_time_payroll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImage").MultiFile();
            }
        })
    </script>

    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <%--<asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">รายการทั่วไป</asp:LinkButton>
                        <asp:LinkButton ID="lbadd" CssClass="btn_menulist" runat="server" CommandName="btnadd" OnCommand="btnCommand" CommandArgument="2">เพิ่มพนักงาน</asp:LinkButton>
                        <asp:LinkButton ID="lbsetapprove" CssClass="btn_menulist" runat="server" CommandName="btnsetapprove" OnCommand="btnCommand" CommandArgument="3">จัดการข้อมูลผู้อนุมัติ</asp:LinkButton>--%>
                        <asp:LinkButton ID="lblreport" CssClass="btn_menulist" runat="server" CommandName="btnreport" OnCommand="btnCommand" CommandArgument="4">รายงาน</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <%--<asp:Label ID="lbl_txt" runat="server"></asp:Label>--%>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewReport" runat="server">

            <asp:Panel ID="box_index" runat="server" Visible="true">
                <div class="col-lg-12">
                    <asp:Label ID="adf" runat="server"></asp:Label>
                    <%-------------- BoxSearch Start--------------%>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล Report</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" id="Div1" runat="server">
                                <asp:FormView ID="Fv_Search_Emp_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <InsertItemTemplate>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label38" runat="server" Text="Employee Code" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label39" runat="server" Text="รหัสพนักงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtempcode_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label121" runat="server" Text="Employee Name" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label122" runat="server" Text="ชื่อพนักงาน" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtempname_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>
                                        </div>

                                        <asp:Panel ID="Panel1" runat="server" Visible="false">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label269" runat="server" Text="Start Date" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label270" runat="server" Text="วันที่" /></b></small>
                                                    </h2>
                                                </label>
                                                <div class="col-sm-2">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtdatestart" runat="server" CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="10"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>

                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label271" runat="server" Text="End Date" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label272" runat="server" Text="ถึงวันที่" /></b></small>
                                                    </h2>
                                                </label>
                                                <div class="col-sm-2">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtdateend" AutoComplete="off" MaxLength="10" CssClass="form-control from-date-datepicker" runat="server"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="BoxSearch_Date" runat="server" Visible="true">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label1" runat="server" Text="Month" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label2" runat="server" Text="รอบการจ่ายเงิน" /></b></small>
                                                    </h2>
                                                </label>

                                                <div class="col-sm-2">
                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกเดือน....</asp:ListItem>
                                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                                <asp:ListItem Value="5">พฤษภาคม</asp:ListItem>
                                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                                <asp:ListItem Value="7">กรกฏาคม</asp:ListItem>
                                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Rewwq21" ValidationGroup="Search" runat="server" Display="None"
                                                                ControlToValidate="ddlmonth" Font-Size="11"
                                                                ErrorMessage="เลือกเดือน ...."
                                                                ValidationExpression="เลือกเดือน ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="Va2li3der4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Rewwq21" Width="160" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="col-sm-1">
                                                    <asp:Label ID="Label437" runat="server" ForeColor="Red" Text="***" />
                                                </div>

                                                <label class="col-sm-2 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label3" runat="server" Text="Location" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label4" runat="server" Text="ประจำสำนักงาน" /></b></small>
                                                    </h2>
                                                </label>

                                                <div class="col-sm-2">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddllocation" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกสำนักงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="ddllocation" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>

                                            </div>
                                        </asp:Panel>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlorg_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlorg_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddldep_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddldep_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label105" runat="server" Text="Section" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label106" runat="server" Text="หน่วยงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlsec_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlsec_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label220" runat="server" Text="EmpType" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label225" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlemptype_s" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                            <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                            <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlemptype_s" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="btnsearch_report" class="btn btn-success" ValidationGroup="Search" runat="server" Text="Search" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" Visible="true" />
                                                        <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnsearch_report" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Button ID="btnexport" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูลทั่วไป)" CommandName="btnExport" CommandArgument="1" OnCommand="btnCommand" title="Export"></asp:Button>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnexport" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <%-------------- BoxSearch End--------------%>

                    <div class="panel-heading">
                        <h5 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list" runat="server" /></strong></font></h5>
                    </div>

                    

                    <asp:GridView ID="GvEmployee_Gv1"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lbllate_idx" runat="server" Text='<%# Eval("late_idx") %>' Visible="false"></asp:Label>--%>
                                        <asp:Label ID="lblemp_code_1" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lblpay_date_1" runat="server" Text='<%# formatDate((String)Eval("pay_date")) %>'></asp:Label>--%>
                                        <asp:Label ID="lblpay_date_1" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                        <asp:DropDownList ID="ddltemp" runat="server" CssClass="form-control" Visible="false" />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_1" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_1" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_1" runat="server" ></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_1" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_1" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_1" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv2"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lblreturn_idx" runat="server" Text='<%# Eval("return_idx") %>' Visible="false"></asp:Label>--%>
                                        <asp:Label ID="lblemp_code_2" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_2" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                        <asp:DropDownList ID="ddltemp_2" runat="server" CssClass="form-control" Visible="true" />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_2" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_2" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_2" runat="server"></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_2" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_2" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_2" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv3"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblsick_idx" runat="server" Text='<%# Eval("sick_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_3" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_3" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_3" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_3" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_3" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_3" runat="server" Text='<%# Eval("unit","{0:f0}") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_3" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_3" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv4"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblsick_non_idx" runat="server" Text='<%# Eval("sick_non_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_4" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_4" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_4" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_4" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_4" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_4" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_4" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_4" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv5"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblholi_idx" runat="server" Text='<%# Eval("holi_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_5" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_5" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_5" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_5" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_5" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_5" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_5" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_5" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv6"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblbirt_idx" runat="server" Text='<%# Eval("birt_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_6" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_6" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_6" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_6" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_6" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_6" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_6" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_6" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv7"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpro_idx" runat="server" Text='<%# Eval("pro_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_7" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_7" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_7" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_7" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_7" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_7" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_7" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_7" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv8"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbltake_idx" runat="server" Text='<%# Eval("take_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_8" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_8" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_8" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_8" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_8" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_8" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_8" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_8" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv9"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblabsen_idx" runat="server" Text='<%# Eval("absen_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_9" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_9" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_9" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_9" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_9" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_9" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_9" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_9" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv10"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lblabsen_re_idx" runat="server" Text='<%# Eval("absen_re_idx") %>' Visible="false"></asp:Label>--%>
                                        <asp:Label ID="lblemp_code_10" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_10" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                        <asp:DropDownList ID="ddltemp_10" runat="server" CssClass="form-control" Visible="false" />
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_10" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_10" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_10" runat="server"></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_10" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_10" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_10" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv11"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblabsen_late_idx" runat="server" Text='<%# Eval("absen_late_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_11" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_11" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_11" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_11" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_11" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_11" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_11" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_11" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv12"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblabsen_all_idx" runat="server" Text='<%# Eval("absen_all_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_12" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_12" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_12" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_12" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_12" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_12" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_12" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_12" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv13"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpb_absen_idx" runat="server" Text='<%# Eval("pb_absen_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_13" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_13" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_13" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_13" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_13" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_13" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_13" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_13" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv14"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblat_over_idx" runat="server" Text='<%# Eval("at_over_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_14" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_14" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_14" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_14" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_14" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_14" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_14" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_14" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv15"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmat_befor_idx" runat="server" Text='<%# Eval("mat_befor_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_15" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_15" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_15" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_15" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_15" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_15" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_15" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_15" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv16"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmat_after_idx" runat="server" Text='<%# Eval("mat_after_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_16" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_16" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_16" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_16" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_16" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_16" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_16" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_16" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv17"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_idx" runat="server" Text='<%# Eval("etc_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_17" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_17" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินหัก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_17" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_17" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_17" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_17" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_17" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbletc_17" runat="server" Text='<%# Eval("etc") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="GvEmployee_Gv18"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="10000"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbltime_idx" runat="server" Text='<%# Eval("time_idx") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblemp_code_18" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่จ่ายเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblpay_date_18" runat="server" Text='<%# Eval("pay_date") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทข้อมูลเวลา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_type_18" runat="server" Text='<%# Eval("money_type") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนเงิน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblmoney_18" runat="server" Text='<%# Eval("money") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblnum_18" runat="server" Text='<%# Eval("num") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หน่วย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblunit_18" runat="server" Text='<%# Eval("unit") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อัตรา" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblcapacity_18" runat="server" Text='<%# Eval("capacity") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>
            </asp:Panel>

        </asp:View>



    </asp:MultiView>

</asp:Content>

