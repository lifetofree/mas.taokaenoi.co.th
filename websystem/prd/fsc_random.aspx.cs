using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

public partial class websystem_prd_fsc_random : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();
    data_fsc _data_fsc = new data_fsc();

    int _emp_idx = 0;
    int _rpos_idx = 0;
    int _temp_idx = 0;
    int _temp_sum = 0;
    int _temp_value = 0;
    string _temp_data = "";
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    static string _urlFscSearchRandomList = _serviceUrl + ConfigurationManager.AppSettings["urlFscSearchRandomList"];
    static string _urlFscGetRandomList = _serviceUrl + ConfigurationManager.AppSettings["urlFscGetRandomList"];
    static string _urlFscSetRandomList = _serviceUrl + ConfigurationManager.AppSettings["urlFscSetRandomList"];
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        linkBtnTrigger(lbSave);
        LinkButton lbRandom = (LinkButton)fvRandom.FindControl("lbRandom");
        linkBtnTrigger(lbRandom);
        LinkButton lbClear = (LinkButton)fvRandom.FindControl("lbClear");
        linkBtnTrigger(lbClear);
        LinkButton lbItemSearch = (LinkButton)fvSearch.FindControl("lbItemSearch");
        linkBtnTrigger(lbItemSearch);
        LinkButton lbItemClear = (LinkButton)fvSearch.FindControl("lbItemClear");
        linkBtnTrigger(lbItemClear);
        LinkButton lbExport = (LinkButton)fvSearch.FindControl("lbExport");
        linkBtnTrigger(lbExport);
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        lbSave.Visible = false;
        divRandom.Visible = false;
        divSearch.Visible = false;

        switch (cmdName)
        {
            case "cmdRandom":
                CheckBoxList cblEmpTypeIdx = (CheckBoxList)fvRandom.FindControl("cblEmpTypeIdx");
                CheckBoxList cblPlantIdx = (CheckBoxList)fvRandom.FindControl("cblPlantIdx");
                CheckBoxList cblNatIdx = (CheckBoxList)fvRandom.FindControl("cblNatIdx");
                CheckBoxList cblRdeptIdx = (CheckBoxList)fvRandom.FindControl("cblRdeptIdx");

                // get emp_type_idx list
                string _empTypeIdx = "";
                // for (int i = 0; i < cblEmpTypeIdx.Items.Count; i++)
                // {
                //     if (cblEmpTypeIdx.Items[i].Selected)
                //     {
                //         _empTypeIdx += cblEmpTypeIdx.Items[i].Value + ",";
                //     }
                // }
                // _empTypeIdx = _empTypeIdx.Length > 1 ? _empTypeIdx.Substring(0, _empTypeIdx.Length - 1) : "0";

                // // get plant_idx list
                string _plantIdx = "";
                // for (int i = 0; i < cblPlantIdx.Items.Count; i++)
                // {
                //     if (cblPlantIdx.Items[i].Selected)
                //     {
                //         _plantIdx += cblPlantIdx.Items[i].Value + ",";
                //     }
                // }
                // _plantIdx = _plantIdx.Length > 1 ? _plantIdx.Substring(0, _plantIdx.Length - 1) : "0";

                // // get nat_idx list
                string _natIdx = "";
                // for (int i = 0; i < cblNatIdx.Items.Count; i++)
                // {
                //     if (cblNatIdx.Items[i].Selected)
                //     {
                //         _natIdx += cblNatIdx.Items[i].Value + ",";
                //     }
                // }
                // _natIdx = _natIdx.Length > 1 ? _natIdx.Substring(0, _natIdx.Length - 1) : "0";

                // // get rdept_idx list
                string _rdeptIdx = "";
                // for (int i = 0; i < cblRdeptIdx.Items.Count; i++)
                // {
                //     if (cblRdeptIdx.Items[i].Selected)
                //     {
                //         _rdeptIdx += cblRdeptIdx.Items[i].Value + ",";
                //     }
                // }
                _rdeptIdx = "23,9,15,2179,26,12,27,127,130,7,24,10,2168,2180,19,25,13,2177,33,16,2166,2184,11,2181,20,28,131,137";//_rdeptIdx.Length > 1 ? _rdeptIdx.Substring(0, _rdeptIdx.Length - 1) : "0";

                if (_rdeptIdx == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกฝ่ายที่ต้องการสุ่มรายชื่อ');", true);
                    return;
                }

                search_fsc_detail _searchList = new search_fsc_detail();
                _searchList.s_emp_type_idx = _empTypeIdx;
                _searchList.s_plant_idx = _plantIdx;
                _searchList.s_nat_idx = _natIdx;
                _searchList.s_rdept_idx = _rdeptIdx;

                _data_fsc.search_fsc_list = new search_fsc_detail[1];
                _data_fsc.search_fsc_list[0] = _searchList;

                // litDebug.Text = _funcTool.convertObjectToJson(_data_fsc);

                _data_fsc = callServicePostFsc(_urlFscSearchRandomList, _data_fsc);
                _funcTool.setGvData(gvRandomList, _data_fsc.fsc_random_list);
                lbSave.Visible = (_data_fsc.fsc_random_list != null) ? true : false;
                divRandom.Visible = true;
                divSearch.Visible = false;
                break;
            case "cmdClear":
                _funcTool.setFvData(fvRandom, FormViewMode.Insert, null);
                _funcTool.setGvData(gvRandomList, null);
                setActiveTab("viewList", 0);
                hlSetTotop.Focus();
                break;
            case "cmdSave":
                _data_fsc.fsc_random_list = new fsc_random_detail[gvRandomList.Rows.Count];
                int j = 0;
                foreach (GridViewRow gvRow in gvRandomList.Rows)
                {
                    HiddenField hfEmpIdx = (HiddenField)gvRow.FindControl("hfEmpIdx");

                    fsc_random_detail _random_list = new fsc_random_detail();
                    _random_list.emp_idx = _funcTool.convertToInt(hfEmpIdx.Value);
                    _data_fsc.fsc_random_list[j] = _random_list;
                    j++;
                }

                // litDebug.Text = _funcTool.convertObjectToJson(_data_fsc);

                _data_fsc = callServicePostFsc(_urlFscSetRandomList, _data_fsc);
                defaultData();
                setActiveTab("viewList", 0);

                hlSetTotop.Focus();
                break;
            case "cmdItemSearch":
                TextBox tbItemSearch = (TextBox)fvSearch.FindControl("tbItemSearch");
                CheckBoxList cblRdeptIdxSearch = (CheckBoxList)fvSearch.FindControl("cblRdeptIdxSearch");
                _data_fsc = (data_fsc)ViewState["EmployeeList"];
                fsc_random_detail[] _tempData = (fsc_random_detail[])_data_fsc.fsc_random_list;

                var selectedRdept = new List<string>();
                for (int k = 0; k < cblRdeptIdxSearch.Items.Count; k++)
                {
                    if (cblRdeptIdxSearch.Items[k].Selected)
                    {
                        selectedRdept.Add(cblRdeptIdxSearch.Items[k].Value);
                    }
                }

                // select all
                var _linqSearch = from o in _tempData
                                  select o;

                if (tbItemSearch.Text.Trim() != "")
                    _linqSearch = _tempData
                                    .Where(o1 => o1.emp_code.Contains(tbItemSearch.Text)
                                    );

                if (selectedRdept.Count > 0)
                    _linqSearch = _linqSearch
                                    .Where(o2 => selectedRdept.Contains(o2.dept_name_th)
                                    );

                _funcTool.setGvData(gvRandomList, _linqSearch.ToList());
                lbSave.Visible = false;
                divRandom.Visible = false;
                divSearch.Visible = true;
                hlSetTotop.Focus();
                break;
            case "cmdItemClear":
                lbSave.Visible = false;
                divRandom.Visible = false;
                divSearch.Visible = true;
                defaultData();
                setActiveTab("viewList", 0);
                hlSetTotop.Focus();
                break;
            case "cmdItemExport":
                // export data
                data_fsc _reportData = (data_fsc)ViewState["EmployeeList"];
                DataTable tableReport = new DataTable();
                int _count_row = 0;

                if (_reportData.fsc_random_list == null)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    return;
                }

                if (_reportData.fsc_random_list != null)
                {
                    tableReport.Columns.Add("#", typeof(String));
                    tableReport.Columns.Add("รหัสพนักงาน", typeof(String));
                    tableReport.Columns.Add("ฝ่าย", typeof(String));
                    tableReport.Columns.Add("แผนก", typeof(String));
                    tableReport.Columns.Add("สถานะแบบประเมิน", typeof(String));
                    tableReport.Columns.Add("วันที่อัพเดท", typeof(String));

                    foreach (var temp_row in _reportData.fsc_random_list)
                    {
                        DataRow add_row = tableReport.NewRow();

                        add_row[0] = (_count_row + 1).ToString();
                        add_row[1] = temp_row.emp_code.ToString();
                        add_row[2] = temp_row.dept_name_th.ToString();
                        add_row[3] = temp_row.sec_name_th.ToString();
                        add_row[4] = temp_row.survey_status == 0 ? "รอดำเนินการ" : "ดำเนินการเรียบร้อย";
                        add_row[5] = temp_row.update_date.ToString();

                        tableReport.Rows.InsertAt(add_row, _count_row++);
                    }

                    WriteExcelWithNPOI(tableReport, "xls", "employee-list");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                }

                setActiveTab("viewList", 0);
                hlSetTotop.Focus();
                break;
        }
    }
    #endregion event command

    #region checkbox list
    protected void getDepartmentList(CheckBoxList cblName, int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        _funcTool.setCblData(cblName, _data_employee.department_list, "dept_name_th", "rdept_idx");
    }
    #endregion checkbox list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewList", 0);

        hlSetTotop.Focus();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["EmployeeList"] = null;
        // ViewState["VisitorSelected"] = null;
        // ViewState["ReportList"] = null;
    }

    protected void defaultData()
    {
        if (ViewState["EmployeeList"] == null)
        {
            _data_fsc = new data_fsc();
            _data_fsc = callServicePostFsc(_urlFscGetRandomList, _data_fsc);

            ViewState["EmployeeList"] = _data_fsc;
        }
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);

        lbSave.Visible = false;
        divRandom.Visible = false;
        divSearch.Visible = false;

        switch (activeTab)
        {
            case "viewList":
                _data_fsc = new data_fsc();
                _data_fsc = callServicePostFsc(_urlFscGetRandomList, _data_fsc);

                ViewState["EmployeeList"] = _data_fsc;

                data_fsc _tempCheck = (data_fsc)ViewState["EmployeeList"];
                if (_tempCheck.fsc_random_list != null && _tempCheck.fsc_random_list.Count() > 0)
                {
                    fsc_random_detail[] _tempData = (fsc_random_detail[])_tempCheck.fsc_random_list;
                    _funcTool.setFvData(fvSearch, FormViewMode.Insert, null);
                    _funcTool.setGvData(gvRandomList, _tempData);
                    lbSave.Visible = false;
                    divRandom.Visible = false;
                    divSearch.Visible = true;

                    try
                    {
                        // set search condition
                        var _linqDept = from d in _tempData
                                        group d by d.dept_name_th into o
                                        select o;

                        // _funcTool.setGvData(gvDeptList, _linqDept.ToList());

                        CheckBoxList cblRdeptIdxSearch = (CheckBoxList)fvSearch.FindControl("cblRdeptIdxSearch");
                        _funcTool.setCblData(cblRdeptIdxSearch, _linqDept.ToList(), "Key", "Key");
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    // CheckBoxList cblRdeptIdx = (CheckBoxList)fvRandom.FindControl("cblRdeptIdx");
                    // getDepartmentList(cblRdeptIdx, 1);
                    lbSave.Visible = false;
                    divRandom.Visible = true;
                    divSearch.Visible = false;
                }
                break;
        }
    }

    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    protected data_fsc callServicePostFsc(string _cmdUrl, data_fsc _data_fsc)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_fsc);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_fsc = (data_fsc)_funcTool.convertJsonToObject(typeof(data_fsc), _local_json);

        return _data_fsc;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }
    #endregion reuse
}