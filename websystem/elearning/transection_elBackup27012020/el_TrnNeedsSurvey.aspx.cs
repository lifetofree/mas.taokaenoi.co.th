﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_TrnNeedsSurvey : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();
    service_mail servicemail = new service_mail();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_training"];
    static string _urlSetInsel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_training"];
    static string _urlSetUpdel_u0_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u0_training_req"];
    static string _urlDelel_u0_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u0_training_req"];
    static string _urlDelel_u1u2_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u1u2_training_req"];
    static string _urlGetErrorel_m0_training = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_training"];
    static string _urlGetel_lu_position = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_lu_position"];


    static string _urlSetInsel_u0_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u0_training_req"];
    static string _urlSetInsel_u1_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u1_training_req"];
    static string _urlSetInsel_u2_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u2_training_req"];
    static string _urlSetInsel_u3_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u3_training_req"];

    static string _urlGetel_u0_training_req = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u0_training_req"];

    static string _urlGetel_m0_sche_trn_dayL = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_sche_trn_dayL"];
    static string _urlsendEmail_Traning = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_Traning"];

    string _localJson = "";
    string _FromTrnNeedsSurRunNo = "traning_needs_survey";
    int _tempInt = 0;

    int emp_idx = 0;




    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
        //node

    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!IsPostBack)
        {
            _func_dmu.zSetDdlMonth(ddlMonthSearch_L);
            ddlMonthSearch_L.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
            _func_dmu.zDropDownList(ddlYearSearch_L,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-YEAR"
                                );
            select_empIdx();
            _data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];
            m0_sche_trn_day obj = new m0_sche_trn_day();
            _data_elearning.el_m0_sche_trn_day_action[0] = obj;
            _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_sche_trn_dayL, _data_elearning);
            int _int = 0;
            if (_data_elearning.el_m0_sche_trn_day_action == null)
            {
                _int = 0;
            }
            else
            {
                obj = _data_elearning.el_m0_sche_trn_day_action[0];
                _int = obj.zItem;
            }

            if (_int > 0)
            {
                pnlrightError.Visible = false;
                pnlright.Visible = true;
                zSetMode(2);
            }
            else
            {
                pnlrightError.Visible = true;
                pnlright.Visible = false;
            }

            if (getViewState("joblevel_idx") >= 7)
            {
                btnApproveList.Visible = true;
                btnApproveCom.Visible = true;
                zShowdataAppList_Status();
            }
            else
            {
                btnApproveList.Visible = false;
                btnApproveCom.Visible = false;
            }




        }
    }

    #endregion Page Load


    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {
        pnlDetailApp.Visible = true;
        switch (AiMode)
        {
            case 0:  //insert mode
                {

                    pnlListData.Visible = false;
                    pnlselect.Visible = true;
                    pnlApp.Visible = false;
                    pnlApproveList.Visible = false;
                    setActiveTab("I");
                    SetViewStateEmpty();
                    actionIndex();
                }
                break;
            case 1:  //update mode
                {

                    //MultiViewBody.SetActiveView(ViewEntryUpdate);
                }
                break;
            case 2://preview mode
                {

                    pnlListData.Visible = true;
                    pnlselect.Visible = false;
                    pnlApp.Visible = false;
                    pnlApproveList.Visible = false;
                    setActiveTab("P");
                    CreateDs_dsListData();
                    ShowListData();

                }
                break;
            case 3://Approve mode
                {
                    pnlListData.Visible = false;
                    pnlselect.Visible = false;
                    pnlApp.Visible = true;
                    pnlApproveList.Visible = false;
                    setActiveTab("A");
                    CreateDs_dsListData();
                    // ShowListData();

                }
                break;
            case 4://Approve List mode
                {
                    pnlListData.Visible = false;
                    pnlselect.Visible = false;
                    pnlApp.Visible = false;
                    pnlApproveList.Visible = true;
                    setActiveTab("A");

                    // ShowListData();

                }
                break;
            case 5://Approve List mode
                {

                    pnlListData.Visible = false;
                    pnlselect.Visible = false;
                    pnlApp.Visible = false;
                    pnlApproveList.Visible = true;
                    setActiveTab("ACON");

                    // ShowListData();

                }
                break;
            case 6:  //Pre mode
                {
                    pnlListData.Visible = false;
                    pnlselect.Visible = false;
                    pnlApp.Visible = true;
                    pnlApproveList.Visible = false;
                    pnlDetailApp.Visible = false;
                    //MultiViewBody.SetActiveView(ViewEntryUpdate);
                }
                break;
            case 7://Approve Conf
                {
                    pnlListData.Visible = false;
                    pnlselect.Visible = false;
                    pnlApp.Visible = true;
                    pnlApproveList.Visible = false;
                    setActiveTab("ACON");
                    CreateDs_dsListData();

                }
                break;

        }


    }
    #endregion zSetMode

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liListData.Attributes.Add("class", "");
        liInsert.Attributes.Add("class", "");
        liApproveList.Attributes.Add("class", "");
        liApproveCom.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");
                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                break;
            case "A":
                liApproveList.Attributes.Add("class", "active");
                break;
            case "ACON":
                liApproveCom.Attributes.Add("class", "active");
                break;
        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {
        Select_DetailList();
        Select_LookupInsert();
        showFooter_GvTraningList();
    }
    protected void select_empIdx()
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        dataelearning.employeeM_action[0] = obj;
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, dataelearning);
        if (dataelearning.employeeM_action != null)
        {
            foreach (var item in dataelearning.employeeM_action)
            {
                //string emp_code = "";
                //int rpos_idx = 0;
                //int rsec_idx = 0;
                //int rdept_idx = 0;
                //int org_idx = 0;
                //int jobgrade_idx = 0;
                //int joblevel_idx = 0;
                //int EmpIDXApprove1 = 0;
                //string NameApprover1 = "";
                //int EmpIDXApprove2 = 0;
                //string NameApprover2 = "";

                ViewState["emp_code"] = item.EmpCode;
                ViewState["rpos_idx"] = item.RPosIDX_J;
                ViewState["rsec_idx"] = item.RSecID;
                ViewState["rdept_idx"] = item.RDeptID;
                ViewState["org_idx"] = item.OrgIDX;
                ViewState["jobgrade_idx"] = item.JobGradeIDX;
                ViewState["joblevel_idx"] = item.JobLevel;
                ViewState["EmpIDXApprove1"] = item.EmpIDXApprove1;
                ViewState["NameApprover1"] = item.NameApprover1;
                ViewState["EmpIDXApprove2"] = item.EmpIDXApprove2;
                ViewState["NameApprover2"] = item.NameApprover2;
            }
        }
    }
    protected void Select_DetailList()
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(_data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);
        _func_dmu.zSetFormViewData(FvDetailUser, _data_elearning.employeeM_action);
        if (_data_elearning.employeeM_action != null)
        {
            foreach (var item in _data_elearning.employeeM_action)
            {
                ViewState["DocCode"] = item.EmpCode;
                ViewState["CEmpIDX_Create"] = item.EmpIDX;
                ViewState["RPosIDX_J"] = item.RPosIDX_J;
                ViewState["RDeptID"] = item.RDeptID;
                ViewState["JobLevel"] = item.JobLevel;
                ViewState["joblevel_idx"] = item.JobLevel;
            }
        }
    }
    protected void Select_DetailListApp(int id)
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.idx = id;
        obj.operation_status_id = "EMP-TRN";
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);
        _func_dmu.zSetFormViewData(FvDetailUserApp, _data_elearning.employeeM_action);

    }
    private void SetViewStateEmpty()
    {
        ViewState["DocCode"] = "";
        ViewState["CEmpIDX_Create"] = "";
        ViewState["RPosIDX_J"] = "0";
        ViewState["RDeptID"] = "0";
        getDatasetEmployee();
        DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];
        _func_dmu.zSetGridData(GvTraningList, dsEmpTraning.Tables["dsEmpTraning"]);

    }
    protected void Select_LookupInsert()
    {
        TextBox txtdate = (TextBox)FvDetailUser.FindControl("txtu0_training_req_date");
        txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
        _func_dmu.zDropDownList(ddltranning,
                                "",
                                "m0_training_idx",
                                "training_name",
                                "0",
                                "m0_training",
                                ""
                                );

        ddltranning.Items.Add(new ListItem("อื่นๆ....", "00"));
        if (ddltranning.SelectedValue == "00")
        {
            pnltraning_other.Visible = true;
        }
        else
        {
            pnltraning_other.Visible = false;
            txttraning_other.Text = "";
        }
        txttraning_date.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);

        employeeM obj = new employeeM();
        _data_elearning.employeeM_action = new employeeM[1];
        obj.RDeptID = int.Parse(ViewState["RDeptID"].ToString());
        _data_elearning.employeeM_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_lu_position, _data_elearning);
        _func_dmu.zSetGridData(GvPosition, _data_elearning.employeeM_action);
        txtremark.Text = "";
        txtAmount.Text = "";
        txtQty.Text = "";
        cb_pos_other.Checked = false;
        txtpos_other.Text = "";
        txtpos_other.Visible = false;

        int RDeptID = 0;
        if (ViewState["RDeptID"].ToString() != "")
        {
            RDeptID = int.Parse(ViewState["RDeptID"].ToString());
        }

        _data_elearning.employeeM_action = new employeeM[1];
        obj.EmpIDX = 0;
        obj.RDeptID = RDeptID;
        _data_elearning.employeeM_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);
        _func_dmu.zSetGridData(GvEmp, _data_elearning.employeeM_action);

        //rdoposition.DataSource = _data_elearning.employeeM_action;
        //rdoposition.DataTextField = "PosNameTH";
        //rdoposition.DataValueField = "RPosIDX_J";
        //rdoposition.DataBind();
        //  cblpos_id.SelectedValue = _rdo_idx_detailtype_print.Text;


    }

    protected void getDatasetEmployee() // สร้าง dataset
    {
        CreateDs_dsEmployeeTable();
        CreateDs_dsEmpTraningTable();
        CreateDs_dsEmpTraningTable1();
        CreateDsel_u1_traning_req();
        CreateDsel_u2_traning_req();
        CreateDsel_u3_traning_req();
    }
    private void CreateDs_dsEmployeeTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmployee");
        ds.Tables["dsEmployee"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmployee"].Columns.Add("RPosIDX_J", typeof(String));
        ViewState["vsEmployeeList"] = ds;
    }
    private void CreateDs_dsEmpTraningTable()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning");
        ds.Tables["dsEmpTraning"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraningList"] = ds;
    }

    private void CreateDs_dsEmpTraningTable1()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsEmpTraning1");
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Position", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsEmpTraning1"].Columns.Add("Date", typeof(String));
        ViewState["vsEmpTraning1List"] = ds;
    }

    private void CreateDs_dsListData()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsListData");
        ds.Tables["dsListData"].Columns.Add("EmpIDX", typeof(String));
        ds.Tables["dsListData"].Columns.Add("DocCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("EmpCode", typeof(String));
        ds.Tables["dsListData"].Columns.Add("FullNameTH", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RDeptID", typeof(String));
        ds.Tables["dsListData"].Columns.Add("RPosIDX_J", typeof(String));
        ds.Tables["dsListData"].Columns.Add("m0_training_idx", typeof(String));
        ds.Tables["dsListData"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_typeid", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_type", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Training_want", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position_id", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Position", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Amount", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Qty", typeof(String));
        ds.Tables["dsListData"].Columns.Add("Date", typeof(String));
        ViewState["vsListData"] = ds;
    }

    private void CreateDsel_u1_traning_req()
    {
        DataSet ds = new DataSet();

        ds.Tables.Add("dsel_u1_traning_req");
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_item", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("icount", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("u1_training_req_idx", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("u0_training_req_idx_ref", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("m0_training_idx_ref", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_name", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_other", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_type_flag", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_type_name", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_needs", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_budget", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_month_study", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("u1_RPosIDX_ref", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("u1_PosIDX_name", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_qty", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_created_by", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_updated_by", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_pos_flag", typeof(String));
        ds.Tables["dsel_u1_traning_req"].Columns.Add("training_req_pos_other", typeof(String));
        ViewState["vsel_u1_traning_reqList"] = ds;
    }

    private void CreateDsel_u2_traning_req()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsel_u2_traning_req");
        ds.Tables["dsel_u2_traning_req"].Columns.Add("training_req_item_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("icount", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("m0_training_idx_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u2_training_req_idx", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u1_training_req_idx_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u0_training_req_idx_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u2_RPosIDX_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u2_EmpIDX_ref", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("u2_EmpIDX_ref_name", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("training_req_qty", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("training_req_updated_by", typeof(String));
        ds.Tables["dsel_u2_traning_req"].Columns.Add("training_req_other", typeof(String));
        ViewState["vsel_u2_traning_reqList"] = ds;
    }
    private void CreateDsel_u3_traning_req()
    {
        DataSet ds = new DataSet();
        ds.Tables.Add("dsel_u3_traning_req");
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_item_ref", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("icount", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("m0_training_idx_ref", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("u3_training_req_idx", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("u1_training_req_idx_ref", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("u0_training_req_idx_ref", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("u3_RPosIDX_ref", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("u3_PosIDX_name", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_qty", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_updated_by", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_pos_flag", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_pos_other", typeof(String));
        ds.Tables["dsel_u3_traning_req"].Columns.Add("training_req_other", typeof(String));
        ViewState["vsel_u3_traning_reqList"] = ds;
    }
    public int getViewState(string sViewState)
    {
        int iReturn = 0;

        if ((ViewState[sViewState] == "") || (ViewState[sViewState] == null))
        {

        }
        else
        {
            iReturn = int.Parse(ViewState[sViewState].ToString());
        }

        return iReturn;

    }
    private void ShowListData(string sName = "")
    {
        _data_elearning.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.filter_keyword = sName;
        obj.u0_EmpIDX_ref = emp_idx;

        obj.JobLevel = getViewState("joblevel_idx");
        obj.u0_RDeptID_ref = getViewState("rdept_idx");
        obj.zmonth = int.Parse(ddlMonthSearch_L.SelectedValue);
        obj.zyear = int.Parse(ddlYearSearch_L.SelectedValue);
        obj.approve_status = int.Parse(ddlStatusapprove_L.SelectedValue);
        obj.operation_status_id = "LIST";
        _data_elearning.el_traning_req_action[0] = obj;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, _data_elearning);

        _func_dmu.zSetGridData(GvListData, _data_elearning.el_traning_req_action);

    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    private void ClearTraning()
    {
        Select_LookupInsert();
    }
    protected void showFooter_GvTraningList()
    {
        return;
        DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];
        _func_dmu.zSetGridData(GvTraningList, dsEmpTraning.Tables["dsEmpTraning"]);
        int TotalQty = 0;
        float TotalAmount = 0;
        foreach (DataRow item in dsEmpTraning.Tables["dsEmpTraning"].Rows)
        {

            TotalQty = TotalQty + int.Parse(item["Qty"].ToString());
            TotalAmount = TotalAmount + float.Parse(item["Amount"].ToString());
        }

        GvTraningList.FooterRow.Font.Bold = true;
        GvTraningList.FooterRow.Cells[0].ColumnSpan = 4;
        GvTraningList.FooterRow.Cells.RemoveAt(1);
        GvTraningList.FooterRow.Cells.RemoveAt(1);
        GvTraningList.FooterRow.Cells.RemoveAt(1);
        GvTraningList.FooterRow.Cells[0].Text = "รวม";
        GvTraningList.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Center;
        GvTraningList.FooterRow.Cells[4].Text = TotalQty.ToString("n2");
        GvTraningList.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
        GvTraningList.FooterRow.Cells[6].Text = TotalAmount.ToString("n0");
        GvTraningList.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            var GvName = (GridView)sender;
            string cmdName = e.CommandName;
            string cmdGvName = GvName.ID;
            if (cmdGvName == "GvTraningList")
            {
                if (cmdName == "btnRemove_GvTraningList")
                {
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsel_u1_traning_req = (DataSet)ViewState["vsel_u1_traning_reqList"];
                    DataSet dsel_u2_traning_req = (DataSet)ViewState["vsel_u2_traning_reqList"];
                    DataSet dsel_u3_traning_req = (DataSet)ViewState["vsel_u3_traning_reqList"];
                    string sItem = dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows[rowIndex]["training_req_item"].ToString();
                    string sicount = dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows.Count.ToString();
                    if (sItem == "")
                    {
                        sItem = "0";
                    }
                    int item = int.Parse(sItem);

                    if (sicount == "")
                    {
                        sicount = "0";
                    }
                    int icount = int.Parse(sicount);

                    if (icount > 0)
                    {
                        for (int i = 1; i <= icount; i++)
                        {
                            // DataSet dsel_u2_traning_req = (DataSet)ViewState["vsel_u2_traning_reqList"];
                            int r_Index = 0, iRowAll = 0;
                            iRowAll = dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows.Count;
                            for (int iRow = 1; iRow <= iRowAll; iRow++)
                            {
                                string sItem1 = dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows[iRow - 1]["training_req_item_ref"].ToString();
                                if (sItem1 == "")
                                {
                                    sItem1 = "0";
                                }
                                int item1 = int.Parse(sItem1);
                                if (item1 == item)
                                {
                                    dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows[iRow - 1].Delete();
                                    dsel_u2_traning_req.AcceptChanges();
                                    iRow = iRowAll + 99;
                                }
                            }


                        }
                        DataSet dsel_u2_traning_req_1 = (DataSet)ViewState["vsel_u2_traning_reqList"];
                        // _func_dmu.zSetGridData(Gvel_u2_traning_req, dsel_u2_traning_req_1.Tables["dsel_u2_traning_req"]);
                    }

                    sicount = dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows.Count.ToString();
                    if (sItem == "")
                    {
                        sItem = "0";
                    }
                    item = int.Parse(sItem);

                    if (sicount == "")
                    {
                        sicount = "0";
                    }
                    icount = int.Parse(sicount);

                    if (icount > 0)
                    {
                        for (int i = 1; i <= icount; i++)
                        {
                            //  DataSet dsel_u3_traning_req = (DataSet)ViewState["vsel_u3_traning_reqList"];
                            int r_Index = 0, iRowAll = 0;
                            iRowAll = dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows.Count;
                            for (int iRow = 1; iRow <= iRowAll; iRow++)
                            {
                                string sItem1 = dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows[iRow - 1]["training_req_item_ref"].ToString();
                                if (sItem1 == "")
                                {
                                    sItem1 = "0";
                                }
                                int item1 = int.Parse(sItem1);
                                if (item1 == item)
                                {
                                    dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows[iRow - 1].Delete();
                                    dsel_u3_traning_req.AcceptChanges();
                                    iRow = iRowAll + 99;
                                }
                            }


                        }
                        // DataSet dsel_u3_traning_req = (DataSet)ViewState["vsel_u3_traning_reqList"];
                        // _func_dmu.zSetGridData(Gvel_u3_traning_req, dsel_u3_traning_req.Tables["dsel_u3_traning_req"]);
                    }


                    dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows[rowIndex].Delete();
                    dsel_u1_traning_req.AcceptChanges();
                    _func_dmu.zSetGridData(GvTraningList, dsel_u1_traning_req.Tables["dsel_u1_traning_req"]);

                }
            }
            else if (cmdGvName == "GvListData")
            {

            }


        }
    }
    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        //var chkName = (CheckBoxList)sender;
        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":
                    CheckBox GvPosition_cb_RPosIDX_J = (CheckBox)sender;
                    GridViewRow gr = (GridViewRow)GvPosition_cb_RPosIDX_J.NamingContainer;
                    Label GvPosition_lb_RPosIDX_J = (Label)gr.FindControl("GvPosition_lb_RPosIDX_J");
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('"+ GvPosition_lb_RPosIDX_J.Text + "');", true);
                    int item = 0;
                    foreach (GridViewRow gr_item in GvEmp.Rows)
                    {
                        CheckBox GvEmp_cb_EmpIDX = (CheckBox)gr_item.FindControl("GvEmp_cb_EmpIDX");
                        Label GvEmp_lb_RPosIDX_J = (Label)gr_item.FindControl("GvEmp_lb_RPosIDX_J");

                        if (int.Parse(GvPosition_lb_RPosIDX_J.Text) == int.Parse(GvEmp_lb_RPosIDX_J.Text))
                        {
                            GvEmp_cb_EmpIDX.Checked = GvPosition_cb_RPosIDX_J.Checked;
                        }
                        if (GvEmp_cb_EmpIDX.Checked == true)
                        {
                            item++;
                        }

                    }
                    txtQty.Text = item.ToString();
                    break;

                case "GvEmp_cb_EmpIDX":

                    int item1 = 0;
                    foreach (GridViewRow gr_item in GvEmp.Rows)
                    {
                        CheckBox GvEmp_cb_EmpIDX = (CheckBox)gr_item.FindControl("GvEmp_cb_EmpIDX");
                        if (GvEmp_cb_EmpIDX.Checked == true)
                        {
                            item1++;
                        }

                    }
                    txtQty.Text = item1.ToString();
                    break;
                case "cb_pos_other":
                    if (cb_pos_other.Checked)
                    {
                        txtpos_other.Visible = true;
                    }
                    else
                    {
                        txtpos_other.Text = "";
                        txtpos_other.Visible = false;
                    }
                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[7].FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[7].FindControl("btnDelete_GvListData");
                TextBox txtapprove_status_GvListData = (TextBox)e.Row.Cells[7].FindControl("txtapprove_status_GvListData");
                TextBox txttraining_req_created_by_GvListData = (TextBox)e.Row.Cells[7].FindControl("txttraining_req_created_by_GvListData");


                Boolean bBl = true;
                if (txtapprove_status_GvListData.Text == "4") // 4	อนุมัติ
                {
                    bBl = false;
                }
                else if (txtapprove_status_GvListData.Text == "6")
                {
                    bBl = false;
                }
                if (txttraining_req_created_by_GvListData.Text != emp_idx.ToString())
                {
                    bBl = false;
                }
                btnUpdate_GvListData.Visible = bBl;
                btnDelete_GvListData.Visible = bBl;
            }
        }
        else if (sGvName == "GvApproveList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnApproveDetail = (LinkButton)e.Row.Cells[9].FindControl("btnApproveDetail");
                LinkButton btnApproveConDetail = (LinkButton)e.Row.Cells[9].FindControl("btnApproveConDetail");
                TextBox txtu0_training_req_idx = (TextBox)e.Row.Cells[9].FindControl("txtu0_training_req_idx_GvAppL");
                TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvAppL");

                if (txtapprove_status.Text == "4")//อนุมัติ
                {
                    btnApproveDetail.Visible = false;
                    btnApproveConDetail.Visible = true;
                }
                else
                {
                    btnApproveDetail.Visible = true;
                    btnApproveConDetail.Visible = false;
                }
            }

        }


    }
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTraningList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                break;
            case "GvListData":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListData(txtFilterKeyword.Text);
                SETFOCUS.Focus();
                break;
            case "GvApproveList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                zShowdataAppList(_func_dmu.zStringToInt(Hddfld_ShowdataAppList.Value));
                SETFOCUS.Focus();
                break;

        }
    }
    #endregion Action

    public void setdataApproveList()
    {
        ddlMonthSearch_A.SelectedValue = (int.Parse(DateTime.Now.ToString("MM", CultureInfo.InstalledUICulture))).ToString();
        _func_dmu.zDropDownList(ddlYearSearch_A,
                            "-",
                            "zyear",
                            "zyear",
                            "0",
                            "trainingLoolup",
                            DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                            "TRN-YEAR"
                            );
    }

    #region btnCommand

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_training_idx;
        int _cemp_idx;

        m0_training objM0_ProductType = new m0_training();

        switch (cmdName)
        {
            case "btnListData":
                zSetMode(2);
                break;
            case "btnInsert":
                zSetMode(0);
                Hddfld_training_req_no.Value = "";
                Hddfld_u0_training_req_idx.Value = "0";
                btnSaveInsert.Visible = true;
                btnSaveUpdate.Visible = false;
                Panel plndocno = (Panel)FvDetailUser.FindControl("plndocno");
                plndocno.Visible = false;
                break;
            case "btnSaveUpdate":

                if (zCheckErrorSave() == false)
                {
                    if (Hddfld_training_req_no.Value == "")
                    {
                        zSetMode(2);
                    }
                    else
                    {
                        zSaveUpdate(int.Parse(Hddfld_u0_training_req_idx.Value));


                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }

                break;
            case "btnApproveDetail":
                zSetMode(3);
                _m0_training_idx = int.Parse(cmdArg);
                Select_DetailListApp(_m0_training_idx);
                zShowdataAppDetail(_m0_training_idx);
                Hddfld_u0_training_req_idx.Value = cmdArg;
                Hddfld_status.Value = "APPROVELIST";
                SETFOCUS.Focus();
                break;
            case "btnApproveConDetail":
                zSetMode(7);//3
                _m0_training_idx = int.Parse(cmdArg);
                Select_DetailListApp(_m0_training_idx);
                zShowdataAppDetail(_m0_training_idx);
                Hddfld_u0_training_req_idx.Value = cmdArg;
                Hddfld_status.Value = "APPROVECONLIST";
                SETFOCUS.Focus();
                break;
            case "btnDetail":
                zSetMode(6);
                _m0_training_idx = int.Parse(cmdArg);
                Select_DetailListApp(_m0_training_idx);
                zShowdataAppDetail(_m0_training_idx);
                Hddfld_u0_training_req_idx.Value = cmdArg;
                Hddfld_status.Value = "LIST";
                SETFOCUS.Focus();
                break;
            case "btnApproveList":
                zSetMode(4);
                setdataApproveList();
                zShowdataAppList(0);
                getLabelTitleAppList("L");
                Hddfld_status.Value = "APPROVELIST";
                btnupdateApproveCon.Visible = false;
                btnupdateApprove.Visible = true;
                SETFOCUS.Focus();
                break;
            case "btnApproveCom":
                zSetMode(5);
                setdataApproveList();
                zShowdataAppList(4);
                getLabelTitleAppList("A");
                Hddfld_status.Value = "APPROVECONLIST";
                btnupdateApproveCon.Visible = true;
                btnupdateApprove.Visible = false;
                break;
            case "btnupdateApprove":
                if (Hddfld_u0_training_req_idx.Value == "")
                {
                    zSetMode(4);
                    zShowdataAppList(0);
                    getLabelTitleAppList("L");
                }
                else
                {
                    zSaveApprove(int.Parse(Hddfld_u0_training_req_idx.Value));
                    zSetMode(4);
                    zShowdataAppList(0);
                    getLabelTitleAppList("L");
                }
                break;
            case "btnupdateApproveCon":
                if (Hddfld_u0_training_req_idx.Value == "")
                {
                    zSetMode(5);
                    zShowdataAppList(0);
                    getLabelTitleAppList("L");
                }
                else
                {
                    zSaveApprove(int.Parse(Hddfld_u0_training_req_idx.Value));
                    zSetMode(5);
                    zShowdataAppList(0);
                    getLabelTitleAppList("L");
                }
                break;
            case "btnUpdate_GvListData":
                _m0_training_idx = int.Parse(cmdArg);
                if (_m0_training_idx > 0)
                {

                    zSetMode(0);
                    zShowdataUpdate(_m0_training_idx);
                    btnSaveInsert.Visible = false;
                    btnSaveUpdate.Visible = true;
                }
                if (Hddfld_training_req_no.Value == "")
                {
                    zSetMode(2);
                }
                SETFOCUS.Focus();
                break;
            case "btnCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnSaveInsert":
                if (zCheckErrorSave() == false)
                {
                    zSaveInsert();

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }

                break;
            case "btnDelete":

                _m0_training_idx = int.Parse(cmdArg);
                zDelete(_m0_training_idx);
                ShowListData();
                break;
            case "btnFilter":
                ShowListData(txtFilterKeyword.Text);
                break;

            case "btnAdd":
                if (zCheckError() == false)
                {
                    addTraning();
                    ClearTraning();
                }
                break;
            case "btnClear":
                ClearTraning();
                break;

            case "btnBack":
                //HiddenField hdf_approve_status = (HiddenField)FvDetailUserApp.FindControl("hdf_approve_status");
                if (Hddfld_status.Value == "APPROVELIST")
                {
                    zSetMode(4);
                    zShowdataAppList(0);
                    getLabelTitleAppList("L");
                }
                else if (Hddfld_status.Value == "APPROVECONLIST")
                {
                    zSetMode(5);
                    zShowdataAppList(4);
                    getLabelTitleAppList("A");
                }
                else
                {
                    zSetMode(2);
                }
                break;
            case "btnFilter_A":
                if (Hddfld_status.Value == "APPROVELIST")
                {
                    zShowdataAppList(0);
                }
                else
                {
                    zShowdataAppList(4);
                }
                break;


        }
    }
    #endregion btnCommand

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            var FvName = (FormView)sender;
            switch (FvName.ID)
            {
                case "fvCRUD":
                    //if (FvName.CurrentMode == FormViewMode.ReadOnly) { }
                    //if (FvName.CurrentMode == FormViewMode.Edit) { }
                    //if (FvName.CurrentMode == FormViewMode.Insert) { }
                    break;
            }
        }
        else if (sender is DropDownList)
        {
            var FvName = (DropDownList)sender;
            switch (FvName.ID)
            {
                case "ddltranning":
                    if (ddltranning.SelectedValue == "00")
                    {
                        pnltraning_other.Visible = true;
                    }
                    else
                    {
                        pnltraning_other.Visible = false;
                        txttraning_other.Text = "";
                    }
                    break;
            }
        }
    }

    #endregion FvDetail_DataBound

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    private Boolean zCheckError()
    {
        Boolean Error = false;
        string sErrorName;

        if (ddltranning.SelectedValue == "00")
        {
            if (txttraning_other.Text == "")
            {
                Error = true;
                sErrorName = "หลักสูตรอื่นๆ";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก" + sErrorName + "');", true);
            }
        }
        else
        {
            if ((ddltranning.SelectedValue == "") || (ddltranning.SelectedValue == "0"))
            {
                Error = true;
                sErrorName = "หลักสูตร";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก" + sErrorName + "');", true);
            }
        }

        DataSet dsel_u1_traning_reqList = (DataSet)ViewState["vsel_u1_traning_reqList"];
        int itemObj = 0, icount = 0;
        traning_req[] objIBomu1 = new traning_req[dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows.Count];
        foreach (DataRow dru1 in dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows)
        {
            // itemObj = 0;
            if (ddltranning.SelectedValue == "00")
            {
                if (dru1["training_req_other"].ToString().Trim() == txttraning_other.Text.Trim())
                {
                    itemObj++;
                }
            }
            else
            {
                if (dru1["m0_training_idx_ref"].ToString() == ddltranning.SelectedValue)
                {
                    itemObj++;
                }
            }

        }
        if (itemObj > 0)
        {
            Error = true;
            sErrorName = "หลักสูตรใหม่เนื่องจากมีหลักสูตรนี้แล้ว";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก" + sErrorName + "');", true);
        }

        int item = 0;
        foreach (GridViewRow gr_item in GvPosition.Rows)
        {
            CheckBox GvPosition_cb_RPosIDX_J = (CheckBox)gr_item.FindControl("GvPosition_cb_RPosIDX_J");

            if (GvPosition_cb_RPosIDX_J.Checked == true)
            {
                item++;
            }

        }
        if ((cb_pos_other.Checked == true) && (txtpos_other.Text != ""))
        {
            item++;
        }
        if (item == 0)
        {
            Error = true;
            sErrorName = "ตำแหน่งที่ต้องการอบรม";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก" + sErrorName + "');", true);
        }

        return Error;
    }


    private Boolean zCheckErrorSave()
    {
        Boolean Error = false;
        string sErrorName;

        int item = GvTraningList.Rows.Count;
        if (item == 0)
        {
            Error = true;
            sErrorName = "ข้อมูลที่ขออบรม";
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอก" + sErrorName + "');", true);
        }

        return Error;
    }

    private void zSaveInsert()
    {
        int u0_training_req_idx = 0;
        traning_req obj_traning_req = new traning_req();
        //traning_req U1
        TextBox txtu0_training_req_date = (TextBox)FvDetailUser.FindControl("txtu0_training_req_date");
        Label LbSecIDX = (Label)FvDetailUser.FindControl("LbSecIDX");
        Label LbRDeptIDX = (Label)FvDetailUser.FindControl("LbRDeptIDX");
        Label LbPosIDX = (Label)FvDetailUser.FindControl("LbPosIDX");

        _data_elearning.el_traning_req_action = new traning_req[1];
        //obj_traning_req.training_req_no = _func_dmu.zRun_Number(_FromTrnNeedsSurRunNo, "", "YYMM", "N", "0000");

        obj_traning_req.u0_training_req_date = _func_dmu.zDateToDB(txtu0_training_req_date.Text);

        obj_traning_req.u0_RSecID_ref = int.Parse(LbSecIDX.Text);
        obj_traning_req.u0_RDeptID_ref = int.Parse(LbRDeptIDX.Text);
        obj_traning_req.u0_RPosIDX_ref = int.Parse(LbPosIDX.Text);
        obj_traning_req.u0_EmpIDX_ref = emp_idx;
        obj_traning_req.training_req_status = int.Parse(ddltraining_req_status.SelectedValue);
        obj_traning_req.training_req_created_by = emp_idx;
        obj_traning_req.training_req_updated_by = emp_idx;
        //node
        obj_traning_req.approve_status = 0;
        obj_traning_req.u0_idx = 1;
        obj_traning_req.node_idx = 1;
        obj_traning_req.actor_idx = 2;
        obj_traning_req.app_flag = 0;
        obj_traning_req.app_user = emp_idx;

        _data_elearning.el_traning_req_action[0] = obj_traning_req;



        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u0_training_req, _data_elearning);

        if (_data_elearning.return_code > 0)
        {
            u0_training_req_idx = _data_elearning.return_code;
            if (u0_training_req_idx > 0)
            {
                DataSet dsel_u1_traning_reqList = (DataSet)ViewState["vsel_u1_traning_reqList"];
                int itemObj = 0, icount = 0;
                // traning_req[] objIBomu1 = new traning_req[1];
                traning_req[] objIBomu1 = new traning_req[dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows.Count];
                foreach (DataRow dru1 in dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows)
                {
                    // itemObj = 0;

                    objIBomu1[itemObj] = new traning_req();
                    objIBomu1[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu1[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru1["m0_training_idx_ref"].ToString());
                    if (int.Parse(dru1["m0_training_idx_ref"].ToString()) == 0)
                    {
                        objIBomu1[itemObj].training_req_other = dru1["training_req_other"].ToString();
                    }
                    else
                    {
                        objIBomu1[itemObj].training_req_other = "";
                    }
                    objIBomu1[itemObj].training_req_type_flag = _func_dmu.zStringToInt(dru1["training_req_type_flag"].ToString());
                    objIBomu1[itemObj].training_req_needs = dru1["training_req_needs"].ToString();
                    objIBomu1[itemObj].training_req_budget = decimal.Parse(dru1["training_req_budget"].ToString());
                    objIBomu1[itemObj].training_req_month_study = _func_dmu.zDateToDB(dru1["training_req_month_study"].ToString());
                    objIBomu1[itemObj].u1_RPosIDX_ref = _func_dmu.zStringToInt(dru1["u1_RPosIDX_ref"].ToString());
                    objIBomu1[itemObj].training_req_qty = _func_dmu.zStringToInt(dru1["training_req_qty"].ToString());
                    objIBomu1[itemObj].training_req_created_by = emp_idx;
                    objIBomu1[itemObj].training_req_updated_by = emp_idx;
                    objIBomu1[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu1[itemObj].training_req_item = _func_dmu.zStringToInt(dru1["training_req_item"].ToString());
                    objIBomu1[itemObj].training_req_pos_flag = _func_dmu.zStringToInt(dru1["training_req_pos_flag"].ToString());
                    objIBomu1[itemObj].training_req_pos_other = dru1["training_req_pos_other"].ToString();

                    itemObj++;
                    icount++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu1;
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u1_training_req, _data_elearning);
                }

                DataSet dsel_u2_traning_reqList = (DataSet)ViewState["vsel_u2_traning_reqList"];
                itemObj = 0;
                // traning_req[] objIBomu_u2 = new traning_req[1];
                traning_req[] objIBomu_u2 = new traning_req[dsel_u2_traning_reqList.Tables["dsel_u2_traning_req"].Rows.Count];
                foreach (DataRow dru2 in dsel_u2_traning_reqList.Tables["dsel_u2_traning_req"].Rows)
                {
                    //itemObj = 0;

                    objIBomu_u2[itemObj] = new traning_req();
                    objIBomu_u2[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu_u2[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru2["m0_training_idx_ref"].ToString());
                    objIBomu_u2[itemObj].u2_RPosIDX_ref = _func_dmu.zStringToInt(dru2["u2_RPosIDX_ref"].ToString());
                    objIBomu_u2[itemObj].u2_EmpIDX_ref = _func_dmu.zStringToInt(dru2["u2_EmpIDX_ref"].ToString());
                    objIBomu_u2[itemObj].training_req_updated_by = emp_idx;
                    objIBomu_u2[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu_u2[itemObj].training_req_item_ref = _func_dmu.zStringToInt(dru2["training_req_item_ref"].ToString());
                    objIBomu_u2[itemObj].training_req_other = dru2["training_req_other"].ToString();

                    itemObj++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu_u2;
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u2_training_req, _data_elearning);
                }

                //u3
                DataSet dsel_u3_traning_reqList = (DataSet)ViewState["vsel_u3_traning_reqList"];
                itemObj = 0;
                // traning_req[] objIBomu_u3 = new traning_req[1];
                traning_req[] objIBomu_u3 = new traning_req[dsel_u3_traning_reqList.Tables["dsel_u3_traning_req"].Rows.Count];
                foreach (DataRow dru3 in dsel_u3_traning_reqList.Tables["dsel_u3_traning_req"].Rows)
                {
                    //itemObj = 0;

                    objIBomu_u3[itemObj] = new traning_req();
                    objIBomu_u3[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu_u3[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru3["m0_training_idx_ref"].ToString());
                    objIBomu_u3[itemObj].u3_RPosIDX_ref = _func_dmu.zStringToInt(dru3["u3_RPosIDX_ref"].ToString());
                    objIBomu_u3[itemObj].training_req_updated_by = emp_idx;
                    objIBomu_u3[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu_u3[itemObj].training_req_item_ref = _func_dmu.zStringToInt(dru3["training_req_item_ref"].ToString());
                    objIBomu_u3[itemObj].training_req_other = dru3["training_req_other"].ToString();
                    objIBomu_u3[itemObj].training_req_pos_flag = _func_dmu.zStringToInt(dru3["training_req_pos_flag"].ToString());
                    objIBomu_u3[itemObj].training_req_pos_other = dru3["training_req_pos_other"].ToString();

                    itemObj++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu_u3;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u3_training_req, _data_elearning);
                }


                if (ddltraining_req_status.SelectedValue == "1")
                {
                    sendEmail(u0_training_req_idx);
                }



            }
        }
    }
    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        traning_req obj_traning_req = new traning_req();
        _data_elearning.el_traning_req_action = new traning_req[1];
        obj_traning_req.u0_training_req_idx = id;
        obj_traning_req.training_req_updated_by = emp_idx;
        _data_elearning.el_traning_req_action[0] = obj_traning_req;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u0_training_req, _data_elearning);
    }
    private void zDeleteU1U2(int id)
    {
        if (id == 0)
        {
            return;
        }
        traning_req obj_traning_req = new traning_req();
        _data_elearning.el_traning_req_action = new traning_req[1];
        obj_traning_req.u0_training_req_idx = id;
        obj_traning_req.training_req_updated_by = emp_idx;
        _data_elearning.el_traning_req_action[0] = obj_traning_req;
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u1u2_training_req, _data_elearning);
    }

    private void addTraning()
    {
        int ic = 0, icount = 0;
        foreach (var item in GvPosition.Rows)
        {
            CheckBox GvPosition_cb_RPosIDX_J = (CheckBox)GvPosition.Rows[ic].FindControl("GvPosition_cb_RPosIDX_J");
            if (GvPosition_cb_RPosIDX_J.Checked == true)
            {
                icount++;
            }

            ic++;
        }
        if ((cb_pos_other.Checked == true) && (txtpos_other.Text != ""))
        {
            icount++;
        }
        if (icount > 0)
        {
            /*
            DataSet dsEmpTraning = (DataSet)ViewState["vsEmpTraningList"];
            DataRow drEmpTraning;
            */

            DataSet Dsel_u1_traning_req = (DataSet)ViewState["vsel_u1_traning_reqList"];
            DataRow drel_u1_traning_req;
            int irow = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows.Count;
            if (irow > 0)
            {
                irow = irow - 1;
                DataRow dataRow = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows[irow];
                if (dataRow["training_req_item"].ToString() != "")
                {
                    irow = int.Parse(dataRow["training_req_item"].ToString());
                }
                else
                {
                    irow = 0;
                }


            }
            else
            {
                irow = 0;
            }
            //****//
            irow++;
            drel_u1_traning_req = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].NewRow();
            drel_u1_traning_req["training_req_item"] = irow.ToString();
            if (ddltranning.SelectedValue != "00")
            {
                drel_u1_traning_req["m0_training_idx_ref"] = ddltranning.SelectedValue;
                drel_u1_traning_req["training_name"] = ddltranning.Items[ddltranning.SelectedIndex].ToString();
                drel_u1_traning_req["training_req_other"] = "";

            }
            else
            {
                drel_u1_traning_req["m0_training_idx_ref"] = "0";
                drel_u1_traning_req["training_name"] = txttraning_other.Text;
                drel_u1_traning_req["training_req_other"] = txttraning_other.Text;
            }
            drel_u1_traning_req["training_req_type_flag"] = rdltranning_type.SelectedValue;
            drel_u1_traning_req["training_req_type_name"] = rdltranning_type.Items[int.Parse(rdltranning_type.SelectedValue)].ToString();
            drel_u1_traning_req["training_req_budget"] = _func_dmu.zFormatfloat(txtAmount.Text.Trim(), 2);
            if (txtremark.Text.Trim().Length > 150)
            {
                drel_u1_traning_req["training_req_needs"] = txtremark.Text.Trim().Substring(0, 150);
            }
            else
            {
                drel_u1_traning_req["training_req_needs"] = txtremark.Text.Trim();
            }
            drel_u1_traning_req["training_req_qty"] = _func_dmu.zFormatfloat(txtQty.Text.Trim(), 0);
            drel_u1_traning_req["training_req_month_study"] = txttraning_date.Text;
            // drel_u1_traning_req["icount"] = icount_1.ToString();
            Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows.Add(drel_u1_traning_req);
            ViewState["vsel_u1_traning_reqList"] = Dsel_u1_traning_req;
            _func_dmu.zSetGridData(GvTraningList, Dsel_u1_traning_req.Tables["dsel_u1_traning_req"]);

            //****//
            DataSet Dsel_u3_traning_req = (DataSet)ViewState["vsel_u3_traning_reqList"];
            DataRow drel_u3_traning_req;
            ic = 0;
            foreach (var item in GvPosition.Rows)
            {
                CheckBox GvPosition_cb_RPosIDX_J = (CheckBox)GvPosition.Rows[ic].FindControl("GvPosition_cb_RPosIDX_J");
                if (GvPosition_cb_RPosIDX_J.Checked == true)
                {

                    // irow++;
                    drel_u3_traning_req = Dsel_u3_traning_req.Tables["dsel_u3_traning_req"].NewRow();
                    drel_u3_traning_req["training_req_item_ref"] = irow.ToString();
                    if (ddltranning.SelectedValue != "00")
                    {
                        drel_u3_traning_req["m0_training_idx_ref"] = ddltranning.SelectedValue;
                        drel_u3_traning_req["training_req_other"] = "";

                    }
                    else
                    {
                        drel_u3_traning_req["m0_training_idx_ref"] = "0";
                        drel_u3_traning_req["training_req_other"] = txttraning_other.Text;
                    }
                    
                    Label GvPosition_lb_RPosIDX_J = (Label)GvPosition.Rows[ic].FindControl("GvPosition_lb_RPosIDX_J");
                    Label GvPosition_lbPosName = (Label)GvPosition.Rows[ic].FindControl("GvPosition_lbPosName");
                    drel_u3_traning_req["u3_RPosIDX_ref"] = GvPosition_lb_RPosIDX_J.Text;
                    drel_u3_traning_req["u3_PosIDX_name"] = GvPosition_lbPosName.Text;

                    int _int = 0;
                    string _string = "";
                    _int = _func_dmu.zBooleanToInt(cb_pos_other.Checked);
                    if (_int > 0)
                    {
                        _string = txtpos_other.Text;
                    }
                    drel_u3_traning_req["training_req_pos_flag"] = _int.ToString();
                    drel_u3_traning_req["training_req_pos_other"] = _string;


                    DataSet Dsel_u2_traning_req = (DataSet)ViewState["vsel_u2_traning_reqList"];
                    DataRow drel_u2_traning_req;
                    int ic_u2 = 0, icount_1 = 0;
                    foreach (var item_u2 in GvEmp.Rows)
                    {
                        CheckBox GvEmp_cb_EmpIDX = (CheckBox)GvEmp.Rows[ic_u2].FindControl("GvEmp_cb_EmpIDX");
                        if (GvEmp_cb_EmpIDX.Checked == true)
                        {
                            Label GvEmp_lb_EmpIDX = (Label)GvEmp.Rows[ic_u2].FindControl("GvEmp_lb_EmpIDX");
                            Label GvEmp_lb_RPosIDX_J = (Label)GvEmp.Rows[ic_u2].FindControl("GvEmp_lb_RPosIDX_J");
                            Label GvEmp_lb_FullNameTH = (Label)GvEmp.Rows[ic_u2].FindControl("GvEmp_lb_FullNameTH");

                            drel_u2_traning_req = Dsel_u2_traning_req.Tables["dsel_u2_traning_req"].NewRow();
                            drel_u2_traning_req["training_req_item_ref"] = irow.ToString();
                            drel_u2_traning_req["m0_training_idx_ref"] = ddltranning.SelectedValue;
                            drel_u2_traning_req["u2_RPosIDX_ref"] = GvEmp_lb_RPosIDX_J.Text;
                            drel_u2_traning_req["u2_EmpIDX_ref"] = GvEmp_lb_EmpIDX.Text;
                            drel_u2_traning_req["u2_EmpIDX_ref_name"] = GvEmp_lb_FullNameTH.Text;
                            drel_u2_traning_req["training_req_other"] = txttraning_other.Text;
                            Dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows.Add(drel_u2_traning_req);
                            ViewState["vsel_u2_traning_reqList"] = Dsel_u2_traning_req;
                            icount_1++;

                        }

                        ic_u2++;
                    }
                    drel_u3_traning_req["icount"] = icount_1.ToString();
                    Dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows.Add(drel_u3_traning_req);

                    // _func_dmu.zSetGridData(Gvel_u2_traning_req, Dsel_u2_traning_req.Tables["dsel_u2_traning_req"]);

                }
                ic++;
            }
            ViewState["vsel_u3_traning_reqList"] = Dsel_u3_traning_req;
            //_func_dmu.zSetGridData(Gvel_u3_traning_req, Dsel_u3_traning_req.Tables["dsel_u3_traning_req"]);
            showFooter_GvTraningList();
        }
        if ((cb_pos_other.Checked == true) && (txtpos_other.Text == "") && (icount == 0))
        {
            DataSet Dsel_u1_traning_req = (DataSet)ViewState["vsel_u1_traning_reqList"];
            DataRow drel_u1_traning_req;
            int irow = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows.Count;
            drel_u1_traning_req = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].NewRow();
            drel_u1_traning_req["training_req_item"] = irow.ToString();
            if (ddltranning.SelectedValue != "00")
            {
                drel_u1_traning_req["m0_training_idx_ref"] = ddltranning.SelectedValue;
                drel_u1_traning_req["training_name"] = ddltranning.Items[ddltranning.SelectedIndex].ToString();
                drel_u1_traning_req["training_req_other"] = "";

            }
            else
            {
                drel_u1_traning_req["m0_training_idx_ref"] = "0";
                drel_u1_traning_req["training_name"] = txttraning_other.Text;
                drel_u1_traning_req["training_req_other"] = txttraning_other.Text;
            }
            drel_u1_traning_req["training_req_type_flag"] = rdltranning_type.SelectedValue;
            drel_u1_traning_req["training_req_type_name"] = rdltranning_type.Items[int.Parse(rdltranning_type.SelectedValue)].ToString();
            drel_u1_traning_req["training_req_budget"] = _func_dmu.zFormatfloat(txtAmount.Text.Trim(), 2);
            if (txtremark.Text.Trim().Length > 150)
            {
                drel_u1_traning_req["training_req_needs"] = txtremark.Text.Trim().Substring(0, 150);
            }
            else
            {
                drel_u1_traning_req["training_req_needs"] = txtremark.Text.Trim();
            }
            drel_u1_traning_req["training_req_qty"] = _func_dmu.zFormatfloat(txtQty.Text.Trim(), 0);
            drel_u1_traning_req["training_req_month_study"] = txttraning_date.Text;

            int _int = 0;
            string _string = "";
            _int = _func_dmu.zBooleanToInt(cb_pos_other.Checked);
            if (_int > 0)
            {
                _string = txtpos_other.Text;
            }
            drel_u1_traning_req["training_req_pos_flag"] = _int.ToString();
            drel_u1_traning_req["training_req_pos_other"] = _string;
            if (_string != "")
            {
                drel_u1_traning_req["u1_PosIDX_name"] = _string;
            }
            drel_u1_traning_req["icount"] = "0";
            Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows.Add(drel_u1_traning_req);
            ViewState["vsel_u1_traning_reqList"] = Dsel_u1_traning_req;
            _func_dmu.zSetGridData(GvTraningList, Dsel_u1_traning_req.Tables["dsel_u1_traning_req"]);
            showFooter_GvTraningList();
        }

    }
    private void zShowdataUpdate(int id)
    {
        TextBox txtu0_training_req_date = (TextBox)FvDetailUser.FindControl("txtu0_training_req_date");
        TextBox txttraining_req_no = (TextBox)FvDetailUser.FindControl("txttraining_req_no");
        data_elearning dt_el_u0 = new data_elearning();
        dt_el_u0.el_traning_req_action = new traning_req[1];
        traning_req obj = new traning_req();
        obj.u0_EmpIDX_ref = emp_idx;
        obj.operation_status_id = "E-U0";
        obj.u0_training_req_idx = id;
        dt_el_u0.el_traning_req_action[0] = obj;
        dt_el_u0 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u0);
        if (dt_el_u0.el_traning_req_action != null)
        {
            foreach (var u0 in dt_el_u0.el_traning_req_action)
            {
                Hddfld_training_req_no.Value = u0.training_req_no;
                txttraining_req_no.Text = Hddfld_training_req_no.Value;
                Hddfld_u0_training_req_idx.Value = u0.u0_training_req_idx.ToString();
                txtu0_training_req_date.Text = u0.u0_training_req_date;
                ddltraining_req_status.SelectedValue = u0.training_req_status.ToString();
                //U1
                data_elearning dt_el_u1 = new data_elearning();
                dt_el_u1.el_traning_req_action = new traning_req[1];
                traning_req obj_u1 = new traning_req();
                obj_u1.u0_EmpIDX_ref = emp_idx;
                obj_u1.operation_status_id = "E-U1";
                obj_u1.u0_training_req_idx = id;
                dt_el_u1.el_traning_req_action[0] = obj_u1;
                dt_el_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u1);
                if (dt_el_u1.el_traning_req_action != null)
                {
                    //u1
                    DataSet Dsel_u1_traning_req = (DataSet)ViewState["vsel_u1_traning_reqList"];
                    DataRow drel_u1_traning_req;
                    foreach (var u1 in dt_el_u1.el_traning_req_action)
                    {

                        drel_u1_traning_req = Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].NewRow();
                        drel_u1_traning_req["training_req_item"] = u1.training_req_item.ToString();
                        drel_u1_traning_req["m0_training_idx_ref"] = u1.m0_training_idx_ref.ToString();
                        drel_u1_traning_req["training_name"] = u1.training_name;
                        if ((u1.training_name == "") || (u1.training_name == null))
                        {
                            drel_u1_traning_req["training_name"] = u1.training_req_other;
                        }
                        drel_u1_traning_req["training_req_other"] = u1.training_req_other;
                        drel_u1_traning_req["training_req_type_flag"] = u1.training_req_type_flag.ToString();
                        drel_u1_traning_req["training_req_type_name"] = rdltranning_type.Items[int.Parse(u1.training_req_type_flag.ToString())].ToString();
                        drel_u1_traning_req["training_req_budget"] = _func_dmu.zFormatfloat(u1.training_req_budget.ToString(), 2);
                        drel_u1_traning_req["training_req_needs"] = u1.training_req_needs;
                        drel_u1_traning_req["training_req_qty"] = _func_dmu.zFormatfloat(u1.training_req_qty.ToString(), 0);
                        drel_u1_traning_req["training_req_month_study"] = u1.training_req_month_study;
                        drel_u1_traning_req["u1_RPosIDX_ref"] = u1.u1_RPosIDX_ref.ToString();
                        drel_u1_traning_req["u1_PosIDX_name"] = u1.pos_name_th;
                        drel_u1_traning_req["training_req_pos_flag"] = u1.training_req_pos_flag.ToString();
                        drel_u1_traning_req["training_req_pos_other"] = u1.training_req_pos_other;


                        data_elearning dt_el_u2 = new data_elearning();
                        dt_el_u2.el_traning_req_action = new traning_req[1];
                        traning_req obj_u2 = new traning_req();
                        obj_u2.u0_EmpIDX_ref = emp_idx;
                        obj_u2.operation_status_id = "E-U2";
                        obj_u2.u0_training_req_idx = id;
                        obj_u2.training_req_item = u1.training_req_item;
                        dt_el_u2.el_traning_req_action[0] = obj_u2;
                        dt_el_u2 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u2);
                        int icount_1 = 0;
                        DataSet Dsel_u2_traning_req = (DataSet)ViewState["vsel_u2_traning_reqList"];
                        DataRow drel_u2_traning_req;
                        if (dt_el_u2.el_traning_req_action != null)
                        {

                            foreach (var u2 in dt_el_u2.el_traning_req_action)
                            {
                                drel_u2_traning_req = Dsel_u2_traning_req.Tables["dsel_u2_traning_req"].NewRow();
                                drel_u2_traning_req["training_req_item_ref"] = u2.training_req_item_ref.ToString();
                                drel_u2_traning_req["m0_training_idx_ref"] = u2.m0_training_idx_ref.ToString();
                                drel_u2_traning_req["u2_RPosIDX_ref"] = u2.u2_RPosIDX_ref.ToString();
                                drel_u2_traning_req["u2_EmpIDX_ref"] = u2.u2_EmpIDX_ref.ToString();
                                drel_u2_traning_req["u2_EmpIDX_ref_name"] = "";// u2.training_req_item_ref.ToString();
                                drel_u2_traning_req["training_req_other"] = u2.training_req_other;
                                Dsel_u2_traning_req.Tables["dsel_u2_traning_req"].Rows.Add(drel_u2_traning_req);
                                ViewState["vsel_u2_traning_reqList"] = Dsel_u2_traning_req;

                                icount_1++;
                            }

                        }
                        drel_u1_traning_req["icount"] = icount_1.ToString();
                        Dsel_u1_traning_req.Tables["dsel_u1_traning_req"].Rows.Add(drel_u1_traning_req);

                        // _func_dmu.zSetGridData(Gvel_u2_traning_req, Dsel_u2_traning_req.Tables["dsel_u2_traning_req"]);

                        data_elearning dt_el_u3 = new data_elearning();
                        dt_el_u3.el_traning_req_action = new traning_req[1];
                        traning_req obj_u3 = new traning_req();
                        obj_u3.u0_EmpIDX_ref = emp_idx;
                        obj_u3.operation_status_id = "E-U3";
                        obj_u3.u0_training_req_idx = id;
                        obj_u3.training_req_item = u1.training_req_item;
                        dt_el_u3.el_traning_req_action[0] = obj_u3;
                        dt_el_u3 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u3);
                        // icount_1 = 0;
                        DataSet Dsel_u3_traning_req = (DataSet)ViewState["vsel_u3_traning_reqList"];
                        DataRow drel_u3_traning_req;
                        if (dt_el_u3.el_traning_req_action != null)
                        {

                            foreach (var u3 in dt_el_u3.el_traning_req_action)
                            {
                                drel_u3_traning_req = Dsel_u3_traning_req.Tables["dsel_u3_traning_req"].NewRow();
                                drel_u3_traning_req["training_req_item_ref"] = u3.training_req_item_ref.ToString();
                                drel_u3_traning_req["m0_training_idx_ref"] = u3.m0_training_idx_ref.ToString();
                                drel_u3_traning_req["u3_RPosIDX_ref"] = u3.u3_RPosIDX_ref.ToString();
                                drel_u3_traning_req["training_req_other"] = u3.training_req_other;
                                drel_u3_traning_req["training_req_pos_flag"] = u3.training_req_pos_flag.ToString();
                                drel_u3_traning_req["training_req_pos_other"] = u3.training_req_pos_other;
                                Dsel_u3_traning_req.Tables["dsel_u3_traning_req"].Rows.Add(drel_u3_traning_req);
                                ViewState["vsel_u3_traning_reqList"] = Dsel_u3_traning_req;

                                icount_1++;
                            }

                        }

                    }

                    ViewState["vsel_u1_traning_reqList"] = Dsel_u1_traning_req;
                    _func_dmu.zSetGridData(GvTraningList, Dsel_u1_traning_req.Tables["dsel_u1_traning_req"]);
                    showFooter_GvTraningList();

                }


            }

        }
    }

    private void zSaveUpdate(int id)
    {
        int u0_training_req_idx = 0;
        traning_req obj_traning_req = new traning_req();
        //traning_req U1
        TextBox txtu0_training_req_date = (TextBox)FvDetailUser.FindControl("txtu0_training_req_date");
        Label LbSecIDX = (Label)FvDetailUser.FindControl("LbSecIDX");
        Label LbRDeptIDX = (Label)FvDetailUser.FindControl("LbRDeptIDX");
        Label LbPosIDX = (Label)FvDetailUser.FindControl("LbPosIDX");

        _data_elearning.el_traning_req_action = new traning_req[1];
        //obj_traning_req.training_req_no = _func_dmu.zRun_Number(_FromTrnNeedsSurRunNo, "", "YYMM", "N", "0000");
        obj_traning_req.operation_status_id = "UPDATE";
        obj_traning_req.u0_training_req_date = _func_dmu.zDateToDB(txtu0_training_req_date.Text);

        obj_traning_req.u0_RSecID_ref = int.Parse(LbSecIDX.Text);
        obj_traning_req.u0_RDeptID_ref = int.Parse(LbRDeptIDX.Text);
        obj_traning_req.u0_RPosIDX_ref = int.Parse(LbPosIDX.Text);
        obj_traning_req.u0_EmpIDX_ref = emp_idx;
        obj_traning_req.u0_training_req_idx = id;
        obj_traning_req.training_req_status = int.Parse(ddltraining_req_status.SelectedValue);
        obj_traning_req.training_req_updated_by = emp_idx;

        //node
        obj_traning_req.approve_status = 0;
        obj_traning_req.u0_idx = 1;
        obj_traning_req.node_idx = 1;
        obj_traning_req.actor_idx = 2;
        obj_traning_req.app_flag = 0;
        obj_traning_req.app_user = emp_idx;

        _data_elearning.el_traning_req_action[0] = obj_traning_req;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u0_training_req, _data_elearning);

        if (_data_elearning.return_code > 0)
        {
            u0_training_req_idx = id;
            if (u0_training_req_idx > 0)
            {
                zDeleteU1U2(u0_training_req_idx);
            }
            if (u0_training_req_idx > 0)
            {
                DataSet dsel_u1_traning_reqList = (DataSet)ViewState["vsel_u1_traning_reqList"];
                traning_req[] objIBomu1 = new traning_req[dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows.Count];
                // traning_req[] objIBomu1 = new traning_req[1];
                int itemObj = 0, icount = 0;
                foreach (DataRow dru1 in dsel_u1_traning_reqList.Tables["dsel_u1_traning_req"].Rows)
                {
                    //itemObj = 0;

                    objIBomu1[itemObj] = new traning_req();
                    objIBomu1[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu1[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru1["m0_training_idx_ref"].ToString());
                    if (int.Parse(dru1["m0_training_idx_ref"].ToString()) == 0)
                    {
                        objIBomu1[itemObj].training_req_other = dru1["training_req_other"].ToString();
                    }
                    else
                    {
                        objIBomu1[itemObj].training_req_other = "";
                    }
                    objIBomu1[itemObj].training_req_type_flag = _func_dmu.zStringToInt(dru1["training_req_type_flag"].ToString());
                    objIBomu1[itemObj].training_req_needs = dru1["training_req_needs"].ToString();
                    objIBomu1[itemObj].training_req_budget = decimal.Parse(dru1["training_req_budget"].ToString());
                    objIBomu1[itemObj].training_req_month_study = _func_dmu.zDateToDB(dru1["training_req_month_study"].ToString());
                    objIBomu1[itemObj].u1_RPosIDX_ref = _func_dmu.zStringToInt(dru1["u1_RPosIDX_ref"].ToString());
                    objIBomu1[itemObj].training_req_qty = _func_dmu.zStringToInt(dru1["training_req_qty"].ToString());
                    objIBomu1[itemObj].training_req_created_by = emp_idx;
                    objIBomu1[itemObj].training_req_updated_by = emp_idx;
                    objIBomu1[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu1[itemObj].training_req_item = _func_dmu.zStringToInt(dru1["training_req_item"].ToString());
                    objIBomu1[itemObj].training_req_pos_flag = _func_dmu.zStringToInt(dru1["training_req_pos_flag"].ToString());
                    objIBomu1[itemObj].training_req_pos_other = dru1["training_req_pos_other"].ToString();



                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u1_training_req, _data_elearning);

                    itemObj++;
                    icount++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu1;
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u1_training_req, _data_elearning);
                }

                DataSet dsel_u2_traning_reqList = (DataSet)ViewState["vsel_u2_traning_reqList"];
                traning_req[] objIBomu2 = new traning_req[dsel_u2_traning_reqList.Tables["dsel_u2_traning_req"].Rows.Count];
                //traning_req[] objIBomu2 = new traning_req[1];
                itemObj = 0;
                foreach (DataRow dru2 in dsel_u2_traning_reqList.Tables["dsel_u2_traning_req"].Rows)
                {
                    // itemObj = 0;

                    objIBomu2[itemObj] = new traning_req();
                    objIBomu2[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu2[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru2["m0_training_idx_ref"].ToString());
                    objIBomu2[itemObj].u2_RPosIDX_ref = _func_dmu.zStringToInt(dru2["u2_RPosIDX_ref"].ToString());
                    objIBomu2[itemObj].u2_EmpIDX_ref = _func_dmu.zStringToInt(dru2["u2_EmpIDX_ref"].ToString());
                    objIBomu2[itemObj].training_req_updated_by = emp_idx;
                    objIBomu2[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu2[itemObj].training_req_item_ref = _func_dmu.zStringToInt(dru2["training_req_item_ref"].ToString());

                    itemObj++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu2;
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u2_training_req, _data_elearning);
                }

                //u3
                DataSet dsel_u3_traning_reqList = (DataSet)ViewState["vsel_u3_traning_reqList"];
                itemObj = 0;
                // traning_req[] objIBomu_u3 = new traning_req[1];
                traning_req[] objIBomu_u3 = new traning_req[dsel_u3_traning_reqList.Tables["dsel_u3_traning_req"].Rows.Count];
                foreach (DataRow dru3 in dsel_u3_traning_reqList.Tables["dsel_u3_traning_req"].Rows)
                {
                    //itemObj = 0;

                    objIBomu_u3[itemObj] = new traning_req();
                    objIBomu_u3[itemObj].u0_training_req_idx_ref = u0_training_req_idx;
                    objIBomu_u3[itemObj].m0_training_idx_ref = _func_dmu.zStringToInt(dru3["m0_training_idx_ref"].ToString());
                    objIBomu_u3[itemObj].u3_RPosIDX_ref = _func_dmu.zStringToInt(dru3["u3_RPosIDX_ref"].ToString());
                    objIBomu_u3[itemObj].training_req_updated_by = emp_idx;
                    objIBomu_u3[itemObj].training_req_status = _func_dmu.zStringToInt(ddltraining_req_status.SelectedValue);
                    objIBomu_u3[itemObj].training_req_item_ref = _func_dmu.zStringToInt(dru3["training_req_item_ref"].ToString());
                    objIBomu_u3[itemObj].training_req_other = dru3["training_req_other"].ToString();
                    objIBomu_u3[itemObj].training_req_pos_flag = _func_dmu.zStringToInt(dru3["training_req_pos_flag"].ToString());
                    objIBomu_u3[itemObj].training_req_pos_other = dru3["training_req_pos_other"].ToString();

                    itemObj++;
                }
                if (itemObj > 0)
                {
                    _data_elearning.el_traning_req_action = objIBomu_u3;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u3_training_req, _data_elearning);
                }

                if (ddltraining_req_status.SelectedValue == "1")
                {
                    sendEmail(int.Parse(Hddfld_u0_training_req_idx.Value));
                }

            }
        }
    }


    private void zShowdataAppDetail(int id)
    {
        data_elearning dt_el_u1 = new data_elearning();
        dt_el_u1.el_traning_req_action = new traning_req[1];
        traning_req obj_u1 = new traning_req();
        // obj_u1.u0_EmpIDX_ref = emp_idx;
        obj_u1.operation_status_id = "E-U1";
        obj_u1.u0_training_req_idx = id;
        dt_el_u1.el_traning_req_action[0] = obj_u1;
        dt_el_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u1);
        _func_dmu.zSetGridData(GvTraningListApp, dt_el_u1.el_traning_req_action);

    }
    private void zShowdataAppList(int iapprove_status)
    {
        Hddfld_ShowdataAppList.Value = iapprove_status.ToString();
        data_elearning dt_el_u1 = new data_elearning();
        dt_el_u1.el_traning_req_action = new traning_req[1];
        traning_req obj_u1 = new traning_req();
        // obj_u1.u0_EmpIDX_ref = emp_idx;
        obj_u1.u0_RDeptID_ref = getViewState("rdept_idx");
        obj_u1.filter_keyword = txtFilterKeyword_A.Text;
        obj_u1.zmonth = int.Parse(ddlMonthSearch_A.SelectedValue);
        obj_u1.zyear = int.Parse(ddlYearSearch_A.SelectedValue);
        obj_u1.operation_status_id = "E-U0-APPLIST";
        obj_u1.approve_status = iapprove_status;
        obj_u1.u0_training_req_idx = 0;
        dt_el_u1.el_traning_req_action[0] = obj_u1;
        dt_el_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u1);
        _func_dmu.zSetGridData(GvApproveList, dt_el_u1.el_traning_req_action);
        zShowdataAppList_Status();
    }

    private void zShowdataAppList_Status()
    {
        // Hddfld_ShowdataAppList.Value = iapprove_status.ToString();
        data_elearning dt_el_u1 = new data_elearning();
        dt_el_u1.el_traning_req_action = new traning_req[1];
        traning_req obj_u1 = new traning_req();
        // obj_u1.u0_EmpIDX_ref = emp_idx;
        obj_u1.u0_RDeptID_ref = getViewState("rdept_idx");
        //obj_u1.filter_keyword = txtFilterKeyword_A.Text;
        //obj_u1.zmonth = int.Parse(ddlMonthSearch_A.SelectedValue);
        // obj_u1.zyear = int.Parse(ddlYearSearch_A.SelectedValue);
        obj_u1.operation_status_id = "E-U0-APPLIST-COUNT";
        obj_u1.approve_status = 0;
        obj_u1.u0_training_req_idx = 0;
        dt_el_u1.el_traning_req_action[0] = obj_u1;
        dt_el_u1 = _func_dmu.zCallServiceNetwork(_urlGetel_u0_training_req, dt_el_u1);
        int iCount = 0;
        if (dt_el_u1.el_traning_req_action != null)
        {
            iCount = dt_el_u1.el_traning_req_action[0].idx;
        }
        if (iCount > 0)
        {
            btnApproveList.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnApproveList.Text = "รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }

    public string getValueType(int id)
    {
        string sReturn = "";
        if (id == 1)
        {
            sReturn = "ฝึกอบรมภายนอก";
        }
        else
        {
            sReturn = "ฝึกอบรมภายใน";
        }
        return sReturn;
    }

    public void getLabelTitleAppList(string id)
    {
        string sReturn = "";
        if (id == "A")
        {
            sReturn = "รายการที่อนุมัติเสร็จแล้ว";
        }
        else
        {
            sReturn = "รายการที่รออนุมัติ";
        }
        //if (Hddfld_status.Value == "APPROVE")
        //{
        //    pnlDetailApp.Visible = true;
        //}
        //else
        //{
        //    pnlDetailApp.Visible = false;
        //}

        lbTitleAppList.Text = sReturn;
        ddlStatusapprove.SelectedIndex = 0;
        txtdetailApprove.Text = "";
    }

    private void sendEmail(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.operation_status_id = "SEND-E_MAIL";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_Traning, _data_elearning);
        }
        catch { }
        /*

        int _tempInt = 0;
        string _mail_subject = "";
        string _mail_body = "";
        string _link = "http://mas.taokaenoi.co.th/it-purchase";
        string emailmove = "";
        string replyempmove = "";

        trainingLoolup obj_trainingLoolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "E-MAIL";
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        if (_data_elearning.trainingLoolup_action != null)
        {
            int i = 0;
            foreach (var item in _data_elearning.trainingLoolup_action)
            {
                if (i == 0)
                {
                    emailmove = item.email_name;
                }
                else
                {
                    emailmove = emailmove + "," + item.email_name;
                }
                i++;
            }
        }

        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_trainingLoolup.operation_status_id = "E-MAIL-LIST";
        obj_trainingLoolup.idx = id;
        _data_elearning.trainingLoolup_action[0] = obj_trainingLoolup;
        _data_elearning = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, _data_elearning);
        if (_data_elearning.trainingLoolup_action != null)
        {
            obj_trainingLoolup = _data_elearning.trainingLoolup_action[0];
            data_elearning _data_sentmail = new data_elearning();
            _data_sentmail.el_traning_req_action = new traning_req[1];
            traning_req create_senmail = new traning_req();

            create_senmail.training_req_no = obj_trainingLoolup.doc_no;
            create_senmail.u0_training_req_date = obj_trainingLoolup.doc_date;
            create_senmail.employee_name = obj_trainingLoolup.employee_name;
            create_senmail.dept_name_th = obj_trainingLoolup.dept_name_th;

            int i = 0;
            string sName = string.Empty;
            foreach(var item in _data_elearning.trainingLoolup_action)
            {
                if (i == 0)
                {
                    sName = item.training_name;
                }
                else
                {
                    sName = sName +" , "+ item.training_name;
                }
                i++;
            }

            create_senmail.training_name = sName;
            _data_sentmail.el_traning_req_action[0] = create_senmail;

            _mail_subject = "[MAS/Training] - " + "แจ้งขอฝึกอบรม";

            _mail_body = create_trainingneeds(_data_sentmail.el_traning_req_action[0]);

            _data_sentmail.el_traning_req_action[0] = create_senmail;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_sentmail));
            servicemail.SendHtmlFormattedEmailFull(emailmove, "", replyempmove, _mail_subject, _mail_body); //sent mail ตอน เทสระบบ

        }
        */
    }
    /*
    #region api e-mail create training
    public string create_trainingneeds(traning_req create_list)
    {
        string body = string.Empty;
        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/template/template_training_needs_survey.html")))
        {
            body = reader.ReadToEnd();
        }


        body = body.Replace("{document_code}", create_list.training_req_no);
        body = body.Replace("{document_date}", create_list.u0_training_req_date);
        body = body.Replace("{name_request}", create_list.employee_name);
        body = body.Replace("{name_department}", create_list.dept_name_th);
        body = body.Replace("{name_training}", create_list.training_name);
        // body = body.Replace("{details_purchase}", create_list.details_purchase);
        // body = body.Replace("{link}", create_list.link);

        return body;
    }


    #endregion
    */
    private void zSaveApprove(int id)
    {
        if (id == 0)
        {
            return;
        }
        traning_req obj_traning_req = new traning_req();
        //traning_req U1
        _data_elearning.el_traning_req_action = new traning_req[1];
        obj_traning_req.operation_status_id = "APPROVE";
        obj_traning_req.training_req_updated_by = emp_idx;
        obj_traning_req.u0_training_req_idx = id;
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_traning_req.approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_traning_req.approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_traning_req.u0_idx = 2;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_traning_req.u0_idx = 3;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_traning_req.u0_idx = 4;
        }
        obj_traning_req.approve_status = int.Parse(ddlStatusapprove.SelectedValue);

        obj_traning_req.node_idx = 2;
        obj_traning_req.actor_idx = 3;
        obj_traning_req.app_flag = 1;
        obj_traning_req.app_user = emp_idx;
        _data_elearning.el_traning_req_action[0] = obj_traning_req;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u0_training_req, _data_elearning);

    }
    public string getTextDoc(int id, string statusText)
    {
        string text = string.Empty;
        if (id == 4) // 4	อนุมัติ
        {
            text = "<span style='color:#26A65B;'>" + statusText + "</span>";
        }
        else if (id == 5) // 5   กลับไปแก้ไข
        {
            text = "<span style='color:#F89406;'>" + statusText + "</span>";
        }
        else if (id == 6)
        {
            text = "<span style='color:#F03434;'>" + statusText + "</span>";
        }
        else
        {
            text = statusText;
        }

        return text;
    }

    public string getTrainingname(int id, string _name1, string _name2)
    {
        string _name = "";
        if (id > 0)
        {
            _name = _name1;
        }
        else
        {
            _name = _name2;
        }
        return _name;
    }

}