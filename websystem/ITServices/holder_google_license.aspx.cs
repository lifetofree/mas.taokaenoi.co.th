﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_holder_google_license : System.Web.UI.Page
{

    #region initial function/data

    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();

    data_googlelicense _data_googlelicense = new data_googlelicense();

    data_softwarelicense _data_softwarelicense = new data_softwarelicense();
    data_softwarelicense_devices _data_softwarelicense_devices = new data_softwarelicense_devices();
    data_employee _data_employee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //static string _keynetworkdevices = ConfigurationManager.AppSettings["keynetworkdevices"];  

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    //Google License
    static string _urlGetU0GoogleLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetU0GoogleLicense"];
    static string _urlGetGoogleLicenseView = _serviceUrl + ConfigurationManager.AppSettings["urlGetGoogleLicenseView"];
    static string _urlGetLogGoogleLicense = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogGoogleLicense"];
    static string _urlSetApproveGGLHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveGGLHolder"];
    static string _urlGetSearchHolderGGL = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchHolderGGL"];


    //Org
    static string _urlGetddlOrganizationSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlOrganizationSW"];
    static string _urlGetddlDeptSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlDeptSW"];
    static string _urlGetddlSectionSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlSectionSW"];
    static string _urlGetddlEmpSW = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlEmpSW"];


    string _localJson = "";
    int _tempInt = 0;

    string _mail_subject = "";
    string _mail_body = "";

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    int m0_node = 0;
    int m0_actor = 0;
    int m0_status = 0;

    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {

        emp_idx = int.Parse(Session["emp_idx"].ToString());


        /// หา Rsec แผนก cehck node
        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

        ViewState["emp_idx_emp"] = _data_employee.employee_list[0].emp_idx; //check เปิดปิด node
        ViewState["rdept_idx_emp"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx_emp"] = _data_employee.employee_list[0].rsec_idx;
        

        if (!IsPostBack)
        {
            initPage();
            btnSearch.Visible = true;
            btnResetSearchPage.Visible = true;
            actionIndex();

            Set_Defult_Index(); //set tab menu 
            ddlOrganizationSearch();

            ///////////////////////////////

            /// หา Rsec แผนก
            data_employee _data_employee_post = new data_employee();

            _data_employee_post.employee_list = new employee_detail[1];
            employee_detail _employee_detail_post = new employee_detail();

            _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

            _data_employee_post.employee_list[0] = _employee_detail_post;

            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
            //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

            _data_employee_post = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

            ViewState["emp_idx_emp"] = _data_employee_post.employee_list[0].emp_idx; //check เปิดปิด node
            ViewState["rdept_idx_emp"] = _data_employee_post.employee_list[0].rdept_idx;
            ViewState["rsec_idx_emp"] = _data_employee_post.employee_list[0].rsec_idx;

            /////////////////////////////


            ////btnToGoogleLicense.Visible = true;
            ////btnDetailGoogleLicense.Visible = false;
            ////btnToReport.Visible = true;

            ////ddlOrganizationSearch();
        }

    }


    #region select data
    protected void actionIndex()
    {

        data_googlelicense _data_googlelicenseindex = new data_googlelicense();
        _data_googlelicenseindex.bind_u0_document_ggl_list = new bind_u0_document_ggl_detail[1];

        bind_u0_document_ggl_detail bind_u0_document_index = new bind_u0_document_ggl_detail();

        bind_u0_document_index.cemp_idx = emp_idx;//int.Parse(ViewState["emp_permission_in"].ToString());
        bind_u0_document_index.rsec_idx = int.Parse(ViewState["rsec_idx_emp"].ToString());

        _data_googlelicenseindex.bind_u0_document_ggl_list[0] = bind_u0_document_index;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_softwarelicense_devicesindex));
        _data_googlelicenseindex = callServiceGoogleLicense(_urlGetU0GoogleLicense, _data_googlelicenseindex);

        if(_data_googlelicenseindex.return_code == 0)
        {
            ViewState["bind_data_index"] = _data_googlelicenseindex.bind_u0_document_ggl_list;

            setGridData(GvMaster, ViewState["bind_data_index"]);
        }



        //ViewState["emp_permission"] = _data_googlelicenseindex.bind_u0_document_ggl_list[0].emp_idx;

        //ViewState["bind_data_index"] = _data_googlelicenseindex.bind_u0_document_ggl_list;

        //setGridData(GvMaster, ViewState["bind_data_index"]);

        ///////(ViewState["rsec_idx_emp"].ToString() != "210") &&
        ////if ((ViewState["rsec_idx_emp"].ToString() != "210") && (ViewState["rsec_idx_emp"].ToString() != "80") &&(ViewState["rsec_idx_emp"].ToString() != "77") && (ViewState["emp_idx_emp"].ToString() != "1347"))
        ////{
        ////    var _linqFilterSelf = from data in _data_googlelicenseindex.bind_u0_document_ggl_list
        ////                          where data.emp_idx == (emp_idx)
        ////                          select data;

        ////    ViewState["bind_data_index"] = _linqFilterSelf.ToList();

        ////    setGridData(GvMaster, ViewState["bind_data_index"]);

        ////    btnSearch.Visible = false;
        ////    btnResetSearchPage.Visible = false;
        ////}
        //////else if((ViewState["emp_idx_emp"].ToString() == "1347"))
        //////{
        //////    ViewState["bind_data_index"] = _data_googlelicenseindex.bind_u0_document_ggl_list;

        //////    setGridData(GvMaster, ViewState["bind_data_index"]);
        //////}
        ////else
        ////{
        ////    ViewState["bind_data_index"] = _data_googlelicenseindex.bind_u0_document_ggl_list;

        ////    setGridData(GvMaster, ViewState["bind_data_index"]);
        ////}
        //else
        //{
        //    ViewState["bind_data_index"] = null;
        //    setGridData(GvMaster, ViewState["bind_data_index"]);
        //}

    }

    protected void SelectViewIndex()
    {
        

        data_googlelicense _data_googlelicenseview = new data_googlelicense();
        _data_googlelicenseview.bind_u0_document_ggl_list = new bind_u0_document_ggl_detail[1];

        bind_u0_document_ggl_detail _bind_u0_document_ggl_detailview = new bind_u0_document_ggl_detail();

        _bind_u0_document_ggl_detailview.u0_idx = int.Parse(ViewState["u0_idx_index"].ToString());

        _data_googlelicenseview.bind_u0_document_ggl_list[0] = _bind_u0_document_ggl_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_googlelicenseview = callServiceGoogleLicense(_urlGetGoogleLicenseView, _data_googlelicenseview);

        setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_googlelicenseview.bind_u0_document_ggl_list);

       
    }

    protected void SelectLogDetailGoogle()
    {

        data_googlelicense _data_googlelicenselog = new data_googlelicense();
        _data_googlelicenselog.log_u0_document_ggl_list = new log_u0_document_ggl_detail[1];

        log_u0_document_ggl_detail _log_u0software_detaillog = new log_u0_document_ggl_detail();

        _log_u0software_detaillog.u0_idx = int.Parse(ViewState["u0_idx_index"].ToString());

        _data_googlelicenselog.log_u0_document_ggl_list[0] = _log_u0software_detaillog;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_googlelicenselog = callServiceGoogleLicense(_urlGetLogGoogleLicense, _data_googlelicenselog);


        //setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_networklog.lognetwork_list);


        rptLogHolderGooglelicense.DataSource = _data_googlelicenselog.log_u0_document_ggl_list;
        rptLogHolderGooglelicense.DataBind();



    }

    protected void ddlOrganizationSearch()
    {
        //FormView FvInsert = (FormView)ViewGoogleInsert.FindControl("FvInsert");
        //Panel PaneldetailLicense = (Panel)FvInsert.FindControl("PaneldetailLicense");
        DropDownList ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");

        ddlorg_idxsearch.Items.Clear();
        ddlorg_idxsearch.AppendDataBoundItems = true;
        ddlorg_idxsearch.Items.Add(new ListItem("เลือกองค์กร ...", "00"));

        data_softwarelicense _data_softwarelicense_orgsearch = new data_softwarelicense();
        _data_softwarelicense_orgsearch.organization_list = new organization_detail[1];
        organization_detail _organization_detail = new organization_detail();

        //_permissionDetail.OrgIDX = 0;

        _data_softwarelicense_orgsearch.organization_list[0] = _organization_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_softwarelicense_orgsearch = callServiceSoftwareLicense(_urlGetddlOrganizationSW, _data_softwarelicense_orgsearch);

        ddlorg_idxsearch.DataSource = _data_softwarelicense_orgsearch.organization_list;
        ddlorg_idxsearch.DataTextField = "OrgNameTH";
        ddlorg_idxsearch.DataValueField = "OrgIDX";
        ddlorg_idxsearch.DataBind();


    }

    #endregion select data

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //int _category_idx;
        //string _category_name;
        int _cemp_idx;

        // *** value insert ***///
        // *** value insert ***///

        m0_ggldataset_detail _m0_ggldataset_detail = new m0_ggldataset_detail();
        u0_gglbuy_detail _u0_gglbuy_detail = new u0_gglbuy_detail();
        m1_gglholderemail_detail _m1_gglholderemail_detail = new m1_gglholderemail_detail();
        m0_gglholderemail_detail _m0_gglholderemail_detail = new m0_gglholderemail_detail();
        ggltranfer_detail _ggltranfer_detail = new ggltranfer_detail();
        del_holdergmail_detail _del_holdergmail_detail = new del_holdergmail_detail();
        bind_gglsearch_detail _bind_gglsearch_detail = new bind_gglsearch_detail();
        bindreport_ggl_detail _bindreport_ggl_detail = new bindreport_ggl_detail();
        approve_u0_document_ggl_detail _approve_u0_document_ggl_detail = new approve_u0_document_ggl_detail();
        search_u0_document_ggl_detail _search_u0_document_ggl_detail = new search_u0_document_ggl_detail();


        switch (cmdName)
        {
            case "btnToIndexList":

                btnToIndexList.BackColor = System.Drawing.Color.LightGray;
                //btnToGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                //btnDetailGoogleLicense.BackColor = System.Drawing.Color.Transparent;
                //btnToReport.BackColor = System.Drawing.Color.Transparent;

                MvMaster.SetActiveView(ViewIndex);

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "btnSearch":

                fvBacktoIndex.Visible = true;
                showsearch.Visible = true;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;

                break;

            case "btnSearchBack":

                showsearch.Visible = false;
                fvBacktoIndex.Visible = false;
                btnSearch.Visible = true;
                btnResetSearchPage.Visible = true;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnViewindex":

                string[] arg1 = new string[3];
                arg1 = e.CommandArgument.ToString().Split(';');
                int u0_idx_index = int.Parse(arg1[0]);
                //string doc_code = arg1[1];
                int noidx_index = int.Parse(arg1[1]);
                int emp_index = int.Parse(arg1[2]);
                int m0_idx_index = int.Parse(arg1[3]);

                ViewState["u0_idx_index"] = u0_idx_index;
                //ViewState["registernamecode"] = doc_code;
                ViewState["noidx_index"] = noidx_index;
                ViewState["emp_index"] = emp_index;
                ViewState["m0_idx_index"] = m0_idx_index;

                gridviewindex.Visible = false;
                btnSearch.Visible = false;
                btnResetSearchPage.Visible = false;
                btnSearchBack.Visible = false;
                showsearch.Visible = false;

                div_show_detailgoogle.Visible = true;

                SelectViewIndex();

                panel_detail_googlelicense.Visible = true;
                SelectLogDetailGoogle();

                div_btn.Visible = true;
                btnCancelView.Visible = true;

                
                setFormData();
                setOntop.Focus();

                break;

            case "btnCancelView":

                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                if(ViewState["bind_data_index"] != null)
                {

                    div_show_detailgoogle.Visible = false;
                    div_btn.Visible = false;
                    panel_detail_googlelicense.Visible = false;

                    gridviewindex.Visible = true;
                    btnSearch.Visible = true;
                    btnResetSearchPage.Visible = true;
                    GvMaster.PageIndex = 0;
                    setGridData(GvMaster, ViewState["bind_data_index"]);
                    setOntop.Focus();
                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }


                break;

            case "btnCancelApprove":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsaveapprove":

                HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
                HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");

                DropDownList ddl_approvestatus = (DropDownList)ViewIndex.FindControl("ddl_approvestatus");
                TextBox txtComment_approve = (TextBox)ViewIndex.FindControl("txtComment_approve");
                TextBox txtu0_codeview = (TextBox)FvViewDetail.FindControl("txtu0_codeview");

                int node = int.Parse(hf_m0_node_idx.Value);
                int actor = int.Parse(hf_m0_actor_idx.Value);
                _cemp_idx = emp_idx;

                if (actor == 2)
                {

                    data_googlelicense _data_googlelicense_approve = new data_googlelicense();

                    _data_googlelicense_approve.approve_u0_document_ggl_list = new approve_u0_document_ggl_detail[1];


                    _approve_u0_document_ggl_detail.u0_idx = int.Parse(ViewState["u0_idx_index"].ToString());
                    _approve_u0_document_ggl_detail.m0_node_idx = node;
                    _approve_u0_document_ggl_detail.m0_actor_idx = actor;
                    _approve_u0_document_ggl_detail.cemp_idx = _cemp_idx;
                    _approve_u0_document_ggl_detail.doc_decision = int.Parse(ddl_approvestatus.SelectedValue);
                    _approve_u0_document_ggl_detail.m0_idx = int.Parse(ViewState["m0_idx_index"].ToString());

                    if (txtComment_approve.Text != "")
                    {
                        _approve_u0_document_ggl_detail.comment = txtComment_approve.Text;
                    }
                    else
                    {
                        _approve_u0_document_ggl_detail.comment = "-";
                    }

                    _data_googlelicense_approve.approve_u0_document_ggl_list[0] = _approve_u0_document_ggl_detail;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_approve));

                    _data_googlelicense_approve = callServiceGoogleLicense(_urlSetApproveGGLHolder, _data_googlelicense_approve);


                    if (_data_googlelicense_approve.return_code == 0)
                    {
                        //actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);    

                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        setError(_data_googlelicense_approve.return_code.ToString() + " - " + _data_googlelicense_approve.return_msg);
                    }

                }

                break;

            case "btnSearchIndex_First":

                DropDownList _ddlorg_idxsearch = (DropDownList)ViewIndex.FindControl("ddlorg_idxsearch");
                DropDownList _ddlrdept_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrdept_idxsearch");
                DropDownList _ddlrsec_idxsearch = (DropDownList)ViewIndex.FindControl("ddlrsec_idxsearch");
                DropDownList _ddlemp_idxsearch = (DropDownList)ViewIndex.FindControl("ddlemp_idxsearch");
                //TextBox _txtcostcenter_no_search = (TextBox)ViewIndex.FindControl("txtcostcenter_no");
                TextBox _txtname_email_search = (TextBox)ViewIndex.FindControl("txtname_email");

                _cemp_idx = emp_idx;

                data_googlelicense _data_googlelicense_search_holder = new data_googlelicense();
                _data_googlelicense_search_holder.search_u0_document_ggl_list = new search_u0_document_ggl_detail[1];

                _search_u0_document_ggl_detail.org_idx = int.Parse(_ddlorg_idxsearch.SelectedValue);
                _search_u0_document_ggl_detail.rdept_idx = int.Parse(_ddlrdept_idxsearch.SelectedValue);
                _search_u0_document_ggl_detail.rsec_idx = int.Parse(_ddlrsec_idxsearch.SelectedValue);
                _search_u0_document_ggl_detail.emp_idx = int.Parse(_ddlemp_idxsearch.SelectedValue);
                //_bind_gglsearch_detail.costcenter_no = _txtcostcenter_no_search.Text;
                _search_u0_document_ggl_detail.email = _txtname_email_search.Text;
                _search_u0_document_ggl_detail.cemp_idx = emp_idx;
                _search_u0_document_ggl_detail.cemp_rsec_idx = int.Parse(ViewState["rsec_idx_emp"].ToString());

                _data_googlelicense_search_holder.search_u0_document_ggl_list[0] = _search_u0_document_ggl_detail;

                // _data_googlelicense_buy.m0_ggldataset_list = m0_ggldataset;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_googlelicense_search));dddd

                _data_googlelicense_search_holder = callServiceGoogleLicense(_urlGetSearchHolderGGL, _data_googlelicense_search_holder);

                ViewState["bind_data_index"] = _data_googlelicense_search_holder.search_u0_document_ggl_list;

                setGridData(GvMaster, ViewState["bind_data_index"]);

                /////(ViewState["rsec_idx_emp"].ToString() != "210") &&

                break;

            case "btnCancelIndex_First":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;




        }

    }

    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status != 0)
        {

            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
            //return "ถูกใช้งานแล้ว";

            return "<span class='status-online f-bold'>ถูกใช้งาน</span>";
        }
        else
        {
            //return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
            return "<span class='status-offline f-bold'>ยังไม่ถูกใช้งาน</span>";
            //return "ยังไม่ถูกใช้งาน";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            
            case "GvMaster":

                setGridData(GvMaster, ViewState["bind_data_index"]);

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                // actionIndex();

                setOntop.Focus();
                break;
          

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label _lbemp_idx = (Label)e.Row.Cells[1].FindControl("lbemp_idx");

                    //ViewState["emp_check_permission"] = _lbemp_idx.Text;

                    

                    //Label _Name_License = (Label)e.Row.Cells[2].FindControl("Name_License");
                    //Label _Count_EmpGoogleLicnese = (Label)e.Row.Cells[2].FindControl("Count_EmpGoogleLicnese");
                    //LinkButton _btnEditCost = (LinkButton)e.Row.Cells[7].FindControl("btnEditCost");
                    //LinkButton _btnDeleteemp = (LinkButton)e.Row.Cells[7].FindControl("btnDeleteemp");

                    //// check ถ้าเมลว่าง สามารถโอนย้ายได้
                    //if (_Name_License.Text == "" && _Count_EmpGoogleLicnese.Text == "0")
                    //{
                    //    _btnEditCost.Visible = true;
                    //}
                    //else if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text == "0")
                    //{
                    //    _btnEditCost.Visible = true;
                    //}
                    //else
                    //{
                    //    _btnEditCost.Visible = false;
                    //}

                    ////Check ว่าสามารถลบ ผู้ถือครอง Google License ได้
                    //if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text != "0")
                    //{
                    //    _btnDeleteemp.Visible = true;
                    //}
                    //else if (_Name_License.Text != "" && _Count_EmpGoogleLicnese.Text == "0")
                    //{
                    //    _btnDeleteemp.Visible = false;
                    //}
                    //else
                    //{
                    //    _btnDeleteemp.Visible = false;
                    //}

                }
                break;

            



        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                ////actionIndex();
                break;



        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
          
        }
    }

    protected void Master_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
           
        
              
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {



        }
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvInsert":

                break;

            case "FvViewDetail":

                //TextBox txtemp_idxview = (TextBox)FvViewDetail.FindControl("txtemp_idxview");

                if (FvViewDetail.CurrentMode == FormViewMode.ReadOnly)
                {


                }

                break;
        }

    }

    #endregion

    #region ddlSelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddName = (DropDownList)sender;

        // RepeaterItem rpItem = ddName.NamingContainer as RepeaterItem;

        switch (ddName.ID)
        {
        
            #region ddl_search

            case "ddlorg_idxsearch":
                if (ddlorg_idxsearch.SelectedValue == "00")
                {
                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarelicensevieworg12 = new data_softwarelicense();
                    _data_softwarelicensevieworg12.organization_list = new organization_detail[1];
                    organization_detail _organization_detail12 = new organization_detail();

                    _organization_detail12.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);

                    _data_softwarelicensevieworg12.organization_list[0] = _organization_detail12;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicensevieworg1));

                    _data_softwarelicensevieworg12 = callServiceSoftwareLicense(_urlGetddlDeptSW, _data_softwarelicensevieworg12); ;


                    ddlrdept_idxsearch.Items.Clear();
                    ddlrdept_idxsearch.AppendDataBoundItems = true;
                    ddlrdept_idxsearch.Items.Add(new ListItem("เลือกฝ่าย ...", "00"));
                    ddlrdept_idxsearch.DataSource = _data_softwarelicensevieworg12.organization_list;
                    ddlrdept_idxsearch.DataTextField = "DeptNameTH";
                    ddlrdept_idxsearch.DataValueField = "RDeptIDX";
                    ddlrdept_idxsearch.DataBind();

                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                break;

            case "ddlrdept_idxsearch":
                if (ddlrdept_idxsearch.SelectedValue == "00")
                {
                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept22 = new data_softwarelicense();
                    _data_softwarelicenseviewdept22.organization_list = new organization_detail[1];
                    organization_detail _organization_detail22 = new organization_detail();

                    _organization_detail22.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail22.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);

                    _data_softwarelicenseviewdept22.organization_list[0] = _organization_detail22;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept22 = callServiceSoftwareLicense(_urlGetddlSectionSW, _data_softwarelicenseviewdept22);


                    ddlrsec_idxsearch.Items.Clear();
                    ddlrsec_idxsearch.AppendDataBoundItems = true;
                    ddlrsec_idxsearch.Items.Add(new ListItem("เลือกแผนก ...", "00"));
                    ddlrsec_idxsearch.DataSource = _data_softwarelicenseviewdept22.organization_list;
                    ddlrsec_idxsearch.DataTextField = "SecNameTH";
                    ddlrsec_idxsearch.DataValueField = "RSecIDX";
                    ddlrsec_idxsearch.DataBind();

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                break;

            case "ddlrsec_idxsearch":
                if (ddlrsec_idxsearch.SelectedValue == "00")
                {

                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));

                }
                else
                {

                    data_softwarelicense _data_softwarelicenseviewdept33 = new data_softwarelicense();
                    _data_softwarelicenseviewdept33.organization_list = new organization_detail[1];
                    organization_detail _organization_detail33 = new organization_detail();

                    _organization_detail33.OrgIDX = int.Parse(ddlorg_idxsearch.SelectedValue);
                    _organization_detail33.RDeptIDX = int.Parse(ddlrdept_idxsearch.SelectedValue);
                    _organization_detail33.RSecIDX = int.Parse(ddlrsec_idxsearch.SelectedValue);

                    _data_softwarelicenseviewdept33.organization_list[0] = _organization_detail33;

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense));

                    _data_softwarelicenseviewdept33 = callServiceSoftwareLicense(_urlGetddlEmpSW, _data_softwarelicenseviewdept33);


                    ddlemp_idxsearch.Items.Clear();
                    ddlemp_idxsearch.AppendDataBoundItems = true;
                    ddlemp_idxsearch.Items.Add(new ListItem("เลือกพนักงาน ...", "00"));
                    ddlemp_idxsearch.DataSource = _data_softwarelicenseviewdept33.organization_list;
                    ddlemp_idxsearch.DataTextField = "emp_name_th";
                    ddlemp_idxsearch.DataValueField = "emp_idx";
                    ddlemp_idxsearch.DataBind();


                }
                break;






            #endregion
     
        }

    }

    #endregion ddlSelectedIndexChanged 

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        btnToIndexList.BackColor = System.Drawing.Color.LightGray;
        //btnToDeviceSoftware.BackColor = System.Drawing.Color.Transparent;
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");

        m0_node = int.Parse(hf_m0_node_idx.Value);// int.Parse(ViewState["M0_Node"].ToString());

        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;

            case 2: // ผู้อนุมติ สร้าง

                setFormDataActor();

                break;
        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        FormView FvViewDetail = (FormView)ViewIndex.FindControl("FvViewDetail");
        HiddenField hf_m0_node_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_node_idx");
        HiddenField hf_m0_actor_idx = (HiddenField)FvViewDetail.FindControl("hf_m0_actor_idx");
        HiddenField hf_doc_decision = (HiddenField)FvViewDetail.FindControl("hf_doc_decision");

        m0_node = int.Parse(hf_m0_node_idx.Value);
        m0_actor = int.Parse(hf_m0_actor_idx.Value);
        m0_status = int.Parse(hf_doc_decision.Value);

        ViewState["m0_node_idx"] = m0_node;


        switch (m0_actor)
        {
            case 1:

                //if ((emp_idx != 0 && emp_idx != 174) || (emp_idx != 0 && emp_idx != 1394))
                //
                show_approve_googlelicense.Visible = false;
                div_btn.Visible = true;
                FvViewDetail.ChangeMode(FormViewMode.ReadOnly);
                FvViewDetail.DataBind();

                //}

                break;

            case 2:
                //ViewState["emp_idx_emp"]
                //if(ViewState["emp_idx_empwhere"].ToString() != "0" || ViewState["emp_idx_empwhere"].ToString() != null) || (ViewState["emp_idx_emp"].ToString() == "1347")
                //{
                if ((ViewState["emp_index"].ToString() == ViewState["emp_idx_emp"].ToString()))
                {
                    if ((m0_actor == 2 && m0_node == 2))
                    {
                        div_btn.Visible = false;

                        show_approve_googlelicense.Visible = true;
                        div_approvstatecreate.Visible = true;
                        div_approve.Visible = true;
                        btnsaveapprove.Visible = true;

                        //litDebug.Text = ViewState["emp_check_permission"].ToString();

                    }
                }
                else
                {
                    div_btn.Visible = true;

                    show_approve_googlelicense.Visible = false;
                    div_approvstatecreate.Visible = false;
                    div_approve.Visible = false;
                    btnsaveapprove.Visible = false;

                    //litDebug.Text = ViewState["emp_check_permission"].ToString();
                }
                //}
                //else
                //{
                //    litDebug.Text = "11222";
                // }


                break;
        }
    }

    #endregion


    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

    }

    protected void setVisible()
    {

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected string getStatusemp(int status_emp_computer)
    {

        if (status_emp_computer != 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='มีผู้ถือครอง'><i class='glyphicon glyphicon-user'></i></span>";
        }
        else
        {
            return string.Empty;
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_googlelicense callServiceGoogleLicense(string _cmdUrl, data_googlelicense _data_googlelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_googlelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_googlelicense = (data_googlelicense)_funcTool.convertJsonToObject(typeof(data_googlelicense), _localJson);

        return _data_googlelicense;
    }


    protected data_softwarelicense callServiceSoftwareLicense(string _cmdUrl, data_softwarelicense _data_softwarelicense)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense = (data_softwarelicense)_funcTool.convertJsonToObject(typeof(data_softwarelicense), _localJson);

        return _data_softwarelicense;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }


    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _data_employee)
    {
        _localJson = _funcTool.convertObjectToJson(_data_employee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _data_employee;
    }

    #endregion reuse
}