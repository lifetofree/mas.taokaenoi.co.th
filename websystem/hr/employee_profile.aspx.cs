﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_profile : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetCountryList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountryList"];
    static string _urlGetProvincetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvincetList"];
    static string _urlGetAmphurList = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurList"];
    static string _urlGetDistList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistList"];
    static string _urlGetApproveLonList = _serviceUrl + ConfigurationManager.AppSettings["urlGetApproveLonList"];
    static string _UrlSetEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["UrlSetEmployeeList"];
    static string _urlSetODSPList = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList"];
    static string _urlSetODSPList_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Update"];
    static string _urlSetODSPList_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Delete"];
    static string _urlGetChildList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildList"];
    static string _urlGetPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorList"];
    static string _urlGetEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationList"];
    static string _urlGetTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainList"];
    static string _urlSetUpdateChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateChildList"];
    static string _urlSetUpdatePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePriorList"];
    static string _urlSetUpdateEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateEducationList"];
    static string _urlSetUpdateTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateTrainList"];
    static string _urlSetDeleteChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteChildList"];
    static string _urlSetDeletePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeletePriorList"];
    static string _urlSetDeleteEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEducationList"];
    static string _urlSetDeleteTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteTrainList"];
    static string _urlSetDeleteEmployeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmployeeList"];
    static string _urlSetResetpass_employeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetResetpass_employeeList"];
    static string _urlurlSetInsertEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertEmployeeList"];
    static string _urlSetUpdateDetail_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList"];
    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string _urlSetupdateapprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdateapprove_employeeList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];
    static string _urlGetReferList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferList"];
    static string _urlSetUpdateReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateReferList"];
    static string _urlSetDeleteReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteReferList"];
    static string _urlSetUpdateDetail_employeeList_profile = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList_profile"];

    static string _urlurlGet_repasswordList = _serviceUrl + ConfigurationManager.AppSettings["urlGet_repasswordList"];
    

    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];

    int emp_idx = 0;
    int defaultInt = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();

            FvEdit_Detail.ChangeMode(FormViewMode.Edit);
            Select_Fv_Detail(FvEdit_Detail, ViewState["EmpIDX"].ToString());
            linkFvTrigger(FvEdit_Detail);
            MvMaster.SetActiveView(ViewEdit);
            Menu_Color_Edit(1);

        }

        linkImgBtnTrigger(btngen_edit);
        linkImgBtnTrigger(btnpos_edit);
        linkImgBtnTrigger(btnaddress_edit);
        linkImgBtnTrigger(btnheal_edit);
        linkImgBtnTrigger(btmapprove_edit);
    }


    #region Path File

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    protected void linkImgBtnTrigger(ImageButton ImgBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = ImgBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    protected void linkFvTrigger(FormView linkFvID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerFv = new PostBackTrigger();
        triggerFv.ControlID = linkFvID.UniqueID;
        updatePanel.Triggers.Add(triggerFv);
    }
    protected void GetPathPhoto(string idemp, Image imgname)
    {
        string getPathLotus = ConfigurationSettings.AppSettings["path_emp_profile"];
        string path = idemp + "/";
        string namefile = idemp + ".jpg";
        string pic = getPathLotus + path + namefile;

        string filePathLotus = Server.MapPath(getPathLotus + idemp);
        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
        SearchDirectories(myDirLotus, idemp);

        if (ViewState["path"].ToString() != "0")
        {
            imgname.Visible = true;
            imgname.ImageUrl = pic;
            imgname.Height = 150;
            imgname.Width = 150;
        }
        else
        {
            imgname.Visible = false;
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                }
            }

            ViewState["path"] = "1";
        }
        catch
        {
            ViewState["path"] = "0";
        }
    }

    #endregion


    #region INSERT&SELECT&UPDATE

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;
    }

    protected void Select_FvEdit_Detail(int Empidx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = Empidx;
        _dataEmployee_.type_select_emp = 4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        //fss.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setFormData(FvEdit_Detail, _dataEmployee.employee_list);
    }

    protected void Select_Fv_Detail(FormView FvName, String Empcode)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        //_dataEmployee_.emp_code = Empcode;
        _dataEmployee_.emp_idx = int.Parse(Empcode);
        _dataEmployee_.type_select_emp = 4;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        setFormData(FvName, _dataEmployee.employee_list);
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("ประจำสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "LocIDX";
        ddlName.DataBind();
    }

    protected void Select_hospital(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกโรงพยาบาล...", "0"));

        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        _dataEmployee.hospital_list[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetHospitalOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.hospital_list;
        ddlName.DataTextField = "Name";
        ddlName.DataValueField = "HosIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_country(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetCountryList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "country_name", "country_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเทศ....", "0"));
    }

    protected void select_nationality(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "NatName", "NatIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
    }

    protected void select_province(DropDownList ddlName)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        //_addresslist.org_idx = 0;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetProvincetList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "prov_name", "prov_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกจังหวัด....", "0"));
    }

    protected void select_amphur(DropDownList ddlName, int prov_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetAmphurList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "amp_name", "amp_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกอำเภอ....", "0"));
    }

    protected void select_dist(DropDownList ddlName, int prov_idx, int amphur_idx)
    {
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();
        _addresslist.prov_idx = prov_idx;
        _addresslist.amp_idx = amphur_idx;
        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetDistList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "dist_name", "dist_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำบล....", "0"));
    }

    protected void select_approve_add(DropDownList ddlName, int org_idx, int dep_idx, int sec_idx, int pos_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = 0;
        _dataEmployee_.org_idx = org_idx;
        _dataEmployee_.rdept_idx = dep_idx;
        _dataEmployee_.rsec_idx = sec_idx;
        _dataEmployee_.rpos_idx = pos_idx;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlGetApproveLonList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_approve1", "emp_idx_approve1");
        ddlName.Items.Insert(0, new ListItem("เลือกผู้อนุมัติ ....", "0"));
    }

    protected void Select_ODSP(int emp_idx)
    {
        var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");

        _dataEmployee.ODSPRelation_list = new ODSP_List[1];
        ODSP_List _dataEmployee_ = new ODSP_List();

        _dataEmployee_.emp_idx = emp_idx;

        _dataEmployee.ODSPRelation_list[0] = _dataEmployee_;
        _dataEmployee = callServiceEmployee(_urlSetODSPList, _dataEmployee);
        setGridData(GvPosAdd_View, _dataEmployee.ODSPRelation_list);
        ViewState["BoxGvPosAdd_View"] = _dataEmployee.ODSPRelation_list;
    }

    protected void Select_Refer_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");

        _dataEmployee_c.BoxReferent_list = new Referent_List[1];
        Referent_List _dataEmployee_1 = new Referent_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxReferent_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetReferList, _dataEmployee_c);
        setGridData(GvReference_View, _dataEmployee_c.BoxReferent_list);
        ViewState["BoxGvRefer_view"] = _dataEmployee_c.BoxReferent_list;
    }

    protected void Select_Child_view(int emp_idx)
    {
        data_employee _dataEmployee_c = new data_employee();
        GridView GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");

        _dataEmployee_c.BoxChild_list = new Child_List[1];
        Child_List _dataEmployee_1 = new Child_List();

        _dataEmployee_1.EmpIDX = emp_idx;

        _dataEmployee_c.BoxChild_list[0] = _dataEmployee_1;
        _dataEmployee_c = callServiceEmployee(_urlGetChildList, _dataEmployee_c);
        setGridData(GvChildAdd_View, _dataEmployee_c.BoxChild_list);
        ViewState["BoxGvChild_view"] = _dataEmployee_c.BoxChild_list;
    }

    protected void Select_Prior_view(int emp_idx)
    {
        data_employee _dataEmployee_p = new data_employee();
        GridView GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");

        _dataEmployee_p.BoxPrior_list = new Prior_List[1];
        Prior_List _dataEmployee_2 = new Prior_List();

        _dataEmployee_2.EmpIDX = emp_idx;

        _dataEmployee_p.BoxPrior_list[0] = _dataEmployee_2;
        _dataEmployee_p = callServiceEmployee(_urlGetPriorList, _dataEmployee_p);
        setGridData(GvPri_View, _dataEmployee_p.BoxPrior_list);
        ViewState["BoxGvPrior_view"] = _dataEmployee_p.BoxPrior_list;
    }

    protected void Select_Education_view(int emp_idx)
    {
        data_employee _dataEmployee_e = new data_employee();
        GridView GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");

        _dataEmployee_e.BoxEducation_list = new Education_List[1];
        Education_List _dataEmployee_ = new Education_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_e.BoxEducation_list[0] = _dataEmployee_;
        _dataEmployee_e = callServiceEmployee(_urlGetEducationList, _dataEmployee_e);
        setGridData(GvEducation_View, _dataEmployee_e.BoxEducation_list);
        ViewState["BoxGvEducation_view"] = _dataEmployee_e.BoxEducation_list;
    }

    protected void Select_Train_view(int emp_idx)
    {
        data_employee _dataEmployee_t = new data_employee();
        GridView GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");

        _dataEmployee_t.BoxTrain_list = new Train_List[1];
        Train_List _dataEmployee_ = new Train_List();

        _dataEmployee_.EmpIDX = emp_idx;

        _dataEmployee_t.BoxTrain_list[0] = _dataEmployee_;
        _dataEmployee_t = callServiceEmployee(_urlGetTrainList, _dataEmployee_t);
        setGridData(GvTrain_View, _dataEmployee_t.BoxTrain_list);
        ViewState["BoxGvTrain_view"] = _dataEmployee_t.BoxTrain_list;
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    #endregion

    protected void Update_Employee()
    {
        //var txt_startdate = (TextBox)FvEdit_Detail.FindControl("txt_startdate");
        //var ddl_target = (DropDownList)FvEdit_Detail.FindControl("ddl_target_edit");
        //var ddlemptype = (DropDownList)FvEdit_Detail.FindControl("ddlemptype");
        //var ddlAffiliation = (DropDownList)FvEdit_Detail.FindControl("ddlAffiliation");
        //var chk_Affiliation_edit = (CheckBox)FvEdit_Detail.FindControl("chk_Affiliation_edit");
        var ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        //var chkprobation = (CheckBox)FvEdit_Detail.FindControl("chkprobation");
        //var chkempout = (CheckBox)FvEdit_Detail.FindControl("chkempout");
        //var txt_empoutdate = (TextBox)FvEdit_Detail.FindControl("txt_empoutdate");
        //var txt_probationdate = (TextBox)FvEdit_Detail.FindControl("txt_probationdate");
        var txt_name_th = (TextBox)FvEdit_Detail.FindControl("txt_name_th");
        var txt_surname_th = (TextBox)FvEdit_Detail.FindControl("txt_surname_th");
        var ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        var txt_name_en = (TextBox)FvEdit_Detail.FindControl("txt_name_en");
        var txt_surname_en = (TextBox)FvEdit_Detail.FindControl("txt_surname_en");
        //var txt_nickname_th = (TextBox)FvEdit_Detail.FindControl("txt_nickname_th");
        //var txt_nickname_en = (TextBox)FvEdit_Detail.FindControl("txt_nickname_en");
        //var ddlcostcenter = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
        var txtbirth = (TextBox)FvEdit_Detail.FindControl("txtbirth");
        //var ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
        var ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
        var ddlnation = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        var ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
        //var ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
        //var ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
        var txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        var txtissued_at = (TextBox)FvEdit_Detail.FindControl("txtissued_at");
        var txtexpcard = (TextBox)FvEdit_Detail.FindControl("txtexpcard");

        //var txtid_passportcard_edit = (TextBox)FvEdit_Detail.FindControl("txtid_passportcard_edit");
        //var txtdate_passport_edit = (TextBox)FvEdit_Detail.FindControl("txtdate_passport_edit");
        //var txtdate_Workpermit_edit = (TextBox)FvEdit_Detail.FindControl("txtdate_Workpermit_edit");

        //var ddllocation = (DropDownList)FvEdit_Detail.FindControl("ddllocation");
        //var ddltypevisa = (DropDownList)FvEdit_Detail.FindControl("ddltypevisa_edit");
        //var txtidcard_visa = (TextBox)FvEdit_Detail.FindControl("txtidcard_visa_edit");
        //var txtdatevisa_s = (TextBox)FvEdit_Detail.FindControl("txtdatevisa_s_edit");
        //var txtdatevisa_e = (TextBox)FvEdit_Detail.FindControl("txtdatevisa_e_edit");

        //var ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
        //var ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
        //var ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
        //var ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");

        //var ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
        //var ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

        //var txtlicens_car = (TextBox)FvEdit_Detail.FindControl("txtlicen_car");
        //var txtlicens_moto = (TextBox)FvEdit_Detail.FindControl("txtlicens_moto");
        //var txtlicens_fork = (TextBox)FvEdit_Detail.FindControl("txtlicens_fork");
        //var txtlicens_truck = (TextBox)FvEdit_Detail.FindControl("txtlicens_truck");

        var dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
        var txtaccountname = (TextBox)FvEdit_Detail.FindControl("txtaccountname");
        var txtaccountnumber = (TextBox)FvEdit_Detail.FindControl("txtaccountnumber");
        var txtemercon = (TextBox)FvEdit_Detail.FindControl("txtemercon");
        var txtrelation = (TextBox)FvEdit_Detail.FindControl("txtrelation");
        var txttelemer = (TextBox)FvEdit_Detail.FindControl("txttelemer");

        var txtfathername = (TextBox)FvEdit_Detail.FindControl("txtfathername_edit");
        var txttelfather = (TextBox)FvEdit_Detail.FindControl("txttelfather_edit");
        var txtlicenfather = (TextBox)FvEdit_Detail.FindControl("txtlicenfather_edit");
        var txtmothername = (TextBox)FvEdit_Detail.FindControl("txtmothername_edit");
        var txttelmother = (TextBox)FvEdit_Detail.FindControl("txttelmother_edit");
        var txtlicenmother = (TextBox)FvEdit_Detail.FindControl("txtlicenmother_edit");
        var txtwifename = (TextBox)FvEdit_Detail.FindControl("txtwifename_edit");
        var txttelwife = (TextBox)FvEdit_Detail.FindControl("txttelwife_edit");
        var txtlicenwifename = (TextBox)FvEdit_Detail.FindControl("txtlicenwife_edit");

        //var txtchildname = (TextBox)FvEdit_Detail.FindControl("txtchildname");
        //var ddlchildnumber = (DropDownList)FvEdit_Detail.FindControl("ddlchildnumber");
        //var GvChildAdd = (GridView)FvEdit_Detail.FindControl("GvChildAdd");

        //var txtorgold = (TextBox)FvEdit_Detail.FindControl("txtorgold_edit");
        //var txtposold = (TextBox)FvEdit_Detail.FindControl("txtposold_edit");
        //var txtworktimeold = (TextBox)FvEdit_Detail.FindControl("txtworktimeold");
        //var txtstartdate_ex = (TextBox)FvEdit_Detail.FindControl("txtstartdate_ex");
        //var txtresigndate_ex = (TextBox)FvEdit_Detail.FindControl("txtresigndate_ex");
        //var GvPri = (GridView)FvEdit_Detail.FindControl("GvPri");

        //var ddleducationback = (DropDownList)FvEdit_Detail.FindControl("ddleducationback");
        //var txtschoolname = (TextBox)FvEdit_Detail.FindControl("txtschoolname");
        //var txtstarteducation = (TextBox)FvEdit_Detail.FindControl("txtstarteducation");
        //var txtendeducation = (TextBox)FvEdit_Detail.FindControl("txtendeducation");
        //var txtstudy = (TextBox)FvEdit_Detail.FindControl("txtstudy");
        //var GvEducation = (GridView)FvEdit_Detail.FindControl("GvEducation");

        //var txttraincourses = (TextBox)FvEdit_Detail.FindControl("txttraincourses");
        //var txttraindate = (TextBox)FvEdit_Detail.FindControl("txttraindate");
        //var txttrainassessment = (TextBox)FvEdit_Detail.FindControl("txttrainassessment");
        //var GvTrain = (GridView)FvEdit_Detail.FindControl("GvTrain");

        //------------------------------- Page 2

        //var ddlorg = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        //var ddldep = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        //var ddlsec = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        //var ddlpos = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

        //------------------------------- Page 3

        var txtaddress_present = (TextBox)FvEdit_Detail.FindControl("txtaddress_present");
        var ddlcountry_present = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
        var ddlprovince_present = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        var ddlamphoe_present = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        var ddldistrict_present = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        var txttelmobile_present = (TextBox)FvEdit_Detail.FindControl("txttelmobile_present");
        var txttelhome_present = (TextBox)FvEdit_Detail.FindControl("txttelhome_present");
        var txtzipcode_present = (TextBox)FvEdit_Detail.FindControl("txtzipcode_present");
        var txtemail_present = (TextBox)FvEdit_Detail.FindControl("txtemail_present");
        var txtpersonal_email = (TextBox)FvEdit_Detail.FindControl("txtpersonal_email");

        var txtaddress_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txtaddress_permanentaddress");
        var ddlcountry_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
        var ddlprovince_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        var ddlamphoe_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        var ddldistrict_permanentaddress = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
        var txttelmobile_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelmobile_permanentaddress");
        var txttelhome_permanentaddress = (TextBox)FvEdit_Detail.FindControl("txttelhome_permanentaddress");

        //------------------------------- Page 4

        var ddlhospital = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
        var txtexaminationdate = (TextBox)FvEdit_Detail.FindControl("txtexaminationdate");
        var txtsecurityid = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");
        var txtexpsecurity = (TextBox)FvEdit_Detail.FindControl("txtexpsecurity");
        var txtheight = (TextBox)FvEdit_Detail.FindControl("txtheight");
        var txtweight = (TextBox)FvEdit_Detail.FindControl("txtweight");
        var ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
        var txtscar = (TextBox)FvEdit_Detail.FindControl("txtscar");
        var ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
        var ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

        //------------------------------- Page 5

        /*var ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
        var ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;*/

        var _dataEmployee_ = new employee_detail[1];
        _dataEmployee_[0] = new employee_detail();

        _dataEmployee_[0].emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        //if (txt_startdate.Text != "") { _dataEmployee_[0].emp_start_date = txt_startdate.Text; } else { _dataEmployee_[0].emp_start_date = "01/01/1900"; }
        //_dataEmployee_[0].TIDX = int.Parse(ddl_target.SelectedValue);
        //_dataEmployee_[0].emp_type_idx = int.Parse(ddlemptype.SelectedValue);
        /*if (chk_Affiliation_edit.Checked)
        {
            _dataEmployee_[0].ACIDX = int.Parse(ddlAffiliation.SelectedValue);
        }
        else
        {
            _dataEmployee_[0].ACIDX = 0;
        }*/
        //if (chkprobation.Checked) { _dataEmployee_[0].emp_probation_status = 1; } else { _dataEmployee_[0].emp_probation_status = 0; }
        //if (chkempout.Checked) { _dataEmployee_[0].emp_status = 0; } else { _dataEmployee_[0].emp_status = 1; }

        //if (txt_empoutdate.Text != "") { _dataEmployee_[0].emp_resign_date = txt_empoutdate.Text; } else { _dataEmployee_[0].emp_resign_date = "01/01/1900"; }
        //if (txt_probationdate.Text != "") { _dataEmployee_[0].emp_probation_date = txt_probationdate.Text; } else { _dataEmployee_[0].emp_probation_date = "01/01/1900"; }

        _dataEmployee_[0].prefix_idx = int.Parse(ddl_prefix_th.SelectedValue);

        if (txt_name_th.Text != "") { _dataEmployee_[0].emp_firstname_th = txt_name_th.Text; } else { _dataEmployee_[0].emp_firstname_th = "-"; }
        if (txt_surname_th.Text != "") { _dataEmployee_[0].emp_lastname_th = txt_surname_th.Text; } else { _dataEmployee_[0].emp_lastname_th = "-"; }

        _dataEmployee_[0].emp_firstname_en = txt_name_en.Text;
        _dataEmployee_[0].emp_lastname_en = txt_surname_en.Text;
        //_dataEmployee_[0].emp_nickname_th = txt_nickname_th.Text;
        //_dataEmployee_[0].emp_nickname_en = txt_nickname_en.Text;
        //_dataEmployee_[0].costcenter_idx = int.Parse(ddlcostcenter.SelectedValue);
        if (txtbirth.Text != "") { _dataEmployee_[0].emp_birthday = txtbirth.Text; } else { _dataEmployee_[0].emp_birthday = "01/01/1900"; } //

        //_dataEmployee_[0].LocIDX = int.Parse(ddllocation.SelectedValue); //No Base

        //_dataEmployee_[0].sex_idx = int.Parse(ddl_sex.SelectedValue);
        _dataEmployee_[0].married_status = int.Parse(ddl_status.SelectedValue); // สถานะแต่งงาน
        _dataEmployee_[0].nat_idx = 177;//int.Parse(ddlnation.SelectedValue);
        _dataEmployee_[0].race_idx = int.Parse(ddlrace.SelectedValue); //
        //_dataEmployee_[0].rel_idx = int.Parse(ddlreligion.SelectedValue);
        //_dataEmployee_[0].mil_idx = int.Parse(ddlmilitary.SelectedValue); // สถานะทางทหาร
        _dataEmployee_[0].identity_card = txtidcard.Text; // รหัสบัตรประชน
        _dataEmployee_[0].issued_at = txtissued_at.Text; // สถานที่ออกบัตร
        if (txtexpcard.Text != "") { _dataEmployee_[0].idate_expired = txtexpcard.Text; } else { _dataEmployee_[0].idate_expired = "01/01/1900"; } // วันที่หมดอายุ
        //_dataEmployee_[0].passportID = txtid_passportcard_edit.Text; // เลขที่ passport
        //_dataEmployee_[0].passport_enddate = txtdate_passport_edit.Text; // วันหมดอายุ passport
        //_dataEmployee_[0].workpermit_enddate = txtdate_Workpermit_edit.Text; // วันหมดอายุ workpermis

        //_dataEmployee_[0].dvcar_idx = int.Parse(ddldvcar.SelectedValue); // สถานะขับขี่รถยนต์ - New Table
        //_dataEmployee_[0].dvmt_idx = int.Parse(ddldvmt.SelectedValue); // สถานะขับขี่รถมอเตอร์ไซ - New Table
        //_dataEmployee_[0].dvfork_idx = int.Parse(ddldvfork.SelectedValue); //  No Base
        //_dataEmployee_[0].dvtruck_idx = int.Parse(ddldvtruck.SelectedValue); //  No Base
        //_dataEmployee_[0].dvcarstatus_idx = int.Parse(ddldvcarstatus.SelectedValue); //  มีรถยนต์ 
        //_dataEmployee_[0].dvmtstatus_idx = int.Parse(ddldvmtstatus.SelectedValue); //  มีรถมอเตอร์ไซต์ 
        //_dataEmployee_[0].dvlicen_car = txtlicens_car.Text; //  No Base
        //_dataEmployee_[0].dvlicen_moto = txtlicens_moto.Text; //  No Base
        //_dataEmployee_[0].dvlicen_fork = txtlicens_fork.Text; //  No Base
        //_dataEmployee_[0].dvlicen_truck = txtlicens_truck.Text; //  No Base

        _dataEmployee_[0].bank_idx = int.Parse(dllbank.SelectedValue); // ธนาคาร - New Table
        _dataEmployee_[0].bank_name = txtaccountname.Text; // ชื่อบัญชี
        _dataEmployee_[0].bank_no = txtaccountnumber.Text; // เลขที่บัญชี

        _dataEmployee_[0].emer_name = txtemercon.Text; // กรณีฉุกเฉินติดต่อ
        _dataEmployee_[0].emer_relation = txtrelation.Text; // ความสัมพันธ์
        _dataEmployee_[0].emer_tel = txttelemer.Text; // เบอร์โทร

        _dataEmployee_[0].fathername = txtfathername.Text; // ชื่อบิดา
        _dataEmployee_[0].telfather = txttelfather.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_father = txtlicenfather.Text; //  No Base
        _dataEmployee_[0].mothername = txtmothername.Text; // ชื่อมารดา
        _dataEmployee_[0].telmother = txttelmother.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_mother = txtlicenmother.Text; //  No Base
        _dataEmployee_[0].wifename = txtwifename.Text; // ชื่อภรรยา
        _dataEmployee_[0].telwife = txttelwife.Text; // เบอร์ติดต่อ
        _dataEmployee_[0].licen_wife = txtlicenwifename.Text; //  No Base

        //_dataEmployee_[0].org_idx = int.Parse(ddlorg.SelectedValue);
        //_dataEmployee_[0].rdept_idx = int.Parse(ddldep.SelectedValue);
        //_dataEmployee_[0].rsec_idx = int.Parse(ddlsec.SelectedValue);
        //_dataEmployee_[0].rpos_idx = int.Parse(ddlpos.SelectedValue);

        _dataEmployee_[0].emp_address = txtaddress_present.Text; // ******************* ที่อยู่ปัจจุบัน
        _dataEmployee_[0].country_idx = int.Parse(ddlcountry_present.SelectedValue);
        _dataEmployee_[0].prov_idx = int.Parse(ddlprovince_present.SelectedValue);
        _dataEmployee_[0].amp_idx = int.Parse(ddlamphoe_present.SelectedValue);
        _dataEmployee_[0].dist_idx = int.Parse(ddldistrict_present.SelectedValue);
        _dataEmployee_[0].emp_mobile_no = txttelmobile_present.Text;  // ******
        _dataEmployee_[0].emp_phone_no = txttelhome_present.Text;   // ******
        _dataEmployee_[0].post_code = txtzipcode_present.Text;
        _dataEmployee_[0].emp_email = txtemail_present.Text;
        _dataEmployee_[0].personal_email = txtpersonal_email.Text;

        _dataEmployee_[0].emp_address_permanent = txtaddress_permanentaddress.Text;  // ******************* ที่อยู่ตามทะเบียนบ้าน
        _dataEmployee_[0].country_idx_permanent = int.Parse(ddlcountry_permanentaddress.SelectedValue);
        _dataEmployee_[0].prov_idx_permanent = int.Parse(ddlprovince_permanentaddress.SelectedValue);
        _dataEmployee_[0].amp_idx_permanent = int.Parse(ddlamphoe_permanentaddress.SelectedValue);
        _dataEmployee_[0].dist_idx_permanent = int.Parse(ddldistrict_permanentaddress.SelectedValue);
        _dataEmployee_[0].PhoneNumber_permanent = txttelmobile_permanentaddress.Text;  // ******

        _dataEmployee_[0].soc_hos_idx = int.Parse(ddlhospital.SelectedValue);  // โรงพยาบาล
        if (txtexaminationdate.Text != "") { _dataEmployee_[0].examinationdate = txtexaminationdate.Text; } else { _dataEmployee_[0].examinationdate = "01/01/1900"; } // วันที่ตรวจสุขภาพ
        _dataEmployee_[0].soc_no = txtsecurityid.Text;  // รหัสบัตรประกันสังคม       
        if (txtexpsecurity.Text != "") { _dataEmployee_[0].soc_expired_date = txtexpsecurity.Text; } else { _dataEmployee_[0].soc_expired_date = "01/01/1900"; } // วันหมดอายุประกันสังคม

        if (txtheight.Text == "") { _dataEmployee_[0].height_s = 0; } else { _dataEmployee_[0].height_s = int.Parse(txtheight.Text); } // ส่วนสูง
        if (txtweight.Text == "") { _dataEmployee_[0].weight_s = 0; } else { _dataEmployee_[0].weight_s = int.Parse(txtweight.Text); } // น้ำหนัก  

        _dataEmployee_[0].btype_idx = int.Parse(ddlblood.SelectedValue);  // กรุ๊ปเลือด
        _dataEmployee_[0].Scar = txtscar.Text;  // แผลเป้น
        _dataEmployee_[0].medicalcertificate_id = int.Parse(ddlmedicalcertificate.SelectedValue); //ใบรับรอง
        _dataEmployee_[0].resultlab_id = int.Parse(ddlresultlab.SelectedValue); // ผลตรวจ LAB

        //_dataEmployee_[0].emp_idx_approve1 = int.Parse(ddlapprove1.SelectedValue);
        //_dataEmployee_[0].emp_idx_approve2 = int.Parse(ddlapprove2.SelectedValue);

        _dtEmployee.employee_list = _dataEmployee_;

        /*var _data_visa_ = new Typevisa_List[1];
        _data_visa_[0] = new Typevisa_List();

        _data_visa_[0].VisaIDX = int.Parse(ddltypevisa.SelectedValue);
        _data_visa_[0].id_card_visa = txtidcard_visa.Text;
        _data_visa_[0].visa_startdate = txtdatevisa_s.Text;
        _data_visa_[0].visa_enddate = txtdatevisa_e.Text;

        _dtEmployee.BoxTypevisa_list = _data_visa_;*/

        try
        {
            //Label2.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));
            _dtEmployee = callServiceEmployee_Check(_urlSetUpdateDetail_employeeList_profile, _dtEmployee);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        catch (FileNotFoundException ex)
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvEdit_Detail":

                Label lbl_emp_idx = (Label)FvEdit_Detail.FindControl("lbl_emp_idx");
                ViewState["fv_emp_idx"] = lbl_emp_idx.Text;
                DropDownList ddlcostcenter_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcostcenter");
                Label lbl_cost_idx = (Label)FvEdit_Detail.FindControl("lbl_cost_idx");
                TextBox txttax_mounth = (TextBox)FvEdit_Detail.FindControl("txttax_mounth");
                TextBox txt_startdate = (TextBox)FvEdit_Detail.FindControl("txt_startdate");

                DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");
                Label lbl_mil_idx = (Label)FvEdit_Detail.FindControl("lbl_mil_idx");

                Label lbl_probation_status = (Label)FvEdit_Detail.FindControl("lbl_probation_status");
                Label lbl_emp_status = (Label)FvEdit_Detail.FindControl("lbl_emp_status");

                Label lbl_org_edit = (Label)FvEdit_Detail.FindControl("lbl_org_edit");
                Label lbl_rdep_edit = (Label)FvEdit_Detail.FindControl("lbl_rdep_edit");
                Label lbl_rsec_edit = (Label)FvEdit_Detail.FindControl("lbl_rsec_edit");
                Label lbl_rpos_edit = (Label)FvEdit_Detail.FindControl("lbl_rpos_edit");

                //ddl ข้อมูล Dropdown fix
                DropDownList ddlemptype = (DropDownList)FvEdit_Detail.FindControl("ddlemptype");
                DropDownList ddl_target_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_target_edit");
                DropDownList ddl_prefix_th = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
                DropDownList ddl_prefix_en = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
                DropDownList ddl_sex = (DropDownList)FvEdit_Detail.FindControl("ddl_sex");
                DropDownList ddl_status = (DropDownList)FvEdit_Detail.FindControl("ddl_status");
                DropDownList ddlrace = (DropDownList)FvEdit_Detail.FindControl("ddlrace");
                DropDownList ddlreligion = (DropDownList)FvEdit_Detail.FindControl("ddlreligion");
                DropDownList ddlmilitary = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary");
                DropDownList ddldvcar = (DropDownList)FvEdit_Detail.FindControl("ddldvcar");
                DropDownList ddldvmt = (DropDownList)FvEdit_Detail.FindControl("ddldvmt");
                DropDownList ddldvfork = (DropDownList)FvEdit_Detail.FindControl("ddldvfork");
                DropDownList ddldvtruck = (DropDownList)FvEdit_Detail.FindControl("ddldvtruck");
                DropDownList ddldvcarstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvcarstatus");
                DropDownList ddldvmtstatus = (DropDownList)FvEdit_Detail.FindControl("ddldvmtstatus");

                DropDownList dllbank = (DropDownList)FvEdit_Detail.FindControl("dllbank");
                DropDownList ddlblood = (DropDownList)FvEdit_Detail.FindControl("ddlblood");
                DropDownList ddlmedicalcertificate = (DropDownList)FvEdit_Detail.FindControl("ddlmedicalcertificate");
                DropDownList ddlresultlab = (DropDownList)FvEdit_Detail.FindControl("ddlresultlab");

                Label lbl_emptype = (Label)FvEdit_Detail.FindControl("lbl_emptype");
                Label lbl_target = (Label)FvEdit_Detail.FindControl("lbl_target");
                Label lbl_prefix_idx = (Label)FvEdit_Detail.FindControl("lbl_prefix_idx");
                Label lbl_sex_idx = (Label)FvEdit_Detail.FindControl("lbl_sex_idx");
                Label lbl_married_status_idx = (Label)FvEdit_Detail.FindControl("lbl_married_status_idx");
                Label lbl_race_idx = (Label)FvEdit_Detail.FindControl("lbl_race_idx");
                Label lbl_rel_idx = (Label)FvEdit_Detail.FindControl("lbl_rel_idx");
                Label lbl_dvcar_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcar_idx");
                Label lbl_dvmt_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmt_idx");
                Label lbl_dvcarstatus_idx = (Label)FvEdit_Detail.FindControl("lbl_dvcarstatus_idx");
                Label lbl_dvmtstatus_idx = (Label)FvEdit_Detail.FindControl("lbl_dvmtstatus_idx");
                Label lbl_dvfork_idx = (Label)FvEdit_Detail.FindControl("lbl_dvfork_idx");
                Label lbl_dvtruck_idx = (Label)FvEdit_Detail.FindControl("lbl_dvtruck_idx");
                Label lbl_bank_idx = (Label)FvEdit_Detail.FindControl("lbl_bank_idx");
                Label lbl_BRHIDX = (Label)FvEdit_Detail.FindControl("lbl_BRHIDX");
                Label lbl_medical_id = (Label)FvEdit_Detail.FindControl("lbl_medical_id");
                Label lbl_resultlab_id = (Label)FvEdit_Detail.FindControl("lbl_resultlab_id");

                //ที่อยู่ปัจจุบัน
                DropDownList ddlcountry_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_present_edit");
                DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
                DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
                DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
                Label lbl_country_edit = (Label)FvEdit_Detail.FindControl("lbl_country_edit");
                Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
                Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
                Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

                //ที่อยู่ตามทะเบียนบ้าน
                DropDownList ddlcountry_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlcountry_permanentaddress_edit");
                DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
                DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
                DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");
                Label lbl_ddlcountry_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlcountry_permanent_edit");
                Label lbl_ddlprovince_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlprovince_permanent_edit");
                Label lbl_ddlamphoe_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddlamphoe_permanent_edit");
                Label lbl_ddldistrict_permanent_edit = (Label)FvEdit_Detail.FindControl("lbl_ddldistrict_permanent_edit");

                DropDownList ddlapprove1 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove1");
                DropDownList ddlapprove2 = (DropDownList)FvEdit_Detail.FindControl("ddlapprove2");
                Label lbl_approve_1_edit = (Label)FvEdit_Detail.FindControl("lbl_approve_1_edit");
                Label lbl_approve_2_edit = (Label)FvEdit_Detail.FindControl("lbl_approve_2_edit");

                DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");

                DropDownList ddlhospital_edit = (DropDownList)FvEdit_Detail.FindControl("ddlhospital");
                Label lbl_hos_edit = (Label)FvEdit_Detail.FindControl("lbl_hos_edit");
                DropDownList ddllocation_edit = (DropDownList)FvEdit_Detail.FindControl("ddllocation");
                Label lbl_location_idx = (Label)FvEdit_Detail.FindControl("lbl_location_idx");
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                Label lbl_nation_idx = (Label)FvEdit_Detail.FindControl("lbl_nation_idx");
                Image image = (Image)FvEdit_Detail.FindControl("image");
                TextBox txtidcard = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                LinkButton btnupload = (LinkButton)FvEdit_Detail.FindControl("btnupload");

                int year = 0;
                var temp_startdate = txt_startdate.Text.Replace("/", "").Substring(4, 4);
                var temp_year = DateTime.Now.Year.ToString();
                var temp_startmount = txt_startdate.Text.Replace("/", "").Substring(2, 2);
                if (txt_startdate.Text != null)
                {
                    // (Year วันเริ่มงาน) != (Year ปัจจุบัน)
                    //if (int.Parse(DateTime.Parse(txt_startdate.Text).Year.ToString()) != int.Parse(DateTime.Now.Year.ToString()))  //กรณีปีไม่เท่ากัน
                    if (int.Parse(temp_startdate.ToString()) != int.Parse(temp_year.ToString()))  //กรณีปีไม่เท่ากัน
                    {
                        //เดือนปัจจุบัน
                        year = 12; //int.Parse(DateTime.Now.Month.ToString());
                    }
                    else //กรณีปีเท่ากัน
                    {
                        //เดือนปัจจุบัน - เดือนที่เริ่มงาน
                        //year = 13 - int.Parse(formatDateTimeCheck(temp_startdate.ToString()).ToString());
                        year = 13 - int.Parse(temp_startmount.ToString());
                    }
                }
                else
                {
                    year = 0;
                }


                if (FvEdit_Detail.CurrentMode == FormViewMode.Edit)
                {
                    linkBtnTrigger(btnupload);
                    

                    ddlemptype.SelectedValue = lbl_emptype.Text;
                    txttax_mounth.Text = year.ToString() + " เดือน";
                    ddl_prefix_th.SelectedValue = lbl_prefix_idx.Text;
                    ddl_prefix_en.SelectedValue = lbl_prefix_idx.Text;
                    ddl_sex.SelectedValue = lbl_sex_idx.Text;
                    ddl_status.SelectedValue = lbl_married_status_idx.Text;
                    ddlrace.SelectedValue = lbl_race_idx.Text;
                    ddlreligion.SelectedValue = lbl_rel_idx.Text;
                    ddldvcar.SelectedValue = lbl_dvcar_idx.Text;
                    ddldvmt.SelectedValue = lbl_dvmt_idx.Text;
                    ddldvcarstatus.SelectedValue = lbl_dvcarstatus_idx.Text;
                    ddldvmtstatus.SelectedValue = lbl_dvmtstatus_idx.Text;
                    ddldvfork.SelectedValue = lbl_dvfork_idx.Text;
                    ddldvtruck.SelectedValue = lbl_dvtruck_idx.Text;
                    dllbank.SelectedValue = lbl_bank_idx.Text;
                    ddlblood.SelectedValue = lbl_BRHIDX.Text;
                    ddlmedicalcertificate.SelectedValue = lbl_medical_id.Text;
                    ddlresultlab.SelectedValue = lbl_resultlab_id.Text;
                    ddllocation_edit.SelectedValue = lbl_location_idx.Text;
                    ddlmilitary_edit.SelectedValue = lbl_mil_idx.Text;


                    Select_Coscenter(ddlcostcenter_edit);
                    ddlcostcenter_edit.SelectedValue = lbl_cost_idx.Text;
                    Select_hospital(ddlhospital_edit);
                    ddlhospital_edit.SelectedValue = lbl_hos_edit.Text;

                    select_approve_add(ddlapprove1, int.Parse(lbl_org_edit.Text), int.Parse(lbl_rdep_edit.Text), int.Parse(lbl_rsec_edit.Text), int.Parse(lbl_rpos_edit.Text));
                    select_approve_add(ddlapprove2, int.Parse(lbl_org_edit.Text), int.Parse(lbl_rdep_edit.Text), int.Parse(lbl_rsec_edit.Text), int.Parse(lbl_rpos_edit.Text));
                    ddlapprove1.SelectedValue = lbl_approve_1_edit.Text;
                    ddlapprove2.SelectedValue = lbl_approve_2_edit.Text;

                    select_country(ddlcountry_present_edit);
                    ddlcountry_present_edit.SelectedValue = lbl_country_edit.Text;
                    select_province(ddlprovince_present_edit);
                    ddlprovince_present_edit.SelectedValue = lbl_prov_edit.Text;
                    select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                    ddlamphoe_present_edit.SelectedValue = lbl_amp_edit.Text;
                    select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                    ddldistrict_present_edit.SelectedValue = lbl_dist_edit.Text;

                    select_country(ddlcountry_permanentaddress_edit);
                    ddlcountry_permanentaddress_edit.SelectedValue = lbl_ddlcountry_permanent_edit.Text;
                    select_province(ddlprovince_permanentaddress_edit);
                    ddlprovince_permanentaddress_edit.SelectedValue = lbl_ddlprovince_permanent_edit.Text;
                    select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                    ddlamphoe_permanentaddress_edit.SelectedValue = lbl_ddlamphoe_permanent_edit.Text;
                    select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                    ddldistrict_permanentaddress_edit.SelectedValue = lbl_ddldistrict_permanent_edit.Text;

                    ddl_target_edit.SelectedValue = lbl_target.Text;

                    //select_org(ddlorg_add_edit);
                    Select_ODSP(int.Parse(lbl_emp_idx.Text.ToString()));
                    Select_Refer_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Child_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Prior_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Education_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Train_view(int.Parse(ViewState["fv_emp_idx"].ToString()));
                    Select_Location(ddllocation_edit);
                    select_nationality(ddlnation_edit);
                    ddlnation_edit.SelectedValue = lbl_nation_idx.Text;

                    ViewState["EmpIDX_Pic"] = lbl_emp_idx.Text;
                    GetPathPhoto(lbl_emp_idx.Text, image);
                }
                break;
        }
    }

    #endregion

    #region Switch-Munu Color

    protected void Menu_Color_Edit(int choice)
    {
        Panel BoxGen = (Panel)FvEdit_Detail.FindControl("BoxGen");
        Panel BoxPos = (Panel)FvEdit_Detail.FindControl("BoxPos");
        Panel BoxAddress = (Panel)FvEdit_Detail.FindControl("BoxAddress");
        Panel BoxHeal = (Panel)FvEdit_Detail.FindControl("BoxHeal");
        Panel BoxApprove = (Panel)FvEdit_Detail.FindControl("BoxApprove");

        switch (choice)
        {
            case 1: //Gen
                btngen_edit.ImageUrl = "~/masterpage/images/hr/color_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                BoxGen.Visible = true;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;

                break;
            case 2: //Pos
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/color_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                BoxGen.Visible = false;
                BoxPos.Visible = true;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;

                break;
            case 3: //Address
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/color_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = true;
                BoxHeal.Visible = false;
                BoxApprove.Visible = false;

                break;
            case 4: //Heal
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/color_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/gray_5.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = true;
                BoxApprove.Visible = false;
                break;
            case 5: //Approve
                btngen_edit.ImageUrl = "~/masterpage/images/hr/gray_1.png";
                btnpos_edit.ImageUrl = "~/masterpage/images/hr/gray_2.png";
                btnaddress_edit.ImageUrl = "~/masterpage/images/hr/gray_3.png";
                btnheal_edit.ImageUrl = "~/masterpage/images/hr/gray_4.png";
                btmapprove_edit.ImageUrl = "~/masterpage/images/hr/color_5.png";

                BoxGen.Visible = false;
                BoxPos.Visible = false;
                BoxAddress.Visible = false;
                BoxHeal.Visible = false;
                BoxApprove.Visible = true;
                break;
        }
    }
    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvPosAdd_View":
                var GvPosAdd_View = (GridView)FvEdit_Detail.FindControl("GvPosAdd_View");
                GvPosAdd_View.PageIndex = e.NewPageIndex;
                GvPosAdd_View.DataSource = ViewState["BoxGvPosAdd_View"];
                GvPosAdd_View.DataBind();
                break;
            case "GvReference_View":
                var GvReference_View = (GridView)FvEdit_Detail.FindControl("GvReference_View");
                GvReference_View.PageIndex = e.NewPageIndex;
                GvReference_View.DataSource = ViewState["BoxGvRefer_view"];
                GvReference_View.DataBind();
                break;
            case "GvChildAdd_View":
                var GvChildAdd_View = (GridView)FvEdit_Detail.FindControl("GvChildAdd_View");
                GvChildAdd_View.PageIndex = e.NewPageIndex;
                GvChildAdd_View.DataSource = ViewState["BoxGvChild_view"];
                GvChildAdd_View.DataBind();
                break;
            case "GvPri_View":
                var GvPri_View = (GridView)FvEdit_Detail.FindControl("GvPri_View");
                GvPri_View.PageIndex = e.NewPageIndex;
                GvPri_View.DataSource = ViewState["BoxGvPrior_view"];
                GvPri_View.DataBind();
                break;
            case "GvEducation_View":
                var GvEducation_View = (GridView)FvEdit_Detail.FindControl("GvEducation_View");
                GvEducation_View.PageIndex = e.NewPageIndex;
                GvEducation_View.DataSource = ViewState["BoxGvEducation_view"];
                GvEducation_View.DataBind();
                break;
            case "GvTrain_View":
                var GvTrain_View = (GridView)FvEdit_Detail.FindControl("GvTrain_View");
                GvTrain_View.PageIndex = e.NewPageIndex;
                GvTrain_View.DataSource = ViewState["BoxGvTrain_view"];
                GvTrain_View.DataBind();
                break;
        }
    }

    #endregion

    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "ckreference_edit":

                var ckreference_edit = (CheckBox)FvEdit_Detail.FindControl("ckreference_edit");
                var GvReference_Edit = (GridView)FvEdit_Detail.FindControl("GvReference_Edit");
                var BoxReference_edit = (Panel)FvEdit_Detail.FindControl("BoxReference");

                if (ckreference_edit.Checked)
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer_edit"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer_edit"] = dsRefer;
                    GvReference_Edit.DataSource = dsRefer.Tables[0];
                    GvReference_Edit.DataBind();

                    BoxReference_edit.Visible = false;
                    GvReference_Edit.DataSource = null;
                    GvReference_Edit.DataBind();
                }
                break;

            case "chkchild_edit":

                var chkchild_edit = (CheckBox)FvEdit_Detail.FindControl("chkchild_edit");
                var GvChildAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvChildAdd_Edit");
                var BoxChild_edit = (Panel)FvEdit_Detail.FindControl("BoxChild");

                if (chkchild_edit.Checked)
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Child_edit"] = null;

                    var dsEquipment = new DataSet();
                    dsEquipment.Tables.Add("Equipment_");

                    dsEquipment.Tables[0].Columns.Add("Child_name", typeof(String));
                    dsEquipment.Tables[0].Columns.Add("Child_num", typeof(String));

                    ViewState["vsTemp_Child_edit"] = dsEquipment;
                    GvChildAdd_Edit.DataSource = dsEquipment.Tables[0];
                    GvChildAdd_Edit.DataBind();

                    BoxChild_edit.Visible = false;
                    GvChildAdd_Edit.DataSource = null;
                    GvChildAdd_Edit.DataBind();
                }
                break;

            case "chkorg_edit":

                var chkorg_edit = (CheckBox)FvEdit_Detail.FindControl("chkorg_edit");
                var GvPri_Edit = (GridView)FvEdit_Detail.FindControl("GvPri_Edit");
                var BoxOrgOld_edit = (Panel)FvEdit_Detail.FindControl("BoxOrgOld");


                if (chkorg_edit.Checked)
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));


                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Prior_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("OrgOld_edit");

                    dsEquipment_.Tables[0].Columns.Add("Pri_Nameorg", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_pos", typeof(String));
                    //dsEquipment_.Tables[0].Columns.Add("Pri_worktime", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_startdate", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Pri_resigndate", typeof(String));

                    ViewState["vsTemp_Prior_edit"] = dsEquipment_;
                    GvPri_Edit.DataSource = dsEquipment_.Tables[0];
                    GvPri_Edit.DataBind();

                    BoxOrgOld_edit.Visible = false;
                    GvPri_Edit.DataSource = null;
                    GvPri_Edit.DataBind();
                }


                break;

            case "ckeducation_edit":

                var ckeducation_edit = (CheckBox)FvEdit_Detail.FindControl("ckeducation_edit");
                var GvEducation_Edit = (GridView)FvEdit_Detail.FindControl("GvEducation_Edit");
                var BoxEducation_edit = (Panel)FvEdit_Detail.FindControl("BoxEducation");

                if (ckeducation_edit.Checked)
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Education_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Edu_edit");

                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification_ID", typeof(int));
                    dsEquipment_.Tables[0].Columns.Add("Edu_qualification", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_name", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_branch", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_start", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Edu_end", typeof(String));

                    ViewState["vsTemp_Education_edit"] = dsEquipment_;
                    GvEducation_Edit.DataSource = dsEquipment_.Tables[0];
                    GvEducation_Edit.DataBind();

                    BoxEducation_edit.Visible = false;
                    GvEducation_Edit.DataSource = null;
                    GvEducation_Edit.DataBind();
                }


                break;

            case "cktraining_edit":

                var cktraining_edit = (CheckBox)FvEdit_Detail.FindControl("cktraining_edit");
                var GvTrain_Edit = (GridView)FvEdit_Detail.FindControl("GvTrain_Edit");
                var BoxTrain_edit = (Panel)FvEdit_Detail.FindControl("BoxTrain");


                if (cktraining_edit.Checked)
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_Train_edit"] = null;

                    var dsEquipment_ = new DataSet();
                    dsEquipment_.Tables.Add("Train_edit");

                    dsEquipment_.Tables[0].Columns.Add("Train_courses", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_date", typeof(String));
                    dsEquipment_.Tables[0].Columns.Add("Train_assessment", typeof(String));

                    ViewState["vsTemp_Train_edit"] = dsEquipment_;
                    GvTrain_Edit.DataSource = dsEquipment_.Tables[0];
                    GvTrain_Edit.DataBind();

                    BoxTrain_edit.Visible = false;
                    GvTrain_Edit.DataSource = null;
                    GvTrain_Edit.DataBind();
                }


                break;

            case "chkposition_edit":

                var chkposition_edit = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
                var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                var BoxPosition_edit = (Panel)FvEdit_Detail.FindControl("BoxPosition_edit");

                if (chkposition_edit.Checked)
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = true;

                }
                else
                {
                    ViewState["vsTemp_PosAdd_edit"] = null;

                    var dsPosadd = new DataSet();
                    dsPosadd.Tables.Add("PosAdd_edit");

                    dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
                    dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
                    dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

                    ViewState["vsTemp_PosAdd_edit"] = dsPosadd;
                    GvPosAdd_Edit.DataSource = dsPosadd.Tables[0];
                    GvPosAdd_Edit.DataBind();

                    BoxPosition_edit.Visible = false;
                    GvPosAdd_Edit.DataSource = null;
                    GvPosAdd_Edit.DataBind();
                }
                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        DropDownList ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
        DropDownList ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
        DropDownList ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

        DropDownList ddlprovince_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_permanentaddress_edit");
        DropDownList ddlamphoe_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_permanentaddress_edit");
        DropDownList ddldistrict_permanentaddress_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_permanentaddress_edit");

        DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
        TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
        TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

        DropDownList ddlprovince_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlprovince_present_edit");
        DropDownList ddlamphoe_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddlamphoe_present_edit");
        DropDownList ddldistrict_present_edit = (DropDownList)FvEdit_Detail.FindControl("ddldistrict_present_edit");
        Label lbl_prov_edit = (Label)FvEdit_Detail.FindControl("lbl_prov_edit");
        Label lbl_amp_edit = (Label)FvEdit_Detail.FindControl("lbl_amp_edit");
        Label lbl_dist_edit = (Label)FvEdit_Detail.FindControl("lbl_dist_edit");

        DropDownList ddl_prefix_th_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_th_edit");
        DropDownList ddl_prefix_en_edit = (DropDownList)FvEdit_Detail.FindControl("ddl_prefix_en_edit");
        DropDownList ddlmilitary_edit = (DropDownList)FvEdit_Detail.FindControl("ddlmilitary_edit");



        switch (ddName.ID)
        {

            case "ddlorg_add_edit":
                select_dep(ddldep_add_edit, int.Parse(ddlorg_add_edit.SelectedValue));
                break;
            case "ddldep_add_edit":
                select_sec(ddlsec_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue));
                break;
            case "ddlsec_add_edit":
                select_pos(ddlpos_add_edit, int.Parse(ddlorg_add_edit.SelectedValue), int.Parse(ddldep_add_edit.SelectedValue), int.Parse(ddlsec_add_edit.SelectedValue));
                break;

            case "ddlOrganizationEdit_Select":
                var ddNameOrg = (DropDownList)sender;
                var row = (GridViewRow)ddNameOrg.NamingContainer;

                var ddlOrg = (DropDownList)row.FindControl("ddlOrganizationEdit_Select");
                var ddlDep = (DropDownList)row.FindControl("ddlDepartmentEdit_Select");
                var ddlSec = (DropDownList)row.FindControl("ddlSectionEdit_Select");
                var ddlPos = (DropDownList)row.FindControl("ddlPositionEdit_Select");

                select_dep(ddlDep, int.Parse(ddlOrg.SelectedValue));
                break;
            case "ddlDepartmentEdit_Select":
                var ddNameOrg_1 = (DropDownList)sender;
                var row_1 = (GridViewRow)ddNameOrg_1.NamingContainer;

                var ddlOrg_1 = (DropDownList)row_1.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_1 = (DropDownList)row_1.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_1 = (DropDownList)row_1.FindControl("ddlSectionEdit_Select");
                var ddlPos_1 = (DropDownList)row_1.FindControl("ddlPositionEdit_Select");

                select_sec(ddlSec_1, int.Parse(ddlOrg_1.SelectedValue), int.Parse(ddlDep_1.SelectedValue));
                break;
            case "ddlSectionEdit_Select":
                var ddNameOrg_2 = (DropDownList)sender;
                var row_2 = (GridViewRow)ddNameOrg_2.NamingContainer;

                var ddlOrg_2 = (DropDownList)row_2.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_2 = (DropDownList)row_2.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_2 = (DropDownList)row_2.FindControl("ddlSectionEdit_Select");
                var ddlPos_2 = (DropDownList)row_2.FindControl("ddlPositionEdit_Select");

                select_pos(ddlPos_2, int.Parse(ddlOrg_2.SelectedValue), int.Parse(ddlDep_2.SelectedValue), int.Parse(ddlSec_2.SelectedValue));
                break;

            case "ddlprovince_present_edit":
                select_amphur(ddlamphoe_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue));
                break;
            case "ddlamphoe_present_edit":
                select_dist(ddldistrict_present_edit, int.Parse(ddlprovince_present_edit.SelectedValue), int.Parse(ddlamphoe_present_edit.SelectedValue));
                break;

            case "ddlprovince_permanentaddress_edit":
                select_amphur(ddlamphoe_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue));
                break;
            case "ddlamphoe_permanentaddress_edit":
                select_dist(ddldistrict_permanentaddress_edit, int.Parse(ddlprovince_permanentaddress_edit.SelectedValue), int.Parse(ddlamphoe_permanentaddress_edit.SelectedValue));
                break;
         
            case "ddl_prefix_th_edit":
                if (ddl_prefix_th_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_th_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_en_edit.SelectedValue = "4";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
            case "ddl_prefix_en_edit":
                if (ddl_prefix_en_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th_edit.SelectedValue = "1";
                    ddlmilitary_edit.SelectedValue = "1";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th_edit.SelectedValue = "2";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else if (ddl_prefix_en_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th_edit.SelectedValue = "3";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                else
                {
                    ddl_prefix_th_edit.SelectedValue = "4";
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
            case "ddlnation_edit":
                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    ddlmilitary_edit.SelectedValue = "3";
                }
                break;
        }
    }

    #endregion

    #region callService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceEmployee_Check(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;
        //Label2.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        ViewState["return_code"] = _dataEmployee.return_code;
        ViewState["return_sucess"] = _dataEmployee.return_msg;

        return _dataEmployee;
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnedit_emp":
                MvMaster.SetActiveView(ViewEdit);
                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                Select_FvEdit_Detail(int.Parse(cmdArg));
                ViewState["Gv_emp_idx"] = int.Parse(cmdArg);
                Menu_Color_Edit(1);
                break;
            case "btnresetpassword_emp":
                _dataEmployee.employee_list = new employee_detail[1];
                employee_detail _dataEmployee_reset = new employee_detail();

                _dataEmployee_reset.emp_idx = int.Parse(cmdArg);

                _dataEmployee.employee_list[0] = _dataEmployee_reset;
                _dataEmployee = callServiceEmployee(_urlSetResetpass_employeeListList, _dataEmployee);

                //Select_Employee_index();
                break;
        
            case "btngeneral_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnposition_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnaddress_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnhealth_edit":
                DropDownList ddlnation_edit = (DropDownList)FvEdit_Detail.FindControl("ddlnation_edit");
                TextBox txtidcard_edit = (TextBox)FvEdit_Detail.FindControl("txtidcard");
                TextBox txtsecurityid_edit = (TextBox)FvEdit_Detail.FindControl("txtsecurityid");

                if (ddlnation_edit.SelectedValue == "177")
                {
                    txtsecurityid_edit.Text = txtidcard_edit.Text;
                }
                else
                {
                    txtsecurityid_edit.Text = "";
                }
                Menu_Color_Edit(int.Parse(cmdArg));
                break;
            case "btnapprove_edit":
                Menu_Color_Edit(int.Parse(cmdArg));
                break;

            case "BtnChangePassword":
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                break;
            case "btnChange_Password":
                TextBox txtpass_1 = (TextBox)FvEdit_Detail.FindControl("txtpass_1");
                TextBox txtpass_2 = (TextBox)FvEdit_Detail.FindControl("txtpass_2");
                Label lbl_massage_alert = (Label)FvEdit_Detail.FindControl("lbl_massage_alert");

                if (txtpass_1.Text == txtpass_2.Text)
                {
                    _dataEmployee.employee_list = new employee_detail[1];
                    employee_detail _dataEmployee_ = new employee_detail();

                    _dataEmployee_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                    _dataEmployee_.emp_password = _funcTool.getMd5Sum(txtpass_1.Text);

                    _dataEmployee.employee_list[0] = _dataEmployee_;
                    _dataEmployee = callServiceEmployee(_urlurlGet_repasswordList, _dataEmployee);
                    //Label2.Text = _dataEmployee.return_code.ToString(); //HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dataEmployee));
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Sucsess", "alert('" + "ดำเนินการเปลี่ยน Password ให้เรียบร้อยแล้ว" + "')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    lbl_massage_alert.ForeColor = System.Drawing.Color.Red;
                    lbl_massage_alert.Text = "ดำเนินการเสร็จเรียบร้อย";
                    //txtpass_1.Text = null;
                    //txtpass_2.Text = null;
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Data", "alert('" + "คุณกรอกรหัสใหม่ไม่ตรงกัน" + "')", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    lbl_massage_alert.ForeColor = System.Drawing.Color.Red;
                    lbl_massage_alert.Text = "คุณกรอกรหัสใหม่ไม่ตรงกัน";
                }

                break;
            case "btnClose_Change_Password":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnUpdate":
                Update_Employee();
                break;

            case "CmdUpload":
                Image image = (Image)FvEdit_Detail.FindControl("image");

                if (UploadFile.HasFile)
                {
                    string getPathfile = ConfigurationManager.AppSettings["path_emp_profile"];
                    string fileName_upload = ViewState["EmpIDX_Pic"].ToString();
                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                    if (!Directory.Exists(filePath_upload))
                    {
                        Directory.CreateDirectory(filePath_upload);
                    }
                    string extension = Path.GetExtension(UploadFile.FileName);

                    UploadFile.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload + extension);

                }
                //GetPathPhoto(ViewState["txtidcard"].ToString(), image);
                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                Select_Fv_Detail(FvEdit_Detail, ViewState["EmpIDX_Pic"].ToString());
                linkFvTrigger(FvEdit_Detail);

                break;


        }
    }
    #endregion

}