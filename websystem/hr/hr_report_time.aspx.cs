﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using Point = DotNet.Highcharts.Options.Point;
using System.Drawing;

public partial class websystem_hr_report_time : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();
    
    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();
    data_emps _dataEmps = new data_emps();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetEmployeelist = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployeeList"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetCountryList = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountryList"];
    static string _urlGetProvincetList = _serviceUrl + ConfigurationManager.AppSettings["urlGetProvincetList"];
    static string _urlGetAmphurList = _serviceUrl + ConfigurationManager.AppSettings["urlGetAmphurList"];
    static string _urlGetDistList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDistList"];
    static string _urlGetApproveLonList = _serviceUrl + ConfigurationManager.AppSettings["urlGetApproveLonList"];
    static string _UrlSetEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["UrlSetEmployeeList"];
    static string _urlSetODSPList = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList"];
    static string _urlSetODSPList_Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Update"];
    static string _urlSetODSPList_Delete = _serviceUrl + ConfigurationManager.AppSettings["urlSetODSPList_Delete"];
    static string _urlGetChildList = _serviceUrl + ConfigurationManager.AppSettings["urlGetChildList"];
    static string _urlGetPriorList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPriorList"];
    static string _urlGetEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetEducationList"];
    static string _urlGetTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTrainList"];
    static string _urlSetUpdateChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateChildList"];
    static string _urlSetUpdatePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdatePriorList"];
    static string _urlSetUpdateEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateEducationList"];
    static string _urlSetUpdateTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateTrainList"];
    static string _urlSetDeleteChildList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteChildList"];
    static string _urlSetDeletePriorList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeletePriorList"];
    static string _urlSetDeleteEducationList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEducationList"];
    static string _urlSetDeleteTrainList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteTrainList"];
    static string _urlSetDeleteEmployeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteEmployeeList"];
    static string _urlSetResetpass_employeeListList = _serviceUrl + ConfigurationManager.AppSettings["urlSetResetpass_employeeList"];
    static string _urlurlSetInsertEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsertEmployeeList"];
    static string _urlSetUpdateDetail_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateDetail_employeeList"];
    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string _urlSetupdateapprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetupdateapprove_employeeList"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];
    static string _urlGetReferList = _serviceUrl + ConfigurationManager.AppSettings["urlGetReferList"];
    static string _urlSetUpdateReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateReferList"];
    static string _urlSetDeleteReferList = _serviceUrl + ConfigurationManager.AppSettings["urlSetDeleteReferList"];

    static string _urlGetSelectReportTime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectReportTime"];
    static string _urlGetSelectDetailReportTime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectDetailReportTime"];
    static string _urlGetSelectParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectParttime"];


    static string imgPath = ConfigurationSettings.AppSettings["path_flie_employee"];

    int emp_idx = 0;
    int defaultInt = 0;
    decimal tot_actual_all_time = 0;
    decimal tot_actual_time_in = 0;
    decimal tot_actual_time_rate = 0;
    decimal tot_actual_time_out = 0;
    decimal tot_actual_time_leave = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        ViewState["RPos_idx_login"] = 0;
        ViewState["Org_idx_login"] = 0;

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + Session["emp_idx"].ToString());
        ViewState["RPos_idx_login"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Org_idx_login"] = _dtEmployee.employee_list[0].org_idx;

        if (
            (int.Parse(ViewState["RPos_idx_login"].ToString()) == 1017 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1018 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 1019 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5916 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5901 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5899 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5900 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5902 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5884 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5903 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5904 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5905 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5906 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5907 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5909 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5872 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5908 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5873 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5874 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5875 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5876 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5885 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5886 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5888 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5989 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 3543 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 377 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 378 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 867 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5947 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5945 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7135 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7086 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8150 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7108 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 7062 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5891 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5878 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 8153 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 5964 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9182 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 9199 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 4699 ||
            int.Parse(ViewState["RPos_idx_login"].ToString()) == 198
            )
            || int.Parse(ViewState["Org_idx_login"].ToString()) != 1
            )
        {

        }
        else
        {
            _funcTool.showAlert(this, "คุณไม่มีสิทธิ์ใช้งาน");
            Response.Redirect("http://mas.taokaenoi.co.th/");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();
            //Select_Employee_index();
            Menu_Color(4);
            MvMaster.SetActiveView(ViewReport);

        }

        linkBtnTrigger(btnsearch_report);
    }


    #region INSERT&SELECT&UPDATE

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;

        ViewState["Box_dataEmployee_Report_0"] = null;
        ViewState["Box_dataEmployee_Report_1"] = null;
        ViewState["Box_dataEmployee_Report_2"] = null;
        ViewState["Box_dataEmployee_Report_3"] = null;
        ViewState["Box_dataEmployee_Report_4"] = null;
        ViewState["Box_dataEmployee_Report_5"] = null;

        ViewState["txtempcode_s"] = "";
        ViewState["txtempname_s"] = "";
        ViewState["txtdatestart"] = "";
        ViewState["ddlorg_rp"] = "";
        ViewState["ddldep_rp"] = "";
        ViewState["ddlsec_rp"] = "";
        ViewState["ddlemptype_s"] = "";
        ViewState["ddllocation"] = "";
        ViewState["empshif_idx"] = 1;
        //Fv_Search_Emp_Index.ChangeMode(FormViewMode.Insert);
        //Fv_Search_Emp_Index.DataBind();
    }

    protected void Select_Employee_report_search()
    {
        FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        //TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
        DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

        ViewState["txtempcode_s"] = txtempcode_s.Text;
        ViewState["txtempname_s"] = txtempname_s.Text;
        ViewState["txtdatestart"] = txtdatestart.Text;
        ViewState["ddlorg_rp"] = int.Parse(ddlorg_rp.SelectedValue);
        ViewState["ddldep_rp"] = int.Parse(ddldep_rp.SelectedValue);
        ViewState["ddlsec_rp"] = int.Parse(ddlsec_rp.SelectedValue);
        ViewState["ddlemptype_s"] = int.Parse(ddlemptype_s.SelectedValue);
        ViewState["ddllocation"] = int.Parse(ddllocation.SelectedValue);
        ViewState["empshif_idx"] = int.Parse(ddlparttime.SelectedValue);

        _dataEmployee.employee_list_report_time = new employee_detail_time[1];
        employee_detail_time _dataEmployee_ = new employee_detail_time();

        _dataEmployee_.emp_code = txtempcode_s.Text;
        _dataEmployee_.emp_name_th = txtempname_s.Text;
        _dataEmployee_.datetime_start = txtdatestart.Text;
        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation.SelectedValue);
        _dataEmployee_.empshif_idx = int.Parse(ddlparttime.SelectedValue);
        //_dataEmployee_.empshif_idx = 1; //int.Parse(ddlsec_rp.SelectedValue);

        _dataEmployee.employee_list_report_time[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetSelectReportTime, _dataEmployee);
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //adf.Text = _dataEmployee.return_code;

        if (_dataEmployee.return_code != "1")
        {
            ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list_report_time;
            setGridData(GvEmployee_Report, _dataEmployee.employee_list_report_time);
        }
        else
        {
            setGridData(GvEmployee_Report, null);
        }

    }

    protected void Select_Employee_detailreport_search(string location , int selection , GridView GvView , Label lbl_count)
    {
        _dataEmployee.employee_list_report_time = new employee_detail_time[1];
        employee_detail_time _dataEmployee_ = new employee_detail_time();

        _dataEmployee_.emp_code = ViewState["txtempcode_s"].ToString();
        _dataEmployee_.emp_name_th = ViewState["txtempname_s"].ToString();
        _dataEmployee_.datetime_start = ViewState["txtdatestart"].ToString();
        _dataEmployee_.org_idx = int.Parse(ViewState["ddlorg_rp"].ToString());
        _dataEmployee_.rdept_idx = int.Parse(ViewState["ddldep_rp"].ToString());
        _dataEmployee_.rsec_idx = int.Parse(ViewState["ddlsec_rp"].ToString());
        _dataEmployee_.emp_type_idx = int.Parse(ViewState["ddlemptype_s"].ToString());
        //_dataEmployee_.LocIDX = location;
        _dataEmployee_.LocName = location;
        _dataEmployee_.empshif_idx = int.Parse(ViewState["empshif_idx"].ToString());
        _dataEmployee_.Select_action = selection;  //21 -- time_all_sum

        _dataEmployee.employee_list_report_time[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetSelectDetailReportTime, _dataEmployee);

        ViewState["Box_dataEmployee_Report_0"] = _dataEmployee.employee_list;
        if (ViewState["Box_dataEmployee_Report_0"] != null)
        {
            employee_detail[] _templist_cheackMat = (employee_detail[])ViewState["Box_dataEmployee_Report_0"];
            var _linqCheckMat = (from dataMat in _templist_cheackMat

                                 select new
                                 {
                                     dataMat
                                 }).ToList();
            lbl_count.Text = "จำนวน : " + _linqCheckMat.Count().ToString() + " รายการ";
        }
        else
        {
            lbl_count.Text = "จำนวน : " + "0" + " รายการ";
        }

        switch (selection)
        {
            case 1: //time_all_sum
                ViewState["Box_dataEmployee_Report_1"] = _dataEmployee.employee_list;
                //lbl_txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
                break;
            case 2: //time_in
                ViewState["Box_dataEmployee_Report_2"] = _dataEmployee.employee_list;
                //lbl_txt.Text = "2";
                break;
            case 3: //time_out
                ViewState["Box_dataEmployee_Report_3"] = _dataEmployee.employee_list;
                //lbl_txt.Text = "3";
                break;
            case 4: //time_in_rate
                ViewState["Box_dataEmployee_Report_4"] = _dataEmployee.employee_list;
                //lbl_txt.Text = "4";
                break;
            case 5: //time_leave
                ViewState["Box_dataEmployee_Report_5"] = _dataEmployee.employee_list;
                //lbl_txt.Text = "5";
                break;
        }

        //adf.Text = ViewState["Box_dataEmployee_Report_1"].ToString();

        if (_dataEmployee != null)
        {
            //ViewState["Box_dataEmployee_DetailReport"] = _dataEmployee.employee_list;
            setGridData(GvView, _dataEmployee.employee_list);
        }
        else
        {
            setGridData(GvView, null);
        }
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected void Select_Location(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกสำนักงาน....", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail search = new employee_detail();

        search.org_idx = int.Parse(ViewState["Org_idx"].ToString());

        _dtEmployee.employee_list[0] = search;

        _dtEmployee = callServiceEmployee(_urlSelect_Location, _dtEmployee);

        ddlName.DataSource = _dtEmployee.employee_list;
        ddlName.DataTextField = "LocName";
        ddlName.DataValueField = "LocIDX";
        ddlName.DataBind();
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    public void Select_Chart_Report()
    {
        FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
        DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

        _dataEmployee.employee_list_report_time = new employee_detail_time[1];
        employee_detail_time _dataEmployee_ = new employee_detail_time();

        _dataEmployee_.emp_code = txtempcode_s.Text;
        _dataEmployee_.emp_name_th = txtempname_s.Text;
        _dataEmployee_.datetime_start = txtdatestart.Text;
        //_dataEmployee_.datetime_end = txtdateend.Text;
        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation.SelectedValue);
        _dataEmployee_.empshif_idx = int.Parse(ddlparttime.SelectedValue);

        _dataEmployee.employee_list_report_time[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetSelectReportTime, _dataEmployee);
        //ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list_report_time;
        //_dataEmployee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _dataEmployee);

        if (_dataEmployee.return_code == "0")
        {
            //int count = _dataEmployee.employee_list_report_time.Length;
            //string[] lv1code = new string[count];
            //object[] lv1count = new object[count];
            //string[] lv2code = new string[count];
            //int i = 0;
            //foreach (var data in _dataEmployee.employee_list_report_time)
            //{
                string time_sum_all = _dataEmployee.employee_list_report_time[0].time_all_sum.ToString();
                string time_in = _dataEmployee.employee_list_report_time[0].time_in.ToString();
                string time_out = _dataEmployee.employee_list_report_time[0].time_out.ToString();
                string time_rate = _dataEmployee.employee_list_report_time[0].time_in_rate.ToString();
                string time_leave = _dataEmployee.employee_list_report_time[0].time_leave.ToString();
                string location = _dataEmployee.employee_list_report_time[0].LocName.ToString();
            //i++;
            //}
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = new[] { "พนักงานทั้งหมด" , "สแกนนิ้วเข้าทั้งหมด" , "สแกนนิ้วออกทั้งหมด", "เข้าสายทั้งหมด" , "ลาทั้งหมด" } });
            //Highcharts.setOptions({ colors: ['#3B97B2', '#67BC42', '#FF56DE', '#E6D605', '#BC36FE'] });

            chart.SetSeries(
                new Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = location,
                    Data = new Data(new object[] { time_sum_all, time_in, time_out, time_rate, time_leave })
                    
        }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_Chart_all_Report()
    {
        FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
        TextBox txtempcode_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_s");
        TextBox txtempname_s = (TextBox)Fv_Search_Emp_Report.FindControl("txtempname_s");
        TextBox txtdatestart = (TextBox)Fv_Search_Emp_Report.FindControl("txtdatestart");
        //TextBox txtdateend = (TextBox)Fv_Search_Emp_Report.FindControl("txtdateend");

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlemptype_s = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlemptype_s");
        DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
        DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

        _dataEmployee.employee_list_report_time = new employee_detail_time[1];
        employee_detail_time _dataEmployee_ = new employee_detail_time();

        _dataEmployee_.emp_code = txtempcode_s.Text;
        _dataEmployee_.emp_name_th = txtempname_s.Text;
        _dataEmployee_.datetime_start = txtdatestart.Text;
        //_dataEmployee_.datetime_end = txtdateend.Text;
        _dataEmployee_.org_idx = int.Parse(ddlorg_rp.SelectedValue);
        _dataEmployee_.rdept_idx = int.Parse(ddldep_rp.SelectedValue);
        _dataEmployee_.rsec_idx = int.Parse(ddlsec_rp.SelectedValue);
        _dataEmployee_.emp_type_idx = int.Parse(ddlemptype_s.SelectedValue);
        _dataEmployee_.LocIDX = int.Parse(ddllocation.SelectedValue);
        _dataEmployee_.empshif_idx = int.Parse(ddlparttime.SelectedValue);

        _dataEmployee.employee_list_report_time[0] = _dataEmployee_;
        //adf.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        _dataEmployee = callServiceEmployee(_urlGetSelectReportTime, _dataEmployee);
        ViewState["Box_dataEmployee_Report"] = _dataEmployee.employee_list_report_time;
        //_dataEmployee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _dataEmployee);
       // adf.Text = _dataEmployee.return_code.ToString();

        if (_dataEmployee.return_code == "0")
        {
            string time_sum_all_1 = _dataEmployee.employee_list_report_time[0].time_all_sum.ToString();
            string time_in_1 = _dataEmployee.employee_list_report_time[0].time_in.ToString();
            string time_out_1 = _dataEmployee.employee_list_report_time[0].time_out.ToString();
            string time_rate_1 = _dataEmployee.employee_list_report_time[0].time_in_rate.ToString();
            string time_leave_1 = _dataEmployee.employee_list_report_time[0].time_leave.ToString();

            string time_sum_all_2 = _dataEmployee.employee_list_report_time[1].time_all_sum.ToString();
            string time_in_2 = _dataEmployee.employee_list_report_time[1].time_in.ToString();
            string time_out_2 = _dataEmployee.employee_list_report_time[1].time_out.ToString();
            string time_rate_2 = _dataEmployee.employee_list_report_time[1].time_in_rate.ToString();
            string time_leave_2 = _dataEmployee.employee_list_report_time[1].time_leave.ToString();

            string time_sum_all_3 = _dataEmployee.employee_list_report_time[2].time_all_sum.ToString();
            string time_in_3 = _dataEmployee.employee_list_report_time[2].time_in.ToString();
            string time_out_3 = _dataEmployee.employee_list_report_time[2].time_out.ToString();
            string time_rate_3 = _dataEmployee.employee_list_report_time[2].time_in_rate.ToString();
            string time_leave_3 = _dataEmployee.employee_list_report_time[2].time_leave.ToString();

            string time_sum_all_4 = _dataEmployee.employee_list_report_time[3].time_all_sum.ToString();
            string time_in_4 = _dataEmployee.employee_list_report_time[3].time_in.ToString();
            string time_out_4 = _dataEmployee.employee_list_report_time[3].time_out.ToString();
            string time_rate_4 = _dataEmployee.employee_list_report_time[3].time_in_rate.ToString();
            string time_leave_4 = _dataEmployee.employee_list_report_time[3].time_leave.ToString();

            string time_sum_all_5 = _dataEmployee.employee_list_report_time[4].time_all_sum.ToString();
            string time_in_5 = _dataEmployee.employee_list_report_time[4].time_in.ToString();
            string time_out_5 = _dataEmployee.employee_list_report_time[4].time_out.ToString();
            string time_rate_5 = _dataEmployee.employee_list_report_time[4].time_in_rate.ToString();
            string time_leave_5 = _dataEmployee.employee_list_report_time[4].time_leave.ToString();

            string time_sum_all_6 = _dataEmployee.employee_list_report_time[5].time_all_sum.ToString();
            string time_in_6 = _dataEmployee.employee_list_report_time[5].time_in.ToString();
            string time_out_6 = _dataEmployee.employee_list_report_time[5].time_out.ToString();
            string time_rate_6 = _dataEmployee.employee_list_report_time[5].time_in_rate.ToString();
            string time_leave_6 = _dataEmployee.employee_list_report_time[5].time_leave.ToString();

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = new[] { "White WareHouse", "นพวงศ์", "เมืองทอง" , "โรงงานโรจนะ" , "สำนักงาน ดร.โทบิ", "หน้าไม้" } });

            chart.SetSeries(
                new Series[]
                {
                    new Series{
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "พนักงานทั้งหมด", 
                        Data = new Data(new object[] { time_sum_all_1, time_sum_all_2, time_sum_all_3, time_sum_all_4, time_sum_all_5, time_sum_all_6 })
                    },
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "สแกนนิ้วเข้าทั้งหมด", 
                        Data = new Data(new object[] { time_in_1, time_in_2, time_in_3, time_in_4, time_in_5, time_in_6 })
                    },
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "สแกนนิ้วออกทั้งหมด", 
                        Data = new Data(new object[] { time_out_1, time_out_2, time_out_3, time_out_4, time_out_5, time_out_6 })
                    },
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "เข้างานไม่ตรงเวลา", 
                        Data = new Data(new object[] { time_rate_1, time_rate_2, time_rate_3, time_rate_4, time_rate_5, time_rate_6 })
                    },
                    new Series
                    {
                        Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                        Name = "พนักงานลาทั้งหมด",
                        Data = new Data(new object[] { time_leave_1, time_leave_2, time_leave_3, time_leave_4, time_leave_5, time_leave_6 })
                    }

                }
                
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        //return DateTime.ParseExact(dateIN, "dd/MM/yyyy", null).Month.ToString();
        return DateTime.ParseExact(dateIN, "MM", null).Month.ToString();
    }

    protected void select_parttime(DropDownList ddlName)
    {
        //ddlparttime
        _dataEmps.emps_parttime_action = new parttime[1];
        parttime dtemployee_ = new parttime();

        _dataEmps.emps_parttime_action[0] = dtemployee_;

        _dataEmps = callService(_urlGetSelectParttime, _dataEmps);
        setDdlData(ddlName, _dataEmps.emps_parttime_action, "parttime_name_th", "m0_parttime_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะการทำงาน....", "0"));
    }

    #endregion


    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "Fv_Search_Emp_Report":
                FormView Fv_Search_Emp_Report = (FormView)ViewReport.FindControl("Fv_Search_Emp_Report");
                DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
                DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
                DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
                DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
                DropDownList ddlparttime = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlparttime");

                if (Fv_Search_Emp_Report.CurrentMode == FormViewMode.Insert)
                {
                    select_org(ddlorg_rp);
                    Select_Location(ddllocation);
                    select_parttime(ddlparttime);
                }
                break;
        }
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 4: //Report
                //lbindex.BackColor = System.Drawing.Color.Transparent;
                //lbadd.BackColor = System.Drawing.Color.Transparent;
                //lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.LightGray;
                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                Fv_Search_Emp_Report.DataBind();
                break;
            case 2:

                break;
        }
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployee":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                   
                }
                break;
            case "GvEmployee_Report":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    try
                    {

                        Label lbltime_all_sum = (Label)e.Row.FindControl("lbltime_all_sum");
                    Label lbltime_in = (Label)e.Row.FindControl("lbltime_in");
                    Label lbltime_out = (Label)e.Row.FindControl("lbltime_out");
                    Label lbltime_in_rate = (Label)e.Row.FindControl("lbltime_in_rate");
                    Label lbltime_leave = (Label)e.Row.FindControl("lbltime_leave");

                   
                        decimal value = Convert.ToDecimal(lbltime_all_sum.Text);
                        int n = Convert.ToInt32(value);
                        decimal value1 = Convert.ToDecimal(lbltime_in.Text);
                        int n1 = Convert.ToInt32(value1);
                        decimal value2 = Convert.ToDecimal(lbltime_out.Text);
                        int n2 = Convert.ToInt32(value2);
                        decimal value3 = Convert.ToDecimal(lbltime_in_rate.Text);
                        int n3 = Convert.ToInt32(value3);
                        decimal value4 = Convert.ToDecimal(lbltime_leave.Text);
                        int n4 = Convert.ToInt32(value4);

                        tot_actual_all_time += Convert.ToDecimal(value);
                        tot_actual_time_in += Convert.ToDecimal(value1);
                        tot_actual_time_out += Convert.ToDecimal(value2);
                        tot_actual_time_rate += Convert.ToDecimal(value3);                      
                        tot_actual_time_leave += Convert.ToDecimal(value4);
                    }
                    catch
                    {

                    }

                    
                    
                                     
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_time_all_sum = (Label)e.Row.FindControl("lit_time_all_sum");
                    Label lit_time_in = (Label)e.Row.FindControl("lit_time_in");
                    Label lit_time_out = (Label)e.Row.FindControl("lit_time_out");
                    Label lit_time_in_rate = (Label)e.Row.FindControl("lit_time_in_rate");
                    Label lit_time_leave = (Label)e.Row.FindControl("lit_time_leave");

                    lit_time_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                    lit_time_in.Text = String.Format("{0:N2}", tot_actual_time_in);
                    lit_time_out.Text = String.Format("{0:N2}", tot_actual_time_out);
                    lit_time_in_rate.Text = String.Format("{0:N2}", tot_actual_time_rate);
                    lit_time_leave.Text = String.Format("{0:N2}", tot_actual_time_leave);
                    }
                    catch
                    {

                    }

                }

                break;
        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvEmployee_Report":
                GvEmployee_Report.PageIndex = e.NewPageIndex;
                GvEmployee_Report.DataBind();
                Select_Employee_report_search();
                break;
            case "GvEmployee_time_all_sum":
                GvEmployee_time_all_sum.PageIndex = e.NewPageIndex;
                GvEmployee_time_all_sum.DataBind();
                Select_Employee_detailreport_search(ViewState["Location"].ToString(), 1, GvEmployee_time_all_sum , lbltime_all_sum);
                break;
            case "GvEmployee_time_in":
                GvEmployee_time_in.PageIndex = e.NewPageIndex;
                GvEmployee_time_in.DataBind();
                Select_Employee_detailreport_search(ViewState["Location"].ToString(), 2, GvEmployee_time_in , lbltime_in);
                break;
            case "GvEmployee_time_out":
                GvEmployee_time_out.PageIndex = e.NewPageIndex;
                GvEmployee_time_out.DataBind();
                Select_Employee_detailreport_search(ViewState["Location"].ToString(), 3, GvEmployee_time_out , lbltime_out);
                break;
            case "GvEmployee_time_in_rate":
                GvEmployee_time_in_rate.PageIndex = e.NewPageIndex;
                GvEmployee_time_in_rate.DataBind();
                Select_Employee_detailreport_search(ViewState["Location"].ToString(), 4, GvEmployee_time_in_rate , lbltime_in_rate);                
                break;
            case "GvEmployee_time_leave":
                GvEmployee_time_leave.PageIndex = e.NewPageIndex;
                GvEmployee_time_leave.DataBind();
                Select_Employee_detailreport_search(ViewState["Location"].ToString(), 5, GvEmployee_time_leave , lbltime_leave);
                break;
        }
    }

    #endregion

    #region GvRowEditing
    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "dd":

                break;

        }

    }
    #endregion

    #region GvRowUpdating
    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "dd":

                break;
      
        }

    }

    #endregion

    #region GvRowCancelingEdit
    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "dd":

                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "dd":

                break;
        }

    }
    #endregion

    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "ckreference":

                /*var ckreference = (CheckBox)FvInsertEmp.FindControl("ckreference");
                var GvReferenceAdd = (GridView)FvInsertEmp.FindControl("GvReference");
                var BoxReference = (Panel)FvInsertEmp.FindControl("BoxReference");

                if (ckreference.Checked)
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = true;
                }
                else
                {
                    ViewState["vsTemp_Refer"] = null;

                    var dsRefer = new DataSet();
                    dsRefer.Tables.Add("Refer_add");

                    dsRefer.Tables[0].Columns.Add("ref_fullname", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_position", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_relation", typeof(String));
                    dsRefer.Tables[0].Columns.Add("ref_tel", typeof(String));

                    ViewState["vsTemp_Refer"] = dsRefer;
                    GvReferenceAdd.DataSource = dsRefer.Tables[0];
                    GvReferenceAdd.DataBind();

                    BoxReference.Visible = false;
                    GvReferenceAdd.DataSource = null;
                    GvReferenceAdd.DataBind();
                }*/
                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddlorg_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlorg_rp");
        DropDownList ddldep_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldep_rp");
        DropDownList ddlsec_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlsec_rp");
        DropDownList ddlpos_rp = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlpos_rp");
        DropDownList ddldatetype = (DropDownList)Fv_Search_Emp_Report.FindControl("ddldatetype");
        Panel BoxSearch_Date = (Panel)Fv_Search_Emp_Report.FindControl("BoxSearch_Date");

        

        switch (ddName.ID)
        { 
            case "ddlOrganizationEdit_Select":
                var ddNameOrg = (DropDownList)sender;
                var row = (GridViewRow)ddNameOrg.NamingContainer;

                var ddlOrg = (DropDownList)row.FindControl("ddlOrganizationEdit_Select");
                var ddlDep = (DropDownList)row.FindControl("ddlDepartmentEdit_Select");
                var ddlSec = (DropDownList)row.FindControl("ddlSectionEdit_Select");
                var ddlPos = (DropDownList)row.FindControl("ddlPositionEdit_Select");

                select_dep(ddlDep, int.Parse(ddlOrg.SelectedValue));
                break;
            case "ddlDepartmentEdit_Select":
                var ddNameOrg_1 = (DropDownList)sender;
                var row_1 = (GridViewRow)ddNameOrg_1.NamingContainer;

                var ddlOrg_1 = (DropDownList)row_1.FindControl("ddlOrganizationEdit_Select");
                var ddlDep_1 = (DropDownList)row_1.FindControl("ddlDepartmentEdit_Select");
                var ddlSec_1 = (DropDownList)row_1.FindControl("ddlSectionEdit_Select");
                var ddlPos_1 = (DropDownList)row_1.FindControl("ddlPositionEdit_Select");

                select_sec(ddlSec_1, int.Parse(ddlOrg_1.SelectedValue), int.Parse(ddlDep_1.SelectedValue));
                break;

            case "ddlorg_rp":
                select_dep(ddldep_rp, int.Parse(ddlorg_rp.SelectedValue));
                break;
            case "ddldep_rp":
                select_sec(ddlsec_rp, int.Parse(ddlorg_rp.SelectedValue), int.Parse(ddldep_rp.SelectedValue));
                break;

            case "ddldatetype":
                if (ddldatetype.SelectedValue == "1" || ddldatetype.SelectedValue == "2" || ddldatetype.SelectedValue == "3" || ddldatetype.SelectedValue == "4")
                {
                    BoxSearch_Date.Visible = true;
                }
                else
                {
                    BoxSearch_Date.Visible = false;
                }
                break;
            case "ddllocation":
                LinkButton btnsearch_report = (LinkButton)Fv_Search_Emp_Report.FindControl("btnsearch_report");
                DropDownList ddllocation = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");

                /*if (ddllocation.SelectedValue == "0")
                {
                    btnsearch_report.Visible = false;
                }
                else
                {
                    btnsearch_report.Visible = true;
                }*/            

                break;
        }
    }

    #endregion

    #region callService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        

        return _dataEmployee;
    }

    protected data_employee callServiceEmployee_Check(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;
        //Label2.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        ViewState["return_code"] = _dataEmployee.return_code;
        ViewState["return_sucess"] = _dataEmployee.return_msg;
        
        return _dataEmployee;
    }

    protected data_emps callService(string _cmdUrl, data_emps _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_emps)_funcTool.convertJsonToObject(typeof(data_emps), _localJson);

        return _dtmaster;
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy");
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
  
            case "btnreport":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewReport);              
                break;
       
            case "btn_search_report":
                Select_Employee_report_search();

                DropDownList ddllocation_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddllocation");
                //try
                //{
                    if (ddllocation_search.SelectedValue == "0")
                    {
                        Select_Chart_all_Report();
                    }
                    else
                    {
                        Select_Chart_Report();
                    }  
                //}
                //catch { }
                
                Panel boxchart = (Panel)ViewReport.FindControl("boxchart");
                if (ViewState["Box_dataEmployee_Report"] != null)
                {
                    employee_detail_time[] _templist_cheackMat = (employee_detail_time[])ViewState["Box_dataEmployee_Report"];
                    var _linqCheckMat = (from dataMat in _templist_cheackMat

                                         select new
                                         {
                                             dataMat
                                         }).ToList();

                    lblsum_list.Text = "จำนวน : " + _linqCheckMat.Count().ToString() + " รายการ / ทั้งหมด : " + GvEmployee_Report.PageCount.ToString() + " หน้า";
                    boxchart.Visible = true;
                }
                else
                {
                    lblsum_list.Text = "จำนวน : " + "0" + " รายการ / ทั้งหมด : " + "0" + " หน้า";
                    boxchart.Visible = false;
                }             
                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
      
            case "btnview_detail":
                box_index.Visible = false;
                box_detail.Visible = true;

                Select_Employee_detailreport_search(cmdArg.ToString(), 1, GvEmployee_time_all_sum , lbltime_all_sum);            
                Select_Employee_detailreport_search(cmdArg.ToString(), 2, GvEmployee_time_in , lbltime_in);
                Select_Employee_detailreport_search(cmdArg.ToString(), 3, GvEmployee_time_out , lbltime_out);
                Select_Employee_detailreport_search(cmdArg.ToString(), 4, GvEmployee_time_in_rate , lbltime_in_rate);
                Select_Employee_detailreport_search(cmdArg.ToString(), 5, GvEmployee_time_leave , lbltime_leave);
                ViewState["Location"] = cmdArg.ToString();
                break;
            case "btnreport_back":
                box_index.Visible = true;
                box_detail.Visible = false;
                break;

            case "btnExport":
                switch (int.Parse(cmdArg))
                {
                    case 1: //Time_all_sum
                        GvEmployee_time_all_sum.AllowPaging = false;
                        GvEmployee_time_all_sum.DataSource = ViewState["Box_dataEmployee_Report_1"];
                        GvEmployee_time_all_sum.DataBind();

                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time_all_sum.xls");
                        // Response.Charset = ""; set character 
                        Response.Charset = "utf-8";
                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        Response.ContentType = "application/vnd.ms-excel";

                        Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                        //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                        StringWriter sw = new StringWriter();
                        HtmlTextWriter hw = new HtmlTextWriter(sw);

                        for (int i = 0; i < GvEmployee_time_all_sum.Rows.Count; i++)
                        {
                            //Apply text style to each Row
                            GvEmployee_time_all_sum.Rows[i].Attributes.Add("class", "number2");
                        }
                        GvEmployee_time_all_sum.RenderControl(hw);
                        //Amount is displayed in number format with 2 decimals
                        Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                        break;

                    case 2: //Time_in
                        GvEmployee_time_in.AllowPaging = false;
                        GvEmployee_time_in.DataSource = ViewState["Box_dataEmployee_Report_2"];
                        GvEmployee_time_in.DataBind();

                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time_in.xls");
                        // Response.Charset = ""; set character 
                        Response.Charset = "utf-8";
                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        Response.ContentType = "application/vnd.ms-excel";

                        Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                        //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                        StringWriter sw2 = new StringWriter();
                        HtmlTextWriter hw2 = new HtmlTextWriter(sw2);

                        for (int i = 0; i < GvEmployee_time_in.Rows.Count; i++)
                        {
                            //Apply text style to each Row
                            GvEmployee_time_in.Rows[i].Attributes.Add("class", "number2");
                        }
                        GvEmployee_time_in.RenderControl(hw2);

                        //Amount is displayed in number format with 2 decimals
                        Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                        Response.Output.Write(sw2.ToString());
                        Response.Flush();
                        Response.End();
                        break;
                    case 3: //Time_out
                        GvEmployee_time_out.AllowPaging = false;
                        GvEmployee_time_out.DataSource = ViewState["Box_dataEmployee_Report_3"];
                        GvEmployee_time_out.DataBind();

                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time_out.xls");
                        // Response.Charset = ""; set character 
                        Response.Charset = "utf-8";
                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        Response.ContentType = "application/vnd.ms-excel";

                        Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                        //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                        StringWriter sw3 = new StringWriter();
                        HtmlTextWriter hw3 = new HtmlTextWriter(sw3);

                        for (int i = 0; i < GvEmployee_time_out.Rows.Count; i++)
                        {
                            //Apply text style to each Row
                            GvEmployee_time_out.Rows[i].Attributes.Add("class", "number2");
                        }
                        GvEmployee_time_out.RenderControl(hw3);

                        //Amount is displayed in number format with 2 decimals
                        Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                        Response.Output.Write(sw3.ToString());
                        Response.Flush();
                        Response.End();
                        break;
                    case 4: //Time_in_rate
                        GvEmployee_time_in_rate.AllowPaging = false;
                        GvEmployee_time_in_rate.DataSource = ViewState["Box_dataEmployee_Report_4"];
                        GvEmployee_time_in_rate.DataBind();

                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time_in_rate.xls");
                        // Response.Charset = ""; set character 
                        Response.Charset = "utf-8";
                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        Response.ContentType = "application/vnd.ms-excel";

                        Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                        //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                        StringWriter sw4 = new StringWriter();
                        HtmlTextWriter hw4 = new HtmlTextWriter(sw4);

                        for (int i = 0; i < GvEmployee_time_in_rate.Rows.Count; i++)
                        {
                            //Apply text style to each Row
                            GvEmployee_time_in_rate.Rows[i].Attributes.Add("class", "number2");
                        }
                        GvEmployee_time_in_rate.RenderControl(hw4);

                        //Amount is displayed in number format with 2 decimals
                        Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                        Response.Output.Write(sw4.ToString());
                        Response.Flush();
                        Response.End();
                        break;
                    case 5: //Time_leave
                        GvEmployee_time_leave.AllowPaging = false;
                        GvEmployee_time_leave.DataSource = ViewState["Box_dataEmployee_Report_5"];
                        GvEmployee_time_leave.DataBind();

                        Response.Clear();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport_Time_leave.xls");
                        // Response.Charset = ""; set character 
                        Response.Charset = "utf-8";
                        Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        Response.ContentType = "application/vnd.ms-excel";

                        Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                        Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                        //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                        StringWriter sw5 = new StringWriter();
                        HtmlTextWriter hw5 = new HtmlTextWriter(sw5);

                        for (int i = 0; i < GvEmployee_time_leave.Rows.Count; i++)
                        {
                            //Apply text style to each Row
                            GvEmployee_time_leave.Rows[i].Attributes.Add("class", "number2");
                        }
                        GvEmployee_time_leave.RenderControl(hw5);

                        //Amount is displayed in number format with 2 decimals
                        Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                        Response.Output.Write(sw5.ToString());
                        Response.Flush();
                        Response.End();
                        break;
                }        
                break;
        }
    }
    #endregion

}