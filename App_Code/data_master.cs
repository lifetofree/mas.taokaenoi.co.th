using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml.Serialization;

/// <summary>
/// Summary description for data_master
/// </summary>
[Serializable]
[XmlRoot("data_master")]
public class data_master
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("m0_booking_type_list")]
    public detail_booking_type[] m0_booking_type_list { get; set; }
    [XmlElement("m0_place_type_list")]
    public detail_place_type[] m0_place_type_list { get; set; }
}

#region booking type
[Serializable]
public class detail_booking_type
{
    [XmlElement("midx")]
    public int midx { get; set; }
    [XmlElement("booking_name")]
    public string booking_name { get; set; }
    [XmlElement("booking_status")]
    public int booking_status { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
#endregion booking type

#region place type
[Serializable]
public class detail_place_type
{
    [XmlElement("midx")]
    public int midx { get; set; }
    [XmlElement("place_name")]
    public string place_name { get; set; }
    [XmlElement("place_status")]
    public int place_status { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}
#endregion place type