﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;

public partial class websystem_hr_import_employee_nat : System.Web.UI.Page
{
    function_tool _functionTool = new function_tool();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        ViewState["forSearch"] = null;
        setActiveTab(cmdArg);
    }

    protected void setActiveTab(string activeTab)
    {
        tabDetail.Attributes.Add("class", "xx");
        tabInsert.Attributes.Add("class", "xx");

        //form
        ImportInsert.Visible = false;

        switch (activeTab)
        {
            case "tabDetail":
                tabDetail.Attributes.Add("class", "active");

                break;

            case "tabInsert":
                tabInsert.Attributes.Add("class", "active");
                mvImport_emp_nat.SetActiveView((View)mvImport_emp_nat.FindControl("vImportInsert"));
                _functionTool.setFvData(ImportInsert, FormViewMode.Insert, null);
                ImportInsert.Visible = true;
                break;


        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        char[] delimiterChars = { ',' };
        string[] words = e.CommandArgument.ToString().Split(delimiterChars);
        string id = words[0];
        string name = words[1];

        //form
        ImportInsert.Visible = false;


        switch (cmdName)
        {
            case "cmdCheck":
                if (name == "Insert")
                {
                    _functionTool.setFvData(ImportInsert, FormViewMode.Insert, null);
                    ImportInsert.Visible = true;


                }

                break;
        }

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        FileUpload File_Upload = (FileUpload)ImportInsert.FindControl("File_Upload");

        //string connectionString = "";
        if (File_Upload.HasFile)
        {
            //string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            //string fileExtension = Path.GetExtension(FileUpload1.PostedFile.FileName);
            //string fileLocation = Server.MapPath("~/App_Data/" + fileName);
            //File_Upload.SaveAs(fileLocation);
            //if (fileExtension == ".xls")
            //{
            //    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
            //      fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            //}
            //else if (fileExtension == ".xlsx")
            //{
            //    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" +
            //      fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            //}
            //OleDbConnection con = new OleDbConnection(connectionString);
            //OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Connection = con;
            //OleDbDataAdapter dAdapter = new OleDbDataAdapter(cmd);
            //DataTable dtExcelRecords = new DataTable();
            //con.Open();
            //DataTable dtExcelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            //string getExcelSheetName = dtExcelSheetName.Rows[0]["Table_Name"].ToString();
            //cmd.CommandText = "SELECT * FROM [" + getExcelSheetName + "]";
            //dAdapter.SelectCommand = cmd;
            //dAdapter.Fill(dtExcelRecords);
            //GridView1.DataSource = dtExcelRecords;
            //GridView1.DataBind();
        }
    }
}