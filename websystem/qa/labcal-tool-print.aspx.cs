﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_labcal_tool_print : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _GETPRINTDATE();
    }

    #region call data
    protected void _GETPRINTDATE()
    {

        // grdUserList.DataBind();

        lblPrintDate.Text = DateTime.Now.ToString();

    }

    #endregion
}