using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using zkemkeeper;
using System.IO;

using libzkfpcsharp;
using System.Runtime.InteropServices;

public partial class websystem_fingerscan_devtest : System.Web.UI.Page
{
    #region initial function/data
    function_scan _funcScan = new function_scan();

    int emp_idx = 0;
    int defaultInt = 0;
    IPAddress defaultIP = new IPAddress(0);

    public CZKEM axCZKEM1 = new CZKEM();
    public bool bIsConnected = false;//the boolean value identifies whether the device is connected
    public int iMachineNumber = 1;//the serial number of the device.After connecting

    private string sIp = "";
    private int iPort = 0;
    private string sFromIp = "";
    private int iFromPort = 0;
    private int iFromFingerIndex = 0;
    private string sToIp = "";
    private int iToPort = 0;
    private int iToFingerIndex = 0;

    private string sdwEnrollNumber = "";
    private string sTmpData = "";
    private string sName = "";
    private string sPassword = "";
    private int iPrivilege = 0;
    private bool bEnabled = true;

    private int idwBackupNumber = 0;

    ArrayList deviceStatus = new ArrayList() { "-- select --", "administrator count", "reigster user count", "fingerprint template count", "password count", "the record number of times which administrator perform management", "attendance records number of times", "fingerprint capacity", "user's capacity", "recording capacity" };

    function_tool _funcTool = new function_tool();
    data_times _data_times = new data_times();
    data_times _data_local = new data_times();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlTmSetTemplate = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetTemplate"];
    static string _urlTmGetTemplate = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetTemplate"];
    #endregion initial function/data

    [DllImport("user32.dll", EntryPoint = "SendMessageA")]
    public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Session["emp_idx"] != null && (Session["emp_idx"].ToString() != "172" || Session["emp_idx"].ToString() != "24047")))
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            litResult.Text = String.Empty;

            ddlDeviceStatus.DataSource = deviceStatus;
            ddlDeviceStatus.DataBind();
        }

        litResult.Text = (_funcTool.calcDateDiff("day", DateTime.Parse("2015-10-02 00:00:00"), DateTime.Parse("2019-04-30 00:00:00"))).ToString() + " || " + _funcTool.calYearMonthDay(DateTime.Parse("2015-10-02 00:00:00"), DateTime.Parse("2019-04-30 00:00:00"));
    }

    #region button command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        sIp = IPAddress.TryParse(tbIp.Text.Trim(), out defaultIP) ? tbIp.Text.Trim() : convertHostToIp(tbIp.Text.Trim());
        iPort = int.TryParse(tbPort.Text.Trim(), out defaultInt) ? int.Parse(tbPort.Text.Trim()) : defaultInt;

        sdwEnrollNumber = tbEmpCode.Text;
        sFromIp = IPAddress.TryParse(tbFromIp.Text.Trim(), out defaultIP) ? tbFromIp.Text.Trim() : convertHostToIp(tbFromIp.Text.Trim());
        iFromPort = int.TryParse(tbFromPort.Text.Trim(), out defaultInt) ? int.Parse(tbFromPort.Text.Trim()) : defaultInt;
        iFromFingerIndex = int.TryParse(tbFromFingerIndex.Text.Trim(), out defaultInt) ? int.Parse(tbFromFingerIndex.Text.Trim()) : defaultInt;

        sToIp = IPAddress.TryParse(tbToIp.Text.Trim(), out defaultIP) ? tbToIp.Text.Trim() : convertHostToIp(tbToIp.Text.Trim());
        iToPort = int.TryParse(tbToPort.Text.Trim(), out defaultInt) ? int.Parse(tbToPort.Text.Trim()) : defaultInt;
        iToFingerIndex = int.TryParse(tbToFingerIndex.Text.Trim(), out defaultInt) ? int.Parse(tbToFingerIndex.Text.Trim()) : defaultInt;

        sName = tbName.Text.Trim();
        sPassword = tbPassword.Text.Trim();
        iPrivilege = int.TryParse(tbPrivilege.Text.Trim(), out defaultInt) ? int.Parse(tbPrivilege.Text.Trim()) : defaultInt;
        bEnabled = Convert.ToBoolean(ddlEnabled.SelectedValue);

        switch (cmdName)
        {
            case "cmdConnect":
                if (sIp != "" && iPort != 0)
                {
                    if (lbConnect.Text == "Disconnect")
                    {
                        _funcScan.setMachineDisconnect();
                        bIsConnected = false;
                        lbConnect.Text = "Connect";
                        litResult.Text += "current state : disconnected" + setNewline();
                    }
                    else if (lbConnect.Text == "Connect")
                    {
                        if (_funcScan.setMachineConnect(sIp, iPort))
                        {
                            bIsConnected = true;
                            lbConnect.Text = "Disconnect";
                            litResult.Text += "current state : connected" + setNewline();
                        }
                        else
                        {
                            bIsConnected = false;
                            litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                        }
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetAttLog":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getAttendanceLog(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdDelAttLog":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.delAttendanceLog(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdRestartDevice":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        _funcScan.restartDevice(iMachineNumber);
                        litResult.Text += "restart success" + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetTime":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getDeviceTime(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdSetTime":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.setDeviceTime(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdBeep":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        _funcScan.beep(150);
                        litResult.Text += "Beep 150 ms" + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdCheckStatus":
                if (sIp != "" && iPort != 0)
                {
                    if (_funcScan.setMachineConnect(sIp, iPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += getTextDeviceStatus(int.Parse(ddlDeviceStatus.SelectedIndex.ToString())) + _funcScan.getDeviceStatus(1, int.Parse(ddlDeviceStatus.SelectedIndex.ToString())) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }


                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetTemplate":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdCopyTemplate":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        // litResult.Text += _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, 0) + setNewline();
                        sTmpData = _funcScan.getUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex);

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();

                        if (sToIp != "" && iToPort != 0)
                        {
                            if (_funcScan.setMachineConnect(sToIp, iToPort))
                            {
                                _funcScan.setEnableDevice(iMachineNumber, false);

                                litResult.Text += _funcScan.setUserTemplate(iMachineNumber, sdwEnrollNumber, iToFingerIndex, 1, sTmpData) + setNewline();

                                _funcScan.setEnableDevice(iMachineNumber, true);
                                _funcScan.setMachineDisconnect();
                            }
                            else
                            {
                                litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                            }
                        }
                        else
                        {
                            litResult.Text += "input error" + setNewline();
                        }
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdDeleteTemplate":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.delUserTemplate(iMachineNumber, sdwEnrollNumber, iFromFingerIndex) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetAllTemplate":
                if (sFromIp != "" && iFromPort != 0)
                {
                    DataTable dtEmpCode = new DataTable();
                    dtEmpCode.Columns.Add("emp_code");

                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        //litResult.Text += _funcScan.getAllUserTemplate(iMachineNumber);
                        string _tempList = _funcScan.getAllUserInfo(iMachineNumber);

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();

                        string[] _s1 = _tempList.Split(new string[] { "<br />----------<br />" }, StringSplitOptions.None);
						
                        foreach (string _items in _s1)
                        {
                            string[] _s2 = _items.Split('|');
                            DataRow dr = dtEmpCode.NewRow();
                            dr["emp_code"] = _s2[0];
                            dtEmpCode.Rows.Add(dr);
                            // litResult.Text += _s2[0] + setNewline();
                        }
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }

                    // litResult.Text += dtEmpCode.Rows.Count.ToString() + setNewline();

                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        int _count = 1;
                        string rewriteIP = "" + tbFromIp.Text.Trim().Replace('.','_') + "_";
                        string rewriteDate = "" + DateTime.Now.ToString("yyyyMMddHHmm") + "_";
                        string _file_name = "~/uploadfiles/fingerscan/" + rewriteIP + rewriteDate + _count.ToString()  + ".txt";
                        foreach (DataRow _row in dtEmpCode.Rows)
                        {
                            //litResult.Text += _emp["emp_code"] + setNewline();
                            //litResult.Text += _funcScan.getAllUserTemplate(iMachineNumber);

                            for (int i = 0; i <= 1; i++) //index 0, index 1
                            {
                                if((_count % 1000) == 0)
                                {
                                    _file_name = "~/uploadfiles/fingerscan/" + rewriteIP + rewriteDate + _count.ToString()  + ".txt";
                                }

                                string _tmp = _funcScan.getUserTemplate(iMachineNumber, _row["emp_code"].ToString(), i);
                                if (_tmp != "0" && _tmp.Length > 10)
                                {
                                    // litResult.Text += _tmp + setNewline();
                                    using (StreamWriter _testData = new StreamWriter(Server.MapPath(_file_name), true))
                                    {
                                        _testData.WriteLine(_row["emp_code"].ToString() + "|" + i.ToString() + "|" + _tmp + "\n"); // Write the file.
                                    }

                                    // write to db
                                    _data_times.finger_template_list = new finger_template_detail[1];
                                    finger_template_detail _templateList = new finger_template_detail();
                                    _templateList.m0_idx = 0;
                                    _templateList.emp_code = _row["emp_code"].ToString();
                                    switch(i) 
                                    {
                                        case 0: case 2: case 4: case 6: case 8:
                                            _templateList.index0 = _tmp;
                                            break;
                                        case 1: case 3: case 5: case 7: case 9:
                                            _templateList.index1 = _tmp;
                                            break;
                                        /*case 2:
											_templateList.index2 = _tmp;
											break;
                                        case 3:
											_templateList.index3 = _tmp;
											break;
										case 4:
											_templateList.index4 = _tmp;
											break;
										case 5:
											_templateList.index5 = _tmp;
											break;
										case 6:
											_templateList.index6 = _tmp;
											break;
										case 7:
											_templateList.index7 = _tmp;
											break;
										case 8:
											_templateList.index8 = _tmp;
											break;
										case 9:
											_templateList.index9 = _tmp;
											break;*/
                                    }
                                    _templateList.ip_host = tbFromIp.Text.Trim();
                                    _data_times.finger_template_list[0] = _templateList;
                                    _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);
                                }
                            }

                            _count++;
                        }
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }

                    _funcScan.setEnableDevice(iMachineNumber, true);
                    _funcScan.setMachineDisconnect();
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdFromMachineToDbPerson":
                string[] emp_list = sdwEnrollNumber.Split(',');

                if (sFromIp != "" && iFromPort != 0)
                {
                    foreach (string _row in emp_list)
                    {
                        if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                        {
                            _funcScan.setEnableDevice(iMachineNumber, false);
                            //litResult.Text += _emp["emp_code"] + setNewline();
                            //litResult.Text += _funcScan.getAllUserTemplate(iMachineNumber);

                            string _tmp = _funcScan.getUserTemplate(iMachineNumber, _row.ToString(), iFromFingerIndex);

                            _funcScan.setEnableDevice(iMachineNumber, true);
                            _funcScan.setMachineDisconnect();
                            
                            if (_tmp != "0" && _tmp.Length > 10)
                            {
                                // write to db
                                _data_times.finger_template_list = new finger_template_detail[1];
                                finger_template_detail _templateList = new finger_template_detail();
                                _templateList.m0_idx = 0;
                                _templateList.emp_code = _row.ToString();
                                switch(iFromFingerIndex) 
                                {
                                    case 0: //case 2: case 4: case 6: case 8:
                                        _templateList.index0 = _tmp;
                                        break;
                                    case 1: //case 3: case 5: case 7: case 9:
                                        _templateList.index1 = _tmp;
                                        break;
                                    case 2:
                                        _templateList.index2 = _tmp;
                                        break;
                                    case 3:
                                        _templateList.index3 = _tmp;
                                        break;
                                    case 4:
                                        _templateList.index4 = _tmp;
                                        break;
                                    case 5:
                                        _templateList.index5 = _tmp;
                                        break;
                                    case 6:
                                        _templateList.index6 = _tmp;
                                        break;
                                    case 7:
                                        _templateList.index7 = _tmp;
                                        break;
                                    case 8:
                                        _templateList.index8 = _tmp;
                                        break;
                                    case 9:
                                        _templateList.index9 = _tmp;
                                        break;/**/
                                }
                                _templateList.ip_host = tbFromIp.Text.Trim();
                                _data_times.finger_template_list[0] = _templateList;
                                _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);

                                litResult.Text += _row + " | " + iFromFingerIndex.ToString() + setNewline();
                            }
                        }
                        else
                        {
                            litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                        }
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetUserInfo":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getUserInfo(iMachineNumber, sdwEnrollNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetAllUserInfo":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getAllUserInfo(iMachineNumber);

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdClearAdmin":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.clearAdministrators(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdDelEnroll":
                if (sFromIp != "" && iFromPort != 0)
                {
                    idwBackupNumber = 12; //12:all fingerprint and password data
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.delUserEnroll(iMachineNumber, sdwEnrollNumber, idwBackupNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdDelAllEnroll":
                if (sFromIp != "" && iFromPort != 0)
                {
                    idwBackupNumber = 12; //12:all fingerprint and password data
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.delAllUserEnroll(iMachineNumber, sdwEnrollNumber, idwBackupNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdSetInfo":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.setUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdGetUserCard":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.getCardUser(iMachineNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdSetUserCard":
                if (sFromIp != "" && iFromPort != 0)
                {
                    if (_funcScan.setMachineConnect(sFromIp, iFromPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        litResult.Text += _funcScan.setCardUser(sdwEnrollNumber) + setNewline();

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();
                    }
                    else
                    {
                        litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litResult.Text += "input error" + setNewline();
                }
                break;
            case "cmdFromDbToMachine":
                search_template_detail _templateTransferList = new search_template_detail();
                _templateTransferList.s_emp_code = tbEmpCode.Text.Trim();
                _data_local.search_template_list = new search_template_detail[1];
                _data_local.search_template_list[0] = _templateTransferList;
                _data_local = callServicePostTimes(_urlTmGetTemplate, _data_local);
                // litResult.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                for (int i = 0; i <= 1; i++) //index 0, index 1
                {
                    sdwEnrollNumber = tbEmpCode.Text.Trim();
                    iToFingerIndex = i;
                    
                    // write to db
                    finger_template_detail _templateList = new finger_template_detail();
                    _templateList.m0_idx = 0;
                    _templateList.emp_code = sdwEnrollNumber;
                    switch(i) 
                    {
                        case 0:
                            _templateList.index0 = _data_local.finger_template_list[0].index0;
                            sTmpData = _data_local.finger_template_list[0].index0;
                            break;
                        case 1:
                            _templateList.index1 = _data_local.finger_template_list[0].index1;
                            sTmpData = _data_local.finger_template_list[0].index1;
                            break;
                        // case 2:
                        //     _templateList.index2 = _tmp;
                        //     break;
                        // case 3:
                        //     _templateList.index3 = _tmp;
                        //     break;
                    }
                    _templateList.ip_host = tbToIp.Text.Trim();
                    _data_times.finger_template_list = new finger_template_detail[1];
                    _data_times.finger_template_list[0] = _templateList;
                    // litResult.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                    // transfer template to machine
                    if (sToIp != "" && iToPort != 0 && sTmpData != "")
                    {
                        if (_funcScan.setMachineConnect(sToIp, iToPort))
                        {
                            _funcScan.setEnableDevice(iMachineNumber, false);

                            litResult.Text += _funcScan.setUserTemplate(iMachineNumber, sdwEnrollNumber, iToFingerIndex, 1, sTmpData) + setNewline();

                            _funcScan.setEnableDevice(iMachineNumber, true);
                            _funcScan.setMachineDisconnect();

                            // insert data to DB
                            _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);
                        }
                        else
                        {
                            litResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                        }
                    }
                    else
                    {
                        litResult.Text += "input error" + setNewline();
                    }
                }
                break;
            case "cmdScannerInit":
                int ret = zkfperrdef.ZKFP_ERR_OK;
                if ((ret = zkfp2.Init()) == zkfperrdef.ZKFP_ERR_OK)
                {
                    int nCount = zkfp2.GetDeviceCount();
                    if (nCount > 0)
                    {
                        for (int i = 0; i < nCount; i++)
                        {
                            // cmbIdx.Items.Add(i.ToString());
                        }
                        // cmbIdx.SelectedIndex = 0;
                        // bnInit.Enabled = false;
                        // bnFree.Enabled = true;
                        // bnOpen.Enabled = true;

                        litResult.Text += "Device connected!" + setNewline();
                        // return true;
                    }
                    else
                    {
                        zkfp2.Terminate();
                        litResult.Text += "No device connected!" + setNewline();
                        // MessageBox.Show("No device connected!");

                        // return false;
                    }
                }
                else
                {
                    litResult.Text += "Initialize fail, ret=" + ret + " !" + setNewline();
                    // MessageBox.Show("Initialize fail, ret=" + ret + " !");

                    // return false;
                }
                break;
        }
    }
    #endregion button command

    #region reuse
    private string setNewline()
    {
        return "<br />----------<br />";
    }

    private string getTextDeviceStatus(int dataIn)
    {
        string _tempText = String.Empty;
        switch (dataIn)
        {
            case 1:
                _tempText = " : administrator count = ";
                break;
            case 2:
                _tempText = " : register user count = ";
                break;
            case 3:
                _tempText = " : fingerprint template count = ";
                break;
            case 4:
                _tempText = " : password count = ";
                break;
            case 5:
                _tempText = " : the record number of times which administrator perform management = ";
                break;
            case 6:
                _tempText = " : attendance records number of times = ";
                break;
            case 7:
                _tempText = " : fingerprint capacity = ";
                break;
            case 8:
                _tempText = " : user's capacity = ";
                break;
            case 9:
                _tempText = " : recording capacity = ";
                break;
            default:
                _tempText = "no value";
                break;
        }
        return dataIn.ToString() + _tempText;
    }

    private string convertHostToIp(string hostName)
    {
        IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
        if (hostEntry.AddressList.Length > 0)
        {
            return hostEntry.AddressList[0].ToString();
        }
        else
        {
            return "0";
        }
    }

    protected data_times callServicePostTimes(string _cmdUrl, data_times _data_times)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_times);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_times = (data_times)_funcTool.convertJsonToObject(typeof(data_times), _localJson);

        return _data_times;
    }
    #endregion reuse
}