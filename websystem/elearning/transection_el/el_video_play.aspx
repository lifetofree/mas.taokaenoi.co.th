﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_video_play.aspx.cs" Inherits="websystem_el_video_play" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <script type="text/javascript"> 


        function openModalTraning() {

            $('#DvUpdpnl_modal_training').modal('show');
        }
        function closeModalTraning() {
            $('#DvUpdpnl_modal_training').modal('hide');
        }

    </script>


    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Menu</a>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>
                        <li id="liInsert" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="เพิ่มไฟล์" />
                        </li>

                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

    </asp:Panel>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_ListDataPag" runat="server">

            <asp:Panel ID="pnlListData" runat="server">

                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>สถานที่</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3" runat="server" visible="false">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร</label>
                                    <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร..." />
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="row">

                    <asp:GridView ID="GvListData"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        DataKeyNames="m0_meeting_idx"
                        ShowFooter="false">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="zplace_name" runat="server" Text='<%# Eval("zplace_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตำแหน่งที่ต้องการแสดง" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="zpositon_status_name" runat="server" Text='<%# Eval("zpositon_status_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนไฟล์" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# Eval("zvideo_item") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatus( (int)Eval("meeting_status")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <asp:LinkButton ID="btnDetail_GvListData"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail" CommandArgument='<%# Eval("m0_meeting_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>

                                    <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                        data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                        ID="btnDelete_GvListData"
                                        Visible="false"
                                        CommandName="btnDelete" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_meeting_idx") %>'
                                        OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                    <i class="fa fa-trash"></i>
                                    </asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>

        <asp:View ID="View_Insert" runat="server">

            <asp:UpdatePanel ID="UpdatePnl_Insert" runat="server"
                UpdateMode="Conditional">
                <ContentTemplate>

                    <asp:FormView ID="fvCRUD" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูล</strong></h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                                    <div class="col-md-4">
                                                        <asp:DropDownList ID="ddlu0_course_idx_ref" runat="server"
                                                            AutoPostBack="true"
                                                            OnSelectedIndexChanged="FvDetail_DataBound"
                                                            CssClass="form-control" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="ddlu0_course_idx_ref"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue="0"
                                                            ErrorMessage="กรุณากรอกสถานที่" />
                                                    </div>

                                                    <div class="col-md-6">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                                    <div class="col-md-10">
                                                        <asp:TextBox ID="txtvideo_description" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" MaxLength="250"
                                                            Rows="9"
                                                            Text='<%# Eval("video_description") %>' />
                                                    </div>

                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลำดับ :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtvideo_item" runat="server"
                                                                CssClass="form-control" TextMode="Number" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtvideo_item"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกลำดับ" />
                                                        </div>

                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทไฟล์ :" />
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddlfile_flag" runat="server"
                                                                CssClass="form-control"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="FvDetail_DataBound">
                                                                <asp:ListItem Value="1" Text="รูปภาพ" />
                                                                <asp:ListItem Value="2" Text="วิดีโอ" Selected="True" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlfldvideo_name" runat="server" Visible="true">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ไฟล์วิดีโอ :" />
                                                            <div class="col-md-6">

                                                                <asp:FileUpload ID="fldvideo_name"
                                                                    ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                                    runat="server" CssClass="control-label multi max-1 accept-mp4|mp3|wmv with-preview"></asp:FileUpload>
                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="fldvideo_name"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณาเลือกไฟล์วิดีโอ" />--%>
                                                            </div>

                                                            <div class="col-md-4">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlfldvideo_images" runat="server" Visible="false">
                                                <div class="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="รูปภาพ :" />
                                                            <div class="col-md-6">

                                                                <asp:FileUpload ID="fldvideo_images"
                                                                    ViewStateMode="Enabled" AutoPostBack="true"
                                                                    Font-Size="small" ClientIDMode="Static" runat="server"
                                                                    CssClass="control-label multi max-1 accept-png|jpg|gif|jpeg with-preview"></asp:FileUpload>

                                                            </div>

                                                            <div class="col-md-4">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label1" class="col-md-2 control-labelnotop text_right" runat="server" Text="นาที :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txthh_qty" runat="server" CssClass="form-control" TextMode="Number" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txthh_qty"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกนาที" />
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label3" class="col-md-2 control-labelnotop text_right" runat="server" Text="วินาที :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtnn_qty" runat="server" CssClass="form-control" TextMode="Number" />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtnn_qty"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกวินาที" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label16" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เริ่ม :" />
                                                        <div class="col-md-4">
                                                            <asp:UpdatePanel ID="pnlmeeting_date_from_ins" runat="server">
                                                                <ContentTemplate>
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtmeeting_date_from" runat="server" CssClass="form-control filter-order-from" />
                                                                        <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label18" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สิ้นสุด :" />
                                                        <div class="col-md-4">
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                <ContentTemplate>
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtmeeting_date_to" runat="server" CssClass="form-control filter-order-from" />
                                                                        <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                                        </span>

                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>



                                            <asp:Panel ID="Panel4" runat="server" Visible="true">

                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlcourse_status" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                        </div>
                                    </div>

                                </div>
                        </InsertItemTemplate>


                    </asp:FormView>

                    <%-- End Select --%>

                    <asp:Panel ID="pnlSave" runat="server">

                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-7">


                                        <asp:LinkButton CssClass="btn btn-success" runat="server"
                                            CommandName="btnSaveInsert" OnCommand="btnCommand"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึก"
                                            data-toggle="tooltip" title="บันทึก"
                                            ValidationGroup="btnSaveInsert"
                                            ID="btnSaveInsert" />

                                        <asp:LinkButton CssClass="btn btn-danger"
                                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                            ID="btnCancel"
                                            CommandName="btnCancel" OnCommand="btnCommand" />


                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveInsert" />

                </Triggers>
            </asp:UpdatePanel>

            <br />

        </asp:View>

        <%-- edit --%>

        <asp:View ID="View_Update" runat="server">
            <asp:UpdatePanel ID="UpdatePnl_Update" runat="server"
                UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FormView ID="fv_Update" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>


                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ข้อมูล</strong></h3>
                                </div>
                                <div class="panel-body">

                                    <div class="form-horizontal" role="form">

                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox
                                                            ID="txtu0_course_idx_ref" runat="server"
                                                            CssClass="form-control"
                                                            Visible="false"
                                                            Text='<%# Eval("m0_meeting_idx") %>' />
                                                        <asp:TextBox
                                                            ID="ddlu0_course_idx_ref" runat="server"
                                                            CssClass="form-control"
                                                            Enabled="false"
                                                            Text='<%# Eval("zplace_name") %>' />
                                                    </div>

                                                    <div class="col-md-6">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">

                                                    <asp:Label ID="Label2" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียด :" />
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="txtvideo_description" runat="server" CssClass="form-control"
                                                            TextMode="MultiLine" MaxLength="250"
                                                            Rows="9"
                                                            Text='<%# Eval("video_description") %>' />
                                                    </div>

                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label13" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลำดับ :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtvideo_item" runat="server"
                                                                CssClass="form-control"
                                                                Enabled="false"
                                                                Text='<%# Eval("video_item") %>' />
                                                        </div>

                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label7" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทไฟล์ :" />
                                                        <div class="col-md-4">
                                                            <asp:DropDownList ID="ddlfile_flag" runat="server"
                                                                CssClass="form-control"
                                                                AutoPostBack="true"
                                                                SelectedValue='<%# Eval("file_flag") %>'
                                                                OnSelectedIndexChanged="FvDetail_DataBound">
                                                                <asp:ListItem Value="1" Text="รูปภาพ" />
                                                                <asp:ListItem Value="2" Text="วิดีโอ" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlfldvideo_name" runat="server" Visible="true">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ไฟล์วิดีโอ :" />
                                                            <div class="col-md-4">

                                                                <asp:TextBox ID="txthddfld_video_name" runat="server"
                                                                    Visible="false"
                                                                    Text='<%# Eval("video_name") %>' />


                                                                <asp:FileUpload ID="fldvideo_name"
                                                                    ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                                    runat="server" CssClass="control-label multi max-1 accept-mp4|mp3|wmv with-preview"></asp:FileUpload>

                                                                <video style="" height="150" width="250" controls>
                                                                    <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/mp4">
                                                                    <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/ogg">
                                                                    <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/wmv">
                                                                    Your browser does not support the video tag.
                                                                </video>

                                                            </div>

                                                            <div class="col-md-4">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlfldvideo_images" runat="server" Visible="false">
                                                <div class="row">

                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="รูปภาพ :" />
                                                            <div class="col-md-4">

                                                                <asp:TextBox ID="txthddfld_video_images" runat="server"
                                                                    Visible="false"
                                                                    Text='<%# Eval("video_images") %>' />

                                                                <asp:FileUpload ID="fldvideo_images"
                                                                    ViewStateMode="Enabled" AutoPostBack="true"
                                                                    Font-Size="small" ClientIDMode="Static" runat="server"
                                                                    CssClass="control-label multi max-1 accept-png|jpg|gif|jpeg with-preview"></asp:FileUpload>

                                                                <asp:Image ID="imgStoreList1" runat="server"
                                                                    ImageUrl='<%# getImgUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_images")) %>'
                                                                    CommandName="btnToAddSO" OnCommand="btnCommand"
                                                                    CommandArgument='<%# Eval("m1_idx") %>'
                                                                    Height="150" Width="250" />

                                                            </div>

                                                            <div class="col-md-4">
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>


                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label1" class="col-md-2 control-labelnotop text_right" runat="server" Text="นาที :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txthh_qty" runat="server"
                                                                CssClass="form-control" TextMode="Number"
                                                                Text='<%# Eval("hh_qty")%>' />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                                ValidationGroup="btnSaveUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txthh_qty"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกนาที" />
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label3" class="col-md-2 control-labelnotop text_right" runat="server" Text="วินาที :" />
                                                        <div class="col-md-2">
                                                            <asp:TextBox ID="txtnn_qty" runat="server"
                                                                CssClass="form-control" TextMode="Number"
                                                                Text='<%# Eval("nn_qty")%>' />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                                ValidationGroup="btnSaveUpdate" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtnn_qty"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกวินาที" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label16" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เริ่ม :" />
                                                        <div class="col-md-4">
                                                            <%--<asp:UpdatePanel ID="pnlmeeting_date_from_update" runat="server">
                                                                <ContentTemplate>--%>
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtmeeting_date_from" runat="server"
                                                                    CssClass="form-control filter-order-from"
                                                                    Text='<%# Eval("meeting_date_from")%>' />
                                                                <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>
                                                            </div>
                                                            <%--</ContentTemplate>
                                                            </asp:UpdatePanel>--%>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="form-group">
                                                    <div class="col-md-12">

                                                        <asp:Label ID="Label18" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่สิ้นสุด :" />
                                                        <div class="col-md-4">
                                                            <%--<asp:UpdatePanel ID="pnlmeeting_date_to_update" runat="server">
                                                                <ContentTemplate>--%>
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txtmeeting_date_to" runat="server"
                                                                    CssClass="form-control filter-order-from"
                                                                    Text='<%# Eval("meeting_date_to")%>' />
                                                                <span class="input-group-addon show-order-sale-log-from-onclick">
                                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                                </span>

                                                            </div>
                                                            <%--</ContentTemplate>
                                                            </asp:UpdatePanel>--%>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <asp:Panel ID="Panel4" runat="server" Visible="true">

                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlcourse_status" runat="server"
                                                                    CssClass="form-control"
                                                                    SelectedValue='<%# Eval("meeting_status") %>'>
                                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <%-- End  --%>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>


                    </asp:FormView>

                    <%-- End Select --%>

                    <asp:Panel ID="Panel1" runat="server">


                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-7">

                                        <asp:LinkButton CssClass="btn btn-success" runat="server"
                                            CommandName="btnSaveUpdate" OnCommand="btnCommand"
                                            Text="<i class='fa fa-save fa-lg'></i> บันทึกการเปลี่ยนแปลง"
                                            data-toggle="tooltip" title="บันทึกการเปลี่ยนแปลง"
                                            ID="btnSaveUpdate"
                                            ValidationGroup="btnSaveUpdate" />

                                        <asp:LinkButton CssClass="btn btn-danger"
                                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                            ID="btnUpdateCancel"
                                            CommandName="btnUpdateCancel" OnCommand="btnCommand" />

                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="Server" AssociatedUpdatePanelID="UpdatePnl_Update" DisplayAfter="1">
                <ProgressTemplate>
                    <div>
                        <img src="loading.gif" />&nbsp;Please wait ...
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <br />


        </asp:View>


        <asp:View ID="viewList" runat="server">

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <h4>
                            <asp:Label ID="ltTitle" CssClass="text-justify" runat="server" Text="Title"></asp:Label>
                        </h4>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">
                        <asp:LinkButton ID="btnToInsertDetail" CssClass="btn btn-primary pull-left" runat="server"
                            data-toggle="tooltip" title="เพิ่ม"
                            CommandName="btnToInsertDetail" OnCommand="btnCommand">
                     <i class="fa fa-plus-square"></i> เพิ่ม</asp:LinkButton>
                    </div>
                </div>
            </div>
            <p></p>
            <div class="row menu-list">
                <asp:Repeater ID="rptList" runat="server"
                    OnItemDataBound="rptItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-4 menu-list-row">
                            <figure class="imghvr-slide-up">
                                <asp:Panel ID="Panel5" runat="server">
                                    <div class="menu-list-box" style="border: 3px solid #D9B4B4; height: 340px; width: 347px;">

                                        <video style="border: 3px solid #FFFAFA;" height="250" width="340"
                                            runat="server"
                                            visible='<%# Convert.ToInt32(Eval("file_flag"))==2?true:false %>'
                                            controls>
                                            <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/mp4">
                                            <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/ogg">
                                            <source src='<%# getVideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>' type="video/wmv">
                                            Your browser does not support the video tag.
                                        </video>

                                        <asp:Image ID="imgStoreList1" runat="server"
                                            Visible='<%# Convert.ToInt32(Eval("file_flag"))==1?true:false %>'
                                            ImageUrl='<%# getImgUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_images")) %>'
                                            CommandName="btnToAddSO" OnCommand="btnCommand"
                                            CommandArgument='<%# Eval("m1_idx") %>'
                                            Height="250" Width="340" />

                                        <asp:Panel ID="Panel2" runat="server" Visible="false">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# getImgUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_images")) %>'></asp:Literal>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# getvideoUrl(((int)Eval("m0_meeting_idx")).ToString(),(string)Eval("video_name")) %>'></asp:Literal>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="font-16 font-red">
                                                    <asp:Literal ID="litStoreNameTh" runat="server" Text='<%# Truncate("ลำดับ "+ Eval("video_item") ,43) %>'></asp:Literal>

                                                </span>

                                            </div>
                                            <div class="col-md-12">
                                                <span class="font-16 font-red">
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Truncate("รายละเอียด : "+Eval("video_description"),43) %>'></asp:Literal>

                                                </span>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-7 text-left">
                                                <span class="font-16">สถานะ
                                                    <asp:Literal ID="litStorePrice" runat="server"
                                                        Text='<%# getStatusName( (int)Eval("meeting_status")) %>'>
                                                    </asp:Literal></span>
                                            </div>
                                            <div class="col-md-5 text-right">


                                                <asp:LinkButton ID="btnUpdateDetail" runat="server"
                                                    CssClass="btn btn-warning btn-sm"
                                                    data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                                    OnCommand="btnCommand" CommandName="btnUpdateDetail"
                                                    CommandArgument='<%# Eval("m1_idx") %>'>
                                                    <i class="fa fa-pencil-alt"></i>
                                                </asp:LinkButton>


                                                <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                                    data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                    ID="btnDeleteDetail"
                                                    CommandName="btnDeleteDetail" OnCommand="btnCommand"
                                                    CommandArgument='<%# 
                                                    Convert.ToString(Eval("m1_idx")) + "|" +  
                                                    Convert.ToString(Eval("m0_meeting_idx")) + "|" +  
                                                    Convert.ToString(Eval("video_name")) + "|" +  
                                                    Convert.ToString(Eval("video_images")) 
                                                    %>'
                                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                                    <i class="fa fa-trash"></i>
                                                </asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>
                            </figure>
                            </p>
                        </div>

                    </ItemTemplate>
                </asp:Repeater>

            </div>

            <div class="row">

                <div class="form-group">
                    <div class="col-md-12">

                        <asp:LinkButton CssClass="btn btn-danger"
                            data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                            Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                            ID="btnCancelDetail"
                            CommandName="btnCancel" OnCommand="btnCommand" />

                    </div>
                </div>
            </div>
            <br />
        </asp:View>


    </asp:MultiView>


    <%-- end search needs --%>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_course_no" runat="server" />
    <asp:HiddenField ID="Hddfld_m0_meeting_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_m1_idx" runat="server" />
    <style type="text/css">
        ord_p1 {
            word-w word-wrap: break-word;
        }
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>


    <script runat="server">
        //IsImage = true = it's an image
        //IsImage = true = it's a video
        //assumption = it's always either an image or a video
        private bool IsImage(string filename)
        {
            bool check = false;
            filename = filename.ToLower();
            if (filename.EndsWith(".png") || filename.EndsWith(".jpg") || filename.EndsWith(".png"))
                check = true;
            else if (filename.EndsWith(".avi") || filename.EndsWith(".mp4"))
                check = false;

            return check;
        }
    </script>
    <script type="text/javascript"> 

        function DivClicked(u0id) {

            var btnHidden = $('.btndiv' + u0id);
            btnHidden.click();
        }


        function openModalRePrint() {
            $('#DvRemarkRePrint').modal('show');

        }

        function openModalUpdateCustomer() {
            $('#DvUpdateCustomer').modal('show');

        }

        function openModal1() {
            $('#ordine1').modal('show');

        }

    </script>

    <asp:HiddenField ID="HiddenField1" runat="server" />
</asp:Content>

