﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_Price_Reference : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_itasset _dtitseet = new data_itasset();
    DataSupportIT _dtsupport = new DataSupportIT();

    string _localJson = String.Empty;
    string topicit = "*ดุลพินิจในการเลือก Hardware Software และ Email ขึ้นอยู่กับฝ่ายสารสนเทศ ในการตัดสินใจว่าเหมาะสมหรือไม่";
    string topichr = "**ดุลพินิจในการเลือก อุปกรณ์สำนักงาน ขึ้นอยู่กับฝ่ายทรัพยากรบุคคล ในการตัดสินใจว่าเหมาะสมหรือไม่";
    string topicweb = "	** Domain / Hosting อื่นๆ สามารถสอบถามเพิ่มเติมได้ครับ ทาง MIS จะดำเนินการประสานงานให้ครับ";
    string topicen = "*ดุลพินิจในการเลือก Utilities และ Machine Process ขึ้นอยู่กับฝ่ายวิศวกรรม ในการตัดสินใจว่าเหมาะสมหรือไม่";
    string topicsap = "** หากมีข้อสงสัยสามารถสอบถามเพิ่มเติมได้ครับ ทาง MIS จะดำเนินการประสานงานให้ครับ";
    string topicqa = "**ดุลพินิจในการเลือก อุปกรณ์ฝ่ายผลิต ขึ้นอยู่กับฝ่ายประกันคุณภาพ ในการตัดสินใจว่าเหมาะสมหรือไม่";

    int search = 0;
    int[] rsec_hr_sec = { 436, 210 };
    int[] deptit = { 20, 21 };
    int check = 0;
    int rowcheck = 0;
    int approve_check = 0;

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_Price"];
    static string _urlSelectSystem_TypePrice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_TypePrice"];
    static string _urlInsert_Reference = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Reference"];
    static string _urlSelectTypeDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeDevices_Ref"];
    static string _urlSelectSoftware = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSoftware_Ref"];
    static string _urlSelectHROffice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHROffice_Ref"];
    static string _urlSelectENMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectENMachine_Ref"];
    static string _urlSelectQALab_Ref = _serviceUrl + ConfigurationManager.AppSettings["urlSelectQALab_Ref"];
    static string _urlSelectPurchase_Ref = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPurchase_Ref"];
    static string _urlSelectDetailDeviceIT = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetailDeviceIT_Ref"];
    static string _urlSelectSearchPrice = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSearchPrice_Ref"];
    static string _urlSelectDetailForEdit = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetailForEdit_Ref"];
    static string _urlEdit_Ref = _serviceUrl + ConfigurationManager.AppSettings["urlEdit_Ref"];
    static string _urlDelete_Ref = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Ref"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string _urlSelectPos = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPos"];
    static string _urlSelectPermissionDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPermissionDevices"];
    static string _urlDeletePer = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePer"];
    static string _urlSelectHardware_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHardware_Memo"];
    static string _urlSelectSoftware_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSoftware_Memo"];
    static string _urlSelectMonitor_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMonitor_Memo"];
    static string _urlInsert_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Memo"];
    static string _urlSelectHistory_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHistory_Memo"];
    static string _urlSelectHistory_MemoPrint_BuyNormal = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHistory_MemoPrint_BuyNormal"];
    static string _urlSelectHistory_MemoPrintDetail_BuyNormal = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHistory_MemoPrintDetail_BuyNormal"];
    static string _urlSelectUser_Buyreplace_normal = _serviceUrl + ConfigurationManager.AppSettings["urlSelectUser_Buyreplace_normal"];
    static string _urlSelect_Holder = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Holder"];
    static string _urlSelectHistory_Software = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHistory_Software"];
    static string _urlSelectm0type = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm0type"];
    static string _urlSelectm1typewherem0 = _serviceUrl + ConfigurationManager.AppSettings["urlSelectm1typewherem0"];
    static string _urlSelect_DevicesGV = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DevicesGV"];

    static string _urlSelect_History_MemoSpecial = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_History_MemoSpecial"];
    static string _urlSelect_History_DetailMemoSpecial = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_History_DetailMemoSpecial"];
    static string _urlSelect_Approve_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Approve_Memo"];
    static string _urlUpdate_Approve_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Approve_Memo"];
    static string _urlSelect_ApproveDetail_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ApproveDetail_Memo"];
    static string _urlSelect_Log_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Log_Memo"];
    static string _urlSelect_Count_Memo = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Count_Memo"];



    #endregion

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            _divMenuLiToViewIT.Attributes.Add("class", "active");
            ViewState["SysIDX"] = "3";
            lbltopic.Text = topicit;
            SetDefaultIT();
            SelectTypeIT();

        }
        linkBtnTrigger(_divMenuBtnToDivIT);
        linkBtnTrigger(_divMenuBtnToDivHR);
        linkBtnTrigger(_divMenuBtnToDivEN);
        linkBtnTrigger(_divMenuBtnToDivSAP);
        linkBtnTrigger(_divMenuBtnToDivWeb);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        //linkBtnTrigger(_divMenuBtnToDivManual);
        linkBtnTrigger(_divMenuBtnToDivQA);
        linkBtnTrigger(_divMenuBtnToDivWaitingApprove);
        linkBtnTrigger(_divMenuBtnToDivHistory);

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;

    }

    protected void select_empIdx_present_create(int empidx)
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + empidx);

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void select_systemtype(DropDownList ddlName, int sysidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();
        system.SysIDX = sysidx;
        _dtitseet.boxprice_reference[0] = system;

        _dtitseet = callServicePostITAsset(_urlSelectSystem_TypePrice, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "type_name", "typidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

    }

    protected void select_system(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();

        _dtitseet.boxprice_reference[0] = system;
        _dtitseet = callServicePostITAsset(_urlSelectSystem, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "SysNameEN", "SysIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหน่วยงานที่ต้องการ...", "0"));

    }

    protected void select_devices(DropDownList ddlName, int typeidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference system = new price_reference();

        if (typeidx == 1)
        {
            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectTypeDevices, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "m0_tdidx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์...", "0"));
        }
        else if (typeidx == 2)
        {
            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectSoftware, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "software_name_idx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกซอฟแวร์...", "0"));
        }
        else if (typeidx == 4)
        {
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;

            // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlSelectHROffice, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสำนักงาน...", "0"));
        }
        else if (typeidx == 11 || typeidx == 12)
        {
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;

            _dtitseet = callServicePostITAsset(_urlSelectENMachine, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกเครื่องจักร...", "0"));
        }
        else if (typeidx == 17 || typeidx == 18 || typeidx == 19)
        {
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;
            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlSelectQALab_Ref, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์...", "0"));
        }
        else if (typeidx == 20 || typeidx == 21 )
        {
            system.typidx = typeidx;
            _dtitseet.boxprice_reference[0] = system;
            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
            _dtitseet = callServicePostITAsset(_urlSelectPurchase_Ref, _dtitseet);
            setDdlData(ddlName, _dtitseet.boxprice_reference, "typedevices", "tdidx");
            ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์...", "0"));
        }
    }

    protected void SelectTypeIT()
    {

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference dataselect = new price_reference();
        dataselect.SysIDX = int.Parse(ViewState["SysIDX"].ToString());
        _dtitseet.boxprice_reference[0] = dataselect;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectDetailDeviceIT, _dtitseet);
        setGridData(GvType, _dtitseet.boxprice_reference);

    }

    protected void SelectSearchData()
    {

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference dataselect = new price_reference();
        dataselect.SysIDX = int.Parse(ViewState["SysIDX"].ToString());// int.Parse(ddlSearchSystem.SelectedValue);
        dataselect.typidx = int.Parse(ddlSearchTypeDevices.SelectedValue);
        //dataselect.tdidx = int.Parse(ddlSearchDevices.SelectedValue);
        //dataselect.leveldevice = txtSeachLevel.Text;
        //dataselect.use_year = ddlSearchYear.SelectedValue;
        _dtitseet.boxprice_reference[0] = dataselect;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectSearchPrice, _dtitseet);
        setGridData(GvType, _dtitseet.boxprice_reference);

    }

    protected void SelectEditIT()
    {


        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference dataselect = new price_reference();

        dataselect.m0idx = int.Parse(ViewState["m0idx"].ToString());
        dataselect.typidx = int.Parse(ViewState["typidx"].ToString());


        _dtitseet.boxprice_reference[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectDetailForEdit, _dtitseet);
        setFormViewData(FvEditIT, _dtitseet.boxprice_reference);
        DropDownList ddlyearedit = (DropDownList)FvEditIT.FindControl("ddlyearedit");
        TextBox txtyear = (TextBox)FvEditIT.FindControl("txtyear");
        TextBox lbltdidx_it = (TextBox)FvEditIT.FindControl("lbltdidx_it");
        Control divtypedevice = (Control)FvEditIT.FindControl("divtypedevice");

        ddlyearedit.SelectedValue = txtyear.Text;

        if (ViewState["tdidx"].ToString() == "0")
        {
            divtypedevice.Visible = false;
        }
        else
        {
            divtypedevice.Visible = true;
        }

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getPostionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dtEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dtEmployee.position_list[0] = _posList;

        _dtEmployee = callServicePostEmp(_urlGetPositionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_pos(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference pos = new price_reference();

        _dtitseet.boxprice_reference[0] = pos;

        _dtitseet = callServicePostITAsset(_urlSelectPos, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxprice_reference, "pos_name", "posidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง...", "0"));

    }

    protected void Select_PermissionDevice(GridView Gvname, int m0idx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.m0idx = m0idx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectPermissionDevices, _dtitseet);
        setGridData(Gvname, _dtitseet.boxprice_reference);
    }

    protected void SelectHardware_Memo(GridView Gvname, int orgidx, int rdepidx, int rsecidx, int posidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.orgidx = orgidx;
        select.rdeptidx = rdepidx;
        select.rsecidx = rsecidx;
        select.rposidx = posidx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectHardware_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxprice_reference);

    }

    protected void SelectSoftware_Memo(GridView Gvname, int orgidx, int rdepidx, int rsecidx, int posidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.orgidx = orgidx;
        select.rdeptidx = rdepidx;
        select.rsecidx = rsecidx;
        select.rposidx = posidx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectSoftware_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxprice_reference);

    }

    protected void SelectMonitor_Memo(GridView Gvname, int orgidx, int rdepidx, int rsecidx, int posidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.orgidx = orgidx;
        select.rdeptidx = rdepidx;
        select.rsecidx = rsecidx;
        select.rposidx = posidx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectMonitor_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxprice_reference);

    }

    protected void SelectHistory(GridView Gvname, int rdeptidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.rdeptidx = rdeptidx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectHistory_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxu0memo_reference);
    }

    protected void SelectHistory_Detail(GridView Gvname, int u0_meidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference select = new u0memo_reference();

        select.u0_meidx = u0_meidx;

        _dtitseet.boxu0memo_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectHistory_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxu0memo_reference);
    }

    protected void SelectHistory_PrintMemo_BuyNormal(FormView Fvname, int u0_meidx, int condition)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference select = new u0memo_reference();

        select.u0_meidx = u0_meidx;
        select.condition = condition;

        _dtitseet.boxu0memo_reference[0] = select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectHistory_MemoPrint_BuyNormal, _dtitseet);
        setFormViewData(Fvname, _dtitseet.boxu0memo_reference);
    }

    protected void SelectHistory_PrintMemoDetail_BuyNormal(GridView gvname, int u0_meidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference select = new u0memo_reference();

        select.u0_meidx = u0_meidx;

        _dtitseet.boxu0memo_reference[0] = select;


        _dtitseet = callServicePostITAsset(_urlSelectHistory_MemoPrintDetail_BuyNormal, _dtitseet);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtitseet));
        setGridData(gvname, _dtitseet.boxu1memo_n_reference);
    }

    protected void Select_HolderIT(DropDownList ddlName, int cempidx)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือกเครื่องถือครอง....", "0"));

        _dtsupport = new DataSupportIT();

        _dtsupport.BoxUserRequest = new UserRequestList[1];
        UserRequestList closejobit = new UserRequestList();

        closejobit.EmpIDX_add = cempidx;

        _dtsupport.BoxUserRequest[0] = closejobit;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));

        _dtsupport = callServicePostITRepair(_urlSelect_Holder, _dtsupport);

        var rtCode = _dtsupport.ReturnCode;

        if (rtCode.ToString() == "0")
        {
            ddlName.DataSource = _dtsupport.BoxUserRequest;
            ddlName.DataTextField = "u0_code";
            ddlName.DataValueField = "DeviceIDX";
            ddlName.DataBind();

            //   ddlholder.SelectedValue = ViewState["DeviceIDX"].ToString();
        }
        else
        {
            ddlName.AppendDataBoundItems = true;
            ddlName.Items.Clear();
            ddlName.Items.Add(new ListItem("เลือกเครื่องถือครอง ....", "0"));
        }

    }

    protected void Select_HolderSoftware(GridView gvName, int u0_didx)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.u0_didx = u0_didx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelectHistory_Software, _dtitseet);
        setGridData(gvName, _dtitseet.boxu1memo_n_reference);

    }

    protected void select_type(DropDownList ddlName)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference device = new devices_reference();

        _dtitseet.boxdevices_reference[0] = device;

        _dtitseet = callServicePostITAsset(_urlSelectm0type, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxdevices_reference, "name_gen", "m0_naidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรุ่นอุปกรณ์...", "0"));

    }

    protected void select_typedevice(GridView gvName, int m0_naidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxdevices_reference = new devices_reference[1];
        devices_reference device = new devices_reference();

        device.m0_naidx = m0_naidx;

        _dtitseet.boxdevices_reference[0] = device;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlSelectm1typewherem0, _dtitseet);
        setGridData(gvName, _dtitseet.boxdevices_reference);
    }

    protected void SelectHistory_PrintMemo_BuySpecial(GridView Gvname, int u0_meidx, int condition)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference select = new u0memo_reference();

        select.u0_meidx = u0_meidx;
        select.condition = condition;

        _dtitseet.boxu0memo_reference[0] = select;

        ////txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelect_History_MemoSpecial, _dtitseet);
        setGridData(Gvname, _dtitseet.boxu1memo_s_reference);

    }

    protected void Selectwaiting_Approve(GridView Gvname, int rdeptidx, int rsecidx, int empidx, int jobgradelevel)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.rdeptidx = rdeptidx;
        select.rsecidx = rsecidx;
        select.joblevel = jobgradelevel;
        select.CEmpIDX = empidx;

        _dtitseet.boxprice_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelect_Approve_Memo, _dtitseet);
        setGridData(Gvname, _dtitseet.boxu0memo_reference);

    }

    protected void SelectLog(Repeater rpname, int u0_meidx)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference select = new u0memo_reference();

        select.u0_meidx = u0_meidx;

        _dtitseet.boxu0memo_reference[0] = select;

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelect_Log_Memo, _dtitseet);
        setRepeaterData(rpname, _dtitseet.boxu0memo_reference);

    }

    protected void select_sumlist(int rdeptidx, int rsecidx, int empidx, int jobgradelevel)
    {
        _dtitseet = new data_itasset();

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference select = new price_reference();

        select.rdeptidx = rdeptidx;
        select.rsecidx = rsecidx;
        select.joblevel = jobgradelevel;
        select.CEmpIDX = empidx;

        _dtitseet.boxprice_reference[0] = select;
        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlSelect_Count_Memo, _dtitseet);

        nav_approve.Text = "<span class='badge progress-bar-danger' >" + _dtitseet.ReturnMsg.ToString() + "</span>";
        nav_approve1.Text = "<span class='badge progress-bar-danger' >" + _dtitseet.ReturnMsg.ToString() + "</span>";

    }

    #endregion

    #region Insert & Edit
    protected void Insert_Ref()
    {
        FileUpload UploadFile = (FileUpload)fv_insert.FindControl("UploadFile");
        DropDownList ddlsystem = (DropDownList)fv_insert.FindControl("ddlsystem");
        TextBox txtlevel = (TextBox)fv_insert.FindControl("txtlevel");
        DropDownList ddltypedevice = (DropDownList)fv_insert.FindControl("ddltypedevice");
        DropDownList ddldevices = (DropDownList)fv_insert.FindControl("ddldevices");
        TextBox txtdetail = (TextBox)fv_insert.FindControl("txtdetail");
        DropDownList ddlyear = (DropDownList)fv_insert.FindControl("ddlyear");
        TextBox txtprice = (TextBox)fv_insert.FindControl("txtprice");
        TextBox txtremark = (TextBox)fv_insert.FindControl("txtremark");
        TextBox txtmaprice = (TextBox)fv_insert.FindControl("txtmaprice");
        int i = 0;

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference datainsert = new price_reference();

        datainsert.SysIDX = int.Parse(ddlsystem.SelectedValue);
        datainsert.leveldevice = txtlevel.Text;
        datainsert.typidx = int.Parse(ddltypedevice.SelectedValue);
        datainsert.tdidx = int.Parse(ddldevices.SelectedValue);
        datainsert.detail = txtdetail.Text;
        datainsert.use_year = ddlyear.SelectedValue;
        datainsert.price = txtprice.Text;
        datainsert.remark = txtremark.Text;
        datainsert.m0status = 1;
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        if (int.Parse(ddlsystem.SelectedValue) == 2)
        {
            datainsert.ma_price = txtmaprice.Text;
        }
        else
        {
            datainsert.ma_price = "0";
        }

        _dtitseet.boxprice_reference[0] = datainsert;

        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new m1price_reference[ds_udoc1_insert.Tables[0].Rows.Count];

        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new m1price_reference();

            if (dtrow["OrgIDX"].ToString() != "0")
            {
                _document1[i].orgidx = int.Parse(dtrow["OrgIDX"].ToString());
                _document1[i].rdeptidx = int.Parse(dtrow["RdeptIDX"].ToString());
                _document1[i].rsecidx = int.Parse(dtrow["SecIDX"].ToString());
                _document1[i].posidx = int.Parse(dtrow["PosIDX"].ToString());

            }
            else
            {
                _document1[i].orgidx = 0;
                _document1[i].rdeptidx = 0;
                _document1[i].rsecidx = 0;
                _document1[i].posidx = int.Parse(dtrow["PosIDX"].ToString());

            }
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            i++;

            _dtitseet.boxm1price_reference = _document1;

        }

        //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlInsert_Reference, _dtitseet);

        ViewState["ReturnCode"] = _dtitseet.ReturnCode;
        if (UploadFile.HasFile)
        {
            string getPathfile = ConfigurationManager.AppSettings["path_flie_price_ref"];
            string fileName_upload = ViewState["ReturnCode"].ToString();
            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }
            string extension = Path.GetExtension(UploadFile.FileName);

            UploadFile.SaveAs(Server.MapPath(getPathfile + ViewState["ReturnCode"].ToString()) + "\\" + fileName_upload + extension);

        }
    }

    protected void Edit_Ref()
    {
        TextBox txteveldevice = (TextBox)FvEditIT.FindControl("txteveldevice");
        TextBox txtprice = (TextBox)FvEditIT.FindControl("txtprice");
        TextBox txtdetail = (TextBox)FvEditIT.FindControl("txtdetail");
        TextBox txtmaprice = (TextBox)FvEditIT.FindControl("txtmaprice");
        TextBox txtremark = (TextBox)FvEditIT.FindControl("txtremark");
        DropDownList ddlyearedit = (DropDownList)FvEditIT.FindControl("ddlyearedit");

        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference datainsert = new price_reference();

        datainsert.leveldevice = txteveldevice.Text;
        datainsert.detail = txtdetail.Text;
        datainsert.use_year = ddlyearedit.SelectedValue;
        datainsert.price = txtprice.Text;
        datainsert.ma_price = txtmaprice.Text;
        datainsert.remark = txtremark.Text;
        datainsert.m0idx = int.Parse(ViewState["m0idx"].ToString());
        datainsert.typidx = int.Parse(ViewState["typidx"].ToString());
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());


        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new m1price_reference[ds_udoc1_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {

            _document1[i] = new m1price_reference();

            if (dtrow["OrgIDX"].ToString() != "0")
            {
                _document1[i].orgidx = int.Parse(dtrow["OrgIDX"].ToString());
                _document1[i].rdeptidx = int.Parse(dtrow["RdeptIDX"].ToString());
                _document1[i].rsecidx = int.Parse(dtrow["SecIDX"].ToString());
                _document1[i].posidx = int.Parse(dtrow["PosIDX"].ToString());
                datainsert.checksearch = 1;
            }
            else
            {
                _document1[i].orgidx = 0;
                _document1[i].rdeptidx = 0;
                _document1[i].rsecidx = 0;
                _document1[i].posidx = int.Parse(dtrow["PosIDX"].ToString());
                datainsert.checksearch = 0;
            }
            _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            i++;


            _dtitseet.boxm1price_reference = _document1;

        }
        _dtitseet.boxprice_reference[0] = datainsert;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlEdit_Ref, _dtitseet);


    }

    protected void Delete_IT()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference datainsert = new price_reference();


        datainsert.m0idx = int.Parse(ViewState["m0idx_delete"].ToString());
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxprice_reference[0] = datainsert;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlDelete_Ref, _dtitseet);
    }

    protected void Delete_Per(int m1idx)
    {
        _dtitseet.boxprice_reference = new price_reference[1];
        price_reference datainsert = new price_reference();


        datainsert.m1idx = m1idx;
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxprice_reference[0] = datainsert;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlDeletePer, _dtitseet);
    }

    protected void Insert_memo(int m0_type_idx, int m0_tmidx)
    {
        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference datainsert = new u0memo_reference();

        datainsert.m0_tmidx = m0_tmidx;
        datainsert.m0_type_idx = m0_type_idx;
        datainsert.unidx = 1;
        datainsert.acidx = 1;
        datainsert.staidx = 1;
        datainsert.doc_status = 1;
        datainsert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        datainsert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        datainsert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        datainsert.rposidx = int.Parse(ViewState["Pos_idx"].ToString());
        datainsert.CempIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxu0memo_reference[0] = datainsert;

        if (m0_tmidx == 1)
        {
            int i = 0;
            var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment_memo"];
            var _document1 = new u1memo_n_reference[ds_udoc1_insert.Tables[0].Rows.Count];


            foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
            {

                _document1[i] = new u1memo_n_reference();

                _document1[i].orgidx = int.Parse(dtrow["orgidx"].ToString());
                _document1[i].rdeptidx = int.Parse(dtrow["rdeptidx"].ToString());
                _document1[i].rsecidx = int.Parse(dtrow["rsecidx"].ToString());
                _document1[i].posidx = int.Parse(dtrow["posidx"].ToString());

                if (m0_type_idx == 1)
                {
                    _document1[i].empidx = 0;
                    _document1[i].u0_didx = 0;
                }
                else if (m0_type_idx == 2)
                {
                    _document1[i].empidx = int.Parse(dtrow["emp_idx"].ToString());
                    _document1[i].u0_didx = int.Parse(dtrow["u0_didx"].ToString());
                }

                _document1[i].hdidx = int.Parse(dtrow["hdidx"].ToString());

                if (dtrow["mtidx"].ToString() != "" && dtrow["mtidx"].ToString() != null)
                {
                    _document1[i].mtidx = int.Parse(dtrow["mtidx"].ToString());
                }
                else
                {
                    _document1[i].mtidx = 0;
                }
                if (dtrow["swidx"].ToString() != "" && dtrow["swidx"].ToString() != null)
                {
                    _document1[i].swidx_comma = dtrow["swidx"].ToString();
                }
                else
                {
                    _document1[i].swidx_comma = "0";
                }

                i++;

                _dtitseet.boxu1memo_n_reference = _document1;


            }
        }
        else if (m0_tmidx == 2)
        {
            int a = 0;
            var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment_memospecial"];
            var _document1 = new u1memo_s_reference[ds_udoc1_insert.Tables[0].Rows.Count];


            foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
            {

                _document1[a] = new u1memo_s_reference();

                _document1[a].orgidx = int.Parse(dtrow["orgidx"].ToString());
                _document1[a].rdeptidx = int.Parse(dtrow["rdeptidx"].ToString());
                _document1[a].rsecidx = int.Parse(dtrow["rsecidx"].ToString());
                _document1[a].posidx = int.Parse(dtrow["posidx"].ToString());
                _document1[a].m0_naidx = int.Parse(dtrow["m0_naidx"].ToString());
                _document1[a].detail = dtrow["detail_more"].ToString();
                _document1[a].remark = dtrow["remark"].ToString();

                string aaa = dtrow["m2_tyidx_comma"].ToString().Replace("<br>", "");
                _document1[a].m2_tyidx_comma = aaa;// dtrow["m2_tyidx_comma"].ToString().Replace("&lt;br&gt", "");

                if (m0_type_idx == 1)
                {
                    _document1[a].empidx = 0;
                    _document1[a].u0_didx = 0;
                }
                else if (m0_type_idx == 2)
                {
                    _document1[a].empidx = int.Parse(dtrow["emp_idx"].ToString());
                    _document1[a].u0_didx = int.Parse(dtrow["u0_didx"].ToString());
                }


                if (dtrow["swidx"].ToString() != "" && dtrow["swidx"].ToString() != null)
                {
                    _document1[a].swidx_comma = dtrow["swidx"].ToString();
                }
                else
                {
                    _document1[a].swidx_comma = "0";
                }


                a++;

                _dtitseet.boxu1memo_s_reference = _document1;
            }
        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlInsert_Memo, _dtitseet);

        ViewState["u0_meidx"] = _dtitseet.ReturnCode.ToString();
        ViewState["CempIDX_Create"] = _dtitseet.ReturnMsg.ToString();
        ViewState["m0_tmidx"] = _dtitseet.ReturnDocCode.ToString();
    }

    protected void Update_memo(int m0_type_idx, int m0_tmidx, int u0_meidx, int nodeidx, int acidx, int staidx, string comment)
    {
        _dtitseet.boxu0memo_reference = new u0memo_reference[1];
        u0memo_reference datainsert = new u0memo_reference();

        datainsert.m0_tmidx = m0_tmidx;
        datainsert.m0_type_idx = m0_type_idx;
        datainsert.unidx = nodeidx;
        datainsert.acidx = acidx;
        datainsert.staidx = staidx;
        datainsert.doc_status = staidx;
        datainsert.CempIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.comment = comment;
        datainsert.u0_meidx = u0_meidx;
        _dtitseet.boxu0memo_reference[0] = datainsert;


        int i = 0;
        var _document1 = new u1memo_s_reference[GvDetail_BuySpecial.Rows.Count];


        foreach (GridViewRow row in GvDetail_BuySpecial.Rows)
        {
            Label lblu1_meidx_s = (Label)row.Cells[0].FindControl("lblu1_meidx_s");
            RadioButtonList rdochoose = (RadioButtonList)row.Cells[5].FindControl("rdochoose");
            TextBox txtcomment = (TextBox)row.Cells[5].FindControl("txtcomment");
            GridView GvComment_Special = (GridView)row.Cells[4].FindControl("GvComment_Special");


            _dtitseet.boxu1memo_s_reference = new u1memo_s_reference[1];
            _document1[i] = new u1memo_s_reference();

            _document1[i].comment_doing = txtcomment.Text;
            _document1[i].u1_meidx_s = int.Parse(lblu1_meidx_s.Text);

            foreach (GridViewRow row1 in GvComment_Special.Rows)
            {
                Literal listaidx_dir = (Literal)row1.Cells[0].FindControl("listaidx_dir");
                Literal litstaidx_it = (Literal)row1.Cells[1].FindControl("litstaidx_it");

                if (listaidx_dir.Text == "3" || litstaidx_it.Text == "3")
                {
                    _document1[i].staidx_doing = 3;

                }
                else
                {
                    _document1[i].staidx_doing = int.Parse(rdochoose.SelectedValue);

                }
            }

            _dtitseet.boxu1memo_s_reference = _document1;
            i++;
        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        _dtitseet = callServicePostITAsset(_urlUpdate_Approve_Memo, _dtitseet);

    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeaterData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);
        return _dtitseet;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);




        return _dtsupport;
    }
    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "FvEditIT":
                FormView FvEditIT = (FormView)ViewIT.FindControl("FvEditIT");

                if (FvEditIT.CurrentMode == FormViewMode.Edit)
                {

                }
                break;

            case "FvDataUser":
                FormView FvDataUser = (FormView)ViewIT.FindControl("FvDataUser");

                if (FvDataUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDataUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDataUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDataUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDataUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDataUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDataUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDataUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDataUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "fvbuynew_normal":
                FormView fvbuynew_normal = (FormView)ViewIT.FindControl("fvbuynew_normal");

                if (fvbuynew_normal.CurrentMode == FormViewMode.Insert)
                {
                    var ddlOrg_buynew = ((DropDownList)fvbuynew_normal.FindControl("ddlOrg_buynew"));
                    var ddlDep_buynew = ((DropDownList)fvbuynew_normal.FindControl("ddlDep_buynew"));
                    var ddlSec_buynew = ((DropDownList)fvbuynew_normal.FindControl("ddlSec_buynew"));
                    var ddlpos_buynew = ((DropDownList)fvbuynew_normal.FindControl("ddlpos_buynew"));


                    getOrganizationList(ddlOrg_buynew);
                    getDepartmentList(ddlDep_buynew, int.Parse(ViewState["Org_idx"].ToString()));
                    getSectionList(ddlSec_buynew, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()));
                    select_pos(ddlpos_buynew);

                    ddlOrg_buynew.SelectedValue = ViewState["Org_idx"].ToString();
                    ddlDep_buynew.SelectedValue = ViewState["rdept_idx"].ToString();
                    ddlSec_buynew.SelectedValue = ViewState["Sec_idx"].ToString();

                    if (rsec_hr_sec.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                    {
                        ddlOrg_buynew.Enabled = true;
                        ddlDep_buynew.Enabled = true;
                        //ddlSec_buynew.Enabled = true;
                    }
                    else
                    {
                        ddlOrg_buynew.Enabled = false;
                        ddlDep_buynew.Enabled = false;
                        //ddlSec_buynew.Enabled = false;
                    }


                }
                break;

            case "FvUserRequest":
                FormView FvUserRequest = (FormView)ViewDetail_History.FindControl("FvUserRequest");

                if (FvUserRequest.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvUserRequest.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvUserRequest.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvUserRequest.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvUserRequest.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvUserRequest.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvUserRequest.FindControl("txtemail"));
                    var txttel = ((TextBox)FvUserRequest.FindControl("txttel"));
                    var txtorg = ((TextBox)FvUserRequest.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    txttel.Text = ViewState["Tel_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                }
                break;


            case "FvTemplate_print":

                if (FvTemplate_print.CurrentMode == FormViewMode.ReadOnly)
                {
                    var GvDetail_Print_BuyNormal = ((GridView)FvTemplate_print.FindControl("GvDetail_Print_BuyNormal"));
                    var GvDetail_Print_BuySpecial = ((GridView)FvTemplate_print.FindControl("GvDetail_Print_BuySpecial"));

                    if (ViewState["m0_tmidx"].ToString() == "1")
                    {
                        SelectHistory_PrintMemoDetail_BuyNormal(GvDetail_Print_BuyNormal, int.Parse(ViewState["u0_meidx"].ToString()));
                    }
                    else if (ViewState["m0_tmidx"].ToString() == "2")
                    {
                        SelectHistory_PrintMemo_BuySpecial(GvDetail_Print_BuySpecial, int.Parse(ViewState["u0_meidx"].ToString()), 2);
                    }
                }
                break;

            case "FvDetailUser_BuySpecial":
                FormView FvDetailUser_BuySpecial = (FormView)ViewIT.FindControl("FvDetailUser_BuySpecial");


                if (FvDetailUser_BuySpecial.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser_BuySpecial.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser_BuySpecial.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }

                break;

            case "FvBuyNew_Special":
                FormView FvBuyNew_Special = (FormView)ViewIT.FindControl("FvBuyNew_Special");

                if (FvBuyNew_Special.CurrentMode == FormViewMode.Insert)
                {
                    var ddlOrg_buynew_s = ((DropDownList)FvBuyNew_Special.FindControl("ddlOrg_buynew_s"));
                    var ddlDep_buynew_s = ((DropDownList)FvBuyNew_Special.FindControl("ddlDep_buynew_s"));
                    var ddlSec_buynew_s = ((DropDownList)FvBuyNew_Special.FindControl("ddlSec_buynew_s"));
                    var ddltypedevice_buynew_s = ((DropDownList)FvBuyNew_Special.FindControl("ddltypedevice_buynew_s"));
                    var ddlPos_buynew_s = ((DropDownList)FvBuyNew_Special.FindControl("ddlPos_buynew_s"));

                    getOrganizationList(ddlOrg_buynew_s);
                    getDepartmentList(ddlDep_buynew_s, int.Parse(ViewState["Org_idx"].ToString()));
                    getSectionList(ddlSec_buynew_s, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()));
                    getPostionList(ddlPos_buynew_s, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()));
                    select_type(ddltypedevice_buynew_s);
                    //select_pos(ddlpos_buynew_s);

                    ddlOrg_buynew_s.SelectedValue = ViewState["Org_idx"].ToString();
                    ddlDep_buynew_s.SelectedValue = ViewState["rdept_idx"].ToString();
                    ddlSec_buynew_s.SelectedValue = ViewState["Sec_idx"].ToString();
                    ddlPos_buynew_s.SelectedValue = ViewState["Pos_idx"].ToString();

                    if (rsec_hr_sec.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                    {
                        ddlOrg_buynew_s.Enabled = true;
                        ddlDep_buynew_s.Enabled = true;
                        // ddlSec_buynew_s.Enabled = true;
                    }
                    else
                    {
                        ddlOrg_buynew_s.Enabled = false;
                        ddlDep_buynew_s.Enabled = false;
                        // ddlSec_buynew_s.Enabled = false;
                    }
                }
                break;


            case "FvBuyReplace_Special":
                FormView FvBuyReplace_Special = (FormView)ViewIT.FindControl("FvBuyReplace_Special");

                if (FvBuyReplace_Special.CurrentMode == FormViewMode.Insert)
                {
                    var ddltypedevice_buyreplace_s = ((DropDownList)FvBuyReplace_Special.FindControl("ddltypedevice_buyreplace_s"));

                    select_type(ddltypedevice_buyreplace_s);
                }

                break;
        }
    }


    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlsystem = (DropDownList)fv_insert.FindControl("ddlsystem");
        DropDownList ddltypedevice = (DropDownList)fv_insert.FindControl("ddltypedevice");
        DropDownList ddldevices = (DropDownList)fv_insert.FindControl("ddldevices");
        DropDownList ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
        DropDownList ddlDep = (DropDownList)fv_insert.FindControl("ddlDep");
        DropDownList ddlSec = (DropDownList)fv_insert.FindControl("ddlSec");
        DropDownList ddlpermission = (DropDownList)fv_insert.FindControl("ddlpermission");

        DropDownList ddlOrg_edit = (DropDownList)FvEditIT.FindControl("ddlOrg_edit");
        DropDownList ddlDep_edit = (DropDownList)FvEditIT.FindControl("ddlDep_edit");
        DropDownList ddlSec_edit = (DropDownList)FvEditIT.FindControl("ddlSec_edit");
        DropDownList ddlperedit = (DropDownList)FvEditIT.FindControl("ddlperedit");
        Control div_showperedit = (Control)FvEditIT.FindControl("div_showperedit");
        Control divdevice_it = (Control)fv_insert.FindControl("divdevice_it");
        Control divma_price = (Control)fv_insert.FindControl("divma_price");
        Control divpermission = (Control)fv_insert.FindControl("divpermission");

        DropDownList ddlOrg_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlOrg_buynew");
        DropDownList ddlDep_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlDep_buynew");
        DropDownList ddlSec_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlSec_buynew");
        DropDownList ddlpos_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlpos_buynew");

        TextBox txtempcode_replace = (TextBox)fvbuyreplace_normal.FindControl("txtempcode_replace");
        Control div_profileemp_replacenormal = (Control)fvbuyreplace_normal.FindControl("div_profileemp_replacenormal");
        TextBox txtorg_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtorg_replacenormal");
        TextBox txtorgidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtorgidx_replacenormal");
        TextBox txtrequesdept_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesdept_replacenormal");
        TextBox txtrequesdeptidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesdeptidx_replacenormal");
        TextBox txtsec_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtsec_replacenormal");
        TextBox txtsecidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtsecidx_replacenormal");
        TextBox txtpos_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtpos_replacenormal");
        TextBox txtrequesname_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesname_replacenormal");
        TextBox txtempidx = (TextBox)fvbuyreplace_normal.FindControl("txtempidx");
        TextBox txtposgroup_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtposgroup_replacenormal");
        TextBox txtposgroupidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtposgroupidx_replacenormal");
        DropDownList ddldevicesholder_replacenormal = (DropDownList)fvbuyreplace_normal.FindControl("ddldevicesholder_replacenormal");

        DropDownList ddlOrg_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlOrg_buynew_s");
        DropDownList ddlDep_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlDep_buynew_s");
        DropDownList ddlSec_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlSec_buynew_s");
        DropDownList ddlPos_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlPos_buynew_s");

        TextBox txtempcode_replace_s = (TextBox)FvBuyReplace_Special.FindControl("txtempcode_replace_s");
        Control div_profileemp_replacespecial = (Control)FvBuyReplace_Special.FindControl("div_profileemp_replacespecial");
        TextBox txtorg_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtorg_replacespecial");
        TextBox txtorgidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtorgidx_replacespecial");
        TextBox txtsec_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtsec_replacespecial");
        TextBox txtpos_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtpos_replacespecial");
        TextBox txtrequesname_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesname_replacespecial");
        TextBox txtempidx_special = (TextBox)FvBuyReplace_Special.FindControl("txtempidx_special");
        TextBox txtposgroup_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposgroup_replacespecial");
        TextBox txtposgroupidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposgroupidx_replacespecial");
        TextBox txtrequesdept_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesdept_replacespecial");
        TextBox txtrequesdeptidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesdeptidx_replacespecial");
        TextBox txtsecidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtsecidx_replacespecial");
        DropDownList ddldevicesholder_replacespecial = (DropDownList)FvBuyReplace_Special.FindControl("ddldevicesholder_replacespecial");
        TextBox txtposidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposidx_replacespecial");
        DropDownList ddltypedevice_buyreplace_s = (DropDownList)FvBuyReplace_Special.FindControl("ddltypedevice_buyreplace_s");

        if (sender is DropDownList)
        {
            DropDownList ddName = (DropDownList)sender;

            switch (ddName.ID)
            {
                case "ddlsystem":
                    select_systemtype(ddltypedevice, int.Parse(ddlsystem.SelectedValue));
                    ddldevices.SelectedValue = "0";

                    if (ddlsystem.SelectedValue == "2")
                    {
                        divma_price.Visible = true;
                    }
                    else
                    {
                        divma_price.Visible = false;
                    }
                    break;

                case "ddltypedevice":
                    if (ddltypedevice.SelectedValue == "1" || ddltypedevice.SelectedValue == "2" || ddltypedevice.SelectedValue == "4"
                        || ddltypedevice.SelectedValue == "11" || ddltypedevice.SelectedValue == "12" || ddltypedevice.SelectedValue == "17" || ddltypedevice.SelectedValue == "18" || ddltypedevice.SelectedValue == "19" || ddltypedevice.SelectedValue == "20" || ddltypedevice.SelectedValue == "21")
                    {
                        divdevice_it.Visible = true;
                        select_devices(ddldevices, int.Parse(ddltypedevice.SelectedValue));
                    }
                    else
                    {
                        divdevice_it.Visible = false;
                    }
                    break;

                case "ddlSearchSystem":

                    select_systemtype(ddlSearchTypeDevices, int.Parse(ddlSearchSystem.SelectedValue));
                    ddlSearchTypeDevices.SelectedValue = "0";
                    // ddlSearchDevices.SelectedValue = "0";
                    break;

                case "ddlOrg":
                    getDepartmentList(ddlDep, int.Parse(ddlOrg.SelectedValue));


                    break;

                case "ddlDep":
                    getSectionList(ddlSec, int.Parse(ddlOrg.SelectedValue), int.Parse(ddlDep.SelectedValue));

                    break;
                case "ddlpermission":
                    if (ddlpermission.SelectedValue == "1")
                    {
                        divpermission.Visible = true;
                    }
                    else
                    {
                        divpermission.Visible = false;

                    }
                    break;

                case "ddlOrg_edit":
                    getDepartmentList(ddlDep_edit, int.Parse(ddlOrg_edit.SelectedValue));

                    break;

                case "ddlDep_edit":
                    getSectionList(ddlSec_edit, int.Parse(ddlOrg_edit.SelectedValue), int.Parse(ddlDep_edit.SelectedValue));

                    break;

                case "ddlperedit":

                    if (ddlperedit.SelectedValue == "1")
                    {
                        div_showperedit.Visible = true;
                    }
                    else
                    {
                        div_showperedit.Visible = false;

                    }
                    break;

                case "ddlOrg_buynew":
                    getDepartmentList(ddlDep_buynew, int.Parse(ddlOrg_buynew.SelectedValue));

                    break;

                case "ddlDep_buynew":
                    getSectionList(ddlSec_buynew, int.Parse(ddlOrg_buynew.SelectedValue), int.Parse(ddlDep_buynew.SelectedValue));
                    break;

                case "ddlOrg_buynew_s":
                    getDepartmentList(ddlDep_buynew_s, int.Parse(ddlOrg_buynew_s.SelectedValue));
                    break;

                case "ddlDep_buynew_s":
                    getSectionList(ddlSec_buynew_s, int.Parse(ddlOrg_buynew_s.SelectedValue), int.Parse(ddlDep_buynew_s.SelectedValue));
                    break;

                case "ddlSec_buynew_s":
                    getPostionList(ddlPos_buynew_s, int.Parse(ddlOrg_buynew_s.SelectedValue), int.Parse(ddlDep_buynew_s.SelectedValue), int.Parse(ddlSec_buynew_s.SelectedValue));
                    break;

                case "ddl_choose":
                    if (ddl_choose.SelectedValue == "1")
                    {
                        foreach (GridViewRow row_special in GvDetail_BuySpecial.Rows)
                        {
                            if (row_special.RowType == DataControlRowType.DataRow)
                            {
                                RadioButtonList rdochoose = (RadioButtonList)row_special.Cells[5].FindControl("rdochoose");


                                rdochoose.SelectedValue = "2";
                            }
                        }
                    }
                    else if (ddl_choose.SelectedValue == "2")
                    {
                        foreach (GridViewRow row_special in GvDetail_BuySpecial.Rows)
                        {
                            if (row_special.RowType == DataControlRowType.DataRow)
                            {
                                RadioButtonList rdochoose = (RadioButtonList)row_special.Cells[5].FindControl("rdochoose");
                                rdochoose.SelectedValue = "3";
                            }
                        }
                    }

                    break;               

            }
        }

        else if (sender is RadioButton)
        {
            RadioButton rdoName = (RadioButton)sender;

            switch (rdoName.ID)
            {
                case "rdohardware":

                    foreach (GridViewRow gvrSelectedRow in GvHardware.Rows)
                    {
                        ((RadioButton)gvrSelectedRow.FindControl("rdohardware")).Checked = false;

                    }
                    RadioButton rbButton = (RadioButton)sender;
                    GridViewRow gvRow = (GridViewRow)rbButton.NamingContainer;
                    ((RadioButton)gvRow.FindControl("rdohardware")).Checked = true;


                    foreach (GridViewRow row_quest in GvHardware.Rows)
                    {
                        if (row_quest.RowType == DataControlRowType.DataRow)
                        {
                            RadioButton rdohardware = (RadioButton)row_quest.Cells[0].FindControl("rdohardware");
                            Label lbltdidx = (Label)row_quest.Cells[1].FindControl("lbltdidx");


                            if (rdohardware.Checked == true && lbltdidx.Text == "2")
                            {
                                SelectMonitor_Memo(GvMonitor, int.Parse(ddlOrg_buynew.SelectedValue), int.Parse(ddlDep_buynew.SelectedValue), int.Parse(ddlSec_buynew.SelectedValue), int.Parse(ddlpos_buynew.SelectedValue));
                                GvMonitor.Visible = true;
                                div_monitor.Visible = true;
                                break;
                            }
                            else
                            {
                                GvMonitor.Visible = false;
                                div_monitor.Visible = false;
                            }
                        }
                    }
                    break;

                case "rdomonitor":

                    foreach (GridViewRow gvrSelectedRow in GvMonitor.Rows)
                    {
                        ((RadioButton)gvrSelectedRow.FindControl("rdomonitor")).Checked = false;

                    }
                    RadioButton rbButton1 = (RadioButton)sender;
                    GridViewRow gvRow1 = (GridViewRow)rbButton1.NamingContainer;
                    ((RadioButton)gvRow1.FindControl("rdomonitor")).Checked = true;

                    break;
            }
        }

        else if (sender is TextBox)
        {
            TextBox txtName = (TextBox)sender;


            switch (txtName.ID)
            {
                case "txtempcode_replace":
                    //SelectUser_Buyreplace_normal(txtempcode_replace.Text);
                    _dtitseet = new data_itasset();

                    _dtitseet.boxprice_reference = new price_reference[1];
                    price_reference select = new price_reference();

                    select.emp_code = txtempcode_replace.Text;

                    _dtitseet.boxprice_reference[0] = select;

                    _dtitseet = callServicePostITAsset(_urlSelectUser_Buyreplace_normal, _dtitseet);

                    if (_dtitseet.ReturnCode.ToString() == "0")
                    {

                        txtorg_replacenormal.Text = _dtitseet.boxprice_reference[0].OrgNameTH.ToString();
                        txtorgidx_replacenormal.Text = _dtitseet.boxprice_reference[0].orgidx.ToString();

                        txtrequesdept_replacenormal.Text = _dtitseet.boxprice_reference[0].DeptNameTH.ToString();
                        txtrequesdeptidx_replacenormal.Text = _dtitseet.boxprice_reference[0].rdeptidx.ToString();

                        txtsec_replacenormal.Text = _dtitseet.boxprice_reference[0].SecNameTH.ToString();
                        txtsecidx_replacenormal.Text = _dtitseet.boxprice_reference[0].rsecidx.ToString();

                        txtpos_replacenormal.Text = _dtitseet.boxprice_reference[0].PosNameTH.ToString();

                        txtrequesname_replacenormal.Text = _dtitseet.boxprice_reference[0].emp_name_th.ToString();
                        txtempidx.Text = _dtitseet.boxprice_reference[0].CEmpIDX.ToString();

                        txtposgroup_replacenormal.Text = _dtitseet.boxprice_reference[0].pos_name.ToString();
                        txtposgroupidx_replacenormal.Text = _dtitseet.boxprice_reference[0].TIDX.ToString();

                        Select_HolderIT(ddldevicesholder_replacenormal, int.Parse(txtempidx.Text));
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด!!! กรุณากรอกข้อมูลรหัสพนักงานใหม่');", true);
                        return;
                    }
                    div_profileemp_replacenormal.Visible = true;

                    break;

                case "txtempcode_replace_s":
                    _dtitseet = new data_itasset();

                    _dtitseet.boxprice_reference = new price_reference[1];
                    price_reference select_s = new price_reference();

                    select_s.emp_code = txtempcode_replace_s.Text;

                    _dtitseet.boxprice_reference[0] = select_s;

                    _dtitseet = callServicePostITAsset(_urlSelectUser_Buyreplace_normal, _dtitseet);

                    if (_dtitseet.ReturnCode.ToString() == "0")
                    {

                        txtorg_replacespecial.Text = _dtitseet.boxprice_reference[0].OrgNameTH.ToString();
                        txtorgidx_replacespecial.Text = _dtitseet.boxprice_reference[0].orgidx.ToString();

                        txtrequesdept_replacespecial.Text = _dtitseet.boxprice_reference[0].DeptNameTH.ToString();
                        txtrequesdeptidx_replacespecial.Text = _dtitseet.boxprice_reference[0].rdeptidx.ToString();

                        txtsec_replacespecial.Text = _dtitseet.boxprice_reference[0].SecNameTH.ToString();
                        txtsecidx_replacespecial.Text = _dtitseet.boxprice_reference[0].rsecidx.ToString();

                        txtpos_replacespecial.Text = _dtitseet.boxprice_reference[0].PosNameTH.ToString();
                        txtposidx_replacespecial.Text = _dtitseet.boxprice_reference[0].rposidx.ToString();

                        txtrequesname_replacespecial.Text = _dtitseet.boxprice_reference[0].emp_name_th.ToString();
                        txtempidx_special.Text = _dtitseet.boxprice_reference[0].CEmpIDX.ToString();

                        txtposgroup_replacespecial.Text = _dtitseet.boxprice_reference[0].pos_name.ToString();
                        txtposgroupidx_replacespecial.Text = _dtitseet.boxprice_reference[0].TIDX.ToString();

                        Select_HolderIT(ddldevicesholder_replacespecial, int.Parse(txtempidx_special.Text));
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด!!! กรุณากรอกข้อมูลรหัสพนักงานใหม่');", true);
                        return;
                    }

                    div_profileemp_replacespecial.Visible = true;
                    divproduct_special.Visible = false;
                    ddltypedevice_buyreplace_s.SelectedValue = "0";

                    break;
            }
        }
    }
    #endregion

    #region Set Default View
    protected void SetDefaultIT()
    {
        _divMenuLiToViewIT.Attributes.Add("class", "active");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        select_sumlist(int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
        _divMenuLiToViewIndex.Attributes.Add("class", "active");
        _divMenuLiToViewBuy_Normal.Attributes.Remove("class");
        _divMenuLiToViewBuy_Special.Attributes.Remove("class");


        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicit;
        div_btn.Visible = true;
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
    }

    protected void SetDefaultHR()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Add("class", "active");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topichr;
        div_btn.Visible = false;
    }

    protected void SetDefaultEN()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Add("class", "active");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicen;
        div_btn.Visible = false;
    }

    protected void SetDefaultSAP()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Add("class", "active");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicsap;
        div_btn.Visible = false;
    }

    protected void SetDefaultWeb()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Add("class", "active");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicweb;
        div_btn.Visible = false;
    }

    protected void SetDefaultQA()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Add("class", "active");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicqa;
        div_btn.Visible = false;
    }

    protected void SetDefaultPurchase()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Add("class", "active");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewIT);
        div_index.Visible = true;
        div_buynormal.Visible = false;
        div_buyspecial.Visible = false;
        select_system(ddlSearchSystem);
        ddlSearchTypeDevices.SelectedValue = "0";
        lbltopic.Text = topicqa;
        div_btn.Visible = false;
    }


    protected void SetDefaultAdd()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Add("class", "active");
        _divMenuLiToViewWaiting.Attributes.Remove("class");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        div_btn.Visible = false;
        MvMaster.SetActiveView(ViewAdd);
        FvDetailUser.ChangeMode(FormViewMode.Insert);
        FvDetailUser.DataBind();

        var ddlsystem = (DropDownList)fv_insert.FindControl("ddlsystem");
        var ddltypedevice = (DropDownList)fv_insert.FindControl("ddltypedevice");
        var ddldevices = (DropDownList)fv_insert.FindControl("ddldevices");
        var ddlyear = (DropDownList)fv_insert.FindControl("ddlyear");
        var ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
        var ddlpos = (DropDownList)fv_insert.FindControl("ddlpos");
        var GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");

        var txtprice = (TextBox)fv_insert.FindControl("txtprice");
        var txtdetail = (TextBox)fv_insert.FindControl("txtdetail");
        var txtremark = (TextBox)fv_insert.FindControl("txtremark");
        var txtlevel = (TextBox)fv_insert.FindControl("txtlevel");
        var divdevice_it = (Control)fv_insert.FindControl("divdevice_it");

        ddltypedevice.SelectedValue = "0";
        ddldevices.SelectedValue = "0";
        ddlyear.SelectedValue = "0";
        txtlevel.Text = "";
        txtprice.Text = "";
        txtdetail.Text = "";
        txtremark.Text = "";

        select_system(ddlsystem);
        getOrganizationList(ddlOrg);
        select_pos(ddlpos);
        divdevice_it.Visible = false;

        CleardataSetFormList(GvReportAdd);
        GvReportAdd.Visible = false;
    }

    protected void SetDefaultHistory()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Add("class", "active");
        _divMenuBtnToViewWaitingApprove.Attributes.Remove("class");
        _divMenuBtnToViewHistory.Attributes.Add("class", "active");

        MvMaster.SetActiveView(ViewHistory);
        SelectHistory(GvHistory, int.Parse(ViewState["rdept_idx"].ToString()));
        setOntop.Focus();
    }

    protected void SetDefaultApprove()
    {
        _divMenuLiToViewIT.Attributes.Remove("class");
        _divMenuLiToViewHR.Attributes.Remove("class");
        _divMenuLiToViewEN.Attributes.Remove("class");
        _divMenuLiToViewSAP.Attributes.Remove("class");
        _divMenuLiToViewWeb.Attributes.Remove("class");
        _divMenuLiToViewQA.Attributes.Remove("class");
        _divMenuLiToViewPurchase.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewWaiting.Attributes.Add("class", "active");
        _divMenuBtnToViewWaitingApprove.Attributes.Add("class", "active");
        _divMenuBtnToViewHistory.Attributes.Remove("class");

        MvMaster.SetActiveView(ViewApprove);
        Selectwaiting_Approve(GvApprove, int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
        select_sumlist(int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
        setOntop.Focus();
    }

    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("OrgName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("OrgIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("DeptName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RdeptIDX", typeof(int));


        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("SecName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("SecIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("PosName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("PosIDX", typeof(int));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form(DropDownList ddlOrg, DropDownList ddlDep, DropDownList ddlSec, DropDownList ddlpos, GridView gvName)
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {

                if (dr["OrgIDX"].ToString() == ddlOrg.SelectedValue &&
                    int.Parse(dr["RdeptIDX"].ToString()) == int.Parse(ddlDep.SelectedValue) &&
                    int.Parse(dr["SecIDX"].ToString()) == int.Parse(ddlSec.SelectedValue) &&
                    int.Parse(dr["PosIDX"].ToString()) == int.Parse(ddlpos.SelectedValue)
                   )
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();


            drContacts["PosIDX"] = ddlpos.SelectedValue;
            drContacts["PosName"] = ddlpos.SelectedItem.Text;

            if (ddlOrg.SelectedValue == "0")
            {
                drContacts["OrgIDX"] = "0";
                drContacts["OrgName"] = "ทุกองค์กร";
            }
            else
            {
                drContacts["OrgIDX"] = ddlOrg.SelectedValue;
                drContacts["OrgName"] = ddlOrg.SelectedItem.Text;
            }

            if (ddlDep.SelectedValue == "0")
            {
                drContacts["RdeptIDX"] = "0";
                drContacts["DeptName"] = "ทุกฝ่าย";
            }
            else
            {
                drContacts["RdeptIDX"] = ddlDep.SelectedValue;
                drContacts["DeptName"] = ddlDep.SelectedItem.Text;
            }


            if (ddlSec.SelectedValue == "0")
            {
                drContacts["SecIDX"] = "0";
                drContacts["SecName"] = "ทุกแผนก";
            }
            else
            {
                drContacts["SecIDX"] = ddlSec.SelectedValue;
                drContacts["SecName"] = ddlSec.SelectedItem.Text;
            }



            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsBuyequipment"] = null;
        GvName.DataSource = ViewState["vsBuyequipment"];
        GvName.DataBind();
        SetViewState_Form();
    }

    protected void SetViewState_FormMemo()
    {

        DataSet dsproductList = new DataSet();
        dsproductList.Tables.Add("dsAddListTable_memo");

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("OrgName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("orgidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("DeptName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("rdeptidx", typeof(int));


        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("SecName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("rsecidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("PosName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("posidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("DevicesName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("u0_didx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("HolderName", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("emp_idx", typeof(int));


        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("Hardwarename", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("hdidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("Monitorname", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("mtidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("Softwarename", typeof(String));
        dsproductList.Tables["dsAddListTable_memo"].Columns.Add("swidx", typeof(String));

        ViewState["vsBuyequipment_memo"] = dsproductList;

    }

    protected void setAddList_Formmemo()
    {

        DropDownList ddlOrg_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlOrg_buynew");
        DropDownList ddlDep_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlDep_buynew");
        DropDownList ddlSec_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlSec_buynew");
        DropDownList ddlpos_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlpos_buynew");

        DropDownList ddldevicesholder_replacenormal = (DropDownList)fvbuyreplace_normal.FindControl("ddldevicesholder_replacenormal");
        TextBox txtorgidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtorgidx_replacenormal");
        TextBox txtrequesdeptidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesdeptidx_replacenormal");
        TextBox txtsecidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtsecidx_replacenormal");
        TextBox txtempidx = (TextBox)fvbuyreplace_normal.FindControl("txtempidx");
        TextBox txtposgroupidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtposgroupidx_replacenormal");

        TextBox txtorg_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtorg_replacenormal");
        TextBox txtrequesdept_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesdept_replacenormal");
        TextBox txtsec_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtsec_replacenormal");
        TextBox txtrequesname_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesname_replacenormal");
        TextBox txtposgroup_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtposgroup_replacenormal");

        if (ViewState["vsBuyequipment_memo"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts_product = (DataSet)ViewState["vsBuyequipment_memo"];



            foreach (DataRow dr in dsContacts_product.Tables["dsAddListTable_memo"].Rows)
            {

                if (dr["DevicesName"].ToString() == ddldevicesholder_replacenormal.SelectedItem.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เครื่องอุปกรณ์นี้ถูกสร้างรายการทดแทนแล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts_product = dsContacts_product.Tables["dsAddListTable_memo"].NewRow();


            foreach (GridViewRow row_hardware in GvHardware.Rows)
            {
                if (row_hardware.RowType == DataControlRowType.DataRow)
                {
                    RadioButton rdohardware = (RadioButton)row_hardware.Cells[0].FindControl("rdohardware");
                    Label lblm0idx = (Label)row_hardware.Cells[1].FindControl("lblm0idx");
                    Label lbname = (Label)row_hardware.Cells[2].FindControl("lbname");
                    Label lbdetail = (Label)row_hardware.Cells[4].FindControl("lbdetail");



                    if (rdohardware.Checked)
                    {
                        drContacts_product["hdidx"] = lblm0idx.Text;
                        drContacts_product["Hardwarename"] = lbname.Text + " " + lbdetail.Text;
                        check = 1;
                    }

                    rdohardware.Checked = false;

                }
            }

            if (check == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกเครื่องอุปกรณ์!!!');", true);
                return;
            }
            else
            {

                foreach (GridViewRow row_monitor in GvMonitor.Rows)
                {
                    if (row_monitor.RowType == DataControlRowType.DataRow)
                    {
                        RadioButton rdomonitor = (RadioButton)row_monitor.Cells[0].FindControl("rdomonitor");
                        Label lblm0idx = (Label)row_monitor.Cells[1].FindControl("lblm0idx");
                        Label lbname = (Label)row_monitor.Cells[2].FindControl("lbname");
                        Label lbdetail = (Label)row_monitor.Cells[4].FindControl("lbdetail");

                        if (rdomonitor.Checked)
                        {
                            drContacts_product["mtidx"] = lblm0idx.Text;
                            drContacts_product["Monitorname"] = lbname.Text + " " + lbdetail.Text;

                        }
                        rdomonitor.Checked = false;
                    }
                }

                foreach (GridViewRow row_software in GvSoftware.Rows)
                {
                    if (row_software.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chksoftware = (CheckBox)row_software.Cells[0].FindControl("chksoftware");
                        Label lblm0idx = (Label)row_software.Cells[1].FindControl("lblm0idx");
                        Label lbname = (Label)row_software.Cells[2].FindControl("lbname");

                        if (chksoftware.Checked)
                        {
                            drContacts_product["swidx"] += lblm0idx.Text + ",";
                            drContacts_product["Softwarename"] += lbname.Text + ",";
                        }
                        chksoftware.Checked = true;
                    }
                }


                if (ViewState["m0_type_idx"].ToString() == "1")
                {
                    drContacts_product["DevicesName"] = "";
                    drContacts_product["u0_didx"] = "0";
                    drContacts_product["HolderName"] = "";
                    drContacts_product["emp_idx"] = "0";

                    drContacts_product["orgidx"] = ddlOrg_buynew.SelectedValue;
                    drContacts_product["OrgName"] = ddlOrg_buynew.SelectedItem.Text;

                    drContacts_product["rdeptidx"] = ddlDep_buynew.SelectedValue;
                    drContacts_product["DeptName"] = ddlDep_buynew.SelectedItem.Text;

                    drContacts_product["rsecidx"] = ddlSec_buynew.SelectedValue;
                    drContacts_product["SecName"] = ddlSec_buynew.SelectedItem.Text;

                    drContacts_product["posidx"] = ddlpos_buynew.SelectedValue;
                    drContacts_product["PosName"] = ddlpos_buynew.SelectedItem.Text;

                }
                else
                {
                    drContacts_product["DevicesName"] = ddldevicesholder_replacenormal.SelectedItem.Text;
                    drContacts_product["u0_didx"] = ddldevicesholder_replacenormal.SelectedValue;
                    drContacts_product["HolderName"] = txtrequesname_replacenormal.Text;
                    drContacts_product["emp_idx"] = txtempidx.Text;

                    drContacts_product["orgidx"] = txtorgidx_replacenormal.Text;
                    drContacts_product["OrgName"] = txtorg_replacenormal.Text;

                    drContacts_product["rdeptidx"] = txtrequesdeptidx_replacenormal.Text;
                    drContacts_product["DeptName"] = txtrequesdept_replacenormal.Text;

                    drContacts_product["rsecidx"] = txtsecidx_replacenormal.Text;
                    drContacts_product["SecName"] = txtsec_replacenormal.Text;

                    drContacts_product["posidx"] = txtposgroupidx_replacenormal.Text;
                    drContacts_product["PosName"] = txtposgroup_replacenormal.Text;

                }



                dsContacts_product.Tables["dsAddListTable_memo"].Rows.Add(drContacts_product);
                ViewState["vsBuyequipment_memo"] = dsContacts_product;
                setGridData(GvProductAdd, dsContacts_product.Tables["dsAddListTable_memo"]);
                GvProductAdd.Visible = true;
                GvMonitor.Visible = false;
                div_monitor.Visible = false;
            }
        }
    }

    protected void CleardataSetFormList_Memo(GridView GvName)
    {
        ViewState["vsBuyequipment_memo"] = null;
        GvName.DataSource = ViewState["vsBuyequipment_memo"];
        GvName.DataBind();
        SetViewState_FormMemo();
    }

    protected void SetViewState_FormMemo_Special()
    {

        DataSet dsproductList = new DataSet();
        dsproductList.Tables.Add("dsAddListTable_memospecial");

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("OrgName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("orgidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("DeptName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("rdeptidx", typeof(int));


        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("SecName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("rsecidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("PosName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("posidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("DevicesName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("u0_didx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("HolderName", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("emp_idx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("name_gen", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("m0_naidx", typeof(int));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("type_name", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("m0_tyidx_comma", typeof(String));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("device_name", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("m2_tyidx_comma", typeof(String));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("Softwarename", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("swidx", typeof(String));

        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("detail_more", typeof(String));
        dsproductList.Tables["dsAddListTable_memospecial"].Columns.Add("remark", typeof(String));


        ViewState["vsBuyequipment_memospecial"] = dsproductList;



    }

    protected void setAddList_Formmemo_Special()
    {

        DropDownList ddlOrg_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlOrg_buynew_s");
        DropDownList ddlDep_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlDep_buynew_s");
        DropDownList ddlSec_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlSec_buynew_s");
        DropDownList ddlPos_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlPos_buynew_s");
        DropDownList ddltypedevice_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddltypedevice_buynew_s");

        TextBox txtorg_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtorg_replacespecial");
        TextBox txtorgidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtorgidx_replacespecial");
        TextBox txtsec_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtsec_replacespecial");
        TextBox txtpos_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtpos_replacespecial");
        TextBox txtposidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposidx_replacespecial");
        TextBox txtrequesname_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesname_replacespecial");
        TextBox txtempidx_special = (TextBox)FvBuyReplace_Special.FindControl("txtempidx_special");
        TextBox txtposgroup_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposgroup_replacespecial");
        TextBox txtposgroupidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtposgroupidx_replacespecial");
        TextBox txtrequesdept_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesdept_replacespecial");
        TextBox txtrequesdeptidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesdeptidx_replacespecial");
        TextBox txtsecidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtsecidx_replacespecial");
        DropDownList ddldevicesholder_replacespecial = (DropDownList)FvBuyReplace_Special.FindControl("ddldevicesholder_replacespecial");
        DropDownList ddltypedevice_buyreplace_s = (DropDownList)FvBuyReplace_Special.FindControl("ddltypedevice_buyreplace_s");


        if (ViewState["vsBuyequipment_memospecial"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts_product = (DataSet)ViewState["vsBuyequipment_memospecial"];

            foreach (DataRow dr in dsContacts_product.Tables["dsAddListTable_memospecial"].Rows)
            {

                if (dr["DevicesName"].ToString() == ddldevicesholder_replacespecial.SelectedItem.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เครื่องอุปกรณ์นี้ถูกสร้างรายการทดแทนแล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts_product = dsContacts_product.Tables["dsAddListTable_memospecial"].NewRow();


            foreach (GridViewRow row_typedevice in GvTypeDevice.Rows)
            {
                if (row_typedevice.RowType == DataControlRowType.DataRow)
                {
                    RadioButtonList rdo_typedevice = (RadioButtonList)row_typedevice.Cells[0].FindControl("rdo_typedevice");
                    Label lbtype_name = (Label)row_typedevice.Cells[0].FindControl("lbtype_name");
                    Label lblm0_tyidx = (Label)row_typedevice.Cells[0].FindControl("lblm0_tyidx");

                    if (rdo_typedevice.SelectedValue != "0")
                    {
                        drContacts_product["m0_tyidx_comma"] += lblm0_tyidx.Text + "," + "<br>";
                        drContacts_product["type_name"] += lbtype_name.Text + "," + "<br>";

                        drContacts_product["device_name"] += rdo_typedevice.SelectedItem.Text + "," + "<br>";
                        drContacts_product["m2_tyidx_comma"] += rdo_typedevice.SelectedValue + "," + "<br>";
                    }
                }

            }


            if (ddltypedevice_buyreplace_s.SelectedValue != "6" && ddltypedevice_buynew_s.SelectedValue != "6")
            {

                foreach (GridViewRow row_software in GvSoftWare_BuySpecial.Rows)
                {
                    if (row_software.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chksoftware = (CheckBox)row_software.Cells[0].FindControl("chksoftware");
                        Label lblm0idx = (Label)row_software.Cells[1].FindControl("lblm0idx");
                        Label lbname = (Label)row_software.Cells[1].FindControl("lbname");

                        if (chksoftware.Checked)
                        {
                            drContacts_product["swidx"] += lblm0idx.Text + ",";
                            drContacts_product["Softwarename"] += lbname.Text + ",";
                        }
                        chksoftware.Checked = true;
                    }
                }
            }
            else
            {
                drContacts_product["swidx"] = "";
                drContacts_product["Softwarename"] = "-";
            }


            if (ViewState["m0_type_idx"].ToString() == "1")
            {
                drContacts_product["DevicesName"] = "";
                drContacts_product["u0_didx"] = "0";
                drContacts_product["HolderName"] = "";
                drContacts_product["emp_idx"] = "0";

                drContacts_product["orgidx"] = ddlOrg_buynew_s.SelectedValue;
                drContacts_product["OrgName"] = ddlOrg_buynew_s.SelectedItem.Text;

                drContacts_product["rdeptidx"] = ddlDep_buynew_s.SelectedValue;
                drContacts_product["DeptName"] = ddlDep_buynew_s.SelectedItem.Text;

                drContacts_product["rsecidx"] = ddlSec_buynew_s.SelectedValue;
                drContacts_product["SecName"] = ddlSec_buynew_s.SelectedItem.Text;

                drContacts_product["posidx"] = ddlPos_buynew_s.SelectedValue;
                drContacts_product["PosName"] = ddlPos_buynew_s.SelectedItem.Text;
                drContacts_product["m0_naidx"] = ddltypedevice_buynew_s.SelectedValue;
                drContacts_product["name_gen"] = ddltypedevice_buynew_s.SelectedItem.Text;
            }
            else
            {
                drContacts_product["DevicesName"] = ddldevicesholder_replacespecial.SelectedItem.Text;
                drContacts_product["u0_didx"] = ddldevicesholder_replacespecial.SelectedValue;
                drContacts_product["HolderName"] = txtrequesname_replacespecial.Text;
                drContacts_product["emp_idx"] = txtempidx_special.Text;

                drContacts_product["orgidx"] = txtorgidx_replacespecial.Text;
                drContacts_product["OrgName"] = txtorg_replacespecial.Text;

                drContacts_product["rdeptidx"] = txtrequesdeptidx_replacespecial.Text;
                drContacts_product["DeptName"] = txtrequesdept_replacespecial.Text;

                drContacts_product["rsecidx"] = txtsecidx_replacespecial.Text;
                drContacts_product["SecName"] = txtsec_replacespecial.Text;

                drContacts_product["posidx"] = txtposidx_replacespecial.Text;
                drContacts_product["PosName"] = txtpos_replacespecial.Text;

                drContacts_product["m0_naidx"] = ddltypedevice_buyreplace_s.SelectedValue;
                drContacts_product["name_gen"] = ddltypedevice_buyreplace_s.SelectedItem.Text;
            }


            drContacts_product["detail_more"] = txtdetail_buyspecial.Text;
            drContacts_product["remark"] = txtremark_buyspecial.Text;

            dsContacts_product.Tables["dsAddListTable_memospecial"].Rows.Add(drContacts_product);
            ViewState["vsBuyequipment_memospecial"] = dsContacts_product;
            setGridData(GvProductSpecialAdd, dsContacts_product.Tables["dsAddListTable_memospecial"]);
            GvProductSpecialAdd.Visible = true;
        }

    }

    protected void CleardataSetFormList_Memo_Special(GridView GvName)
    {
        ViewState["vsBuyequipment_memospecial"] = null;
        ViewState["vsTemp_Refer"] = null;
        GvName.DataSource = ViewState["vsBuyequipment_memospecial"];
        GvName.DataBind();
        SetViewState_FormMemo_Special();
    }


    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);

                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                    }
                    break;

                case "bnDeleteEditList":
                    GridView GvReportAdd_edit = (GridView)FvEditIT.FindControl("GvReportAdd_edit");
                    GridViewRow rowSelect_edit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_edit = rowSelect_edit.RowIndex;
                    DataSet dsContacts_edit = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts_edit.Tables["dsAddListTable"].Rows[rowIndex_edit].Delete();
                    dsContacts_edit.AcceptChanges();
                    setGridData(GvReportAdd_edit, dsContacts_edit.Tables["dsAddListTable"]);

                    if (dsContacts_edit.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd_edit.Visible = false;
                    }
                    break;

                case "bnDeleteListMemo":
                    GridView GvProductAdd = (GridView)ViewIT.FindControl("GvProductAdd");
                    GridViewRow rowSelect_memo = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_memo = rowSelect_memo.RowIndex;
                    DataSet dsContacts_memo = (DataSet)ViewState["vsBuyequipment_memo"];
                    dsContacts_memo.Tables["dsAddListTable_memo"].Rows[rowIndex_memo].Delete();
                    dsContacts_memo.AcceptChanges();
                    setGridData(GvProductAdd, dsContacts_memo.Tables["dsAddListTable_memo"]);

                    if (dsContacts_memo.Tables["dsAddListTable_memo"].Rows.Count < 1)
                    {
                        GvProductAdd.Visible = false;
                        div_save.Visible = false;
                    }

                    break;

                case "bnDeleteListMemo_S":
                    GridView GvProductSpecialAdd = (GridView)ViewIT.FindControl("GvProductSpecialAdd");
                    GridViewRow rowSelect_memos = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_memos = rowSelect_memos.RowIndex;
                    DataSet dsContacts_memos = (DataSet)ViewState["vsBuyequipment_memospecial"];
                    dsContacts_memos.Tables["dsAddListTable_memospecial"].Rows[rowIndex_memos].Delete();
                    dsContacts_memos.AcceptChanges();
                    setGridData(GvProductSpecialAdd, dsContacts_memos.Tables["dsAddListTable_memospecial"]);

                    if (dsContacts_memos.Tables["dsAddListTable_memospecial"].Rows.Count < 1)
                    {
                        GvProductSpecialAdd.Visible = false;
                        div_save_s.Visible = false;
                    }

                    break;

            }
        }
    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0NodeIDX = (HiddenField)Fvdetail_Memo.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)Fvdetail_Memo.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)Fvdetail_Memo.FindControl("hfM0StatusIDX");
        Label txtrdeptidx = (Label)Fvdetail_Memo.FindControl("txtrdeptidx");

        ViewState["m0_node"] = hfM0NodeIDX.Value;
        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        ViewState["m0_status"] = hfM0StatusIDX.Value;

        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //txt.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + ViewState["m0_status"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง
                btnprint_special.Visible = false;
                btnprint_normal.Visible = false;
                break;

            case 2: // ผู้มีสิทธิ์อนุมัติ
                if (ViewState["m0_node"].ToString() == "2" && hfM0StatusIDX.Value == "1" && ViewState["History_Approve"].ToString() == "2" && (
                    (ViewState["JobGradeIDX"].ToString() == "11" && ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21")
                    || ((ViewState["EmpIDX"].ToString() == "174" || ViewState["EmpIDX"].ToString() == "23064") && deptit.Contains(int.Parse(ViewState["rdept_idx"].ToString())))
                    ))
                {
                    panel_approve.Visible = true;
                }
                else
                {
                    panel_approve.Visible = false;
                }
                btnprint_special.Visible = false;
                btnprint_normal.Visible = false;
                break;

            case 3: // เจ้าหน้าที่IT
                if (ViewState["Sec_idx"].ToString() == "80" && ViewState["m0_node"].ToString() == "3" && ViewState["History_Approve"].ToString() == "2")
                {
                    panel_approve.Visible = true;
                }
                else
                {
                    panel_approve.Visible = false;
                }
                btnprint_special.Visible = false;
                btnprint_normal.Visible = false;
                break;
            case 4: // ผู้จัดการIT

                if ((ViewState["EmpIDX"].ToString() == "178" || ViewState["EmpIDX"].ToString() == "23063") && ViewState["m0_node"].ToString() == "4" && ViewState["History_Approve"].ToString() == "2")
                {
                    panel_approve.Visible = true;
                }
                else
                {
                    panel_approve.Visible = false;
                }
                if (ViewState["m0_node"].ToString() == "9" && ViewState["m0_status"].ToString() == "2")
                {
                    btnprint_special.Visible = true;
                    btnprint_normal.Visible = false;
                }
                else
                {
                    btnprint_special.Visible = false;
                    btnprint_normal.Visible = false;
                }
                break;
        }

    }

    #endregion

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                }
            }

            ViewState["path"] = "1";
        }
        catch
        {
            ViewState["path"] = "0";
        }
    }

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvType":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvIT = (GridView)e.Row.Cells[0].FindControl("GvIT");
                    Label lbltypidx = (Label)e.Row.Cells[0].FindControl("lbltypidx");
                    Label lblm0idx = (Label)e.Row.Cells[0].FindControl("lblm0idx");


                    _dtitseet.boxprice_reference = new price_reference[1];
                    price_reference dataselect = new price_reference();

                    dataselect.SysIDX = int.Parse(ViewState["SysIDX"].ToString());
                    dataselect.typidx = int.Parse(lbltypidx.Text);

                    _dtitseet.boxprice_reference[0] = dataselect;
                    //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                    _dtitseet = callServicePostITAsset(_urlSelectDetailDeviceIT, _dtitseet);
                    setGridData(GvIT, _dtitseet.boxprice_reference);


                    if (ViewState["SysIDX"].ToString() == "2")
                    {
                        GvIT.Columns[5].Visible = true;
                        GvIT.Columns[2].Visible = false;
                    }
                    else
                    {
                        GvIT.Columns[5].Visible = false;
                    }

                    if (ViewState["SysIDX"].ToString() == "26" || lbltypidx.Text == "3" || lbltypidx.Text == "10")
                    {
                        GvIT.Columns[2].Visible = false;
                    }

                    if (ViewState["Sec_idx"].ToString() == "80" && ViewState["SysIDX"].ToString() == "3")
                    {
                        GvIT.Columns[9].Visible = true;
                    }
                    else if (ViewState["Sec_idx"].ToString() == "77" && ViewState["SysIDX"].ToString() == "2")
                    {
                        GvIT.Columns[9].Visible = true;
                    }
                    else if (ViewState["rdept_idx"].ToString() == "13" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "14" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "136" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "137" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "138" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "139" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "140" && ViewState["SysIDX"].ToString() == "9" ||
                        ViewState["rdept_idx"].ToString() == "141" && ViewState["SysIDX"].ToString() == "9")
                    {
                        GvIT.Columns[9].Visible = true;
                    }
                    else if (ViewState["rdept_idx"].ToString() == "11" && ViewState["SysIDX"].ToString() == "11" ||
                       ViewState["rdept_idx"].ToString() == "133" && ViewState["SysIDX"].ToString() == "11" ||
                        ViewState["rdept_idx"].ToString() == "134" && ViewState["SysIDX"].ToString() == "11" ||
                       ViewState["rdept_idx"].ToString() == "1146" && ViewState["SysIDX"].ToString() == "11")
                    {
                        GvIT.Columns[9].Visible = true;
                    }
                    else if (ViewState["Sec_idx"].ToString() == "210")
                    {
                        GvIT.Columns[9].Visible = true;
                    }

                    else
                    {
                        GvIT.Columns[9].Visible = false;
                    }

                }


                break;

            case "GvIT":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    GridView Gvperdevices = (GridView)e.Row.Cells[8].FindControl("Gvperdevices");

                    Label lblm0idx = (Label)e.Row.Cells[0].FindControl("lblm0idx");
                    var image = ((System.Web.UI.WebControls.Image)e.Row.FindControl("image"));
                    Label lbltypidx_it = (Label)e.Row.Cells[0].FindControl("lbltypidx_it");
                    Label lbpricenotshow = (Label)e.Row.Cells[4].FindControl("lbpricenotshow");
                    Label lbprice = (Label)e.Row.Cells[4].FindControl("lbprice");
                    Label lbmaprice = (Label)e.Row.Cells[5].FindControl("lbmaprice");
                    Label lbmapriceshow = (Label)e.Row.Cells[5].FindControl("lbmapriceshow");


                    if (lblm0idx.Text == "0" || lblm0idx.Text == String.Empty)
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {
                        e.Row.Visible = true;

                        lbprice.Text = string.Format("{0:N2}", Convert.ToDecimal(lbpricenotshow.Text));
                        if (lbmapriceshow.Text != "")
                        {
                            lbmaprice.Text = string.Format("{0:N2}", Convert.ToDecimal(lbmapriceshow.Text));

                        }

                        string getPathLotus = ConfigurationSettings.AppSettings["path_flie_price_ref"];
                        string path = lblm0idx.Text + "/";
                        string namefile = lblm0idx.Text + ".jpg";
                        string pic = getPathLotus + path + namefile;

                        string filePathLotus = Server.MapPath(getPathLotus + lblm0idx.Text);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                        SearchDirectories(myDirLotus, lblm0idx.Text);

                        if (ViewState["path"].ToString() != "0")
                        {
                            image.Visible = true;
                            image.ImageUrl = pic;
                            image.Height = 150;
                            image.Width = 150;
                        }
                        else
                        {
                            image.Visible = false;
                        }

                    }

                    Select_PermissionDevice(Gvperdevices, int.Parse(lblm0idx.Text));

                    for (int rowIndex = Gvperdevices.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                    {
                        GridViewRow currentRow = Gvperdevices.Rows[rowIndex];
                        GridViewRow previousRow = Gvperdevices.Rows[rowIndex + 1];

                        if (((Literal)currentRow.Cells[0].FindControl("lblorgidx")).Text == ((Literal)previousRow.Cells[0].FindControl("lblorgidx")).Text)
                        {
                            if (previousRow.Cells[0].RowSpan < 2)
                            {
                                currentRow.Cells[0].RowSpan = 2;

                            }
                            else
                            {
                                currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                            }
                            previousRow.Cells[0].Visible = false;
                        }
                        if (((Literal)currentRow.Cells[1].FindControl("lblrdepidx")).Text == ((Literal)previousRow.Cells[1].FindControl("lblrdepidx")).Text)
                        {
                            if (previousRow.Cells[1].RowSpan < 2)
                            {
                                currentRow.Cells[1].RowSpan = 2;

                            }
                            else
                            {
                                currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                            }
                            previousRow.Cells[1].Visible = false;
                        }
                        if (((Literal)currentRow.Cells[2].FindControl("lblrsecidx")).Text == ((Literal)previousRow.Cells[2].FindControl("lblrsecidx")).Text)
                        {
                            if (previousRow.Cells[2].RowSpan < 2)
                            {
                                currentRow.Cells[2].RowSpan = 2;

                            }
                            else
                            {
                                currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;

                            }
                            previousRow.Cells[2].Visible = false;
                        }
                    }
                }
                break;

            case "GvHardware":
            case "GvSoftware":
            case "GvMonitor":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblm0idx = (Label)e.Row.Cells[1].FindControl("lblm0idx");
                    Label lbpricenotshow = (Label)e.Row.Cells[3].FindControl("lbpricenotshow");
                    Label lbprice = (Label)e.Row.Cells[3].FindControl("lbprice");



                    if (lblm0idx.Text == "0" || lblm0idx.Text == String.Empty)
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {
                        e.Row.Visible = true;

                        lbprice.Text = string.Format("{0:N2}", Convert.ToDecimal(lbpricenotshow.Text));

                    }
                }
                break;

            case "GvHistory":
            case "GvApprove":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton btndetail = (LinkButton)e.Row.Cells[5].FindControl("btndetail");
                    Label lbltype_memo = (Label)e.Row.Cells[2].FindControl("lbltype_memo");
                    Label lbltype_buy = (Label)e.Row.Cells[2].FindControl("lbltype_buy");
                    Literal litm0_tmidx = (Literal)e.Row.Cells[2].FindControl("litm0_tmidx");
                    Literal lim0_type_idx = (Literal)e.Row.Cells[2].FindControl("lim0_type_idx");
                    Label lblunidx = (Label)e.Row.Cells[6].FindControl("lblunidx");
                    Label lblstaidx = (Label)e.Row.Cells[6].FindControl("lblstaidx");
                    Label lbStatusDoc = (Label)e.Row.Cells[6].FindControl("lbStatusDoc");

                    linkBtnTrigger(btndetail);

                    if (litm0_tmidx.Text == "1")
                    {
                        lbltype_memo.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0039e6");
                    }
                    else
                    {
                        lbltype_memo.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b366ff");
                    }

                    if (lim0_type_idx.Text == "1")
                    {
                        lbltype_buy.ForeColor = System.Drawing.ColorTranslator.FromHtml("#39e600");
                    }
                    else
                    {
                        lbltype_buy.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                    }

                    if (lblstaidx.Text == "3")
                    {
                        lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                    }
                    else if (lblstaidx.Text == "2")
                    {
                        lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                    }
                    else
                    {
                        if (lblunidx.Text == "2")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                        }
                        else if (lblunidx.Text == "3")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                        }
                        else if (lblunidx.Text == "4")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b3b300");
                        }
                        else if (lblunidx.Text == "9")
                        {
                            lbStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        }
                    }
                }
                break;

            case "GvDetail_Print_BuyNormal":
            case "GvDetail_BuyNormal":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbprice = (Label)e.Row.Cells[2].FindControl("lbprice");
                    Label lbprice_show = (Label)e.Row.Cells[2].FindControl("lbprice_show");
                    Literal lbtype_monitor = (Literal)e.Row.Cells[3].FindControl("lbtype_monitor");
                    Literal lbsoftwarename = (Literal)e.Row.Cells[3].FindControl("lbsoftwarename");
                    Control div_fullname = (Control)e.Row.Cells[1].FindControl("div_fullname");

                    lbprice_show.Text = string.Format("{0:N2}", Convert.ToDecimal(lbprice.Text));

                    if (lbtype_monitor.Text != "" && lbtype_monitor.Text != String.Empty && lbtype_monitor.Text != null)
                    {
                        lbtype_monitor.Text = lbtype_monitor.Text;
                    }
                    else
                    {
                        lbtype_monitor.Text = "ไม่มี";
                    }

                    if (lbsoftwarename.Text != "" && lbsoftwarename.Text != String.Empty && lbsoftwarename.Text != null)
                    {
                        lbsoftwarename.Text = lbsoftwarename.Text;
                    }
                    else
                    {
                        lbsoftwarename.Text = "ไม่มี";
                    }

                    if (ViewState["m0_type_idx"].ToString() == "2")
                    {
                        div_fullname.Visible = true;
                    }
                    else
                    {
                        div_fullname.Visible = false;
                    }
                }
                break;

            case "GvHistory_Devices":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal lbleveldevice = (Literal)e.Row.Cells[1].FindControl("lbleveldevice");
                    DropDownList ddldevicesholder_replacenormal = (DropDownList)fvbuyreplace_normal.FindControl("ddldevicesholder_replacenormal");

                    lbleveldevice.Text = ddldevicesholder_replacenormal.SelectedItem.Text;

                }
                break;

            case "GvHistory_Devices_S":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Literal lbleveldevice = (Literal)e.Row.Cells[1].FindControl("lbleveldevice");
                    DropDownList ddldevicesholder_replacespecial = (DropDownList)FvBuyReplace_Special.FindControl("ddldevicesholder_replacespecial");

                    lbleveldevice.Text = ddldevicesholder_replacespecial.SelectedItem.Text;

                }
                break;


            case "GvProductAdd":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblemp_idx = (Label)e.Row.Cells[0].FindControl("lblemp_idx");
                    Control div_buyreplace = (Control)e.Row.Cells[0].FindControl("div_buyreplace");

                    if (lblemp_idx.Text != String.Empty && lblemp_idx.Text != null && lblemp_idx.Text != "0")
                    {
                        div_buyreplace.Visible = true;
                    }
                    else
                    {
                        div_buyreplace.Visible = false;
                    }
                }
                break;

            case "GvTypeDevice":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeDevice.EditIndex != e.Row.RowIndex)
                    {
                        Label lblm0_naidx = (Label)e.Row.Cells[0].FindControl("lblm0_naidx");
                        Label lblm0_tyidx = (Label)e.Row.Cells[0].FindControl("lblm0_tyidx");
                        RadioButtonList rdo_typedevice = (RadioButtonList)e.Row.Cells[1].FindControl("rdo_typedevice");

                        _dtitseet = new data_itasset();

                        _dtitseet.boxdevices_reference = new devices_reference[1];
                        devices_reference device = new devices_reference();

                        device.m0_naidx = int.Parse(lblm0_naidx.Text);
                        device.m0_tyidx = int.Parse(lblm0_tyidx.Text);

                        _dtitseet.boxdevices_reference[0] = device;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

                        _dtitseet = callServicePostITAsset(_urlSelect_DevicesGV, _dtitseet);

                        rdo_typedevice.DataSource = _dtitseet.boxdevices_reference;
                        rdo_typedevice.DataTextField = "device_name";
                        rdo_typedevice.DataValueField = "m2_tyidx";
                        rdo_typedevice.DataBind();


                    }
                }
                break;

            case "GvProductSpecialAdd":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lblemp_idx = (Label)e.Row.Cells[0].FindControl("lblemp_idx");
                    Control div_buyreplace = (Control)e.Row.Cells[0].FindControl("div_buyreplace");

                    if (lblemp_idx.Text != String.Empty && lblemp_idx.Text != null && lblemp_idx.Text != "0")
                    {
                        div_buyreplace.Visible = true;
                    }
                    else
                    {
                        div_buyreplace.Visible = false;
                    }

                }
                break;

            case "GvDetail_BuySpecial":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvDetail_BuySpecial.EditIndex != e.Row.RowIndex)
                    {
                        Label lblu1_meidx_s = (Label)e.Row.Cells[0].FindControl("lblu1_meidx_s");
                        GridView GvDevice_Special = (GridView)e.Row.Cells[2].FindControl("GvDevice_Special");
                        GridView GvComment_Special = (GridView)e.Row.Cells[4].FindControl("GvComment_Special");
                        Control div_rdochoose = (Control)e.Row.Cells[5].FindControl("div_rdochoose");
                        Control div_fullname = (Control)e.Row.Cells[1].FindControl("div_fullname");
                        Literal lbsoftwarename = (Literal)e.Row.Cells[3].FindControl("lbsoftwarename");

                        _dtitseet = new data_itasset();

                        _dtitseet.boxu1memo_s_reference = new u1memo_s_reference[1];
                        u1memo_s_reference select = new u1memo_s_reference();

                        select.u1_meidx_s = int.Parse(lblu1_meidx_s.Text);

                        _dtitseet.boxu1memo_s_reference[0] = select;

                        //txt.Text += HttpUtilityHtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                        _dtitseet = callServicePostITAsset(_urlSelect_History_DetailMemoSpecial, _dtitseet);
                        setGridData(GvDevice_Special, _dtitseet.boxu1memo_s_reference);


                        if (int.Parse(ViewState["m0_node"].ToString()) > 2 && int.Parse(ViewState["m0_actor"].ToString()) > 2)
                        {
                            GvDetail_BuySpecial.Columns[4].Visible = true;
                        }
                        else
                        {
                            GvDetail_BuySpecial.Columns[4].Visible = false;
                        }

                        if (lbsoftwarename.Text == "")
                        {
                            lbsoftwarename.Text = "-";//GvDetail_BuySpecial.Columns[3].Visible = false;
                        }
                        //else
                        //{
                        //   // GvDetail_BuySpecial.Columns[3].Visible = true;

                        //}


                        _dtitseet = new data_itasset();

                        _dtitseet.boxu1memo_s_reference = new u1memo_s_reference[1];
                        u1memo_s_reference selectlog = new u1memo_s_reference();

                        selectlog.u1_meidx_s = int.Parse(lblu1_meidx_s.Text);

                        _dtitseet.boxu1memo_s_reference[0] = selectlog;

                        ////txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                        _dtitseet = callServicePostITAsset(_urlSelect_ApproveDetail_Memo, _dtitseet);
                        setGridData(GvComment_Special, _dtitseet.boxu1memo_s_reference);


                        if (ViewState["History_Approve"].ToString() == "2") // && approve_check == 1
                        {
                            GvDetail_BuySpecial.Columns[5].Visible = true;
                            if (approve_check == 1)
                            {
                                div_rdochoose.Visible = false;
                            }
                            else
                            {
                                div_rdochoose.Visible = true;
                            }
                        }
                        else
                        {
                            GvDetail_BuySpecial.Columns[5].Visible = false;
                        }

                        if (ViewState["m0_type_idx"].ToString() == "2")
                        {
                            div_fullname.Visible = true;
                        }
                        else
                        {
                            div_fullname.Visible = false;
                        }
                    }

                }
                break;

            case "GvComment_Special":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Literal listaidx_dir = (Literal)e.Row.Cells[0].FindControl("listaidx_dir");
                    Label listatus_dir = (Label)e.Row.Cells[0].FindControl("listatus_dir");
                    Literal litstaidx_it = (Literal)e.Row.Cells[1].FindControl("litstaidx_it");
                    Label litstatus_it = (Label)e.Row.Cells[1].FindControl("litstatus_it");
                    Literal listaidx_headerit = (Literal)e.Row.Cells[2].FindControl("listaidx_headerit");
                    Label listatus_headerit = (Label)e.Row.Cells[2].FindControl("listatus_headerit");

                    if (listaidx_dir.Text == "0")
                    {
                        listatus_dir.Text = "";
                    }
                    else if (listaidx_dir.Text == "2")
                    {
                        listatus_dir.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        listatus_dir.Text = listatus_dir.Text;
                    }
                    else if (listaidx_dir.Text == "3")
                    {
                        listatus_dir.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        listatus_dir.Text = listatus_dir.Text;
                    }

                    if (litstaidx_it.Text == "0")
                    {
                        litstatus_it.Text = "";
                    }
                    else if (litstaidx_it.Text == "2")
                    {
                        litstatus_it.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        litstatus_it.Text = litstatus_it.Text;
                    }
                    else if (litstaidx_it.Text == "3")
                    {
                        litstatus_it.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        litstatus_it.Text = litstatus_it.Text;
                    }

                    if (listaidx_headerit.Text == "0")
                    {
                        listatus_headerit.Text = "";
                    }
                    else if (listaidx_headerit.Text == "2")
                    {
                        listatus_headerit.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        listatus_headerit.Text = listatus_headerit.Text;
                    }
                    else if (listaidx_headerit.Text == "3")
                    {
                        listatus_headerit.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                        listatus_headerit.Text = listatus_headerit.Text;
                    }



                    if (int.Parse(listaidx_dir.Text) == 3 || int.Parse(litstaidx_it.Text) == 3 || int.Parse(listaidx_headerit.Text) == 3)
                    {
                        approve_check = 1;
                    }
                    else
                    {
                        approve_check = 0;
                    }


                }
                break;

            case "GvDetail_Print_BuySpecial":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvDevice_Print_Special = (GridView)e.Row.Cells[2].FindControl("GvDevice_Print_Special");
                    Label lblu1_meidx_s = (Label)e.Row.Cells[0].FindControl("lblu1_meidx_s");
                    Control div_fullname = (Control)e.Row.Cells[1].FindControl("div_fullname");
                    Literal lbsoftwarename = (Literal)e.Row.Cells[3].FindControl("lbsoftwarename");
                    GridView GvDetail_Print_BuySpecial = (GridView)FvTemplate_print.FindControl("GvDetail_Print_BuySpecial");

                    _dtitseet = new data_itasset();

                    _dtitseet.boxu1memo_s_reference = new u1memo_s_reference[1];
                    u1memo_s_reference select = new u1memo_s_reference();

                    select.u1_meidx_s = int.Parse(lblu1_meidx_s.Text);

                    _dtitseet.boxu1memo_s_reference[0] = select;

                    //txt.Text += HttpUtilityHtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                    _dtitseet = callServicePostITAsset(_urlSelect_History_DetailMemoSpecial, _dtitseet);
                    setGridData(GvDevice_Print_Special, _dtitseet.boxu1memo_s_reference);

                    if (ViewState["m0_type_idx"].ToString() == "2")
                    {
                        div_fullname.Visible = true;
                    }
                    else
                    {
                        div_fullname.Visible = false;
                    }

                    if (lbsoftwarename.Text == "")
                    {
                        lbsoftwarename.Text = "-";//GvDetail_BuySpecial.Columns[3].Visible = false;
                    }
                }
                break;

        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvHistory":
                GvHistory.PageIndex = e.NewPageIndex;
                SelectHistory(GvHistory, int.Parse(ViewState["rdept_idx"].ToString()));
                break;

            case "GvApprove":
                GvApprove.PageIndex = e.NewPageIndex;
                Selectwaiting_Approve(GvApprove, int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["JobGradeIDX"].ToString()));
                break;
        }
    }
    #endregion

    #region MergeCell
    protected void MergeCell(GridView gvName)
    {
        switch (gvName.ID)
        {
            case "GvHistory_Devices":
            case "GvHistory_Devices_S":
                for (int rowIndex = gvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = gvName.Rows[rowIndex];
                    GridViewRow previousRow = gvName.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[0].FindControl("lbldevices")).Text == ((Literal)previousRow.Cells[0].FindControl("lbldevices")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                    if (((Literal)currentRow.Cells[1].FindControl("lbleveldevice")).Text == ((Literal)previousRow.Cells[1].FindControl("lbleveldevice")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }
                }
                break;
        }

    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        Control divbtnsave = (Control)fv_insert.FindControl("divbtnsave");
        Panel Panel_Addper = (Panel)FvEditIT.FindControl("Panel_Addper");
        LinkButton btnshowper = (LinkButton)FvEditIT.FindControl("btnshowper");
        LinkButton btnhideper = (LinkButton)FvEditIT.FindControl("btnhideper");
        DropDownList ddlpos_edit = (DropDownList)FvEditIT.FindControl("ddlpos_edit");
        DropDownList ddlDep_edit = (DropDownList)FvEditIT.FindControl("ddlDep_edit");
        DropDownList ddlSec_edit = (DropDownList)FvEditIT.FindControl("ddlSec_edit");
        DropDownList ddlOrg_edit_edit = (DropDownList)FvEditIT.FindControl("ddlOrg_edit");
        Control div_showperedit = (Control)FvEditIT.FindControl("div_showperedit");
        DropDownList ddlperedit = (DropDownList)FvEditIT.FindControl("ddlperedit");
        GridView GvReportAdd_edit = (GridView)FvEditIT.FindControl("GvReportAdd_edit");

        DropDownList ddlOrg_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlOrg_buynew");
        DropDownList ddlDep_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlDep_buynew");
        DropDownList ddlSec_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlSec_buynew");
        DropDownList ddlpos_buynew = (DropDownList)fvbuynew_normal.FindControl("ddlpos_buynew");

        TextBox txtorgidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtorgidx_replacenormal");
        TextBox txtrequesdeptidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtrequesdeptidx_replacenormal");
        TextBox txtsecidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtsecidx_replacenormal");
        TextBox txtempidx = (TextBox)fvbuyreplace_normal.FindControl("txtempidx");
        TextBox txtposgroupidx_replacenormal = (TextBox)fvbuyreplace_normal.FindControl("txtposgroupidx_replacenormal");
        DropDownList ddldevicesholder_replacenormal = (DropDownList)fvbuyreplace_normal.FindControl("ddldevicesholder_replacenormal");
        TextBox txtempcode_replace = (TextBox)fvbuyreplace_normal.FindControl("txtempcode_replace");

        Control div_profileemp_replacenormal = (Control)fvbuyreplace_normal.FindControl("div_profileemp_replacenormal");
        DropDownList ddltypedevice_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddltypedevice_buynew_s");
        DropDownList ddlOrg_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlOrg_buynew_s");
        DropDownList ddlDep_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlDep_buynew_s");
        DropDownList ddlSec_buynew_s = (DropDownList)FvBuyNew_Special.FindControl("ddlSec_buynew_s");

        DropDownList ddltypedevice_buyreplace_s = (DropDownList)FvBuyReplace_Special.FindControl("ddltypedevice_buyreplace_s");
        TextBox txtorgidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtorgidx_replacespecial");
        TextBox txtrequesdeptidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtrequesdeptidx_replacespecial");
        TextBox txtsecidx_replacespecial = (TextBox)FvBuyReplace_Special.FindControl("txtsecidx_replacespecial");
        DropDownList ddldevicesholder_replacespecial = (DropDownList)FvBuyReplace_Special.FindControl("ddldevicesholder_replacespecial");
        TextBox txtempcode_replace_s = (TextBox)FvBuyReplace_Special.FindControl("txtempcode_replace_s");
        Control div_profileemp_replacespecial = (Control)FvBuyReplace_Special.FindControl("div_profileemp_replacespecial");



        switch (cmdName)
        {

            case "_divMenuBtnToDivIT":
                SetDefaultIT();
                ViewState["SysIDX"] = "3";
                SelectTypeIT();

                break;
            case "_divMenuBtnToDivHR":
                SetDefaultHR();
                ViewState["SysIDX"] = "9";
                SelectTypeIT();

                break;
            case "_divMenuBtnToDivEN":
                SetDefaultEN();
                ViewState["SysIDX"] = "11";
                SelectTypeIT();

                break;
            case "_divMenuBtnToDivSAP":
                SetDefaultSAP();
                ViewState["SysIDX"] = "2";
                SelectTypeIT();

                break;

            case "_divMenuBtnToDivWeb":

                SetDefaultWeb();
                ViewState["SysIDX"] = "26";
                SelectTypeIT();

                break;

            case "_divMenuBtnToDivQA":
                SetDefaultQA();
                ViewState["SysIDX"] = "36";
                SelectTypeIT();
                break;

            case "_divMenuBtnToDivPurchase":
                SetDefaultPurchase();
                ViewState["SysIDX"] = "41";
                SelectTypeIT();
                break;

            case "_divMenuBtnToDivAdd":
                SetDefaultAdd();

                break;

            //case "_divMenuBtnToDivManual":
            //    Response.Write("<script>window.open('https://docs.google.com/document/d/1D-czxYhJzsz3ZH4NiXEch_KKIDumYEXw1iPrBEQcIn8/edit?usp=sharing','_blank');</script>");

            //    break;

            case "btnAdd":
                Insert_Ref();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdEditIT":

                string[] arg1_Editit = new string[4];
                arg1_Editit = e.CommandArgument.ToString().Split(';');
                int m0idx_Editit = int.Parse(arg1_Editit[0]);
                int typidx_Editit = int.Parse(arg1_Editit[1]);
                int sysidx_Editit = int.Parse(arg1_Editit[2]);
                int tdidx_Editit = int.Parse(arg1_Editit[3]);

                ViewState["m0idx"] = m0idx_Editit;
                ViewState["typidx"] = typidx_Editit;
                ViewState["sysidx"] = sysidx_Editit;
                ViewState["tdidx"] = tdidx_Editit;

                MvMaster.SetActiveView(ViewEdit);

                FvEditIT.ChangeMode(FormViewMode.Edit);
                FvEditIT.DataBind();

                SelectEditIT();



                Control divedit_maprice = (Control)FvEditIT.FindControl("divedit_maprice");
                GridView Gvperdevices_edit = (GridView)FvEditIT.FindControl("Gvperdevices_edit");
                GridView GvReportAdd_edit1 = (GridView)FvEditIT.FindControl("GvReportAdd_edit");

                DropDownList ddlOrg_edit = (DropDownList)FvEditIT.FindControl("ddlOrg_edit");

                if (ViewState["sysidx"].ToString() == "2")
                {
                    divedit_maprice.Visible = true;
                }
                else
                {
                    divedit_maprice.Visible = false;
                }

                Select_PermissionDevice(Gvperdevices_edit, m0idx_Editit);
                getOrganizationList(ddlOrg_edit);
                CleardataSetFormList(GvReportAdd_edit1);


                break;

            case "CmdUpdate":
                Edit_Ref();
                SelectTypeIT();
                MvMaster.SetActiveView(ViewIT);
                break;

            case "CmdDel":
                string[] arg1_deleteit = new string[2];
                arg1_deleteit = e.CommandArgument.ToString().Split(';');
                int m0idx_delete = int.Parse(arg1_deleteit[0]);
                int sysidx_delete = int.Parse(arg1_deleteit[1]);

                //txt.Text = arg1_deleteit[0] + "," + arg1_deleteit[1];

                ViewState["m0idx_delete"] = m0idx_delete;
                ViewState["SysIDX"] = sysidx_delete;
                Delete_IT();
                SelectTypeIT();

                break;

            case "btnsearch":
                search = 1;
                ViewState["SysIDX"] = ddlSearchSystem.SelectedValue;
                SelectSearchData();
                if (ViewState["SysIDX"].ToString() == "3")
                {
                    SetDefaultIT();
                }
                else if (ViewState["SysIDX"].ToString() == "9")
                {
                    SetDefaultHR();
                }
                else if (ViewState["SysIDX"].ToString() == "11")
                {
                    SetDefaultEN();
                }
                else if (ViewState["SysIDX"].ToString() == "2")
                {
                    SetDefaultSAP();
                }
                else if (ViewState["SysIDX"].ToString() == "26")
                {
                    SetDefaultWeb();
                }
                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdAdddataset":
                DropDownList ddlOrg = (DropDownList)fv_insert.FindControl("ddlOrg");
                DropDownList ddlDep = (DropDownList)fv_insert.FindControl("ddlDep");
                DropDownList ddlSec = (DropDownList)fv_insert.FindControl("ddlSec");
                DropDownList ddlpos = (DropDownList)fv_insert.FindControl("ddlpos");
                GridView GvReportAdd = (GridView)fv_insert.FindControl("GvReportAdd");


                divbtnsave.Visible = true;
                setAddList_Form(ddlOrg, ddlDep, ddlSec, ddlpos, GvReportAdd);
                break;

            case "CmdDelPer":
                GridView Gvperdevices_edit1 = (GridView)FvEditIT.FindControl("Gvperdevices_edit");
                TextBox txtm0idx = (TextBox)FvEditIT.FindControl("txtm0idx");

                string[] arg1_deleteper = new string[2];
                arg1_deleteper = e.CommandArgument.ToString().Split(';');
                int m1idx = int.Parse(arg1_deleteper[0]);

                Delete_Per(m1idx);
                Select_PermissionDevice(Gvperdevices_edit1, int.Parse(txtm0idx.Text));


                break;

            case "Cmdshowper":
                DropDownList ddlOrg_edit1 = (DropDownList)FvEditIT.FindControl("ddlOrg_edit");

                btnshowper.Visible = false;
                btnhideper.Visible = true;
                Panel_Addper.Visible = true;
                select_pos(ddlpos_edit);
                ddlOrg_edit1.SelectedValue = "0";
                ddlDep_edit.SelectedValue = "0";
                ddlSec_edit.SelectedValue = "0";
                div_showperedit.Visible = false;
                ddlperedit.SelectedValue = "0";
                break;


            case "Cmdhideper":
                btnshowper.Visible = true;
                btnhideper.Visible = false;
                Panel_Addper.Visible = false;
                CleardataSetFormList(GvReportAdd_edit);

                break;

            case "Cancel":
                FvEditIT.ChangeMode(FormViewMode.ReadOnly);
                FvEditIT.DataBind();
                MvMaster.SetActiveView(ViewIT);
                lbltopic.Text = topicit;
                SetDefaultIT();
                SelectTypeIT();
                break;

            case "CmdAddPerEdit":

                setAddList_Form(ddlOrg_edit_edit, ddlDep_edit, ddlSec_edit, ddlpos_edit, GvReportAdd_edit);

                break;

            case "cmddata_index":
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewBuy_Normal.Attributes.Remove("class");
                _divMenuLiToViewBuy_Special.Attributes.Remove("class");

                div_index.Visible = true;
                div_buynormal.Visible = false;
                div_buyspecial.Visible = false;
                break;
            case "cmddbuy_normal":
                _divMenuLiToViewBuy_Normal.Attributes.Add("class", "active");
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewBuy_Special.Attributes.Remove("class");


                div_index.Visible = false;
                div_buynormal.Visible = true;
                div_buyspecial.Visible = false;

                FvDataUser.ChangeMode(FormViewMode.Insert);
                FvDataUser.DataBind();

                imgbuynew.ImageUrl = "~/images/price-reference/picbtnbuynew.png";
                imgbuyreplace.ImageUrl = "~/images/price-reference/picbtnbuyreplace.png";

                divbuy_new.Visible = false;
                divbuy_replace.Visible = false;

                CleardataSetFormList_Memo(GvProductAdd);
                GvProductAdd.Visible = false;

                div_save.Visible = false;
                div_product_normal.Visible = false;
                ddlpos_buynew.SelectedValue = "0";

                break;

            case "cmdbuy_special":
                _divMenuLiToViewBuy_Special.Attributes.Add("class", "active");
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewBuy_Normal.Attributes.Remove("class");

                div_index.Visible = false;
                div_buynormal.Visible = false;
                div_buyspecial.Visible = true;
                divbuy_newspecial.Visible = false;
                imgbuynew_s.ImageUrl = "~/images/price-reference/picbtnbuynew_s.png";
                imgbuyreplace_s.ImageUrl = "~/images/price-reference/picbtnbuyreplace_s.png";

                FvDetailUser_BuySpecial.ChangeMode(FormViewMode.Insert);
                FvDetailUser_BuySpecial.DataBind();
                CleardataSetFormList_Memo_Special(GvProductSpecialAdd);
                divproduct_special.Visible = false;
                GvProductSpecialAdd.Visible = false;
                break;



            case "btnsystem":
                string cmdArg_sys = e.CommandArgument.ToString();

                switch (cmdArg_sys)
                {
                    case "1":
                        ViewState["m0_type_idx"] = "1";
                        imgbuynew.ImageUrl = "~/images/price-reference/picbtnbuynew-active.png";
                        imgbuyreplace.ImageUrl = "~/images/price-reference/picbtnbuyreplace.png";
                        divbuy_new.Visible = true;
                        divbuy_replace.Visible = false;
                        fvbuynew_normal.ChangeMode(FormViewMode.Insert);
                        fvbuynew_normal.DataBind();
                        div_product_normal.Visible = false;
                        GvMonitor.Visible = false;
                        div_monitor.Visible = false;
                        break;

                    case "2":
                        ViewState["m0_type_idx"] = "2";
                        imgbuynew.ImageUrl = "~/images/price-reference/picbtnbuynew.png";
                        imgbuyreplace.ImageUrl = "~/images/price-reference/picbtnbuyreplace-active.png";
                        divbuy_new.Visible = false;
                        divbuy_replace.Visible = true;
                        div_product_normal.Visible = false;
                        GvMonitor.Visible = false;
                        div_monitor.Visible = false;
                        div_profileemp_replacenormal.Visible = false;
                        break;
                    case "3":
                        ViewState["m0_type_idx"] = "1";
                        imgbuynew_s.ImageUrl = "~/images/price-reference/picbtnbuynew-active_s.png";
                        imgbuyreplace_s.ImageUrl = "~/images/price-reference/picbtnbuyreplace_s.png";
                        divbuy_newspecial.Visible = true;
                        divbuy_replacespecial.Visible = false;
                        divproduct_special.Visible = false;

                        ddltypedevice_buyreplace_s.SelectedValue = "0";
                        ddltypedevice_buynew_s.SelectedValue = "0";
                        break;
                    case "4":
                        ViewState["m0_type_idx"] = "2";
                        imgbuynew_s.ImageUrl = "~/images/price-reference/picbtnbuynew_s.png";
                        imgbuyreplace_s.ImageUrl = "~/images/price-reference/picbtnbuyreplace-active_s.png";
                        divbuy_newspecial.Visible = false;
                        divbuy_replacespecial.Visible = true;
                        divproduct_special.Visible = false;
                        ddltypedevice_buyreplace_s.SelectedValue = "0";
                        ddltypedevice_buynew_s.SelectedValue = "0";
                        break;
                }
                CleardataSetFormList_Memo(GvProductAdd);
                GvProductAdd.Visible = false;
                div_save.Visible = false;

                CleardataSetFormList_Memo_Special(GvProductSpecialAdd);
                GvProductSpecialAdd.Visible = false;
                div_save_s.Visible = false;
                break;

            case "CmdSearch":
                //CleardataSetFormList_Memo(GvProductAdd);

                if (ViewState["m0_type_idx"].ToString() == "1")
                {
                    SelectHardware_Memo(GvHardware, int.Parse(ddlOrg_buynew.SelectedValue), int.Parse(ddlDep_buynew.SelectedValue), int.Parse(ddlSec_buynew.SelectedValue), int.Parse(ddlpos_buynew.SelectedValue));

                    SelectSoftware_Memo(GvSoftware, int.Parse(ddlOrg_buynew.SelectedValue), int.Parse(ddlDep_buynew.SelectedValue), int.Parse(ddlSec_buynew.SelectedValue), int.Parse(ddlpos_buynew.SelectedValue));

                    div_historydevices.Visible = false;


                }
                else if (ViewState["m0_type_idx"].ToString() == "2")
                {
                    Select_HolderSoftware(GvHistory_Devices, int.Parse(ddldevicesholder_replacenormal.SelectedValue));
                    MergeCell(GvHistory_Devices);
                    div_historydevices.Visible = true;

                    SelectHardware_Memo(GvHardware, int.Parse(txtorgidx_replacenormal.Text), int.Parse(txtrequesdeptidx_replacenormal.Text), int.Parse(txtsecidx_replacenormal.Text), int.Parse(txtposgroupidx_replacenormal.Text));

                    SelectSoftware_Memo(GvSoftware, int.Parse(txtorgidx_replacenormal.Text), int.Parse(txtrequesdeptidx_replacenormal.Text), int.Parse(txtsecidx_replacenormal.Text), int.Parse(txtposgroupidx_replacenormal.Text));
                }

                div_product_normal.Visible = true;
                div_btndataset.Visible = true;
                break;

            case "CmdInsert_Dataset":
                if (ViewState["m0_type_idx"].ToString() == "1" || ViewState["m0_type_idx"].ToString() == "2" && (ddldevicesholder_replacenormal.SelectedValue != "0" || txtempcode_replace.Text != String.Empty))
                {
                    setAddList_Formmemo();

                    if (check != 0)
                    {
                        div_save.Visible = true;
                        txtempcode_replace.Text = String.Empty;
                        ddldevicesholder_replacenormal.SelectedValue = "0";
                        txtempcode_replace.Focus();
                        GvMonitor.Visible = false;
                        div_monitor.Visible = false;
                        div_profileemp_replacenormal.Visible = false;

                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด!!! กรุณาระบุข้อมูลรหัสพนักงาน หรือ อุปกรณ์ถือครอง');", true);
                }
                setOntop.Focus();
                break;

            case "CmdInsert_Buynew":
                Insert_memo(int.Parse(ViewState["m0_type_idx"].ToString()), 1);

                MvMaster.SetActiveView(ViewDetail_History);
                select_empIdx_present_create(int.Parse(ViewState["CempIDX_Create"].ToString()));
                FvUserRequest.ChangeMode(FormViewMode.Insert);
                FvUserRequest.DataBind();

                SelectHistory_PrintMemo_BuyNormal(Fvdetail_Memo, int.Parse(ViewState["u0_meidx"].ToString()), 1);

                Fvdetail_Memo.ChangeMode(FormViewMode.ReadOnly);
                Fvdetail_Memo.DataBind();

                SelectHistory_PrintMemoDetail_BuyNormal(GvDetail_BuyNormal, int.Parse(ViewState["u0_meidx"].ToString()));
                FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);

                SelectHistory_PrintMemo_BuyNormal(FvTemplate_print, int.Parse(ViewState["u0_meidx"].ToString()), 1);

                if (ViewState["m0_tmidx"].ToString() == "1")
                {
                    btnprint_normal.Visible = true;
                    btnprint_special.Visible = false;
                }
                else
                {
                    btnprint_normal.Visible = false;
                    btnprint_special.Visible = true;
                }
                break;

            case "_divMenuBtnToDivHistory":
                SetDefaultHistory();
                break;


            case "CmdDetail":
                ViewState["u0_meidx"] = null;

                string[] arg_detail = new string[8];
                arg_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_meidx"] = arg_detail[0].ToString();
                ViewState["CempIDX_Create"] = arg_detail[1].ToString();
                ViewState["m0_type_idx"] = arg_detail[2].ToString();
                ViewState["m0_tmidx"] = arg_detail[3].ToString();
                ViewState["History_Approve"] = arg_detail[4].ToString();
                ViewState["m0_node"] = arg_detail[5].ToString();
                ViewState["m0_status"] = arg_detail[6].ToString();
                ViewState["m0_actor"] = arg_detail[7].ToString();

                MvMaster.SetActiveView(ViewDetail_History);
                select_empIdx_present_create(int.Parse(ViewState["CempIDX_Create"].ToString()));
                FvUserRequest.ChangeMode(FormViewMode.Insert);
                FvUserRequest.DataBind();

                SelectHistory_PrintMemo_BuyNormal(Fvdetail_Memo, int.Parse(ViewState["u0_meidx"].ToString()), 1);
                Fvdetail_Memo.ChangeMode(FormViewMode.ReadOnly);
                Fvdetail_Memo.DataBind();

                if (ViewState["m0_tmidx"].ToString() == "1")
                {
                    SelectHistory_PrintMemoDetail_BuyNormal(GvDetail_BuyNormal, int.Parse(ViewState["u0_meidx"].ToString()));
                    FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);

                    SelectHistory_PrintMemo_BuyNormal(FvTemplate_print, int.Parse(ViewState["u0_meidx"].ToString()), 1);
                    GvDetail_BuySpecial.Visible = false;
                    GvDetail_BuyNormal.Visible = true;
                    btnprint_normal.Visible = true;
                    btnprint_special.Visible = false;
                }
                else
                {
                    SelectHistory_PrintMemo_BuySpecial(GvDetail_BuySpecial, int.Parse(ViewState["u0_meidx"].ToString()), 1);
                    FvTemplate_print.ChangeMode(FormViewMode.ReadOnly);

                    if (ViewState["m0_node"].ToString() == "9" && ViewState["m0_status"].ToString() == "2")
                    {
                        SelectHistory_PrintMemo_BuyNormal(FvTemplate_print, int.Parse(ViewState["u0_meidx"].ToString()), 2);
                    }
                    setFormDataActor();
                    GvDetail_BuyNormal.Visible = false;
                    GvDetail_BuySpecial.Visible = true;
                    ddl_approve.SelectedValue = "0";
                    txtremark_approve.Text = String.Empty;
                }

                SelectLog(rpLog, int.Parse(ViewState["u0_meidx"].ToString()));
                break;

            case "btnCancel_his":
                SetDefaultHistory();

                break;

            case "CmdSearch_S":
                divproduct_special.Visible = true;
                if (ViewState["m0_type_idx"].ToString() == "1")
                {
                    div_historydevices_s.Visible = false;
                    select_typedevice(GvTypeDevice, int.Parse(ddltypedevice_buynew_s.SelectedValue));
                    SelectSoftware_Memo(GvSoftWare_BuySpecial, int.Parse(ddlOrg_buynew_s.SelectedValue), int.Parse(ddlDep_buynew_s.SelectedValue), int.Parse(ddlSec_buynew_s.SelectedValue), 0);

                }
                else
                {
                    div_historydevices_s.Visible = true;
                    Select_HolderSoftware(GvHistory_Devices_S, int.Parse(ddldevicesholder_replacespecial.SelectedValue));
                    MergeCell(GvHistory_Devices_S);
                    select_typedevice(GvTypeDevice, int.Parse(ddltypedevice_buyreplace_s.SelectedValue));
                    SelectSoftware_Memo(GvSoftWare_BuySpecial, int.Parse(txtorgidx_replacespecial.Text), int.Parse(txtrequesdeptidx_replacespecial.Text), int.Parse(txtsecidx_replacespecial.Text), 0);

                }

                if (ddltypedevice_buyreplace_s.SelectedValue == "6" || ddltypedevice_buynew_s.SelectedValue == "6")
                {
                    div_software_special.Visible = false;
                }
                else
                {
                    div_software_special.Visible = true;
                }
                div_dataset_s.Visible = true;
                break;

            case "CmdInsert_Dataset_Special":
                if (ViewState["m0_type_idx"].ToString() == "1" || ViewState["m0_type_idx"].ToString() == "2" && (ddldevicesholder_replacespecial.SelectedValue != "0" || txtempcode_replace_s.Text != String.Empty))
                {
                    setAddList_Formmemo_Special();
                    txtdetail_buyspecial.Text = String.Empty;
                    txtremark_buyspecial.Text = String.Empty;
                    div_save_s.Visible = true;
                    ddltypedevice_buynew_s.SelectedValue = "0";
                    ddltypedevice_buyreplace_s.SelectedValue = "0";
                    ddldevicesholder_replacespecial.SelectedValue = "0";
                    txtempcode_replace_s.Text = String.Empty;
                    div_profileemp_replacespecial.Visible = false;
                    foreach (GridViewRow row_typedevice in GvTypeDevice.Rows)
                    {
                        if (row_typedevice.RowType == DataControlRowType.DataRow)
                        {
                            RadioButtonList rdo_typedevice = (RadioButtonList)row_typedevice.Cells[0].FindControl("rdo_typedevice");

                            rdo_typedevice.ClearSelection();
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด!!! กรุณาระบุข้อมูลรหัสพนักงาน หรือ อุปกรณ์ถือครอง');", true);
                }
                setOntop.Focus();
                break;

            case "CmdInsert_BuySpecial":
                Insert_memo(int.Parse(ViewState["m0_type_idx"].ToString()), 2);
                SetDefaultHistory();
                break;

            case "_divMenuBtnToDivWaitingApprove":
                SetDefaultApprove();
                break;

            case "CmdApprove":
                Update_memo(int.Parse(ViewState["m0_type_idx"].ToString()), int.Parse(ViewState["m0_tmidx"].ToString()), int.Parse(ViewState["u0_meidx"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue), txtremark_approve.Text);
                SetDefaultApprove();
                break;

            case "btnCancel_memo":
                if (ViewState["History_Approve"].ToString() == "1")
                {
                    SetDefaultHistory();
                }
                else
                {
                    SetDefaultApprove();
                }
                break;
        }
    }
    #endregion
}