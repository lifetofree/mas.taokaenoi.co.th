﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using DotNet.Highcharts.Enums;

public partial class websystem_ITServices_Ma_Online : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    datama_online _datama = new datama_online();
    // data_employee _dtEmployee = new data_employee();
    data_employee _dataEmployee = new data_employee();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlSelectTypeMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectTypeMA"];
    static string urlInsert_MasterMAList = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_MasterMAList"];
    static string urlSelectMasterMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterMA"];
    static string urlDelete_MasterMAList = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_MasterMAList"];
    static string urlSelectDevicesMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDevicesMA"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string urlSelectHolderDevicesMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHolderDevicesMA"];
    static string urlSelectSearchDevicesMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSearchDevicesMA"];
    static string urlSelectSearchListMA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSearchListMA"];
    static string urlInsertListMA = _serviceUrl + ConfigurationManager.AppSettings["urlInsertListMA"];
    static string urlSelecttListMA_User = _serviceUrl + ConfigurationManager.AppSettings["urlSelecttListMA_User"];
    static string urlSelectDetail_Device = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetail_Device"];
    static string urlSelectDetail_MA = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetail_MA"];
    static string urlUpdate_ApproveMA = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveMA"];
    static string urlSelecttListMA_Support = _serviceUrl + ConfigurationManager.AppSettings["urlSelecttListMA_Support"];
    static string urlSelectLog = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLog"];
    static string urlUpdate_ApproveAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveAdmin"];
    static string urlSelectStatus = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatus"];
    static string urlSelecttSearchMA_Support = _serviceUrl + ConfigurationManager.AppSettings["urlSelecttSearchMA_Support"];
    static string urlSelecttSearch_Report = _serviceUrl + ConfigurationManager.AppSettings["urlSelecttSearch_Report"];
    static string urlSelectExport_Report = _serviceUrl + ConfigurationManager.AppSettings["urlSelectExport_Report"];
    static string urlDelete_MaU1 = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_MaU1"];
    static string urlUpdate_AdminEdit = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_AdminEdit"];


    string _local_xml = String.Empty;
    string u0_didx = String.Empty;
    string value = String.Empty;
    string ma = String.Empty;
    int empidx = 0;

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            lbindex.BackColor = System.Drawing.Color.LightGray;
            SelecttListMA_User();
            SetViewState();
            SETBoxAllSearch.Visible = false;
            BoxButtonSearchShow_Search.Visible = true;
            BoxButtonSearchHide_Search.Visible = false;
            getOrganizationList(ddlSearchOrg);
            getStatusList(ddlSearchStatus);
            _divMenuLiToDivIndex.Attributes.Add("class", "active");
            liToInsertList.Attributes.Remove("class");
            lireport.Attributes.Remove("class");


        }

        if (ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21")
        {
            lbladmin.Visible = false;
            liToInsertList.Visible = false;
            lireport.Visible = false;
            BoxSearch.Visible = false;
        }

        linkBtnTrigger(lblguide);
        linkBtnTrigger(lblflow);
    }
    #endregion

    #region Select

    protected void select_empIdx_present()
    {

        _dataEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dataEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dataEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dataEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dataEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dataEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dataEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dataEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dataEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dataEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dataEmployee.employee_list[0].costcenter_idx;

    }

    protected void SelectTypeMA(DropDownList ddlName)
    {

        _datama.BoxM0_MaList = new M0_maList[1];
        M0_maList m0type = new M0_maList();

        _datama.BoxM0_MaList[0] = m0type;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectTypeMA, _datama);

        setDdlData(ddlName, _datama.BoxM0_MaList, "detail", "m0_tdidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทอุปกรณ์...", "0"));

    }

    protected void SelectDevices()
    {
        DropDownList ddlsearchtype = (DropDownList)FvMaList.FindControl("ddlsearchtype");
        DropDownList ddldevices = (DropDownList)FvMaList.FindControl("ddldevices");


        _datama.BoxM0_MaList = new M0_maList[1];
        M0_maList m0type = new M0_maList();
        m0type.typeidx = int.Parse(ddlsearchtype.SelectedValue);

        _datama.BoxM0_MaList[0] = m0type;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectDevicesMA, _datama);

        setDdlData(ddldevices, _datama.BoxMa_Online_List, "u0_code", "u0_didx");
        ddldevices.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์...", "0"));

    }

    protected void SelectMasterList()
    {
        datama_online _datama = new datama_online();
        _datama.BoxM0_MaList = new M0_maList[1];
        M0_maList select = new M0_maList();


        _datama.BoxM0_MaList[0] = select;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _datama = callServiceMA(urlSelectMasterMA, _datama);

        setGridData(GvMaster, _datama.BoxM0_MaList);
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกผู้ถือครอง...", "0"));
    }

    protected void getStatusList(DropDownList ddlName)
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];

        _datama = callServiceMA(urlSelectStatus, _datama);
        setDdlData(ddlName, _datama.BoxMa_Online_List, "node_desc", "nodidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกขั้นตอนดำเนินการ...", "0"));
    }

    protected void SelectHolerDevices()
    {
        DropDownList ddlempidx = (DropDownList)FvMaList.FindControl("ddlempidx");
        DropDownList ddl_holderdevice = (DropDownList)FvMaList.FindControl("ddl_holderdevice");


        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List u0doc = new Ma_Online_List();
        u0doc.CEmpIDX = int.Parse(ddlempidx.SelectedValue);

        _datama.BoxMa_Online_List[0] = u0doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectHolderDevicesMA, _datama);

        setDdlData(ddl_holderdevice, _datama.BoxMa_Online_List, "u0_code", "u0_didx");
        ddl_holderdevice.Items.Insert(0, new ListItem("กรุณาเลือกผู้ถือครอง...", "0"));

    }

    protected void SelectDevicesMA()
    {
        var ddlsearch = (DropDownList)FvMaList.FindControl("ddlsearch");
        var ddlsearchtype = (DropDownList)FvMaList.FindControl("ddlsearchtype");
        var ddldevices = (DropDownList)FvMaList.FindControl("ddldevices");
        var ddl_holderdevice = (DropDownList)FvMaList.FindControl("ddl_holderdevice");


        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        if (ViewState["ddlsearch"].ToString() == "1")
        {
            doc.deviceidx = int.Parse(ddldevices.SelectedValue);
        }
        else
        {

            doc.deviceidx = int.Parse(ddl_holderdevice.SelectedValue);
        }

        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectSearchDevicesMA, _datama);
        setFormViewData(FvDevice, _datama.BoxMa_Online_List);
    }

    protected void SelectListMA()
    {
        var ddlsearch = (DropDownList)FvMaList.FindControl("ddlsearch");
        var ddlsearchtype = (DropDownList)FvMaList.FindControl("ddlsearchtype");
        var ddldevices = (DropDownList)FvMaList.FindControl("ddldevices");
        var ddl_holderdevice = (DropDownList)FvMaList.FindControl("ddl_holderdevice");


        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        if (ViewState["ddlsearch"].ToString() == "1")
        {
            doc.deviceidx = int.Parse(ddldevices.SelectedValue);
        }
        else
        {

            doc.deviceidx = int.Parse(ddl_holderdevice.SelectedValue);
        }

        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectSearchListMA, _datama);
        setGridData(GvMa, _datama.BoxM0_MaList);
    }

    protected void SelecttListMA_User()
    {


        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _datama.BoxMa_Online_List[0] = doc;


        _datama = callServiceMA(urlSelecttListMA_User, _datama);
        setGridData(GvSelectMA_User, _datama.BoxMa_Online_List);
    }

    protected void SelectDetail_Device()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectDetail_Device, _datama);
        setFormViewData(FvDetailDevices, _datama.BoxMa_Online_List);

        ViewState["rdept_name_Holder"] = _datama.BoxMa_Online_List[0].DeptNameTH;
        ViewState["FullName_Holder"] = _datama.BoxMa_Online_List[0].EmpName;
        ViewState["Org_name_Holder"] = _datama.BoxMa_Online_List[0].OrgNameTH;

        ViewState["EmpCode_Holder"] = _datama.BoxMa_Online_List[0].EmpCode;
        ViewState["Positname_Holder"] = _datama.BoxMa_Online_List[0].PosNameTH;
        ViewState["Email_Holder"] = _datama.BoxMa_Online_List[0].Email;
        ViewState["Tel_Holder"] = _datama.BoxMa_Online_List[0].MobileNo;
        ViewState["Secname_Holder"] = _datama.BoxMa_Online_List[0].SecNameTH;

    }

    protected void SelectDetail_MA()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectDetail_MA, _datama);

        // ViewState["Detail_MA"] += _datama.BoxMa_Online_List[0].m0idx;
        setGridData(GvDetailMA, _datama.BoxMa_Online_List);
    }

    protected void SelecttListMA_Support()
    {


        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];

        _datama = callServiceMA(urlSelecttListMA_Support, _datama);
        setGridData(GvSelectMA_User, _datama.BoxMa_Online_List);
    }

    protected void SelecttSearchMA_Support()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.unidx = int.Parse(ddlSearchStatus.SelectedValue);
        doc.orgidx = int.Parse(ddlSearchOrg.SelectedValue);
        doc.RdeptIDX = int.Parse(ddlSearchDep.SelectedValue);
        doc.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        doc.CreateDate = AddStartdate.Text;
        doc.EndDate = AddEndDate.Text;

        _datama.BoxMa_Online_List[0] = doc;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

        _datama = callServiceMA(urlSelecttSearchMA_Support, _datama);
        setGridData(GvSelectMA_User, _datama.BoxMa_Online_List);
    }

    protected void SelecttSearch_Report()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.unidx = int.Parse(ddlSearchStatus_Report.SelectedValue);
        doc.orgidx = int.Parse(ddlSearchOrg_Report.SelectedValue);
        doc.RdeptIDX = int.Parse(ddlSearchDept_Report.SelectedValue);
        doc.IFSearchbetween = int.Parse(ddlSearchDate_report.SelectedValue);
        doc.CreateDate = AddStartDate_report.Text;
        doc.EndDate = AddEndDate_report.Text;

        _datama.BoxMa_Online_List[0] = doc;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

        _datama = callServiceMA(urlSelecttSearch_Report, _datama);
        setGridData(GvReport, _datama.BoxMa_Online_List);
    }

    protected void SelectLog()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectLog, _datama);
        setRptData(rptLog, _datama.BoxMa_Online_List);
    }

    public void Select_ChartDeptName()
    {
        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();


        doc.unidx = int.Parse(ddlSearchStatus_Report.SelectedValue);
        doc.orgidx = int.Parse(ddlSearchOrg_Report.SelectedValue);
        doc.RdeptIDX = int.Parse(ddlSearchDept_Report.SelectedValue);
        doc.IFSearchbetween = int.Parse(ddlSearchDate_report.SelectedValue);
        doc.CreateDate = AddStartDate_report.Text;
        doc.EndDate = AddEndDate_report.Text;

        _datama.BoxMa_Online_List[0] = doc;


        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(datasupport));

        _local_xml = serviceexcute.actionExec("conn_mis", "datama_online", "services_ma", _datama, 216);
        _datama = (datama_online)_funcTool.convertXmlToObject(typeof(datama_online), _local_xml);

        if (_datama.ReturnCode == "0")
        {
            int count = _datama.BoxMa_Online_List.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _datama.BoxMa_Online_List)
            {
                lv1code[i] = data.DeptNameTH.ToString();
                lv1count[i] = data.countdept.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Department Name",
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    protected void SelectDevicesMA_edit()
    {


        _datama = new datama_online();
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List doc = new Ma_Online_List();

        doc.deviceidx = int.Parse(ViewState["deviceidx"].ToString());

        _datama.BoxMa_Online_List[0] = doc;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_datama));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlSelectSearchListMA, _datama);
        setGridData(GvMA_Edit, _datama.BoxM0_MaList);
    }

    #endregion

    #region Insert
    protected void Insert_Master()
    {
        _datama = new datama_online();

        _datama.BoxM0_MaList = new M0_maList[1];
        M0_maList insertmaster = new M0_maList();



        insertmaster.typeidx = int.Parse(ddltype.SelectedValue);
        insertmaster.ma_name = txtaddma.Text;
        insertmaster.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insertmaster.m0status = int.Parse(ddlStatus.SelectedValue);

        _datama.BoxM0_MaList[0] = insertmaster;

        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
        _datama = callServiceMA(urlInsert_MasterMAList, _datama);

        if (_datama.ReturnCode == "0")
        {

            SelectMasterList();

        }
        else
        {
            setError(_datama.ReturnCode.ToString() + " - " + _datama.ReturnMsg);
        }
    }

    #endregion

    #region Update

    protected void Update_Approve()
    {
        Label lblu2idx = (Label)FvDetailDevices.FindControl("lblu2idx");

        _datama.BoxMa2_Online_List = new Ma2_Online_List[1];
        Ma2_Online_List update = new Ma2_Online_List();


        update.comment_user = txtusercomment.Text;
        update.approveidx = int.Parse(ViewState["EmpIDX"].ToString());
        update.u2idx = int.Parse(lblu2idx.Text);

        _datama.BoxMa2_Online_List[0] = update;


        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List updateu0 = new Ma_Online_List();

        updateu0.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
        updateu0.staidx = int.Parse(ddl_approve.SelectedValue);
        updateu0.unidx = int.Parse(ViewState["m0_node"].ToString());
        updateu0.acidx = int.Parse(ViewState["m0_actor"].ToString());

        _datama.BoxMa_Online_List[0] = updateu0;



        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

        _datama = callServiceMA(urlUpdate_ApproveMA, _datama);

    }

    protected void Update_Approve_Admin()
    {
        _datama.BoxMa_Online_List = new Ma_Online_List[1];
        Ma_Online_List update = new Ma_Online_List();

        update.comment_it = txtadmincomment.Text;
        update.staidx = int.Parse(ddladmin_approve.SelectedValue);
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        update.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
        update.unidx = int.Parse(ViewState["m0_node"].ToString());
        update.acidx = int.Parse(ViewState["m0_actor"].ToString());

        _datama.BoxMa_Online_List[0] = update;
        _datama = callServiceMA(urlUpdate_ApproveAdmin, _datama);

    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetaiAdmin":
                FormView FvDetaiAdmin = (FormView)ViewInsert.FindControl("FvDetaiAdmin");

                if (FvDetaiAdmin.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetaiAdmin.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetaiAdmin.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetaiAdmin.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetaiAdmin.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetaiAdmin.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetaiAdmin.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetaiAdmin.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetaiAdmin.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();



                }
                break;

            case "FvMaList":
                DropDownList ddlsearchtype = (DropDownList)FvMaList.FindControl("ddlsearchtype");

                SelectTypeMA(ddlsearchtype);

                break;

            case "FvDevice":

                if (FvDevice.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewApprove.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode_Holder"].ToString();
                    txtrequesname.Text = ViewState["FullName_Holder"].ToString();
                    txtorg.Text = ViewState["Org_name_Holder"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_Holder"].ToString();
                    txtsec.Text = ViewState["Secname_Holder"].ToString();
                    txtpos.Text = ViewState["Positname_Holder"].ToString();
                    txttel.Text = ViewState["Tel_Holder"].ToString();
                    txtemail.Text = ViewState["Email_Holder"].ToString();

                }
                break;

            case "FvDetailDevices":
                if (FvDetailDevices.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;


        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        var ddlseach = (DropDownList)FvMaList.FindControl("ddlseach");
        var panel_typedevice = (Panel)FvMaList.FindControl("panel_typedevice");
        var panel_holder = (Panel)FvMaList.FindControl("panel_holder");
        var ddlorgidx = (DropDownList)FvMaList.FindControl("ddlorgidx");
        var ddldeptidx = (DropDownList)FvMaList.FindControl("ddldeptidx");
        var ddlsecidx = (DropDownList)FvMaList.FindControl("ddlsecidx");
        var ddlempidx = (DropDownList)FvMaList.FindControl("ddlempidx");
        var ddl_holderdevice = (DropDownList)FvMaList.FindControl("ddl_holderdevice");
        var ddlsearchtype = (DropDownList)FvMaList.FindControl("ddlsearchtype");
        var ddldevices = (DropDownList)FvMaList.FindControl("ddldevices");

        switch (ddName.ID)
        {
            case "ddlsearchtype":

                SelectDevices();

                break;

            case "ddlseach":
                div_FvDevice.Visible = false;
                //   div_ma.Visible = false;

                if (ddlseach.SelectedValue == "1")
                {
                    panel_typedevice.Visible = true;
                    panel_holder.Visible = false;
                    ddlorgidx.SelectedValue = "0";
                    ddldeptidx.SelectedValue = "0";
                    ddlsecidx.SelectedValue = "0";
                    ddlempidx.SelectedValue = "0";
                    ddl_holderdevice.SelectedValue = "0";
                }
                else if (ddlseach.SelectedValue == "2")
                {
                    panel_typedevice.Visible = false;
                    panel_holder.Visible = true;
                    ddlsearchtype.SelectedValue = "0";
                    ddldevices.SelectedValue = "0";
                    getOrganizationList(ddlorgidx);

                }
                else
                {
                    panel_typedevice.Visible = false;
                    panel_holder.Visible = false;
                    ddlorgidx.SelectedValue = "0";
                    ddldeptidx.SelectedValue = "0";
                    ddlsecidx.SelectedValue = "0";
                    ddlempidx.SelectedValue = "0";
                    ddl_holderdevice.SelectedValue = "0";
                    ddlsearchtype.SelectedValue = "0";
                    ddldevices.SelectedValue = "0";

                    //FvDevice.DataSource = null;
                    //FvDevice.DataBind();
                }

                break;

            case "ddlorgidx":
                getDepartmentList(ddldeptidx, int.Parse(ddlorgidx.SelectedValue));

                break;
            case "ddldeptidx":
                getSectionList(ddlsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddldeptidx.SelectedValue));

                break;

            case "ddlsecidx":
                getEmpList(ddlempidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddldeptidx.SelectedValue), int.Parse(ddlsecidx.SelectedValue));

                break;

            case "ddlempidx":
                SelectHolerDevices();
                break;


            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddlSearchOrg":
                getDepartmentList(ddlSearchDep, int.Parse(ddlSearchOrg.SelectedValue));
                break;

            case "ddlSearchDate_report":

                if (int.Parse(ddlSearchDate_report.SelectedValue) == 3)
                {
                    AddEndDate_report.Enabled = true;
                }
                else
                {
                    AddEndDate_report.Enabled = false;
                    AddEndDate_report.Text = string.Empty;
                }

                break;

            case "ddlSearchOrg_Report":
                getDepartmentList(ddlSearchDept_Report, int.Parse(ddlSearchOrg_Report.SelectedValue));

                break;


        }
    }
    #endregion

    #region bind Node

    protected void setFormData()
    {

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailDevices.FindControl("hfM0NodeIDX");
        ViewState["m0_node"] = hfM0NodeIDX.Value;

        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;


            case 2: // พิจารณาผลโดยผู้ใข้งานทั่วไป

                setFormDataActor();

                break;

            case 9: // จบการดำเนินการ


                setFormDataActor();

                break;
        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailDevices.FindControl("hfM0ActoreIDX");
        Label lblAEmpIDX = (Label)FvDetailDevices.FindControl("lblAEmpIDX");

        Control panel_user_detailcomment = (Control)FvDetailDevices.FindControl("panel_user_detailcomment");
        /// CheckBox chkma_edit = ((CheckBox)GvMA_Edit.FindControl("chkma_edit"));

        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //  litDebug.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString();

        switch (m0_actor)
        {

            case 1: // เจ้าหน้าที่ IT
                if (ViewState["m0_node"].ToString() == "1" && ViewState["rdept_idx"].ToString() == "20" ||
                    ViewState["m0_node"].ToString() == "1" && ViewState["rdept_idx"].ToString() == "21")
                {
                    panel_user_comment.Visible = false;
                    panel_user_detailcomment.Visible = false;
                    div_approve.Visible = true;
                    panel_admin.Visible = true;
                }
                else
                {
                    panel_user_comment.Visible = false;
                    panel_user_detailcomment.Visible = true;
                }


                break;

            case 2: // User

                // litDebug.Text = ViewState["value_edit"].ToString();
                if (ViewState["value_edit"].ToString() == "1")
                {
                    if (ViewState["m0_node"].ToString() == "2" && lblAEmpIDX.Text == ViewState["EmpIDX"].ToString())
                    {
                        panel_user_comment.Visible = true;
                        div_approve.Visible = true;
                    }
                    else if (ViewState["m0_node"].ToString() != "9")
                    {
                        panel_user_comment.Visible = false;
                        panel_user_detailcomment.Visible = false;
                    }
                    else
                    {
                        panel_user_comment.Visible = false;
                        panel_user_detailcomment.Visible = true;
                    }

                }
                else if (ViewState["value_edit"].ToString() == "2")
                {
                    panel_user_comment.Visible = false;
                    panel_user_detailcomment.Visible = false;
                    div_approve.Visible = false;
                    if (ViewState["rdept_idx"].ToString() == "20" || ViewState["rdept_idx"].ToString() == "21")
                    {
                        //  chkma_edit.Enabled = true;

                    }
                }


                break;

        }

    }

    #endregion

    #region SetFunction

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRptData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void setError(string _errorText)
    {
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void SetDefault_master()
    {
        Panel_Add.Visible = false;
        BoxButtonSearchShow.Visible = true;
        BoxButtonSearchHide.Visible = false;
        txtaddma.Text = String.Empty;

        div_FvDevice.Visible = false;
        div_ma.Visible = false;



    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void SetViewState()
    {
        var dsEmp = new DataSet();
        dsEmp.Tables.Add("TempDevice");

        dsEmp.Tables[0].Columns.Add("u0_didx", typeof(int));
        dsEmp.Tables[0].Columns.Add("u0_code", typeof(string));
        dsEmp.Tables[0].Columns.Add("m0idx", typeof(int));
        dsEmp.Tables[0].Columns.Add("ma_name", typeof(string));
        dsEmp.Tables[0].Columns.Add("CEmpIDX", typeof(int));

        ViewState["vsBuyequipment"] = dsEmp;
    }

    protected void SetDefaultreport()
    {
        ddltypereport.SelectedValue = "0";
        ddlSearchDate_report.SelectedValue = "0";
        AddStartDate_report.Text = String.Empty;
        AddEndDate_report.Text = String.Empty;
        // ddlSearchOrg_Report.SelectedValue = "0";
        getOrganizationList(ddlSearchOrg_Report);
        ddlSearchDept_Report.SelectedValue = "0";
        getStatusList(ddlSearchStatus_Report);
        div_table.Visible = false;
        div_grant.Visible = false;
    }



    #endregion

    #region CallService

    protected datama_online callServiceMA(string _cmdUrl, datama_online _datama)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_datama);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _datama = (datama_online)_funcTool.convertJsonToObject(typeof(datama_online), _localJson);

        return _datama;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }


    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion

    #region GridView

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectMasterList();

                break;

            case "GvMa":

                GvMa.PageIndex = e.NewPageIndex;
                GvMa.DataBind();
                SelectListMA();

                break;

            case "GvReportAdd":

                GvReportAdd.PageIndex = e.NewPageIndex;
                GvReportAdd.DataSource = ViewState["vsBuyequipment"];
                GvReportAdd.DataBind();

                break;

            case "GvSelectMA_User":

                GvSelectMA_User.PageIndex = e.NewPageIndex;
                GvSelectMA_User.DataBind();

                if (ViewState["paging"].ToString() == "1")
                {
                    SelecttListMA_User();

                }
                else
                {
                    SelecttListMA_Support();
                }
                break;

            /*   case "GvDetailMA":

                   GvDetailMA.PageIndex = e.NewPageIndex;
                   GvDetailMA.DataBind();
                   SelectDetail_MA();
                   break;
                   */

            case "GvReport":

                GvReport.PageIndex = e.NewPageIndex;
                GvReport.DataBind();
                SelecttSearch_Report();
                break;

                //case "GvMA_Edit":
                //    GvMA_Edit.PageIndex = e.NewPageIndex;
                //    GvMA_Edit.DataBind();
                //    SelectDevicesMA_edit();

                //    break;

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        var ddltypeUpdate = (DropDownList)e.Row.FindControl("ddltypeUpdate");
                        var lbltypeidx = (Label)e.Row.FindControl("lbltypeidx");

                        SelectTypeMA(ddltypeUpdate);
                        ddltypeUpdate.SelectedValue = lbltypeidx.Text;
                    }

                }



                break;

            case "GvSelectMA_User":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var btnedit = ((LinkButton)e.Row.FindControl("btnedit"));
                    var lblunidx = ((Label)e.Row.FindControl("lblunidx"));
                    var lblacidx = ((Label)e.Row.FindControl("lblacidx"));

                    if (GvSelectMA_User.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        if (ViewState["rdept_idx"].ToString() == "20" && lblunidx.Text == "2" && lblacidx.Text == "2" ||
                            ViewState["rdept_idx"].ToString() == "21" && lblunidx.Text == "2" && lblacidx.Text == "2")
                        {
                            btnedit.Visible = true;
                        }
                        else
                        {
                            btnedit.Visible = false;
                        }
                    }
                }

                break;

            case "GvDetailMA":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //var btndelete_edit = ((LinkButton)e.Row.FindControl("btndelete_edit"));
                    //var hfM0NodeIDX = ((HiddenField)FvDetailDevices.FindControl("hfM0ActoreIDX"));
                    //var hfM0ActoreIDX = ((HiddenField)FvDetailDevices.FindControl("hfM0ActoreIDX"));
                    var lblm0idx = ((Label)e.Row.FindControl("lblm0idx"));
                    if (GvDetailMA.EditIndex != e.Row.RowIndex) //to overlook header row
                    {


                        ViewState["Detail_MA"] += lblm0idx.Text + ",";

                        // litDebug.Text = ViewState["m0_node"].ToString() + "/" + ViewState["m0_actor"].ToString() + "/" + ViewState["value_edit"].ToString();
                        //if (ViewState["rdept_idx"].ToString() == "20" && hfM0NodeIDX.Value == "2" &&
                        //    hfM0ActoreIDX.Value == "2" && ViewState["value_edit"].ToString() == "2" ||
                        //    ViewState["rdept_idx"].ToString() == "21" && hfM0NodeIDX.Value == "2" &&
                        //    hfM0ActoreIDX.Value == "2" && ViewState["value_edit"].ToString() == "2")
                        //{

                        //    btndelete_edit.Visible = true;

                        //}
                        //else
                        //{
                        //    btndelete_edit.Visible = false;
                        //}
                    }
                }
                break;

            case "GvMA_Edit":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (GvMA_Edit.EditIndex != e.Row.RowIndex) //to overlook header row
                {
                    var chkma_edit = (CheckBox)e.Row.FindControl("chkma_edit");
                    var lblm0idx_edit = (Literal)e.Row.FindControl("lblm0idx_edit");
                    // litDebug.Text = ViewState["Detail_MA"].ToString();
                    if (ViewState["value_edit"].ToString() == "2")
                    {
                        chkma_edit.Enabled = true;
                        div_btnedit.Visible = true;
                    }
                    else
                    {
                        chkma_edit.Enabled = false;
                        div_btnedit.Visible = false;
                    }

                    string[] ToId = ViewState["Detail_MA"].ToString().Split(',');
                    foreach (string To_check in ToId)
                    {
                        if (To_check != String.Empty)
                        {
                            if (int.Parse(To_check) == int.Parse(lblm0idx_edit.Text))
                            {
                                chkma_edit.Checked = true;
                            }
                        }
                    }

                }

                break;

            case "GvReport":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;

        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0idx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddltypeUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltypeUpdate");
                var txtmanameupdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtmanameupdate");
                var ddlStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlStatusUpdate");

                GvMaster.EditIndex = -1;

                _datama = new datama_online();

                _datama.BoxM0_MaList = new M0_maList[1];
                M0_maList insertmaster = new M0_maList();


                insertmaster.m0idx = m0idx;
                insertmaster.typeidx = int.Parse(ddltypeUpdate.SelectedValue);
                insertmaster.ma_name = txtmanameupdate.Text;
                insertmaster.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                insertmaster.m0status = int.Parse(ddlStatusUpdate.SelectedValue);

                _datama.BoxM0_MaList[0] = insertmaster;

                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
                _datama = callServiceMA(urlInsert_MasterMAList, _datama);

                if (_datama.ReturnCode == "0")
                {

                    SelectMasterList();

                }
                else
                {
                    setError(_datama.ReturnCode.ToString() + " - " + _datama.ReturnMsg);
                }

                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;
        }
    }

    protected void Master_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvReportAdd":

                var dsvsBuyequipmentDelete = (DataSet)ViewState["vsBuyequipment"];
                var drDriving = dsvsBuyequipmentDelete.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["vsBuyequipment"] = dsvsBuyequipmentDelete;
                GvReportAdd.EditIndex = -1;
                GvReportAdd.DataSource = ViewState["vsBuyequipment"];
                GvReportAdd.DataBind();

                break;
        }
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        M0_maList _master = new M0_maList();
        Ma_Online_List _document = new Ma_Online_List();
        //  Ma1_Online_List _document1 = new Ma1_Online_List();
        Ma2_Online_List _document2 = new Ma2_Online_List();

        DropDownList ddlseach = (DropDownList)FvMaList.FindControl("ddlseach");
        Label lblcode = (Label)FvDevice.FindControl("lblcode");
        Label lblu0didx = (Label)FvDevice.FindControl("lblu0didx");
        Label lblEmpIDX = (Label)FvDevice.FindControl("lblEmpIDX");
        Label lblNOrgIDX = (Label)FvDevice.FindControl("lblNOrgIDX");
        Label lblNRDeptIDX = (Label)FvDevice.FindControl("lblNRDeptIDX");
        Label lblNRSecIDX = (Label)FvDevice.FindControl("lblNRSecIDX");
        TextBox txtremark = (TextBox)panel_comment.FindControl("txtremark");
        CheckBox chkma_edit = (CheckBox)GvMA_Edit.FindControl("chkma_edit");

        switch (cmdName)
        {

            case "btnIndex_User":
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbladmin.BackColor = System.Drawing.Color.Transparent;
                lbinsertma.BackColor = System.Drawing.Color.Transparent;
                lbinsert.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;

                ViewState["paging"] = "1";

                //MvMaster.SetActiveView(ViewIndex);
                //SetDefault();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsert_support":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbladmin.BackColor = System.Drawing.Color.LightGray;
                lbinsertma.BackColor = System.Drawing.Color.Transparent;
                lbinsert.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;
                ViewState["paging"] = "2";

                SetDefault_master();
                MvMaster.SetActiveView(ViewApprove);
                BoxSearch.Visible = true;
                div_showdata.Visible = false;
                gridviewindex.Visible = true;


                SelecttListMA_Support();

                _divMenuLiToDivIndex.Attributes.Add("class", "active");
                liToInsertList.Attributes.Remove("class");
                lireport.Attributes.Remove("class");

                //SetDefault();
                //  Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnInsert":

                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbladmin.BackColor = System.Drawing.Color.Transparent;
                lbinsertma.BackColor = System.Drawing.Color.Transparent;
                lbinsert.BackColor = System.Drawing.Color.LightGray;
                lbreport.BackColor = System.Drawing.Color.Transparent;

                liToInsertList.Attributes.Add("class", "active");
                _divMenuLiToDivIndex.Attributes.Remove("class");
                lireport.Attributes.Remove("class");


                SetDefault_master();
                ViewState["vsBuyequipment"] = null;
                MvMaster.SetActiveView(ViewInsert);
                SetViewState();

                FvDetaiAdmin.ChangeMode(FormViewMode.Insert);
                FvDetaiAdmin.DataBind();

                FvMaList.ChangeMode(FormViewMode.Insert);
                FvMaList.DataBind();



                break;

            case "btnInsertma":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbladmin.BackColor = System.Drawing.Color.Transparent;
                lbinsertma.BackColor = System.Drawing.Color.LightGray;
                lbinsert.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;

                liToInsertList.Attributes.Add("class", "active");
                _divMenuLiToDivIndex.Attributes.Remove("class");
                lireport.Attributes.Remove("class");


                MvMaster.SetActiveView(ViewInsertMaster);
                SetDefault_master();
                SelectTypeMA(ddltype);
                SelectMasterList();
                ddltype.SelectedValue = "0";
                litDebug.Text = String.Empty;

                break;

            case "btnReport":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbladmin.BackColor = System.Drawing.Color.Transparent;
                lbinsertma.BackColor = System.Drawing.Color.Transparent;
                lbinsert.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.LightGray;

                lireport.Attributes.Add("class", "active");
                _divMenuLiToDivIndex.Attributes.Remove("class");
                liToInsertList.Attributes.Remove("class");


                MvMaster.SetActiveView(ViewReport);
                //getOrganizationList(ddlSearchOrg_Report);
                //getStatusList(ddlSearchStatus_Report);
                SetDefaultreport();
                break;


            case "btnInsertmaster":
                Insert_Master();
                Panel_Add.Visible = false;
                SetDefault_master();
                SelectMasterList();
                ddltype.SelectedValue = "0";

                break;


            case "btncancelmaster":
                Panel_Add.Visible = false;
                SetDefault_master();
                SelectMasterList();
                ddltype.SelectedValue = "0";
                litDebug.Text = String.Empty;

                break;

            case "BtnHideSETBoxAllSearchShow":

                Panel_Add.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                litDebug.Text = String.Empty;

                break;

            case "BtnHideSETBoxAllSearchHide":

                Panel_Add.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;
                litDebug.Text = String.Empty;


                break;


            case "BtnHideSETBoxAllSearchShow_Search":
                SETBoxAllSearch.Visible = true;
                BoxButtonSearchShow_Search.Visible = false;
                BoxButtonSearchHide_Search.Visible = true;
                break;

            case "BtnHideSETBoxAllSearchHide_Search":
                SETBoxAllSearch.Visible = false;
                BoxButtonSearchShow_Search.Visible = true;
                BoxButtonSearchHide_Search.Visible = false;

                break;

            case "btnDelete":

                int m0idx = int.Parse(cmdArg);

                _datama.BoxM0_MaList = new M0_maList[1];

                _master.m0idx = m0idx;
                _master.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _datama.BoxM0_MaList[0] = _master;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _datama = callServiceMA(urlDelete_MasterMAList, _datama);


                if (_datama.ReturnCode == "0")
                {

                    SelectMasterList();

                }
                else
                {
                    setError(_datama.ReturnCode.ToString() + " - " + _datama.ReturnMsg);
                }

                break;

            /*   case "btnDelete_edit":
                   int m0idx_edit = int.Parse(cmdArg);

                   //  litDebug.Text = Convert.ToString(m0idx_edit);
                   _datama.BoxMa_Online_List = new Ma_Online_List[1];

                   _document.m0idx = m0idx_edit;
                   _document.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
                   _document.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                   _document.unidx = int.Parse(ViewState["m0_node"].ToString());
                   _document.acidx = int.Parse(ViewState["m0_actor"].ToString());

                   _datama.BoxMa_Online_List[0] = _document;


                   //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

                   _datama = callServiceMA(urlDelete_MaU1, _datama);

                  // SelectDetail_MA();
                   //if (_datama.ReturnCode == "0")
                   //{

                   //    SelectDetail_MA();

                   //}
                   //else
                   //{
                   //    setError(_datama.ReturnCode.ToString() + " - " + _datama.ReturnMsg);
                   //}


                   break;*/

            case "btnsearch_insert":
                ViewState["ddlsearch"] = ddlseach.SelectedValue;
                SelectDevicesMA();
                SelectListMA();
                div_FvDevice.Visible = true;
                break;

            case "btninsert_ma":

                value = String.Empty;
                ma = String.Empty;


                foreach (GridViewRow row in GvMa.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkma = (row.Cells[0].FindControl("chkma") as CheckBox);
                        if (chkma.Checked)
                        {

                            value = (row.Cells[1].FindControl("lblm0idx") as Literal).Text;
                            ma = (row.Cells[1].FindControl("lblma") as Literal).Text;



                            var dsEquiment = (DataSet)ViewState["vsBuyequipment"];
                            var drEquiment = dsEquiment.Tables[0].NewRow();


                            drEquiment["u0_didx"] = int.Parse(lblu0didx.Text);
                            drEquiment["u0_code"] = lblcode.Text;
                            drEquiment["m0idx"] = int.Parse(value);
                            drEquiment["ma_name"] = ma;
                            drEquiment["CEmpIDX"] = int.Parse(lblEmpIDX.Text);


                            dsEquiment.Tables[0].Rows.Add(drEquiment);
                            ViewState["vsBuyequipment"] = dsEquiment;


                            //litDebug.Text += value + ",";
                        }

                    }
                }

                GvReportAdd.DataSource = (DataSet)ViewState["vsBuyequipment"];
                GvReportAdd.DataBind();
                div_ma.Visible = true;

                break;

            case "CmdInsert":
                Label lblm0idx = (Label)GvReportAdd.FindControl("lblm0idx");
                Label lblu0_didx = (Label)GvReportAdd.FindControl("lblu0_didx");
                Label lblEmpIDX_ = (Label)FvDevice.FindControl("lblEmpIDX");
                Label lblu0didx_ = (Label)FvDevice.FindControl("lblu0didx");

                u0_didx = String.Empty;
                m0idx = 0;


                _datama.BoxMa_Online_List = new Ma_Online_List[1];
                _document.deviceidx = int.Parse(lblu0didx_.Text);
                _document.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                _document.unidx = 1;
                _document.acidx = 1;
                _document.staidx = 1;
                _document.comment_it = txtremark.Text;
                _document.orgidx = int.Parse(lblNOrgIDX.Text);
                _document.RdeptIDX = int.Parse(lblNRDeptIDX.Text);
                _document.RsecIDX = int.Parse(lblNRSecIDX.Text);
                _document.AEmpIDX = int.Parse(lblEmpIDX_.Text);
                _datama.BoxMa_Online_List[0] = _document;


                int i = 0;
                var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
                var _document1 = new Ma1_Online_List[ds_udoc1_insert.Tables[0].Rows.Count];

                foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
                {

                    //  u0_didx = (row.Cells[1].FindControl("lblu0_didx") as Label).Text;
                    //m0idx = int.Parse((row.Cells[2].FindControl("lblm0idx") as Label).Text);
                    //    empidx = int.Parse((row.Cells[2].FindControl("lblemp") as Literal).Text);

                    _document1[i] = new Ma1_Online_List();

                    //   _datama.BoxMa1_Online_List = new Ma1_Online_List[1];
                    _document1[i].m0idx = int.Parse(dtrow["m0idx"].ToString());
                    _datama.BoxMa1_Online_List = _document1;

                    i++;
                }
                //   litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

                _datama = callServiceMA(urlInsertListMA, _datama);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "ViewMaList":
                string[] arg_detail = new string[3];
                arg_detail = e.CommandArgument.ToString().Split(';');
                int IDX = int.Parse(arg_detail[0]);
                int deviceidx = int.Parse(arg_detail[1]);
                int value_edit = int.Parse(arg_detail[2]);

                ViewState["U0IDX_Detail"] = IDX;
                ViewState["deviceidx"] = deviceidx;
                ViewState["value_edit"] = value_edit;

                //litDebug.Text = ViewState["value_edit"].ToString();


                //litDebug.Text = ViewState["TypeIDX"].ToString();

                gridviewindex.Visible = false;
                BoxSearch.Visible = false;
                div_showdata.Visible = true;
                SelectDetail_Device(); //ดึงข้อมูลผู้ถือครองมาใช้bind FvDetailUser

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();


                SelectDetail_MA();
                SelectDevicesMA_edit();
                setFormData();

                SelectLog();


                break;


            case "CmdUpdate":
                Update_Approve();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdUpdate_admin":
                Update_Approve_Admin();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnsearch":
                SelecttSearchMA_Support();
                break;

            case "btnsearch_report":

                if (ddltypereport.SelectedValue == "1")
                {
                    div_table.Visible = true;
                    div_grant.Visible = false;
                    SelecttSearch_Report();
                }
                else
                {
                    div_table.Visible = false;
                    div_grant.Visible = true;
                    Select_ChartDeptName();
                }
                break;

            case "btnexport":
                _datama = new datama_online();
                _datama.BoxMa_Online_List = new Ma_Online_List[1];
                Ma_Online_List doc = new Ma_Online_List();

                doc.unidx = int.Parse(ddlSearchStatus_Report.SelectedValue);
                doc.orgidx = int.Parse(ddlSearchOrg_Report.SelectedValue);
                doc.RdeptIDX = int.Parse(ddlSearchDept_Report.SelectedValue);
                doc.IFSearchbetween = int.Parse(ddlSearchDate_report.SelectedValue);
                doc.CreateDate = AddStartDate_report.Text;
                doc.EndDate = AddEndDate_report.Text;

                _datama.BoxMa_Online_List[0] = doc;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));

                _datama = callServiceMA(urlSelectExport_Report, _datama);
                GridView2.DataSource = _datama.Export_BoxMa_Online_List;
                GridView2.DataBind();


                GridView2.AllowSorting = false;
                GridView2.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GridView2.Columns[0].Visible = true;
                GridView2.HeaderRow.BackColor = Color.White;

                foreach (TableCell cell in GridView2.HeaderRow.Cells)
                {
                    cell.BackColor = GridView2.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GridView2.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView2.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView2.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView2.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
                break;


            case "CmdSave_edit":
                int ii = 0;
                // Ma1_Online_List _document1_edit = new Ma1_Online_List();
                //  _datama.BoxMa1_Online_List = new Ma1_Online_List[1];
                _datama.BoxMa_Online_List = new Ma_Online_List[1];

                _document.u0idx = int.Parse(ViewState["U0IDX_Detail"].ToString());
                _document.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _datama.BoxMa_Online_List[0] = _document;

                var _document1_edit = new Ma1_Online_List[GvMA_Edit.Rows.Count];


                foreach (GridViewRow row in GvMA_Edit.Rows)
                {
                    //if (row.RowType == DataControlRowType.DataRow)
                    //{

                    CheckBox chkma_edit_ = (row.Cells[0].FindControl("chkma_edit") as CheckBox);
                    Literal lblm0idx_edit = (row.Cells[1].FindControl("lblm0idx_edit") as Literal);

                    _document1_edit[ii] = new Ma1_Online_List();

                    if (chkma_edit_.Checked)
                    {
                        litDebug.Text += lblm0idx_edit.Text + ",";
                        _document1_edit[ii].m0idx = int.Parse(lblm0idx_edit.Text);
                        _document1_edit[ii].u1status = 1;


                    }
                    else
                    {
                        _document1_edit[ii].u1status = 2;
                    }



                    ii++;

                }
                _datama.BoxMa1_Online_List = _document1_edit;

                // litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));


                _datama = callServiceMA(urlUpdate_AdminEdit, _datama);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "btnguide":


                Response.Write("<script>window.open('https://docs.google.com/document/d/1mtWYsrOvQE7HOyAA4hxPwqOZNpqYMTh23JBK6eijkBs/edit?usp=sharing','_blank');</script>");

                break;


            case "btnflow":

                Response.Write("<script>window.open('http://mas.taokaenoi.co.th/images/ma-online/FlowChart.jpg','_blank');</script>");

                break;
        }
    }
    #endregion
}